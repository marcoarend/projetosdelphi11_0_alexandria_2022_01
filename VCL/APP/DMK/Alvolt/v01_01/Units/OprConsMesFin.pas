unit OprConsMesFin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, DBCtrls, dmkLabel, Mask, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants, dmkValUsu, dmkImage,
  UnDmkEnums, ZAbstractRODataset, ZAbstractDataset, ZDataset, UnGrl_DmkDB,
  Vcl.ComCtrls, dmkEditDateTimePicker, dmkRadioGroup;

type
  TFmOprConsMesFin = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    EdControle: TdmkEdit;
    DBEdAnoRef: TdmkDBEdit;
    Label6: TLabel;
    DBEdMesRef: TdmkDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdNome: TdmkEdit;
    TPProdDta: TdmkEditDateTimePicker;
    Label53: TLabel;
    EdProdDta: TdmkEdit;
    LaValor: TLabel;
    EdValor: TdmkEdit;
    RGKindLct: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGKindLctClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenOprConsMesFin(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TZQuery;
    FDsCab: TDataSource;
  end;

  var
  FmOprConsMesFin: TFmOprConsMesFin;

implementation

uses UnMyObjects, Module, UnInternalConsts, DmkDAC_PF, OprConsMes;

{$R *.DFM}

procedure TFmOprConsMesFin.BtOKClick(Sender: TObject);
var
  ProdDta, Nome: String;
  Codigo, AnoRef, MesRef, Controle, KindLct: Integer;
  ValrCred, ValrDebi, ValrInvt: Double;
  SQLType: TSQLType;
begin
  SQLType             := ImgTipo.SQLType;
  //
  Codigo              := Geral.IMV(DBEdCodigo.Text);
  AnoRef              := Geral.IMV(DBEdAnoRef.Text);
  MesRef              := Geral.IMV(DBEdMesRef.Text);
  Controle            := EdControle.ValueVariant;
  ProdDta             := Geral.FDT_TP_Ed(TPProdDta.Date, EdProdDta.Text);
  Nome                := EdNome.Text;
  KindLct             := RGKindLct.ItemIndex;
  ValrCred            := 0;
  ValrDebi            := 0;
  ValrInvt            := 0;
  case KindLct of
    1: ValrCred := EdValor.ValueVariant;
    2: ValrDebi := EdValor.ValueVariant;
    3: ValrInvt := EdValor.ValueVariant;
  end;
  //
  if MyObjects.FIC(KindLct < 1, RGKindLct, 'Informe o tipo de lan�amento!') then
    Exit;
  if MyObjects.FIC(EdValor.ValueVariant = 0, EdValor, 'Informe o valor!')
    then Exit;
  //
  Controle := Grl_DmkDB.GetNxtCodigoInt('oprconsmesfin', 'Controle', SQLType, Controle);
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, 'oprconsmesfin', False, [
  'Codigo', 'ProdDta', 'Nome',
  'KindLct', 'ValrCred', 'ValrDebi',
  'ValrInvt'], [
  'Controle', 'AnoRef', 'MesRef'], [
  Codigo, ProdDta, Nome,
  KindLct, ValrCred, ValrDebi,
  ValrInvt
  ], [
  Controle, AnoRef, MesRef], True, dmksqlinsInsOnly, '', TDeviceType.stDesktop, False) then
  begin
    FmOprConsMes.AtualizaSomas(Codigo, AnoRef, MesRef);
    ReopenOprConsMesFin(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;

      //TPProdDta.Date           := ;
      //EdProdDta.Text           :=
      //EdNome.Text              := ;
      //EdValrCred.ValueVariant    := ;
      //EdValrDebi.ValueVariant    := ;
      //
      //if RGKind.Focused then
        //RGKind.SetFocus;
      Geral.MB_Info('Lan�amento inclu�do com sucesso!');
    end else Close;
  end;
end;

procedure TFmOprConsMesFin.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOprConsMesFin.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdAnoRef.DataSource := FDsCab;
  DBEdMesRef.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmOprConsMesFin.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  //
  Agora := Now();
  TPProdDta.Date := Agora;
  EdProdDta.ValueVariant := Agora;
end;

procedure TFmOprConsMesFin.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOprConsMesFin.ReopenOprConsMesFin(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    Grl_DmkDB.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmOprConsMesFin.RGKindLctClick(Sender: TObject);
begin
  LaValor.Caption := RGKindLct.Items[RGKindLct.ItemIndex];
end;

end.
