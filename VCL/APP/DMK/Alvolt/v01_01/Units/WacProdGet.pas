unit WacProdGet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  VCLTee.TeEngine, VCLTee.Series, VCLTee.TeeProcs, VCLTee.Chart, VCLTee.TeCanvas,
  //mySQLDbTables,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, Vcl.ComCtrls, dmkDBGridZTO,
  dmkImage, Variants, TypInfo,
  //VCLTee.TeEngine, VCLTee.TeeProcs, VCLTee.Chart,  VCLTee.Series,
  UnDevicesPF, UnInternalConsts, UnProjGroup_Vars, AdvGlowButton;
  {

  DBCtrls, ComCtrls, dmkCheckBox, dmkEdit, PlanRecurrEdit,
  ZAbstractConnection, ZConnection, ZCompatibility, ZAbstractRODataset,
  ZAbstractDataset, ZDataset, uLkJSON,
  System.Generics.Collections, dmkDBGridZTO, Vcl.Menus, mySQLDbTables,
  //
  Vcl.ButtonGroup, AdvSmoothPanel, AdvSmoothExpanderPanel,
  AdvSmoothExpanderButtonPanel, AdvToolBtn, AdvGlassButton, AdvGlowButton}

type
  TFmWacProdGet = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PB1: TProgressBar;
    PB2: TProgressBar;
    QrAnos: TZQuery;
    DsAnos: TDataSource;
    QrAnosQuantia: TFloatField;
    QrAnosANO: TLargeintField;
    QrMeses: TZQuery;
    DsMeses: TDataSource;
    QrMesesQuantia: TFloatField;
    QrMesesMES: TLargeintField;
    QrDias: TZQuery;
    DsDias: TDataSource;
    QrDiasQuantia: TFloatField;
    QrDiasDIA: TLargeintField;
    QrInstantes: TZQuery;
    DsInstantes: TDataSource;
    QrInstantesQuantia: TFloatField;
    QrInstantesHora: TWideMemoField;
    QrTotal: TZQuery;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    Panel6: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    dmkDBGridZTO1: TdmkDBGridZTO;
    dmkDBGridZTO2: TdmkDBGridZTO;
    dmkDBGridZTO3: TdmkDBGridZTO;
    Panel7: TPanel;
    LaData: TLabel;
    DBGInstante: TdmkDBGridZTO;
    PnFrx: TPanel;
    LBAvisos: TListBox;
    QrChart: TZQuery;
    Label1: TLabel;
    Chart1: TChart;
    Series1: TBarSeries;
    AGBDia: TAdvGlowButton;
    AGBMes: TAdvGlowButton;
    AdvGlowButton2: TAdvGlowButton;
    AdvGlowButton3: TAdvGlowButton;
    AdvGlowButton4: TAdvGlowButton;
    AdvGlowButton1: TAdvGlowButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrAnosBeforeClose(DataSet: TDataSet);
    procedure QrAnosAfterScroll(DataSet: TDataSet);
    procedure QrMesesBeforeClose(DataSet: TDataSet);
    procedure QrMesesAfterScroll(DataSet: TDataSet);
    procedure QrDiasAfterScroll(DataSet: TDataSet);
    procedure QrDiasBeforeClose(DataSet: TDataSet);
    procedure QrInstantesHoraGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrTotalBeforeClose(DataSet: TDataSet);
    procedure QrTotalAfterScroll(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure QrTotalAfterOpen(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Chart1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FSeries: Integer;

    FYCount: Integer;
    FYValues: array of array of String;
    FTitles: array of String;
    FXSource: String;
    //
    FRecorrencia: String;
    procedure MostraEdicao(Codigo: Integer);
    //
    procedure ConfiguraChart();
    procedure ConfiguraSerieArea(ChartSerie: TChartSeries);
    procedure ConfiguraSerieBar(ChartSerie: TChartSeries);

    procedure MostraGraficoDias();
    procedure MostraGraficoMomento();

    procedure ReopenAnos();
    procedure ReopenMeses();
    procedure ReopenDias();
    procedure ReopenInstantes();
    function  ReopenTotal(): Boolean;

    function  ObtemObjeto(const AObject: TObject; const Nome: String; const
              Classe: TClass; var Objeto: TObject): Boolean;
    function  SVP_O(Compo: TObject; Propriede: String; Valor: Variant): Boolean;
    function  SVP_Objeto(const AObject: TObject; const Nome: String; const
              Classe: TClass; const Propriedade: String; const Valor: Variant):
              Boolean;
    procedure SVP_Primit(Compo: TComponent; Propriedade: String; Valor: Variant);

  public
    { Public declarations }
    FCodigo: Integer;
    FQuery: TZQuery;
  end;

  var
  FmWacProdGet: TFmWacProdGet;

implementation

uses UnMyObjects, Module, UnGrl_DmkDB, UnDmkEnums, UnDmkProcFunc, UnMyJSON,
  UnGrl_Geral;

{$R *.DFM}

const
  myclChartFramTodos = $00EEEEED; // 237-238-238
  myclChartAreaFundo = clWhite;
  //
  myclSerieFramLinVI = $00EEEEED; // 237-238-238
  myclSerieAreaLinEx = $00AAA8A6; // 166-168-170
  myclSerieAreaAreas = $00C0BEBC; // 188-190-192
  myclSerieAreaLinVI = $00C0BEBC; // 00B3B1AF; // 175-177-179
  myclSerieAreaLinHI = $00BBB9B7; // 183-185-187
  myclSerieAreaLinHE = $00EDEDEC; // 236-237-237

var
  VlrInstantes: array[0..287] of Variant;
  VlrDiasMes: array[0..30] of Variant;

procedure TFmWacProdGet.BtOKClick(Sender: TObject);
begin
  LBAvisos.Align := alClient;
  LBAvisos.Visible := True;
  Chart1.Visible := False;
  //
  DevicesPF.ObtemProducao(VAR_DeviceAssembler, QrTotal, LaAviso1, LaAviso2, PB1,
    LBAvisos);
end;

procedure TFmWacProdGet.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWacProdGet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //Chart1.Color := myclChartAreaFundo;
end;

procedure TFmWacProdGet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QrTotal.Close;
end;

procedure TFmWacProdGet.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  if not ReopenTotal() then
    ReopenTotal();
  //
  LBAvisos.Align := alClient;
  //
  //ConfiguraChart();
  //AGBDia.Down := True;
end;

procedure TFmWacProdGet.FormDestroy(Sender: TObject);
var
  Texto: String;
begin
  try
    Chart1.FreeAllSeries();
    Chart1.Free;
    Chart1 := nil;
  except
    on E: Exception do
    begin
      Texto := E.Message + sLineBreak;
      Geral.MB_Erro(Texto);
    end;
  end;
end;

procedure TFmWacProdGet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWacProdGet.FormShow(Sender: TObject);
begin
  ////ConfiguraChart();
  //DBGInstante.SetFocus;
  ////ConfiguraChart();
  //AGBDiaClick(AGBDia);
end;

procedure TFmWacProdGet.MostraEdicao(Codigo: Integer);
begin
//
end;

procedure TFmWacProdGet.MostraGraficoDias();
var
  I, H: integer;
  ChartSerie: TChartSeries;
  DataI, DataF: TDateTime;
  sDataI, SDataF: String;
begin
  DataI  := EncodeDate(QrAnosAno.Value, QrMesesMES.Value, 1);
  DataF  := IncMonth(DataI, 1) - 1;
  sDataI := Geral.FDT(DataI, 1);
  sDataF := Geral.FDT(DataF, 1);
  while Chart1.SeriesCount > 0 do
    Chart1.Series[0].Free;
  Chart1.Visible := True;
  //
  Chart1.Title.Text.Text := Geral.FDT(DataI, 7);
  //
  ChartSerie := TBarSeries.Create(Chart1);
  ConfiguraSerieArea(ChartSerie);
  //
  Chart1.AddSeries(ChartSerie);

  // do VCLTee.Chart.CustomChart:
  ChartSerie.Pen.Width := 1;
  ChartSerie.LegendTitle := 'M�s';

  // 24 h de 5 em 5 min
  for I := Low(VlrDiasMes) to High(VlrDiasMes) do
    VlrDiasMes[I] := Null;
  //QrChart.SQL.Text := QrInstantes.SQL.Text;
  //Grl_DmkDB.AbreQuery(QrChart, Dmod.MyDB);
  Grl_DmkDB.AbreSQLQuery0(QrChart, Dmod.MyDB, [
  'SELECT strftime("%d", ProdDta)  DiaMes, ',
  'Quantia ',
  //'SUM(Quantia) / 1000 Quantia ',
  'FROM wacprodvalues  ',
  'WHERE ProdDta  ',
  'BETWEEN "' + sDataI + '" AND "' + sDataF + ' 23:59:59"  ',
  'GROUP BY DiaMes',
  'ORDER BY DiaMes ',
  '']);
  //
  QrChart.First;
  while not QrChart.Eof do
  begin
    I := Trunc(QrChart.FieldByName('DiaMes').AsFloat) - 1;
    VlrDiasMes[I] := QrChart.FieldByName('Quantia').AsFloat;
    //
    QrChart.Next;
  end;
  ChartSerie.DataSources.Clear;
  for I := Low(VlrDiasMes) to High(VlrDiasMes) do
  begin
(*
    H := (I div 12);
    if H = (I / 12) then
    begin
      if VlrDiasMes[I] = Null then
        ChartSerie.AddX(I, Geral.FF0(H) + ':00')
      else
        ChartSerie.AddXY(I, VlrDiasMes[I] * 12, Geral.FF0(H) + ':00')
    end else
*)
    begin
      if VlrDiasMes[I] = Null then
        ChartSerie.AddNull(I)
      else
        ChartSerie.AddXY(I, VlrDiasMes[I] / 1000);
    end;
  end;
end;

procedure TFmWacProdGet.MostraGraficoMomento();
var
  I, H: integer;
  ChartSerie: TChartSeries;
  Data: TDateTime;
  sData: String;
begin
  //Chart1.Color := clWhite;
  Data  := EncodeDate(QrAnosAno.Value, QrMesesMES.Value, QrDiasDia.Value);
  sData := Geral.FDT(Data, 1);
  while Chart1.SeriesCount > 0 do
    Chart1.Series[0].Free;
  Chart1.Visible := True;
  //
  Chart1.Title.Text.Text := Geral.FDT(Data, 2);
  //
  ChartSerie := TAreaSeries.Create(Chart1);
  ConfiguraSerieArea(ChartSerie);
  //
  Chart1.AddSeries(ChartSerie);

  // do VCLTee.Chart.CustomChart:
  ChartSerie.Pen.Width := 1;
  ChartSerie.LegendTitle := 'Dia';

  // 24 h de 5 em 5 min
  for I := Low(VlrInstantes) to High(VlrInstantes) do
    VlrInstantes[I] := Null;
  //QrChart.SQL.Text := QrInstantes.SQL.Text;
  //Grl_DmkDB.AbreQuery(QrChart, Dmod.MyDB);
  Grl_DmkDB.AbreSQLQuery0(QrChart, Dmod.MyDB, [
  'SELECT (strftime("%H", ProdDta) * 12) + ( ',
  'strftime("%M", ProdDta) / 5) MinFive, Quantia ',
  'FROM wacprodvalues  ',
  'WHERE ProdDta  ',
  'BETWEEN "' + sData + '" AND "' + sData + ' 23:59:59"  ',
  'ORDER BY MinFive ',
  '']);
  //
  QrChart.First;
  while not QrChart.Eof do
  begin
    I := Trunc(QrChart.FieldByName('MinFive').AsFloat);
    VlrInstantes[I] := QrChart.FieldByName('Quantia').AsFloat;
    //
    QrChart.Next;
  end;
  ChartSerie.DataSources.Clear;
  for I := Low(VlrInstantes) to High(VlrInstantes) do
  begin
    H := (I div 12);
    if H = (I / 12) then
    begin
      if VlrInstantes[I] = Null then
        ChartSerie.AddX(I, Geral.FF0(H) + ':00')
      else
        ChartSerie.AddXY(I, VlrInstantes[I] * 12, Geral.FF0(H) + ':00')
    end else
    begin
      if VlrInstantes[I] = Null then
        ChartSerie.AddNull(I)
      else
        ChartSerie.AddXY(I, VlrInstantes[I] * 12);
    end;
  end;
  Chart1.LeftAxis.Maximum := 4500;
{
  ChartSerie.AddXY(0, 1);
  ChartSerie.AddXY(1, 31.5);
  ChartSerie.AddXY(2, 28.54);
  ChartSerie.AddXY(3, 31.58);
  ChartSerie.AddNull(4)
}
end;

function TFmWacProdGet.ObtemObjeto(const AObject: TObject; const Nome: String;
  const Classe: TClass; var Objeto: TObject): Boolean;
var
  I, Count: Integer;
  PropInfo: PPropInfo;
  TempList: PPropList;
  LObject: TObject;
begin
  Result := False;
  Objeto := nil;
  Count := GetPropList(AObject, TempList);
  if Count > 0 then
  try
    for I := 0 to Count - 1 do
    begin
      PropInfo := TempList^[I];
      if GetPropName(PropInfo) = Nome then
      begin
        //Geral.MB_Info('Nome PropInfo: ' + GetPropName(PropInfo));
        if (PropInfo^.PropType^.Kind = tkClass) and
        Assigned(PropInfo^.GetProc) and
        Assigned(PropInfo^.SetProc) then
        begin
          LObject := GetObjectProp(AObject, PropInfo);
          if LObject <> nil then
          begin
            Objeto := LObject;
            Result := True;
            //
            Exit;
          end;
        end;
      end;
    end;
  finally
    FreeMem(TempList);
  end;
end;

procedure TFmWacProdGet.QrAnosAfterScroll(DataSet: TDataSet);
begin
  ReopenMeses();
  QrMeses.Last;
end;

procedure TFmWacProdGet.QrAnosBeforeClose(DataSet: TDataSet);
begin
  QrMeses.Close;
end;

procedure TFmWacProdGet.QrDiasAfterScroll(DataSet: TDataSet);
begin
  ReopenInstantes();
  QrInstantes.Last;
end;

procedure TFmWacProdGet.QrDiasBeforeClose(DataSet: TDataSet);
begin
  QrInstantes.Close;
end;

procedure TFmWacProdGet.QrInstantesHoraGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

procedure TFmWacProdGet.QrMesesAfterScroll(DataSet: TDataSet);
begin
  ReopenDias();
  QrDias.Last;
end;

procedure TFmWacProdGet.QrMesesBeforeClose(DataSet: TDataSet);
begin
  QrDias.Close;
end;

procedure TFmWacProdGet.QrTotalAfterOpen(DataSet: TDataSet);
var
  Quantia: Double;
  DiaZero: TDateTime;
begin
  if Lowercase(QrTotal.FieldByName('TypeOf').AsString) <> 'null' then
  begin
    Quantia := QrTotal.FieldByName('Quantia').AsFloat;
    Self.Caption := 'WAC-PRODU-001 :: Produ��o Total Real WAC de ' +
    Geral.FFT(Quantia, 2, siNegativo) + ' kWh';
    Self.Resizing(Self.WindowState);
    //
    ReopenAnos();
  end else
  begin
    DiaZero := EncodeDate(2000, 1, 1);
    if MyObjects.ObtemData((*DtDefault*) Date(), (*DtSelect*) DiaZero,
    (*MinData*)DiaZero, (*HrDefault*)0, (*HabilitaHora*)False,
    'Instala��o Sistema',
    'Aten��o: Informe a data que o sistema fotovoltaico entrou em funcionamento '
    + 'para que todos dados de gera��o anteriores a data atual possam ser importados!') then
    begin
      //if VAR_GETDATA = 0 then Exit;
      //
      //DiaZero := VAR_GETDATA;
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, stIns, 'wacprodvalues', False, [
      'ProdDta', 'Quantia'], [
      ], [
      DiaZero, 0], [
      ], True, dmksqlinsInsOnly, '', stDesktop);
    end;
    //
    QrTotal.Close;
  end;
end;

procedure TFmWacProdGet.QrTotalAfterScroll(DataSet: TDataSet);
begin
  ReopenAnos();
  QrAnos.Last;
end;

procedure TFmWacProdGet.QrTotalBeforeClose(DataSet: TDataSet);
begin
  QrAnos.Close;
end;

procedure TFmWacProdGet.ReopenAnos();
begin
  Grl_DmkDB.AbreSQLQuery0(QrAnos, Dmod.MyDB, [
  'SELECT CAST(strftime("%Y", ProdDta) as integer) ANO, ',
  'SUM(Quantia)/1000 Quantia ',
  'FROM wacprodvalues ',
  'GROUP BY ANO ',
  'ORDER BY ANO desc ',
  '']);
end;

procedure TFmWacProdGet.ReopenDias();
var
  AnoMes: String;
begin
  //Dias do M�ses selecionado
  AnoMes := Geral.FF0((QrAnosAno.Value* 100) + QrMesesMES.Value);
  Grl_DmkDB.AbreSQLQuery0(QrDias, Dmod.MyDB, [
  'SELECT CAST(strftime("%d", ProdDta) as integer) DIA, ',
  'SUM(Quantia)/1000 Quantia ',
  'FROM wacprodvalues ',
  'WHERE strftime("%Y%m", ProdDta) = "' + AnoMes + '" ',
  'GROUP BY DIA ',
  'ORDER BY DIA ',
  '']);
end;

procedure TFmWacProdGet.ReopenInstantes();
var
  Data: TDateTime;
  sData: String;
  Ano, Mes, Dia: Word;
  N: Integer;
begin
  //Dias do M�ses selecionado
  Data  := EncodeDate(QrAnosAno.Value, QrMesesMES.Value, QrDiasDia.Value);
  case Trunc(Date()) - Trunc(Data) of
    0: sData := ' - HOJE';
    1: sData := ' - Ontem';
    2: sData := ' - Anteontem';
    3: sData := '';
  end;
  sData := Geral.FDT(Data, 2) + sData;
  LaData.Caption :=  sData;
  //
  sData := Geral.FDT(Data, 1);
  Grl_DmkDB.AbreSQLQuery0(QrInstantes, Dmod.MyDB, [
  'SELECT strftime("%H:%M", ProdDta) Hora, Quantia',
  //'SELECT TIME(ProdDta) Hora, Quantia',
  'FROM wacprodvalues ',
  'WHERE ProdDta ',
  'BETWEEN "' + sData + '" AND "' + sData + ' 23:59:59" ',
  'ORDER BY Hora',
  '']);
end;

procedure TFmWacProdGet.ReopenMeses;
begin
  //Meses do ano selecionado
  Grl_DmkDB.AbreSQLQuery0(QrMeses, Dmod.MyDB, [
  'SELECT CAST(strftime("%m", ProdDta) as integer) MES, ',
  'SUM(Quantia)/1000 Quantia ',
  'FROM wacprodvalues ',
  'WHERE strftime("%Y", ProdDta) = "' + Geral.FF0(QrAnosAno.Value) + '" ',
  'GROUP BY MES ',
  'ORDER BY MES ',
  '']);
end;

function TFmWacProdGet.ReopenTotal(): Boolean;
begin
  Result := Grl_DmkDB.AbreSQLQuery0(QrTotal, Dmod.MyDB, [
  'SELECT TypeOf(SUM(Quantia)) TypeOf,  SUM(Quantia) / 1000 Quantia ',
  'FROM wacprodvalues ',
  '']);
end;

function TFmWacProdGet.SVP_O(Compo: TObject; Propriede: String;
  Valor: Variant): Boolean;
var
  PropInfo: PPropInfo;
begin
  Result := False;
  if Compo <> nil then
  begin
    PropInfo := GetPropInfo(Compo, Propriede);
    if PropInfo <> nil then
    begin
      SetPropValue(Compo, Propriede, Valor);
      Result := Valor;
    end;
  end;
end;

function TFmWacProdGet.SVP_Objeto(const AObject: TObject; const Nome: String;
  const Classe: TClass; const Propriedade: String;
  const Valor: Variant): Boolean;
var
  Objeto: TObject;
begin
  Result := False;
  if ObtemObjeto(AObject, Nome, Classe, Objeto) then
    Result := SVP_O(Objeto, Propriedade, Valor);
end;

procedure TFmWacProdGet.SVP_Primit(Compo: TComponent; Propriedade: String;
  Valor: Variant);
var
  PropInfo: PPropInfo;
begin
  if Compo <> nil then
  begin
    PropInfo := GetPropInfo(Compo, Propriedade);
    if PropInfo <> nil then
      SetPropValue(Compo, Propriedade, Valor)
  end;
end;

procedure TFmWacProdGet.Chart1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  iSeries, I: Integer;
  i5min, Posic: Double;
var
  Ypos, XPos: Integer;
begin
  if Chart1.Axes.Top.IAxisSize > 0 then
  begin
    i5min := (X - Chart1.Axes.Top.IStartPos) / Chart1.Axes.Top.IAxisSize;
    i5min := Round(i5min * (12 * 24));
    Posic := i5min / (12 * 24);
  end else
  begin
    Posic := 0;
  end;
  Label1.Caption := FormatDateTime('hh:nn', Posic);
  //
  with Chart1, Chart1.Canvas do
  begin
    //DoubleBuffered := True;
    //
    Refresh;
    //
    if (X < ChartRect.Left)
    or (X > ChartRect.Right)
    or (Y < ChartRect.Top)
    or (Y > ChartRect.Bottom) then
      Exit;
    //Ypos := Series[ 0 ].CalcYPosValue( MinValue );
    Ypos := Y;
    Pen.Color := myclSerieAreaLinEx;
    Pen.Width := 1;
    Pen.Style := psSolid;
(*
    Geral.MB_Info('CRL: ' + Geral.FF0(ChartRect.Left) +
    '  CRR: ' + Geral.FF0(ChartRect.Right));
*)
    MoveTo( ChartRect.Left, YPos );
    LineTo( ChartRect.Right, YPos );
    //
    //Xpos := Series[ 0 ].CalcXposValue( MaxValue );
    Xpos := X;
    Pen.Color := myclSerieAreaLinEx;
    Pen.Width := 1;
    Pen.Style := psSolid;
    MoveTo(XPos, ChartRect.Top);
    LineTo(Xpos, ChartRect.Bottom);
    //

  end;
{
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'No series under the mouse');
  //
  for iSeries := Chart1.SeriesCount - 1 downto 0 do
  begin
    I := Chart1[iSeries].Clicked(X,Y);
    if I > -1 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'Series under the mouse. iSeries: ' + IntToStr(iSeries) +
      ', I: ' + IntToStr(I) +
      ' Hora: ' + FormatDateTime('hh:nn', (I * 300) / 86400));
    end else
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,

      'X : ' + IntToStr(X) +
      ', Y: ' + IntToStr(Y) +
      ' Width: ' + Geral.FF0(Chart1.ChartWidth) +
      ' Height: ' + Geral.FF0(Chart1.ChartHeight) +
      ' XCenter: ' + Geral.FF0(Chart1.ChartXCenter) +
      ' YCenter: ' + Geral.FF0(Chart1.ChartYCenter) +
      ' X ini: ' + Geral.FF0(Chart1.Axes.Left.IStartPos) +
      ' X fim: ' + Geral.FF0(Chart1.Axes.Left.IEndPos) +
      ' X Width: ' + Geral.FF0(Chart1.Axes.Left.IAxisSize) +
      ' Y ini: ' + Geral.FF0(Chart1.Axes.Top.IStartPos) +
      ' Y fim: ' + Geral.FF0(Chart1.Axes.Top.IEndPos) +
      ' Y Width: ' + Geral.FF0(Chart1.Axes.Top.IAxisSize));

    end;
  end;
}
end;

procedure TFmWacProdGet.ConfiguraChart();
var
  I, J: Integer;
  YSource: String;
  iSerie, ChartSerie: TChartSeries;
begin
  //Chart1.MouseMove := Chart1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  // Remover series (gr�ficos)
  while Chart1.SeriesCount > 0 do
    Chart1.Series[0].Free;
  // Desfazer 3D
  Chart1.View3D := False;
  // Tirar legendas do lado
  Chart1.Legend.Visible := False;
  Chart1.Title.Font.Size := 14;
  Chart1.Title.Font.Name := 'Calibri';
  Chart1.Title.Font.Color := VAR_COR_TIT_C;

  // Configurar Gr�fico em si (sem nenhuma s�rie - fundo vazio)
  Chart1.Color := myclChartAreaFundo;
  Chart1.Frame.Color := myclChartFramTodos;

  Chart1.LeftAxis.Axis.Color := clSilver;
  Chart1.LeftAxis.Axis.Width := 1;
  Chart1.LeftAxis.Grid.Color     := myclSerieAreaLinHE;
  Chart1.LeftAxis.Grid.DrawEvery := 3; //
  Chart1.LeftAxis.Grid.Style     := TPenStyle.psSolid;

  Chart1.BottomAxis.Axis.Color := clSilver;
  Chart1.BottomAxis.Axis.Width := 1;
  Chart1.BottomAxis.MinorTicks.Visible := False;
  Chart1.BottomAxis.Grid.Visible   := True;
  Chart1.BottomAxis.Grid.DrawEvery := 1;
  Chart1.BottomAxis.Grid.Color     := myclSerieAreaLinHE;
  Chart1.BottomAxis.Grid.Style     := TPenStyle.psSolid;

  //Chart1.BottomAxis.Grid.Color     := 12;
  Chart1.BottomAxis.Ticks.Color    := myclSerieAreaLinVI;
  Chart1.BottomAxis.Ticks.Width    := 1;



  Chart1.TopAxis.Axis.Color := clWhite;
  Chart1.TopAxis.Axis.Width := 1;

  Chart1.RightAxis.Axis.Color := clWhite;
  Chart1.RightAxis.Axis.Width := 1;

 //
  // Evitar erro no Destroy;
  iSerie := nil;
  ChartSerie := nil;
end;

procedure TFmWacProdGet.ConfiguraSerieArea(ChartSerie: TChartSeries);
begin
  //Chart1.Color := myclChartAreaFundo;
  //
  SVP_Objeto(ChartSerie,  'LinePen', TChartPen, 'Color', myclSerieAreaLinEx);
  SVP_Objeto(ChartSerie,  'LinePen', TChartPen, 'Width', 2);
  // (B) - Area do gr�fico �rea
  SVP_Primit(ChartSerie, 'AreaColor', myclSerieAreaAreas);
  // (C) - Linhas verticais dentro da �rea ocupada do gr�fico �rea
  SVP_Objeto(ChartSerie,  'AreaLinesPen', TChartPen, 'Color', myclSerieAreaLinVI);
  SVP_Objeto(ChartSerie,  'AreaLinesPen', TChartPen, 'Width', 1);

end;

procedure TFmWacProdGet.ConfiguraSerieBar(ChartSerie: TChartSeries);
begin
//
end;

end.
