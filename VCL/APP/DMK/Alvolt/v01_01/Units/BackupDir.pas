unit BackupDir;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkCheckBox, dmkEdit, PlanRecurrEdit,
  ZAbstractConnection, ZConnection, ZCompatibility, ZAbstractRODataset,
  ZAbstractDataset, ZDataset;

type
  TFmBackupDir = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label32: TLabel;
    EdOrigem: TdmkEdit;
    SBOrigem: TSpeedButton;
    Label1: TLabel;
    EdDestino: TdmkEdit;
    SBDestino: TSpeedButton;
    GBOpcoes: TGroupBox;
    CkZipar: TdmkCheckBox;
    CkSubDir: TdmkCheckBox;
    EdExtOmit: TdmkEdit;
    CkExtOmit: TdmkCheckBox;
    BtRepeticoes: TBitBtn;
    RERecorrencia: TPlannerRecurrencyEditor;
    GBZip: TGroupBox;
    EdSenha: TdmkEdit;
    LaSenha: TLabel;
    CkEncryptar: TdmkCheckBox;
    EdDias: TdmkEdit;
    LaAviso: TLabel;
    LaDias: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBOrigemClick(Sender: TObject);
    procedure SBDestinoClick(Sender: TObject);
    procedure CkZiparClick(Sender: TObject);
    procedure CkEncryptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkExtOmitClick(Sender: TObject);
    procedure BtRepeticoesClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FRecorrencia: String;
    procedure SelecionaDiretorio(dmkEdit: TdmkEdit);
    procedure MostraEdicao(Codigo: Integer);
    procedure MostraRecorrencia();
  public
    { Public declarations }
    FCodigo: Integer;
    FQuery: TZQuery;
  end;

  var
  FmBackupDir: TFmBackupDir;

implementation

uses UnMyObjects, Module, UnGrl_DmkDB, UnDmkEnums, UnitAgendaAux, UnDmkProcFunc;

{$R *.DFM}

procedure TFmBackupDir.BtOKClick(Sender: TObject);
var
  Codigo, SubDir, Zipar, Dias, Encrypt, OmitirExt: Integer;
  Origem, Destino, SenhaCrypt, ExtOmit, Recorrencia: String;
  SQLTipo: TSQLType;
begin
  Origem  := EdOrigem.ValueVariant;
  Destino := EdDestino.ValueVariant;
  Dias    := EdDias.ValueVariant;
  //
  if MyObjects.FIC(Origem = '', EdOrigem, 'Diret�rio origem n�o definido!') then Exit;
  if MyObjects.FIC(Destino = '', EdDestino, 'Diret�rio destino n�o definido!') then Exit;
  //
  SubDir := Geral.BoolToInt(CkSubDir.Checked);
  Zipar  := Geral.BoolToInt(CkZipar.Checked);
  //
  if Zipar = 1 then
  begin
    Encrypt    := Geral.BoolToInt(CkEncryptar.Checked);
    SenhaCrypt := EdSenha.ValueVariant;
    //
    if MyObjects.FIC((Encrypt = 1) and (SenhaCrypt = ''), EdSenha, 'Senha n�o definida!') then Exit;
  end else
  begin
    Encrypt    := 0;
    SenhaCrypt := '';
  end;
  OmitirExt := Geral.BoolToInt(CkExtOmit.Checked);
  //
  if OmitirExt = 1  then
  begin
    ExtOmit := EdExtOmit.ValueVariant;
    //
    if MyObjects.FIC(ExtOmit = '', EdExtOmit, 'Extens�es a omitir n�o definidas!') then Exit;
  end else
    ExtOmit := '';
  //
  Recorrencia := FRecorrencia;
  //
  if FCodigo = 0 then
  begin
    Codigo  := Grl_DmkDB.ObtemCodigoInt('backupdir', 'Codigo', Dmod.QrAux, Dmod.MyDB);
    SQLTipo := stIns;
  end else
  begin
    Codigo  := FCodigo;
    SQLTipo := stUpd;
  end;
  if Grl_DmkDB.CarregaSQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLTipo, 'backupdir', False,
    ['Origem', 'Destino', 'SubDir', 'Zipar', 'Encrypt',
     'SenhaCrypt', 'OmitirExt', 'ExtOmit', 'Dias', 'Recorrencia'], ['Codigo'],
    [Origem, Destino, SubDir, Zipar, Encrypt,
     SenhaCrypt, OmitirExt, ExtOmit, Dias, Recorrencia], [Codigo],
     False, False, '', stDesktop) then
  begin
    Close;
  end;
end;

procedure TFmBackupDir.BtRepeticoesClick(Sender: TObject);
begin
  MostraRecorrencia;
end;

procedure TFmBackupDir.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBackupDir.CkEncryptarClick(Sender: TObject);
var
  Visi: Boolean;
begin
  Visi := CkEncryptar.Checked;
  //
  LaSenha.Visible := Visi;
  EdSenha.Visible := Visi;
end;

procedure TFmBackupDir.CkExtOmitClick(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := CkExtOmit.Checked;
  //
  EdExtOmit.Enabled := Enab;
end;

procedure TFmBackupDir.CkZiparClick(Sender: TObject);
begin
  GBZip.Visible := CkZipar.Checked;
end;

procedure TFmBackupDir.EdSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
    EdSenha.ValueVariant := VAR_BDSENHA;
end;

procedure TFmBackupDir.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBackupDir.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnAgendaAux.ConfiguraRecorrencia(RERecorrencia);
end;

procedure TFmBackupDir.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBackupDir.FormShow(Sender: TObject);
begin
  MostraEdicao(FCodigo);
end;

procedure TFmBackupDir.MostraEdicao(Codigo: Integer);
begin
  if Codigo = 0 then
  begin
    EdOrigem.ValueVariant  := '';
    EdDestino.ValueVariant := '';
    CkSubDir.Checked       := True;
    CkZipar.Checked        := False;
    CkEncryptar.Checked    := False;
    CkExtOmit.Checked      := True;
    EdExtOmit.ValueVariant := '.~*; .dcu; .exe; .map';
    EdDias.ValueVariant    := 0;
    //
    FRecorrencia := '';
    //
    GBZip.Visible   := False;
    LaSenha.Visible := False;
    EdSenha.Visible := False;
    //
    EdExtOmit.Enabled := True;
  end else
  begin
    EdOrigem.ValueVariant  := dmkPF.SimplificaBarras(FQuery.FieldByName('Origem').AsString);
    EdDestino.ValueVariant := dmkPF.SimplificaBarras(FQuery.FieldByName('Destino').AsString);
    CkSubDir.Checked       := Geral.IntToBool(FQuery.FieldByName('SubDir').AsInteger);
    CkZipar.Checked        := Geral.IntToBool(FQuery.FieldByName('Zipar').AsInteger);
    EdDias.ValueVariant    := FQuery.FieldByName('Dias').AsInteger;
    CkEncryptar.Checked    := Geral.IntToBool(FQuery.FieldByName('Encrypt').AsInteger);
    EdSenha.ValueVariant   := FQuery.FieldByName('SenhaCrypt').AsString;
    CkExtOmit.Checked      := Geral.IntToBool(FQuery.FieldByName('OmitirExt').AsInteger);
    EdExtOmit.ValueVariant := FQuery.FieldByName('ExtOmit').AsString;
    //
    FRecorrencia := FQuery.FieldByName('Recorrencia').AsString;
    //
    GBZip.Visible   := CkZipar.Checked = True;
    LaSenha.Visible := CkEncryptar.Checked = True;
    EdSenha.Visible := CkEncryptar.Checked = True;
  end;
end;

procedure TFmBackupDir.MostraRecorrencia;
begin
  RERecorrencia.Recurrency := FRecorrencia;
  //
  if RERecorrencia.Execute then
    FRecorrencia := RERecorrencia.Recurrency;
end;

procedure TFmBackupDir.SelecionaDiretorio(dmkEdit: TdmkEdit);
var
  Dir: string;
  Cam: integer;
begin
  MyObjects.DefineDiretorio(Self, dmkEdit);
  //
  Dir := dmkEdit.ValueVariant;
  //
  Cam := Length(Dir);
  if Cam > 0 then
  begin
    if Copy(Dir, Cam, 1) = '\' then
      dmkEdit.ValueVariant := Copy(Dir, 1, Cam - 1)
    else
      dmkEdit.ValueVariant := Dir;
  end;
end;

procedure TFmBackupDir.SBDestinoClick(Sender: TObject);
begin
  SelecionaDiretorio(EdDestino);
end;

procedure TFmBackupDir.SBOrigemClick(Sender: TObject);
begin
  SelecionaDiretorio(EdOrigem);
end;

end.
