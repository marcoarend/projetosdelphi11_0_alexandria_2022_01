unit UnDevicesPF;

interface

uses
  Windows, SysUtils, Classes, Menus, UnDmkEnums, mySQLDbTables, Forms, TypInfo,
  UnInternalConsts, UnProjGroup_Vars, UnMyJSON, UnAppEnums, dmkGeral,
  System.Generics.Collections, Vcl.StdCtrls, Vcl.ComCtrls, Data.DB;

type
  TUnDevicesPF = class(TObject)
  private
    { Private declarations }
    procedure AvisoAssemblerNaoImplementado(DeviceAssembler: TDevicesAssembler;
              ProcName: String);
  public
    { Public declarations }
    function  AtualizaDevicesBD(DeviceAssembler: TDevicesAssembler): Boolean;
    function  AtualizaLoggersBD(DeviceAssembler: TDevicesAssembler): Boolean;
    function  GetJsonTextFromURL(DeviceAssembler: TDevicesAssembler;
              Request: String; var JSON: String; const AvisaErro: Boolean =
              True; const LBAvisos: TListBox = nil; const UseURLBaseAPI: Boolean =
              True; const sHTTP: String = 'http://'): Boolean;
    function  ObtemDevicesAtivos(const DeviceAssembler: TDevicesAssembler; var
              Lista: TrInverters): Boolean;
    function  ObtemProducao(DeviceAssembler: TDevicesAssembler; QrTotal:
              TDataset; LaAviso1, LaAviso2: TLabel; PB: TProgressBar; LBAvisos:
              TListBox): Boolean;
    function  ObtemReal_WAC_Sum_Produced_Periodo(const DeviceAssembler:
              TDevicesAssembler; const DataI, DataF: TDateTime; LaAviso1, LaAviso2:
              TLabel; PB: TProgressBar; LBAvisos: TListBox): Boolean;
    function  ObtemUrlBaseAPI(const DeviceAssembler: TDevicesAssembler; const
              AvisaVersao: Boolean; var Res: ObtemUrlBaseAPIResult; const
              AvisaErro: Boolean; LBAvisos: TListBox): Boolean;
  end;

var
  DevicesPF: TUnDevicesPF;


implementation

uses UnGrl_DmkDB, UnDevicesPF_Fronius;

{ TUnDevicesPF }

function TUnDevicesPF.AtualizaDevicesBD(DeviceAssembler: TDevicesAssembler): Boolean;
const
  sProcName = 'TUnDevicesPF.AtualizaDevicesBD()';
begin
  Result := False;
  case TDevicesAssembler(DeviceAssembler) of
    assembFronius: Result := DevicesPF_Fronius.AtualizaDevicesBD();
    else
      AvisoAssemblerNaoImplementado(DeviceAssembler, sProcName);
  end;
end;

function TUnDevicesPF.AtualizaLoggersBD(
  DeviceAssembler: TDevicesAssembler): Boolean;
const
  sProcName = 'TUnDevicesPF.AtualizaLoggersBD()';
begin
  Result := False;
  case TDevicesAssembler(DeviceAssembler) of
    assembFronius: Result := DevicesPF_Fronius.AtualizaLoggersBD();
    else
      AvisoAssemblerNaoImplementado(DeviceAssembler, sProcName);
  end;
end;

procedure TUnDevicesPF.AvisoAssemblerNaoImplementado(
  DeviceAssembler: TDevicesAssembler; ProcName: String);
begin
  Geral.MB_Aviso('Equipamento n�o implementado: "' +
  AppEnums.NomeDevicesAssembler(
  DeviceAssembler) + '"' + sLineBreak + 'Enum: ' + Geral.FF0(Integer(
  DeviceAssembler)) + ': ' + GetEnumName(TypeInfo(TDevicesAssembler),
  Integer(DeviceAssembler)) + ')' + sLineBreak + 'ProcFunc: ' + ProcName);
end;

function TUnDevicesPF.GetJsonTextFromURL(DeviceAssembler: TDevicesAssembler;
  Request: String; var JSON: String; const AvisaErro: Boolean = True; const
  LBAvisos: TListBox = nil; const UseURLBaseAPI: Boolean = True; const sHTTP:
  String = 'http://'): Boolean;
const
  sProcName = 'TUnDevicesPF.GetJsonTextFromURL()';
var
  URL: String;
begin
  JSON := '';
  Result := False;
  case TDevicesAssembler(DeviceAssembler) of
    assembFronius: Result := DevicesPF_Fronius.GetJsonTextFromURL(Request, JSON,
                   AvisaErro, LBAvisos, UseURLBaseAPI, sHTTP);
    else
      AvisoAssemblerNaoImplementado(DeviceAssembler, sProcName);
  end;
end;

function TUnDevicesPF.ObtemDevicesAtivos(const DeviceAssembler:
  TDevicesAssembler; var Lista: TrInverters): Boolean;
const
  sProcName = 'TUnDevicesPF.ObtemDevicesAtivos()';
//var
  //URL: String;
begin
  Result := False;
  case TDevicesAssembler(DeviceAssembler) of
    assembFronius: Result := DevicesPF_Fronius.ObtemDevicesAtivos(Lista);
    else
      AvisoAssemblerNaoImplementado(DeviceAssembler, sProcName);
  end;
end;

function TUnDevicesPF.ObtemProducao(DeviceAssembler: TDevicesAssembler; QrTotal:
  TDataset; LaAviso1, LaAviso2: TLabel; PB: TProgressBar; LBAvisos: TListBox): Boolean;
const
  sProcName = 'TUnDevicesPF.ObtemProducao()';
begin
  Result := False;
  case TDevicesAssembler(DeviceAssembler) of
    assembFronius: Result := DevicesPF_Fronius.ObtemProducao(QrTotal, LaAviso1,
                   LaAviso2, PB, LBAvisos);
    else
      AvisoAssemblerNaoImplementado(DeviceAssembler, sProcName);
  end;
end;

function TUnDevicesPF.ObtemReal_WAC_Sum_Produced_Periodo(
  const DeviceAssembler: TDevicesAssembler; const DataI, DataF: TDateTime;
  LaAviso1, LaAviso2: TLabel; PB: TProgressBar; LBAvisos: TListBox): Boolean;
const
  sProcName = 'TUnDevicesPF.ObtemReal_WAC_Sum_Produced_Periodo()';
begin
  Result := False;
  case TDevicesAssembler(DeviceAssembler) of
    assembFronius: Result := DevicesPF_Fronius.ObtemReal_WAC_Sum_Produced_Periodo(
                   DataI, DataF, LaAviso1, LaAviso2, PB, LBAvisos);
    else
      AvisoAssemblerNaoImplementado(DeviceAssembler, sProcName);
  end;
end;

function TUnDevicesPF.ObtemUrlBaseAPI(const DeviceAssembler: TDevicesAssembler;
  const AvisaVersao: Boolean; var Res: ObtemUrlBaseAPIResult; const AvisaErro:
  Boolean; LBAvisos: TListBox): Boolean;
const
  sProcName = 'TUnDevicesPF.ObtemUrlBaseAPI()';
var
  URL: String;
begin
  Result := False;
  Res.Versao := '';
  Res.UrlBaseAPI := '';
  case TDevicesAssembler(DeviceAssembler) of
    assembFronius: Result := DevicesPF_Fronius.ObtemUrlBaseAPI(AvisaVersao, Res,
                   AvisaErro, LBAvisos);
    else
      AvisoAssemblerNaoImplementado(DeviceAssembler, sProcName);
  end;
end;

end.
