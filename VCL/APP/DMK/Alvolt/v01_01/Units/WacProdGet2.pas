unit WacProdGet2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  VCLTee.TeEngine, VCLTee.Series, VCLTee.TeeProcs, VCLTee.Chart,
  VCLTee.TeCanvas, dmkDBGridZTO, ZAbstractRODataset, ZAbstractDataset, ZDataset,
  AdvGlowButton, Variants, UnDevicesPF, UnProjGroup_Vars, UndmkProcFunc;

type
  TChartShowed = (chshNone=0,
    chshSecsPeriod=1, chshSecsMoment=2,
    chshDaysPeriod=3, chshDaysMoment=4,
    chshMnthPeriod=5, chshMnthMoment=6,
    chshYersPeriod=7, chshYersMoment=8);
  TFmWacProdGet2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrAnos: TZQuery;
    QrAnosQuantia: TFloatField;
    QrAnosANO: TLargeintField;
    QrMeses: TZQuery;
    QrMesesQuantia: TFloatField;
    QrMesesMES: TLargeintField;
    DsAnos: TDataSource;
    DsMeses: TDataSource;
    QrInstantes: TZQuery;
    QrInstantesQuantia: TFloatField;
    QrInstantesHora: TWideMemoField;
    QrDias: TZQuery;
    QrDiasQuantia: TFloatField;
    QrDiasDIA: TLargeintField;
    DsDias: TDataSource;
    DsInstantes: TDataSource;
    QrTotal: TZQuery;
    QrChart: TZQuery;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    Panel6: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    dmkDBGridZTO1: TdmkDBGridZTO;
    dmkDBGridZTO2: TdmkDBGridZTO;
    DBGDias: TdmkDBGridZTO;
    Panel7: TPanel;
    LaData: TLabel;
    DBGInstante: TdmkDBGridZTO;
    PnFrx: TPanel;
    LBAvisos: TListBox;
    Chart1: TChart;
    AGBSecs: TAdvGlowButton;
    AGBDays: TAdvGlowButton;
    AGBMnth: TAdvGlowButton;
    AGBYers: TAdvGlowButton;
    AGBPeriodo: TAdvGlowButton;
    AdvGlowButton1: TAdvGlowButton;
    PB1: TProgressBar;
    PB2: TProgressBar;
    TimerOP: TTimer;
    Series1: TBarSeries;
    QrMax: TZQuery;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrAnosAfterScroll(DataSet: TDataSet);
    procedure QrAnosBeforeClose(DataSet: TDataSet);
    procedure QrMesesAfterScroll(DataSet: TDataSet);
    procedure QrMesesBeforeClose(DataSet: TDataSet);
    procedure QrDiasAfterScroll(DataSet: TDataSet);
    procedure QrDiasBeforeClose(DataSet: TDataSet);
    procedure QrTotalAfterOpen(DataSet: TDataSet);
    procedure QrTotalAfterScroll(DataSet: TDataSet);
    procedure QrTotalBeforeClose(DataSet: TDataSet);
    procedure QrInstantesHoraGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure FormShow(Sender: TObject);
    procedure AGBSecsClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Chart1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure BtOKClick(Sender: TObject);
    procedure TimerOPTimer(Sender: TObject);
    procedure AGBDaysClick(Sender: TObject);
    procedure AGBMnthClick(Sender: TObject);
    procedure Chart1ClickSeries(Sender: TCustomChart; Series: TChartSeries;
      ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
    FMaxInstProd, FMaxDayProd, FMaxMonthProd, FMaxYearProd: Double;
    FProducedInterval,     // 300
    FHourIntervals,        // 12
    FDayIntervals: Double; // 288
    FChartShowed: TChartShowed;
    FLastIndex: Integer;
    //
    procedure ConfiguraChart();
    procedure ConfiguraSerieArea(ChartSerie: TChartSeries);
    procedure ConfiguraSerieBar(ChartSerie: TChartSeries);
    //
    procedure MostraGraficoDias();
    procedure MostraGraficoMeses();
    procedure MostraGraficoMomento();
    //
    function  ObtemProducao(): Boolean;
    //
    procedure ReopenAnos();
    procedure ReopenMeses();
    procedure ReopenDias();
    procedure ReopenInstantes();
    function  ReopenTotal(): Boolean;

  public
    { Public declarations }
  end;

  var
  FmWacProdGet2: TFmWacProdGet2;

implementation

uses UnMyObjects, Module, UnGrl_DmkDB;

{$R *.DFM}

const
  myclChartFramTodos = $00EEEEED; // 237-238-238
  myclChartAreaFundo = clWhite;
  //
  myclSerieFramLinVI = $00EEEEED; // 237-238-238
  myclSerieAreaLinEx = $00AAA8A6; // 166-168-170
  myclSerieAreaAreas = $00C0BEBC; // 188-190-192
  myclSerieAreaLinVI = $00C0BEBC; // 00B3B1AF; // 175-177-179
  myclSerieAreaLinHI = $00BBB9B7; // 183-185-187
  myclSerieAreaLinHE = $00EDEDEC; // 236-237-237

var
  VlrInstantes: array[0..287] of Variant;
  ValrAcumInst: array[0..287] of Variant;
  VlrDiasMes: array[0..30] of Variant;
  VlrMesesAno: array[0..11] of Variant;
  sDataGraficoDias,
  sDataGraficoMeses: String;

procedure TFmWacProdGet2.AGBMnthClick(Sender: TObject);
begin
  if AGBMnth.Down then
    MostraGraficoMeses();
end;

procedure TFmWacProdGet2.AGBSecsClick(Sender: TObject);
begin
  if AGBSecs.Down then
    MostraGraficoMomento();
end;

procedure TFmWacProdGet2.AGBDaysClick(Sender: TObject);
begin
  if AGBDays.Down then
    MostraGraficoDias();
end;

procedure TFmWacProdGet2.BtOKClick(Sender: TObject);
var
  Obteve: Boolean;
begin
  BtOK.Enabled := False;
  Obteve := ObtemProducao();
  BtOK.Enabled := Obteve;
  TimerOP.Enabled := Obteve = False;
end;

procedure TFmWacProdGet2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWacProdGet2.Chart1ClickSeries(Sender: TCustomChart;
  Series: TChartSeries; ValueIndex: Integer; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Geral.MB_Info(Geral.FF0(ValueIndex));
end;

procedure TFmWacProdGet2.Chart1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
const
  MidBlue  = $00CD9F87;
  DarkBlue = $00A36644;
var
  iSeries, I: Integer;
  i5min, Posic: Double;
  Texto, Hora, Producao: String;
var
  Ypos, XPos, Index: Integer;
begin
  Index := -1;
  if Chart1[0] <> nil then
    Index := Chart1[0].Clicked(X,Y);
  //
  case FChartShowed of
    TChartShowed.chshNone: ;// Nada
    TChartShowed.chshSecsPeriod:
    begin
      Texto := sDataGraficoDias;
      if Chart1.Axes.Top.IAxisSize > 0 then
      begin
        i5min := (X - Chart1.Axes.Top.IStartPos) / Chart1.Axes.Top.IAxisSize;
        //
        I     := Round(i5min * FDayIntervals);
        Posic := I / FDayIntervals;
        Hora  := Geral.FDT(Posic, 102);
        Texto := Texto + ' ' + FormatDateTime('hh:nn', Posic);
        if VlrInstantes[I] <> null then
          Texto := Texto + '  - 5 min: ' +
          Geral.FFT(VlrInstantes[I] * FHourIntervals / 1000, 2, siNegativo);
        if ValrAcumInst[I] <> null then
          Texto := Texto + ' - acumulado: ' +
          Geral.FFT(ValrAcumInst[I] / 1000, 2, siNegativo);
        //QrInstantes.Locate('Hora', Hora, []);
      end else
      begin
        Posic := 0;
        Producao := '';
      end;
      //Label1.Caption := FormatDateTime('hh:nn', Posic);

      Chart1.Title.Text.Text := Texto;
      //
      with Chart1, Chart1.Canvas do
      begin
        Refresh;
        //
        if (X < ChartRect.Left)
        or (X > ChartRect.Right)
        or (Y < ChartRect.Top)
        or (Y > ChartRect.Bottom) then
          Exit;
        Ypos := Y;
        Pen.Color := myclSerieAreaLinEx;
        Pen.Width := 1;
        Pen.Style := psSolid;
        MoveTo( ChartRect.Left, YPos );
        LineTo( ChartRect.Right, YPos );
        Xpos := X;
        Pen.Color := myclSerieAreaLinEx;
        Pen.Width := 1;
        Pen.Style := psSolid;
        MoveTo(XPos, ChartRect.Top);
        LineTo(Xpos, ChartRect.Bottom);
      end;
    end;
    TChartShowed.chshSecsMoment: ; // nada
    TChartShowed.chshDaysPeriod:
    begin
      if (Index > - 1) then
      begin
        Texto := sDataGraficoDias;
        //YValor := Chart1[0].XValue[Index];
        if VlrDiasMes[Index] > 0 then
        begin
          if FLastIndex > -1 then
            Chart1[0].ValueColor[FLastIndex] := DarkBlue;
          FLastIndex := Index;
          Chart1[0].ValueColor[Index] := MidBlue;
          Texto := Texto + ' - dia ' + Geral.FF0(Index + 1) + ' = ' +
            Geral.FFT(VlrDiasMes[Index] / 1000, 2, siNegativo) + ' Kwh';
          Chart1.Title.Text.Text := Texto;
        end;
      end;
    end;
    TChartShowed.chshDaysMoment: ;// nada
    TChartShowed.chshMnthPeriod:
    begin
      if (Index > - 1) then
      begin
        Texto := sDataGraficoMeses;
        if VlrMesesAno[Index] > 0 then
        begin
          if FLastIndex > -1 then
            Chart1[0].ValueColor[FLastIndex] := DarkBlue;
          FLastIndex := Index;
          Chart1[0].ValueColor[Index] := MidBlue;
          Texto := Texto + ' - m�s ' + Geral.FF0(Index + 1) + ' = ' +
            Geral.FFT(VlrMesesAno[Index] / 1000, 1, siNegativo) + ' Kwh';
          Chart1.Title.Text.Text := Texto;
        end;
      end;
    end;
    TChartShowed.chshMnthMoment: ; // nada
    TChartShowed.chshYersPeriod:
    begin
    end;
    TChartShowed.chshYersMoment: ; // nada
  end;
end;

procedure TFmWacProdGet2.ConfiguraChart();
var
  I, J: Integer;
  YSource: String;
  iSerie, ChartSerie: TChartSeries;
begin
  // Remover series (gr�ficos)
  while Chart1.SeriesCount > 0 do
    Chart1.Series[0].Free;
  // Desfazer 3D
  Chart1.View3D := False;
  // Tirar legendas do lado
  Chart1.Legend.Visible := False;
  Chart1.Title.Font.Size := 14;
  Chart1.Title.Font.Name := 'Calibri';
  Chart1.Title.Font.Color := VAR_COR_TIT_C;

  // Configurar Gr�fico em si (sem nenhuma s�rie - fundo vazio)
  Chart1.Color := myclChartAreaFundo;
  Chart1.Frame.Color := myclChartFramTodos;

  Chart1.LeftAxis.Axis.Color := clSilver;
  Chart1.LeftAxis.Axis.Width := 1;
  Chart1.LeftAxis.Grid.Color     := myclSerieAreaLinHE;
  Chart1.LeftAxis.Grid.DrawEvery := 3; //
  Chart1.LeftAxis.Grid.Style     := TPenStyle.psSolid;

  Chart1.BottomAxis.Axis.Color := clSilver;
  Chart1.BottomAxis.Axis.Width := 1;
  Chart1.BottomAxis.MinorTicks.Visible := False;
  Chart1.BottomAxis.Grid.Visible   := True;
  Chart1.BottomAxis.Grid.DrawEvery := 1;
  Chart1.BottomAxis.Grid.Color     := myclSerieAreaLinHE;
  Chart1.BottomAxis.Grid.Style     := TPenStyle.psSolid;

  Chart1.BottomAxis.Ticks.Color    := myclSerieAreaLinVI;
  Chart1.BottomAxis.Ticks.Width    := 1;



  Chart1.TopAxis.Axis.Color := clWhite;
  Chart1.TopAxis.Axis.Width := 1;

  Chart1.RightAxis.Axis.Color := clWhite;
  Chart1.RightAxis.Axis.Width := 1;

 //
  // Evitar erro no Destroy;
  iSerie := nil;
  ChartSerie := nil;
end;

procedure TFmWacProdGet2.ConfiguraSerieArea(ChartSerie: TChartSeries);
begin
  ChartSerie.Pen.Width := 1;
  ChartSerie.LegendTitle := '???';
  //
  MyObjects.SVP_Objeto(ChartSerie,  'LinePen', TChartPen, 'Color',
  myclSerieAreaLinEx);
  MyObjects.SVP_Objeto(ChartSerie,  'LinePen', TChartPen, 'Width', 2);
  // (B) - Area do gr�fico �rea
  MyObjects.SVP_Primit(ChartSerie, 'AreaColor', myclSerieAreaAreas);
  // (C) - Linhas verticais dentro da �rea ocupada do gr�fico �rea
  MyObjects.SVP_Objeto(ChartSerie,  'AreaLinesPen', TChartPen, 'Color', myclSerieAreaLinVI);
  MyObjects.SVP_Objeto(ChartSerie,  'AreaLinesPen', TChartPen, 'Width', 1);
end;

procedure TFmWacProdGet2.ConfiguraSerieBar(ChartSerie: TChartSeries);
begin
  ChartSerie.Pen.Width := 1;
  ChartSerie.LegendTitle := '???';
  //
  ChartSerie.Marks.Visible := False;
end;

procedure TFmWacProdGet2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Chart1.Color := myclChartAreaFundo;
end;

procedure TFmWacProdGet2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QrTotal.Close;
end;

procedure TFmWacProdGet2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FChartShowed := TChartShowed.chshNone;
  FMaxInstProd := 0;
  FMaxDayProd  := 0;
  FMaxMonthProd  := 0;
  FMaxYearProd  := 0;
  FLastIndex    := -1;
  //
  FProducedInterval := 300; // segundos em 5 minutos >> cada item de leitura
  FHourIntervals    := 3600 / FProducedInterval;  // 12  intervalos de 5 min por hora
  FDayIntervals     := 86400 / FProducedInterval;  // 288  intervalos de 5 min por dia
  //
  Chart1.Align := alClient;
  //LBAvisos.Align := alClient;
  //
  if not ReopenTotal() then
    ReopenTotal();
  //
  ConfiguraChart();
  AGBSecs.Down := True;
  AGBPeriodo.Down := True;
  MostraGraficoMomento();
end;

procedure TFmWacProdGet2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWacProdGet2.FormShow(Sender: TObject);
begin
  ////ConfiguraChart();
  //DBGInstante.SetFocus;
  ////ConfiguraChart();
  //AGBDiaClick(AGBDia);
end;

procedure TFmWacProdGet2.MostraGraficoDias();
var
  I, H: integer;
  ChartSerie: TChartSeries;
  DataI, DataF: TDateTime;
  sDataI, SDataF: String;
begin
  DataI  := EncodeDate(QrAnosAno.Value, QrMesesMES.Value, 1);
  DataF  := IncMonth(DataI, 1) - 1;
  sDataI := Geral.FDT(DataI, 1);
  sDataF := Geral.FDT(DataF, 1);
  while Chart1.SeriesCount > 0 do
    Chart1.Series[0].Free;
  Chart1.Visible := True;
  //
  sDataGraficoDias := Geral.FDT(DataI, 7);
  Chart1.Title.Text.Text := sDataGraficoDias;
  Chart1.LeftAxis.Automatic := False;
  Chart1.LeftAxis.Maximum := FMaxDayProd;
  Chart1.LeftAxis.Minimum := 0;
  //
  ChartSerie := TBarSeries.Create(Chart1);
  ConfiguraSerieBar(ChartSerie);
  //
  Chart1.AddSeries(ChartSerie);
  FChartShowed := TChartShowed.chshDaysPeriod;

  // 31 dias do m�s
  for I := Low(VlrDiasMes) to High(VlrDiasMes) do
    VlrDiasMes[I] := Null;
  //QrChart.SQL.Text := QrInstantes.SQL.Text;
  //Grl_DmkDB.AbreQuery(QrChart, Dmod.MyDB);
  Grl_DmkDB.AbreSQLQuery0(QrChart, Dmod.MyDB, [
  'SELECT strftime("%d", ProdDta)  DiaMes, ',
  //'SUM(Quantia) / 1000 Quantia ',
  'SUM(Quantia) Quantia ',
  'FROM wacprodvalues  ',
  'WHERE ProdDta  ',
  'BETWEEN "' + sDataI + '" AND "' + sDataF + ' 23:59:59"  ',
  'GROUP BY DiaMes',
  'ORDER BY DiaMes ',
  '']);
  //
  QrChart.First;
  while not QrChart.Eof do
  begin
    I := Geral.IMV(QrChart.FieldByName('DiaMes').AsString) - 1;
    VlrDiasMes[I] := QrChart.FieldByName('Quantia').AsFloat;
    //
    QrChart.Next;
  end;
  ChartSerie.DataSources.Clear;
  for I := Low(VlrDiasMes) to High(VlrDiasMes) do
  begin
    begin
      if VlrDiasMes[I] = Null then
        ChartSerie.AddNull(I + 1)
      else
        ChartSerie.AddXY(I + 1, VlrDiasMes[I] / 1000, Geral.FF0(I + 1));
    end;
  end;
end;

procedure TFmWacProdGet2.MostraGraficoMeses();
var
  I, H: integer;
  ChartSerie: TChartSeries;
  DataI, DataF: TDateTime;
  sDataI, sDataF, sTickCaption: String;
begin
  DataI  := EncodeDate(QrAnosAno.Value, 1, 1);
  DataF  := IncMonth(DataI, 12) - 1;
  sDataI := Geral.FDT(DataI, 1);
  sDataF := Geral.FDT(DataF, 1);
  while Chart1.SeriesCount > 0 do
    Chart1.Series[0].Free;
  Chart1.Visible := True;
  //
  sDataGraficoMeses := Geral.FDT(DataI, 25);
  Chart1.Title.Text.Text := sDataGraficoMeses;
  Chart1.LeftAxis.Automatic := False;
  Chart1.LeftAxis.Maximum := FMaxMonthProd;
  Chart1.LeftAxis.Minimum := 0;
  //
  ChartSerie := TBarSeries.Create(Chart1);
  ConfiguraSerieBar(ChartSerie);
  //
  Chart1.AddSeries(ChartSerie);
  FChartShowed := TChartShowed.chshMnthPeriod;
  // 12 meses do ano
  for I := Low(VlrMesesAno) to High(VlrMesesAno) do
    VlrMesesAno[I] := Null;
  Grl_DmkDB.AbreSQLQuery0(QrChart, Dmod.MyDB, [
  'SELECT strftime("%m", ProdDta)  MesAno,  ',
  'SUM(Quantia) Quantia  ',
  'FROM wacprodvalues   ',
  'WHERE ProdDta   ',
    'BETWEEN "' + sDataI + '" AND "' + sDataF + ' 23:59:59"  ',
  'GROUP BY MesAno ',
  'ORDER BY MesAno  ',
  '']);
  //Geral.MB_SQL(Self, QrChart);
  //
  QrChart.First;
  while not QrChart.Eof do
  begin
    I := Geral.IMV(QrChart.FieldByName('MesAno').AsString) - 1;
    VlrMesesAno[I] := QrChart.FieldByName('Quantia').AsFloat;
    //
    QrChart.Next;
  end;
  ChartSerie.DataSources.Clear;
  for I := Low(VlrMesesAno) to High(VlrMesesAno) do
  begin
    sTickCaption :=  DmkPF.VerificaMes(I + 1, False, True);
    if VlrMesesAno[I] = Null then
      //ChartSerie.AddNull(Geral.FF0(I + 1))
      ChartSerie.AddXY(I + 1, 0, sTickCaption)
    else
      ChartSerie.AddXY(I + 1, VlrMesesAno[I] / 1000, sTickCaption);
  end;
end;

procedure TFmWacProdGet2.MostraGraficoMomento();
var
  I, H: integer;
  ChartSerie: TChartSeries;
  Data: TDateTime;
  sData: String;
  Acumulado: Double;
begin
  //Chart1.Color := clWhite;
  Data  := EncodeDate(QrAnosAno.Value, QrMesesMES.Value, QrDiasDia.Value);
  sData := Geral.FDT(Data, 1);
  while Chart1.SeriesCount > 0 do
    Chart1.Series[0].Free;
  Chart1.Visible := True;
  //
  Chart1.Title.Text.Text := Geral.FDT(Data, 2);
  Chart1.LeftAxis.Automatic := False;
  Chart1.LeftAxis.Maximum := FMaxInstProd;
  Chart1.LeftAxis.Minimum := 0;
  //
  ChartSerie := TAreaSeries.Create(Chart1);
  ConfiguraSerieArea(ChartSerie);
  //
  Chart1.AddSeries(ChartSerie);
  FChartShowed := TChartShowed.chshSecsPeriod;

  // 24 h de 5 em 5 min
  for I := Low(VlrInstantes) to High(VlrInstantes) do
  begin
    VlrInstantes[I] := Null;
    ValrAcumInst[I] := 0;
  end;
  //QrChart.SQL.Text := QrInstantes.SQL.Text;
  //Grl_DmkDB.AbreQuery(QrChart, Dmod.MyDB);
  Grl_DmkDB.AbreSQLQuery0(QrChart, Dmod.MyDB, [
  'SELECT (strftime("%H", ProdDta) * ' + Geral.FF0(Trunc(FHourIntervals)) + ') + ( ',
  'strftime("%M", ProdDta) / 5) MinFive, Quantia ',
  'FROM wacprodvalues  ',
  'WHERE ProdDta  ',
  'BETWEEN "' + sData + '" AND "' + sData + ' 23:59:59"  ',
  'ORDER BY MinFive ',
  '']);
  //
  Acumulado := 0;
  QrChart.First;
  while not QrChart.Eof do
  begin
    I := Trunc(QrChart.FieldByName('MinFive').AsFloat);
    VlrInstantes[I] := QrChart.FieldByName('Quantia').AsFloat;
    Acumulado := Acumulado + VlrInstantes[I];
    ValrAcumInst[I] := Acumulado;
    //
    QrChart.Next;
  end;
  ChartSerie.DataSources.Clear;
  for I := Low(VlrInstantes) to High(VlrInstantes) do
  begin
    H := (I div Trunc(FHourIntervals));
    if H = (I / FHourIntervals) then
    begin
      if VlrInstantes[I] = Null then
        ChartSerie.AddX(I, Geral.FF0(H) + ':00')
      else
        ChartSerie.AddXY(I, VlrInstantes[I] * FHourIntervals, Geral.FF0(H) + ':00')
    end else
    begin
      if VlrInstantes[I] = Null then
        ChartSerie.AddNull(I)
      else
        ChartSerie.AddXY(I, VlrInstantes[I] * FHourIntervals);
    end;
  end;
end;

function TFmWacProdGet2.ObtemProducao(): Boolean;
begin
  LBAvisos.Visible := True;
  //Chart1.Visible := False;
  GBAvisos1.Visible := True;
  //
  Result := DevicesPF.ObtemProducao(VAR_DeviceAssembler, QrTotal,
    LaAviso1, LaAviso2, PB1, LBAvisos);
  if Result then
  begin
    GBAvisos1.Visible := False;
    //Chart1.Visible := True;
    LBAvisos.Visible := False;
    AGBSecs.Down := True;
    AGBPeriodo.Down := True;
    //
    MostraGraficoMomento();
  end;
end;

procedure TFmWacProdGet2.QrAnosAfterScroll(DataSet: TDataSet);
begin
  ReopenMeses();
  QrMeses.Last;
  if (AGBMnth.Down) and (AGBPeriodo.Down) then
    MostraGraficoMeses();
end;

procedure TFmWacProdGet2.QrAnosBeforeClose(DataSet: TDataSet);
begin
  QrMeses.Close;
end;

procedure TFmWacProdGet2.QrDiasAfterScroll(DataSet: TDataSet);
begin
  ReopenInstantes();
  if (AGBSecs.Down) and (AGBPeriodo.Down) then
    MostraGraficoMomento();
  QrInstantes.Last;
end;

procedure TFmWacProdGet2.QrDiasBeforeClose(DataSet: TDataSet);
begin
  QrInstantes.Close;
end;

procedure TFmWacProdGet2.QrInstantesHoraGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TStringField(Sender).Value;
end;

procedure TFmWacProdGet2.QrMesesAfterScroll(DataSet: TDataSet);
begin
  ReopenDias();
  if (AGBDays.Down) and (AGBPeriodo.Down) then
    MostraGraficoDias();
  QrDias.Last;
end;

procedure TFmWacProdGet2.QrMesesBeforeClose(DataSet: TDataSet);
begin
  QrDias.Close;
end;

procedure TFmWacProdGet2.QrTotalAfterOpen(DataSet: TDataSet);
var
  Quantia: Double;
  DiaZero: TDateTime;
begin
  if Lowercase(QrTotal.FieldByName('TypeOf').AsString) <> 'null' then
  begin
    Quantia := QrTotal.FieldByName('Quantia').AsFloat;
    Self.Caption := 'WAC-PRODU-001 :: Produ��o Total Real WAC de ' +
    Geral.FFT(Quantia, 2, siNegativo) + ' kWh';
    Self.Resizing(Self.WindowState);
    //
    if FMaxInstProd = 0 then
    begin
      Grl_DmkDB.AbreSQLQuery0(QrMax, Dmod.MyDB, [
      'SELECT MAX(Quantia * ' + Geral.FF0(Trunc(FHourIntervals)) + ') Quantia ',
      'FROM wacprodvalues ',
      '']);
      FMaxInstProd := QrMax.FieldByName('Quantia').AsFloat;
      if FMaxInstProd = 0 then
        FMaxInstProd := 1000;
      //
      Grl_DmkDB.AbreSQLQuery0(QrMax, Dmod.MyDB, [
      'SELECT strftime("%Y%m%d", ProdDta) Periodo, ',
      'SUM(Quantia) / 1000 Quantia ',
      'FROM wacprodvalues ',
      'GROUP BY Periodo ',
      'ORDER BY Quantia desc ',
      'LIMIT 0,1 ',
      '']);
      FMaxDayProd := QrMax.FieldByName('Quantia').AsFloat;
      if FMaxDayProd = 0 then
        FMaxDayProd := 6;
      FMaxMonthProd := FMaxDayProd * 31;
      FMaxYearProd := FMaxDayProd * 366;
      end;
    ReopenAnos();
  end else
  begin
    DiaZero := EncodeDate(2000, 1, 1);
    if MyObjects.ObtemData((*DtDefault*) Date(), (*DtSelect*) DiaZero,
    (*MinData*)DiaZero, (*HrDefault*)0, (*HabilitaHora*)False,
    'Instala��o Sistema',
    'Aten��o: Informe a data que o sistema fotovoltaico entrou em funcionamento '
    + 'para que todos dados de gera��o anteriores a data atual possam ser importados!') then
    begin
      //if VAR_GETDATA = 0 then Exit;
      //
      //DiaZero := VAR_GETDATA;
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, stIns, 'wacprodvalues', False, [
      'ProdDta', 'Quantia'], [
      ], [
      DiaZero, 0], [
      ], True, dmksqlinsInsOnly, '', stDesktop);
    end;
    //
    QrTotal.Close;
  end;
end;

procedure TFmWacProdGet2.QrTotalAfterScroll(DataSet: TDataSet);
begin
  ReopenAnos();
  QrAnos.Last;
end;

procedure TFmWacProdGet2.QrTotalBeforeClose(DataSet: TDataSet);
begin
  QrAnos.Close;
end;

procedure TFmWacProdGet2.ReopenAnos();
begin
  Grl_DmkDB.AbreSQLQuery0(QrAnos, Dmod.MyDB, [
  'SELECT CAST(strftime("%Y", ProdDta) as integer) ANO, ',
  'SUM(Quantia)/1000 Quantia ',
  'FROM wacprodvalues ',
  'GROUP BY ANO ',
  'ORDER BY ANO desc ',
  '']);
end;

procedure TFmWacProdGet2.ReopenDias();
var
  AnoMes: String;
begin
  //Dias do M�ses selecionado
  AnoMes := Geral.FF0((QrAnosAno.Value* 100) + QrMesesMES.Value);
  Grl_DmkDB.AbreSQLQuery0(QrDias, Dmod.MyDB, [
  'SELECT CAST(strftime("%d", ProdDta) as integer) DIA, ',
  'SUM(Quantia)/1000 Quantia ',
  'FROM wacprodvalues ',
  'WHERE strftime("%Y%m", ProdDta) = "' + AnoMes + '" ',
  'GROUP BY DIA ',
  'ORDER BY DIA ',
  '']);
end;

procedure TFmWacProdGet2.ReopenInstantes();
var
  Data: TDateTime;
  sData: String;
  Ano, Mes, Dia: Word;
  N: Integer;
begin
  //Dias do M�ses selecionado
  Data  := EncodeDate(QrAnosAno.Value, QrMesesMES.Value, QrDiasDia.Value);
  case Trunc(Date()) - Trunc(Data) of
    0: sData := ' - HOJE';
    1: sData := ' - Ontem';
    2: sData := ' - Anteontem';
    3: sData := '';
  end;
  sData := Geral.FDT(Data, 2) + sData;
  LaData.Caption :=  sData;
  //
  sData := Geral.FDT(Data, 1);
  Grl_DmkDB.AbreSQLQuery0(QrInstantes, Dmod.MyDB, [
  'SELECT strftime("%H:%M", ProdDta) Hora, Quantia',
  //'SELECT TIME(ProdDta) Hora, Quantia',
  'FROM wacprodvalues ',
  'WHERE ProdDta ',
  'BETWEEN "' + sData + '" AND "' + sData + ' 23:59:59" ',
  'ORDER BY Hora',
  '']);
end;

procedure TFmWacProdGet2.ReopenMeses();
begin
  //Meses do ano selecionado
  Grl_DmkDB.AbreSQLQuery0(QrMeses, Dmod.MyDB, [
  'SELECT CAST(strftime("%m", ProdDta) as integer) MES, ',
  'SUM(Quantia)/1000 Quantia ',
  'FROM wacprodvalues ',
  'WHERE strftime("%Y", ProdDta) = "' + Geral.FF0(QrAnosAno.Value) + '" ',
  'GROUP BY MES ',
  'ORDER BY MES ',
  '']);
end;

function TFmWacProdGet2.ReopenTotal: Boolean;
begin
  Result := Grl_DmkDB.AbreSQLQuery0(QrTotal, Dmod.MyDB, [
  'SELECT TypeOf(SUM(Quantia)) TypeOf,  SUM(Quantia) / 1000 Quantia ',
  'FROM wacprodvalues ',
  '']);
end;

procedure TFmWacProdGet2.TimerOPTimer(Sender: TObject);
begin
  TimerOP.Enabled := False;
  TimerOP.Enabled := not ObtemProducao();
end;

end.
