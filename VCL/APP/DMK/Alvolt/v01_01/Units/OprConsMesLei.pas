unit OprConsMesLei;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes;

type
  TFmOprConsMesLei = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label6: TLabel;
    EdMedOpeValInjet: TdmkEdit;
    Label5: TLabel;
    EdMedOpeLeiConsu: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FOK: Boolean;
  end;

  var
  FmOprConsMesLei: TFmOprConsMesLei;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmOprConsMesLei.BtOKClick(Sender: TObject);
begin
  FOK := True;
  Close;
end;

procedure TFmOprConsMesLei.BtSaidaClick(Sender: TObject);
begin
  FOK := False;
  Close;
end;

procedure TFmOprConsMesLei.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOprConsMesLei.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FOK := False;
end;

procedure TFmOprConsMesLei.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
