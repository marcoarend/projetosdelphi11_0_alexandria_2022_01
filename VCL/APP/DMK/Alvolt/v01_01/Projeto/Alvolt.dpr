program Alvolt;

uses
  Vcl.Forms,
  MyVCLSkin in '..\..\..\..\..\UTL\_UNT\v01_01\MyVCLSkin.pas',
  UnInternalConsts2 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts2.pas',
  dmkDAC_PF in '..\..\..\..\..\UTL\MySQL\v01_01\dmkDAC_PF.pas',
  UnMsgInt in '..\..\..\..\..\UTL\_UNT\v01_01\UnMsgInt.pas',
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnInternalConsts in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnDmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  ZCF2 in '..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  ResIntStrings in '..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnMyObjects in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  Module in '..\Units\Module.pas' {DMod: TDataModule},
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnInternalConsts3 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts3.pas',
  UnLock_MLA in '..\..\..\UnLock\v01_01\UnLock_MLA.pas' {FmUnLock_MLA},
  MyListas in '..\Units\MyListas.pas',
  UMySQLDB in '..\..\..\..\..\UTL\MySQL\v01_01\UMySQLDB.pas',
  MyGlyfs in '..\..\..\..\..\UTL\_FRM\v01_01\MyGlyfs.pas' {FmMyGlyfs},
  UnDmkWeb in '..\..\..\..\..\MDL\WEB\v01_01\UnDmkWeb.pas',
  dmkSOAP in '..\..\..\..\..\MDL\WEB\v01_01\WebService\dmkSOAP.pas',
  NovaVersao in '..\..\..\..\..\UTL\_FRM\v01_01\NovaVersao.pas' {FmNovaVersao},
  UnGrl_Consts in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UnitMD5 in '..\..\..\..\..\UTL\Encrypt\v01_01\UnitMD5.pas',
  ABOUT in '..\..\..\..\..\UTL\_FRM\v01_01\ABOUT.pas' {FmAbout},
  UnVCL_ZDB in '..\..\..\..\..\UTL\ZeosDB\UnVCL_ZDB.pas',
  UnGrl_DmkDB in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_DmkDB.pas',
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  VerifiDB_SQLite in '..\..\..\..\..\UTL\ZeosDB\VerifiDB_SQLite.pas' {FmVerifiDB_SQLite},
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnitAgendaAux in '..\..\..\..\..\MDL\AGEN\v02_01\UnitAgendaAux.pas',
  ZForge in '..\..\..\..\..\UTL\_FRM\v01_01\ZForge.pas' {FmZForge},
  UnGrl_Geral in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  UnDmkListas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnDmkListas.pas',
  UnGrlUsuarios in '..\..\..\..\..\..\MultiOS\MDL\USR\v02_01\UnGrlUsuarios.pas',
  UnGrl_REST in '..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_REST.pas',
  UnDmkHTML2 in '..\..\..\..\..\UTL\WEB\HTML\v01_01\UnDmkHTML2.pas',
  WBFuncs in '..\..\..\..\..\UTL\WEB\HTML\v01_01\WBFuncs.pas',
  OAuth in '..\..\..\..\..\UTL\WEB\HTML\v01_01\OAuth.pas' {FmOAuth},
  UnGrlEnt in '..\..\..\..\..\..\MultiOS\MDL\ENT\v02_01\UnGrlEnt.pas',
  UnALL_Jan in '..\..\..\..\..\UTL\_UNT\v01_01\UnALL_Jan.pas',
  uLkJSON in '..\..\..\..\..\UTL\JSON\uLkJSON.pas',
  UnMyJSON in '..\..\..\..\..\UTL\JSON\UnMyJSON.pas',
  WacProdGet in '..\Units\WacProdGet.pas' {FmWacProdGet},
  OpcoesApp in '..\Units\OpcoesApp.pas' {FmOpcoesApp},
  IPsNaRede in '..\..\..\..\..\UTL\_FRM\v01_01\IPsNaRede.pas' {FmIPsNaRede},
  UnAppJan in '..\Units\UnAppJan.pas',
  UnProjGroup_Vars in '..\Units\UnProjGroup_Vars.pas',
  UnAppPF in '..\Units\UnAppPF.pas',
  UnDevicesPF in '..\Units\UnDevicesPF.pas',
  UnAppEnums in '..\Units\UnAppEnums.pas',
  UnDevicesPF_Fronius in '..\Units\UnDevicesPF_Fronius.pas',
  GetData in '..\..\..\..\..\UTL\_FRM\v01_01\GetData.pas' {FmGetData},
  SelRadioGroup in '..\..\..\..\..\UTL\_FRM\v01_01\SelRadioGroup.pas' {FmSelRadioGroup},
  TesteChart in '..\..\..\..\CLI\BDER\v02_01\TesteChart.pas' {FmTesteChart},
  MeuFrx in '..\..\..\..\..\UTL\Print\v01_01\MeuFrx.pas' {FmMeuFrx},
  CustomFR3Imp in '..\..\..\..\..\UTL\Print\v01_01\CustomFR3Imp.pas' {FmCustomFR3Imp},
  WacProdGet2 in '..\Units\WacProdGet2.pas' {FmWacProdGet2},
  OprConsMes in '..\Units\OprConsMes.pas' {FmOprConsMes},
  OprConsMesFin in '..\Units\OprConsMesFin.pas' {FmOprConsMesFin},
  Alvolt_dmk in '..\Units\Alvolt_dmk.pas' {FmAlvolt},
  UComm in '..\Units\UComm.pas',
  PingClient in '..\Units\PingClient.pas',
  OprConsMesIts in '..\Units\OprConsMesIts.pas' {FmOprConsMesIts},
  OprConsMesLei in '..\Units\OprConsMesLei.pas' {FmOprConsMesLei},
  MeuDBUsesZ in '..\..\..\..\..\UTL\ZeosDB\MeuDBUsesZ.pas' {FmMeuDBUsesZ},
  ModuleGeralZ in '..\..\..\..\..\UTL\ZeosDB\ModuleGeralZ.pas' {DModGZ: TDataModule},
  PesqNomeZ in '..\..\..\..\..\UTL\_FRM\v01_01\PesqNomeZ.pas' {FmPesqNomeZ};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.CreateForm(TFmAlvolt, FmAlvolt);
  Application.CreateForm(TFmMeuDBUsesZ, FmMeuDBUsesZ);
  Application.Run;
end.
