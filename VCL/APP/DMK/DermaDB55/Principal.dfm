object FmPrincipal: TFmPrincipal
  Left = 0
  Top = 0
  Caption = 'FmPrincipal'
  ClientHeight = 575
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 534
    Width = 635
    Height = 41
    Align = alBottom
    Caption = 'Panel1'
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 48
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Teste'
      TabOrder = 0
      OnClick = BitBtn1Click
    end
  end
  object dmkDBGridZTO2: TdmkDBGridZTO
    Left = 0
    Top = 0
    Width = 352
    Height = 534
    Align = alClient
    DataSource = DsDatabases
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    RowColors = <>
  end
  object Panel2: TPanel
    Left = 352
    Top = 0
    Width = 283
    Height = 534
    Align = alRight
    Caption = 'Panel2'
    TabOrder = 2
    object Panel12: TPanel
      Left = 1
      Top = 1
      Width = 122
      Height = 532
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Panel13: TPanel
        Left = 0
        Top = 0
        Width = 122
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 4
          Top = 0
          Width = 41
          Height = 13
          Caption = 'Localiza:'
        end
        object EdLocTable: TEdit
          Left = 4
          Top = 16
          Width = 113
          Height = 21
          TabOrder = 0
        end
      end
      object GridTabelas: TDBGrid
        Left = 0
        Top = 41
        Width = 122
        Height = 491
        Align = alClient
        DataSource = DsTabelas
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object Panel29: TPanel
      Left = 161
      Top = 1
      Width = 121
      Height = 532
      Align = alRight
      Caption = 'Panel29'
      TabOrder = 1
      object Panel30: TPanel
        Left = 1
        Top = 1
        Width = 119
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label12: TLabel
          Left = 4
          Top = 0
          Width = 28
          Height = 13
          Caption = 'Filtro:'
        end
        object EdCampo: TEdit
          Left = 4
          Top = 16
          Width = 113
          Height = 21
          TabOrder = 0
        end
      end
      object GridCampos2: TDBGrid
        Left = 1
        Top = 42
        Width = 119
        Height = 489
        Align = alClient
        DataSource = DsCampos
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Field'
            Title.Caption = 'Campo'
            Visible = True
          end>
      end
    end
  end
  object DsDatabases: TDataSource
    DataSet = QrDatabases
    Left = 24
    Top = 108
  end
  object DsCampos: TDataSource
    DataSet = QrCampos
    Left = 172
    Top = 108
  end
  object DsTabelas: TDataSource
    DataSet = QrTabelas
    Left = 100
    Top = 108
  end
  object MyDB: TMySQLDatabase
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    DatasetOptions = []
    Left = 28
    Top = 16
  end
  object QrDatabases: TMySQLQuery
    Database = MyDB
    BeforeClose = QrDatabasesBeforeClose
    AfterScroll = QrDatabasesAfterScroll
    Left = 28
    Top = 64
  end
  object MyEx: TMySQLDatabase
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    DatasetOptions = []
    Left = 104
    Top = 16
  end
  object QrTabelas: TMySQLQuery
    Database = MyEx
    BeforeClose = QrTabelasBeforeClose
    AfterScroll = QrTabelasAfterScroll
    Left = 104
    Top = 60
  end
  object QrCampos: TMySQLQuery
    Database = MyEx
    Left = 172
    Top = 64
  end
end
