unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO, mySQLDbTables;

type
  TFmPrincipal = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    DsDatabases: TDataSource;
    dmkDBGridZTO2: TdmkDBGridZTO;
    DsCampos: TDataSource;
    Panel2: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    Label1: TLabel;
    EdLocTable: TEdit;
    GridTabelas: TDBGrid;
    Panel29: TPanel;
    Panel30: TPanel;
    Label12: TLabel;
    EdCampo: TEdit;
    GridCampos2: TDBGrid;
    DsTabelas: TDataSource;
    MyDB: TMySQLDatabase;
    QrDatabases: TMySQLQuery;
    MyEx: TMySQLDatabase;
    QrTabelas: TMySQLQuery;
    QrCampos: TMySQLQuery;
    procedure BitBtn1Click(Sender: TObject);
    procedure QrDatabasesAfterScroll(DataSet: TDataSet);
    procedure QrTabelasAfterScroll(DataSet: TDataSet);
    procedure QrTabelasBeforeClose(DataSet: TDataSet);
    procedure QrDatabasesBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenDatabases(DBSel, DBLoc: String);
    procedure ReopenTabelas();
    procedure ReopenCampos();
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

{$R *.dfm}

procedure TFmPrincipal.BitBtn1Click(Sender: TObject);
begin
  MyDB.Connected      := False;
  MyDB.Host           := 'localhost'; //MyDB.Host;
  MyDB.DatabaseName   := 'bluederm';
  MyDB.Port           := 3306; //MyDB.Port;
  MyDB.UserName       := 'root'; //MyDB.UserName;
  MyDB.UserPassword   := 'wkljweryhvbirt'; //MyDB.UserPassword;
  MyDB.Connected      := True;
  //
  if MyDB.Connected then
  begin
    ReopenDatabases('', 'bluederm');
    ReopenCampos();
  end;
end;

procedure TFmPrincipal.QrDatabasesAfterScroll(DataSet: TDataSet);
begin
  MyEx.Connected      := False;
//http://www.microolap.com/products/connectivity/mysqldac/help/FAQ/q_07.htm
  //MyEx.ConnectionCharacterSet      := 'latin1';
  //MyEx.ConnectionCollation         := 'latin1_swedish_ci';
  MyEx.Host           := MyDB.Host;
  MyEx.DatabaseName   := QrDatabases.Fields[0].AsString;
  MyEx.Port           := MyDB.Port;
  MyEx.UserName       := MyDB.UserName;
  MyEx.UserPassword   := MyDB.UserPassword;
  MyEx.Connected      := True;
  //
  ReopenTabelas();
end;

procedure TFmPrincipal.QrDatabasesBeforeClose(DataSet: TDataSet);
begin
  QrTabelas.Close;
end;

procedure TFmPrincipal.QrTabelasAfterScroll(DataSet: TDataSet);
begin
  ReopenCampos();
end;

procedure TFmPrincipal.QrTabelasBeforeClose(DataSet: TDataSet);
begin
  QrCampos.Close;
end;

procedure TFmPrincipal.ReopenCampos();
var
  Campos: String;
begin
  if MyEx.Connected then
  begin
    QrCampos.Close;
    QrCampos.Database := MyEx;
    //
    if Trim(EdCampo.Text) <> '' then
      Campos := ' "%' + EdCampo.Text + '%"'
    else
      Campos := '';
    QrCampos.Close;
    //
    if (QrTabelas.State = dsBrowse) then
      if QrTabelas.RecordCount > 0 then
      begin
        QrCampos.SQL.Clear;
  //      QrCampos.SQL.Add('DESCRIBE ' + QrTabelas.Fields[0].AsString + Campos);
        QrCampos.SQL.Add('SHOW COLUMNS FROM ' + QrTabelas.Fields[0].AsString + Campos);
        QrCampos.Open;
      end;
  end;
end;

procedure TFmPrincipal.ReopenDatabases(DBSel, DBLoc: String);
var
  MyDB: String;
var
  DBAtu: String;
begin
  if Trim(DBLoc) <> '' then
    DBAtu := DBLoc
  else
  begin
    if QrDatabases.State <> dsInactive then
      DBAtu := QrDatabases.Fields[0].AsString
    else
      DBAtu := '';
  end;
  //
  MyDB := Trim(DBSel);
  //
  QrDatabases.Close;
  QrDatabases.SQL.Clear;
  QrDatabases.SQL.Add('SHOW DATABASES ');

  if MyDB <> '' then
    QrDatabases.SQL.Add('LIKE "%' + MyDB + '%"');

  QrDatabases.Open;
  //
  MyDB := Trim(DBAtu);
  try
    if MyDB <> '' then
      QrDatabases.Locate('database', MyDB, [loCaseInsensitive]);
  except
    ;
  end;
end;

procedure TFmPrincipal.ReopenTabelas();
begin
  MyEx.Connected      := False;
  MyEx.Host           := MyDB.Host;
  MyEx.DatabaseName   := QrDatabases.Fields[0].AsString;
  MyEx.Port           := MyDB.Port;
  MyEx.UserName       := MyDB.UserName;
  MyEx.UserPassword   := MyDB.UserPassword;
  MyEx.Connected      := True;
  //
  if MyEx.Connected then
  begin
    QrTabelas.Close;
    QrTabelas.Database := MyEx;
    QrTabelas.SQL.Clear;
    QrTabelas.SQL.Add('SHOW TABLES FROM '+ QrDatabases.Fields[0].AsString +
      ' LIKE "%' + EdLocTable.Text + '%"');
    QrTabelas.Open;
  end;
end;

end.
