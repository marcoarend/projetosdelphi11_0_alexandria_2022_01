program DermaAD2;

uses
  ExceptionLog,
  Forms,
  Windows,
  Messages,
  Dialogs,
  Module in 'Module.pas' {Dmod: TDataModule},
  DermaAD_MLA in 'DermaAD_MLA.pas' {FmDermaAD_MLA},
  Principal in 'Principal.pas' {FmPrincipal},
  Senhas in 'Senhas.pas' {FmSenhas},
  ZCF2 in '..\..\..\Outros\Geral\Units\ZCF2.pas',
  MyLinguas in '..\..\..\Outros\Geral\Units\MyLinguas.pas',
  MyVCLSkin in '..\..\..\Outros\Geral\Units\MyVCLSkin.pas',
  ResIntStrings in '..\..\..\Outros\Geral\Units\ResIntStrings.pas',
  UnInternalConsts2 in '..\..\..\Outros\Geral\Units\UnInternalConsts2.pas',
  UnInternalConsts3 in '..\..\..\Outros\Geral\Units\UnInternalConsts3.pas',
  UnInternalConsts in '..\..\..\Outros\Geral\Units\UnInternalConsts.pas',
  UnMLAGeral in '..\..\..\Outros\Geral\Units\UnMLAGeral.pas',
  UnMsgInt in '..\..\..\Outros\Geral\Units\UnMsgInt.pas',
  UnMyVCLref in '..\..\..\Outros\Geral\Units\UnMyVCLref.pas',
  MeuFrx in '..\..\..\Outros\Print\Geral\MeuFrx.pas' {FmMeuFrx},
  UsuariosNew in 'UsuariosNew.pas' {FmUsuariosNew},
  Empresa in 'Empresa.pas' {FmEmpresa},
  Travar in 'Travar.pas' {FmTravar},
  Travado in 'Travado.pas' {FmTravado},
  MyListas in 'MyListas.pas',
  VerifiDB in '..\..\..\Outros\MySQL\VerifiDB.pas' {FmVerifiDB},
  Backup3 in '..\..\..\Outros\MySQL\Backup3.pas' {FmBackup3},
  GModule in '..\..\..\Outros\MySQL\GModule.pas' {GMod: TDataModule},
  InstallMySQL41 in '..\..\..\Outros\MySQL\InstallMySQL41.pas' {FmInstallMySQL41},
  ModuleGeral in '..\..\..\Outros\MySQL\ModuleGeral.pas' {DModG: TDataModule},
  MyDBCheck in '..\..\..\Outros\MySQL\MyDBCheck.pas',
  MyInis in '..\..\..\Outros\MySQL\MyInis.pas' {FmMyInis},
  ServerConnect in '..\..\..\Outros\MySQL\ServerConnect.pas' {FmServerConnect},
  ServiceManager in '..\..\..\Outros\MySQL\ServiceManager.pas',
  ServicoManager in '..\..\..\Outros\MySQL\ServicoManager.pas' {FmServicoManager},
  Servidor in '..\..\..\Outros\MySQL\Servidor.pas' {FmServidor},
  UCreate in '..\..\..\Outros\MySQL\UCreate.pas',
  UMySQLModule in '..\..\..\Outros\MySQL\UMySQLModule.pas',
  UnGOTOy in '..\..\..\Outros\MySQL\UnGOTOy.pas',
  UnMySQLCuringa in '..\..\..\Outros\MySQL\UnMySQLCuringa.pas',
  UnLock_MLA in '..\..\..\Outros\Geral\Forms\UnLock_MLA.pas' {FmUnLock_MLA},
  CalcPercent in '..\..\..\Outros\Geral\Forms\CalcPercent.pas' {FmCalcPercent},
  Calculadora in '..\..\..\Outros\Geral\Forms\Calculadora.pas' {FmCalculadora},
  ConfJanela in '..\..\..\Outros\Geral\Forms\ConfJanela.pas' {FmConfJanela},
  Curinga in '..\..\..\Outros\Geral\Forms\Curinga.pas' {FmCuringa},
  DataDef in '..\..\..\Outros\Geral\Forms\DataDef.pas' {FmDataDef},
  Feriados in '..\..\..\Outros\Geral\Forms\Feriados.pas' {FmFeriados},
  GetData in '..\..\..\Outros\Geral\Forms\GetData.pas' {FmGetData},
  GetPercent in '..\..\..\Outros\Geral\Forms\GetPercent.pas' {FmGetPercent},
  GetPeriodo in '..\..\..\Outros\Geral\Forms\GetPeriodo.pas' {FmGetPeriodo},
  GetValor in '..\..\..\Outros\Geral\Forms\GetValor.pas' {FmGetValor},
  GetValues in '..\..\..\Outros\Geral\Forms\GetValues.pas' {FmGetValues},
  InfoSeq in '..\..\..\Outros\Geral\Forms\InfoSeq.pas' {FmInfoSeq},
  ListaSetores in '..\..\..\Outros\Geral\Forms\ListaSetores.pas' {FmListaSetores},
  MyDBGridReorder in '..\..\..\Outros\Geral\Forms\MyDBGridReorder.pas' {FmMyDBGridReorder},
  MyGlyfs in '..\..\..\Outros\Geral\Forms\MyGlyfs.pas' {FmMyGlyfs},
  MyGrid in '..\..\..\Outros\Geral\Forms\MyGrid.pas' {FmMyGrid},
  NomeX in '..\..\..\Outros\Geral\Forms\NomeX.pas' {FmNomeX},
  NovaVersao in '..\..\..\Outros\Geral\Forms\NovaVersao.pas' {FmNovaVersao},
  Periodo in '..\..\..\Outros\Geral\Forms\Periodo.pas' {FmPeriodo},
  Ponto in '..\..\..\Outros\Geral\Forms\Ponto.pas' {FmPonto},
  PontoFunci in '..\..\..\Outros\Geral\Forms\PontoFunci.pas' {FmPontoFunci},
  QuaisItens in '..\..\..\Outros\Geral\Forms\QuaisItens.pas' {FmQuaisItens},
  Recibo in '..\..\..\Outros\Geral\Forms\Recibo.pas' {FmRecibo},
  Recibos in '..\..\..\Outros\Geral\Forms\Recibos.pas' {FmRecibos},
  Senha in '..\..\..\Outros\Geral\Forms\Senha.pas' {FmSenha},
  SenhaBoss in '..\..\..\Outros\Geral\Forms\SenhaBoss.pas' {FmSenhaBoss},
  SenhaEspecial in '..\..\..\Outros\Geral\Forms\SenhaEspecial.pas' {FmSenhaEspecial},
  Terminais in '..\..\..\Outros\Geral\Forms\Terminais.pas' {FmTerminais},
  reinit in '..\..\..\Outros\Richedit\reinit.pas',
  SelCod in '..\..\..\Outros\Geral\Forms\SelCod.pas' {FmSelCod},
  PesqDescri in '..\..\..\Outros\Geral\Forms\PesqDescri.pas' {FmPesqDescri},
  PesqEntidade in '..\..\..\Outros\Entidades\PesqEntidade.pas' {FmPesqEntidade},
  PesqEntiNome in '..\..\..\Outros\Entidades\PesqEntiNome.pas' {FmPesqEntNome},
  ZStepCodUni in '..\..\..\Outros\ZStepCod\ZStepCodUni.pas' {FmZStepCodUni},
  CEP_Result in '..\..\..\Outros\CEP\CEP_Result.pas' {FmCEP_Result},
  VerifiDBLocal in '..\..\..\Outros\MySQL\VerifiDBLocal.pas' {FmVerifiDBLocal},
  ModuleFin in 'ModuleFin.pas' {DmodFin: TDataModule},
  IGPM in '..\..\..\Outros\Tabelas\IGPM.pas' {FmIGPM},
  ListServ in '..\..\..\Outros\Tabelas\ListServ.pas' {FmListServ},
  UnMyObjects in '..\..\..\Outros\Geral\Units\UnMyObjects.pas',
  PesqNome in '..\..\..\Outros\Geral\Forms\PesqNome.pas' {FmPesqNome},
  UnitMyXML in '..\..\..\XML\UnitMyXML.pas',
  SelLista in 'SelLista.pas' {FmSelLista},
  EmiteCheque_3 in '..\..\..\Outros\Print\Printers\EmiteCheque_3.pas' {FmEmiteCheque_3},
  UnBematech_DP_20 in '..\..\..\Outros\Print\Printers\UnBematech_DP_20.pas',
  EmiteCheque_1 in '..\..\..\Outros\Print\Printers\EmiteCheque_1.pas' {FmEmiteCheque_1},
  PertoChek in '..\..\..\Outros\Print\Printers\PertoChek.pas',
  EmiteCheque_0 in '..\..\..\Outros\Print\Printers\EmiteCheque_0.pas' {FmEmiteCheque_0},
  UnLic_Dmk in '..\..\..\Outros\Geral\Units\UnLic_Dmk.pas',
  GetBiosInformation in '..\..\..\Outros\Geral\Units\GetBiosInformation.pas',
  PCsNaNet in '..\..\..\Outros\internet\PCsNaNet.pas' {FmPCsNaNet},
  UnLock_Dmk in '..\UnLock\UnLock_Dmk.pas' {FmUnLock_Dmk},
  UnitMD5 in '..\..\..\Outros\Encrypt\UnitMD5.pas',
  MeuDBUses in '..\..\..\Outros\MySQL\MeuDBUses.pas' {FmMeuDBUses},
  SelRadioGroup in '..\..\..\Geral\SelRadioGroup.pas' {FmSelRadioGroup},
  PesqESel in '..\..\..\Outros\Geral\Forms\PesqESel.pas' {FmPesqESel},
  dmkSOAP in '..\..\..\Outros\WebService\dmkSOAP.pas',
  UnDmkProcFunc in '..\..\..\Geral\Units\UnDmkProcFunc.pas',
  UCreateEDrop in '..\..\..\Geral\Units\UCreateEDrop.pas',
  dmkDAC_PF in '..\..\..\Outros\MySQL\dmkDAC_PF.pas',
  GetDataEUser in '..\..\..\Outros\Geral\Forms\GetDataEUser.pas' {FmGetDataEUser},
  SelCods in '..\..\..\Outros\Geral\Forms\SelCods.pas' {FmSelCods},
  ZForge in '..\..\..\Outros\WinZIP\ZForge.pas' {FmZForge},
  EntiImagens in '..\..\..\Outros\Entidades\EntiImagens.pas' {FmEntiImagens},
  ObtemFoto3 in '..\..\..\Outros\Imagem\Camera\ObtemFoto3.pas' {FmObtemFoto3},
  ImgMarquee in '..\..\..\Outros\Imagem\ImgMarquee.pas',
  FlipReverseRotateLibrary in '..\..\..\Outros\Imagem\FlipReverseRotateLibrary.pas',
  Textos2 in '..\..\..\Outros\Textos\Textos2.pas' {FmTextos2},
  CartaG in '..\..\..\Outros\Textos\CartaG.pas' {FmCartaG},
  Cartas in '..\..\..\Outros\Textos\Cartas.pas' {FmCartas},
  Textos in '..\..\..\Outros\Textos\Textos.pas' {FmTextos},
  dmkGeral in '..\..\..\..\dmkComp\dmkGeral.pas',
  LinkRankSkin in '..\..\..\Outros\Geral\Forms\LinkRankSkin.pas' {FmLinkRankSkin},
  CoresVCLSkin in '..\DermaDB50\CoresVCLSkin.pas' {FmCoresVCLSkin},
  Usuarios in 'Usuarios.pas' {FmUsuarios},
  UnDmkWeb in '..\..\..\Outros\Web\UnDmkWeb.pas',
  Perfis in 'Perfis.pas' {FmPerfis},
  VerificaConexoes in '..\..\..\Outros\Geral\Forms\VerificaConexoes.pas' {FmVerificaConexoes};

{$R *.RES}

const
  NomeForm = 'FmDermaAD_MLA';
var
  Form : PChar;
begin
  Form := PChar(NomeForm);
  if OpenMutex(MUTEX_ALL_ACCESS, False, Form) = 0 then
  begin
    CreateMutex(nil, False, Form);
    Application.Initialize;
  try
      Application.Title := 'Derma AD';
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.CreateForm(TFmLinkRankSkin, FmLinkRankSkin);
  Application.CreateForm(TFmCoresVCLSkin, FmCoresVCLSkin);
  Application.CreateForm(TGMod, GMod);
  Application.CreateForm(TFmMeuDBUses, FmMeuDBUses);
  Application.CreateForm(TFmDermaAD_MLA, FmDermaAD_MLA);
  except
      Application.Terminate;
      Exit;
    end;
    Application.Run;
  end else begin
    Application.MessageBox('O Aplicativo j� esta em uso.', 'Erro',
    MB_OK+MB_ICONERROR);
    SetForeGroundWindow(FindWindow('TFmAcademy_MLA', Form));
  end;
(*begin
  if OpenMutex(MUTEX_ALL_ACCESS, False, 'FmDermaAD_MLA') = 0 then
  begin
    CreateMutex(nil, False, 'FmDermaAD_MLA');
    IC2_NOMEFORMAPP := 'FmDermaAD_MLA';
    Application.Initialize;
    Application.Title := 'Derma AD';
    Application.CreateForm(TFmPrincipal, FmPrincipal);
    Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
    Application.CreateForm(TGMod, GMod);
    FmDermaAD_MLA.Show;
    FmDermaAD_MLA.Update;
    Application.CreateForm(TDmod, Dmod);
    FmDermaAD_MLA.EdSenha.Text := FmDermaAD_MLA.EdSenha.Text+'*';
    FmDermaAD_MLA.EdSenha.Refresh;
    Application.CreateForm(TFmPrincipal, FmPrincipal);
    FmDermaAD_MLA.EdLogin.Text := '';
    FmDermaAD_MLA.EdSenha.Text := '';
    FmDermaAD_MLA.EdSenha.Refresh;
    FmDermaAD_MLA.EdLogin.ReadOnly := False;
    FmDermaAD_MLA.EdSenha.ReadOnly := False;
    FmDermaAD_MLA.EdLogin.SetFocus;
    FmDermaAD_MLA.Refresh;
    Application.Run;
  end
  else
  begin
    ShowMessage('O Aplicativo j� esta em uso');
    SetForeGroundWindow(FindWindow('TFmDermaAD_MLA', 'FmDermaAD_MLA'));
  end;*)
end.

