program DermaAD2;

uses
  Module in '..\Units\Module.pas' {Dmod: TDataModule},
  DermaAD_MLA in '..\Units\DermaAD_MLA.pas' {FmDermaAD_MLA},
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  Senhas in '..\Units\Senhas.pas' {FmSenhas},
  UsuariosNew in '..\Units\UsuariosNew.pas' {FmUsuariosNew},
  Travar in '..\Units\Travar.pas' {FmTravar},
  Travado in '..\Units\Travado.pas' {FmTravado},
  MyListas in '..\Units\MyListas.pas',
  ModuleFin in '..\Units\ModuleFin.pas' {DmodFin: TDataModule},
  SelLista in '..\Units\SelLista.pas' {FmSelLista},
  Usuarios in '..\Units\Usuarios.pas' {FmUsuarios},
  Perfis in '..\Units\Perfis.pas' {FmPerfis},
  Forms,
  Windows,
  Messages,
  Dialogs,
  ZCF2 in '..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  MyVCLSkin in '..\..\..\..\..\UTL\_UNT\v01_01\MyVCLSkin.pas',
  ResIntStrings in '..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnInternalConsts2 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts2.pas',
  UnInternalConsts3 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts3.pas',
  UnMLAGeral in '..\..\..\..\..\UTL\_UNT\v01_01\UnMLAGeral.pas',
  UnMsgInt in '..\..\..\..\..\UTL\_UNT\v01_01\UnMsgInt.pas',
  UnMyVCLref in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyVCLref.pas',
  MeuFrx in '..\..\..\..\..\UTL\Print\v01_01\MeuFrx.pas' {FmMeuFrx},
  VerifiDB in '..\..\..\..\..\UTL\MySQL\v01_01\VerifiDB.pas' {FmVerifiDB},
  Backup3 in '..\..\..\..\..\UTL\MySQL\v01_01\Backup3.pas' {FmBackup3},
  GModule in '..\..\..\..\..\UTL\MySQL\v01_01\GModule.pas' {GMod: TDataModule},
  InstallMySQL41 in '..\..\..\..\..\UTL\MySQL\v01_01\InstallMySQL41.pas' {FmInstallMySQL41},
  ModuleGeral in '..\..\..\..\..\UTL\MySQL\v01_01\ModuleGeral.pas' {DModG: TDataModule},
  MyDBCheck in '..\..\..\..\..\UTL\MySQL\v01_01\MyDBCheck.pas',
  MyInis in '..\..\..\..\..\UTL\MySQL\v01_01\MyInis.pas' {FmMyInis},
  ServerConnect in '..\..\..\..\..\UTL\MySQL\v01_01\ServerConnect.pas' {FmServerConnect},
  ServiceManager in '..\..\..\..\..\UTL\MySQL\v01_01\ServiceManager.pas',
  ServicoManager in '..\..\..\..\..\UTL\MySQL\v01_01\ServicoManager.pas' {FmServicoManager},
  Servidor in '..\..\..\..\..\UTL\MySQL\v01_01\Servidor.pas' {FmServidor},
  UCreate in '..\..\..\..\..\UTL\MySQL\v01_01\UCreate.pas',
  UMySQLModule in '..\..\..\..\..\UTL\MySQL\v01_01\UMySQLModule.pas',
  UnGOTOy in '..\..\..\..\..\UTL\MySQL\v01_01\UnGOTOy.pas',
  UnMySQLCuringa in '..\..\..\..\..\UTL\MySQL\v01_01\UnMySQLCuringa.pas',
  UnLock_MLA in '..\..\..\UnLock\v01_01\UnLock_MLA.pas' {FmUnLock_MLA},
  CalcPercent in '..\..\..\..\..\UTL\_FRM\v01_01\CalcPercent.pas' {FmCalcPercent},
  ConfJanela in '..\..\..\..\..\UTL\_FRM\v01_01\ConfJanela.pas' {FmConfJanela},
  Curinga in '..\..\..\..\..\UTL\_FRM\v01_01\Curinga.pas' {FmCuringa},
  DataDef in '..\..\..\..\..\UTL\_FRM\v01_01\DataDef.pas' {FmDataDef},
  Feriados in '..\..\..\..\..\UTL\_FRM\v01_01\Feriados.pas' {FmFeriados},
  GetData in '..\..\..\..\..\UTL\_FRM\v01_01\GetData.pas' {FmGetData},
  GetPercent in '..\..\..\..\..\UTL\_FRM\v01_01\GetPercent.pas' {FmGetPercent},
  GetPeriodo in '..\..\..\..\..\UTL\_FRM\v01_01\GetPeriodo.pas' {FmGetPeriodo},
  GetValor in '..\..\..\..\..\UTL\_FRM\v01_01\GetValor.pas' {FmGetValor},
  InfoSeq in '..\..\..\..\..\UTL\_FRM\v01_01\InfoSeq.pas' {FmInfoSeq},
  MyDBGridReorder in '..\..\..\..\..\UTL\_FRM\v01_01\MyDBGridReorder.pas' {FmMyDBGridReorder},
  MyGlyfs in '..\..\..\..\..\UTL\_FRM\v01_01\MyGlyfs.pas' {FmMyGlyfs},
  NomeX in '..\..\..\..\..\UTL\_FRM\v01_01\NomeX.pas' {FmNomeX},
  NovaVersao in '..\..\..\..\..\UTL\_FRM\v01_01\NovaVersao.pas' {FmNovaVersao},
  Periodo in '..\..\..\..\..\UTL\_FRM\v01_01\Periodo.pas' {FmPeriodo},
  Ponto in '..\..\..\..\..\UTL\_FRM\v01_01\Ponto.pas' {FmPonto},
  PontoFunci in '..\..\..\..\..\UTL\_FRM\v01_01\PontoFunci.pas' {FmPontoFunci},
  QuaisItens in '..\..\..\..\..\UTL\_FRM\v01_01\QuaisItens.pas' {FmQuaisItens},
  Recibo in '..\..\..\..\..\UTL\_FRM\v01_01\Recibo.pas' {FmRecibo},
  Recibos in '..\..\..\..\..\UTL\_FRM\v01_01\Recibos.pas' {FmRecibos},
  Senha in '..\..\..\..\..\UTL\_FRM\v01_01\Senha.pas' {FmSenha},
  SenhaBoss in '..\..\..\..\..\UTL\_FRM\v01_01\SenhaBoss.pas' {FmSenhaBoss},
  SenhaEspecial in '..\..\..\..\..\UTL\_FRM\v01_01\SenhaEspecial.pas' {FmSenhaEspecial},
  Terminais in '..\..\..\..\..\UTL\_FRM\v01_01\Terminais.pas' {FmTerminais},
  SelCod in '..\..\..\..\..\UTL\_FRM\v01_01\SelCod.pas' {FmSelCod},
  PesqDescri in '..\..\..\..\..\UTL\_FRM\v01_01\PesqDescri.pas' {FmPesqDescri},
  ZStepCodUni in '..\..\..\..\..\UTL\_FRM\v01_01\ZStepCodUni.pas' {FmZStepCodUni},
  CEP_Result in '..\..\..\..\..\MDL\ENT\v02_01\CEP\CEP_Result.pas' {FmCEP_Result},
  VerifiDBLocal in '..\..\..\..\..\UTL\MySQL\v01_01\VerifiDBLocal.pas' {FmVerifiDBLocal},
  IGPM in '..\..\..\..\..\UTL\Tabelas\v01_01\IGPM.pas' {FmIGPM},
  ListServ in '..\..\..\..\..\UTL\Tabelas\v01_01\ListServ.pas' {FmListServ},
  UnMyObjects in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  PesqNome in '..\..\..\..\..\UTL\_FRM\v01_01\PesqNome.pas' {FmPesqNome},
  UnitMyXML in '..\..\..\..\..\UTL\XML\v01_01\UnitMyXML.pas',
  UnLic_Dmk in '..\..\..\..\..\UTL\_UNT\v01_01\UnLic_Dmk.pas',
  GetBiosInformation in '..\..\..\..\..\UTL\_UNT\v01_01\GetBiosInformation.pas',
  UnLock_Dmk in '..\..\..\UnLock\v01_01\UnLock_Dmk.pas' {FmUnLock_Dmk},
  UnitMD5 in '..\..\..\..\..\UTL\Encrypt\v01_01\UnitMD5.pas',
  MeuDBUses in '..\..\..\..\..\UTL\MySQL\v01_01\MeuDBUses.pas' {FmMeuDBUses},
  SelRadioGroup in '..\..\..\..\..\UTL\_FRM\v01_01\SelRadioGroup.pas' {FmSelRadioGroup},
  PesqESel in '..\..\..\..\..\UTL\_FRM\v01_01\PesqESel.pas' {FmPesqESel},
  dmkSOAP in '..\..\..\..\..\MDL\WEB\v01_01\WebService\dmkSOAP.pas',
  UnDmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  UCreateEDrop in '..\..\..\..\..\UTL\_UNT\v01_01\UCreateEDrop.pas',
  dmkDAC_PF in '..\..\..\..\..\UTL\MySQL\v01_01\dmkDAC_PF.pas',
  GetDataEUser in '..\..\..\..\..\UTL\_FRM\v01_01\GetDataEUser.pas' {FmGetDataEUser},
  SelCods in '..\..\..\..\..\UTL\_FRM\v01_01\SelCods.pas' {FmSelCods},
  ZForge in '..\..\..\..\..\UTL\_FRM\v01_01\ZForge.pas' {FmZForge},
  EntiImagens in '..\..\..\..\..\MDL\ENT\v02_01\EntiImagens.pas' {FmEntiImagens},
  LinkRankSkin in '..\..\..\..\..\UTL\_FRM\v01_01\LinkRankSkin.pas' {FmLinkRankSkin},
  CoresVCLSkin in '..\..\..\..\..\UTL\_FRM\v01_01\CoresVCLSkin.pas' {FmCoresVCLSkin},
  UnDmkWeb in '..\..\..\..\..\MDL\WEB\v01_01\UnDmkWeb.pas',
  VerificaConexoes in '..\..\..\..\..\UTL\_FRM\v01_01\VerificaConexoes.pas' {FmVerificaConexoes},
  SelStr in '..\..\..\..\..\UTL\_FRM\v01_01\SelStr.pas' {FmSelStr},
  MailCfg in '..\..\..\..\..\MDL\MAIL\v01_01\MailCfg.pas' {FmMailCfg},
  UnMailEnv in '..\..\..\..\..\MDL\MAIL\v01_01\Units\VCL\UnMailEnv.pas',
  MailEnv in '..\..\..\..\..\MDL\MAIL\v01_01\MailEnv.pas' {FmMailEnv},
  MailEnvEmail in '..\..\..\..\..\MDL\MAIL\v01_01\MailEnvEmail.pas' {FmMailEnvEmail},
  UnMail in '..\..\..\..\..\MDL\MAIL\v01_01\Units\VCL\UnMail.pas',
  UnTreeView in '..\..\..\..\..\UTL\TreeView\v01_01\UnTreeView.pas',
  WebBrowser in '..\..\..\..\..\MDL\WEB\v01_01\WebBrowser.pas' {FmWebBrowser},
  WBFuncs in '..\..\..\..\..\UTL\WEB\HTML\v01_01\WBFuncs.pas',
  WLogin in '..\..\..\..\..\MDL\WEB\v01_01\WLogin.pas' {FmWLogin},
  WSuporte in '..\..\..\..\..\MDL\HELP\v01_01\Geral\WSuporte.pas' {FmWSuporte},
  WOrdSerIncRap in '..\..\..\..\..\MDL\HELP\v01_01\Geral\WOrdSerIncRap.pas' {FmWOrdSerIncRap},
  UnFTP in '..\..\..\..\..\MDL\ARQ\v01_01\Geral\UnFTP.pas',
  FTPUploads in '..\..\..\..\..\MDL\ARQ\v01_01\Geral\FTPUploads.pas' {FmFTPUploads},
  ApplicationConfiguration in '..\..\..\..\..\MDL\ARQ\v01_01\Geral\ApplicationConfiguration.pas',
  FTPSiteInfo in '..\..\..\..\..\MDL\ARQ\v01_01\Geral\FTPSiteInfo.pas',
  OrderFieldsImp in '..\..\..\..\..\UTL\_FRM\v01_01\OrderFieldsImp.pas' {FmOrderFieldsImp},
  AtivaCod in '..\..\..\..\..\UTL\_FRM\v01_01\AtivaCod.pas' {FmAtivaCod},
  Restaura2 in '..\..\..\..\..\UTL\MySQL\v01_01\Restaura2.pas' {FmRestaura2},
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  FilialUsrs in '..\..\..\..\..\MDL\EMP\v01_01\FilialUsrs.pas' {FmFilialUsrs},
  Matriz in '..\..\..\..\..\MDL\EMP\v01_01\Matriz.pas' {FmMatriz},
  Entidades in '..\..\..\..\..\MDL\ENT\v01_01\Entidades.pas' {FmEntidades},
  Entidade2 in '..\..\..\..\..\MDL\ENT\v02_01\Entidade2.pas' {FmEntidade2},
  EntidadesNew in '..\..\..\..\..\MDL\ENT\v01_01\EntidadesNew.pas' {FmEntidadesNew},
  EntidadesLoc in '..\..\..\..\..\MDL\ENT\v01_01\EntidadesLoc.pas' {FmEntidadesLoc},
  EntiTransp in '..\..\..\..\..\MDL\ENT\v02_01\EntiTransp.pas' {FmEntiTransp},
  EntiContat in '..\..\..\..\..\MDL\ENT\v02_01\EntiContat.pas' {FmEntiContat},
  EntiCargos in '..\..\..\..\..\MDL\ENT\v02_01\EntiCargos.pas' {FmEntiCargos},
  EntiMail in '..\..\..\..\..\MDL\ENT\v02_01\EntiMail.pas' {FmEntiMail},
  EntiTipCto in '..\..\..\..\..\MDL\ENT\v02_01\EntiTipCto.pas' {FmEntiTipCto},
  EntiTel in '..\..\..\..\..\MDL\ENT\v02_01\EntiTel.pas' {FmEntiTel},
  EntiRespon in '..\..\..\..\..\MDL\ENT\v02_01\EntiRespon.pas' {FmEntiRespon},
  EntiCtas in '..\..\..\..\..\MDL\ENT\v02_01\EntiCtas.pas' {FmEntiCtas},
  CNAE21Cad in '..\..\..\..\..\UTL\Tabelas\v01_01\CNAE21Cad.pas' {FmCNAE21Cad},
  CNAE21 in '..\..\..\..\..\UTL\Tabelas\v01_01\CNAE21.pas' {FmCNAE21},
  PesqRCNAE21 in '..\..\..\..\..\UTL\Tabelas\v01_01\PesqRCNAE21.pas' {FmPesqRCNAE21},
  EntidadesImp in '..\..\..\..\..\MDL\ENT\v02_01\EntidadesImp.pas' {FmEntidadesImp},
  EntiSel in '..\..\..\..\..\MDL\ENT\v02_01\EntiSel.pas' {FmEntiSel},
  UnConsultasWeb in '..\..\..\..\..\MDL\WEB\v01_01\UnConsultasWeb.pas',
  EntiRFCPF in '..\..\..\..\..\MDL\ENT\v02_01\EntiRFCPF.pas' {FmEntiRFCPF},
  EntiRFCNPJ in '..\..\..\..\..\MDL\ENT\v02_01\EntiRFCNPJ.pas' {FmEntiRFCNPJ},
  EntiRE_IE in '..\..\..\..\..\MDL\ENT\v02_01\EntiRE_IE.pas' {FmEntiRE_IE},
  EntiMapa in '..\..\..\..\..\MDL\ENT\v02_01\EntiMapa.pas' {FmEntiMapa},
  Entidade2Imp in '..\..\..\..\..\MDL\ENT\v02_01\Entidade2Imp.pas' {FmEntidade2Imp},
  Entidade2ImpOne in '..\..\..\..\..\MDL\ENT\v02_01\Entidade2ImpOne.pas' {FmEntidade2ImpOne},
  EntiSrvPro in '..\..\..\..\..\MDL\ENT\v02_01\EntiSrvPro.pas' {FmEntiSrvPro},
  CfgCadLista in '..\..\..\..\..\UTL\MySQL\v01_01\CfgCadLista.pas',
  CadLista in '..\..\..\..\..\UTL\MySQL\v01_01\CadLista.pas' {FmCadLista},
  CadItemGrupo in '..\..\..\..\..\UTL\MySQL\v01_01\CadItemGrupo.pas' {FmCadItemGrupo},
  CadRegistroSimples in '..\..\..\..\..\UTL\MySQL\v01_01\CadRegistroSimples.pas' {FmCadRegistroSimples},
  PesqCNAE21 in '..\..\..\..\..\UTL\Tabelas\v01_01\PesqCNAE21.pas' {FmPesqCNAE21},
  EntiImporCfg in '..\..\..\..\..\MDL\ENT\v02_01\EntiImporCfg.pas' {FmEntiImporCfg},
  ContatosEnt in '..\..\..\..\..\MDL\ENT\v02_01\ContatosEnt.pas' {FmContatosEnt},
  EntiConEnt in '..\..\..\..\..\MDL\ENT\v02_01\EntiConEnt.pas' {FmEntiConEnt},
  cfgAtributos in '..\..\..\..\..\UTL\_FRM\v01_01\cfgAtributos.pas',
  AtrDef in '..\..\..\..\..\UTL\_FRM\v01_01\AtrDef.pas' {FmAtrDef},
  AtrCad in '..\..\..\..\..\UTL\_FRM\v01_01\AtrCad.pas' {FmAtrCad},
  AtrIts in '..\..\..\..\..\UTL\_FRM\v01_01\AtrIts.pas' {FmAtrIts},
  EntiProd in '..\..\..\..\..\MDL\ENT\v01_01\EntiProd.pas' {FmEntiProd},
  EntiTransfere in '..\..\..\..\..\MDL\ENT\v01_01\EntiTransfere.pas' {FmEntiTransfere},
  EntiCliInt in '..\..\..\..\..\MDL\EMP\v01_01\EntiCliInt.pas' {FmEntiCliInt},
  UnInternalConsts in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnDmkImg in '..\..\..\..\..\UTL\Imagem\v01_01\UnDmkImg.pas',
  UnEntities in '..\..\..\..\..\MDL\ENT\v02_01\UnEntities.pas',
  Opcoes in '..\..\..\..\..\UTL\_FRM\v01_01\Opcoes.pas' {FmOpcoes},
  UnALL_Jan in '..\..\..\..\..\UTL\_UNT\v01_01\UnALL_Jan.pas',
  UnMailAllOS in '..\..\..\..\..\MDL\MAIL\v01_01\Units\AllOS\UnMailAllOS.pas',
  ReciboMany in '..\..\..\..\..\UTL\_FRM\v01_01\ReciboMany.pas' {FmReciboMany},
  PerfisJanelas in '..\Units\PerfisJanelas.pas' {FmPerfisJanelas},
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  HorVerao in '..\..\..\..\..\UTL\_FRM\v01_01\HorVerao.pas' {FmHorVerao},
  ABOUT in '..\..\..\..\..\UTL\_FRM\v01_01\ABOUT.pas' {FmAbout},
  UnGrl_Consts in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UMySQLDB in '..\..\..\..\..\UTL\MySQL\v01_01\UMySQLDB.pas',
  UnDmkHTML2 in '..\..\..\..\..\UTL\WEB\HTML\v01_01\UnDmkHTML2.pas',
  TextosHTML in '..\..\..\..\..\UTL\Textos\v01_01\TextosHTML.pas' {FmTextosHTML},
  TextosHTMLTab in '..\..\..\..\..\UTL\Textos\v01_01\TextosHTMLTab.pas' {FmTextosHTMLTab},
  UnTextos_Jan in '..\..\..\..\..\MDL\TEXT\v01_01\UnTextos_Jan.pas',
  Enti9Digit in '..\..\..\..\..\MDL\ENT\v02_01\Enti9Digit.pas' {FmEnti9Digit},
  UnWAceites in '..\..\..\..\..\MDL\TEXT\v01_01\Geral\UnWAceites.pas',
  WAceite in '..\..\..\..\..\MDL\TEXT\v01_01\Geral\WAceite.pas' {FmWAceite},
  MyTreeViewReorder in '..\..\..\..\..\UTL\TreeView\v01_01\MyTreeViewReorder.pas' {FmMyTreeViewReorder},
  UnAppPF in '..\Units\UnAppPF.pas',
  GerlShowGrid in '..\..\..\..\..\UTL\_FRM\v01_01\GerlShowGrid.pas' {FmGerlShowGrid},
  UnDmkListas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnDmkListas.pas',
  EntiAssina in '..\..\..\..\..\MDL\ENT\v02_01\EntiAssina.pas' {FmEntiAssina},
  LogOff in '..\..\..\..\..\UTL\_FRM\v01_01\LogOff.pas' {FmLogOff},
  Entidade3 in '..\..\..\..\..\MDL\ENT\v03_01\Entidade3.pas' {FmEntidade3},
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnGrl_Geral in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  EntiGrupos in '..\..\..\..\..\MDL\ENT\v02_01\EntiGrupos.pas' {FmEntiGrupos},
  EntiGruIts in '..\..\..\..\..\MDL\ENT\v02_01\EntiGruIts.pas' {FmEntiGruIts},
  UnReordena in '..\..\..\..\..\UTL\_UNT\v01_01\UnReordena.pas',
  UnitDmkTags in '..\..\..\..\..\UTL\_UNT\v01_01\UnitDmkTags.pas',
  UnCEP in '..\..\..\..\..\MDL\ENT\v02_01\CEP\UnCEP.pas',
  EntiCEP2 in '..\..\..\..\..\MDL\ENT\v02_01\CEP\EntiCEP2.pas' {FmEntiCEP2},
  UnProjGroup_Vars in '..\Units\UnProjGroup_Vars.pas',
  Vcl.Themes,
  Vcl.Styles,
  UnGrl_DmkREST in '..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_DmkREST.pas',
  UnServices in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\Comp\UnServices.pas',
  ConsultaCEP in '..\..\..\..\..\UTL\CEP\delphi-consulta-cep-master\ConsultaCEP.pas',
  CadListaOrdem in '..\..\..\..\..\UTL\MySQL\v01_01\CadListaOrdem.pas' {FmCadListaOrdem},
  ObtemFoto4 in '..\..\..\..\..\UTL\Imagem\v01_01\Camera\ObtemFoto4.pas' {FmObtemFoto4},
  VFrames in '..\..\..\..\..\UTL\Imagem\v01_01\Camera\Common\VFrames.pas',
  GdiPlus in '..\..\..\..\..\UTL\Imagem\v01_01\Camera\Common\GdiPlus\Lib\GdiPlus.pas',
  VSample in '..\..\..\..\..\UTL\Imagem\v01_01\Camera\Common\VSample.pas',
  EntiDocs2 in '..\..\..\..\..\MDL\ENT\v02_01\EntiDocs2.pas' {FmEntiDocs2},
  EntiDocLoad in '..\..\..\..\..\MDL\ENT\v02_01\EntiDocLoad.pas' {FmEntiDocLoad},
  Sel2RadioGroups in '..\..\..\..\..\UTL\_FRM\v01_01\Sel2RadioGroups.pas' {FmSel2RadioGroups},
  Sel3RadioGroups in '..\..\..\..\..\UTL\_FRM\v01_01\Sel3RadioGroups.pas' {FmSel3RadioGroups},
  SelCheckGroup in '..\..\..\..\..\UTL\_FRM\v01_01\SelCheckGroup.pas' {FmSelCheckGroup},
  GetDatasPeriodo in '..\..\..\..\..\UTL\_FRM\v01_01\GetDatasPeriodo.pas' {FmGetDatasPeriodo},
  Sel2Cod in '..\..\..\..\..\UTL\_FRM\v01_01\Sel2Cod.pas' {FmSel2Cod},
  UnProjGroup_Consts in '..\Units\UnProjGroup_Consts.pas',
  UnDmkPing in '..\..\..\..\..\UTL\_UNT\v01_01\UnDmkPing.pas',
  CAPICOM_TLB in '..\..\..\..\..\UTL\CAPICOM\v01_01\TLB\CAPICOM_TLB.pas',
  CNPJa in '..\..\..\..\..\UTL\_UNT\CNPJa.pas',
  CNPJaConsulta in '..\..\..\..\..\UTL\_UNT\ConsultaCNPJ\CNPJaConsulta.pas' {FmCNPJaConsulta},
  Pkg.Json.DTO in '..\..\..\..\..\UTL\_UNT\ConsultaCNPJ\Pkg.Json.DTO.pas',
  RootUnit in '..\..\..\..\..\UTL\_UNT\ConsultaCNPJ\RootUnit.pas',
  GetHora in '..\..\..\..\..\UTL\_FRM\v01_01\GetHora.pas' {FmGetHora},
  SelListArr in '..\..\..\..\..\UTL\_FRM\v01_01\SelListArr.pas' {FmSelListArr},
  TextoDeLista in '..\..\..\..\..\UTL\_FRM\v01_01\TextoDeLista.pas' {FmTextoDeLista};

{$R *.RES}

const
  NomeForm = 'FmDermaAD_MLA';
var
  Form : PChar;
begin
  Form := PChar(NomeForm);
  if CO_VERMCW > CO_VERMLA then
    CO_VERSAO := CO_VERMCW
  else
    CO_VERSAO := CO_VERMLA;
  if OpenMutex(MUTEX_ALL_ACCESS, False, Form) = 0 then
  begin
    CreateMutex(nil, False, Form);
    Application.Initialize;
  try
      TStyleManager.TrySetStyle('Windows10');
  Application.Title := 'Derma AD';
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.CreateForm(TFmLinkRankSkin, FmLinkRankSkin);
  Application.CreateForm(TFmCoresVCLSkin, FmCoresVCLSkin);
  Application.CreateForm(TGMod, GMod);
  Application.CreateForm(TFmMeuDBUses, FmMeuDBUses);
  Application.CreateForm(TFmDermaAD_MLA, FmDermaAD_MLA);
  except
      Application.Terminate;
      Exit;
    end;
    Application.Run;
  end else begin
    Application.MessageBox('O Aplicativo j� esta em uso.', 'Erro',
    MB_OK+MB_ICONERROR);
    SetForeGroundWindow(FindWindow('TFmAcademy_MLA', Form));
  end;
(*begin
  if OpenMutex(MUTEX_ALL_ACCESS, False, 'FmDermaAD_MLA') = 0 then
  begin
    CreateMutex(nil, False, 'FmDermaAD_MLA');
    IC2_NOMEFORMAPP := 'FmDermaAD_MLA';
    Application.Initialize;
    Application.Title := 'Derma AD';
    Application.CreateForm(TFmPrincipal, FmPrincipal);
    Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
    Application.CreateForm(TGMod, GMod);
    FmDermaAD_MLA.Show;
    FmDermaAD_MLA.Update;
    Application.CreateForm(TDmod, Dmod);
    FmDermaAD_MLA.EdSenha.Text := FmDermaAD_MLA.EdSenha.Text+'*';
    FmDermaAD_MLA.EdSenha.Refresh;
    Application.CreateForm(TFmPrincipal, FmPrincipal);
    FmDermaAD_MLA.EdLogin.Text := '';
    FmDermaAD_MLA.EdSenha.Text := '';
    FmDermaAD_MLA.EdSenha.Refresh;
    FmDermaAD_MLA.EdLogin.ReadOnly := False;
    FmDermaAD_MLA.EdSenha.ReadOnly := False;
    FmDermaAD_MLA.EdLogin.SetFocus;
    FmDermaAD_MLA.Refresh;
    Application.Run;
  end
  else
  begin
    ShowMessage('O Aplicativo j� esta em uso');
    SetForeGroundWindow(FindWindow('TFmDermaAD_MLA', 'FmDermaAD_MLA'));
  end;*)

(*
  ObtemFoto4 in '..\..\..\..\..\UTL\Imagem\v01_01\Camera\ObtemFoto4.pas' {FmObtemFoto4},
  VFrames in '..\..\..\..\..\UTL\Imagem\v01_01\Camera\Common\VFrames.pas',
  VSample in '..\..\..\..\..\UTL\Imagem\v01_01\Camera\Common\VSample.pas',
  EntiCEP in '..\..\..\..\..\MDL\ENT\v02_01\CEP\EntiCEP.pas' {FmEntiCEP},
*)
end.

