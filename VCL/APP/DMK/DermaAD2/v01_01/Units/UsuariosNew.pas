unit UsuariosNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DBCtrls, UnMsgInt, UnInternalConsts, UnInternalConsts2,
  Db, (*DBTables,*) UMySQLModule, ExtCtrls, mySQLDbTables, Variants, dmkEdit,
  dmkGeral, dmkImage, UndmkProcFunc, DmkDAC_PF, UnDmkEnums, dmkDBLookupComboBox,
  dmkEditCB;

type
  TFmUsuariosNew = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdNumero: TdmkEdit;
    EdLogin: TEdit;
    EdConfSenha: TEdit;
    EdSenha: TEdit;
    QrPerfis: TmySQLQuery;
    DsPerfis: TDataSource;
    QrPerfisCodigo: TIntegerField;
    QrPerfisDescricao: TWideStringField;
    Label6: TLabel;
    QrFuncionarios: TmySQLQuery;
    DsFuncionarios: TDataSource;
    QrFuncionariosCodigo: TIntegerField;
    QrFuncionariosNome: TWideStringField;
    CkAtivo: TCheckBox;
    SBFuncionario: TSpeedButton;
    EdIP_Default: TEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    QrLoc: TmySQLQuery;
    EdFuncionario: TdmkEditCB;
    CBFuncionario: TdmkDBLookupComboBox;
    EdPerfil: TdmkEditCB;
    CBPerfil: TdmkDBLookupComboBox;
    SBPerfil: TSpeedButton;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SBFuncionarioClick(Sender: TObject);
    procedure SBPerfilClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FLoginAtu: string;
    FNumeroAtu: Integer;
  end;

var
  FmUsuariosNew: TFmUsuariosNew;

implementation

uses Usuarios, Module, {$IFNDEF SemEntidade}ModuleGeral,{$ENDIF} UnMyObjects,
  Principal;

{$R *.DFM}

procedure TFmUsuariosNew.BtDesisteClick(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
     UMyMod.PoeEmLivreY(Dmod.MyDB, VAR_LIVRES, 'Senhas', Geral.IMV(EdNumero.Text))
  else
    UMyMod.UpdUnlockY(FmUsuarios.QrSenhasNumero.Value, Dmod.MyDB, 'Senhas', 'Numero');
  FmUsuarios.ImgTipo.SQLType := stLok;
  Close;
end;

procedure TFmUsuariosNew.BtConfirmaClick(Sender: TObject);
  function LocalizaUsuario(Usuario: String): Boolean;
  var
   SQL: String;
  begin
    if FNumeroAtu <> 0 then
      SQL := 'AND Numero <> ' + Geral.FF0(FNumeroAtu)
    else
      SQL := '';
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Login ',
    'FROM senhas ',
    'WHERE Login = "'+ Usuario +'" ',
    SQL,
    '']);
    if QrLoc.RecordCount > 0 then
      Result := True //J� existe
    else
      Result := False; //N�o existe
  end;
const
  SitSenha = 1;
  DataSenha = '000-00-00';
  SenhaDia = '';
var
  Funcionario, Perfil, Numero, Ativo: Integer;
  Senha, Login, IP_Default: String;
  Res: Boolean;
begin
  Funcionario := EdFuncionario.ValueVariant;
  Perfil      := EdPerfil.ValueVariant;
  //
  if (Funcionario = 0) and (Application.Title = 'Finan�as') then
  begin
    Geral.MensagemBox('Defina o Funcion�rio', 'Aviso', MB_OK+MB_ICONWARNING);
    CBFuncionario.SetFocus;
    Exit;
  end;
  if Perfil = 0 then
  begin
    Geral.MensagemBox(FIN_MSG_DEFPERFIL, 'Aviso', MB_OK+MB_ICONWARNING);
    CBPerfil.SetFocus;
    Exit;
  end;
  if EdLogin.Text = '' then
  begin
    Geral.MensagemBox(FIN_MSG_DEFLOGIN, 'Aviso', MB_OK+MB_ICONWARNING);
    EdLogin.SetFocus;
    Exit;
  end;
  if EdSenha.Text = '' then
  begin
    Geral.MensagemBox(FIN_MSG_DEFSENHA, 'Aviso', MB_OK+MB_ICONWARNING);
    EdSenha.SetFocus;
    Exit;
  end;
  if EdConfSenha.Text = '' then
  begin
    Geral.MensagemBox('Confirme a senha!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdConfSenha.SetFocus;
    Exit;
  end;
  if LocalizaUsuario(EdLogin.Text) then
  begin
    Geral.MensagemBox('Login indispon�vel!' + #13#10 +
      'Este login j� foi utilizado por outro usu�rio!' + #13#10 +
      'Escolha outro login e tente novamente!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdLogin.SetFocus;
    Exit;
  end;
  if EdSenha.Text <> EdConfSenha.Text then
  begin
    Geral.MensagemBox(FIN_MSG_SENHANAOCONFERE, 'Aviso', MB_OK+MB_ICONWARNING);
    EdSenha.SetFocus;
    Exit;
  end
  else
  begin
    Numero     := EdNumero.ValueVariant;
    Senha      := ' Senha=AES_ENCRYPT("' + EdSenha.Text + '", "' + CO_USERSPNOW + '")';
    Login      := EdLogin.Text;
    Ativo      := Geral.BoolToInt(CkAtivo.Checked);
    IP_Default := EdIP_Default.Text;
    //
    if FLoginAtu = '' then
      FLoginAtu := Login;
{
    if CkAtivo.Checked then Ativo := 'V' else Ativo := 'F';
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('');
    if ImgTipo.SQLType = stIns then
      Dmod.QrUpdU.SQL.Add('INSERT INTO senhas SET SitSenha=1, ')
    else Dmod.QrUpdU.SQL.Add('UPDATE senhas SET ');
    Dmod.QrUpdU.SQL.Add('Login=:P0');
    Dmod.QrUpdU.SQL.Add(',Senha=AES_ENCRYPT(:P1, :P2)');
    Dmod.QrUpdU.SQL.Add(',Perfil=:P3');
    Dmod.QrUpdU.SQL.Add(',Funcionario=:P4');
    Dmod.QrUpdU.SQL.Add(',Ativo=:P5');
    Dmod.QrUpdU.SQL.Add(',IP_Default=:P6');
    if ImgTipo.SQLType = stIns then
      Dmod.QrUpdU.SQL.Add(', Numero=:Px')
    else Dmod.QrUpdU.SQL.Add('WHERE Numero=:Px');
    Dmod.QrUpdU.Params[00].AsString  := EdLogin.Text;
    Dmod.QrUpdU.Params[01].AsString  := EdSenha.Text;
    Dmod.QrUpdU.Params[02].AsString  := CO_USERSPNOW;
    Dmod.QrUpdU.Params[03].AsInteger := Perfil;
    Dmod.QrUpdU.Params[04].AsInteger := Funcionario;
    Dmod.QrUpdU.Params[05].AsString  := Ativo;
    Dmod.QrUpdU.Params[06].AsString  := EdIP_Default.Text;
    //
    Dmod.QrUpdU.Params[07].AsInteger := Numero;
    Dmod.QrUpdU.ExecSQL;
}
    Res    := False;
    Numero := UMyMod.BPGS1I32('senhas', 'Numero', '', '', tsPos, ImgTipo.SQLType, Numero);
    if ImgTipo.SQLType = stIns then
    begin
      Res := UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'senhas', False, [
        'Numero', CO_JOKE_SQL, 'Perfil', 'Funcionario', 'DataSenha', 'SenhaDia',
        'SitSenha', 'IP_Default', 'Ativo'], ['Login'], [
        Numero, Senha, Perfil, Funcionario, DataSenha, SenhaDia,
        SitSenha, IP_Default, Ativo], [FLoginAtu], True);
    end else
    begin
      Res := UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'senhas', False, [
        'Numero', CO_JOKE_SQL, 'Perfil', 'Funcionario', 'DataSenha', 'SenhaDia',
        'SitSenha', 'IP_Default', 'Login', 'Ativo'], ['Login'], [
        Numero, Senha, Perfil, Funcionario, DataSenha, SenhaDia,
        SitSenha, IP_Default, Login, Ativo], [FLoginAtu], True);
    end;
    if Res then
    begin
      //BOSS e Admin
      if ((VAR_SENHA = Uppercase(VAR_BOSSSENHA)) and (VAR_LOGIN =
        UpperCase(VAR_BOSSLOGIN))) or (VAR_SENHA = CO_MASTER) or
        ((VAR_SENHA = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> '')) then
      begin
        if ImgTipo.SQLType = stIns then
        begin
          if Geral.MB_Pergunta('Deseja configurar agora a(s) empresa(s) que o usu�rio ter� acesso?') = ID_YES then
            FmPrincipal.MostraFilialUsrs();
        end;
      end;
      //
      FmUsuarios.LocCod(Numero, Numero);
    end;
  end;
  UMyMod.UpdUnlockY(FmUsuarios.QrSenhasNumero.Value, Dmod.MyDB, 'Senhas', 'Numero');
  UMyMod.UpdUnlockY(Numero, Dmod.MyDB, 'Senhas', 'Numero');
  FmUsuarios.ImgTipo.SQLType := stLok;
  Close;
end;

procedure TFmUsuariosNew.FormCreate(Sender: TObject);
begin
  QrPerfis.Open;
  QrFuncionarios.Close;
  QrFuncionarios.SQL.Clear;
  QrFuncionarios.SQL.Add('SELECT Codigo, Nome');
  QrFuncionarios.SQL.Add('FROM entidades');
  QrFuncionarios.SQL.Add('WHERE Nome<>"" ');
  QrFuncionarios.SQL.Add('AND Tipo=1');
  QrFuncionarios.SQL.Add('ORDER BY Nome');
  QrFuncionarios.Open;
  //
  if VAR_USUARIO < 0 then
  begin
    EdPerfil.Enabled := True;
    CBPerfil.Enabled := True;
    SBPerfil.Enabled := True;
    //
    EdFuncionario.Enabled := True;
    CBFuncionario.Enabled := True;
    SBFuncionario.Enabled := True;
  end;
end;

procedure TFmUsuariosNew.SBFuncionarioClick(Sender: TObject);
{$IFNDEF SemEntidade}
var
  Codigo: Integer;
{$ENDIF}
begin
  {$IFNDEF SemEntidade}
  Codigo := EdFuncionario.ValueVariant;
  //
  DModG.CadastroDeEntidade(Codigo, fmcadEntidade2, fmcadEntidade2, True);
  //
  if VAR_ENTIDADE <> 0 then
  begin
    QrFuncionarios.Close;
    QrFuncionarios.Open;
    //
    if QrFuncionarios.Locate('Codigo', VAR_ENTIDADE, []) then
    begin
      EdFuncionario.ValueVariant := VAR_ENTIDADE;
    end else begin
      Geral.MB_Aviso('O funcion�rio n� ' + Geral.FF0(VAR_ENTIDADE) +
        ' n�o foi localizado. Certifique-se de t�-lo cadastrado ' +
        'como funcion�rio no cadastro das entidades!');
      EdFuncionario.SetFocus;
      Exit;
    end;
  end;
  {$ENDIF}
end;

procedure TFmUsuariosNew.SBPerfilClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := EdPerfil.ValueVariant;
  //
  FmPrincipal.MostraPerfis(Codigo);
  //
  if VAR_MARCACAR <> 0 then
  begin
    QrPerfis.Close;
    QrPerfis.Open;
    //
    EdPerfil.ValueVariant := VAR_MARCACAR;
    CBPerfil.KeyValue     := VAR_MARCACAR;
    EdPerfil.SetFocus;
  end;
end;

procedure TFmUsuariosNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdLogin.SetFocus;
end;

end.

