unit Module;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) UnInternalConsts, mySQLDbTables, MySQLBatch, frxClass, frxDBSet,
  dmkGeral;

type
  TDmod = class(TDataModule)
    QrUpdL: TmySQLQuery;
    QrUpdM: TmySQLQuery;
    QrUser: TmySQLQuery;
    QrUserLogin: TWideStringField;
    QrLocaliza: TmySQLQuery;
    QrLocalizaRecord: TIntegerField;
    QrFields: TmySQLQuery;
    QrUpdX: TmySQLQuery;
    QrDataBalX: TmySQLQuery;
    QrDataBalXData: TDateField;
    QrRecCountX: TmySQLQuery;
    QrRecCountXRecord: TIntegerField;
    QrLocDataX: TmySQLQuery;
    QrLocDataXRecord: TDateField;
    QrDelLogX: TmySQLQuery;
    QrInsLogX: TmySQLQuery;
    QrCountX: TmySQLQuery;
    QrCountXRecord: TIntegerField;
    QrSelX: TmySQLQuery;
    QrSelXLk: TIntegerField;
    Query1: TmySQLQuery;
    QrLocX: TmySQLQuery;
    QrLocXRecord: TIntegerField;
    QrDuplicStrX: TmySQLQuery;
    QrDuplicStrXNOME: TWideStringField;
    QrDuplicStrXCODIGO: TIntegerField;
    QrDuplicStrXANTERIOR: TIntegerField;
    QrDuplicIntX: TmySQLQuery;
    QrDuplicIntXINTEIRO1: TIntegerField;
    QrDuplicIntXINTEIRO2: TIntegerField;
    QrDuplicIntXCODIGO: TIntegerField;
    QrLivreX: TmySQLQuery;
    QrLivreXCodigo: TIntegerField;
    QrLivreX_D: TmySQLQuery;
    QrLivreX_DCodigo: TFloatField;
    QrUpdU: TmySQLQuery;
    QrSenha: TmySQLQuery;
    QrSenhaLogin: TWideStringField;
    QrSenhaNumero: TIntegerField;
    QrSenhaSenha: TWideStringField;
    QrSenhaPerfilF: TIntegerField;
    QrSenhaLk: TIntegerField;
    QrSenhaPerfilW: TIntegerField;
    QrDuplic: TmySQLQuery;
    QrDuplicNOME: TWideStringField;
    QrDuplicCODIGO: TIntegerField;
    QrUpdW: TmySQLQuery;
    MyLocDatabase: TmySQLDatabase;
    MyDB: TmySQLDatabase;
    QrAux: TmySQLQuery;
    QrPriorNext: TmySQLQuery;
    StringField1: TWideStringField;
    QrMaster2: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrControle: TmySQLQuery;
    ZZDB: TmySQLDatabase;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrAgora: TmySQLQuery;
    StringField2: TWideStringField;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroNOMEpUF: TWideStringField;
    QrTerceiroNOMEeUF: TWideStringField;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TWideStringField;
    QrTerceiroFantasia: TWideStringField;
    QrTerceiroRespons1: TWideStringField;
    QrTerceiroRespons2: TWideStringField;
    QrTerceiroPai: TWideStringField;
    QrTerceiroMae: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroIEST: TWideStringField;
    QrTerceiroNome: TWideStringField;
    QrTerceiroApelido: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroRG: TWideStringField;
    QrTerceiroELograd: TSmallintField;
    QrTerceiroERua: TWideStringField;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroECompl: TWideStringField;
    QrTerceiroEBairro: TWideStringField;
    QrTerceiroECidade: TWideStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TWideStringField;
    QrTerceiroETe1: TWideStringField;
    QrTerceiroEte2: TWideStringField;
    QrTerceiroEte3: TWideStringField;
    QrTerceiroECel: TWideStringField;
    QrTerceiroEFax: TWideStringField;
    QrTerceiroEEmail: TWideStringField;
    QrTerceiroEContato: TWideStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPLograd: TSmallintField;
    QrTerceiroPRua: TWideStringField;
    QrTerceiroPNumero: TIntegerField;
    QrTerceiroPCompl: TWideStringField;
    QrTerceiroPBairro: TWideStringField;
    QrTerceiroPCidade: TWideStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TWideStringField;
    QrTerceiroPTe1: TWideStringField;
    QrTerceiroPte2: TWideStringField;
    QrTerceiroPte3: TWideStringField;
    QrTerceiroPCel: TWideStringField;
    QrTerceiroPFax: TWideStringField;
    QrTerceiroPEmail: TWideStringField;
    QrTerceiroPContato: TWideStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TWideStringField;
    QrTerceiroResponsavel: TWideStringField;
    QrTerceiroProfissao: TWideStringField;
    QrTerceiroCargo: TWideStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TWideStringField;
    QrTerceiroCliente2: TWideStringField;
    QrTerceiroFornece1: TWideStringField;
    QrTerceiroFornece2: TWideStringField;
    QrTerceiroFornece3: TWideStringField;
    QrTerceiroFornece4: TWideStringField;
    QrTerceiroTerceiro: TWideStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TWideStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TWideStringField;
    QrTerceiroObservacoes: TWideMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroCLograd: TSmallintField;
    QrTerceiroCRua: TWideStringField;
    QrTerceiroCNumero: TIntegerField;
    QrTerceiroCCompl: TWideStringField;
    QrTerceiroCBairro: TWideStringField;
    QrTerceiroCCidade: TWideStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TWideStringField;
    QrTerceiroCTel: TWideStringField;
    QrTerceiroCCel: TWideStringField;
    QrTerceiroCFax: TWideStringField;
    QrTerceiroCContato: TWideStringField;
    QrTerceiroLLograd: TSmallintField;
    QrTerceiroLRua: TWideStringField;
    QrTerceiroLNumero: TIntegerField;
    QrTerceiroLCompl: TWideStringField;
    QrTerceiroLBairro: TWideStringField;
    QrTerceiroLCidade: TWideStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TWideStringField;
    QrTerceiroLTel: TWideStringField;
    QrTerceiroLCel: TWideStringField;
    QrTerceiroLFax: TWideStringField;
    QrTerceiroLContato: TWideStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TWideStringField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroAccount: TIntegerField;
    QrTerceiroLogo2: TBlobField;
    QrTerceiroConjugeNome: TWideStringField;
    QrTerceiroConjugeNatal: TDateField;
    QrTerceiroNome1: TWideStringField;
    QrTerceiroNatal1: TDateField;
    QrTerceiroNome2: TWideStringField;
    QrTerceiroNatal2: TDateField;
    QrTerceiroNome3: TWideStringField;
    QrTerceiroNatal3: TDateField;
    QrTerceiroNome4: TWideStringField;
    QrTerceiroNatal4: TDateField;
    QrTerceiroCreditosI: TIntegerField;
    QrTerceiroCreditosL: TIntegerField;
    QrTerceiroCreditosF2: TFloatField;
    QrTerceiroCreditosD: TDateField;
    QrTerceiroCreditosU: TDateField;
    QrTerceiroCreditosV: TDateField;
    QrTerceiroMotivo: TIntegerField;
    QrTerceiroQuantI1: TIntegerField;
    QrTerceiroQuantI2: TIntegerField;
    QrTerceiroQuantI3: TIntegerField;
    QrTerceiroQuantI4: TIntegerField;
    QrTerceiroQuantN1: TFloatField;
    QrTerceiroQuantN2: TFloatField;
    QrTerceiroAgenda: TWideStringField;
    QrTerceiroSenhaQuer: TWideStringField;
    QrTerceiroSenha1: TWideStringField;
    QrTerceiroLimiCred: TFloatField;
    QrTerceiroDesco: TFloatField;
    QrTerceiroCasasApliDesco: TSmallintField;
    QrTerceiroTempD: TFloatField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TIntegerField;
    QrTerceiroUserAlt: TIntegerField;
    QrTerceiroCPF_Pai: TWideStringField;
    QrTerceiroSSP: TWideStringField;
    QrTerceiroCidadeNatal: TWideStringField;
    QrTerceiroUFNatal: TSmallintField;
    QrMas: TmySQLQuery;
    QrTerminais: TmySQLQuery;
    QrEndereco: TmySQLQuery;
    QrEnderecoCodigo: TIntegerField;
    QrEnderecoCadastro: TDateField;
    QrEnderecoNOMEDONO: TWideStringField;
    QrEnderecoCNPJ_CPF: TWideStringField;
    QrEnderecoIE_RG: TWideStringField;
    QrEnderecoNIRE_: TWideStringField;
    QrEnderecoRUA: TWideStringField;
    QrEnderecoNUMERO: TLargeintField;
    QrEnderecoCOMPL: TWideStringField;
    QrEnderecoBAIRRO: TWideStringField;
    QrEnderecoCIDADE: TWideStringField;
    QrEnderecoNOMELOGRAD: TWideStringField;
    QrEnderecoNOMEUF: TWideStringField;
    QrEnderecoPais: TWideStringField;
    QrEnderecoLograd: TLargeintField;
    QrEnderecoTipo: TSmallintField;
    QrEnderecoCEP: TLargeintField;
    QrEnderecoTE1: TWideStringField;
    QrEnderecoFAX: TWideStringField;
    QrEnderecoENatal: TDateField;
    QrEnderecoPNatal: TDateField;
    QrEnderecoECEP_TXT: TWideStringField;
    QrEnderecoNUMERO_TXT: TWideStringField;
    QrEnderecoE_ALL: TWideStringField;
    QrEnderecoCNPJ_TXT: TWideStringField;
    QrEnderecoFAX_TXT: TWideStringField;
    QrEnderecoTE1_TXT: TWideStringField;
    QrEnderecoNATAL_TXT: TWideStringField;
    QrEnderecoRespons1: TWideStringField;
    QlLocal: TMySQLBatchExecute;
    QrSSit: TmySQLQuery;
    QrSSitSitSenha: TSmallintField;
    QrBSit: TmySQLQuery;
    QrBSitSitSenha: TSmallintField;
    QrBoss: TmySQLQuery;
    QrBossMasSenha: TWideStringField;
    QrBossMasLogin: TWideStringField;
    QrBossEm: TWideStringField;
    QrBossCNPJ: TWideStringField;
    QrBossMasZero: TWideStringField;
    QrMaster2SolicitaSenha: TSmallintField;
    QrMaster2Em: TWideStringField;
    QrMaster2CNPJ: TWideStringField;
    QrMaster2Monitorar: TSmallintField;
    QrMaster2Licenca: TWideStringField;
    QrMaster2Distorcao: TIntegerField;
    QrMaster2DataI: TDateField;
    QrMaster2DataF: TDateField;
    QrMaster2Hoje: TDateField;
    QrMaster2Hora: TTimeField;
    QrMaster2MasLogin: TWideStringField;
    QrMaster2MasSenha: TWideStringField;
    QrMaster2MasAtivar: TWideStringField;
    QrMaster2Limite: TSmallintField;
    QrMaster2SitSenha: TSmallintField;
    QrTerminaisIP: TWideStringField;
    QrTerminaisTerminal: TIntegerField;
    QrTerminaisLicenca: TWideStringField;
    QrSenhas: TmySQLQuery;
    QrSenhasSenhaNew: TWideStringField;
    QrSenhasSenhaOld: TWideStringField;
    QrSenhaslogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasFuncionario: TIntegerField;
    QrAuxL: TmySQLQuery;
    QrUpdZ: TmySQLQuery;
    QrLocY: TmySQLQuery;
    QrLocYRecord: TIntegerField;
    QrCountY: TmySQLQuery;
    QrCountYRecord: TIntegerField;
    QrErrQuit: TmySQLQuery;
    QrErrQuitControle1: TIntegerField;
    QrErrQuitControle2: TIntegerField;
    QrErrQuitData: TDateField;
    QrErrQuitSit: TIntegerField;
    QrErrQuitDescricao: TWideStringField;
    QrSQL: TmySQLQuery;
    QrIdx: TmySQLQuery;
    mySQLQuery1: TmySQLQuery;
    mySQLQuery2: TmySQLQuery;
    mySQLQuery3: TmySQLQuery;
    mySQLQuery4: TmySQLQuery;
    mySQLQuery5: TmySQLQuery;
    QrNTV: TmySQLQuery;
    QrNTI: TmySQLQuery;
    QrControleDono: TIntegerField;
    QrControleMoeda: TWideStringField;
    QrControleCorRecibo: TIntegerField;
    QrControleContraSenha: TWideStringField;
    frxDsMaster: TfrxDBDataset;
    QrControleMoraDD: TFloatField;
    QrControleMulta: TFloatField;
    QrControleMensalSempre: TSmallintField;
    QrControleEntraSemValor: TSmallintField;
    QrControleIdleMinutos: TIntegerField;
    QrControleMyPagCar: TSmallintField;
    QrControleMyPgQtdP: TSmallintField;
    QrControleMyPgPeri: TSmallintField;
    QrControleMyPgDias: TSmallintField;
    QrControleVendaCartPg: TIntegerField;
    QrControleVendaParcPg: TIntegerField;
    QrControleVendaPeriPg: TIntegerField;
    QrControleVendaDiasPg: TIntegerField;
    QrControleCNABCtaJur: TIntegerField;
    QrControleCNABCtaMul: TIntegerField;
    QrControleCNABCtaTar: TIntegerField;
    QrControleMeuLogoPath: TWideStringField;
    QrControleLogoNF: TWideStringField;
    QrControleLogoBig1: TWideStringField;
    QrControleMoedaBr: TIntegerField;
    QrSenhasIP_Default: TWideStringField;
    QrEnderecoNO_TIPO_DOC: TWideStringField;
    QrDono: TmySQLQuery;
    QrDonoCodigo: TIntegerField;
    QrDonoCadastro: TDateField;
    QrDonoNOMEDONO: TWideStringField;
    QrDonoCNPJ_CPF: TWideStringField;
    QrDonoIE_RG: TWideStringField;
    QrDonoNIRE_: TWideStringField;
    QrDonoRUA: TWideStringField;
    QrDonoCOMPL: TWideStringField;
    QrDonoBAIRRO: TWideStringField;
    QrDonoCIDADE: TWideStringField;
    QrDonoNOMELOGRAD: TWideStringField;
    QrDonoNOMEUF: TWideStringField;
    QrDonoPais: TWideStringField;
    QrDonoLograd: TLargeintField;
    QrDonoTipo: TSmallintField;
    QrDonoCEP: TLargeintField;
    QrDonoTE1: TWideStringField;
    QrDonoTE2: TWideStringField;
    QrDonoFAX: TWideStringField;
    QrDonoENatal: TDateField;
    QrDonoPNatal: TDateField;
    QrDonoECEP_TXT: TWideStringField;
    QrDonoNUMERO_TXT: TWideStringField;
    QrDonoCNPJ_TXT: TWideStringField;
    QrDonoFAX_TXT: TWideStringField;
    QrDonoTE1_TXT: TWideStringField;
    QrDonoTE2_TXT: TWideStringField;
    QrDonoNATAL_TXT: TWideStringField;
    QrDonoE_LNR: TWideStringField;
    QrDonoE_CUC: TWideStringField;
    QrDonoEContato: TWideStringField;
    QrDonoECel: TWideStringField;
    QrDonoCEL_TXT: TWideStringField;
    QrDonoE_LN2: TWideStringField;
    QrDonoAPELIDO: TWideStringField;
    QrDonoNUMERO: TFloatField;
    QrDonoRespons1: TWideStringField;
    QrDonoRespons2: TWideStringField;
    QrDonoFONES: TWideStringField;
    QrMaster: TmySQLQuery;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterLimite: TSmallintField;
    QrMasterSolicitaSenha: TIntegerField;
    QrMasterUsaAccMngr: TSmallintField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrEnderecoCalcFields(DataSet: TDataSet);
    procedure QrDonoCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenMaster();
  public
    { Public declarations }
    procedure VerificaSenha(Index: Integer; Login, Senha: String);
    function Privilegios(Usuario : Integer) : Boolean;
    procedure RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
    function  TabelasQueNaoQueroCriar(): String;
    procedure ReopenControle();
    procedure ReopenParamsEspecificos(Empresa: Integer);
  end;

var
  Dmod: TDmod;
  FVerifi: Boolean;

implementation

uses UnMyObjects, Principal, UnMLAGeral, DermaAD_MLA, SenhaBoss, UMySQLModule, Travado,
UnGOTOy, ModuleGeral, MyListas, DmkDAC_PF, UnDmkProcFunc;

{$R *.DFM}

procedure TDmod.DataModuleCreate(Sender: TObject);
var
  I, Server: Integer;
  DataBase, IP, OthSetCon: String;
begin
  //
  if MyDB.Connected then
    Application.MessageBox('MyDB est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  if ZZDB.Connected then
    Application.MessageBox('ZZDB est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  if MyLocDataBase.Connected then
    Application.MessageBox('MyLocDataBase est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  //
  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  Geral.WriteAppKeyCU('Porta', 'Dermatek', VAR_PORTA, ktInteger);
  VAR_BDSENHA := 'wkljweryhvbirt';
  //
(*
  ZZDB.UserPassword := VAR_BDSENHA;
  ZZDB.Port         := VAR_PORTA;
  try
    ZZDB.Connected := True;
  except
    ZZDB.UserPassword := '852456';
    try
      ZZDB.Connected := True;
      QrUpd.Database := ZZDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE user SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      if VAR_SERVIDOR = 2 then ShowMessage('Banco de dados teste n�o se conecta!');
    end;
  end;
*)
  //
{
  MyDB.Connected := False;
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.DataBaseName := '';
}
  //
  MyLocDataBase.Connected := False;
  MyLocDataBase.UserPassword := VAR_BDSENHA;
  MyLocDataBase.DataBaseName := '';
  //
  //DBMLocal.AliasName := '';
  //DBMMoney.AliasName := '';
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  try
    Database := Geral.ReadAppKeyCU('Cashier_Database', 'Dermatek', ktString, '');
    //FmDermaAD_MLA.Info.Lines.Add('Database: '+Database);
    if Database = '' then
    begin
      //FmDermaAD_MLA.Info.Lines.Add('Database do Cashier n�o foi definido');
      if InputQuery('Database para conex�o','Informe o database',Database) then
        Geral.WriteAppKeyCU('Cashier_Database', 'Dermatek', Database, ktString)
      else begin
        //FmDermaAD_MLA.Info.Lines.Add('Conex�o abortada pelo usu�rio!');
        Application.MessageBox('Conex�o abortada pelo usu�rio!', 'Aviso',
        MB_OK+MB_ICONEXCLAMATION);
        Application.Terminate;
        Exit;
      end;
    end;
    //FmDermaAD_MLA.Info.Lines.Add('Databse: '+Database);
    if Database = '' then //ShowMessage('Database n�o defenido');
    begin
      Application.MessageBox('Database n�o definido ou inexistente! O aplicativo ser� encerrado',
      'Erro', MB_OK+MB_ICONERROR);
      Application.Terminate;
      Exit;
    end;
    TMeuDB := DataBase;
    //FmDermaAD_MLA.Info.Lines.Add('DBMMoney.AliasName = '+VAR_BDCASHIER);
  except
    ShowMessage('Imposs�vel conectar database - Verifique a configura��o da ODBC.');
    Application.Terminate;
    Exit;
  end;
  //
  try
    Database := Geral.ReadAppKey('CashLoc_Database', 'Dermatek', ktString,
                  '', HKEY_LOCAL_MACHINE);
    //FmDermaAD_MLA.Info.Lines.Add('Database Local = '+Database);
    if Database = '' then
    begin
      if InputQuery('Database para conex�o','Informe o database',Database) then
        Geral.WriteAppKeyCU('CashLoc_Database', 'Dermatek', Database, ktString)
      else begin
        Application.MessageBox('Conex�o abortada pelo usu�rio!', 'Aviso',
        MB_OK+MB_ICONEXCLAMATION);
        Application.Terminate;
        Exit;
      end;
    end;
    //ODBC := 'ODBC\ODBC.INI\'+Database;
    //Database := Geral.ReadAppKey('Database', ODBC, ktString, '', HKEY_CURRENT_USER);
    //if Database = '' then
    //FmDermaAD_MLA.Info.Lines.Add('Database local n�o definido.');
    TLocDB := 'locbderm' //DataBase;
    //FmDermaAD_MLA.Info.Lines.Add('DBMMoney.AliasName = '+VAR_BDCASHLOC);
(*    DBMLocal.AliasName := Database;
    try
      DBMLocal.Connected := True;
      FmDermaAD_MLA.Info.Lines.Add('Database local conectado com sucesso.');
    except
      FmDermaAD_MLA.Info.Lines.Add('N�o foi poss�vel conectar database local.');
    end;*)
  except
    ShowMessage('Imposs�vel conectar database local - Verifique a configura��o da ODBC.');
    Application.Terminate;
    Exit;
  end;
  //
(*  try
    DBMLocal.Connected := False;
    DBMLocal.AliasName := VAR_BDCASHLOC;
    DBMLocal.Connected := True;
    FmDermaAD_MLA.Info.Lines.Add('DBMLocal conectado com sucesso.');
  except
    ShowMessage('O alias "'+VAR_BDCASHLOC+'" n�o existe, ou n�o est� cadastrado no ODBC.');
    Application.Terminate;
    Exit;
  end;
  try
    DBMMoney.Connected := False;
    DBMMoney.AliasName := VAR_BDCASHIER;
    DBMMoney.Connected := True;
    FmDermaAD_MLA.Info.Lines.Add('DBMMoney conectado com sucesso.');
  except
    ShowMessage('O alias "'+VAR_BDCASHIER+'" n�o existe, ou n�o est� cadastrado no ODBC.');
    Application.Terminate;
    Exit;
  end;*)

  //AtzRecovery.AtualizaVersao;

  VAR_FORMATDATE       := 'yyyy/mm/dd';
  VAR_FORMATDATE2      := 'dd/mm/yyyy';
  VAR_FORMATDATE3      := 'dd/mm/yy'; // para labels, n�o usar para c�lculo!
  VAR_FORMATDATE4      := 'yyyymmdd';
  VAR_FORMATTIMESTAMP4 := 'mm/yy';
  VAR_FORMATTIME       := 'hh:nn:ss';
  VAR_FORMATTIME2      := 'hh:nn';
  //
  if ParamCount > 0 then
  begin
    for i := 0 to ParamCount do
    begin
      if i = 3 then //IP aplicativo origem
      begin
        if ParamStr(i) <> '#' then
          IP := ParamStr(i);
      end;
    end;
  end;
  if IP = '' then
    IP := Geral.ReadAppKey('IPServer', Application.Title, ktString, CO_VAZIO, HKEY_LOCAL_MACHINE);
  //
  if IP = CO_VAZIO then
  begin
    IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
  VAR_IP := IP;
  FmPrincipal.StatusBar1.Panels[03].Text := ' ' + VAR_IP + ' : ' + FormatFloat('0', VAR_PORTA);
  FmPrincipal.StatusBar1.Panels[07].Text := ' ' + Geral.FileVerInfo(Application.ExeName, 3);
  FmPrincipal.StatusBar1.Panels[05].Text := ' ' + TMeuDB;
  FmPrincipal.StatusBar1.Update;
  Application.ProcessMessages;
{
  MyDB.DataBaseName := TMeuDB;
  MyDB.Host := VAR_IP;
  MyDB.Port := VAR_PORTA;
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.Connected := True;
}

  // 2019-09-27
  OthSetCon := Geral.ReadAppKeyCU('OthSetCon', Application.Title, ktString, '');
  if OthSetCon = EmptyStr then
    VAR_BDSENHA := CO_USERSPNOW
  else
    VAR_BDSENHA := dmkPF.Criptografia(OthSetCon, CO_LLM_SecureStr);
  //
  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  Server := Geral.ReadAppKeyCU('Server', Application.Title, ktInteger, 0);
  //
  case Server of
    1:   VAR_SERVIDOR := 0;
    2:   VAR_SERVIDOR := 1;
    3:   VAR_SERVIDOR := 2;
    else VAR_SERVIDOR := -1;
  end;
  VAR_IP       := Geral.ReadAppKeyCU('IPServer', Application.Title, ktString, CO_VAZIO);
  DataBase     := Geral.ReadAppKeyCU('Database', Application.Title, ktString, 'mysql');
  VAR_SQLUSER  := Geral.ReadAppKeyCU('OthUser', Application.Title, ktString, 'root');
  if VAR_BDSENHA = '' then
    VAR_BDSENHA := CO_USERSPNOW;
  if VAR_SQLUSER = '' then
    VAR_SQLUSER := 'root';
{
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, TMeuDB, VAR_IP, VAR_PORTA, 'root',
  VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)True);
}
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, TMeuDB, VAR_IP, VAR_PORTA, 'root',
  VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)True);
  // FIM 2019-09-27




  MyLocDataBase.DataBaseName := TLocDB;
  MyLocDataBase.Port := VAR_PORTA;
  //FmDermaAD_MLA.DBLocal.DatabaseName := VAR_BDCASHLOC;
  ReopenMaster();
  VAR_EMPRESANOME := QrMasterEm.Value;
  //if VAR_SERVIDOR = 2 then GOTOy.LiberaUso;
  //CriaQv_GOTO;
  MyList.ConfiguracoesIniciais(1, MyDB.DatabaseName);
  try
    Application.CreateForm(TDmodG, DmodG);
    //
    DModG.MyPID_DB_Cria();
  except
    Application.MessageBox(PChar('Imposs�vel criar Modulo de dados Geral'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
end;

procedure TDMod.VerificaSenha(Index: Integer; Login, Senha: String);
begin
  //Compatibilidfade
  //
  //Trava senha
  VAR_SENHARESULT := -1;
  //
  if (Senha = VAR_BOSS) or (Senha = CO_MASTER) then VAR_SENHARESULT := 2
  else
  begin
    //Desativado no EmporiumAD - n�o usa aqui, s� no Emporium
(*    if Index = 7 then
    begin
      QrSenha.Close;
      QrSenha.Params[0].AsString := Login;
      QrSenha.Params[1].AsString := Senha;
      QrSenha.Open;
      if QrSenha.RecordCount > 0 then
      begin
        QrUNPerfis.Close;
        QrUNPerfis.Params[0].AsInteger := QrSenhaPerfilW.Value;
        QrUNPerfis.Open;
        if QrUNPerfisVendas.Value = 'V' then VAR_SENHARESULT := 2
        else MessageBox(0,'Acesso negado. Senha Inv�lida',
        'Permiss�o por senha', MB_OK+MB_ICONEXCLAMATION);
        QrUNPerfis.Close;
      end else ShowMessage('Login inv�lido.');
      QrSenha.Close;
    end else ShowMessage('Em constru��o');*)
  end;
end;

function TDMod.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
  QrPerfis.Close;
  if Usuario > -1000 then
  begin
    QrPerfis.SQL.Clear;
    QrPerfis.SQL.Add('SELECT pip.Libera, pit.Janela');
    QrPerfis.SQL.Add('FROM perfisits pit');
    QrPerfis.SQL.Add('LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela');
    QrPerfis.SQL.Add('AND pip.Codigo='+IntToStr(Usuario)); // Condi��o do LEFT JOIN
  end;
  QrPerfis.Open;
  if QrPerfis.RecordCount > 0 then Result := True;
end;

procedure TDmod.QrDonoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrDonoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoTe1.Value);
  QrDonoTE2_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoTe2.Value);
  QrDonoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoFax.Value);
  QrDonoCEL_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoECel.Value);
  QrDonoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrDonoCNPJ_CPF.Value);
  //
  QrDonoNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrDonoRua.Value, Trunc(QrDonoNumero.Value), False);
  QrDonoE_LNR.Value := QrDonoNOMELOGRAD.Value;
  if Trim(QrDonoE_LNR.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ';
  QrDonoE_LNR.Value := QrDonoE_LNR.Value + QrDonoRua.Value;
  if Trim(QrDonoRua.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ', ' + QrDonoNUMERO_TXT.Value;
  if Trim(QrDonoCompl.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ' + QrDonoCompl.Value;
  if Trim(QrDonoBairro.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' - ' + QrDonoBairro.Value;
  //
  QrDonoE_LN2.Value := '';
  if Trim(QrDonoCidade.Value) <>  '' then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + ' - '+QrDonoCIDADE.Value;
  QrDonoE_LN2.Value := QrDonoE_LN2.Value + ' - '+QrDonoNOMEUF.Value;
  if QrDonoCEP.Value > 0 then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + '  CEP ' +Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  QrDonoE_CUC.Value := QrDonoE_LNR.Value + QrDonoE_LN2.Value;
  //
  QrDonoECEP_TXT.Value :=Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  if QrDonoTipo.Value = 0 then Natal := QrDonoENatal.Value
  else Natal := QrDonoPNatal.Value;
  if Natal < 2 then QrDonoNATAL_TXT.Value := ''
  else QrDonoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
  //
  if Trim(QrDonoTE1.Value) <> '' then
    QrDonoFONES.Value := QrDonoTE1_TXT.Value;
  if Trim(QrDonoTe2.Value) <> '' then
    QrDonoFONES.Value := QrDonoFONES.Value + ' - ' + QrDonoTE2_TXT.Value;
end;

procedure TDmod.QrEnderecoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrEnderecoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoTe1.Value);
  QrEnderecoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoFax.Value);
  QrEnderecoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEnderecoCNPJ_CPF.Value);
  //
  QrEnderecoNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrDonoRua.Value, QrEnderecoNumero.Value, False);
  QrEnderecoE_ALL.Value := QrEnderecoNOMELOGRAD.Value;
  if Trim(QrEnderecoE_ALL.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ';
  QrEnderecoE_ALL.Value := QrEnderecoE_ALL.Value + QrEnderecoRua.Value;
  if Trim(QrEnderecoRua.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ', ' + QrEnderecoNUMERO_TXT.Value;
  if Trim(QrEnderecoCompl.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ' + QrEnderecoCompl.Value;
  if Trim(QrEnderecoBairro.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoBairro.Value;
  if QrEnderecoCEP.Value > 0 then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  if Trim(QrEnderecoCidade.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoCidade.Value;
  //
  QrEnderecoECEP_TXT.Value :=Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  //
  if QrEnderecoTipo.Value = 0 then
  begin
    Natal := QrEnderecoENatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CNPJ';
  end else begin
    Natal := QrEnderecoPNatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CPF';
  end;
  if Natal < 2 then QrEnderecoNATAL_TXT.Value := ''
  else QrEnderecoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
end;

procedure TDmod.RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
//var
  //Saldo: Double;
begin
  {
  if Tipo < 2 then
  begin
    QrSomaM.Close;
    QrSomaM.SQL.Clear;
    QrSomaM.SQL.Add('SELECT Inicial Valor FROM carteiras');
    QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Codigo=:P1');
    QrSomaM.Params[0].AsInteger := Tipo;
    QrSomaM.Params[1].AsInteger := Carteira;
    QrSomaM.Open;
    Saldo := QrSomaMValor.Value;
  end else Saldo := 0;
  QrSomaM.Close;
  QrSomaM.SQL.Clear;
  if Tipo = 2 then begin
    QrSomaM.SQL.Add('SELECT SUM(IF(Sit=0, (Credito-Debito),');
    QrSomaM.SQL.Add('IF(Sit=1, (Credito-Debito+Pago), 0))) Valor FROM lanctos');
  end else
    QrSomaM.SQL.Add('SELECT (SUM(Credito) - SUM(Debito)) Valor FROM lanctos');
  QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Carteira=:P1');
  QrSomaM.Params[0].AsInteger := Tipo;
  QrSomaM.Params[1].AsInteger := Carteira;
  QrSomaM.Open;
  Saldo := Saldo + QrSomaMValor.Value;
  QrSomaM.Close;

  QrUpdM.Close;
  QrUpdM.SQL.Clear;
  QrUpdM.SQL.Add('UPDATE carteiras SET Saldo=:P0');
  QrUpdM.SQL.Add('WHERE Tipo=:P1 AND Codigo=:P2');
  QrUpdM.Params[0].AsFloat := Saldo;
  QrUpdM.Params[1].AsInteger := Tipo;
  QrUpdM.Params[2].AsInteger := Carteira;
  QrUpdM.ExecSQL;
  }
end;

procedure TDmod.ReopenControle;
begin
  // Compatibilidade
end;

procedure TDmod.ReopenMaster;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMaster, Dmod.MyDB, [
  'SELECT ma.Em, te.Tipo, ',
  '/* ',
  'te.Logo, te.Logo2, ',
  'te.IE, te.ECidade, uf.Nome NOMEUF, te.EFax, ',
  'te.ERua, (te.ENumero+0.000) ENumero, te.EBairro, te.ECEP, te.ECompl, ',
  'te.EContato, te.ECel, te.ETe1, te.ETe2, te.ETe3, te.EPais, ',
  'te.Respons1, te.Respons2,*/ ',
  'ma.Limite, ma.SolicitaSenha, ',
  'ma.UsaAccMngr, cm.Dono, cm.Versao, cm.CNPJ ',
  'FROM Entidades te, Controle cm, Master ma ',
  '/*/Ufs uf*/ ',
  'WHERE te.Codigo=cm.Dono ',
  'AND ((te.CNPJ=cm.CNPJ AND te.Tipo=0) OR (te.CPF=cm.CNPJ AND te.Tipo=1)) ',
  '/*AND uf.Codigo=te.EUF*/ ',
  '']);
end;

procedure TDmod.ReopenParamsEspecificos(Empresa: Integer);
begin
  //Compatibilidade
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
  Result := '';
end;

/////////////////////////////////////////////////////////////////////////////////////

end.

