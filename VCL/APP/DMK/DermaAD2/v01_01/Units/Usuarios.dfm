object FmUsuarios: TFmUsuarios
  Left = 268
  Top = 229
  BorderStyle = bsSizeToolWin
  Caption = 'AD2-USUAR-001 :: Cadastro de Usu'#225'rios'
  ClientHeight = 352
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 196
    Align = alClient
    BevelOuter = bvNone
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object Label2: TLabel
      Left = 16
      Top = 60
      Width = 32
      Height = 13
      Caption = 'Login :'
    end
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label4: TLabel
      Left = 16
      Top = 104
      Width = 37
      Height = 13
      Caption = 'Senha :'
    end
    object Label3: TLabel
      Left = 260
      Top = 60
      Width = 78
      Height = 13
      Caption = 'Perfil do usu'#225'rio:'
    end
    object Label6: TLabel
      Left = 260
      Top = 104
      Width = 58
      Height = 13
      Caption = 'Funcion'#225'rio:'
      FocusControl = DBEdit5
    end
    object Label5: TLabel
      Left = 16
      Top = 144
      Width = 49
      Height = 13
      Caption = 'IP padr'#227'o:'
      FocusControl = DBEdit6
    end
    object DBEdit1: TDBEdit
      Left = 16
      Top = 32
      Width = 37
      Height = 21
      Hint = 'N'#250'mero sequencial de cadastro de senha'
      TabStop = False
      DataField = 'Numero'
      DataSource = DsSenhas
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
    end
    object DBEdit3: TDBEdit
      Left = 16
      Top = 120
      Width = 200
      Height = 21
      Hint = 'Senha do usu'#225'rio'
      DataField = 'SenhaNew'
      DataSource = DsSenhas
      PasswordChar = '*'
      TabOrder = 2
    end
    object DBEdit2: TDBEdit
      Left = 16
      Top = 76
      Width = 200
      Height = 21
      Hint = 'Identifica'#231#227'o do usu'#225'rio'
      DataField = 'Login'
      DataSource = DsSenhas
      TabOrder = 1
    end
    object DBEdit4: TDBEdit
      Left = 260
      Top = 76
      Width = 300
      Height = 21
      Hint = 'Identifica'#231#227'o do usu'#225'rio'
      DataField = 'NOMEPERFIL'
      DataSource = DsSenhas
      TabOrder = 3
    end
    object DBEdit5: TDBEdit
      Left = 260
      Top = 120
      Width = 300
      Height = 21
      DataField = 'NOMEFUNCIONARIO'
      DataSource = DsSenhas
      TabOrder = 4
    end
    object DBCheckBox1: TDBCheckBox
      Left = 60
      Top = 34
      Width = 97
      Height = 17
      Caption = 'Ativo.'
      DataField = 'Ativo'
      DataSource = DsSenhas
      TabOrder = 5
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBEdit6: TDBEdit
      Left = 16
      Top = 160
      Width = 545
      Height = 21
      DataField = 'IP_Default'
      DataSource = DsSenhas
      TabOrder = 6
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 264
        Height = 32
        Caption = 'Cadastro de Usu'#225'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 264
        Height = 32
        Caption = 'Cadastro de Usu'#225'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 264
        Height = 32
        Caption = 'Cadastro de Usu'#225'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBCntrl: TGroupBox
    Left = 0
    Top = 288
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 172
      Height = 47
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
    end
    object LaRegistro: TStaticText
      Left = 174
      Top = 15
      Width = 123
      Height = 47
      Align = alClient
      BevelInner = bvLowered
      BevelKind = bkFlat
      Caption = '[N]: 0'
      TabOrder = 2
    end
    object Panel3: TPanel
      Left = 297
      Top = 15
      Width = 485
      Height = 47
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object BtAltera: TBitBtn
        Tag = 11
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object Panel2: TPanel
        Left = 376
        Top = 0
        Width = 109
        Height = 47
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 2
        object BtSaida: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 188
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtExcluiClick
      end
      object BtFilial: TBitBtn
        Tag = 10049
        Left = 280
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Filial'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtFilialClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 244
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrSenhas: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSenhasAfterOpen
    AfterScroll = QrSenhasAfterScroll
    SQL.Strings = (
      'SELECT AES_DECRYPT(se.Senha, :P0) SenhaNew, se.*, '
      'pe.Descricao NOMEPERFIL,'
      'en.Nome NOMEFUNCIONARIO '
      'FROM senhas se'
      'LEFT JOIN perfis    pe ON pe.Codigo=se.Perfil'
      'LEFT JOIN entidades en ON en.Codigo=se.Funcionario'
      'WHERE se.Numero=:P1')
    Left = 632
    Top = 57
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhasLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'DNRECOVERY.senhas.Login'
      Size = 128
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'DNRECOVERY.senhas.Numero'
    end
    object QrSenhasSenha: TWideStringField
      FieldName = 'Senha'
      Origin = 'DNRECOVERY.senhas.Senha'
      Size = 128
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'DNRECOVERY.senhas.Perfil'
    end
    object QrSenhasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DNRECOVERY.senhas.Lk'
    end
    object QrSenhasNOMEPERFIL: TWideStringField
      FieldName = 'NOMEPERFIL'
      Origin = 'DNRECOVERY.perfis.Descricao'
      Size = 128
    end
    object QrSenhasNOMEFUNCIONARIO: TWideStringField
      FieldName = 'NOMEFUNCIONARIO'
      Size = 100
    end
    object QrSenhasFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
    object QrSenhasSenhaNew: TWideStringField
      FieldName = 'SenhaNew'
      Size = 30
    end
    object QrSenhasAtivo: TIntegerField
      FieldName = 'Ativo'
    end
    object QrSenhasIP_Default: TWideStringField
      FieldName = 'IP_Default'
      Size = 50
    end
  end
  object DsSenhas: TDataSource
    DataSet = QrSenhas
    Left = 660
    Top = 57
  end
end
