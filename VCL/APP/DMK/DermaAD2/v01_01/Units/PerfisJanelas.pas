unit PerfisJanelas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkPermissoes, dmkEdit,
  dmkDBGridDAC, mySQLDbTables, DmkDAC_PF, dmkDBGrid;

type
  TFmPerfisJanelas = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtUsuarios: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    Label3: TLabel;
    EdJanela: TdmkEdit;
    BtRefresh: TBitBtn;
    QrPerfIts: TmySQLQuery;
    QrPerfItsLibera: TSmallintField;
    QrPerfItsOlha: TSmallintField;
    QrPerfItsInclui: TSmallintField;
    QrPerfItsAltera: TSmallintField;
    QrPerfItsExclui: TSmallintField;
    QrPerfItsJanela: TWideStringField;
    QrPerfItsDescricao: TWideStringField;
    QrPerfItsCodigo: TIntegerField;
    QrPerfItsNome: TWideStringField;
    QrPerfItsModulo: TWideStringField;
    DsPerfIts: TDataSource;
    Splitter1: TSplitter;
    QrPerfItsPerfilTXT: TWideStringField;
    DBGrade: TdmkDBGridDAC;
    DsUsuarios: TDataSource;
    QrUsuarios: TmySQLQuery;
    QrUsuariosNumero: TIntegerField;
    QrUsuariosLogin: TWideStringField;
    QrUsuariosNOMEFUNCIONARIO: TWideStringField;
    Panel6: TPanel;
    StaticText1: TStaticText;
    dmkDBGrid2: TdmkDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure QrPerfItsAfterScroll(DataSet: TDataSet);
    procedure QrPerfItsBeforeClose(DataSet: TDataSet);
    procedure DBGradeCellClick(Column: TColumn);
    procedure DBGradeDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtUsuariosClick(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaJanelaPerfil(Janela: String);
    procedure ReopenPerfisIts(Janela: String; Perfil: Integer);
    procedure ReopenUsuarios(Perfil: Integer);
  public
    { Public declarations }
    FJanela: String;
  end;

  var
  FmPerfisJanelas: TFmPerfisJanelas;

implementation

uses UnMyObjects, Module, Principal;

{$R *.DFM}

procedure TFmPerfisJanelas.AtualizaJanelaPerfil(Janela: String);
var
  Perfil: Integer;
  Qry, Qry2: TmySQLQuery;
begin
  Qry  := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  Qry2 := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM perfis ',
      '']);
    while not Qry.EOF do
    begin
      Perfil := Qry.FieldByName('Codigo').AsInteger;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
        'SELECT * ',
        'FROM perfits ',
        'WHERE Janela="' + Janela + '" ',
        'AND Codigo=' + Geral.FF0(Perfil),
        '']);
      if Qry2.RecordCount = 0 then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Qry2, Dmod.MyDB, [
          'INSERT INTO perfits SET ',
          'Codigo=' + Geral.FF0(Perfil) + ', ',
          'Janela="' + Janela + '"',
          '']);
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
    Qry2.Free;
  end;
end;

procedure TFmPerfisJanelas.BtRefreshClick(Sender: TObject);
var
  Janela: String;
begin
  Janela := EdJanela.ValueVariant;
  //
  if MyObjects.FIC(Janela = '', EdJanela, 'Defina a janela!') then Exit;
  //
  ReopenPerfisIts(Janela, 0);
end;

procedure TFmPerfisJanelas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPerfisJanelas.BtUsuariosClick(Sender: TObject);
var
  Usuario: Integer;
begin
  if (QrUsuarios.State <> dsInactive) and (QrUsuarios.RecordCount > 0) then
    Usuario := QrUsuariosNumero.Value
  else
    Usuario := 0;
  //
  FmPrincipal.MostraUsuarios(Usuario);
end;

procedure TFmPerfisJanelas.DBGradeCellClick(Column: TColumn);
var
  Reabre: Boolean;
  Codigo: Integer;
begin
  if (QrPerfIts.State <> dsInactive) and (QrPerfIts.RecordCount > 0) then
  begin
    if (QrPerfItsModulo.Value <> '') and ((Pos(LowerCase(QrPerfItsModulo.Value),
      LowerCase(FmPrincipal.QrMasterHabilModulos.Value)) = 0) and
      (LowerCase(TMeuDB) <> 'dcontrol')) then
    begin
      Geral.MB_Aviso('M�dulo n�o habilitado! Solicite a libera��o junto a Dermatek!')
    end else
    begin
      Codigo := QrPerfItsCodigo.Value;
      Reabre := False;
      //
      if Column.FieldName = 'Libera' then
      begin
        FmPrincipal.GradeClica(libjanAll, stUpd, QrPerfIts, Codigo);
        Reabre := True;
      end else
      if Column.FieldName = 'Olha' then
      begin
        FmPrincipal.GradeClica(libjanSee, stUpd, QrPerfIts, Codigo);
        Reabre := True;
      end else
      if Column.FieldName = 'Inclui' then
      begin
        FmPrincipal.GradeClica(libjanIns, stUpd, QrPerfIts, Codigo);
        Reabre := True;
      end else
      if Column.FieldName = 'Altera' then
      begin
        FmPrincipal.GradeClica(libjanUpd, stUpd, QrPerfIts, Codigo);
        Reabre := True;
      end else
      if Column.FieldName = 'Exclui' then
      begin
        FmPrincipal.GradeClica(libjanDel, stUpd, QrPerfIts, Codigo);
        Reabre := True;
      end;
      //
      if Reabre then
        ReopenPerfisIts(QrPerfItsJanela.Value, QrPerfItsCodigo.Value);
    end;
  end;
end;

procedure TFmPerfisJanelas.DBGradeDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if (QrPerfIts.State <> dsInactive) and (QrPerfIts.RecordCount > 0) then
  begin
    if (QrPerfItsModulo.Value <> '') and ((Pos(LowerCase(QrPerfItsModulo.Value),
      LowerCase(FmPrincipal.QrMasterHabilModulos.Value)) = 0) and (LowerCase(TMeuDB) <> 'dcontrol'))
    then
      Cor := clSilver
    else
      Cor := clBlack;
    //
    with DBGrade.Canvas do
    begin
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmPerfisJanelas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FJanela <> '' then
  begin
    EdJanela.ValueVariant := FJanela;
    //
    ReopenPerfisIts(FJanela, 0);
  end;
end;

procedure TFmPerfisJanelas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPerfisJanelas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPerfisJanelas.QrPerfItsAfterScroll(DataSet: TDataSet);
begin
  ReopenUsuarios(QrPerfItsCodigo.Value);
end;

procedure TFmPerfisJanelas.QrPerfItsBeforeClose(DataSet: TDataSet);
begin
  QrUsuarios.Close;
end;

procedure TFmPerfisJanelas.ReopenPerfisIts(Janela: String; Perfil: Integer);
begin
  AtualizaJanelaPerfil(Janela);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPerfIts, Dmod.MyDB, [
    'SELECT per.Descricao PerfilTXT, pip.Libera, pip.Olha, ',
    'pip.Inclui, pip.Altera, pip.Exclui, pip.Codigo, ',
    'pit.Janela, pit.Nome, pit.Descricao, pit.Modulo ',
    'FROM perfjan pit ',
    'LEFT JOIN perfits pip ON pip.Janela=pit.Janela ',
    'LEFT JOIN perfis per ON per.Codigo = pip.Codigo ',
    'WHERE pit.Janela LIKE "%' + Janela + '%"',
    'AND pip.Codigo <> 0 ',
    'ORDER BY per.Descricao, pit.Janela ',
    '']);
  if Perfil <> 0 then
    QrPerfIts.Locate('Codigo', Perfil, []);
end;

procedure TFmPerfisJanelas.ReopenUsuarios(Perfil: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUsuarios, Dmod.MyDB, [
    'SELECT se.Numero, se.Login, en.Nome NOMEFUNCIONARIO ',
    'FROM senhas se ',
    'LEFT JOIN entidades en ON en.Codigo=se.Funcionario ',
    'WHERE se.Perfil=' + Geral.FF0(Perfil),
    'ORDER BY se.Login ',
    '']);
end;

end.
