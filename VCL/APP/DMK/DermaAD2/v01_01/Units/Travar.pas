unit Travar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DBCtrls, StdCtrls, Buttons, Db, (*DBTables,*) 
      
  UnMLAGeral,   
  mySQLDbTables, ComCtrls;

type
  TFmTravar = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    RichEdit1: TRichEdit;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  FmTravar: TFmTravar;

implementation

uses UnMyObjects, Module, UnInternalConsts, Principal;

{$R *.DFM}

procedure TFmTravar.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTravar.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTravar.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmTravar.BtConfirmaClick(Sender: TObject);
begin
  if Application.MessageBox('Deseja realmente travar a licen�a?',
  'Travamento de licen�a',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1
  +MB_APPLMODAL) = ID_YES Then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE Master SET Monitorar=0, Limite=0');
    Dmod.QrUpd.ExecSQL;
    FmPrincipal.Close;
  end;
end;

end.
