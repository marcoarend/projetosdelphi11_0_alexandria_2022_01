unit Empresa;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, ComCtrls, mySQLDbTables, dmkGeral, Grids,
  DBGrids, dmkDBGrid, Menus, dmkEdit, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmEmpresa = class(TForm)
    PainelDados: TPanel;
    QrEmpresa: TmySQLQuery;
    DsEmpresa: TDataSource;
    QrEmpresaCNPJ_TXT: TWideStringField;
    TbTerminais: TmySQLTable;
    DsTerminais: TDataSource;
    QrEmpresaMonitorar: TSmallintField;
    QrEmpresaMasLogin: TWideStringField;
    QrEmpresaMasSenha: TWideStringField;
    QrEmpresaMasAtivar: TWideStringField;
    QrEmpresaSolicitaSenha: TSmallintField;
    QrEmpresaEm: TWideStringField;
    QrEmpresaCNPJ: TWideStringField;
    QrEmpresaLicenca: TWideStringField;
    QrEmpresaDistorcao: TIntegerField;
    QrEmpresaDataI: TDateField;
    QrEmpresaDataF: TDateField;
    QrEmpresaHoje: TDateField;
    QrEmpresaHora: TTimeField;
    QrEmpresaLimite: TSmallintField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PainelDados1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DBRazao: TDBEdit;
    DBHD: TDBEdit;
    DBLogin: TDBEdit;
    DBDataI: TDBEdit;
    DBDataF: TDBEdit;
    DBSenha: TDBEdit;
    DBEdit8: TDBEdit;
    DBCNPJ: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit22: TDBEdit;
    DBAtiva: TDBCheckBox;
    DBRadioGroup1: TDBRadioGroup;
    PainelDados2: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    Label21: TLabel;
    EdRazao: TdmkEdit;
    EdHD: TdmkEdit;
    EdCNPJ: TdmkEdit;
    CkAtiva: TCheckBox;
    EdLogin: TdmkEdit;
    TPDataI: TDateTimePicker;
    TPDataF: TDateTimePicker;
    RGMonitorar: TRadioGroup;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelControle: TPanel;
    BtAltera: TBitBtn;
    BtSaida: TBitBtn;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    BtConfirma2: TBitBtn;
    BtDesiste2: TBitBtn;
    Panel2: TPanel;
    BtFilial: TBitBtn;
    BtSaida2: TBitBtn;
    QrFiliais: TmySQLQuery;
    DsFiliais: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    QrFiliaisCodigo: TIntegerField;
    QrFiliaisFilial: TIntegerField;
    QrFiliaisNOMEENT: TWideStringField;
    QrFiliaisCNPJCPF: TWideStringField;
    QrFiliaisIERG: TWideStringField;
    QrFiliaisFormaSociet: TWideStringField;
    QrFiliaisSimples: TSmallintField;
    QrFiliaisAtividade: TWideStringField;
    BtUsuario: TBitBtn;
    PMFilial: TPopupMenu;
    Incluinovafilial1: TMenuItem;
    Alterafilialatual1: TMenuItem;
    Excluifilial1: TMenuItem;
    PMUsuario: TPopupMenu;
    dmkDBGrid2: TdmkDBGrid;
    Splitter1: TSplitter;
    Adicionausurio1: TMenuItem;
    Retirausurio1: TMenuItem;
    QrUsuarios: TmySQLQuery;
    QrUsuariosNumero: TIntegerField;
    QrUsuariosControle: TIntegerField;
    QrUsuariosEmpresa: TIntegerField;
    QrUsuariosLogin: TWideStringField;
    QrUsuariosNOMEFUNCIONARIO: TWideStringField;
    DsUsuarios: TDataSource;
    QrFiliaisCNPJCPF_TXT: TWideStringField;
    EdDono: TdmkEdit;
    DBEdit1: TDBEdit;
    DsControle: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEmpresaMasSenhaNew: TWideStringField;
    EdSenha: TEdit;
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure EdCNPJExit(Sender: TObject);
    procedure QrEmpresaCalcFields(DataSet: TDataSet);
    procedure EdHDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormResize(Sender: TObject);
    procedure Incluinovafilial1Click(Sender: TObject);
    procedure Alterafilialatual1Click(Sender: TObject);
    procedure BtUsuarioClick(Sender: TObject);
    procedure Adicionausurio1Click(Sender: TObject);
    procedure Retirausurio1Click(Sender: TObject);
    procedure QrFiliaisBeforeClose(DataSet: TDataSet);
    procedure QrFiliaisAfterScroll(DataSet: TDataSet);
    procedure QrFiliaisCalcFields(DataSet: TDataSet);
    procedure BtFilialClick(Sender: TObject);
  private
    procedure CriaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure ReopenFiliais(Codigo: Integer);
    procedure ReopenUsuarios(Controle: Integer);
  public
    { Public declarations }
  end;

var
  FmEmpresa: TFmEmpresa;

implementation
  uses UnMyObjects, Module, SelCod, MyDBCheck;

{$R *.DFM}

procedure TFmEmpresa.DefParams;
begin
  VAR_GOTOTABELA := 'Master';
  VAR_GOTOmySQLTABLE := QrEmpresa;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM empresa');
  //
//  VAR_SQL1.Add('AND Codigo=:P0');
  //
//  VAR_SQLa.Add('AND Nome Like :P0');
  //
  QrEmpresa.Close;
  QrEmpresa.Params[0].AsString := CO_USERSPNOW;
  QrEmpresa.Open;
  //
  Dmod.QrControle.Close;
  Dmod.QrControle.Open;
end;

procedure TFmEmpresa.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
begin
  if Mostra then
  begin
    PainelControle.Visible  := False;
    PainelConfirma.Visible  := True;
    EdDono.ValueVariant     := Dmod.QrControleDono.Value;
    if QrEmpresa.RecordCount > 0 then
    begin
      EdRazao.Text          := QrEmpresaEm.Value;
      EdCNPJ.Text           :=  Geral.FormataCNPJ_TT(QrEmpresaCNPJ.Value);
      EdHD.Text             := QrEmpresaLicenca.Value;
      TPDataI.Date          := QrEmpresaDataI.Value;
      TPDataF.Date          := QrEmpresaDataF.Value;
      EdLogin.Text          := QrEmpresaMasLogin.Value;
      EdSenha.Text          := QrEmpresaMasSenhaNew.Value;
      RGMonitorar.ItemIndex := QrEmpresaMonitorar.Value;
      CkAtiva.Checked       := QrEmpresaMasAtivar.Value = 'V';
    end else begin
      EdRazao.Text          := '';
      EdCNPJ.Text           := '';
      EdHD.Text             := '';
      TPDataI.Date          := Date;
      TPDataF.Date          := Date;
      EdLogin.Text          := '';
      EdSenha.Text          := '';
      RGMonitorar.ItemIndex := 1;
      CkAtiva.Checked       := True;
    end;
    PainelDados2.Visible    := True;
    PainelDados1.Visible    := False;
    TabSheet2.TabVisible    := False;
    TabSheet3.TabVisible    := False;
    EdRazao.SetFocus;
  end else begin
    PainelControle.Visible  := True;
    PainelConfirma.Visible  := False;
    PainelDados1.Visible    := True;
    PainelDados2.Visible    := False;
    TabSheet2.TabVisible    := True;
    TabSheet3.TabVisible    := True;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEmpresa.CriaOForm;
begin
  DefParams;
end;


///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEmpresa.Adicionausurio1Click(Sender: TObject);
const
  Aviso   = '...';
  Titulo  = 'Adiciona Usu�rio';
  Prompt  = 'Informe o usu�rio:';
  Campo   = 'Descricao';
var
  Codigo, Controle: Integer;
begin
  Codigo := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Numero Codigo, Login ' + Campo,
  'FROM senhas ',
  'WHERE Numero > 0 ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, False);
  if Codigo <> 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Numero FROM senhasits ');
    Dmod.QrAux.SQL.Add('WHERE Numero=:P0 ');
    Dmod.QrAux.SQL.Add('AND Empresa=:P1');
    Dmod.QrAux.Params[00].AsInteger := VAR_SELCOD;
    Dmod.QrAux.Params[01].AsInteger := QrFiliaisCodigo.Value;
    Dmod.QrAux.Open;
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Application.MessageBox(PChar('O usu�rio ' + FormatFloat('0', VAR_SELCOD) +
      ' j� foi adicionado anteriormente!'), 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    Controle := UMyMod.BuscaEmLivreY_Def('senhasits', 'Controle', stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'senhasits', False, [
    'Numero', 'Entidade', 'Empresa'], ['Controle'], [
    VAR_SELCOD, 0, QrFiliaisCodigo.Value], [Controle], False) then
      ReopenUsuarios(VAR_SELCOD);
  end;
end;

procedure TFmEmpresa.Alterafilialatual1Click(Sender: TObject);
begin
(*
  Screen.Cursor := crHourGlass;
  try
    Application.CreateForm(TFmEntidades, FmEntidades);
    FmEntidades.LocCod(QrFiliaisCodigo.Value, QrFiliaisCodigo.Value);
    if QrFiliaisCodigo.Value = FmEntidades.QrEntidadesCodigo.Value then
      FmEntidades.BtAltera.Enabled := True;
    FmEntidades.FPodeNeg := True;
  finally
    Screen.Cursor := crDefault;
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
    ReopenFiliais(QrFiliaisCodigo.Value);
  end;
*)
end;

procedure TFmEmpresa.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(True, stUpd, 0);
end;

procedure TFmEmpresa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmpresa.BtUsuarioClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMUsuario, BtUsuario);
end;

procedure TFmEmpresa.BtConfirmaClick(Sender: TObject);
var
  CNPJ, Ativar: String;
begin
  CNPJ := Geral.SoNumero_TT(EdCNPJ.Text);
  if CkAtiva.Checked then Ativar := 'V' else Ativar := 'F';
  Dmod.QrUpdU.SQL.Clear;
  if QrEmpresa.RecordCount = 0 then
    Dmod.QrUpdU.SQL.Add('INSERT INTO master SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE master SET ');
  Dmod.QrUpdU.SQL.Add('Em=:P0, CNPJ=:P1, Monitorar=:P2, Licenca=:P3, DataI=:P4, ');
  Dmod.QrUpdU.SQL.Add('DataF=:P5, MasLogin=:P6, MasSenha=AES_ENCRYPT(:P7, :P8), ');
  Dmod.QrUpdU.SQL.Add('MasAtivar=:P9');
  Dmod.QrUpdU.Params[00].AsString  := EdRazao.Text;
  Dmod.QrUpdU.Params[01].AsString  := CNPJ;
  Dmod.QrUpdU.Params[02].AsInteger := RGMonitorar.ItemIndex;
  Dmod.QrUpdU.Params[03].AsFloat   := Geral.DMV(EdHD.Text);
  Dmod.QrUpdU.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataI.Date);
  Dmod.QrUpdU.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataF.Date);
  Dmod.QrUpdU.Params[06].AsString  := EdLogin.Text;
  Dmod.QrUpdU.Params[07].AsString  := EdSenha.Text;
  Dmod.QrUpdU.Params[08].AsString  := CO_USERSPNOW;
  Dmod.QrUpdU.Params[09].AsString  := Ativar;
  Dmod.QrUpdU.ExecSQL;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE controle SET Dono=:P0, CNPJ=:P1');
  Dmod.QrUpdU.Params[00].AsInteger := EdDono.ValueVariant;
  Dmod.QrUpdU.Params[01].AsString  := CNPJ;
  Dmod.QrUpdU.ExecSQL;
  //
  QrEmpresa.Close;
  QrEmpresa.Open;
  //
  Dmod.QrControle.Close;
  Dmod.QrControle.Open;
  //
  MostraEdicao(False, stLok, 0);
end;

procedure TFmEmpresa.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(False, stLok, 0);
end;

procedure TFmEmpresa.BtFilialClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFilial, BtFilial);
end;

procedure TFmEmpresa.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  TbTerminais.Open;
  ReopenFiliais(0);
end;

procedure TFmEmpresa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEmpresa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmpresa.EdCNPJExit(Sender: TObject);
var
  Num : String;
  CNPJ : String;
begin
  CNPJ := Geral.SoNumero_TT(EdCNPJ.Text);
  if CNPJ <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CNPJ);
    if MLAGeral.FormataCNPJ_TFT(CNPJ) <> Num then
    begin
      ShowMessage(SMLA_NUMEROINVALIDO2);
      EdCNPJ.SetFocus;
    end else EdCNPJ.Text := Geral.FormataCNPJ_TT(CNPJ);
  end else EdCNPJ.Text := CO_VAZIO;
end;

procedure TFmEmpresa.QrEmpresaCalcFields(DataSet: TDataSet);
begin
  QrEmpresaCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEmpresaCNPJ.Value);
end;

procedure TFmEmpresa.QrFiliaisAfterScroll(DataSet: TDataSet);
begin
  ReopenUsuarios(0);
end;

procedure TFmEmpresa.QrFiliaisBeforeClose(DataSet: TDataSet);
begin
  QrUsuarios.Close;
end;

procedure TFmEmpresa.QrFiliaisCalcFields(DataSet: TDataSet);
begin
  QrFiliaisCNPJCPF_TXT.Value := Geral.FormataCNPJ_TT(QrFiliaisCNPJCPF.Value);
end;

procedure TFmEmpresa.EdHDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Chave: Double;
  Serial: Integer;
begin
  if Key=VK_F4 then
  begin
    GOTOy.CriaChavedeAcesso;
    Chave  := Geral.DMV(UMyMod.LeChavedeAcesso);
    Serial := dmkPF.GetDiskSerialNumber('C');
    EdHD.Text := FormatFloat('0', (Serial * Chave) - Serial - Chave);
  end;
end;

procedure TFmEmpresa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], 'Configura��es da Empresa', True, taCenter, 2, 10, 20);
end;

procedure TFmEmpresa.Incluinovafilial1Click(Sender: TObject);
var
  Codigo: Integer;
  Filial: String;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT MIN(Codigo) Codigo FROM entidades');
  Dmod.QrAux.SQL.Add('WHERE Codigo < -10');
  Dmod.QrAux.Open;
  //
  Codigo := Dmod.QrAux.FieldByName('Codigo').AsInteger;
  if Codigo > -10 then Codigo := -10;
  Filial := FormatFloat('0', Codigo - 1);
  //
  if InputQuery('Inclus�o de Nova Filial', 'Informe o c�digo da filial:',
  Filial) then
  begin
    Codigo := Geral.IMV(Filial);
    if Codigo = 0 then
    begin
      Application.MessageBox('O c�digo n�o pode ser zero!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      Exit;
    end;
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Codigo FROM entidades');
    Dmod.QrAux.SQL.Add('WHERE Codigo=' + Filial);
    Dmod.QrAux.Open;
    //
    if Codigo > -11 then
    begin
      Application.MessageBox(PChar('O c�digo deve ser menor que -10'), 'Aviso',
      MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Application.MessageBox(PChar('O c�digo ' + Filial + ' j� existe!'), 'Aviso',
      MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if Application.MessageBox(PChar('Confirma a inclus�o da filial ' + Filial +
    '?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      // UMyMod.SQLInsUpd(Dmod.QrUpd, 'entidades', False,)
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns,  'entidades', False, [
      'Tipo', 'CodUsu'],['Codigo'], [0, Codigo], [Codigo], True) then
        ReopenFiliais(Codigo);
    end;
  end;
end;

procedure TFmEmpresa.ReopenFiliais(Codigo: Integer);
begin
  QrFiliais.Close;
  QrFiliais.Open;
  QrFiliais.Locate('Codigo', Codigo, []);
end;

procedure TFmEmpresa.ReopenUsuarios(Controle: Integer);
begin
  QrUsuarios.Close;
  QrUsuarios.Params[0].AsInteger := QrFiliaisCodigo.value;
  QrUsuarios.Open;
  QrUsuarios.Locate('Controle', Controle, []);
end;

procedure TFmEmpresa.Retirausurio1Click(Sender: TObject);
begin
  //
end;

end.

