unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, Menus, Db, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UnMLAGeral, mySQLDbTables, ExtCtrls, ToolWin, Grids,
  dmkGeral, UnDmkEnums, Vcl.Imaging.pngimage, UnDmkProcFunc, dmkPageControl,
  UnGrl_Vars;

type
  TLibJan = (libjanAll, libjanSee, libjanIns, libjanUpd, libjanDel);
  TFmPrincipal = class(TForm)
    ODlg1: TOpenDialog;
    QrAparencias: TmySQLQuery;
    QrAparenciasCodigo: TIntegerField;
    QrAparenciasNome: TWideStringField;
    QrAparenciasTituloPainelItem: TIntegerField;
    QrAparenciasTituloPainelTexto: TIntegerField;
    QrAparenciasDadosPainel: TIntegerField;
    QrAparenciasDadosTexto: TIntegerField;
    QrAparenciasGradeAItem: TIntegerField;
    QrAparenciasGradeATitulo: TIntegerField;
    QrAparenciasGradeATexto: TIntegerField;
    QrAparenciasGradeAGrade: TIntegerField;
    QrAparenciasGradeBItem: TIntegerField;
    QrAparenciasGradeBTitulo: TIntegerField;
    QrAparenciasGradeBTexto: TIntegerField;
    QrAparenciasGradeBGrade: TIntegerField;
    QrAparenciasEditAItem: TIntegerField;
    QrAparenciasEditATexto: TIntegerField;
    QrAparenciasEditBItem: TIntegerField;
    QrAparenciasEditBTexto: TIntegerField;
    QrAparenciasConfirmaPainelItem: TIntegerField;
    QrAparenciasControlePainelItem: TIntegerField;
    QrAparenciasFormPesquisa: TIntegerField;
    QrAparenciasLabelPesquisa: TIntegerField;
    QrAparenciasLabelDigite: TIntegerField;
    QrAparenciasEditPesquisaItem: TIntegerField;
    QrAparenciasEditPesquisaTexto: TIntegerField;
    QrAparenciasLk: TIntegerField;
    QrLControle: TmySQLQuery;
    QrAgora: TmySQLQuery;
    QrAgoraAgora: TWideStringField;
    QrLControleAparencias: TIntegerField;
    Image1: TImage;
    Image2: TImage;
    QrMaster: TmySQLQuery;
    QrMasterMonitorar: TSmallintField;
    Timer1: TTimer;
    StatusBar1: TStatusBar;
    StatusBar2: TStatusBar;
    AdvToolBarPager1: TdmkPageControl;
    AdvPage2: TTabSheet;
    AdvToolBar16: TPanel;
    AGBUsuarios: TBitBtn;
    AdvToolBarPager13: TTabSheet;
    AdvToolBar9: TPanel;
    AGMBVerifiBD: TBitBtn;
    AdvGlowButton6: TBitBtn;
    AGBPerfis: TBitBtn;
    AGBEmpresa: TBitBtn;
    Memo1: TMemo;
    TimerIdle: TTimer;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterMasLogin: TWideStringField;
    QrMasterMasSenha: TWideStringField;
    QrMasterEm: TWideStringField;
    QrMasterCNPJ: TWideStringField;
    QrMasterMasSenhaNew: TWideStringField;
    QrMasterHabilModulos: TWideMemoField;
    AGBBoss: TBitBtn;
    BalloonHint1: TBalloonHint;
    AGBJanelas: TBitBtn;
    AdvPage1: TTabSheet;
    AdvToolBar20: TPanel;
    AdvGlowButton28: TBitBtn;
    AdvToolBar10: TPanel;
    AdvGlowButton19: TBitBtn;
    AGBFiliais: TBitBtn;
    TmVersao: TTimer;
    Panel17: TPanel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    procedure Button2Click(Sender: TObject);
//    procedure Reparar1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure AGBUsuariosClick(Sender: TObject);
    procedure AGBPerfisClick(Sender: TObject);
    procedure AGBEmpresaClick(Sender: TObject);
    procedure AGMBVerifiBDClick(Sender: TObject);
    procedure AdvGlowButton6Click(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure AGBBossClick(Sender: TObject);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure AdvToolBarButton7Click(Sender: TObject);
    procedure AdvToolBarButton8Click(Sender: TObject);
    procedure AGBJanelasClick(Sender: TObject);
    procedure AdvGlowButton28Click(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure AGBFiliaisClick(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
  private
    { Private declarations }
    FALiberar: Boolean;
    procedure ShowHint(Sender: TObject);
    procedure MostraLogoff();
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    procedure AppIdle(Sender: TObject; var Done: Boolean);
    procedure ReopenMaster();
    procedure MostraPerfisJanelas(Janela: String);
    procedure ConfiguraMenu();
  public
    { Public declarations }
    FEntInt, FLinModErr,
    FTipoNovoEnti: Integer;
    procedure MostraSenhasEspecificas;
    procedure TravaAplicativo;
    function RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
             Data: TDateTime; Arquivo: String): Boolean;
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade: TStringGrid);
    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
              Entidade: Integer; Aba: Boolean = False);
    procedure ReCaptionComponentesDeForm(Form: TForm);
    procedure MostraFilialUsrs();
    procedure MostraPerfis(Codigo: Integer);
    procedure AcoesIniciaisDoAplicativo();
    procedure GradeClica(LibJan: TLibJan; Acao: TSQLType;
              QueryPerfIts: TMySQLQuery; Codigo: Integer);
    procedure MostraUsuarios(Numero: Integer);
    procedure InfoSeqIni(Texto: String);
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses Usuarios, Matriz, Senha, MyGlyfs, MyVCLSkin, UnGOTOy, Module, About,
  Travar, Perfis, DermaAD_MLA, UCreate, UMySQLModule, MyDBCheck, UnMyObjects,
  LinkRankSkin, NovaVersao, UnDmkWeb, MyListas, UsuariosNew, SenhaBoss,
  ModuleGeral, FilialUsrs, PerfisJanelas;

{$R *.DFM}



(*procedure TFmPrincipal.ProcessoEmDOS(LinhaComando : PChar);
var
  Startupinfo : TStartupinfo;
  ProcessInformation : TProcessInformation;
  rc : Boolean;
begin
  Fillchar(Startupinfo, SizeOf(Startupinfo), #0);
  with Startupinfo do
    cb := SizeOf(Startupinfo);
  rc := CreateProcess(NIL, LinhaComando, NIL, NIL, FALSE, NORMAL_PRIORITY_CLASS,
                      NIL, NIL, Startupinfo, ProcessInformation);
  if rc then
    WaitForSingleObject(ProcessInformation.hProcess, INFINITE);
end;*)

procedure TFmPrincipal.Button2Click(Sender: TObject);
begin
//  WinExec('command.com /c mysqldump --opt BlueMoney > C:\Dermatek\Backups\BlueMo~1.sql', SW_SHOWNORMAL);
//  WinExec('command.com /c mysqldump --opt BlueSkin > C:\Dermatek\Backups\BlueSkin.sql', SW_SHOWNORMAL);
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  ConfiguraMenu();
  //
  StatusBar1.Panels[03].Text := ' ' + VAR_IP + ' : ' + Geral.FF0(VAR_PORTA);
  StatusBar1.Panels[05].Text := ' ' + TMeuDB;
  StatusBar1.Panels[07].Text := ' ' + Geral.FileVerInfo(Application.ExeName, 3);
  //
  if not FALiberar then
    Timer1.Enabled := True;
end;

procedure TFmPrincipal.ConfiguraMenu();
const
  CO_TotBotoes  = 6;
var
  Esq, Larg: Integer;
begin
  Esq  := AGBBoss.Left;
  Larg := AGBBoss.Width;
  //
  if VAR_SENHA = CO_MASTER then
  begin
    AGBEmpresa.Visible := True;
    AdvToolBar16.Width := ((CO_TotBotoes + 1) * Esq) + (CO_TotBotoes * Larg);
  end else
  begin
    AGBEmpresa.Visible := False;
    AdvToolBar16.Width := ((CO_TotBotoes + 1) * Esq) + ((CO_TotBotoes - 1) * Larg);
  end;
end;

procedure TFmPrincipal.MostraFilialUsrs;
begin
  if DBCheck.CriaFm(TFmFilialUsrs, FmFilialUsrs, afmoSoBoss) then
  begin
    FmFilialUsrs.ShowModal;
    FmFilialUsrs.Destroy;
  end;
end;

procedure TFmPrincipal.MostraLogoff;
begin
  FmPrincipal.Enabled := False;
  //
  FmDermaAD_MLA.Show;
  FmDermaAD_MLA.EdLogin.Text   := '';
  FmDermaAD_MLA.EdSenha.Text   := '';
  FmDermaAD_MLA.EdLogin.SetFocus;
end;

procedure TFmPrincipal.MostraPerfis(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPerfis, FmPerfis, afmoSoBoss) then
  begin
    if Codigo <> 0 then
      FmPerfis.LocCod(Codigo, Codigo);
    FmPerfis.ShowModal;
    FmPerfis.Destroy;
    //
    FmPerfis := nil;
  end;
end;

procedure TFmPrincipal.MostraPerfisJanelas(Janela: String);
begin
  if DBCheck.CriaFm(TFmPerfisJanelas, FmPerfisJanelas, afmoSoBoss) then
  begin
    if Janela <> '' then
      FmPerfisJanelas.FJanela := Janela;
    FmPerfisJanelas.ShowModal;
    FmPerfisJanelas.Destroy;
    //
    FmPerfisJanelas := nil;
  end;
end;

procedure TFmPrincipal.MostraSenhasEspecificas;
begin
  //
end;

procedure TFmPrincipal.MostraUsuarios(Numero: Integer);
begin
  Application.CreateForm(TFmUsuarios, FmUsuarios);
  //
  if Numero <> 0 then
    FmUsuarios.LocCod(Numero, Numero);
  //
  FmUsuarios.ShowModal;
  FmUsuarios.Destroy;
  //
  FmUsuarios := nil;
end;

procedure TFmPrincipal.QrMasterCalcFields(DataSet: TDataSet);
begin
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  //MeuVCLSkin.VCLSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
end;

procedure TFmPrincipal.GradeClica(LibJan: TLibJan; Acao: TSQLType;
  QueryPerfIts: TMySQLQuery; Codigo: Integer);
var
  Libera, Olha, Inclui, Altera, Exclui: Integer;
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM perfits ');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND Janela=:P1');
  Dmod.QrUpd.Params[00].AsInteger := Codigo;
  Dmod.QrUpd.Params[01].AsString  := QueryPerfIts.FieldByName('Janela').AsString;
  Dmod.QrUpd.ExecSQL;
  //
  if Acao in ([stIns,stUpd]) then
  begin
    if Acao = stIns then
    begin
      Olha   := 1;
      Inclui := 1;
      Altera := 1;
      Exclui := 1;
    end else
    begin
      Olha   := QueryPerfIts.FieldByName('Olha').AsInteger;
      Inclui := QueryPerfIts.FieldByName('Inclui').AsInteger;
      Altera := QueryPerfIts.FieldByName('Altera').AsInteger;
      Exclui := QueryPerfIts.FieldByName('Exclui').AsInteger;
      case libJan of
        libjanAll:
        begin
          Olha   := dmkPF.EscolhaDe2Int(QueryPerfIts.FieldByName('Libera').AsInteger = 1, 0, 1);
          Inclui := dmkPF.EscolhaDe2Int(QueryPerfIts.FieldByName('Libera').AsInteger = 1, 0, 1);
          Altera := dmkPF.EscolhaDe2Int(QueryPerfIts.FieldByName('Libera').AsInteger = 1, 0, 1);
          Exclui := dmkPF.EscolhaDe2Int(QueryPerfIts.FieldByName('Libera').AsInteger = 1, 0, 1);
        end;
        libjanSee: Olha   := dmkPF.EscolhaDe2Int(QueryPerfIts.FieldByName('Olha').AsInteger = 1, 0, 1);
        libjanIns: Inclui := dmkPF.EscolhaDe2Int(QueryPerfIts.FieldByName('Inclui').AsInteger = 1, 0, 1);
        libjanUpd: Altera := dmkPF.EscolhaDe2Int(QueryPerfIts.FieldByName('Altera').AsInteger = 1, 0, 1);
        libjanDel: Exclui := dmkPF.EscolhaDe2Int(QueryPerfIts.FieldByName('Exclui').AsInteger = 1, 0, 1);
      end;
    end;
    if (Olha=1) and (Inclui=1) and (Altera=1) and (Exclui=1) then
      Libera := 1
    else
      Libera := 0;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO perfits SET Libera=:P0, ');
    Dmod.QrUpd.SQL.Add('Olha=:P1, Inclui=:P2, Altera=:P3, Exclui=:P4, ');
    Dmod.QrUpd.SQL.Add('Codigo=:Pa, Janela=:Pb');
    //
    Dmod.QrUpd.Params[00].AsInteger := Libera;
    Dmod.QrUpd.Params[01].AsInteger := Olha;
    Dmod.QrUpd.Params[02].AsInteger := Inclui;
    Dmod.QrUpd.Params[03].AsInteger := Altera;
    Dmod.QrUpd.Params[04].AsInteger := Exclui;
    //
    Dmod.QrUpd.Params[05].AsInteger := Codigo;
    Dmod.QrUpd.Params[06].AsString  := QueryPerfIts.FieldByName('Janela').AsString;
    Dmod.QrUpd.ExecSQL;
  end;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  VAR_TYPE_LOG          := ttlCliIntUni;
  VAR_USA_TAG_BITBTN    := True;
  VAR_STTERMINAL        := StatusBar1.Panels[09];
  VAR_STDATALICENCA     := StatusBar1.Panels[11];
  VAR_STAVISOS          := StatusBar2.Panels[01];
  VAR_SKINUSANDO        := StatusBar1.Panels[13];
  Application.OnHint    := ShowHint;
  Application.OnMessage := MyObjects.FormMsg;
  Application.OnIdle    := AppIdle;
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar2.SimplePanel := True;
    StatusBar2.SimpleText := Application.Hint;
  end else StatusBar2.SimplePanel := False;
end;

procedure TFmPrincipal.Image1Click(Sender: TObject);
var
  Pode: Boolean;
begin
  if (VAR_SENHA = CO_MASTER) or (VAR_SENHA = VAR_BOSS) then  Pode := True
  else Pode := False;
  case QrMasterMonitorar.Value of
    0: GOTOy.CriaFmUnLock_MLA;
    1: if pode then TravaAplicativo;
    2: GOTOy.CriaFmUnLock_MLA;
    3: if pode then TravaAplicativo;
    4: ; // n�o faz nada, � vital�cio
  end;
end;

procedure TFmPrincipal.InfoSeqIni(Texto: String);
begin
// Compatibilidade
end;

procedure TFmPrincipal.TravaAplicativo;
begin
  //UMyMod.FormShow(TFmTravar, FmTravar, afmoNegarComAviso);
  if DBCheck.CriaFm(TFmTravar, FmTravar, afmoNegarComAviso) then
  begin
    FmTravar.ShowModal;
    FmTravar.Destroy;
  end;
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmDermaAD_MLA.Show;
  Enabled := False;
  FmDermaAD_MLA.Refresh;
  FmDermaAD_MLA.EdSenha.Text := FmDermaAD_MLA.EdSenha.Text+'*';
  FmDermaAD_MLA.EdSenha.Refresh;
  FmDermaAD_MLA.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
  except
    Application.MessageBox(PChar('Imposs�vel criar Modulo de dados'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  FmDermaAD_MLA.EdSenha.Text := FmDermaAD_MLA.EdSenha.Text+'*';
  FmDermaAD_MLA.EdSenha.Refresh;
  FmDermaAD_MLA.EdLogin.Text := '';
  FmDermaAD_MLA.EdLogin.PasswordChar := '*';
  FmDermaAD_MLA.EdSenha.Text := '';
  FmDermaAD_MLA.EdSenha.Refresh;
  FmDermaAD_MLA.EdLogin.ReadOnly := False;
  FmDermaAD_MLA.EdSenha.ReadOnly := False;
  FmDermaAD_MLA.EdLogin.SetFocus;
  //FmDermaAD_MLA.ReloadSkin;
  FmDermaAD_MLA.Refresh;
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
var
  x: Integer;
begin
  if ((VAR_SENHA = Uppercase(VAR_BOSSSENHA)) and (VAR_LOGIN = 
    UpperCase(VAR_BOSSLOGIN))) or (VAR_SENHA = CO_MASTER) or
    ((VAR_SENHA = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> '')) then
  begin
    TimerIdle.Enabled := False;
    //
    if FmUsuarios <> nil then
      FmUsuarios.Close;
    if FmUsuariosNew <> nil then
      FmUsuariosNew.Close;
    if FmPerfis <> nil then
      FmPerfis.Close;
    if FmPerfisJanelas <> nil then
      FmPerfisJanelas.Close;
    if FmSenhaBoss <> nil then
      FmSenhaBoss.Close;
    if FmMatriz <> nil then
      FmMatriz.Close;
    //
    if FmNovaVersao = nil then
      MostraLogoff;
  end;
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
(*****
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(AdvToolBarButton8, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
*)
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
//
end;

function TFmPrincipal.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
{$IfNDef sUcriar}
  Result := Ucriar.RecriaTabelaLocal(Tabela, Acao);
{$EndIf}
end;

procedure TFmPrincipal.ReopenMaster;
begin
  QrMaster.Close;
  QrMaster.Database := Dmod.MyDB;
  QrMaster.Params[0].AsString := CO_USERSPNOW;
  QrMaster.Open;
end;

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade: TStringGrid);
begin
   // Compatibilidade
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo;
var
  i: Integer;
  Janela: String;
begin
  try
    Screen.Cursor := crHourGlass;
    //
    if DModG <> nil then
    begin
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      //
      ReopenMaster();
      //
      TmVersao.Enabled := True;
      //
      if ParamCount > 0 then
      begin
        for i := 0 to ParamCount do
        begin
          if i = 2 then //Janela
          begin
            if ParamStr(i) <> '#' then
            begin
              Janela := ParamStr(i);
              //
              MostraPerfisJanelas(Janela);
            end;
          end;
        end;
      end;
      DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtMostra);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
var
  Link: String;
begin
  Link := DmkWeb.ObtemLinkAjuda(CO_DMKID_APP, 0);
  //
  DmkWeb.MostraWebBrowser(Link);
end;

procedure TFmPrincipal.AGBPerfisClick(Sender: TObject);
begin
  MostraPerfis(0);
end;

procedure TFmPrincipal.AdvGlowButton28Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AGBJanelasClick(Sender: TObject);
begin
  MostraPerfisJanelas('')
end;

procedure TFmPrincipal.AGBBossClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSenhaBoss, FmSenhaBoss, afmoSoBoss) then
  begin
    FmSenhaBoss.EdEmpresa.ValueVariant := QrMasterEm.Value;
    FmSenhaBoss.EdEmpresa.Enabled      := False;
    FmSenhaBoss.EdCNPJ.ValueVariant    := QrMasterCNPJ_TXT.Value;
    FmSenhaBoss.EdCNPJ.Enabled         := False;
    FmSenhaBoss.EdLogin.ValueVariant   := QrMasterMasLogin.Value;
    FmSenhaBoss.EdSenha1.Text          := QrMasterMasSenhaNew.Value;
    FmSenhaBoss.EdSenha2.Text          := QrMasterMasSenhaNew.Value;
    //
    FmSenhaBoss.ShowModal;
    FmSenhaBoss.Destroy;
    //
    if (VAR_SENHATXT <> '') and (VAR_BOSS <> '') then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE master SET MasSenha=AES_ENCRYPT(:P0, :P1), MasLogin=:P2');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsString := VAR_SENHATXT;
      Dmod.QrUpd.Params[01].AsString := CO_USERSPNOW;
      Dmod.QrUpd.Params[02].AsString := VAR_BOSS;
      Dmod.QrUpd.ExecSQL;
    end;
    FmSenhaBoss := nil;
  end;
end;

procedure TFmPrincipal.AGBEmpresaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoMaster) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
    //
    FmMatriz := nil;
  end;
end;

procedure TFmPrincipal.AGBFiliaisClick(Sender: TObject);
begin
  MostraFilialUsrs();
end;

procedure TFmPrincipal.AGBUsuariosClick(Sender: TObject);
begin
  MostraUsuarios(0);
end;

procedure TFmPrincipal.AdvGlowButton6Click(Sender: TObject);
begin
  VerificaNovasVersoes;
end;

procedure TFmPrincipal.AdvToolBarButton7Click(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.AdvToolBarButton8Click(Sender: TObject);
begin
  VerificaNovasVersoes;
end;

procedure TFmPrincipal.AGMBVerifiBDClick(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.AppIdle(Sender: TObject; var Done: Boolean);
begin
  TimerIdle.Enabled := False;
  TimerIdle.Enabled := True;
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
var
  Versao: Integer;
  ArqNome: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'DermaAD2',
              'DermaAD2', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value),
              CO_VERSAO, CO_DMKID_APP, DModG.ObtemAgora(), Memo1, dtExecAux,
              Versao, ArqNome, False, ApenasVerifica, BalloonHint1);
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
  Entidade: Integer; Aba: Boolean = False);
begin
  // Compatibilidade
end;

end.
