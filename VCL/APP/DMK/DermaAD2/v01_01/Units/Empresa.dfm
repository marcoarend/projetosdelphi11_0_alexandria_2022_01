object FmEmpresa: TFmEmpresa
  Left = 394
  Top = 170
  Caption = 'MAS-CADAS-000 :: Configura'#231#227'o do adquirinte do aplicativo'
  ClientHeight = 562
  ClientWidth = 632
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 632
    Height = 466
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 632
      Height = 466
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      ExplicitTop = 4
      object TabSheet1: TTabSheet
        Caption = 'Cadastro'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PainelDados2: TPanel
          Left = 0
          Top = 177
          Width = 624
          Height = 169
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object Label13: TLabel
            Left = 12
            Top = 8
            Width = 250
            Height = 13
            Caption = 'C'#243'digo da entidade, Raz'#227'o Social / Nome Completo:'
            FocusControl = EdRazao
          end
          object Label14: TLabel
            Left = 12
            Top = 48
            Width = 86
            Height = 13
            Caption = 'Chave de acesso:'
            FocusControl = EdHD
          end
          object Label15: TLabel
            Left = 364
            Top = 48
            Width = 60
            Height = 13
            Caption = 'Master login:'
          end
          object Label16: TLabel
            Left = 164
            Top = 48
            Width = 55
            Height = 13
            Caption = 'Data inicial:'
          end
          object Label17: TLabel
            Left = 264
            Top = 48
            Width = 48
            Height = 13
            Caption = 'Data final:'
          end
          object Label19: TLabel
            Left = 492
            Top = 48
            Width = 67
            Height = 13
            Caption = 'Master senha:'
          end
          object Label21: TLabel
            Left = 480
            Top = 8
            Width = 61
            Height = 13
            Caption = 'CNPJ / CPF:'
            FocusControl = EdCNPJ
          end
          object EdRazao: TdmkEdit
            Left = 52
            Top = 24
            Width = 425
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 100
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdHD: TdmkEdit
            Left = 12
            Top = 64
            Width = 149
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnKeyDown = EdHDKeyDown
          end
          object EdCNPJ: TdmkEdit
            Left = 480
            Top = 24
            Width = 141
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnExit = EdCNPJExit
          end
          object CkAtiva: TCheckBox
            Left = 540
            Top = 132
            Width = 81
            Height = 17
            Caption = 'Senha ativa.'
            TabOrder = 9
          end
          object EdLogin: TdmkEdit
            Left = 364
            Top = 64
            Width = 125
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 30
            ParentFont = False
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object TPDataI: TDateTimePicker
            Left = 164
            Top = 64
            Width = 97
            Height = 21
            Date = 37845.652141365700000000
            Time = 37845.652141365700000000
            Color = clWhite
            TabOrder = 4
          end
          object TPDataF: TDateTimePicker
            Left = 264
            Top = 64
            Width = 97
            Height = 21
            Date = 37845.652141365700000000
            Time = 37845.652141365700000000
            Color = clWhite
            TabOrder = 5
          end
          object RGMonitorar: TRadioGroup
            Left = 12
            Top = 88
            Width = 609
            Height = 37
            Caption = ' Status do Aplicativo: '
            Columns = 5
            ItemIndex = 1
            Items.Strings = (
              'Bloqueado'
              'Monitorado'
              'Vencido'
              'Alugado'
              'Liberado')
            TabOrder = 8
          end
          object EdDono: TdmkEdit
            Left = 12
            Top = 24
            Width = 37
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 100
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdSenha: TEdit
            Left = 492
            Top = 64
            Width = 128
            Height = 21
            PasswordChar = '*'
            TabOrder = 7
          end
        end
        object PainelDados1: TPanel
          Left = 0
          Top = 0
          Width = 624
          Height = 177
          Align = alTop
          BevelOuter = bvNone
          Enabled = False
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 12
            Top = 8
            Width = 250
            Height = 13
            Caption = 'C'#243'digo da entidade, Raz'#227'o Social / Nome Completo:'
            FocusControl = DBRazao
          end
          object Label2: TLabel
            Left = 12
            Top = 48
            Width = 86
            Height = 13
            Caption = 'Chave de acesso:'
            FocusControl = DBHD
          end
          object Label3: TLabel
            Left = 364
            Top = 48
            Width = 60
            Height = 13
            Caption = 'Master login:'
            FocusControl = DBLogin
          end
          object Label4: TLabel
            Left = 164
            Top = 48
            Width = 55
            Height = 13
            Caption = 'Data inicial:'
            FocusControl = DBDataI
          end
          object Label5: TLabel
            Left = 264
            Top = 48
            Width = 48
            Height = 13
            Caption = 'Data final:'
            FocusControl = DBDataF
          end
          object Label7: TLabel
            Left = 492
            Top = 48
            Width = 67
            Height = 13
            Caption = 'Master senha:'
            FocusControl = DBSenha
          end
          object Label8: TLabel
            Left = 188
            Top = 132
            Width = 48
            Height = 13
            Caption = 'Distor'#231#227'o:'
            FocusControl = DBEdit8
          end
          object Label9: TLabel
            Left = 480
            Top = 8
            Width = 61
            Height = 13
            Caption = 'CNPJ / CPF:'
            FocusControl = DBCNPJ
          end
          object Label10: TLabel
            Left = 12
            Top = 132
            Width = 25
            Height = 13
            Caption = 'Hoje:'
            FocusControl = DBEdit10
          end
          object Label11: TLabel
            Left = 112
            Top = 132
            Width = 26
            Height = 13
            Caption = 'Hora:'
            FocusControl = DBEdit22
          end
          object DBRazao: TDBEdit
            Left = 48
            Top = 24
            Width = 429
            Height = 21
            Hint = 'N'#186' do banco'
            TabStop = False
            DataField = 'Em'
            DataSource = DsEmpresa
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 3
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
          end
          object DBHD: TDBEdit
            Left = 12
            Top = 64
            Width = 149
            Height = 21
            DataField = 'Licenca'
            DataSource = DsEmpresa
            TabOrder = 1
          end
          object DBLogin: TDBEdit
            Left = 364
            Top = 64
            Width = 125
            Height = 21
            DataField = 'MasLogin'
            DataSource = DsEmpresa
            TabOrder = 2
          end
          object DBDataI: TDBEdit
            Left = 164
            Top = 64
            Width = 97
            Height = 21
            DataField = 'DataI'
            DataSource = DsEmpresa
            TabOrder = 3
          end
          object DBDataF: TDBEdit
            Left = 264
            Top = 64
            Width = 97
            Height = 21
            DataField = 'DataF'
            DataSource = DsEmpresa
            TabOrder = 4
          end
          object DBSenha: TDBEdit
            Left = 492
            Top = 64
            Width = 129
            Height = 21
            DataField = 'MasSenhaNew'
            DataSource = DsEmpresa
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            PasswordChar = '*'
            TabOrder = 5
          end
          object DBEdit8: TDBEdit
            Left = 188
            Top = 148
            Width = 81
            Height = 21
            DataField = 'Distorcao'
            DataSource = DsEmpresa
            TabOrder = 6
          end
          object DBCNPJ: TDBEdit
            Left = 480
            Top = 24
            Width = 141
            Height = 21
            Hint = 'Nome do banco'
            DataField = 'CNPJ_TXT'
            DataSource = DsEmpresa
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 7
          end
          object DBEdit10: TDBEdit
            Left = 12
            Top = 148
            Width = 97
            Height = 21
            DataField = 'Hoje'
            DataSource = DsEmpresa
            TabOrder = 8
          end
          object DBEdit22: TDBEdit
            Left = 112
            Top = 148
            Width = 73
            Height = 21
            DataField = 'Hora'
            DataSource = DsEmpresa
            TabOrder = 9
          end
          object DBAtiva: TDBCheckBox
            Left = 540
            Top = 140
            Width = 81
            Height = 17
            Caption = 'Senha ativa.'
            DataField = 'MasAtivar'
            DataSource = DsEmpresa
            TabOrder = 10
            ValueChecked = 'V'
            ValueUnchecked = 'F'
          end
          object DBRadioGroup1: TDBRadioGroup
            Left = 12
            Top = 92
            Width = 609
            Height = 37
            Caption = ' Status do Aplicativo: '
            Columns = 5
            DataField = 'Monitorar'
            DataSource = DsEmpresa
            Items.Strings = (
              'Bloqueado'
              'Monitorado'
              'Vencido'
              'Alugado'
              'Liberado')
            ParentBackground = True
            TabOrder = 11
            Values.Strings = (
              '0'
              '1'
              '2')
          end
          object DBEdit1: TDBEdit
            Left = 12
            Top = 24
            Width = 33
            Height = 21
            Hint = 'N'#186' do banco'
            TabStop = False
            DataField = 'Dono'
            DataSource = DsControle
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 3
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 12
          end
        end
        object PainelConfirma: TPanel
          Left = 0
          Top = 346
          Width = 624
          Height = 46
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          Visible = False
          object BtConfirma: TBitBtn
            Tag = 14
            Left = 12
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
            Caption = '&Confirma'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtConfirmaClick
            NumGlyphs = 2
          end
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 528
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtDesisteClick
            NumGlyphs = 2
          end
        end
        object PainelControle: TPanel
          Left = 0
          Top = 392
          Width = 624
          Height = 46
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 3
          object BtAltera: TBitBtn
            Tag = 11
            Left = 12
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Altera banco atual'
            Caption = '&Altera'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtAlteraClick
            NumGlyphs = 2
          end
          object BtSaida: TBitBtn
            Tag = 13
            Left = 528
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Terminais'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 217
          Height = 438
          Align = alLeft
          DataSource = DsTerminais
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Terminal'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IP'
              Width = 128
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Filiais'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter1: TSplitter
          Left = 0
          Top = 206
          Width = 624
          Height = 3
          Cursor = crVSplit
          Align = alTop
          ExplicitLeft = 8
          ExplicitTop = 220
        end
        object Panel1: TPanel
          Left = 0
          Top = 392
          Width = 624
          Height = 46
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          Visible = False
          object BtConfirma2: TBitBtn
            Tag = 14
            Left = 12
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
            Caption = '&Confirma'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtConfirmaClick
            NumGlyphs = 2
          end
          object BtDesiste2: TBitBtn
            Tag = 15
            Left = 528
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtDesisteClick
            NumGlyphs = 2
          end
        end
        object Panel2: TPanel
          Left = 0
          Top = 346
          Width = 624
          Height = 46
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object BtFilial: TBitBtn
            Tag = 10049
            Left = 12
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Filial'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtFilialClick
            NumGlyphs = 2
          end
          object BtSaida2: TBitBtn
            Tag = 14
            Left = 528
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
          object BtUsuario: TBitBtn
            Tag = 10047
            Left = 108
            Top = 4
            Width = 90
            Height = 39
            Cursor = crHandPoint
            Caption = '&Usu'#225'rio'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtUsuarioClick
            NumGlyphs = 2
          end
        end
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 624
          Height = 206
          Align = alTop
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Entidade'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Filial'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Nome'
              Width = 272
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJCPF_TXT'
              Title.Caption = 'CNPJ/CPF'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IERG'
              Title.Caption = 'I.E./RG'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Atividade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FormaSociet'
              Title.Caption = 'Forma societ'#225'ria'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Simples'
              Title.Caption = 'Simples Estadual'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsFiliais
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Entidade'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Filial'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Nome'
              Width = 272
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJCPF_TXT'
              Title.Caption = 'CNPJ/CPF'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IERG'
              Title.Caption = 'I.E./RG'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Atividade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FormaSociet'
              Title.Caption = 'Forma societ'#225'ria'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Simples'
              Title.Caption = 'Simples Estadual'
              Visible = True
            end>
        end
        object dmkDBGrid2: TdmkDBGrid
          Left = 0
          Top = 209
          Width = 624
          Height = 137
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Numero'
              Title.Caption = 'Usu'#225'rio'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Login'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFUNCIONARIO'
              Title.Caption = 'Nome do funcion'#225'rio'
              Width = 308
              Visible = True
            end>
          Color = clWindow
          DataSource = DsUsuarios
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 3
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Numero'
              Title.Caption = 'Usu'#225'rio'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Login'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFUNCIONARIO'
              Title.Caption = 'Nome do funcion'#225'rio'
              Width = 308
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 632
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 584
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 52
      Height = 52
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 52
      Top = 0
      Width = 532
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 485
        Height = 32
        Caption = 'Configura'#231#227'o do adquirinte do aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 485
        Height = 32
        Caption = 'Configura'#231#227'o do adquirinte do aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 485
        Height = 32
        Caption = 'Configura'#231#227'o do adquirinte do aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 632
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 628
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrEmpresa: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmpresaCalcFields
    SQL.Strings = (
      'SELECT *, AES_DECRYPT(MasSenha, :P0) MasSenhaNew'
      'FROM master')
    Left = 12
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresaCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Calculated = True
    end
    object QrEmpresaMonitorar: TSmallintField
      FieldName = 'Monitorar'
      Origin = 'master.Monitorar'
    end
    object QrEmpresaMasLogin: TWideStringField
      FieldName = 'MasLogin'
      Origin = 'master.MasLogin'
      Size = 30
    end
    object QrEmpresaMasSenha: TWideStringField
      FieldName = 'MasSenha'
      Origin = 'master.MasSenha'
      Size = 30
    end
    object QrEmpresaMasAtivar: TWideStringField
      FieldName = 'MasAtivar'
      Origin = 'master.MasAtivar'
      Size = 1
    end
    object QrEmpresaSolicitaSenha: TSmallintField
      FieldName = 'SolicitaSenha'
      Origin = 'master.SolicitaSenha'
    end
    object QrEmpresaEm: TWideStringField
      FieldName = 'Em'
      Origin = 'master.Em'
      Size = 100
    end
    object QrEmpresaCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'master.CNPJ'
      Size = 30
    end
    object QrEmpresaLicenca: TWideStringField
      FieldName = 'Licenca'
      Origin = 'master.Licenca'
      Size = 50
    end
    object QrEmpresaDistorcao: TIntegerField
      FieldName = 'Distorcao'
      Origin = 'master.Distorcao'
    end
    object QrEmpresaDataI: TDateField
      FieldName = 'DataI'
      Origin = 'master.DataI'
    end
    object QrEmpresaDataF: TDateField
      FieldName = 'DataF'
      Origin = 'master.DataF'
    end
    object QrEmpresaHoje: TDateField
      FieldName = 'Hoje'
      Origin = 'master.Hoje'
    end
    object QrEmpresaHora: TTimeField
      FieldName = 'Hora'
      Origin = 'master.Hora'
    end
    object QrEmpresaLimite: TSmallintField
      FieldName = 'Limite'
      Origin = 'master.Limite'
    end
    object QrEmpresaMasSenhaNew: TWideStringField
      FieldName = 'MasSenhaNew'
      Size = 30
    end
  end
  object DsEmpresa: TDataSource
    DataSet = QrEmpresa
    Left = 40
    Top = 8
  end
  object TbTerminais: TmySQLTable
    Database = Dmod.MyDB
    TableName = 'terminais'
    Left = 72
    Top = 8
  end
  object DsTerminais: TDataSource
    DataSet = TbTerminais
    Left = 100
    Top = 8
  end
  object QrFiliais: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFiliaisBeforeClose
    AfterScroll = QrFiliaisAfterScroll
    OnCalcFields = QrFiliaisCalcFields
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Filial,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJCPF, '
      'IF(ent.Tipo=0, ent.IE, ent.RG) IERG,'
      'FormaSociet, Simples, Atividade  '
      'FROM entidades ent'
      'WHERE ent.Codigo < -10')
    Left = 500
    Top = 28
    object QrFiliaisCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFiliaisFilial: TIntegerField
      FieldName = 'Filial'
      Required = True
    end
    object QrFiliaisNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrFiliaisCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrFiliaisIERG: TWideStringField
      FieldName = 'IERG'
    end
    object QrFiliaisFormaSociet: TWideStringField
      FieldName = 'FormaSociet'
    end
    object QrFiliaisSimples: TSmallintField
      FieldName = 'Simples'
      Required = True
    end
    object QrFiliaisAtividade: TWideStringField
      FieldName = 'Atividade'
      Size = 50
    end
    object QrFiliaisCNPJCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJCPF_TXT'
      Size = 40
      Calculated = True
    end
  end
  object DsFiliais: TDataSource
    DataSet = QrFiliais
    Left = 528
    Top = 28
  end
  object PMFilial: TPopupMenu
    Left = 48
    Top = 372
    object Incluinovafilial1: TMenuItem
      Caption = '&Inclui nova filial'
      OnClick = Incluinovafilial1Click
    end
    object Alterafilialatual1: TMenuItem
      Caption = '&Altera filial atual'
      OnClick = Alterafilialatual1Click
    end
    object Excluifilial1: TMenuItem
      Caption = '&Exclui filial'
      Enabled = False
    end
  end
  object PMUsuario: TPopupMenu
    Left = 136
    Top = 372
    object Adicionausurio1: TMenuItem
      Caption = '&Adiciona usu'#225'rio'
      OnClick = Adicionausurio1Click
    end
    object Retirausurio1: TMenuItem
      Caption = '&Retira usu'#225'rio'
      OnClick = Retirausurio1Click
    end
  end
  object QrUsuarios: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT si.Numero, si.Controle, si.Empresa, '
      'se.Login, en.Nome NOMEFUNCIONARIO '
      'FROM senhasits si'
      'LEFT JOIN senhas se ON si.Numero=se.Numero'
      'LEFT JOIN entidades en ON en.Codigo=se.Funcionario'
      'WHERE si.Empresa=:P0')
    Left = 556
    Top = 28
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUsuariosNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrUsuariosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrUsuariosEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrUsuariosLogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
    object QrUsuariosNOMEFUNCIONARIO: TWideStringField
      FieldName = 'NOMEFUNCIONARIO'
      Size = 100
    end
  end
  object DsUsuarios: TDataSource
    DataSet = QrUsuarios
    Left = 584
    Top = 28
  end
  object DsControle: TDataSource
    DataSet = Dmod.QrControle
    Left = 176
    Top = 56
  end
end
