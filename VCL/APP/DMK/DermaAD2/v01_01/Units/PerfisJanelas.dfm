object FmPerfisJanelas: TFmPerfisJanelas
  Left = 339
  Top = 185
  Caption = 'AD2-PERFI-002 :: Perfis - Janelas'
  ClientHeight = 668
  ClientWidth = 1150
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1150
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1090
      Top = 0
      Width = 60
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1031
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 223
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Perfis - Janelas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 223
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Perfis - Janelas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 223
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Perfis - Janelas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1150
    Height = 469
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1150
      Height = 469
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1150
        Height = 469
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 2
          Top = 263
          Width = 1146
          Height = 12
          Cursor = crVSplit
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          ExplicitTop = 262
          ExplicitWidth = 1145
        end
        object Panel5: TPanel
          Left = 2
          Top = 18
          Width = 1146
          Height = 60
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 0
          object Label3: TLabel
            Left = 10
            Top = 5
            Width = 44
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Janela:'
          end
          object EdJanela: TdmkEdit
            Left = 10
            Top = 25
            Width = 375
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object BtRefresh: TBitBtn
            Tag = 18
            Left = 393
            Top = 5
            Width = 147
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Reabre'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtRefreshClick
          end
        end
        object DBGrade: TdmkDBGridDAC
          Left = 2
          Top = 78
          Width = 1146
          Height = 185
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'PerfilTXT'
              Title.Caption = 'Perfil'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Janela'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              Title.Caption = 'ID Janela'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Libera'
              Title.Caption = 'Tudo'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Olha'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Inclui'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Altera'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Exclui'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Janela'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Modulo'
              Title.Caption = 'M'#243'dulo'
              Width = 65
              Visible = True
            end>
          Color = clWindow
          DataSource = DsPerfIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGradeCellClick
          OnDrawColumnCell = DBGradeDrawColumnCell
          EditForceNextYear = False
          Columns = <
            item
              Expanded = False
              FieldName = 'PerfilTXT'
              Title.Caption = 'Perfil'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Janela'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              Title.Caption = 'ID Janela'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Libera'
              Title.Caption = 'Tudo'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Olha'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Inclui'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Altera'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Exclui'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Janela'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Modulo'
              Title.Caption = 'M'#243'dulo'
              Width = 65
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 2
          Top = 275
          Width = 1146
          Height = 192
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object StaticText1: TStaticText
            Left = 0
            Top = 0
            Width = 1146
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'Usu'#225'rio que utilizam o perfil selecionado'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object dmkDBGrid2: TdmkDBGrid
            Left = 0
            Top = 20
            Width = 1146
            Height = 172
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Numero'
                Title.Caption = 'Usu'#225'rio'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Login'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEFUNCIONARIO'
                Title.Caption = 'Nome do funcion'#225'rio'
                Width = 308
                Visible = True
              end>
            Color = clWindow
            DataSource = DsUsuarios
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Numero'
                Title.Caption = 'Usu'#225'rio'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Login'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEFUNCIONARIO'
                Title.Caption = 'Nome do funcion'#225'rio'
                Width = 308
                Visible = True
              end>
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 528
    Width = 1150
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1146
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 582
    Width = 1150
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 971
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 969
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtUsuarios: TBitBtn
        Tag = 10047
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Usu'#225'rios'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtUsuariosClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 504
    Top = 35
  end
  object QrPerfIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPerfItsBeforeClose
    AfterScroll = QrPerfItsAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT pip.Libera, pip.Olha, '
      'pip.Inclui, pip.Altera, pip.Exclui, pip.Codigo,'
      'pit.Janela, pit.Nome, pit.Descricao, pit.Modulo'
      'FROM perfjan pit'
      
        'LEFT JOIN perfits pip ON pip.Janela=pit.Janela AND pip.Codigo=:P' +
        '0')
    Left = 536
    Top = 201
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfItsLibera: TSmallintField
      FieldName = 'Libera'
      MaxValue = 1
    end
    object QrPerfItsOlha: TSmallintField
      FieldName = 'Olha'
      MaxValue = 1
    end
    object QrPerfItsInclui: TSmallintField
      FieldName = 'Inclui'
      MaxValue = 1
    end
    object QrPerfItsAltera: TSmallintField
      FieldName = 'Altera'
      MaxValue = 1
    end
    object QrPerfItsExclui: TSmallintField
      FieldName = 'Exclui'
      MaxValue = 1
    end
    object QrPerfItsJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 13
    end
    object QrPerfItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPerfItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPerfItsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrPerfItsModulo: TWideStringField
      FieldName = 'Modulo'
      Size = 4
    end
    object QrPerfItsPerfilTXT: TWideStringField
      FieldName = 'PerfilTXT'
      Size = 30
    end
  end
  object DsPerfIts: TDataSource
    DataSet = QrPerfIts
    Left = 564
    Top = 201
  end
  object DsUsuarios: TDataSource
    DataSet = QrUsuarios
    Left = 424
    Top = 352
  end
  object QrUsuarios: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT si.Numero, si.Controle, si.Empresa, '
      'se.Login, en.Nome NOMEFUNCIONARIO '
      'FROM senhasits si'
      'LEFT JOIN senhas se ON si.Numero=se.Numero'
      'LEFT JOIN entidades en ON en.Codigo=se.Funcionario'
      'WHERE si.Empresa=:P0')
    Left = 396
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUsuariosNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrUsuariosLogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
    object QrUsuariosNOMEFUNCIONARIO: TWideStringField
      FieldName = 'NOMEFUNCIONARIO'
      Size = 100
    end
  end
end
