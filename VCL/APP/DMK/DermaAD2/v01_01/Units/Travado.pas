unit Travado;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ShellApi,
  ExtDlgs, ToolWin, ComCtrls, ZCF2;

type
  TFmTravado = class(TForm)
    BtCancela: TBitBtn;
    LaDemo: TLabel;
    Label4: TLabel;
    LaAluguel: TLabel;
    procedure BtCancelaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmTravado: TFmTravado;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmTravado.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTravado.FormCreate(Sender: TObject);
begin
  Label4.Caption := 'Execute o aplicativo principal ['+Dmod.MyDb.DataBaseName+']';
end;

end.
