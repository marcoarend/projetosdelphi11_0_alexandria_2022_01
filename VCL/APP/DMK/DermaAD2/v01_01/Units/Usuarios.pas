unit Usuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  UnInternalConsts, UnInternalConsts2, UnMsgInt, ZCF2, ResIntStrings, dmkGeral,
  UMySQLModule, mySQLDbTables, UnGOTOy, Menus, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmUsuarios = class(TForm)
    QrSenhas: TmySQLQuery;
    DsSenhas: TDataSource;
    QrSenhasLogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasSenha: TWideStringField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasLk: TIntegerField;
    QrSenhasNOMEPERFIL: TWideStringField;
    QrSenhasNOMEFUNCIONARIO: TWideStringField;
    QrSenhasFuncionario: TIntegerField;
    QrSenhasSenhaNew: TWideStringField;
    QrSenhasAtivo: TIntegerField;
    QrSenhasIP_Default: TWideStringField;
    PainelDados: TPanel;
    Label2: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBEdit6: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    BtExclui: TBitBtn;
    BtFilial: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtFilialClick(Sender: TObject);
    procedure QrSenhasAfterScroll(DataSet: TDataSet);
    procedure QrSenhasAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    procedure CriaOForm;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ConfiguraAtivo;
  public
    { Public declarations }    
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmUsuarios: TFmUsuarios;
  Livre : Boolean;

implementation

uses Senha, UsuariosNew, Module, Principal, UnMyObjects, MyDBCheck, MyGlyfs;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmUsuarios.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmUsuarios.QrSenhasAfterOpen(DataSet: TDataSet);
begin
  ConfiguraAtivo;
end;

procedure TFmUsuarios.QrSenhasAfterScroll(DataSet: TDataSet);
begin
  ConfiguraAtivo;
end;

procedure TFmUsuarios.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSenhasNumero.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////
procedure TFmUsuarios.DefParams;
begin
  VAR_GOTOTABELA := 'Senhas';
  VAR_GOTOmySQLTABLE := QrSenhas;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_NUMERO;
  VAR_GOTONOME := 'Login';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;
  //
  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT AES_DECRYPT(se.Senha, "'+
    CO_USERSPNOW+'") SenhaNew, se.*, ');
  VAR_SQLx.Add('pe.Descricao NOMEPERFIL,');
  VAR_SQLx.Add('en.Nome NOMEFUNCIONARIO');
  VAR_SQLx.Add('FROM senhas se');
  VAR_SQLx.Add('LEFT JOIN perfis    pe ON pe.Codigo=se.Perfil');
  VAR_SQLx.Add('LEFT JOIN entidades en ON en.Codigo=se.Funcionario');
  VAR_SQLx.Add('WHERE se.Numero > 0');
  //
  VAR_SQL1.Add('AND se.Numero=:P1');
  //
//  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmUsuarios.ConfiguraAtivo;
begin
  if QrSenhasAtivo.Value = 1 then
  begin
    BtExclui.Caption := 'Desa&tivar'; 
    FmMyGlyfs.ObtemBitmapDeGlyph(224, BtExclui);
  end else
  begin
    BtExclui.Caption := 'Ati&var';
    FmMyGlyfs.ObtemBitmapDeGlyph(223, BtExclui);
  end;
  //Para atualizar a imagem do Glyph
  BtExclui.Enabled := not BtExclui.Enabled;
  BtExclui.Enabled := not BtExclui.Enabled;
end;

procedure TFmUsuarios.CriaOForm;
begin
  DefParams;
  if (VAR_SENHA = VAR_BOSS) or (VAR_SENHA = CO_MASTER) then
  begin
    Livre := True;
    Va(vpLast);
  end
  else
  begin
    Livre := False;
    LocCod(VAR_USUARIO, VAR_USUARIO);
    if QrSenhasNumero.Value <> VAR_USUARIO then Application.Terminate;
  end;
end;

procedure TFmUsuarios.AlteraRegistro;
var
  Senhas : Integer;
begin
  Senhas := QrSenhasNumero.Value;
  if QrSenhasNumero.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Senhas, Dmod.MyDB, 'Senhas', 'Numero') then
  begin
    try
      UMyMod.UpdLockY(Senhas, Dmod.MyDB, 'Senhas', 'Numero');
      ImgTipo.SQLType := stUpd;
      GOTOy.BotoesSb(ImgTipo.SQLType);
      if DBCheck.CriaFm(TFmUsuariosNew, FmUsuariosNew, afmoNegarComAviso) then
      begin
        FmUsuariosNew.ImgTipo.SQLType            := stUpd;
        FmUsuariosNew.EdNumero.Text              := IntToStr(Senhas);
        FmUsuariosNew.Edlogin.Text               := QrSenhasLogin.Value;
        FmUsuariosNew.EdSenha.Text               := QrSenhasSenhaNew.Value;
        FmUsuariosNew.EdConfSenha.Text           := QrSenhasSenhaNew.Value;
        FmUsuariosNew.EdPerfil.ValueVariant      := QrSenhasPerfil.Value;
        FmUsuariosNew.CBPerfil.KeyValue          := QrSenhasPerfil.Value;
        FmUsuariosNew.EdFuncionario.ValueVariant := QrSenhasFuncionario.Value;
        FmUsuariosNew.CBFuncionario.KeyValue     := QrSenhasFuncionario.Value;
        FmUsuariosNew.EdIP_Default.Text          := QrSenhasIP_Default.Value;
        FmUsuariosNew.FLoginAtu                  := QrSenhasLogin.Value;
        FmUsuariosNew.FNumeroAtu                := QrSenhasNumero.Value;
        GOTOy.BotoesSb(ImgTipo.SQLType);
        FmUsuariosNew.ShowModal;
        FmUsuariosNew.Destroy;
        //
        FmUsuariosNew := nil;
      end;
    finally
      LocCod(Senhas, Senhas);
    end;
  end;
end;

procedure TFmUsuarios.IncluiRegistro;
begin
  if DBCheck.CriaFm(TFmUsuariosNew, FmUsuariosNew, afmoSoBoss) then
  begin
    FmUsuariosNew.ImgTipo.SQLType := stIns;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    FmUsuariosNew.ShowModal;
    FmUsuariosNew.Destroy;
    //
    FmUsuariosNew := nil;
  end;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////


procedure TFmUsuarios.SpeedButton1Click(Sender: TObject);
begin
  if Livre then Va(vpFirst);
end;

procedure TFmUsuarios.SpeedButton2Click(Sender: TObject);
begin
  if Livre then Va(vpPrior);
end;

procedure TFmUsuarios.SpeedButton3Click(Sender: TObject);
begin
  if Livre then Va(vpNext);
end;

procedure TFmUsuarios.SpeedButton4Click(Sender: TObject);
begin
  if Livre then Va(vpLast);
end;

procedure TFmUsuarios.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmUsuarios.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

Procedure TFmUsuarios.BtDesisteClick(Sender: TObject);
begin
(*  if ImgTipo.SQLType = 'Inclus�o' then
     UMyMod.PoeEmLivreM('Senhas', QrSenhasNumero.Value);
  if QrSenhas.State in ([dsInsert, dsEdit]) then
     QrSenhas.Cancel;
  QrSenhas.Refresh;

  Edit1.Text := '';
  ImgTipo.SQLType := 'Travado';
  PainelConfirma.Visible:=False;
  PainelDados.Enabled:=False;
  PainelControle.Visible:=True;
  UMyMod.UpdUnlockW(QrSenhasNumero.Value, 'Senhas', 'Numero');
  QrSenhas.Refresh;*)
end;

procedure TFmUsuarios.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  CriaOForm;
  //
  ConfiguraAtivo;
end;

procedure TFmUsuarios.BtExcluiClick(Sender: TObject);
var
  Login: String;
  Numero, Ativo: Integer;
begin
  (*
  VAR_FSENHA := 2;
  Application.CreateForm(TFmSenha, FmSenha);
  FmSenha.FUsuario := QrSenhasNumero.Value;
  FmSenha.ShowModal;
  FmSenha.Destroy;
  LocCod(QrSenhasNumero.Value, QrSenhasNumero.Value);
  *)
  if (QrSenhas.State = dsInactive) or (QrSenhas.RecordCount = 0) then Exit;  
  //
  Login  := QrSenhasLogin.Value;
  Numero := QrSenhasNumero.Value;
  if QrSenhasAtivo.Value = 1 then
    Ativo := 0
  else
    Ativo := 1;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'senhas', False, [
    'Ativo'], ['Login'], [
    Ativo], [Login], True)
  then
    LocCod(Numero, Numero);
end;

procedure TFmUsuarios.BtFilialClick(Sender: TObject);
begin
  FmPrincipal.MostraFilialUsrs();
end;

procedure TFmUsuarios.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmUsuarios.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ConfiguraAtivo;
end;

procedure TFmUsuarios.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmUsuarios.BtAlteraClick(Sender: TObject);
begin
  if (QrSenhas.State = dsInactive) or (QrSenhas.RecordCount = 0) then
    Exit;
  //  
  AlteraRegistro;
end;

end.



