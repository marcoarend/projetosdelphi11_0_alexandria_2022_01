unit Perfis;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  UMySQLModule, mySQLDbTables, Grids, DBGrids, dmkDBGridDAC, ComCtrls, dmkGeral,
  dmkEdit, UnDmkProcFunc, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmPerfis = class(TForm)
    PainelDados: TPanel;
    DsPerfis: TDataSource;
    QrPerf: TmySQLQuery;
    PainelEdita: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    Panel1: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrPerfCodigo: TIntegerField;
    QrPerfDescricao: TWideStringField;
    QrPerfIts: TmySQLQuery;
    DsPerfisItsPerf: TDataSource;
    QrLoc: TmySQLQuery;
    QrLocRegistros: TFloatField;
    Panel2: TPanel;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    BtRefresh: TBitBtn;
    QrPerfItsLibera: TSmallintField;
    QrPerfItsOlha: TSmallintField;
    QrPerfItsInclui: TSmallintField;
    QrPerfItsAltera: TSmallintField;
    QrPerfItsExclui: TSmallintField;
    QrPerfItsJanela: TWideStringField;
    QrPerfItsDescricao: TWideStringField;
    QrPerfItsCodigo: TIntegerField;
    QrPerfItsNome: TWideStringField;
    Grade: TdmkDBGridDAC;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    BtExclui2: TBitBtn;
    Panel8: TPanel;
    Label3: TLabel;
    RGPesquisa: TRadioGroup;
    EdPesquisa: TdmkEdit;
    QrPerfItsModulo: TWideStringField;
    CkEditRelat: TCheckBox;
    DBCkEditRelat: TDBCheckBox;
    QrPerfEditRelat: TSmallintField;
    PB1: TProgressBar;
    BtDuplica: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPerfAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrPerfAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPerfBeforeOpen(DataSet: TDataSet);
    procedure QrPerfAfterClose(DataSet: TDataSet);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtExclui2Click(Sender: TObject);
    procedure dmkDBGridDAC1CellClick(Column: TColumn);
    procedure BtRefreshClick(Sender: TObject);
    procedure QrPerfBeforeClose(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure EdPesquisaChange(Sender: TObject);
    procedure RGPesquisaClick(Sender: TObject);
    procedure EdPesquisaClick(Sender: TObject);
    procedure GradeDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtDuplicaClick(Sender: TObject);
  private
    FJanela: String;
    procedure CriaOForm;
    procedure SubQuery1Reopen(Janela: String);
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MarcarDesmarcarTodos(Marcar: Boolean);
    procedure DuplicaPefilAtual;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmPerfis: TFmPerfis;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Principal, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPerfis.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPerfis.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPerfCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPerfis.DefParams;
begin
  VAR_GOTOTABELA := 'Perfis';
  VAR_GOTOMYSQLTABLE := QrPerf;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_DESCRICAO;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT Codigo, Descricao, EditRelat ');
  VAR_SQLx.Add('FROM perfis');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Descricao Like :P0');
  //
end;

procedure TFmPerfis.MarcarDesmarcarTodos(Marcar: Boolean);
var
  Codigo: Integer;
  Txt: String;
  Acao: TSQLType;
begin
  if (QrPerfIts.State <> dsInactive) and (QrPerfIts.RecordCount > 0) then
  begin
    if Marcar then
    begin
      Txt  := 'marcar';
      Acao := stIns;
    end else
    begin
      Txt  := 'desmarcar';
      Acao := stDel;
    end;
    //
    if Geral.MB_Pergunta('Deseja ' + Txt + ' todos?') <> ID_YES then Exit;  
    //
    Codigo       := QrPerfCodigo.Value;
    PB1.Position := 0;
    PB1.Max      := QrPerfIts.RecordCount;
    QrPerfIts.DisableControls;
    try
      QrPerfIts.First;
      while not QrPerfIts.Eof do
      begin
        FmPrincipal.GradeClica(libjanAll, Acao, QrPerfIts, Codigo);
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        QrPerfIts.Next;
      end;
    finally
      QrPerfIts.EnableControls;
      PB1.Position := 0;
      SubQuery1Reopen(FJanela);
    end;
  end;
end;

procedure TFmPerfis.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    EdNome.Text         := CO_VAZIO;
    EdNome.Visible      := True;
    GBCntrl.Visible     := False;
    //
    if SQLType = stIns then
    begin
      EdCodigo.Text       := FormatFloat(FFormatFloat, Codigo);
      EdNome.Text         := '';
      CkEditRelat.Checked := False;
    end else begin
      EdCodigo.Text       := DBEdCodigo.Text;
      EdNome.Text         := DBEdNome.Text;
      CkEditRelat.Checked := DBCkEditRelat.Checked;
    end;
    EdNome.SetFocus;
  end else begin
    GBCntrl.Visible:=True;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPerfis.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  va(vpLast);
end;

procedure TFmPerfis.AlteraRegistro;
var
  Perfis : Integer;
begin
  Perfis := QrPerfCodigo.Value;
  if QrPerfCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Perfis, Dmod.MyDB, 'Perfis', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Perfis, Dmod.MyDB, 'Perfis', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPerfis.IncluiRegistro;
var
  Perfis : Integer;
begin
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Perfis := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                'Perfis', 'Perfis', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Perfis))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro('Inclus�o cancelada. Limite de cadastros extrapolado');
      Exit;
    end;
    MostraEdicao(True, stIns, Perfis);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPerfis.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPerfis.RGPesquisaClick(Sender: TObject);
begin
  SubQuery1Reopen(QrPerfItsJanela.Value);
end;

procedure TFmPerfis.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPerfis.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPerfis.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPerfis.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPerfis.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPerfis.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmPerfis.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmPerfis.BtSaidaClick(Sender: TObject);
begin
  VAR_MARCACAR := QrPerfCodigo.Value;
  Close;
end;

procedure TFmPerfis.BtConfirmaClick(Sender: TObject);
var
  Codigo, EditRelat: Integer;
  Nome: String;
begin
  Nome      := EdNome.Text;
  EditRelat := Geral.BoolToInt(CkEditRelat.Checked);
  //
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO perfis SET ')
  else
    Dmod.QrUpdU.SQL.Add('UPDATE perfis SET ');
  //
  Dmod.QrUpdU.SQL.Add('Descricao=:P0, EditRelat=:P1, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else
    Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  //
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := EditRelat;  
  Dmod.QrUpdU.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[03].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[04].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Perfis', 'Codigo');
  //
  MostraEdicao(False, stLok, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmPerfis.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Perfis', Codigo);
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Perfis', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Perfis', 'Codigo');
end;

procedure TFmPerfis.BtDuplicaClick(Sender: TObject);
begin
  DuplicaPefilAtual;
end;

procedure TFmPerfis.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  Grade.Align       := alClient;
  CriaOForm;
  //
  PB1.Position            := 0;
  EdPesquisa.ValueVariant := '';
  RGPesquisa.ItemIndex    := 0;
end;

procedure TFmPerfis.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPerfCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPerfis.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmPerfis.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPerfis.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmPerfis.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPerfis.QrPerfAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPerfis.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Perfis', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmPerfis.QrPerfAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrPerfCodigo.Value, False);
  SubQuery1Reopen(FJanela);
  BtTudo.Enabled   := QrPerf.RecordCount > 0;
  BtNenhum.Enabled := QrPerf.RecordCount > 0;
end;

procedure TFmPerfis.SbQueryClick(Sender: TObject);
begin
  //LocCod(QrPerfCodigo.Value,
  //CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Perfis', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPerfis.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPerfis.QrPerfBeforeClose(DataSet: TDataSet);
begin
  BtTudo.Enabled   := False;
  BtNenhum.Enabled := False;
end;

procedure TFmPerfis.QrPerfBeforeOpen(DataSet: TDataSet);
begin
  QrPerfCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPerfis.SubQuery1Reopen(Janela: String);
var
  Loc, SQL: String;
begin
  QrPerfIts.Close;
  if (QrPerf.State in ([dsBrowse])) then
    if (QrPerf.RecordCount>0) then
    begin
      if Length(EdPesquisa.ValueVariant) > 2 then
      begin
        case RGPesquisa.ItemIndex of
          0: SQL := 'AND pit.Janela LIKE "%' + EdPesquisa.ValueVariant + '%"';
          1: SQL := 'AND pit.Descricao LIKE "%' + EdPesquisa.ValueVariant + '%"';
          2: SQL := 'AND pit.Nome LIKE "%' + EdPesquisa.ValueVariant + '%"';
        end;
      end else
        SQL := '';
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrPerfIts, Dmod.MyDB, [
        'SELECT DISTINCT pip.Libera, pip.Olha, ',
        'pip.Inclui, pip.Altera, pip.Exclui, pip.Codigo, ',
        'pit.Janela, pit.Nome, pit.Descricao, pit.Modulo ',
        'FROM perfjan pit ',
        'LEFT JOIN perfits pip ON pip.Janela=pit.Janela AND pip.Codigo=' + Geral.FF0(QrPerfCodigo.Value),
        'WHERE pit.Janela <> ""',
        SQL,
        '']);
      if Janela <> '' then Loc := Janela else Loc := FJanela;
      if Loc <> '' then
        QrPerfIts.Locate('Janela', Loc, [loCaseInsensitive]);
    end;
end;

procedure TFmPerfis.QrPerfAfterClose(DataSet: TDataSet);
begin
  QrPerfIts.Close;
end;

procedure TFmPerfis.BtTudoClick(Sender: TObject);
begin
  MarcarDesmarcarTodos(True);
end;

procedure TFmPerfis.BtNenhumClick(Sender: TObject);
begin
  MarcarDesmarcarTodos(False);
end;

procedure TFmPerfis.BtExclui2Click(Sender: TObject);
begin
  if (QrPerf.State = dsInactive) or (QrPerf.RecordCount = 0) then Exit;
  //  
  if Geral.MB_Pergunta('Confirma a exclus�o de toda lista de perfis? ' +
    'Isto ir� eliminar todos os acessos a todos perfis!. Deseja excluir assim mesmo?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM perfisits');
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM perfisitsperf');
    Dmod.QrUpd.ExecSQL;
    //
    SubQuery1Reopen('');
    Geral.MB_Aviso('As listas de perfis foram excluidas. Para '+
      'recri�-las execute o verificador de banco de dados no aplicativo '+
      'principal!');
  end;
end;

procedure TFmPerfis.dmkDBGridDAC1CellClick(Column: TColumn);
var
  Codigo: Integer;
begin
  if (QrPerfIts.State <> dsInactive) and (QrPerfIts.RecordCount > 0) then
  begin
    if (QrPerfItsModulo.Value <> '') and ((Pos(LowerCase(QrPerfItsModulo.Value),
      LowerCase(FmPrincipal.QrMasterHabilModulos.Value)) = 0) and
      (LowerCase(TMeuDB) <> 'dcontrol')) then
    begin
      Geral.MB_Aviso('M�dulo n�o habilitado! Solicite a libera��o junto a Dermatek!')
    end else
    begin
      Codigo := QrPerfCodigo.Value;
      //
      if Column.FieldName = 'Libera' then
        FmPrincipal.GradeClica(libjanAll, stUpd, QrPerfIts, Codigo)
      else
      if Column.FieldName = 'Olha' then
        FmPrincipal.GradeClica(libjanSee, stUpd, QrPerfIts, Codigo)
      else
      if Column.FieldName = 'Inclui' then
        FmPrincipal.GradeClica(libjanIns, stUpd, QrPerfIts, Codigo)
      else
      if Column.FieldName = 'Altera' then
        FmPrincipal.GradeClica(libjanUpd, stUpd, QrPerfIts, Codigo)
      else
      if Column.FieldName = 'Exclui' then
        FmPrincipal.GradeClica(libjanDel, stUpd, QrPerfIts, Codigo);
      //
      SubQuery1Reopen(QrPerfItsJanela.Value);
    end;
  end;
end;

procedure TFmPerfis.DuplicaPefilAtual;
var
  Codigo: Integer;
  Janela, Nome: String;
begin
  if (QrPerf.State = dsInactive) or (QrPerf.RecordCount = 0) then Exit;
  //
  if Geral.MB_Pergunta('Confirma a duplica��o do perfil atual?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      QrPerf.DisableControls;
      QrPerfIts.DisableControls;
      //
      Codigo := UMyMod.BuscaEmLivreY_Def('perfis', 'Codigo', stIns, 0);
      Nome   := Copy(QrPerfDescricao.Value, 1, 40) + ' ' + CO_COPIA;
      //
      if MyObjects.FIC(Codigo = 0, nil, 'Falha ao definir C�digo para a tabela "perfis"!') then Exit;
      //
      if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'perfis', TMeuDB,
        ['Codigo'], [QrPerfCodigo.Value],
        ['Codigo', 'Descricao'], [Codigo, Nome],
        '', False, LaAviso1, LaAviso2) then
      begin
        if QrPerfIts.RecordCount > 0 then
        begin
          QrPerfIts.First;
          while not QrPerfIts.Eof do
          begin
            Janela := QrPerfItsJanela.Value;
            //
            UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'perfits', TMeuDB,
              ['Codigo', 'Janela'], [QrPerfItsCodigo.Value, Janela],
              ['Codigo', 'Janela'], [Codigo, Janela], '',
              False, LaAviso1, LaAviso2);
            //
            QrPerfIts.Next;
          end;
        end;
      end;
    finally
      QrPerf.EnableControls;
      QrPerfIts.EnableControls;
      Va(vpLast);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      Screen.Cursor := crDefault;
      Geral.MB_Aviso('Duplica��o finalizada!');
    end;
  end;
end;

procedure TFmPerfis.EdPesquisaChange(Sender: TObject);
begin
  SubQuery1Reopen(QrPerfItsJanela.Value);
end;

procedure TFmPerfis.EdPesquisaClick(Sender: TObject);
begin
  EdPesquisa.ValueVariant := '';
end;

procedure TFmPerfis.GradeDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if (QrPerfIts.State <> dsInactive) and (QrPerfIts.RecordCount > 0) then
  begin
    if (QrPerfItsModulo.Value <> '') and ((Pos(LowerCase(QrPerfItsModulo.Value),
      LowerCase(FmPrincipal.QrMasterHabilModulos.Value)) = 0) and
      (LowerCase(TMeuDB) <> 'dcontrol'))
    then
      Cor := clSilver
    else
      Cor := clBlack;
    //
    with Grade.Canvas do
    begin
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmPerfis.BtRefreshClick(Sender: TObject);
begin
  SubQuery1Reopen(QrPerfItsJanela.Value);
end;

end.

