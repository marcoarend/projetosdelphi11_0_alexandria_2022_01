object FmUsuariosNew: TFmUsuariosNew
  Left = 399
  Top = 281
  Caption = 'AD2-USUAR-002 :: Edi'#231#227'o de Usu'#225'rio'
  ClientHeight = 362
  ClientWidth = 529
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 529
    Height = 200
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 12
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label5: TLabel
      Left = 76
      Top = 12
      Width = 29
      Height = 13
      Caption = 'Login:'
    end
    object Label2: TLabel
      Left = 256
      Top = 12
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object Label3: TLabel
      Left = 388
      Top = 12
      Width = 109
      Height = 13
      Caption = 'Confirma'#231#227'o da senha:'
    end
    object Label4: TLabel
      Left = 20
      Top = 54
      Width = 78
      Height = 13
      Caption = 'Perfil do usu'#225'rio:'
    end
    object Label6: TLabel
      Left = 20
      Top = 95
      Width = 58
      Height = 13
      Caption = 'Funcion'#225'rio:'
    end
    object SBFuncionario: TSpeedButton
      Left = 491
      Top = 111
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBFuncionarioClick
    end
    object Label7: TLabel
      Left = 20
      Top = 138
      Width = 49
      Height = 13
      Caption = 'IP padr'#227'o:'
    end
    object SBPerfil: TSpeedButton
      Left = 491
      Top = 70
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBPerfilClick
    end
    object EdNumero: TdmkEdit
      Left = 20
      Top = 28
      Width = 51
      Height = 21
      Hint = 'Identifica'#231#227'o do usu'#225'rio'
      TabStop = False
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdLogin: TEdit
      Left = 76
      Top = 28
      Width = 173
      Height = 21
      TabOrder = 1
    end
    object EdConfSenha: TEdit
      Left = 388
      Top = 28
      Width = 124
      Height = 21
      PasswordChar = '*'
      TabOrder = 3
    end
    object EdSenha: TEdit
      Left = 256
      Top = 28
      Width = 124
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
    object CkAtivo: TCheckBox
      Left = 195
      Top = 156
      Width = 53
      Height = 17
      Caption = 'Ativo.'
      Checked = True
      State = cbChecked
      TabOrder = 9
    end
    object EdIP_Default: TEdit
      Left = 20
      Top = 154
      Width = 169
      Height = 21
      TabOrder = 8
    end
    object EdFuncionario: TdmkEditCB
      Left = 20
      Top = 111
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBFuncionario
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBFuncionario: TdmkDBLookupComboBox
      Left = 76
      Top = 111
      Width = 410
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsFuncionarios
      TabOrder = 7
      dmkEditCB = EdFuncionario
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdPerfil: TdmkEditCB
      Left = 20
      Top = 70
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPerfil
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPerfil: TdmkDBLookupComboBox
      Left = 76
      Top = 70
      Width = 410
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Descricao'
      ListSource = DsPerfis
      TabOrder = 5
      dmkEditCB = EdPerfil
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 529
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 481
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 433
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 222
        Height = 32
        Caption = 'Edi'#231#227'o de Usu'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 222
        Height = 32
        Caption = 'Edi'#231#227'o de Usu'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 222
        Height = 32
        Caption = 'Edi'#231#227'o de Usu'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 248
    Width = 529
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 525
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 292
    Width = 529
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 383
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 381
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrPerfis: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao FROM perfis'
      'ORDER BY Descricao')
    Left = 272
    Top = 192
    object QrPerfisCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DNRECOVERY.perfis.Codigo'
    end
    object QrPerfisDescricao: TWideStringField
      DisplayWidth = 100
      FieldName = 'Descricao'
      Origin = 'DNRECOVERY.perfis.Descricao'
      Size = 100
    end
  end
  object DsPerfis: TDataSource
    DataSet = QrPerfis
    Left = 300
    Top = 192
  end
  object QrFuncionarios: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM entidades '
      'WHERE Cliente2="V"'
      'AND Nome<>""'
      'ORDER BY Nome')
    Left = 344
    Top = 192
    object QrFuncionariosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFuncionariosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsFuncionarios: TDataSource
    DataSet = QrFuncionarios
    Left = 372
    Top = 192
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 432
    Top = 192
  end
end
