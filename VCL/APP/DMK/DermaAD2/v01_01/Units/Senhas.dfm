object FmSenhas: TFmSenhas
  Left = 359
  Top = 341
  Caption = ' Senhas Espec'#237'ficas'
  ClientHeight = 193
  ClientWidth = 445
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 445
    Height = 97
    Align = alClient
    TabOrder = 0
    object Label2: TLabel
      Left = 12
      Top = 8
      Width = 39
      Height = 13
      Caption = 'Usu'#225'rio:'
    end
    object Label1: TLabel
      Left = 12
      Top = 52
      Width = 43
      Height = 13
      Caption = 'Senha 1:'
    end
    object Label3: TLabel
      Left = 156
      Top = 52
      Width = 43
      Height = 13
      Caption = 'Senha 2:'
    end
    object Label4: TLabel
      Left = 300
      Top = 52
      Width = 43
      Height = 13
      Caption = 'Senha 3:'
    end
    object EdUsuario: TdmkEditCB
      Left = 12
      Top = 24
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBUsuario
    end
    object CBUsuario: TdmkDBLookupComboBox
      Left = 80
      Top = 24
      Width = 361
      Height = 21
      KeyField = 'Numero'
      ListField = 'NOMEENTIDADE'
      TabOrder = 1
      dmkEditCB = EdUsuario
      UpdType = utYes
    end
    object EdSenha1: TdmkEdit
      Left = 12
      Top = 68
      Width = 141
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdSenha2: TdmkEdit
      Left = 156
      Top = 68
      Width = 141
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdSenha3: TdmkEdit
      Left = 300
      Top = 68
      Width = 141
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 145
    Width = 445
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Left = 12
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      NumGlyphs = 2
    end
    object BtDesiste: TBitBtn
      Left = 349
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 445
    Height = 48
    Align = alTop
    Caption = ' Senhas Espec'#237'ficas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 443
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 449
      ExplicitHeight = 44
    end
  end
end
