unit DermaAD_MLA;
{2548}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Menus, Mask, DBCtrls, Db, (*DBTables,*) ZCF2, mySQLDbTables,
  UnInternalConsts, UnMsgInt, UnMLAGeral, UnInternalConsts2, dmkGeral,
  (* de SkinForm *)
  IniFiles,
  {$IFDEF GIFSUPPORT}
  GifImage,
  {$ENDIF GIFSUPPORT}
  Jpeg;
  (* fim de SkinForm*)

type
  TFmDermaAD_MLA = class(TForm)
    PopupMenu1: TPopupMenu;
    Close1: TMenuItem;
    EdSenha: TEdit;
    EdLogin: TEdit;
    Image1: TImage;
    QrBoss: TmySQLQuery;
    QrBossMasSenha: TWideStringField;
    QrBossMasLogin: TWideStringField;
    QrBossEm: TWideStringField;
    QrBossCNPJ: TWideStringField;
    QrSenhas: TmySQLQuery;
    QrSenhasLogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasSenha: TWideStringField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasFuncionario: TIntegerField;
    QrSenhasDataSenha: TDateField;
    QrSenhasSenhaDia: TWideStringField;
    LaConexao: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Close1Click(Sender: TObject);
    procedure EdSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLoginKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    function Privilegios(Usuario : Integer) : Boolean;
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormHide(Sender: TObject);
    procedure LaConexaoClick(Sender: TObject);
    procedure LaConexaoMouseEnter(Sender: TObject);
    procedure LaConexaoMouseLeave(Sender: TObject);
  private
    { Private declarations }
    procedure ControleMudou(Sender: TObject);
    procedure DefineCores;

  public
    { Public declarations }
    function VerificaDuplicado(Tabela, CampoDescri, CampoCod, Descricao :
             String; Codigo : Integer) : Boolean;

    procedure PrivilegioTotal;

  end;

var
  FmDermaAD_MLA: TFmDermaAD_MLA;
  SkinConta : Integer;

implementation

uses UnMyObjects, Principal, Module, UnGOTOy, VerificaConexoes;

{$R *.DFM}

function TFmDermaAD_MLA.VerificaDuplicado(Tabela, CampoDescri, CampoCod, Descricao :
 String; Codigo : Integer) : Boolean;
var
  Duplicado : Boolean;
begin
  Duplicado := False;
  Dmod.QrDuplic.Close;
  Dmod.QrDuplic.SQL.Clear;
  Dmod.QrDuplic.SQL.Add('SELECT '+CampoDescri+' NOME, '+CampoCod+
  ' CODIGO FROM '+tabela+'');
  Dmod.QrDuplic.SQL.Add('WHERE '+CampoDescri+'=:Nome');
  Dmod.QrDuplic.Params[0].AsString := Descricao;
  Dmod.QrDuplic.Open;
  if Dmod.QrDuplic.RecordCount > 1 then Duplicado := True;
  if (Dmod.QrDuplic.RecordCount = 1) and
     (Dmod.QrDuplicCODIGO.Value <> Codigo) then Duplicado := True;
  if Duplicado then
    Geral.MensagemBox(VAR_MSG_ITEMDUPLICADO, FIN_MSG_WARNING, MB_OK+MB_ICONWARNING);
  Result := Duplicado;
  Dmod.QrDuplic.Close;
end;

////////////////////////FIM CONTROLE GERAL BD \\\\\\\\\\\\\\\\\\\\\\\\\


procedure TFmDermaAD_MLA.ControleMudou(Sender: TObject);
begin
  MyObjects.ControleCor(FmDermaAD_MLA);
end;

function TFmDermaAD_MLA.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
end;

procedure TFmDermaAD_MLA.PrivilegioTotal;
begin
//
end;

procedure TFmDermaAD_MLA.FormCreate(Sender: TObject);
var
  WinPath : Array[0..144] of Char;
begin
  IC2_NOMEFORMAPP := Name;
  Screen.OnActiveControlChange := ControleMudou;
  DefineCores;
  VAR_CAMINHOTXTBMP := 'C:\Dermatek\Skins\VCLSkin\mxskin33.skn';
  GetWindowsDirectory(WinPath,144);
  VAR_LIVRES := 'Livres';
  IC2_COMPANT := nil;

  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0, HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2,3,4,5]) then
  begin
    Geral.WriteAppKey('Server', Application.Title, 1,ktInteger, HKEY_LOCAL_MACHINE);
    VAR_SERVIDOR := 1;
  end;
  VAR_BD := 0;
end;

procedure TFmDermaAD_MLA.Close1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmDermaAD_MLA.EdSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
     GOTOy.AnaliseSenha(EdLogin, EdSenha, FmDermaAD_MLA,
     FmPrincipal, EdSenha, False, nil, nil, nil);
  if (Key = VK_ESCAPE) then Close;
end;

procedure TFmDermaAD_MLA.EdLoginKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
     GOTOy.AnaliseSenha(EdLogin, EdSenha, FmDermaAD_MLA,
     FmPrincipal, EdLogin, False, nil, nil, nil);

  if (Key = VK_ESCAPE) then Close;
end;

procedure TFmDermaAD_MLA.Timer1Timer(Sender: TObject);
begin
  EdSenha.Text := EdSenha.Text + '*';
  Refresh;
end;

procedure TFmDermaAD_MLA.DefineCores;
//var
  //Aparencia: Integer;
begin
  IC2_AparenciasTituloPainelItem   := clBtnFace;
  IC2_AparenciasTituloPainelTexto  := clWindowText;
  IC2_AparenciasDadosPainel        := clBtnFace;
  IC2_AparenciasDadosTexto         := clWindowText;
  IC2_AparenciasGradeAItem         := clWindow;
  IC2_AparenciasGradeATitulo       := clWindowText;
  IC2_AparenciasGradeATexto        := clWindowText;
  IC2_AparenciasGradeAGrade        := clBtnFace;
  IC2_AparenciasGradeBItem         := clWindow;
  IC2_AparenciasGradeBTitulo       := clWindowText;
  IC2_AparenciasGradeBTexto        := clWindowText;
  IC2_AparenciasGradeBGrade        := clBtnFace;
  IC2_AparenciasEditAItem          := clWindow;
  IC2_AparenciasEditATexto         := clWindowText;
  IC2_AparenciasEditBItem          := clBtnFace;
  IC2_AparenciasEditBTexto         := clWindowText;
  IC2_AparenciasConfirmaPainelItem := clBtnFace;
  IC2_AparenciasControlePainelItem := clBtnFace;
  IC2_AparenciasFormPesquisa       := clBtnFace;
  IC2_AparenciasLabelPesquisa      := clWindowText;
  IC2_AparenciasLabelDigite        := clWindowText;
  IC2_AparenciasEditPesquisaItem   := clWindow;
  IC2_AparenciasEditPesquisaTexto  := clWindowText;
  IC2_AparenciasEditItemIn         := $00FF8000;
  IC2_AparenciasEditItemOut        := clWindow;
  IC2_AparenciasEditTextIn         := $00D5FAFF;
  IC2_AparenciasEditTextOut        := clWindowText;
(*  if Geral.ReadAppKey('Versao', Application.Title, ktInteger, 0, HKEY_LOCAL_MACHINE) <> 0 then
  begin
    Aparencia := MLAGeral.Inteiro0paraVazio(Geral.ReadAppKey('Aparencia',
    Application.Title, ktString, '-1000', HKEY_LOCAL_MACHINE));
    if Aparencia = -1000 then
       Geral.WriteAppKey('Aparencia', Application.Title, '0', ktString,
    HKEY_LOCAL_MACHINE);
    QrAparencias.Close;
    QrAparencias.Params[0].AsInteger := Aparencia;
    QrAparencias.Open;
    if GOTOy.Registros(QrAparencias)>0 then
    begin
      IC2_AparenciasTituloPainelItem := QrAparenciasTituloPainelItem.Value;
      IC2_AparenciasTituloPainelTexto := QrAparenciasTituloPainelTexto.Value;
      IC2_AparenciasDadosPainel := QrAparenciasDadosPainel.Value;
      IC2_AparenciasDadosTexto := QrAparenciasDadosTexto.Value;
      IC2_AparenciasGradeAItem := QrAparenciasGradeAItem.Value;
      IC2_AparenciasGradeATitulo := QrAparenciasGradeATitulo.Value;
      IC2_AparenciasGradeATexto := QrAparenciasGradeATexto.Value;
      IC2_AparenciasGradeAGrade := QrAparenciasGradeAGrade.Value;
      IC2_AparenciasGradeBItem := QrAparenciasGradeBItem.Value;
      IC2_AparenciasGradeBTitulo := QrAparenciasGradeBTitulo.Value;
      IC2_AparenciasGradeBTexto := QrAparenciasGradeBTexto.Value;
      IC2_AparenciasGradeBGrade := QrAparenciasGradeBGrade.Value;
      IC2_AparenciasEditAItem := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditATexto := QrAparenciasEditATexto.Value;
      IC2_AparenciasEditBItem := QrAparenciasEditBItem.Value;
      IC2_AparenciasEditBTexto := QrAparenciasEditBTexto.Value;
      IC2_AparenciasConfirmaPainelItem := QrAparenciasConfirmaPainelItem.Value;
      IC2_AparenciasControlePainelItem := QrAparenciasControlePainelItem.Value;
      IC2_AparenciasFormPesquisa := QrAparenciasFormPesquisa.Value;
      IC2_AparenciasLabelPesquisa := QrAparenciasLabelPesquisa.Value;
      IC2_AparenciasLabelDigite := QrAparenciasLabelDigite.Value;
      IC2_AparenciasEditPesquisaItem := QrAparenciasEditPesquisaItem.Value;
      IC2_AparenciasEditPesquisaTexto := QrAparenciasEditPesquisaTexto.Value;
      IC2_AparenciasEditItemIn         := $00D9FFFF;
      IC2_AparenciasEditItemOut        := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditTextIn         := $00A00000;
      IC2_AparenciasEditTextOut        := QrAparenciasEditATexto.Value;
      IC2_AparenciasEditItemIn         := QrAparenciasEditATexto.Value;
      IC2_AparenciasEditItemOut        := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditTextIn         := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditTextOut        := QrAparenciasEditATexto.Value;
    QrAparencias.Close;
  end;*)
end;

procedure TFmDermaAD_MLA.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TFmDermaAD_MLA.FormHide(Sender: TObject);
begin
  if VAR_TERMINATE = False then
  begin
    FmPrincipal.StatusBar1.Panels[01].Text := EdLogin.Text;
    FmPrincipal.AcoesIniciaisDoAplicativo();
  end;
end;

procedure TFmDermaAD_MLA.LaConexaoClick(Sender: TObject);
begin
  Application.CreateForm(TFmVerificaConexoes,  FmVerificaConexoes);
  FmVerificaConexoes.ShowModal;
  FmVerificaConexoes.Destroy;
end;

procedure TFmDermaAD_MLA.LaConexaoMouseEnter(Sender: TObject);
begin
  LaConexao.Font.Color := clBlue;
  LaConexao.Font.Style := [fsUnderline];
end;

procedure TFmDermaAD_MLA.LaConexaoMouseLeave(Sender: TObject);
begin
  LaConexao.Font.Color := clBlue;
  LaConexao.Font.Style := [];
end;

end.

