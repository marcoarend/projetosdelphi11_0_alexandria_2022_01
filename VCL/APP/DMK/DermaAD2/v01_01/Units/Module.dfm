object Dmod: TDmod
  OnCreate = DataModuleCreate
  Height = 561
  Width = 803
  PixelsPerInch = 96
  object QrUpdL: TMySQLQuery
    Database = MyLocDatabase
    Left = 16
    Top = 24
  end
  object QrUpdM: TMySQLQuery
    Database = MyDB
    Left = 536
    Top = 56
  end
  object QrUser: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Login FROM senhas'
      'WHERE Numero=:Usuario')
    Left = 152
    Top = 224
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Usuario'
        ParamType = ptUnknown
      end>
    object QrUserLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'DBMBWET.senhas.Login'
      Size = 128
    end
  end
  object QrLocaliza: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MIN(Codigo) Record'
      'FROM pq'
      'WHERE Codigo > 0'
      '')
    Left = 152
    Top = 272
    object QrLocalizaRecord: TIntegerField
      FieldName = 'Record'
      Origin = 'DBMBWET.pq.Codigo'
    end
  end
  object QrFields: TMySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SHOW FIELDS FROM :p0')
    Left = 20
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrUpdX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SHOW FIELDS FROM :p0')
    Left = 452
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrDataBalX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MAX(Data) Data FROM pesagem'
      'WHERE Tipo=4')
    Left = 424
    Top = 400
    object QrDataBalXData: TDateField
      FieldName = 'Data'
      Origin = 'DBMBWET.pesagem.Data'
    end
  end
  object QrRecCountX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Record From Cores')
    Left = 453
    Top = 354
    object QrRecCountXRecord: TIntegerField
      FieldName = 'Record'
    end
  end
  object QrLocDataX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MIN(Data) Record FROM faturas'
      '')
    Left = 424
    Top = 356
    object QrLocDataXRecord: TDateField
      FieldName = 'Record'
      Origin = 'DBMMONEY.faturas.Data'
    end
  end
  object QrDelLogX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'DELETE FROM logs'
      'WHERE Tipo=:P0'
      'AND Usuario=:P1'
      'AND ID=:P2')
    Left = 424
    Top = 308
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrInsLogX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'INSERT INTO logs SET'
      'Data=NOW(),'
      'Tipo=:P0,'
      'Usuario=:P1,'
      'ID=:P2')
    Left = 452
    Top = 308
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrCountX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  ArtigosGrupos')
    Left = 452
    Top = 264
    object QrCountXRecord: TIntegerField
      FieldName = 'Record'
    end
  end
  object QrSelX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Lk FROM lanctos')
    Left = 424
    Top = 264
    object QrSelXLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.lanctos.Lk'
    end
  end
  object Query1: TMySQLQuery
    Database = MyDB
    Left = 424
    Top = 220
  end
  object QrLocX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MIN(Codigo) Record FROM carteiras'
      '')
    Left = 452
    Top = 220
    object QrLocXRecord: TIntegerField
      FieldName = 'Record'
      Origin = 'DBMMONEY.carteiras.Codigo'
    end
  end
  object QrDuplicStrX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Nome NOME, Codigo CODIGO, IDCodigo ANTERIOR'
      'FROM pq'
      'WHERE Nome=:Nome')
    Left = 452
    Top = 176
    ParamData = <
      item
        DataType = ftString
        Name = 'Nome'
        ParamType = ptUnknown
      end>
    object QrDuplicStrXNOME: TWideStringField
      FieldName = 'NOME'
      Origin = 'DBMBWET.pq.Nome'
      Size = 128
    end
    object QrDuplicStrXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrDuplicStrXANTERIOR: TIntegerField
      FieldName = 'ANTERIOR'
      Origin = 'DBMBWET.pq.IDCodigo'
    end
  end
  object QrDuplicIntX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT NF INTEIRO1, Cliente INTEIRO2, Codigo CODIGO'
      'FROM pqpse'
      'WHERE NF=1'
      'AND Cliente=1')
    Left = 424
    Top = 176
    object QrDuplicIntXINTEIRO1: TIntegerField
      FieldName = 'INTEIRO1'
      Origin = 'DBMBWET.pqpse.NF'
    end
    object QrDuplicIntXINTEIRO2: TIntegerField
      FieldName = 'INTEIRO2'
      Origin = 'DBMBWET.pqpse.Cliente'
    end
    object QrDuplicIntXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pqpse.Codigo'
    end
  end
  object QrLivreX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Codigo FROM livresl'
      '')
    Left = 424
    Top = 132
    object QrLivreXCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMLOCAL.livresl.Codigo'
    end
  end
  object QrLivreX_D: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Controle Codigo FROM lanctos')
    Left = 452
    Top = 132
    object QrLivreX_DCodigo: TFloatField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.lanctos.Controle'
    end
  end
  object QrUpdU: TMySQLQuery
    Database = MyDB
    Left = 300
    Top = 96
  end
  object QrSenha: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM senhas'
      'WHERE Login=:P0'
      'AND Senha=:P1')
    Left = 152
    Top = 316
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhaLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'DBMBWET.senhas.Login'
      Size = 128
    end
    object QrSenhaNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'DBMBWET.senhas.Numero'
    end
    object QrSenhaSenha: TWideStringField
      FieldName = 'Senha'
      Origin = 'DBMBWET.senhas.Senha'
      Size = 128
    end
    object QrSenhaPerfilF: TIntegerField
      FieldName = 'PerfilF'
      Origin = 'DBMBWET.senhas.PerfilF'
    end
    object QrSenhaLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMBWET.senhas.Lk'
    end
    object QrSenhaPerfilW: TIntegerField
      FieldName = 'PerfilW'
      Origin = 'DBMBWET.senhas.PerfilW'
    end
  end
  object QrDuplic: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Nome NOME, Codigo CODIGO FROM pq'
      'WHERE Nome=:Nome')
    Left = 152
    Top = 88
    ParamData = <
      item
        DataType = ftString
        Name = 'Nome'
        ParamType = ptUnknown
      end>
    object QrDuplicNOME: TWideStringField
      FieldName = 'NOME'
      Origin = 'DBMBWET.pq.Nome'
      Size = 128
    end
    object QrDuplicCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pq.Codigo'
    end
  end
  object QrUpdW: TMySQLQuery
    Database = MyDB
    Left = 300
    Top = 48
  end
  object MyLocDatabase: TMySQLDatabase
    Port = 3308
    Host = '127.0.0.1'
    ConnectOptions = []
    Params.Strings = (
      'Port=3308'
      'TIMEOUT=30'
      'Host=127.0.0.1')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 120
    Top = 16
  end
  object MyDB: TMySQLDatabase
    Port = 3308
    ConnectOptions = []
    Params.Strings = (
      'Port=3308'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 60
    Top = 464
  end
  object QrAux: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SHOW FIELDS FROM :p0')
    Left = 152
    Top = 180
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrPriorNext: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT NOW() Agora')
    Left = 152
    Top = 404
    object StringField1: TWideStringField
      FieldName = 'Agora'
      Size = 19
    end
  end
  object QrMaster2: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM master')
    Left = 20
    Top = 191
    object QrMaster2SolicitaSenha: TSmallintField
      FieldName = 'SolicitaSenha'
    end
    object QrMaster2Em: TWideStringField
      FieldName = 'Em'
      Size = 100
    end
    object QrMaster2CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 30
    end
    object QrMaster2Monitorar: TSmallintField
      FieldName = 'Monitorar'
    end
    object QrMaster2Licenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
    object QrMaster2Distorcao: TIntegerField
      FieldName = 'Distorcao'
    end
    object QrMaster2DataI: TDateField
      FieldName = 'DataI'
    end
    object QrMaster2DataF: TDateField
      FieldName = 'DataF'
    end
    object QrMaster2Hoje: TDateField
      FieldName = 'Hoje'
    end
    object QrMaster2Hora: TTimeField
      FieldName = 'Hora'
    end
    object QrMaster2MasLogin: TWideStringField
      FieldName = 'MasLogin'
      Size = 30
    end
    object QrMaster2MasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrMaster2MasAtivar: TWideStringField
      FieldName = 'MasAtivar'
      Size = 1
    end
    object QrMaster2Limite: TSmallintField
      FieldName = 'Limite'
    end
    object QrMaster2SitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrUpd: TMySQLQuery
    Database = MyDB
    Left = 300
    Top = 144
  end
  object QrControle: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Dono, Moeda, CorRecibo, ContraSenha, MoraDD, Multa,'
      'MensalSempre, EntraSemValor, IdleMinutos, MyPagCar, MyPgQtdP,'
      'MyPgPeri, MyPgDias, VendaCartPg, VendaParcPg, VendaPeriPg,'
      'VendaDiasPg, CNABCtaJur, CNABCtaMul, CNABCtaTar,'
      'MeuLogoPath, LogoNF, LogoBig1, MoedaBr'
      'FROM controle')
    Left = 20
    Top = 240
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
    end
    object QrControleMoeda: TWideStringField
      FieldName = 'Moeda'
      Size = 4
    end
    object QrControleCorRecibo: TIntegerField
      FieldName = 'CorRecibo'
    end
    object QrControleContraSenha: TWideStringField
      FieldName = 'ContraSenha'
      Size = 50
    end
    object QrControleMoraDD: TFloatField
      FieldName = 'MoraDD'
    end
    object QrControleMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrControleMensalSempre: TSmallintField
      FieldName = 'MensalSempre'
    end
    object QrControleEntraSemValor: TSmallintField
      FieldName = 'EntraSemValor'
    end
    object QrControleIdleMinutos: TIntegerField
      FieldName = 'IdleMinutos'
    end
    object QrControleMyPagCar: TSmallintField
      FieldName = 'MyPagCar'
    end
    object QrControleMyPgQtdP: TSmallintField
      FieldName = 'MyPgQtdP'
    end
    object QrControleMyPgPeri: TSmallintField
      FieldName = 'MyPgPeri'
    end
    object QrControleMyPgDias: TSmallintField
      FieldName = 'MyPgDias'
    end
    object QrControleVendaCartPg: TIntegerField
      FieldName = 'VendaCartPg'
    end
    object QrControleVendaParcPg: TIntegerField
      FieldName = 'VendaParcPg'
    end
    object QrControleVendaPeriPg: TIntegerField
      FieldName = 'VendaPeriPg'
    end
    object QrControleVendaDiasPg: TIntegerField
      FieldName = 'VendaDiasPg'
    end
    object QrControleCNABCtaJur: TIntegerField
      FieldName = 'CNABCtaJur'
    end
    object QrControleCNABCtaMul: TIntegerField
      FieldName = 'CNABCtaMul'
    end
    object QrControleCNABCtaTar: TIntegerField
      FieldName = 'CNABCtaTar'
    end
    object QrControleMeuLogoPath: TWideStringField
      FieldName = 'MeuLogoPath'
      Size = 255
    end
    object QrControleLogoNF: TWideStringField
      FieldName = 'LogoNF'
      Size = 255
    end
    object QrControleLogoBig1: TWideStringField
      FieldName = 'LogoBig1'
      Size = 255
    end
    object QrControleMoedaBr: TIntegerField
      FieldName = 'MoedaBr'
    end
  end
  object ZZDB: TMySQLDatabase
    Port = 3308
    Host = '127.0.0.1'
    ConnectOptions = []
    Params.Strings = (
      'Port=3308'
      'TIMEOUT=30'
      'Host=127.0.0.1')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 184
    Top = 16
  end
  object QrTerminal: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM terminais'
      'WHERE IP=:P0')
    Left = 208
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrPerfis: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pip.Libera, pit.Janela '
      'FROM perfisits pit'
      
        'LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela AND pip.Cod' +
        'igo=:P0')
    Left = 304
    Top = 252
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfisLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfisJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 100
    end
  end
  object QrAgora: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 152
    Top = 359
    object StringField2: TWideStringField
      FieldName = 'Agora'
      Required = True
      Size = 19
    end
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
  end
  object QrTerceiro: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT ufp.Nome NOMEpUF, ufe.Nome NOMEeUF, en.* '
      'FROM entidades en'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'WHERE en.Codigo=:P0')
    Left = 220
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerceiroNOMEpUF: TWideStringField
      FieldName = 'NOMEpUF'
      Required = True
      Size = 2
    end
    object QrTerceiroNOMEeUF: TWideStringField
      FieldName = 'NOMEeUF'
      Required = True
      Size = 2
    end
    object QrTerceiroCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTerceiroRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrTerceiroFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrTerceiroPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrTerceiroMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrTerceiroCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTerceiroIE: TWideStringField
      FieldName = 'IE'
    end
    object QrTerceiroIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrTerceiroNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrTerceiroApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrTerceiroCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTerceiroRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrTerceiroELograd: TSmallintField
      FieldName = 'ELograd'
      Required = True
    end
    object QrTerceiroERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrTerceiroENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrTerceiroECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrTerceiroEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrTerceiroECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 25
    end
    object QrTerceiroEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrTerceiroECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrTerceiroEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrTerceiroETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrTerceiroEte2: TWideStringField
      FieldName = 'Ete2'
    end
    object QrTerceiroEte3: TWideStringField
      FieldName = 'Ete3'
    end
    object QrTerceiroECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrTerceiroEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrTerceiroEEmail: TWideStringField
      FieldName = 'EEmail'
      Size = 100
    end
    object QrTerceiroEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrTerceiroENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrTerceiroPLograd: TSmallintField
      FieldName = 'PLograd'
      Required = True
    end
    object QrTerceiroPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrTerceiroPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrTerceiroPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrTerceiroPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrTerceiroPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrTerceiroPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrTerceiroPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrTerceiroPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrTerceiroPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrTerceiroPte2: TWideStringField
      FieldName = 'Pte2'
    end
    object QrTerceiroPte3: TWideStringField
      FieldName = 'Pte3'
    end
    object QrTerceiroPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrTerceiroPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrTerceiroPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrTerceiroPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrTerceiroPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrTerceiroSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrTerceiroResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrTerceiroProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrTerceiroCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrTerceiroRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrTerceiroDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrTerceiroAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrTerceiroAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrTerceiroCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrTerceiroCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrTerceiroFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrTerceiroFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrTerceiroFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrTerceiroFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrTerceiroTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrTerceiroCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrTerceiroInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrTerceiroLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrTerceiroVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrTerceiroMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrTerceiroObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTerceiroTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTerceiroCLograd: TSmallintField
      FieldName = 'CLograd'
      Required = True
    end
    object QrTerceiroCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrTerceiroCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrTerceiroCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrTerceiroCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrTerceiroCCidade: TWideStringField
      FieldName = 'CCidade'
      Size = 25
    end
    object QrTerceiroCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrTerceiroCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrTerceiroCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrTerceiroCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrTerceiroCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrTerceiroCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrTerceiroCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrTerceiroLLograd: TSmallintField
      FieldName = 'LLograd'
      Required = True
    end
    object QrTerceiroLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrTerceiroLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrTerceiroLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrTerceiroLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrTerceiroLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrTerceiroLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrTerceiroLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrTerceiroLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrTerceiroLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrTerceiroLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrTerceiroLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrTerceiroLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrTerceiroComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrTerceiroSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrTerceiroNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrTerceiroGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrTerceiroAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrTerceiroLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrTerceiroConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrTerceiroConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
    end
    object QrTerceiroNome1: TWideStringField
      FieldName = 'Nome1'
      Size = 30
    end
    object QrTerceiroNatal1: TDateField
      FieldName = 'Natal1'
    end
    object QrTerceiroNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 30
    end
    object QrTerceiroNatal2: TDateField
      FieldName = 'Natal2'
    end
    object QrTerceiroNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 30
    end
    object QrTerceiroNatal3: TDateField
      FieldName = 'Natal3'
    end
    object QrTerceiroNome4: TWideStringField
      FieldName = 'Nome4'
      Size = 30
    end
    object QrTerceiroNatal4: TDateField
      FieldName = 'Natal4'
    end
    object QrTerceiroCreditosI: TIntegerField
      FieldName = 'CreditosI'
    end
    object QrTerceiroCreditosL: TIntegerField
      FieldName = 'CreditosL'
    end
    object QrTerceiroCreditosF2: TFloatField
      FieldName = 'CreditosF2'
    end
    object QrTerceiroCreditosD: TDateField
      FieldName = 'CreditosD'
    end
    object QrTerceiroCreditosU: TDateField
      FieldName = 'CreditosU'
    end
    object QrTerceiroCreditosV: TDateField
      FieldName = 'CreditosV'
    end
    object QrTerceiroMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrTerceiroQuantI1: TIntegerField
      FieldName = 'QuantI1'
    end
    object QrTerceiroQuantI2: TIntegerField
      FieldName = 'QuantI2'
    end
    object QrTerceiroQuantI3: TIntegerField
      FieldName = 'QuantI3'
    end
    object QrTerceiroQuantI4: TIntegerField
      FieldName = 'QuantI4'
    end
    object QrTerceiroQuantN1: TFloatField
      FieldName = 'QuantN1'
    end
    object QrTerceiroQuantN2: TFloatField
      FieldName = 'QuantN2'
    end
    object QrTerceiroAgenda: TWideStringField
      FieldName = 'Agenda'
      Required = True
      Size = 1
    end
    object QrTerceiroSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Required = True
      Size = 1
    end
    object QrTerceiroSenha1: TWideStringField
      FieldName = 'Senha1'
      Size = 6
    end
    object QrTerceiroLimiCred: TFloatField
      FieldName = 'LimiCred'
      Required = True
    end
    object QrTerceiroDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrTerceiroCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Required = True
    end
    object QrTerceiroTempD: TFloatField
      FieldName = 'TempD'
    end
    object QrTerceiroLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTerceiroDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTerceiroDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTerceiroUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTerceiroUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTerceiroCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Size = 18
    end
    object QrTerceiroSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrTerceiroCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrTerceiroUFNatal: TSmallintField
      FieldName = 'UFNatal'
      Required = True
    end
  end
  object QrMas: TMySQLQuery
    Database = MyDB
    Left = 228
    Top = 332
  end
  object QrTerminais: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM terminais')
    Left = 304
    Top = 300
    object QrTerminaisIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminaisTerminal: TIntegerField
      FieldName = 'Terminal'
    end
    object QrTerminaisLicenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
  end
  object QrEndereco: TMySQLQuery
    Database = MyDB
    OnCalcFields = QrEnderecoCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDON' +
        'O, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero END NUME' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Logr' +
        'ad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 12
    Top = 286
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnderecoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEnderecoCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEnderecoNOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrEnderecoCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEnderecoIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEnderecoNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrEnderecoRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEnderecoNUMERO: TLargeintField
      FieldName = 'NUMERO'
    end
    object QrEnderecoCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEnderecoBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEnderecoCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEnderecoNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEnderecoNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEnderecoPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrEnderecoLograd: TLargeintField
      FieldName = 'Lograd'
    end
    object QrEnderecoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEnderecoCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrEnderecoTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEnderecoFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEnderecoENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEnderecoPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrEnderecoECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrEnderecoNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrEnderecoE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrEnderecoCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEnderecoFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEnderecoTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEnderecoNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEnderecoRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrEnderecoNO_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO_DOC'
      Calculated = True
    end
  end
  object QlLocal: TMySQLBatchExecute
    Action = baContinue
    Database = MyLocDatabase
    Delimiter = ';'
    Left = 368
    Top = 4
  end
  object QrSSit: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM senhas'
      'WHERE login=:P0'
      '')
    Left = 372
    Top = 424
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrBSit: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM master')
    Left = 316
    Top = 424
    object QrBSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrBoss: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MasSenha MasZero, '
      'AES_DECRYPT(MasSenha, :P0) MasSenha, MasLogin, Em, CNPJ'
      'FROM master')
    Left = 288
    Top = 424
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBossMasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrBossMasLogin: TWideStringField
      FieldName = 'MasLogin'
      Required = True
      Size = 30
    end
    object QrBossEm: TWideStringField
      FieldName = 'Em'
      Size = 100
    end
    object QrBossCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 30
    end
    object QrBossMasZero: TWideStringField
      FieldName = 'MasZero'
      Size = 30
    end
  end
  object QrSenhas: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT AES_DECRYPT(se.Senha, :P0) SenhaNew, se.Senha SenhaOld, '
      'se.login, se.Numero, se.Perfil, se.Funcionario, se.IP_Default'
      'FROM senhas se'
      'WHERE se.Login=:P1')
    Left = 344
    Top = 424
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhasSenhaNew: TWideStringField
      FieldName = 'SenhaNew'
      Size = 30
    end
    object QrSenhasSenhaOld: TWideStringField
      FieldName = 'SenhaOld'
      Size = 30
    end
    object QrSenhaslogin: TWideStringField
      FieldName = 'login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
    end
    object QrSenhasFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
    object QrSenhasIP_Default: TWideStringField
      FieldName = 'IP_Default'
      Size = 50
    end
  end
  object QrAuxL: TMySQLQuery
    Database = MyLocDatabase
    Left = 16
    Top = 68
  end
  object QrUpdZ: TMySQLQuery
    Database = MyDB
    Left = 300
    Top = 192
  end
  object QrLocY: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT MIN(Codigo) Record FROM carteiras'
      '')
    Left = 512
    Top = 135
    object QrLocYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrCountY: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  Entidades')
    Left = 512
    Top = 179
    object QrCountYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrErrQuit: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT lan.Controle Controle1, la2.Controle Controle2, '
      'la2.Data, lan.Sit, lan.Descricao'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN lanctos   la2 ON la2.ID_pgto=lan.Controle'
      ''
      'WHERE lan.Tipo=2'
      'AND lan.Sit=2'
      'AND lan.Credito-lan.Pago = 0'
      'AND lan.Controle<>la2.Controle'
      'AND lan.ID_Quit=0'
      '')
    Left = 540
    Top = 280
    object QrErrQuitControle1: TIntegerField
      FieldName = 'Controle1'
      Required = True
    end
    object QrErrQuitControle2: TIntegerField
      FieldName = 'Controle2'
      Required = True
    end
    object QrErrQuitData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrErrQuitSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrErrQuitDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object QrSQL: TMySQLQuery
    Database = MyDB
    Left = 80
    Top = 140
  end
  object QrIdx: TMySQLQuery
    Database = MyDB
    Left = 16
    Top = 332
  end
  object mySQLQuery1: TMySQLQuery
    Database = MyDB
    Left = 44
    Top = 332
  end
  object mySQLQuery2: TMySQLQuery
    Database = MyDB
    Left = 72
    Top = 332
  end
  object mySQLQuery3: TMySQLQuery
    Database = MyDB
    Left = 72
    Top = 376
  end
  object mySQLQuery4: TMySQLQuery
    Database = MyDB
    Left = 44
    Top = 376
  end
  object mySQLQuery5: TMySQLQuery
    Database = MyDB
    Left = 16
    Top = 376
  end
  object QrNTV: TMySQLQuery
    Database = ZZDB
    Left = 60
    Top = 4
  end
  object QrNTI: TMySQLQuery
    Database = ZZDB
    Left = 60
    Top = 48
  end
  object frxDsMaster: TfrxDBDataset
    UserName = 'frxDsMaster'
    CloseDataSource = False
    DataSet = QrMaster
    BCDToCurrency = False
    DataSetOptions = []
    Left = 188
    Top = 136
  end
  object QrDono: TMySQLQuery
    Database = MyDB
    OnCalcFields = QrDonoCalcFields
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, EContato, E' +
        'Cel,'
      'Respons1, Respons2,  '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDON' +
        'O, '
      
        'CASE WHEN en.Tipo=0 THEN en.Fantasia   ELSE en.Apelido END APELI' +
        'DO, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero+0.000     ELSE en.PNumero+0.' +
        '000 END NUMERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Logr' +
        'ad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe2        ELSE en.PTe2    END TE2,' +
        ' '
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX '
      'FROM controle co'
      'LEFT JOIN entidades en ON en.Codigo=co.Dono'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd')
    Left = 40
    Top = 286
    object QrDonoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDonoCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrDonoNOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrDonoCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrDonoIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrDonoNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrDonoRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrDonoCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrDonoBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrDonoCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrDonoNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrDonoNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrDonoPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrDonoLograd: TLargeintField
      FieldName = 'Lograd'
    end
    object QrDonoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDonoCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrDonoTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrDonoTE2: TWideStringField
      FieldName = 'TE2'
    end
    object QrDonoFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrDonoENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrDonoPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrDonoECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrDonoNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrDonoCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoTE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE2_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoE_LNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LNR'
      Size = 255
      Calculated = True
    end
    object QrDonoE_CUC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_CUC'
      Size = 255
      Calculated = True
    end
    object QrDonoEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrDonoECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrDonoCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoE_LN2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LN2'
      Size = 255
      Calculated = True
    end
    object QrDonoAPELIDO: TWideStringField
      FieldName = 'APELIDO'
      Size = 60
    end
    object QrDonoNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrDonoRespons1: TWideStringField
      FieldName = 'Respons1'
      Size = 60
    end
    object QrDonoRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 60
    end
    object QrDonoFONES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FONES'
      Size = 255
      Calculated = True
    end
  end
  object QrMaster: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT ma.Em, te.Tipo, te.Logo,  te.Logo2,cm.Dono, cm.Versao, '
      'cm.CNPJ, te.IE, te.ECidade, uf.Nome NOMEUF, te.EFax,'
      
        'te.ERua, (te.ENumero+0.000) ENumero, te.EBairro, te.ECEP, te.ECo' +
        'mpl,'
      'te.EContato, te.ECel, te.ETe1, te.ETe2, te.ETe3, te.EPais,'
      'te.Respons1, te.Respons2, ma.Limite, ma.SolicitaSenha,'
      'ma.UsaAccMngr'
      'FROM Entidades te, Controle cm, Ufs uf, Master ma'
      'WHERE te.Codigo=cm.Dono'
      
        'AND ((te.CNPJ=cm.CNPJ AND te.Tipo=0) OR (te.CPF=cm.CNPJ AND te.T' +
        'ipo=1))'
      'AND uf.Codigo=te.EUF'
      '')
    Left = 164
    Top = 135
    object QrMasterCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterEm: TWideStringField
      FieldName = 'Em'
      Origin = 'master.Em'
      Required = True
      Size = 100
    end
    object QrMasterTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Origin = 'controle.Dono'
      Required = True
    end
    object QrMasterVersao: TIntegerField
      FieldName = 'Versao'
      Origin = 'controle.Versao'
      Required = True
    end
    object QrMasterCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'controle.CNPJ'
      Required = True
      Size = 18
    end
    object QrMasterLimite: TSmallintField
      FieldName = 'Limite'
      Origin = 'master.Limite'
      Required = True
    end
    object QrMasterSolicitaSenha: TIntegerField
      FieldName = 'SolicitaSenha'
      Origin = 'master.SolicitaSenha'
    end
    object QrMasterUsaAccMngr: TSmallintField
      FieldName = 'UsaAccMngr'
    end
  end
end
