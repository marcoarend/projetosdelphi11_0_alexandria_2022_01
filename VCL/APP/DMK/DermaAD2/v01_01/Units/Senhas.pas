unit Senhas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DBCtrls, StdCtrls, Buttons, Db, (*DBTables,*) UnMLAGeral, mySQLDbTables,
  ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmSenhas = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Label2: TLabel;
    EdUsuario: TdmkEditCB;
    CBUsuario: TdmkDBLookupComboBox;
    EdSenha1: TdmkEdit;
    Label1: TLabel;
    EdSenha2: TdmkEdit;
    Label3: TLabel;
    EdSenha3: TdmkEdit;
    Label4: TLabel;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  FmSenhas: TFmSenhas;

implementation

uses UnMyObjects, Module, UnInternalConsts, Principal;

{$R *.DFM}

procedure TFmSenhas.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSenhas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSenhas.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

end.
