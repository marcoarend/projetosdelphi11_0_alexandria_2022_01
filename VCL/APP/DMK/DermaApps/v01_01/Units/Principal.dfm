object FmPrincipal: TFmPrincipal
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'DermaApps'
  ClientHeight = 391
  ClientWidth = 777
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 120
  TextHeight = 17
  object Panel1: TPanel
    Left = 0
    Top = 328
    Width = 777
    Height = 63
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 0
    object BtSaida: TBitBtn
      Left = 645
      Top = 5
      Width = 117
      Height = 53
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
    object BtBaixar: TBitBtn
      Left = 10
      Top = 5
      Width = 118
      Height = 53
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Baixar'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtBaixarClick
    end
  end
  object StaticText3: TStaticText
    Left = 0
    Top = 63
    Width = 777
    Height = 24
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Alignment = taCenter
    BorderStyle = sbsSunken
    Caption = 
      'Selecione o(s) aplicativo(s) que voc'#234' ir'#225' utilizar e clique no b' +
      'ot'#227'o Baixar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object LBApps: TListBox
    Left = 0
    Top = 87
    Width = 777
    Height = 241
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Columns = 3
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -22
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 27
    MultiSelect = True
    ParentFont = False
    TabOrder = 2
  end
  object Memo3: TMemo
    Left = 471
    Top = 146
    Width = 242
    Height = 117
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Lines.Strings = (
      'Memo3')
    TabOrder = 3
    Visible = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 777
    Height = 63
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object BtAtualiza: TBitBtn
      Left = 10
      Top = 5
      Width = 118
      Height = 53
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Atualiza'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtAtualizaClick
    end
  end
  object XMLDocument1: TXMLDocument
    Left = 240
    Top = 160
    DOMVendorDesc = 'MSXML'
  end
  object BalloonHint1: TBalloonHint
    Left = 135
    Top = 97
  end
  object TmVersao: TTimer
    Enabled = False
    Interval = 500
    OnTimer = TmVersaoTimer
    Left = 288
    Top = 104
  end
end
