unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.CheckLst, Vcl.Buttons, Xml.xmldom,
  Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc,

  ActiveX, ComObj;

type
  TFmPrincipal = class(TForm)
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtBaixar: TBitBtn;
    StaticText3: TStaticText;
    XMLDocument1: TXMLDocument;
    LBApps: TListBox;
    Memo3: TMemo;
    Panel2: TPanel;
    BtAtualiza: TBitBtn;
    BalloonHint1: TBalloonHint;
    TmVersao: TTimer;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtBaixarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtAtualizaClick(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
  private
    { Private declarations }
    FCodigo, FLink, FCategor: TStringList;
    procedure ReopenAplicativos_REST;
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
  public
    { Public declarations }
  end;

var
  FmPrincipal: TFmPrincipal;
const
  FURL = 'http://www.dermatek.net.br/dmkREST.php';
  FCodApl = 31;
  FCNPJ = '03143014000152';

implementation

uses UnDmkWeb, dmkGeral, UnDmkProcFunc, MyListas;

{$R *.dfm}

procedure TFmPrincipal.ReopenAplicativos_REST;
var
  XML: TStringStream;
  i: Integer;
  Codigo, Nome, Link, Categoria: String;
begin
  XML    := TStringStream.Create('');
  Codigo := '';
  Nome   := '';
  try
    LBApps.Items.Clear;
    //
    if DmkWeb.URLPost(FURL,
      [
      'method=REST_Dmk_ListaAplicativosDestop'
      ], XML) then
    begin
      XMLDocument1.LoadFromStream(XML);
      //
      for i := 0 to XMLDocument1.DocumentElement.ChildNodes.Count - 1 do
      begin
        with XMLDocument1.DocumentElement.ChildNodes[i] do
        begin
          Codigo    := ChildNodes['Codigo'].Text;
          Nome      := ChildNodes['Nome'].Text;
          Link      := ChildNodes['Link'].Text;
          Categoria := ChildNodes['Categoria'].Text;
          //
          FCodigo.Add(Codigo);
          FLink.Add(Link);
          FCategor.Add(Categoria);
          LBApps.Items.Add(Nome);
        end;
      end;
    end;
  finally
    XML.Free;
  end;
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(BtAtualiza, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean): Boolean;
var
  Versao: Integer;
  ArqNome: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'DermaBK', 'DermaBK',
              '', CO_VERSAO, CO_DMKID_APP, Now, Memo3, dtExecAux, Versao,
              ArqNome, False, ApenasVerifica, BalloonHint1);
end;

procedure TFmPrincipal.BtAtualizaClick(Sender: TObject);
begin
  VerificaNovasVersoes;
end;

procedure TFmPrincipal.BtBaixarClick(Sender: TObject);
var
  Dir, NomeApp, ArqNome: String;
  CodApp, Versao, Categoria: Integer;
  TipDownload: TDownType;
begin
  if (LBApps <> nil) and (LBApps.ItemIndex > -1) then
  begin
    NomeApp   := LBApps.Items.Strings[LBApps.ItemIndex];
    CodApp    := Geral.IMV(FCodigo[LBApps.ItemIndex]);
    Dir       := ExtractFilePath(Application.ExeName);
    Categoria := Geral.IMV(FCategor[LBApps.ItemIndex]);
    //
    //Geral.AbreArquivo(Dir); => N�o precisa pois logo abaixo cria o �cone
    //
    case Categoria of
        1: TipDownload := dtExecAux;
      else TipDownload := dtExec;
    end;
    //
    if DmkWeb.VerificaAtualizacaoVersao2(True, True, NomeApp, NomeApp,
        Geral.SoNumero_TT(FCNPJ), 0, CodApp, Now, Memo3, TipDownload, Versao,
        ArqNome, False, False, nil, True) then
    begin
      ArqNome := NomeApp;
      ArqNome := ExtractFilePath(Application.ExeName) + NomeApp + '.exe';
      //
      dmkPF.ConfiguraIconeAplicativo(True, False, ArqNome);
      //
      dmkPF.AdicionaRegraPortaFirewall('MySQL', '', 3306, True); //Entrada
      dmkPF.AdicionaRegraPortaFirewall('MySQL', '', 3306, False); //Sa�da
      //
      Application.Terminate;
    end;
  end;
end;

procedure TFmPrincipal.BtSaidaClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  FCodigo  := TStringList.Create;
  FLink    := TStringList.Create;
  FCategor := TStringList.Create;
  //
  ReopenAplicativos_REST;
  //
  TmVersao.Enabled := True;
end;

procedure TFmPrincipal.FormDestroy(Sender: TObject);
begin
  FCodigo.Free;
  FLink.Free;
  FCategor.Free;
end;

end.
