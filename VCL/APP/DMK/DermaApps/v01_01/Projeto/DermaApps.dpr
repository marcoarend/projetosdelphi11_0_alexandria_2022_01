program DermaApps;

uses
  Vcl.Forms,
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnDmkWeb in '..\..\..\..\..\MDL\WEB\v01_01\UnDmkWeb.pas',
  dmkSOAP in '..\..\..\..\..\MDL\WEB\v01_01\WebService\dmkSOAP.pas',
  MyListas in '..\Units\MyListas.pas',
  dmkGeral in '..\..\..\..\..\..\dmkComp\dmkGeral.pas',
  UnMsgInt in '..\..\..\..\..\UTL\_UNT\v01_01\UnMsgInt.pas',
  UnDmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  UnInternalConsts2 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts2.pas',
  UnInternalConsts in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  ZCF2 in '..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  ResIntStrings in '..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnitMD5 in '..\..\..\..\..\UTL\Encrypt\v01_01\UnitMD5.pas',
  UnMyObjects in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  dmkDAC_PF in '..\..\..\..\..\UTL\MySQL\v01_01\dmkDAC_PF.pas',
  NovaVersao in '..\..\..\..\..\UTL\_FRM\v01_01\NovaVersao.pas' {FmNovaVersao},
  UnGrl_Consts in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnDmkListas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnDmkListas.pas',
  UMySQLDB in '..\..\..\..\..\UTL\MySQL\v01_01\UMySQLDB.pas',
  UnGrl_Geral in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  GetData in '..\..\..\..\..\UTL\_FRM\v01_01\GetData.pas' {FmGetData};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'DermaApps';
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
