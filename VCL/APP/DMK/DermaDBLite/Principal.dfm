object FmPrincipal: TFmPrincipal
  Left = 0
  Top = 0
  Caption = 'Resources'
  ClientHeight = 428
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 635
    Height = 428
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = ' Conex'#227'o ao MySQL '
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 627
        Height = 400
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 24
          Top = 24
          Width = 69
          Height = 13
          Caption = 'Servidor Host:'
        end
        object Label2: TLabel
          Left = 24
          Top = 68
          Width = 30
          Height = 13
          Caption = 'Porta:'
        end
        object Label3: TLabel
          Left = 24
          Top = 112
          Width = 40
          Height = 13
          Caption = 'Usu'#225'rio:'
        end
        object Label4: TLabel
          Left = 24
          Top = 156
          Width = 34
          Height = 13
          Caption = 'Senha:'
        end
        object EdHost: TEdit
          Left = 24
          Top = 40
          Width = 121
          Height = 21
          TabOrder = 0
          Text = 'localhost'
        end
        object EdPorta: TEdit
          Left = 24
          Top = 84
          Width = 121
          Height = 21
          TabOrder = 1
          Text = '3307'
        end
        object EdUser: TEdit
          Left = 24
          Top = 128
          Width = 121
          Height = 21
          TabOrder = 2
          Text = 'root'
        end
        object EdPassword: TEdit
          Left = 24
          Top = 172
          Width = 121
          Height = 21
          TabOrder = 3
        end
        object Panel4: TPanel
          Left = 0
          Top = 359
          Width = 627
          Height = 41
          Align = alBottom
          TabOrder = 4
          object BtConnect: TBitBtn
            Left = 28
            Top = 4
            Width = 113
            Height = 25
            Caption = 'Conectar'
            TabOrder = 0
            OnClick = BtConnectClick
          end
          object BtVerificaConexoes: TBitBtn
            Left = 464
            Top = 8
            Width = 113
            Height = 25
            Caption = 'Verifica conex'#245'es'
            TabOrder = 1
            OnClick = BtVerificaConexoesClick
          end
          object BitBtn1: TBitBtn
            Left = 228
            Top = 8
            Width = 75
            Height = 25
            Caption = 'BitBtn1'
            TabOrder = 2
            OnClick = BitBtn1Click
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Registro do windows '
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 627
        Height = 400
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel3: TPanel
          Left = 0
          Top = 332
          Width = 627
          Height = 68
          Align = alBottom
          TabOrder = 0
          object BtAK: TBitBtn
            Left = 276
            Top = 24
            Width = 75
            Height = 25
            Caption = 'Escreve e l'#234
            TabOrder = 0
          end
        end
        object MeAK: TMemo
          Left = 0
          Top = 0
          Width = 627
          Height = 332
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
    end
  end
  object MyDB: TMySQLDatabase
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    DatasetOptions = []
    Left = 332
    Top = 164
  end
  object QrTeste: TMySQLQuery
    Database = MyDB
    Left = 368
    Top = 252
  end
end
