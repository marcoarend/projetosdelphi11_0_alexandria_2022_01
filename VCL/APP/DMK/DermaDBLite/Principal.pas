unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Data.DB,
  mySQLDbTables, Vcl.ExtCtrls, Vcl.ComCtrls;

type
  TFmPrincipal = class(TForm)
    MyDB: TMySQLDatabase;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    EdHost: TEdit;
    Label2: TLabel;
    EdPorta: TEdit;
    Label3: TLabel;
    EdUser: TEdit;
    Label4: TLabel;
    EdPassword: TEdit;
    Panel2: TPanel;
    Panel3: TPanel;
    BtAK: TBitBtn;
    MeAK: TMemo;
    Panel4: TPanel;
    BtConnect: TBitBtn;
    BtVerificaConexoes: TBitBtn;
    BitBtn1: TBitBtn;
    QrTeste: TMySQLQuery;
    procedure BtConnectClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtVerificaConexoesClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReCaptionComponentesDeForm(Form: TForm);
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses VerificaConexoes;

{$R *.dfm}

procedure TFmPrincipal.BitBtn1Click(Sender: TObject);
begin
  QrTeste.SQL.Text := 'SELECT AES_ENCRYPT("Teste", "jurubeba") Server_PWE';
  QrTeste.Open;
  ShowMessage(QrTeste.Fields[0].AsString);
end;

procedure TFmPrincipal.BtConnectClick(Sender: TObject);
begin
  if MyDB.Connected then
    MyDB.Disconnect;
  MyDB.Host         := EdHost.Text;
  MyDB.Port         := StrToInt(EdPorta.Text);
  MyDB.UserName     := EdUser.Text;
  MyDB.UserPassword := EdPassword.Text;
  //
  try
    MyDB.Connect;
    ShowMessage('Conex�o OK!');
  except
    on E: Exception do
      ShowMessage('N�o foi poss�vel conectar' + sLineBreak +
      E.Message)
  end;
end;

procedure TFmPrincipal.BtVerificaConexoesClick(Sender: TObject);
begin
  Application.CreateForm(TFmVerificaConexoes, FmVerificaConexoes);
  FmVerificaConexoes.ShowModal;
  FmVerificaConexoes.Destroy;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // Compatibilidade!
end;

end.
