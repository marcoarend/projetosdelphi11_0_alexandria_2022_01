program DermaDBLite;

uses
  Vcl.Forms,
  Principal in 'Principal.pas' {FmPrincipal},
  UnDmkRegistry in '..\..\..\UTL\_UNT\v01_01\UnDmkRegistry.pas',
  Vcl.Themes,
  Vcl.Styles,
  VerificaConexoes in '..\..\..\UTL\_FRM\v01_01\VerificaConexoes.pas' {FmVerificaConexoes},
  UnGrl_Vars in '..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnDmkEnums in '..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnMyObjects in '..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  GetData in '..\..\..\UTL\_FRM\v01_01\GetData.pas' {FmGetData},
  UnDmkProcFunc in '..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  ZCF2 in '..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  ResIntStrings in '..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnInternalConsts in '..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  MyGlyfs in '..\..\..\UTL\_FRM\v01_01\MyGlyfs.pas' {FmMyGlyfs},
  MyLisats in 'MyLisats.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Glossy');
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.Run;
end.
