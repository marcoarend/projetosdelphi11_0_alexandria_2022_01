unit OldSisNFe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBLookupComboBox, dmkEditCB;

type
  TFmOldSisNFe = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrNotaFiscal: TMySQLQuery;
    DsNotaFiscal: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    RGSistema: TRadioGroup;
    Panel5: TPanel;
    DBGNFe: TDBGrid;
    Panel6: TPanel;
    QrClientes: TMySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_ENT: TWideStringField;
    DsClientes: TDataSource;
    Label17: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrNotaFiscalLOJA: TWideStringField;
    QrNotaFiscalNOTA: TLargeintField;
    QrNotaFiscalMODELO: TLargeintField;
    QrNotaFiscalSERIE: TWideStringField;
    QrNotaFiscalCFOP: TLargeintField;
    QrNotaFiscalCANCELADA: TWideStringField;
    QrNotaFiscalCLIENTE: TLargeintField;
    QrNotaFiscalRAZAOSOCIAL: TWideStringField;
    QrNotaFiscalUF: TWideStringField;
    QrNotaFiscalCNPJ: TWideStringField;
    QrNotaFiscalINSCRICAOESTADUAL: TWideStringField;
    QrNotaFiscalVENDEDOR: TLargeintField;
    QrNotaFiscalORCAMENTO: TLargeintField;
    QrNotaFiscalEMISSAO: TDateField;
    QrNotaFiscalTOTALNOTA: TFloatField;
    QrNotaFiscalBASEICMSTRIB: TFloatField;
    QrNotaFiscalICMSTRIB: TFloatField;
    QrNotaFiscalBASEICMSSUB: TFloatField;
    QrNotaFiscalICMSSUB: TFloatField;
    QrNotaFiscalVALORFRETE: TFloatField;
    QrNotaFiscalVALORIPI: TFloatField;
    QrNotaFiscalTRANSPORTADORA: TLargeintField;
    QrNotaFiscalQUANTIDADE: TLargeintField;
    QrNotaFiscalESPECIE: TWideStringField;
    QrNotaFiscalMARCA: TWideStringField;
    QrNotaFiscalNUMERO: TWideStringField;
    QrNotaFiscalPESO: TLargeintField;
    QrNotaFiscalPESOLIQUIDO: TLargeintField;
    QrNotaFiscalUSUARIO: TWideStringField;
    QrNotaFiscalRECIBONFE: TWideStringField;
    QrNotaFiscalDANFE: TWideStringField;
    QrNotaFiscalNFEPENDENTE: TWideStringField;
    QrNotaFiscalRECIBORETORNONFE: TWideStringField;
    QrNotaFiscalMOTIVORETORNONFE: TWideStringField;
    QrNotaFiscalALIQUOTA: TFloatField;
    QrNotaFiscalFRETE: TLargeintField;
    QrNotaFiscalOBSERVACAO: TWideStringField;
    QrNotaFiscalCIF: TLargeintField;
    QrNotaFiscalDADOSADICIONAIS: TWideMemoField;
    QrNotaFiscalIMPRESSO: TWideStringField;
    QrNotaFiscalENVIADOEMAIL: TWideStringField;
    QrNotaFiscalPROTOCOLOCANCELAMENTO: TWideStringField;
    QrNotaFiscalDATAHORACANCELAMENTO: TWideStringField;
    QrNotaFiscalPIS: TFloatField;
    QrNotaFiscalCOFINS: TFloatField;
    QrNotaFiscalOUTRASDESPESAS: TFloatField;
    QrNotaFiscalTIPOPESSOA: TWideStringField;
    QrNotaFiscalTIPOPAGAMENTO: TWideStringField;
    QrNotaFiscalCOMPUTADOR: TWideStringField;
    QrNotaFiscalSEGURO: TFloatField;
    QrNotaFiscalPLACA: TWideStringField;
    QrNotaFiscalUFPLACA: TWideStringField;
    QrNotaFiscalTOTALPRODUTO: TFloatField;
    QrNotaFiscalIMPOSTOA: TFloatField;
    QrNotaFiscalCONTINGENCIAPENDENTE: TWideStringField;
    QrNotaFiscalIMPRIMEPRECOLIQUIDO: TWideStringField;
    QrNotaFiscalDESCONTO: TFloatField;
    QrNotaFiscalCLIENTEERRADO: TWideStringField;
    QrNotaFiscalCONDICAO: TLargeintField;
    QrNotaFiscalORIGEM: TLargeintField;
    QrNotaFiscalESPECIAL: TWideStringField;
    QrNotaFiscalItens: TMySQLQuery;
    QrNotaFiscalItensLOJA: TWideStringField;
    QrNotaFiscalItensNOTA: TLargeintField;
    QrNotaFiscalItensMODELO: TLargeintField;
    QrNotaFiscalItensSERIE: TWideStringField;
    QrNotaFiscalItensCFOP: TLargeintField;
    QrNotaFiscalItensSEQUENCIA: TLargeintField;
    QrNotaFiscalItensPRODUTO: TLargeintField;
    QrNotaFiscalItensNCM: TWideStringField;
    QrNotaFiscalItensDESCRICAO: TWideStringField;
    QrNotaFiscalItensUNIDADE: TWideStringField;
    QrNotaFiscalItensQUANTIDADE: TFloatField;
    QrNotaFiscalItensFISCAL: TWideStringField;
    QrNotaFiscalItensDESCONTO: TFloatField;
    QrNotaFiscalItensVALORIPI: TFloatField;
    QrNotaFiscalItensALIQUOTAICMS: TFloatField;
    QrNotaFiscalItensALIQUOTAIPI: TFloatField;
    QrNotaFiscalItensREDUCAOBASE: TFloatField;
    QrNotaFiscalItensBASEALIQUOTAICMS: TFloatField;
    QrNotaFiscalItensVALORICMS: TFloatField;
    QrNotaFiscalItensREDUCAOA: TFloatField;
    QrNotaFiscalItensREDUCAOB: TFloatField;
    QrNotaFiscalItensREDUCAOC: TFloatField;
    QrNotaFiscalItensREDUCAOD: TFloatField;
    QrNotaFiscalItensPIS: TFloatField;
    QrNotaFiscalItensCOFINS: TFloatField;
    QrNotaFiscalItensSEGURO: TFloatField;
    QrNotaFiscalItensOUTRASDESPESAS: TFloatField;
    QrNotaFiscalItensVALORFRETE: TFloatField;
    QrNotaFiscalItensBASECALCULOIPI: TFloatField;
    QrNotaFiscalItensICMSSUBSTITUIDO: TFloatField;
    QrNotaFiscalItensBASEICMSSUBSTITUIDO: TFloatField;
    QrNotaFiscalItensMVA: TFloatField;
    QrNotaFiscalItensCSOSN: TWideStringField;
    QrNotaFiscalItensBASEICMSUB: TFloatField;
    QrNotaFiscalItensICMSUB: TFloatField;
    QrNotaFiscalItensIMPOSTOA: TFloatField;
    QrNotaFiscalItensICMSSUBSPED: TFloatField;
    QrNotaFiscalItensVALORIPISPED: TFloatField;
    QrNotaFiscalItensPRECOBRUTO: TFloatField;
    QrNotaFiscalItensCEST: TWideStringField;
    QrNotaFiscalItensVALOR: TFloatField;
    DsNotaFiscalItens: TDataSource;
    Splitter1: TSplitter;
    DBGrid2: TDBGrid;
    QrNotaAntiga: TMySQLQuery;
    QrNotaAntigaNOTAFISCAL: TLargeintField;
    QrNotaAntigaMODELO: TLargeintField;
    QrNotaAntigaDATA: TDateField;
    QrNotaAntigaCLIENTE: TWideStringField;
    QrNotaAntigaCPFCGC: TWideStringField;
    QrNotaAntigaCHAVENFE: TWideStringField;
    QrNotaAntigaVALOR: TFloatField;
    DsNotaAntiga: TDataSource;
    EdPsqTxt: TdmkEdit;
    Label1: TLabel;
    EdNumeroNFe: TdmkEdit;
    BtXML: TBitBtn;
    QrTabelaXML: TMySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrNotaFiscalBeforeClose(DataSet: TDataSet);
    procedure QrNotaFiscalAfterScroll(DataSet: TDataSet);
    procedure RGSistemaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtXMLClick(Sender: TObject);
    procedure QrNotaFiscalAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReabreNotas();
  public
    { Public declarations }
  end;

  var
  FmOldSisNFe: TFmOldSisNFe;

implementation

uses UnMyObjects, DmkDAC_PF, Module;

{$R *.DFM}

procedure TFmOldSisNFe.BtXMLClick(Sender: TObject);
begin
(*
  QrNotaFiscal.First;
  while not QrNotaFiscal.Eof do
  begin

    //
    QrNotaFiscal.Next;
  end;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrTabelaXML, Dmod.DBAnt, [
  'SELECT NOTAFISCAL, XMLNOTA, XMLCANCELAMENTO',
  'FROM tabelaxml',
  'WHERE LOJA = "' + QrNotaFiscalLOJA.Value + '"',
  'AND NOTAFISCAL=' + Geral.FF0(QrNotaFiscalNota.Value),
  'AND MODELO=' + Geral.FF0(QrNotaFiscalModelo.Value),
  'AND SERIE = "' + QrNotaFiscalSERIE.Value + '"',
  '']);
  if QrTabelaXML.FieldByName('XMLCANCELAMENTO').AsString <> EmptyStr then
  begin
    Geral.MB_Aviso(sLineBreak + sLineBreak + sLineBreak + sLineBreak +
    '************* ***************** *************' + sLineBreak +
    '************* ATEN��O!!!!!!!!!! *************' + sLineBreak +
    '************* XML Cancelamnto!! *************' + sLineBreak +
    '************* ***************** *************' + sLineBreak);
    Geral.MB_Aviso(QrTabelaXML.FieldByName('XMLCANCELAMENTO').AsString);
  end else
    Geral.MB_Info(QrTabelaXML.FieldByName('XMLNOTA').AsString);
end;

procedure TFmOldSisNFe.BtOKClick(Sender: TObject);
begin
  ReabreNotas();
end;

procedure TFmOldSisNFe.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOldSisNFe.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOldSisNFe.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
end;

procedure TFmOldSisNFe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOldSisNFe.QrNotaFiscalAfterOpen(DataSet: TDataSet);
begin
  BtXML.Enabled := QrNotaFiscal.RecordCount > 0;
end;

procedure TFmOldSisNFe.QrNotaFiscalAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNotaFiscalItens, Dmod.DBAnt, [
  'SELECT * ',
  'FROM notafiscalitens ',
  'WHERE NOTA=' + Geral.FF0(QrNotaFiscalNOTA.Value),
  'AND Modelo=' + Geral.FF0(QrNotaFiscalModelo.Value),
  'AND Serie="' + QrNotaFiscalSerie.Value + '"',
  '']);
end;

procedure TFmOldSisNFe.QrNotaFiscalBeforeClose(DataSet: TDataSet);
begin
  QrNotaFiscalItens.Close;
  BtXML.Enabled := False;
end;

procedure TFmOldSisNFe.ReabreNotas();
var
  Cliente, NumeroNFe: Integer;
  SQL_Cliente, SQL_NumNFe, Txt: String;
begin
  SQL_Cliente := EmptyStr;
  SQL_NumNFe  := EmptyStr;
  Cliente     := EdCliente.ValueVariant;
  NumeroNFe   := EdNumeroNFe.ValueVariant;
  //
  case RGSistema.ItemIndex of
    0: // Sistema anterior
    begin
      if Cliente <> 0 then
        SQL_Cliente := 'AND CLIENTE=' + Geral.FF0(Cliente);
      //
      if NumeroNFe = 0 then
        SQL_NumNFe := 'WHERE NOTA<>0 '
      else
        SQL_NumNFe := 'WHERE NOTA=' + Geral.FF0(NumeroNFe);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrNotaFiscal, Dmod.DBAnt, [
      'SELECT * ',
      'FROM notafiscal ',
      SQL_NumNFe,
      SQL_Cliente,
      '']);
      //
      DBGNFe.DataSource := DsNotaFiscal;
    end;
    1: // Sistema anterior ao anterior
    begin
      //
      if NumeroNFe = 0 then
        SQL_NumNFe := 'WHERE NOTAFISCAL<>0 '
      else
        SQL_NumNFe := 'WHERE NOTAFISCAL=' + Geral.FF0(NumeroNFe);
      //
      Txt := EdPsqTxt.ValueVariant;
      if Txt <> EmptyStr then
      begin
        SQL_Cliente := Geral.ATS([
        'AND (',
        '  Cliente LIKE "%' + Txt + '%"',
        '  OR',
        '  CPFCGC LIKE "%' + Txt + '%" ',
        ')']);
      end;
      UnDmkDAC_PF.AbreMySQLQuery0(QrNotaAntiga, Dmod.DBAnt, [
      'SELECT * ',
      'FROM notaantiga ',
      SQL_NumNFe,
      SQL_Cliente,
      '']);
      //
      DBGNFe.DataSource := DsNotaAntiga;
    end;
  end;
end;

procedure TFmOldSisNFe.RGSistemaClick(Sender: TObject);
begin
  QrNotaFiscal.Close;
  QrNotaAntiga.Close;
  //
  ReabreNotas();
end;

end.
