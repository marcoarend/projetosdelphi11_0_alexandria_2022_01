unit OldSisOrcamento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBLookupComboBox, dmkEditCB,
  dmkDBGridZTO;

type
  TFmOldSisOrcamento = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrClientes: TMySQLQuery;
    DsClientes: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_ENT: TWideStringField;
    QrVendedores: TMySQLQuery;
    DsVendedores: TDataSource;
    DBGVendedores: TdmkDBGridZTO;
    QrOrcamentos: TMySQLQuery;
    QrOrcamentosVENDEDOR: TLargeintField;
    QrOrcamentosORCAMENTO: TLargeintField;
    QrOrcamentosEMISSAO: TDateField;
    QrOrcamentosEXECUCAO: TDateField;
    QrOrcamentosDATADELECAO: TDateField;
    QrOrcamentosCLIENTE: TLargeintField;
    QrOrcamentosCONDICAO: TLargeintField;
    QrOrcamentosENTRADA: TWideStringField;
    QrOrcamentosVALORENTRADA: TFloatField;
    QrOrcamentosPARCELAS: TLargeintField;
    QrOrcamentosINTERVALO: TLargeintField;
    QrOrcamentosDELETADO: TWideStringField;
    QrOrcamentosPERCENTUAL: TFloatField;
    QrOrcamentosVALOR: TFloatField;
    QrOrcamentosNOTAFISCAL: TLargeintField;
    QrOrcamentosNOTAFISCALSERIE: TWideStringField;
    QrOrcamentosCUPOM: TLargeintField;
    QrOrcamentosMOTIVO: TWideStringField;
    QrOrcamentosIDENTIFICACAOCOMPRA: TWideStringField;
    QrOrcamentosUSUARIO: TWideStringField;
    QrOrcamentosDESCONTO: TWideStringField;
    QrOrcamentosNOMECLIENTE: TWideStringField;
    QrOrcamentosENDERECOCLIENTE: TWideStringField;
    QrOrcamentosBAIRRO: TWideStringField;
    QrOrcamentosCPFCGC: TWideStringField;
    QrOrcamentosINSCRICAO: TWideStringField;
    QrOrcamentosCIDADE: TWideStringField;
    QrOrcamentosUF: TWideStringField;
    QrOrcamentosNUMEROCLIENTE: TLargeintField;
    QrOrcamentosTIPOPESSOA: TWideStringField;
    QrOrcamentosCEP: TWideStringField;
    QrOrcamentosDEVOLUCAO: TWideStringField;
    QrOrcamentosENDERECOENTREGA: TWideStringField;
    QrOrcamentosCONDICAOCOTACAO: TWideStringField;
    QrOrcamentosHORA: TTimeField;
    QrOrcamentosTIPOPAGAMENTO: TLargeintField;
    QrOrcamentosQUANTIDADEPAGAMENTO: TLargeintField;
    QrOrcamentosCARENCIA: TLargeintField;
    QrOrcamentosLIBERACAOGERENTE: TWideStringField;
    QrOrcamentosTEXTODEVOLUCAO: TWideStringField;
    QrOrcamentosOBSERVACAOCOTACAO: TWideStringField;
    QrOrcamentosCODIGOCIDADE: TLargeintField;
    QrOrcamentosFONE: TWideStringField;
    QrOrcamentosOBRA: TLargeintField;
    QrOrcamentosTIPOEMISSAOVENDEDORLOJA: TLargeintField;
    QrOrcamentosUSUARIOVENDEDORLOJA: TWideStringField;
    QrOrcamentosXTIPO: TWideStringField;
    QrOrcamentosXDOCUMENTO: TWideStringField;
    QrOrcamentosXSERIE: TWideStringField;
    QrOrcamentosXDOCECF: TWideStringField;
    QrOrcamentosORCAMENTOANTIGO: TLargeintField;
    QrOrcamentosTIPOSOLICITACAO: TWideStringField;
    QrOrcamentosSOLICITACAO: TDateTimeField;
    QrOrcamentosVENDEDOREFETIVO: TLargeintField;
    QrOrcamentosVENDEDORPARCEIRO: TLargeintField;
    QrOrcamentosHORAEXECUCAO: TTimeField;
    QrOrcamentosDESCONTOTOTAL: TFloatField;
    QrOrcamentosDESCONTOPERCENTUAL: TFloatField;
    QrOrcamentosUSUARIOALTEROUCONDICAO: TWideStringField;
    QrOrcamentosIMPRIMEPRECOLIQUIDO: TWideStringField;
    QrOrcamentosCONSIGNACAO: TDateField;
    QrOrcamentosVALORFRETE: TFloatField;
    QrOrcamentosLOJANFCE: TWideStringField;
    QrOrcamentosDESCONTOIMPORTACAO: TFloatField;
    QrOrcamentosINTERNET: TWideStringField;
    QrVendedoresVENDEDOR: TLargeintField;
    DBGrid1: TDBGrid;
    Splitter1: TSplitter;
    DBGrid2: TDBGrid;
    DsOrcamentos: TDataSource;
    QrOrcamentosItens: TMySQLQuery;
    DsOrcamentosItens: TDataSource;
    QrOrcamentosItensSEQUENCIA: TLargeintField;
    QrOrcamentosItensProduto: TLargeintField;
    QrOrcamentosItensDESCRICAO: TWideStringField;
    QrOrcamentosItensMarca: TWideStringField;
    QrOrcamentosItensQuantidade: TFloatField;
    QrOrcamentosItensPrecoVenda: TFloatField;
    QrOrcamentosItensPrecoBruto: TFloatField;
    QrOrcamentosItensPrecoEfetivo: TFloatField;
    Panel5: TPanel;
    EdCliente: TdmkEditCB;
    Label17: TLabel;
    CBCliente: TdmkDBLookupComboBox;
    EdOrcamento: TdmkEdit;
    Label1: TLabel;
    DBGrid3: TDBGrid;
    QrOrcamentoPagamento: TMySQLQuery;
    QrOrcamentoPagamentoVENDEDOR: TLargeintField;
    QrOrcamentoPagamentoORCAMENTO: TLargeintField;
    QrOrcamentoPagamentoVENCIMENTO: TDateField;
    QrOrcamentoPagamentoVALOR: TFloatField;
    DsOrcamentoPagamento: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrOrcamentosBeforeClose(DataSet: TDataSet);
    procedure QrOrcamentosAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmOldSisOrcamento: TFmOldSisOrcamento;

implementation

uses UnMyObjects, DmkDAC_PF, Module;

{$R *.DFM}

procedure TFmOldSisOrcamento.BtOKClick(Sender: TObject);
var
  I, Cliente, Orcamento: Integer;
  SQL_CLIENTE, SQL_Orcamento: String;
  SQL_VENDEDORES, Corda: String;
begin
  Cliente       := EdCliente.ValueVariant;
  Orcamento     := EdOrcamento.ValueVariant;
  SQL_CLIENTE   := EmptyStr;
  SQL_Orcamento := EmptyStr;
  //
  if MyObjects.FIC((Cliente = 0) and (Orcamento = 0), EdCliente,
  'Informe um cliente ou um or�amento!') then Exit;
  begin
    Corda := MyObjects.CordaDeZTOChecks(DBGVendedores, QrVendedores, 'VENDEDOR',
    (*PermiteTodos*) False);
    //
    SQL_VENDEDORES := EmptyStr;
    if Corda <> EmptyStr then
      SQL_VENDEDORES := 'AND VENDEDOR IN (' + Corda + ')';
    //
    if Cliente <> 0 then
     SQL_CLIENTE := 'AND Cliente=' + Geral.FF0(Cliente);
    //
    if Orcamento <> 0 then
     SQL_Orcamento := 'AND ORCAMENTO=' + Geral.FF0(Orcamento);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrOrcamentos, Dmod.DBAnt, [
    'SELECT * ',
    'FROM orcamento ',
    'WHERE ORCAMENTO <> 0',
    SQL_CLIENTE,
    SQL_ORCAMENTO,
    SQL_VENDEDORES,
    'ORDER BY ORCAMENTO DESC',
    ''])
  end;
end;

procedure TFmOldSisOrcamento.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOldSisOrcamento.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOldSisOrcamento.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrVendedores, Dmod.DBAnt);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
end;

procedure TFmOldSisOrcamento.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOldSisOrcamento.QrOrcamentosAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrcamentosItens, Dmod.DBAnt, [
  'SELECT its.SEQUENCIA, its.Produto,  ',
  'prd.DESCRICAO, prd.Marca, its.Quantidade, ',
  'its.PrecoVenda, its.PrecoBruto,  ',
  'its.PrecoEfetivo  ',
  'FROM orcamentoitens its ',
  'LEFT JOIN produto prd ON prd.PRODUTO=its.PRODUTO ',
  'WHERE ORCAMENTO=' + Geral.FF0(QrOrcamentosORCAMENTO.Value),
  'AND VENDEDOR=' + Geral.FF0(QrOrcamentosVENDEDOR.Value),
  'ORDER BY SEQUENCIA ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrcamentoPagamento, Dmod.DBAnt, [
  'SELECT pgt.*  ',
  'FROM orcamentopagamento pgt ',
  'WHERE ORCAMENTO=' + Geral.FF0(QrOrcamentosORCAMENTO.Value),
  'AND VENDEDOR=' + Geral.FF0(QrOrcamentosVENDEDOR.Value),
  'ORDER BY VENCIMENTO ',
  '']);
end;

procedure TFmOldSisOrcamento.QrOrcamentosBeforeClose(DataSet: TDataSet);
begin
  QrOrcamentosItens.Close;
  QrOrcamentoPagamento.Close;
end;

end.
