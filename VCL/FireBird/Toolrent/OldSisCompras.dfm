object FmOldSisCompras: TFmOldSisCompras
  Left = 339
  Top = 185
  Caption = 'SIS-ANTGO-003 :: Compras no Sistema Anterior'
  ClientHeight = 629
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 1018
    object GB_R: TGroupBox
      Left = 1216
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 970
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1168
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 922
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 356
        Height = 32
        Caption = 'Compras no Sistema Anterior'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 356
        Height = 32
        Caption = 'Compras no Sistema Anterior'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 356
        Height = 32
        Caption = 'Compras no Sistema Anterior'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1264
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitWidth = 1018
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1260
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1014
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1264
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitWidth = 1018
    object PnSaiDesis: TPanel
      Left = 1118
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 872
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1116
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 870
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1264
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 1018
    object dmkDBGridZTO1: TdmkDBGridZTO
      Left = 0
      Top = 62
      Width = 1264
      Height = 405
      Align = alClient
      DataSource = DsCompras
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'DataNF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Entrada'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RAZAOSOCIAL'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FORNECEDOR'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOTAFISCAL'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MODELO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SERIE'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PRODUTO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QUANTIDADE'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PRECO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CUSTO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SEQUENCIA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MARGEM'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ALIQUOTAICM'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ATUALIZA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALORICMSTRIBCOMPRA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ALIQUOTAIPICOMPRA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALORIPICOMPRA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MVACOMPRA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ALIQUOTAICMSTCOMPRA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALORICMSSTCOMPRA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALORFRETENOTACOMPRA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALORFRETETRANSPORTADORACOMPRA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OUTRASDESPESASCOMPRA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CUSTOREPOSICAO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOVOPRECOVENDA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CODIGOBARRAS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CST'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CFOP'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DESCONTO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BASECALCULOICMS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ALIQUOTAICMS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALORICMS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BASEICMSSUBSTITUICAO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ALIQUOTAICMSSUBSTITUICAODESTINO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALORICMSSUBSTITUICAO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BASECALCULOIPI'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IPI'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALORIPI'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CST_PIS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BASECALCULOPIS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ALIQUOTAPIS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QTDEBASECALCULOPIS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ALIQUOTAPISREAL'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALORPIS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CST_COFINS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BASECALCULOCOFINS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ALIQUOTACOFINS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QTDEBASECALCULOCOFINS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ALIQUOTACOFINSREAL'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALORCOFINS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QUANTIDADESPED'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TOTALITENSSPED'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FRETE'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SEGURO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OUTRASDESPESAS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ALIQUOTAICMSTLOCAL'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FISCAL'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NCM'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CEST'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CFOPORIGEM'
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1264
      Height = 62
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitWidth = 1018
      object Label15: TLabel
        Left = 8
        Top = 2
        Width = 40
        Height = 13
        Caption = 'EAN 13:'
      end
      object Label1: TLabel
        Left = 206
        Top = 2
        Width = 48
        Height = 13
        Caption = 'Reduzido:'
      end
      object Label2: TLabel
        Left = 264
        Top = 2
        Width = 55
        Height = 13
        Caption = 'Refer'#234'ncia:'
      end
      object Label7: TLabel
        Left = 480
        Top = 2
        Width = 121
        Height = 13
        Caption = 'Descri'#231#227'o da mercadoria:'
      end
      object Label17: TLabel
        Left = 8
        Top = 44
        Width = 57
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Fornecedor:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object EdEAN13: TdmkEdit
        Left = 8
        Top = 18
        Width = 195
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdEAN13Change
        OnKeyDown = EdEAN13KeyDown
        OnRedefinido = EdEAN13Redefinido
      end
      object EdGraGruX: TdmkEditCB
        Left = 206
        Top = 18
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGruXChange
        OnRedefinido = EdGraGruXRedefinido
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdReferencia: TdmkEdit
        Left = 264
        Top = 18
        Width = 211
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdReferenciaChange
        OnKeyDown = EdReferenciaKeyDown
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 480
        Top = 18
        Width = 529
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_GG1'
        ListSource = DsGraGXVend
        TabOrder = 3
        dmkEditCB = EdGraGruX
        UpdType = utYes
        LocF7SQLText.Strings = (
          'SELECT lpc.GraGruX _CODIGO, gg1.REFERENCIA, gg1.Nome _NOME'
          'FROM gragxvend lpc'
          'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX'
          'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
          'WHERE gg1.Nome LIKE "%$#%"'
          'OR gg1.Referencia LIKE "%$#%"'
          'OR gg1.Patrimonio LIKE "%$#%"'
          '')
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdFornecedor: TdmkEditCB
        Left = 72
        Top = 40
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecedor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornecedor: TdmkDBLookupComboBox
        Left = 128
        Top = 40
        Width = 577
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsFornecedores
        ParentFont = False
        TabOrder = 5
        dmkEditCB = EdFornecedor
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrFornecedores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 228
    Top = 240
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedoresNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 255
    end
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 228
    Top = 296
  end
  object QrGraGXVend: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, ggx.GraGru1,'
      'gg1.Referencia, gg1.Nome NO_GG1,  gg1.NCM, gg1.UnidMed,'
      'ggo.GraGruX, ggo.ItemValr, ggo.ItemUnid'
      'FROM gragxvend ggo'
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggo.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggo.Aplicacao=:P0'
      'ORDER BY NO_GG1'
      '')
    Left = 312
    Top = 242
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGXVendControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGXVendReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXVendNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGXVendGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGXVendItemValr: TFloatField
      FieldName = 'ItemValr'
    end
    object QrGraGXVendItemUnid: TIntegerField
      FieldName = 'ItemUnid'
    end
    object QrGraGXVendGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGXVendNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGXVendMaxPercDesco: TFloatField
      FieldName = 'MaxPercDesco'
    end
    object QrGraGXVendUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGXVendEAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
  end
  object DsGraGXVend: TDataSource
    DataSet = QrGraGXVend
    Left = 312
    Top = 290
  end
  object QrPesq1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  gg1.Referencia=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 572
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesq1EAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
  end
  object QrPesq2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Referencia'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  cpl.GraGruX=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 600
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesq2Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
    object QrPesq2EAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
  end
  object QrCompras: TMySQLQuery
    Database = Dmod.DBAnt
    SQL.Strings = (
      'SELECT cab.DataNF, cab.Entrada, '
      'cli.RAZAOSOCIAL, its.* '
      'FROM comprasitens its '
      'LEFT JOIN compras cab ON '
      '  cab.FORNECEDOR=its.FORNECEDOR AND '
      '  cab.NOTAFISCAL=its.NOTAFISCAL AND '
      '  cab.MODELO=its.MODELO AND '
      '  cab.SERIE=its.SERIE '
      'LEFT JOIN CLIENTE cli ON cli.CLIENTE=its.Fornecedor'
      'WHERE its.PRODUTO <> 0 '
      'AND PRODUTO=1418'
      'ORDER BY cab.DataNF, its.SEQUENCIA DESC')
    Left = 584
    Top = 252
    object QrComprasDataNF: TDateField
      FieldName = 'DataNF'
    end
    object QrComprasEntrada: TDateField
      FieldName = 'Entrada'
    end
    object QrComprasRAZAOSOCIAL: TWideStringField
      FieldName = 'RAZAOSOCIAL'
      Size = 200
    end
    object QrComprasFORNECEDOR: TLargeintField
      FieldName = 'FORNECEDOR'
      Required = True
    end
    object QrComprasNOTAFISCAL: TLargeintField
      FieldName = 'NOTAFISCAL'
      Required = True
    end
    object QrComprasMODELO: TLargeintField
      FieldName = 'MODELO'
      Required = True
    end
    object QrComprasSERIE: TWideStringField
      FieldName = 'SERIE'
      Required = True
      Size = 3
    end
    object QrComprasPRODUTO: TLargeintField
      FieldName = 'PRODUTO'
      Required = True
    end
    object QrComprasQUANTIDADE: TFloatField
      FieldName = 'QUANTIDADE'
    end
    object QrComprasPRECO: TFloatField
      FieldName = 'PRECO'
    end
    object QrComprasCUSTO: TFloatField
      FieldName = 'CUSTO'
    end
    object QrComprasSEQUENCIA: TLargeintField
      FieldName = 'SEQUENCIA'
      Required = True
    end
    object QrComprasMARGEM: TFloatField
      FieldName = 'MARGEM'
    end
    object QrComprasALIQUOTAICM: TFloatField
      FieldName = 'ALIQUOTAICM'
    end
    object QrComprasATUALIZA: TWideStringField
      FieldName = 'ATUALIZA'
      Size = 1
    end
    object QrComprasVALORICMSTRIBCOMPRA: TFloatField
      FieldName = 'VALORICMSTRIBCOMPRA'
    end
    object QrComprasALIQUOTAIPICOMPRA: TFloatField
      FieldName = 'ALIQUOTAIPICOMPRA'
    end
    object QrComprasVALORIPICOMPRA: TFloatField
      FieldName = 'VALORIPICOMPRA'
    end
    object QrComprasMVACOMPRA: TFloatField
      FieldName = 'MVACOMPRA'
    end
    object QrComprasALIQUOTAICMSTCOMPRA: TFloatField
      FieldName = 'ALIQUOTAICMSTCOMPRA'
    end
    object QrComprasVALORICMSSTCOMPRA: TFloatField
      FieldName = 'VALORICMSSTCOMPRA'
    end
    object QrComprasVALORFRETENOTACOMPRA: TFloatField
      FieldName = 'VALORFRETENOTACOMPRA'
    end
    object QrComprasVALORFRETETRANSPORTADORACOMPRA: TFloatField
      FieldName = 'VALORFRETETRANSPORTADORACOMPRA'
    end
    object QrComprasOUTRASDESPESASCOMPRA: TFloatField
      FieldName = 'OUTRASDESPESASCOMPRA'
    end
    object QrComprasCUSTOREPOSICAO: TFloatField
      FieldName = 'CUSTOREPOSICAO'
    end
    object QrComprasNOVOPRECOVENDA: TFloatField
      FieldName = 'NOVOPRECOVENDA'
    end
    object QrComprasCODIGOBARRAS: TWideStringField
      FieldName = 'CODIGOBARRAS'
      Size = 50
    end
    object QrComprasCST: TWideStringField
      FieldName = 'CST'
      Size = 3
    end
    object QrComprasCFOP: TLargeintField
      FieldName = 'CFOP'
    end
    object QrComprasDESCONTO: TFloatField
      FieldName = 'DESCONTO'
    end
    object QrComprasBASECALCULOICMS: TFloatField
      FieldName = 'BASECALCULOICMS'
    end
    object QrComprasALIQUOTAICMS: TFloatField
      FieldName = 'ALIQUOTAICMS'
    end
    object QrComprasVALORICMS: TFloatField
      FieldName = 'VALORICMS'
    end
    object QrComprasBASEICMSSUBSTITUICAO: TFloatField
      FieldName = 'BASEICMSSUBSTITUICAO'
    end
    object QrComprasALIQUOTAICMSSUBSTITUICAODESTINO: TFloatField
      FieldName = 'ALIQUOTAICMSSUBSTITUICAODESTINO'
    end
    object QrComprasVALORICMSSUBSTITUICAO: TFloatField
      FieldName = 'VALORICMSSUBSTITUICAO'
    end
    object QrComprasBASECALCULOIPI: TFloatField
      FieldName = 'BASECALCULOIPI'
    end
    object QrComprasIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrComprasVALORIPI: TFloatField
      FieldName = 'VALORIPI'
    end
    object QrComprasCST_PIS: TLargeintField
      FieldName = 'CST_PIS'
    end
    object QrComprasBASECALCULOPIS: TFloatField
      FieldName = 'BASECALCULOPIS'
    end
    object QrComprasALIQUOTAPIS: TFloatField
      FieldName = 'ALIQUOTAPIS'
    end
    object QrComprasQTDEBASECALCULOPIS: TFloatField
      FieldName = 'QTDEBASECALCULOPIS'
    end
    object QrComprasALIQUOTAPISREAL: TFloatField
      FieldName = 'ALIQUOTAPISREAL'
    end
    object QrComprasVALORPIS: TFloatField
      FieldName = 'VALORPIS'
    end
    object QrComprasCST_COFINS: TLargeintField
      FieldName = 'CST_COFINS'
    end
    object QrComprasBASECALCULOCOFINS: TFloatField
      FieldName = 'BASECALCULOCOFINS'
    end
    object QrComprasALIQUOTACOFINS: TFloatField
      FieldName = 'ALIQUOTACOFINS'
    end
    object QrComprasQTDEBASECALCULOCOFINS: TFloatField
      FieldName = 'QTDEBASECALCULOCOFINS'
    end
    object QrComprasALIQUOTACOFINSREAL: TFloatField
      FieldName = 'ALIQUOTACOFINSREAL'
    end
    object QrComprasVALORCOFINS: TFloatField
      FieldName = 'VALORCOFINS'
    end
    object QrComprasQUANTIDADESPED: TFloatField
      FieldName = 'QUANTIDADESPED'
    end
    object QrComprasTOTALITENSSPED: TFloatField
      FieldName = 'TOTALITENSSPED'
    end
    object QrComprasFRETE: TFloatField
      FieldName = 'FRETE'
    end
    object QrComprasSEGURO: TFloatField
      FieldName = 'SEGURO'
    end
    object QrComprasOUTRASDESPESAS: TFloatField
      FieldName = 'OUTRASDESPESAS'
    end
    object QrComprasALIQUOTAICMSTLOCAL: TFloatField
      FieldName = 'ALIQUOTAICMSTLOCAL'
    end
    object QrComprasFISCAL: TLargeintField
      FieldName = 'FISCAL'
    end
    object QrComprasNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrComprasCEST: TWideStringField
      FieldName = 'CEST'
      Size = 10
    end
    object QrComprasCFOPORIGEM: TLargeintField
      FieldName = 'CFOPORIGEM'
    end
  end
  object DsCompras: TDataSource
    DataSet = QrCompras
    Left = 584
    Top = 308
  end
end
