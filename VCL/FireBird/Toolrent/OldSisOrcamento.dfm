object FmOldSisOrcamento: TFmOldSisOrcamento
  Left = 339
  Top = 185
  Caption = 'SIS-ANTGO-001 :: Or'#231'amento Sistema Anterior'
  ClientHeight = 629
  ClientWidth = 1081
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1081
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1033
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 985
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 342
        Height = 32
        Caption = 'Or'#231'amento Sistema Anterior'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 342
        Height = 32
        Caption = 'Or'#231'amento Sistema Anterior'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 342
        Height = 32
        Caption = 'Or'#231'amento Sistema Anterior'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1081
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1077
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1081
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 935
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 933
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1081
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Splitter1: TSplitter
      Left = 0
      Top = 245
      Width = 1081
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ExplicitWidth = 222
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1081
      Height = 125
      Align = alTop
      TabOrder = 0
      object DBGVendedores: TdmkDBGridZTO
        Left = 1
        Top = 1
        Width = 124
        Height = 123
        Align = alLeft
        DataSource = DsVendedores
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'VENDEDOR'
            Width = 63
            Visible = True
          end>
      end
      object Panel5: TPanel
        Left = 125
        Top = 1
        Width = 500
        Height = 123
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Label17: TLabel
          Left = 4
          Top = 4
          Width = 35
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cliente:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label1: TLabel
          Left = 4
          Top = 44
          Width = 55
          Height = 13
          Caption = 'Or'#231'amento:'
        end
        object EdCliente: TdmkEditCB
          Left = 4
          Top = 20
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cliente'
          UpdCampo = 'Cliente'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 60
          Top = 20
          Width = 417
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          KeyField = 'Codigo'
          ListField = 'NO_ENT'
          ListSource = DsClientes
          ParentFont = False
          TabOrder = 1
          dmkEditCB = EdCliente
          QryCampo = 'Cliente'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdOrcamento: TdmkEdit
          Left = 4
          Top = 60
          Width = 77
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object DBGrid3: TDBGrid
        Left = 625
        Top = 1
        Width = 455
        Height = 123
        Align = alClient
        DataSource = DsOrcamentoPagamento
        TabOrder = 2
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'VALOR'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VENCIMENTO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VENDEDOR'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ORCAMENTO'
            Visible = True
          end>
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 125
      Width = 1081
      Height = 120
      Align = alTop
      DataSource = DsOrcamentos
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object DBGrid2: TDBGrid
      Left = 0
      Top = 248
      Width = 1081
      Height = 219
      Align = alClient
      DataSource = DsOrcamentosItens
      TabOrder = 2
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'SEQUENCIA'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Produto'
          Width = 57
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DESCRICAO'
          Width = 600
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Marca'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Quantidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoVenda'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoBruto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoEfetivo'
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 384
    Top = 284
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 255
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 384
    Top = 340
  end
  object QrVendedores: TMySQLQuery
    Database = Dmod.DBAnt
    SQL.Strings = (
      'SELECT DISTINCT VENDEDOR'
      'FROM Orcamento'
      'ORDER BY VENDEDOR')
    Left = 264
    Top = 284
    object QrVendedoresVENDEDOR: TLargeintField
      FieldName = 'VENDEDOR'
      Required = True
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 264
    Top = 340
  end
  object QrOrcamentos: TMySQLQuery
    Database = Dmod.DBAnt
    BeforeClose = QrOrcamentosBeforeClose
    AfterScroll = QrOrcamentosAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM Orcamento')
    Left = 152
    Top = 120
    object QrOrcamentosVENDEDOR: TLargeintField
      FieldName = 'VENDEDOR'
      Required = True
    end
    object QrOrcamentosORCAMENTO: TLargeintField
      FieldName = 'ORCAMENTO'
      Required = True
    end
    object QrOrcamentosEMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrOrcamentosEXECUCAO: TDateField
      FieldName = 'EXECUCAO'
    end
    object QrOrcamentosDATADELECAO: TDateField
      FieldName = 'DATADELECAO'
    end
    object QrOrcamentosCLIENTE: TLargeintField
      FieldName = 'CLIENTE'
    end
    object QrOrcamentosCONDICAO: TLargeintField
      FieldName = 'CONDICAO'
    end
    object QrOrcamentosENTRADA: TWideStringField
      FieldName = 'ENTRADA'
      Size = 1
    end
    object QrOrcamentosVALORENTRADA: TFloatField
      FieldName = 'VALORENTRADA'
    end
    object QrOrcamentosPARCELAS: TLargeintField
      FieldName = 'PARCELAS'
    end
    object QrOrcamentosINTERVALO: TLargeintField
      FieldName = 'INTERVALO'
    end
    object QrOrcamentosDELETADO: TWideStringField
      FieldName = 'DELETADO'
      Size = 1
    end
    object QrOrcamentosPERCENTUAL: TFloatField
      FieldName = 'PERCENTUAL'
    end
    object QrOrcamentosVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrOrcamentosNOTAFISCAL: TLargeintField
      FieldName = 'NOTAFISCAL'
    end
    object QrOrcamentosNOTAFISCALSERIE: TWideStringField
      FieldName = 'NOTAFISCALSERIE'
      Size = 3
    end
    object QrOrcamentosCUPOM: TLargeintField
      FieldName = 'CUPOM'
    end
    object QrOrcamentosMOTIVO: TWideStringField
      FieldName = 'MOTIVO'
      Size = 50
    end
    object QrOrcamentosIDENTIFICACAOCOMPRA: TWideStringField
      FieldName = 'IDENTIFICACAOCOMPRA'
      Size = 1024
    end
    object QrOrcamentosUSUARIO: TWideStringField
      FieldName = 'USUARIO'
      Size = 10
    end
    object QrOrcamentosDESCONTO: TWideStringField
      FieldName = 'DESCONTO'
      Size = 1
    end
    object QrOrcamentosNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 50
    end
    object QrOrcamentosENDERECOCLIENTE: TWideStringField
      FieldName = 'ENDERECOCLIENTE'
      Size = 50
    end
    object QrOrcamentosBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 50
    end
    object QrOrcamentosCPFCGC: TWideStringField
      FieldName = 'CPFCGC'
    end
    object QrOrcamentosINSCRICAO: TWideStringField
      FieldName = 'INSCRICAO'
    end
    object QrOrcamentosCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 50
    end
    object QrOrcamentosUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrOrcamentosNUMEROCLIENTE: TLargeintField
      FieldName = 'NUMEROCLIENTE'
    end
    object QrOrcamentosTIPOPESSOA: TWideStringField
      FieldName = 'TIPOPESSOA'
      Size = 1
    end
    object QrOrcamentosCEP: TWideStringField
      FieldName = 'CEP'
      Size = 8
    end
    object QrOrcamentosDEVOLUCAO: TWideStringField
      FieldName = 'DEVOLUCAO'
      Size = 1
    end
    object QrOrcamentosENDERECOENTREGA: TWideStringField
      FieldName = 'ENDERECOENTREGA'
      Size = 200
    end
    object QrOrcamentosCONDICAOCOTACAO: TWideStringField
      FieldName = 'CONDICAOCOTACAO'
      Size = 50
    end
    object QrOrcamentosHORA: TTimeField
      FieldName = 'HORA'
    end
    object QrOrcamentosTIPOPAGAMENTO: TLargeintField
      FieldName = 'TIPOPAGAMENTO'
    end
    object QrOrcamentosQUANTIDADEPAGAMENTO: TLargeintField
      FieldName = 'QUANTIDADEPAGAMENTO'
    end
    object QrOrcamentosCARENCIA: TLargeintField
      FieldName = 'CARENCIA'
    end
    object QrOrcamentosLIBERACAOGERENTE: TWideStringField
      FieldName = 'LIBERACAOGERENTE'
      Size = 10
    end
    object QrOrcamentosTEXTODEVOLUCAO: TWideStringField
      FieldName = 'TEXTODEVOLUCAO'
      Size = 50
    end
    object QrOrcamentosOBSERVACAOCOTACAO: TWideStringField
      FieldName = 'OBSERVACAOCOTACAO'
      Size = 200
    end
    object QrOrcamentosCODIGOCIDADE: TLargeintField
      FieldName = 'CODIGOCIDADE'
    end
    object QrOrcamentosFONE: TWideStringField
      FieldName = 'FONE'
      Size = 15
    end
    object QrOrcamentosOBRA: TLargeintField
      FieldName = 'OBRA'
    end
    object QrOrcamentosTIPOEMISSAOVENDEDORLOJA: TLargeintField
      FieldName = 'TIPOEMISSAOVENDEDORLOJA'
    end
    object QrOrcamentosUSUARIOVENDEDORLOJA: TWideStringField
      FieldName = 'USUARIOVENDEDORLOJA'
      Size = 10
    end
    object QrOrcamentosXTIPO: TWideStringField
      FieldName = 'XTIPO'
      Size = 1
    end
    object QrOrcamentosXDOCUMENTO: TWideStringField
      FieldName = 'XDOCUMENTO'
      Size = 10
    end
    object QrOrcamentosXSERIE: TWideStringField
      FieldName = 'XSERIE'
      Size = 3
    end
    object QrOrcamentosXDOCECF: TWideStringField
      FieldName = 'XDOCECF'
      Size = 10
    end
    object QrOrcamentosORCAMENTOANTIGO: TLargeintField
      FieldName = 'ORCAMENTOANTIGO'
    end
    object QrOrcamentosTIPOSOLICITACAO: TWideStringField
      FieldName = 'TIPOSOLICITACAO'
      Size = 1
    end
    object QrOrcamentosSOLICITACAO: TDateTimeField
      FieldName = 'SOLICITACAO'
    end
    object QrOrcamentosVENDEDOREFETIVO: TLargeintField
      FieldName = 'VENDEDOREFETIVO'
    end
    object QrOrcamentosVENDEDORPARCEIRO: TLargeintField
      FieldName = 'VENDEDORPARCEIRO'
    end
    object QrOrcamentosHORAEXECUCAO: TTimeField
      FieldName = 'HORAEXECUCAO'
    end
    object QrOrcamentosDESCONTOTOTAL: TFloatField
      FieldName = 'DESCONTOTOTAL'
    end
    object QrOrcamentosDESCONTOPERCENTUAL: TFloatField
      FieldName = 'DESCONTOPERCENTUAL'
    end
    object QrOrcamentosUSUARIOALTEROUCONDICAO: TWideStringField
      FieldName = 'USUARIOALTEROUCONDICAO'
      Size = 10
    end
    object QrOrcamentosIMPRIMEPRECOLIQUIDO: TWideStringField
      FieldName = 'IMPRIMEPRECOLIQUIDO'
      Size = 1
    end
    object QrOrcamentosCONSIGNACAO: TDateField
      FieldName = 'CONSIGNACAO'
    end
    object QrOrcamentosVALORFRETE: TFloatField
      FieldName = 'VALORFRETE'
    end
    object QrOrcamentosLOJANFCE: TWideStringField
      FieldName = 'LOJANFCE'
      Size = 3
    end
    object QrOrcamentosDESCONTOIMPORTACAO: TFloatField
      FieldName = 'DESCONTOIMPORTACAO'
    end
    object QrOrcamentosINTERNET: TWideStringField
      FieldName = 'INTERNET'
      Size = 1
    end
  end
  object DsOrcamentos: TDataSource
    DataSet = QrOrcamentos
    Left = 152
    Top = 168
  end
  object QrOrcamentosItens: TMySQLQuery
    Database = Dmod.DBAnt
    SQL.Strings = (
      'SELECT its.SEQUENCIA, its.Produto,  '
      'prd.DESCRICAO, prd.Marca, its.Quantidade, '
      'its.PrecoVenda, its.PrecoBruto,  '
      'its.PrecoEfetivo  '
      'FROM orcamentoitens its '
      'LEFT JOIN produto prd ON prd.PRODUTO=its.PRODUTO '
      'WHERE ORCAMENTO = 28567 ')
    Left = 152
    Top = 220
    object QrOrcamentosItensSEQUENCIA: TLargeintField
      FieldName = 'SEQUENCIA'
      Required = True
    end
    object QrOrcamentosItensProduto: TLargeintField
      FieldName = 'Produto'
      Required = True
    end
    object QrOrcamentosItensDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Size = 200
    end
    object QrOrcamentosItensMarca: TWideStringField
      FieldName = 'Marca'
      Size = 15
    end
    object QrOrcamentosItensQuantidade: TFloatField
      FieldName = 'Quantidade'
    end
    object QrOrcamentosItensPrecoVenda: TFloatField
      FieldName = 'PrecoVenda'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOrcamentosItensPrecoBruto: TFloatField
      FieldName = 'PrecoBruto'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOrcamentosItensPrecoEfetivo: TFloatField
      FieldName = 'PrecoEfetivo'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsOrcamentosItens: TDataSource
    DataSet = QrOrcamentosItens
    Left = 152
    Top = 268
  end
  object QrOrcamentoPagamento: TMySQLQuery
    Database = Dmod.DBAnt
    SQL.Strings = (
      'SELECT pgt.* '
      'FROM orcamentopagamento pgt'
      'WHERE Orcamento>=28000')
    Left = 152
    Top = 316
    object QrOrcamentoPagamentoVENDEDOR: TLargeintField
      FieldName = 'VENDEDOR'
      Required = True
    end
    object QrOrcamentoPagamentoORCAMENTO: TLargeintField
      FieldName = 'ORCAMENTO'
      Required = True
    end
    object QrOrcamentoPagamentoVENCIMENTO: TDateField
      FieldName = 'VENCIMENTO'
      Required = True
    end
    object QrOrcamentoPagamentoVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsOrcamentoPagamento: TDataSource
    DataSet = QrOrcamentoPagamento
    Left = 148
    Top = 368
  end
end
