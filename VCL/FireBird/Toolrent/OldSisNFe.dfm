object FmOldSisNFe: TFmOldSisNFe
  Left = 339
  Top = 185
  Caption = 'PRD-GRUPO-047 :: Item de Tabela de Presta'#231#227'o de Servi'#231'o'
  ClientHeight = 629
  ClientWidth = 1104
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1104
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1056
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1008
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 282
        Height = 32
        Caption = 'NFe-s Sistema Anterior'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 282
        Height = 32
        Caption = 'NFe-s Sistema Anterior'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 282
        Height = 32
        Caption = 'NFe-s Sistema Anterior'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1104
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1100
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1104
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 958
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 956
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtXML: TBitBtn
        Tag = 14
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&XML'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtXMLClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1104
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1104
      Height = 61
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object RGSistema: TRadioGroup
        Left = 0
        Top = 0
        Width = 165
        Height = 61
        Align = alLeft
        Caption = ' Sistema: '
        ItemIndex = 0
        Items.Strings = (
          'Sistema anterior'
          'Sistama anterior ao anterior')
        TabOrder = 0
        OnClick = RGSistemaClick
      end
      object Panel6: TPanel
        Left = 165
        Top = 0
        Width = 939
        Height = 61
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label17: TLabel
          Left = 4
          Top = 8
          Width = 35
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cliente:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label1: TLabel
          Left = 532
          Top = 12
          Width = 63
          Height = 13
          Caption = 'N'#250'mero NFe:'
        end
        object EdCliente: TdmkEditCB
          Left = 48
          Top = 4
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cliente'
          UpdCampo = 'Cliente'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 104
          Top = 4
          Width = 417
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          KeyField = 'Codigo'
          ListField = 'NO_ENT'
          ListSource = DsClientes
          ParentFont = False
          TabOrder = 1
          dmkEditCB = EdCliente
          QryCampo = 'Cliente'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdPsqTxt: TdmkEdit
          Left = 48
          Top = 28
          Width = 473
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdNumeroNFe: TdmkEdit
          Left = 532
          Top = 28
          Width = 77
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 61
      Width = 1104
      Height = 406
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Splitter1: TSplitter
        Left = 0
        Top = 177
        Width = 1104
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitTop = 120
        ExplicitWidth = 812
      end
      object DBGNFe: TDBGrid
        Left = 0
        Top = 0
        Width = 1104
        Height = 177
        Align = alClient
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 180
        Width = 1104
        Height = 226
        Align = alBottom
        DataSource = DsNotaFiscalItens
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrNotaFiscal: TMySQLQuery
    Database = Dmod.DBAnt
    AfterOpen = QrNotaFiscalAfterOpen
    BeforeClose = QrNotaFiscalBeforeClose
    AfterScroll = QrNotaFiscalAfterScroll
    SQL.Strings = (
      'SELECT * FROM notafiscal')
    Left = 384
    Top = 284
    object QrNotaFiscalLOJA: TWideStringField
      FieldName = 'LOJA'
      Required = True
      Size = 3
    end
    object QrNotaFiscalNOTA: TLargeintField
      FieldName = 'NOTA'
      Required = True
    end
    object QrNotaFiscalMODELO: TLargeintField
      FieldName = 'MODELO'
      Required = True
    end
    object QrNotaFiscalSERIE: TWideStringField
      FieldName = 'SERIE'
      Required = True
      Size = 3
    end
    object QrNotaFiscalCFOP: TLargeintField
      FieldName = 'CFOP'
      Required = True
    end
    object QrNotaFiscalCANCELADA: TWideStringField
      FieldName = 'CANCELADA'
      Size = 1
    end
    object QrNotaFiscalCLIENTE: TLargeintField
      FieldName = 'CLIENTE'
    end
    object QrNotaFiscalRAZAOSOCIAL: TWideStringField
      FieldName = 'RAZAOSOCIAL'
      Size = 100
    end
    object QrNotaFiscalUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrNotaFiscalCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 15
    end
    object QrNotaFiscalINSCRICAOESTADUAL: TWideStringField
      FieldName = 'INSCRICAOESTADUAL'
      Size = 15
    end
    object QrNotaFiscalVENDEDOR: TLargeintField
      FieldName = 'VENDEDOR'
    end
    object QrNotaFiscalORCAMENTO: TLargeintField
      FieldName = 'ORCAMENTO'
    end
    object QrNotaFiscalEMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrNotaFiscalTOTALNOTA: TFloatField
      FieldName = 'TOTALNOTA'
    end
    object QrNotaFiscalBASEICMSTRIB: TFloatField
      FieldName = 'BASEICMSTRIB'
    end
    object QrNotaFiscalICMSTRIB: TFloatField
      FieldName = 'ICMSTRIB'
    end
    object QrNotaFiscalBASEICMSSUB: TFloatField
      FieldName = 'BASEICMSSUB'
    end
    object QrNotaFiscalICMSSUB: TFloatField
      FieldName = 'ICMSSUB'
    end
    object QrNotaFiscalVALORFRETE: TFloatField
      FieldName = 'VALORFRETE'
    end
    object QrNotaFiscalVALORIPI: TFloatField
      FieldName = 'VALORIPI'
    end
    object QrNotaFiscalTRANSPORTADORA: TLargeintField
      FieldName = 'TRANSPORTADORA'
    end
    object QrNotaFiscalQUANTIDADE: TLargeintField
      FieldName = 'QUANTIDADE'
    end
    object QrNotaFiscalESPECIE: TWideStringField
      FieldName = 'ESPECIE'
    end
    object QrNotaFiscalMARCA: TWideStringField
      FieldName = 'MARCA'
    end
    object QrNotaFiscalNUMERO: TWideStringField
      FieldName = 'NUMERO'
    end
    object QrNotaFiscalPESO: TLargeintField
      FieldName = 'PESO'
    end
    object QrNotaFiscalPESOLIQUIDO: TLargeintField
      FieldName = 'PESOLIQUIDO'
    end
    object QrNotaFiscalUSUARIO: TWideStringField
      FieldName = 'USUARIO'
      Size = 10
    end
    object QrNotaFiscalRECIBONFE: TWideStringField
      FieldName = 'RECIBONFE'
      Size = 50
    end
    object QrNotaFiscalDANFE: TWideStringField
      FieldName = 'DANFE'
      Size = 50
    end
    object QrNotaFiscalNFEPENDENTE: TWideStringField
      FieldName = 'NFEPENDENTE'
      Size = 1
    end
    object QrNotaFiscalRECIBORETORNONFE: TWideStringField
      FieldName = 'RECIBORETORNONFE'
      Size = 50
    end
    object QrNotaFiscalMOTIVORETORNONFE: TWideStringField
      FieldName = 'MOTIVORETORNONFE'
      Size = 120
    end
    object QrNotaFiscalALIQUOTA: TFloatField
      FieldName = 'ALIQUOTA'
    end
    object QrNotaFiscalFRETE: TLargeintField
      FieldName = 'FRETE'
    end
    object QrNotaFiscalOBSERVACAO: TWideStringField
      FieldName = 'OBSERVACAO'
      Size = 80
    end
    object QrNotaFiscalCIF: TLargeintField
      FieldName = 'CIF'
    end
    object QrNotaFiscalDADOSADICIONAIS: TWideMemoField
      FieldName = 'DADOSADICIONAIS'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNotaFiscalIMPRESSO: TWideStringField
      FieldName = 'IMPRESSO'
      Size = 1
    end
    object QrNotaFiscalENVIADOEMAIL: TWideStringField
      FieldName = 'ENVIADOEMAIL'
      Size = 1
    end
    object QrNotaFiscalPROTOCOLOCANCELAMENTO: TWideStringField
      FieldName = 'PROTOCOLOCANCELAMENTO'
      Size = 50
    end
    object QrNotaFiscalDATAHORACANCELAMENTO: TWideStringField
      FieldName = 'DATAHORACANCELAMENTO'
      Size = 50
    end
    object QrNotaFiscalPIS: TFloatField
      FieldName = 'PIS'
    end
    object QrNotaFiscalCOFINS: TFloatField
      FieldName = 'COFINS'
    end
    object QrNotaFiscalOUTRASDESPESAS: TFloatField
      FieldName = 'OUTRASDESPESAS'
    end
    object QrNotaFiscalTIPOPESSOA: TWideStringField
      FieldName = 'TIPOPESSOA'
      Size = 1
    end
    object QrNotaFiscalTIPOPAGAMENTO: TWideStringField
      FieldName = 'TIPOPAGAMENTO'
      Size = 1
    end
    object QrNotaFiscalCOMPUTADOR: TWideStringField
      FieldName = 'COMPUTADOR'
      Size = 50
    end
    object QrNotaFiscalSEGURO: TFloatField
      FieldName = 'SEGURO'
    end
    object QrNotaFiscalPLACA: TWideStringField
      FieldName = 'PLACA'
      Size = 7
    end
    object QrNotaFiscalUFPLACA: TWideStringField
      FieldName = 'UFPLACA'
      Size = 2
    end
    object QrNotaFiscalTOTALPRODUTO: TFloatField
      FieldName = 'TOTALPRODUTO'
    end
    object QrNotaFiscalIMPOSTOA: TFloatField
      FieldName = 'IMPOSTOA'
    end
    object QrNotaFiscalCONTINGENCIAPENDENTE: TWideStringField
      FieldName = 'CONTINGENCIAPENDENTE'
      Size = 1
    end
    object QrNotaFiscalIMPRIMEPRECOLIQUIDO: TWideStringField
      FieldName = 'IMPRIMEPRECOLIQUIDO'
      Size = 1
    end
    object QrNotaFiscalDESCONTO: TFloatField
      FieldName = 'DESCONTO'
    end
    object QrNotaFiscalCLIENTEERRADO: TWideStringField
      FieldName = 'CLIENTEERRADO'
      Size = 10
    end
    object QrNotaFiscalCONDICAO: TLargeintField
      FieldName = 'CONDICAO'
    end
    object QrNotaFiscalORIGEM: TLargeintField
      FieldName = 'ORIGEM'
    end
    object QrNotaFiscalESPECIAL: TWideStringField
      FieldName = 'ESPECIAL'
      Size = 1
    end
  end
  object DsNotaFiscal: TDataSource
    DataSet = QrNotaFiscal
    Left = 384
    Top = 332
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 116
    Top = 64
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 255
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 116
    Top = 120
  end
  object QrNotaFiscalItens: TMySQLQuery
    Database = Dmod.DBAnt
    SQL.Strings = (
      'SELECT * FROM notafiscalitens'
      'WHERE NOTA=1627')
    Left = 384
    Top = 381
    object QrNotaFiscalItensLOJA: TWideStringField
      FieldName = 'LOJA'
      Required = True
      Size = 3
    end
    object QrNotaFiscalItensNOTA: TLargeintField
      FieldName = 'NOTA'
      Required = True
    end
    object QrNotaFiscalItensMODELO: TLargeintField
      FieldName = 'MODELO'
      Required = True
    end
    object QrNotaFiscalItensSERIE: TWideStringField
      FieldName = 'SERIE'
      Required = True
      Size = 3
    end
    object QrNotaFiscalItensCFOP: TLargeintField
      FieldName = 'CFOP'
      Required = True
    end
    object QrNotaFiscalItensSEQUENCIA: TLargeintField
      FieldName = 'SEQUENCIA'
      Required = True
    end
    object QrNotaFiscalItensPRODUTO: TLargeintField
      FieldName = 'PRODUTO'
    end
    object QrNotaFiscalItensNCM: TWideStringField
      FieldName = 'NCM'
      Size = 8
    end
    object QrNotaFiscalItensDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Size = 200
    end
    object QrNotaFiscalItensUNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 10
    end
    object QrNotaFiscalItensQUANTIDADE: TFloatField
      FieldName = 'QUANTIDADE'
    end
    object QrNotaFiscalItensFISCAL: TWideStringField
      FieldName = 'FISCAL'
      Size = 4
    end
    object QrNotaFiscalItensDESCONTO: TFloatField
      FieldName = 'DESCONTO'
    end
    object QrNotaFiscalItensVALORIPI: TFloatField
      FieldName = 'VALORIPI'
    end
    object QrNotaFiscalItensALIQUOTAICMS: TFloatField
      FieldName = 'ALIQUOTAICMS'
    end
    object QrNotaFiscalItensALIQUOTAIPI: TFloatField
      FieldName = 'ALIQUOTAIPI'
    end
    object QrNotaFiscalItensREDUCAOBASE: TFloatField
      FieldName = 'REDUCAOBASE'
    end
    object QrNotaFiscalItensBASEALIQUOTAICMS: TFloatField
      FieldName = 'BASEALIQUOTAICMS'
    end
    object QrNotaFiscalItensVALORICMS: TFloatField
      FieldName = 'VALORICMS'
    end
    object QrNotaFiscalItensREDUCAOA: TFloatField
      FieldName = 'REDUCAOA'
    end
    object QrNotaFiscalItensREDUCAOB: TFloatField
      FieldName = 'REDUCAOB'
    end
    object QrNotaFiscalItensREDUCAOC: TFloatField
      FieldName = 'REDUCAOC'
    end
    object QrNotaFiscalItensREDUCAOD: TFloatField
      FieldName = 'REDUCAOD'
    end
    object QrNotaFiscalItensPIS: TFloatField
      FieldName = 'PIS'
    end
    object QrNotaFiscalItensCOFINS: TFloatField
      FieldName = 'COFINS'
    end
    object QrNotaFiscalItensSEGURO: TFloatField
      FieldName = 'SEGURO'
    end
    object QrNotaFiscalItensOUTRASDESPESAS: TFloatField
      FieldName = 'OUTRASDESPESAS'
    end
    object QrNotaFiscalItensVALORFRETE: TFloatField
      FieldName = 'VALORFRETE'
    end
    object QrNotaFiscalItensBASECALCULOIPI: TFloatField
      FieldName = 'BASECALCULOIPI'
    end
    object QrNotaFiscalItensICMSSUBSTITUIDO: TFloatField
      FieldName = 'ICMSSUBSTITUIDO'
    end
    object QrNotaFiscalItensBASEICMSSUBSTITUIDO: TFloatField
      FieldName = 'BASEICMSSUBSTITUIDO'
    end
    object QrNotaFiscalItensMVA: TFloatField
      FieldName = 'MVA'
    end
    object QrNotaFiscalItensCSOSN: TWideStringField
      FieldName = 'CSOSN'
      Size = 4
    end
    object QrNotaFiscalItensBASEICMSUB: TFloatField
      FieldName = 'BASEICMSUB'
    end
    object QrNotaFiscalItensICMSUB: TFloatField
      FieldName = 'ICMSUB'
    end
    object QrNotaFiscalItensIMPOSTOA: TFloatField
      FieldName = 'IMPOSTOA'
    end
    object QrNotaFiscalItensICMSSUBSPED: TFloatField
      FieldName = 'ICMSSUBSPED'
    end
    object QrNotaFiscalItensVALORIPISPED: TFloatField
      FieldName = 'VALORIPISPED'
    end
    object QrNotaFiscalItensPRECOBRUTO: TFloatField
      FieldName = 'PRECOBRUTO'
    end
    object QrNotaFiscalItensCEST: TWideStringField
      FieldName = 'CEST'
      Size = 10
    end
    object QrNotaFiscalItensVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object DsNotaFiscalItens: TDataSource
    DataSet = QrNotaFiscalItens
    Left = 388
    Top = 428
  end
  object QrNotaAntiga: TMySQLQuery
    Database = Dmod.DBAnt
    SQL.Strings = (
      'SELECT ant.*  '
      'FROM notaantiga ant')
    Left = 196
    Top = 345
    object QrNotaAntigaNOTAFISCAL: TLargeintField
      FieldName = 'NOTAFISCAL'
    end
    object QrNotaAntigaMODELO: TLargeintField
      FieldName = 'MODELO'
    end
    object QrNotaAntigaDATA: TDateField
      FieldName = 'DATA'
    end
    object QrNotaAntigaCLIENTE: TWideStringField
      FieldName = 'CLIENTE'
      Size = 100
    end
    object QrNotaAntigaCPFCGC: TWideStringField
      FieldName = 'CPFCGC'
      Size = 50
    end
    object QrNotaAntigaCHAVENFE: TWideStringField
      FieldName = 'CHAVENFE'
      Size = 50
    end
    object QrNotaAntigaVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object DsNotaAntiga: TDataSource
    DataSet = QrNotaAntiga
    Left = 196
    Top = 392
  end
  object QrTabelaXML: TMySQLQuery
    Database = FmDB_Converte_Tisolin.DBAnt
    Left = 564
    Top = 373
  end
end
