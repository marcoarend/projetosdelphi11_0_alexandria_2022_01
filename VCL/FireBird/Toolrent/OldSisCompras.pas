unit OldSisCompras;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB;

type
  TFmOldSisCompras = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel3: TPanel;
    QrFornecedores: TMySQLQuery;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNO_ENT: TWideStringField;
    DsFornecedores: TDataSource;
    QrGraGXVend: TMySQLQuery;
    QrGraGXVendControle: TIntegerField;
    QrGraGXVendReferencia: TWideStringField;
    QrGraGXVendNO_GG1: TWideStringField;
    QrGraGXVendGraGruX: TIntegerField;
    QrGraGXVendItemValr: TFloatField;
    QrGraGXVendItemUnid: TIntegerField;
    QrGraGXVendGraGru1: TIntegerField;
    QrGraGXVendNCM: TWideStringField;
    QrGraGXVendMaxPercDesco: TFloatField;
    QrGraGXVendUnidMed: TIntegerField;
    QrGraGXVendEAN13: TWideStringField;
    DsGraGXVend: TDataSource;
    Label15: TLabel;
    EdEAN13: TdmkEdit;
    Label1: TLabel;
    EdGraGruX: TdmkEditCB;
    EdReferencia: TdmkEdit;
    Label2: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    Label7: TLabel;
    Label17: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    QrPesq1: TMySQLQuery;
    QrPesq1Controle: TIntegerField;
    QrPesq1EAN13: TWideStringField;
    QrPesq2: TMySQLQuery;
    QrPesq2Referencia: TWideStringField;
    QrPesq2EAN13: TWideStringField;
    QrCompras: TMySQLQuery;
    DsCompras: TDataSource;
    QrComprasDataNF: TDateField;
    QrComprasEntrada: TDateField;
    QrComprasRAZAOSOCIAL: TWideStringField;
    QrComprasFORNECEDOR: TLargeintField;
    QrComprasNOTAFISCAL: TLargeintField;
    QrComprasMODELO: TLargeintField;
    QrComprasSERIE: TWideStringField;
    QrComprasPRODUTO: TLargeintField;
    QrComprasQUANTIDADE: TFloatField;
    QrComprasPRECO: TFloatField;
    QrComprasCUSTO: TFloatField;
    QrComprasSEQUENCIA: TLargeintField;
    QrComprasMARGEM: TFloatField;
    QrComprasALIQUOTAICM: TFloatField;
    QrComprasATUALIZA: TWideStringField;
    QrComprasVALORICMSTRIBCOMPRA: TFloatField;
    QrComprasALIQUOTAIPICOMPRA: TFloatField;
    QrComprasVALORIPICOMPRA: TFloatField;
    QrComprasMVACOMPRA: TFloatField;
    QrComprasALIQUOTAICMSTCOMPRA: TFloatField;
    QrComprasVALORICMSSTCOMPRA: TFloatField;
    QrComprasVALORFRETENOTACOMPRA: TFloatField;
    QrComprasVALORFRETETRANSPORTADORACOMPRA: TFloatField;
    QrComprasOUTRASDESPESASCOMPRA: TFloatField;
    QrComprasCUSTOREPOSICAO: TFloatField;
    QrComprasNOVOPRECOVENDA: TFloatField;
    QrComprasCODIGOBARRAS: TWideStringField;
    QrComprasCST: TWideStringField;
    QrComprasCFOP: TLargeintField;
    QrComprasDESCONTO: TFloatField;
    QrComprasBASECALCULOICMS: TFloatField;
    QrComprasALIQUOTAICMS: TFloatField;
    QrComprasVALORICMS: TFloatField;
    QrComprasBASEICMSSUBSTITUICAO: TFloatField;
    QrComprasALIQUOTAICMSSUBSTITUICAODESTINO: TFloatField;
    QrComprasVALORICMSSUBSTITUICAO: TFloatField;
    QrComprasBASECALCULOIPI: TFloatField;
    QrComprasIPI: TFloatField;
    QrComprasVALORIPI: TFloatField;
    QrComprasCST_PIS: TLargeintField;
    QrComprasBASECALCULOPIS: TFloatField;
    QrComprasALIQUOTAPIS: TFloatField;
    QrComprasQTDEBASECALCULOPIS: TFloatField;
    QrComprasALIQUOTAPISREAL: TFloatField;
    QrComprasVALORPIS: TFloatField;
    QrComprasCST_COFINS: TLargeintField;
    QrComprasBASECALCULOCOFINS: TFloatField;
    QrComprasALIQUOTACOFINS: TFloatField;
    QrComprasQTDEBASECALCULOCOFINS: TFloatField;
    QrComprasALIQUOTACOFINSREAL: TFloatField;
    QrComprasVALORCOFINS: TFloatField;
    QrComprasQUANTIDADESPED: TFloatField;
    QrComprasTOTALITENSSPED: TFloatField;
    QrComprasFRETE: TFloatField;
    QrComprasSEGURO: TFloatField;
    QrComprasOUTRASDESPESAS: TFloatField;
    QrComprasALIQUOTAICMSTLOCAL: TFloatField;
    QrComprasFISCAL: TLargeintField;
    QrComprasNCM: TWideStringField;
    QrComprasCEST: TWideStringField;
    QrComprasCFOPORIGEM: TLargeintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdEAN13Change(Sender: TObject);
    procedure EdEAN13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdEAN13Redefinido(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdReferenciaChange(Sender: TObject);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    //procedure ReopenStqaLocIts(Controle: Integer);
    procedure PesquisaPorEAN13(Limpa: Boolean);
    procedure PesquisaPorGraGruX();
    procedure PesquisaPorReferencia(Limpa: Boolean);
  public
    { Public declarations }
  end;

  var
  FmOldSisCompras: TFmOldSisCompras;

implementation

uses UnMyObjects, Module, DmkDAC_PF, MeuDBUses;

{$R *.DFM}

procedure TFmOldSisCompras.BtOKClick(Sender: TObject);
var
  I, Fornecedor, Produto: Integer;
  SQL_FORNECEDOR, SQL_Produto: String;
  SQL_VENDEDORES, Corda: String;
begin
  Fornecedor     := EdFornecedor.ValueVariant;
  Produto        := EdGraGruX.ValueVariant;
  SQL_FORNECEDOR := EmptyStr;
  SQL_PRODUTO    := EmptyStr;
  //
  if MyObjects.FIC((FOrnecedor = 0) and (Produto = 0), EdFornecedor,
  'Informe um fornecedor ou um produto!') then Exit;
  begin
{
    Corda := MyObjects.CordaDeZTOChecks(DBGVendedores, QrVendedores, 'VENDEDOR',
    (*PermiteTodos*) False);
    //
}
    SQL_FORNECEDOR := EmptyStr;
    if Fornecedor <> 0 then
     SQL_FORNECEDOR := 'AND FORNECEDOR=' + Geral.FF0(Fornecedor);
    //
    if Produto <> 0 then
     SQL_PRODUTO := 'AND PRODUTO=' + Geral.FF0(Produto);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCompras, Dmod.DBAnt, [
    'SELECT cab.DataNF, cab.Entrada, cli.RAZAOSOCIAL, its.* ',
    'FROM comprasitens its ',
    'LEFT JOIN compras cab ON ',
    '  cab.FORNECEDOR=its.FORNECEDOR AND ',
    '  cab.NOTAFISCAL=its.NOTAFISCAL AND ',
    '  cab.MODELO=its.MODELO AND ',
    '  cab.SERIE=its.SERIE ',
    'LEFT JOIN CLIENTE cli ON cli.CLIENTE=its.Fornecedor ',
    'WHERE its.PRODUTO <> 0 ',
    SQL_FORNECEDOR,
    SQL_PRODUTO,
    'ORDER BY cab.DataNF, its.SEQUENCIA DESC',
    '']);
    //Geral.MB_Teste(QrCompras.SQL.Text);
  end;
end;

procedure TFmOldSisCompras.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOldSisCompras.EdEAN13Change(Sender: TObject);
begin
  if EdEAN13.Focused then
    PesquisaPorEAN13(False);
end;

procedure TFmOldSisCompras.EdEAN13KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
  begin
    CBGraGruX.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmOldSisCompras.EdEAN13Redefinido(Sender: TObject);
var
  EAN13: String;
begin
  EAN13 := Geral.SoNumero_TT(StringReplace(EdEAN13.ValueVariant, #$D#$A, '', [rfReplaceAll]));
  if EAN13 <> EdEAN13.ValueVariant then
    EdEAN13.ValueVariant := EAN13;
end;

procedure TFmOldSisCompras.EdGraGruXChange(Sender: TObject);
begin
  if not EdReferencia.Focused then
    PesquisaPorGraGruX();
(*
  if EdGraGruX.ValueVariant <> 0 then
    DBEdNCM.DataField := 'NCM'
  else
    DBEdNCM.DataField := '';
*)
end;

procedure TFmOldSisCompras.EdGraGruXRedefinido(Sender: TObject);
begin
(*
  EdPercDesco.ValueVariant := 0;
  if ImgTipo.SQLType = stIns then
  begin
    EdPrcUni.ValueVariant := QrGraGXVendItemValr.Value;
    //FPrcUni               := QrGraGXVendItemValr.Value;
  end;
  ReopenEstoque();
*)
end;

procedure TFmOldSisCompras.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    PesquisaPorReferencia(False);
end;

procedure TFmOldSisCompras.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
  begin
    CBGraGruX.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
end;

procedure TFmOldSisCompras.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOldSisCompras.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedores, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXVend, Dmod.MyDB, [
  'SELECT ggx.Controle, ggx.GraGru1, ggx.EAN13, ',
  'gg1.Referencia, gg1.Nome NO_GG1, gg1.NCM, gg1.UnidMed, ',
  'ggo.GraGruX, ggo.ItemValr, ggo.ItemUnid, ggo.MaxPercDesco ',
  'FROM gragxvend ggo  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=ggo.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'ORDER BY NO_GG1',
  '']);
end;

procedure TFmOldSisCompras.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOldSisCompras.PesquisaPorEAN13(Limpa: Boolean);
var
  EAN13: String;
begin
  EAN13 := EdEAN13.Text;
  if Length(EAN13) = 13 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
    'SELECT ggx.Controle, ggx.EAN13 ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
    'WHERE ggx.EAN13="' + EAN13 + '"' ,
    'AND NOT (ggo.GraGruX IS NULL)',
    'AND ggo.Aplicacao <> 0 ',
    //
    'AND ggx.Ativo=1 ',
    '']);
    //
    //Geral.MB_Teste(QrPesq1.SQL.Text);
    //
    QrPesq1.Open;
    if QrPesq1.RecordCount > 0 then
    begin
      if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
      begin
        EdGraGruX.ValueVariant := QrPesq1Controle.Value;
      end;
      if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
        CBGraGruX.KeyValue := QrPesq1Controle.Value;
      //
(*
      if QrEstoqueQtde.Value > 0 then
        DBEdQtde.SetFocus
      else
        Geral.MB_Info('Mercadoria sem estoque positivo!');
*)
    end else if Limpa then
    begin
      EdEAN13.ValueVariant := '';
      EdReferencia.ValueVariant := '';
    end;
  end;
end;

procedure TFmOldSisCompras.PesquisaPorGraGruX();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT gg1.Referencia, ggx.EAN13 ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  ggo.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND NOT (ggo.GraGruX IS NULL) ',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
    //
    if EdEAN13.ValueVariant <> QrPesq2EAN13.Value then
      EdEAN13.ValueVariant := QrPesq2EAN13.Value;
    //
  end else
  begin
    EdReferencia.ValueVariant := '';
    EdEAN13.ValueVariant := '';
  end;
end;

procedure TFmOldSisCompras.PesquisaPorReferencia(Limpa: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT ggx.Controle, ggx.EAN13 ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdReferencia.Text + '"' ,
  'AND NOT (ggo.GraGruX IS NULL)',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    if EdEAN13.ValueVariant <> QrPesq1EAN13.Value then
      EdEAN13.ValueVariant := QrPesq1EAN13.Value;
    if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
      EdGraGruX.ValueVariant := QrPesq1Controle.Value;
    if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
      CBGraGruX.KeyValue := QrPesq1Controle.Value;
  end else if Limpa then
  begin
    EdReferencia.ValueVariant := '';
    EdEAN13.ValueVariant := '';
  end;
end;

(*
procedure TFmOldSisCompras.ReopenStqaLocIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqaLocIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM stqalocits ',
  '']);
end;
*)

end.
