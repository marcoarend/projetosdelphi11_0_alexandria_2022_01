unit UnXXe_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, mySQLDbTables,
  DB, dmkGeral, UnDmkEnums, UnMsgInt, DmkCoding, dmkEdit, dmkRadioGroup,
  dmkMemo, dmkEditDateTimePicker, dmkCheckGroup, dmkeditCB, dmkDBLookupComboBox,
  CAPICOM_TLB, RTTi, ShellApi,
//Precisa?
  //XMLDoc, xmldom, XMLIntf, msxmldom,
  //UrlMon, InvokeRegistry, Rio,
  //SOAPHTTPClient, SOAPHTTPTrans, JwaWinCrypt, WinInet, DateUtils,
  //OleCtrls, SHDocVw, Grids, DBGrids, DmkDAC_PF,
  (* 2023-12-19
  ACBrNFeWebServices, ACBrDFeXsLibXml2,
  *)
  ACBrDFeSSL, UnDmkProcFunc;

const
  CO_COD_evexxe110110CCe           = 110110;
  CO_COD_evexxe110111Can           = 110111;
  CO_COD_evexxe110112Enc           = 110112;
  CO_COD_evexxe110113EPEC          = 110113;
  CO_COD_evexxe110114IncCondutor   = 110114;
  CO_COD_evexxe110140EPEC          = 110140;
  CO_COD_evexxe110160RegMultimodal = 110160;
  CO_COD_evexxe240130AutCTeComplem = 240130;
  CO_COD_evexxe240131CanCTeComplem = 240131;
  CO_COD_evexxe240140CTeSubst      = 240140;
  CO_COD_evexxe240150CTeAnul       = 240150;
  CO_COD_evexxe240160CTeMultimodal = 240150;
  CO_COD_evexxe310620RegPassManu   = 310620;
  CO_COD_evexxe510620RegPassBRId   = 510620;
  //
  CO_TXT_evexxe110110CCe           = 'Carta de Corre��o';
  CO_TXT_evexxe110111Can           = 'Cancelamento';
  CO_TXT_evexxe110112Enc           = 'Encerramento';
  CO_TXT_evexxe110113EPEC          = 'EPEC';
  CO_TXT_evexxe110114IncCondutor   = 'Inclus�o de Condutor';
  CO_TXT_evexxe110140EPEC          = 'EPEC';
  CO_TXT_evexxe110160RegMultimodal = 'Registros do Multimodal';
  CO_TXT_evexxe240130AutCTeComplem = 'Autorizado CT-e Complementar';
  CO_TXT_evexxe240131CanCTeComplem = 'Cancelado CT-e Complementar';
  CO_TXT_evexxe240140CTeSubst      = 'CT-e de Substitui��o';
  CO_TXT_evexxe240150CTeAnul       = 'CT-e de Anula��o';
  CO_TXT_evexxe240160CTeMultimodal = 'Multimodal';
  CO_TXT_evexxe310620RegPassManu   = 'Registro de Passagem';
  CO_TXT_evexxe510620RegPassBRId   = 'Registro de Passagem BRId';
  //
  MaxEventoXXe = Integer(High(TEventoXXe));
  sEventoXXe: array[0..MaxEventoXXe] of string = (
  CO_TXT_evexxe110110CCe           ,
  CO_TXT_evexxe110111Can           ,
  CO_TXT_evexxe110112Enc           ,
  CO_TXT_evexxe110113EPEC          ,
  CO_TXT_evexxe110114IncCondutor   ,
  CO_TXT_evexxe110140EPEC          ,
  CO_TXT_evexxe110160RegMultimodal ,
  CO_TXT_evexxe240130AutCTeComplem ,
  CO_TXT_evexxe240131CanCTeComplem ,
  CO_TXT_evexxe240140CTeSubst      ,
  CO_TXT_evexxe240150CTeAnul       ,
  CO_TXT_evexxe240160CTeMultimodal ,
  CO_TXT_evexxe310620RegPassManu   ,
  CO_TXT_evexxe510620RegPassBRId
  );
  nEventoXXe: array[0..MaxEventoXXe] of Integer = (
  CO_COD_evexxe110110CCe           ,
  CO_COD_evexxe110111Can           ,
  CO_COD_evexxe110112Enc           ,
  CO_COD_evexxe110113EPEC          ,
  CO_COD_evexxe110114IncCondutor   ,
  CO_COD_evexxe110140EPEC          ,
  CO_COD_evexxe110160RegMultimodal ,
  CO_COD_evexxe240130AutCTeComplem ,
  CO_COD_evexxe240131CanCTeComplem ,
  CO_COD_evexxe240140CTeSubst      ,
  CO_COD_evexxe240150CTeAnul       ,
  CO_COD_evexxe240160CTeMultimodal ,
  CO_COD_evexxe310620RegPassManu   ,
  CO_COD_evexxe510620RegPassBRId
  );

(*
110110 Carta de Corre��o
110111 Cancelamento
110112 Encerramento
110113 EPEC
110114 Inclus�o de Condutor
110140 EPEC
110160 Registros do Multimodal
240130 Autorizado CT-e Complementar
240131 Cancelado CT-e Complementar
240140 CT-e de Substitui��o
240150 CT-e de Anula��o
240160 Multimodal
310620 Registro de Passagem
510620 Registro de Passagem BRId
*)

var
  VAR_CERTIFICADO_DIGITAL: ICertificate2;


type
  TUnXXe_PF = class(TObject)
  private
    { Private declarations }
    function  TabelaCabAXXe(TipoXXe: TTipoXXe): String;
    function  TabelaEveRCabXXe(TipoXXe: TTipoXXe): String;
    function  StatusOK_XXe(TipoXXe: TTipoXXe): String;
  public
    { Public declarations }
    function  AlteraPedido_TipoNF(Empresa, FatPedCab, Cliente, RegrFiscal,
              De, Para: Integer): Boolean;
    function  PermiteCriacaoDeEvento(TipoXXe: TTipoXXe; TpEvento: TEventoXXe;
              FatID, FatNum, Empresa: Integer): Boolean;
    function  Ajusta_dh_XXe(var dhRecbto: String): Boolean;
    function  Ajusta_dh_XXe_UTC(var dh: String;  var UTC: Double): Boolean;
    function  Alltrim(const Search: string): string;
    function  ChaveDeAcessoDesmontada(const Chave: String;
              const VersaoXXe: Double): String;
    function  DecimalPonto(sValor: string): string;
    function  DesmontaChaveDeAcesso(const Chave: String; const Versao: Double;
              var UF_IBGE, AAMM, CNPJ, DocMod, DocSer, DocNum, tpEmis, CodSeg:
              String): Boolean;
    function  ZeraCodSegChaveDeAcesso(const ChaveAtual: String; var ChaveNova:
              String): Boolean;
    function  FDT_XXe_UTC(DataHora: TDateTime; TZD_UTC: Variant): String;
    function  FormataID_XXe(Id: String): String;
    function  JaExisteEventoUnicoVinculado(TipoXXe: TTipoXXe; TpEvento:
              TEventoXXe; FatID, FatNum, Empresa: Integer): Boolean;
    function  ObtemCertificado(const NumeroSerial: String;
              out Cert: ICertificate2): Boolean;
    function  ObtemNomeAmbiente(Ambiente: String): String;
(*
    function  ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
    function  ObtemNomeDaTag(Tag: TTipoTagXML): String;
*)
    function  SeparaIntervalosDeTamanhosDeXXe(const Source: String;
              var DestTraco, DestVirgula, FormatStr: String): Boolean;
    function  SoNumeroDeTamanhoDeCampoDeXXe(const Source: String;
              var Dest: String): Boolean;
    function  StrZero(Num: Real; Zeros, Deci: Integer): String;
    function  TextoAmbiente(Ambiente: Byte): String;
    function  TipoDoc_ide_Mod(ide_Mod: Integer): String;
    procedure ValidaXML_XXe(ArquivoXML: String);
    function  ValidaTexto_XML(Txt, Codigo, ID: String): String;
    function  VarTypeValido(Valor: Variant; Nome: String): Boolean;
    function  VerificaChaveAcessoXXe(Chave: String; PermiteNulo: Boolean): Boolean;
    function  PerrmiteDesencerrarXXe(Status: Integer): Boolean;
    function  XXeEstahAutorizada(Codigo: Integer): Boolean;
    function  XXeEstahCancelada(Codigo: Integer): Boolean;
    function  XXeEstahCancelada_TXT(Codigo: Integer): String;
    function  XXeObtemStatusXXedeChave(TipoXXe: TTipoXXe; Chave: String): Integer;
    function  XXeDesmontaEPreencheObjectsChaveCTe(Preparando: Boolean; EdChave,
              EdModelo: TdmkEdit; CBModelo: TdmkDBLookupComboBox; EdSerie,
              EdNumero: TdmkEdit; EdEmitente: TdmkEditCB; CBEmitente:
              TdmkDBLookupComboBox; ControlFocus: TWinControl): Boolean;
    //procedure MostraMMC_SnapIn(); Movido para UnitWin
    procedure AvisoNaoImplemVerNFe(Versao: Integer);
    function  AssinarACBr(ConteudoXML: String): String;

  end;

var
  XXe_PF: TUnXXe_PF;

implementation

uses UnMLAGeral, MyDBCheck, (*20230330 NFeValidaXML_0001,*) DmkDAC_PF, Module, ModuleGeral,
  ModuleNFe_0000, UMySQLModule;

{ TUnXXe_PF }

function TUnXXe_PF.Ajusta_dh_XXe(var dhRecbto: String): Boolean;
var
  P: Integer;
begin
  P := pos('T', dhRecbto);
  if P > 0 then
    dhRecbto[P] := ' ';
  dhRecbto := Trim(dhRecbto);
  // 2014-11-15 Campo B07 do distDFeInt_v9.99.xsd
  P := pos('.', dhRecbto);
  if P > 0 then
    dhRecbto := Copy(dhRecbto, 1, P -1);
  // FIM 2014-11-15
  Result := True;
end;

function TUnXXe_PF.Ajusta_dh_XXe_UTC(var dh: String; var UTC: Double): Boolean;
var
  P, Sinal: Integer;
  A, B: String;
begin
  A := Copy(dh, 1, 19);
  B := Copy(dh, 20);
  P := pos('T', A);
  if P > 0 then
    A[P] := ' ';
  dh := A;
  //
  UTC := 0;
  Sinal := 1;
  if Length(B) > 0 then
  begin
    case Ord(B[1]) of
      Ord('-'):
      begin
        Sinal := -1;
        B := Copy(B, 2);
      end;
      Ord('0')..Ord('9'): ; // nada
      else B := Copy(B, 2);
    end;
    UTC := StrToTime(B) * Sinal;
  end;
  //
  Result := True;
end;

function TUnXXe_PF.Alltrim(const Search: string): string;
const
  BlackSpace = [#33..#126];
var
  Index: byte;
begin
  Index:=1;
  while (Index <= Length(Search)) and not (Search[Index] in BlackSpace) do
    begin
      Index:=Index + 1;
    end;
  Result:=Copy(Search, Index, 255);
  Index := Length(Result);
  while (Index > 0) and not (Result[Index] in BlackSpace) do
    begin
      Index:=Index - 1;
    end;
  Result := Copy(Result, 1, Index);
end;

function TUnXXe_PF.AlteraPedido_TipoNF(Empresa, FatPedCab, Cliente, RegrFiscal,
  De, Para: Integer): Boolean;
const
  EhServico = False;
var
  Continua, EhSubsTrib: Boolean;
  ID, Item_MadeBy: Integer;
  CFOP: String;
  ICMS_Aliq: Double;
  CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer;
begin
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
  'UPDATE stqmovnfsa ',
  'SET Tipo=' + Geral.FF0(Para), // VAR_FATID_000?
  'WHERE OriCodi=' + Geral.FF0(FatPedCab),
  'AND Tipo=' + Geral.FF0(De), //VAR_FATID_000?
  'AND Empresa=' + Geral.FF0(Empresa),
  '']));
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
  'UPDATE stqmovvala ',
  'SET Tipo=' + Geral.FF0(Para), // VAR_FATID_000?
  'WHERE OriCodi=' + Geral.FF0(FatPedCab),
  'AND Tipo=' + Geral.FF0(De), //VAR_FATID_000?
  'AND Empresa=' + Geral.FF0(Empresa),
  '']));
  //
  DmNFe_0000.ReopenSMVA(Para, Empresa, FatPedCab);
  //
  DmNFe_0000.QrSMVA.First;
  while not DmNFe_0000.QrSMVA.Eof do
  begin
    ID := DmNFe_0000.QrSMVAID.Value;
    Item_MadeBy := DmNFe_0000.QrSMVAMadeBy.Value;
    EhSubsTrib  := DmNFe_0000.QrSMVAUsaSubsTrib.Value = 1;
    Continua    := DmNFe_0000.ObtemNumeroCFOP_Emissao('', [0], Empresa,
                   Cliente, Item_MadeBy, RegrFiscal, EhServico, EhSubsTrib,
                   CFOP, ICMS_Aliq, CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio,
                   CFOP_SubsTrib);
    //
    if Continua then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovvala', False, [
      'CFOP',
      'CFOP_Contrib', 'CFOP_MesmaUF', 'CFOP_Proprio'
      ], [
      'ID'], [
      CFOP,
      CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio], [
      ID], False) then ;
    end else
    begin
      Exit;
    end;
    //
    DmNFe_0000.QrSMVA.Next;
  end;
  Result := True;
end;

function TUnXXe_PF.AssinarACBr(ConteudoXML: String): String;
var
  docElement, infElement: String;
  SignatureNode, SelectionNamespaces, IdSignature, IdAttr: String;
  s: String;
  //DFeSSLXmlSignLibXml2: TDFeSSLXmlSignLibXml2;
  ADFeSSL: TDFeSSL;
begin
////////////////////////////////////////////////////////////////////////////////
/// Na verdade n�o precisa assinar. � assinado pelo componente ACBrNFe.
/// Mas se precisar, aqui consegui assinar diretamente pelo componenete TDFeSSL!
///
///
  Result := '';
  DmNFe_0000.ConfigurarComponenteNFe();
  //
  ADFeSSL := TDFeSSL.Create;
  try
    with ADFeSSL do
    begin
      URLPFX := DmNFe_0000.ACBrNFe1.Configuracoes.Certificados.URLPFX;
      ArquivoPFX := DmNFe_0000.ACBrNFe1.Configuracoes.Certificados.ArquivoPFX;
      DadosPFX := DmNFe_0000.ACBrNFe1.Configuracoes.Certificados.DadosPFX;
      NameSpaceURI := DmNFe_0000.ACBrNFe1.GetNameSpaceURI;
      NumeroSerie := DmNFe_0000.ACBrNFe1.Configuracoes.Certificados.NumeroSerie;
      Senha := DmNFe_0000.ACBrNFe1.Configuracoes.Certificados.Senha;

      ProxyHost := DmNFe_0000.ACBrNFe1.Configuracoes.WebServices.ProxyHost;
      ProxyPass := DmNFe_0000.ACBrNFe1.Configuracoes.WebServices.ProxyPass;
      ProxyPort := DmNFe_0000.ACBrNFe1.Configuracoes.WebServices.ProxyPort;
      ProxyUser := DmNFe_0000.ACBrNFe1.Configuracoes.WebServices.ProxyUser;
      TimeOut := DmNFe_0000.ACBrNFe1.Configuracoes.WebServices.TimeOut;
      TimeOutPorThread := DmNFe_0000.ACBrNFe1.Configuracoes.WebServices.TimeOutPorThread;

      SSLCryptLib := DmNFe_0000.ACBrNFe1.Configuracoes.Geral.SSLCryptLib;
      SSLHttpLib := DmNFe_0000.ACBrNFe1.Configuracoes.Geral.SSLHttpLib;
      SSLXmlSignLib := DmNFe_0000.ACBrNFe1.Configuracoes.Geral.SSLXmlSignLib;
    end;
    //DFeSSLXmlSignLibXml2 := TDFeSSLXmlSignLibXml2(ADFeSSL);
    //
    docElement := 'evento';
    infElement := 'infEvento';

    //s := DFeSSLXmlSignLibXml2.Assinar(ConteudoXML, docElement, infElement);
    Result := ADFeSSL.Assinar(ConteudoXML, docElement, infElement, SignatureNode, SelectionNamespaces, IdSignature, IdAttr);
    //Geral.MB_info(Result);
  finally
    FreeAndNil(ADFeSSL);
  end;
  //Halt(0);
////////////////////////////////////////////////////////////////////////////////
//// fim N�o Precisa!!!???
////////////////////////////////////////////////////////////////////////////////
end;

procedure TUnXXe_PF.AvisoNaoImplemVerNFe(Versao: Integer);
begin
  Geral.MB_Aviso('Versao de NFe n�o implementada: ' +
  Geral.FFT_Dot(Versao / 100, 2, siPositivo));
end;

function TUnXXe_PF.ChaveDeAcessoDesmontada(const Chave: String;
  const VersaoXXe: Double): String;
var
  UF_IBGE, AAMM, CNPJ, ModDoc, SerDoc, NumDoc, tpEmis, CodSer, xTpDoc: String;
begin
  DesmontaChaveDeAcesso(Chave, VersaoXXe,
    UF_IBGE, AAMM, CNPJ, ModDoc, SerDoc, NumDoc, tpEmis, CodSer);
(* ini 2020-12-06
  case Geral.IMV('0' + Trim(ModDoc)) of
    55: xTpDoc := 'NF-e';
    57: xTpDoc := 'CT-e';
    58: xTpDoc := 'MDF-e';
    else xTpDoc := '????';
  end;
*)
  xTpDoc := TipoDoc_ide_Mod(Geral.IMV('0' + Trim(ModDoc)));
  //fim 2020-12-06
  //
  Result :=  sLineBreak +
    'UF:           ' + UF_IBGE + sLineBreak +
    'AnoMes:       ' + AAMM    + sLineBreak +
    'CNPJ:         ' + CNPJ    + sLineBreak +
    'Modelo ' + XTpDoc + ':  ' + ModDoc  + sLineBreak +
    'S�rie ' + XTpDoc + ':       ' + SerDoc  + sLineBreak +
    'N�mero ' + XTpDoc + ':      ' + NumDoc  + sLineBreak +
    'Tipo emiss�o: ' + tpEmis  + sLineBreak +
    'N� Rand�mico: ' + CodSer  + sLineBreak;
end;

function TUnXXe_PF.DecimalPonto(sValor: string): string;
begin
  Result := StringReplace(sValor, '.', '',[rfReplaceAll]);
  Result := StringReplace(Result, ',', '.',[rfReplaceAll]);
end;

function TUnXXe_PF.DesmontaChaveDeAcesso(const Chave: String;
  const Versao: Double; var UF_IBGE, AAMM, CNPJ, DocMod, DocSer, DocNum,
  tpEmis, CodSeg: String): Boolean;
begin
  Result := VerificaChaveAcessoXXe(Chave, False);
  if Result then
  begin
    UF_IBGE := Copy(Chave,  1,  2);
    AAMM    := Copy(Chave,  3,  4);
    CNPJ    := Copy(Chave,  7, 14);
    DocMod  := Copy(Chave, 21,  2);
    DocSer  := Copy(Chave, 23,  3);
    DocNum  := Copy(Chave, 26,  9);
    tpEmis  := Copy(Chave, 35,  1);
    CodSeg  := Copy(Chave, 36,  8);
  end else
  begin
    UF_IBGE := '';
    AAMM    := '';
    CNPJ    := '';
    DocMod  := '';
    Docser  := '';
    DocNum  := '';
    tpEmis  := '';
    CodSeg  := '';
  end;
end;

function TUnXXe_PF.FDT_XXe_UTC(DataHora: TDateTime; TZD_UTC: Variant): String;
var
  Sinal: String;
begin
  if TZD_UTC <> Null then
  begin
    if TZD_UTC < 0 then
      Sinal := '-'
    else
      Sinal := '+';
    Result := FormatDateTime('yyyy-mm-dd', DataHora) + 'T' +
              FormatDateTime('hh:nn:ss', DataHora) + Sinal +
              FormatDateTime('hh:nn', TZD_UTC);
  end else
  begin
    Result := FormatDateTime('yyyy-mm-dd', DataHora) + 'T' +
              FormatDateTime('hh:nn:ss', DataHora);
  end;
end;

function TUnXXe_PF.FormataID_XXe(Id: String): String;
begin
  Result :=
    Copy(Id, 01, 04) + ' ' +
    Copy(Id, 05, 04) + ' ' +
    Copy(Id, 09, 04) + ' ' +
    Copy(Id, 13, 04) + ' ' +
    Copy(Id, 17, 04) + ' ' +
    Copy(Id, 21, 04) + ' ' +
    Copy(Id, 25, 04) + ' ' +
    Copy(Id, 29, 04) + ' ' +
    Copy(Id, 33, 04) + ' ' +
    Copy(Id, 37, 04) + ' ' +
    Copy(Id, 41, 04);
end;

function TUnXXe_PF.JaExisteEventoUnicoVinculado(TipoXXe: TTipoXXe; TpEvento:
  TEventoXXe; FatID, FatNum, Empresa: Integer): Boolean;
var
  Qry: TmySQLQuery;
  Tabela, Stats: String;
begin
  Result := False;
  Stats  := StatusOK_XXe(TipoXXe);
  Tabela := TabelaEveRCabXXe(TipoXXe);
  Qry := TmySQLQuery.Create(Dmod.MyDB);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + LowerCase(Tabela),
    'WHERE FatID=' + Geral.FF0(FatID),
    'AND FatNum=' + Geral.FF0(FatNum),
    'AND Empresa='+ Geral.FF0(Empresa),
    'AND tpEvento=' + Geral.FF0(nEventoXXe[Integer(tpEvento)]),
    'AND Status IN (' + Stats + ') ',
    '']);
    Result := Qry.RecordCount > 0;
    if Result then
      Geral.MB_Aviso('J� existe um evento �nico e v�lido!');
  finally
    Qry.Free;
  end;
end;

{
function TUnXXe_PF.ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'ID'                    ;
    ttx_idLote      : Result := 'ID do lote'            ;
    ttx_versao      : Result := 'Vers�o XML'            ;
    ttx_tpAmb       : Result := 'Ambiente'              ;
    ttx_verAplic    : Result := 'Vers�o do aplicativo'  ;
    ttx_cOrgao      : Result := 'Org�o'                 ;
    ttx_cStat       : Result := 'Status'                ;
    ttx_xMotivo     : Result := 'Motivo'                ;
    ttx_cUF         : Result := 'UF'                    ;
    ttx_dhRecbto    : Result := 'Data/hora recibo'      ;
    ttx_chNFe       : Result := 'Chave NF-e'            ;
    ttx_nProt       : Result := 'Protocolo'             ;
    ttx_digVal      : Result := 'Valor "digest"'        ;
    ttx_ano         : Result := 'Ano'                   ;
    ttx_CNPJ        : Result := 'CNPJ'                  ;
    ttx_mod         : Result := 'Modelo NF'             ;
    ttx_serie       : Result := 'S�rie NF'              ;
    ttx_nNFIni      : Result := 'N�mero inicial NF'     ;
    ttx_nNFFin      : Result := 'N�mero final NF'       ;
    ttx_nRec        : Result := 'N�mero do Recibo'      ;
    ttx_tMed        : Result := 'Tempo m�dio'           ;
    ttx_tpEvento    : Result := 'Tipo de evento'        ;
    ttx_xEvento     : Result := 'Descri��o do evento'   ;
    ttx_CNPJDest    : Result := 'CNPJ do destinat�rio'  ;
    ttx_CPFDest     : Result := 'CPF do destinat�rio'   ;
    ttx_emailDest   : Result := 'E-mail do destinat�rio';
    ttx_nSeqEvento  : Result := 'Sequencial do evento'  ;
    ttx_dhRegEvento : Result := 'Data/hora registro'    ;
    ttx_dhResp      : Result := 'Data/hora msg resposta';
    ttx_indCont     : Result := 'Indicador continua��o' ;
    ttx_ultNSU      : Result := '�ltima NSU pesquisada' ;
    ttx_maxNSU      : Result := 'NSU M�xima encontrada' ;
    ttx_NSU         : Result := 'NSU do docum. fiscal'  ;
    ttx_CPF         : Result := 'CPF'                   ;
    ttx_xNome       : Result := 'Raz�o Social ou Nome'  ;
    ttx_IE          : Result := 'Inscri��o Estadual'    ;
    ttx_dEmi        : Result := 'Data da emiss�o'       ;
    ttx_tpNF        : Result := 'Tipo de opera��o NF-e' ;
    ttx_vNF         : Result := 'Valor total da NF-e'   ;
    ttx_cSitNFe     : Result := 'Situa��o da NF-e'      ;
    ttx_cSitConf    : Result := 'Sit. Manifes. Destinat';
    ttx_dhEvento    : Result := 'Data/hora do evento'   ;
    ttx_descEvento  : Result := 'Evento'                ;
    //ttx_x Correcao   : Result := x Correcao             ;
    ttx_cJust       : Result := 'C�digo da justifica��o';
    ttx_xJust       : Result := 'Texto da justifica��o' ;
    ttx_verEvento   : Result := 'Vers�o do evento'      ;
    ttx_schema      : Result := 'Schema XML'            ;
    ttx_docZip      : Result := 'Resumo do documento'   ;
    ttx_dhEmi       : Result := 'Data / hora de emiss�o';

    //
    else              Result := '? ? ? ? ?';
  end;
end;
}

function TUnXXe_PF.ObtemCertificado(const NumeroSerial: String;
  out Cert: ICertificate2): Boolean;
var
  Store        : IStore;
  Certs        : ICertificates;
  //CertContext  : ICertContext;
  i, J : Integer;
  Teste: String;
  //sXML: AnsiString;
begin
  Result := False;
  Cert   := nil;
  Store  := CoStore.Create;
  //Reposit�rios de Certifcados da M�quina

  Store.Open(CAPICOM_CURRENT_USER_STORE, 'MY', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);
  //Abre a lista de certificados

  Certs := Store.Certificates ;
  //Aloca todos os certificados instalados na m�quia

  i := 0;
  //loop de procura ao certificado requerido pelo n�mero serial
  while i < Certs.Count do
   begin
     //Cria objeto para acesso a leitura do certificado
     Cert := IInterface(Certs.Item[i + 1]) as ICertificate2;
     if UpperCase(Cert.SerialNumber) = Uppercase(NumeroSerial) then
     begin
       //se o n�mero do serial for igual ao que queremos utilizar
       Result := True;
       Exit;
     end;
     i := i + 1;
  end;
  //
  if Result = False then
  begin
    Cert := nil;
    ShowMessage('N�o foi poss�vel carregar o certificado:' +
      #13#10 + 'N�mero Serial: ' + NumeroSerial + '!');
    {
    Geral.MensagemBox('N�o foi poss�vel carregar o certificado:' +
      #13#10 + 'N�mero Serial: ' + NumeroSerial + '!'),
      'Aviso', MB_OK+MB_ICONWARNING);
    }
  end;
  //

  // 2020-10-31
  for J := 0 to Cert.ExtendedProperties.Count - 1 do
  begin
    teste := Cert.ExtendedProperties.Item[J];
    Geral.MB_Info(Teste);
  end;

end;

function TUnXXe_PF.ObtemNomeAmbiente(Ambiente: String): String;
var
  I: Integer;
begin
  if Ambiente = '' then Result := '? ? ? ? ?' else
    begin
    I := StrToInt(Ambiente);
    case I of
      1: Result := 'Produ��o';
      2: Result := 'Homologa��o';
      else Result := '*** Erro ***';
    end;
  end;
end;

function TUnXXe_PF.PermiteCriacaoDeEvento(TipoXXe: TTipoXXe; TpEvento:
  TEventoXXe; FatID, FatNum, Empresa: Integer): Boolean;
var
  Qry: TmySQLQuery;
  Tabela, No_Evento: String;
begin
  //True  => Permite
  //False => N�o permite
  //Descri��o: N�o permitir criar mais de um evento sem envi�-lo para o fisco
  //para n�o gerar erros no aplicativo e evitar cria��o de registros a toa
  //
  Result := True;
  //
  case TipoXXe of
    (*55*)tipoxxeNFe:  Tabela := 'nfeevercab';
    (*57*)tipoxxeCTe:  Tabela := 'cteevercab';
    (*58*)tipoxxeMDFe: Tabela := 'mdfeevercab';
    else
    begin
      Geral.MB_Erro('Tipo de documento fiscal eletr�nico n�o implementado! [2]');
      Tabela := '??eevercab'
    end;
  end;
  //
  No_Evento := sEventoXXe[Integer(TpEvento)];
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + Tabela,
      'WHERE FatID=' + Geral.FF0(FatID),
      'AND FatNum=' + Geral.FF0(FatNum),
      'AND Empresa='+ Geral.FF0(Empresa),
      'AND tpEvento=' + Geral.FF0(nEventoXXe[Integer(tpEvento)]),
      'AND (Status BETWEEN 10 AND 50 OR Status = 0) ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      Result := False;
      Geral.MB_Aviso(
      'Voc� deve primeiro enviar o evento ' +
      Geral.FF0(nEventoXXe[Integer(TpEvento)]) + ' - "' +
      NO_Evento + '" criado anteriormente ao FISCO antes de gerar um novo!');
    end;
  finally
    Qry.Free;
  end;
end;

function TUnXXe_PF.PerrmiteDesencerrarXXe(Status: Integer): Boolean;
begin
  if (Status in ([50..89, 91..199]))
  or ((Status > 299) and (Status < 400)) then
    Result := False
  else
    Result := True;
end;

function TUnXXe_PF.SeparaIntervalosDeTamanhosDeXXe(const Source: String;
  var DestTraco, DestVirgula, FormatStr: String): Boolean;
var
  i:Integer;
  Num, OldSep, NewSep, Texto: String;
  isTraco, isVirgula, DefTraco: Boolean;
begin
  Result := True;
  if Geral.SoNumero_TT(Source) = Source then
  begin
    DestTraco := Source;
    DestVirgula := '';
    Exit;
  end;
  Texto := Source + Char(2);
  DestTraco   := '';
  DestVirgula := '';
  FormatStr   := '';
  OldSep := '';
  NewSep := '';
  DefTraco := False;
  isTraco := False;
  isVirgula := False;
  if Length(Texto) > 0 then
  begin
    for i := 1 to Length(Texto) do
    begin
      if Texto[i] = Char(2) then
      begin
        if isTraco   then DestTraco   := DestTraco + Num else
        if isVirgula then DestVirgula := DestVirgula + Num;
      end else
      if CharInSet(Texto[i], (['0'..'9'])) then
        Num := Num + Texto[i]
      else NewSep := Texto[i];
      //
      if (NewSep <> '') then
      begin
        if NewSep = '-' then
        begin
          if DefTraco then Geral.MB_Erro(
          'Erro. Intervalo de m�nimo e m�ximo com mais de uma defini��o!');
          DestTraco := Num + NewSep;
          Num := '';
          isTraco   := True;
          isVirgula := False;
        end else
        if NewSep = ',' then
        begin
          if OldSep = '-' then
            DestTraco := DestTraco + Num
          else
            DestVirgula := DestVirgula + Num + NewSep;
          Num := '';
          isTraco   := False;
          isVirgula := True;
        end else Geral.MB_Erro(
          'Erro. Separador desconhecido "' + NewSep + '"');
        OldSep := NewSep;
        NewSep := '';
      end;
    end;
    i := Length(DestVirgula);
    if i > 0 then
      if DestVirgula[i] = ',' then
        DestVirgula := Copy(DestVirgula, 1, i-1);
    if DestVirgula <> '' then
    begin
      //Geral.MB_Erro('- = ' + DestTraco + sLineBreak + ', = ' + DestVirgula);
      FormatStr := DestVirgula;
      DestVirgula := '';
    end;
  end;
end;

function TUnXXe_PF.SoNumeroDeTamanhoDeCampoDeXXe(const Source: String;
  var Dest: String): Boolean;
var
  i:Integer;
begin
  Result := True;
  Dest   := '';
  if Length(Source) > 0 then
  begin
    for i := 1 to Length(Source) do
    begin
      if CharInSet(Source[i], (['0'..'9', '-'])) then Dest := Dest + Source[i] else
      begin
        Result := False;
        if Source[i] = ',' then Dest := Dest + Source[i] else
        if Lowercase(Copy(Source, i, 2)) = 'ou' then Dest := Dest + ',';
      end;
    end;
  end;
end;

function TUnXXe_PF.StatusOK_XXe(TipoXXe: TTipoXXe): String;
begin
  case TipoXXe of
    (*55*)tipoxxeNFe:  Result := '135, 136';
    (*57*)tipoxxeCTe:  Result := '134, 135, 136';
    (*58*)tipoxxeMDFe: Result := '135, 136, 137';
    else
    begin
      Geral.MB_Erro('Tipo de documento fiscal eletr�nico n�o implementado! [1]');
      Result := '??';
    end;
  end;
end;

function TUnXXe_PF.StrZero(Num: Real; Zeros, Deci: Integer): String;
var
  tam, z : Integer;
  res, zer : String;
begin
  str(Num:Zeros:Deci, res);
  res := Alltrim(res);
  tam := length(res);
  zer := '';
  for z := 1 to (Zeros-tam) do
    zer := zer + '0';
  result := zer+res
end;

function TUnXXe_PF.TabelaCabAXXe(TipoXXe: TTipoXXe): String;
begin
  case TipoXXe of
    (*55*)tipoxxeNFe:  Result := 'nfecaba';
    (*57*)tipoxxeCTe:  Result := 'ctecaba';
    (*58*)tipoxxeMDFe: Result := 'mdfecaba';
    else
    begin
      Geral.MB_Erro('Tipo de documento fiscal eletr�nico n�o implementado! [4]');
      Result := '??ecaba'
    end;
  end;
end;

function TUnXXe_PF.TabelaEveRCabXXe(TipoXXe: TTipoXXe): String;
begin
  case TipoXXe of
    (*55*)tipoxxeNFe:  Result := 'nfeevercab';
    (*57*)tipoxxeCTe:  Result := 'cteevercab';
    (*58*)tipoxxeMDFe: Result := 'mdfeevercab';
    else
    begin
      Geral.MB_Erro('Tipo de documento fiscal eletr�nico n�o implementado! [3]');
      Result := '??eevercab'
    end;
  end;
end;

function TUnXXe_PF.TextoAmbiente(Ambiente: Byte): String;
begin
  case Ambiente of
    1: Result := 'Produ��o';
    2: Result := 'Homologa��o';
    else Result := '[Desconhecido]';
  end;
end;

function TUnXXe_PF.TipoDoc_ide_Mod(ide_Mod: Integer): String;
begin
  case ide_mod of
    55: Result := 'NF-e';
    57: Result := 'CT-e';
    58: Result := 'MDF-e';
    65: Result := 'NFC-e';
    else Result := '????';
  end;
end;

function TUnXXe_PF.ValidaTexto_XML(Txt, Codigo, ID: String): String;
var
  I: Integer;
  Res: String;
begin
  Res := '';
  for I := 1 to Length(Txt) do
    //Ress := Res + MLAGeral.CharToUTF8XML(Ord(Txt[I]));
    Res := Res + MLAGeral.CharToMyXML_Enviar(Ord(Txt[I]));
  Result := Res;
end;

procedure TUnXXe_PF.ValidaXML_XXe(ArquivoXML: String);
begin
(* 20230330
  if DBCheck.CriaFm(TFmNFeValidaXML_0001, FmNFeValidaXML_0001, afmoNegarComAviso) then
  begin
    FmNFeValidaXML_0001.EdArquivo.ValueVariant := ArquivoXML;
    FmNFeValidaXML_0001.ShowModal;
    FmNFeValidaXML_0001.Destroy;
  end;
*)
end;

function TUnXXe_PF.VarTypeValido(Valor: Variant; Nome: String): Boolean;
var
  Tipo: TVarType;
begin
  Tipo := VarType(Valor);
  case Tipo of
    varEmpty    : Result := False; //= $0000; { vt_empty        0 }
    varNull     : Result := False;     //= $0001; { vt_null         1 }
    varSmallint : Result := Valor > 0; //= $0002; { vt_i2           2 }
    varInteger  : Result := Valor > 0;  //= $0003; { vt_i4           3 }
    varSingle   : Result := Valor > 0;  //= $0004; { vt_r4           4 }
    varDouble   : Result := Valor > 0;  //= $0005; { vt_r8           5 }
    varCurrency : Result := Valor > 0; //= $0006; { vt_cy           6 }
    varDate     : Result := Valor > 0;  //= $0007; { vt_date         7 }
    //varOleStr   : Result := 'OleStr ';  //= $0008; { vt_bstr         8 }
    //varDispatch : Result := 'Dispatch'; //= $0009; { vt_dispatch     9 }
    //varError    : Result := 'Error  ';  //= $000A; { vt_error       10 }
    varBoolean  : Result := Valor = True;  //= $000B; { vt_bool        11 }
    varVariant  : Result := Geral.VariantToString(Valor) <> '';  //= $000C; { vt_variant     12 }
    //varUnknown  : Result := 'Unknown';  //= $000D; { vt_unknown     13 }
  //varDecimal  = $000E; { vt_decimal     14 } {UNSUPPORTED as of v6.x code base}
  //varUndef0F  = $000F; { undefined      15 } {UNSUPPORTED per Microsoft}
    varShortInt : Result := Valor > 0; //= $0010; { vt_i1          16 }
    varByte     : Result := Valor > 0;  //= $0011; { vt_ui1         17 }
    varWord     : Result := Valor > 0;  //= $0012; { vt_ui2         18 }
    varLongWord : Result := Valor > 0; //= $0013; { vt_ui4         19 }
    varInt64    : Result := Valor > 0;  //= $0014; { vt_i8          20 }
  //varWord64   = $0015; { vt_ui8         21 } {UNSUPPORTED as of v6.x code base}
  {  if adding new items, update Variants' varLast, BaseTypeMap and OpTypeMap }

    //varStrArg   : Result := 'StrArg';   //= $0048; { vt_clsid       72 }
    varString   : Result := Valor <> '';  //= $0100; { Pascal string 256 } {not OLE compatible }
{$IFDEF DELPHI12_UP} // nao tenho certeza em qual delphi comecou, mas acho que eh no 2009
    varUString  : Result := Valor <> '';  //= $0100; { Pascal string 256 } {not OLE compatible }
{$ENDIF}
    //varAny      : Result := 'Any    ';  //= $0101; { Corba any     257 } {not OLE compatible }
    // custom types range from $110 (272) to $7FF (2047)

    //varTypeMask : Result := 'TypeMask'; //= $0FFF;
    //varArray    : Result := 'Array  ';  //= $2000;
    //varByRef    : Result := 'ByRef  ';  //= $4000;
    $0008       : Result := Valor <> '';
    else begin
      Geral.MB_Erro('Tipo de vari�vel desconhecida: ' + sLineBreak  +
      'Nome da vari�vel: ' + Nome + sLineBreak  +
      'Tipo de vari�vel = '  + IntToHex(VarType(Valor), 4));
      Geral.MB_Aviso('Valor vari�vel desconhecida: ' + sLineBreak  +
      Geral.VariantToString(Valor));
      Result := False;
    end;
  end;
end;

function TUnXXe_PF.VerificaChaveAcessoXXe(Chave: String;
  PermiteNulo: Boolean): Boolean;
var
  Dados: String;
  DV: Char;
  T: Integer;
begin
  Result := False;
  T := Length(Chave);
  if T <> 44 then
  begin
    if (T = 0) and PermiteNulo then
      Result := True
    else Geral.MensagemBox('Chave de acesso inv�lida!' + sLineBreak +
    'Quantidade de caracteres: ' + IntToStr(T) + ' (deve ser 44).', 'Aviso',
    MB_OK+MB_ICONWARNING);
  end else begin
    DV := Chave[44];
    Dados := Copy(Chave, 1, 43);
    Result := Geral.Modulo11_2a9_Back(dados, 0) = DV;
    if not Result then
      Geral.MensagemBox('Chave de acesso inv�lida!' + sLineBreak +
      'D�gito verificador n�o confere!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

function TUnXXe_PF.XXeDesmontaEPreencheObjectsChaveCTe(Preparando: Boolean;
  EdChave, EdModelo: TdmkEdit; CBModelo: TdmkDBLookupComboBox; EdSerie,
  EdNumero: TdmkEdit; EdEmitente: TdmkEditCB; CBEmitente: TdmkDBLookupComboBox;
  ControlFocus: TWinControl): Boolean;
const
  VersaoXXe = 0.00;
var
  Chave, UF_IBGE, AAMM, CNPJ, ModDoc, SerDoc, NumDoc, tpEmis, CodSer, xTpDoc: String;
  Entidade: Integer;
begin
  if Preparando then
  begin
    Result := True;
    Exit;
  end else
    Result := False;
  //
  Chave := EdChave.Text;
  if Length(Chave) = 44 then
  begin
    Result := XXe_PF.DesmontaChaveDeAcesso(Chave, VersaoXXe,
    UF_IBGE, AAMM, CNPJ, ModDoc, SerDoc, NumDoc, tpEmis, CodSer);
    //
    if EdModelo <> nil then
      EdModelo.ValueVariant  := Geral.IMV(ModDoc);
    if CBModelo <> nil then
      CBModelo.KeyValue      := Geral.IMV(ModDoc);
    if EdSerie <> nil then
      EdSerie.Text           := SerDoc;
    if EdNumero <> nil then
      EdNumero.ValueVariant  := Geral.IMV(NumDoc);
    if DModG.ObtemEntidadeDeCNPJCFP(CNPJ, Entidade) then
    begin
      if EdEmitente <> nil then
        EdEmitente.ValueVariant := Entidade;
      if CBEmitente <> nil then
        CBEmitente.KeyValue     := Entidade;
      if ControlFocus <> nil then
        ControlFocus.SetFocus;
    end else
    begin
      if Geral.MB_Pergunta('N�o existe cadastro de entidade para o CNPJ ' +
      Geral.FormataCNPJ_TT(CNPJ) + slineBreak + 'Deseja cadastr�-lo?') = ID_YES
      then
      begin
        Entidade := 0;
        VAR_CNPJ_A_CADASTRAR := CNPJ;
        DmodG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);
      end;
      if DModG.ObtemEntidadeDeCNPJCFP(CNPJ, Entidade) then
      begin
        if CBEmitente <> nil then
          UnDmkDAC_PF.AbreQuery(TmySQLQuery(CBEmitente.DataSource.DataSet), Dmod.MyDB);
        if EdEmitente <> nil then
          EdEmitente.ValueVariant := Entidade;
        if CBEmitente <> nil then
          CBEmitente.KeyValue     := Entidade;
        if ControlFocus <> nil then
          ControlFocus.SetFocus;
      end;
    end;
    //
    if EdEmitente <> nil then
    begin
      if EdEmitente.ValueVariant = 0 then
      EdEmitente.SetFocus;
    end;
  end;
end;

function TUnXXe_PF.XXeEstahAutorizada(Codigo: Integer): Boolean;
begin
  Result := Codigo = 100;
end;

function TUnXXe_PF.XXeEstahCancelada(Codigo: Integer): Boolean;
begin
  Result := Codigo = 101;
end;

function TUnXXe_PF.XXeEstahCancelada_TXT(Codigo: Integer): String;
begin
  if Codigo = 101 then
    Result := 'Cancelada'
  else
    Result := '';
end;

function TUnXXe_PF.XXeObtemStatusXXedeChave(TipoXXe: TTipoXXe;
  Chave: String): Integer;
var
  Qry: TmySQLQuery;
  Tabela: String;
begin
  Result := 0;
  Tabela := TabelaEveRCabXXe(TipoXXe);
  Qry := TmySQLQuery.Create(Dmod.MyDB);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Status ',
    'FROM ' + LowerCase(Tabela),
    'WHERE ID="' + Chave + '"',
    '']);
    Result := Qry.FieldByName('Status').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnXXe_PF.ZeraCodSegChaveDeAcesso(const ChaveAtual: String; var
  ChaveNova: String): Boolean;
var
  I: Integer;
begin
  Result := False;
  //
  if Length(ChaveAtual) = 44 then
  begin
    try
      ChaveNova := ChaveAtual;
      for I := 36 to 43 do
        ChaveNova[I] := '0';
      Result := true;
    except
      on E: Exception do
        Geral.MB_Erro('N�o foi possivel zerar o c�digo rand�mico de seguran�a!' +
        sLineBreak + E.Message);
    end;
  end else
    Geral.MB_Aviso('a Chave "' + ChaveAtual + '" n�o possui 44 d�gitos!');
end;

{
function TUnXXe_PF.ObtemNomeDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'Id'                ;
    ttx_idLote      : Result := 'idLote'            ;
    ttx_versao      : Result := 'versao'            ;
    ttx_tpAmb       : Result := 'tpAmb'             ;
    ttx_verAplic    : Result := 'verAplic'          ;
    ttx_cOrgao      : Result := 'cOrgao'            ;
    ttx_cStat       : Result := 'cStat'             ;
    ttx_xMotivo     : Result := 'xMotivo'           ;
    ttx_cUF         : Result := 'cUF'               ;
    ttx_dhRecbto    : Result := 'dhRecbto'          ;
    ttx_chNFe       : Result := 'chNFe'             ;
    ttx_nProt       : Result := 'nProt'             ;
    ttx_digVal      : Result := 'digVal'            ;
    ttx_ano         : Result := 'ano'               ;
    ttx_CNPJ        : Result := 'CNPJ'              ;
    ttx_mod         : Result := 'mod'               ;
    ttx_serie       : Result := 'serie'             ;
    ttx_nNFIni      : Result := 'nNFIni'            ;
    ttx_nNFFin      : Result := 'nNFFin'            ;
    ttx_nRec        : Result := 'nRec'              ;
    ttx_tMed        : Result := 'tMed'              ;
    ttx_tpEvento    : Result := 'tpEvento'          ;
    ttx_xEvento     : Result := 'xEvento'           ;
    ttx_CNPJDest    : Result := 'CNPJDest'          ;
    ttx_CPFDest     : Result := 'CPFDest'           ;
    ttx_emailDest   : Result := 'emailDest'         ;
    ttx_nSeqEvento  : Result := 'nSeqEvento'        ;
    ttx_dhRegEvento : Result := 'dhRegEvento'       ;
    ttx_dhResp      : Result := 'dhResp'            ;
    ttx_indCont     : Result := 'indCont'           ;
    ttx_ultNSU      : Result := 'ultNSU'            ;
    ttx_maxNSU      : Result := 'maxNSU'            ;
    ttx_NSU         : Result := 'NSU'               ;
    ttx_CPF         : Result := 'CPF'               ;
    ttx_xNome       : Result := 'xNome'             ;
    ttx_IE          : Result := 'IE'                ;
    ttx_dEmi        : Result := 'dEmi'              ;
    ttx_tpNF        : Result := 'tpNF'              ;
    ttx_vNF         : Result := 'vNF'               ;
    ttx_cSitNFe     : Result := 'cSitNFe'           ;
    ttx_cSitConf    : Result := 'cSitConf'          ;
    ttx_dhEvento    : Result := 'dhEvento'          ;
    ttx_descEvento  : Result := 'descEvento'        ;
    //ttx_x Correcao   : Result := x Correcao         ;
    ttx_cJust       : Result := 'cJust'             ;
    ttx_xJust       : Result := 'xJust'             ;
    ttx_verEvento   : Result := 'verEvento'         ;
    ttx_schema      : Result := 'schema'            ;
    ttx_docZip      : Result := 'docZip'            ;
    ttx_dhEmi       : Result := 'dhEmi'             ;
    //
    else           Result :=      '???';
  end;
end;
}

end.
