unit UnEfdIcmsIpi_Jan;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) dmkEdit, dmkRadioGroup,
  dmkMemo, dmkCheckGroup, UnDmkProcFunc, TypInfo, UnMyObjects,
  dmkEditDateTimePicker, SPED_Listas;

const
  VerAtual_EfdIcmsIpi_Jan = '3.0.1';
  CO_SEII_3_0_1__ = 3000100;
  CO_SEII_3_0_2_a = 3002001;

  //FImporExpor = 2; // 2 = Exporta - N�o mexer!!!

type
  TUnEfdIcmsIpi_Jan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormEFD_C001();
    procedure MostraFormEFD_D001();
    //procedure MostraFormEFD_E001();
    procedure MostraFormEfdIcmsIpiExporta();
    //
    procedure MostraFormEfdIcmsIpiE001();
    //
    procedure MostraFormSPEDEFDEnce();
    procedure MostraFormSpedEfdIcmsIpiClaProd(ImporExpor, AnoMes, Empresa,
              LinArq: Integer; Registro: String; Data: TDateTime);
    procedure MostraFormSpedefdIcmsIpiBalancos(ImporExpor, AnoMes, Empresa,
              LinArq: Integer; Registro: String; Data: TDateTime);
    procedure MostraFormEfdInnCTsGer();

    function  VersaoTxtDeEfdIcmsIpi(Forca: Boolean): String;
    function  VersaoIntDeEfdIcmsIpi_PF(): Integer;
  end;
var
  EfdIcmsIpi_Jan: TUnEfdIcmsIpi_Jan;

implementation

uses UnMLAGeral, MyDBCheck, DmkDAC_PF, Module, ModuleGeral,
  UMySQLModule, UnGrade_PF, ModProd,
///////////////////////////     SPED ICMS IPI    ///////////////////////////////
  UnEfdIcmsIpi_PF_v03_0_1,
  UnEfdIcmsIpi_Jan_v03_0_1, UnEfdIcmsIpi_Jan_v03_0_2_a,
///////////////////////////     SPED ICMS IPI    ///////////////////////////////
  EfdInnCTsGer,
///
  ModAppGraG1;

{ TUnEfdIcmsIpi_Jan }

procedure TUnEfdIcmsIpi_Jan.MostraFormEfdIcmsIpiE001();
var
  Versao: Integer;
begin
  Versao := VersaoIntDeEfdIcmsIpi_PF();
  case Versao of
    CO_SEII_3_0_1__: EfdIcmsIpi_Jan_v03_0_1.MostraFormEfdIcmsIpiE001();
    CO_SEII_3_0_2_a: EfdIcmsIpi_Jan_v03_0_2_a.MostraFormEfdIcmsIpiE001();
  end;
end;


procedure TUnEfdIcmsIpi_Jan.MostraFormEfdIcmsIpiExporta();
var
  Versao: Integer;
begin
  Versao := VersaoIntDeEfdIcmsIpi_PF();
  //
  case Versao of
    CO_SEII_3_0_1__: EfdIcmsIpi_Jan_v03_0_1.MostraFormEfdIcmsIpiExporta();
    CO_SEII_3_0_2_a: EfdIcmsIpi_Jan_v03_0_2_a.MostraFormEfdIcmsIpiExporta();
  end;
end;

procedure TUnEfdIcmsIpi_Jan.MostraFormEfdInnCTsGer();
begin
  if DBCheck.CriaFm(TFmEfdInnCTsGer, FmEfdInnCTsGer, afmoNegarComAviso) then
  begin
    FmEfdInnCTsGer.ShowModal;
    FmEfdInnCTsGer.Destroy;
  end;
end;

procedure TUnEfdIcmsIpi_Jan.MostraFormEFD_C001();
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "C001"');
(*
  if DBCheck.CriaFm(TFmEFD_C001, FmEFD_C001, afmoNegarComAviso) then
  begin
    FmEFD_C001.ShowModal;
    FmEFD_C001.Destroy;
  end;
*)
end;

procedure TUnEfdIcmsIpi_Jan.MostraFormEFD_D001();
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "D001"');
(*
  if DBCheck.CriaFm(TFmEFD_D001, FmEFD_D001, afmoNegarComAviso) then
  begin
    FmEFD_D001.ShowModal;
    FmEFD_D001.Destroy;
  end;
*)
end;

procedure TUnEfdIcmsIpi_Jan.MostraFormSPEDEFDEnce();
var
  Versao: Integer;
begin
  Versao := VersaoIntDeEfdIcmsIpi_PF();
  case Versao of
    CO_SEII_3_0_1__: EfdIcmsIpi_Jan_v03_0_1.MostraFormSPEDEFDEnce();
    CO_SEII_3_0_2_a: EfdIcmsIpi_Jan_v03_0_2_a.MostraFormSPEDEFDEnce()
  end;
end;

procedure TUnEfdIcmsIpi_Jan.MostraFormSpedefdIcmsIpiBalancos(ImporExpor,
  AnoMes, Empresa, LinArq: Integer; Registro: String; Data: TDateTime);
var
  Versao: Integer;
begin
  Versao := VersaoIntDeEfdIcmsIpi_PF();
  case Versao of
    CO_SEII_3_0_1__:
      EfdIcmsIpi_Jan_v03_0_1.MostraFormSpedefdIcmsIpiBalancos(ImporExpor,
      AnoMes, Empresa, LinArq, Registro, Data);
    CO_SEII_3_0_2_a:
      EfdIcmsIpi_Jan_v03_0_2_a.MostraFormSpedefdIcmsIpiBalancos(ImporExpor,
      AnoMes, Empresa, LinArq, Registro, Data);
  end;
end;

procedure TUnEfdIcmsIpi_Jan.MostraFormSpedEfdIcmsIpiClaProd(ImporExpor,
  AnoMes, Empresa, LinArq: Integer; Registro: String; Data: TDateTime);
var
  Versao: Integer;
begin
  Versao := VersaoIntDeEfdIcmsIpi_PF();
  //
  case Versao of
    CO_SEII_3_0_1__:
      EfdIcmsIpi_Jan_v03_0_1.MostraFormSpedEfdIcmsIpiClaProd(ImporExpor,
        AnoMes, Empresa, LinArq, Registro, Data);
    CO_SEII_3_0_2_a:
      EfdIcmsIpi_Jan_v03_0_2_a.MostraFormSpedEfdIcmsIpiClaProd(ImporExpor,
        AnoMes, Empresa, LinArq, Registro, Data);
  end;
end;

function TUnEfdIcmsIpi_Jan.VersaoIntDeEfdIcmsIpi_PF(): Integer;
const
  NaoForca = False;
  sVersoes: array [0..2] of String = (
    'N�o sei',
    '3.0.1 - de 01.01.2019 a 31.12.2020',
    '3.0.2.a - em desenvolvimento para 2020'
  );
var
  //iVersao, Codigo: Integer;
  SPED_EFD_ICMS_IPI_VersaoGuia,
  sVersao: String;
  //SQLType: TSQLType;
begin
  Result := 0;
  sVersao := VersaoTxtDeEfdIcmsIpi(NaoForca);
  //
  if sVersao = '3.0.1'   then Result := CO_SEII_3_0_1__ else
  if sVersao = '3.0.2.a' then Result := CO_SEII_3_0_2_a else
  begin
    Result := -1;
    Geral.MB_Erro('Vers�o do Guia Pr�tico do SPED EFD ICMS IPI n�o definida!');
  end;
end;

function TUnEfdIcmsIpi_Jan.VersaoTxtDeEfdIcmsIpi(Forca: Boolean): String;
const
  sVersoes: array [0..2] of String = (
    'N�o sei',
    '3.0.1 - de 01.01.2019 a 31.12.2020',
    '3.0.2.a - em desenvolvimento para 2020'
  );
var
  iVersao, Codigo: Integer;
  //sVersao,
  SPED_EFD_ICMS_IPI_VersaoGuia: String;
  SQLType: TSQLType;
begin
  Result := '';
  Result := DModG.QrParamsEmpSPED_EFD_ICMS_IPI_VersaoGuia.Value;
  if Forca or (Trim(Result) = '') then
  begin
    Codigo  := DModG.QrParamsEmpCodigo.Value;
    //
    iVersao := MyObjects.SelRadioGroup(
    'Vers�o do Guia Pr�tico do SPED EFD ICMS IPI',
    'Selecione a Vers�o do Guia Pr�tico do SPED EFD ICMS IPI: ',
    sVersoes, 1);
    case iVersao of
      0: Result := '3.0.1';
      1: Result := '3.0.1';
      2: Result := '3.0.2.a';
      else Result := '';
    end;
    if iVersao > -1 then
    begin
      SPED_EFD_ICMS_IPI_VersaoGuia := Result;
      SQLType := stUpd;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'paramsemp', False, [
      'SPED_EFD_ICMS_IPI_VersaoGuia'], [
      'Codigo'], [
      SPED_EFD_ICMS_IPI_VersaoGuia], [
      Codigo], True) then
        DModG.ReopenParamsEmp(Geral.IMV(VAR_LIB_EMPRESAS));
    end;
  end;
  //
  //if Result = '' then
    //Geral.MB_Erro('Vers�o do Guia Pr�tico do SPED EFD ICMS IPI n�o definida!');
end;

end.
