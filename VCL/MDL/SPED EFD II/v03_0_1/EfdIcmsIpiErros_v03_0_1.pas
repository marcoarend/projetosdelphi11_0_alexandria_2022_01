unit EfdIcmsIpiErros_v03_0_1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, frxClass, frxDBSet, DmkDAC_PF, UnGrade_Jan, dmkImage,
  SPED_LIstas, UnDmkEnums;

type
  TFmEfdIcmsIpiErros_v03_0_1 = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    QrSpedEfdIcmsIpiErrs: TmySQLQuery;
    DsSpedEfdIcmsIpiErrs: TDataSource;
    QrSpedEfdIcmsIpiErrsLinArq: TIntegerField;
    QrSpedEfdIcmsIpiErrsREG: TWideStringField;
    QrSpedEfdIcmsIpiErrsCampo: TWideStringField;
    QrSpedEfdIcmsIpiErrsErro: TSmallintField;
    QrSpedEfdIcmsIpiErrsDESCRI_ERRO: TWideStringField;
    QrLinha: TmySQLQuery;
    DsLinha: TDataSource;
    Panel3: TPanel;
    DBGLinha: TDBGrid;
    LaConteudo: TLabel;
    LaNotas: TLabel;
    QrNotas: TmySQLQuery;
    QrNotasFatID: TIntegerField;
    QrNotasFatNum: TIntegerField;
    QrNotasEmpresa: TIntegerField;
    QrNotasIDCtrl: TIntegerField;
    QrNotasLoteEnv: TIntegerField;
    QrNotasversao: TFloatField;
    QrNotasId: TWideStringField;
    QrNotaside_cUF: TSmallintField;
    QrNotaside_cNF: TIntegerField;
    QrNotaside_natOp: TWideStringField;
    QrNotaside_indPag: TSmallintField;
    QrNotaside_mod: TSmallintField;
    QrNotaside_serie: TIntegerField;
    QrNotaside_nNF: TIntegerField;
    QrNotaside_dEmi: TDateField;
    QrNotaside_dSaiEnt: TDateField;
    QrNotaside_tpNF: TSmallintField;
    QrNotaside_cMunFG: TIntegerField;
    QrNotaside_tpImp: TSmallintField;
    QrNotaside_tpEmis: TSmallintField;
    QrNotaside_cDV: TSmallintField;
    QrNotaside_tpAmb: TSmallintField;
    QrNotaside_finNFe: TSmallintField;
    QrNotaside_procEmi: TSmallintField;
    QrNotaside_verProc: TWideStringField;
    QrNotasemit_CNPJ: TWideStringField;
    QrNotasemit_CPF: TWideStringField;
    QrNotasemit_xNome: TWideStringField;
    QrNotasemit_xFant: TWideStringField;
    QrNotasemit_xLgr: TWideStringField;
    QrNotasemit_nro: TWideStringField;
    QrNotasemit_xCpl: TWideStringField;
    QrNotasemit_xBairro: TWideStringField;
    QrNotasemit_cMun: TIntegerField;
    QrNotasemit_xMun: TWideStringField;
    QrNotasemit_UF: TWideStringField;
    QrNotasemit_CEP: TIntegerField;
    QrNotasemit_cPais: TIntegerField;
    QrNotasemit_xPais: TWideStringField;
    QrNotasemit_fone: TWideStringField;
    QrNotasemit_IE: TWideStringField;
    QrNotasemit_IEST: TWideStringField;
    QrNotasemit_IM: TWideStringField;
    QrNotasemit_CNAE: TWideStringField;
    QrNotasdest_CNPJ: TWideStringField;
    QrNotasdest_CPF: TWideStringField;
    QrNotasdest_xNome: TWideStringField;
    QrNotasdest_xLgr: TWideStringField;
    QrNotasdest_nro: TWideStringField;
    QrNotasdest_xCpl: TWideStringField;
    QrNotasdest_xBairro: TWideStringField;
    QrNotasdest_cMun: TIntegerField;
    QrNotasdest_xMun: TWideStringField;
    QrNotasdest_UF: TWideStringField;
    QrNotasdest_CEP: TWideStringField;
    QrNotasdest_cPais: TIntegerField;
    QrNotasdest_xPais: TWideStringField;
    QrNotasdest_fone: TWideStringField;
    QrNotasdest_IE: TWideStringField;
    QrNotasdest_ISUF: TWideStringField;
    QrNotasICMSTot_vBC: TFloatField;
    QrNotasICMSTot_vICMS: TFloatField;
    QrNotasICMSTot_vBCST: TFloatField;
    QrNotasICMSTot_vST: TFloatField;
    QrNotasICMSTot_vProd: TFloatField;
    QrNotasICMSTot_vFrete: TFloatField;
    QrNotasICMSTot_vSeg: TFloatField;
    QrNotasICMSTot_vDesc: TFloatField;
    QrNotasICMSTot_vII: TFloatField;
    QrNotasICMSTot_vIPI: TFloatField;
    QrNotasICMSTot_vPIS: TFloatField;
    QrNotasICMSTot_vCOFINS: TFloatField;
    QrNotasICMSTot_vOutro: TFloatField;
    QrNotasICMSTot_vNF: TFloatField;
    QrNotasISSQNtot_vServ: TFloatField;
    QrNotasISSQNtot_vBC: TFloatField;
    QrNotasISSQNtot_vISS: TFloatField;
    QrNotasISSQNtot_vPIS: TFloatField;
    QrNotasISSQNtot_vCOFINS: TFloatField;
    QrNotasRetTrib_vRetPIS: TFloatField;
    QrNotasRetTrib_vRetCOFINS: TFloatField;
    QrNotasRetTrib_vRetCSLL: TFloatField;
    QrNotasRetTrib_vBCIRRF: TFloatField;
    QrNotasRetTrib_vIRRF: TFloatField;
    QrNotasRetTrib_vBCRetPrev: TFloatField;
    QrNotasRetTrib_vRetPrev: TFloatField;
    QrNotasModFrete: TSmallintField;
    QrNotasTransporta_CNPJ: TWideStringField;
    QrNotasTransporta_CPF: TWideStringField;
    QrNotasTransporta_XNome: TWideStringField;
    QrNotasTransporta_IE: TWideStringField;
    QrNotasTransporta_XEnder: TWideStringField;
    QrNotasTransporta_XMun: TWideStringField;
    QrNotasTransporta_UF: TWideStringField;
    QrNotasRetTransp_vServ: TFloatField;
    QrNotasRetTransp_vBCRet: TFloatField;
    QrNotasRetTransp_PICMSRet: TFloatField;
    QrNotasRetTransp_vICMSRet: TFloatField;
    QrNotasRetTransp_CFOP: TWideStringField;
    QrNotasRetTransp_CMunFG: TWideStringField;
    QrNotasVeicTransp_Placa: TWideStringField;
    QrNotasVeicTransp_UF: TWideStringField;
    QrNotasVeicTransp_RNTC: TWideStringField;
    QrNotasCobr_Fat_nFat: TWideStringField;
    QrNotasCobr_Fat_vOrig: TFloatField;
    QrNotasCobr_Fat_vDesc: TFloatField;
    QrNotasCobr_Fat_vLiq: TFloatField;
    QrNotasInfAdic_InfAdFisco: TWideMemoField;
    QrNotasInfAdic_InfCpl: TWideMemoField;
    QrNotasExporta_UFEmbarq: TWideStringField;
    QrNotasExporta_XLocEmbarq: TWideStringField;
    QrNotasCompra_XNEmp: TWideStringField;
    QrNotasCompra_XPed: TWideStringField;
    QrNotasCompra_XCont: TWideStringField;
    QrNotasStatus: TIntegerField;
    QrNotasinfProt_Id: TWideStringField;
    QrNotasinfProt_tpAmb: TSmallintField;
    QrNotasinfProt_verAplic: TWideStringField;
    QrNotasinfProt_dhRecbto: TDateTimeField;
    QrNotasinfProt_nProt: TWideStringField;
    QrNotasinfProt_digVal: TWideStringField;
    QrNotasinfProt_cStat: TIntegerField;
    QrNotasinfProt_xMotivo: TWideStringField;
    QrNotasinfCanc_Id: TWideStringField;
    QrNotasinfCanc_tpAmb: TSmallintField;
    QrNotasinfCanc_verAplic: TWideStringField;
    QrNotasinfCanc_dhRecbto: TDateTimeField;
    QrNotasinfCanc_nProt: TWideStringField;
    QrNotasinfCanc_digVal: TWideStringField;
    QrNotasinfCanc_cStat: TIntegerField;
    QrNotasinfCanc_xMotivo: TWideStringField;
    QrNotasinfCanc_cJust: TIntegerField;
    QrNotasinfCanc_xJust: TWideStringField;
    QrNotas_Ativo_: TSmallintField;
    QrNotasFisRegCad: TIntegerField;
    QrNotasCartEmiss: TIntegerField;
    QrNotasTabelaPrc: TIntegerField;
    QrNotasCondicaoPg: TIntegerField;
    QrNotasLk: TIntegerField;
    QrNotasDataCad: TDateField;
    QrNotasDataAlt: TDateField;
    QrNotasUserCad: TIntegerField;
    QrNotasUserAlt: TIntegerField;
    QrNotasAlterWeb: TSmallintField;
    QrNotasAtivo: TSmallintField;
    QrNotasFreteExtra: TFloatField;
    QrNotasSegurExtra: TFloatField;
    QrNotasICMSRec_pRedBC: TFloatField;
    QrNotasICMSRec_vBC: TFloatField;
    QrNotasICMSRec_pAliq: TFloatField;
    QrNotasICMSRec_vICMS: TFloatField;
    QrNotasIPIRec_pRedBC: TFloatField;
    QrNotasIPIRec_vBC: TFloatField;
    QrNotasIPIRec_pAliq: TFloatField;
    QrNotasIPIRec_vIPI: TFloatField;
    QrNotasPISRec_pRedBC: TFloatField;
    QrNotasPISRec_vBC: TFloatField;
    QrNotasPISRec_pAliq: TFloatField;
    QrNotasPISRec_vPIS: TFloatField;
    QrNotasCOFINSRec_pRedBC: TFloatField;
    QrNotasCOFINSRec_vBC: TFloatField;
    QrNotasCOFINSRec_pAliq: TFloatField;
    QrNotasCOFINSRec_vCOFINS: TFloatField;
    QrNotasDataFiscal: TDateField;
    QrNotasprotNFe_versao: TFloatField;
    QrNotasretCancNFe_versao: TFloatField;
    QrNotasSINTEGRA_ExpDeclNum: TWideStringField;
    QrNotasSINTEGRA_ExpDeclDta: TDateField;
    QrNotasSINTEGRA_ExpNat: TWideStringField;
    QrNotasSINTEGRA_ExpRegNum: TWideStringField;
    QrNotasSINTEGRA_ExpRegDta: TDateField;
    QrNotasSINTEGRA_ExpConhNum: TWideStringField;
    QrNotasSINTEGRA_ExpConhDta: TDateField;
    QrNotasSINTEGRA_ExpConhTip: TWideStringField;
    QrNotasSINTEGRA_ExpPais: TWideStringField;
    QrNotasSINTEGRA_ExpAverDta: TDateField;
    QrNotasCodInfoEmit: TIntegerField;
    QrNotasCodInfoDest: TIntegerField;
    QrNotasCriAForca: TSmallintField;
    QrNotaside_hSaiEnt: TTimeField;
    QrNotaside_dhCont: TDateTimeField;
    QrNotaside_xJust: TWideStringField;
    QrNotasemit_CRT: TSmallintField;
    QrNotasdest_email: TWideStringField;
    QrNotasVagao: TWideStringField;
    QrNotasBalsa: TWideStringField;
    QrNotasCodInfoTrsp: TIntegerField;
    QrNotasOrdemServ: TIntegerField;
    QrNotasSituacao: TSmallintField;
    QrNotasAntigo: TWideStringField;
    QrNotasNFG_Serie: TWideStringField;
    QrNotasNF_ICMSAlq: TFloatField;
    QrNotasNF_CFOP: TWideStringField;
    QrNotasImportado: TSmallintField;
    QrNotasNFG_SubSerie: TWideStringField;
    QrNotasNFG_ValIsen: TFloatField;
    QrNotasNFG_NaoTrib: TFloatField;
    QrNotasNFG_Outros: TFloatField;
    DsNotas: TDataSource;
    DBGNotas: TDBGrid;
    QrNotasNO_FATID: TWideStringField;
    frxErros: TfrxReport;
    frxDslinha: TfrxDBDataset;
    frxDsNotas: TfrxDBDataset;
    frxDsSpedEfdIcmsIpiErrs: TfrxDBDataset;
    QrC100: TmySQLQuery;
    QrC100FatID: TIntegerField;
    QrC100FatNum: TIntegerField;
    QrC100Empresa: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBConfirma: TGroupBox;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtNF: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel47: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    BtReduzido: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSpedEfdIcmsIpiErrsCalcFields(DataSet: TDataSet);
    procedure QrSpedEfdIcmsIpiErrsBeforeClose(DataSet: TDataSet);
    procedure QrSpedEfdIcmsIpiErrsAfterScroll(DataSet: TDataSet);
    procedure QrNotasCalcFields(DataSet: TDataSet);
    procedure frxErrosGetValue(const VarName: string; var Value: Variant);
    procedure BtOKClick(Sender: TObject);
    procedure BtNFClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtReduzidoClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenLinha();
    procedure ReopenNotas();
  public
    { Public declarations }
    FImporExpor, FAnoMes, FEmpresa: Integer;
    FEmprTXT, FDataIni, FDataFim: String;
    procedure ReopenErros();
    function  SQLNotas_0150(): String;
    function  SQLNotas_0190(): String;
    function  SQLNotas_C100(): String;
    function  SQLNotas_C170(): String;
  end;

  var
  FmEfdIcmsIpiErros_v03_0_1: TFmEfdIcmsIpiErros_v03_0_1;

implementation

uses UnMyObjects, Module, EfdIcmsIpiExporta_v03_0_1, ModuleNFe_0000, UMySQLModule, ModProd;

{$R *.DFM}

procedure TFmEfdIcmsIpiErros_v03_0_1.BtNFClick(Sender: TObject);
begin
  Grade_Jan.MostraFormEntrada(QrNotasFatID.Value, QrNotasIDCtrl.Value);
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.BtOKClick(Sender: TObject);
{
const
  Fator = 0.0265; // ??
var
  I: Integer;
  MD_NIVEL1: TfrxMasterData;
  MEMO1, Memo: TfrxMemoView;
  L, W: Double;
}
begin
{
  MD_NIVEL1 := frxErros.FindObject('MD_NIVEL1') as TfrxMasterData;
  MEMO1 := frxErros.FindObject('MEMO1') as TfrxMemoView;
  //
  L := 0;
  W := 0;
  for I := 0 to DBGLinha.Columns.Count -1 do
  begin
    L := L + W;
    W := DBGLinha.Columns[I].Width / (Fator * 100);
    Memo := TfrxMemoView.Create(MD_NIVEL1);
    Memo.CreateUniqueName;
    Memo.Visible := True;
    Memo.SetBounds(L, 0.4 / Fator, W, 0.27 / Fator);
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := 6;
    Memo.Memo.Text   := 'TESTE';
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.Width := 0.1;
    //
  end;
}
  MyObjects.frxMostra(frxErros, 'Erros de exporta��o SPED-EFD');
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.BtReduzidoClick(Sender: TObject);
const
  NewNome = '';
var
  GraGruX: Integer;
  Campo, Valor: String;
  //
  function ObtemGraGruX(Fld: String): Boolean;
  var
    I: Integer;
  begin
    GraGruX := 0;
    for I := 0 to QrLinha.FieldCount -1 do
    begin
      //Geral.MB_Info(QrLinha.Fields[I].FieldName);
      if Uppercase(QrLinha.Fields[I].FieldName) = Uppercase(Fld) then
      begin
        Campo := QrLinha.Fields[I].FieldName;
        Break;
      end
    end;
    if Campo <> '' then
      Valor := QrLinha.FieldByName(Campo).AsString;
    if Valor <> '' then
      GraGruX := Geral.IMV(Valor);
    Result := GraGruX <> 0;
  end;
begin
  Campo   := '';
  GraGruX := 0;
  if QrLinha.State <>  dsInactive then
  begin
    if not ObtemGraGruX('GraGruX') then
    if not ObtemGraGruX('COD_ITEM') then
    ;
  end;
  //
  Grade_Jan.MostraFormGraGruN_GGX(GraGruX, NewNome);
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.frxErrosGetValue(const VarName: string;
  var Value: Variant);
const
  Fator = 1;
  //
  function ObtemLeft(Index: Integer): Integer;
  var
    I: Integer;
  begin
    Result := 0;
    for I := 0 to Index -1(*DBGLinha.Columns.Count - 1*) do
    begin
      Result := Result + DBGLinha.Columns[I].Width;
    end;
    Result := Result * Fator;
  end;
var
  frxMemo: TfrxMemoView;
  Indice: Integer;
begin
  if VarName = 'VARF_COD_EMPRESA' then
    Value := FEmpresa
  else
  if VarName = 'VARF_NO_EMPRESA' then
    Value := FmEfdIcmsIpiExporta_v03_0_1.CBEmpresa.Text
  else
  if VarName = 'VARF_PERIODO' then
    Value := FmEfdIcmsIpiExporta_v03_0_1.EdMes.Text
  else
  if Copy(VarName, 1, 9) = 'Me_Tit_00' then
  begin
    Indice := Geral.IMV(Copy(VarName, 8)) - 1;
    if DBGLinha.Columns.Count > Indice then
    begin
      Value := True;
      frxMemo := frxErros.FindObject(VarName) as TfrxMemoView;
      frxMemo.Left := ObtemLeft(Indice);
      frxMemo.Width := DBGLinha.Columns[Indice].Width * Fator;
      frxMemo.Memo.Text := DBGLinha.Columns[Indice].Title.Caption;
    end else Value := False;
  end
  else
  if Copy(VarName, 1, 9) = 'Me_Val_00' then
  begin
    Indice := Geral.IMV(Copy(VarName, 8)) - 1;
    if DBGLinha.Columns.Count > Indice then
    begin
      Value := True;
      frxMemo := frxErros.FindObject(VarName) as TfrxMemoView;
      frxMemo.Left := ObtemLeft(Indice);
      frxMemo.Width := DBGLinha.Columns[Indice].Width * Fator;
      frxMemo.Memo.Text := QrLinha.FieldByName(DBGLinha.Columns[Indice].FieldName).AsString;
    end else Value := False;
  end;
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.QrNotasCalcFields(DataSet: TDataSet);
begin
  QrNotasNO_FATID.Value := DmNFe_0000.NomeFatID_NFe(QrNotasFatID.Value);
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.QrSpedEfdIcmsIpiErrsAfterScroll(DataSet: TDataSet);
begin
  ReopenLinha();
  ReopenNotas();
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.QrSpedEfdIcmsIpiErrsBeforeClose(DataSet: TDataSet);
begin
  QrLinha.Close;
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.QrSpedEfdIcmsIpiErrsCalcFields(DataSet: TDataSet);
var
  x: String;
begin
(*
  case QrSpedEfdIcmsIpiErrsErro.Value of
      1: x := 'Campo desconhecido';
      2: x := 'Campo de conte�do obrigat�rio sem conte�do';
      3: x := 'Erro de sequencia na gera��o de linha';
      4: x := 'Tipo de campo incorreto (C ou N)';
      5: x := 'Decimais difere do esperado';
      6: x := 'Documento inv�lido';
      7: x := 'Tamanho do texto difere do obrigat�rio';
      8: x := 'Tamanho do texto excede o m�ximo';
      9: x := 'Tipo de obrigatoriedade desconhecida';
     10: x := 'C�digo de servi�o LC 116/03 pode ser inv�lido';
     11: x := 'C�digo NCM n�o cadastrado para o produto';
     12: x := 'Campo n�o pode ser alfanum�rico';
     13: x := 'CFOP n�o consta na tabela SPED EFD "' + CO_NOME_TbSPEDEFD_CFOP + '"';
     14: x := 'Codigo do modelo do documento fiscal inv�lido';
     15: x := 'CFOP inv�lido. 1� caracter deve ser 1, 2 ou 3 para entrada e 5, 6 ou 7 para sa�da';
     16: x := 'Valor inv�lido. Soma dos VL_BC_ICMS dos registros anal�ticos n�o confere com o registro mestre';
     17: x := 'C�digo do munic�pio n�o definido';
     18: x := 'Campo obrigat�rio. Valor da opera��o';
     19: x := 'Registro filho obrigat�rio n�o informado';
     //
    else x := 'ERRO N�O IMPLEMENTADO. AVISE A DERMATEK!';
  end;
  QrSpedEfdIcmsIpiErrsDESCRI_ERRO.Value := x;
*)
  QrSpedEfdIcmsIpiErrsDESCRI_ERRO.Value := sEFD_Export_Err[QrSpedEfdIcmsIpiErrsErro.Value];
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.ReopenErros();
begin
{
  QrSpedEfdIcmsIpiErrs.Close;
  QrSpedEfdIcmsIpiErrs.Params[00].AsInteger := FImporExpor;
  QrSpedEfdIcmsIpiErrs.Params[01].AsInteger := FAnoMes;
  QrSpedEfdIcmsIpiErrs.Params[02].AsInteger := FEmpresa;
  UnDmkDAC_PF.AbreQuery(QrSpedEfdIcmsIpiErrs, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSpedEfdIcmsIpiErrs, Dmod.MyDB, [
  'SELECT * ',
  'FROM spedefdicmsipierrs ',
  'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
  'AND AnoMes=' + Geral.FF0(FAnoMes),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  '']);
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.ReopenLinha();
const
  CamposNot: array[0..11] of String = (
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'Lk',
    'DataCad', 'UserCad', 'DataAlt', 'UserAlt', 'AlterWeb', 'Ativo');
var
  Tabela: String;
  I, J, tTit, tTex: Integer;
  Achou: Boolean;
  Coluna: TColumn;
  CanvasSize: Integer;
  CanvasName: String;
begin
  Tabela := 'spedefdicmsipi' + QrSpedEfdIcmsIpiErrsREG.Value;
  QrLinha.Close;
  QrLinha.SQL.Clear;
  QrLinha.SQL.Add('SELECT * FROM ' + Tabela);
  QrLinha.SQL.Add('WHERE ImporExpor=:P0');
  QrLinha.SQL.Add('AND AnoMes=:P1');
  QrLinha.SQL.Add('AND Empresa=:P2');
  QrLinha.SQL.Add('AND LinArq=:P3');
  QrLinha.Params[00].AsInteger := FImporExpor;
  QrLinha.Params[01].AsInteger := FAnoMes;
  QrLinha.Params[02].AsInteger := FEmpresa;
  QrLinha.Params[03].AsInteger := QrSpedEfdIcmsIpiErrsLinArq.Value;
  UnDmkDAC_PF.AbreQuery(QrLinha, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  CanvasSize := Canvas.Font.Size;
  CanvasName := Canvas.Font.Name;
  //
  DBGLinha.Columns.Clear;
  for I := 0 to QrLinha.FieldCount -1 do
  begin
    Achou := False;
    for J := 0 to High(CamposNot) do
    begin
      if Uppercase(QrLinha.Fields[I].FieldName) = Uppercase(CamposNot[J]) then
      begin
        Achou := True;
        Break;
      end;
    end;
    if not Achou then
    begin
      Coluna := TColumn.Create(DBGLinha.Columns);
      Coluna.FieldName := QrLinha.Fields[I].FieldName;
      Canvas.Font.Size := Coluna.Title.Font.Size;
      Canvas.Font.Name := Coluna.Title.Font.Name;
      tTit := Canvas.TextWidth(QrLinha.Fields[I].FieldName);
      //
      Canvas.Font.Size := Coluna.Font.Size;
      Canvas.Font.Name := Coluna.Font.Name;
      tTex := Canvas.TextWidth(Qrlinha.FieldByName(QrLinha.Fields[I].FieldName).AsString);
      //
      if tTex > tTit then
        Coluna.Width := tTex + 8
      else
        Coluna.Width := tTit + 8;
    end;
  end;
  Canvas.Font.Size := CanvasSize;
  Canvas.Font.Name := CanvasName;
end;

procedure TFmEfdIcmsIpiErros_v03_0_1.ReopenNotas();
var
  Abre: Boolean;
begin
  Abre := True;
  QrNotas.Close;
  QrNotas.SQL.Clear;
  if QrLinha.FieldByName('REG').AsString = '0150' then
    QrNotas.SQL.Text := SQLNotas_0150()
  else
  if QrLinha.FieldByName('REG').AsString = '0190' then
    QrNotas.SQL.Text := SQLNotas_0190()
  else
  if QrLinha.FieldByName('REG').AsString = 'C100' then
    QrNotas.SQL.Text := SQLNotas_C100()
  else
  if QrLinha.FieldByName('REG').AsString = 'C170' then
    QrNotas.SQL.Text := SQLNotas_C170()
  else
    Abre := False;
  if Abre then
  begin
    UnDmkDAC_PF.AbreQuery(QrNotas, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    LaNotas.Caption := 'Notas Fiscais dependentes';
    LaNotas.Font.Color := clHotLight;
    DBGNotas.Visible := True;
  end else
  begin
    // Abrir sem registros para poder imprimir
    QrNotas.SQL.Add('SELECT * FROM nfecaba WHERE FatID=-99999999');
    UnDmkDAC_PF.AbreQuery(QrNotas, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    LaNotas.Caption := 'Pesquisa de NFs n�o implementado. Solicite � DERMATEK';
    LaNotas.Font.Color := clRed;
    DBGNotas.Visible := False;
  end;
end;

function TFmEfdIcmsIpiErros_v03_0_1.SQLNotas_0150(): String;
var
  Entidade: String;
begin
  Entidade := QrLinha.FieldByName('COD_PART').AsString;
  if Trim(Entidade) = '' then
    Entidade := '0';
  //
  Result :=
'SELECT * FROM nfecaba' + sLineBreak +
'WHERE Empresa=' + FEmprTXT + sLineBreak +
'AND DataFiscal BETWEEN "' + FDataIni + '" AND "' + FDataFim + '"' + sLineBreak +
'AND CodInfoEmit=' + Entidade + sLineBreak +
'OR CodInfoDest=' + Entidade;
end;

function TFmEfdIcmsIpiErros_v03_0_1.SQLNotas_0190(): String;
var
  Unidade: String;
begin
  Unidade := QrLinha.FieldByName('UNID').AsString;
  Result :=
'SELECT nfea.*' + sLineBreak +
'FROM nfeitsi nfei' + sLineBreak +
'LEFT JOIN nfecaba nfea ON ' + sLineBreak +
'    nfea.FatID=nfei.FatID AND' + sLineBreak +
'    nfea.FatNum=nfei.FatNum AND' + sLineBreak +
'    nfea.Empresa=nfei.Empresa' + sLineBreak +
'WHERE nfea.Empresa=' + FEmprTXT + sLineBreak +
'AND nfea.DataFiscal BETWEEN "' + FDataIni + '" AND "' + FDataFim + '"' + sLineBreak +
'AND nfei.prod_uTrib="' + Unidade + '"';

end;

function TFmEfdIcmsIpiErros_v03_0_1.SQLNotas_C100: String;
var
  FatID, FatNum, Empresa: Integer;
begin
  Empresa := QrLinha.FieldByName('Empresa').AsInteger;
  FatID := QrLinha.FieldByName('FatID').AsInteger;
  FatNum := QrLinha.FieldByName('FatNum').AsInteger;
  //
  Result := 
'SELECT * FROM nfecaba' + sLineBreak +
'WHERE Empresa=' + FormatFloat('0', Empresa) + sLineBreak +
'AND FatID=' + FormatFloat('0', FatID) + sLineBreak +
'AND FatNum=' + FormatFloat('0', FatNum) + sLineBreak +
'';
end;

function TFmEfdIcmsIpiErros_v03_0_1.SQLNotas_C170(): String;
var
  ImporExpor, AnoMes, C100, FatID, FatNum, Empresa: Integer;
begin
  ImporExpor := QrLinha.FieldByName('ImporExpor').AsInteger;
  AnoMes  := QrLinha.FieldByName('AnoMes').AsInteger;
  Empresa := QrLinha.FieldByName('Empresa').AsInteger;
  C100  := QrLinha.FieldByName('C100').AsInteger;
  UnDmkDAC_PF.AbreMySQLQuery0(QrC100, Dmod.MyDB, [
  'SELECT FatID, FatNum, Empresa ',
  'FROM spedefdicmsipic100 ',
  'WHERE ImporExpor=' + FormatFloat('0', ImporExpor),
  'AND AnoMes=' + FormatFloat('0', AnoMes),
  'AND Empresa=' + FormatFloat('0', Empresa),
  'AND LinArq=' + FormatFloat('0', C100),
  '']);
  FatID := QrC100FatID.Value;
  FatNum := QrC100FatNum.Value;
  Result :=
'SELECT * FROM nfecaba' + sLineBreak +
'WHERE Empresa=' + FormatFloat('0', Empresa) + sLineBreak +
'AND FatID=' + FormatFloat('0', FatID) + sLineBreak +
'AND FatNum=' + FormatFloat('0', FatNum) + sLineBreak +
'';
end;

end.
