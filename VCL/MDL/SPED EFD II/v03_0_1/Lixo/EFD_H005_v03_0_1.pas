unit EFD_H005_v03_0_1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmEFD_H005_v03_0_1 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    TPDT_INV: TdmkEditDateTimePicker;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdLinArq: TdmkEdit;
    Label6: TLabel;
    EdMOT_INV: TdmkEdit;
    Label2: TLabel;
    EdNO_MOT_INV: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdMOT_INVChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEFD_H005_v03_0_1: TFmEFD_H005_v03_0_1;

implementation

uses UnMyObjects, Module, EFD_E001, UMySQLModule, SPED_Listas, UnDmkProcFunc;

{$R *.DFM}

procedure TFmEFD_H005_v03_0_1.BtOKClick(Sender: TObject);
const
  REG = 'E005';
var
  DT_INV, MOT_INV: String;
  ImporExpor, AnoMes, Empresa, LinArq: Integer;
  //VL_INV: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  LinArq         := EdLinArq.ValueVariant;
  //REG            := ;
  DT_INV         := Geral.FDT(TPDT_INV.Date, 1);
  //VL_INV         := ;
  MOT_INV        := EdMOT_INV.Text;

  //
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_h005', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  SQLType, LinArq, siPositivo, EdLinArq);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efd_h005', False, [
  'REG', 'DT_INV', (*'VL_INV',*)
  'MOT_INV'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  REG, DT_INV, (*VL_INV,*)
  MOT_INV], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
    FmEFD_E001.ReopenEFD_H005(LinArq);
    Close;
  end;
end;

procedure TFmEFD_H005_v03_0_1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_H005_v03_0_1.EdMOT_INVChange(Sender: TObject);
begin
  EdNO_MOT_INV.Text := dmkPF.TextoDeLista(sEFD_MOT_INV, EdMOT_INV.ValueVariant);
end;

procedure TFmEFD_H005_v03_0_1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_H005_v03_0_1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
