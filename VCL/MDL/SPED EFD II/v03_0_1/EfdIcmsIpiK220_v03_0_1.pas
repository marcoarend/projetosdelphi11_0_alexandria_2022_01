unit EfdIcmsIpiK220_v03_0_1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup, AppListas,
  UnEfdIcmsIpi_PF, UnEfdIcmsIpi_PF_v03_0_1, UnEfdIcmsIpi_Jan,
  UnDmkProcFunc, UnAppEnums, UnProjGroup_Consts;

type
  TFmEfdIcmsIpiK220_v03_0_1 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    QrGraGruX1: TmySQLQuery;
    QrGraGruX1Controle: TIntegerField;
    QrGraGruX1NO_PRD_TAM_COR: TWideStringField;
    QrGraGruX1GraGru1: TIntegerField;
    QrGraGruX1SIGLAUNIDMED: TWideStringField;
    QrGraGruX1GerBxaEstq: TSmallintField;
    QrGraGruX1NCM: TWideStringField;
    QrGraGruX1UnidMed: TIntegerField;
    QrGraGruX1Ex_TIPI: TWideStringField;
    DsGraGruX1: TDataSource;
    Panel5: TPanel;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    DsEntidades: TDataSource;
    QrGraGruX2: TmySQLQuery;
    QrGraGruX2Controle: TIntegerField;
    QrGraGruX2NO_PRD_TAM_COR: TWideStringField;
    QrGraGruX2GraGru1: TIntegerField;
    QrGraGruX2SIGLAUNIDMED: TWideStringField;
    QrGraGruX2GerBxaEstq: TSmallintField;
    QrGraGruX2NCM: TWideStringField;
    QrGraGruX2UnidMed: TIntegerField;
    QrGraGruX2Ex_TIPI: TWideStringField;
    DsGraGruX2: TDataSource;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    EdPeriApu: TdmkEdit;
    EdKndTab: TdmkEdit;
    EdKndCod: TdmkEdit;
    EdKndNSU: TdmkEdit;
    EdKndItmOri: TdmkEdit;
    EdKndAID: TdmkEdit;
    EdKndNiv: TdmkEdit;
    EdIDSeq1: TdmkEdit;
    EdKndItmDst: TdmkEdit;
    Label15: TLabel;
    TPDT_MOV: TdmkEditDateTimePicker;
    StaticText1: TStaticText;
    Label232: TLabel;
    EdGGXOri: TdmkEditCB;
    CBGGXOri: TdmkDBLookupComboBox;
    DBText1: TDBText;
    Label1: TLabel;
    EdGGXDSt: TdmkEditCB;
    CBGGXDst: TdmkDBLookupComboBox;
    DBText2: TDBText;
    StaticText2: TStaticText;
    EdQTD_ORI: TdmkEdit;
    StaticText3: TStaticText;
    EdQTD_DEST: TdmkEdit;
    StaticText4: TStaticText;
    EdQTD_: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdMovimIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMovimIDChange(Sender: TObject);
    procedure EdAnoMesChange(Sender: TObject);
  private
    { Private declarations }
    //FArrTxtMovimID: MyArrayLista;
    procedure ReopenGraGruX();

  public
    { Public declarations }
    FBalID, FBalNum, FBalItm, FBalEnt, FESOMIEM, FOrigemIDKnd, FID_SEK: Integer;
  end;

  var
  FmEfdIcmsIpiK220_v03_0_1: TFmEfdIcmsIpiK220_v03_0_1;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, MyListas,
  ModuleFin, UnFinanceiro, GraGruX, SpedEfdIcmsIpi_v03_0_1;

{$R *.DFM}

procedure TFmEfdIcmsIpiK220_v03_0_1.BtOKClick(Sender: TObject);
const
  COD_DOC_OS = '';
var
  DT_MOV, COD_ITEM_ORI, COD_ITEM_DEST: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU,
  KndItmOri, KndItmDst, KndAID, KndNiv, IDSeq1, ID_SEK, GGXDst, GGXOri, ESOMIEM,
  OrigemIDKnd: Integer;
  QTD, QTD_ORI, QTD_DEST: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  PeriApu        := EdPeriApu.ValueVariant;
  if SQLType = stIns then
  begin
    KndTab         := Integer(TEstqSPEDTabSorc.estsNoSrc);
    KndCod         := PeriApu;
    KndNSU         := PeriApu;
    KndItmOri      := 0;
    KndItmDst      := 0;
    KndAID         := Integer(TEstqMovimID.emidAjuste);
    KndNiv         := Integer(TEstqMovimNiv.eminSemNiv);
    IDSeq1         := 0; // Abaixo
    ID_SEK         := Integer(TSPED_EFD_ComoLancou.seclLancadoManual);
    ESOMIEM        := Integer(TEstqSPED220OMIEM.esomiemIndef);
    OrigemIDKnd    := Integer(TOrigemIDKnd.oidk000ND);
  end else
  begin
    KndTab         := EdKndTab.ValueVariant;
    KndCod         := EdKndCod.ValueVariant;
    KndNSU         := EdKndNSU.ValueVariant;
    KndItmOri      := EdKndItmOri.ValueVariant;
    KndItmDst      := EdKndItmDst.ValueVariant;
    KndAID         := EdKndAID.ValueVariant;
    KndNiv         := EdKndNiv.ValueVariant;
    IDSeq1         := EdIDSeq1.ValueVariant;
    ID_SEK         := FID_SEK;
    ESOMIEM        := FESOMIEM;
    OrigemIDKnd    := FOrigemIDKnd;
  end;
  //ID_SEK         := ;
  DT_MOV         := Geral.FDT(TPDT_MOV.Date, 1);
  GGXOri         := EdGGXOri.ValueVariant;
  GGXDst         := EdGGXDst.ValueVariant;
  COD_ITEM_ORI   := dmkPF.ITS_Null(GGXOri);
  COD_ITEM_DEST  := dmkPF.ITS_Null(GGXDst);
  QTD_ORI        := EdQTD_ORI.ValueVariant;
  QTD_DEST       := EdQTD_DEST.ValueVariant;
  QTD            := EdQTD_.ValueVariant;
  //
  if MyObjects.FIC(GGXOri = 0, EdGGXOri,
    'Informe o c�digo do produto de origem!') then Exit;
  if MyObjects.FIC(GGXDst = 0, EdGGXDst,
    'Informe o c�digo do produto de destino!') then Exit;
  if MyObjects.FIC(Trim(QrGraGruX1SIGLAUNIDMED.Value) = '', EdGGXOri,
    'C�digo do produto origem sem sigla de unidade definida!') then Exit;
  if MyObjects.FIC(Trim(QrGraGruX2SIGLAUNIDMED.Value) = '', EdGGXDst,
    'C�digo do produto destino sem sigla de unidade definida!') then Exit;
  //
  (*Campo 01 (REG) - Valor V�lido: [K220]*)
  (*Campo 02 (DT_MOV) -: Valida��o: a data deve estar compreendida no per�odo
  informado nos campos DT_INI e DT_FIN do Registro K100.*)

  if not EfdIcmsIpi_PF.ValidaDataDentroDoAnoMesSPED(AnoMes, Trunc(TPDT_MOV.Date),
  COD_DOC_OS, (*Obrigatorio*)True) then Exit;

  (*Campo 03 (COD_ITEM_ORI) -: Valida��o: o c�digo do item de origem dever�
  existir no campo COD_ITEM do Registro 0200.*)

  if not EfdIcmsIpi_PF.ValidaGGXEstahDentroDoAnoMesSPED(AnoMes, GGXOri,
  COD_DOC_OS) then Exit;

  (*Campo 04 (COD_ITEM_DEST) -: Valida��o: o c�digo do item de destino dever�
  existir no campo COD_ITEM do Registro 0200. O valor informado deve ser
  diferente do COD_ITEM_ORI.*)

  if not EfdIcmsIpi_PF.ValidaGGXEstahDentroDoAnoMesSPED(AnoMes, GGXDst,
  COD_DOC_OS) then Exit;
  if not EfdIcmsIpi_PF.ValidaGGXOri_DifereGGXDstDentroDoAnoMesSPED(AnoMes,
  GGXOri, GGXDst, COD_DOC_OS) then Exit;

  (*Campo 05 (QTD_ORI) -: Preenchimento: informar a quantidade movimentada do
  item de origem codificado no campo COD_ITEM_ORI.
  Valida��o: este campo deve ser maior que zero.*)

  if not EfdIcmsIpi_PF.ValidaQtdeDentroDoAnoMesSPED(AnoMes, QTD_ORI, COD_DOC_OS,
  (*MaiorQueZero*)True) then Exit;

  (*Campo 06 (QTD_DEST) -: Preenchimento: informar a quantidade movimentada do
  item de destino codificado no campo COD_ITEM_DEST.
  Valida��o: este campo deve ser maior que zero.*)

  if not EfdIcmsIpi_PF.ValidaQtdeDentroDoAnoMesSPED(AnoMes, QTD_DEST, COD_DOC_OS,
  (*MaiorQueZero*)True) then Exit;

  //
  KndItmOri := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik220', 'KndItmOri', [
  'ImporExpor', 'AnoMes', 'KndTab'], [ImporExpor, AnoMes, KndTab],
  SQLType, KndItmOri, siPositivo, EdKndItmOri);
  //
  KndItmDst := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik220', 'KndItmDst', [
  'ImporExpor', 'AnoMes', 'KndTab'], [ImporExpor, AnoMes, KndTab],
  SQLType, KndItmDst, siPositivo, EdKndItmDst);
  //
  IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik220', 'IDSeq1', [
  'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
  ImporExpor, AnoMes, Empresa, PeriApu],
  SQLType, IDSeq1, siPositivo, EdIDSeq1);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdicmsipik220', False, [
  'KndAID', 'KndNiv', 'IDSeq1',
  'ID_SEK', 'DT_MOV', 'COD_ITEM_ORI',
  'COD_ITEM_DEST', 'QTD', 'GGXDst',
  'GGXOri', 'ESOMIEM', 'OrigemIDKnd',
  'KndItmOri', 'KndItmDst'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'QTD_ORI', 'QTD_DEST'], [
  KndAID, KndNiv, IDSeq1,
  ID_SEK, DT_MOV, COD_ITEM_ORI,
  COD_ITEM_DEST, QTD, GGXDst,
  GGXOri, ESOMIEM, OrigemIDKnd,
  KndItmOri, KndItmDst], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, QTD_ORI, QTD_DEST], True) then
  begin
    DfSEII_v03_0_1.ReopenK220(KndItmOri, KndItmDst);
    Close;
  end;
end;

procedure TFmEfdIcmsIpiK220_v03_0_1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpiK220_v03_0_1.EdAnoMesChange(Sender: TObject);
begin
  EfdIcmsIpi_PF.ConfigDtasMinMaxAnoMesSPED(EdAnoMes.ValueVariant, TPDT_MOV);
end;

procedure TFmEfdIcmsIpiK220_v03_0_1.EdMovimIDChange(Sender: TObject);
(*
var
  MovimID: Integer;
*)
begin
(*
  MovimID := EdMovimID.ValueVariant;
  EdMovimID_Txt.text := sEstqMovimID_FRENDLY[MovimID];
*)
end;

procedure TFmEfdIcmsIpiK220_v03_0_1.EdMovimIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(*
var
  TitCols: array[0..1] of String;
*)
begin
(*
  if (Key = VK_F4) then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdMovimID.Text := Geral.SelecionaItemLista(sEstqMovimID_FRENDLY, 0,
    'SEL-LISTA-000 :: Sele��o do ID do movimento', TitCols, Screen.Width)
  end;
*)
end;

procedure TFmEfdIcmsIpiK220_v03_0_1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiK220_v03_0_1.FormCreate(Sender: TObject);
begin
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  FBalID  := 0;
  FBalNum := 0;
  FBalItm := 0;
  FBalEnt := 0;
  ReopenGraGruX();
end;

procedure TFmEfdIcmsIpiK220_v03_0_1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdIcmsIpiK220_v03_0_1.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
  procedure Abre(Qry: TmySQLQuery);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
    'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
    SQL_LFT,
    'WHERE ggx.Controle > 0 ', //-900000 ',
    SQL_AND,
    'ORDER BY NO_PRD_TAM_COR ',
    '']);
  end;
begin
  SQL_AND := '';
  SQL_LFT := '';
  if TdmkAppID(CO_DMKID_APP) = dmkappB_L_U_E_D_E_R_M then
  begin
    SQL_AND := 'AND NOT (pqc.PQ IS NULL)';
    SQL_LFT := 'LEFT JOIN pqcli pqc ON pqc.PQ=gg1.Nivel1';
    //
    Abre(QrGraGruX1);
    Abre(QrGraGruX2);
  end;
end;

end.
