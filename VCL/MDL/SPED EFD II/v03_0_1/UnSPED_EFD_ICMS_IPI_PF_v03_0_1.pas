unit UnSPED_EFD_ICMS_IPI_PF_v03_0_1;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) dmkEdit, dmkRadioGroup,
  dmkMemo, AdvToolBar, dmkCheckGroup, UnDmkProcFunc, TypInfo, UnMyObjects,
  dmkEditDateTimePicker;

type
{
  TEstagioVS_SPED = (evsspedNone=0, evsspedEncerraVS=1, evsspedGeraSPED=2,
                     evsspedExportaSPED=3);
  TCorrApoFormaLctoPosNegSPED = (caflpnIndef=0, caflpnMovNormEsquecido=1,
                               caflpnDesfazimentoMov=2);
}
  TUnSPED_EFD_ICMS_IPI_PF_v03_0_1 = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormEfdIcmsIpiE001();
    procedure MostraFormSPEDEFDEnce();
    procedure MostraFormSpedefdIcmsIpiBalancos(ImporExpor, AnoMes, Empresa,
              LinArq: Integer; Registro: String; Data: TDateTime);
    procedure MostraFormSpedEfdIcmsIpiClaProd(ImporExpor, AnoMes, Empresa,
              LinArq: Integer; Registro: String; Data: TDateTime);
    procedure MostraFormEfdIcmsIpiExporta();
  end;
var
  SPED_EFD_ICMS_IPI_PF_v03_0_1: TUnSPED_EFD_ICMS_IPI_PF_v03_0_1;

implementation

uses UnMLAGeral, MyDBCheck, DmkDAC_PF, Module, ModuleGeral, UnDmkWeb, Restaura2,
  UMySQLModule, UnGrade_PF,
///////////////////////////     SPED ICMS IPI    ///////////////////////////////
  EfdIcmsIpiExporta_v03_0_1, EfdIcmsIpiE001_v03_0_1, EfdIcmsIpiEnce_v03_0_1,
  SpedEfdIcmsipiBalancos_v03_0_1, SpedEfdIcmsIpiClaProd_v03_0_1,
///////////////////////////     SPED ICMS IPI    ///////////////////////////////
  ModAppGraG1, ModVS;


{ TUnSPED_EFD_ICMS_IPI_PF_v03_0_1 }

procedure TUnSPED_EFD_ICMS_IPI_PF_v03_0_1.MostraFormEfdIcmsIpiE001();
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiE001_v03_0_1, FmEfdIcmsIpiE001_v03_0_1, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiE001_v03_0_1.ShowModal;
    FmEfdIcmsIpiE001_v03_0_1.Destroy;
  end;
end;

procedure TUnSPED_EFD_ICMS_IPI_PF_v03_0_1.MostraFormSPEDEFDEnce();
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiEnce_v03_0_1, FmEfdIcmsIpiEnce_v03_0_1, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiEnce_v03_0_1.ShowModal;
    FmEfdIcmsIpiEnce_v03_0_1.Destroy;
  end;
end;

procedure TUnSPED_EFD_ICMS_IPI_PF_v03_0_1.MostraFormSpedefdIcmsIpiBalancos(
  ImporExpor, AnoMes, Empresa, LinArq: Integer; Registro: String;
  Data: TDateTime);
var
  Ano, Mes: Word;
begin
  if DBCheck.CriaFm(TFmSpedefdIcmsIpiBalancos_v03_0_1, FmSpedefdIcmsIpiBalancos_v03_0_1, afmoNegarComAviso) then
  begin
    FmSpedefdIcmsIpiBalancos_v03_0_1.FImporExpor := ImporExpor;
    FmSpedefdIcmsIpiBalancos_v03_0_1.FAnoMes     := AnoMes;
    FmSpedefdIcmsIpiBalancos_v03_0_1.FEmpresa    := Empresa;
    FmSpedefdIcmsIpiBalancos_v03_0_1.FRegPai     := LinArq;
    FmSpedefdIcmsIpiBalancos_v03_0_1.FData       := Data;
    //
    dmkPF.AnoMesDecode(AnoMes, Ano, Mes);
    FmSpedefdIcmsIpiBalancos_v03_0_1.CBMes.ItemIndex := Mes - 1;
    FmSpedefdIcmsIpiBalancos_v03_0_1.CBAno.Text      := Geral.FF0(Ano);
    FmSpedefdIcmsIpiBalancos_v03_0_1.TPData.Date     := Data;
    //
    if Registro = 'H010' then
    begin
      FmSpedefdIcmsIpiBalancos_v03_0_1.PCRegistro.ActivePageIndex := 1;
      FmSpedefdIcmsIpiBalancos_v03_0_1.LaData.Visible             := False;
      FmSpedefdIcmsIpiBalancos_v03_0_1.TPData.Visible             := False;
      FmSpedefdIcmsIpiBalancos_v03_0_1.PnPeriodo.Enabled          := True;
    end else
    if Registro = 'K200' then
    begin
      FmSpedefdIcmsIpiBalancos_v03_0_1.PCRegistro.ActivePageIndex := 2;
      FmSpedefdIcmsIpiBalancos_v03_0_1.LaData.Visible             := True;
      FmSpedefdIcmsIpiBalancos_v03_0_1.TPData.Visible             := True;
      FmSpedefdIcmsIpiBalancos_v03_0_1.PnPeriodo.Enabled          := False;
    end else
      FmSpedefdIcmsIpiBalancos_v03_0_1.PCRegistro.ActivePageIndex := 0;
    //
    if FmSpedefdIcmsIpiBalancos_v03_0_1.PCRegistro.ActivePageIndex > 0 then
      FmSpedefdIcmsIpiBalancos_v03_0_1.ShowModal
    else
      Geral.MB_Erro('Registro n�o implementdo em balan�o de SPED EFD: ' +
      Registro);
    FmSpedefdIcmsIpiBalancos_v03_0_1.Destroy;
  end;
end;

procedure TUnSPED_EFD_ICMS_IPI_PF_v03_0_1.MostraFormSpedEfdIcmsIpiClaProd(
  ImporExpor, AnoMes, Empresa, LinArq: Integer; Registro: String;
  Data: TDateTime);
var
  Ano, Mes: Word;
begin
  if DBCheck.CriaFm(TFmSpedEfdIcmsIpiClaProd_v03_0_1, FmSpedEfdIcmsIpiClaProd_v03_0_1, afmoNegarComAviso) then
  begin
    FmSpedEfdIcmsIpiClaProd_v03_0_1.FImporExpor := ImporExpor;
    FmSpedEfdIcmsIpiClaProd_v03_0_1.FAnoMes     := AnoMes;
    FmSpedEfdIcmsIpiClaProd_v03_0_1.FEmpresa    := Empresa;
    FmSpedEfdIcmsIpiClaProd_v03_0_1.FPeriApu    := LinArq;
    FmSpedEfdIcmsIpiClaProd_v03_0_1.FData       := Data;
    //
    dmkPF.AnoMesDecode(AnoMes, Ano, Mes);
    FmSpedEfdIcmsIpiClaProd_v03_0_1.CBMes.ItemIndex := Mes - 1;
    FmSpedEfdIcmsIpiClaProd_v03_0_1.CBAno.Text      := Geral.FF0(Ano);
    FmSpedEfdIcmsIpiClaProd_v03_0_1.TPData.Date     := Data;
    //
    if Registro = 'K220' then
    begin
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PCRegistro.ActivePageIndex := 1;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.LaData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.TPData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PnPeriodo.Enabled          := True;
    end else
    if Registro = 'K230' then
    begin
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PCRegistro.ActivePageIndex := 2;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.LaData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.TPData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PnPeriodo.Enabled          := True;
    end else
(*
    if Registro = 'K200' then
    begin
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PCRegistro.ActivePageIndex := 2;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.LaData.Visible             := True;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.TPData.Visible             := True;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PnPeriodo.Enabled          := False;
    end else
*)
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PCRegistro.ActivePageIndex := 0;
    //
    if FmSpedEfdIcmsIpiClaProd_v03_0_1.PCRegistro.ActivePageIndex > 0 then
      FmSpedEfdIcmsIpiClaProd_v03_0_1.ShowModal
    else
      Geral.MB_Erro('Registro n�o implementdo em SPED EFD K2XX: ' +
      Registro);
    FmSpedEfdIcmsIpiClaProd_v03_0_1.Destroy;
  end;
end;

procedure TUnSPED_EFD_ICMS_IPI_PF_v03_0_1.MostraFormEfdIcmsIpiExporta();
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiExporta_v03_0_1, FmEfdIcmsIpiExporta_v03_0_1, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiExporta_v03_0_1.EdEmpresa.ValueVariant := 1;
    FmEfdIcmsIpiExporta_v03_0_1.CBEmpresa.KeyValue := 1;
    FmEfdIcmsIpiExporta_v03_0_1.RGCOD_FIN.ItemIndex := 1;
    FmEfdIcmsIpiExporta_v03_0_1.RGPreenche.ItemIndex := 2;
    FmEfdIcmsIpiExporta_v03_0_1.EdMes.ValueVariant := Date - 21;
    FmEfdIcmsIpiExporta_v03_0_1.ShowModal;
    FmEfdIcmsIpiExporta_v03_0_1.Destroy;
  end;
end;

end.
