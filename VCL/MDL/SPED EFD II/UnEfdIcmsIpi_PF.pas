unit UnEfdIcmsIpi_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) dmkEdit, dmkRadioGroup,
  dmkMemo, dmkCheckGroup, UnDmkProcFunc, TypInfo, UnMyObjects,
  dmkEditDateTimePicker, SPED_Listas, UnMyVCLRef;

type
  TMyArrOf2ArrOfInt = array of array[0..1] of Integer;
  //
  TUnEfdIcmsIpi_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  AnoMesAnterior(): Integer;
    procedure AvisoItemJaImportado(MeAviso: TMemo; IDItem, GraGruX: Integer;
              REG: String);
    procedure AvisoQtdeZero(MeAviso: TMemo; IDItem, GraGruX, MovimCod, Controle:
              Integer; REG: String);
    procedure BaixaLayoutSPED(Memo: TMemo);
    function  DefineDatasPeriodoSPED(const Empresa, AnoMes: Integer;
              const Registro: String; const LastDtIni, LastDtFim: TDateTime;
              var NewDtIni, NewDtFim: TDateTime): Boolean;
    function  DefineDatasPeriCorrApoSPED(const Empresa: Integer; const Registro:
              String; const DtLancto, DtCorrApo: TDateTime;
              var ThatDtIni, ThatDtFim: TDateTime): Boolean;
    function  DefineQuantidadePositivaOuNegativa(const regOri, REG: String;
              const ValAntes, ValNovo: Double; const FormaLcto:
              TCorrApoFormaLctoPosNegSPED; var QTD_COR_POS, QTD_COR_NEG:
              Double): Boolean;
    function  EncerraMesSPED(ImporExpor, AnoMes, Empresa: Integer; Campo:
              String): Boolean;

              //

    function  DataFimObrigatoria(DtHrFimOpe, DiaFim: TDateTime): TDateTime;
    function  DefineTabProdOuSubPrd(GraGruX: Integer): TTabProdOuSubPrd;
    function  DefineTabelaInsProdOuSubPrd(TabProdOuSubPrd: TTabProdOuSubPrd;
              TabProduto, TabSubProd, ProcName: String): String;
    function  ErrGrandeza(TabSrc, GGxSrc, GGXDst: String): Boolean;
              // ...

    function  HabilitaDocOP(KndTab: Integer): Boolean;
    procedure HabilitaMovimCodPorKndTab(Form: TForm; KndTab: Integer; Objetos:
              array of TObject);
    function  LiberaAcaoVS_SPED(ImporExpor, AnoMes, Empresa: Integer; EstagioVS_SPED:
              TEstagioVS_SPED): Boolean;

    //
    function  ObtemIND_ESTeCOD_PART(const Empresa, ClientMO, EntiSitio, GraGruX:
              Integer; var IND_EST, COD_PART: String; var Entidade: Integer):
              String;
    function  ObtemSPED_EFD_KndRegOrigem(const REG: String; var Origem:
              TSPED_EFD_KndRegOrigem): Boolean;
    function  ObtemSPED_EFD_REGdeKndRegOrigem(const Origem:
              TSPED_EFD_KndRegOrigem; var REG: String): Boolean;
    function  ObtemTipoPeriodoRegistro(Empresa: Integer; Registro, ProcOri: String):
              Integer;
    function  ObtemPeriodoSPEDdeData(Data: TDateTime; TipoPeriodoFiscal:
              TTipoPeriodoFiscal): Integer;
    function  ObtemDatasDePeriodoSPED(const Ano, Mes, Periodo: Integer;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; var DataIni, DataFim:
              TDateTime): Boolean;
    function  ObtemTipoDeProducaoSPED(EntiEmp: Integer): Integer;
    procedure ReopenTbSpedEfdXXX(Query: TmySQLQuery; DtIni, DtFim: TDateTime;
              TabName, FldOrd: String);
    function  SPEDEFDEnce_AnoMes(Campo: String): Integer;
    function  SPEDEFDEnce_DataMinMovim(Campo: String): TDateTime;
    function  SPEDEFDEnce_Periodo(Campo: String): Integer;
    function  SPEDEFDEnce_DataUltimoInventario(Empresa: Integer): TDateTime;
    function  SQLPeriodoFiscal(TipoPeriodoFiscal: TTipoPeriodoFiscal; CampoPsq,
              CampoRes: String; VirgulaFinal: Boolean): String;
    function  VerificaGGX_SPED(GraGruX, KndCod, KndNSU, KndItm: Integer;
              REG: String): Boolean;

    function  DefineDebCred(Valor: Double): TDebCred; overload;
    function  DefineDebCred(ValPos, ValNeg: Double): TDebCred; overload;
    procedure ConfigDtasMinMaxAnoMesSPED(AnoMes: Integer; TPDT_SPED:
              TdmkEditDateTimePicker);
    function  ValidaDataItemDentroDoAnoMesSPED(AnoMes: Integer; Data, DtIniOP,
              DtFimOP: TDateTime; OrdemDeServico: String): Boolean;
    function  ValidaDataDentroDoAnoMesSPED(AnoMes: Integer; Data:
              TDateTime; OrdemDeServico: String; Obrigatorio: Boolean): Boolean;
    function  ValidaDataInicialDentroDoAnoMesSPED(AnoMes: Integer; DataIni,
              DataFim: TDateTime; OrdemDeServico: String): Boolean;
    function  ValidaDataFinalDentroDoAnoMesSPED(AnoMes: Integer; DataIni,
              DataFim: TDateTime; OrdemDeServico: String): Boolean;
    function  ValidaDataUnicaDentroDoAnoMesSPED(AnoMes: Integer; Data:
              TDateTime; OrdemDeServico: String): Boolean;
    function  ValidaOSDentroDoAnoMesSPED(AnoMes: Integer; DataIni,
              DataFim: TDateTime; OrdemDeServico: String): Boolean;
    function  ValidaQtdeDentroDoAnoMesSPED(AnoMes: Integer; Qtde: Double;
              OrdemDeServico: String; MaiorQueZero: Boolean): Boolean;
    function  ValidaGGXEstahDentroDoAnoMesSPED(AnoMes: Integer; GraGruX:
              Integer; OrdemDeServico: String): Boolean;
    function  ValidaGGXOri_DifereGGXDstDentroDoAnoMesSPED(AnoMes, GGXOri,
              GGXDst: Integer; OrdemDeServico: String): Boolean;
    function  ValidaExigenciaValorMaiorQueZeroOuZero(AnoMes: Integer; DataIni,
              DataFim: TDateTime; Qtd: Double; OrdemDeServico: String): Boolean;
    function  ValidaCOD_INS_SUBST_DentroDoAnoMesSPED(AnoMes, GraGruX: Integer;
              COD_INS_SUBST, COD_DOC_OS: String): Boolean;
    function  TxtValidoDeGGX(GraGruX: Integer; REG, sProcName: String): String;
  end;
var
  EfdIcmsIpi_PF: TUnEfdIcmsIpi_PF;

implementation

uses UnMLAGeral, MyDBCheck, DmkDAC_PF, Module, ModuleGeral, UnDmkWeb, Restaura2,
  UMySQLModule, UnGrade_PF, ModProd,
///////////////////////////     SPED ICMS IPI    ///////////////////////////////
  UnEfdIcmsIpi_PF_v03_0_1,
///////////////////////////     SPED ICMS IPI    ///////////////////////////////
  ModAppGraG1;


{ TUnEfdIcmsIpi_PF }

function TUnEfdIcmsIpi_PF.AnoMesAnterior(): Integer;
begin
  Result := DmkPF.DataToAnoMes(Date);
  Result := DmkPF.IncrementaAnoMes(Result, -1);
end;

procedure TUnEfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso: TMemo; IDItem, GraGruX:
  Integer; REG: String);
begin
  if MeAviso <> nil then
    MeAviso.Lines.Add('Item j� importado! > ID: ' + Geral.FF0(IDItem) +
    ' Reduzido: ' + Geral.FF0(GraGruX) + ' ' + REG);
end;

procedure TUnEfdIcmsIpi_PF.AvisoQtdeZero(MeAviso: TMemo; IDItem, GraGruX,
  MovimCod, Controle: Integer; REG: String);
begin
  if MeAviso <> nil then
    MeAviso.Lines.Add('Item com quantidade zerada! > ID: ' + Geral.FF0(IDItem) +
    ' Reduzido: ' + Geral.FF0(GraGruX) + ' ' + REG + ' IME-C ' + Geral.FF0(
    MovimCod) + ' IME-I ' + Geral.FF0(Controle));
end;

procedure TUnEfdIcmsIpi_PF.BaixaLayoutSPED(Memo: TMemo);
var
  Res: Boolean;
  ArqNome: String;
  Versao: Int64;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT VerSPEDLay ',
      'FROM spedefdicmsipictrl ',
      '']);
    Res := DmkWeb.VerificaAtualizacaoVersao2(True, True, CO_WEBID_SPEDLAY,
             'Layout Sped', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value),
             Qry.FieldByName('VerSPEDLay').AsInteger, 0, DModG.ObtemAgora(), Memo,
             dtSQLs, Versao, ArqNome, False);
    if (Res) and (VAR_DOWNLOAD_OK) then
    begin
      //C:\Projetos\Delphi 2007\Aplicativos\Auxiliares\DermaBK\Restaura.pas
      Application.CreateForm(TFmRestaura2, FmRestaura2);
      FmRestaura2.Show;
      FmRestaura2.EdBackFile.Text := CO_DIR_RAIZ_DMK + '\' + ArqNome + '.SQL';
      //FmRestaura2.BeRestore.DatabaseName := DMod.MyDB.DatabaseName;
      FmRestaura2.FDB := DModG.AllID_DB;
      FmRestaura2.BtDiretoClick(Self);
      FmRestaura2.Close;
      //
      //Atualiza vers�o no banco de dados
      UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
        'UPDATE spedefdicmsipictrl  ',
        'SET VerSPEDLay=' + Geral.FF0(Versao),
        '']);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnEfdIcmsIpi_PF.ConfigDtasMinMaxAnoMesSPED(AnoMes: Integer;
  TPDT_SPED: TdmkEditDateTimePicker);
var
  DtIni, DtFim: TDateTime;
begin
  TPDT_SPED.MinDate := 0;
  TPDT_SPED.MaxDate := 0;
  if AnoMes > 0 then
  begin
    DtIni := dmkPF.DatadeAnoMes(AnoMes, 1, 0);
    DtFim := IncMonth(DtIni, 1) - 1;
    TPDT_SPED.Date := DtFim;
    TPDT_SPED.MinDate := DtIni;
    TPDT_SPED.MaxDate := DtFim;
  end;
end;

function TUnEfdIcmsIpi_PF.DataFimObrigatoria(DtHrFimOpe,
  DiaFim: TDateTime): TDateTime;
begin
  if DtHrFimOpe < 2 then
    Result :=  DiaFim
  else
  if DtHrFimOpe > DiaFim then
    Result :=  DiaFim
  else
    Result := DtHrFimOpe;
end;

function TUnEfdIcmsIpi_PF.DefineDatasPeriCorrApoSPED(const Empresa: Integer;
  const Registro: String; const DtLancto, DtCorrApo: TDateTime;
  var ThatDtIni, ThatDtFim: TDateTime): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF.DefineDatasPeriCorrApoSPED()';
var
  TipoPer, PeriAtu, PeriAnt, PerLine, DiaIni, DiaFim: Integer;
  Dia, Mes, Ano: Word;
begin
  TipoPer := ObtemTipoPeriodoRegistro(Empresa, Registro, sProcName);
  PeriAtu := ObtemPeriodoSPEDdeData(DtLancto, TTipoPeriodoFiscal(TipoPer));
  PeriAnt := ObtemPeriodoSPEDdeData(DtCorrApo, TTipoPeriodoFiscal(TipoPer));
  PerLine := PeriAtu - PeriAnt;
  if not PerLine in ([1,2]) then
    Geral.MB_Aviso('Diferen�a de tempo pass�vel de recusa!');
  case TTipoPeriodoFiscal(TipoPer) of
    spedperDecendial:
    begin
      DecodeDate(DtCorrApo, Ano, Mes, Dia);
      case Dia of
        00..10:
        begin
          ThatDtIni := EncodeDate(Ano, Mes, 1);
          ThatDtFim := EncodeDate(Ano, Mes, 10);
        end;
        11..20:
        begin
          ThatDtIni := EncodeDate(Ano, Mes, 11);
          ThatDtFim := EncodeDate(Ano, Mes, 20);
        end;
        21..31:
        begin
          ThatDtIni := EncodeDate(Ano, Mes, 21);
          ThatDtFim := IncMonth(ThatDtIni, 1);
          DecodeDate(ThatDtFim, Ano, Mes, Dia);
          ThatDtFim := EncodeDate(Ano, Mes, 1) - 1;
        end;
      end;
    end;
    spedperMensal:
    begin
      DecodeDate(DtCorrApo, Ano, Mes, Dia);
      ThatDtIni := EncodeDate(Ano, Mes, 1);
      ThatDtFim := IncMonth(ThatDtIni, 1) - 1;
    end;
  end;
end;

function TUnEfdIcmsIpi_PF.DefineDatasPeriodoSPED(const Empresa, AnoMes: Integer;
  const Registro: String; const LastDtIni, LastDtFim: TDateTime; var NewDtIni,
  NewDtFim: TDateTime): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF.DefineDatasPeriodoSPED()';
  //
  procedure OverPeriodo();
  begin
    Geral.MB_Aviso('N�o h� mais per�odos dispon�veis para o m�s selecionado!');
    NewDtIni := 0;
    NewDtFim := 0;
  end;
var
  Periodo, AnoAM, MesAM, Incremento: Integer;
  AnoPe, MesPe, DiaPe: word;
begin
  Geral.AnoMesToAnoEMes(AnoMes, AnoAM, MesAM);
  NewDtIni := Geral.AnoMesToData(AnoMes, 1);
  NewDtFim := IncMonth(NewDtIni) - 1;
  Result := False;
  //
  Periodo := ObtemTipoPeriodoRegistro(Empresa, Registro, sProcName);
{
  DModG.ReopenParamsEmp(Empresa);
  Periodo := 0;
  Incremento := 0;
  //
  if Registro = 'E100' then
    Periodo := DModG.QrParamsEmpSPED_EFD_Peri_E100.Value
  else
  if Registro = 'E500' then
    Periodo := DModG.QrParamsEmpSPED_EFD_Peri_E500.Value
  else
  if Registro = 'K100' then
    Periodo := DModG.QrParamsEmpSPED_EFD_Peri_K100.Value;
  //
  if Periodo < 1 then
  begin
    Geral.MB_Erro(
    'Tipo de per�odo indefinido em "TUnEfdIcmsIpi_PF.DefineDatasPeriodoSPED()"');
    Exit;
  end;
}
  case TTipoPeriodoFiscal(Periodo) of
    TTipoPeriodoFiscal.spedperDecendial:
    begin
      if LastDtIni = 0 then
      begin
        NewDtIni := Geral.AnoMesToData(AnoMes, 1);
        NewDtFim := NewDtIni + 9;
      end else
      begin
        DecodeDate(LastDtFim, AnoPe, MesPe, DiaPe);
        DiaPe := DiaPe + 1;
        if DiaPe > 21 then
          OverPeriodo()
        else
        begin
          NewDtIni := EncodeDate(AnoPe, MesPe, DiaPe);
          NewDtFim := NewDtIni + 9;
          if DiaPe >= 20  then
            NewDtFim :=
              Geral.AnoMesToData(Geral.IncrementaMes_AnoMes(AnoMes, 1), 1) - 1;
        end;
      end;
    end;
    TTipoPeriodoFiscal.spedperMensal:
    begin
      if LastDtIni = 0 then
      begin
        NewDtIni := Geral.AnoMesToData(AnoMes, 1);
        NewDtFim := Geral.AnoMesToData(AnoMes + 1, 1) - 1;
      end else
        OverPeriodo();
    end;
    else begin
    Geral.MB_Erro(
    'Tipo de per�odo n�o implementado em "TUnEfdIcmsIpi_PF.DefineDatasPeriodoSPED()"');
    Exit;
    end;
  end;
end;

function TUnEfdIcmsIpi_PF.DefineDebCred(ValPos, ValNeg: Double): TDebCred;
begin
  if (ValPos <> 0) and (ValNeg <> 0) then
    Result    := TDebCred.debcredAmbos
  else
  if ValPos > 0 then
    Result    := TDebCred.debcredCred
  else
  if ValNeg > 0 then
    Result    := TDebCred.debcredDeb
  else
    Result    := TDebCred.debcredND;
end;

function TUnEfdIcmsIpi_PF.DefineDebCred(Valor: Double): TDebCred;
begin
  if Valor >= 0 then
    Result    := TDebCred.debcredCred
  else
    Result    := TDebCred.debcredDeb;
end;

function TUnEfdIcmsIpi_PF.DefineQuantidadePositivaOuNegativa(const RegOri, REG:
  String; const ValAntes, ValNovo: Double; const FormaLcto:
  TCorrApoFormaLctoPosNegSPED; var QTD_COR_POS, QTD_COR_NEG: Double): Boolean;
  //
  procedure Aviso();
  begin
    Geral.MB_Aviso('Falta de implementa��o:' + sLineBreak +
    'Fun��o: ' + 'EfdIcmsIpi_PF.DefineQuantidadePositivaOuNegativa()' + sLineBreak +
    // GetEnumName(TypeInfo(TOrigemIDKnd),Integer(OrigemIDKnd)))
    'Forma de corre��o: ' + GetEnumName(TypeInfo(TCorrApoFormaLctoPosNegSPED), Integer(FormaLcto)) + sLineBreak +
    'Registro de origem: ' + RegOri + sLineBreak +
    'Registro de corre��o: ' + REG + sLineBreak +
    'Quantidade anterior: ' + Geral.FFT(ValAntes, 3, siNegativo) + sLineBreak +
    'Quantidade nova: ' + Geral.FFT(ValNovo, 3, siNegativo) + sLineBreak +
    'Quantidade corre��o: ' + Geral.FFT(ValNovo - ValAntes, 3, siNegativo) + sLineBreak +
    'QTD_COR_POS: ' + Geral.FFT(QTD_COR_POS, 3, siNegativo) + sLineBreak +
    'QTD_COR_NEG: ' + Geral.FFT(QTD_COR_NEG, 3, siNegativo) + sLineBreak +
    sLineBreak);
  end;
var
  ValCorApo: Double;
begin
  Result      := False;
  QTD_COR_NEG := 0;
  QTD_COR_POS := 0;
  if (ValNovo = 0) and (ValAntes = 0) then
  begin
    Result := True;
    Exit;
  end;
  ValCorApo   := ValNovo - ValAntes;
  case FormaLcto of
    //caflpnIndef=0,
    caflpnMovNormEsquecido:
    begin
(*
Boa tarde,

  Esqueci de lan�ar no SPED EFD do m�s anterior uma reclassifica��o de
  produto de classe A para B. No SPED atual vou lan�a-lo no K270/K275.
  Devo lan�ar as quantidades no QTD_COR_POS ou no QTD_COR_NEG?
  Devo lan�ar no K280 tamb�m?
  // Resposta
Prezado(a) Contribuinte,

se o erro apontado seria objeto de informa��o no registro K220 no per�odo
da ocorr�ncia, ent�o a corre��o ser� efetuada nos registros K270/K275,
conforme est� descrito no Guia Pr�tico. Se o produto A est� quantificado em
n�mero maior que o correto, lan�ar quantidade negativa. Por consequ�ncia, o
"B" est� menor, lan�ar quantidade positiva.

O K280 dever� ser corrigido tamb�m, considerando que o estoque final de um
per�odo � o estoque inicial do per�odo subsequente, e, portanto,
influenciar� no estoque final escriturado dos per�odos subsequentes.
*)
      if RegOri = 'K210' then
      begin
        if REG = 'K270' then QTD_COR_NEG := ValCorApo else
        //if REG = 'K275' then QTD_COR_POS := ? else
        Aviso();
      end
      else
      if RegOri = 'K215' then
      begin
        //if REG = 'K270' then QTD_COR_NEG := ? else
        if REG = 'K275' then QTD_COR_POS := ValCorApo else
        Aviso();
      end
      else
      if RegOri = 'K220' then
      begin
        if REG = 'K270' then QTD_COR_NEG := ValCorApo else
        if REG = 'K275' then QTD_COR_POS := ValCorApo else
        Aviso();
      end
      else
      if (RegOri = 'K230')
      or (RegOri = 'K250') then
      begin
        if REG = 'K270' then QTD_COR_POS := ValCorApo else
        //if REG = 'K275' then QTD_COR_NEG := ValCorApo else
        Aviso();
      end
      else
      if (RegOri = 'K235')
      or (RegOri = 'K255') then
      begin
        if REG = 'K275' then QTD_COR_NEG := ValCorApo else
        //if REG = 'K275' then QTD_COR_NEG := ValCorApo else
        Aviso();
      end
      else
      if (RegOri = 'K260') then
      begin
        if (REG = 'K270') then
        begin
          if ValCorApo < 0 then
            QTD_COR_NEG := - ValCorApo
          else
            QTD_COR_POS := ValCorApo;
        end else
        //if REG = 'K275' then QTD_COR_NEG := ValCorApo else
        Aviso();
      end
      else
      if (RegOri = 'K291')
      or (RegOri = 'K301') then
      begin
        if (REG = 'K270') then QTD_COR_POS := ValCorApo
        else
        Aviso();
      end
      else
      if (RegOri = 'K292')
      or (RegOri = 'K302') then
      begin
        if (REG = 'K270') then QTD_COR_NEG := ValCorApo
        else
        Aviso();
      end
      else Aviso();
    end;
    //caflpnDesfazimentoMov:
    else Aviso();
  end;
  if (QTD_COR_NEG < 0) or (QTD_COR_POS < 0) then Aviso() else
  if (QTD_COR_NEG = 0) and (QTD_COR_POS = 0) then Aviso() else
  Result := True;
end;

function TUnEfdIcmsIpi_PF.DefineTabelaInsProdOuSubPrd(
  TabProdOuSubPrd: TTabProdOuSubPrd; TabProduto, TabSubProd, ProcName: String):
  String;
const
  sProcName = 'TUnEfdIcmsIpi_PF.DefineTabelaInsProdOuSubPrd()';
begin
  case TabProdOuSubPrd of
    TTabProdOuSubPrd.tpspProducao: Result := TabProduto;
    TTabProdOuSubPrd.tpspSubProd : Result := TabSubProd;
    else
    begin
      Result := 'efdicmsipik?????????';
      //
      Geral.MB_Info('TabProdOuSubPrd: ' + GetEnumName(TypeInfo(TTabProdOuSubPrd),
      Integer(TabProdOuSubPrd)) + ' n�o definido em: ' +
      sProcName + sLineBreak + ProcName);
    end;
  end;
end;

function TUnEfdIcmsIpi_PF.DefineTabProdOuSubPrd(
  GraGruX: Integer): TTabProdOuSubPrd;
const
  sProcName = 'DefineTabProdOuSubPrd()';
var
  IsOk: Boolean;
  Tipo_Item: Integer;
begin
  Result := TTabProdOuSubPrd.tpspProducao;
  if DmodG.QrParamsEmpSPED_EFD_MovSubPrd.Value = 0 then
  begin
    // se n�o informa Subproduto no SPED...
    Tipo_Item := DModG.MyCompressDB.SelectInteger(
    //aSQL : string; var IsOk : boolean; aFieldName : string):integer;
    ' SELECT ' +
    ' IF(gg1.Nivel2 <> 0 AND gg2.Tipo_Item>-1, ' +
    '   gg2.Tipo_Item, pgt.Tipo_Item) Tipo_Item  ' +
    ' FROM gragrux    ggx ' +
    ' LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1' +
    ' LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel1' +
    ' LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip' +
    ' WHERE ggx.Controle=' + Geral.FF0(GraGruX),
    IsOK, 'Tipo_Item');
    if IsOk = False then
    begin
      Tipo_Item := -999999999;
      Geral.MB_Erro('Tipo_Item nao encontrado para o Reduzido ' +
      Geral.FF0(GraGruX) + ' em ' + sProcName);
    end;
    //
    //se for subproduto ent�o:
    if (Tipo_Item = CO_SPED_TIPO_ITEM_05_INT(*Subproduto*)) then
      Result := TTabProdOuSubPrd.tpspSubProd;
  end;
end;

function TUnEfdIcmsIpi_PF.EncerraMesSPED(ImporExpor, AnoMes, Empresa: Integer;
  Campo: String): Boolean;
const
  Valor = 1;
var
  Qry: TmySQLQuery;
  Nome: String;
  MovimXX, GerLibEFD, EnvioEFD: Integer;
  SQLType: TSQLType;
begin
  if VAR_USUARIO <> -1 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
  end;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM spedefdicmsipience ',
    'WHERE ImporExpor=' + Geral.FF0(AnoMes),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
    if Qry.RecordCount > 0 then
      SQLType      := stUpd
    else
      SQLType      := stIns;
    //AnoMes         := ;
    //Empresa        := ;
    //Nome           := ;
    //MovimXX        := ;
    //GerLibEFD      := ;
    //EnvioEFD       := ;

    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'spedefdicmsipience', False, [
    Campo], [
    'ImporExpor', 'AnoMes', 'Empresa'], [
    Valor], [
    ImporExpor, AnoMes, Empresa], True);
  finally
    Qry.Free;
  end;
end;

function TUnEfdIcmsIpi_PF.ErrGrandeza(TabSrc, GGxSrc, GGXDst: String): Boolean;
  function ParteSQL(GGXStr: String): String;
  begin
    Result := Geral.ATS([
    'SELECT ggx.GraGru1 Nivel1, ggx.Controle Reduzido, ',
    'CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_GG1, med.Sigla, ',
    'CAST(med.Grandeza AS DECIMAL(11,0)) Grandeza, ',
    'CAST(xco.Grandeza AS DECIMAL(11,0)) xco_Grandeza,  ',
    'CAST(IF(xco.Grandeza > 0, xco.Grandeza,  ',
    //
    '  IF(ggx.GraGruY=1024 AND med.Grandeza = 0, 0, ',
    '  IF((ggx.GraGruY<2048 OR  ',
    '  xco.CouNiv2<>1), 2,  ',
    '  IF(xco.CouNiv2=1, 1, -1)))) AS DECIMAL(11,0))  dif_Grandeza ',
(*
    '  IF((ggx.GraGruY<2048 OR  ',
    '  xco.CouNiv2<>1), 2,  ',
    '  IF(xco.CouNiv2=1, 1, -1))) AS DECIMAL(11,0))  dif_Grandeza ',
*)
    'FROM ' + TabSrc + ' ses ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=ses.' + GGXStr,
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    nv1 ON nv1.Codigo=xco.CouNiv1  ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    nv2 ON nv2.Codigo=xco.CouNiv2  ',
(*
    'WHERE (IF(xco.Grandeza>0, xco.Grandeza, ',
    '  IF(ggx.GraGruY=1024 AND med.Grandeza = 0, 0, ',
    '  IF((ggx.GraGruY<2048 OR xco.CouNiv2<>1), 2, ',
    '    IF(xco.CouNiv2=1, 1, -1)))) <> med.Grandeza) ',
*)
  'WHERE (IF(xco.Grandeza>0, xco.Grandeza,  ',
  '  IF(ggx.GraGruY=1024 AND med.Grandeza = 0, 0,  ',
  '  IF((ggx.GraGruY<2048 OR xco.CouNiv2<>1), 2,  ',
  '  IF((ggx.GraGruY=2048 AND ses.AreaM2=0 AND ses.PesoKg<>0), med.Grandeza,  ',
  '    IF(xco.CouNiv2=1, 1, -1))))) <> med.Grandeza)  ',
    //
(*
    '  IF((ggx.GraGruY<2048 OR xco.CouNiv2<>1), 2, ',
    '    IF(xco.CouNiv2=1, 1, -1))) <> med.Grandeza) ',
*)
    '']);
  end;
var
  SQLSrc, SQLDst, SQLUni: String;
begin
  SQLSrc := '';
  SQLDst := '';
  SQLUni := '';
  if GGxSrc <> '' then
    SQLSrc := ParteSQL(GGXSrc);
  if (GGxSrc <> '') and (GGXDst <> '') then
    SQLUni := 'UNION ';
  if GGxDst <> '' then
    SQLDst := ParteSQL(GGXDst);
  Result := DfModAppGraG1.GrandesasInconsistentes([
  ' DROP TABLE IF EXISTS _couros_err_grandeza_; ',
  'CREATE TABLE _couros_err_grandeza_ ',
  ' ',
  SQLSrc,
  SQLUni,
  SQLDst,
  '; ',
  'SELECT * FROM _couros_err_grandeza_ ',
  ''], DModG.MyPID_DB);
end;

function TUnEfdIcmsIpi_PF.HabilitaDocOP(KndTab: Integer): Boolean;
const
  sProcName = 'UnEfdIcmsIpi_PF.HabilitaDocOP()';
var
  EstqSPEDTabSorc: TEstqSPEDTabSorc;
begin
  Result := False;
  EstqSPEDTabSorc := TEstqSPEDTabSorc(KndTab);
  case EstqSPEDTabSorc of
    (*0*)estsND,
    (*2*)estsPQx,
    (*3*)estsNoSrc: Result := False;
    (*1*)estsVMI: Result := True;
    else Geral.MB_Erro('"EstqSPEDTabSorc" indefinido em ' + sProcName)
  end;
end;

procedure TUnEfdIcmsIpi_PF.HabilitaMovimCodPorKndTab(Form: TForm;
  KndTab: Integer; Objetos: array of TObject);
var
  EstqSPEDTabSorc: TEstqSPEDTabSorc;
  I: Integer;
  Habilita: Boolean;
begin
  Habilita := HabilitaDocOP(KndTab);
  //
  for I := Low(Objetos) to High(Objetos) do
    MyVCLRef.SET_Object(Form, Objetos[I], 'Enabled', Habilita);
end;

function TUnEfdIcmsIpi_PF.LiberaAcaoVS_SPED(ImporExpor, AnoMes, Empresa: Integer;
  EstagioVS_SPED: TEstagioVS_SPED): Boolean;
var
  FldPre, FldPos, AskPre, AskPos: String;
  Continua: Boolean;
  AMPre, AMPos: Integer;
begin
  Result := False;
  AMPre := 0;
  AMPos := 0;
  case EstagioVS_SPED of
    TEstagioVS_SPED.evsspedEncerraVS:
    begin
      FldPre := 'MovimXX';
      FldPos := 'GerLibEFD';
      AskPre :=
      'O movimento de couros ainda n�o foi encerrado para o per�odo selecionado.'
      + sLineBreak +
      'Ap�s o encerramento os novos lan�amentos feitos com data anterior ao per�odo ser�o lan�ados em SPED posterior como ajuste!'
      + sLineBreak + 'Deseja encerrar o per�odo?';
    end;
    TEstagioVS_SPED.evsspedGeraSPED:
    begin
      FldPre := 'GerLibEFD';
      FldPos := 'EnvioEFD';
      AskPre :=
      'A gera��o do SPED ainda n�o foi liberada para o per�odo selecionado.'
      + sLineBreak + 'Deseja liberar?';
    end;
    TEstagioVS_SPED.evsspedExportaSPED:
    begin
      FldPre := 'EnvioEFD';
      FldPos := 'EncerraEFD';
      AskPre :=
      'A exporta��o do SPED ainda n�o foi liberada para o per�odo selecionado.'
      + sLineBreak + 'Deseja liberar?';
    end;
    //TEstagioVS_SPED.evsspedNone:
    else
    begin
      FldPre := '';
      FldPos := '';
      AskPre := '';
      AskPos := '';
    end;
  end;
  //
  AMPre := SPEDEFDEnce_AnoMes(FldPre);
  AMPos := SPEDEFDEnce_AnoMes(FldPos);
  //
  if AMPre < AnoMes then
  begin
    if Geral.MB_Pergunta(AskPre) = ID_Yes then
    begin
      Continua := EfdIcmsIpi_PF.EncerraMesSPED(ImporExpor, AnoMes, Empresa, FldPre);
      if Continua then
      begin
        // reconfirmar que criou!
        AMPre := SPEDEFDEnce_AnoMes(FldPre);
        Continua := AMPre >= AnoMes;
      end;
    end else
      Continua := False;
  end;
  if Continua then
  begin
    Continua := AMPos < AnoMes;
(*
    if not Continua then
    begin
      if Geral.MB_Pergunta(AskPos) = ID_Yes then
      begin
        Continua := EfdIcmsIpi_PF.EncerraMesSPED(AnoMes, Empresa, FldPos);
        if Continua then
        begin
          // reconfirmar que criou!
          AMPos := SPEDEFDEnce_AnoMes(FldPos);
          Continua := AMPos >= AnoMes;
        end;
      end;
    end;
*)
  end;
  // Ini 2019-01-04
  if not Continua then
  begin
    case EstagioVS_SPED of
      TEstagioVS_SPED.evsspedEncerraVS: ;
      TEstagioVS_SPED.evsspedGeraSPED:  ;
      TEstagioVS_SPED.evsspedExportaSPED: Continua := AMPre = AnoMes;
      else ;
    end;
  end;
  // Fim 2019-01-04
  Result := Continua;
end;

function TUnEfdIcmsIpi_PF.ObtemTipoDeProducaoSPED(EntiEmp: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SPED_EFD_Producao ',
      'FROM paramsemp ',
      'WHERE Codigo=' + Geral.FF0(EntiEmp),
      '']);
      //
    Result := Qry.FieldByName('SPED_EFD_Producao').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnEfdIcmsIpi_PF.ObtemTipoPeriodoRegistro(Empresa: Integer;
  Registro, ProcOri: String): Integer;
const
  sProcName = 'TUnEfdIcmsIpi_PF.ObtemTipoPeriodoRegistro()';
begin
  DModG.ReopenParamsEmp(Empresa);
  Result := 0;
  //
  if Registro = 'E100' then
    Result := DModG.QrParamsEmpSPED_EFD_Peri_E100.Value
  else
  if Registro = 'E500' then
    Result := DModG.QrParamsEmpSPED_EFD_Peri_E500.Value
  else
  if Registro = 'K100' then
    Result := DModG.QrParamsEmpSPED_EFD_Peri_K100.Value
  else
  begin
    Geral.MB_Erro(
    'Tipo de per�odo indefinido em "' + sProcName + '"' +
    sLineBreak +
    'Origem: "' + ProcOri + '"');
    Exit;
  end;
  //
  if Result < 1 then
  begin
    Geral.MB_Erro(
    'Tipo de per�odo n�o definido na filial para:' + sLineBreak +
    'Empresa: ' + Geral.FF0(Empresa) + sLineBreak +
    'Registro: ' + Registro + sLineBreak +
    'procedimento: "' + sProcName + '"' + sLineBreak +
    'Origem: "' + ProcOri + '"' + sLineBreak +
    'Configure em Op��es >> Filiais na guia SPED EFD ');
    Exit;
  end;
end;

function TUnEfdIcmsIpi_PF.ObtemDatasDePeriodoSPED(const Ano, Mes, Periodo: Integer;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; var DataIni, DataFim:
  TDateTime): Boolean;
begin
  Result := False;
  case TipoPeriodoFiscal of
    //spedperIndef=0,
    (*1*)spedperDiario:
    begin
      DataIni := EncodeDate(Ano, Mes, Periodo);
      DataFim := DataIni;
      Result  := True;
    end;
    {
    (*2*)spedperSemanal:
    begin
      Errado? semana pode comecar de 1 a 7!
      DataIni :=       DataIni := EncodeDate(Ano, Mes, Periodo);
      DataFim := DataIni;

      DataFim := DataIni;
      Result  := EncodeDate(Ano, Mes, (Periodo * 7) - 6);
    end;
    }
    (*3*)spedperDecendial:
    begin
      DataIni := EncodeDate(Ano, Mes, (Periodo * 10) - 9);
      if Periodo = 3 then
        DataFim := IncMonth(EncodeDate(Ano, Mes, 1), 1) - 1
      else
        DataFim := DataIni + 9;
      Result  := True;
    end;
    {
    (*4*)spedperQuinzenal:
    begin
      Meses := Geral.Periodo2000(Data);
      DecodeDate(Data, Ano, Mes, Dia);
      Periodos := (Dia div 15) + 1;
      if Periodos > 2 then
        Periodos := 2;
      Result := (Meses - 1) * 2;
      Result := Result + Periodos;
    end;
    }
    (*5*)spedperMensal:
    begin
      DataIni := EncodeDate(Ano, Mes, 1);
      DataFim := IncMonth(EncodeDate(Ano, Mes, 1), 1) - 1;
      Result  := True;
    end;
    else Geral.MB_Erro('Tipo de periodo n�o imlementado!');
  end;
end;

function TUnEfdIcmsIpi_PF.ObtemIND_ESTeCOD_PART(const Empresa, ClientMO, EntiSitio,
  GraGruX: Integer; var IND_EST, COD_PART: String; var Entidade: Integer): String;
begin
  Result   := '';
  IND_EST  := '';
  COD_PART := '';
  Entidade := 0;
  // o produto pertence a empresa declarante
  if ((ClientMO  = Empresa))
  or ((ClientMO = 0) and (EntiSitio = Empresa))
  then
  begin
    if EntiSitio = Empresa then
    //Da empresa em seu poder
    begin
      IND_EST  := Geral.FF0(Integer(spedipiePropInfPoderInf));//=0,
      COD_PART := '';
      Entidade := Empresa;
    end else
    //Da empresa com terceiros
    begin
      IND_EST  := Geral.FF0(Integer(spedipiePropInfPoderTer)); //=1,
      COD_PART := Geral.FF0(EntiSitio);
      Entidade := EntiSitio;
      if EntiSitio = 0 then
        Result := 'COD_PART: ' + Geral.FF0(EntiSitio) +
        ' Da empresa com terceiro indefinido! Empresa: ' + Geral.FF0(Empresa) +
        ' cliente de servi�o: ' + Geral.FF0(ClientMO);
    end;
  end else
  // o produto N�O pertence a empresa declarante
  begin
    if EntiSitio = Empresa then
    //De terceiros c/ a empresa
    begin
      if ClientMO <> 0 then
      begin
        IND_EST := Geral.FF0(Integer(spedipiePropTerPoderInf)); //=2,
        COD_PART := Geral.FF0(ClientMO);
        Entidade := ClientMO;
        if (CLientMO = 0) then
          Result := 'COD_PART: ' + Geral.FF0(CLientMO) +
          ' De terceiro indefinido! Empresa: ' + Geral.FF0(Empresa) +
          ' Empresa onde est� o estoque: ' + Geral.FF0(EntiSitio);
      end else
      begin
        // Errado! n�o pode chegar aqui!
        IND_EST := Geral.FF0(Integer(spedipiePropTerPoderInf)); //=2,
        COD_PART := Geral.FF0(EntiSitio);
        Entidade := EntiSitio;
        if (EntiSitio = 0) then
          Result := 'COD_PART: ' + Geral.FF0(EntiSitio) +
          ' De terceiro indefinido! Empresa: ' + Geral.FF0(Empresa) +
          ' cliente de servi�o: ' + Geral.FF0(ClientMO);
      end;
    end else
    if (ClientMO <> Empresa) and (EntiSitio = 0) then
    //De terceiros c/ a empresa
    begin
      IND_EST := Geral.FF0(Integer(spedipiePropTerPoderInf)); //=2,
      COD_PART := Geral.FF0(ClientMO);
      Entidade := ClientMO;
      if (ClientMO = 0) then
        Result := 'COD_PART: ' + Geral.FF0(ClientMO) +
        ' De cliente de servi�o indefinido! Empresa: ' + Geral.FF0(Empresa) +
        ' cliente de servi�o: ' + Geral.FF0(ClientMO);
    end else
    // De terceiros com terceiros
    begin
      IND_EST  := '-';
      COD_PART := '0';
      Entidade := EntiSitio;
      //
      //MeAviso.Lines.Add('COD_ITEM: ' + COD_ITEM +
      Result := 'COD_ITEM: ' + Geral.FF0(GraGruX) +
      ' De terceiros com terceiros! COD_PART: ' + Geral.FF0(EntiSitio);
    end;
  end;
end;

function TUnEfdIcmsIpi_PF.ObtemPeriodoSPEDdeData(Data: TDateTime;
  TipoPeriodoFiscal: TTipoPeriodoFiscal): Integer;
var
  Meses, Periodo, Dias, Semanas, Periodos: Integer;
  Dia, Mes, Ano: Word;
begin
  Result := 0;
{///////////////////////////////////////////////////////////////////////////////
Per�odo
                http://www31.receita.fazenda.gov.br/SicalcWeb/calcpfPer_odo.html
                extraido em 13/05/2017
Para as receitas que a aplica��o poder� efetuar o c�lculo de acr�scimos legais
  (multa de mora e juros), essa informa��o ser� obrigat�ria. Para as demais, a
  aplica��o solicitar�, em seu lugar, o "Per�odo de Apura��o".
Para tipo de per�odo igual a "Exerc�cio", dever� ser informado o ano
  correspondente, com quatro algarismos. N�o dever� ser informado o ano-base e
  sim o exerc�cio da declara��o.
Para tipo de per�odo igual a "Trimestral", dever� ser informado o trimestre
  correspondente, no formato TAAAA, onde T � igual ao n�mero do trimestre
  ( 1,2,3 ou 4) e AAAA � igual ao ano com quatro algarismos.
Para tipo de per�odo igual a "Mensal", dever� ser informado o m�s
  correspondente, no formato MMAAAA, onde MM � igual ao n�mero do m�s com dois
  algarismos(01 a 12) e AAAA � igual ao ano com quatro algarismos.
Para tipo de per�odo igual a "Quinzenal", dever� ser informada a quinzena
  correspondente, no formato QMMAAAA, onde Q � igual ao n�mero da quinzena
  (1 ou 2), MM � igual ao n�mero do m�s com dois algarismos (01 a 12) e AAAA �
  igual ao ano com quatro algarismos.
Para tipo de per�odo igual a "Decendial", dever� ser informado o dec�ndio
  correspondente, no formato DMMAAAA, onde D � igual ao n�mero do dec�ndio
  (1, 2 ou 3), MM � igual ao n�mero do m�s com dois algarismos (01 a 12) e AAAA
  � igual ao ano com quatro algarismos.
Para tipo de per�odo igual a "Semanal", dever� ser informada a semana
  correspondente, no formato SMMAAAA, onde S � igual ao n�mero da semana
  (1 a 5), MM � igual ao n�mero do m�s com dois algarismos (01 a 12) e AAAA �
  igual ao ano com quatro algarismos.
  OBS: Para os tributos de periodicidade semanal, que a pr�pria p�gina apresenta
    os valores dos acr�scimos legais, quando devidos, e do respectivo
    vencimento, a semana se inicia no domingo e termina no s�bado. Portanto, a
    primeira semana de um determinado m�s � aquela onde aparece o primeiro
    s�bado, mesmo que seja o primeiro dia do m�s.
Para tipo de per�odo igual a "Di�rio", informe o dia correspondente, no formato
  DDMMAAAA, onde DD � igual ao dia do m�s (01 a 31), MM � igual ao n�mero do m�s
  com dois algarismos (01 a 12) e AAAA � igual ao ano com quatro algarismos.e
///////////////////////////////////////////////////////////////////////////////}
  case TipoPeriodoFiscal of
    //spedperIndef=0,
    (*1*)spedperDiario   : Result := Trunc(Data) - Trunc(EncodeDate(1999, 12, 31));
    (*2*)spedperSemanal  :
    begin
      Dias := Trunc(Data) - Trunc(EncodeDate(2000, 1, 1));
      Result := (Dias div 7) + 1;
    end;
    (*3*)spedperDecendial:
    begin
      Meses := Geral.Periodo2000(Data);
      DecodeDate(Data, Ano, Mes, Dia);
      Semanas := ((Dia-1) div 10) + 1;
      if Semanas > 3 then
        Semanas := 3;
      Result := (Meses - 1) * 3;
      Result := Result + Semanas;
    end;
    (*4*)spedperQuinzenal:
    begin
      Meses := Geral.Periodo2000(Data);
      DecodeDate(Data, Ano, Mes, Dia);
      Periodos := (Dia div 15) + 1;
      if Periodos > 2 then
        Periodos := 2;
      Result := (Meses - 1) * 2;
      Result := Result + Periodos;
    end;
    (*5*)spedperMensal: Result := Geral.Periodo2000(Data);
    else Geral.MB_Erro('Tipo de periodo n�o imlementado!');
  end;
end;

function TUnEfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(const REG: String; var
  Origem: TSPED_EFD_KndRegOrigem): Boolean;
const
  sprocName = 'ObtemSPED_EFD_KndRegOrigem()';
begin
  Result := False;
  //if (REG = 'K200') (***************) then Origem := sek2708oriK200  ->> N�o existe!
  if (REG = 'K230') or (REG = 'K235') then Origem := sek2708oriK230K235 else
  if (REG = 'K250') or (REG = 'K255') then Origem := sek2708oriK250K255 else
  if (REG = 'K210') or (REG = 'K215') then Origem := sek2708oriK210K215 else
  if (REG = 'K260') or (REG = 'K265') then Origem := sek2708oriK260K265 else
  if (REG = 'K260') or (REG = 'K265') then Origem := sek2708oriK260K265 else
  if (REG = 'K220') (***************) then Origem := sek2708oriK220 else
  //if (REG = 'K290') (***************) then Origem := sek2708oriK290  ->> N�o existe!
  if (REG = 'K291') (***************) then Origem := sek2708oriK291 else
  if (REG = 'K292') (***************) then Origem := sek2708oriK292 else
  //if (REG = 'K300') (***************) then Origem := sek2708oriK300  ->> N�o existe!
  if (REG = 'K301') (***************) then Origem := sek2708oriK301 else
  if (REG = 'K302') (***************) then Origem := sek2708oriK302 else
  Origem := sek2708oriIndef;
  //
  Result := Integer(Origem) > 0;
  if not Result then
    Geral.MB_ERRO('"REG" ' + REG + ' n�o implementado em ' + sProcName);
end;

function TUnEfdIcmsIpi_PF.ObtemSPED_EFD_REGdeKndRegOrigem(
  const Origem: TSPED_EFD_KndRegOrigem; var REG: String): Boolean;
const
  sprocName = 'ObtemSPED_EFD_REGdeKndRegOrigem()';
begin
  Result := False;
  REG := '';
  case Origem of
    sek2708oriIndef     : REG := '';
    sek2708oriK230K235  : REG := 'K230';
    sek2708oriK250K255  : REG := 'K250';
    sek2708oriK210K215  : REG := 'K210';
    sek2708oriK260K265  : REG := 'K260';
    sek2708oriK220      : REG := 'K220';
    sek2708oriK291      : REG := 'K291';
    sek2708oriK292      : REG := 'K292';
    sek2708oriK301      : REG := 'K301';
    sek2708oriK302      : REG := 'K302';
    else
      Geral.MB_ERRO('"SPED_EFD_KndRegOrigem" ' + Geral.FF0(Integer(Origem)) +
        ' n�o implementado em ' + sProcName);
  end;
  //
  Result := REG <> '';
  if not Result then
    Geral.MB_ERRO('"REG" indefinido em ' + sProcName);
end;

procedure TUnEfdIcmsIpi_PF.ReopenTbSpedEfdXXX(Query: TmySQLQuery; DtIni,
  DtFim: TDateTime; TabName, FldOrd: String);
var
  Ini, Fim: String;
begin
  Ini := Geral.FDT(DtIni, 1);
  Fim := Geral.FDT(DtFim, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.AllID_DB, [
  'SELECT * ',
  'FROM ' + TabName,
  'WHERE DataIni <= "' + Ini + '" ',
  'AND (  ',
  '  DataFim >= "' + Fim + '" ',
  '  OR ',
  '  DataFim < "1900-01-01" ',
  ') ',
  'ORDER BY ' + FldOrd,
  '']);
end;

function TUnEfdIcmsIpi_PF.SPEDEFDEnce_AnoMes(Campo: String): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(AnoMes) AnoMes ',
    'FROM spedefdicmsipience ',
    'WHERE ' + Campo + '=1 ', // 1=Feito
    '']);
    Result := Qry.FieldByName('AnoMes').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnEfdIcmsIpi_PF.SPEDEFDEnce_DataMinMovim(Campo: String): TDateTime;
var
  AnoA, MesA, DiaA: Word;
begin
(*
  AnoMes := SPEDEFDEnce_AnoMes(Campo);
  //
  if AnoMes <> 0 then
    Result := Geral.AnoMesToData(AnoMes, 1);
*)
  if (not DModG.SoUmaEmpresaLogada(True))
  or (MyObjects.FIC(VAR_LIB_EMPRESA_SEL = 0, nil,
  'Empresa logada n�o definida!')) then
  begin
    DecodeDate(DModG.ObtemAgora(), AnoA, MesA, DiaA);
    Result := EncodeDate(AnoA, 1, 1);
    //
    Exit;
  end;
  //
  Result := SPEDEFDEnce_DataUltimoInventario(VAR_LIB_EMPRESA_SEL) + 1;
end;

function TUnEfdIcmsIpi_PF.SPEDEFDEnce_DataUltimoInventario(Empresa: Integer):
  TDateTime;
var
  Agora: TDateTime;
  Qry: TmySQLQuery;
  AnoA, MesA, DiaA, AnoI, MesI, DiaI: Word;
  DataX: TDateTime;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Agora := DModG.ObtemAgora();
    DecodeDate(Agora, AnoA, MesA, DiaA);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(DT_INV) UltDT_INV',
    'FROM efdicmsipih005',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    '']);
    DataX := Qry.FieldByName('UltDT_INV').AsDateTime;
    DecodeDate(DataX, AnoI, MesI, DiaI);
    if AnoI < (AnoA - 1) then
    begin
      DataX := EncodeDate(AnoA - 1, 12, 31);
      if Int(Agora) - Int(DataX) < 60 then
        DataX := IncMonth(DataX, -12);
    end;
    //
    Result := DataX;
  finally
    Qry.Free;
  end;
end;

function TUnEfdIcmsIpi_PF.SPEDEFDEnce_Periodo(Campo: String): Integer;
var
  AnoMes: Integer;
begin
  AnoMes := SPEDEFDEnce_AnoMes(Campo);
  //
  if AnoMes <> 0 then
    Result := Geral.AnoMesToPeriodo(AnoMes)
  else
    Result := 0;
end;

function TUnEfdIcmsIpi_PF.SQLPeriodoFiscal(TipoPeriodoFiscal: TTipoPeriodoFiscal;
  CampoPsq, CampoRes: String; VirgulaFinal: Boolean): String;
var
  Virgula: String;
begin
  if VirgulaFinal then
    Virgula := ', '
  else
    Virgula := ' ';
  //
  case TipoPeriodoFiscal of
    //spedperIndef=0, spedperDiario=1, spedperSemanal=2,
    spedperDecendial: Result := Geral.ATS([
      'CASE ',
      '  WHEN ' + CampoPsq + ' = 0 THEN 0 ',
      '  WHEN DAYOFMONTH(' + CampoPsq + ') < 11 THEN 1 ',
      '  WHEN DAYOFMONTH(' + CampoPsq + ') < 21 THEN 2 ',
      '  ELSE 3 END ' + CampoRes]);
    //spedperQuinzenal=4,
    spedperMensal: Result := Geral.ATS([
      'CASE ',
      '  WHEN ' + CampoPsq + ' = 0 THEN 0 ',
      '  ELSE 1 END ' + CampoRes]);
    else Result := Geral.ATS([
      'CASE ',
      '  WHEN ' + CampoPsq + ' = 0 THEN #ERR ',
      '  ELSE #ERR END ' + CampoRes]);
  end;
  Result := Result + Virgula;
end;

function TUnEfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX: Integer; REG,
  sProcName: String): String;
begin
  Result := Geral.FF0(GraGruX);
  if GraGruX = 0 then
    Geral.MB_Erro('Reduzido = 0 (inv�lido) no REGISTRO "' + REG + '"' +
    sLineBreak + sProcName);
end;

function TUnEfdIcmsIpi_PF.ValidaCOD_INS_SUBST_DentroDoAnoMesSPED(AnoMes,
  GraGruX: Integer; COD_INS_SUBST, COD_DOC_OS: String): Boolean;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K235]*)
  (*Campo 05 (COD_INS_SUBST) � Preenchimento: informar o c�digo do item
  componente/insumo que estava previsto para ser consumido no Registro 0210 e
  que foi substitu�do pelo COD_ITEM deste registro.
  Valida��o: o c�digo do insumo substitu�do deve existir no Registro 0210 para o
  mesmo produto resultante � K230/0200.
  O tipo do componente/insumo (campo TIPO_ITEM do Registro 0200) deve ser igual
  a 00, 01, 02, 03, 04, 05 ou 10.*)

  //  Implementar???
end;

function TUnEfdIcmsIpi_PF.ValidaDataDentroDoAnoMesSPED(AnoMes: Integer;
  Data: TDateTime; OrdemDeServico: String; Obrigatorio: Boolean): Boolean;
var
  DtAMIni, DtAMFim: TDateTime;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K220]*)
  (*Campo 02 (DT_MOV) -: Valida��o: a data deve estar compreendida no per�odo
  informado nos campos DT_INI e DT_FIN do Registro K100.*)
  if Obrigatorio and (Data < 2) then
  begin
    Geral.MB_Aviso('Informe a data!');
    Result := False;
    Exit;
  end;
  if Data > 2 then
  begin
    dmkPF.DatasIniFimDoPeriodo(AnoMes, DtAMIni, DtAMFim);
    if (Data < DtAMIni) or (Data > DtAMFim) then
    begin
      Geral.MB_Aviso('Data deve ser dentro do per�odo!');
      Result := False;
      Exit;
    end;
  end;
end;

function TUnEfdIcmsIpi_PF.ValidaDataFinalDentroDoAnoMesSPED(AnoMes: Integer;
  DataIni, DataFim: TDateTime; OrdemDeServico: String): Boolean;
var
  DtAMIni, DtAMFim: TDateTime;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K210]*)
  //
  (*Campo 03 (DT_FIN_OS) - Preenchimento: informar a data de conclus�o da ordem
  de servi�o. Ficar� em branco, caso a ordem de servi�o n�o seja conclu�da at� a
  data de encerramento do per�odo de apura��o.
  Valida��o: se preenchido, DT_FIN_OS deve estar compreendida no per�odo de
  apura��o do K100 e ser maior ou igual a DT_INI_OS.*)
  if DataFim > 2 then
  begin
    dmkPF.DatasIniFimDoPeriodo(AnoMes, DtAMIni, DtAMFim);
    if (DataFim < DtAMIni) or (DataFim > DtAMFim) then
    begin
      Geral.MB_Aviso('Data final quando informada deve ser dentro do per�odo!');
      Result := False;
      Exit;
    end else
    if DataIni > DataFim then
    begin
      Geral.MB_Aviso('Data final deve ser maior ou igual a data inicial!');
      Result := False;
      Exit;
    end;
  end;
end;

function TUnEfdIcmsIpi_PF.ValidaDataInicialDentroDoAnoMesSPED(AnoMes: Integer;
  DataIni, DataFim: TDateTime; OrdemDeServico: String): Boolean;
var
  DtAMIni, DtAMFim: TDateTime;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K210]*)
  //
  (*Campo 02 (DT_INI_OS) - Preenchimento: a data de in�cio dever� ser informada
  se existir ordem de servi�o, ainda que iniciada em per�odo de apura��o (K100)
  anterior.
  Valida��o: obrigat�rio se informado o campo COD_DOC_OS ou o campo DT_FIN_OS.
  A data informada deve ser menor ou igual a DT_FIN do registro K100.*)
  dmkPF.DatasIniFimDoPeriodo(AnoMes, DtAMIni, DtAMFim);
  //if (Trim(OrdemDeServico) <> '') or (DataFim >= 2) then
  if (Trim(OrdemDeServico) <> '') and (DataFim >= 2) then
  begin
    if DataIni < 2 then
    begin
      Geral.MB_Aviso('Data inicial deve ser informada!');
      Result := False;
      Exit;
    end;
  end;
  if (DataIni > DataFim) and (DataFim >= 2) then
  begin
    Geral.MB_Aviso('Data inicial deve ser menor ou igual a data final!');
    Result := False;
    Exit;
  end;
  if (DataIni > DtAMFim) then
  begin
    Geral.MB_Aviso('Data inicial deve ser menor ou igual a data final do per�odo!');
    Result := False;
    Exit;
  end;
end;

function TUnEfdIcmsIpi_PF.ValidaDataItemDentroDoAnoMesSPED(AnoMes: Integer;
  Data, DtIniOP, DtFimOP: TDateTime; OrdemDeServico: String): Boolean;
var
  DtAMIni, DtAMFim: TDateTime;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K235]*)
  (*Campo 02 (DT_SA�DA) - Valida��o: a data deve estar compreendida no per�odo
  da ordem de produ��o, se existente, campos DT_INI_OP e DT_FIN_OP do Registro
  K230. Se DT_FIN_OP do Registro K230 � Itens Produzidos estiver em branco, o
  campo DT_SA�DA dever� ser maior que o campo DT_INI_OP do Registro K230 e menor
  ou igual a DT_FIN do Registro K100. E em qualquer hip�tese a data deve estar
  compreendida no per�odo de apura��o � K100.*)
  dmkPF.DatasIniFimDoPeriodo(AnoMes, DtAMIni, DtAMFim);

  if (DtIniOP > 2) and (Data < DtIniOP) then
  begin
    Geral.MB_Aviso(
    'A data deve ser maior ou igual a data inicial do registro pai!');
    Result := False;
    Exit;
  end;

  if (DtFimOP > 2) and (Data > DtFimOP) then
  begin
    Geral.MB_Aviso(
    'A data deve ser menor ou igual a data final do registro pai!');
    Result := False;
    Exit;
  end;

  if (Data < DtAMIni) or (Data > DtAMFim) then
  begin
    Geral.MB_Aviso(
    'A data deve estar dentro do per�odo SPED!');
    Result := False;
    Exit;
  end;
end;

function TUnEfdIcmsIpi_PF.ValidaDataUnicaDentroDoAnoMesSPED(AnoMes: Integer;
  Data: TDateTime; OrdemDeServico: String): Boolean;
var
  DtAMIni, DtAMFim: TDateTime;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K250]*)
  //
  (*Campo 02 (DT_PROD) - Valida��o: a data deve estar compreendida no per�odo
  informado nos campos DT_INI e DT_FIN*)
  dmkPF.DatasIniFimDoPeriodo(AnoMes, DtAMIni, DtAMFim);
  if Data < 2 then
  begin
    Geral.MB_Aviso('Data deve ser informada!');
    Result := False;
    Exit;
  end;
  if (Data < DtAMIni) then
  begin
    Geral.MB_Aviso('Data deve ser maior ou igual a data inicial do per�odo!');
    Result := False;
    Exit;
  end;
  if (Data > DtAMFim) then
  begin
    Geral.MB_Aviso('Data deve ser menor ou igual a data final do per�odo!');
    Result := False;
    Exit;
  end;
end;

function TUnEfdIcmsIpi_PF.ValidaExigenciaValorMaiorQueZeroOuZero(AnoMes: Integer;
  DataIni, DataFim: TDateTime; Qtd: Double; OrdemDeServico: String): Boolean;
var
  DtAMIni, DtAMFim: TDateTime;
  A1, A2, A3, A4: Boolean;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K230]*)
  (*Campo 06 (QTD_ENC) � Preenchimento: n�o � admitida quantidade negativa.
  Valida��o:
    a) deve ser maior que zero quando: os campos DT_INI_OP e DT_FIN_OP estiverem
    preenchidos e compreendidos no per�odo do Registro K100 ou todos os tr�s
    campos DT_FIN_OP, DT_INI_OP e COD_DOC_OP n�o estiverem preenchidos;
    b) deve ser igual a zero quando o campo DT_FIN_OP estiver preenchido e for
    menor que o campo DT_INI do Registro 0000.*)
  dmkPF.DatasIniFimDoPeriodo(AnoMes, DtAMIni, DtAMFim);

  A1 := (DataIni > 2) and (DataFim > 2);
  A2 := (DataIni >= DtAMIni) and (DataIni <= DtAMFim) and
        (DataFim >= DtAMIni) and (DataFim <= DtAMFim);
  //
  A3 := (DataIni < 2) and (DataFim < 2) and (Trim(OrdemDeServico) = '');
  A4 := (A1 and A2) or A3;
  //
  if A4 and (Qtd <= 0) then
  begin
    Geral.MB_Aviso('N�o � admitida quantidade negativa ou zerada!');
    Result := False;
    Exit;
  end;
  //
  if ((DataFim > 2) and (DataFim < DtAMIni)) and (Qtd <> 0) then
  begin
    Geral.MB_Aviso(
    'Quantidade deve ser igual a zero quando o campo DT_FIN_OP estiver preenchido e for menor que o campo DT_INI do Registro 0000.!');
    Result := False;
    Exit;
  end;
end;

function TUnEfdIcmsIpi_PF.ValidaGGXEstahDentroDoAnoMesSPED(AnoMes,
  GraGruX: Integer; OrdemDeServico: String): Boolean;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K210]*)
  //
  (*Campo 05 (COD_ITEM_ORI) - Valida��o: o c�digo do item de origem dever�
  existir no campo COD_ITEM do Registro 0200.*)

  (*////////////// � feiro s� depois na exporta��o!!!??? //////////////*)
end;

function TUnEfdIcmsIpi_PF.ValidaGGXOri_DifereGGXDstDentroDoAnoMesSPED(AnoMes,
  GGXOri, GGXDst: Integer; OrdemDeServico: String): Boolean;
begin
  Result := not MyObjects.FIC(GGXOri = GGXDst, nil,
  'Reduzido deve ser diferente do reduzido origem / destino!');
end;

function TUnEfdIcmsIpi_PF.ValidaOSDentroDoAnoMesSPED(AnoMes: Integer; DataIni,
  DataFim: TDateTime; OrdemDeServico: String): Boolean;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K210]*)
  //
  (*Campo 04 (COD_DOC_OS) � Preenchimento: informar o c�digo da ordem de
  servi�o, caso exista.
  Valida��o: obrigat�rio se informado o campo DT_INI_OS.*)
  if DataIni > 2 then
  begin
    if Trim(OrdemDeServico) = '' then
    begin
      Geral.MB_Aviso(
      'Ordem de servi�o deve ser informada quando for informada a data inicial do per�odo!');
      Result := False;
    end;
  end;
end;

function TUnEfdIcmsIpi_PF.ValidaQtdeDentroDoAnoMesSPED(AnoMes: Integer;
  Qtde: Double; OrdemDeServico: String; MaiorQueZero: Boolean): Boolean;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K210]*)
  (*Campo 06 (QTD_ORI) � Preenchimento: n�o � admitida quantidade negativa.*)
  if Qtde < 0 then
  begin
    Geral.MB_Aviso('N�o � admitida quantidade negativa!');
    Result := False;
  end;
  if MaiorQueZero and (Qtde = 0) then
  begin
    Geral.MB_Aviso('N�o � admitida quantidade zerada!');
    Result := False;
  end;
end;

function TUnEfdIcmsIpi_PF.VerificaGGX_SPED(GraGruX, KndCod, KndNSU,
  KndItm: Integer; REG: String): Boolean;
begin
  if GraGruX = 0 then
  begin
    Result := False;
    Geral.MB_Erro('O reduzido ficou indefinido no registro ' + REG + sLineBreak +
    'ID Codigo: ' + Geral.FF0(KndCod) + sLineBreak +
    'IME-C: ' + Geral.FF0(KndNSU) + sLineBreak +
    'IME-I: ' + Geral.FF0(KndItm) + sLineBreak +
    '');
  end else Result := True;
end;

end.

