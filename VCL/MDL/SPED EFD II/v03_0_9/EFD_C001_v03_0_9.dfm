object FmEFD_C001_v03_0_9: TFmEFD_C001_v03_0_9
  Left = 368
  Top = 194
  Caption = 'EFD-SPEDC-001 :: Mercadorias'
  ClientHeight = 692
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 113
    Width = 1008
    Height = 579
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 57
      Width = 1008
      Height = 522
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet2: TTabSheet
        Caption = ' Per'#237'odos '
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 494
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Panel9: TPanel
            Left = 103
            Top = 1
            Width = 896
            Height = 403
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 896
              Height = 403
              Align = alClient
              TabOrder = 0
              object PageControl2: TPageControl
                Left = 2
                Top = 15
                Width = 892
                Height = 386
                ActivePage = TabSheet1
                Align = alClient
                TabOrder = 0
                object TabSheet3: TTabSheet
                  Caption = ' C100 :: NF (01) / NF Avulsa (1B) / NF Produtor (04) / NF-e (55)'
                  object dmkDBGrid2: TdmkDBGrid
                    Left = 0
                    Top = 0
                    Width = 884
                    Height = 358
                    Align = alClient
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'DT_DOC'
                        Title.Caption = 'Dta doc.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DT_E_S'
                        Title.Caption = 'Dta sai/ent.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CFOP'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CST_ICMS'
                        Title.Caption = 'CST'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_MOD'
                        Title.Caption = 'Mod.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SER'
                        Title.Caption = 'S'#233'rie'
                        Width = 32
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NUM_DOC'
                        Title.Caption = 'N'#186' N.F.'
                        Width = 60
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_SIT'
                        Title.Caption = 'Sit.'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Terceiro'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_TERC'
                        Title.Caption = 'Nome fornecedor'
                        Width = 92
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_DOC'
                        Title.Caption = '$ Docum.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_MERC'
                        Title.Caption = '$ Merc.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ALIQ_ICMS'
                        Title.Caption = '%ICMS'
                        Width = 40
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_ICMS'
                        Title.Caption = '$ ICMS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_PIS'
                        Title.Caption = 'PIS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_COFINS'
                        Title.Caption = 'COFINS'
                        Visible = True
                      end>
                    Color = clWindow
                    DataSource = DsEFD_C100
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'DT_DOC'
                        Title.Caption = 'Dta doc.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DT_E_S'
                        Title.Caption = 'Dta sai/ent.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CFOP'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CST_ICMS'
                        Title.Caption = 'CST'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_MOD'
                        Title.Caption = 'Mod.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SER'
                        Title.Caption = 'S'#233'rie'
                        Width = 32
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NUM_DOC'
                        Title.Caption = 'N'#186' N.F.'
                        Width = 60
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_SIT'
                        Title.Caption = 'Sit.'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Terceiro'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_TERC'
                        Title.Caption = 'Nome fornecedor'
                        Width = 92
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_DOC'
                        Title.Caption = '$ Docum.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_MERC'
                        Title.Caption = '$ Merc.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ALIQ_ICMS'
                        Title.Caption = '%ICMS'
                        Width = 40
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_ICMS'
                        Title.Caption = '$ ICMS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_PIS'
                        Title.Caption = 'PIS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_COFINS'
                        Title.Caption = 'COFINS'
                        Visible = True
                      end>
                  end
                end
                object TabSheet1: TTabSheet
                  Caption = 'C500 :: Energia el'#233'trica (06) / '#193'gua (29) / G'#225's (28)'
                  ImageIndex = 1
                  object dmkDBGrid1: TdmkDBGrid
                    Left = 0
                    Top = 0
                    Width = 884
                    Height = 358
                    Align = alClient
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'DT_DOC'
                        Title.Caption = 'Dta doc.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DT_E_S'
                        Title.Caption = 'Dta sai/ent.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CFOP'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CST_ICMS'
                        Title.Caption = 'CST'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_MOD'
                        Title.Caption = 'Mod.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SER'
                        Title.Caption = 'S'#233'rie'
                        Width = 32
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SUB'
                        Title.Caption = 'sub'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NUM_DOC'
                        Title.Caption = 'N'#186' N.F.'
                        Width = 60
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_SIT'
                        Title.Caption = 'Sit.'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Terceiro'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_TERC'
                        Title.Caption = 'Nome fornecedor'
                        Width = 92
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_DOC'
                        Title.Caption = '$ Docum.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_FORN'
                        Title.Caption = '$ Fornec.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ALIQ_ICMS'
                        Title.Caption = '%ICMS'
                        Width = 40
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_ICMS'
                        Title.Caption = '$ ICMS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_PIS'
                        Title.Caption = 'PIS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_COFINS'
                        Title.Caption = 'COFINS'
                        Visible = True
                      end>
                    Color = clWindow
                    DataSource = DsEFD_C500
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'DT_DOC'
                        Title.Caption = 'Dta doc.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DT_E_S'
                        Title.Caption = 'Dta sai/ent.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CFOP'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CST_ICMS'
                        Title.Caption = 'CST'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_MOD'
                        Title.Caption = 'Mod.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SER'
                        Title.Caption = 'S'#233'rie'
                        Width = 32
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SUB'
                        Title.Caption = 'sub'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NUM_DOC'
                        Title.Caption = 'N'#186' N.F.'
                        Width = 60
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_SIT'
                        Title.Caption = 'Sit.'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Terceiro'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_TERC'
                        Title.Caption = 'Nome fornecedor'
                        Width = 92
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_DOC'
                        Title.Caption = '$ Docum.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_FORN'
                        Title.Caption = '$ Fornec.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ALIQ_ICMS'
                        Title.Caption = '%ICMS'
                        Width = 40
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_ICMS'
                        Title.Caption = '$ ICMS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_PIS'
                        Title.Caption = 'PIS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_COFINS'
                        Title.Caption = 'COFINS'
                        Visible = True
                      end>
                  end
                end
              end
            end
          end
          object Panel10: TPanel
            Left = 1
            Top = 1
            Width = 102
            Height = 403
            Align = alLeft
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object DBGCab: TdmkDBGrid
              Left = 0
              Top = 0
              Width = 102
              Height = 403
              Align = alClient
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'MES_ANO'
                  Title.Caption = 'MES / ANO'
                  Width = 63
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsEFD_C001
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'MES_ANO'
                  Title.Caption = 'MES / ANO'
                  Width = 63
                  Visible = True
                end>
            end
          end
          object TGroupBox
            Left = 1
            Top = 429
            Width = 998
            Height = 64
            Align = alBottom
            TabOrder = 2
            object Panel3: TPanel
              Left = 2
              Top = 15
              Width = 994
              Height = 47
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object BtC500: TBitBtn
                Tag = 10083
                Left = 260
                Top = 2
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = 'C&500'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtC500Click
              end
              object BtPeriodo: TBitBtn
                Tag = 191
                Left = 12
                Top = 2
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = '&Per'#237'odo'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtPeriodoClick
              end
              object Panel2: TPanel
                Left = 885
                Top = 0
                Width = 109
                Height = 47
                Align = alRight
                Alignment = taRightJustify
                BevelOuter = bvNone
                TabOrder = 3
                object BtSaida0: TBitBtn
                  Tag = 13
                  Left = 4
                  Top = 2
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Sa'#237'da'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtSaida0Click
                end
              end
              object BtC100: TBitBtn
                Tag = 10085
                Left = 136
                Top = 2
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = 'C&100'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtC100Click
              end
            end
          end
          object Panel16: TPanel
            Left = 1
            Top = 404
            Width = 998
            Height = 25
            Align = alBottom
            TabOrder = 3
          end
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 57
      Align = alTop
      Caption = ' Empresa e per'#237'odo selecionado: '
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 40
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 23
          Height = 13
          Caption = 'Filial:'
        end
        object Label2: TLabel
          Left = 932
          Top = 0
          Width = 60
          Height = 13
          Caption = 'M'#202'S / ANO:'
          Enabled = False
          FocusControl = DBEdit1
        end
        object EdEmpresa: TdmkEditCB
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 64
          Top = 16
          Width = 865
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object DBEdit1: TDBEdit
          Left = 932
          Top = 16
          Width = 65
          Height = 21
          DataField = 'MES_ANO'
          DataSource = DsEFD_C001
          Enabled = False
          TabOrder = 2
        end
      end
    end
  end
  object Panel11: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 150
        Height = 32
        Caption = 'Mercadorias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 150
        Height = 32
        Caption = 'Mercadorias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 150
        Height = 32
        Caption = 'Mercadorias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 65
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel12: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 48
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 31
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object DsEFD_C001: TDataSource
    DataSet = QrEFD_C001
    Left = 392
    Top = 48
  end
  object QrEFD_C001: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEFD_C001AfterOpen
    BeforeClose = QrEFD_C001BeforeClose
    AfterScroll = QrEFD_C001AfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT,'
      'CONCAT(RIGHT(c001.AnoMes, 2), "/",'
      'LEFT(LPAD(c001.AnoMes, 6, "0"), 4)) MES_ANO, c001.*'
      'FROM efdicmsipic001 c001'
      'LEFT JOIN entidades ent ON ent.Codigo=c001.Empresa'
      'WHERE c001.ImporExpor=1'
      'AND c001.Empresa=-11'
      'ORDER BY c001.AnoMes DESC'
      '')
    Left = 392
    Top = 4
    object QrEFD_C001NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrEFD_C001MES_ANO: TWideStringField
      FieldName = 'MES_ANO'
      Required = True
      Size = 7
    end
    object QrEFD_C001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_C001AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_C001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_C001LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_C001REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_C001IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtPeriodo
    CanUpd01 = BtC500
    Left = 68
    Top = 16
  end
  object PMPeriodo: TPopupMenu
    OnPopup = PMPeriodoPopup
    Left = 36
    Top = 608
    object Incluinovoperiodo1: TMenuItem
      Caption = 'Inclui novo periodo'
      OnClick = Incluinovoperiodo1Click
    end
    object Excluiperiodoselecionado1: TMenuItem
      Caption = '&Exclui periodo selecionado'
      Enabled = False
      OnClick = Excluiperiodoselecionado1Click
    end
    object Itemns1: TMenuItem
      Caption = '&Item(ns)'
      Enabled = False
      Visible = False
    end
  end
  object PMC500: TPopupMenu
    OnPopup = PMC500Popup
    Left = 316
    Top = 608
    object IncluiC500: TMenuItem
      Caption = 'Inclui &nova NF C500'
      OnClick = IncluiC500Click
    end
    object AlteraC500: TMenuItem
      Caption = '&Altera NF C500 selecionada'
      OnClick = AlteraC500Click
    end
    object ExcluiC500: TMenuItem
      Caption = '&Exclui NF C500 selecionada'
      OnClick = ExcluiC500Click
    end
  end
  object QrEFD_C500: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) '
      'NO_TERC, c500.*'
      'FROM efdicmsipic500 c500 '
      'LEFT JOIN entidades ent ON ent.Codigo=c500.Terceiro'
      'WHERE c500.ImporExpor=3 '
      'AND c500.Empresa=-11 '
      'AND c500.AnoMes=201001'
      '')
    Left = 540
    Top = 4
    object QrEFD_C500NO_TERC: TWideStringField
      FieldName = 'NO_TERC'
      Size = 100
    end
    object QrEFD_C500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_C500AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_C500Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_C500LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_C500REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_C500IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Size = 1
    end
    object QrEFD_C500IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Size = 1
    end
    object QrEFD_C500COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_C500COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrEFD_C500COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Size = 2
    end
    object QrEFD_C500SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEFD_C500SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEFD_C500COD_CONS: TWideStringField
      FieldName = 'COD_CONS'
      Size = 2
    end
    object QrEFD_C500NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrEFD_C500DT_DOC: TDateField
      FieldName = 'DT_DOC'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_C500DT_E_S: TDateField
      FieldName = 'DT_E_S'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_C500VL_DOC: TFloatField
      FieldName = 'VL_DOC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_DESC: TFloatField
      FieldName = 'VL_DESC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_FORN: TFloatField
      FieldName = 'VL_FORN'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_SERV_NT: TFloatField
      FieldName = 'VL_SERV_NT'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_TERC: TFloatField
      FieldName = 'VL_TERC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_DA: TFloatField
      FieldName = 'VL_DA'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrEFD_C500VL_PIS: TFloatField
      FieldName = 'VL_PIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500TP_LIGACAO: TWideStringField
      FieldName = 'TP_LIGACAO'
      Size = 1
    end
    object QrEFD_C500COD_GRUPO_TENSAO: TWideStringField
      FieldName = 'COD_GRUPO_TENSAO'
      Size = 2
    end
    object QrEFD_C500Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEFD_C500Importado: TSmallintField
      FieldName = 'Importado'
    end
    object QrEFD_C500CST_ICMS: TWideStringField
      FieldName = 'CST_ICMS'
      Size = 3
    end
    object QrEFD_C500CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrEFD_C500ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500Lk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrEFD_C500DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_C500DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_C500UserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrEFD_C500UserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrEFD_C500AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEFD_C500AWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrEFD_C500AWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrEFD_C500Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEFD_C500CHV_DOCe: TWideStringField
      FieldName = 'CHV_DOCe'
      Size = 44
    end
    object QrEFD_C500FIN_DOCe: TSmallintField
      FieldName = 'FIN_DOCe'
      Required = True
    end
    object QrEFD_C500CHV_DOCe_REF: TWideStringField
      FieldName = 'CHV_DOCe_REF'
      Size = 44
    end
    object QrEFD_C500IND_DEST: TSmallintField
      FieldName = 'IND_DEST'
      Required = True
    end
    object QrEFD_C500COD_MUN_DEST: TIntegerField
      FieldName = 'COD_MUN_DEST'
      Required = True
    end
    object QrEFD_C500COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 50
    end
    object QrEFD_C500COD_MOD_DOC_REF: TSmallintField
      FieldName = 'COD_MOD_DOC_REF'
      Required = True
    end
    object QrEFD_C500HASH_DOC_REF: TWideStringField
      FieldName = 'HASH_DOC_REF'
      Size = 32
    end
    object QrEFD_C500SER_DOC_REF: TWideStringField
      FieldName = 'SER_DOC_REF'
      Size = 4
    end
    object QrEFD_C500NUM_DOC_REF: TIntegerField
      FieldName = 'NUM_DOC_REF'
      Required = True
    end
    object QrEFD_C500MES_DOC_REF: TIntegerField
      FieldName = 'MES_DOC_REF'
      Required = True
    end
    object QrEFD_C500ENER_INJET: TFloatField
      FieldName = 'ENER_INJET'
      Required = True
    end
    object QrEFD_C500OUTRAS_DED: TFloatField
      FieldName = 'OUTRAS_DED'
      Required = True
    end
    object QrEFD_C500Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsEFD_C500: TDataSource
    DataSet = QrEFD_C500
    Left = 540
    Top = 52
  end
  object PMC100: TPopupMenu
    Left = 188
    Top = 612
    object IncluiC1001: TMenuItem
      Caption = 'Inclui Nova NF C100'
      OnClick = IncluiC1001Click
    end
    object AlteraC1001: TMenuItem
      Caption = 'Altera NF C100 Selecionada'
      OnClick = AlteraC1001Click
    end
    object ExcluiC1001: TMenuItem
      Caption = 'Exclui NF C100 Selecionada'
      OnClick = ExcluiC1001Click
    end
  end
  object QrEFD_C100: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) '
      'NO_TERC, c100.*'
      'FROM efdicmsipic100 c100 '
      'LEFT JOIN entidades ent ON ent.Codigo=c100.Terceiro'
      'WHERE c100.ImporExpor=3 '
      'AND c100.Empresa=-11 '
      'AND c100.AnoMes=201001'
      '')
    Left = 468
    Top = 4
    object QrEFD_C100Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_C100NO_TERC: TWideStringField
      FieldName = 'NO_TERC'
      Size = 100
    end
    object QrEFD_C100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_C100AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_C100Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_C100LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_C100REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_C100IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Size = 1
    end
    object QrEFD_C100IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Size = 1
    end
    object QrEFD_C100COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_C100COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrEFD_C100COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Size = 2
    end
    object QrEFD_C100SER: TWideStringField
      FieldName = 'SER'
      Size = 3
    end
    object QrEFD_C100NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrEFD_C100CHV_NFE: TWideStringField
      FieldName = 'CHV_NFE'
      Size = 44
    end
    object QrEFD_C100DT_DOC: TDateField
      FieldName = 'DT_DOC'
    end
    object QrEFD_C100DT_E_S: TDateField
      FieldName = 'DT_E_S'
    end
    object QrEFD_C100VL_DOC: TFloatField
      FieldName = 'VL_DOC'
    end
    object QrEFD_C100IND_PGTO: TWideStringField
      FieldName = 'IND_PGTO'
      Size = 1
    end
    object QrEFD_C100VL_DESC: TFloatField
      FieldName = 'VL_DESC'
    end
    object QrEFD_C100VL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
    end
    object QrEFD_C100VL_MERC: TFloatField
      FieldName = 'VL_MERC'
    end
    object QrEFD_C100IND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEFD_C100VL_FRT: TFloatField
      FieldName = 'VL_FRT'
    end
    object QrEFD_C100VL_SEG: TFloatField
      FieldName = 'VL_SEG'
    end
    object QrEFD_C100VL_OUT_DA: TFloatField
      FieldName = 'VL_OUT_DA'
    end
    object QrEFD_C100VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrEFD_C100VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrEFD_C100VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
    end
    object QrEFD_C100VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
    end
    object QrEFD_C100VL_IPI: TFloatField
      FieldName = 'VL_IPI'
    end
    object QrEFD_C100VL_PIS: TFloatField
      FieldName = 'VL_PIS'
    end
    object QrEFD_C100VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
    end
    object QrEFD_C100VL_PIS_ST: TFloatField
      FieldName = 'VL_PIS_ST'
    end
    object QrEFD_C100VL_COFINS_ST: TFloatField
      FieldName = 'VL_COFINS_ST'
    end
    object QrEFD_C100ParTipo: TIntegerField
      FieldName = 'ParTipo'
    end
    object QrEFD_C100ParCodi: TIntegerField
      FieldName = 'ParCodi'
    end
    object QrEFD_C100FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrEFD_C100FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrEFD_C100ConfVal: TSmallintField
      FieldName = 'ConfVal'
    end
    object QrEFD_C100Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEFD_C100Importado: TSmallintField
      FieldName = 'Importado'
    end
    object QrEFD_C100CST_ICMS: TWideStringField
      FieldName = 'CST_ICMS'
      Size = 3
    end
    object QrEFD_C100CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrEFD_C100ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrEFD_C100VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
  end
  object DsEFD_C100: TDataSource
    DataSet = QrEFD_C100
    Left = 468
    Top = 52
  end
  object QrEfdInnC500Its: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efdicmsipic510')
    Left = 613
    Top = 6
    object QrEfdInnC500ItsAnoMes: TIntegerField
      FieldName = 'AnoMes'
      Required = True
    end
    object QrEfdInnC500ItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnC500ItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEfdInnC500ItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnC500ItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrEfdInnC500ItsQTD: TFloatField
      FieldName = 'QTD'
      Required = True
    end
    object QrEfdInnC500ItsUNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrEfdInnC500ItsVL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      Required = True
    end
    object QrEfdInnC500ItsVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
    end
    object QrEfdInnC500ItsCST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrEfdInnC500ItsCFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrEfdInnC500ItsVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
    end
    object QrEfdInnC500ItsALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      Required = True
    end
    object QrEfdInnC500ItsVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
    end
    object QrEfdInnC500ItsVL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Required = True
    end
    object QrEfdInnC500ItsALIQ_ST: TFloatField
      FieldName = 'ALIQ_ST'
      Required = True
    end
    object QrEfdInnC500ItsVL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Required = True
    end
    object QrEfdInnC500ItsCST_PIS: TWideStringField
      FieldName = 'CST_PIS'
      Size = 2
    end
    object QrEfdInnC500ItsNAT_BC_CRED: TWideStringField
      FieldName = 'NAT_BC_CRED'
      Required = True
      Size = 2
    end
    object QrEfdInnC500ItsVL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
      Required = True
    end
    object QrEfdInnC500ItsALIQ_PIS: TFloatField
      FieldName = 'ALIQ_PIS'
      Required = True
    end
    object QrEfdInnC500ItsVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
    end
    object QrEfdInnC500ItsCOD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEfdInnC500ItsCST_COFINS: TWideStringField
      FieldName = 'CST_COFINS'
      Size = 2
    end
    object QrEfdInnC500ItsVL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
      Required = True
    end
    object QrEfdInnC500ItsALIQ_COFINS: TFloatField
      FieldName = 'ALIQ_COFINS'
      Required = True
    end
    object QrEfdInnC500ItsVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
    end
  end
  object DsEfdIcmsIpic500: TDataSource
    Left = 613
    Top = 54
  end
end
