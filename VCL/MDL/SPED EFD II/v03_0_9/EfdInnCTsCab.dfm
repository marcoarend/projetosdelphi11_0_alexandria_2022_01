object FmEfdInnCTsCab: TFmEfdInnCTsCab
  Left = 339
  Top = 185
  Caption = 'SPE-ENTRA-003 :: CT-e de Entrada'
  ClientHeight = 608
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 225
      Height = 48
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 200
        Height = 32
        Caption = 'CT-e de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 200
        Height = 32
        Caption = 'CT-e de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 200
        Height = 32
        Caption = 'CT-e de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 273
      Top = 0
      Width = 687
      Height = 48
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 683
        Height = 31
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 538
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 91
    Width = 1008
    Height = 447
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object PainelDados: TPanel
      Left = 0
      Top = 123
      Width = 1008
      Height = 324
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Label17: TLabel
        Left = 12
        Top = 0
        Width = 74
        Height = 13
        Caption = 'Data aquisi'#231#227'o:'
      end
      object Label11: TLabel
        Left = 132
        Top = 0
        Width = 67
        Height = 13
        Caption = 'Data emiss'#227'o:'
      end
      object LaCI: TLabel
        Left = 736
        Top = 1
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
        Enabled = False
      end
      object LaSitPedido: TLabel
        Left = 108
        Top = 0
        Width = 6
        Height = 13
        Caption = '0'
        Visible = False
      end
      object Label18: TLabel
        Left = 216
        Top = 80
        Width = 48
        Height = 13
        Caption = '$ Servi'#231'o:'
      end
      object Label29: TLabel
        Left = 405
        Top = 80
        Width = 82
        Height = 13
        Caption = '$ N'#227'o tribut. [F4]:'
      end
      object Label19: TLabel
        Left = 250
        Top = 0
        Width = 75
        Height = 13
        Caption = 'Transportadora:'
        Enabled = False
      end
      object Label108: TLabel
        Left = 676
        Top = 40
        Width = 139
        Height = 13
        Caption = 'Indicador do tipo de frete [F4]'
        FocusControl = EdIND_FRT
      end
      object Label3: TLabel
        Left = 595
        Top = 80
        Width = 67
        Height = 13
        Caption = '$ Documento:'
      end
      object Label7: TLabel
        Left = 311
        Top = 80
        Width = 58
        Height = 13
        Caption = '$ Desconto:'
      end
      object Label66: TLabel
        Left = 12
        Top = 41
        Width = 31
        Height = 13
        Caption = 'CFOP:'
      end
      object SbCFOP: TSpeedButton
        Left = 650
        Top = 54
        Width = 25
        Height = 25
        Caption = '...'
        OnClick = SbCFOPClick
      end
      object Label20: TLabel
        Left = 501
        Top = 80
        Width = 59
        Height = 13
        Caption = '$ Opera'#231#227'o:'
      end
      object Label476: TLabel
        Left = 12
        Top = 120
        Width = 222
        Height = 13
        Caption = 'ICMS (retorno: RedBC, BC , % e valor):'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 12
        Top = 80
        Width = 192
        Height = 13
        Caption = 'Tipo de conhecimento de transporte [F4]'
        FocusControl = EdTP_CT_e
      end
      object Label480: TLabel
        Left = 12
        Top = 200
        Width = 226
        Height = 13
        Caption = 'COFINS  (retorno: CST, BC , % e valor):'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label24: TLabel
        Left = 13
        Top = 160
        Width = 197
        Height = 13
        Caption = 'PIS (retorno: CST, BC , % e valor):'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 12
        Top = 280
        Width = 182
        Height = 13
        Caption = 'C'#243'digo da Base de C'#225'lculo do Cr'#233'dito:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 12
        Top = 240
        Width = 205
        Height = 13
        Caption = 'Indicador da Natureza do Frete Contratado:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object TPDT_A_P: TdmkEditDateTimePicker
        Left = 12
        Top = 16
        Width = 112
        Height = 21
        Date = 40060.000000000000000000
        Time = 0.661368159722769600
        TabOrder = 0
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPDT_DOC: TdmkEditDateTimePicker
        Left = 132
        Top = 16
        Width = 112
        Height = 21
        Date = 40060.000000000000000000
        Time = 0.661368159722769600
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 795
        Top = 17
        Width = 200
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOME'
        ListSource = DsCI
        TabOrder = 5
        dmkEditCB = EdCliInt
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCliInt: TdmkEditCB
        Left = 736
        Top = 17
        Width = 55
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdVL_SERV: TdmkEdit
        Left = 216
        Top = 96
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = CalculaValorTotalDocumento
      end
      object EdVL_NT: TdmkEdit
        Left = 405
        Top = 96
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = CalculaValorTotalDocumento
        OnKeyDown = EdVL_NTKeyDown
      end
      object EdVL_BC_ICMS: TdmkEdit
        Left = 141
        Top = 136
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 19
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = CalculaValorTotalDocumento
      end
      object EdVL_ICMS: TdmkEdit
        Left = 275
        Top = 136
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 21
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdTransportador: TdmkEditCB
        Left = 250
        Top = 16
        Width = 55
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransportador
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransportador: TdmkDBLookupComboBox
        Left = 309
        Top = 16
        Width = 424
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOME'
        ListSource = DsTransportadores
        TabOrder = 3
        dmkEditCB = EdTransportador
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdIND_FRT: TdmkEdit
        Left = 676
        Top = 56
        Width = 21
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '4'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '-1'
        QryCampo = 'IND_PGTO'
        UpdCampo = 'IND_PGTO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = -1
        ValWarn = False
        OnChange = EdIND_FRTChange
        OnKeyDown = EdIND_FRTKeyDown
      end
      object EdIND_FRT_TXT: TdmkEdit
        Left = 698
        Top = 56
        Width = 299
        Height = 21
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdVL_DOC: TdmkEdit
        Left = 595
        Top = 96
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdVL_DESC: TdmkEdit
        Left = 311
        Top = 96
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = CalculaValorTotalDocumento
      end
      object EdCFOP: TdmkEditCB
        Left = 12
        Top = 56
        Width = 55
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCFOP
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCFOP: TdmkDBLookupComboBox
        Left = 69
        Top = 56
        Width = 580
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCFOP
        TabOrder = 7
        TabStop = False
        dmkEditCB = EdCFOP
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdVL_OPR: TdmkEdit
        Left = 501
        Top = 96
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdVL_OPRChange
      end
      object EdVL_RED_BC: TdmkEdit
        Left = 59
        Top = 136
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = CalculaValorTotalDocumento
        OnKeyDown = EdVL_RED_BCKeyDown
      end
      object EdALIQ_ICMS: TdmkEdit
        Left = 224
        Top = 136
        Width = 48
        Height = 21
        Alignment = taRightJustify
        TabOrder = 20
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = CalculaValorTotalDocumento
      end
      object EdICMS_CST: TdmkEdit
        Left = 12
        Top = 136
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 3
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000'
        QryName = 'QrNFeItsN'
        QryCampo = 'UC_ICMS_CST_B'
        UpdCampo = 'UC_ICMS_CST_B'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdICMS_CSTChange
        OnKeyDown = EdICMS_CSTKeyDown
      end
      object EdUCTextoB: TdmkEdit
        Left = 357
        Top = 136
        Width = 640
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 22
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdTP_CT_e_TXT: TdmkEdit
        Left = 34
        Top = 96
        Width = 179
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdTP_CT_e: TdmkEdit
        Left = 12
        Top = 96
        Width = 21
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '9'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTP_CT_eChange
        OnKeyDown = EdTP_CT_eKeyDown
      end
      object EdCST_PIS: TdmkEdit
        Left = 12
        Top = 176
        Width = 45
        Height = 21
        TabOrder = 23
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrNFeItsQ'
        QryCampo = 'UC_PIS_CST'
        UpdCampo = 'UC_PIS_CST'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdCST_PISChange
        OnKeyDown = EdCST_PISKeyDown
      end
      object EdCST_COFINS: TdmkEdit
        Left = 12
        Top = 216
        Width = 45
        Height = 21
        TabOrder = 28
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrNFeItsS'
        QryCampo = 'UC_COFINS_CST'
        UpdCampo = 'UC_COFINS_CST'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdCST_COFINSChange
        OnKeyDown = EdCST_COFINSKeyDown
      end
      object EdVL_BC_COFINS: TdmkEdit
        Left = 58
        Top = 216
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 29
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdVL_BC_COFINSChange
        OnKeyDown = EdVL_BC_COFINSKeyDown
      end
      object EdVL_BC_PIS: TdmkEdit
        Left = 58
        Top = 176
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 24
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdVL_BC_PISChange
        OnKeyDown = EdVL_BC_PISKeyDown
      end
      object EdALIQ_PIS: TdmkEdit
        Left = 140
        Top = 176
        Width = 48
        Height = 21
        Alignment = taRightJustify
        TabOrder = 25
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdALIQ_PISChange
      end
      object EdVL_PIS: TdmkEdit
        Left = 190
        Top = 176
        Width = 72
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 26
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdVL_PISChange
      end
      object EdVL_COFINS: TdmkEdit
        Left = 190
        Top = 216
        Width = 72
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 31
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdVL_COFINSChange
      end
      object EdCST_COFINS_TXT: TdmkEdit
        Left = 266
        Top = 216
        Width = 731
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 32
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCST_PIS_TXT: TdmkEdit
        Left = 266
        Top = 176
        Width = 731
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 27
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdALIQ_COFINS: TdmkEdit
        Left = 140
        Top = 216
        Width = 48
        Height = 21
        Alignment = taRightJustify
        TabOrder = 30
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdALIQ_COFINSChange
      end
      object EdNAT_BC_CRED: TdmkEdit
        Left = 12
        Top = 296
        Width = 45
        Height = 21
        TabOrder = 35
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrNFeItsS'
        QryCampo = 'NAT_BC_CRED'
        UpdCampo = 'NAT_BC_CRED'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdNAT_BC_CREDChange
        OnKeyDown = EdNAT_BC_CREDKeyDown
      end
      object EdNAT_BC_CRED_TXT: TdmkEdit
        Left = 58
        Top = 296
        Width = 939
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 36
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdIND_NAT_FRT: TdmkEdit
        Left = 12
        Top = 256
        Width = 45
        Height = 21
        TabOrder = 33
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 1
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrNFeItsS'
        QryCampo = 'NAT_BC_CRED'
        UpdCampo = 'NAT_BC_CRED'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdIND_NAT_FRTChange
        OnKeyDown = EdIND_NAT_FRTKeyDown
      end
      object EdIND_NAT_FRT_TXT: TdmkEdit
        Left = 58
        Top = 256
        Width = 939
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 34
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 123
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaCHV_CTE: TLabel
        Left = 12
        Top = 2
        Width = 127
        Height = 13
        Caption = 'Chave de acesso do CT-e:'
      end
      object LaCOD_MOD: TLabel
        Left = 428
        Top = 1
        Width = 38
        Height = 13
        Caption = 'Modelo:'
        Enabled = False
      end
      object LaSER: TLabel
        Left = 471
        Top = 1
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
        Enabled = False
      end
      object LaNUM_DOC: TLabel
        Left = 580
        Top = 2
        Width = 86
        Height = 13
        Caption = 'N'#250'm. conhecimto:'
        Enabled = False
      end
      object SbNF_VP: TSpeedButton
        Left = 676
        Top = 14
        Width = 25
        Height = 25
        Caption = '<>'
        Enabled = False
        OnClick = SbNF_VPClick
      end
      object Label12: TLabel
        Left = 708
        Top = 1
        Width = 125
        Height = 13
        Caption = 'Atrelamento com XML BD:'
        Enabled = False
      end
      object Label26: TLabel
        Left = 12
        Top = 40
        Width = 210
        Height = 13
        Caption = 'C'#243'digo da situa'#231#227'o do documento fiscal [F4]'
        FocusControl = EdCOD_SIT
      end
      object SbChave: TSpeedButton
        Left = 388
        Top = 14
        Width = 25
        Height = 25
        Caption = '>'
        OnClick = SbChaveClick
      end
      object Label30: TLabel
        Left = 515
        Top = 1
        Width = 44
        Height = 13
        Caption = 'Subs'#233'rie:'
      end
      object Label105: TLabel
        Left = 12
        Top = 80
        Width = 85
        Height = 13
        Caption = 'Cidade de origem:'
      end
      object Label16: TLabel
        Left = 508
        Top = 80
        Width = 88
        Height = 13
        Caption = 'Cidade de destino:'
      end
      object Label15: TLabel
        Left = 320
        Top = 40
        Width = 73
        Height = 13
        Caption = 'Movimenta'#231#227'o:'
      end
      object EdCHV_CTE: TdmkEdit
        Left = 12
        Top = 16
        Width = 373
        Height = 21
        MaxLength = 44
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCOD_MOD: TdmkEdit
        Left = 428
        Top = 16
        Width = 39
        Height = 21
        Alignment = taCenter
        Enabled = False
        MaxLength = 2
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '57'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '57'
        ValWarn = False
      end
      object EdSER: TdmkEdit
        Left = 471
        Top = 16
        Width = 42
        Height = 21
        Enabled = False
        MaxLength = 3
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNUM_DOC: TdmkEdit
        Left = 580
        Top = 16
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        MaxLength = 9
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNFe_StaLnk: TdmkEdit
        Left = 708
        Top = 16
        Width = 28
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        MaxLength = 9
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNFe_FatID: TdmkEdit
        Left = 740
        Top = 16
        Width = 39
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        MaxLength = 2
        ReadOnly = True
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNFe_FatNum: TdmkEdit
        Left = 783
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        MaxLength = 3
        ReadOnly = True
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCOD_SIT: TdmkEdit
        Left = 12
        Top = 56
        Width = 21
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '4'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '-1'
        QryCampo = 'IND_PGTO'
        UpdCampo = 'IND_PGTO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = -1
        ValWarn = False
        OnChange = EdCOD_SITChange
        OnKeyDown = EdCOD_SITKeyDown
      end
      object EdCOD_SIT_TXT: TdmkEdit
        Left = 34
        Top = 56
        Width = 283
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkTipoNF: TdmkCheckBox
        Left = 844
        Top = 20
        Width = 221
        Height = 17
        Caption = 'Conhecimento de Frete Eletr'#244'nico (CT-e)'
        Checked = True
        State = cbChecked
        TabOrder = 0
        Visible = False
        OnClick = CkTipoNFClick
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object EdSUB: TdmkEdit
        Left = 515
        Top = 16
        Width = 62
        Height = 21
        MaxLength = 3
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCOD_MUN_ORIG: TdmkEditCB
        Left = 12
        Top = 96
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ECodMunici'
        UpdCampo = 'ECodMunici'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCOD_MUN_ORIG
        IgnoraDBLookupComboBox = True
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCOD_MUN_ORIG: TdmkDBLookupComboBox
        Left = 69
        Top = 96
        Width = 432
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsMuniciOrig
        TabOrder = 14
        dmkEditCB = EdCOD_MUN_ORIG
        QryCampo = 'ECodMunici'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCOD_MUN_DEST: TdmkEditCB
        Left = 508
        Top = 96
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ECodMunici'
        UpdCampo = 'ECodMunici'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCOD_MUN_DEST
        IgnoraDBLookupComboBox = True
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCOD_MUN_DEST: TdmkDBLookupComboBox
        Left = 565
        Top = 96
        Width = 432
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsMuniciDest
        TabOrder = 16
        dmkEditCB = EdCOD_MUN_DEST
        QryCampo = 'ECodMunici'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdRegrFiscal: TdmkEditCB
        Left = 320
        Top = 56
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdRegrFiscalRedefinido
        DBLookupComboBox = CBRegrFiscal
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBRegrFiscal: TdmkDBLookupComboBox
        Left = 380
        Top = 56
        Width = 617
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFisRegCad
        TabOrder = 12
        dmkEditCB = EdRegrFiscal
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 43
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label25: TLabel
      Left = 12
      Top = 4
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object Label5: TLabel
      Left = 60
      Top = 4
      Width = 38
      Height = 13
      Caption = 'Tipo ID:'
    end
    object Label2: TLabel
      Left = 104
      Top = 4
      Width = 54
      Height = 13
      Caption = 'ID Entrada:'
    end
    object Label13: TLabel
      Left = 164
      Top = 4
      Width = 32
      Height = 13
      Caption = 'IME-C:'
    end
    object LaAreaP2: TLabel
      Left = 516
      Top = 4
      Width = 37
      Height = 13
      Caption = #193'rea ft'#178':'
      Enabled = False
    end
    object LaAreaM2: TLabel
      Left = 440
      Top = 4
      Width = 39
      Height = 13
      Caption = #193'rea m'#178':'
      Enabled = False
    end
    object LaPeso: TLabel
      Left = 360
      Top = 4
      Width = 27
      Height = 13
      Caption = 'Peso:'
    end
    object LaPecas: TLabel
      Left = 284
      Top = 4
      Width = 33
      Height = 13
      Caption = 'Pe'#231'as:'
    end
    object Label6: TLabel
      Left = 224
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label10: TLabel
      Left = 588
      Top = 4
      Width = 50
      Height = 13
      Caption = 'Valor total:'
    end
    object Label1: TLabel
      Left = 664
      Top = 4
      Width = 46
      Height = 13
      Caption = 'Motorista:'
    end
    object SBMotorista: TSpeedButton
      Left = 892
      Top = 20
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBMotoristaClick
    end
    object Label23: TLabel
      Left = 916
      Top = 4
      Width = 30
      Height = 13
      Caption = 'Placa:'
    end
    object EdEmpresa: TdmkEdit
      Left = 12
      Top = 20
      Width = 45
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEmpresaChange
    end
    object EdMovFatID: TdmkEdit
      Left = 60
      Top = 20
      Width = 41
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMovFatNum: TdmkEdit
      Left = 104
      Top = 20
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMovimCod: TdmkEdit
      Left = 164
      Top = 20
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdPesoKg: TdmkEdit
      Left = 360
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdAreaP2: TdmkEditCalc
      Left = 512
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaP2'
      UpdCampo = 'AreaP2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      dmkEditCalcA = EdAreaM2
      CalcType = ctP2toM2
      CalcFrac = cfCento
    end
    object EdAreaM2: TdmkEditCalc
      Left = 436
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaM2'
      UpdCampo = 'AreaM2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      dmkEditCalcA = EdAreaP2
      CalcType = ctM2toP2
      CalcFrac = cfQuarto
    end
    object EdPecas: TdmkEdit
      Left = 284
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'PesoKg'
      UpdCampo = 'PesoKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdControle: TdmkEdit
      Left = 224
      Top = 20
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Conta'
      UpdCampo = 'Conta'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdValorT: TdmkEdit
      Left = 588
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ValorT'
      UpdCampo = 'ValorT'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdValorTRedefinido
    end
    object EdMotorista: TdmkEditCB
      Left = 664
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBMotorista
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBMotorista: TdmkDBLookupComboBox
      Left = 720
      Top = 20
      Width = 169
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsMotorista
      TabOrder = 11
      dmkEditCB = EdMotorista
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdPlaca: TdmkEdit
      Left = 916
      Top = 20
      Width = 80
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 12
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Placa'
      UpdCampo = 'Placa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object QrMotorista: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 244
    Top = 40
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsMotorista: TDataSource
    DataSet = QrMotorista
    Left = 244
    Top = 88
  end
  object QrTransportadores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NOME'
      'FROM entidades'
      'WHERE Fornece3='#39'V'#39
      'ORDER BY Nome')
    Left = 68
    Top = 4
    object QrTransportadoresTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrTransportadoresCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTransportadoresCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTransportadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportadoresNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
  end
  object DsTransportadores: TDataSource
    DataSet = QrTransportadores
    Left = 96
    Top = 4
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NOME'
      'FROM entidades'
      'WHERE Cliente2='#39'V'#39
      'ORDER BY Nome')
    Left = 408
    Top = 4
    object QrCITipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCICNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrCICPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 408
    Top = 52
  end
  object Qr00_NFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, IDCtrl,'
      'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk,'
      'ide_mod, ide_serie, ide_nNF  '
      'FROM nfecaba '
      'WHERE FatID=53 '
      'AND Empresa=-11 '
      'AND ide_mod>0 '
      'AND ide_serie>0 '
      'AND ide_nNF>0 ')
    Left = 478
    Top = 9
    object Qr00_NFeCabAFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object Qr00_NFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object Qr00_NFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object Qr00_NFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object Qr00_NFeCabAAtrelaFatID: TIntegerField
      FieldName = 'AtrelaFatID'
      Required = True
    end
    object Qr00_NFeCabAAtrelaStaLnk: TSmallintField
      FieldName = 'AtrelaStaLnk'
      Required = True
    end
    object Qr00_NFeCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
      Required = True
    end
    object Qr00_NFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      Required = True
    end
    object Qr00_NFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Required = True
    end
    object Qr00_NFeCabAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object Qr00_NFeCabAAtrelaFatNum: TIntegerField
      FieldName = 'AtrelaFatNum'
      Required = True
    end
  end
  object QrMuniciOrig: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 772
    Top = 12
    object QrMuniciOrigCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMuniciOrigNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMuniciOrig: TDataSource
    DataSet = QrMuniciOrig
    Left = 772
    Top = 60
  end
  object QrMuniciDest: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 860
    Top = 8
    object QrMuniciDestCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMuniciDestNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMuniciDest: TDataSource
    DataSet = QrMuniciDest
    Left = 864
    Top = 60
  end
  object QrCFOP: TMySQLQuery
    Left = 680
    Top = 13
    object QrCFOPCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCFOP: TDataSource
    DataSet = QrCFOP
    Left = 680
    Top = 62
  end
  object QrFisRegCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.ModeloNF,'
      'frc.Nome, frc.Financeiro, imp.Nome NO_MODELO_NF,'
      'frc.TipoMov,'
      'FrtInnCFOP, FrtInnICMS_CST, FrtInnALIQ_ICMS, '
      'FrtTES_ICMS, FrtTES_BC_ICMS, FrtInnPIS_CST, '
      'FrtInnALIQ_PIS, FrtTES_PIS, FrtTES_BC_PIS, '
      'FrtInnCOFINS_CST, FrtInnALIQ_COFINS, FrtTES_COFINS, '
      'FrtTES_BC_COFINS, FrtInnIND_NAT_FRT, FrtInnNAT_BC_CRED'
      'FROM fisregcad frc'
      'LEFT JOIN imprime imp ON imp.Codigo=frc.ModeloNF'
      'ORDER BY Nome')
    Left = 588
    Top = 16
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFisRegCadModeloNF: TIntegerField
      FieldName = 'ModeloNF'
      Required = True
    end
    object QrFisRegCadNO_MODELO_NF: TWideStringField
      FieldName = 'NO_MODELO_NF'
      Size = 100
    end
    object QrFisRegCadFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
    object QrFisRegCadTipoMov: TSmallintField
      FieldName = 'TipoMov'
    end
    object QrFisRegCadFrtInnCFOP: TIntegerField
      FieldName = 'FrtInnCFOP'
      Required = True
    end
    object QrFisRegCadFrtInnICMS_CST: TIntegerField
      FieldName = 'FrtInnICMS_CST'
      Required = True
    end
    object QrFisRegCadFrtInnALIQ_ICMS: TFloatField
      FieldName = 'FrtInnALIQ_ICMS'
      Required = True
    end
    object QrFisRegCadFrtTES_ICMS: TSmallintField
      FieldName = 'FrtTES_ICMS'
      Required = True
    end
    object QrFisRegCadFrtTES_BC_ICMS: TSmallintField
      FieldName = 'FrtTES_BC_ICMS'
      Required = True
    end
    object QrFisRegCadFrtInnPIS_CST: TWideStringField
      FieldName = 'FrtInnPIS_CST'
      Size = 2
    end
    object QrFisRegCadFrtInnALIQ_PIS: TFloatField
      FieldName = 'FrtInnALIQ_PIS'
      Required = True
    end
    object QrFisRegCadFrtTES_PIS: TSmallintField
      FieldName = 'FrtTES_PIS'
      Required = True
    end
    object QrFisRegCadFrtTES_BC_PIS: TSmallintField
      FieldName = 'FrtTES_BC_PIS'
      Required = True
    end
    object QrFisRegCadFrtInnCOFINS_CST: TWideStringField
      FieldName = 'FrtInnCOFINS_CST'
      Required = True
      Size = 2
    end
    object QrFisRegCadFrtInnALIQ_COFINS: TFloatField
      FieldName = 'FrtInnALIQ_COFINS'
      Required = True
    end
    object QrFisRegCadFrtTES_COFINS: TSmallintField
      FieldName = 'FrtTES_COFINS'
      Required = True
    end
    object QrFisRegCadFrtTES_BC_COFINS: TSmallintField
      FieldName = 'FrtTES_BC_COFINS'
      Required = True
    end
    object QrFisRegCadFrtInnIND_NAT_FRT: TWideStringField
      FieldName = 'FrtInnIND_NAT_FRT'
      Required = True
      Size = 1
    end
    object QrFisRegCadFrtInnNAT_BC_CRED: TWideStringField
      FieldName = 'FrtInnNAT_BC_CRED'
      Required = True
      Size = 2
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 588
    Top = 68
  end
end
