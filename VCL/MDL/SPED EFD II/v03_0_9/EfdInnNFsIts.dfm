object FmEfdInnNFsIts: TFmEfdInnNFsIts
  Left = 368
  Top = 178
  Caption = 'SPE-ENTRA-002 :: Itens de NF-e de Entrada'
  ClientHeight = 582
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MaxHeight = 660
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 426
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Painel1: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 37
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 910
        Top = 12
        Width = 29
        Height = 13
        Caption = 'Linha:'
      end
      object Label3: TLabel
        Left = 214
        Top = 12
        Width = 53
        Height = 13
        Caption = 'ID entrada:'
      end
      object Label6: TLabel
        Left = 338
        Top = 12
        Width = 32
        Height = 13
        Caption = 'IME-C:'
      end
      object Label9: TLabel
        Left = 658
        Top = 12
        Width = 31
        Height = 13
        Caption = 'ID DF:'
      end
      object Label11: TLabel
        Left = 758
        Top = 12
        Width = 36
        Height = 13
        Caption = 'ID item:'
      end
      object Label15: TLabel
        Left = 442
        Top = 12
        Width = 56
        Height = 13
        Caption = 'Tipo IME-C:'
      end
      object Label25: TLabel
        Left = 570
        Top = 12
        Width = 19
        Height = 13
        Caption = 'Par:'
      end
      object Label27: TLabel
        Left = 10
        Top = 12
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label14: TLabel
        Left = 114
        Top = 12
        Width = 38
        Height = 13
        Caption = 'Tipo ID:'
      end
      object EdLinha: TdmkEdit
        Left = 941
        Top = 8
        Width = 57
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMovFatNum: TdmkEdit
        Left = 273
        Top = 8
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 377
        Top = 8
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdControle: TdmkEdit
        Left = 693
        Top = 8
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdControleChange
      end
      object EdConta: TdmkEdit
        Left = 797
        Top = 8
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMovimNiv: TdmkEdit
        Left = 505
        Top = 8
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMovimTwn: TdmkEdit
        Left = 593
        Top = 8
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEdit
        Left = 64
        Top = 8
        Width = 45
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMovFatID: TdmkEdit
        Left = 157
        Top = 12
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object PainelDados: TPanel
      Left = 0
      Top = 37
      Width = 1008
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 10
        Top = 8
        Width = 48
        Height = 13
        Caption = 'Reduzido:'
      end
      object SBProduto: TSpeedButton
        Left = 769
        Top = 22
        Width = 25
        Height = 25
        Caption = '...'
        OnClick = SBProdutoClick
      end
      object Label12: TLabel
        Left = 795
        Top = 8
        Width = 24
        Height = 13
        Caption = 'Lote:'
      end
      object Label66: TLabel
        Left = 10
        Top = 49
        Width = 31
        Height = 13
        Caption = 'CFOP:'
      end
      object SbCFOP: TSpeedButton
        Left = 853
        Top = 63
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SbCFOPClick
      end
      object Label4: TLabel
        Left = 878
        Top = 48
        Width = 27
        Height = 13
        Caption = '% IPI:'
      end
      object Label7: TLabel
        Left = 921
        Top = 48
        Width = 25
        Height = 13
        Caption = '$ IPI:'
      end
      object EdGraGruX: TdmkEditCB
        Left = 10
        Top = 24
        Width = 55
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 65
        Top = 24
        Width = 704
        Height = 21
        Color = clWhite
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 1
        dmkEditCB = EdGraGruX
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdxLote: TdmkEdit
        Left = 796
        Top = 24
        Width = 200
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCFOP: TdmkEditCB
        Left = 10
        Top = 64
        Width = 55
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCFOPChange
        DBLookupComboBox = CBCFOP
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCFOP: TdmkDBLookupComboBox
        Left = 65
        Top = 64
        Width = 784
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCFOP
        TabOrder = 4
        dmkEditCB = EdCFOP
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdOri_IPIpIPI: TdmkEdit
        Left = 878
        Top = 64
        Width = 39
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdOri_IPIpIPIChange
      end
      object EdOri_IPIvIPI: TdmkEdit
        Left = 921
        Top = 64
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdOri_IPIvIPIChange
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 129
      Width = 1008
      Height = 297
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object Panel6: TPanel
        Left = 0
        Top = 221
        Width = 1008
        Height = 76
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        object CkDtCorrApo: TCheckBox
          Left = 622
          Top = 7
          Width = 187
          Height = 17
          Caption = #201' corre'#231#227'o de apontamento. Data:'
          TabOrder = 0
          OnClick = CkDtCorrApoClick
        end
        object TPDtCorrApo: TdmkEditDateTimePicker
          Left = 825
          Top = 5
          Width = 168
          Height = 21
          Date = 44621.000000000000000000
          Time = 0.833253726850671200
          Enabled = False
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpSPED_EFD_MIN_MAX
        end
        object GroupBox1: TGroupBox
          Left = 8
          Top = 0
          Width = 445
          Height = 62
          Caption = ' Ajustes (C197):'
          TabOrder = 2
          object Panel9: TPanel
            Left = 2
            Top = 15
            Width = 441
            Height = 45
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label5: TLabel
              Left = 81
              Top = 4
              Width = 26
              Height = 13
              Caption = '$ BC:'
            end
            object Label8: TLabel
              Left = 287
              Top = 4
              Width = 38
              Height = 13
              Caption = '$ ICMS:'
            end
            object Label29: TLabel
              Left = 243
              Top = 4
              Width = 40
              Height = 13
              Caption = '% ICMS:'
            end
            object Label30: TLabel
              Left = 359
              Top = 4
              Width = 43
              Height = 13
              Caption = '$ Outros:'
            end
            object Label31: TLabel
              Left = 5
              Top = 4
              Width = 59
              Height = 13
              Caption = '$ Opera'#231#227'o:'
            end
            object Label32: TLabel
              Left = 165
              Top = 4
              Width = 49
              Height = 13
              Caption = '$ Red.BC:'
            end
            object EdAjusteVL_BC_ICMS: TdmkEdit
              Left = 82
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdVL_BC_ICMSChange
            end
            object EdAjusteALIQ_ICMS: TdmkEdit
              Left = 242
              Top = 20
              Width = 40
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdALIQ_ICMSChange
            end
            object EdAjusteVL_ICMS: TdmkEdit
              Left = 284
              Top = 20
              Width = 72
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdAjusteVL_OUTROS: TdmkEdit
              Left = 360
              Top = 20
              Width = 72
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdAjusteVL_OPR: TdmkEdit
              Left = 6
              Top = 20
              Width = 72
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdAjusteVL_RED_BC: TdmkEdit
              Left = 166
              Top = 20
              Width = 72
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 41
        Width = 1008
        Height = 180
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object GBRecuImpost: TGroupBox
          Left = 0
          Top = 0
          Width = 1008
          Height = 180
          Align = alClient
          Caption = ' Recupera'#231#227'o de impostos: '
          TabOrder = 0
          object Panel2: TPanel
            Left = 2
            Top = 15
            Width = 1004
            Height = 163
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label13: TLabel
              Left = 261
              Top = 0
              Width = 143
              Height = 13
              Caption = 'ICMSST (BC , % e valor):'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label24: TLabel
              Left = 54
              Top = 80
              Width = 171
              Height = 13
              Caption = ' (Cr'#233'dito: BC , % e valor): [F4]'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label26: TLabel
              Left = 54
              Top = 120
              Width = 167
              Height = 13
              Caption = '(Cr'#233'dito: BC , % e valor): [F4]'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label16: TLabel
              Left = 54
              Top = 40
              Width = 135
              Height = 13
              Caption = '(Cr'#233'dito: BC , % e valor): [F4]'
            end
            object Label476: TLabel
              Left = 10
              Top = 0
              Width = 35
              Height = 13
              Caption = 'ICMS:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label477: TLabel
              Left = 8
              Top = 40
              Width = 13
              Height = 13
              Caption = 'IPI'
            end
            object Label478: TLabel
              Left = 8
              Top = 80
              Width = 21
              Height = 13
              Caption = 'PIS'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label480: TLabel
              Left = 8
              Top = 120
              Width = 46
              Height = 13
              Caption = 'COFINS'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label20: TLabel
              Left = 54
              Top = 0
              Width = 135
              Height = 13
              Caption = '(Cr'#233'dito: BC , % e valor): [F4]'
            end
            object EdALIQ_ICMS: TdmkEdit
              Left = 136
              Top = 16
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdALIQ_ICMSChange
            end
            object EdVL_ICMS: TdmkEdit
              Left = 186
              Top = 16
              Width = 72
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdALIQ_PIS: TdmkEdit
              Left = 136
              Top = 96
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 15
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdALIQ_PISChange
            end
            object EdVL_PIS: TdmkEdit
              Left = 186
              Top = 96
              Width = 72
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 16
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdALIQ_COFINS: TdmkEdit
              Left = 136
              Top = 136
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 20
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdALIQ_COFINSChange
            end
            object EdVL_COFINS: TdmkEdit
              Left = 186
              Top = 136
              Width = 72
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 21
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdALIQ_IPI: TdmkEdit
              Left = 136
              Top = 56
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 10
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdALIQ_IPIChange
            end
            object EdVL_IPI: TdmkEdit
              Left = 186
              Top = 56
              Width = 72
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 11
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdICMS_CST: TdmkEdit
              Left = 8
              Top = 16
              Width = 45
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '999'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000'
              QryName = 'QrNFeItsN'
              QryCampo = 'UC_ICMS_CST_B'
              UpdCampo = 'UC_ICMS_CST_B'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdICMS_CSTChange
              OnKeyDown = EdICMS_CSTKeyDown
            end
            object EdUCTextoB: TdmkEdit
              Left = 462
              Top = 16
              Width = 531
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCST_IPI: TdmkEdit
              Left = 8
              Top = 56
              Width = 45
              Height = 21
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryName = 'QrNFeItsO'
              QryCampo = 'UC_IPI_CST'
              UpdCampo = 'UC_IPI_CST'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCST_IPIChange
              OnExit = EdCST_IPIExit
              OnKeyDown = EdCST_IPIKeyDown
            end
            object EdUCTextoIPI_CST: TdmkEdit
              Left = 262
              Top = 56
              Width = 731
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 12
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnRedefinido = EdUCTextoIPI_CSTRedefinido
            end
            object EdCST_PIS: TdmkEdit
              Left = 8
              Top = 96
              Width = 45
              Height = 21
              TabOrder = 13
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryName = 'QrNFeItsQ'
              QryCampo = 'UC_PIS_CST'
              UpdCampo = 'UC_PIS_CST'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCST_PISChange
              OnKeyDown = EdCST_PISKeyDown
            end
            object EdUCTextoPIS_CST: TdmkEdit
              Left = 262
              Top = 96
              Width = 731
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 17
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCST_COFINS: TdmkEdit
              Left = 8
              Top = 136
              Width = 45
              Height = 21
              TabOrder = 18
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryName = 'QrNFeItsS'
              QryCampo = 'UC_COFINS_CST'
              UpdCampo = 'UC_COFINS_CST'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCST_COFINSChange
              OnKeyDown = EdCST_COFINSKeyDown
            end
            object EdUCTextoCOFINS_CST: TdmkEdit
              Left = 262
              Top = 136
              Width = 731
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 22
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdVL_BC_ICMS: TdmkEdit
              Left = 54
              Top = 16
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdVL_BC_ICMSChange
              OnKeyDown = EdVL_BC_ICMSKeyDown
            end
            object EdVL_BC_PIS: TdmkEdit
              Left = 54
              Top = 96
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 14
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdVL_BC_PISChange
              OnKeyDown = EdVL_BC_PISKeyDown
            end
            object EdVL_BC_IPI: TdmkEdit
              Left = 54
              Top = 56
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdVL_BC_IPIChange
              OnKeyDown = EdVL_BC_IPIKeyDown
            end
            object EdVL_BC_COFINS: TdmkEdit
              Left = 54
              Top = 136
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 19
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdVL_BC_COFINSChange
              OnKeyDown = EdVL_BC_COFINSKeyDown
            end
            object EdVL_BC_ICMS_ST: TdmkEdit
              Left = 262
              Top = 16
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdALIQ_ST: TdmkEdit
              Left = 344
              Top = 16
              Width = 40
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdALIQ_ICMSChange
            end
            object EdVL_ICMS_ST: TdmkEdit
              Left = 386
              Top = 16
              Width = 72
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label19: TLabel
          Left = 11
          Top = 0
          Width = 86
          Height = 13
          Caption = 'Total qtde l'#237'quido:'
        end
        object Label10: TLabel
          Left = 276
          Top = 0
          Width = 50
          Height = 13
          Caption = 'Valor Item:'
          Enabled = False
        end
        object Label17: TLabel
          Left = 825
          Top = 0
          Width = 75
          Height = 13
          Caption = '$ Desp. Acess.:'
        end
        object Label18: TLabel
          Left = 565
          Top = 0
          Width = 75
          Height = 13
          Caption = '$ Valor produto:'
        end
        object Label21: TLabel
          Left = 909
          Top = 0
          Width = 58
          Height = 13
          Caption = '$ Desconto:'
        end
        object Label22: TLabel
          Left = 749
          Top = 0
          Width = 46
          Height = 13
          Caption = '$ Seguro:'
        end
        object Label23: TLabel
          Left = 659
          Top = 0
          Width = 36
          Height = 13
          Caption = '$ Frete:'
        end
        object Label28: TLabel
          Left = 156
          Top = 0
          Width = 117
          Height = 13
          Caption = 'Movimenta estoque [F4]:'
          FocusControl = EdIND_MOV
        end
        object EdQTD: TdmkEdit
          Left = 10
          Top = 16
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Color = clWhite
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdVL_ITEM: TdmkEdit
          Left = 272
          Top = 16
          Width = 96
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdVL_ITEMChange
        end
        object Edprod_vOutro: TdmkEdit
          Left = 827
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdOri_IPIvIPIChange
        end
        object Edprod_vProd: TdmkEdit
          Left = 561
          Top = 16
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdOri_IPIvIPIChange
        end
        object EdVL_DESC: TdmkEdit
          Left = 909
          Top = 16
          Width = 88
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdOri_IPIvIPIChange
        end
        object Edprod_vSeg: TdmkEdit
          Left = 749
          Top = 16
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdOri_IPIvIPIChange
        end
        object Edprod_vFrete: TdmkEdit
          Left = 659
          Top = 16
          Width = 88
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdOri_IPIvIPIChange
        end
        object DBEdit1: TDBEdit
          Left = 104
          Top = 16
          Width = 48
          Height = 21
          DataField = 'SIGLAUNIDMED'
          DataSource = DsGraGruX
          TabOrder = 1
        end
        object EdIND_MOV: TdmkEdit
          Left = 156
          Top = 16
          Width = 21
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '4'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '-1'
          QryCampo = 'IND_PGTO'
          UpdCampo = 'IND_PGTO'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = -1
          ValWarn = False
          OnChange = EdIND_MOVChange
          OnKeyDown = EdIND_MOVKeyDown
        end
        object EdIND_MOV_TXT: TdmkEdit
          Left = 178
          Top = 16
          Width = 95
          Height = 21
          ReadOnly = True
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 307
        Height = 32
        Caption = 'Itens de NF-e de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 307
        Height = 32
        Caption = 'Itens de NF-e de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 307
        Height = 32
        Caption = 'Itens de NF-e de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 474
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 518
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 859
        Top = 0
        Width = 145
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 20
          Top = 4
          Width = 119
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrPQ1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Nome NOMEPQ, pfo.* '
      'FROM pqfor pfo'
      'LEFT JOIN pq pq ON pfo.PQ=pq.Codigo'
      'WHERE pfo.CI=:P0'
      'AND pfo.IQ=:P1'
      'ORDER BY pq.Nome')
    Left = 792
    Top = 18
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
    object QrPQ1NOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQ1PQ: TIntegerField
      FieldName = 'PQ'
    end
  end
  object DsPQ1: TDataSource
    DataSet = QrPQ1
    Left = 792
    Top = 66
  end
  object QrLocalizaPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM pq'
      'WHERE Codigo=:P0'
      'AND IQ=:P1')
    Left = 612
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object QrPQCli: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPQCliCalcFields
    Left = 848
    Top = 18
    object QrPQCliCustoPadrao: TFloatField
      FieldName = 'CustoPadrao'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQCliMoedaPadrao: TIntegerField
      FieldName = 'MoedaPadrao'
    end
    object QrPQCliNOMEMOEDAPADRAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMOEDAPADRAO'
      Size = 5
      Calculated = True
    end
  end
  object DsPQCli: TDataSource
    DataSet = QrPQCli
    Left = 848
    Top = 66
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ICMSRec_pAliq, PISRec_pAliq,  '
      'COFINSRec_pAliq  '
      'FROM gragru1 '
      'WHERE Nivel1=1 ')
    Left = 528
    Top = 52
    object QrGraGru1ICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrGraGru1PISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrGraGru1COFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
  end
  object PMProduto: TPopupMenu
    Left = 568
    Top = 64
    object Cadastrodoproduto1: TMenuItem
      Caption = '&Cadastro do produto'
      OnClick = Cadastrodoproduto1Click
    end
    object Dadosfiscais1: TMenuItem
      Caption = 'Dados fiscais'
      OnClick = Dadosfiscais1Click
    end
  end
  object QrCFOP: TMySQLQuery
    Left = 912
    Top = 17
    object QrCFOPCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCFOP: TDataSource
    DataSet = QrCFOP
    Left = 912
    Top = 66
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq,'
      'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle > -900000'
      'ORDER BY NO_PRD_TAM_COR')
    Left = 124
    Top = 36
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 166
    end
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGruXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGruXEx_TIPI: TWideStringField
      FieldName = 'Ex_TIPI'
      Size = 3
    end
    object QrGraGruXGrandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrGraGruXTipo_Item: TIntegerField
      FieldName = 'Tipo_Item'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 124
    Top = 84
  end
end
