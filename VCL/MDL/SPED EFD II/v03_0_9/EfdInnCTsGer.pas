unit EfdInnCTsGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, mySQLDbTables, AppListas, UnDmkProcFunc, dmkDBGridZTO,
  dmkCheckBox, UnProjGroup_Consts, UnProjGroup_Vars, UnAppEnums,
  dmkEditDateTimePicker, UnApp_Jan;

type
  TFmEfdInnCTsGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrEfdInnCTsCab: TMySQLQuery;
    DsEfdInnCTsCab: TDataSource;
    DGDados: TDBGrid;
    QrFornecedor: TmySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    QrMotorista: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    Panel6: TPanel;
    Panel5: TPanel;
    Label2: TLabel;
    Label22: TLabel;
    BtReabre: TBitBtn;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    EdMotorista: TdmkEditCB;
    CBMotorista: TdmkDBLookupComboBox;
    EdNFeNum: TdmkEdit;
    Label10: TLabel;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    Label13: TLabel;
    Label14: TLabel;
    QrEfdInnCTsCabMovFatID: TIntegerField;
    QrEfdInnCTsCabMovFatNum: TIntegerField;
    QrEfdInnCTsCabMovimCod: TIntegerField;
    QrEfdInnCTsCabEmpresa: TIntegerField;
    QrEfdInnCTsCabControle: TIntegerField;
    QrEfdInnCTsCabIsLinked: TSmallintField;
    QrEfdInnCTsCabSqLinked: TIntegerField;
    QrEfdInnCTsCabCliInt: TIntegerField;
    QrEfdInnCTsCabTerceiro: TIntegerField;
    QrEfdInnCTsCabPecas: TFloatField;
    QrEfdInnCTsCabPesoKg: TFloatField;
    QrEfdInnCTsCabAreaM2: TFloatField;
    QrEfdInnCTsCabAreaP2: TFloatField;
    QrEfdInnCTsCabValorT: TFloatField;
    QrEfdInnCTsCabMotorista: TIntegerField;
    QrEfdInnCTsCabPlaca: TWideStringField;
    QrEfdInnCTsCabIND_OPER: TWideStringField;
    QrEfdInnCTsCabIND_EMIT: TWideStringField;
    QrEfdInnCTsCabCOD_MOD: TWideStringField;
    QrEfdInnCTsCabCOD_SIT: TSmallintField;
    QrEfdInnCTsCabSER: TWideStringField;
    QrEfdInnCTsCabSUB: TWideStringField;
    QrEfdInnCTsCabNUM_DOC: TIntegerField;
    QrEfdInnCTsCabCHV_CTE: TWideStringField;
    QrEfdInnCTsCabCTeStatus: TIntegerField;
    QrEfdInnCTsCabDT_DOC: TDateField;
    QrEfdInnCTsCabDT_A_P: TDateField;
    QrEfdInnCTsCabTP_CT_e: TSmallintField;
    QrEfdInnCTsCabCHV_CTE_REF: TWideStringField;
    QrEfdInnCTsCabVL_DOC: TFloatField;
    QrEfdInnCTsCabVL_DESC: TFloatField;
    QrEfdInnCTsCabIND_FRT: TWideStringField;
    QrEfdInnCTsCabVL_SERV: TFloatField;
    QrEfdInnCTsCabVL_BC_ICMS: TFloatField;
    QrEfdInnCTsCabVL_ICMS: TFloatField;
    QrEfdInnCTsCabVL_NT: TFloatField;
    QrEfdInnCTsCabCOD_INF: TWideStringField;
    QrEfdInnCTsCabCOD_CTA: TWideStringField;
    QrEfdInnCTsCabCOD_MUN_ORIG: TIntegerField;
    QrEfdInnCTsCabCOD_MUN_DEST: TIntegerField;
    QrEfdInnCTsCabNFe_FatID: TIntegerField;
    QrEfdInnCTsCabNFe_FatNum: TIntegerField;
    QrEfdInnCTsCabNFe_StaLnk: TSmallintField;
    QrEfdInnCTsCabVSVmcWrn: TSmallintField;
    QrEfdInnCTsCabVSVmcObs: TWideStringField;
    QrEfdInnCTsCabVSVmcSeq: TWideStringField;
    QrEfdInnCTsCabVSVmcSta: TSmallintField;
    QrEfdInnCTsCabCST_ICMS: TIntegerField;
    QrEfdInnCTsCabCFOP: TIntegerField;
    QrEfdInnCTsCabALIQ_ICMS: TFloatField;
    QrEfdInnCTsCabVL_OPR: TFloatField;
    QrEfdInnCTsCabVL_RED_BC: TFloatField;
    QrEfdInnCTsCabCOD_OBS: TWideStringField;
    QrEfdInnCTsCabRegrFiscal: TIntegerField;
    QrEfdInnCTsCabIND_NAT_FRT: TWideStringField;
    QrEfdInnCTsCabVL_ITEM: TFloatField;
    QrEfdInnCTsCabCST_PIS: TWideStringField;
    QrEfdInnCTsCabNAT_BC_CRED: TWideStringField;
    QrEfdInnCTsCabVL_BC_PIS: TFloatField;
    QrEfdInnCTsCabALIQ_PIS: TFloatField;
    QrEfdInnCTsCabVL_PIS: TFloatField;
    QrEfdInnCTsCabCST_COFINS: TWideStringField;
    QrEfdInnCTsCabVL_BC_COFINS: TFloatField;
    QrEfdInnCTsCabALIQ_COFINS: TFloatField;
    QrEfdInnCTsCabVL_COFINS: TFloatField;
    QrEfdInnCTsCabNO_TRANSPORTA: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QrEfdInnCTsCabBeforeClose(DataSet: TDataSet);
    procedure QrEfdInnCTsCabAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    procedure LocalizaRegistro;
    procedure ReabrePesquisa();
  public
    { Public declarations }
    //FCodigo, FControle, FSerieFch, FFicha: Integer;
    //
  end;

  var
  FmEfdInnCTsGer: TFmEfdInnCTsGer;

implementation

uses UnMyObjects, Module, UnVS_CRC_PF, DmkDAC_PF, ModVS_CRC;

{$R *.DFM}

procedure TFmEfdInnCTsGer.BtOKClick(Sender: TObject);
begin
  LocalizaRegistro;
end;

procedure TFmEfdInnCTsGer.BtReabreClick(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmEfdInnCTsGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdInnCTsGer.DGDadosDblClick(Sender: TObject);
var
  IDCtrl, Janela, Empresa, FatID, FatNum, MovimCod: Integer;
begin
  //
  if (QrEfdInnCTsCab.State <> dsInactive) and (QrEfdInnCTsCab.RecordCount > 0) then
  begin
    IDCtrl := 0;
    Empresa  := QrEfdInnCTsCabEmpresa.Value;
    FatID    := QrEfdInnCTsCabMovFatID.Value;
    FatNum   := QrEfdInnCTsCabMovFatNum.Value;
    MovimCod := QrEfdInnCTsCabMovimCod.Value;
    //
    case QrEfdInnCTsCabMovFatID.Value of
      VAR_FATID_0010: // PQ, Uso e Consumo, Embalagens, Patrimonio, etc.
        App_Jan.MostraFormEntradaInsumos(Empresa, FatID, FatNum, MovimCod);
      VAR_FATID_1003, // Couro in natura
      VAR_FATID_1041: // Couro curtido
        App_Jan.MostraFormEntradaMateriaPrima(Empresa, FatID, FatNum, MovimCod);
      else Geral.MB_Info('FatID n�o implementado: ' + Geral.FF0(FatID) +
        sLineBreak + 'Solicite � DERMATEK sua implanta��o!');
    end;
  end;
end;

procedure TFmEfdInnCTsGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdInnCTsGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DmModVS_CRC.QrIMEI.Close;
  //
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  //
  TPIni.Date := 0; //Date - 90;
  TPFim.Date := 0; //Date;
end;

procedure TFmEfdInnCTsGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdInnCTsGer.FormShow(Sender: TObject);
begin
  EdFornecedor.SetFocus;
end;

procedure TFmEfdInnCTsGer.LocalizaRegistro;
begin
  if (QrEfdInnCTsCab.State <> dsInactive) and (QrEfdInnCTsCab.RecordCount > 0) then
  begin
    Close;
  end;
end;

procedure TFmEfdInnCTsGer.QrEfdInnCTsCabAfterOpen(DataSet: TDataSet);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Registros encontrados: ' +
  Geral.FF0(QrEfdInnCTsCab.RecordCount));
end;

procedure TFmEfdInnCTsGer.QrEfdInnCTsCabBeforeClose(DataSet: TDataSet);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, '...');
end;

procedure TFmEfdInnCTsGer.ReabrePesquisa();
var
  Observ, Marca, SQL1, SQL2, SQL3, SQL4, SQL5, SQL6, SQL7, SQL8, SQL9, SQL0,
  SQLA, SQLB, SQLC, SQLD, SQLE, SQLF, SQLG, SQLH, SQLI, SQLJ, SQLK, SQL_MovimID,
  MovimNivs: String;
  ATT_MovimNiv, Tabela: String;
  DtIni, DtFim: TDateTime;
  CkIni, CkFim: Boolean;
  // ini 2022-04-09
  TabMaeNome, SQL_TabMaeLeftJoin, SQL_TabMaeAND: String;
  //
  CTeNum, Motorista, Fornecedor: Integer;
begin
  Fornecedor  := EdFornecedor.ValueVariant;
  Motorista   := EdMotorista.ValueVariant;
  CTeNum      := EdNFeNum.ValueVariant;
  DtIni := Trunc(TPIni.Date);
  DtFim := Trunc(TPFim.Date);
  CkIni := DtIni > 2;
  CkFim := DtFim > 2;
  //
  SQL0 := dmkPF.SQL_Periodo('WHERE DT_A_P ', DtIni, DtFim, CkIni, CkFim);
  //
  if CTeNum > 0 then
    SQL1 := 'AND eicc.NUM_DOC=' + Geral.FF0(CteNum)
  else
    SQL1 := '';

  if Fornecedor <> 0 then
    SQL2 := 'AND eicc.Terceiro=' + Geral.FF0(Fornecedor)
  else
    SQL2 := '';
  //
  if Motorista <> 0 then
    SQL3 := 'AND eicc.Motorista=' + Geral.FF0(Motorista)
  else
    SQL3 := '';
  //
  //
  if (TPIni.Date > 2) or (TPFim.Date > 2) then
  begin
  end else
    SQL4 := '';

  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnCTsCab, Dmod.MyDB, [
  'SELECT eicc.*, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TRANSPORTA',
  'FROM efdinnctscab eicc',
  'LEFT JOIN entidades ent ON ent.Codigo=eicc.Terceiro',
  SQL0,
  SQL1,
  SQL2,
  SQL3,
  SQL4,
  //
  'ORDER BY eicc.DT_A_P DESC ',
  '']);
  //Geral.MB_Teste(QrEfdInnCTsCab.SQL.Text);
end;

end.
