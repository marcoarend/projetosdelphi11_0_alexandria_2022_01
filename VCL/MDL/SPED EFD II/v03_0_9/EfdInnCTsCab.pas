unit EfdInnCTsCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkCheckBox, dmkEditCalc, SPED_Listas, UnFinanceiro, UnGrl_Consts;

type
  TFmEfdInnCTsCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrMotorista: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMotorista: TDataSource;
    QrTransportadores: TMySQLQuery;
    QrTransportadoresTipo: TSmallintField;
    QrTransportadoresCNPJ: TWideStringField;
    QrTransportadoresCPF: TWideStringField;
    QrTransportadoresCodigo: TIntegerField;
    QrTransportadoresNOME: TWideStringField;
    DsTransportadores: TDataSource;
    QrCI: TMySQLQuery;
    QrCITipo: TSmallintField;
    QrCICNPJ: TWideStringField;
    QrCICPF: TWideStringField;
    QrCICodigo: TIntegerField;
    QrCINOME: TWideStringField;
    DsCI: TDataSource;
    Panel3: TPanel;
    PainelDados: TPanel;
    Label17: TLabel;
    Label11: TLabel;
    LaCI: TLabel;
    LaSitPedido: TLabel;
    TPDT_A_P: TdmkEditDateTimePicker;
    TPDT_DOC: TdmkEditDateTimePicker;
    CBCliInt: TdmkDBLookupComboBox;
    EdCliInt: TdmkEditCB;
    Panel5: TPanel;
    LaCHV_CTE: TLabel;
    EdCHV_CTE: TdmkEdit;
    LaCOD_MOD: TLabel;
    EdCOD_MOD: TdmkEdit;
    EdSER: TdmkEdit;
    LaSER: TLabel;
    LaNUM_DOC: TLabel;
    EdNUM_DOC: TdmkEdit;
    SbNF_VP: TSpeedButton;
    EdNFe_StaLnk: TdmkEdit;
    Label12: TLabel;
    EdNFe_FatID: TdmkEdit;
    EdNFe_FatNum: TdmkEdit;
    EdCOD_SIT: TdmkEdit;
    Label26: TLabel;
    EdCOD_SIT_TXT: TdmkEdit;
    CkTipoNF: TdmkCheckBox;
    Label18: TLabel;
    EdVL_SERV: TdmkEdit;
    Label29: TLabel;
    EdVL_NT: TdmkEdit;
    Qr00_NFeCabA: TMySQLQuery;
    Qr00_NFeCabAFatID: TIntegerField;
    Qr00_NFeCabAFatNum: TIntegerField;
    Qr00_NFeCabAEmpresa: TIntegerField;
    Qr00_NFeCabAIDCtrl: TIntegerField;
    Qr00_NFeCabAAtrelaFatID: TIntegerField;
    Qr00_NFeCabAAtrelaStaLnk: TSmallintField;
    Qr00_NFeCabAide_mod: TSmallintField;
    Qr00_NFeCabAide_serie: TIntegerField;
    Qr00_NFeCabAide_nNF: TIntegerField;
    Qr00_NFeCabAId: TWideStringField;
    SbChave: TSpeedButton;
    Qr00_NFeCabAAtrelaFatNum: TIntegerField;
    Label30: TLabel;
    EdSUB: TdmkEdit;
    EdVL_BC_ICMS: TdmkEdit;
    EdVL_ICMS: TdmkEdit;
    Label19: TLabel;
    EdTransportador: TdmkEditCB;
    CBTransportador: TdmkDBLookupComboBox;
    Label108: TLabel;
    EdIND_FRT: TdmkEdit;
    EdIND_FRT_TXT: TdmkEdit;
    EdVL_DOC: TdmkEdit;
    Label3: TLabel;
    EdVL_DESC: TdmkEdit;
    Label7: TLabel;
    QrMuniciOrig: TMySQLQuery;
    QrMuniciOrigCodigo: TIntegerField;
    QrMuniciOrigNome: TWideStringField;
    DsMuniciOrig: TDataSource;
    QrMuniciDest: TMySQLQuery;
    QrMuniciDestCodigo: TIntegerField;
    QrMuniciDestNome: TWideStringField;
    DsMuniciDest: TDataSource;
    EdCOD_MUN_ORIG: TdmkEditCB;
    Label105: TLabel;
    CBCOD_MUN_ORIG: TdmkDBLookupComboBox;
    EdCOD_MUN_DEST: TdmkEditCB;
    Label16: TLabel;
    CBCOD_MUN_DEST: TdmkDBLookupComboBox;
    QrCFOP: TMySQLQuery;
    QrCFOPCodigo: TLargeintField;
    QrCFOPNome: TWideStringField;
    DsCFOP: TDataSource;
    Label66: TLabel;
    EdCFOP: TdmkEditCB;
    CBCFOP: TdmkDBLookupComboBox;
    SbCFOP: TSpeedButton;
    EdVL_OPR: TdmkEdit;
    Label20: TLabel;
    EdVL_RED_BC: TdmkEdit;
    EdALIQ_ICMS: TdmkEdit;
    Label476: TLabel;
    EdICMS_CST: TdmkEdit;
    EdUCTextoB: TdmkEdit;
    EdTP_CT_e_TXT: TdmkEdit;
    Label8: TLabel;
    EdTP_CT_e: TdmkEdit;
    QrFisRegCad: TMySQLQuery;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadModeloNF: TIntegerField;
    QrFisRegCadNO_MODELO_NF: TWideStringField;
    QrFisRegCadFinanceiro: TSmallintField;
    QrFisRegCadTipoMov: TSmallintField;
    DsFisRegCad: TDataSource;
    Label15: TLabel;
    EdRegrFiscal: TdmkEditCB;
    CBRegrFiscal: TdmkDBLookupComboBox;
    Panel6: TPanel;
    Label25: TLabel;
    EdEmpresa: TdmkEdit;
    Label5: TLabel;
    EdMovFatID: TdmkEdit;
    Label2: TLabel;
    EdMovFatNum: TdmkEdit;
    Label13: TLabel;
    EdMovimCod: TdmkEdit;
    EdPesoKg: TdmkEdit;
    EdAreaP2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    LaPeso: TLabel;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    EdControle: TdmkEdit;
    Label6: TLabel;
    Label10: TLabel;
    EdValorT: TdmkEdit;
    EdMotorista: TdmkEditCB;
    Label1: TLabel;
    CBMotorista: TdmkDBLookupComboBox;
    SBMotorista: TSpeedButton;
    EdPlaca: TdmkEdit;
    Label23: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    EdCST_PIS: TdmkEdit;
    Label480: TLabel;
    EdCST_COFINS: TdmkEdit;
    EdVL_BC_COFINS: TdmkEdit;
    EdVL_BC_PIS: TdmkEdit;
    Label24: TLabel;
    EdALIQ_PIS: TdmkEdit;
    EdVL_PIS: TdmkEdit;
    EdVL_COFINS: TdmkEdit;
    EdCST_COFINS_TXT: TdmkEdit;
    EdCST_PIS_TXT: TdmkEdit;
    EdALIQ_COFINS: TdmkEdit;
    EdNAT_BC_CRED: TdmkEdit;
    EdNAT_BC_CRED_TXT: TdmkEdit;
    Label4: TLabel;
    Label9: TLabel;
    EdIND_NAT_FRT: TdmkEdit;
    EdIND_NAT_FRT_TXT: TdmkEdit;
    QrFisRegCadFrtInnCFOP: TIntegerField;
    QrFisRegCadFrtInnICMS_CST: TIntegerField;
    QrFisRegCadFrtInnALIQ_ICMS: TFloatField;
    QrFisRegCadFrtTES_ICMS: TSmallintField;
    QrFisRegCadFrtTES_BC_ICMS: TSmallintField;
    QrFisRegCadFrtInnPIS_CST: TWideStringField;
    QrFisRegCadFrtInnALIQ_PIS: TFloatField;
    QrFisRegCadFrtTES_PIS: TSmallintField;
    QrFisRegCadFrtTES_BC_PIS: TSmallintField;
    QrFisRegCadFrtInnCOFINS_CST: TWideStringField;
    QrFisRegCadFrtInnALIQ_COFINS: TFloatField;
    QrFisRegCadFrtTES_COFINS: TSmallintField;
    QrFisRegCadFrtTES_BC_COFINS: TSmallintField;
    QrFisRegCadFrtInnIND_NAT_FRT: TWideStringField;
    QrFisRegCadFrtInnNAT_BC_CRED: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBMotoristaClick(Sender: TObject);
    procedure EdCOD_SITChange(Sender: TObject);
    procedure EdCOD_SITKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkTipoNFClick(Sender: TObject);
    procedure EdIND_FRTChange(Sender: TObject);
    procedure EdIND_FRTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    // Meu
    procedure CalculaValorTotalDocumento(Sender: TObject);
    procedure SbNF_VPClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure SbChaveClick(Sender: TObject);
    procedure EdICMS_CSTChange(Sender: TObject);
    procedure EdICMS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbCFOPClick(Sender: TObject);
    procedure EdTP_CT_eChange(Sender: TObject);
    procedure EdTP_CT_eKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure EdVL_NTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdValorTRedefinido(Sender: TObject);
    procedure EdCST_PISChange(Sender: TObject);
    procedure EdCST_PISKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCST_COFINSChange(Sender: TObject);
    procedure EdCST_COFINSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdVL_PISChange(Sender: TObject);
    procedure EdVL_COFINSChange(Sender: TObject);
    procedure EdNAT_BC_CREDChange(Sender: TObject);
    procedure EdNAT_BC_CREDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdIND_NAT_FRTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdIND_NAT_FRTChange(Sender: TObject);
    procedure EdVL_BC_PISChange(Sender: TObject);
    procedure EdALIQ_PISChange(Sender: TObject);
    procedure EdVL_BC_COFINSChange(Sender: TObject);
    procedure EdALIQ_COFINSChange(Sender: TObject);
    procedure EdVL_OPRChange(Sender: TObject);
    procedure EdRegrFiscalRedefinido(Sender: TObject);
    procedure EdVL_RED_BCKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdVL_BC_PISKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdVL_BC_COFINSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    F_IND_PGTO_EFD, F_IND_FRT_EFD, F_COD_SIT_EFD, F_TP_CT_e,
    F_NAT_BC_CRED, F_IND_NAT_FRT: MyArrayLista;
    //
    function ItemDeListaSpedNaoDefinido(Codigo: Integer; Texto, Mensagem:
             String; Lista: MyArrayLista): Boolean;
    procedure ReopenMunici(Query: TmySQLQuery; UF: String);
    procedure ReopenCFOP();
    procedure RedefineIndicadorDaNaturezaDoFrete();
    procedure CalculaRetornoImpostos(Qual: TRetornImpost);
    procedure SetaParametrosDaRegraFiscal();
    procedure SetaBasesDeCalculo();

  public
    { Public declarations }
    FThisFatID, FThisFatNum, FThisMovimCod, FControle: Integer;
    //
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmEfdInnCTsCab: TFmEfdInnCTsCab;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral, (*VSInnCab,*) NFeXMLGerencia, NFe_PF, UnMySQLCuringa,
  ModuleNFe_0000, UnGrade_Jan, UnSPED_Geral_PF;

{$R *.DFM}

procedure TFmEfdInnCTsCab.BtOKClick(Sender: TObject);
var
  Placa, IND_OPER, IND_EMIT, COD_MOD, SER, SUB, CHV_CTE, DT_DOC, DT_A_P,
  CHV_CTE_REF, IND_FRT, COD_INF, COD_CTA, (*VSVmcObs, VSVmcSeq,*) COD_OBS: String;
  MovFatID, MovFatNum, MovimCod, Empresa, Controle, (*IsLinked, SqLinked,*)
  CliInt, Terceiro, Motorista, COD_SIT, NUM_DOC, CTeStatus, TP_CT_e, COD_MUN_ORIG,
  COD_MUN_DEST, NFe_FatID, NFe_FatNum, NFe_StaLnk, (*VSVmcWrn, VSVmcSta,*)
  CST_ICMS, CFOP, RegrFiscal: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT, VL_DOC, VL_DESC, VL_SERV, VL_BC_ICMS,
  VL_ICMS, VL_NT, ALIQ_ICMS, VL_OPR, VL_RED_BC: Double;
  SQLType: TSQLType;
  //
  DataViagemInvalida: Boolean;
  //
  // ITENS PIS/COFINS
  IND_NAT_FRT, NAT_BC_CRED, CST_PIS, CST_COFINS: String;
  VL_ITEM, VL_BC_PIS, ALIQ_PIS, VL_PIS, VL_BC_COFINS, ALIQ_COFINS, VL_COFINS: Double;
  //
  function ChaveDocNotOK(): Boolean;
  var
    UF_IBGE, AAMM, CNPJ, ModCTe, SerCTe, NumCTe, tpEmis, AleCTe: String;
  begin
    Result := Length(CHV_CTE) <> 44;
    if MyObjects.FIC(Result = True, EdCHV_CTE,
      'Defina a chave de acesso do documento (44 d�gitos)!') then Exit;
    //
    NFeXMLGeren.DesmontaChaveDeAcesso(CHV_CTE, 2.00,
      UF_IBGE, AAMM, CNPJ, ModCTe, SerCTe, NumCTe, tpEmis, AleCTe);
    //
    Result := not (Geral.IMV(ModCTe) in ([55, 57, 65, 67]));
    if MyObjects.FIC(Result = True, EdCHV_CTE,
      'Chave de acesso do documento n�o implementada!: '+ EdCHV_CTE.Text) then ; //Exit;
    //
    Result := Geral.IMV('0' + COD_MOD) <> Geral.IMV('0' + ModCTe);
    if MyObjects.FIC(Result, EdCOD_MOD,
      'O modelo do documento n�o confere com a chave!') then Exit;
    //
    Result := SER <> SerCTe;
    if MyObjects.FIC(Result, EdSER,
      'A s�ire do documento n�o confere com a chave!') then Exit;
    //
    Result := NUM_DOC <> Geral.IMV(NumCTe);
    if MyObjects.FIC(Result, EdNUM_DOC,
      'O n�mero do CT do documento n�o confere com a chave!') then Exit;
  end;
  //
var
  Fic1, Fic2: Boolean;
begin
  //
  //IsLinked       := ;
  //SqLinked       := ;
  Terceiro       := EdTransportador.ValueVariant;
  CliInt         := EdCliInt.ValueVariant;
  //
  SQLType        := ImgTipo.SQLType;
  MovFatID       := EdMovFatID.ValueVariant;
  MovFatNum      := EdMovFatNum.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  Controle       := EdControle.ValueVariant;
  FControle      := EdControle.ValueVariant;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  Motorista      := EdMotorista.ValueVariant;
  IND_OPER       := '0'; // Aquisi��o
  IND_EMIT       := '1'; // Terceiros
  COD_MOD        := EdCOD_MOD.ValueVariant;
  COD_SIT        := EdCOD_SIT.ValueVariant;
  SER            := EdSER.ValueVariant;
  SUB            := EdSUB.ValueVariant;
  NUM_DOC        := EdNUM_DOC.ValueVariant;
  CHV_CTE        := EdCHV_CTE.ValueVariant;
  CTeStatus      := 100;
  DT_DOC         := Geral.FDT(TPDT_DOC.Date, 1);
  DT_A_P         := Geral.FDT(TPDT_A_P.Date, 1);
  //IND_PGTO       := EdIND_PGTO.ValueVariant;
  IND_FRT        := EdIND_FRT.ValueVariant;
  TP_CT_e        := EdTP_CT_e.ValueVariant;
  CHV_CTE_REF    := '';

(*
  VL_DOC         := EdVL_DOC       .ValueVariant;
  VL_DESC        := EdVL_DESC      .ValueVariant;
  VL_ABAT_NT     := 0.00;  // Abatimento n�o comercial (Ex.: Desconto ICMS nas remessas para ZFM)
  VL_MERC        := EdVL_MERC      .ValueVariant;
  VL_FRT         := EdVL_FRT       .ValueVariant;
  VL_SEG         := EdVL_SEG       .ValueVariant;
  VL_OUT_DA      := EdVL_OUT_DA    .ValueVariant;
  VL_BC_ICMS     := EdVL_BC_ICMS   .ValueVariant;
  VL_ICMS        := EdVL_ICMS      .ValueVariant;
  VL_BC_ICMS_ST  := EdVL_BC_ICMS_ST.ValueVariant;
  VL_ICMS_ST     := EdVL_ICMS_ST   .ValueVariant;
  VL_IPI         := EdVL_IPI       .ValueVariant;
  VL_PIS         := EdVL_PIS       .ValueVariant;
  VL_COFINS      := EdVL_COFINS    .ValueVariant;
  VL_PIS_ST      := EdVL_PIS_ST    .ValueVariant;
  VL_COFINS_ST   := EdVL_COFINS_ST .ValueVariant;
*)
  VL_DOC         := EdVL_DOC.ValueVariant;
  VL_DESC        := EdVL_DESC.ValueVariant;
  IND_FRT        := EdIND_FRT.ValueVariant;
  VL_SERV        := EdVL_SERV.ValueVariant;
  VL_BC_ICMS     := EdVL_BC_ICMS.ValueVariant;
  VL_ICMS        := EdVL_ICMS.ValueVariant;
  VL_NT          := EdVL_NT.ValueVariant;
  COD_INF        := '';
  COD_CTA        := '';
  COD_MUN_ORIG   := EdCOD_MUN_ORIG.ValueVariant;
  COD_MUN_DEST   := EdCOD_MUN_DEST.ValueVariant;
  NFe_FatID      := EdNFe_FatID.ValueVariant;
  NFe_FatNum     := EdNFe_FatNum.ValueVariant;
  NFe_StaLnk     := EdNFe_StaLnk.ValueVariant;
(*
  VSVmcWrn       := EdVSVmcWrn     .ValueVariant;
  VSVmcObs       := EdVSVmcObs     .ValueVariant;
  VSVmcSeq       := EdVSVmcSeq     .ValueVariant;
  VSVmcSta       := EdVSVmcSta     .ValueVariant;
*)
  CST_ICMS       := EdICMS_CST.ValueVariant;
  CFOP           := EdCFOP.ValueVariant;
  ALIQ_ICMS      := EdALIQ_ICMS.ValueVariant;
  VL_OPR         := EdVL_OPR.ValueVariant;
  VL_RED_BC      := EdVL_RED_BC.ValueVariant;;
  COD_OBS        := '';

  NFe_FatID      := EdNFe_FatID.ValueVariant;
  NFe_FatNum     := EdNFe_FatNum.ValueVariant;
  NFe_StaLnk     := EdNFe_StaLnk.ValueVariant;
  //
  RegrFiscal     := EdRegrFiscal.ValueVariant;
  //
  IND_NAT_FRT    := EdIND_NAT_FRT.ValueVariant;
  NAT_BC_CRED    := EdNAT_BC_CRED.ValueVariant;
  CST_PIS        := EdCST_PIS.ValueVariant;
  CST_COFINS     := EdCST_COFINS.ValueVariant;
  VL_ITEM        := VL_DOC;
  VL_BC_PIS      := EdVL_BC_PIS.ValueVariant;
  ALIQ_PIS       := EdALIQ_PIS.ValueVariant;
  VL_PIS         := EdVL_PIS.ValueVariant;
  VL_BC_COFINS   := EdVL_BC_COFINS.ValueVariant;
  ALIQ_COFINS    := EdALIQ_COFINS.ValueVariant;
  VL_COFINS      := EdVL_COFINS.ValueVariant;
  //

  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(CliInt = 0, nil, 'Cliente interno n�o definido!') then Exit;
  //
  if MyObjects.FIC(TPDT_A_P.Date < 2, TPDT_A_P, 'Defina a data de aquisi��o!') then Exit;
  if MyObjects.FIC(TPDT_DOC.Date < 2, TPDT_DOC, 'Defina a data da emiss�o do documento!') then Exit;
  DataViagemInvalida := (Trunc(TPDT_DOC.Date) > Trunc(TPDT_A_P.Date));
  if MyObjects.FIC(DataViagemInvalida, TPDT_DOC,
    'A data de viagem n�o pode ser menor que a data da emiss�o do documento!') then Exit;
  //
  if ChaveDocNotOK() then ; //Exit;
  //
(*
  if MyObjects.FIC(Pecas = 0, EdPecas, 'Informe a quantidade de pe�as!') then
    Exit;
  if MyObjects.FIC(PesoKg = 0, EdPesoKg, 'Informe o peso (kg)!') then
    Exit;
*)
  //
  if MyObjects.FIC(Terceiro = 0, EdTransportador, 'Defina um transportador!') then Exit;
  //
  //if ItemDeListaSpedNaoDefinido(EdIND_PGTO.ValueVariant, EdIND_PGTO_TXT.Text,
    //'Informe o Indicador do tipo de pagamento', F_IND_PGTO_EFD) then Exit;
  if ItemDeListaSpedNaoDefinido(EdIND_FRT.ValueVariant, EdIND_FRT_TXT.Text,
    'Informe o Indicador do tipo de frete', F_IND_FRT_EFD) then Exit;
  if ItemDeListaSpedNaoDefinido(EdCOD_SIT.ValueVariant, EdCOD_SIT_TXT.Text,
    'Informe o C�digo da situa��o do documento fiscal', F_COD_SIT_EFD) then Exit;
  if ItemDeListaSpedNaoDefinido(EdTP_CT_e.ValueVariant, EdTP_CT_e_TXT.Text,
    'Informe o Tipo de conhecimento de transporte', F_TP_CT_e) then Exit;
  //
  if MyObjects.FIC(COD_MUN_ORIG < 1000000, EdCOD_MUN_ORIG, 'Informe a cidade de origem!') then Exit;
  if MyObjects.FIC(COD_MUN_DEST < 1000000, EdCOD_MUN_DEST, 'Informe a cidade de destino!') then Exit;
  Fic1 := (COD_MOD = '57') or (COD_MOD = '63') or (COD_MOD = '67');
  Fic2 := Length(CHV_CTE) < 44;
  if MyObjects.FIC(Fic1 and Fic2, nil, 'Informe a chave CT-e!') then Exit;
  //if not VS_CRC_PF.ValidaCampoNF(5, Edide_nNF, True) then Exit;
  if DModG.ExigePisCofins() then
  begin
     if EdVL_PIS.ValueVariant >= 0.01 then
       if MyObjects.FIC(Length(EdCST_PIS.Text) <> 2, EdCST_PIS,
       'Informe o CST do PIS com dois d�gitos!') then Exit;
     //
     if EdVL_COFINS.ValueVariant >= 0.01 then
       if MyObjects.FIC(Length(EdCST_COFINS.Text) <> 2, EdCST_COFINS,
       'Informe o CST da COFINS com dois d�gitos!') then Exit;
  end;
  //
  if MyObjects.FIC(RegrFiscal = 0, EdRegrFiscal, 'Informe a movimenta��o (Regra Fiscal)!') then
    Exit;
  //
  FControle := UMyMod.BPGS1I32('efdinnctscab', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdinnctscab', False, [
  'MovFatID', 'MovFatNum', 'MovimCod',
  'Empresa', (*'IsLinked', 'SqLinked',*)
  'CliInt', 'Terceiro', 'Pecas',
  'PesoKg', 'AreaM2', 'AreaP2',
  'ValorT', 'Motorista', 'Placa',
  'IND_OPER', 'IND_EMIT', 'COD_MOD',
  'COD_SIT', 'SER', 'SUB',
  'NUM_DOC', 'CHV_CTE', 'CTeStatus',
  'DT_DOC', 'DT_A_P', 'TP_CT_e',
  'CHV_CTE_REF', 'VL_DOC', 'VL_DESC',
  'IND_FRT', 'VL_SERV', 'VL_BC_ICMS',
  'VL_ICMS', 'VL_NT', 'COD_INF',
  'COD_CTA', 'COD_MUN_ORIG', 'COD_MUN_DEST',
  'NFe_FatID', 'NFe_FatNum', 'NFe_StaLnk',
  (*'VSVmcWrn', 'VSVmcObs', 'VSVmcSeq',
  'VSVmcSta',*) 'CST_ICMS', 'CFOP',
  'ALIQ_ICMS', 'VL_OPR', 'VL_RED_BC',
  'COD_OBS', 'RegrFiscal',
  'IND_NAT_FRT',
  'VL_ITEM', 'CST_PIS', 'NAT_BC_CRED',
  'VL_BC_PIS', 'ALIQ_PIS', 'VL_PIS',
  'CST_COFINS', 'VL_BC_COFINS', 'ALIQ_COFINS',
  'VL_COFINS'], [
  'Controle'], [
  MovFatID, MovFatNum, MovimCod,
  Empresa, (*IsLinked, SqLinked,*)
  CliInt, Terceiro, Pecas,
  PesoKg, AreaM2, AreaP2,
  ValorT, Motorista, Placa,
  IND_OPER, IND_EMIT, COD_MOD,
  COD_SIT, SER, SUB,
  NUM_DOC, CHV_CTE, CTeStatus,
  DT_DOC, DT_A_P, TP_CT_e,
  CHV_CTE_REF, VL_DOC, VL_DESC,
  IND_FRT, VL_SERV, VL_BC_ICMS,
  VL_ICMS, VL_NT, COD_INF,
  COD_CTA, COD_MUN_ORIG, COD_MUN_DEST,
  NFe_FatID, NFe_FatNum, NFe_StaLnk,
  (*VSVmcWrn, VSVmcObs, VSVmcSeq,
  VSVmcSta,*) CST_ICMS, CFOP,
  ALIQ_ICMS, VL_OPR, VL_RED_BC,
  COD_OBS, RegrFiscal,
  IND_NAT_FRT,
  VL_ITEM, CST_PIS, NAT_BC_CRED,
  VL_BC_PIS, ALIQ_PIS, VL_PIS,
  CST_COFINS, VL_BC_COFINS, ALIQ_COFINS,
  VL_COFINS], [
  FControle], True) then
  begin
(*
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
    'AtrelaFatID', 'AtrelaFatNum', 'AtrelaStaLnk'], [
    'FatID', 'FatNum', 'Empresa'], [
    MovFatID, MovFatNum, NFe_StaLnk], [
    NFe_FatID, NFe_FatNum, Empresa], False);
*)
    Close;
  end;
end;

{
procedure TFmEfdInnCTsCab.BtOKClick(Sender: TObject);
var
  Placa: String;
  MovFatID, MovFatNum, Controle, Motorista: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  SQLType: TSQLType;
var
  CHV_NFE, DT_DOC, DT_E_S, IND_PGTO, IND_FRT(*, VSVmcObs, VSVmcSeq*): String;
  MovimCod, Empresa, COD_MOD, COD_SIT, SER, NUM_DOC, NFeStatus, NFe_FatID, NFe_FatNum,
  NFe_StaLnk(*, VSVmcWrn, VSVmcSta*), Terceiro, CliInt: Integer;
  (*
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST: Double;
  *)
  //
  DataChegadaInvalida: Boolean;

  //
  function ChaveDocNotOK(): Boolean;
  var
    UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe: String;
  begin
    Result := Length(CHV_NFE) <> 44;
    if MyObjects.FIC(Result = True, EdCHV_NFE,
      'Defina a chave de acesso do documento!') then Exit;
    //
    NFeXMLGeren.DesmontaChaveDeAcesso(CHV_NFE, 2.00,
      UF_IBGE, AAMM, CNPJ, ModNFe, SerNFe, NumNFe, tpEmis, AleNFe);
    //
    Result := not (Geral.IMV(ModNFe) in ([55, 57, 65, 67]));
    if MyObjects.FIC(Result = True, EdCHV_NFE,
      'Chave de acesso do documento n�o implementada!: '+ EdCHV_NFE.Text) then ; //Exit;
    //
    Result := COD_MOD <> Geral.IMV(ModNFe);
    if MyObjects.FIC(Result, EdCOD_MOD,
      'O modelo do documento n�o confere com a chave!') then Exit;
    //
    Result := SER <> Geral.IMV(SerNFe);
    if MyObjects.FIC(Result, EdSER,
      'A s�ire do documento n�o confere com a chave!') then Exit;
    //
    Result := NUM_DOC <> Geral.IMV(NumNFe);
    if MyObjects.FIC(Result, EdNUM_DOC,
      'O n�mero da NF do documento n�o confere com a chave!') then Exit;
  end;
  //
begin
  //
  SQLType        := ImgTipo.SQLType;
  MovFatID       := EdMovFatID.ValueVariant;
  MovFatNum      := EdMovFatNum.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  Controle       := EdControle.ValueVariant;
  FControle      := EdControle.ValueVariant;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  Motorista      := EdMotorista.ValueVariant;
  Placa          := EdPlaca.ValueVariant;
  COD_MOD        := EdCOD_MOD.ValueVariant;
  COD_SIT        := EdCOD_SIT.ValueVariant;
  SER            := EdSER.ValueVariant;
  NUM_DOC        := EdNUM_DOC.ValueVariant;
  CHV_NFE        := EdCHV_NFE      .ValueVariant;
  NFeStatus      := 100;
  DT_DOC         := Geral.FDT(TPDT_DOC.Date, 1);
  DT_E_S         := Geral.FDT(TPDT_E_S.Date, 1);
  IND_PGTO       := EdIND_PGTO     .ValueVariant;
  IND_FRT        := EdIND_FRT      .ValueVariant;
(*
  VL_DOC         := EdVL_DOC       .ValueVariant;
  VL_DESC        := EdVL_DESC      .ValueVariant;
  VL_ABAT_NT     := 0.00;  // Abatimento n�o comercial (Ex.: Desconto ICMS nas remessas para ZFM)
  VL_MERC        := EdVL_MERC      .ValueVariant;
  VL_FRT         := EdVL_FRT       .ValueVariant;
  VL_SEG         := EdVL_SEG       .ValueVariant;
  VL_OUT_DA      := EdVL_OUT_DA    .ValueVariant;
  VL_BC_ICMS     := EdVL_BC_ICMS   .ValueVariant;
  VL_ICMS        := EdVL_ICMS      .ValueVariant;
  VL_BC_ICMS_ST  := EdVL_BC_ICMS_ST.ValueVariant;
  VL_ICMS_ST     := EdVL_ICMS_ST   .ValueVariant;
  VL_IPI         := EdVL_IPI       .ValueVariant;
  VL_PIS         := EdVL_PIS       .ValueVariant;
  VL_COFINS      := EdVL_COFINS    .ValueVariant;
  VL_PIS_ST      := EdVL_PIS_ST    .ValueVariant;
  VL_COFINS_ST   := EdVL_COFINS_ST .ValueVariant;
*)
  NFe_FatID          := EdNFe_FatID        .ValueVariant;
  NFe_FatNum         := EdNFe_FatNum       .ValueVariant;
  NFe_StaLnk         := EdNFe_StaLnk       .ValueVariant;
(*
  VSVmcWrn       := EdVSVmcWrn     .ValueVariant;
  VSVmcObs       := EdVSVmcObs     .ValueVariant;
  VSVmcSeq       := EdVSVmcSeq     .ValueVariant;
  VSVmcSta       := EdVSVmcSta     .ValueVariant;
*)
  Terceiro := EdFornecedor.ValueVariant;
  CliInt := EdCliInt.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(CliInt = 0, nil, 'Cliente interno n�o definido!') then Exit;
  //
  if MyObjects.FIC(TPDT_E_S.Date < 2, TPDT_E_S, 'Defina a data de entrada!') then Exit;
  if MyObjects.FIC(TPDT_DOC.Date < 2, TPDT_DOC, 'Defina a data da emiss�o do documento!') then Exit;
  DataChegadaInvalida := (Trunc(TPDT_DOC.Date) > Trunc(TPDT_E_S.Date));
  if MyObjects.FIC(DataChegadaInvalida, TPDT_DOC,
    'A data de chegada n�o pode ser menor que a data da emiss�o do documento!') then Exit;
  //
  if ChaveDocNotOK() then ; //Exit;
  //
  if MyObjects.FIC(Pecas = 0, EdPecas, 'Informe a quantidade de pe�as!') then
    Exit;
  if MyObjects.FIC(PesoKg = 0, EdPesoKg, 'Informe o peso (kg)!') then
    Exit;
  //
  if MyObjects.FIC(Terceiro = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
  //
  if ItemDeListaSpedNaoDefinido(EdIND_PGTO.ValueVariant, EdIND_PGTO_TXT.Text,
    'Informe o Indicador do tipo de pagamento', F_IND_PGTO_EFD) then Exit;
  if ItemDeListaSpedNaoDefinido(EdIND_FRT.ValueVariant, EdIND_FRT_TXT.Text,
    'Informe o Indicador do tipo de frete', F_IND_FRT_EFD) then Exit;
  if ItemDeListaSpedNaoDefinido(EdCOD_SIT.ValueVariant, EdCOD_SIT_TXT.Text,
    'Informe o C�digo da situa��o do documento fiscal', F_COD_SIT_EFD) then Exit;
  //
  //if not VS_CRC_PF.ValidaCampoNF(5, Edide_nNF, True) then Exit;
  //
  FControle := UMyMod.BPGS1I32('efdinnnfscab', 'Controle', '', '', tsPos, SQLTYpe, FControle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdinnnfscab', False, [
  'MovFatID', 'MovFatNum', 'MovimCod', 'Empresa',
  'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT',
  'Motorista', 'Placa', 'COD_MOD',
  'COD_SIT', 'SER', 'NUM_DOC',
  'CHV_NFE', 'NFeStatus', 'DT_DOC',
  'DT_E_S', (*'VL_DOC',*) 'IND_PGTO',
  (*'VL_DESC', 'VL_ABAT_NT', 'VL_MERC',*)
  'IND_FRT', (*'VL_FRT', 'VL_SEG',
  'VL_OUT_DA', 'VL_BC_ICMS', 'VL_ICMS',
  'VL_BC_ICMS_ST', 'VL_ICMS_ST', 'VL_IPI',
  'VL_PIS', 'VL_COFINS', 'VL_PIS_ST',
  'VL_COFINS_ST',*) 'NFe_FatID', 'NFe_FatNum',
  'NFe_StaLnk'(*, 'VSVmcWrn', 'VSVmcObs',
  'VSVmcSeq', 'VSVmcSta'*), 'Terceiro', 'CliInt'], [
  'Controle'], [
  MovFatID, MovFatNum, MovimCod, Empresa,
  Pecas, PesoKg,
  AreaM2, AreaP2, ValorT,
  Motorista, Placa, COD_MOD,
  COD_SIT, SER, NUM_DOC,
  CHV_NFE, NFeStatus, DT_DOC,
  DT_E_S, (*VL_DOC,*) IND_PGTO,
  (*VL_DESC, VL_ABAT_NT, VL_MERC,*)
  IND_FRT, (*VL_FRT, VL_SEG,
  VL_OUT_DA, VL_BC_ICMS, VL_ICMS,
  VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI,
  VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST,*) NFe_FatID, NFe_FatNum,
  NFe_StaLnk(*, VSVmcWrn, VSVmcObs,
  VSVmcSeq, VSVmcSta*), Terceiro, CliInt], [
  FControle], True) then
  begin
    //if (Empresa = CI) and (NFVP_FatID <> 0) and (NFVP_FatNum <> 0) (*and (NFVP_FatNum <> 0)*) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
      'AtrelaFatID', 'AtrelaFatNum', 'AtrelaStaLnk'], [
      'FatID', 'FatNum', 'Empresa'], [
      MovFatID, MovFatNum, NFe_StaLnk], [
      NFe_FatID, NFe_FatNum, Empresa], False);
    end;
    Close;
  end;
end;
}

procedure TFmEfdInnCTsCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdInnCTsCab.CalculaRetornoImpostos(Qual: TRetornImpost);
const
  sProcName = 'FmPQEIts.CalculaRetornoImpostos()';
var
  pICMS, pPIS, pCofins, vICMS, vPIS, vCofins, ICMS_vBC,
  PISS_vBC, COFINS_vBC, _VL_OPR, AjusteVL_OUTROS, VL_ICMS_ST: Double;
  AjusteVL_OPR: Double;
begin
  //VlrItem   := EdVlrItem.ValueVariant;

  ICMS_vBC   := EdVL_BC_ICMS.ValueVariant;
  PISS_vBC   := EdVL_BC_PIS.ValueVariant;
  COFINS_vBC := EdVL_BC_COFINS.ValueVariant;

  pICMS     := EdALIQ_ICMS.ValueVariant;
  pPIS      := EdALIQ_PIS.ValueVariant;
  pCofins   := EdALIQ_COFINS.ValueVariant;
  vICMS     := Geral.RoundC(ICMS_vBC * pICMS / 100, 2);
  vPIS      := Geral.RoundC(PISS_vBC * pPIS / 100, 2);
  vCofins   := Geral.RoundC(COFINS_vBC * pCOFINS / 100, 2);
  case Qual of
    //retimpostIndef=1,
    //retimpostNenhum: Exit;
    retimpostTodos:
    begin
      EdVL_ICMS.ValueVariant := vICMS;
      EdVL_PIS.ValueVariant := vPIS;
      EdVL_COFINS.ValueVariant := vCOFINS;
    end;
    retimpostICMS: EdVL_ICMS.ValueVariant := vICMS;
    retimpostPIS: EdVL_PIS.ValueVariant := vPIS;
    retimpostCOFINS: EdVL_COFINS.ValueVariant := vCOFINS;
    //retimpostIPI=7);
    else Geral.MB_Erro('TRetornImpost n�o implementado em ' + sProcName);
  end;
  //

(*  Parei aqui!
  AjusteVL_OUTROS := SPED_PF.FormulaVL_OUTROS(_VL_OPR, ICMS_vBC, VL_ICMS_ST,
  VL_RED_BC, IPI_vIPI);
*)

end;

procedure TFmEfdInnCTsCab.CalculaValorTotalDocumento(Sender: TObject);
var
  VL_SERV, VL_DESC, VL_NT, VL_RED_BC, VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_OPR,
  VL_DOC: Double;
begin
  VL_SERV    := EdVL_SERV.ValueVariant;
  VL_DESC    := EdVL_DESC.ValueVariant;
  VL_NT      := EdVL_NT.ValueVariant;
  VL_RED_BC  := EdVL_RED_BC.ValueVariant;
  //
  if TdmkEdit(Sender) = EdVL_BC_ICMS then
    VL_BC_ICMS := EdVL_BC_ICMS.ValueVariant
  else
    VL_BC_ICMS := VL_SERV - VL_DESC - VL_NT - VL_RED_BC;
  //
  ALIQ_ICMS  := EdALIQ_ICMS.ValueVariant;
  //
  if TdmkEdit(Sender) = EdVL_ICMS then
    VL_ICMS := EdVL_BC_ICMS.ValueVariant
  else
    VL_ICMS := Geral.RoundC(VL_BC_ICMS * ALIQ_ICMS / 100, 2);
  //
  if TdmkEdit(Sender) = EdVL_OPR then
    VL_OPR := EdVL_OPR.ValueVariant
  else
    VL_OPR := VL_SERV - VL_DESC - VL_NT;
  //
(*????*)
  if TdmkEdit(Sender) = EdVL_DOC then
    VL_DOC := EdVL_DOC.ValueVariant
  else
    VL_DOC := VL_SERV - VL_DESC - VL_NT;
  //
(**)
  //////////////////////////////////////////////////////////////////////////////
  ///
  if TdmkEdit(Sender) <> EdVL_BC_ICMS then
    EdVL_BC_ICMS.ValueVariant := VL_BC_ICMS;
  //
  if TdmkEdit(Sender) <> EdVL_ICMS then
    EdVL_ICMS.ValueVariant := VL_ICMS;
  //
  if TdmkEdit(Sender) <> EdVL_OPR then
    EdVL_OPR.ValueVariant := VL_OPR;
  //
(*
  if TdmkEdit(Sender) <> EdVL_DOC then
    EdVL_DOC.ValueVariant := VL_DOC;
*)
  //
end;

procedure TFmEfdInnCTsCab.CkTipoNFClick(Sender: TObject);
var
  EhCTe: Boolean;
begin
  EhCTe := CkTipoNF.Checked;
  //
  LaCHV_CTE.Enabled := EhCTe;
  EdCHV_CTE.Enabled := EhCTe;
  //
  LaCOD_MOD.Enabled := not EhCTe;
  EdCOD_MOD.Enabled := not EhCTe;
  LaSer.Enabled     := not EhCTe;
  EdSER.Enabled     := not EhCTe;
  LaNUM_DOC.Enabled := not EhCTe;
  EdNUM_DOC.Enabled := not EhCTe;
end;

procedure TFmEfdInnCTsCab.EdALIQ_COFINSChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostCOFINS);
end;

procedure TFmEfdInnCTsCab.EdALIQ_PISChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostPIS);
end;

procedure TFmEfdInnCTsCab.EdCOD_SITChange(Sender: TObject);
begin
  EdCOD_SIT_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCOD_SIT_EFD,  EdCOD_SIT.ValueVariant);
end;

procedure TFmEfdInnCTsCab.EdCOD_SITKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdCOD_SIT.Text := Geral.SelecionaItem(F_COD_SIT_EFD, 0,
      'SEL-LISTA-000 :: Situa��o do Documento Fiscal',
      TitCols, Screen.Width)
  end;
end;

procedure TFmEfdInnCTsCab.EdCST_COFINSChange(Sender: TObject);
begin
  EdCST_COFINS_TXT.Text := UFinanceiro.CST_COFINS_Get(EdCST_COFINS.Text);
end;

procedure TFmEfdInnCTsCab.EdCST_COFINSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCST_COFINS.Text := UFinanceiro.ListaDeCST_COFINS();
end;

procedure TFmEfdInnCTsCab.EdCST_PISChange(Sender: TObject);
begin
  EdCST_PIS_TXT.Text := UFinanceiro.CST_PIS_Get(EdCST_PIS.Text);
end;

procedure TFmEfdInnCTsCab.EdCST_PISKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCST_PIS.Text := UFinanceiro.ListaDeCST_PIS();
end;

procedure TFmEfdInnCTsCab.EdEmpresaChange(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCI, Dmod.MyDB, [
  'SELECT Codigo, Tipo, CNPJ, CPF,',
  'IF(Tipo=0, RazaoSocial, Nome) NOME',
  'FROM entidades',
  'WHERE Codigo=' + Geral.FF0(EdEmpresa.ValueVariant),
  'ORDER BY Nome',
  '']);
end;

procedure TFmEfdInnCTsCab.EdICMS_CSTChange(Sender: TObject);
begin
  EdUCTextoB.Text := DmNFe_0000.ObtemDescricao_TabGov_1(EdICMS_CST.ValueVariant,
    CO_NOME_tbspedefd_ICMS_CST, TPDT_A_P.Date);
end;

procedure TFmEfdInnCTsCab.EdICMS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', CO_NOME_tbspedefd_ICMS_CST, DModG.AllID_DB,
    ''(*Extra*), EdICMS_CST, (*CBICMS_CST*)nil, dmktfInteger);
(*
  if Key = VK_F3 then
    EdICMS_CST.Text := UFinanceiro.ListaDeTributacaoPeloICMS();
*)
(*if Key = VK_F3 then
    EdCSOSN.Text := UFinanceiro.ListaDeCSOSN();*)
end;

procedure TFmEfdInnCTsCab.EdIND_FRTChange(Sender: TObject);
begin
  EdIND_FRT_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_FRT_EFD,  EdIND_FRT.ValueVariant);
end;

procedure TFmEfdInnCTsCab.EdIND_FRTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_FRT.Text := Geral.SelecionaItem(F_IND_FRT_EFD, 0,
      'SEL-LISTA-000 :: Indicador do Tipo de Frete',
      TitCols, Screen.Width)
  end;
end;

procedure TFmEfdInnCTsCab.EdIND_NAT_FRTChange(Sender: TObject);
begin
  //EdIND_NAT_FRT_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_Txt(spedIND_NAT_FRT,  EdIND_NAT_FRT.ValueVariant);
  EdIND_NAT_FRT_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedIND_NAT_FRT,
    EdIND_NAT_FRT.ValueVariant, F_IND_NAT_FRT);
  //
end;

procedure TFmEfdInnCTsCab.EdIND_NAT_FRTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_NAT_FRT.Text := Geral.SelecionaItem(F_IND_NAT_FRT, 0,
      'SEL-LISTA-000 :: Indicador do tipo de pagamento',
      TitCols, Screen.Width)
  end;
end;

procedure TFmEfdInnCTsCab.EdNAT_BC_CREDChange(Sender: TObject);
var
  NAT_BC_CRED: Byte;
begin
  EdNAT_BC_CRED_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedNAT_BC_CRED,
    EdNAT_BC_CRED.ValueVariant, F_NAT_BC_CRED);
  //
end;

procedure TFmEfdInnCTsCab.EdNAT_BC_CREDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdNAT_BC_CRED.Text := Geral.SelecionaItem(F_NAT_BC_CRED, 0,
      'SEL-LISTA-000 :: Indicador do tipo de pagamento',
      TitCols, Screen.Width)
  end;
end;

procedure TFmEfdInnCTsCab.EdRegrFiscalRedefinido(Sender: TObject);
begin
  SetaParametrosDaRegraFiscal();
end;

procedure TFmEfdInnCTsCab.EdTP_CT_eChange(Sender: TObject);
begin
  EdTP_CT_e_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeTP_CT_e,  EdTP_CT_e.ValueVariant);
end;

procedure TFmEfdInnCTsCab.EdTP_CT_eKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdTP_CT_e.Text := Geral.SelecionaItem(F_TP_CT_e, 0,
      'SEL-LISTA-000 :: Tipo de conhecimento de transporte',
      TitCols, Screen.Width)
  end;
end;

procedure TFmEfdInnCTsCab.EdValorTRedefinido(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    EdVL_SERV.ValueVariant := EdValorT.ValueVariant;
    EdVL_DOC.ValueVariant := EdValorT.ValueVariant;
  end;
end;

procedure TFmEfdInnCTsCab.EdVL_BC_COFINSChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostCOFINS);
end;

procedure TFmEfdInnCTsCab.EdVL_BC_COFINSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Aliquota: Double;
begin
  if Key = VK_F4 then
  begin
    EdVL_BC_COFINS.ValueVariant := EdVL_OPR.ValueVariant;
    if SPED_Geral_PF.DefineAliquotaCOFINSPeloRegimeTributario(Aliquota) = True then
      EdALIQ_COFINS.ValueVariant := Aliquota;
  end;
end;

procedure TFmEfdInnCTsCab.EdVL_BC_PISChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostPIS);
end;

procedure TFmEfdInnCTsCab.EdVL_BC_PISKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Aliquota: Double;
begin
  if Key = VK_F4 then
  begin
    EdVL_BC_PIS.ValueVariant := EdVL_OPR.ValueVariant;
    if SPED_Geral_PF.DefineAliquotaPISPeloRegimeTributario(Aliquota) = True then
      EdALIQ_PIS.ValueVariant := Aliquota;
  end;
end;

procedure TFmEfdInnCTsCab.EdVL_COFINSChange(Sender: TObject);
begin
  RedefineIndicadorDaNaturezaDoFrete();
end;

procedure TFmEfdInnCTsCab.EdVL_NTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdVL_NT.ValueVariant := EdVL_SERV.ValueVariant - EdVL_DESC.ValueVariant;
end;

procedure TFmEfdInnCTsCab.EdVL_OPRChange(Sender: TObject);
begin
  SetaBasesDeCalculo();
end;

procedure TFmEfdInnCTsCab.EdVL_PISChange(Sender: TObject);
begin
  RedefineIndicadorDaNaturezaDoFrete();
end;

procedure TFmEfdInnCTsCab.EdVL_RED_BCKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdVL_BC_ICMS.ValueVariant := EdVL_OPR.ValueVariant;
end;

procedure TFmEfdInnCTsCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdInnCTsCab.FormCreate(Sender: TObject);
begin
  FControle := 0;
  ImgTipo.SQLType := stLok;
  //
  F_IND_PGTO_EFD := UnNFe_PF.ListaIND_PAG_EFD();
  F_IND_FRT_EFD  := UnNFe_PF.ListaIND_FRT_EFD();
  F_COD_SIT_EFD  := UnNFe_PF.ListaCOD_SIT_EFD();
  F_TP_CT_e      := UnNFe_PF.ListaTP_CT_e_EFD();
  F_NAT_BC_CRED  := UnNFe_PF.ListaNAT_BC_CRED();
  F_IND_NAT_FRT  := UnNFe_PF.ListaIND_NAT_FRT();
  //
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransportadores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
  ReopenMunici(QrMuniciOrig, '');
  ReopenMunici(QrMuniciDest, '');
  ReopenCFOP();
  //
  EdCOD_MUN_ORIG.ValueVariant := DModG.QrEmpresasCodMunici.Value;
  EdCOD_MUN_DEST.ValueVariant := DModG.QrEmpresasCodMunici.Value;
  //
  EdTP_CT_e_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeTP_CT_e,  EdTP_CT_e.ValueVariant);
  //
  EdUCTextoB.Text := DmNFe_0000.ObtemDescricao_TabGov_1(EdICMS_CST.ValueVariant,
    CO_NOME_tbspedefd_ICMS_CST, TPDT_A_P.Date);
  EdCST_PIS_TXT.Text := UFinanceiro.CST_PIS_Get(EdCST_PIS.Text);
  EdCST_COFINS_TXT.Text := UFinanceiro.CST_COFINS_Get(EdCST_COFINS.Text);
  //
  EdCFOP.ValueVariant := 2352;
  //CBCFOP.KeyValue     := 2352;
  EdIND_FRT.ValueVariant := 1; // frete por conta do destinatario
end;

procedure TFmEfdInnCTsCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdInnCTsCab.FormShow(Sender: TObject);
begin
  EdCOD_SIT_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCOD_SIT_EFD,  EdCOD_SIT.ValueVariant);
  EdIND_FRT_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_FRT_EFD,  EdIND_FRT.ValueVariant);
  EdTP_CT_e_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeTP_CT_e,  EdTP_CT_e.ValueVariant);
  //
  EdUCTextoB.Text := DmNFe_0000.ObtemDescricao_TabGov_1(EdICMS_CST.ValueVariant,
    CO_NOME_tbspedefd_ICMS_CST, TPDT_A_P.Date);
end;

function TFmEfdInnCTsCab.ItemDeListaSpedNaoDefinido(Codigo: Integer;
  Texto, Mensagem: String; Lista: MyArrayLista): Boolean;
begin
  Result := (Codigo < 0) or (Texto = EmptyStr);
  if Result = True then
    Geral.MB_Aviso('Informe o campo: "' + Mensagem + '"')
  else
  if UnNFe_PF.ListaContemplaCodigoSelecionado(Codigo, Lista) = False then
  begin
    Result := True;
    Geral.MB_Aviso('Aten��o! O valor informado para o campo "' + Mensagem +
    ' n�o foi reconhecido pelo ERP como v�lido!');
  end;
end;

procedure TFmEfdInnCTsCab.RedefineIndicadorDaNaturezaDoFrete;
begin
  if ImgTipo.SQLType = stIns then
  begin
    if (EdVL_PIS.ValueVariant >= 0.01) or (EdVL_COFINS.ValueVariant >= 0.01) then
      EdIND_NAT_FRT.ValueVariant := '2'
    else
      EdIND_NAT_FRT.ValueVariant := '3'
  end;

(*
Se voc� esta lan�ando um conhecimento de transporte no programa Manuten��o de Notas Fiscais de Entrada (FREC0200), n�o ser� poss�vel escolher o indicador de natureza de frete utilizado. Pois o sistema quem realiza e grava estes indicadores, conforme o lan�amento do CTe.
Abaixo seguem alguns exemplos do tratamento do sistema em rela��o ao INDICADOR DE NATUREZA DE FRETE:
EXEMPLO:
Nota de Conhecimento de Frete de Nro. 1000, originado de uma nota fiscal de SA�DA de Nro 2000;
Se o tipo de nota (Nro.2000) for 'Venda':
- Se o tipo de nota do Conhec.Frete (Nro.2000) Gera Receita, Indicador = 0;
- Se o tipo de nota do Conhec.Frete (Nro.2000) n�o Gera Receita, Indicador = 1;
- Se o tipo de nota (Nro.2000) for Transfer�ncia e for entre mesma raiz de CNPJ:
    Se Item da nota (Nro.2000) verificar no Cadastro do Item, na Pasta Cont�bil, se o grupo do item (Tipo do Item - SRF) pertence a produto acabado, ent�o Indicador = 4;
    Se item da nota (Nro.2000) verificar no Cadastro do Item, na Pasta Cont�bil, se o grupo do item pertence a produto em elabora��o, ent�o Indicador = 5 sen�o = 9 (Ex: mat�ria prima);
- Se o tipo de nota de sa�da (2000) for diferente dos acima, Indicador = 9

Nota de Conhecimento de Frete de Nro. 1000 originado de uma nota de ENTRADA de Nro 3000:
Se o tipo de nota (Nro.3000) for:
Compra;
Prest.de Servi�os
Prest.de Servi�os Insumos e o CFOP for Industria e Com�rcio
- Se o tipo de nota do Conhec.Frete (Nro.3000), se Calcula PIS/COFINS, Indicador = 2;
- Se o tipo de nota do Conhec.Frete (Nro.3000), se n�o Calcula PIS/COFINS, Indicador = 3;
- Se o tipo de nota (Nro.3000) for transfer�ncia e for entre mesma raiz de CNPJ:
    Se Item da nota (Nro.3000) e verificar no Cadastro do Item, na Pasta Cont�bil, se o grupo do item pertence a produto acabado, ent�o Indicador = 4;
    Se item da nota (Nro.3000) e verificar no Cadastro do Item, na Pasta Cont�bil, se o grupo do item pertence a produto em elabora��o, ent�o Indicador = 5;

- Se o tipo de nota de entrada (3000) for diferente dos acima, Indicador = 9.
O aviso gerado pelo validador do SPED PIS COFINS � que somente os indicadores 0,2,9 podem creditar pis e cofins no CTE (Registro D100 - D101 e D105).
Outras formas de buscar este artigo:
-Como informar o indicador de natureza de frete ao lan�ar o CTE
*)

end;

procedure TFmEfdInnCTsCab.ReopenCFOP;
begin
  //WHERE Codigo=CONCAT(SUBSTRING(1234, 1, 1), '.', SUBSTRING(1234, 2, 4))
  UnDmkDAC_PF.AbreMySQLQuery0(QrCFOP, Dmod.MyDB, [
  'SELECT CAST(CONCAT(SUBSTRING(cfp.Codigo, 1, 1), SUBSTRING(cfp.Codigo, 3, 3)) AS SIGNED) Codigo,',
  'cfp.Nome',
  'FROM cfop2003 cfp',
  'ORDER BY cfp.Nome',
  '']);
end;

procedure TFmEfdInnCTsCab.ReopenMunici(Query: TmySQLQuery; UF: String);
var
  SQL: String;
begin
  SQL := '';
  if Length(UF) <> 0 then
    SQL := 'WHERE LEFT(mun.Codigo, 2)="' +
      Geral.FF0(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UF)) + '"';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.AllID_DB, [
  'SELECT mun.Codigo, mun.Nome ',
  'FROM dtb_munici mun ',
  SQL,
  'ORDER BY mun.Nome ',
  '']);
end;

procedure TFmEfdInnCTsCab.SbCFOPClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormCFOP2003();
  if VAR_CADASTRO <> 0 then
  begin
    ReopenCFOP();
    UMyMod.SetaCodigoPesquisado(EdCFOP, CBCFOP, QrCFOP, VAR_CADASTRO);
  end;
end;

procedure TFmEfdInnCTsCab.SbChaveClick(Sender: TObject);
var
  CHV_CTE, UF_IBGE, AAMM, CNPJ, ModCTe, SerCTe, NumCTe, tpEmis, AleCTe: String;
  Continua: Boolean;
begin
  CHV_CTE := EdCHV_CTE.ValueVariant;
  Continua := Length(CHV_CTE) <> 44;
  if MyObjects.FIC(Continua = True, EdCHV_CTE,
    'Defina a chave de acesso do documento!') then Exit;
  //
  NFeXMLGeren.DesmontaChaveDeAcesso(CHV_CTE, 2.00,
    UF_IBGE, AAMM, CNPJ, ModCTe, SerCTe, NumCTe, tpEmis, AleCTe);

  //
(*
  Continua := ModNFe <> '55';
  if MyObjects.FIC(Continua = True, EdCHV_NFE,
    'Chave de acesso do documento n�o implementada!: '+ EdCHV_NFE.Text) then Exit;
  //
  Continua := COD_MOD <> Geral.IMV(ModNFe);
  if MyObjects.FIC(Continua, EdCOD_MOD,
    'O modelo do documento n�o confere com a chave!') then Exit;
  //
  Continua := SER <> Geral.IMV(SerNFe);
  if MyObjects.FIC(Continua, EdSER,
    'A s�ire do documento n�o confere com a chave!') then Exit;
  //
  Continua := NUM_DOC <> Geral.IMV(NumNFe);
  if MyObjects.FIC(Continua, EdNUM_DOC,
    'O n�mero da NF do documento n�o confere com a chave!') then Exit;
*)
  EdCOD_MOD.ValueVariant := ModCTe;
  EdSER.ValueVariant     := SerCTe;
  EdNUM_DOC.ValueVariant := NumCTe;
  //
(*
  SbNF_VPClick(Self);
*)
end;

procedure TFmEfdInnCTsCab.SBMotoristaClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
  UnDmkDAC_PF.AbreQuery(QrMotorista, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdMotorista.Text := IntToStr(VAR_ENTIDADE);
    CBMotorista.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmEfdInnCTsCab.SbNF_VPClick(Sender: TObject);
(*
var
  Empresa, CI, Emitente, ide_mod, ide_Serie, ide_nNF: Integer;
  SQL_CNPJ_CPF_Emit, SQL_CNPJ_CPF_Dest: String;
  Continua: Boolean;
*)
begin
(*
  Continua := True;
  //
  EdNFe_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceIndefinido);
  EdNFe_FatID.ValueVariant   := 0;
  EdNFe_FatNum.ValueVariant  := 0;
  //
  if not DModG.ObtemEmpresaSelecionada(TdmkEditCB(EdEmpresa), Empresa) then Exit;
  Emitente := EdFornecedor.ValueVariant;
  //
  if Continua then
  begin
    case QrFornecedoresTipo.Value of
      0: SQL_CNPJ_CPF_Emit := 'AND emit_CNPJ="' + Geral.SoNumero_TT(QrFornecedoresCNPJ.Value) + '"';
      1: SQL_CNPJ_CPF_Emit := 'AND emit_CPF="' + Geral.SoNumero_TT(QrFornecedoresCPF.Value) + '"';
      else SQL_CNPJ_CPF_Emit := 'AND emit_CPF_CPF=????';
    end;
    //
    case QrCITipo.Value of
      0: SQL_CNPJ_CPF_Dest := 'AND dest_CNPJ="' + Geral.SoNumero_TT(QrCICNPJ.Value) + '"';
      1: SQL_CNPJ_CPF_Dest := 'AND dest_CPF="' + Geral.SoNumero_TT(QrCICPF.Value) + '"';
      else SQL_CNPJ_CPF_Dest := 'AND dest_CPF_CPF=????';
    end;
    //
    if MyObjects.FIC(Emitente = 0, EdFornecedor, 'Defina um fornecedor!') then Exit;
    //if MyObjects.FIC(CI = 0, EdCI, 'Informe o cliente interno') then Exit;
    //
    // NF de venda
    ide_mod   := EdCOD_MOD.ValueVariant;
    ide_Serie := EdSER.ValueVariant;
    ide_nNF   := EdNUM_DOC.ValueVariant;

    UnDmkDAC_PF.AbreMySQLQuery0(Qr00_NFeCabA, Dmod.MyDB, [
    'SELECT FatID, FatNum, Empresa, IDCtrl,',
    'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk,',
    'ide_mod, ide_serie, ide_nNF, Id  ',
    'FROM nfecaba ',
    'WHERE FatID=' + Geral.FF0(VAR_FATID_0053),
    'AND Empresa=' + Geral.FF0(Empresa),
    SQL_CNPJ_CPF_Emit,
    SQL_CNPJ_CPF_Dest,
    'AND ide_mod IN (0,' + Geral.FF0(ide_mod) + ')',
    'AND ide_serie >= 0 ', // + Geral.FF0(ide_Serie),
    'AND ide_nNF=' + Geral.FF0(ide_nNF),
    'ORDER BY ide_mod DESC ',
    '']);
    if Qr00_NFeCabA.RecordCount > 0 then
    begin
      if not Qr00_NFeCabA.Locate('ide_mod;ide_serie', VarArrayOf([ide_mod, ide_serie]), []) then
      begin
        if not Qr00_NFeCabA.Locate('ide_serie', ide_serie, []) then
        begin
          if not Qr00_NFeCabA.Locate('ide_mod', ide_mod, []) then
          begin

          end;
        end;
      end;
      //Geral.MB_Teste(Qr00_NFeCabA.SQL.Text);
      //
      if ide_Serie <> Qr00_NFeCabAide_serie.Value then
        EdSER.ValueVariant := Qr00_NFeCabAide_serie.Value;
      //
      EdCHV_NFE.ValueVariant     := Qr00_NFeCabAId.Value;
      EdNFe_StaLnk.ValueVariant  := Integer(TEFDAtrelamentoNFeComEstq.eanceSohCabecalho); // = 2
      EdNFe_FatID.ValueVariant   := Qr00_NFeCabAFatID.Value;
      EdNFe_FatNum.ValueVariant  := Qr00_NFeCabAFatNum.Value;
    end;
  end;
*)
end;

procedure TFmEfdInnCTsCab.SetaBasesDeCalculo();
begin
  if  QrFisRegCadFrtTES_BC_ICMS.Value = 2 then
    FmEfdInnCTsCab.EdVL_BC_ICMS.ValueVariant    := EdVL_OPR.ValueVariant;
  if QrFisRegCadFrtTES_BC_PIS.Value = 2 then
    FmEfdInnCTsCab.EdVL_BC_PIS.ValueVariant       := EdVL_OPR.ValueVariant;
  if QrFisRegCadFrtTES_BC_COFINS.Value = 2 then
    FmEfdInnCTsCab.EdVL_BC_COFINS.ValueVariant    := EdVL_OPR.ValueVariant;
end;

procedure TFmEfdInnCTsCab.SetaParametrosDaRegraFiscal();
begin
  if ImgTipo.SQLType = stIns then
  begin
    if EdRegrFiscal.ValueVariant then
    begin

  //

      FmEfdInnCTsCab.EdCFOP.ValueVariant          := QrFisRegCadFrtInnCFOP.Value;
      FmEfdInnCTsCab.CBCFOP.KeyValue              := QrFisRegCadFrtInnCFOP.Value;
      //
      FmEfdInnCTsCab.EdICMS_CST.ValueVariant      := QrFisRegCadFrtInnICMS_CST.Value;
      //
      if QrFisRegCadFrtTES_ICMS.Value = 2 then
        FmEfdInnCTsCab.EdALIQ_ICMS.ValueVariant     := QrFisRegCadFrtInnALIQ_ICMS.Value;
      FmEfdInnCTsCab.EdCST_PIS.ValueVariant         := QrFisRegCadFrtInnPIS_CST.Value;
      if QrFisRegCadFrtTES_PIS.Value = 2 then
        FmEfdInnCTsCab.EdALIQ_PIS.ValueVariant        := QrFisRegCadFrtInnALIQ_PIS.Value;
      //
      FmEfdInnCTsCab.EdCST_COFINS.ValueVariant      := QrFisRegCadFrtInnCOFINS_CST.Value;
      if QrFisRegCadFrtTES_COFINS.Value = 2 then
        FmEfdInnCTsCab.EdALIQ_COFINS.ValueVariant     := QrFisRegCadFrtInnALIQ_COFINS.Value;

      //
      FmEfdInnCTsCab.EdIND_NAT_FRT.ValueVariant     := QrFisRegCadFrtInnIND_NAT_FRT.Value;
      FmEfdInnCTsCab.EdNAT_BC_CRED.ValueVariant     := QrFisRegCadFrtInnNAT_BC_CRED.Value;
      //
      SetaBasesDeCalculo();
    end;
  end;
end;

(*  EdFornecedorChange
var
  Fornecedor: Integer;
begin
  Fornecedor := EdFornecedor.ValueVariant;
  UnDmkDAC_PF.AbreMySQLQuery0(QrFornecedores, Dmod.MyDB, [
  'SELECT Codigo, Tipo, CNPJ, CPF,',
  'IF(Tipo=0, RazaoSocial, Nome) NOME',
  'FROM entidades',
  'WHERE Codigo=' + Geral.FF0(Fornecedor),
  'ORDER BY Nome',
  '']);
  if EdFornecedor.ValueVariant <> Fornecedor then
    EdFornecedor.ValueVariant := Fornecedor;
  if CBFornecedor.KeyValue <> Fornecedor then
    CBFornecedor.KeyValue := Fornecedor;
*)

(*
object EdFornecedor: TdmkEditCB
  Left = 300
  Top = 16
  Width = 56
  Height = 21
  Alignment = taRightJustify
  Enabled = False
  TabOrder = 3
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ValMin = '-2147483647'
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
  ValWarn = False
  DBLookupComboBox = CBFornecedor
  IgnoraDBLookupComboBox = False
  AutoSetIfOnlyOneReg = setregOnlyManual
end
object Label3: TLabel
  Left = 300
  Top = 0
  Width = 132
  Height = 13
  Caption = 'Fornecedor (Transportador):'
  Enabled = False
end
object CBFornecedor: TdmkDBLookupComboBox
  Left = 360
  Top = 16
  Width = 433
  Height = 21
  Enabled = False
  KeyField = 'Codigo'
  ListField = 'NOME'
  ListSource = DsFornecedores
  TabOrder = 4
  dmkEditCB = EdFornecedor
  UpdType = utYes
  LocF7SQLMasc = '$#'
  LocF7PreDefProc = f7pNone
end
object QrFornecedores: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT Codigo, Tipo, CNPJ, CPF,'
    'IF(Tipo=0, RazaoSocial, Nome) NOME'
    'FROM entidades'
    'WHERE Fornece2='#39'V'#39
    'ORDER BY Nome')
  Left = 628
  Top = 8
  object QrFornecedoresCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrFornecedoresNOME: TWideStringField
    FieldName = 'NOME'
    Size = 50
  end
  object QrFornecedoresTipo: TSmallintField
    FieldName = 'Tipo'
    Required = True
  end
  object QrFornecedoresCNPJ: TWideStringField
    FieldName = 'CNPJ'
    Size = 18
  end
  object QrFornecedoresCPF: TWideStringField
    FieldName = 'CPF'
    Size = 18
  end
end
object DsFornecedores: TDataSource
  DataSet = QrFornecedores
  Left = 628
  Top = 60
end

*)

(*
procedure TFmEfdInnCTsCab.EdIND_PGTOChange(Sender: TObject);
begin
  EdIND_PGTO_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_PAG_EFD,  EdIND_PGTO.ValueVariant);
end;

procedure TFmEfdInnCTsCab.EdIND_PGTOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_PGTO.Text := Geral.SelecionaItem(F_IND_PGTO_EFD, 0,
      'SEL-LISTA-000 :: Indicador do tipo de pagamento',
      TitCols, Screen.Width)
  end;
end;

*)

(*
object Label16: TLabel
  Left = 796
  Top = 0
  Width = 171
  Height = 13
  Caption = 'Indicador do tipo de pagamento [F4]'
  FocusControl = EdIND_PGTO
end
object EdIND_PGTO: TdmkEdit
  Left = 796
  Top = 16
  Width = 21
  Height = 21
  Alignment = taRightJustify
  TabOrder = 3
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ValMin = '0'
  ValMax = '4'
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '-1'
  QryCampo = 'IND_PGTO'
  UpdCampo = 'IND_PGTO'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = -1
  ValWarn = False
end
object EdIND_PGTO_TXT: TdmkEdit
  Left = 818
  Top = 16
  Width = 179
  Height = 21
  ReadOnly = True
  TabOrder = 4
  FormatType = dmktfString
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = ''
  ValWarn = False
end
*)
end.
