object FmEfdIcmsIpiE110_v03_0_9: TFmEfdIcmsIpiE110_v03_0_9
  Left = 339
  Top = 185
  Caption = 'EII-SPEDE-110 :: Apura'#231#227'o do ICMS - Opera'#231#245'es pr'#243'prias'
  ClientHeight = 588
  ClientWidth = 609
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 609
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 561
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 513
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 495
        Height = 32
        Caption = 'Apura'#231#227'o do ICMS - Opera'#231#245'es pr'#243'prias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 495
        Height = 32
        Caption = 'Apura'#231#227'o do ICMS - Opera'#231#245'es pr'#243'prias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 495
        Height = 32
        Caption = 'Apura'#231#227'o do ICMS - Opera'#231#245'es pr'#243'prias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 518
    Width = 609
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 463
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 461
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 609
    Height = 426
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 609
      Height = 426
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 61
        Width = 609
        Height = 365
        Align = alClient
        Caption = ' Dados da Apura'#231#227'o: '
        TabOrder = 1
        object Label7: TStaticText
          Left = 48
          Top = 20
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 
            '02. Valor total dos d'#233'bitos por "Sa'#237'das e presta'#231#245'es com d'#233'bito ' +
            'do imposto"'
          FocusControl = EdVL_TOT_DEBITOS
          TabOrder = 14
        end
        object Label8: TStaticText
          Left = 48
          Top = 44
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 
            '03. Valor total dos ajustes a d'#233'bito decorrentes do documento fi' +
            'scal'
          FocusControl = EdVL_AJ_DEBITOS
          TabOrder = 15
        end
        object Label9: TStaticText
          Left = 48
          Top = 68
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '04. Valor total de "Ajustes a d'#233'bito".'
          FocusControl = EdVL_TOT_AJ_DEBITOS
          TabOrder = 16
        end
        object Label10: TStaticText
          Left = 48
          Top = 92
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '05. Valor total de Ajustes "Estornos de cr'#233'ditos"'
          FocusControl = EdVL_ESTORNOS_CRED
          TabOrder = 17
        end
        object Label11: TStaticText
          Left = 48
          Top = 116
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 
            '06. Valor total dos cr'#233'ditos por "Entradas e aquisi'#231#245'es com cr'#233'd' +
            'ito do imposto"'
          FocusControl = EdVL_TOT_CREDITOS
          TabOrder = 18
        end
        object Label12: TStaticText
          Left = 48
          Top = 140
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 
            '07. Valor total dos ajustes a cr'#233'dito decorrentes do documento f' +
            'iscal.'
          FocusControl = EdVL_AJ_CREDITOS
          TabOrder = 19
        end
        object Label13: TStaticText
          Left = 48
          Top = 164
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '08. Valor total de "Ajustes a cr'#233'dito"'
          FocusControl = EdVL_TOT_AJ_CREDITOS
          TabOrder = 20
        end
        object Label14: TStaticText
          Left = 48
          Top = 188
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '09. Valor total de Ajustes "Estornos de D'#233'bitos"'
          FocusControl = EdVL_ESTORNOS_DEB
          TabOrder = 21
        end
        object Label15: TStaticText
          Left = 48
          Top = 212
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 
            '10. Valor   total   de   "Saldo   credor   do   per'#237'odo anterior' +
            '"'
          FocusControl = EdVL_SLD_CREDOR_ANT
          TabOrder = 22
        end
        object Label16: TStaticText
          Left = 48
          Top = 236
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '11. Valor do saldo devedor apurado'
          FocusControl = EdVL_SLD_APURADO
          TabOrder = 23
        end
        object Label17: TStaticText
          Left = 48
          Top = 260
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '12. Valor total de "Dedu'#231#245'es"'
          FocusControl = EdVL_TOT_DED
          TabOrder = 24
        end
        object Label18: TStaticText
          Left = 48
          Top = 284
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '13. Valor total de "ICMS a recolher" (11-12)'
          FocusControl = EdVL_ICMS_RECOLHER
          TabOrder = 25
        end
        object Label19: TStaticText
          Left = 48
          Top = 308
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 
            '14. Valor total de "Saldo credor a transportar para o per'#237'odo se' +
            'guinte"'
          FocusControl = EdVL_SLD_CREDOR_TRANSPORTAR
          TabOrder = 26
        end
        object Label20: TStaticText
          Left = 48
          Top = 332
          Width = 380
          Height = 21
          Alignment = taRightJustify
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '15. Valores recolhidos ou a recolher, extra-apura'#231#227'o.'
          FocusControl = EdDEB_ESP
          TabOrder = 27
        end
        object EdVL_TOT_DEBITOS: TdmkEdit
          Left = 432
          Top = 20
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_TOT_DEBITOS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdVL_AJ_DEBITOS: TdmkEdit
          Left = 432
          Top = 44
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_AJ_DEBITOS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdVL_TOT_AJ_DEBITOS: TdmkEdit
          Left = 432
          Top = 68
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_TOT_AJ_DEBITOS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdVL_ESTORNOS_CRED: TdmkEdit
          Left = 432
          Top = 92
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_ESTORNOS_CRED'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdVL_TOT_CREDITOS: TdmkEdit
          Left = 432
          Top = 116
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_TOT_CREDITOS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdVL_AJ_CREDITOS: TdmkEdit
          Left = 432
          Top = 140
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_AJ_CREDITOS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdVL_TOT_AJ_CREDITOS: TdmkEdit
          Left = 432
          Top = 164
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_TOT_AJ_CREDITOS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdVL_ESTORNOS_DEB: TdmkEdit
          Left = 432
          Top = 188
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_ESTORNOS_DEB'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdVL_SLD_CREDOR_ANT: TdmkEdit
          Left = 432
          Top = 212
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_SLD_CREDOR_ANT'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdVL_SLD_APURADO: TdmkEdit
          Left = 432
          Top = 236
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryName = 'VL_SLD_APURADO'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdVL_TOT_DED: TdmkEdit
          Left = 432
          Top = 260
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_TOT_DED'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdVL_ICMS_RECOLHER: TdmkEdit
          Left = 432
          Top = 284
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_ICMS_RECOLHER'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdVL_SLD_CREDOR_TRANSPORTAR: TdmkEdit
          Left = 432
          Top = 308
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_SLD_CREDOR_TRANSPORTAR'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdDEB_ESP: TdmkEdit
          Left = 432
          Top = 332
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryName = 'DEB_ESP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 609
        Height = 61
        Align = alTop
        Caption = ' Dados do intervalo: '
        Enabled = False
        TabOrder = 0
        object Label3: TLabel
          Left = 32
          Top = 16
          Width = 56
          Height = 13
          Caption = 'ImporExpor:'
        end
        object Label4: TLabel
          Left = 96
          Top = 16
          Width = 42
          Height = 13
          Caption = 'AnoMes:'
        end
        object Label5: TLabel
          Left = 152
          Top = 16
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label6: TLabel
          Left = 200
          Top = 16
          Width = 28
          Height = 13
          Caption = 'E100:'
        end
        object Label1: TLabel
          Left = 328
          Top = 16
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
        end
        object Label2: TLabel
          Left = 444
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Data final:'
        end
        object Label21: TLabel
          Left = 264
          Top = 16
          Width = 33
          Height = 13
          Caption = 'LinArq:'
        end
        object EdImporExpor: TdmkEdit
          Left = 32
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAnoMes: TdmkEdit
          Left = 96
          Top = 32
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdEmpresa: TdmkEdit
          Left = 152
          Top = 32
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdE100: TdmkEdit
          Left = 200
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object TPDT_INI: TdmkEditDateTimePicker
          Left = 328
          Top = 32
          Width = 112
          Height = 21
          Date = 40761.000000000000000000
          Time = 0.337560104169824600
          TabOrder = 4
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object TPDT_FIN: TdmkEditDateTimePicker
          Left = 444
          Top = 32
          Width = 112
          Height = 21
          Date = 40761.000000000000000000
          Time = 0.337560104169824600
          TabOrder = 5
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdLinArq: TdmkEdit
          Left = 264
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 474
    Width = 609
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 605
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
end
