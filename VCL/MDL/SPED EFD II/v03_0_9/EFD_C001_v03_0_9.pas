unit EFD_C001_v03_0_9;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Menus, ComCtrls, Grids,
  DBGrids, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, Variants,
  dmkDBGrid, frxClass, frxDBSet, dmkCheckGroup, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmEFD_C001_v03_0_9 = class(TForm)
    PainelDados: TPanel;
    DsEFD_C001: TDataSource;
    QrEFD_C001: TmySQLQuery;
    dmkPermissoes1: TdmkPermissoes;
    PMPeriodo: TPopupMenu;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    Itemns1: TMenuItem;
    Panel9: TPanel;
    Panel10: TPanel;
    DBGCab: TdmkDBGrid;
    Panel11: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel12: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    DBEdit1: TDBEdit;
    Panel3: TPanel;
    BtC500: TBitBtn;
    BtPeriodo: TBitBtn;
    Panel2: TPanel;
    BtSaida0: TBitBtn;
    PB1: TProgressBar;
    Panel16: TPanel;
    PMC500: TPopupMenu;
    GroupBox3: TGroupBox;
    Incluinovoperiodo1: TMenuItem;
    Excluiperiodoselecionado1: TMenuItem;
    QrEFD_C001NO_ENT: TWideStringField;
    QrEFD_C001MES_ANO: TWideStringField;
    QrEFD_C001ImporExpor: TSmallintField;
    QrEFD_C001AnoMes: TIntegerField;
    QrEFD_C001Empresa: TIntegerField;
    QrEFD_C001LinArq: TIntegerField;
    QrEFD_C001REG: TWideStringField;
    QrEFD_C001IND_MOV: TWideStringField;
    QrEFD_C500: TmySQLQuery;
    DsEFD_C500: TDataSource;
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    IncluiC500: TMenuItem;
    AlteraC500: TMenuItem;
    ExcluiC500: TMenuItem;
    dmkDBGrid1: TdmkDBGrid;
    QrEFD_C500ImporExpor: TSmallintField;
    QrEFD_C500AnoMes: TIntegerField;
    QrEFD_C500Empresa: TIntegerField;
    QrEFD_C500LinArq: TIntegerField;
    QrEFD_C500REG: TWideStringField;
    QrEFD_C500IND_OPER: TWideStringField;
    QrEFD_C500IND_EMIT: TWideStringField;
    QrEFD_C500COD_PART: TWideStringField;
    QrEFD_C500COD_MOD: TWideStringField;
    QrEFD_C500COD_SIT: TWideStringField;
    QrEFD_C500SER: TWideStringField;
    QrEFD_C500SUB: TWideStringField;
    QrEFD_C500COD_CONS: TWideStringField;
    QrEFD_C500NUM_DOC: TIntegerField;
    QrEFD_C500DT_DOC: TDateField;
    QrEFD_C500DT_E_S: TDateField;
    QrEFD_C500VL_DOC: TFloatField;
    QrEFD_C500VL_DESC: TFloatField;
    QrEFD_C500VL_FORN: TFloatField;
    QrEFD_C500VL_SERV_NT: TFloatField;
    QrEFD_C500VL_TERC: TFloatField;
    QrEFD_C500VL_DA: TFloatField;
    QrEFD_C500VL_BC_ICMS: TFloatField;
    QrEFD_C500VL_ICMS: TFloatField;
    QrEFD_C500VL_BC_ICMS_ST: TFloatField;
    QrEFD_C500VL_ICMS_ST: TFloatField;
    QrEFD_C500COD_INF: TWideStringField;
    QrEFD_C500VL_PIS: TFloatField;
    QrEFD_C500VL_COFINS: TFloatField;
    QrEFD_C500TP_LIGACAO: TWideStringField;
    QrEFD_C500COD_GRUPO_TENSAO: TWideStringField;
    QrEFD_C500Terceiro: TIntegerField;
    QrEFD_C500Importado: TSmallintField;
    QrEFD_C500CST_ICMS: TWideStringField;
    QrEFD_C500CFOP: TWideStringField;
    QrEFD_C500ALIQ_ICMS: TFloatField;
    QrEFD_C500VL_RED_BC: TFloatField;
    QrEFD_C500NO_TERC: TWideStringField;
    TabSheet3: TTabSheet;
    BtC100: TBitBtn;
    PMC100: TPopupMenu;
    IncluiC1001: TMenuItem;
    AlteraC1001: TMenuItem;
    ExcluiC1001: TMenuItem;
    QrEFD_C100: TmySQLQuery;
    DsEFD_C100: TDataSource;
    dmkDBGrid2: TdmkDBGrid;
    QrEFD_C100NO_TERC: TWideStringField;
    QrEFD_C100ImporExpor: TSmallintField;
    QrEFD_C100AnoMes: TIntegerField;
    QrEFD_C100Empresa: TIntegerField;
    QrEFD_C100LinArq: TIntegerField;
    QrEFD_C100REG: TWideStringField;
    QrEFD_C100IND_OPER: TWideStringField;
    QrEFD_C100IND_EMIT: TWideStringField;
    QrEFD_C100COD_PART: TWideStringField;
    QrEFD_C100COD_MOD: TWideStringField;
    QrEFD_C100COD_SIT: TWideStringField;
    QrEFD_C100SER: TWideStringField;
    QrEFD_C100NUM_DOC: TIntegerField;
    QrEFD_C100CHV_NFE: TWideStringField;
    QrEFD_C100DT_DOC: TDateField;
    QrEFD_C100DT_E_S: TDateField;
    QrEFD_C100VL_DOC: TFloatField;
    QrEFD_C100IND_PGTO: TWideStringField;
    QrEFD_C100VL_DESC: TFloatField;
    QrEFD_C100VL_ABAT_NT: TFloatField;
    QrEFD_C100VL_MERC: TFloatField;
    QrEFD_C100IND_FRT: TWideStringField;
    QrEFD_C100VL_FRT: TFloatField;
    QrEFD_C100VL_SEG: TFloatField;
    QrEFD_C100VL_OUT_DA: TFloatField;
    QrEFD_C100VL_BC_ICMS: TFloatField;
    QrEFD_C100VL_ICMS: TFloatField;
    QrEFD_C100VL_BC_ICMS_ST: TFloatField;
    QrEFD_C100VL_ICMS_ST: TFloatField;
    QrEFD_C100VL_IPI: TFloatField;
    QrEFD_C100VL_PIS: TFloatField;
    QrEFD_C100VL_COFINS: TFloatField;
    QrEFD_C100VL_PIS_ST: TFloatField;
    QrEFD_C100VL_COFINS_ST: TFloatField;
    QrEFD_C100ParTipo: TIntegerField;
    QrEFD_C100ParCodi: TIntegerField;
    QrEFD_C100FatID: TIntegerField;
    QrEFD_C100FatNum: TIntegerField;
    QrEFD_C100ConfVal: TSmallintField;
    QrEFD_C100Terceiro: TIntegerField;
    QrEFD_C100Importado: TSmallintField;
    QrEFD_C100CST_ICMS: TWideStringField;
    QrEFD_C100CFOP: TWideStringField;
    QrEFD_C100ALIQ_ICMS: TFloatField;
    QrEFD_C100VL_RED_BC: TFloatField;
    QrEFD_C500Lk: TIntegerField;
    QrEFD_C500DataCad: TDateField;
    QrEFD_C500DataAlt: TDateField;
    QrEFD_C500UserCad: TIntegerField;
    QrEFD_C500UserAlt: TIntegerField;
    QrEFD_C500AlterWeb: TSmallintField;
    QrEFD_C500AWServerID: TIntegerField;
    QrEFD_C500AWStatSinc: TSmallintField;
    QrEFD_C500Ativo: TSmallintField;
    QrEFD_C500CHV_DOCe: TWideStringField;
    QrEFD_C500FIN_DOCe: TSmallintField;
    QrEFD_C500CHV_DOCe_REF: TWideStringField;
    QrEFD_C500IND_DEST: TSmallintField;
    QrEFD_C500COD_MUN_DEST: TIntegerField;
    QrEFD_C500COD_CTA: TWideStringField;
    QrEFD_C500COD_MOD_DOC_REF: TSmallintField;
    QrEFD_C500HASH_DOC_REF: TWideStringField;
    QrEFD_C500SER_DOC_REF: TWideStringField;
    QrEFD_C500NUM_DOC_REF: TIntegerField;
    QrEFD_C500MES_DOC_REF: TIntegerField;
    QrEFD_C500ENER_INJET: TFloatField;
    QrEFD_C500OUTRAS_DED: TFloatField;
    QrEFD_C100Codigo: TIntegerField;
    QrEfdInnC500Its: TMySQLQuery;
    DsEfdIcmsIpic500: TDataSource;
    QrEfdInnC500ItsAnoMes: TIntegerField;
    QrEfdInnC500ItsEmpresa: TIntegerField;
    QrEfdInnC500ItsCodigo: TIntegerField;
    QrEfdInnC500ItsControle: TIntegerField;
    QrEfdInnC500ItsGraGruX: TIntegerField;
    QrEfdInnC500ItsQTD: TFloatField;
    QrEfdInnC500ItsUNID: TWideStringField;
    QrEfdInnC500ItsVL_ITEM: TFloatField;
    QrEfdInnC500ItsVL_DESC: TFloatField;
    QrEfdInnC500ItsCST_ICMS: TIntegerField;
    QrEfdInnC500ItsCFOP: TIntegerField;
    QrEfdInnC500ItsVL_BC_ICMS: TFloatField;
    QrEfdInnC500ItsALIQ_ICMS: TFloatField;
    QrEfdInnC500ItsVL_ICMS: TFloatField;
    QrEfdInnC500ItsVL_BC_ICMS_ST: TFloatField;
    QrEfdInnC500ItsALIQ_ST: TFloatField;
    QrEfdInnC500ItsVL_ICMS_ST: TFloatField;
    QrEfdInnC500ItsCST_PIS: TWideStringField;
    QrEfdInnC500ItsNAT_BC_CRED: TWideStringField;
    QrEfdInnC500ItsVL_BC_PIS: TFloatField;
    QrEfdInnC500ItsALIQ_PIS: TFloatField;
    QrEfdInnC500ItsVL_PIS: TFloatField;
    QrEfdInnC500ItsCOD_CTA: TWideStringField;
    QrEfdInnC500ItsCST_COFINS: TWideStringField;
    QrEfdInnC500ItsVL_BC_COFINS: TFloatField;
    QrEfdInnC500ItsALIQ_COFINS: TFloatField;
    QrEfdInnC500ItsVL_COFINS: TFloatField;
    QrEFD_C500Codigo: TIntegerField;
    procedure BtC500Click(Sender: TObject);
    procedure BtSaida0Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPeriodoClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrEFD_C001BeforeClose(DataSet: TDataSet);
    procedure QrEFD_C001AfterScroll(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure QrEFD_C001AfterOpen(DataSet: TDataSet);
    procedure PMPeriodoPopup(Sender: TObject);
    procedure RGEmitenteClick(Sender: TObject);
    procedure PMC500Popup(Sender: TObject);
    procedure Incluinovoperiodo1Click(Sender: TObject);
    procedure Excluiperiodoselecionado1Click(Sender: TObject);
    procedure IncluiC500Click(Sender: TObject);
    procedure AlteraC500Click(Sender: TObject);
    procedure ExcluiC500Click(Sender: TObject);
    procedure BtC100Click(Sender: TObject);
    procedure IncluiC1001Click(Sender: TObject);
    procedure AlteraC1001Click(Sender: TObject);
    procedure ExcluiC1001Click(Sender: TObject);
    procedure IncluiitemNFC5001Click(Sender: TObject);
  private
    { Private declarations }
    function  PeriodoJaExiste(ImporExpor, AnoMes, Empresa: Integer;
              Avisa: Boolean): Boolean;
    procedure InsUpdC100(SQLType: TSQLType);
    procedure InsUpdC500(SQLType: TSQLType);
    //

  public
    { Public declarations }
    FEmpresa: Integer;
    FExpImpTXT, FEmprTXT: String;
    //
    procedure ReopenEFD_C001(AnoMes: Integer);
    procedure ReopenEFD_C500(LinArq: Integer);
    procedure ReopenEFD_C100(LinArq: Integer);
  end;

var
  FmEFD_C001_v03_0_9: TFmEFD_C001_v03_0_9;

implementation

uses
  UnMyObjects, Module, MyDBCheck, ModuleGeral, ModProd, UCreate,
  CfgExpFile, UnGrade_Tabs, ModuleNFe_0000,
  Principal, Periodo, (*EFD_E100, EFD_E110, EFD_E111, EFD_E116,
  EFD_E112, EFD_E113, EFD_E115, EFD_C100*)EFD_C500_v03_0_9, EfdInnC500Its;

{$R *.DFM}

const
  FFormatFloat = '00000';
  FImporExpor = 3; // Criar! N�o Mexer

procedure TFmEFD_C001_v03_0_9.EdEmpresaChange(Sender: TObject);
begin
  if EdEmpresa.ValueVariant <> 0 then
  begin
    FEmpresa := DModG.QrEmpresasCodigo.Value;
    FEmprTXT := FormatFloat('0', FEmpresa);
  end else
  begin
    FEmpresa := 0;
    FEmprTXT := '0';
  end;
  BtPeriodo.Enabled := FEmpresa <> 0;
  ReopenEFD_C001(0);
end;

procedure TFmEFD_C001_v03_0_9.ExcluiC1001Click(Sender: TObject);
begin
{ TODO : Exclus�o de C100 }
end;

procedure TFmEFD_C001_v03_0_9.ExcluiC500Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o da NF Tipo C500?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipic500', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [QrEFD_C500ImporExpor.Value, QrEFD_C500AnoMes.Value,
    QrEFD_C500Empresa.Value, QrEFD_C500LinArq.Value], '') then
    //
    ReopenEFD_C500(0);
  end;
end;

procedure TFmEFD_C001_v03_0_9.Excluiperiodoselecionado1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do per�odo selecionado?',
  'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipic001', [
    'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
    [QrEFD_C001ImporExpor.Value, QrEFD_C001AnoMes.Value,
    QrEFD_C001Empresa.Value], '') then
    ReopenEFD_C001(0);
  end;
end;

procedure TFmEFD_C001_v03_0_9.IncluiC1001Click(Sender: TObject);
begin
  InsUpdC100(stIns);
end;

procedure TFmEFD_C001_v03_0_9.IncluiC500Click(Sender: TObject);
begin
  InsUpdC500(stIns);
end;

procedure TFmEFD_C001_v03_0_9.IncluiitemNFC5001Click(Sender: TObject);
begin
  InsUpdC500(stIns);
end;

procedure TFmEFD_C001_v03_0_9.Incluinovoperiodo1Click(Sender: TObject);
const
  LinArq = 0;
  REG = 'E001';
  IND_MOV = 1;
var
  Ano, Mes, Dia: Word;
  Cancelou: Boolean;
  AnoMes: Integer;
begin
  DecodeDate(Date, Ano, Mes, Dia);
  MLAGeral.EscolhePeriodo_MesEAno(TFmPeriodo, FmPeriodo, Mes, Ano, Cancelou, True, True);
  if not Cancelou then
  begin
    AnoMes := (Ano * 100) + Mes;
    //ShowMessage(FormatFloat('0', (Ano * 100) + Mes));
    if not PeriodoJaExiste(FImporExpor, AnoMes, FEmpresa, True) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efdicmsipic001', False, [
      'LinArq', 'REG', 'IND_MOV'], [
      'ImporExpor', 'AnoMes', 'Empresa'], [
      LinArq, REG, IND_MOV], [
      FImporExpor, AnoMes, FEmpresa], True) then
        ReopenEFD_C001(AnoMes);
    end;
    ReopenEFD_C001(AnoMes);
  end;
end;

procedure TFmEFD_C001_v03_0_9.InsUpdC100(SQLType: TSQLType);
begin
(*
  if UmyMod.FormInsUpd_Cria(TFmEFD_C100, FmEFD_C100, afmoNegarComAviso,
  QrEFD_C100, SQLType) then
  begin
    FmEFD_C100.ImgTipo.SQLType := SQLType;
    FmEFD_C100.EdImporExpor.ValueVariant := QrEFD_C001ImporExpor.Value;
    FmEFD_C100.EdAnoMes.ValueVariant := QrEFD_C001AnoMes.Value;
    FmEFD_C100.EdEmpresa.ValueVariant := QrEFD_C001Empresa.Value;
    //
    if SQLType = stIns then
    begin
      FmEFD_C100.RGIND_OPER.ItemIndex := 0;
      FmEFD_C100.RGIND_EMIT.ItemIndex := 1;
      FmEFD_C100.TPDT_DOC.Date := Date;
      FmEFD_C100.TPDT_E_S.Date := Date;
    end else
    begin
    end;
    //
    FmEFD_C100.ShowModal;
    FmEFD_C100.Destroy;
  end;
*)
end;

procedure TFmEFD_C001_v03_0_9.InsUpdC500(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmEFD_C500_v03_0_9, FmEFD_C500_v03_0_9, afmoNegarComAviso,
  QrEFD_C500, SQLType) then
  begin
    FmEFD_C500_v03_0_9.FAnoMes := QrEFD_C001AnoMes.Value;
    FmEFD_C500_v03_0_9.ReopenTabelas();
    FmEFD_C500_v03_0_9.ImgTipo.SQLType := SQLType;
    FmEFD_C500_v03_0_9.EdAnoMes.ValueVariant := QrEFD_C001AnoMes.Value;
    FmEFD_C500_v03_0_9.EdEmpresa.ValueVariant := QrEFD_C001Empresa.Value;
    //
    if SQLType = stIns then
    begin
      FmEFD_C500_v03_0_9.RGIND_OPER.ItemIndex := 0;
      FmEFD_C500_v03_0_9.RGIND_EMIT.ItemIndex := 1;
      FmEFD_C500_v03_0_9.TPDT_DOC.Date := Date;
      FmEFD_C500_v03_0_9.TPDT_E_S.Date := Date;
    end else
    begin

    end;
    //
    FmEFD_C500_v03_0_9.ShowModal;
    FmEFD_C500_v03_0_9.Destroy;
  end;
end;

function TFmEFD_C001_v03_0_9.PeriodoJaExiste(ImporExpor, AnoMes, Empresa: Integer;
Avisa: Boolean): Boolean;
var
  Qry1: TmySQLQuery;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  Qry1.Close;
  Qry1.Database := Dmod.MyDB;
  UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
  'SELECT COUNT(*) Registros ',
  'FROM efdicmsipic001 ',
  'WHERE ImporExpor=' + FormatFloat('0', ImporExpor),
  'AND AnoMes=' + FormatFloat('0', AnoMes),
  'AND Empresa=' + FormatFloat('0', Empresa),
  '']);
  Result := Qry1.FieldByName('Registros').AsInteger > 0;
  if Result and Avisa then
  begin
    Geral.MensagemBox('O periodo selecionado j� existe!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmEFD_C001_v03_0_9.PMPeriodoPopup(Sender: TObject);
begin
  Excluiperiodoselecionado1.Enabled :=
    (QrEFD_C001.State <> dsInactive) and
    (QrEFD_C001.RecordCount > 0)
  and
    (QrEFD_C500.State <> dsInactive) and
    (QrEFD_C500.RecordCount = 0);
end;

procedure TFmEFD_C001_v03_0_9.PMC500Popup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEFD_C001.State <> dsInactive) and (QrEFD_C001.RecordCount > 0);
  IncluiC500.Enabled := Habilita;
  //
  Habilita := Habilita and
    (QrEFD_C500.State <> dsInactive) and (QrEFD_C500.RecordCount > 0);
  AlteraC500.Enabled := Habilita;
  ExcluiC500.Enabled := Habilita and
    (QrEFD_C500.State <> dsInactive) and (QrEFD_C500.RecordCount > 0);
end;

procedure TFmEFD_C001_v03_0_9.RGEmitenteClick(Sender: TObject);
begin
  ReopenEFD_C500(0);
end;

procedure TFmEFD_C001_v03_0_9.ReopenEFD_C001(AnoMes: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_C001, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT, ',
  'CONCAT(RIGHT(e001.AnoMes, 2), "/", ',
  'LEFT(LPAD(e001.AnoMes, 6, "0"), 4)) MES_ANO, e001.* ',
  'FROM efdicmsipic001 e001 ',
  'LEFT JOIN entidades ent ON ent.Codigo=e001.Empresa ',
  'WHERE e001.ImporExpor=' + FormatFloat('0', FImporExpor),
  'AND e001.Empresa=' + FormatFloat('0', FEmpresa),
  'ORDER BY e001.AnoMes DESC ',
  '']);
  //
  QrEFD_C001.Locate('AnoMes', AnoMes, []);
end;

procedure TFmEFD_C001_v03_0_9.ReopenEFD_C100(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_C100, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)  ',
  'NO_TERC, c100.* ',
  'FROM efdicmsipic100 c100  ',
  'LEFT JOIN entidades ent ON ent.Codigo=c100.Terceiro ',
  'WHERE c100.ImporExpor=' + FormatFloat('0', QrEFD_C001ImporExpor.Value),
  'AND c100.Empresa=' + FormatFloat('0', QrEFD_C001Empresa.Value),
  'AND c100.AnoMes=' + FormatFloat('0', QrEFD_C001AnoMes.Value),
  '']);
  //
  QrEFD_C100.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_C001_v03_0_9.ReopenEFD_C500(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_C500, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)  ',
  'NO_TERC, c500.* ',
  'FROM efdicmsipic500 c500  ',
  'LEFT JOIN entidades ent ON ent.Codigo=c500.Terceiro ',
  'WHERE c500.ImporExpor=' + FormatFloat('0', QrEFD_C001ImporExpor.Value),
  'AND c500.Empresa=' + FormatFloat('0', QrEFD_C001Empresa.Value),
  'AND c500.AnoMes=' + FormatFloat('0', QrEFD_C001AnoMes.Value),
  //'ORDER BY c500.?',
  '']);
  //
  QrEFD_C500.Locate('LinArq', LinArq, []);
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEFD_C001_v03_0_9.BtC100Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMC100, BtC100);
end;

procedure TFmEFD_C001_v03_0_9.BtC500Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMC500, BtC500);
end;

procedure TFmEFD_C001_v03_0_9.BtSaida0Click(Sender: TObject);
begin
  //VAR_CADASTRO := Qr?.Value;
  Close;
end;

procedure TFmEFD_C001_v03_0_9.AlteraC1001Click(Sender: TObject);
begin
  InsUpdC100(stUpd);
end;

procedure TFmEFD_C001_v03_0_9.AlteraC500Click(Sender: TObject);
begin
  InsUpdC500(stUpd);
end;

procedure TFmEFD_C001_v03_0_9.BtPeriodoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPeriodo, BtPeriodo);
end;

procedure TFmEFD_C001_v03_0_9.FormCreate(Sender: TObject);
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FEmprTXT := '';
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  ImgTipo.SQLType := stLok;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmEFD_C001_v03_0_9.SbNumeroClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Codigo(QrCadComItensCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEFD_C001_v03_0_9.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEFD_C001_v03_0_9.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrCadComItensCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmEFD_C001_v03_0_9.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEFD_C001_v03_0_9.QrEFD_C001AfterOpen(DataSet: TDataSet);
begin
  BtC500.Enabled := QrEFD_C001.RecordCount > 0;
end;

procedure TFmEFD_C001_v03_0_9.QrEFD_C001AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_C500(0);
end;

procedure TFmEFD_C001_v03_0_9.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_C001_v03_0_9.SbQueryClick(Sender: TObject);
begin
{
  LocCod(QrCadComItensCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cadcomitens', Dmod.MyDB, CO_VAZIO));
}
end;

procedure TFmEFD_C001_v03_0_9.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEFD_C001_v03_0_9.QrEFD_C001BeforeClose(DataSet: TDataSet);
begin
  BtC500.Enabled := False;
  //
  QrEFD_C500.Close;
end;

end.

