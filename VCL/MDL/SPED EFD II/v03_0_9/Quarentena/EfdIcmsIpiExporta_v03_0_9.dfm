object FmEfdIcmsIpiExporta_v03_0_9: TFmEfdIcmsIpiExporta_v03_0_9
  Left = 339
  Top = 185
  Caption = 'SPE-EFDII-002 :: Exporta'#231#227'o de Arquivo SPED EFD ICMS/IPI'
  ClientHeight = 631
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 455
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 112
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 23
        Height = 13
        Caption = 'Filial:'
      end
      object LaMes: TLabel
        Left = 640
        Top = 3
        Width = 23
        Height = 13
        Caption = 'M'#234's:'
      end
      object BtMes: TSpeedButton
        Left = 724
        Top = 19
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = BtMesClick
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 573
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdMes: TdmkEdit
        Left = 640
        Top = 19
        Width = 82
        Height = 21
        Alignment = taCenter
        TabOrder = 2
        FormatType = dmktfMesAno
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = True
        PermiteNulo = False
        ValueVariant = Null
        ValWarn = False
        OnChange = EdMesChange
        OnRedefinido = EdMesRedefinido
      end
      object RGCOD_FIN: TRadioGroup
        Left = 8
        Top = 44
        Width = 573
        Height = 40
        Caption = ' Finalidade do arquivo: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '? - N'#227'o definido'
          '0 - Remessa de arquivo original'
          '1 - Remessa de arquivo substituto')
        TabOrder = 3
        OnClick = RGCOD_FINClick
      end
      object CkCorrigeCriadosAForca: TCheckBox
        Left = 12
        Top = 88
        Width = 193
        Height = 17
        Caption = 'Tenta corrigir itens criados a for'#231'a.'
        TabOrder = 4
      end
      object RGPreenche: TRadioGroup
        Left = 584
        Top = 44
        Width = 413
        Height = 40
        Caption = ' Preenchimento do arquivo: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '? - N'#227'o definido'
          'N'#227'o (vazio)'
          'Sim (informar dados)')
        TabOrder = 5
        OnClick = RGCOD_FINClick
      end
      object CkRecriaRegAotmaticos: TCheckBox
        Left = 751
        Top = 20
        Width = 194
        Height = 17
        Caption = 'Recria registros autom'#225'ticos.'
        Checked = True
        State = cbChecked
        TabOrder = 6
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 112
      Width = 1008
      Height = 343
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Registros a serem exportados '
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 315
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object TVBlocos: TTreeView
            Left = 1
            Top = 28
            Width = 998
            Height = 286
            Align = alClient
            Indent = 19
            MultiSelect = True
            TabOrder = 0
            OnClick = TVBlocosClick
            OnKeyDown = TVBlocosKeyDown
            Items.NodeData = {
              03010000001E0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000
              00000000000100}
          end
          object Panel5: TPanel
            Left = 1
            Top = 1
            Width = 998
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object SbBloco_K_estoque: TSpeedButton
              Left = 4
              Top = 2
              Width = 105
              Height = 22
              Caption = 'Bloco K - estoque'
              OnClick = SbBloco_K_estoqueClick
            end
            object SbTeste_minimo_EFD_ICMS_IPI: TSpeedButton
              Left = 116
              Top = 2
              Width = 161
              Height = 22
              Caption = 'EFD ICMS IPI B'#225'sico'
              OnClick = SbTeste_minimo_EFD_ICMS_IPIClick
            end
            object SbBloco_K_completo: TSpeedButton
              Left = 448
              Top = 2
              Width = 105
              Height = 22
              Caption = 'Bloco K completo'
              OnClick = SbBloco_K_completoClick
            end
            object SbImplementado: TSpeedButton
              Left = 282
              Top = 2
              Width = 161
              Height = 22
              Caption = 'EFD ICMS IPI Implem.'
              OnClick = SbImplementadoClick
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Texto Gerado '
        ImageIndex = 1
        object MeGerado: TMemo
          Left = 0
          Top = 0
          Width = 1000
          Height = 315
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssBoth
          TabOrder = 0
          WordWrap = False
          OnKeyUp = MeGeradoKeyUp
          OnMouseDown = MeGeradoMouseDown
          OnMouseUp = MeGeradoMouseUp
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Erros e Avisos '
        ImageIndex = 2
        object MeErros: TMemo
          Left = 0
          Top = 0
          Width = 1000
          Height = 315
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          WordWrap = False
          OnKeyUp = MeGeradoKeyUp
          OnMouseDown = MeGeradoMouseDown
          OnMouseUp = MeGeradoMouseUp
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' Documentos sem defini'#231#227'o de Modelo Fiscal '
        ImageIndex = 3
        object Label4: TLabel
          Left = 0
          Top = 0
          Width = 373
          Height = 18
          Align = alTop
          Alignment = taCenter
          Caption = 'Duplo clique na linha abre a janela para edi'#231#227'o da NF'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 18
          Width = 1000
          Height = 371
          Align = alClient
          DataSource = DsErrMod
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'DataFiscal'
              Title.Caption = 'Data fiscal'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Terceiro'
              Width = 47
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TERCEIRO'
              Title.Caption = 'Nome do Cliente / Fornecedor'
              Width = 314
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ide_dEmi'
              Title.Caption = 'Emiss'#227'o'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ide_serie'
              Title.Caption = 'S'#233'ie'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ide_nNF'
              Title.Caption = 'N'#186' N.F.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMSTot_vNF'
              Title.Caption = 'Valor NF'
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        Visible = False
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        Visible = False
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 534
        Height = 32
        Caption = 'Exporta'#231#227'o de Arquivo SPED EFD ICMS/IPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 534
        Height = 32
        Caption = 'Exporta'#231#227'o de Arquivo SPED EFD ICMS/IPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 534
        Height = 32
        Caption = 'Exporta'#231#227'o de Arquivo SPED EFD ICMS/IPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel47: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 560
    Width = 1008
    Height = 71
    Align = alBottom
    TabOrder = 3
    object Panel16: TPanel
      Left = 2
      Top = 0
      Width = 1004
      Height = 21
      Align = alBottom
      ParentBackground = False
      TabOrder = 0
      object StatusBar: TStatusBar
        Left = 1
        Top = 1
        Width = 1002
        Height = 19
        Panels = <
          item
            Text = ' Posi'#231#227'o do cursor no texto gerado:'
            Width = 192
          end
          item
            Width = 100
          end
          item
            Text = ' Arquivo salvo:'
            Width = 96
          end
          item
            Width = 50
          end>
      end
    end
    object PainelConfirma: TPanel
      Left = 2
      Top = 21
      Width = 1004
      Height = 48
      Align = alBottom
      TabOrder = 1
      object Label2: TLabel
        Left = 152
        Top = 4
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
        Enabled = False
      end
      object Label3: TLabel
        Left = 268
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Data final:'
        Enabled = False
      end
      object Panel2: TPanel
        Left = 872
        Top = 1
        Width = 131
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object TPDataIni: TdmkEditDateTimePicker
        Left = 152
        Top = 19
        Width = 113
        Height = 21
        Date = 44649.000000000000000000
        Time = 0.499141331019927700
        Enabled = False
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        LastDayTimePicker = TPDataFim
        DatePurpose = dmkdpNone
      end
      object TPDataFim: TdmkEditDateTimePicker
        Left = 268
        Top = 19
        Width = 113
        Height = 21
        Date = 44649.000000000000000000
        Time = 0.499141331019927700
        Enabled = False
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object BtErros: TBitBtn
        Left = 460
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Erros'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtErrosClick
      end
      object BtEstoque: TBitBtn
        Tag = 5
        Left = 584
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Esto&que'
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtEstoqueClick
      end
    end
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pem.SPED_EFD_IND_PERFIL, pem.SPED_EFD_IND_ATIV,'
      'pem.SPED_EFD_CadContador, pem.SPED_EFD_CRCContador,'
      'pem.SPED_EFD_EscriContab, pem.SPED_EFD_EnderContab,'
      'SPED_EFD_Path,'
      'IF(ctd.Tipo=0, ctd.RazaoSocial, ctd.Nome) NO_CTD,'
      'ctd.CPF CPF_CTD, ctb.CNPJ CNPJ_CTB,'
      'IF(ctb.Tipo=0, ctb.RazaoSocial, ctb.Nome) NO_CTB'
      'FROM paramsemp pem'
      'LEFT JOIN entidades ctd ON ctd.Codigo=pem.SPED_EFD_CadContador'
      'LEFT JOIN entidades ctb ON ctb.Codigo=pem.SPED_EFD_EscriContab'
      'WHERE pem.Codigo=:P0'
      ''
      '')
    Left = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsEmpSPED_EFD_IND_PERFIL: TWideStringField
      FieldName = 'SPED_EFD_IND_PERFIL'
      Size = 1
    end
    object QrParamsEmpSPED_EFD_IND_ATIV: TSmallintField
      FieldName = 'SPED_EFD_IND_ATIV'
    end
    object QrParamsEmpSPED_EFD_CadContador: TIntegerField
      FieldName = 'SPED_EFD_CadContador'
    end
    object QrParamsEmpSPED_EFD_CRCContador: TWideStringField
      FieldName = 'SPED_EFD_CRCContador'
      Size = 15
    end
    object QrParamsEmpSPED_EFD_EscriContab: TIntegerField
      FieldName = 'SPED_EFD_EscriContab'
    end
    object QrParamsEmpSPED_EFD_EnderContab: TSmallintField
      FieldName = 'SPED_EFD_EnderContab'
    end
    object QrParamsEmpSPED_EFD_Path: TWideStringField
      FieldName = 'SPED_EFD_Path'
      Size = 255
    end
    object QrParamsEmpNO_CTD: TWideStringField
      FieldName = 'NO_CTD'
      Size = 100
    end
    object QrParamsEmpNO_CTB: TWideStringField
      FieldName = 'NO_CTB'
      Size = 100
    end
    object QrParamsEmpCPF_CTD: TWideStringField
      FieldName = 'CPF_CTD'
      Size = 18
    end
    object QrParamsEmpCNPJ_CTB: TWideStringField
      FieldName = 'CNPJ_CTB'
      Size = 18
    end
    object QrParamsEmpSPED_EFD_ID_0200: TSmallintField
      FieldName = 'SPED_EFD_ID_0200'
    end
    object QrParamsEmpSPED_EFD_ID_0150: TSmallintField
      FieldName = 'SPED_EFD_ID_0150'
    end
    object QrParamsEmpSPED_EFD_DtaFiscalSaida: TSmallintField
      FieldName = 'SPED_EFD_DtaFiscalSaida'
    end
    object QrParamsEmpSPED_EFD_DtaFiscalEntrada: TSmallintField
      FieldName = 'SPED_EFD_DtaFiscalEntrada'
      Required = True
    end
  end
  object QrEmpresa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo, ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, '
      'IE, ECodMunici, PCodMunici, NIRE, SUFRAMA,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOME_ENT, '
      'IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJ_CPF, '
      'IF(en.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF, '
      'IF(en.Tipo=0, en.Fantasia, en.Apelido) NO_2_ENT, '
      'IF(en.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD, '
      'IF(en.Tipo=0, en.ERua, en.PRua) RUA, '
      'IF(en.Tipo=0, en.ECompl, en.PCompl) COMPL,'
      'IF(en.Tipo=0, en.EBairro, en.PBairro) BAIRRO,'
      'IF(en.Tipo=0, en.ETe1, en.PTe1) TE1,'
      'IF(en.Tipo=0, en.EFax, en.PFax) FAX,'
      'IF(en.Tipo=0, en.EEmail, en.PEmail) EMAIL,'
      'en.Codigo'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      ''
      'WHERE en.Codigo=:P0'
      '')
    Left = 44
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmpresaTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmpresaENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEmpresaPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrEmpresaELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrEmpresaPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrEmpresaECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEmpresaPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrEmpresaNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrEmpresaCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEmpresaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEmpresaIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmpresaECodMunici: TIntegerField
      FieldName = 'ECodMunici'
    end
    object QrEmpresaPCodMunici: TIntegerField
      FieldName = 'PCodMunici'
    end
    object QrEmpresaNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEmpresaSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
    object QrEmpresaNO_2_ENT: TWideStringField
      FieldName = 'NO_2_ENT'
      Size = 60
    end
    object QrEmpresaNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEmpresaRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEmpresaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEmpresaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEmpresaTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEmpresaFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEmpresaEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
  end
  object QrVersao: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COD_VER'
      'FROM spedefdvers'
      'WHERE (DT_FIN >=:P0)'
      'OR (DT_INI <:P1 AND DT_FIN<2)'
      ' ')
    Left = 24
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrVersaoCodTxt: TWideStringField
      FieldName = 'CodTxt'
      Size = 60
    end
  end
  object QrCampos: TMySQLQuery
    Database = Dmod.MyDB
    Left = 216
    Top = 64
    object QrCamposBloco: TWideStringField
      FieldName = 'Bloco'
      Origin = 'spedefdflds.Bloco'
      Size = 1
    end
    object QrCamposRegistro: TWideStringField
      FieldName = 'Registro'
      Origin = 'spedefdflds.Registro'
      Size = 4
    end
    object QrCamposNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'spedefdflds.Numero'
    end
    object QrCamposVersaoIni: TIntegerField
      FieldName = 'VersaoIni'
      Origin = 'spedefdflds.VersaoIni'
    end
    object QrCamposVersaoFim: TIntegerField
      FieldName = 'VersaoFim'
      Origin = 'spedefdflds.VersaoFim'
    end
    object QrCamposCampo: TWideStringField
      FieldName = 'Campo'
      Origin = 'spedefdflds.Campo'
      Size = 50
    end
    object QrCamposTipo: TWideStringField
      FieldName = 'Tipo'
      Origin = 'spedefdflds.Tipo'
      Size = 1
    end
    object QrCamposTam: TSmallintField
      FieldName = 'Tam'
      Origin = 'spedefdflds.Tam'
    end
    object QrCamposTObrig: TSmallintField
      FieldName = 'TObrig'
      Origin = 'spedefdflds.TObrig'
    end
    object QrCamposDecimais: TSmallintField
      FieldName = 'Decimais'
      Origin = 'spedefdflds.Decimais'
    end
    object QrCamposDescrLin1: TWideStringField
      FieldName = 'DescrLin1'
      Origin = 'spedefdflds.DescrLin1'
      Size = 80
    end
    object QrCamposDescrLin2: TWideStringField
      FieldName = 'DescrLin2'
      Origin = 'spedefdflds.DescrLin2'
      Size = 80
    end
    object QrCamposDescrLin3: TWideStringField
      FieldName = 'DescrLin3'
      Origin = 'spedefdflds.DescrLin3'
      Size = 80
    end
    object QrCamposDescrLin4: TWideStringField
      FieldName = 'DescrLin4'
      Origin = 'spedefdflds.DescrLin4'
      Size = 80
    end
    object QrCamposDescrLin5: TWideStringField
      FieldName = 'DescrLin5'
      Origin = 'spedefdflds.DescrLin5'
      Size = 80
    end
    object QrCamposDescrLin6: TWideStringField
      FieldName = 'DescrLin6'
      Origin = 'spedefdflds.DescrLin6'
      Size = 80
    end
    object QrCamposDescrLin7: TWideStringField
      FieldName = 'DescrLin7'
      Origin = 'spedefdflds.DescrLin7'
      Size = 80
    end
    object QrCamposDescrLin8: TWideStringField
      FieldName = 'DescrLin8'
      Origin = 'spedefdflds.DescrLin8'
      Size = 80
    end
    object QrCamposCObrig: TWideStringField
      FieldName = 'CObrig'
      Origin = 'spedefdflds.CObrig'
      Size = 2
    end
    object QrCamposEObrig: TWideStringField
      FieldName = 'EObrig'
      Origin = 'spedefdflds.CObrig'
      Size = 2
    end
    object QrCamposSObrig: TWideStringField
      FieldName = 'SObrig'
      Origin = 'spedefdflds.CObrig'
      Size = 2
    end
  end
  object QrEnder: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo, ENumero, PNumero, ELograd, PLograd, '
      'ECEP, PCEP, IE, ECodMunici, PCodMunici,'
      ' NIRE, SUFRAMA, ECodiPais, PCodiPais,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOME_ENT, '
      'IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJ_CPF, '
      'IF(en.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF, '
      'IF(en.Tipo=0, en.Fantasia, en.Apelido) NO_2_ENT, '
      'IF(en.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD, '
      'IF(en.Tipo=0, en.ERua, en.PRua) RUA, '
      'IF(en.Tipo=0, en.ECompl, en.PCompl) COMPL,'
      'IF(en.Tipo=0, en.EBairro, en.PBairro) BAIRRO,'
      'IF(en.Tipo=0, en.ETe1, en.PTe1) TE1,'
      'IF(en.Tipo=0, en.EFax, en.PFax) FAX,'
      'IF(en.Tipo=0, en.EEmail, en.PEmail) EMAIL,'
      'en.Codigo'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      ''
      'WHERE en.Codigo=:P0'
      '')
    Left = 808
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnderTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEnderENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEnderPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrEnderELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrEnderPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrEnderECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEnderPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrEnderIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEnderECodMunici: TIntegerField
      FieldName = 'ECodMunici'
    end
    object QrEnderPCodMunici: TIntegerField
      FieldName = 'PCodMunici'
    end
    object QrEnderNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEnderSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
    object QrEnderNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrEnderCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEnderNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEnderNO_2_ENT: TWideStringField
      FieldName = 'NO_2_ENT'
      Size = 60
    end
    object QrEnderNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEnderRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEnderCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEnderBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEnderTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEnderFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEnderEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrEnderCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnderECodiPais: TIntegerField
      FieldName = 'ECodiPais'
    end
    object QrEnderPCodiPais: TIntegerField
      FieldName = 'PCodiPais'
    end
  end
  object QrSelEnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT CodInfoEmit Entidade'
      'FROM nfecaba'
      'WHERE ide_tpAmb<>2 '
      'AND Empresa=:P0'
      'AND CodInfoEmit <>:P1'
      'AND DataFiscal BETWEEN :P2 AND :P3'
      'AND Status IN (100,101,102,110,301)'
      ''
      'UNION'
      ''
      'SELECT DISTINCT CodInfoDest Entidade'
      'FROM nfecaba'
      'WHERE ide_tpAmb<>2 '
      'AND Empresa=:P0'
      'AND CodInfoEmit <>:P1'
      'AND DataFiscal BETWEEN :P2 AND :P3'
      'AND Status IN (100,101,102,110,301)'
      ''
      'ORDER BY Entidade')
    Left = 272
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSelEntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrUniMedi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT nfei.prod_uTrib'
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.EMpresa'
      'WHERE  nfea.ide_tpAmb<>2 '
      'AND nfea.Status IN (100,101,102,110,301)'
      'AND nfea.Empresa=:P0'
      'AND nfea.DataFiscal BETWEEN :P1 AND :P2'
      'ORDER BY prod_uTrib')
    Left = 300
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrUniMediNoUnidMed: TWideStringField
      FieldName = 'NoUnidMed'
      Size = 6
    end
  end
  object QrForca: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfea.DataFiscal, nfea.CriAForca, '
      'nfei.FatID, nfei.FatNum, nfei.Empresa, '
      'nfei.nItem, nfei.GraGruX GGX_NfeI, '
      'nfei.prod_uTrib, smia.GraGruX GGX_SMIA'
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.EMpresa'
      'LEFT JOIN stqmovitsa smia ON smia.Tipo=nfei.FatID'
      '     AND smia.OriCodi=nfei.FatNum'
      '     AND smia.Empresa=nfei.EMpresa'
      '     AND smia.OriCtrl=nfei.MeuID'
      'WHERE nfea.CriAForca = 1'
      'AND smia.GraGruX <> 0'
      'AND (smia.GraGruX <> nfei.GraGruX'
      '     OR nfei.prod_uTrib = '#39#39
      '     )')
    Left = 328
    Top = 64
    object QrForcaDataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrForcaCriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
    object QrForcaFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrForcaFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrForcaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrForcanItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrForcaGGX_NfeI: TIntegerField
      FieldName = 'GGX_NfeI'
    end
    object QrForcaprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrForcaGGX_SMIA: TIntegerField
      FieldName = 'GGX_SMIA'
    end
  end
  object QrGGX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.UnidMed,'
      'unm.Sigla'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 356
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGGXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGGXSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
  end
  object QrCorrige001: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa, '
      'nfei.nItem, nfei.prod_cProd '
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.Empresa'
      'WHERE nfea.Empresa=nfea.CodInfoEmit'
      'AND GraGruX=0'
      'AND prod_cProd <> '#39#39)
    Left = 44
    Top = 484
    object QrCorrige001FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCorrige001FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCorrige001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCorrige001nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrCorrige001prod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
  end
  object QrCorrige1x3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa, '
      'nfei.nItem, nfei.Nivel1'
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.Empresa'
      'WHERE nfei.FatID IN (103,113)'
      'AND GraGruX=0'
      'AND Nivel1 < 0')
    Left = 44
    Top = 528
    object QrCorrige1x3FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCorrige1x3FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCorrige1x3Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCorrige1x3nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrCorrige1x3Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
  object QrReduzidos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT nfei.GraGruX'
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.EMpresa'
      'WHERE nfea.ide_tpAmb<>2 '
      'AND Status IN (100,101,102,110,301)'
      'AND nfea.Empresa=:P0'
      'AND nfea.DataFiscal BETWEEN :P1 AND :P2'
      'AND GraGruX <> 0'
      'ORDER BY GraGruX')
    Left = 468
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrReduzidosGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrSPEDEFDIcmsIpiErrs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM SPEDEFDErrs'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      ''
      ''
      '')
    Left = 44
    Top = 392
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrProduto2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.cGTIN_EAN , gg1.NCM,'
      'gg1.EX_TIPI, gg1.COD_LST, gg1.SPEDEFD_ALIQ_ICMS,'
      'unm.SIGLA, pgt.Tipo_Item'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle')
    Left = 120
    Top = 448
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProduto2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProduto2NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrProduto2cGTIN_EAN: TWideStringField
      FieldName = 'cGTIN_EAN'
      Size = 14
    end
    object QrProduto2NCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrProduto2EX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Size = 3
    end
    object QrProduto2COD_LST: TWideStringField
      FieldName = 'COD_LST'
      Size = 4
    end
    object QrProduto2SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrProduto2Tipo_Item: TSmallintField
      FieldName = 'Tipo_Item'
    end
    object QrProduto2SPEDEFD_ALIQ_ICMS: TFloatField
      FieldName = 'SPEDEFD_ALIQ_ICMS'
    end
    object QrProduto2prod_CEST: TIntegerField
      FieldName = 'prod_CEST'
    end
    object QrProduto2GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
  end
  object QrCabA_C: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'FatID, FatNum, Empresa, IDCtrl, Id,  ide_indPag, ide_mod, '
      'ide_serie, ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF,'
      'ide_tpAmb, ide_finNFe, emit_CNPJ, emit_CPF, '
      'dest_CNPJ, dest_CPF, '
      'ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST, '
      'ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete, '
      'ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII, '
      'ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS, '
      'ICMSTot_vOutro, ICMSTot_vNF, ISSQNtot_vServ, '
      'ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS, '
      'ISSQNtot_vCOFINS, RetTrib_vRetPIS, RetTrib_vRetCOFINS, '
      'RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF, '
      'RetTrib_vBCRetPrev, RetTrib_vRetPrev, ModFrete, '
      'Cobr_Fat_nFat, Cobr_Fat_vOrig, '
      'Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco, '
      'InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq, '
      'Compra_XNEmp, Compra_XPed, Compra_XCont, '
      ''
      'Status, infProt_Id, '
      'infProt_cStat, infCanc_cStat, '
      ''
      'ICMSRec_pRedBC, '
      'ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS, '
      'IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq, '
      'IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC, '
      'PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC, '
      'COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS, '
      
        'DataFiscal, SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_E' +
        'xpNat, '
      'SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum, '
      'SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais, '
      'SINTEGRA_ExpAverDta, CodInfoEmit, CodInfoDest, '
      'CriAForca, ide_hSaiEnt, ide_dhCont, '
      'emit_CRT, dest_email, '
      'Vagao, Balsa, CodInfoTrsp, '
      'OrdemServ, Situacao, Antigo, '
      'NFG_Serie, NF_ICMSAlq, NF_CFOP, '
      'Importado, NFG_SubSerie, NFG_ValIsen, '
      'NFG_NaoTrib, NFG_Outros, COD_MOD, COD_SIT, VL_ABAT_NT,'
      'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk, FisRegCad'
      'FROM nfecaba nfea '
      'WHERE ide_tpAmb<>2'
      'AND Status IN (100,101,102,110,301)'
      'AND  ('
      '  COD_MOD IN ('#39'01'#39','#39'1B'#39','#39'04'#39','#39'55'#39')'
      '  OR ide_mod IN (1,55)'
      ')'
      'AND Empresa=:P0'
      'AND DataFiscal BETWEEN :P1 AND :P2'
      '')
    Left = 120
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCabA_CFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCabA_CFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCabA_CEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCabA_CIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCabA_CId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrCabA_Cide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrCabA_Cide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrCabA_Cide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrCabA_Cide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrCabA_Cide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrCabA_Cide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrCabA_Cide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrCabA_Cide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrCabA_Cide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrCabA_Cemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrCabA_Cemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrCabA_Cdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrCabA_Cdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrCabA_CICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrCabA_CICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrCabA_CICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrCabA_CICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrCabA_CICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrCabA_CICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrCabA_CICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrCabA_CICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrCabA_CICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrCabA_CICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrCabA_CICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrCabA_CICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrCabA_CICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrCabA_CICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrCabA_CISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
    end
    object QrCabA_CISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
    end
    object QrCabA_CISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
    end
    object QrCabA_CISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
    end
    object QrCabA_CISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
    end
    object QrCabA_CRetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
    end
    object QrCabA_CRetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
    end
    object QrCabA_CRetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
    end
    object QrCabA_CRetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
    end
    object QrCabA_CRetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
    end
    object QrCabA_CRetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
    end
    object QrCabA_CRetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
    end
    object QrCabA_CModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrCabA_CCobr_Fat_nFat: TWideStringField
      FieldName = 'Cobr_Fat_nFat'
      Size = 60
    end
    object QrCabA_CCobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
    end
    object QrCabA_CCobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
    end
    object QrCabA_CCobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
    end
    object QrCabA_CInfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCabA_CInfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCabA_CExporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Size = 2
    end
    object QrCabA_CExporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Size = 60
    end
    object QrCabA_CCompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrCabA_CCompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrCabA_CCompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
    object QrCabA_CStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrCabA_CinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrCabA_CinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrCabA_CinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrCabA_CDataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrCabA_CSINTEGRA_ExpDeclNum: TWideStringField
      FieldName = 'SINTEGRA_ExpDeclNum'
      Size = 11
    end
    object QrCabA_CSINTEGRA_ExpDeclDta: TDateField
      FieldName = 'SINTEGRA_ExpDeclDta'
    end
    object QrCabA_CSINTEGRA_ExpNat: TWideStringField
      FieldName = 'SINTEGRA_ExpNat'
      Size = 1
    end
    object QrCabA_CSINTEGRA_ExpRegNum: TWideStringField
      FieldName = 'SINTEGRA_ExpRegNum'
      Size = 12
    end
    object QrCabA_CSINTEGRA_ExpRegDta: TDateField
      FieldName = 'SINTEGRA_ExpRegDta'
    end
    object QrCabA_CSINTEGRA_ExpConhNum: TWideStringField
      FieldName = 'SINTEGRA_ExpConhNum'
      Size = 16
    end
    object QrCabA_CSINTEGRA_ExpConhDta: TDateField
      FieldName = 'SINTEGRA_ExpConhDta'
    end
    object QrCabA_CSINTEGRA_ExpConhTip: TWideStringField
      FieldName = 'SINTEGRA_ExpConhTip'
      Size = 2
    end
    object QrCabA_CSINTEGRA_ExpPais: TWideStringField
      FieldName = 'SINTEGRA_ExpPais'
      Size = 4
    end
    object QrCabA_CSINTEGRA_ExpAverDta: TDateField
      FieldName = 'SINTEGRA_ExpAverDta'
    end
    object QrCabA_CCodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrCabA_CCodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object QrCabA_CCriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
    object QrCabA_Cide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrCabA_Cide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrCabA_Cemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
    object QrCabA_Cdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrCabA_CVagao: TWideStringField
      FieldName = 'Vagao'
    end
    object QrCabA_CBalsa: TWideStringField
      FieldName = 'Balsa'
    end
    object QrCabA_CCodInfoTrsp: TIntegerField
      FieldName = 'CodInfoTrsp'
    end
    object QrCabA_COrdemServ: TIntegerField
      FieldName = 'OrdemServ'
    end
    object QrCabA_CSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrCabA_CAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrCabA_CNFG_Serie: TWideStringField
      FieldName = 'NFG_Serie'
      Size = 3
    end
    object QrCabA_CNF_ICMSAlq: TFloatField
      FieldName = 'NF_ICMSAlq'
    end
    object QrCabA_CNF_CFOP: TWideStringField
      FieldName = 'NF_CFOP'
      Size = 4
    end
    object QrCabA_CImportado: TSmallintField
      FieldName = 'Importado'
    end
    object QrCabA_CNFG_SubSerie: TWideStringField
      FieldName = 'NFG_SubSerie'
      Size = 3
    end
    object QrCabA_CNFG_ValIsen: TFloatField
      FieldName = 'NFG_ValIsen'
    end
    object QrCabA_CNFG_NaoTrib: TFloatField
      FieldName = 'NFG_NaoTrib'
    end
    object QrCabA_CNFG_Outros: TFloatField
      FieldName = 'NFG_Outros'
    end
    object QrCabA_CCOD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrCabA_CCOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
    end
    object QrCabA_CVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
    end
    object QrCabA_CAtrelaFatID: TIntegerField
      FieldName = 'AtrelaFatID'
      Required = True
    end
    object QrCabA_CAtrelaStaLnk: TSmallintField
      FieldName = 'AtrelaStaLnk'
      Required = True
    end
    object QrCabA_CFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
      Required = True
    end
    object QrCabA_CAtrelaFatNum: TIntegerField
      FieldName = 'AtrelaFatNum'
    end
  end
  object QrCorrige002: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodInfoEmit, CodInfoDest,'
      'ide_tpNF, DataFiscal,'
      'FatID, FatNum, Empresa, IDCtrl, ide_mod, '
      'ide_serie, ide_nNF, ide_dEmi, ide_dSaiEnt, '
      'ide_finNFe, emit_CNPJ, emit_CPF, Status, '
      'NFG_Serie, NFG_SubSerie, COD_MOD'
      'FROM nfecaba nfea '
      'WHERE ide_tpAmb<>2'
      'AND COD_SIT in (98,99)'
      'AND Empresa=:P0'
      'AND DataFiscal BETWEEN :P1 AND :P2')
    Left = 44
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCorrige002FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCorrige002FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCorrige002Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCorrige002IDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCorrige002ide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrCorrige002ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrCorrige002ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrCorrige002ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrCorrige002ide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrCorrige002ide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrCorrige002emit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrCorrige002emit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrCorrige002Status: TIntegerField
      FieldName = 'Status'
    end
    object QrCorrige002NFG_Serie: TWideStringField
      FieldName = 'NFG_Serie'
      Size = 3
    end
    object QrCorrige002NFG_SubSerie: TWideStringField
      FieldName = 'NFG_SubSerie'
      Size = 3
    end
    object QrCorrige002COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrCorrige002CodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrCorrige002ide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrCorrige002DataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrCorrige002CodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
  end
  object QrItsI_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'ORDER BY nItem')
    Left = 120
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsI_FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrItsI_FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrItsI_Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrItsI_nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrItsI_GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrItsI_prod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrItsI_prod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrItsI_prod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrItsI_prod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrItsI_prod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrItsI_MeuID: TIntegerField
      FieldName = 'MeuID'
    end
    object QrItsI_prod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrItsI_prod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrItsI_prod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrItsI_prod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrItsI_prod_cBarra: TWideStringField
      FieldName = 'prod_cBarra'
      Size = 30
    end
    object QrItsI_prod_CEST: TIntegerField
      FieldName = 'prod_CEST'
      Required = True
    end
    object QrItsI_prod_indEscala: TWideStringField
      FieldName = 'prod_indEscala'
      Size = 1
    end
    object QrItsI_prod_CNPJFab: TWideStringField
      FieldName = 'prod_CNPJFab'
      Size = 14
    end
    object QrItsI_prod_cBenef: TWideStringField
      FieldName = 'prod_cBenef'
      Size = 10
    end
    object QrItsI_prod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrItsI_prod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrItsI_prod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrItsI_prod_qCom: TFloatField
      FieldName = 'prod_qCom'
      Required = True
    end
    object QrItsI_prod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
      Required = True
    end
    object QrItsI_prod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrItsI_prod_cBarraTrib: TWideStringField
      FieldName = 'prod_cBarraTrib'
      Size = 30
    end
    object QrItsI_prod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
      Required = True
    end
    object QrItsI_prod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Required = True
    end
    object QrItsI_prod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Required = True
    end
    object QrItsI_prod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrItsI_prod_indTot: TSmallintField
      FieldName = 'prod_indTot'
      Required = True
    end
    object QrItsI_prod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrItsI_prod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrItsI_Tem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
      Required = True
    end
    object QrItsI__Ativo_: TSmallintField
      FieldName = '_Ativo_'
      Required = True
    end
    object QrItsI_InfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Required = True
    end
    object QrItsI_EhServico: TIntegerField
      FieldName = 'EhServico'
      Required = True
    end
    object QrItsI_UsaSubsTrib: TSmallintField
      FieldName = 'UsaSubsTrib'
      Required = True
    end
    object QrItsI_ICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      Required = True
    end
    object QrItsI_ICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
      Required = True
    end
    object QrItsI_ICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      Required = True
    end
    object QrItsI_ICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
      Required = True
    end
    object QrItsI_IPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      Required = True
    end
    object QrItsI_IPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
      Required = True
    end
    object QrItsI_IPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      Required = True
    end
    object QrItsI_IPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
      Required = True
    end
    object QrItsI_PISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      Required = True
    end
    object QrItsI_PISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
      Required = True
    end
    object QrItsI_PISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      Required = True
    end
    object QrItsI_PISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
      Required = True
    end
    object QrItsI_COFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      Required = True
    end
    object QrItsI_COFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
      Required = True
    end
    object QrItsI_COFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      Required = True
    end
    object QrItsI_COFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
      Required = True
    end
    object QrItsI_Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrItsI_UnidMedCom: TIntegerField
      FieldName = 'UnidMedCom'
      Required = True
    end
    object QrItsI_UnidMedTrib: TIntegerField
      FieldName = 'UnidMedTrib'
      Required = True
    end
    object QrItsI_ICMSRec_vBCST: TFloatField
      FieldName = 'ICMSRec_vBCST'
      Required = True
    end
    object QrItsI_ICMSRec_vICMSST: TFloatField
      FieldName = 'ICMSRec_vICMSST'
      Required = True
    end
    object QrItsI_ICMSRec_pAliqST: TFloatField
      FieldName = 'ICMSRec_pAliqST'
      Required = True
    end
    object QrItsI_Tem_II: TSmallintField
      FieldName = 'Tem_II'
      Required = True
    end
    object QrItsI_prod_nFCI: TWideStringField
      FieldName = 'prod_nFCI'
      Size = 36
    end
    object QrItsI_StqMovValA: TIntegerField
      FieldName = 'StqMovValA'
      Required = True
    end
    object QrItsI_AtrelaID: TIntegerField
      FieldName = 'AtrelaID'
      Required = True
    end
  end
  object QrItsN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 188
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsNnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrItsNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrItsNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrItsNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrItsNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrItsNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrItsNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrItsNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrItsNCOD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrItsNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrItsNICMS_vICMSOp: TFloatField
      FieldName = 'ICMS_vICMSOp'
    end
    object QrItsNICMS_pDif: TFloatField
      FieldName = 'ICMS_pDif'
    end
    object QrItsNICMS_vICMSDif: TFloatField
      FieldName = 'ICMS_vICMSDif'
    end
    object QrItsNICMS_vICMSDeson: TFloatField
      FieldName = 'ICMS_vICMSDeson'
    end
    object QrItsNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrItsNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrItsNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrItsNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrItsNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrItsNICMS_CSOSN: TIntegerField
      FieldName = 'ICMS_CSOSN'
    end
    object QrItsNICMS_UFST: TWideStringField
      FieldName = 'ICMS_UFST'
      Size = 2
    end
    object QrItsNICMS_pBCOp: TFloatField
      FieldName = 'ICMS_pBCOp'
    end
    object QrItsNICMS_vBCSTRet: TFloatField
      FieldName = 'ICMS_vBCSTRet'
    end
    object QrItsNICMS_vICMSSTRet: TFloatField
      FieldName = 'ICMS_vICMSSTRet'
    end
    object QrItsNICMS_motDesICMS: TSmallintField
      FieldName = 'ICMS_motDesICMS'
    end
    object QrItsNICMS_pCredSN: TFloatField
      FieldName = 'ICMS_pCredSN'
    end
    object QrItsNICMS_vCredICMSSN: TFloatField
      FieldName = 'ICMS_vCredICMSSN'
    end
  end
  object QrStqMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Baixa '
      'FROM stqmovitsa'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1'
      'AND Empresa=:P2'
      'AND GraGruX=:P3'
      ''
      'UNION'
      ''
      'SELECT Baixa '
      'FROM stqmovitsb'
      'WHERE Tipo=:P4'
      'AND OriCodi=:P5'
      'AND Empresa=:P6'
      'AND GraGruX=:P7'
      '')
    Left = 704
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end>
    object QrStqMovItsBaixa: TSmallintField
      FieldName = 'Baixa'
      Required = True
    end
  end
  object QrItsO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 216
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsOIND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrItsOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrItsOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrItsOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
    end
    object QrItsOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrItsOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrItsOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrItsOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
  end
  object QrItsQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsq'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 244
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsQPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrItsQPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
    end
    object QrItsQPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
    end
    object QrItsQPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
    end
    object QrItsQPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
    end
    object QrItsQPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
    end
  end
  object QrItsS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitss'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 272
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsSCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrItsSCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrItsSCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
    end
    object QrItsSCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
    end
    object QrItsSCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
    end
    object QrItsSCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
    end
  end
  object QrGruI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT n.ICMS_Orig, n.ICMS_CST, i.ICMSRec_pAliq, '
      
        'SUM(i.ICMSRec_vBC) ICMSRec_vBC, SUM(i.ICMSRec_vICMS) ICMSRec_vIC' +
        'MS, '
      'i.prod_CFOP, SUM(i.prod_vProd + '
      'i.prod_vFrete + i.prod_vSeg + i.prod_vOutro) Valor,'
      'SUM(i.prod_vProd) prod_vProd, SUM(o.IPI_vIPI) IPI_vIPI    '
      'FROM nfeitsi i'
      'LEFT JOIN nfeitsn n ON n.FatID=i.FatID '
      '  AND n.FatNum=i.FatNum AND n.Empresa=i.Empresa'
      '  AND n.nItem=i.nItem'
      'LEFT JOIN nfeitso o ON o.FatID=i.FatID '
      '  AND o.FatNum=i.FatNum AND o.Empresa=i.Empresa'
      '  AND o.nItem=i.nItem'
      'WHERE i.FatID=:P0'
      'AND i.FatNum=:P1'
      'AND i.Empresa=:P2'
      'GROUP BY n.ICMS_Orig, n.ICMS_CST, i.ICMSRec_pAliq, i.prod_CFOP')
    Left = 120
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGruInCST: TSmallintField
      FieldName = 'nCST'
    end
    object QrGruInCFOP: TIntegerField
      FieldName = 'nCFOP'
    end
    object QrGruIValor: TFloatField
      FieldName = 'Valor'
    end
    object QrGruIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrGruIIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrGruIICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrGruIICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrGruIICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrGruIICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrGruIpICMS: TFloatField
      FieldName = 'pICMS'
    end
  end
  object QrCorrI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa, nfei.nItem, nfei.G' +
        'raGruX'
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.EMpresa'
      'WHERE prod_uTrib=""'
      'AND  nfea.Empresa=:P0'
      'AND nfea.DataFiscal BETWEEN :P1 AND :P2')
    Left = 752
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCorrIFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCorrIFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCorrIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCorrInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrCorrIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrErrCFOP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfi.FatID, nfi.FatNum, nfi.Empresa,  '
      'nfi.nItem, nfi.prod_CFOP, t002.* '
      'FROM nfeItsi nfi '
      'LEFT JOIN nfecaba nfa ON nfa.FatID=nfi.FatID '
      '     AND nfa.FatNum=nfi.FatNum '
      '     AND nfa.Empresa=nfi.Empresa '
      'LEFT JOIN tbspedefd002 t002 ON t002.Codigo=nfi.prod_CFOP '
      'WHERE nfa.DataFiscal BETWEEN "2010-01-01" AND "2010-01-31" '
      'AND t002.Codigo IS NULL ')
    Left = 780
    Top = 8
    object QrErrCFOPFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrErrCFOPFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrErrCFOPEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrErrCFOPnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrErrCFOPprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrErrCFOPCodTxt: TWideStringField
      FieldName = 'CodTxt'
      Required = True
    end
    object QrErrCFOPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrErrCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrErrCFOPDataIni: TDateField
      FieldName = 'DataIni'
      Required = True
    end
    object QrErrCFOPDataFim: TDateField
      FieldName = 'DataFim'
    end
  end
  object QrCFOP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM tbspedefd002'
      'WHERE Codigo="9999"')
    Left = 748
    Top = 52
  end
  object QrAllI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      
        'SUM(i.ICMSRec_vBC) ICMSRec_vBC, SUM(i.ICMSRec_vICMS) ICMSRec_vIC' +
        'MS, '
      'SUM(i.prod_vProd + '
      'i.prod_vFrete + i.prod_vSeg + i.prod_vOutro) Valor,'
      'SUM(i.prod_vProd) prod_vProd, SUM(o.IPI_vIPI) IPI_vIPI    '
      'FROM nfeitsi i'
      'LEFT JOIN nfeitsn n ON n.FatID=i.FatID '
      '  AND n.FatNum=i.FatNum AND n.Empresa=i.Empresa'
      '  AND n.nItem=i.nItem'
      'LEFT JOIN nfeitso o ON o.FatID=i.FatID '
      '  AND o.FatNum=i.FatNum AND o.Empresa=i.Empresa'
      '  AND o.nItem=i.nItem'
      'WHERE i.FatID=:P0'
      'AND i.FatNum=:P1'
      'AND i.Empresa=:P2')
    Left = 120
    Top = 392
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrAllIValor: TFloatField
      FieldName = 'Valor'
    end
    object QrAllIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrAllIIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrAllIICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrAllIICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrAllIICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrAllIICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
  end
  object QrBlocos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _spedefd_blcs_'
      'ORDER BY Ordem, Bloco'
      '')
    Left = 628
    Top = 264
    object QrBlocosBloco: TWideStringField
      FieldName = 'Bloco'
      Size = 1
    end
    object QrBlocosOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrBlocosAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object QrErrMod: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfa.DataFiscal, nfa.FatID, nfa.FatNum, nfa.Empresa, '
      
        'nfa.ide_mod, nfa.ide_serie, nfa.ide_nNF, nfa.ide_dEmi, nfa.ICMST' +
        'ot_vNF, '
      
        'IF(nfa.CodInfoEmit=:P0, nfa.CodInfoDest, nfa.CodInfoEmit) Tercei' +
        'ro,'
      
        'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, nfa.IDCtr' +
        'l'
      'FROM nfecaba nfa'
      'LEFT JOIN entidades ent ON ent.Codigo='
      '     IF(CodInfoEmit=:P1, CodInfoDest, CodInfoEmit)'
      'WHERE ide_tpAmb<>2'
      'AND Status IN (100,101,102,110,301)'
      'AND DataFiscal BETWEEN :P2 AND :P3'
      'AND EFD_EXP_REG='#39#39
      'ORDER BY DataFiscal, ide_dEmi, NO_TERCEIRO')
    Left = 836
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrErrModDataFiscal: TDateField
      FieldName = 'DataFiscal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrErrModFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrErrModFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrErrModEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrErrModide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrErrModide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrErrModide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrErrModide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrErrModICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrErrModTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrErrModNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrErrModIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object DsErrMod: TDataSource
    DataSet = QrErrMod
    Left = 860
    Top = 8
  end
  object QrEFD_E100: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E100BeforeClose
    AfterScroll = QrEFD_E100AfterScroll
    SQL.Strings = (
      'SELECT * FROM efd_e100'
      'WHERE ImporExpor=3'
      'AND AnoMes=201001'
      'AND Empresa=-11')
    Left = 596
    Top = 440
    object QrEFD_E100DT_INI: TDateField
      FieldName = 'DT_INI'
    end
    object QrEFD_E100DT_FIN: TDateField
      FieldName = 'DT_FIN'
    end
    object QrEFD_E100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E100AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E100Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E100LinArq: TIntegerField
      FieldName = 'LinArq'
    end
  end
  object QrEFD_E110: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E110BeforeClose
    AfterScroll = QrEFD_E110AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e110 e110 '
      'WHERE e110.ImporExpor=3 '
      'AND e110.Empresa=-11 '
      'AND e110.AnoMes=201001'
      'AND e110.LinArq>0')
    Left = 596
    Top = 488
    object QrEFD_E110ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E110AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E110Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E110LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E110E100: TIntegerField
      FieldName = 'E100'
    end
    object QrEFD_E110REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E110VL_TOT_DEBITOS: TFloatField
      FieldName = 'VL_TOT_DEBITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_AJ_DEBITOS: TFloatField
      FieldName = 'VL_AJ_DEBITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_TOT_AJ_DEBITOS: TFloatField
      FieldName = 'VL_TOT_AJ_DEBITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_TOT_CREDITOS: TFloatField
      FieldName = 'VL_TOT_CREDITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_ESTORNOS_CRED: TFloatField
      FieldName = 'VL_ESTORNOS_CRED'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_AJ_CREDITOS: TFloatField
      FieldName = 'VL_AJ_CREDITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_TOT_AJ_CREDITOS: TFloatField
      FieldName = 'VL_TOT_AJ_CREDITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_ESTORNOS_DEB: TFloatField
      FieldName = 'VL_ESTORNOS_DEB'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_SLD_CREDOR_ANT: TFloatField
      FieldName = 'VL_SLD_CREDOR_ANT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_SLD_APURADO: TFloatField
      FieldName = 'VL_SLD_APURADO'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_TOT_DED: TFloatField
      FieldName = 'VL_TOT_DED'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_ICMS_RECOLHER: TFloatField
      FieldName = 'VL_ICMS_RECOLHER'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_SLD_CREDOR_TRANSPORTAR: TFloatField
      FieldName = 'VL_SLD_CREDOR_TRANSPORTAR'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110DEB_ESP: TFloatField
      FieldName = 'DEB_ESP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object QrEFD_E111: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E111BeforeClose
    AfterScroll = QrEFD_E111AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e111 e111'
      'WHERE e111.ImporExpor=3'
      'AND e111.Empresa=-11'
      'AND e111.AnoMes=201001'
      'AND e111.LinArq>0'
      '')
    Left = 780
    Top = 244
    object QrEFD_E111ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E111AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E111Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E111LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E111E110: TIntegerField
      FieldName = 'E110'
    end
    object QrEFD_E111REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E111COD_AJ_APUR: TWideStringField
      FieldName = 'COD_AJ_APUR'
      Size = 8
    end
    object QrEFD_E111DESCR_COMPL_AJ: TWideStringField
      FieldName = 'DESCR_COMPL_AJ'
      Size = 255
    end
    object QrEFD_E111VL_AJ_APUR: TFloatField
      FieldName = 'VL_AJ_APUR'
    end
  end
  object QrEFD_E115: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e115 e115'
      'WHERE e115.ImporExpor=3'
      'AND e115.Empresa=-11'
      'AND e115.AnoMes=201001'
      'AND e115.LinArq>0'
      '')
    Left = 864
    Top = 244
    object QrEFD_E115ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E115AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E115Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E115LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E115E110: TIntegerField
      FieldName = 'E110'
    end
    object QrEFD_E115REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E115COD_INF_ADIC: TWideStringField
      FieldName = 'COD_INF_ADIC'
      Size = 8
    end
    object QrEFD_E115VL_INF_ADIC: TFloatField
      FieldName = 'VL_INF_ADIC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E115DESCR_COMPL_AJ: TWideStringField
      FieldName = 'DESCR_COMPL_AJ'
      Size = 255
    end
  end
  object QrEFD_E116: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e116 e116'
      'WHERE e116.ImporExpor=3'
      'AND e116.Empresa=-11'
      'AND e116.AnoMes=201001'
      'AND e116.LinArq>0'
      '')
    Left = 892
    Top = 244
    object QrEFD_E116ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E116AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E116Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E116LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E116E110: TIntegerField
      FieldName = 'E110'
    end
    object QrEFD_E116REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E116COD_OR: TWideStringField
      FieldName = 'COD_OR'
      Size = 3
    end
    object QrEFD_E116VL_OR: TFloatField
      FieldName = 'VL_OR'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEFD_E116DT_VCTO: TDateField
      FieldName = 'DT_VCTO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrEFD_E116COD_REC: TWideStringField
      FieldName = 'COD_REC'
      Size = 255
    end
    object QrEFD_E116NUM_PROC: TWideStringField
      FieldName = 'NUM_PROC'
      Size = 15
    end
    object QrEFD_E116IND_PROC: TWideStringField
      FieldName = 'IND_PROC'
      Size = 1
    end
    object QrEFD_E116PROC: TWideStringField
      FieldName = 'PROC'
      Size = 255
    end
    object QrEFD_E116TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
    object QrEFD_E116MES_REF: TDateField
      FieldName = 'MES_REF'
    end
  end
  object QrEFD_E112: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e112 e112'
      'WHERE e112.ImporExpor=3'
      'AND e112.Empresa=-11'
      'AND e112.AnoMes=201001'
      'AND e112.LinArq>0'
      '')
    Left = 808
    Top = 244
    object QrEFD_E112ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E112AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E112Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E112LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E112E111: TIntegerField
      FieldName = 'E111'
    end
    object QrEFD_E112REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E112NUM_DA: TWideStringField
      FieldName = 'NUM_DA'
      Size = 255
    end
    object QrEFD_E112NUM_PROC: TWideStringField
      FieldName = 'NUM_PROC'
      Size = 15
    end
    object QrEFD_E112IND_PROC: TWideStringField
      FieldName = 'IND_PROC'
      Size = 1
    end
    object QrEFD_E112PROC: TWideStringField
      FieldName = 'PROC'
      Size = 255
    end
    object QrEFD_E112TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
  end
  object QrEFD_E113: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e113 e113'
      'WHERE e113.ImporExpor=3'
      'AND e113.Empresa=-11'
      'AND e113.AnoMes=201001'
      'AND e113.LinArq>0'
      '')
    Left = 836
    Top = 244
    object QrEFD_E113ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E113AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E113Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E113LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E113E111: TIntegerField
      FieldName = 'E111'
    end
    object QrEFD_E113REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E113COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_E113COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrEFD_E113SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEFD_E113SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEFD_E113NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrEFD_E113DT_DOC: TDateField
      FieldName = 'DT_DOC'
    end
    object QrEFD_E113COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_E113VL_AJ_ITEM: TFloatField
      FieldName = 'VL_AJ_ITEM'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object QrEFD_D100: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E100BeforeClose
    SQL.Strings = (
      'SELECT * FROM efd_d100'
      'WHERE ImporExpor<>2'
      'AND AnoMes=201001'
      'AND Empresa=-11')
    Left = 540
    Top = 460
    object QrEFD_D100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_D100AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_D100Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_D100LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_D100REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_D100IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Size = 1
    end
    object QrEFD_D100IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Size = 1
    end
    object QrEFD_D100COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_D100COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrEFD_D100COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Size = 2
    end
    object QrEFD_D100SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEFD_D100SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEFD_D100NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrEFD_D100CHV_CTE: TWideStringField
      FieldName = 'CHV_CTE'
      Size = 44
    end
    object QrEFD_D100DT_DOC: TDateField
      FieldName = 'DT_DOC'
    end
    object QrEFD_D100DT_A_P: TDateField
      FieldName = 'DT_A_P'
    end
    object QrEFD_D100TP_CTE: TSmallintField
      FieldName = 'TP_CTE'
      Required = True
    end
    object QrEFD_D100CHV_CTE_REF: TWideStringField
      FieldName = 'CHV_CTE_REF'
      Size = 44
    end
    object QrEFD_D100VL_DOC: TFloatField
      FieldName = 'VL_DOC'
    end
    object QrEFD_D100VL_DESC: TFloatField
      FieldName = 'VL_DESC'
    end
    object QrEFD_D100IND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEFD_D100VL_SERV: TFloatField
      FieldName = 'VL_SERV'
    end
    object QrEFD_D100VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrEFD_D100VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrEFD_D100VL_NT: TFloatField
      FieldName = 'VL_NT'
    end
    object QrEFD_D100COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrEFD_D100COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEFD_D100Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEFD_D100Importado: TSmallintField
      FieldName = 'Importado'
    end
    object QrEFD_D100Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_D100DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_D100DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_D100UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_D100UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_D100AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_D100Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_D100CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrEFD_D100CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrEFD_D100ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrEFD_D100VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
  end
  object QrBlcs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM spedefdblcs'
      'WHERE Implementd <> 0'
      'ORDER BY Ordem, Registro')
    Left = 356
    Top = 92
    object QrBlcsBloco: TWideStringField
      FieldName = 'Bloco'
      Size = 1
    end
    object QrBlcsRegistro: TWideStringField
      FieldName = 'Registro'
      Size = 4
    end
    object QrBlcsNivel: TSmallintField
      FieldName = 'Nivel'
    end
    object QrBlcsRegisPai: TWideStringField
      FieldName = 'RegisPai'
      Size = 4
    end
    object QrBlcsOcorAciNiv: TIntegerField
      FieldName = 'OcorAciNiv'
    end
    object QrBlcsOcorEstNiv: TIntegerField
      FieldName = 'OcorEstNiv'
    end
    object QrBlcsImplementd: TSmallintField
      FieldName = 'Implementd'
    end
    object QrBlcsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrBlcsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrNiv2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Registro '
      'FROM _spedefd_blcs_'
      'WHERE Nivel=2'
      'AND Implementd <> 0'
      'AND ('
      '  Bloco = "C"'
      '  OR'
      '  Bloco = "D"'
      ')')
    Left = 384
    Top = 92
    object QrNiv2Registro: TWideStringField
      FieldName = 'Registro'
      Size = 4
    end
  end
  object QrEFD_C500: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) '
      'NO_TERC, c500.*'
      'FROM efdicmsipic500 c500 '
      'LEFT JOIN entidades ent ON ent.Codigo=c500.Terceiro'
      'WHERE c500.ImporExpor=3 '
      'AND c500.Empresa=-11 '
      'AND c500.AnoMes=201001'
      '')
    Left = 540
    Top = 408
    object QrEFD_C500NO_TERC: TWideStringField
      FieldName = 'NO_TERC'
      Size = 100
    end
    object QrEFD_C500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_C500AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_C500Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_C500LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_C500REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_C500IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Size = 1
    end
    object QrEFD_C500IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Size = 1
    end
    object QrEFD_C500COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_C500COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrEFD_C500COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Size = 2
    end
    object QrEFD_C500SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEFD_C500SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEFD_C500COD_CONS: TWideStringField
      FieldName = 'COD_CONS'
      Size = 2
    end
    object QrEFD_C500NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrEFD_C500DT_DOC: TDateField
      FieldName = 'DT_DOC'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_C500DT_E_S: TDateField
      FieldName = 'DT_E_S'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_C500VL_DOC: TFloatField
      FieldName = 'VL_DOC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_DESC: TFloatField
      FieldName = 'VL_DESC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_FORN: TFloatField
      FieldName = 'VL_FORN'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_SERV_NT: TFloatField
      FieldName = 'VL_SERV_NT'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_TERC: TFloatField
      FieldName = 'VL_TERC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_DA: TFloatField
      FieldName = 'VL_DA'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrEFD_C500VL_PIS: TFloatField
      FieldName = 'VL_PIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500TP_LIGACAO: TWideStringField
      FieldName = 'TP_LIGACAO'
      Size = 1
    end
    object QrEFD_C500COD_GRUPO_TENSAO: TWideStringField
      FieldName = 'COD_GRUPO_TENSAO'
      Size = 2
    end
    object QrEFD_C500Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEFD_C500Importado: TSmallintField
      FieldName = 'Importado'
    end
    object QrEFD_C500CST_ICMS: TWideStringField
      FieldName = 'CST_ICMS'
      Size = 3
    end
    object QrEFD_C500CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrEFD_C500ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500CHV_DOCe: TWideStringField
      FieldName = 'CHV_DOCe'
      Size = 44
    end
    object QrEFD_C500FIN_DOCe: TSmallintField
      FieldName = 'FIN_DOCe'
      Required = True
    end
    object QrEFD_C500CHV_DOCe_REF: TWideStringField
      FieldName = 'CHV_DOCe_REF'
      Size = 44
    end
    object QrEFD_C500IND_DEST: TSmallintField
      FieldName = 'IND_DEST'
      Required = True
    end
    object QrEFD_C500COD_MUN_DEST: TIntegerField
      FieldName = 'COD_MUN_DEST'
      Required = True
    end
    object QrEFD_C500COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 50
    end
    object QrEFD_C500COD_MOD_DOC_REF: TSmallintField
      FieldName = 'COD_MOD_DOC_REF'
      Required = True
    end
    object QrEFD_C500HASH_DOC_REF: TWideStringField
      FieldName = 'HASH_DOC_REF'
      Size = 32
    end
    object QrEFD_C500SER_DOC_REF: TWideStringField
      FieldName = 'SER_DOC_REF'
      Size = 4
    end
    object QrEFD_C500NUM_DOC_REF: TIntegerField
      FieldName = 'NUM_DOC_REF'
      Required = True
    end
    object QrEFD_C500MES_DOC_REF: TIntegerField
      FieldName = 'MES_DOC_REF'
      Required = True
    end
    object QrEFD_C500ENER_INJET: TFloatField
      FieldName = 'ENER_INJET'
      Required = True
    end
    object QrEFD_C500OUTRAS_DED: TFloatField
      FieldName = 'OUTRAS_DED'
      Required = True
    end
  end
  object QrEFD_D500: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E100BeforeClose
    SQL.Strings = (
      'SELECT * FROM efd_d500'
      'WHERE ImporExpor<>2'
      'AND AnoMes=201001'
      'AND Empresa=-11')
    Left = 540
    Top = 508
    object QrEFD_D500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_D500AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_D500Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_D500LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_D500REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_D500IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Size = 1
    end
    object QrEFD_D500IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Size = 1
    end
    object QrEFD_D500COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_D500COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrEFD_D500COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Size = 2
    end
    object QrEFD_D500SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEFD_D500SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEFD_D500NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrEFD_D500DT_DOC: TDateField
      FieldName = 'DT_DOC'
    end
    object QrEFD_D500DT_A_P: TDateField
      FieldName = 'DT_A_P'
    end
    object QrEFD_D500VL_DOC: TFloatField
      FieldName = 'VL_DOC'
    end
    object QrEFD_D500VL_DESC: TFloatField
      FieldName = 'VL_DESC'
    end
    object QrEFD_D500VL_SERV: TFloatField
      FieldName = 'VL_SERV'
    end
    object QrEFD_D500VL_SERV_NT: TFloatField
      FieldName = 'VL_SERV_NT'
    end
    object QrEFD_D500VL_TERC: TFloatField
      FieldName = 'VL_TERC'
    end
    object QrEFD_D500VL_DA: TFloatField
      FieldName = 'VL_DA'
    end
    object QrEFD_D500VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrEFD_D500VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrEFD_D500COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrEFD_D500VL_PIS: TFloatField
      FieldName = 'VL_PIS'
    end
    object QrEFD_D500VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
    end
    object QrEFD_D500COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEFD_D500TP_ASSINANTE: TWideStringField
      FieldName = 'TP_ASSINANTE'
      Size = 1
    end
    object QrEFD_D500Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEFD_D500Importado: TSmallintField
      FieldName = 'Importado'
    end
    object QrEFD_D500CST_ICMS: TWideStringField
      FieldName = 'CST_ICMS'
      Size = 3
    end
    object QrEFD_D500CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrEFD_D500ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrEFD_D500VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
  end
  object QrEFD_E500: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E100BeforeClose
    AfterScroll = QrEFD_E100AfterScroll
    SQL.Strings = (
      'SELECT * FROM efd_e100'
      'WHERE ImporExpor=3'
      'AND AnoMes=201001'
      'AND Empresa=-11')
    Left = 648
    Top = 416
    object QrEFD_E500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E500AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E500Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E500LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E500IND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrEFD_E500DT_INI: TDateField
      FieldName = 'DT_INI'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_E500DT_FIN: TDateField
      FieldName = 'DT_FIN'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object QrEFD_E520: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E520BeforeClose
    AfterScroll = QrEFD_E520AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e520 E520 ')
    Left = 648
    Top = 464
    object QrEFD_E520ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E520AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E520Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E520LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E520E500: TIntegerField
      FieldName = 'E500'
    end
    object QrEFD_E520REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E520VL_SD_ANT_IPI: TFloatField
      FieldName = 'VL_SD_ANT_IPI'
    end
    object QrEFD_E520VL_DEB_IPI: TFloatField
      FieldName = 'VL_DEB_IPI'
    end
    object QrEFD_E520VL_CRED_IPI: TFloatField
      FieldName = 'VL_CRED_IPI'
    end
    object QrEFD_E520VL_OD_IPI: TFloatField
      FieldName = 'VL_OD_IPI'
    end
    object QrEFD_E520VL_OC_IPI: TFloatField
      FieldName = 'VL_OC_IPI'
    end
    object QrEFD_E520VL_SC_IPI: TFloatField
      FieldName = 'VL_SC_IPI'
    end
    object QrEFD_E520VL_SD_IPI: TFloatField
      FieldName = 'VL_SD_IPI'
    end
  end
  object QrEFD_E530: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e520 E520 ')
    Left = 648
    Top = 512
    object QrEFD_E530ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E530AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E530Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E530LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E530E520: TIntegerField
      FieldName = 'E520'
    end
    object QrEFD_E530REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E530IND_AJ: TWideStringField
      FieldName = 'IND_AJ'
      Size = 1
    end
    object QrEFD_E530VL_AJ: TFloatField
      FieldName = 'VL_AJ'
    end
    object QrEFD_E530COD_AJ: TWideStringField
      FieldName = 'COD_AJ'
      Size = 3
    end
    object QrEFD_E530IND_DOC: TWideStringField
      FieldName = 'IND_DOC'
      Size = 1
    end
    object QrEFD_E530NUM_DOC: TWideStringField
      FieldName = 'NUM_DOC'
      Size = 255
    end
    object QrEFD_E530DESCR_AJ: TWideStringField
      FieldName = 'DESCR_AJ'
      Size = 255
    end
  end
  object QrEFD_H005: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrEFD_H005AfterScroll
    SQL.Strings = (
      'SELECT * FROM efd_h005')
    Left = 700
    Top = 392
    object QrEFD_H005ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_H005AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_H005Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_H005LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_H005REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_H005DT_INV: TDateField
      FieldName = 'DT_INV'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_H005VL_INV: TFloatField
      FieldName = 'VL_INV'
    end
    object QrEFD_H005MOT_INV: TWideStringField
      FieldName = 'MOT_INV'
      Size = 2
    end
    object QrEFD_H005Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_H005DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_H005DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_H005UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_H005UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_H005AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_H005Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrEFD_H010: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_H010BeforeClose
    AfterScroll = QrEFD_H010AfterScroll
    SQL.Strings = (
      'SELECT h010.* '
      'FROM efd_h010 h010'
      'WHERE h010.ImporExpor=3'
      'AND h010.Empresa=-11'
      'AND h010.AnoMes=201401'
      'AND h010.H005=1'
      '')
    Left = 700
    Top = 440
    object QrEFD_H010ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_H010AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_H010Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_H010H005: TIntegerField
      FieldName = 'H005'
    end
    object QrEFD_H010LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_H010REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_H010COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_H010UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrEFD_H010QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrEFD_H010VL_UNIT: TFloatField
      FieldName = 'VL_UNIT'
      DisplayFormat = '#,###,###,##0.000000'
    end
    object QrEFD_H010VL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_H010IND_PROP: TWideStringField
      FieldName = 'IND_PROP'
      Size = 1
    end
    object QrEFD_H010COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_H010TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
    object QrEFD_H010COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEFD_H010VL_ITEM_IR: TFloatField
      FieldName = 'VL_ITEM_IR'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_H010Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_H010DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_H010DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_H010UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_H010UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_H010AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_H010Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrEFD_H020: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM efd_h020')
    Left = 700
    Top = 488
    object QrEFD_H020ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_H020AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_H020Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_H020H010: TIntegerField
      FieldName = 'H010'
    end
    object QrEFD_H020LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_H020REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_H020CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrEFD_H020BC_ICMS: TFloatField
      FieldName = 'BC_ICMS'
    end
    object QrEFD_H020VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrEFD_H020Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_H020DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_H020DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_H020UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_H020UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_H020AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_H020Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrProduto1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.cGTIN_EAN , gg1.NCM,'
      'gg1.EX_TIPI, gg1.COD_LST, gg1.SPEDEFD_ALIQ_ICMS,'
      'unm.SIGLA, pgt.Tipo_Item'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle')
    Left = 496
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProduto1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProduto1NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrProduto1cGTIN_EAN: TWideStringField
      FieldName = 'cGTIN_EAN'
      Size = 14
    end
    object QrProduto1NCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrProduto1EX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Size = 3
    end
    object QrProduto1COD_LST: TWideStringField
      FieldName = 'COD_LST'
      Size = 4
    end
    object QrProduto1SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrProduto1Tipo_Item: TSmallintField
      FieldName = 'Tipo_Item'
    end
    object QrProduto1SPEDEFD_ALIQ_ICMS: TFloatField
      FieldName = 'SPEDEFD_ALIQ_ICMS'
    end
  end
  object QrEFD_K100: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K100BeforeClose
    AfterScroll = QrEFD_K100AfterScroll
    SQL.Strings = (
      'SELECT h010.* '
      'FROM efd_h010 h010'
      'WHERE h010.ImporExpor=3'
      'AND h010.Empresa=-11'
      'AND h010.AnoMes=201401'
      'AND h010.H005=1'
      '')
    Left = 752
    Top = 320
    object QrEFD_K100DT_INI: TDateField
      FieldName = 'DT_INI'
    end
    object QrEFD_K100DT_FIN: TDateField
      FieldName = 'DT_FIN'
    end
    object QrEFD_K100PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
  end
  object QrEFD_K200: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DT_EST, COD_ITEM, IND_EST, COD_PART, Entidade,'
      'SUM(QTD) QTD'
      ' FROM efdicmsipik200 '
      'WHERE ImporExpor=3'
      'AND AnoMes=201812'
      'AND Empresa=-11'
      'AND PeriApu=1'
      'GROUP BY DT_EST, COD_ITEM, IND_EST, COD_PART, Entidade'
      'ORDER BY DT_EST, COD_ITEM, IND_EST, COD_PART, Entidade')
    Left = 752
    Top = 368
    object QrEFD_K200DT_EST: TDateField
      FieldName = 'DT_EST'
    end
    object QrEFD_K200COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K200IND_EST: TWideStringField
      FieldName = 'IND_EST'
      Size = 1
    end
    object QrEFD_K200COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_K200QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrEFD_K200Entidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object QrEFD_K220: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'CONCAT(gg1o.Nome, '
      'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), '
      'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) '
      'NO_PRD_TAM_COR_ORI, '
      'CONCAT(gg1d.Nome, '
      'IF(gtid.PrintTam=0, "", CONCAT(" ", gtid.Nome)), '
      'IF(gccd.PrintCor=0,"", CONCAT(" ", gccd.Nome))) '
      'NO_PRD_TAM_COR_DEST, '
      'K220.* '
      'FROM efd_K220 K220 '
      'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K220.COD_ITEM_ORI '
      'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC '
      'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad '
      'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI '
      'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 '
      'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed '
      ' '
      'LEFT JOIN gragrux    ggxd ON ggxd.Controle=K220.COD_ITEM_DEST '
      'LEFT JOIN gragruc    ggcd ON ggcd.Controle=ggxd.GraGruC '
      'LEFT JOIN gracorcad  gccd ON gccd.Codigo=ggcd.GraCorCad '
      'LEFT JOIN gratamits  gtid ON gtid.Controle=ggxd.GraTamI '
      'LEFT JOIN gragru1    gg1d ON gg1d.Nivel1=ggxd.GraGru1 '
      'LEFT JOIN unidmed    unmd ON unmd.Codigo=gg1d.UnidMed '
      'WHERE k220.ImporExpor=3'
      'AND k220.Empresa=-11'
      'AND k220.AnoMes=201601'
      'AND k220.K100=1'
      'ORDER BY k220.DT_MOV, COD_ITEM_ORI, COD_ITEM_DEST ')
    Left = 752
    Top = 512
    object QrEFD_K220DT_MOV: TDateField
      FieldName = 'DT_MOV'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_K220COD_ITEM_ORI: TWideStringField
      FieldName = 'COD_ITEM_ORI'
      Size = 60
    end
    object QrEFD_K220COD_ITEM_DEST: TWideStringField
      FieldName = 'COD_ITEM_DEST'
      Size = 60
    end
    object QrEFD_K220QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrEFD_K220ID_SEK: TIntegerField
      FieldName = 'ID_SEK'
    end
  end
  object QrEFD_K230: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K230BeforeClose
    AfterScroll = QrEFD_K230AfterScroll
    SQL.Strings = (
      'SELECT * FROM efdicmsipik230 '
      'WHERE ImporExpor=3'
      'AND AnoMes=201812'
      'AND Empresa=-11'
      'AND PeriApu=3')
    Left = 808
    Top = 392
    object QrEFD_K230ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K230AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K230Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K230PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrEFD_K230KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrEFD_K230KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrEFD_K230KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrEFD_K230KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrEFD_K230KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrEFD_K230KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrEFD_K230IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrEFD_K230ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K230DT_INI_OP: TDateField
      FieldName = 'DT_INI_OP'
    end
    object QrEFD_K230DT_FIN_OP: TDateField
      FieldName = 'DT_FIN_OP'
    end
    object QrEFD_K230COD_DOC_OP: TWideStringField
      FieldName = 'COD_DOC_OP'
      Size = 30
    end
    object QrEFD_K230COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K230QTD_ENC: TFloatField
      FieldName = 'QTD_ENC'
    end
    object QrEFD_K230GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K230OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrEFD_K230Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K230DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K230DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K230UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K230UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K230AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K230Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_K230AWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrEFD_K230AWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
  end
  object QrEFD_K235: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR,'
      'k235.*'
      'FROM efd_k235 k235'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k235.COD_ITEM'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE k235.ImporExpor=3'
      'AND k235.Empresa=-11'
      'AND k235.AnoMes=201601'
      'AND k235.K230=1')
    Left = 808
    Top = 440
    object QrEFD_K235DT_SAIDA: TDateField
      FieldName = 'DT_SAIDA'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_K235COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K235QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_K235COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrEFD_K235ID_SEK: TIntegerField
      FieldName = 'ID_SEK'
    end
  end
  object QrEFD_K250: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K250BeforeClose
    AfterScroll = QrEFD_K250AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'IF(k250.DT_PROD = 0, "", '
      'DATE_FORMAT(k250.DT_PROD, "%d/%m/%Y")) DT_PROD_Txt,'
      'K250.* '
      'FROM efd_K250 k250 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K250.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k250.ImporExpor=3'
      'AND k250.Empresa=-11'
      'AND k250.AnoMes=201601'
      'AND k250.K100=1'
      'ORDER BY DT_PROD, COD_DOC_OP, COD_ITEM ')
    Left = 808
    Top = 488
    object QrEFD_K250DT_PROD: TDateField
      FieldName = 'DT_PROD'
    end
    object QrEFD_K250COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K250QTD: TFloatField
      FieldName = 'QTD'
    end
  end
  object QrEFD_K255: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR,'
      'k235.*'
      'FROM efd_k235 k235'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k235.COD_ITEM'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE k235.ImporExpor=3'
      'AND k235.Empresa=-11'
      'AND k235.AnoMes=201601'
      'AND k235.K230=1')
    Left = 808
    Top = 536
    object QrEFD_K255DT_CONS: TDateField
      FieldName = 'DT_CONS'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_K255COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K255QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_K255COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrEFD_K255ID_SEK: TIntegerField
      FieldName = 'ID_SEK'
    end
  end
  object QrGraGru1Cons: TMySQLQuery
    Database = Dmod.MyDB
    Left = 120
    Top = 500
    object QrGraGru1ConsQtd_Comp: TFloatField
      FieldName = 'Qtd_Comp'
    end
    object QrGraGru1ConsPerda: TFloatField
      FieldName = 'Perda'
    end
    object QrGraGru1ConsGGX_CONSU: TIntegerField
      FieldName = 'GGX_CONSU'
    end
  end
  object QrPsq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 120
    Top = 548
  end
  object QrEFD_1010: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_1010')
    Left = 960
    Top = 416
    object QrEFD_1010ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_1010AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_1010Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_1010LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_1010REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_1010IND_EXP: TWideStringField
      FieldName = 'IND_EXP'
      Size = 1
    end
    object QrEFD_1010IND_CCRF: TWideStringField
      FieldName = 'IND_CCRF'
      Size = 1
    end
    object QrEFD_1010IND_COMB: TWideStringField
      FieldName = 'IND_COMB'
      Size = 1
    end
    object QrEFD_1010IND_USINA: TWideStringField
      FieldName = 'IND_USINA'
      Size = 1
    end
    object QrEFD_1010IND_VA: TWideStringField
      FieldName = 'IND_VA'
      Size = 1
    end
    object QrEFD_1010IND_EE: TWideStringField
      FieldName = 'IND_EE'
      Size = 1
    end
    object QrEFD_1010IND_CART: TWideStringField
      FieldName = 'IND_CART'
      Size = 1
    end
    object QrEFD_1010IND_FORM: TWideStringField
      FieldName = 'IND_FORM'
      Size = 1
    end
    object QrEFD_1010IND_AER: TWideStringField
      FieldName = 'IND_AER'
      Size = 1
    end
    object QrEFD_1010Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_1010DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_1010DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_1010UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_1010UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_1010AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_1010Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_1010IND_GIAF1: TWideStringField
      FieldName = 'IND_GIAF1'
      Size = 1
    end
    object QrEFD_1010IND_GIAF3: TWideStringField
      FieldName = 'IND_GIAF3'
      Size = 1
    end
    object QrEFD_1010IND_GIAF4: TWideStringField
      FieldName = 'IND_GIAF4'
      Size = 1
    end
    object QrEFD_1010IND_REST_RESSARC_COMPL_ICMS: TWideStringField
      FieldName = 'IND_REST_RESSARC_COMPL_ICMS'
      Size = 1
    end
  end
  object QrEnt: TMySQLQuery
    Database = Dmod.MyDB
    Left = 868
    Top = 60
  end
  object QrNFeEFD_C170: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeefd_c170 '
      'WHERE FatID=51 '
      'AND FatNum=3 '
      'AND Empresa=-11 '
      'AND nItem=1')
    Left = 960
    Top = 464
    object QrNFeEFD_C170FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEFD_C170FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEFD_C170Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEFD_C170nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeEFD_C170GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNFeEFD_C170COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrNFeEFD_C170DESCR_COMPL: TWideStringField
      FieldName = 'DESCR_COMPL'
      Size = 255
    end
    object QrNFeEFD_C170QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrNFeEFD_C170UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrNFeEFD_C170VL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
    end
    object QrNFeEFD_C170VL_DESC: TFloatField
      FieldName = 'VL_DESC'
    end
    object QrNFeEFD_C170IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
    object QrNFeEFD_C170CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrNFeEFD_C170CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrNFeEFD_C170COD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrNFeEFD_C170VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrNFeEFD_C170ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrNFeEFD_C170VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrNFeEFD_C170VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
    end
    object QrNFeEFD_C170ALIQ_ST: TFloatField
      FieldName = 'ALIQ_ST'
    end
    object QrNFeEFD_C170VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
    end
    object QrNFeEFD_C170IND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrNFeEFD_C170CST_IPI: TWideStringField
      FieldName = 'CST_IPI'
      Size = 2
    end
    object QrNFeEFD_C170COD_ENQ: TWideStringField
      FieldName = 'COD_ENQ'
      Size = 3
    end
    object QrNFeEFD_C170VL_BC_IPI: TFloatField
      FieldName = 'VL_BC_IPI'
    end
    object QrNFeEFD_C170ALIQ_IPI: TFloatField
      FieldName = 'ALIQ_IPI'
    end
    object QrNFeEFD_C170VL_IPI: TFloatField
      FieldName = 'VL_IPI'
    end
    object QrNFeEFD_C170CST_PIS: TSmallintField
      FieldName = 'CST_PIS'
    end
    object QrNFeEFD_C170VL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
    end
    object QrNFeEFD_C170ALIQ_PIS_p: TFloatField
      FieldName = 'ALIQ_PIS_p'
    end
    object QrNFeEFD_C170QUANT_BC_PIS: TFloatField
      FieldName = 'QUANT_BC_PIS'
    end
    object QrNFeEFD_C170ALIQ_PIS_r: TFloatField
      FieldName = 'ALIQ_PIS_r'
    end
    object QrNFeEFD_C170VL_PIS: TFloatField
      FieldName = 'VL_PIS'
    end
    object QrNFeEFD_C170CST_COFINS: TSmallintField
      FieldName = 'CST_COFINS'
    end
    object QrNFeEFD_C170VL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
    end
    object QrNFeEFD_C170ALIQ_COFINS_p: TFloatField
      FieldName = 'ALIQ_COFINS_p'
    end
    object QrNFeEFD_C170QUANT_BC_COFINS: TFloatField
      FieldName = 'QUANT_BC_COFINS'
    end
    object QrNFeEFD_C170ALIQ_COFINS_r: TFloatField
      FieldName = 'ALIQ_COFINS_r'
    end
    object QrNFeEFD_C170VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
    end
    object QrNFeEFD_C170COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrNFeEFD_C170Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeEFD_C170DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeEFD_C170DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeEFD_C170UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeEFD_C170UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeEFD_C170AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeEFD_C170Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM unidmed'
      'WHERE UPPER(SIGLA)=:P0'
      'ORDER BY Codigo'
      '')
    Left = 384
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object Qr0220: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT med.Sigla, c170.UNID, SUM(itsi.prod_qTrib) prod_qTrib,'
      'SUM(c170.QTD) QTD,  '
      'SUM(itsi.prod_qTrib) / SUM(c170.QTD) FATOR'
      'FROM nfeefd_c170 c170'
      'LEFT JOIN nfecaba caba ON'
      '  caba.FatID = c170.FatID'
      '  AND caba.FatNum=c170.FatNum'
      '  AND caba.Empresa=c170.Empresa'
      'LEFT JOIN nfeitsi itsi ON'
      '  itsi.FatID = c170.FatID'
      '  AND itsi.FatNum=c170.FatNum'
      '  AND itsi.Empresa=c170.Empresa'
      '  AND itsi.nItem=c170.nItem'
      'LEFT JOIN gragrux ggx ON ggx.Controle=c170.GraGrux'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE caba.DataFiscal BETWEEN "2017-01-01" AND "2017-01-31"'
      'AND UPPER(med.Sigla) <> UPPER(c170.UNID)'
      'AND c170.GraGruX=4'
      'GROUP BY med.Sigla, c170.UNID')
    Left = 120
    Top = 596
    object Qr0220Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object Qr0220UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object Qr0220prod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object Qr0220QTD: TFloatField
      FieldName = 'QTD'
    end
    object Qr0220FATOR: TFloatField
      FieldName = 'FATOR'
    end
  end
  object PMBloco_K_estoque: TPopupMenu
    Left = 628
    Top = 312
    object Comcadastros1: TMenuItem
      Caption = 'Estoque com &cadastros'
      OnClick = Comcadastros1Click
    end
    object N1: TMenuItem
      Caption = 'Estoque &sem cadastros'
      OnClick = N1Click
    end
  end
  object QrEFD_K280: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DT_EST, COD_ITEM, IND_EST, COD_PART, Entidade,'
      'SUM(QTD_COR_POS) QTD_COR_POS, '
      'SUM(QTD_COR_NEG) QTD_COR_NEG '
      'FROM efdicmsipik280  '
      'WHERE ImporExpor=3 '
      'AND AnoMes=201812 '
      'AND Empresa=-11 '
      'AND PeriApu=1 '
      'GROUP BY DT_EST, COD_ITEM, IND_EST, COD_PART, Entidade'
      'ORDER BY DT_EST, COD_ITEM, IND_EST, COD_PART, Entidade')
    Left = 752
    Top = 560
    object QrEFD_K280DT_EST: TDateField
      FieldName = 'DT_EST'
    end
    object QrEFD_K280COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K280IND_EST: TWideStringField
      FieldName = 'IND_EST'
      Size = 1
    end
    object QrEFD_K280COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_K280QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
    end
    object QrEFD_K280QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
    end
    object QrEFD_K280Entidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object QrEFD_K290: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrEFD_K290AfterScroll
    SQL.Strings = (
      'SELECT DT_INI_OP, DT_FIN_OP, COD_DOC_OP'
      'FROM efdicmsipik290'
      'WHERE ImporExpor=3'
      'AND AnoMes=0'
      'AND Empresa=0'
      'AND PeriApu=0'
      'ORDER BY COD_DOC_OP')
    Left = 860
    Top = 416
    object QrEFD_K290DT_INI_OP: TDateField
      FieldName = 'DT_INI_OP'
    end
    object QrEFD_K290DT_FIN_OP: TDateField
      FieldName = 'DT_FIN_OP'
    end
    object QrEFD_K290COD_DOC_OP: TWideStringField
      FieldName = 'COD_DOC_OP'
      Size = 30
    end
  end
  object QrEFD_K300: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrEFD_K300AfterScroll
    SQL.Strings = (
      'SELECT DT_PROD'
      'FROM efdicmsipik300'
      'WHERE ImporExpor=3'
      'AND AnoMes=0'
      'AND Empresa=0'
      'AND PeriApu=0'
      'ORDER BY DT_PROD')
    Left = 908
    Top = 392
    object QrEFD_K300DT_PROD: TDateField
      FieldName = 'DT_PROD'
    end
  end
  object QrEFD_K301: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COD_ITEM, SUM(QTD) QTD'
      'FROM efdicmsipik301'
      'WHERE ImporExpor=3 '
      'AND Empresa=-11'
      'AND AnoMes=201811'
      'AND PeriApu=1'
      'AND IDSeq1 IN'
      '('
      '  SELECT IDSeq1 '
      '  FROM efdicmsipik300'
      '  WHERE ImporExpor=3 '
      '  AND Empresa=-11'
      '  AND AnoMes=201811'
      '  AND PeriApu=1'
      ')'
      'GROUP BY COD_ITEM'
      'ORDER BY GraGruX')
    Left = 908
    Top = 440
    object QrEFD_K301COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K301QTD: TFloatField
      FieldName = 'QTD'
    end
  end
  object QrEFD_K302: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COD_ITEM, SUM(QTD) QTD'
      'FROM efdicmsipik302'
      'WHERE ImporExpor=3 '
      'AND Empresa=-11'
      'AND AnoMes=201811'
      'AND PeriApu=1'
      'AND IDSeq1 IN'
      '('
      '  SELECT IDSeq1 '
      '  FROM efdicmsipik300'
      '  WHERE ImporExpor=3 '
      '  AND Empresa=-11'
      '  AND AnoMes=201811'
      '  AND PeriApu=1'
      ')'
      'GROUP BY COD_ITEM'
      'ORDER BY GraGruX')
    Left = 908
    Top = 488
    object QrEFD_K302COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K302QTD: TFloatField
      FieldName = 'QTD'
    end
  end
  object QrEFD_K291: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COD_ITEM, SUM(QTD) QTD'
      'FROM efdicmsipik301'
      'WHERE ImporExpor=3 '
      'AND Empresa=-11'
      'AND AnoMes=201811'
      'AND PeriApu=1'
      'AND IDSeq1 IN'
      '('
      '  SELECT IDSeq1 '
      '  FROM efdicmsipik300'
      '  WHERE ImporExpor=3 '
      '  AND Empresa=-11'
      '  AND AnoMes=201811'
      '  AND PeriApu=1'
      ')'
      'GROUP BY COD_ITEM'
      'ORDER BY GraGruX')
    Left = 860
    Top = 464
    object QrEFD_K291COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K291QTD: TFloatField
      FieldName = 'QTD'
    end
  end
  object QrEFD_K292: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COD_ITEM, SUM(QTD) QTD'
      'FROM efdicmsipik302'
      'WHERE ImporExpor=3 '
      'AND Empresa=-11'
      'AND AnoMes=201811'
      'AND PeriApu=1'
      'AND IDSeq1 IN'
      '('
      '  SELECT IDSeq1 '
      '  FROM efdicmsipik300'
      '  WHERE ImporExpor=3 '
      '  AND Empresa=-11'
      '  AND AnoMes=201811'
      '  AND PeriApu=1'
      ')'
      'GROUP BY COD_ITEM'
      'ORDER BY GraGruX')
    Left = 860
    Top = 512
    object QrEFD_K292COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K292QTD: TFloatField
      FieldName = 'QTD'
    end
  end
  object PMBloco_K_completo: TPopupMenu
    Left = 628
    Top = 360
    object MenuItem1: TMenuItem
      Caption = 'Bloco K com &cadastros'
      OnClick = MenuItem1Click
    end
    object MenuItem2: TMenuItem
      Caption = 'Bloco K &sem cadastros'
      OnClick = MenuItem2Click
    end
  end
  object frxSEII_Estq: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxSEII_EstqGetValue
    Left = 200
    Top = 248
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsSEII_0190
        DataSetName = 'frxDsSEII_0190'
      end
      item
        DataSet = frxDsSEII_0200
        DataSetName = 'frxDsSEII_0200'
      end
      item
        DataSet = frxDsSEII_k200
        DataSetName = 'frxDsSEII_k200'
      end
      item
        DataSet = frxDsSEII_K280
        DataSetName = 'frxDsSEII_K280'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574781180000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Dados de Texto de SPED EFD ICMS IPI Gerado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA_SPED]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSEII_k200
        DataSetName = 'frxDsSEII_k200'
        RowCount = 0
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 442.204912360000000000
          Height = 15.118110240000000000
          DataField = 'DESCR_ITEM'
          DataSet = frxDsSEII_k200
          DataSetName = 'frxDsSEII_k200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSEII_k200."DESCR_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Width = 75.590573150000000000
          Height = 15.118110240000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsSEII_k200
          DataSetName = 'frxDsSEII_k200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSEII_k200."COD_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeValPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826800940000000000
          Height = 15.118110240000000000
          DataSet = frxDsSEII_k200
          DataSetName = 'frxDsSEII_k200'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSEII_k200."QTD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Width = 56.692901180000000000
          Height = 15.118110240000000000
          DataField = 'UNID_INV'
          DataSet = frxDsSEII_k200
          DataSetName = 'frxDsSEII_k200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSEII_k200."UNID_INV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 986.457330000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 47.244116460000000000
        Top = 914.646260000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Top = 34.015769999999970000
          Width = 438.425284720000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 34.015769999999970000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxDsSEII_k200."QTD">,MD002,1) + SUM(<frxDsSEII_K280."QTD_' +
              'COR_POS">,MD003,1) - SUM(<frxDsSEII_K280."QTD_COR_NEG">,MD003,1)' +
              ']')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 34.015769999999970000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSEII_k200."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 20.787401570000040000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Geral')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 20.787401570000040000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'K280 (-)')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 20.787401570000040000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'K280 (+)')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 20.787401570000040000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'K200 (+)')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 34.015769999999970000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSEII_K280."QTD_COR_NEG">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 34.015769999999970000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSEII_K280."QTD_COR_POS">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_00: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 181.417440000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSEII_k200."IND_EST"'
        object Me_GH0: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779529999999937000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsSEII_k200."NO_IND_EST"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_00: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        object Me_FT0: TfrxMemoView
          AllowVectorExport = True
          Width = 619.842519690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSEII_k200."NO_IND_EST"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSEII_k200."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_02: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 272.126160000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSEII_k200."GRUPOS"'
        object Me_GH2: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsSEII_k200."GRUPOS"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSEII_k200."COD_PART"'
        object Me_GH1: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000079000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsSEII_k200."NO_TERCEIRO"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 393.071120000000000000
        Width = 680.315400000000000000
        object Me_FT1: TfrxMemoView
          AllowVectorExport = True
          Width = 619.842519690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSEII_k200."NO_TERCEIRO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSEII_k200."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FT_02: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 355.275820000000000000
        Width = 680.315400000000000000
        object Me_FT2: TfrxMemoView
          AllowVectorExport = True
          Width = 619.842519690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSEII_k200."GRUPOS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu2PesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSEII_k200."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795290240000000000
        Top = 120.944960000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSEII_k200."REG"'
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779529999999994000
          Width = 680.314985040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE - K200')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Top = 22.677180000000010000
          Width = 442.204912360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          AllowVectorExport = True
          Left = -3.779530000000000000
          Top = 22.677180000000010000
          Width = 75.590575590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 570.709030000000000000
          Top = 22.677180000000010000
          Width = 105.826800940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 514.016080000000000000
          Top = 22.677180000000010000
          Width = 56.692901180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 7.559060000000000000
        Top = 468.661720000000000000
        Width = 680.315400000000000000
      end
      object MD003: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 695.433520000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSEII_K280
        DataSetName = 'frxDsSEII_K280'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 336.378072360000000000
          Height = 15.118110240000000000
          DataField = 'DESCR_ITEM'
          DataSet = frxDsSEII_K280
          DataSetName = 'frxDsSEII_K280'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSEII_K280."DESCR_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Width = 75.590573150000000000
          Height = 15.118110240000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsSEII_K280
          DataSetName = 'frxDsSEII_K280'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSEII_K280."COD_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 105.826800940000000000
          Height = 15.118110240000000000
          DataField = 'QTD_COR_POS'
          DataSet = frxDsSEII_K280
          DataSetName = 'frxDsSEII_K280'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSEII_K280."QTD_COR_POS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 411.968770000000000000
          Width = 56.692901180000000000
          Height = 15.118110240000000000
          DataField = 'UNID_INV'
          DataSet = frxDsSEII_K280
          DataSetName = 'frxDsSEII_K280'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSEII_K280."UNID_INV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826800940000000000
          Height = 15.118110240000000000
          DataField = 'QTD_COR_NEG'
          DataSet = frxDsSEII_K280
          DataSetName = 'frxDsSEII_K280'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSEII_K280."QTD_COR_NEG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 559.370440000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSEII_K280."IND_EST"'
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsSEII_k280."NO_IND_EST"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 808.819420000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Width = 468.661319690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSEII_k280."NO_IND_EST"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826800940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSEII_K280."QTD_COR_NEG">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 105.826800940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSEII_K280."QTD_COR_POS">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 650.079160000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSEII_K280."GRUPOS"'
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsSEII_k280."GRUPOS"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 604.724800000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSEII_K280."COD_PART"'
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsSEII_k280."NO_TERCEIRO"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 771.024120000000000000
        Width = 680.315400000000000000
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Width = 468.661319690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSEII_k280."NO_TERCEIRO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826800940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSEII_K280."QTD_COR_NEG">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 105.826800940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSEII_K280."QTD_COR_POS">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 733.228820000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Width = 468.661319690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSEII_k280."GRUPOS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826800940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSEII_K280."QTD_COR_NEG">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 105.826800940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSEII_K280."QTD_COR_POS">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader5: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795290240000000000
        Top = 498.897960000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSEII_K280."REG"'
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000079000
          Width = 680.314985040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'CORRE'#199#195'O DE APOPNTAMENTO DE ESTOQUE - K280')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 22.677180000000020000
          Width = 442.204912360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000020000
          Width = 75.590575590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 22.677180000000020000
          Width = 105.826800940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 517.795610000000000000
          Top = 22.677180000000020000
          Width = 56.692901180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object GroupFooter5: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 7.559060000000000000
        Top = 846.614720000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object QrSEII_0190: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _spedefdicmsipi_0190_')
    Left = 200
    Top = 296
    object QrSEII_0190REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrSEII_0190UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrSEII_0190DESCR: TWideStringField
      FieldName = 'DESCR'
      Size = 255
    end
    object QrSEII_0190Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object frxDsSEII_0190: TfrxDBDataset
    UserName = 'frxDsSEII_0190'
    CloseDataSource = False
    FieldAliases.Strings = (
      'REG=REG'
      'UNID=UNID'
      'DESCR=DESCR'
      'Ativo=Ativo')
    DataSet = QrSEII_0190
    BCDToCurrency = False
    DataSetOptions = []
    Left = 200
    Top = 344
  end
  object QrSEII_0200: TMySQLQuery
    Database = Dmod.MyDB
    Left = 200
    Top = 392
    object QrSEII_0200REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrSEII_0200COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrSEII_0200DESCR_ITEM: TWideStringField
      FieldName = 'DESCR_ITEM'
      Size = 255
    end
    object QrSEII_0200COD_BARRA: TWideStringField
      FieldName = 'COD_BARRA'
      Size = 255
    end
    object QrSEII_0200COD_ANT_ITEM: TWideStringField
      FieldName = 'COD_ANT_ITEM'
      Size = 60
    end
    object QrSEII_0200UNID_INV: TWideStringField
      FieldName = 'UNID_INV'
      Size = 6
    end
    object QrSEII_0200TIPO_ITEM: TSmallintField
      FieldName = 'TIPO_ITEM'
    end
    object QrSEII_0200COD_NCM: TWideStringField
      FieldName = 'COD_NCM'
      Size = 8
    end
    object QrSEII_0200EX_IPI: TWideStringField
      FieldName = 'EX_IPI'
      Size = 3
    end
    object QrSEII_0200COD_GEN: TSmallintField
      FieldName = 'COD_GEN'
    end
    object QrSEII_0200COD_LST: TWideStringField
      FieldName = 'COD_LST'
      Size = 5
    end
    object QrSEII_0200ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrSEII_0200CEST: TIntegerField
      FieldName = 'CEST'
    end
    object QrSEII_0200Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSEII_0200GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object frxDsSEII_0200: TfrxDBDataset
    UserName = 'frxDsSEII_0200'
    CloseDataSource = False
    FieldAliases.Strings = (
      'REG=REG'
      'COD_ITEM=COD_ITEM'
      'DESCR_ITEM=DESCR_ITEM'
      'COD_BARRA=COD_BARRA'
      'COD_ANT_ITEM=COD_ANT_ITEM'
      'UNID_INV=UNID_INV'
      'TIPO_ITEM=TIPO_ITEM'
      'COD_NCM=COD_NCM'
      'EX_IPI=EX_IPI'
      'COD_GEN=COD_GEN'
      'COD_LST=COD_LST'
      'ALIQ_ICMS=ALIQ_ICMS'
      'CEST=CEST'
      'Ativo=Ativo')
    DataSet = QrSEII_0200
    BCDToCurrency = False
    DataSetOptions = []
    Left = 200
    Top = 440
  end
  object frxDsSEII_K280: TfrxDBDataset
    UserName = 'frxDsSEII_K280'
    CloseDataSource = False
    FieldAliases.Strings = (
      'REG=REG'
      'DT_EST=DT_EST'
      'COD_ITEM=COD_ITEM'
      'QTD_COR_POS=QTD_COR_POS'
      'QTD_COR_NEG=QTD_COR_NEG'
      'IND_EST=IND_EST'
      'COD_PART=COD_PART'
      'Ativo=Ativo'
      'NO_IND_EST=NO_IND_EST'
      'NO_TERCEIRO=NO_TERCEIRO'
      'DESCR_ITEM=DESCR_ITEM'
      'GRUPOS=GRUPOS'
      'UNID_INV=UNID_INV')
    DataSet = QrSEII_K280
    BCDToCurrency = False
    DataSetOptions = []
    Left = 284
    Top = 440
  end
  object QrSEII_K280: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT IF(k200.COD_PART="", "SOFTCOURO IND'#218'STRIA E COM'#201'RCIO LTDA' +
        '", _150.Nome) NO_TERCEIRO,  '
      '_200.DESCR_ITEM, '
      
        ' ELT(k200.IND_EST + 1,"Estoque de propriedade do informante e em' +
        ' seu poder","Estoque de propriedade do informante e em posse de ' +
        'terceiros","Estoque de propriedade de terceiros e em posse do in' +
        'formante","???") NO_IND_EST,'
      'k200.*, IF(gg1.PrdGrupTip=-2, "Insumos", "Produtos") GRUPOS  '
      'FROM _spedefdicmsipi_K280_ k200 '
      
        'LEFT JOIN _spedefdicmsipi_0150_ _150 ON _150.COD_PART=k200.COD_P' +
        'ART '
      
        'LEFT JOIN _spedefdicmsipi_0200_ _200 ON _200.COD_ITEM=k200.COD_I' +
        'TEM '
      
        'LEFT JOIN bluederm_softcouro.gragrux ggx ON ggx.Controle=_200.Gr' +
        'aGruX'
      
        'LEFT JOIN bluederm_softcouro.gragru1 gg1 ON gg1.Nivel1=ggx.GraGr' +
        'u1'
      'ORDER BY k200.COD_PART, k200.DT_EST, GRUPOS, DESCR_ITEM ')
    Left = 284
    Top = 392
    object QrSEII_K280REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrSEII_K280DT_EST: TDateField
      FieldName = 'DT_EST'
    end
    object QrSEII_K280COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrSEII_K280QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
    end
    object QrSEII_K280QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
    end
    object QrSEII_K280IND_EST: TWideStringField
      FieldName = 'IND_EST'
      Size = 1
    end
    object QrSEII_K280COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrSEII_K280Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSEII_K280NO_IND_EST: TWideStringField
      FieldName = 'NO_IND_EST'
      Size = 255
    end
    object QrSEII_K280NO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrSEII_K280DESCR_ITEM: TWideStringField
      FieldName = 'DESCR_ITEM'
      Size = 255
    end
    object QrSEII_K280GRUPOS: TWideStringField
      FieldName = 'GRUPOS'
      Required = True
      Size = 8
    end
    object QrSEII_K280UNID_INV: TWideStringField
      FieldName = 'UNID_INV'
      Size = 15
    end
  end
  object frxDsSEII_k200: TfrxDBDataset
    UserName = 'frxDsSEII_k200'
    CloseDataSource = False
    FieldAliases.Strings = (
      'REG=REG'
      'DT_EST=DT_EST'
      'COD_ITEM=COD_ITEM'
      'QTD=QTD'
      'IND_EST=IND_EST'
      'COD_PART=COD_PART'
      'Ativo=Ativo'
      'NO_TERCEIRO=NO_TERCEIRO'
      'NO_IND_EST=NO_IND_EST'
      'DESCR_ITEM=DESCR_ITEM'
      'GRUPOS=GRUPOS'
      'UNID_INV=UNID_INV')
    DataSet = QrSEII_K200
    BCDToCurrency = False
    DataSetOptions = []
    Left = 288
    Top = 344
  end
  object QrSEII_K200: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT IF(k200.COD_PART="", "SOFTCOURO IND'#218'STRIA E COM'#201'RCIO LTDA' +
        '", _150.Nome) NO_TERCEIRO,  '
      '_200.DESCR_ITEM, '
      
        ' ELT(k200.IND_EST + 1,"Estoque de propriedade do informante e em' +
        ' seu poder","Estoque de propriedade do informante e em posse de ' +
        'terceiros","Estoque de propriedade de terceiros e em posse do in' +
        'formante","???") NO_IND_EST,'
      'k200.*, IF(gg1.PrdGrupTip=-2, "Insumos", "Produtos") GRUPOS  '
      'FROM _spedefdicmsipi_K200_ k200 '
      
        'LEFT JOIN _spedefdicmsipi_0150_ _150 ON _150.COD_PART=k200.COD_P' +
        'ART '
      
        'LEFT JOIN _spedefdicmsipi_0200_ _200 ON _200.COD_ITEM=k200.COD_I' +
        'TEM '
      
        'LEFT JOIN bluederm_softcouro.gragrux ggx ON ggx.Controle=_200.Gr' +
        'aGruX'
      
        'LEFT JOIN bluederm_softcouro.gragru1 gg1 ON gg1.Nivel1=ggx.GraGr' +
        'u1'
      'ORDER BY k200.COD_PART, k200.DT_EST, GRUPOS, DESCR_ITEM ')
    Left = 288
    Top = 296
    object QrSEII_K200REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrSEII_K200DT_EST: TDateField
      FieldName = 'DT_EST'
    end
    object QrSEII_K200COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrSEII_K200QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrSEII_K200IND_EST: TWideStringField
      FieldName = 'IND_EST'
      Size = 1
    end
    object QrSEII_K200COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrSEII_K200Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSEII_K200NO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrSEII_K200NO_IND_EST: TWideStringField
      FieldName = 'NO_IND_EST'
      Size = 255
    end
    object QrSEII_K200DESCR_ITEM: TWideStringField
      FieldName = 'DESCR_ITEM'
      Size = 255
    end
    object QrSEII_K200GRUPOS: TWideStringField
      FieldName = 'GRUPOS'
      Required = True
      Size = 8
    end
    object QrSEII_K200UNID_INV: TWideStringField
      FieldName = 'UNID_INV'
      Size = 15
    end
  end
  object QrEFD_K210: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K210BeforeClose
    AfterScroll = QrEFD_K210AfterScroll
    SQL.Strings = (
      'SELECT * FROM efdicmsipik210 '
      'WHERE ImporExpor=3'
      'AND AnoMes=201811'
      'AND Empresa=-11'
      'AND PeriApu=1')
    Left = 752
    Top = 412
    object QrEFD_K210ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K210AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K210Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K210PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrEFD_K210KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrEFD_K210KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrEFD_K210KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrEFD_K210KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrEFD_K210KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrEFD_K210KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrEFD_K210IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrEFD_K210ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K210DT_INI_OS: TDateField
      FieldName = 'DT_INI_OS'
    end
    object QrEFD_K210DT_FIN_OS: TDateField
      FieldName = 'DT_FIN_OS'
    end
    object QrEFD_K210COD_DOC_OS: TWideStringField
      FieldName = 'COD_DOC_OS'
      Size = 30
    end
    object QrEFD_K210COD_ITEM_ORI: TWideStringField
      FieldName = 'COD_ITEM_ORI'
      Size = 60
    end
    object QrEFD_K210QTD_ORI: TFloatField
      FieldName = 'QTD_ORI'
    end
    object QrEFD_K210GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K210OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrEFD_K210OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrEFD_K210Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K210DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K210DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K210UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K210UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K210AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K210Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_K210AWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrEFD_K210AWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
  end
  object QrEFD_K215: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COD_ITEM_DES, '
      'SUM(QTD_DES) QTD_DES, ID_SEK'
      'FROM efdicmsipik215 '
      'WHERE ImporExpor=3'
      'AND AnoMes=201811'
      'AND Empresa=-11'
      'AND PeriApu=1'
      'GROUP BY COD_ITEM_DES')
    Left = 752
    Top = 460
    object QrEFD_K215COD_ITEM_DES: TWideStringField
      FieldName = 'COD_ITEM_DES'
      Size = 60
    end
    object QrEFD_K215QTD_DES: TFloatField
      FieldName = 'QTD_DES'
    end
    object QrEFD_K215ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
  end
  object QrEfdInnNFsCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM efdinnnfscab'
      'WHERE MovFatID>0'
      'AND MovFatNum>0'
      'AND Empresa=-11')
    Left = 388
    Top = 249
    object QrEfdInnNFsCabMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnNFsCabMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnNFsCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnNFsCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnNFsCabControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnNFsCabTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrEfdInnNFsCabPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrEfdInnNFsCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrEfdInnNFsCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrEfdInnNFsCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrEfdInnNFsCabValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrEfdInnNFsCabMotorista: TIntegerField
      FieldName = 'Motorista'
      Required = True
    end
    object QrEfdInnNFsCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrEfdInnNFsCabCOD_MOD: TSmallintField
      FieldName = 'COD_MOD'
      Required = True
    end
    object QrEfdInnNFsCabCOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
      Required = True
    end
    object QrEfdInnNFsCabSER: TIntegerField
      FieldName = 'SER'
      Required = True
    end
    object QrEfdInnNFsCabNUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Required = True
    end
    object QrEfdInnNFsCabCHV_NFE: TWideStringField
      FieldName = 'CHV_NFE'
      Size = 44
    end
    object QrEfdInnNFsCabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
      Required = True
    end
    object QrEfdInnNFsCabDT_DOC: TDateField
      FieldName = 'DT_DOC'
      Required = True
    end
    object QrEfdInnNFsCabDT_E_S: TDateField
      FieldName = 'DT_E_S'
      Required = True
    end
    object QrEfdInnNFsCabVL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Required = True
    end
    object QrEfdInnNFsCabIND_PGTO: TWideStringField
      FieldName = 'IND_PGTO'
      Size = 1
    end
    object QrEfdInnNFsCabVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
    end
    object QrEfdInnNFsCabVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Required = True
    end
    object QrEfdInnNFsCabVL_MERC: TFloatField
      FieldName = 'VL_MERC'
      Required = True
    end
    object QrEfdInnNFsCabIND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEfdInnNFsCabVL_FRT: TFloatField
      FieldName = 'VL_FRT'
      Required = True
    end
    object QrEfdInnNFsCabVL_SEG: TFloatField
      FieldName = 'VL_SEG'
      Required = True
    end
    object QrEfdInnNFsCabVL_OUT_DA: TFloatField
      FieldName = 'VL_OUT_DA'
      Required = True
    end
    object QrEfdInnNFsCabVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
    end
    object QrEfdInnNFsCabVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
    end
    object QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Required = True
    end
    object QrEfdInnNFsCabVL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Required = True
    end
    object QrEfdInnNFsCabVL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Required = True
    end
    object QrEfdInnNFsCabVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
    end
    object QrEfdInnNFsCabVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
    end
    object QrEfdInnNFsCabVL_PIS_ST: TFloatField
      FieldName = 'VL_PIS_ST'
      Required = True
    end
    object QrEfdInnNFsCabVL_COFINS_ST: TFloatField
      FieldName = 'VL_COFINS_ST'
      Required = True
    end
    object QrEfdInnNFsCabNFe_FatID: TIntegerField
      FieldName = 'NFe_FatID'
      Required = True
    end
    object QrEfdInnNFsCabNFe_FatNum: TIntegerField
      FieldName = 'NFe_FatNum'
      Required = True
    end
    object QrEfdInnNFsCabNFe_StaLnk: TSmallintField
      FieldName = 'NFe_StaLnk'
      Required = True
    end
    object QrEfdInnNFsCabVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
      Required = True
    end
    object QrEfdInnNFsCabVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrEfdInnNFsCabVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrEfdInnNFsCabVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
      Required = True
    end
    object QrEfdInnNFsCabIsLinked: TSmallintField
      FieldName = 'IsLinked'
      Required = True
    end
    object QrEfdInnNFsCabSqLinked: TIntegerField
      FieldName = 'SqLinked'
      Required = True
    end
    object QrEfdInnNFsCabCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrEfdInnNFsCabRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Required = True
    end
  end
  object QrEfdInnNFsIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM efdinnnfsits'
      'WHERE Controle=27'
      'ORDER BY Conta')
    Left = 388
    Top = 305
    object QrEfdInnNFsItsMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnNFsItsMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnNFsItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnNFsItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnNFsItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrEfdInnNFsItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrEfdInnNFsItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnNFsItsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEfdInnNFsItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
    object QrEfdInnNFsItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrEfdInnNFsItsprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      Required = True
    end
    object QrEfdInnNFsItsprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Required = True
    end
    object QrEfdInnNFsItsprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Required = True
    end
    object QrEfdInnNFsItsprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrEfdInnNFsItsQTD: TFloatField
      FieldName = 'QTD'
      Required = True
    end
    object QrEfdInnNFsItsUNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrEfdInnNFsItsVL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      Required = True
    end
    object QrEfdInnNFsItsVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
    end
    object QrEfdInnNFsItsIND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
    object QrEfdInnNFsItsCFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrEfdInnNFsItsCOD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrEfdInnNFsItsVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      Required = True
    end
    object QrEfdInnNFsItsVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
    end
    object QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_ST: TFloatField
      FieldName = 'ALIQ_ST'
      Required = True
    end
    object QrEfdInnNFsItsVL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Required = True
    end
    object QrEfdInnNFsItsIND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrEfdInnNFsItsCOD_ENQ: TWideStringField
      FieldName = 'COD_ENQ'
      Size = 3
    end
    object QrEfdInnNFsItsVL_BC_IPI: TFloatField
      FieldName = 'VL_BC_IPI'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_IPI: TFloatField
      FieldName = 'ALIQ_IPI'
      Required = True
    end
    object QrEfdInnNFsItsVL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Required = True
    end
    object QrEfdInnNFsItsVL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_PIS_p: TFloatField
      FieldName = 'ALIQ_PIS_p'
      Required = True
    end
    object QrEfdInnNFsItsQUANT_BC_PIS: TFloatField
      FieldName = 'QUANT_BC_PIS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_PIS_r: TFloatField
      FieldName = 'ALIQ_PIS_r'
      Required = True
    end
    object QrEfdInnNFsItsVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
    end
    object QrEfdInnNFsItsVL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_COFINS_p: TFloatField
      FieldName = 'ALIQ_COFINS_p'
      Required = True
    end
    object QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField
      FieldName = 'QUANT_BC_COFINS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_COFINS_r: TFloatField
      FieldName = 'ALIQ_COFINS_r'
      Required = True
    end
    object QrEfdInnNFsItsVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
    end
    object QrEfdInnNFsItsCOD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEfdInnNFsItsVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Required = True
    end
    object QrEfdInnNFsItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
      Required = True
    end
    object QrEfdInnNFsItsxLote: TWideStringField
      FieldName = 'xLote'
    end
    object QrEfdInnNFsItsOri_IPIpIPI: TFloatField
      FieldName = 'Ori_IPIpIPI'
      Required = True
    end
    object QrEfdInnNFsItsOri_IPIvIPI: TFloatField
      FieldName = 'Ori_IPIvIPI'
      Required = True
    end
    object QrEfdInnNFsItsIsLinked: TSmallintField
      FieldName = 'IsLinked'
      Required = True
    end
    object QrEfdInnNFsItsSqLinked: TIntegerField
      FieldName = 'SqLinked'
      Required = True
    end
    object QrEfdInnNFsItsSqLnkID: TIntegerField
      FieldName = 'SqLnkID'
      Required = True
    end
    object QrEfdInnNFsItsCST_IPI: TWideStringField
      FieldName = 'CST_IPI'
      Size = 2
    end
    object QrEfdInnNFsItsCST_PIS: TWideStringField
      FieldName = 'CST_PIS'
      Size = 2
    end
    object QrEfdInnNFsItsCST_COFINS: TWideStringField
      FieldName = 'CST_COFINS'
      Size = 2
    end
    object QrEfdInnNFsItsCST_ICMS: TWideStringField
      FieldName = 'CST_ICMS'
      Size = 3
    end
  end
  object QrFisRegMvt: TMySQLQuery
    Left = 504
    Top = 297
    object QrFisRegMvtTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
    end
  end
  object QrIndMov: TMySQLQuery
    SQL.Strings = (
      'SELECT '
      
        '(IF((gg1.Nivel2 <> 0) and (gg2.Tipo_Item in (0,1,2,3,4,5,6,7,8,1' +
        '0)), 0, '
      '  IF(pgt.TipPrd=3, 1, '
      '    IF(pgt.Tipo_Item in (0,1,2,3,4,5,6,7,8,10), 0, '
      '      1)'
      '  )'
      ') + 0.000) IND_MOV'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragru2 gg2 ON gg1.Nivel2=gg2.Nivel2'
      'LEFT JOIN prdgruptip pgt ON gg1.PrdGrupTip=pgt.Codigo'
      'WHERE ggx.Controle>0')
    Left = 504
    Top = 245
    object QrIndMovIND_MOV: TFloatField
      FieldName = 'IND_MOV'
    end
  end
  object QrI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT itsi.*, '
      'itsn.ICMS_Orig, itsn.ICMS_CST, itsn.ICMS_vBC,'
      'itsn.ICMS_pICMS, itsn.ICMS_vICMS, itsn.ICMS_vBCST,'
      'itsn.ICMS_pICMSST, itsn.ICMS_vICMSST, '
      ''
      'itso.IPI_cEnq, itso.IND_APUR, itso.IPI_CST, '
      'itso.IPI_vBC, itso.IPI_pIPI, itso.IPI_vIPI,'
      ''
      'itsq.PIS_CST, itsq.PIS_vBC, '
      'itsq.PIS_pPIS, itsq.PIS_vPIS,'
      ''
      'itsr.PISST_vBC, '
      'itsr.PISST_pPIS, itsr.PISST_vPIS,'
      ''
      'itss.COFINS_CST, itss.COFINS_vBC, '
      'itss.COFINS_pCOFINS, itss.COFINS_vCOFINS,'
      ''
      'itst.COFINSST_vBC, '
      'itst.COFINSST_pCOFINS, itst.COFINSST_vCOFINS'
      ''
      ''
      'FROM nfeitsi itsi'
      ''
      'LEFT JOIN nfeitsn itsn ON '
      '  itsn.FatID=itsi.FatID'
      '  AND itsn.FatNum=itsi.FatNum'
      '  AND itsn.Empresa=itsi.Empresa'
      '  AND itsn.nItem=itsi.nItem'
      ''
      'LEFT JOIN nfeitso itso ON'
      '  itso.FatID=itsi.FatID'
      '  AND itso.FatNum=itsi.FatNum'
      '  AND itso.Empresa=itsi.Empresa'
      '  AND itso.nItem=itsi.nItem'
      ''
      'LEFT JOIN nfeitsq itsq ON'
      '  itsq.FatID=itsi.FatID'
      '  AND itsq.FatNum=itsi.FatNum'
      '  AND itsq.Empresa=itsi.Empresa'
      '  AND itsq.nItem=itsi.nItem'
      ''
      'LEFT JOIN nfeitsr itsr ON'
      '  itsr.FatID=itsi.FatID'
      '  AND itsr.FatNum=itsi.FatNum'
      '  AND itsr.Empresa=itsi.Empresa'
      '  AND itsr.nItem=itsi.nItem'
      ''
      'LEFT JOIN nfeitss itss ON'
      '  itss.FatID=itsi.FatID'
      '  AND itss.FatNum=itsi.FatNum'
      '  AND itss.Empresa=itsi.Empresa'
      '  AND itss.nItem=itsi.nItem'
      ''
      'LEFT JOIN nfeitst itst ON'
      '  itst.FatID=itsi.FatID'
      '  AND itst.FatNum=itsi.FatNum'
      '  AND itst.Empresa=itsi.Empresa'
      '  AND itst.nItem=itsi.nItem'
      ''
      ''
      'WHERE itsi.FatID=1'
      'AND itsi.FatNum=14238'
      'AND itsi.Empresa=-11'
      '')
    Left = 388
    Top = 361
    object QrIFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrIFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrIEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrInItem: TIntegerField
      FieldName = 'nItem'
      Required = True
    end
    object QrIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrIprod_cBarra: TWideStringField
      FieldName = 'prod_cBarra'
      Size = 30
    end
    object QrIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrIprod_CEST: TIntegerField
      FieldName = 'prod_CEST'
      Required = True
    end
    object QrIprod_indEscala: TWideStringField
      FieldName = 'prod_indEscala'
      Size = 1
    end
    object QrIprod_CNPJFab: TWideStringField
      FieldName = 'prod_CNPJFab'
      Size = 14
    end
    object QrIprod_cBenef: TWideStringField
      FieldName = 'prod_cBenef'
      Size = 10
    end
    object QrIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrIprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
      Required = True
    end
    object QrIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
      Required = True
    end
    object QrIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
      Required = True
    end
    object QrIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      Required = True
    end
    object QrIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrIprod_cBarraTrib: TWideStringField
      FieldName = 'prod_cBarraTrib'
      Size = 30
    end
    object QrIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
      Required = True
    end
    object QrIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
      Required = True
    end
    object QrIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Required = True
    end
    object QrIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Required = True
    end
    object QrIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
      Required = True
    end
    object QrIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrIprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
      Required = True
    end
    object QrIprod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrIprod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
      Required = True
    end
    object QrI_Ativo_: TSmallintField
      FieldName = '_Ativo_'
      Required = True
    end
    object QrIInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Required = True
    end
    object QrIEhServico: TIntegerField
      FieldName = 'EhServico'
      Required = True
    end
    object QrIUsaSubsTrib: TSmallintField
      FieldName = 'UsaSubsTrib'
      Required = True
    end
    object QrIICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
      Required = True
    end
    object QrIICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
      Required = True
    end
    object QrIICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      Required = True
    end
    object QrIICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
      Required = True
    end
    object QrIIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
      Required = True
    end
    object QrIIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
      Required = True
    end
    object QrIIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      Required = True
    end
    object QrIIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
      Required = True
    end
    object QrIPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
      Required = True
    end
    object QrIPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
      Required = True
    end
    object QrIPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      Required = True
    end
    object QrIPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
      Required = True
    end
    object QrICOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
      Required = True
    end
    object QrICOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
      Required = True
    end
    object QrICOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      Required = True
    end
    object QrICOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
      Required = True
    end
    object QrIMeuID: TIntegerField
      FieldName = 'MeuID'
      Required = True
    end
    object QrINivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrIGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrIUnidMedCom: TIntegerField
      FieldName = 'UnidMedCom'
      Required = True
    end
    object QrIUnidMedTrib: TIntegerField
      FieldName = 'UnidMedTrib'
      Required = True
    end
    object QrIICMSRec_vBCST: TFloatField
      FieldName = 'ICMSRec_vBCST'
      Required = True
    end
    object QrIICMSRec_vICMSST: TFloatField
      FieldName = 'ICMSRec_vICMSST'
      Required = True
    end
    object QrIICMSRec_pAliqST: TFloatField
      FieldName = 'ICMSRec_pAliqST'
      Required = True
    end
    object QrITem_II: TSmallintField
      FieldName = 'Tem_II'
      Required = True
    end
    object QrIprod_nFCI: TWideStringField
      FieldName = 'prod_nFCI'
      Size = 36
    end
    object QrIStqMovValA: TIntegerField
      FieldName = 'StqMovValA'
      Required = True
    end
    object QrIAtrelaID: TIntegerField
      FieldName = 'AtrelaID'
      Required = True
    end
    object QrIICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrIICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrIICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrIICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrIICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrIICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrIICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrIICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrIIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrIIND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrIIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrIIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrIIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrIIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrIPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrIPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
    end
    object QrIPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
    end
    object QrIPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
    end
    object QrIPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
    end
    object QrIPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
    end
    object QrIPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
    end
    object QrICOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrICOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrICOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
    end
    object QrICOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
    end
    object QrICOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
    end
    object QrICOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
    end
    object QrICOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
    end
  end
  object QrAtrela0053: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_A_;'
      'CREATE TABLE _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_A_'
      'SELECT NFe_FatID, NFe_FatNum, DT_E_S DataFiscalInn'
      'FROM bluederm_colosso.efdinnnfscab'
      'WHERE NFe_FatID=53'
      'AND  NFe_FatNum<>0'
      'AND Empresa=-11'
      'AND DT_E_S BETWEEN "2022-03-01" AND "2022-03-31";'
      ''
      'DROP TABLE IF EXISTS _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_B_;'
      'CREATE TABLE _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_B_'
      
        'SELECT FatID, FatNum, Empresa, ide_dEmi, DataFiscal DataFiscalNu' +
        'l'
      'FROM bluederm_colosso.nfecaba'
      'WHERE FatID=53'
      'AND  FatNum<>0'
      'AND Empresa=-11'
      'AND DataFiscal < "1900-01-01";'
      ''
      'SELECT b.*, a.*'
      'FROM _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_B_ b'
      'LEFT JOIN _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_A_ a '
      '  ON b.FatNum=a.NFe_FatNum'
      'WHERE NOT (a.NFe_FatNum) IS NULL'
      ';')
    Left = 944
    Top = 305
    object QrAtrela0053NFe_FatNum: TIntegerField
      FieldName = 'NFe_FatNum'
    end
    object QrAtrela0053FatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrAtrela0053FatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrAtrela0053Empresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrAtrela0053ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrAtrela0053NFe_FatID: TIntegerField
      FieldName = 'NFe_FatID'
    end
    object QrAtrela0053DataFiscalNul: TDateField
      FieldName = 'DataFiscalNul'
      Required = True
    end
    object QrAtrela0053DataFiscalInn: TDateField
      FieldName = 'DataFiscalInn'
    end
  end
  object QrAnal0053: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CST_ICMS, CFOP, ALIQ_ICMS, '
      'SUM(VL_BC_ICMS) VL_BC_ICMS,'
      'SUM(VL_ICMS) VL_ICMS,'
      'SUM(VL_BC_ICMS_ST) VL_BC_ICMS_ST,'
      'SUM(VL_ICMS_ST) VL_ICMS_ST,'
      'SUM(VL_RED_BC) VL_RED_BC,'
      'SUM(VL_IPI) VL_IPI,'
      'SUM(prod_vProd + prod_vFrete + prod_vOutro '
      '  - prod_vFrete + VL_IPI) VL_OPR '
      ''
      'FROM efdinnnfsits'
      'WHERE Controle>0'
      'GROUP BY CST_ICMS, CFOP, ALIQ_ICMS')
    Left = 856
    Top = 309
    object QrAnal0053CST_ICMS: TWideStringField
      FieldName = 'CST_ICMS'
      Required = True
      Size = 3
    end
    object QrAnal0053CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrAnal0053ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      Required = True
    end
    object QrAnal0053VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrAnal0053VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrAnal0053VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
    end
    object QrAnal0053VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
    end
    object QrAnal0053VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
    object QrAnal0053VL_IPI: TFloatField
      FieldName = 'VL_IPI'
    end
    object QrAnal0053VL_OPR: TFloatField
      FieldName = 'VL_OPR'
    end
  end
  object QrEfdInnCTsCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vic.*, '
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  '
      'FROM efdinnctscab vic '
      'LEFT JOIN entidades ter ON ter.Codigo=vic.Terceiro ')
    Left = 389
    Top = 415
    object QrEfdInnCTsCabMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnCTsCabMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnCTsCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnCTsCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnCTsCabControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnCTsCabIsLinked: TSmallintField
      FieldName = 'IsLinked'
      Required = True
    end
    object QrEfdInnCTsCabSqLinked: TIntegerField
      FieldName = 'SqLinked'
      Required = True
    end
    object QrEfdInnCTsCabCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrEfdInnCTsCabTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrEfdInnCTsCabPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrEfdInnCTsCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrEfdInnCTsCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrEfdInnCTsCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrEfdInnCTsCabValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrEfdInnCTsCabMotorista: TIntegerField
      FieldName = 'Motorista'
      Required = True
    end
    object QrEfdInnCTsCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrEfdInnCTsCabIND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabIND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabCOD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Required = True
      Size = 2
    end
    object QrEfdInnCTsCabCOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
      Required = True
    end
    object QrEfdInnCTsCabSER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEfdInnCTsCabSUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEfdInnCTsCabNUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Required = True
    end
    object QrEfdInnCTsCabCHV_CTE: TWideStringField
      FieldName = 'CHV_CTE'
      Size = 44
    end
    object QrEfdInnCTsCabCTeStatus: TIntegerField
      FieldName = 'CTeStatus'
      Required = True
    end
    object QrEfdInnCTsCabDT_DOC: TDateField
      FieldName = 'DT_DOC'
      Required = True
    end
    object QrEfdInnCTsCabDT_A_P: TDateField
      FieldName = 'DT_A_P'
      Required = True
    end
    object QrEfdInnCTsCabTP_CT_e: TSmallintField
      FieldName = 'TP_CT_e'
      Required = True
    end
    object QrEfdInnCTsCabCHV_CTE_REF: TWideStringField
      FieldName = 'CHV_CTE_REF'
      Size = 44
    end
    object QrEfdInnCTsCabVL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Required = True
    end
    object QrEfdInnCTsCabVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
    end
    object QrEfdInnCTsCabIND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEfdInnCTsCabVL_SERV: TFloatField
      FieldName = 'VL_SERV'
      Required = True
    end
    object QrEfdInnCTsCabVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_NT: TFloatField
      FieldName = 'VL_NT'
      Required = True
    end
    object QrEfdInnCTsCabCOD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrEfdInnCTsCabCOD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 30
    end
    object QrEfdInnCTsCabCOD_MUN_ORIG: TIntegerField
      FieldName = 'COD_MUN_ORIG'
      Required = True
    end
    object QrEfdInnCTsCabCOD_MUN_DEST: TIntegerField
      FieldName = 'COD_MUN_DEST'
      Required = True
    end
    object QrEfdInnCTsCabNFe_FatID: TIntegerField
      FieldName = 'NFe_FatID'
      Required = True
    end
    object QrEfdInnCTsCabNFe_FatNum: TIntegerField
      FieldName = 'NFe_FatNum'
      Required = True
    end
    object QrEfdInnCTsCabNFe_StaLnk: TSmallintField
      FieldName = 'NFe_StaLnk'
      Required = True
    end
    object QrEfdInnCTsCabVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
      Required = True
    end
    object QrEfdInnCTsCabVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrEfdInnCTsCabVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrEfdInnCTsCabVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
      Required = True
    end
    object QrEfdInnCTsCabCST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabCFOP: TIntegerField
      FieldName = 'CFOP'
      Required = True
    end
    object QrEfdInnCTsCabALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_OPR: TFloatField
      FieldName = 'VL_OPR'
      Required = True
    end
    object QrEfdInnCTsCabVL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
      Required = True
    end
    object QrEfdInnCTsCabCOD_OBS: TWideStringField
      FieldName = 'COD_OBS'
      Size = 6
    end
  end
  object QrNatOp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfa.FisRegCad, frc.ide_natOp'
      'FROM nfecaba nfa'
      'LEFT JOIN fisregcad frc ON frc.Codigo=nfa.FisRegCad'
      'WHERE nfa.DataFiscal BETWEEN "2022-01-01" AND "2022-02-28"'
      'GROUP BY nfa.FisRegCad')
    Left = 408
    Top = 497
    object QrNatOpFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
      Required = True
    end
    object QrNatOpide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
  end
  object QrCAT66SP2018: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT its.CFOP'
      'FROM efdinnnfsits its'
      'LEFT JOIN efdinnnfscab cab ON cab.Controle=its.Controle'
      'WHERE cab.NFe_FatID=53'
      'AND cab.Empresa=-11'
      'AND cab.NFe_FatNum IN (194, 198, 200, 208, 236, 251)'
      'AND EFD_II_C195=1'
      'ORDER BY CFOP')
    Left = 624
    Top = 193
    object QrCAT66SP2018CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrCAT66SP2018AjusteVL_OUTROS: TFloatField
      FieldName = 'AjusteVL_OUTROS'
    end
  end
end
