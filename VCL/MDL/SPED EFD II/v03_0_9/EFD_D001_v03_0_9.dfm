object FmEFD_D001_v03_0_9: TFmEFD_D001_v03_0_9
  Left = 368
  Top = 194
  Caption = 'EFD-SPEDD-001 :: Servi'#231'os'
  ClientHeight = 692
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 113
    Width = 1008
    Height = 579
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PCPeriodos: TPageControl
      Left = 0
      Top = 57
      Width = 1008
      Height = 522
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet2: TTabSheet
        Caption = ' Per'#237'odos '
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 494
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Panel9: TPanel
            Left = 103
            Top = 1
            Width = 896
            Height = 403
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 896
              Height = 403
              Align = alClient
              TabOrder = 0
              object PCServicos: TPageControl
                Left = 2
                Top = 15
                Width = 892
                Height = 386
                ActivePage = TabSheet1
                Align = alClient
                TabOrder = 0
                object TabSheet1: TTabSheet
                  Caption = 'D100 :: Nota Fiscal de Servi'#231'o de Transporte'
                  object dmkDBGridZTO1: TdmkDBGridZTO
                    Left = 0
                    Top = 0
                    Width = 884
                    Height = 358
                    Align = alClient
                    DataSource = DsEFD_D100
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'DT_DOC'
                        Title.Caption = 'Dta doc.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DT_A_P'
                        Title.Caption = 'Dta sai/ent.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CFOP'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CST_ICMS'
                        Title.Caption = 'CST'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_MOD'
                        Title.Caption = 'Mod.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SER'
                        Title.Caption = 'S'#233'rie'
                        Width = 32
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SUB'
                        Title.Caption = 'Sub'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NUM_DOC'
                        Title.Caption = 'N'#186' N.F.'
                        Width = 60
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_SIT'
                        Title.Caption = 'Sit.'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Terceiro'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_TERC'
                        Title.Caption = 'Nome fornecedor'
                        Width = 92
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_DOC'
                        Title.Caption = '$ Docum.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_SERV'
                        Title.Caption = '$ Servi'#231'o'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ALIQ_ICMS'
                        Title.Caption = '%ICMS'
                        Width = 40
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_ICMS'
                        Title.Caption = '$ ICMS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'IND_FRT'
                        Title.Caption = 'Frete'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CHV_CTE'
                        Title.Caption = 'Chave CT-e'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'TP_CTE'
                        Title.Caption = 'Tipo CT-e'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CHV_CTE_REF'
                        Title.Caption = 'Chave CT-e referenciada'
                        Visible = True
                      end>
                  end
                end
                object TabSheet3: TTabSheet
                  Caption = 'D500 :: Comunica'#231#227'o (21)  e Telecomunica'#231#227'o (22)'
                  ImageIndex = 1
                  object dmkDBGrid1: TdmkDBGrid
                    Left = 0
                    Top = 0
                    Width = 884
                    Height = 358
                    Align = alClient
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'DT_DOC'
                        Title.Caption = 'Dta doc.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DT_A_P'
                        Title.Caption = 'Dta sai/ent.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CFOP'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CST_ICMS'
                        Title.Caption = 'CST'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_MOD'
                        Title.Caption = 'Mod.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SER'
                        Title.Caption = 'S'#233'rie'
                        Width = 32
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SUB'
                        Title.Caption = 'Sub'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NUM_DOC'
                        Title.Caption = 'N'#186' N.F.'
                        Width = 60
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_SIT'
                        Title.Caption = 'Sit.'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Terceiro'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_TERC'
                        Title.Caption = 'Nome fornecedor'
                        Width = 92
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_DOC'
                        Title.Caption = '$ Docum.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_SERV'
                        Title.Caption = '$ Servi'#231'o'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ALIQ_ICMS'
                        Title.Caption = '%ICMS'
                        Width = 40
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_ICMS'
                        Title.Caption = '$ ICMS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_PIS'
                        Title.Caption = 'PIS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_COFINS'
                        Title.Caption = 'COFINS'
                        Visible = True
                      end>
                    Color = clWindow
                    DataSource = DsEFD_D500
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'DT_DOC'
                        Title.Caption = 'Dta doc.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DT_A_P'
                        Title.Caption = 'Dta sai/ent.'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CFOP'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CST_ICMS'
                        Title.Caption = 'CST'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_MOD'
                        Title.Caption = 'Mod.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SER'
                        Title.Caption = 'S'#233'rie'
                        Width = 32
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SUB'
                        Title.Caption = 'Sub'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NUM_DOC'
                        Title.Caption = 'N'#186' N.F.'
                        Width = 60
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COD_SIT'
                        Title.Caption = 'Sit.'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Terceiro'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_TERC'
                        Title.Caption = 'Nome fornecedor'
                        Width = 92
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_DOC'
                        Title.Caption = '$ Docum.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_SERV'
                        Title.Caption = '$ Servi'#231'o'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ALIQ_ICMS'
                        Title.Caption = '%ICMS'
                        Width = 40
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_ICMS'
                        Title.Caption = '$ ICMS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_PIS'
                        Title.Caption = 'PIS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VL_COFINS'
                        Title.Caption = 'COFINS'
                        Visible = True
                      end>
                  end
                end
              end
            end
          end
          object Panel10: TPanel
            Left = 1
            Top = 1
            Width = 102
            Height = 403
            Align = alLeft
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object DBGCab: TdmkDBGrid
              Left = 0
              Top = 0
              Width = 102
              Height = 403
              Align = alClient
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'MES_ANO'
                  Title.Caption = 'MES / ANO'
                  Width = 63
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsEFD_D001
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'MES_ANO'
                  Title.Caption = 'MES / ANO'
                  Width = 63
                  Visible = True
                end>
            end
          end
          object TGroupBox
            Left = 1
            Top = 429
            Width = 998
            Height = 64
            Align = alBottom
            TabOrder = 2
            object Panel3: TPanel
              Left = 2
              Top = 15
              Width = 994
              Height = 47
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object BtD500: TBitBtn
                Tag = 10084
                Left = 252
                Top = 2
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = 'D&500'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtD500Click
              end
              object BtPeriodo: TBitBtn
                Tag = 191
                Left = 12
                Top = 2
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = '&Per'#237'odo'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtPeriodoClick
              end
              object Panel2: TPanel
                Left = 885
                Top = 0
                Width = 109
                Height = 47
                Align = alRight
                Alignment = taRightJustify
                BevelOuter = bvNone
                TabOrder = 2
                object BtSaida0: TBitBtn
                  Tag = 13
                  Left = 4
                  Top = 2
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Sa'#237'da'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtSaida0Click
                end
              end
              object BtD100: TBitBtn
                Tag = 10085
                Left = 132
                Top = 2
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = 'D&100'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 3
                OnClick = BtD100Click
              end
            end
          end
          object Panel16: TPanel
            Left = 1
            Top = 404
            Width = 998
            Height = 25
            Align = alBottom
            TabOrder = 3
          end
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 57
      Align = alTop
      Caption = ' Empresa e per'#237'odo selecionado: '
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 40
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 23
          Height = 13
          Caption = 'Filial:'
        end
        object Label2: TLabel
          Left = 932
          Top = 0
          Width = 60
          Height = 13
          Caption = 'M'#202'S / ANO:'
          Enabled = False
          FocusControl = DBEdit1
        end
        object EdEmpresa: TdmkEditCB
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 64
          Top = 16
          Width = 865
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object DBEdit1: TDBEdit
          Left = 932
          Top = 16
          Width = 65
          Height = 21
          DataField = 'MES_ANO'
          DataSource = DsEFD_D001
          Enabled = False
          TabOrder = 2
        end
      end
    end
  end
  object Panel11: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 104
        Height = 32
        Caption = 'Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 104
        Height = 32
        Caption = 'Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 104
        Height = 32
        Caption = 'Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 65
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel12: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 48
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 31
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object DsEFD_D001: TDataSource
    DataSet = QrEFD_D001
    Left = 204
    Top = 352
  end
  object QrEFD_D001: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEFD_D001AfterOpen
    BeforeClose = QrEFD_D001BeforeClose
    AfterScroll = QrEFD_D001AfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT,'
      'CONCAT(RIGHT(d001.AnoMes, 2), "/",'
      'LEFT(LPAD(d001.AnoMes, 6, "0"), 4)) MES_ANO, d001.*'
      'FROM tbspedefdd001 d001'
      'LEFT JOIN entidades ent ON ent.Codigo=d001.Empresa'
      'WHERE d001.ImporExpor=1'
      'AND d001.Empresa=-11'
      'ORDER BY d001.AnoMes DESC'
      '')
    Left = 204
    Top = 304
    object QrEFD_D001NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrEFD_D001MES_ANO: TWideStringField
      FieldName = 'MES_ANO'
      Required = True
      Size = 7
    end
    object QrEFD_D001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
      Origin = 'tbspedefdd001.ImporExpor'
    end
    object QrEFD_D001AnoMes: TIntegerField
      FieldName = 'AnoMes'
      Origin = 'tbspedefdd001.AnoMes'
    end
    object QrEFD_D001Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'tbspedefdd001.Empresa'
    end
    object QrEFD_D001LinArq: TIntegerField
      FieldName = 'LinArq'
      Origin = 'tbspedefdd001.LinArq'
    end
    object QrEFD_D001REG: TWideStringField
      FieldName = 'REG'
      Origin = 'tbspedefdd001.REG'
      Size = 4
    end
    object QrEFD_D001IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Origin = 'tbspedefdd001.IND_MOV'
      Size = 1
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtPeriodo
    CanUpd01 = BtD500
    Left = 68
    Top = 16
  end
  object PMPeriodo: TPopupMenu
    OnPopup = PMPeriodoPopup
    Left = 36
    Top = 608
    object Incluinovoperiodo1: TMenuItem
      Caption = 'Inclui novo periodo'
      OnClick = Incluinovoperiodo1Click
    end
    object Excluiperiodoselecionado1: TMenuItem
      Caption = '&Exclui periodo selecionado'
      Enabled = False
      OnClick = Excluiperiodoselecionado1Click
    end
    object Itemns1: TMenuItem
      Caption = '&Item(ns)'
      Enabled = False
      Visible = False
    end
  end
  object PMD500: TPopupMenu
    OnPopup = PMD500Popup
    Left = 304
    Top = 596
    object IncluiD500: TMenuItem
      Caption = 'Inclui &nova NF D500'
      OnClick = IncluiD500Click
    end
    object AlteraD500: TMenuItem
      Caption = '&Altera NF D500 selecionada'
      OnClick = AlteraD500Click
    end
    object ExcluiD500: TMenuItem
      Caption = '&Exclui NF D500 selecionada'
      OnClick = ExcluiD500Click
    end
  end
  object QrEFD_D500: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) '
      'NO_TERC, d500.*'
      'FROM tbspedefdd500 d500 '
      'LEFT JOIN entidades ent ON ent.Codigo=d500.Terceiro'
      'WHERE d500.ImporExpor=3 '
      'AND d500.Empresa=-11 '
      'AND d500.AnoMes=201001'
      '')
    Left = 396
    Top = 304
    object QrEFD_D500NO_TERC: TWideStringField
      FieldName = 'NO_TERC'
      Size = 100
    end
    object QrEFD_D500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
      Origin = 'tbspedefdd500.ImporExpor'
    end
    object QrEFD_D500AnoMes: TIntegerField
      FieldName = 'AnoMes'
      Origin = 'tbspedefdd500.AnoMes'
    end
    object QrEFD_D500Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'tbspedefdd500.Empresa'
    end
    object QrEFD_D500LinArq: TIntegerField
      FieldName = 'LinArq'
      Origin = 'tbspedefdd500.LinArq'
    end
    object QrEFD_D500REG: TWideStringField
      FieldName = 'REG'
      Origin = 'tbspedefdd500.REG'
      Size = 4
    end
    object QrEFD_D500IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Origin = 'tbspedefdd500.IND_OPER'
      Size = 1
    end
    object QrEFD_D500IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Origin = 'tbspedefdd500.IND_EMIT'
      Size = 1
    end
    object QrEFD_D500COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Origin = 'tbspedefdd500.COD_PART'
      Size = 60
    end
    object QrEFD_D500COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Origin = 'tbspedefdd500.COD_MOD'
      Size = 2
    end
    object QrEFD_D500COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Origin = 'tbspedefdd500.COD_SIT'
      Size = 2
    end
    object QrEFD_D500SER: TWideStringField
      FieldName = 'SER'
      Origin = 'tbspedefdd500.SER'
      Size = 4
    end
    object QrEFD_D500SUB: TWideStringField
      FieldName = 'SUB'
      Origin = 'tbspedefdd500.SUB'
      Size = 3
    end
    object QrEFD_D500NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Origin = 'tbspedefdd500.NUM_DOC'
    end
    object QrEFD_D500DT_DOC: TDateField
      FieldName = 'DT_DOC'
      Origin = 'tbspedefdd500.DT_DOC'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_D500DT_A_P: TDateField
      FieldName = 'DT_A_P'
      Origin = 'tbspedefdd500.DT_A_P'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_D500VL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Origin = 'tbspedefdd500.VL_DOC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_D500VL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Origin = 'tbspedefdd500.VL_DESC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_D500VL_SERV: TFloatField
      FieldName = 'VL_SERV'
      Origin = 'tbspedefdd500.VL_SERV'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_D500VL_SERV_NT: TFloatField
      FieldName = 'VL_SERV_NT'
      Origin = 'tbspedefdd500.VL_SERV_NT'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_D500VL_TERC: TFloatField
      FieldName = 'VL_TERC'
      Origin = 'tbspedefdd500.VL_TERC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_D500VL_DA: TFloatField
      FieldName = 'VL_DA'
      Origin = 'tbspedefdd500.VL_DA'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_D500VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Origin = 'tbspedefdd500.VL_BC_ICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_D500VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Origin = 'tbspedefdd500.VL_ICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_D500COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Origin = 'tbspedefdd500.COD_INF'
      Size = 6
    end
    object QrEFD_D500VL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Origin = 'tbspedefdd500.VL_PIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_D500VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Origin = 'tbspedefdd500.VL_COFINS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_D500COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Origin = 'tbspedefdd500.COD_CTA'
      Size = 255
    end
    object QrEFD_D500TP_ASSINANTE: TWideStringField
      FieldName = 'TP_ASSINANTE'
      Origin = 'tbspedefdd500.TP_ASSINANTE'
      Size = 1
    end
    object QrEFD_D500Terceiro: TIntegerField
      FieldName = 'Terceiro'
      Origin = 'tbspedefdd500.Terceiro'
    end
    object QrEFD_D500Importado: TSmallintField
      FieldName = 'Importado'
      Origin = 'tbspedefdd500.Importado'
    end
    object QrEFD_D500CST_ICMS: TWideStringField
      FieldName = 'CST_ICMS'
      Origin = 'tbspedefdd500.CST_ICMS'
      Size = 3
    end
    object QrEFD_D500CFOP: TWideStringField
      FieldName = 'CFOP'
      Origin = 'tbspedefdd500.CFOP'
      Size = 4
    end
    object QrEFD_D500ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      Origin = 'tbspedefdd500.ALIQ_ICMS'
    end
    object QrEFD_D500VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
      Origin = 'tbspedefdd500.VL_RED_BC'
    end
  end
  object DsEFD_D500: TDataSource
    DataSet = QrEFD_D500
    Left = 396
    Top = 352
  end
  object QrEFD_D100: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) '
      'NO_TERC, d100.*'
      'FROM tbspedefdd100 d100 '
      'LEFT JOIN entidades ent ON ent.Codigo=d100.Terceiro'
      'WHERE d100.ImporExpor=3 '
      'AND d100.Empresa=-11 '
      'AND d100.AnoMes=201001'
      '')
    Left = 300
    Top = 304
    object QrEFD_D100NO_TERC: TWideStringField
      FieldName = 'NO_TERC'
      Size = 100
    end
    object QrEFD_D100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_D100AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_D100Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_D100LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_D100REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_D100IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Size = 1
    end
    object QrEFD_D100IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Size = 1
    end
    object QrEFD_D100COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_D100COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrEFD_D100COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Size = 2
    end
    object QrEFD_D100SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEFD_D100SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEFD_D100NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrEFD_D100CHV_CTE: TWideStringField
      FieldName = 'CHV_CTE'
      Size = 44
    end
    object QrEFD_D100DT_DOC: TDateField
      FieldName = 'DT_DOC'
    end
    object QrEFD_D100DT_A_P: TDateField
      FieldName = 'DT_A_P'
    end
    object QrEFD_D100TP_CTE: TSmallintField
      FieldName = 'TP_CTE'
      Required = True
    end
    object QrEFD_D100CHV_CTE_REF: TWideStringField
      FieldName = 'CHV_CTE_REF'
      Size = 44
    end
    object QrEFD_D100VL_DOC: TFloatField
      FieldName = 'VL_DOC'
    end
    object QrEFD_D100VL_DESC: TFloatField
      FieldName = 'VL_DESC'
    end
    object QrEFD_D100IND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEFD_D100VL_SERV: TFloatField
      FieldName = 'VL_SERV'
    end
    object QrEFD_D100VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrEFD_D100VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrEFD_D100VL_NT: TFloatField
      FieldName = 'VL_NT'
    end
    object QrEFD_D100COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrEFD_D100COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEFD_D100Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEFD_D100Importado: TSmallintField
      FieldName = 'Importado'
    end
    object QrEFD_D100Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_D100DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_D100DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_D100UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_D100UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_D100AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_D100Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_D100CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrEFD_D100CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrEFD_D100ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrEFD_D100VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
  end
  object DsEFD_D100: TDataSource
    DataSet = QrEFD_D100
    Left = 300
    Top = 352
  end
  object PMD100: TPopupMenu
    OnPopup = PMD500Popup
    Left = 184
    Top = 592
    object IncluinovaNFD1001: TMenuItem
      Caption = 'Inclui &nova NF D100'
      OnClick = IncluinovaNFD1001Click
    end
    object AlteraNFD100selecionada1: TMenuItem
      Caption = '&Altera NF D100 selecionada'
      OnClick = AlteraNFD100selecionada1Click
    end
    object ExcluiNFD100selecionada1: TMenuItem
      Caption = '&Exclui NF D100 selecionada'
      OnClick = ExcluiNFD100selecionada1Click
    end
  end
end
