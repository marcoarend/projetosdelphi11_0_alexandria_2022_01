unit EfdInnD001Ger;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Menus, ComCtrls, Grids,
  DBGrids, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, Variants,
  dmkDBGrid, frxClass, frxDBSet, dmkCheckGroup, DmkDAC_PF, dmkImage, UnDmkEnums,
  dmkDBGridZTO;

type
  TFmEfdInnD001Ger = class(TForm)
    PainelDados: TPanel;
    DsEFD_D001: TDataSource;
    QrEFD_D001: TmySQLQuery;
    dmkPermissoes1: TdmkPermissoes;
    PMPeriodo: TPopupMenu;
    PCPeriodos: TPageControl;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    Itemns1: TMenuItem;
    Panel9: TPanel;
    Panel10: TPanel;
    DBGCab: TdmkDBGrid;
    Panel11: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel12: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    DBEdit1: TDBEdit;
    Panel3: TPanel;
    BtD500: TBitBtn;
    BtPeriodo: TBitBtn;
    Panel2: TPanel;
    BtSaida0: TBitBtn;
    PB1: TProgressBar;
    Panel16: TPanel;
    PMD500: TPopupMenu;
    GroupBox3: TGroupBox;
    Incluinovoperiodo1: TMenuItem;
    Excluiperiodoselecionado1: TMenuItem;
    QrEfdInnD500Cab: TMySQLQuery;
    DsEfdInnD500Cab: TDataSource;
    PCServicos: TPageControl;
    TabSheet1: TTabSheet;
    IncluiD500: TMenuItem;
    AlteraD500: TMenuItem;
    ExcluiD500: TMenuItem;
    QrEFD_D001NO_ENT: TWideStringField;
    QrEFD_D001MES_ANO: TWideStringField;
    QrEFD_D001ImporExpor: TSmallintField;
    QrEFD_D001AnoMes: TIntegerField;
    QrEFD_D001Empresa: TIntegerField;
    QrEFD_D001LinArq: TIntegerField;
    QrEFD_D001REG: TWideStringField;
    QrEFD_D001IND_MOV: TWideStringField;
    QrEfdInnD500CabNO_TERC: TWideStringField;
    QrEfdInnD500CabAnoMes: TIntegerField;
    QrEfdInnD500CabEmpresa: TIntegerField;
    QrEfdInnD500CabIND_OPER: TWideStringField;
    QrEfdInnD500CabIND_EMIT: TWideStringField;
    QrEfdInnD500CabCOD_MOD: TWideStringField;
    QrEfdInnD500CabSER: TWideStringField;
    QrEfdInnD500CabSUB: TWideStringField;
    QrEfdInnD500CabNUM_DOC: TIntegerField;
    QrEfdInnD500CabDT_DOC: TDateField;
    QrEfdInnD500CabDT_A_P: TDateField;
    QrEfdInnD500CabVL_DOC: TFloatField;
    QrEfdInnD500CabVL_DESC: TFloatField;
    QrEfdInnD500CabVL_SERV: TFloatField;
    QrEfdInnD500CabVL_SERV_NT: TFloatField;
    QrEfdInnD500CabVL_TERC: TFloatField;
    QrEfdInnD500CabVL_DA: TFloatField;
    QrEfdInnD500CabVL_BC_ICMS: TFloatField;
    QrEfdInnD500CabVL_ICMS: TFloatField;
    QrEfdInnD500CabCOD_INF: TWideStringField;
    QrEfdInnD500CabVL_PIS: TFloatField;
    QrEfdInnD500CabVL_COFINS: TFloatField;
    QrEfdInnD500CabCOD_CTA: TWideStringField;
    QrEfdInnD500CabTP_ASSINANTE: TWideStringField;
    QrEfdInnD500CabTerceiro: TIntegerField;
    QrEfdInnD500CabImportado: TSmallintField;
    QrEfdInnD500CabCOD_SIT: TWideStringField;
    TabSheet3: TTabSheet;
    dmkDBGrid1: TdmkDBGrid;
    QrEFD_D100: TmySQLQuery;
    DsEFD_D100: TDataSource;
    dmkDBGridZTO1: TdmkDBGridZTO;
    BtD100: TBitBtn;
    PMD100: TPopupMenu;
    IncluinovaNFD1001: TMenuItem;
    AlteraNFD100selecionada1: TMenuItem;
    ExcluiNFD100selecionada1: TMenuItem;
    QrEFD_D100NO_TERC: TWideStringField;
    QrEFD_D100ImporExpor: TSmallintField;
    QrEFD_D100AnoMes: TIntegerField;
    QrEFD_D100Empresa: TIntegerField;
    QrEFD_D100LinArq: TIntegerField;
    QrEFD_D100REG: TWideStringField;
    QrEFD_D100IND_OPER: TWideStringField;
    QrEFD_D100IND_EMIT: TWideStringField;
    QrEFD_D100COD_PART: TWideStringField;
    QrEFD_D100COD_MOD: TWideStringField;
    QrEFD_D100COD_SIT: TWideStringField;
    QrEFD_D100SER: TWideStringField;
    QrEFD_D100SUB: TWideStringField;
    QrEFD_D100NUM_DOC: TIntegerField;
    QrEFD_D100CHV_CTE: TWideStringField;
    QrEFD_D100DT_DOC: TDateField;
    QrEFD_D100DT_A_P: TDateField;
    QrEFD_D100TP_CTE: TSmallintField;
    QrEFD_D100CHV_CTE_REF: TWideStringField;
    QrEFD_D100VL_DOC: TFloatField;
    QrEFD_D100VL_DESC: TFloatField;
    QrEFD_D100IND_FRT: TWideStringField;
    QrEFD_D100VL_SERV: TFloatField;
    QrEFD_D100VL_BC_ICMS: TFloatField;
    QrEFD_D100VL_ICMS: TFloatField;
    QrEFD_D100VL_NT: TFloatField;
    QrEFD_D100COD_INF: TWideStringField;
    QrEFD_D100COD_CTA: TWideStringField;
    QrEFD_D100Terceiro: TIntegerField;
    QrEFD_D100Importado: TSmallintField;
    QrEFD_D100Lk: TIntegerField;
    QrEFD_D100DataCad: TDateField;
    QrEFD_D100DataAlt: TDateField;
    QrEFD_D100UserCad: TIntegerField;
    QrEFD_D100UserAlt: TIntegerField;
    QrEFD_D100AlterWeb: TSmallintField;
    QrEFD_D100Ativo: TSmallintField;
    QrEFD_D100CST_ICMS: TIntegerField;
    QrEFD_D100CFOP: TIntegerField;
    QrEFD_D100ALIQ_ICMS: TFloatField;
    QrEFD_D100VL_RED_BC: TFloatField;
    QrEfdInnD500CabCodigo: TIntegerField;
    QrEfdInnD500CabControle: TIntegerField;
    QrEfdInnD500Its: TMySQLQuery;
    QrEfdInnD500ItsAnoMes: TIntegerField;
    QrEfdInnD500ItsEmpresa: TIntegerField;
    QrEfdInnD500ItsCodigo: TIntegerField;
    QrEfdInnD500ItsControle: TIntegerField;
    QrEfdInnD500ItsConta: TIntegerField;
    QrEfdInnD500ItsGraGruX: TIntegerField;
    QrEfdInnD500ItsQTD: TFloatField;
    QrEfdInnD500ItsUNID: TWideStringField;
    QrEfdInnD500ItsVL_ITEM: TFloatField;
    QrEfdInnD500ItsVL_DESC: TFloatField;
    QrEfdInnD500ItsCST_ICMS: TIntegerField;
    QrEfdInnD500ItsCFOP: TIntegerField;
    QrEfdInnD500ItsVL_BC_ICMS: TFloatField;
    QrEfdInnD500ItsALIQ_ICMS: TFloatField;
    QrEfdInnD500ItsVL_ICMS: TFloatField;
    QrEfdInnD500ItsVL_BC_ICMS_ST: TFloatField;
    QrEfdInnD500ItsALIQ_ST: TFloatField;
    QrEfdInnD500ItsVL_ICMS_ST: TFloatField;
    QrEfdInnD500ItsCST_PIS: TWideStringField;
    QrEfdInnD500ItsNAT_BC_CRED: TWideStringField;
    QrEfdInnD500ItsVL_BC_PIS: TFloatField;
    QrEfdInnD500ItsALIQ_PIS: TFloatField;
    QrEfdInnD500ItsVL_PIS: TFloatField;
    QrEfdInnD500ItsCOD_CTA: TWideStringField;
    QrEfdInnD500ItsCST_COFINS: TWideStringField;
    QrEfdInnD500ItsVL_BC_COFINS: TFloatField;
    QrEfdInnD500ItsALIQ_COFINS: TFloatField;
    QrEfdInnD500ItsVL_COFINS: TFloatField;
    QrEfdInnD500ItsNO_PRD_TAM_COR: TWideStringField;
    QrEfdInnD500ItsSIGLAUNIDMED: TWideStringField;
    DsEfdInnD500Its: TDataSource;
    N1: TMenuItem;
    IncluiitemNFD5001: TMenuItem;
    AlteraitemNFD500selecionada1: TMenuItem;
    ExcluiitemNFD500selecionada1: TMenuItem;
    DBGrid1: TDBGrid;
    Splitter1: TSplitter;
    QrEfdInnD500ItsVL_Outro: TFloatField;
    QrEfdInnD500ItsVL_OPR: TFloatField;
    QrEfdInnD500ItsVL_RED_BC: TFloatField;
    QrEfdInnD500ItsVL_BC_ICMS_UF: TFloatField;
    QrEfdInnD500ItsVL_ICMS_UF: TFloatField;
    QrSum: TMySQLQuery;
    QrSumVL_BC_ICMS: TFloatField;
    QrSumVL_ICMS: TFloatField;
    QrSumVL_BC_ICMS_UF: TFloatField;
    QrSumVL_ICMS_UF: TFloatField;
    QrSumVL_RED_BC: TFloatField;
    QrSumVL_PIS: TFloatField;
    QrSumVL_COFINS: TFloatField;
    procedure BtD500Click(Sender: TObject);
    procedure BtSaida0Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPeriodoClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrEFD_D001BeforeClose(DataSet: TDataSet);
    procedure QrEFD_D001AfterScroll(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure QrEFD_D001AfterOpen(DataSet: TDataSet);
    procedure PMPeriodoPopup(Sender: TObject);
    procedure RGEmitenteClick(Sender: TObject);
    procedure PMD500Popup(Sender: TObject);
    procedure Incluinovoperiodo1Click(Sender: TObject);
    procedure Excluiperiodoselecionado1Click(Sender: TObject);
    procedure IncluiD500Click(Sender: TObject);
    procedure AlteraD500Click(Sender: TObject);
    procedure ExcluiD500Click(Sender: TObject);
    procedure IncluinovaNFD1001Click(Sender: TObject);
    procedure AlteraNFD100selecionada1Click(Sender: TObject);
    procedure ExcluiNFD100selecionada1Click(Sender: TObject);
    procedure BtD100Click(Sender: TObject);
    procedure QrEfdInnD500CabAfterScroll(DataSet: TDataSet);
    procedure QrEfdInnD500CabBeforeClose(DataSet: TDataSet);
    procedure IncluiitemNFD5001Click(Sender: TObject);
    procedure AlteraitemNFD500selecionada1Click(Sender: TObject);
    procedure ExcluiitemNFD500selecionada1Click(Sender: TObject);
  private
    { Private declarations }
    function  PeriodoJaExiste(ImporExpor, AnoMes, Empresa: Integer;
              Avisa: Boolean): Boolean;
    procedure InsUpdD100(SQLType: TSQLType);
    procedure InsUpdEfdInnD500Cab(SQLType: TSQLType);
    procedure InsUpdEfdInnD500Its(SQLType: TSQLType);
    //

  public
    { Public declarations }
    FEmpresa: Integer;
    FExpImpTXT, FEmprTXT: String;
    //
    procedure ReopenEFD_D001(AnoMes: Integer);
    procedure ReopenEFD_D100(LinArq: Integer);

    procedure ReopenEfdInnd500Cab(Controle: Integer);
    procedure ReopenEfdInnd500Its(Conta: Integer);

    procedure AtualizaTotaisD500(Controle: Integer);

  end;

var
  FmEfdInnD001Ger: TFmEfdInnD001Ger;

implementation

uses UnMyObjects, Module, MyDBCheck, ModuleGeral, ModProd, UCreate,
CfgExpFile, UnGrade_Tabs, ModuleNFe_0000,
  Principal, Periodo, (*EFD_E100, EFD_E110, EFD_E111, EFD_E116,
  EFD_E112, EFD_E113, EFD_E115, EFD_D100,*) EfdInnD500Cab, EfdInnD500Its;

{$R *.DFM}

const
  FFormatFloat = '00000';
  FImporExpor = 3; // Criar! N�o Mexer

procedure TFmEfdInnD001Ger.EdEmpresaChange(Sender: TObject);
begin
  if EdEmpresa.ValueVariant <> 0 then
  begin
    FEmpresa := DModG.QrEmpresasCodigo.Value;
    FEmprTXT := FormatFloat('0', FEmpresa);
  end else
  begin
    FEmpresa := 0;
    FEmprTXT := '0';
  end;
  BtPeriodo.Enabled := FEmpresa <> 0;
  ReopenEFD_D001(0);
end;

procedure TFmEfdInnD001Ger.ExcluiD500Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da NF Tipo D500?') = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdinnd500cab', [
    'Controle'], ['='],
    [QrEfdInnd500CabControle.Value], '') then
    //
    ReopenEfdInnD500Cab(0);
  end;
end;

procedure TFmEfdInnD001Ger.ExcluiitemNFD500selecionada1Click(Sender: TObject);
var
  Controle, Conta: Integer;
begin
  if QrEfdInnD500Its.RecordCount > 0 then
  begin
    if UMyMod.ExcluiRegistroInt1('Confirma a retirada do item selecionado?',
    'EfdInnD500Its', 'Conta', QrEfdInnD500ItsConta.Value, Dmod.MyDB) = ID_YES then
    begin
      Controle := QrEfdInnD500ItsControle.Value;
      Conta := GOTOy.LocalizaPriorNextIntQr(QrEfdInnD500Its,
        QrEfdInnD500ItsConta, QrEfdInnD500ItsConta.Value);
      AtualizaTotaisD500(Controle);
      ReopenEfdInnD500Cab(Controle);
      ReopenEfdInnD500Its(Conta);
    end;
  end;
end;

procedure TFmEfdInnD001Ger.ExcluiNFD100selecionada1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o da NF Tipo D100?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipid100', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [QrEFD_D100ImporExpor.Value, QrEFD_D100AnoMes.Value,
    QrEFD_D100Empresa.Value, QrEFD_D100LinArq.Value], '') then
    //
    ReopenEFD_D100(0);
  end;
end;

procedure TFmEfdInnD001Ger.Excluiperiodoselecionado1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do per�odo selecionado?',
  'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipid001', [
    'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
    [QrEFD_D001ImporExpor.Value, QrEFD_D001AnoMes.Value,
    QrEFD_D001Empresa.Value], '') then
    ReopenEFD_D001(0);
  end;
end;

procedure TFmEfdInnD001Ger.IncluiD500Click(Sender: TObject);
begin
  InsUpdEfdInnD500Cab(stIns);
end;

procedure TFmEfdInnD001Ger.IncluiitemNFD5001Click(Sender: TObject);
begin
  InsUpdEfdInnD500Its(stIns);
end;

procedure TFmEfdInnD001Ger.IncluinovaNFD1001Click(Sender: TObject);
begin
  InsUpdD100(stIns);
end;

procedure TFmEfdInnD001Ger.Incluinovoperiodo1Click(Sender: TObject);
const
  LinArq = 0;
  REG = 'E001';
  IND_MOV = 1;
var
  Ano, Mes, Dia: Word;
  Cancelou: Boolean;
  AnoMes: Integer;
begin
  DecodeDate(Date, Ano, Mes, Dia);
  MLAGeral.EscolhePeriodo_MesEAno(TFmPeriodo, FmPeriodo, Mes, Ano, Cancelou, True, True);
  if not Cancelou then
  begin
    AnoMes := (Ano * 100) + Mes;
    //ShowMessage(FormatFloat('0', (Ano * 100) + Mes));
    if not PeriodoJaExiste(FImporExpor, AnoMes, FEmpresa, True) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efdicmsipid001', False, [
      'LinArq', 'REG', 'IND_MOV'], [
      'ImporExpor', 'AnoMes', 'Empresa'], [
      LinArq, REG, IND_MOV], [
      FImporExpor, AnoMes, FEmpresa], True) then
        ReopenEFD_D001(AnoMes);
    end;
    ReopenEFD_D001(AnoMes);
  end;
end;

procedure TFmEfdInnD001Ger.InsUpdD100(SQLType: TSQLType);
begin
{
  if UmyMod.FormInsUpd_Cria(TFmEFD_D100, FmEFD_D100, afmoNegarComAviso,
  QrEFD_D100, SQLType) then
  begin
    FmEFD_D100.FAnoMes := QrEFD_D001AnoMes.Value;
    FmEFD_D100.ReopenTabelas();
    FmEFD_D100.ImgTipo.SQLType := SQLType;
    FmEFD_D100.EdImporExpor.ValueVariant := QrEFD_D001ImporExpor.Value;
    FmEFD_D100.EdAnoMes.ValueVariant := QrEFD_D001AnoMes.Value;
    FmEFD_D100.EdEmpresa.ValueVariant := QrEFD_D001Empresa.Value;
    //
    if SQLType = stIns then
    begin
      FmEFD_D100.RGIND_OPER.ItemIndex := 0;
      FmEFD_D100.RGIND_EMIT.ItemIndex := 1;
      FmEFD_D100.TPDT_DOC.Date := Date;
      FmEFD_D100.TPDT_A_P.Date := Date;
    end else
    begin
    end;
    //
    FmEFD_D100.ShowModal;
    FmEFD_D100.Destroy;
  end;
}
end;

procedure TFmEfdInnD001Ger.InsUpdEfdInnD500Cab(SQLType: TSQLType);
(*
begin
  if UmyMod.FormInsUpd_Cria(TFmEFD_D500_v03_0_9, FmEFD_D500_v03_0_9, afmoNegarComAviso,
  QrEFD_D500, SQLType) then
  begin
    FmEFD_D500_v03_0_9.FAnoMes := QrEFD_D001AnoMes.Value;
    FmEFD_D500_v03_0_9.ReopenTabelas();
    FmEFD_D500_v03_0_9.ImgTipo.SQLType := SQLType;
    FmEFD_D500_v03_0_9.EdImporExpor.ValueVariant := QrEFD_D001ImporExpor.Value;
    FmEFD_D500_v03_0_9.EdAnoMes.ValueVariant := QrEFD_D001AnoMes.Value;
    FmEFD_D500_v03_0_9.EdEmpresa.ValueVariant := QrEFD_D001Empresa.Value;
    //
    if SQLType = stIns then
    begin
      FmEFD_D500_v03_0_9.RGIND_OPER.ItemIndex := 0;
      FmEFD_D500_v03_0_9.RGIND_EMIT.ItemIndex := 1;
      FmEFD_D500_v03_0_9.TPDT_DOC.Date := Date;
      FmEFD_D500_v03_0_9.TPDT_A_P.Date := Date;
    end else
    begin
    end;
    //
    FmEFD_D500_v03_0_9.ShowModal;
    FmEFD_D500_v03_0_9.Destroy;
  end;
*)
var
  Controle: Integer;
begin
  if DBCheck.CriaFm(TFmEfdInnD500Cab, FmEfdInnD500Cab, afmoNegarComAviso) then
  begin
    FmEfdInnD500Cab.FAnoMes := QrEFD_D001AnoMes.Value;
    FmEfdInnD500Cab.ReopenTabelas();
    FmEfdInnD500Cab.ImgTipo.SQLType := SQLType;
    FmEfdInnD500Cab.EdAnoMes.ValueVariant := QrEFD_D001AnoMes.Value;
    FmEfdInnD500Cab.EdEmpresa.ValueVariant := QrEFD_D001Empresa.Value;
    FmEfdInnD500Cab.EdCodigo.ValueVariant := 0; // n�o usa ainda
    //
    if SQLType = stIns then
    begin
      FmEfdInnD500Cab.RGIND_OPER.ItemIndex := 0;
      FmEfdInnD500Cab.RGIND_EMIT.ItemIndex := 1;
      //
      FmEfdInnD500Cab.EdCOD_SIT.Text := '00';
      FmEfdInnD500Cab.TPDT_DOC.Date := Date;
      FmEfdInnD500Cab.TPDT_A_P.Date := Date;
    end else
    begin
      FmEfdInnD500Cab.EdVL_BC_ICMS.Enabled    := True;
      FmEfdInnD500Cab.EdVL_ICMS.Enabled       := True;
      FmEfdInnD500Cab.EdVL_PIS.Enabled        := True;
      FmEfdInnD500Cab.EdVL_COFINS.Enabled     := True;

      FmEfdInnD500Cab.RGIND_OPER.ItemIndex := 0;
      FmEfdInnD500Cab.RGIND_EMIT.ItemIndex := 1;
      //
      FmEfdInnD500Cab.EdControle.ValueVariant      := QrEfdInnD500CabControle.Value;
      //FmEfdInnD500Cab.RGIND_OPER.ItemIndex         := QrEfdInnD500CabIND_OPER.Value;
      //FmEfdInnD500Cab.RGIND_EMIT.ItemIndex         := QrEfdInnD500CabIND_EMIT.Value;
      FmEfdInnD500Cab.EdTerceiro.ValueVariant      := QrEfdInnD500CabTerceiro.Value;
      FmEfdInnD500Cab.CBTerceiro.KeyValue          := QrEfdInnD500CabTerceiro.Value;
      FmEfdInnD500Cab.EdCOD_MOD.Text               := QrEfdInnD500CabCOD_MOD.Value;
      FmEfdInnD500Cab.EdCOD_SIT.Text               := QrEfdInnD500CabCOD_SIT.Value;
      FmEfdInnD500Cab.EdSER.Text                   := QrEfdInnD500CabSER.Value;
      FmEfdInnD500Cab.EdSUB.Text                   := QrEfdInnD500CabSUB.Value;
      FmEfdInnD500Cab.EdNUM_DOC.ValueVariant       := QrEfdInnD500CabNUM_DOC.Value;
      FmEfdInnD500Cab.TPDT_DOC.Date                := QrEfdInnD500CabDT_DOC.Value;
      FmEfdInnD500Cab.TPDT_A_P.Date                := QrEfdInnD500CabDT_A_P.Value;
      FmEfdInnD500Cab.EdVL_DOC.ValueVariant        := QrEfdInnD500CabVL_DOC.Value;
      FmEfdInnD500Cab.EdVL_DESC.ValueVariant       := QrEfdInnD500CabVL_DESC.Value;
      FmEfdInnD500Cab.EdVL_SERV.ValueVariant       := QrEfdInnD500CabVL_SERV.Value;
      FmEfdInnD500Cab.EdVL_SERV_NT.ValueVariant    := QrEfdInnD500CabVL_SERV_NT.Value;
      FmEfdInnD500Cab.EdVL_TERC.ValueVariant       := QrEfdInnD500CabVL_TERC.Value;
      FmEfdInnD500Cab.EdVL_DA.ValueVariant         := QrEfdInnD500CabVL_DA.Value;
      FmEfdInnD500Cab.EdVL_BC_ICMS.ValueVariant    := QrEfdInnD500CabVL_BC_ICMS.Value;
      FmEfdInnD500Cab.EdVL_ICMS.ValueVariant       := QrEfdInnD500CabVL_ICMS.Value;
      FmEfdInnD500Cab.EdVL_PIS.ValueVariant        := QrEfdInnD500CabVL_PIS.Value;
      FmEfdInnD500Cab.EdVL_COFINS.ValueVariant     := QrEfdInnD500CabVL_COFINS.Value;
      //FmEfdInnD500Cab.EdCOD_CTA.ValueVariant         := QrEfdInnD500CabHASH_DOC_REF.Value;
      //FmEfdInnD500Cab.RGTP_ASSINANTE.ItemIndex     := QrEfdInnD500CabTP_ASSINANTE.Value;
      //
    end;
    //
    FmEfdInnD500Cab.ShowModal;
    Controle := FmEfdInnD500Cab.FControle;
    FmEfdInnD500Cab.Destroy;
    //
    if SQLType = stIns then
    begin
      if (Controle <> 0) and (Controle = QrEfdInnD500CabControle.Value) then
        InsUpdEfdInnD500Its(stIns);
    end;
  end;
end;

procedure TFmEfdInnD001Ger.InsUpdEfdInnD500Its(SQLType: TSQLType);
var
  NewConta, Conta, GraGruX, Grandeza: Integer;
  CST_B, IPI_CST, PIS_CST, COFINS_CST: Integer;
  ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq, COFINSRec_pAliq, ICMSSTRec_pAliq: Double;
begin
  if DBCheck.CriaFm(TFmEfdInnD500Its, FmEfdInnD500Its, afmoNegarComAviso) then
  begin
    FmEfdInnD500Its.ImgTipo.SQLType := SQLType;
(*
    FmEfdInnD500Its.FData           := QrEfdInnNFsCabDT_E_S.Value;
    FmEfdInnD500Its.FRegrFiscal     := QrEfdInnNFsCabRegrFiscal.Value;
    //
    FmEfdInnD500Its.FQrCab                         := QrEfdInnNFsCab;
    FmEfdInnD500Its.FQrIts                         := QrEfdInnNFsIts;
    FmEfdInnD500Its.EdEmpresa.ValueVariant         := QrPQEEmpresa.Value;
    FmEfdInnD500Its.EdMovFatID.ValueVariant        := FEFDInnNFSMainFatID;
    FmEfdInnD500Its.EdMovFatNum.ValueVariant       := QrPQECodigo.Value;
    FmEfdInnD500Its.EdMovimCod.ValueVariant        := CO_MovimCod_ZERO; // N�o tem! QrPQEMovimCod.Value;
    FmEfdInnD500Its.EdMovimNiv.ValueVariant        := 0;
    FmEfdInnD500Its.EdMovimTwn.ValueVariant        := 0;
    FmEfdInnD500Its.EdControle.ValueVariant        := QrEfdInnNFsCabControle.Value;
    //FmEfdInnD500Its.FQrIts                         := QrVSInnNFs;
*)
    FmEfdInnD500Its.EdEmpresa.ValueVariant        := QrEfdInnD500CabEmpresa.Value;
    FmEfdInnD500Its.EdAnoMes.ValueVariant         := QrEfdInnD500CabAnoMes.Value;
    FmEfdInnD500Its.EdCodigo.ValueVariant         := QrEfdInnD500CabCodigo.Value;
    FmEfdInnD500Its.EdControle.ValueVariant       := QrEfdInnD500CabControle.Value;
    FmEfdInnD500Its.FData                         := QrEfdInnD500CabDT_A_P.Value;
    //
    if SQLType = stIns then
    begin

    end else
    begin
      FmEfdInnD500Its.EdControle.ValueVariant                      := QrEfdInnD500ItsControle.Value;
      FmEfdInnD500Its.EdConta.ValueVariant                         := QrEfdInnD500ItsConta.Value;

      FmEfdInnD500Its.EdGraGruX.ValueVariant                       := QrEfdInnD500ItsGraGruX.Value;
      FmEfdInnD500Its.CBGraGruX.KeyValue                           := QrEfdInnD500ItsGraGruX.Value;
      FmEfdInnD500Its.EdCFOP.Text                                  := Geral.FormataCFOP(Geral.FF0(QrEfdInnD500ItsCFOP.Value));
      FmEfdInnD500Its.EdQTD.ValueVariant                           := QrEfdInnD500ItsQTD.Value;
      FmEfdInnD500Its.EdVL_ITEM.ValueVariant                       := QrEfdInnD500ItsVL_ITEM.Value;
      FmEfdInnD500Its.EdVL_DESC.ValueVariant                       := QrEfdInnD500ItsVL_DESC.Value;

      FmEfdInnD500Its.EdCST_ICMS.ValueVariant                      := QrEfdInnD500ItsCST_ICMS.Value;
      FmEfdInnD500Its.EdVL_BC_ICMS.ValueVariant                    := QrEfdInnD500ItsVL_BC_ICMS.Value;
      FmEfdInnD500Its.EdALIQ_ICMS.ValueVariant                     := QrEfdInnD500ItsALIQ_ICMS.Value;
      FmEfdInnD500Its.EdVL_ICMS.ValueVariant                       := QrEfdInnD500ItsVL_ICMS.Value;
      FmEfdInnD500Its.EdVL_BC_ICMS_ST.ValueVariant                 := QrEfdInnD500ItsVL_BC_ICMS_ST.Value;
      FmEfdInnD500Its.EdALIQ_ST.ValueVariant                       := QrEfdInnD500ItsALIQ_ST.Value;
      FmEfdInnD500Its.EdVL_ICMS_ST.ValueVariant                    := QrEfdInnD500ItsVL_ICMS_ST.Value;

      FmEfdInnD500Its.EdCST_PIS.ValueVariant                       := QrEfdInnD500ItsCST_PIS.Value;
      FmEfdInnD500Its.EdVL_BC_PIS .ValueVariant                    := QrEfdInnD500ItsVL_BC_PIS.Value;
      FmEfdInnD500Its.EdALIQ_PIS.ValueVariant                      := QrEfdInnD500ItsALIQ_PIS.Value;
      FmEfdInnD500Its.EdVL_PIS.ValueVariant                        := QrEfdInnD500ItsVL_PIS.Value;
      FmEfdInnD500Its.EdCST_COFINS.ValueVariant                    := QrEfdInnD500ItsCST_COFINS.Value;
      FmEfdInnD500Its.EdVL_BC_COFINS.ValueVariant                  := QrEfdInnD500ItsVL_BC_COFINS.Value;
      FmEfdInnD500Its.EdALIQ_COFINS.ValueVariant                   := QrEfdInnD500ItsALIQ_COFINS.Value;
      FmEfdInnD500Its.EdVL_COFINS.ValueVariant                     := QrEfdInnD500ItsVL_COFINS.Value;
      //FmEfdInnD500Its.EdCOD_CTA.ValueVariant                       := QrEfdInnD500ItsCOD_CTA.Value;
      //FmEfdInnD500Its.EdVL_ABAT_NT.ValueVariant                    := QrEfdInnD500ItsVL_ABAT_NT.Value;
      //
      FmEfdInnD500Its.EdVL_Outro.ValueVariant                      := QrEfdInnD500ItsVL_Outro.Value;
      FmEfdInnD500Its.EdVL_OPR.ValueVariant                        := QrEfdInnD500ItsVL_OPR.Value;
      FmEfdInnD500Its.EdVL_RED_BC.ValueVariant                     := QrEfdInnD500ItsVL_RED_BC.Value;
      FmEfdInnD500Its.EdVL_BC_ICMS_UF.ValueVariant                 := QrEfdInnD500ItsVL_BC_ICMS_UF.Value;
      FmEfdInnD500Its.EdVL_ICMS_UF.ValueVariant                    := QrEfdInnD500ItsVL_ICMS_UF.Value;
(*
      FmEfdInnD500Its.EdAjusteVL_BC_ICMS.ValueVariant              := QrEfdInnD500ItsAjusteVL_BC_ICMS.Value;
      FmEfdInnD500Its.EdAjusteALIQ_ICMS.ValueVariant               := QrEfdInnD500ItsAjusteALIQ_ICMS.Value;
      FmEfdInnD500Its.EdAjusteVL_ICMS.ValueVariant                 := QrEfdInnD500ItsAjusteVL_ICMS.Value;
      FmEfdInnD500Its.EdAjusteVL_OUTROS.ValueVariant               := QrEfdInnD500ItsAjusteVL_OUTROS.Value;
*)
      FmEfdInnD500Its.EdNAT_BC_CRED.ValueVariant                   := QrEfdInnD500ItsNAT_BC_CRED.Value;
    end;
    FmEfdInnD500Its.ShowModal;
    NewConta := FmEfdInnD500Its.FNewConta;
    FmEfdInnD500Its.Destroy;
        //
    ReopenEfdInnD500Its(NewConta);
  end;
end;

function TFmEfdInnD001Ger.PeriodoJaExiste(ImporExpor, AnoMes, Empresa: Integer;
Avisa: Boolean): Boolean;
var
  Qry1: TmySQLQuery;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  Qry1.Close;
  Qry1.Database := Dmod.MyDB;
  UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
  'SELECT COUNT(*) Registros ',
  'FROM efdicmsipid001 ',
  'WHERE ImporExpor=' + FormatFloat('0', ImporExpor),
  'AND AnoMes=' + FormatFloat('0', AnoMes),
  'AND Empresa=' + FormatFloat('0', Empresa),
  '']);
  Result := Qry1.FieldByName('Registros').AsInteger > 0;
  if Result and Avisa then
  begin
    Geral.MensagemBox('O periodo selecionado j� existe!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmEfdInnD001Ger.PMPeriodoPopup(Sender: TObject);
begin
  Excluiperiodoselecionado1.Enabled :=
    (QrEFD_D001.State <> dsInactive) and
    (QrEFD_D001.RecordCount > 0)
  and
    (QrEfdInnD500Cab.State <> dsInactive) and
    (QrEfdInnD500Cab.RecordCount = 0);
end;

procedure TFmEfdInnD001Ger.PMD500Popup(Sender: TObject);
(*
var
  Habilita: Boolean;
begin
  Habilita := (QrEFD_D001.State <> dsInactive) and (QrEFD_D001.RecordCount > 0);
  IncluiD500.Enabled := Habilita;
  //
  Habilita := Habilita and
    (QrEFD_D500.State <> dsInactive) and (QrEFD_D500.RecordCount > 0);
  AlteraD500.Enabled := Habilita;
  ExcluiD500.Enabled := Habilita and
    (QrEFD_D500.State <> dsInactive) and (QrEFD_D500.RecordCount > 0);
*)
begin
  MyObjects.HabilitaMenuItemCabUpd(AlteraD500, QrEfdInnD500Cab);
  MyObjects.HabilitaMenuItemCabDel(ExcluiD500, QrEfdInnD500Cab, QrEfdInnD500Its);
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiitemNFD5001, QrEfdInnD500Cab);
  MyObjects.HabilitaMenuItemItsDel(AlteraitemNFD500selecionada1, QrEfdInnD500Its);
  MyObjects.HabilitaMenuItemItsUpd(ExcluiitemNFD500selecionada1, QrEfdInnD500Its);
end;

procedure TFmEfdInnD001Ger.RGEmitenteClick(Sender: TObject);
begin
  ReopenEfdInnD500Cab(0);
end;

procedure TFmEfdInnD001Ger.ReopenEFD_D001(AnoMes: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_D001, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT, ',
  'CONCAT(RIGHT(e001.AnoMes, 2), "/", ',
  'LEFT(LPAD(e001.AnoMes, 6, "0"), 4)) MES_ANO, e001.* ',
  'FROM efdicmsipid001 e001 ',
  'LEFT JOIN entidades ent ON ent.Codigo=e001.Empresa ',
  'WHERE e001.ImporExpor=' + FormatFloat('0', FImporExpor),
  'AND e001.Empresa=' + FormatFloat('0', FEmpresa),
  'ORDER BY e001.AnoMes DESC ',
  '']);
  //
  QrEFD_D001.Locate('AnoMes', AnoMes, []);
end;

procedure TFmEfdInnD001Ger.ReopenEFD_D100(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_D100, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)  ',
  'NO_TERC, d100.* ',
  'FROM efdicmsipid100 d100  ',
  'LEFT JOIN entidades ent ON ent.Codigo=d100.Terceiro ',
  'WHERE d100.ImporExpor=' + FormatFloat('0', QrEFD_D001ImporExpor.Value),
  'AND d100.Empresa=' + FormatFloat('0', QrEFD_D001Empresa.Value),
  'AND d100.AnoMes=' + FormatFloat('0', QrEFD_D001AnoMes.Value),
  //'ORDER BY d100.?',
  '']);
  //
  QrEFD_D100.Locate('LinArq', LinArq, []);
end;

procedure TFmEfdInnD001Ger.ReopenEfdInnD500Cab(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnd500Cab, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)  ',
  'NO_TERC, d500.* ',
  'FROM efdinnd500cab d500  ',
  'LEFT JOIN entidades ent ON ent.Codigo=d500.Terceiro ',
  'WHERE d500.Empresa=' + FormatFloat('0', QrEFD_D001Empresa.Value),
  'AND d500.AnoMes=' + FormatFloat('0', QrEFD_D001AnoMes.Value),
  'ORDER BY DT_A_P DESC, Controle DESC ',
  '']);
  //
  QrEfdInnD500Cab.Locate('Controle', Controle, []);
end;

procedure TFmEfdInnD001Ger.ReopenEfdInnd500Its(Conta: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrEfdInnD500Its, Dmod.MyDB, [
  'SELECT vin.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM efdInnd500its     vin',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vin.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE vin.Controle=' + Geral.FF0(QrEfdInnD500CabControle.Value),
  '']);
  //
  QrEfdInnD500Its.Locate('Conta', Conta, []);
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEfdInnD001Ger.AlteraitemNFD500selecionada1Click(Sender: TObject);
begin
  InsUpdEfdInnD500Its(stUpd);
end;

procedure TFmEfdInnD001Ger.AlteraNFD100selecionada1Click(Sender: TObject);
begin
  InsUpdD100(stUpd);
end;

procedure TFmEfdInnD001Ger.AtualizaTotaisD500(Controle: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(VL_BC_ICMS) VL_BC_ICMS, ',
  'SUM(VL_ICMS) VL_ICMS, ',
  'SUM(VL_BC_ICMS_UF) VL_BC_ICMS_UF, ',
  'SUM(VL_ICMS_UF) VL_ICMS_UF, ',
  'SUM(VL_RED_BC) VL_RED_BC,',
  'SUM(VL_PIS) VL_PIS,',
  'SUM(VL_COFINS) VL_COFINS',
  'FROM efdinnd500its',
  'WHERE Controle=' + Geral.FF0(QrEfdInnD500CabControle.Value),
  '']);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'efdinnd500cab', False, [
  'VL_BC_ICMS', 'VL_ICMS',
  //'VL_BC_ICMS_UF', 'VL_ICMS_UF',
  'VL_PIS', 'VL_COFINS'], [
  'Controle'], [
  QrSumVL_BC_ICMS.Value, QrSumVL_ICMS.Value,
  //QrSumVL_BC_ICMS_UF.Value, QrSumVL_ICMS_UF.Value,
  QrSumVL_PIS.Value, QrSumVL_COFINS.Value], [
  Controle], True) then
  begin
    //
  end;
end;

procedure TFmEfdInnD001Ger.BtD100Click(Sender: TObject);
begin
  PCServicos.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMD100, BtD100);
end;

procedure TFmEfdInnD001Ger.BtD500Click(Sender: TObject);
begin
  PCServicos.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMD500, BtD500);
end;

procedure TFmEfdInnD001Ger.BtSaida0Click(Sender: TObject);
begin
  //VAR_CADASTRO := Qr?.Value;
  Close;
end;

procedure TFmEfdInnD001Ger.AlteraD500Click(Sender: TObject);
begin
  InsUpdEfdInnD500Cab(stUpd);
end;

procedure TFmEfdInnD001Ger.BtPeriodoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPeriodo, BtPeriodo);
end;

procedure TFmEfdInnD001Ger.FormCreate(Sender: TObject);
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FEmprTXT := '';
  PCPeriodos.Align := alClient;
  PCPeriodos.ActivePageIndex := 0;
  PCServicos.ActivePageIndex := 1;
  ImgTipo.SQLType := stLok;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmEfdInnD001Ger.SbNumeroClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Codigo(QrCadComItensCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEfdInnD001Ger.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEfdInnD001Ger.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrCadComItensCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmEfdInnD001Ger.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEfdInnD001Ger.QrEfdInnD500CabAfterScroll(DataSet: TDataSet);
begin
  ReopenEfdInnD500Its(0);
end;

procedure TFmEfdInnD001Ger.QrEfdInnD500CabBeforeClose(DataSet: TDataSet);
begin
  QrEfdInnD500Its.Close;
end;

procedure TFmEfdInnD001Ger.QrEFD_D001AfterOpen(DataSet: TDataSet);
begin
  BtD100.Enabled := QrEFD_D001.RecordCount > 0;
  BtD500.Enabled := QrEFD_D001.RecordCount > 0;
end;

procedure TFmEfdInnD001Ger.QrEFD_D001AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_D100(0);
  ReopenEfdInnD500Cab(0);
end;

procedure TFmEfdInnD001Ger.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdInnD001Ger.SbQueryClick(Sender: TObject);
begin
{
  LocCod(QrCadComItensCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cadcomitens', Dmod.MyDB, CO_VAZIO));
}
end;

procedure TFmEfdInnD001Ger.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdInnD001Ger.QrEFD_D001BeforeClose(DataSet: TDataSet);
begin
  BtD500.Enabled := False;
  //
  QrEFD_D100.Close;
  QrEfdInnD500Cab.Close;
end;

end.

