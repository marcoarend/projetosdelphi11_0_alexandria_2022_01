object FmEfdInnCTsGer: TFmEfdInnCTsGer
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-037 :: Pesquisa de Ficha RMP'
  ClientHeight = 676
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 292
        Height = 32
        Caption = 'Pesquisa de Ficha RMP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 292
        Height = 32
        Caption = 'Pesquisa de Ficha RMP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 292
        Height = 32
        Caption = 'Pesquisa de Ficha RMP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 514
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 514
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 514
        Align = alClient
        TabOrder = 0
        object DGDados: TDBGrid
          Left = 2
          Top = 97
          Width = 1004
          Height = 415
          Align = alClient
          DataSource = DsEfdInnCTsCab
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DGDadosDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SER'
              Title.Caption = 'S'#233'rie'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NUM_DOC'
              Title.Caption = 'N'#176' CT-e'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Terceiro'
              Title.Caption = 'Transportadora'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TRANSPORTA'
              Title.Caption = 'Nome da transportadora'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CHV_CTE'
              Title.Caption = 'Chave CT-e'
              Width = 290
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DT_A_P'
              Title.Caption = 'Data Aquis.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DT_DOC'
              Title.Caption = 'Data emiss.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorT'
              Title.Caption = 'Valor Total'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFe_FatID'
              Title.Caption = 'Fat. ID'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFe_FatNum'
              Title.Caption = 'Fat.NUm'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimCod'
              Title.Caption = 'Movim.Cod.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Motorista'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUB'
              Title.Caption = 'Sub S'#233'rie'
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 82
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 1004
            Height = 82
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 242
            ExplicitHeight = 808
            object Label2: TLabel
              Left = 8
              Top = 0
              Width = 69
              Height = 13
              Caption = 'Transportador:'
            end
            object Label22: TLabel
              Left = 476
              Top = 0
              Width = 135
              Height = 13
              Caption = 'Motorista: (entrada in natura)'
            end
            object Label10: TLabel
              Left = 240
              Top = 40
              Width = 63
              Height = 13
              Caption = 'N'#250'mero NFe:'
            end
            object Label13: TLabel
              Left = 8
              Top = 40
              Width = 55
              Height = 13
              Caption = 'Data inicial:'
            end
            object Label14: TLabel
              Left = 124
              Top = 40
              Width = 48
              Height = 13
              Caption = 'Data final:'
            end
            object BtReabre: TBitBtn
              Tag = 18
              Left = 871
              Top = 5
              Width = 120
              Height = 40
              Caption = '&Reabre'
              NumGlyphs = 2
              TabOrder = 6
              OnClick = BtReabreClick
            end
            object EdFornecedor: TdmkEditCB
              Left = 8
              Top = 16
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Fornecedor'
              UpdCampo = 'Fornecedor'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBFornecedor
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBFornecedor: TdmkDBLookupComboBox
              Left = 64
              Top = 16
              Width = 408
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsFornecedor
              TabOrder = 1
              dmkEditCB = EdFornecedor
              QryCampo = 'Fornecedor'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdMotorista: TdmkEditCB
              Left = 476
              Top = 17
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Motorista'
              UpdCampo = 'Motorista'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBMotorista
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBMotorista: TdmkDBLookupComboBox
              Left = 532
              Top = 17
              Width = 265
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsMotorista
              TabOrder = 3
              dmkEditCB = EdMotorista
              QryCampo = 'Motorista'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdNFeNum: TdmkEdit
              Left = 240
              Top = 56
              Width = 92
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Pecas'
              UpdCampo = 'Pecas'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object TPIni: TdmkEditDateTimePicker
              Left = 8
              Top = 56
              Width = 112
              Height = 21
              Date = 44661.000000000000000000
              Time = 0.317081238426908400
              TabOrder = 4
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object TPFim: TdmkEditDateTimePicker
              Left = 124
              Top = 56
              Width = 112
              Height = 21
              Date = 44661.000000000000000000
              Time = 0.317081238426908400
              TabOrder = 5
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 562
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 606
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 22
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Localiza'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEfdInnCTsCab: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEfdInnCTsCabAfterOpen
    BeforeClose = QrEfdInnCTsCabBeforeClose
    SQL.Strings = (
      'SELECT eicc.*, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TRANSPORTA'
      'FROM efdinnctscab eicc'
      'LEFT JOIN entidades ent ON ent.Codigo=eicc.Terceiro'
      'WHERE eicc.DT_A_P > "1900-01-01"'
      'AND eicc.Terceiro>0'
      'AND eicc.Motorista=0')
    Left = 72
    Top = 196
    object QrEfdInnCTsCabMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnCTsCabMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnCTsCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnCTsCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnCTsCabControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnCTsCabIsLinked: TSmallintField
      FieldName = 'IsLinked'
      Required = True
    end
    object QrEfdInnCTsCabSqLinked: TIntegerField
      FieldName = 'SqLinked'
      Required = True
    end
    object QrEfdInnCTsCabCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrEfdInnCTsCabTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrEfdInnCTsCabPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrEfdInnCTsCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrEfdInnCTsCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrEfdInnCTsCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrEfdInnCTsCabValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnCTsCabMotorista: TIntegerField
      FieldName = 'Motorista'
      Required = True
    end
    object QrEfdInnCTsCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrEfdInnCTsCabIND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabIND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabCOD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Required = True
      Size = 2
    end
    object QrEfdInnCTsCabCOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
      Required = True
    end
    object QrEfdInnCTsCabSER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEfdInnCTsCabSUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEfdInnCTsCabNUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Required = True
    end
    object QrEfdInnCTsCabCHV_CTE: TWideStringField
      FieldName = 'CHV_CTE'
      Size = 44
    end
    object QrEfdInnCTsCabCTeStatus: TIntegerField
      FieldName = 'CTeStatus'
      Required = True
    end
    object QrEfdInnCTsCabDT_DOC: TDateField
      FieldName = 'DT_DOC'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEfdInnCTsCabDT_A_P: TDateField
      FieldName = 'DT_A_P'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEfdInnCTsCabTP_CT_e: TSmallintField
      FieldName = 'TP_CT_e'
      Required = True
    end
    object QrEfdInnCTsCabCHV_CTE_REF: TWideStringField
      FieldName = 'CHV_CTE_REF'
      Size = 44
    end
    object QrEfdInnCTsCabVL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Required = True
    end
    object QrEfdInnCTsCabVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
    end
    object QrEfdInnCTsCabIND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEfdInnCTsCabVL_SERV: TFloatField
      FieldName = 'VL_SERV'
      Required = True
    end
    object QrEfdInnCTsCabVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_NT: TFloatField
      FieldName = 'VL_NT'
      Required = True
    end
    object QrEfdInnCTsCabCOD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrEfdInnCTsCabCOD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 30
    end
    object QrEfdInnCTsCabCOD_MUN_ORIG: TIntegerField
      FieldName = 'COD_MUN_ORIG'
      Required = True
    end
    object QrEfdInnCTsCabCOD_MUN_DEST: TIntegerField
      FieldName = 'COD_MUN_DEST'
      Required = True
    end
    object QrEfdInnCTsCabNFe_FatID: TIntegerField
      FieldName = 'NFe_FatID'
      Required = True
    end
    object QrEfdInnCTsCabNFe_FatNum: TIntegerField
      FieldName = 'NFe_FatNum'
      Required = True
    end
    object QrEfdInnCTsCabNFe_StaLnk: TSmallintField
      FieldName = 'NFe_StaLnk'
      Required = True
    end
    object QrEfdInnCTsCabVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
      Required = True
    end
    object QrEfdInnCTsCabVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrEfdInnCTsCabVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrEfdInnCTsCabVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
      Required = True
    end
    object QrEfdInnCTsCabCST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabCFOP: TIntegerField
      FieldName = 'CFOP'
      Required = True
    end
    object QrEfdInnCTsCabALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_OPR: TFloatField
      FieldName = 'VL_OPR'
      Required = True
    end
    object QrEfdInnCTsCabVL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
      Required = True
    end
    object QrEfdInnCTsCabCOD_OBS: TWideStringField
      FieldName = 'COD_OBS'
      Size = 6
    end
    object QrEfdInnCTsCabRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Required = True
    end
    object QrEfdInnCTsCabIND_NAT_FRT: TWideStringField
      FieldName = 'IND_NAT_FRT'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabVL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      Required = True
    end
    object QrEfdInnCTsCabCST_PIS: TWideStringField
      FieldName = 'CST_PIS'
      Size = 2
    end
    object QrEfdInnCTsCabNAT_BC_CRED: TWideStringField
      FieldName = 'NAT_BC_CRED'
      Required = True
      Size = 2
    end
    object QrEfdInnCTsCabVL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
      Required = True
    end
    object QrEfdInnCTsCabALIQ_PIS: TFloatField
      FieldName = 'ALIQ_PIS'
      Required = True
    end
    object QrEfdInnCTsCabVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
    end
    object QrEfdInnCTsCabCST_COFINS: TWideStringField
      FieldName = 'CST_COFINS'
      Size = 2
    end
    object QrEfdInnCTsCabVL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
      Required = True
    end
    object QrEfdInnCTsCabALIQ_COFINS: TFloatField
      FieldName = 'ALIQ_COFINS'
      Required = True
    end
    object QrEfdInnCTsCabVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
    end
    object QrEfdInnCTsCabNO_TRANSPORTA: TWideStringField
      FieldName = 'NO_TRANSPORTA'
      Size = 100
    end
  end
  object DsEfdInnCTsCab: TDataSource
    DataSet = QrEfdInnCTsCab
    Left = 72
    Top = 244
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 148
    Top = 196
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 148
    Top = 244
  end
  object QrMotorista: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 224
    Top = 196
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsMotorista: TDataSource
    DataSet = QrMotorista
    Left = 224
    Top = 244
  end
end
