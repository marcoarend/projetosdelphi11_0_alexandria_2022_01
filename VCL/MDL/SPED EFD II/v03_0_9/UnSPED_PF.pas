unit UnSPED_PF;
interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, UnMsgInt, Db,
  DbCtrls, Buttons, ZCF2, mySQLDbTables, ComCtrls, Grids, DBGrids, CommCtrl,
  Consts, UnDmkProcFunc, Variants, MaskUtils, frxClass, frxPreview,
  mySQLExceptions,
  // Dermatek
  dmkGeral, dmkEdit, dmkDBEdit, dmkEditF7, dmkEditDateTimePicker, dmkCheckGroup,
  dmkDBLookupCombobox, dmkEditCB, dmkDBGrid, dmkDBGridDAC, dmkImage, UnDmkEnums;

type
  TUnSPED_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AtualizaTabelasSPEDEFD();
    //
    procedure CriaNovoPeriodoSPED_MPUsoCons(Empresa, FImporExpor, FAnoMes: Integer; DataIni,
              DataFim: TDateTime;
              LaAviso1, LaAviso2: TLabel; PB1: TProgressBar);
    function  ExcluiPeriodoSelecionado(Empresa, FAnoMes, FImporExpor: Integer): Boolean;
    function  ValidaPeriodoInteiroDeUmMes(const DtI, DtF: TDateTime): Boolean;
    //
    function  InsAltEfdInnNFsCab(SQLType: TSQLType; MovFatID, MovFatNum,
              MovimCod, Empresa, _Controle, IsLinked, SqLinked, Terceiro, CliInt,
              TpEntrd, RegrFiscal: Integer; Pecas,
              PesoKg, AreaM2, AreaP2, ValorT: Double; Motorista: Integer; Placa: String;
              COD_MOD, COD_SIT, SER, NUM_DOC: Integer; CHV_NFE: String; NFeStatus: Integer;
              DT_DOC, DT_E_S: String; VL_DOC: Double; IND_PGTO: String; VL_DESC, VL_ABAT_NT,
              VL_MERC: Double; IND_FRT: String; VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
              VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
              VL_COFINS_ST: Double; NFe_FatID, NFe_FatNum, NFe_StaLnk: Integer;
              VSVmcWrn: Integer; VSVmcObs, VSVmcSeq: String; VSVmcSta: Integer;
              LnkTabName, LnkFldName: String): Boolean;
              //
    function  InsAltEfdInnNFsIts(SQLType: TSQLType; MovFatID, MovFatNum,
  MovimCod, Empresa, MovimNiv, MovimTwn, Controle, Conta, SqLinked, GraGru1,
  GraGruX: Integer; prod_vProd, prod_vFrete, prod_vSeg, prod_vOutro,
  QTD: Double; UNID: String; VL_ITEM, VL_DESC: Double; IND_MOV, CST_ICMS: String;
  CFOP: Integer; COD_NAT: String; VL_BC_ICMS, ALIQ_ICMS, VL_ICMS,
  VL_BC_ICMS_ST, ALIQ_ST, VL_ICMS_ST: Double; IND_APUR, CST_IPI:
  String; COD_ENQ: String; VL_BC_IPI, ALIQ_IPI, VL_IPI: Double; CST_PIS:
  String; VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS, ALIQ_PIS_r, VL_PIS: Double; CST_COFINS:
  String; VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS, ALIQ_COFINS_r,
  VL_COFINS: Double; COD_CTA: String; VL_ABAT_NT: Double; DtCorrApo, xLote:
  String; Ori_IPIpIPI, Ori_IPIvIPI: Double; LnkTabName, LnkFldName: String;
  SqLnkID, EFD_II_C195: Integer; AjusteVL_BC_ICMS, AjusteALIQ_ICMS,
  AjusteVL_ICMS, AjusteVL_OUTROS, AjusteVL_OPR, AjusteVL_RED_BC: Double): Boolean;
    function  ObtemCST_B_de_CST_ICMS(CST: String): String;
    function  MontaCST_ICMS_Inn_de_CST_ICMS_emi(ICMS_Orig: Integer; CST_emi:
              String): String;
    function  FormulaVL_OPR(prod_vProd, prod_vFrete, prod_vSeg, prod_vOutro,
              prod_vDesc, prod_vIPI: Double): Double;
    function  FormulaVL_OUTROS(_VL_OPR, ICMS_vBC, ICMS_vICMSST, _VL_RED_BC,
              IPI_vIPI: Double): Double;
    function  FormulaVL_RED_BC(VL_OPR, VL_BC, pRedBC: Double): Double;
    procedure ReopenNFeItens_EFD(Qry: TmySQLQuery; FatID, FatNum, Empresa:
              Integer);
    function  NFeJaAtrelada(NFe_FatID, NFe_FatNum, Empresa: Integer): Boolean;
    function  ImpedePeloID_da_NFe(Tabela, CampoId_NFe, Id_NFe, SQLIdx: String): Boolean;
    function  FaltaInformarCST(EdCST_ICMS, EdCST_IPI, EdCST_PIS, EdCST_COFINS:
              TdmkEdit): Boolean;
  end;

var
  SPED_PF: TUnSPED_PF;

implementation

uses MyDBCheck, Module, ModuleGeral, ModuleNFe_0000, DmkDAC_PF, UMySQLModule,
  UnMyObjects, ModuleFin, UnFinanceiro, SPED_EFD_II_Tabs, SPED_EFD_DownTabs,
  ModuleSPED;


{ TUnSPED_PF }

const
 CO_Tabela_0000 = 'spedefdicmsipi0000';
 CO_Tabela_C001 = 'spedefdicmsipiC001';

procedure TUnSPED_PF.AtualizaTabelasSPEDEFD();
begin
  if Geral.MB_Pergunta(
  'Deseja verificar se h� atualiza��o das tabelas de consulta do SPED EFD?') = ID_YES then
  begin
    if DBCheck.CriaFm(TFmSPED_EFD_DownTabs, FmSPED_EFD_DownTabs, afmoNegarComAviso) then
    begin
      FmSPED_EFD_DownTabs.ShowModal;
      FmSPED_EFD_DownTabs.Destroy;
    end;
  end;
end;

procedure TUnSPED_PF.CriaNovoPeriodoSPED_MPUsoCons(Empresa, FImporExpor, FAnoMes: Integer; DataIni,
  DataFim: TDateTime;
  LaAviso1, LaAviso2: TLabel; PB1: TProgressBar);
const
  sProcName = 'TUnSPED_PF.CriaNovoPeriodoSPED_MPUsoCons()';
var
(*
  I, J, LinArq, Distancia, NivelExclu: Integer;
  Lista: TStringList;
  Levenshtein_Meu, Levenshtein_Inn: String;
*)
  Ano, Mes: Integer;
  //
  // 0000
  REG, NOME, CNPJ, CPF, UF, IE, IM, SUFRAMA, IND_PERFIL: String;
  COD_VER, COD_FIN, COD_MUN, IND_ATIV: Integer;
  DT_INI, DT_FIN: String; //TDateTime;
(*
  //Empresa,
  Entidade, UnidMed, GraGruX: Integer;
  //  001
  CEP: Integer;
  FANTASIA, _END, NUM, COMPL, BAIRRO, FONE, FAX, EMAIL: String;
  //  005
  // 100
  CRC: String;
  // 0150
  COD_PART: String;
  COD_PAIS: Integer;
  // 0190
  UNID, DESCR: String;
  // 0200
  COD_ITEM, DESCR_ITEM, COD_BARRA, COD_ANT_ITEM, UNID_INV, TIPO_ITEM, COD_NCM,
  EX_IPI, COD_GEN, COD_LST: String;
  ALIQ_ICMS: Double;
  // 400
  COD_NAT, DESCR_NAT: String;
  // 990
  QTD_LIN_0: Integer;
*)
  // C001
  IND_MOV: String;
  // C100
(*
  IND_OPER, IND_EMIT, COD_MOD, SER, CHV_NFE, IND_PGTO, IND_FRT, COD_SIT: String;
  NUM_DOC: Integer;
  DT_DOC, DT_E_S: String; //TDateTime;
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST: Double;
  // C170
  DESCR_COMPL, IND_APUR, CST_IPI, COD_ENQ, COD_CTA: String;
  C100, NUM_ITEM, CST_ICMS, CFOP: Integer;
  QTD, VL_ITEM, ALIQ_ST, VL_BC_IPI, ALIQ_IPI, CST_PIS, VL_BC_PIS, ALIQ_PIS_p,
  QUANT_BC_PIS, ALIQ_PIS_r, CST_COFINS, VL_BC_COFINS, ALIQ_COFINS_p,
  QUANT_BC_COFINS, ALIQ_COFINS_r: Double;
  // C190
  VL_OPR, VL_RED_BC: Double;
  COD_OBS: String;
  // C500
  C500: Integer;
  SUB, TP_LIGACAO, COD_CONS, COD_INF, COD_GRUPO_TENSAO: String;
  VL_FORN, VL_SERV_NT, VL_TERC, VL_DA: Double;
  // C990
  QTD_LIN_C: Integer;
  // D100
  SUB_C, CHV_CTE, DT_A_P, CHV_CTE_REF: String;
  D100, TP_CTE: Integer;
  VL_SERV, VL_NT: Double;
  // D190
  // D500
  D500, TP_ASSINANTE: Integer;
  // D990
  QTD_LIN_D: Integer;
  //
  // E100
  E100: Integer;
  // E110
  E110: Integer;
  VL_TOT_DEBITOS, VL_AJ_DEBITOS, VL_TOT_AJ_DEBITOS, VL_ESTORNOS_CRED,
  VL_TOT_CREDITOS, VL_AJ_CREDITOS, VL_TOT_AJ_CREDITOS, VL_ESTORNOS_DEB,
  VL_SLD_CREDOR_ANT, VL_SLD_APURADO, VL_TOT_DED, VL_ICMS_RECOLHER,
  VL_SLD_CREDOR_TRANSPORTAR, DEB_ESP: Double;
  // E111
  COD_AJ_APUR, DESCR_COMPL_AJ: String;
  VL_AJ_APUR: Double;
  // E116
  COD_OR, DT_VCTO, COD_REC, NUM_PROC, IND_PROC, PROC, MES_REF: String;
  VL_OR: Double;
  // E990
  QTD_LIN_E: Integer;
  // G990
  QTD_LIN_G: Integer;
  // H005
  DT_INV: String;
  VL_INV: Double;
  // H010
  VL_UNIT: Double;
  IND_PROP, TXT_COMPL: String;
  // H990
  QTD_LIN_H: Integer;
  // 1990
  QTD_LIN_1: Integer;
  // 9900
  REG_BLC: String;
  QTD_REG_BLC: Integer;
  // 9990
  QTD_LIN_9: Integer;
  // 9999
  QTD_LIN: Integer;
  //
  //DataIni, DataFim: TDateTime;
  MySQLQuery: TmySQLQuery;
  NomeTab: String;
*)
  LinArq: Integer;
begin
(*
  BtCarrega.Enabled := False;
  if RGConfig.ItemIndex > 1 then
  begin
    Geral.MensagemBox(
    'Configura��o de carregamento e importa��o n�o implementado:' + sLineBreak +
    RGConfig.Items[RGConfig.ItemIndex] + sLineBreak + sLineBreak + 'Solicite � DERMATEK',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
*)
  try
    Screen.Cursor := crHourGlass;
(*
  FCarregou := False;
  Lista := TStringList.Create;
  try
    MeIgnor.Lines.Clear;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas tempor�rias');
    GradeCriar.RecriaTempTableNovo(ntrttSPEDINN_0200, DModG.QrUpdPID1, False);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando carregamento');
    Geral.MyExtractStrings(['|'], [' '], PChar(MeImportado.Lines[0]), Lista);
    COD_VER := Geral.IMV(Lista[01]);
    if MyObjects.FIC(not (COD_VER in ([3,4])), nil,
      'Vers�o de arquivo n�o implementada!' + sLineBreak +
      'Solicite implementa��o � DERMATEK!') then
      Exit;
    DataIni := Geral.ValidaDataBR(Lista[03], True, False);
    DataFim := Geral.ValidaDataBR(Lista[04], True, False);
*)
    //
    COD_VER := Geral.IMV(DModG.QrVersaoEFD_IICodTxt.Value);
    if not ValidaPeriodoInteiroDeUmMes(DataIni, DataFim) then
      Exit;
    if MyObjects.FIC(Empresa = 0, nil, 'Empresa n�o definida!') then
      Exit;
    DmNFe_0000.ReopenEmit(Empresa);
    CNPJ := Geral.SoNumero_TT(DmNFe_0000.QrEmitCNPJ.Value);
(*
    if not DefineEntidadeCarregada(CNPJ, True) then
      Exit;
    if MyObjects.FIC(EdEmp_Codigo.ValueVariant = 0, EdEmp_Codigo, 'Empresa n�o definida!') then
      Exit;
    Empresa := EdEmp_Codigo.ValueVariant;
    //
*)
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo dados duplic�veis');
    if not ExcluiPeriodoSelecionado(Empresa, FAnoMes, FImporExpor) then
      Exit;
    //
    PB1.Position := 0;
    PB1.Max := 100;
(*
    PB1.Max := MeImportado.Lines.Count;
    for I := 0 to MeImportado.Lines.Count - 1 do
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando linha n� ' +
      FormatFloat('0000', I) + ' = ' + MeImportado.Lines[I]);
      PB1.Position := PB1.Position + 1;
      LinArq := I;
      Lista.Clear;
      //
      Geral.MyExtractStrings(['|'], [' '], PChar(MeImportado.Lines[I]), Lista);
      //
      if Lista.Count > 0 then
      begin
        REG := Lista[00];
        if QuantidadeDeCamposNaoConferem(COD_VER, REG, Lista.Count) then
          Exit;
        if REG = '0000' then
        begin
*)
          LinArq     := 0;
          REG        := '0000';
          //
          COD_FIN    := 0; // Remessa de arquivo original
          DT_INI     := Geral.FDT(DataIni, 1);
          DT_FIN     := Geral.FDT(DataFim, 1);
          NOME       := DmNFe_0000.QrEmitNO_ENT.Value;
          CNPJ       := Geral.SoNumero_TT(DmNFe_0000.QrEmitCNPJ.Value);
          CPF        := Geral.SoNumero_TT(DmNFe_0000.QrEmitCPF.Value);
          UF         := DmNFe_0000.QrEmitNO_UF.Value;
          IE         := DmNFe_0000.QrEmitIE.Value;
          COD_MUN    := DmNFe_0000.QrEmitLCodMunici.Value;
          IM         := DmNFe_0000.QrEmitNIRE.Value;
          SUFRAMA    := DmNFe_0000.QrEmitSUFRAMA.Value;
          IND_PERFIL := DModG.QrPrmsEmpEFD_IISPED_EFD_IND_PERFIL.Value;
          IND_ATIV   := DModG.QrPrmsEmpEFD_IISPED_EFD_IND_ATIV.Value;
          //
(*
    QrEmitCodigo: TIntegerField;
    QrEmitTipo: TSmallintField;
    QrEmitCNPJ: TWideStringField;
    QrEmitCPF: TWideStringField;
    QrEmitIE: TWideStringField;
    QrEmitRG: TWideStringField;
    QrEmitCNAE: TWideStringField;
    QrEmitNO_ENT: TWideStringField;
    QrEmitFANTASIA: TWideStringField;
    QrEmitRUA: TWideStringField;
    QrEmitCOMPL: TWideStringField;
    QrEmitBAIRRO: TWideStringField;
    QrEmitTe1: TWideStringField;
    QrEmitNO_LOGRAD: TWideStringField;
    QrEmitNO_Munici: TWideStringField;
    QrEmitNO_UF: TWideStringField;
    QrEmitNO_Pais: TWideStringField;
    QrEmitSUFRAMA: TWideStringField;
    QrEmitL_CNPJ: TWideStringField;
    QrEmitL_Ativo: TSmallintField;
    QrEmitLLograd: TSmallintField;
    QrEmitLRua: TWideStringField;
    QrEmitLCompl: TWideStringField;
    QrEmitLNumero: TIntegerField;
    QrEmitLBairro: TWideStringField;
    QrEmitLCidade: TWideStringField;
    QrEmitLUF: TSmallintField;
    QrEmitNO_LLOGRAD: TWideStringField;
    QrEmitLCodMunici: TIntegerField;
    QrEmitNO_LMunici: TWideStringField;
    QrEmitNO_LUF: TWideStringField;
    QrEmitNIRE: TWideStringField;
    QrEmitIEST: TWideStringField;
*)
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_Tabela_0000, False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq',
          'REG', 'COD_VER', 'COD_FIN',
          'DT_INI', 'DT_FIN', 'NOME',
          'CNPJ', 'CPF', 'UF',
          'IE', 'COD_MUN', 'IM',
          'SUFRAMA', 'IND_PERFIL', 'IND_ATIV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq,
          REG, COD_VER, COD_FIN,
          DT_INI, DT_FIN, NOME,
          CNPJ, CPF, UF,
          IE, COD_MUN, IM,
          SUFRAMA, IND_PERFIL, IND_ATIV], [
          ], True);
(*
        end else
        if REG = '0001' then
        begin
          IND_MOV       := 0; // 0=Sem movimento, 1=Com movimento
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = '0005' then
        begin
          FANTASIA      := Lista[01];
          CEP           := Geral.IMV(Lista[02]);
          _END          := Lista[03];
          NUM           := Lista[04];
          COMPL         := Lista[05];
          BAIRRO        := Lista[06];
          FONE          := Lista[07];
          FAX           := Lista[08];
          EMAIL         := Lista[09];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0005', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'FANTASIA',
          'CEP', 'END', 'NUM',
          'COMPL', 'BAIRRO', 'FONE',
          'FAX', 'EMAIL'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, FANTASIA,
          CEP, _END, NUM,
          COMPL, BAIRRO, FONE,
          FAX, EMAIL], [
          ], True);
        end else
        if REG = '0100' then
        begin
          NOME          := Lista[01];
          CPF           := Lista[02];
          CRC           := Lista[03];
          CNPJ          := Lista[04];
          CEP           := Geral.IMV(Lista[05]);
          _END          := Lista[06];
          NUM           := Lista[07];
          COMPL         := Lista[08];
          BAIRRO        := Lista[09];
          FONE          := Lista[10];
          FAX           := Lista[11];
          EMAIL         := Lista[12];
          COD_MUN       := Geral.IMV(Lista[13]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0100', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'NOME',
          'CPF', 'CRC', 'CNPJ',
          'CEP', 'END', 'NUM',
          'COMPL', 'BAIRRO', 'FONE',
          'FAX', 'EMAIL', 'COD_MUN'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, NOME,
          CPF, CRC, CNPJ,
          CEP, _END, NUM,
          COMPL, BAIRRO, FONE,
          FAX, EMAIL, COD_MUN], [
          ], True);
        end else
        if REG = '0150' then
        begin
          COD_PART  := Lista[01];
          NOME      := Lista[02];
          COD_PAIS  := Geral.IMV(Lista[03]);
          CNPJ      := Lista[04];
          CPF       := Lista[05];
          IE        := Lista[06];
          COD_MUN   := Geral.IMV(Lista[07]);
          SUFRAMA   := Lista[08];
          _END      := Lista[09];
          NUM       := Lista[10];
          COMPL     := Lista[11];
          BAIRRO    := Lista[12];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0150', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'COD_PART',
          'NOME', 'COD_PAIS', 'CNPJ',
          'CPF', 'IE', 'COD_MUN',
          'SUFRAMA', 'END', 'NUM',
          'COMPL', 'BAIRRO'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, COD_PART,
          NOME, COD_PAIS, CNPJ,
          CPF, IE, COD_MUN,
          SUFRAMA, _END, NUM,
          COMPL, BAIRRO], [
          ], True);
        end else
        if REG = '0190' then
        begin
          UNID      := Lista[01];
          DESCR     := Lista[02];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0190', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'UNID',
          'DESCR'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, UNID,
          DESCR], [
          ], True);
        end else
        if REG = '0200' then
        begin
          COD_ITEM      := Lista[01];
          DESCR_ITEM    := Lista[02];
          COD_BARRA     := Lista[03];
          COD_ANT_ITEM  := Lista[04];
          UNID_INV      := Lista[05];
          TIPO_ITEM     := Lista[06];
          COD_NCM       := Lista[07];
          EX_IPI        := Lista[08];
          COD_GEN       := Lista[09];
          COD_LST       := Lista[10];
          ALIQ_ICMS     := Geral.DMV(Lista[11]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0200', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'COD_ITEM',
          'DESCR_ITEM', 'COD_BARRA', 'COD_ANT_ITEM',
          'UNID_INV', 'TIPO_ITEM', 'COD_NCM',
          'EX_IPI', 'COD_GEN', 'COD_LST',
          'ALIQ_ICMS'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, COD_ITEM,
          DESCR_ITEM, COD_BARRA, COD_ANT_ITEM,
          UNID_INV, TIPO_ITEM, COD_NCM,
          EX_IPI, COD_GEN, COD_LST,
          ALIQ_ICMS], [
          ], True);
        end else
        if REG = '0400' then
        begin
          COD_NAT       := Lista[01];
          DESCR_NAT     := Lista[02];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0400', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'COD_NAT',
          'DESCR_NAT'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, COD_NAT,
          DESCR_NAT], [
          ], True);
        end else
        if REG = '0990' then
        begin
          QTD_LIN_0     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_0'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_0], [
          ], True);
        end else
        if REG = 'C001' then
        begin
*)
          IND_MOV       := '1'; // Sem dados informados
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, CO_Tabela_C001, False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
(*
        end else
        if REG = 'C100' then
        begin
          C100           := I; // Usa no C170 e C190 filhos
          IND_OPER       := Lista[01];
          IND_EMIT       := Lista[02];
          COD_PART       := Lista[03];
          COD_MOD        := Lista[04];
          COD_SIT        := Lista[05];
          SER            := Lista[06];
          NUM_DOC        := Geral.IMV(Lista[07]);
          CHV_NFE        := Lista[08];
          DT_DOC         := Geral.FDT(Geral.ValidaDataBR(Lista[09], True, False), 1);
          DT_E_S         := Geral.FDT(Geral.ValidaDataBR(Lista[10], True, False), 1);
          VL_DOC         := Geral.DMV(Lista[11]);
          IND_PGTO       := Lista[12];
          VL_DESC        := Geral.DMV(Lista[13]);
          VL_ABAT_NT     := Geral.DMV(Lista[14]);
          VL_MERC        := Geral.DMV(Lista[15]);
          IND_FRT        := Lista[16];
          VL_FRT         := Geral.DMV(Lista[17]);
          VL_SEG         := Geral.DMV(Lista[18]);
          VL_OUT_DA      := Geral.DMV(Lista[19]);
          VL_BC_ICMS     := Geral.DMV(Lista[20]);
          VL_ICMS        := Geral.DMV(Lista[21]);
          VL_BC_ICMS_ST  := Geral.DMV(Lista[22]);
          VL_ICMS_ST     := Geral.DMV(Lista[23]);
          VL_IPI         := Geral.DMV(Lista[24]);
          VL_PIS         := Geral.DMV(Lista[25]);
          VL_COFINS      := Geral.DMV(Lista[26]);
          VL_PIS_ST      := Geral.DMV(Lista[27]);
          VL_COFINS_ST   := Geral.DMV(Lista[28]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdC100', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_OPER',
          'IND_EMIT', 'COD_PART', 'COD_MOD',
          'COD_SIT', 'SER', 'NUM_DOC',
          'CHV_NFE', 'DT_DOC', 'DT_E_S',
          'VL_DOC', 'IND_PGTO', 'VL_DESC',
          'VL_ABAT_NT', 'VL_MERC', 'IND_FRT',
          'VL_FRT', 'VL_SEG', 'VL_OUT_DA',
          'VL_BC_ICMS', 'VL_ICMS', 'VL_BC_ICMS_ST',
          'VL_ICMS_ST', 'VL_IPI', 'VL_PIS',
          'VL_COFINS', 'VL_PIS_ST', 'VL_COFINS_ST'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_OPER,
          IND_EMIT, COD_PART, COD_MOD,
          COD_SIT, SER, NUM_DOC,
          CHV_NFE, DT_DOC, DT_E_S,
          VL_DOC, IND_PGTO, VL_DESC,
          VL_ABAT_NT, VL_MERC, IND_FRT,
          VL_FRT, VL_SEG, VL_OUT_DA,
          VL_BC_ICMS, VL_ICMS, VL_BC_ICMS_ST,
          VL_ICMS_ST, VL_IPI, VL_PIS,
          VL_COFINS, VL_PIS_ST, VL_COFINS_ST], [
          ], True);
        end else
        if REG = 'C170' then
        begin
          NUM_ITEM        := Geral.IMV(Lista[01]);
          COD_ITEM        := Lista[02];
          DESCR_COMPL     := Lista[03];
          QTD             := Geral.DMV(Lista[04]);
          UNID            := Lista[05];
          VL_ITEM         := Geral.DMV(Lista[06]);
          VL_DESC         := Geral.DMV(Lista[07]);
          IND_MOV         := Lista[08];
          CST_ICMS        := Geral.IMV(Lista[09]);
          CFOP            := Geral.IMV(Lista[10]);
          COD_NAT         := Lista[11];
          VL_BC_ICMS      := Geral.DMV(Lista[12]);
          ALIQ_ICMS       := Geral.DMV(Lista[13]);
          VL_ICMS         := Geral.DMV(Lista[14]);
          VL_BC_ICMS_ST   := Geral.DMV(Lista[15]);
          ALIQ_ST         := Geral.DMV(Lista[16]);
          VL_ICMS_ST      := Geral.DMV(Lista[17]);
          IND_APUR        := Lista[18];
          CST_IPI         := Lista[19];
          COD_ENQ         := Lista[20];
          VL_BC_IPI       := Geral.DMV(Lista[21]);
          ALIQ_IPI        := Geral.DMV(Lista[22]);
          VL_IPI          := Geral.DMV(Lista[23]);
          CST_PIS         := Geral.DMV(Lista[24]);
          VL_BC_PIS       := Geral.DMV(Lista[25]);
          ALIQ_PIS_p      := Geral.DMV(Lista[26]);
          QUANT_BC_PIS    := Geral.DMV(Lista[27]);
          ALIQ_PIS_r      := Geral.DMV(Lista[28]);
          VL_PIS          := Geral.DMV(Lista[29]);
          CST_COFINS      := Geral.DMV(Lista[30]);
          VL_BC_COFINS    := Geral.DMV(Lista[31]);
          ALIQ_COFINS_p   := Geral.DMV(Lista[32]);
          QUANT_BC_COFINS := Geral.DMV(Lista[33]);
          ALIQ_COFINS_r   := Geral.DMV(Lista[34]);
          VL_COFINS       := Geral.DMV(Lista[35]);
          COD_CTA         := Lista[36];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdC170', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'C100', 'REG',
          'NUM_ITEM', 'COD_ITEM', 'DESCR_COMPL',
          'QTD', 'UNID', 'VL_ITEM',
          'VL_DESC', 'IND_MOV', 'CST_ICMS',
          'CFOP', 'COD_NAT', 'VL_BC_ICMS',
          'ALIQ_ICMS', 'VL_ICMS', 'VL_BC_ICMS_ST',
          'ALIQ_ST', 'VL_ICMS_ST', 'IND_APUR',
          'CST_IPI', 'COD_ENQ', 'VL_BC_IPI',
          'ALIQ_IPI', 'VL_IPI', 'CST_PIS',
          'VL_BC_PIS', 'ALIQ_PIS_p', 'QUANT_BC_PIS',
          'ALIQ_PIS_r', 'VL_PIS', 'CST_COFINS',
          'VL_BC_COFINS', 'ALIQ_COFINS_p', 'QUANT_BC_COFINS',
          'ALIQ_COFINS_r', 'VL_COFINS', 'COD_CTA'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, C100, REG,
          NUM_ITEM, COD_ITEM, DESCR_COMPL,
          QTD, UNID, VL_ITEM,
          VL_DESC, IND_MOV, CST_ICMS,
          CFOP, COD_NAT, VL_BC_ICMS,
          ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST,
          ALIQ_ST, VL_ICMS_ST, IND_APUR,
          CST_IPI, COD_ENQ, VL_BC_IPI,
          ALIQ_IPI, VL_IPI, CST_PIS,
          VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
          ALIQ_PIS_r, VL_PIS, CST_COFINS,
          VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS,
          ALIQ_COFINS_r, VL_COFINS, COD_CTA], [
          ], True);
        end else
        if REG = 'C190' then
        begin
          CST_ICMS        := Geral.IMV(Lista[01]);
          CFOP            := Geral.IMV(Lista[02]);
          ALIQ_ICMS       := Geral.DMV(Lista[03]);
          VL_OPR          := Geral.DMV(Lista[04]);
          VL_BC_ICMS      := Geral.DMV(Lista[05]);
          VL_ICMS         := Geral.DMV(Lista[06]);
          VL_BC_ICMS_ST   := Geral.DMV(Lista[07]);
          VL_ICMS_ST      := Geral.DMV(Lista[08]);
          VL_RED_BC       := Geral.DMV(Lista[09]);
          VL_IPI          := Geral.DMV(Lista[10]);
          COD_OBS         := Lista[11];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdC190', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'C100', 'REG',
          'CST_ICMS', 'CFOP', 'ALIQ_ICMS',
          'VL_OPR', 'VL_BC_ICMS', 'VL_ICMS',
          'VL_BC_ICMS_ST', 'VL_ICMS_ST', 'VL_RED_BC',
          'VL_IPI', 'COD_OBS'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, C100, REG,
          CST_ICMS, CFOP, ALIQ_ICMS,
          VL_OPR, VL_BC_ICMS, VL_ICMS,
          VL_BC_ICMS_ST, VL_ICMS_ST, VL_RED_BC,
          VL_IPI, COD_OBS], [
          ], True);
        end else
        if REG = 'C500' then
        begin
          C500             := I;
          IND_OPER         := Lista[01];
          IND_EMIT         := Lista[02];
          COD_PART         := Lista[03];
          COD_MOD          := Lista[04];
          COD_SIT          := Lista[05];
          SER              := Lista[06];
          SUB              := Lista[07];
          COD_CONS         := Lista[08];
          NUM_DOC          := Geral.IMV(Lista[09]);
          DT_DOC           := Geral.FDT(Geral.ValidaDataBR(Lista[10], True, False), 1);
          DT_E_S           := Geral.FDT(Geral.ValidaDataBR(Lista[11], True, False), 1);
          VL_DOC           := Geral.DMV(Lista[12]);
          VL_DESC          := Geral.DMV(Lista[13]);
          VL_FORN          := Geral.DMV(Lista[14]);
          VL_SERV_NT       := Geral.DMV(Lista[15]);
          VL_TERC          := Geral.DMV(Lista[16]);
          VL_DA            := Geral.DMV(Lista[17]);
          VL_BC_ICMS       := Geral.DMV(Lista[18]);
          VL_ICMS          := Geral.DMV(Lista[19]);
          VL_BC_ICMS_ST    := Geral.DMV(Lista[20]);
          VL_ICMS_ST       := Geral.DMV(Lista[21]);
          COD_INF          := Lista[22];
          VL_PIS           := Geral.DMV(Lista[23]);
          VL_COFINS        := Geral.DMV(Lista[24]);
          TP_LIGACAO       := Lista[25];
          COD_GRUPO_TENSAO := Lista[26];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdC500', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_OPER',
          'IND_EMIT', 'COD_PART', 'COD_MOD',
          'COD_SIT', 'SER', 'SUB',
          'COD_CONS', 'NUM_DOC', 'DT_DOC',
          'DT_E_S', 'VL_DOC', 'VL_DESC',
          'VL_FORN', 'VL_SERV_NT', 'VL_TERC',
          'VL_DA', 'VL_BC_ICMS', 'VL_ICMS',
          'VL_BC_ICMS_ST', 'VL_ICMS_ST', 'COD_INF',
          'VL_PIS', 'VL_COFINS', 'TP_LIGACAO',
          'COD_GRUPO_TENSAO'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_OPER,
          IND_EMIT, COD_PART, COD_MOD,
          COD_SIT, SER, SUB,
          COD_CONS, NUM_DOC, DT_DOC,
          DT_E_S, VL_DOC, VL_DESC,
          VL_FORN, VL_SERV_NT, VL_TERC,
          VL_DA, VL_BC_ICMS, VL_ICMS,
          VL_BC_ICMS_ST, VL_ICMS_ST, COD_INF,
          VL_PIS, VL_COFINS, TP_LIGACAO,
          COD_GRUPO_TENSAO], [
          ], True);
        end else
        if REG = 'C590' then
        begin
          CST_ICMS         := Geral.IMV(Lista[01]);
          CFOP             := Geral.IMV(Lista[02]);
          ALIQ_ICMS        := Geral.IMV(Lista[03]);
          VL_OPR           := Geral.DMV(Lista[04]);
          VL_BC_ICMS       := Geral.DMV(Lista[05]);
          VL_ICMS          := Geral.DMV(Lista[06]);
          VL_BC_ICMS_ST    := Geral.DMV(Lista[07]);
          VL_ICMS_ST       := Geral.DMV(Lista[08]);
          VL_RED_BC        := Geral.DMV(Lista[09]);
          COD_OBS          := Lista[10];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdC590', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'C500', 'REG',
          'CST_ICMS', 'CFOP', 'ALIQ_ICMS',
          'VL_OPR', 'VL_BC_ICMS', 'VL_ICMS',
          'VL_BC_ICMS_ST', 'VL_ICMS_ST', 'VL_RED_BC',
          'COD_OBS'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, C500, REG,
          CST_ICMS, CFOP, ALIQ_ICMS,
          VL_OPR, VL_BC_ICMS, VL_ICMS,
          VL_BC_ICMS_ST, VL_ICMS_ST, VL_RED_BC,
          COD_OBS], [
          ], True);
        end else
        if REG = 'C990' then
        begin
          QTD_LIN_C     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdC990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_C'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_C], [
          ], True);
        end else
        if REG = 'D001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdD001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = 'D100' then
        begin
          D100          := I;
          IND_OPER      := Lista[01];
          IND_EMIT      := Lista[02];
          COD_PART      := Lista[03];
          COD_MOD       := Lista[04];
          COD_SIT       := Lista[05];
          SER           := Lista[06];
          SUB_C         := Lista[07];
          NUM_DOC       := Geral.IMV(Lista[08]);
          CHV_CTE       := Lista[09];
          DT_DOC        := Geral.FDT(Geral.ValidaDataBR(Lista[10], True, False), 1);
          DT_A_P        := Geral.FDT(Geral.ValidaDataBR(Lista[11], True, False), 1);
          TP_CTE        := Geral.IMV(Lista[12]);
          CHV_CTE_REF   := Lista[13];
          VL_DOC        := Geral.DMV(Lista[14]);
          VL_DESC       := Geral.DMV(Lista[15]);
          IND_FRT       := Lista[16];
          VL_SERV       := Geral.DMV(Lista[17]);
          VL_BC_ICMS    := Geral.DMV(Lista[18]);
          VL_ICMS       := Geral.DMV(Lista[19]);
          VL_NT         := Geral.DMV(Lista[20]);
          COD_INF       := Lista[21];
          COD_CTA       := Lista[22];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdD100', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_OPER',
          'IND_EMIT', 'COD_PART', 'COD_MOD',
          'COD_SIT', 'SER', 'SUB',
          'NUM_DOC', 'CHV_CTE', 'DT_DOC',
          'DT_A_P', 'TP_CTE', 'CHV_CTE_REF',
          'VL_DOC', 'VL_DESC', 'IND_FRT',
          'VL_SERV', 'VL_BC_ICMS', 'VL_ICMS',
          'VL_NT', 'COD_INF', 'COD_CTA'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_OPER,
          IND_EMIT, COD_PART, COD_MOD,
          COD_SIT, SER, SUB_C,
          NUM_DOC, CHV_CTE, DT_DOC,
          DT_A_P, TP_CTE, CHV_CTE_REF,
          VL_DOC, VL_DESC, IND_FRT,
          VL_SERV, VL_BC_ICMS, VL_ICMS,
          VL_NT, COD_INF, COD_CTA], [
          ], True);
        end else
        if REG = 'D190' then
        begin
          CST_ICMS      := Geral.IMV(Lista[01]);
          CFOP          := Geral.IMV(Lista[02]);
          ALIQ_ICMS     := Geral.DMV(Lista[03]);
          VL_OPR        := Geral.DMV(Lista[04]);
          VL_BC_ICMS    := Geral.DMV(Lista[05]);
          VL_ICMS       := Geral.DMV(Lista[06]);
          VL_RED_BC     := Geral.DMV(Lista[07]);
          COD_OBS       := Lista[08];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdD190', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'D100', 'REG',
          'CST_ICMS', 'CFOP', 'ALIQ_ICMS',
          'VL_OPR', 'VL_BC_ICMS', 'VL_ICMS',
          'VL_RED_BC', 'COD_OBS'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, D100, REG,
          CST_ICMS, CFOP, ALIQ_ICMS,
          VL_OPR, VL_BC_ICMS, VL_ICMS,
          VL_RED_BC, COD_OBS], [
          ], True);
        end else
        if REG = 'D500' then
        begin
          D500          := I;
          IND_OPER      := Lista[01];
          IND_EMIT      := Lista[02];
          COD_PART      := Lista[03];
          COD_MOD       := Lista[04];
          COD_SIT       := Lista[05];
          SER           := Lista[06];
          SUB           := Lista[07];
          NUM_DOC       := Geral.IMV(Lista[08]);
          DT_DOC        := Geral.FDT(Geral.ValidaDataBR(Lista[09], True, False), 1);
          DT_A_P        := Geral.FDT(Geral.ValidaDataBR(Lista[10], True, False), 1);
          VL_DOC        := Geral.DMV(Lista[11]);
          VL_DESC       := Geral.DMV(Lista[12]);
          VL_SERV       := Geral.DMV(Lista[13]);
          VL_SERV_NT    := Geral.DMV(Lista[14]);
          VL_TERC       := Geral.DMV(Lista[15]);
          VL_DA         := Geral.DMV(Lista[16]);
          VL_BC_ICMS    := Geral.DMV(Lista[17]);
          VL_ICMS       := Geral.DMV(Lista[18]);
          COD_INF       := Lista[19];
          VL_PIS        := Geral.DMV(Lista[20]);
          VL_COFINS     := Geral.DMV(Lista[21]);
          COD_CTA       := Lista[22];
          TP_ASSINANTE  := Geral.IMV(Lista[23]);
          //

          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdD500', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_OPER',
          'IND_EMIT', 'COD_PART', 'COD_MOD',
          'COD_SIT', 'SER', 'SUB',
          'NUM_DOC', 'DT_DOC', 'DT_A_P',
          'VL_DOC', 'VL_DESC', 'VL_SERV',
          'VL_SERV_NT', 'VL_TERC', 'VL_DA',
          'VL_BC_ICMS', 'VL_ICMS', 'COD_INF',
          'VL_PIS', 'VL_COFINS', 'COD_CTA',
          'TP_ASSINANTE'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_OPER,
          IND_EMIT, COD_PART, COD_MOD,
          COD_SIT, SER, SUB,
          NUM_DOC, DT_DOC, DT_A_P,
          VL_DOC, VL_DESC, VL_SERV,
          VL_SERV_NT, VL_TERC, VL_DA,
          VL_BC_ICMS, VL_ICMS, COD_INF,
          VL_PIS, VL_COFINS, COD_CTA,
          TP_ASSINANTE], [
          ], True);
        end else
        if REG = 'D590' then
        begin
          CST_ICMS      := Geral.IMV(Lista[01]);
          CFOP          := Geral.IMV(Lista[02]);
          ALIQ_ICMS     := Geral.DMV(Lista[03]);
          VL_OPR        := Geral.DMV(Lista[04]);
          VL_BC_ICMS    := Geral.DMV(Lista[05]);
          VL_ICMS       := Geral.DMV(Lista[06]);
          VL_BC_ICMS_ST := Geral.DMV(Lista[07]);
          VL_ICMS_ST    := Geral.DMV(Lista[08]);
          VL_RED_BC     := Geral.DMV(Lista[09]);
          COD_OBS       := Lista[10];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdD590', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'D500', 'REG',
          'CST_ICMS', 'CFOP', 'ALIQ_ICMS',
          'VL_OPR', 'VL_BC_ICMS', 'VL_ICMS',
          'VL_BC_ICMS_ST', 'VL_ICMS_ST', 'VL_RED_BC',
          'COD_OBS'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, D500, REG,
          CST_ICMS, CFOP, ALIQ_ICMS,
          VL_OPR, VL_BC_ICMS, VL_ICMS,
          VL_BC_ICMS_ST, VL_ICMS_ST, VL_RED_BC,
          COD_OBS], [
          ], True);
        end else
        if REG = 'D990' then
        begin
          QTD_LIN_D     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdD990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_D'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_D], [
          ], True);
        end else
        if REG = 'E001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdE001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = 'E100' then
        begin
          E100          := I;
          DT_INI        := Geral.FDT(Geral.ValidaDataBR(Lista[01], True, False), 1);
          DT_FIN        := Geral.FDT(Geral.ValidaDataBR(Lista[02], True, False), 1);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdE100', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'DT_INI',
          'DT_FIN'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, DT_INI,
          DT_FIN], [
          ], True);
        end else
        if REG = 'E110' then
        begin
          E110 := I;
          VL_TOT_DEBITOS     := Geral.DMV(Lista[01]);
          VL_AJ_DEBITOS      := Geral.DMV(Lista[02]);
          VL_TOT_AJ_DEBITOS  := Geral.DMV(Lista[03]);
          VL_ESTORNOS_CRED   := Geral.DMV(Lista[04]);
          VL_TOT_CREDITOS    := Geral.DMV(Lista[05]);
          VL_AJ_CREDITOS     := Geral.DMV(Lista[06]);
          VL_TOT_AJ_CREDITOS := Geral.DMV(Lista[07]);
          VL_ESTORNOS_DEB    := Geral.DMV(Lista[08]);
          VL_SLD_CREDOR_ANT  := Geral.DMV(Lista[09]);
          VL_SLD_APURADO     := Geral.DMV(Lista[10]);
          VL_TOT_DED         := Geral.DMV(Lista[11]);
          VL_ICMS_RECOLHER   := Geral.DMV(Lista[12]);
          VL_SLD_CREDOR_TRANSPORTAR := Geral.DMV(Lista[13]);
          DEB_ESP            := Geral.DMV(Lista[14]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdE110', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'E100', 'REG',
          'VL_TOT_DEBITOS', 'VL_AJ_DEBITOS', 'VL_TOT_AJ_DEBITOS',
          'VL_ESTORNOS_CRED', 'VL_TOT_CREDITOS', 'VL_AJ_CREDITOS',
          'VL_TOT_AJ_CREDITOS', 'VL_ESTORNOS_DEB', 'VL_SLD_CREDOR_ANT',
          'VL_SLD_APURADO', 'VL_TOT_DED', 'VL_ICMS_RECOLHER',
          'VL_SLD_CREDOR_TRANSPORTAR', 'DEB_ESP'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, E100, REG,
          VL_TOT_DEBITOS, VL_AJ_DEBITOS, VL_TOT_AJ_DEBITOS,
          VL_ESTORNOS_CRED, VL_TOT_CREDITOS, VL_AJ_CREDITOS,
          VL_TOT_AJ_CREDITOS, VL_ESTORNOS_DEB, VL_SLD_CREDOR_ANT,
          VL_SLD_APURADO, VL_TOT_DED, VL_ICMS_RECOLHER,
          VL_SLD_CREDOR_TRANSPORTAR, DEB_ESP], [
          ], True);
        end else
        if REG = 'E111' then
        begin
          COD_AJ_APUR        := Lista[01];
          DESCR_COMPL_AJ     := Lista[02];
          VL_AJ_APUR         := Geral.DMV(Lista[03]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdE111', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'E110', 'REG',
          'COD_AJ_APUR', 'DESCR_COMPL_AJ', 'VL_AJ_APUR'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, E110, REG,
          COD_AJ_APUR, DESCR_COMPL_AJ, VL_AJ_APUR], [
          ], True);
        end else
        if REG = 'E116' then
        begin
          COD_OR             := Lista[01];
          VL_OR              := Geral.DMV(Lista[02]);
          DT_VCTO            := Geral.FDT(Geral.ValidaDataBR(Lista[03], True, False), 1);
          COD_REC            := Lista[04];
          NUM_PROC           := Lista[05];
          IND_PROC           := Lista[06];
          PROC               := Lista[07];
          TXT_COMPL          := Lista[08];
          MES_REF            := Lista[09];
          if MES_REF = '' then
            MES_REF := Geral.FDT(Geral.ValidaDataBR('00000000', True, False), 1)
          else
            MES_REF := Geral.FDT(Geral.ValidaDataBR('01' + MES_REF, False, False), 1);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdE116', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq',
          'E110', 'REG', 'COD_OR',
          'VL_OR', 'DT_VCTO', 'COD_REC',
          'NUM_PROC', 'IND_PROC', 'PROC',
          'TXT_COMPL', 'MES_REF'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq,
          E110, REG, COD_OR,
          VL_OR, DT_VCTO, COD_REC,
          NUM_PROC, IND_PROC, PROC,
          TXT_COMPL, MES_REF], [
          ], True);
        end else
        if REG = 'E990' then
        begin
          QTD_LIN_E     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdE990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_E'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_E], [
          ], True);
        end else
        if REG = 'G001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdG001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = 'G990' then
        begin
          QTD_LIN_G     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdG990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_G'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_G], [
          ], True);
        end else
        if REG = 'H001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdH001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = 'H005' then
        begin
          DT_INV        := Geral.FDT(Geral.ValidaDataBR(Lista[01], True, False), 1);
          VL_INV        := Geral.DMV(Lista[02]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdH005', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'DT_INV',
          'VL_INV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, DT_INV,
          VL_INV], [
          ], True);
        end else
        if REG = 'H010' then
        begin
          COD_ITEM      := Lista[01];
          UNID          := Lista[02];
          QTD           := Geral.DMV(Lista[03]);
          VL_UNIT       := Geral.DMV(Lista[04]);
          VL_ITEM       := Geral.DMV(Lista[05]);
          IND_PROP      := Lista[06];
          COD_PART      := Lista[07];
          TXT_COMPL     := Lista[08];
          COD_CTA       := Lista[09];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdH010', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'COD_ITEM',
          'UNID', 'QTD', 'VL_UNIT',
          'VL_ITEM', 'IND_PROP', 'COD_PART',
          'TXT_COMPL', 'COD_CTA'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, COD_ITEM,
          UNID, QTD, VL_UNIT,
          VL_ITEM, IND_PROP, COD_PART,
          TXT_COMPL, COD_CTA], [
          ], True);
        end else
        if REG = 'H990' then
        begin
          QTD_LIN_H     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdH990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_H'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_H], [
          ], True);
        end else
        if REG = '1001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd1001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = '1990' then
        begin
          QTD_LIN_1     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd1990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_1'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_1], [
          ], True);
        end else
        if REG = '9001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd9001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = '9900' then
        begin
          REG_BLC       := Lista[01];
          QTD_REG_BLC   := Geral.IMV(Lista[02]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd9900', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'REG_BLC',
          'QTD_REG_BLC'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, REG_BLC,
          QTD_REG_BLC], [
          ], True);
        end else
        if REG = '9990' then
        begin
          QTD_LIN_9     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd9990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_9'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_9], [
          ], True);
        end else
        if REG = '9999' then
        begin
          QTD_LIN       := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd9999', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN], [
          ], True);
        end else
        begin
          MeIgnor.Lines.Add('Linha: ' + FormatFloat('0000', I) + ' Registro: ' + REG);
        end;
      end else Geral.MensagemBox('A linha ' + IntToStr(I+1)  +
      ' n�o possui texto algum!', 'Aviso', MB_OK+MB_ICONWARNING);
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregamento finalizado! Abrindo tabelas.');
    //
    // O "0" n�o porque QrErr usa na exporta��o e n�o na importa��o!
    for I := 1 to High(FTbs_SPED_EFD) do // -1
    begin
      MySQLQuery := nil;
      NomeTab := 'Qr' + Copy(FTbs_SPED_EFD[I], 8, 4);
      if (FindComponent(NomeTab) as TmySQLQuery) <> nil then
        UMyMod.ReabreQuery(FindComponent(NomeTab) as TmySQLQuery, Dmod.MyDB, [FImporExpor, FAnoMes, Empresa], Tit)
      else begin
        Geral.MensagemBox('N�o foi poss�vel lozalizar a Query "' + NomeTab +
        '". Avise a DERMATEK', 'ERRO', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando integridade do arquivo.');
    Qr9900.First;
    while not Qr9900.Eof do
    begin
      NomeTab := 'Qr' + Qr9900REG_BLC.Value;
      if (FindComponent(NomeTab) as TmySQLQuery) <> nil then
        MySQLQuery := FindComponent(NomeTab) as TmySQLQuery
      else begin
        MySQLQuery := nil;
        Geral.MensagemBox('N�o foi poss�vel lozalizar a Query "' + NomeTab +
        '". Avise a DERMATEK', 'ERRO', MB_OK+MB_ICONWARNING);
      end;
      if MySQLQuery <> nil then
      begin
        if MySQLQuery.RecordCount <> Qr9900QTD_REG_BLC.Value then
        begin
          Geral.MensagemBox('A quantidade de linhas informadas para o registro "'
          + Qr9900REG_BLC.Value + '" n�o confere com o informado no arquivo!',
          'Aviso', MB_OK+MB_ICONWARNING);
          Exit;
        end;
      end;
      //
      Qr9900.Next;
    end;
    //
    FCarregou := True;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando validade do arquivo.');
    //
    QrVersao.Close;
    QrVersao.Params[00].AsInteger := COD_VER;
    QrVersao.Open;
    if QrVersao.RecordCount > 0 then
    begin
      if (DataFim < QrVersaoDT_INI.Value) or (
      (DataIni > QrVersaoDT_FIN.Value) and
      (QrVersaoDT_FIN.Value > 0)) then
        Geral.MensagemBox('AVISO!' + sLineBreak +
        'A vers�o informada no arquivo (vers�o = ' + IntToStr(COD_VER) +
        ') n�o condiz com o per�odo informado no arquivo!' + sLineBreak +
        'Informe o respons�vel pelo aplicativo gerador do arquivo!',
        'Aviso', MB_OK+MB_ICONINFORMATION);
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando registros ignorados.');
    if MeIgnor.Lines.Count > 0 then
    begin
      Geral.MensagemBox('Carregamento cancelado!' + sLineBreak +
      'Existem ' + FormatFloat('0', MeIgnor.Lines.Count) +
      ' linhas no arquivo sem implementa��o no aplicativo!' + sLineBreak +
      'Informe a DERMATEK!', 'Aviso', MB_OK+MB_ICONWARNING);
      //
      Exit;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando cadastros das entidades.');
    PB1.Position := 0;
    PB1.Max := Qr0150.RecordCount;
    Qr0150.First;
    while not Qr0150.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando cadastros das entidades. ' +
      Qr0150CNPJ.Value + Qr0150CPF.Value + ' (' + Qr0150COD_PART.Value + ')');
      //
      Entidade := 0;
      if Qr0150CNPJ.Value <> '' then
        DModG.ObtemEntidadeDeCNPJCFP(Qr0150CNPJ.Value, Entidade)
      else
      if Qr0150CPF.Value <> '' then
        DModG.ObtemEntidadeDeCNPJCFP(Qr0150CPF.Value, Entidade)
      else
      if Qr0150COD_PART.Value <> '' then
        DModG.ObtemEntidadeDeCOD_PART(Qr0150COD_PART.Value, Entidade);
      //
      if Entidade <> 0 then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'spedefd0150', False, [
        'Entidade'], [
        'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
        Entidade], [
        Qr0150ImporExpor.Value, Qr0150AnoMes.Value,
        Qr0150Empresa.Value, Qr0150LinArq.Value], False);
      end;
      //
      Qr0150.Next;
    end;
    UMyMod.ReabreQuery(QrErr0150, Dmod.MyDB, [FImporExpor, FAnoMes, Empresa], Tit);
    if QrErr0150.RecordCount > 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Atrele as entidades caso existam, ou cadastre-as antes de continuar!');
      PageControl1.ActivePageIndex := 2;
      PageControl2.ActivePageIndex := 0;
      //
      Geral.MensagemBox('Carregamento cancelado!' + sLineBreak +
      'Existem ' + FormatFloat('0', QrErr0150.RecordCount) +
      ' entidades sem cadastro no aplicativo!' + sLineBreak +
      'Atrele as entidades caso existam, ou cadastre antes de continuar!',
      'Aviso', MB_OK+MB_ICONWARNING);
      //
      Exit;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando cadastros das unidades de medida.');
    PB1.Position := 0;
    PB1.Max := Qr0190.RecordCount;
    Qr0190.First;
    while not Qr0190.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
       'Verificando cadastros das unidades de medida. ' + Qr0190UNID.Value);
      //
      UnidMed := 0;
      if Qr0190UNID.Value <> '' then
      begin
        QrLocUM.Close;
        QrLocUM.Params[0].AsString := Qr0190UNID.Value;
        QrLocUM.Open;
        //
        if QrLocUM.RecordCount > 0 then
          UnidMed := QrLocUMCodigo.Value;
      end;
      //
      if UnidMed <> 0 then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'spedefd0190', False, [
        'UnidMed'], [
        'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
        UnidMed], [
        Qr0190ImporExpor.Value, Qr0190AnoMes.Value,
        Qr0190Empresa.Value, Qr0190LinArq.Value], False);
      end;
      //
      Qr0190.Next;
    end;
    UMyMod.ReabreQuery(QrErr0190, Dmod.MyDB, [FImporExpor, FAnoMes, Empresa], Tit);
    if QrErr0190.RecordCount > 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Atrele as unidades de medida caso existam, ou cadastre-as antes de continuar!');
      PageControl1.ActivePageIndex := 2;
      PageControl2.ActivePageIndex := 1;
      //
      Geral.MensagemBox('Carregamento cancelado!' + sLineBreak +
      'Existem ' + FormatFloat('0', QrErr0190.RecordCount) +
      ' unidades de medida sem cadastro no aplicativo!' + sLineBreak +
      'Atrele as unidades de medida caso existam, ou cadastre antes de continuar!',
      'Aviso', MB_OK+MB_ICONWARNING);
      //
      Exit;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando cadastros dos produtos.');
    PB1.Position := 0;
    PB1.Max := Qr0200.RecordCount;
    Qr0200.First;
    while not Qr0200.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando cadastros dos produtos. ' +
      Qr0200COD_ITEM.Value + ' : ' + Qr0200DESCR_ITEM.Value);
      //
      GraGruX := 0;
      if Qr0200COD_ITEM.Value <> '' then
      begin
        QrLocGGX.Close;
        QrLocGGX.SQL.Clear;
        QrLocGGX.SQL.Add('SELECT ggx.Controle');
        QrLocGGX.SQL.Add('FROM gragrux ggx');
        QrLocGGX.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
        QrLocGGX.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
        QrLocGGX.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
        QrLocGGX.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
        QrLocGGX.SQL.Add('WHERE ggx.Controle=' + Qr0200COD_ITEM.Value);
        if CkNomeGGXIgual.Checked then
        begin
          // Usar Params[]  para evitar erro de '"'
          QrLocGGX.SQL.Add('AND gg1.Nome=:P0');
          QrLocGGX.Params[00].AsString := Qr0200DESCR_ITEM.Value;
        end;
        QrLocGGX.Open;
        //
        if QrLocGGX.RecordCount > 0 then
        begin
          GraGruX  := QrLocGGXControle.Value;
          NivelExclu := 0;
        end else
          DmProd.ObtemNovoGraGruXdeExcluido(
            Geral.IMV(Geral.SoNumero_TT(Qr0200COD_ITEM.Value)),
            GraGruX, NivelExclu);
      end;
      //
      if GraGruX <> 0 then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'spedefd0200', False, [
        'GraGruX', 'NivelExclu'], [
        'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
        GraGruX, NivelExclu], [
        Qr0200ImporExpor.Value, Qr0200AnoMes.Value,
        Qr0200Empresa.Value, Qr0200LinArq.Value], False);
      end;
      //
      Qr0200.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Comparando cadastro de produtos.');
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO _spedinn_0200_');
    DModG.QrUpdPID1.SQL.Add('SELECT LinArq, 0 Distancia, GraGruX,');
    DModG.QrUpdPID1.SQL.Add('"" Nome, COD_ITEM, DESCR_ITEM , COD_ANT_ITEM, ');
    DModG.QrUpdPID1.SQL.Add('"" Levenshtein_Meu, "" Levenshtein_Inn, NivelExclu');
    DModG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.spedefd0200');
    DModG.QrUpdPID1.SQL.Add('WHERE ImporExpor=:P0');
    DModG.QrUpdPID1.SQL.Add('AND AnoMes=:P1');
    DModG.QrUpdPID1.SQL.Add('AND Empresa=:P2;');
    DModG.QrUpdPID1.SQL.Add('');
    DModG.QrUpdPID1.Params[00].AsInteger := FImporExpor;
    DModG.QrUpdPID1.Params[01].AsInteger := FAnoMes;
    DModG.QrUpdPID1.Params[02].AsInteger := Empresa;
    DModG.QrUpdPID1.ExecSQL;
    //
    QrINN_0200.Close;
    QrINN_0200.Open;
    PB1.Position := 0;
    PB1.Max := QrINN_0200.RecordCount;
    while not QrINN_0200.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Comparando cadastro do produto: ' +
        QrINN_0200COD_ITEM.Value);
      QrGG1.Close;
      QrGG1.Params[0].AsInteger := QrINN_0200GraGruX.Value;
      QrGG1.Open;
      Levenshtein_Meu :=
        Copy(MLAGeral.SimplificaPalavras(QrGG1Nome.Value), 1, 120);
      Levenshtein_Inn :=
        Copy(MLAGeral.SimplificaPalavras(QrINN_0200DESCR_ITEM.Value), 1, 120);
      Distancia := MLAGeral.EditDistance_Levenshtein(
        Levenshtein_Meu, Levenshtein_Inn);
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_spedinn_0200_', False, [
      'Nome', 'Distancia', 'Levenshtein_Meu', 'Levenshtein_Inn'
      ], ['LinArq'], [
      QrGG1Nome.Value, Distancia, Levenshtein_Meu, Levenshtein_Inn
      ], [QrINN_0200LinArq.Value], False);
      //
      QrINN_0200.Next;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando exist�ncia de cadastro de produtos.');
    UMyMod.ReabreQuery(QrErr0200, Dmod.MyDB, [FImporExpor, FAnoMes, Empresa], Tit);
    if QrErr0200.RecordCount > 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Atrele os produtos caso existam, ou cadastre-os antes de continuar!');
      PageControl1.ActivePageIndex := 2;
      PageControl2.ActivePageIndex := 2;
      //
      Geral.MensagemBox('Carregamento cancelado!' + sLineBreak +
      'Existem ' + FormatFloat('0', QrErr0200.RecordCount) +
      ' produtos sem cadastro no aplicativo!' + sLineBreak +
      'Atrele os produtos caso existam, ou cadastre antes de continuar!',
      'Aviso', MB_OK+MB_ICONWARNING);
      //
      Exit;
    end;
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando itens de entrada (C170).');
    PB1.Position := 0;
    PB1.Max := QrC170.RecordCount;
    QrC170.First;
    while not QrC170.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando itens de entrada (C170). ' +
        QrC170COD_ITEM.Value);
      //
      GraGruX := 0;
      if QrC170COD_ITEM.Value <> '' then
      begin
        QrLoc0200.Close;
        QrLoc0200.Params[00].AsInteger := FImporExpor;
        QrLoc0200.Params[01].AsInteger := FAnoMes;
        QrLoc0200.Params[02].AsInteger := Empresa;
        QrLoc0200.Params[03].AsString  := QrC170COD_ITEM.Value;
        QrLoc0200.Open;
        //
        if QrLoc0200.RecordCount > 0 then
          GraGruX := QrLoc0200GraGruX.Value;
      end;
      //
      if GraGruX <> 0 then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'spedefdC170', False, [
        'GraGruX'], [
        'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
        GraGruX], [
        QrC170ImporExpor.Value, QrC170AnoMes.Value,
        QrC170Empresa.Value, QrC170LinArq.Value], False);
      end;
      //
      QrC170.Next;
    end;
    UMyMod.ReabreQuery(QrErrC170, Dmod.MyDB, [FImporExpor, FAnoMes, Empresa], Tit);
    if QrErrC170.RecordCount > 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Atrele os produtos caso existam, ou cadastre-os antes de continuar!');
      PageControl1.ActivePageIndex := 2;
      PageControl2.ActivePageIndex := 3;
      //
      Geral.MensagemBox('Carregamento cancelado!' + sLineBreak +
      'Existem ' + FormatFloat('0', QrErrC170.RecordCount) +
      ' produtos sem cadastro no aplicativo!' + sLineBreak +
      'Atrele os produtos caso existam, ou cadastre antes de continuar!',
      'Aviso', MB_OK+MB_ICONWARNING);
      //
      Exit;
    end;
    QrAdver_0200.Close;
    QrAdver_0200.Open;
    if QrAdver_0200.RecordCount > 0 then
      Geral.MensagemBox('Carregamento finalizado com advert�ncias!' + sLineBreak +
      'Existem ' + FormatFloat('0', QrAdver_0200.RecordCount) +
      ' produtos com nomes distantes!',
      'Aviso', MB_OK+MB_ICONWARNING);
        //
    PB1.Position := 0;
    //
    if ImportaEstoque() then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
    //
  finally
    if LaAviso1.Caption <> '' then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'CARREGAMENTO PARALISADO EM: ' + LaAviso1.Caption)
    else
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Carregamento finalizado!');
    //
    Screen.Cursor := crDefault;
    if Lista <> nil then
      Lista.Free;
  end;
*)
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUnSPED_PF.ExcluiPeriodoSelecionado(Empresa, FAnoMes, FImporExpor: Integer): Boolean;
var
  EmpTXT, IE_TXT, AM_TXT: String;
  I: Integer;
begin
  Result := True;
  Exit;
(*
  Result := False;
  EmpTXT := FormatFloat('0', Empresa);
  AM_TXT := FormatFloat('0', FAnoMes);
  IE_TXT := FormatFloat('0', FImporExpor);
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT REG');
  Dmod.QrAux.SQL.Add('FROM spedefd0000');
  Dmod.QrAux.SQL.Add('WHERE ImporExpor=' + IE_TXT);
  Dmod.QrAux.SQL.Add('AND Empresa=' + EmpTXT);
  Dmod.QrAux.SQL.Add('AND AnoMes=' + AM_TXT);
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('UNION');
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('SELECT REG');
  Dmod.QrAux.SQL.Add('FROM spedefd0001');
  Dmod.QrAux.SQL.Add('WHERE ImporExpor=' + IE_TXT);
  Dmod.QrAux.SQL.Add('AND Empresa=' + EmpTXT);
  Dmod.QrAux.SQL.Add('AND AnoMes=' + AM_TXT);
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('UNION');
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('SELECT REG');
  Dmod.QrAux.SQL.Add('FROM spedefdc100');
  Dmod.QrAux.SQL.Add('WHERE ImporExpor=' + IE_TXT);
  Dmod.QrAux.SQL.Add('AND Empresa=' + EmpTXT);
  Dmod.QrAux.SQL.Add('AND AnoMes=' + AM_TXT);
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.Open;
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SHOW TABLES');
    Dmod.QrAux.SQL.Add('FROM bluederm');
    Dmod.QrAux.SQL.Add('LIKE "spedefd%"');
    Dmod.QrAux.SQL.Add('');
    Dmod.QrAux.Open;
    //
    if Dmod.QrAux.RecordCount - FNot_SPED_EFD <> FQtd_SPED_EFD then
    begin
      Geral.MensagemBox(
      'Importa��o cancelada! Quantidade de tabelas desatualizadas!' + sLineBreak +
      'AVISE A DERMATEK!', 'Aviso', MB_OK+MB_ICONWARNING);
    end else begin
      if Geral.MensagemBox(
      'J� existem dados para este per�odo!' + sLineBreak +
      'Os dados j� existentes ser�o exclu�dos! Deseja continuar assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Screen.Cursor := crHourGlass;
        try
          // Por preven��o excluir do "0" mesmo que n�o use!
          for I := 0 to High(FTbs_SPED_EFD)  do // -1
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('DELETE FROM ' + FTbs_SPED_EFD[I]);
            Dmod.QrUpd.SQL.Add('WHERE ImporExpor=' + IE_TXT);
            Dmod.QrUpd.SQL.Add('AND Empresa=' + EmpTXT);
            Dmod.QrUpd.SQL.Add('AND AnoMes=' + AM_TXT);
            Dmod.QrUpd.ExecSQL;
          end;
          Result := True;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
    // Evitar exclus�o errada!
    if (GRADE_TABS_PARTIPO_0004 <> 0) and (FAnoMes <> 0) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo dados duplic�veis no estoque');
      QrRecupera.Close;
      QrRecupera.Params[00].AsInteger := VAR_FATID_0251;
      QrRecupera.Params[01].AsInteger := GRADE_TABS_PARTIPO_0004;
      QrRecupera.Params[02].AsInteger := FAnoMes;
      QrRecupera.Open;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0');
      Dmod.QrUpd.SQL.Add('AND Empresa=:P1');
      Dmod.QrUpd.SQL.Add('AND partipo=:P2');
      Dmod.QrUpd.SQL.Add('AND parcodi=:P3');
      Dmod.QrUpd.Params[00].AsInteger := VAR_FATID_0251;
      Dmod.QrUpd.Params[01].AsInteger := Empresa;
      Dmod.QrUpd.Params[02].AsInteger := GRADE_TABS_PARTIPO_0004;
      Dmod.QrUpd.Params[03].AsInteger := FAnoMes;
      Dmod.QrUpd.ExecSQL;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo Notas Ficais existentes');
      QrNFsA.Close;
{
SELECT FatID, FatNum, Empresa
FROM nfecaba
WHERE FatID=:P0
AND EFD_INN_AnoMes=:P1
AND EFD_INN_Empresa=:P2
}
      QrNFsA.Params[00].AsInteger := VAR_FATID_0251;
      QrNFsA.Params[01].AsInteger := FAnoMes;
      QrNFsA.Params[02].AsInteger := Empresa;
      QrNFsA.Open;
      PB1.Position := 0;
      PB1.Max := QrNFsA.RecordCount;
      while not QrNFsA.Eof do
      begin
        PB1.Position := PB1.Position + 1;
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo Nota Fiscal - ID = ' +
          FormatFloat('000000000', QrNFsAIDCtrl.Value));
        DmNFe_0000.ExcluiNfe(0, QrNFsAFatID.Value, QrNFsAFatNum.Value,
          QrNFsAEmpresa.Value, True, True);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'livres', False, [
        'Codigo', 'Tabela'], [], [
        QrNFsAIDCtrl.Value, 'nfecaba'], [
        ], False);
        //
        QrNFsA.Next;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Recuperando c�digos "StqMovItsA.IDCtrl"');
      PB1.Position := 0;
      PB1.Max := QrRecupera.RecordCount;
      QrRecupera.First;
      while not QrRecupera.Eof do
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'Recuperando c�digos "StqMovItsA.IDCtrl" ' + FormatFloat('0',
          QrRecuperaIDCtrl.Value));
        PB1.Position := PB1.Position + 1;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'livres', False, [
        'Codigo', 'Tabela'], [], [
        QrRecuperaIDCtrl.Value, 'stqmovitsa'], [
        ], False);
        //
        QrRecupera.Next;
      end;
    end;
  end else
    Result := True;
*)
end;

function TUnSPED_PF.FaltaInformarCST(EdCST_ICMS, EdCST_IPI, EdCST_PIS,
  EdCST_COFINS: TdmkEdit): Boolean;
var
  Msg: String;
  dmkEd: TdmkEdit;
begin
  if EdCST_ICMS.Text = EmptyStr then
  begin
    Msg := 'CST do ICMS' + sLineBreak;
  end;
end;

function TUnSPED_PF.FormulaVL_OPR(prod_vProd, prod_vFrete, prod_vSeg,
  prod_vOutro, prod_vDesc, prod_vIPI: Double): Double;
begin
  Result :=
    prod_vProd
    + prod_vFrete
    + prod_vSeg
    + prod_vOutro
    - prod_vDesc
    + prod_vIPI;
end;

function TUnSPED_PF.FormulaVL_OUTROS(_VL_OPR, ICMS_vBC, ICMS_vICMSST,
  _VL_RED_BC, IPI_vIPI: Double): Double;
begin
(*
A f�rmula VL_OPR � VL_BC_ICMS � VL_ICMS_ST � VL_RED_BC � VL_IPI , � usada automaticamente pelo estado (SP) com base no C190
*)
  if (ICMS_vBC = _VL_OPR) and (IPI_vIPI > 0) then
    Result := _VL_OPR - ICMS_vBC - ICMS_vICMSST -_VL_RED_BC (*- IPI_vIPI*)
  else
    Result := _VL_OPR - ICMS_vBC - ICMS_vICMSST -_VL_RED_BC - IPI_vIPI;
end;

function TUnSPED_PF.FormulaVL_RED_BC(VL_OPR, VL_BC, pRedBC: Double): Double;
var
  Fator, Total: Double;
begin
  if pRedBC = 0.00 then
    Result := 0.00
  else
  if pRedBC >= 100 then
     Result := VL_OPR
  else
  begin
    Fator := (100-pRedBC) / 100;
    Total := VL_BC / Fator;
    Result := Total - VL_BC;
  end;
end;

function TUnSPED_PF.ImpedePeloID_da_NFe(Tabela, CampoId_NFe, Id_NFe, SQLIdx: String): Boolean;
begin
  if Id_NFe = EmptyStr then
    Result := False
  else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmSPED.QrAux, Dmod.MyDB, [
    'SELECT ' + CampoId_NFe + ' Id ',
    'FROM ' + Lowercase(Tabela),
    'WHERE ' + CampoId_NFe + '="' + Id_NFe + '" ',
    SQLIdx,
    ' ']);
    Result := DmSPED.QrAux.RecordCount > 0;
  end;
  if Result = true then
    Geral.MB_Aviso('A NFe ' + Id_NFe + ' j� foi inclu�da e n�o pode ser duplicada!');
end;

function TUnSPED_PF.InsAltEfdInnNFsCab(SQLType: TSQLType; MovFatID, MovFatNum,
  MovimCod, Empresa, _Controle, IsLinked, SqLinked, Terceiro, CliInt, TpEntrd,
  RegrFiscal: Integer; Pecas,
  PesoKg, AreaM2, AreaP2, ValorT: Double; Motorista: Integer; Placa: String;
  COD_MOD, COD_SIT, SER, NUM_DOC: Integer; CHV_NFE: String; NFeStatus: Integer;
  DT_DOC, DT_E_S: String; VL_DOC: Double; IND_PGTO: String; VL_DESC, VL_ABAT_NT,
  VL_MERC: Double; IND_FRT: String; VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST: Double; NFe_FatID, NFe_FatNum, NFe_StaLnk: Integer;
  VSVmcWrn: Integer; VSVmcObs, VSVmcSeq: String; VSVmcSta: Integer;
  LnkTabName, LnkFldName: String): Boolean;
var
  Controle: Integer;
begin
  if SQLType = stIns then
  begin
    if NfeJaAtrelada(NFe_FatID, NFe_FatNum, Empresa) then
      Exit;
  end else
  begin
    //
  end;
  Controle := UMyMod.BPGS1I32('efdinnnfscab', 'Controle', '', '', tsPos, SQLType, _Controle);
  SqLinked := UMyMod.BPGS1I32('efdinnnfslsq', 'Codigo', '', '', tsPos, SQLType, SqLinked);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdinnnfscab', False, [
  'MovFatID', 'MovFatNum', 'MovimCod',
  'Empresa', 'Terceiro', 'CliInt',
  'TpEntrd', 'RegrFiscal', 'Pecas',
  'PesoKg', 'AreaM2', 'AreaP2',
  'ValorT', 'Motorista', 'Placa',
  'COD_MOD', 'COD_SIT', 'SER',
  'NUM_DOC', 'CHV_NFE', 'NFeStatus',
  'DT_DOC', 'DT_E_S', 'VL_DOC',
  'IND_PGTO', 'VL_DESC', 'VL_ABAT_NT',
  'VL_MERC', 'IND_FRT', 'VL_FRT',
  'VL_SEG', 'VL_OUT_DA', 'VL_BC_ICMS',
  'VL_ICMS', 'VL_BC_ICMS_ST', 'VL_ICMS_ST',
  'VL_IPI', 'VL_PIS', 'VL_COFINS',
  'VL_PIS_ST', 'VL_COFINS_ST', 'NFe_FatID',
  'NFe_FatNum', 'NFe_StaLnk', 'VSVmcWrn',
  'VSVmcObs', 'VSVmcSeq', 'VSVmcSta',
  'IsLinked', 'SqLinked'], [
  'Controle'], [
  MovFatID, MovFatNum, MovimCod,
  Empresa, Terceiro, CliInt,
  TpEntrd, RegrFiscal, Pecas,
  PesoKg, AreaM2, AreaP2,
  ValorT, Motorista, Placa,
  COD_MOD, COD_SIT, SER,
  NUM_DOC, CHV_NFE, NFeStatus,
  DT_DOC, DT_E_S, VL_DOC,
  IND_PGTO, VL_DESC, VL_ABAT_NT,
  VL_MERC, IND_FRT, VL_FRT,
  VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST,
  VL_IPI, VL_PIS, VL_COFINS,
  VL_PIS_ST, VL_COFINS_ST, NFe_FatID,
  NFe_FatNum, NFe_StaLnk, VSVmcWrn,
  VSVmcObs, VSVmcSeq, VSVmcSta,
  IsLinked, SqLinked], [
  Controle], True) then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Lowercase(LnkTabName), False, [
    'SqLinked'], [LnkFldName], [SqLinked], [MovFatNum], True);
  end;
end;

function TUnSPED_PF.InsAltEfdInnNFsIts(SQLType: TSQLType; MovFatID, MovFatNum,
  MovimCod, Empresa, MovimNiv, MovimTwn, Controle, Conta, SqLinked, GraGru1,
  GraGruX: Integer; prod_vProd, prod_vFrete, prod_vSeg, prod_vOutro,
  QTD: Double; UNID: String; VL_ITEM, VL_DESC: Double; IND_MOV, CST_ICMS: String;
  CFOP: Integer; COD_NAT: String; VL_BC_ICMS, ALIQ_ICMS, VL_ICMS,
  VL_BC_ICMS_ST, ALIQ_ST, VL_ICMS_ST: Double; IND_APUR, CST_IPI:
  String; COD_ENQ: String; VL_BC_IPI, ALIQ_IPI, VL_IPI: Double; CST_PIS:
  String; VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS, ALIQ_PIS_r, VL_PIS: Double; CST_COFINS:
  String; VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS, ALIQ_COFINS_r,
  VL_COFINS: Double; COD_CTA: String; VL_ABAT_NT: Double; DtCorrApo, xLote:
  String; Ori_IPIpIPI, Ori_IPIvIPI: Double; LnkTabName, LnkFldName: String;
  SqLnkID, EFD_II_C195: Integer; AjusteVL_BC_ICMS, AjusteALIQ_ICMS,
  AjusteVL_ICMS, AjusteVL_OUTROS, AjusteVL_OPR, AjusteVL_RED_BC: Double): Boolean;
begin
  Result := False;
  if SQLType = stUpd then
  begin
    // ?
  end;
  Conta := UMyMod.BPGS1I32('efdinnnfsits', 'Conta', '', '', tsPos, SQLType, Conta);
  SqLinked := UMyMod.BPGS1I32('efdinnnfslsq', 'Codigo', '', '', tsPos, SQLType, SqLinked);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdinnnfsits', False, [
  'MovFatID', 'MovFatNum', 'MovimCod',
  'Empresa', 'MovimNiv', 'MovimTwn',
  'Controle', 'SqLinked', 'SqLnkID', 'GraGru1', 'GraGruX',
  'prod_vProd', 'prod_vFrete', 'prod_vSeg',
  'prod_vOutro', 'QTD', 'UNID',
  'VL_ITEM', 'VL_DESC', 'IND_MOV',
  'CST_ICMS', 'CFOP', 'COD_NAT',
  'VL_BC_ICMS', 'ALIQ_ICMS', 'VL_ICMS',
  'VL_BC_ICMS_ST', 'ALIQ_ST', 'VL_ICMS_ST',
  'IND_APUR', 'CST_IPI', 'COD_ENQ',
  'VL_BC_IPI', 'ALIQ_IPI', 'VL_IPI',
  'CST_PIS', 'VL_BC_PIS', 'ALIQ_PIS_p',
  'QUANT_BC_PIS', 'ALIQ_PIS_r', 'VL_PIS',
  'CST_COFINS', 'VL_BC_COFINS', 'ALIQ_COFINS_p',
  'QUANT_BC_COFINS', 'ALIQ_COFINS_r', 'VL_COFINS',
  'COD_CTA', 'VL_ABAT_NT', 'DtCorrApo',
  'xLote', 'Ori_IPIpIPI', 'Ori_IPIvIPI',
  'EFD_II_C195', 'AjusteVL_BC_ICMS',
  'AjusteALIQ_ICMS', 'AjusteVL_ICMS', 'AjusteVL_OUTROS',
  'AjusteVL_OPR', 'AjusteVL_RED_BC'], [
  'Conta'], [
  MovFatID, MovFatNum, MovimCod,
  Empresa, MovimNiv, MovimTwn,
  Controle, SqLinked, SqLnkID, GraGru1, GraGruX,
  prod_vProd, prod_vFrete, prod_vSeg,
  prod_vOutro, QTD, UNID,
  VL_ITEM, VL_DESC, IND_MOV,
  CST_ICMS, CFOP, COD_NAT,
  VL_BC_ICMS, ALIQ_ICMS, VL_ICMS,
  VL_BC_ICMS_ST, ALIQ_ST, VL_ICMS_ST,
  IND_APUR, CST_IPI, COD_ENQ,
  VL_BC_IPI, ALIQ_IPI, VL_IPI,
  CST_PIS, VL_BC_PIS, ALIQ_PIS_p,
  QUANT_BC_PIS, ALIQ_PIS_r, VL_PIS,
  CST_COFINS, VL_BC_COFINS, ALIQ_COFINS_p,
  QUANT_BC_COFINS, ALIQ_COFINS_r, VL_COFINS,
  COD_CTA, VL_ABAT_NT, DtCorrApo,
  xLote, Ori_IPIpIPI, Ori_IPIvIPI,
  EFD_II_C195, AjusteVL_BC_ICMS,
  AjusteALIQ_ICMS, AjusteVL_ICMS, AjusteVL_OUTROS,
  AjusteVL_OPR, AjusteVL_RED_BC], [
  Conta], True) then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Lowercase(LnkTabName), False, [
    'SqLinked'], [LnkFldName], [SqLinked], [SqLnkID], True);
  end;
end;

function TUnSPED_PF.MontaCST_ICMS_Inn_de_CST_ICMS_emi(ICMS_Orig: Integer;
  CST_emi: String): String;
begin
  //corrigir primeiro d�gito do CST do ICMS!
  Result := Geral.FF0(ICMS_Orig) +
            ObtemCST_B_de_CST_ICMS(CST_emi);
end;

function TUnSPED_PF.NFeJaAtrelada(NFe_FatID, NFe_FatNum, Empresa: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT *  ',
    'FROM efdinnnfscab ',
    'WHERE NFe_FatID=' + Geral.FF0(NFe_FatID),
    'AND NFe_FatNum=' + Geral.FF0(NFe_FatNum),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
    Result := Qry.RecordCount > 0;
    if Result then
      Geral.MB_Erro('Nota fiscal j� atrelada a outro lan�amento!' + sLineBreak +
      'ID Movimento: ' + Geral.FF0(Qry.FieldByName('MovFatID').AsInteger) +
      sLineBreak +
      'C�digo: ' + Geral.FF0(Qry.FieldByName('MovFatNum').AsInteger) +
      sLineBreak);
  finally
    Qry.Free;
  end;
end;

function TUnSPED_PF.ObtemCST_B_de_CST_ICMS(CST: String): String;
const
  sProcName = 'TUnSPED_PF.ObtemCST_B_de_CST_ICMS()';
var
  C: String;
begin
  C := CST;
  while Length(C) < 3 do
    C := '0' + C;
  while Length(C) >3 do
    C := Copy(C, 2);
  Result := Copy(C, 2);
  //
  if Length(Result) <> 2 then
    Geral.MB_Erro('N�o foi poss�vel definir o CST B do ICMS em ' + sProcName);
end;

procedure TUnSPED_PF.ReopenNFeItens_EFD(Qry: TmySQLQuery; FatID, FatNum,
  Empresa: Integer);
begin
  // CUIDADO!!!! Usa em v�rios lugares distintos
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT pgt.IND_MOV, itsi.*,  ',
  'itsn.ICMS_Orig, itsn.ICMS_CST, itsn.ICMS_vBC, ',
  'itsn.ICMS_pICMS, itsn.ICMS_vICMS, itsn.ICMS_vBCST, ',
  'itsn.ICMS_pICMSST, itsn.ICMS_vICMSST,  ',
  ' ',
  'itso.IPI_cEnq, itso.IND_APUR, itso.IPI_CST,  ',
  'itso.IPI_vBC, itso.IPI_pIPI, itso.IPI_vIPI, ',
  ' ',
  'itsq.PIS_CST, itsq.PIS_vBC,  ',
  'itsq.PIS_pPIS, itsq.PIS_vPIS, ',
  ' ',
  'itsr.PISST_vBC,  ',
  'itsr.PISST_pPIS, itsr.PISST_vPIS, ',
  ' ',
  'itss.COFINS_CST, itss.COFINS_vBC,  ',
  'itss.COFINS_pCOFINS, itss.COFINS_vCOFINS, ',
  ' ',
  'itst.COFINSST_vBC,  ',
  'itst.COFINSST_pCOFINS, itst.COFINSST_vCOFINS ',
  ' ',
  ' ',
  'FROM nfeitsi itsi ',
  ' ',
  'LEFT JOIN nfeitsn itsn ON  ',
  '  itsn.FatID=itsi.FatID ',
  '  AND itsn.FatNum=itsi.FatNum ',
  '  AND itsn.Empresa=itsi.Empresa ',
  '  AND itsn.nItem=itsi.nItem ',
  ' ',
  'LEFT JOIN nfeitso itso ON ',
  '  itso.FatID=itsi.FatID ',
  '  AND itso.FatNum=itsi.FatNum ',
  '  AND itso.Empresa=itsi.Empresa ',
  '  AND itso.nItem=itsi.nItem ',
  ' ',
  'LEFT JOIN nfeitsq itsq ON ',
  '  itsq.FatID=itsi.FatID ',
  '  AND itsq.FatNum=itsi.FatNum ',
  '  AND itsq.Empresa=itsi.Empresa ',
  '  AND itsq.nItem=itsi.nItem ',
  ' ',
  'LEFT JOIN nfeitsr itsr ON ',
  '  itsr.FatID=itsi.FatID ',
  '  AND itsr.FatNum=itsi.FatNum ',
  '  AND itsr.Empresa=itsi.Empresa ',
  '  AND itsr.nItem=itsi.nItem ',
  ' ',
  'LEFT JOIN nfeitss itss ON ',
  '  itss.FatID=itsi.FatID ',
  '  AND itss.FatNum=itsi.FatNum ',
  '  AND itss.Empresa=itsi.Empresa ',
  '  AND itss.nItem=itsi.nItem ',
  ' ',
  'LEFT JOIN nfeitst itst ON ',
  '  itst.FatID=itsi.FatID ',
  '  AND itst.FatNum=itsi.FatNum ',
  '  AND itst.Empresa=itsi.Empresa ',
  '  AND itst.nItem=itsi.nItem ',
  ' ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=itsi.prod_cProd',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
  'WHERE itsi.FatID=' + Geral.FF0(FatID),
  'AND itsi.FatNum=' + Geral.FF0(FatNum),
  'AND itsi.Empresa=' + Geral.FF0(Empresa),
  '',
  'ORDER BY itsi.nItem ',
  '']);
  //Geral.MB_Teste(Qry.SQL.Text);
end;

function TUnSPED_PF.ValidaPeriodoInteiroDeUmMes(const DtI,
  DtF: TDateTime): Boolean;
var
  //AnoIa, MesIa, DiaIa,
  AnoIi, MesIi, DiaIi,
  AnoFf, AnoFd, MesFf, MesFd, DiaFf, DiaFd: Word;
  AnoMes: Integer;
begin
  //FAnoMes := 0;
  AnoMes := 0;
  Result := False;
  DecodeDate(DtI, AnoIi, MesIi, DiaIi);
  //DecodeDate(DtI-1, AnoIi, MesIi, DiaIi);
  DecodeDate(DtF, AnoFf, MesFf, DiaFf);
  DecodeDate(DtF + 1, AnoFd, MesFd, DiaFd);
  //FAnoMes := AnoIi * 100 + MesIi;
  AnoMes := AnoIi * 100 + MesIi;
  //
  if DiaIi <> 1 then
    Geral.MensagemBox(
    'Carregamento cancelado! Dia inicial deve ser o primeiro dia (dia 1) do m�s,'
    + sLineBreak +
    'mesmo que as atividades tenham se iniciado ap�s o dia primeiro!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else if DiaFd <> 1 then
    Geral.MensagemBox(
    'Carregamento cancelado! Dia final deve ser o �ltimo dia do m�s,'
    + sLineBreak +
    'mesmo que as atividades tenham sido finalizados antes do �ltimo dia do m�s!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else if MesIi <> MesFf then
    Geral.MensagemBox(
    'Carregamento cancelado! O per�odo deve ser de um �nico m�s!' + sLineBreak +
    'Data inicial selecionada:  ' + Geral.FDT(DtI, 3) + sLineBreak +
    'Data Final selecionada:   ' + Geral.FDT(DtF, 3),
    'Aviso', MB_OK+MB_ICONWARNING)
  //else if FAnoMes < 200001 then
  else if AnoMes < 200001 then
    Geral.MensagemBox(
    'Carregamento cancelado! O per�odo deve ser de um ano ap�s 2000!' + sLineBreak +
    'Data inicial selecionada:  ' + Geral.FDT(DtI, 3) + sLineBreak +
    'Data Final selecionada:   ' + Geral.FDT(DtF, 3),
    'Aviso', MB_OK+MB_ICONWARNING)
  else
    Result := True;
end;

end.
