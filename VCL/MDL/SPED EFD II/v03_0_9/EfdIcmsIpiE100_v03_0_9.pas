unit EfdIcmsIpiE100_v03_0_9;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmEfdIcmsIpiE100_v03_0_9 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    TPDT_INI: TdmkEditDateTimePicker;
    Label1: TLabel;
    TPDT_FIN: TdmkEditDateTimePicker;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdLinArq: TdmkEdit;
    Label6: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEfdIcmsIpiE100_v03_0_9: TFmEfdIcmsIpiE100_v03_0_9;

implementation

uses UnMyObjects, Module, EfdIcmsIpiE001_v03_0_9, UMySQLModule,
  SpedEfdIcmsIpi_v03_0_2_a;

{$R *.DFM}

procedure TFmEfdIcmsIpiE100_v03_0_9.BtOKClick(Sender: TObject);
const
  REG = 'E100';
var
  DT_INI, DT_FIN: String;
  ImporExpor, AnoMes, Empresa, LinArq: Integer;
begin
  DT_INI := Geral.FDT(TPDT_INI.Date, 1);
  DT_FIN := Geral.FDT(TPDT_FIN.Date, 1);
  //
  ImporExpor := EdImporExpor.ValueVariant;
  AnoMes := EdAnoMes.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  //
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipie100', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  ImgTipo.SQLType, 0, siPositivo, EdLinArq);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'efdicmsipie100', False, [
  'REG', 'DT_INI', 'DT_FIN'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  REG, DT_INI, DT_FIN], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
    DfSEII_v03_0_2_a.ReopenE100(LinArq);
    Close;
  end;
end;

procedure TFmEfdIcmsIpiE100_v03_0_9.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpiE100_v03_0_9.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiE100_v03_0_9.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
