unit EfdInnC500Cab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, UnDmkEnums, UnDmkProcFunc, SPED_Listas;

type
  TFmEfdInnC500Cab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    BtOK: TBitBtn;
    Panel6: TPanel;
    QrTerceiros: TmySQLQuery;
    DsTerceiros: TDataSource;
    QrTerceirosCodigo: TIntegerField;
    QrTerceirosNO_ENT: TWideStringField;
    Panel7: TPanel;
    QrTbSPEDEFD017: TmySQLQuery;
    QrTbSPEDEFD017CodTxt: TWideStringField;
    QrTbSPEDEFD017Nome: TWideStringField;
    DsTbSPEDEFD017: TDataSource;
    GroupBox3: TGroupBox;
    EdCOD_MOD: TdmkEditCB;
    CBCOD_MOD: TdmkDBLookupComboBox;
    QrTbSPEDEFD018: TmySQLQuery;
    DsTbSPEDEFD018: TDataSource;
    QrTbSPEDEFD018CodTxt: TWideStringField;
    QrTbSPEDEFD018Nome: TWideStringField;
    QrTbSPEDEFD028: TmySQLQuery;
    QrTbSPEDEFD028CodTxt: TWideStringField;
    QrTbSPEDEFD028Nome: TWideStringField;
    QrCST_ICMS: TmySQLQuery;
    QrCST_ICMSCodTxt: TWideStringField;
    QrCST_ICMSNome: TWideStringField;
    DsCST_ICMS: TDataSource;
    QrTbSPEDEFD002: TmySQLQuery;
    QrTbSPEDEFD002CodTxt: TWideStringField;
    QrTbSPEDEFD002Nome: TWideStringField;
    DsTbSPEDEFD002: TDataSource;
    DsTbSPEDEFD028: TDataSource;
    GroupBox4: TGroupBox;
    EdCOD_SIT: TdmkEditCB;
    CBCOD_SIT: TdmkDBLookupComboBox;
    Panel8: TPanel;
    Label2: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdSER: TdmkEdit;
    EdSUB: TdmkEdit;
    EdNUM_DOC: TdmkEdit;
    TPDT_DOC: TdmkEditDateTimePicker;
    TPDT_E_S: TdmkEditDateTimePicker;
    EdCOD_CONS: TdmkEdit;
    EdCOD_CONS_TXT: TdmkEdit;
    QrMuniciDest: TMySQLQuery;
    QrMuniciDestCodigo: TIntegerField;
    QrMuniciDestNome: TWideStringField;
    DsMuniciDest: TDataSource;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel10: TPanel;
    Panel11: TPanel;
    RGTP_LIGACAO: TdmkRadioGroup;
    RGCOD_GRUPO_TENSAO: TdmkRadioGroup;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    EdVL_DOC: TdmkEdit;
    EdVL_DESC: TdmkEdit;
    EdVL_FORN: TdmkEdit;
    EdVL_SERV_NT: TdmkEdit;
    EdVL_TERC: TdmkEdit;
    EdVL_DA: TdmkEdit;
    EdVL_BC_ICMS: TdmkEdit;
    EdVL_ICMS: TdmkEdit;
    EdVL_BC_ICMS_ST: TdmkEdit;
    EdVL_ICMS_ST: TdmkEdit;
    EdVL_PIS: TdmkEdit;
    EdVL_COFINS: TdmkEdit;
    Panel12: TPanel;
    LaCHV_DOCe: TLabel;
    Label28: TLabel;
    LaCHV_DOCe_REF: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    EdCHV_DOCe: TdmkEdit;
    EdFIN_DOCe: TdmkEdit;
    EdFIN_DOCe_TXT: TdmkEdit;
    EdCHV_DOCe_REF: TdmkEdit;
    EdIND_DEST: TdmkEdit;
    EdIND_DEST_TXT: TdmkEdit;
    EdCOD_MUN_DEST: TdmkEditCB;
    CBCOD_MUN_DEST: TdmkDBLookupComboBox;
    EdCOD_CTA: TdmkEdit;
    EdHASH_DOC_REF: TdmkEdit;
    EdCOD_MOD_DOC_REF: TdmkEdit;
    EdSER_DOC_REF: TdmkEdit;
    EdNUM_DOC_REF: TdmkEdit;
    EdMES_DOC_REF: TdmkEdit;
    Panel13: TPanel;
    CBTerceiro: TdmkDBLookupComboBox;
    EdTerceiro: TdmkEditCB;
    Label1: TLabel;
    EdControle: TdmkEdit;
    Label37: TLabel;
    Label6: TLabel;
    Label24: TLabel;
    EdENER_INJET: TdmkEdit;
    EdOUTRAS_DED: TdmkEdit;
    RGIND_OPER: TdmkRadioGroup;
    RGIND_EMIT: TdmkRadioGroup;
    EdAnoMes: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdEmpresa: TdmkEdit;
    EdCodigo: TdmkEdit;
    Label3: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCOD_CONSRedefinido(Sender: TObject);
    procedure EdFIN_DOCeChange(Sender: TObject);
    procedure EdIND_DESTChange(Sender: TObject);
    procedure EdCOD_MODChange(Sender: TObject);
    procedure EdVL_FORNChange(Sender: TObject);
    procedure EdVL_SERV_NTChange(Sender: TObject);
    procedure EdVL_TERCChange(Sender: TObject);
    procedure EdVL_DAChange(Sender: TObject);
    procedure EdVL_DESCChange(Sender: TObject);
    procedure EdENER_INJETChange(Sender: TObject);
    procedure EdOUTRAS_DEDChange(Sender: TObject);
  private
    { Private declarations }
    function COD_MOD_Permitido(): Boolean;
    function COD_SIT_Permitido(): Boolean;
    procedure CalculaVL_DOC();

  public
    { Public declarations }
    FAnoMes, FControle: Integer;
    //
    procedure ReopenTabelas();
  end;

  var
  FmEfdInnC500Cab: TFmEfdInnC500Cab;

implementation

uses UnMyObjects, Module, EfdInnC001Ger, UMySQLModule, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmEfdInnC500Cab.BtOKClick(Sender: TObject);
const
  Importado = 0;
  //
  COD_INF = '';
var
  AnoMes, Empresa: Integer;
  //
  IND_OPER, IND_EMIT, COD_MOD, COD_SIT, SER, SUB, COD_CONS, NUM_DOC,
  DT_DOC, DT_E_S: String;
  VL_DOC, VL_DESC, VL_FORN, VL_SERV_NT, VL_TERC, VL_DA, VL_BC_ICMS, VL_ICMS,
  VL_BC_ICMS_ST, VL_ICMS_ST,
  //COD_INF: String;
  VL_PIS, VL_COFINS: Double;
  TP_LIGACAO, COD_GRUPO_TENSAO: String;
  Terceiro: Integer;
var
  CHV_DOCe, CHV_DOCe_REF, COD_CTA, HASH_DOC_REF, SER_DOC_REF: String;
  FIN_DOCe, IND_DEST, COD_MUN_DEST, COD_MOD_DOC_REF, NUM_DOC_REF, MES_DOC_REF: Integer;
  ENER_INJET, OUTRAS_DED: Double;
  Codigo, Controle: Integer;
  SQLType: TSQLType;
begin
  SQLType  := ImgTipo.SQLtype;
  Codigo   := EdCodigo.ValueVariant;
  Controle := EdControle.ValueVariant;
  AnoMes   := EdAnoMes.ValueVariant;
  Empresa  := EdEmpresa.ValueVariant;
  //
  Terceiro := EdTerceiro.ValueVariant;
  if MyObjects.FIC(Terceiro = 0, EdTerceiro,
    'Informe o Fornecedor!') then
    Exit;
  //
  if MyObjects.FIC(RGIND_OPER.ItemIndex < 0, RGIND_OPER,
    'Informe o indicador do tipo de opera��o!') then
    Exit;
  //
  if MyObjects.FIC(RGIND_EMIT.ItemIndex < 0, RGIND_EMIT,
    'Informe o indicador do emitente do documento fiscal!') then
    Exit;
  //
  if not COD_MOD_Permitido() then
    Exit;
  //
  if not COD_SIT_Permitido() then
    Exit;
  //
  if MyObjects.FIC(EdNUM_DOC.ValueVariant < 1, EdNUM_DOC,
    'Informe o n�mero do documento!') then
    Exit;
  //
  if MyObjects.FIC(Int(TPDT_DOC.Date) > Geral.AAAAMM_To_Date(
    EdAnoMes.ValueVariant, qdmLast), TPDT_DOC, 'Data de emiss�o inv�lida!') then
    Exit;
  //
  if MyObjects.FIC(Int(TPDT_E_S.Date) > Geral.AAAAMM_To_Date(
    EdAnoMes.ValueVariant, qdmLast), TPDT_E_S, 'Data de entrada/sa�da inv�lida!') then
    Exit;
  if MyObjects.FIC(Int(TPDT_E_S.Date) < Geral.AAAAMM_To_Date(
    EdAnoMes.ValueVariant, qdmFirst), TPDT_E_S, 'Data de entrada/sa�da inv�lida!') then
    Exit;
  //
  if MyObjects.FIC(EdVL_DOC.ValueVariant < 0.01, EdVL_DOC,
    'Informe o valor total do documento fiscal!') then
    Exit;
  //
  if MyObjects.FIC(EdVL_FORN.ValueVariant < 0.01, EdVL_FORN,
    'Informe o valor total fornecido/consumido!') then
    Exit;
  //
  IND_OPER         := FormatFloat('0', RGIND_OPER.ItemIndex);
  IND_EMIT         := FormatFloat('0', RGIND_EMIT.ItemIndex);
  //COD_PART         := FormatFloat('0', Terceiro);
  COD_MOD          := EdCOD_MOD.Text;
  COD_SIT          := EdCOD_SIT.Text;
  SER              := EdSER.Text;
  SUB              := EdSUB.Text;
  COD_CONS         := EdCOD_CONS.Text;
  NUM_DOC          := EdNUM_DOC.ValueVariant;
  DT_DOC           := Geral.FDT(TPDT_DOC.Date, 1);
  DT_E_S           := Geral.FDT(TPDT_E_S.Date, 1);
  VL_DOC           := EdVL_DOC.ValueVariant;
  VL_DESC          := EdVL_DESC.ValueVariant;
  VL_FORN          := EdVL_FORN.ValueVariant;
  VL_SERV_NT       := EdVL_SERV_NT.ValueVariant;
  VL_TERC          := EdVL_TERC.ValueVariant;
  VL_DA            := EdVL_DA.ValueVariant;
  VL_BC_ICMS       := EdVL_BC_ICMS.ValueVariant;
  VL_ICMS          := EdVL_ICMS.ValueVariant;
  VL_BC_ICMS_ST    := EdVL_BC_ICMS_ST.ValueVariant;
  VL_ICMS_ST       := EdVL_ICMS_ST.ValueVariant;
  VL_PIS           := EdVL_PIS.ValueVariant;
  VL_COFINS        := EdVL_COFINS.ValueVariant;
  TP_LIGACAO       := FormatFloat('0', RGTP_LIGACAO.ItemIndex);
  COD_GRUPO_TENSAO := FormatFloat('00', RGCOD_GRUPO_TENSAO.ItemIndex);
  //COD_INF: String;
  //
  if RGTP_LIGACAO.ItemIndex < 0 then
    TP_LIGACAO := ' ';
  if RGCOD_GRUPO_TENSAO.ItemIndex < 0 then
    COD_GRUPO_TENSAO := '  ';
  //
  CHV_DOCe         := EdCHV_DOCe.ValueVariant;
  CHV_DOCe_REF     := ''; //EdCHV_DOCe_REF.ValueVariant;
  COD_CTA          := ''; //EdCOD_CTA.ValueVariant;
  HASH_DOC_REF     := ''; //EdHASH_DOC_REF.ValueVariant;
  SER_DOC_REF      := ''; //EdSER_DOC_REF.ValueVariant;

  FIN_DOCe         := 0; //EdFIN_DOCe.ValueVariant;
  IND_DEST         := 0; //EdIND_DEST.ValueVariant;
  COD_MUN_DEST     := 0; //EdCOD_MUN_DEST.ValueVariant;
  COD_MOD_DOC_REF  := 0; //EdCOD_MOD_DOC_REF.ValueVariant;
  NUM_DOC_REF      := 0; //EdNUM_DOC_REF.ValueVariant;
  MES_DOC_REF      := 0; //EdMES_DOC_REF.ValueVariant;

  ENER_INJET       := EdENER_INJET.ValueVariant;
  OUTRAS_DED       := EdOUTRAS_DED.ValueVariant;

  //
  Controle := UMyMod.BPGS1I32('efdinnc500cab', 'Controle', '', '', tsPos, SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdinnc500cab', False, [
  'AnoMes', 'Empresa', 'Codigo',
  'IND_OPER', 'IND_EMIT',
  'COD_MOD', 'COD_SIT', 'SER',
  'SUB', 'COD_CONS', 'NUM_DOC',
  'DT_DOC', 'DT_E_S', 'VL_DOC',
  'VL_DESC', 'VL_FORN', 'VL_SERV_NT',
  'VL_TERC', 'VL_DA', 'VL_BC_ICMS',
  'VL_ICMS', 'VL_BC_ICMS_ST', 'VL_ICMS_ST',
  'COD_INF', 'VL_PIS', 'VL_COFINS',
  'TP_LIGACAO', 'COD_GRUPO_TENSAO', 'Terceiro',
  'Importado', 'CHV_DOCe', 'FIN_DOCe',
  'CHV_DOCe_REF', 'IND_DEST', 'COD_MUN_DEST',
  'COD_CTA', 'COD_MOD_DOC_REF', 'HASH_DOC_REF',
  'SER_DOC_REF', 'NUM_DOC_REF', 'MES_DOC_REF',
  'ENER_INJET', 'OUTRAS_DED'], [
  'Controle'], [
  AnoMes, Empresa, Codigo,
  IND_OPER, IND_EMIT,
  COD_MOD, COD_SIT, SER,
  SUB, COD_CONS, NUM_DOC,
  DT_DOC, DT_E_S, VL_DOC,
  VL_DESC, VL_FORN, VL_SERV_NT,
  VL_TERC, VL_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST,
  COD_INF, VL_PIS, VL_COFINS,
  TP_LIGACAO, COD_GRUPO_TENSAO, Terceiro,
  Importado, CHV_DOCe, FIN_DOCe,
  CHV_DOCe_REF, IND_DEST, COD_MUN_DEST,
  COD_CTA, COD_MOD_DOC_REF, HASH_DOC_REF,
  SER_DOC_REF, NUM_DOC_REF, MES_DOC_REF,
  ENER_INJET, OUTRAS_DED], [
  Controle], True) then
  begin
    FControle := Controle;
    FmEfdInnC001Ger.ReopenEfdInnC500Cab(Controle);
    Close;
  end;
end;

procedure TFmEfdInnC500Cab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdInnC500Cab.CalculaVL_DOC();
var
  VL_DOC, VL_DESC, VL_FORN, VL_SERV_NT, VL_TERC, VL_DA, VL_BC_ICMS, ENER_INJET,
  OUTRAS_DED: Double;
begin

  VL_FORN          := EdVL_FORN.ValueVariant;
  VL_DA            := EdVL_DA.ValueVariant;
  VL_SERV_NT       := EdVL_SERV_NT.ValueVariant;
  VL_TERC          := EdVL_TERC.ValueVariant;

  VL_DESC          := EdVL_DESC.ValueVariant;
  ENER_INJET       := EdENER_INJET.ValueVariant;
  OUTRAS_DED       := EdOUTRAS_DED.ValueVariant;


//Campo 13 (VL_DOC) - Valida��o: O valor deste campo deve corresponder ao somat�rio dos campos VL_FORN, VL_DA,
//VL_SERV_NT e VL_TERC subtra�do do somat�rio de VL_DESC, ENER_INJET e OUTRAS_DED.

  VL_DOC           := (VL_FORN + VL_DA + VL_SERV_NT + VL_TERC) -
                      (VL_DESC + ENER_INJET + OUTRAS_DED);

  EdVL_DOC.ValueVariant := VL_DOC;
  //VL_BC_ICMS := ???
  //EdVL_BC_ICMS.ValueVariant := VL_BC_ICMS;
end;

function TFmEfdInnC500Cab.COD_MOD_Permitido(): Boolean;
var
  Modelo: Integer;
begin
  Modelo := Geral.IMV(EdCOD_MOD.Text);
  Result := Modelo in ([06,28,29,55,66]);
  if not Result then
    Geral.MensagemBox('C�digo do Modelo do Documento Fiscal inv�lido!',
    'Aviso', MB_OK+MB_ICONWARNING);
end;

function TFmEfdInnC500Cab.COD_SIT_Permitido(): Boolean;
var
  Sit: Integer;
begin
  Sit := Geral.IMV(EdCOD_SIT.Text);
  Result := (EdCOD_SIT.Text <> '') and (Sit in ([00,01,02,03,06,07,08]));
  if not Result then
    Geral.MensagemBox('C�digo da Situa��o do Documento Fiscal inv�lido!',
    'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmEfdInnC500Cab.EdCOD_CONSRedefinido(Sender: TObject);
var
  Modelo, CodCons: Integer;
  Txt: String;
begin
  Txt := '';
  Modelo := Geral.IMV(EdCOD_MOD.Text);
  CodCons := Geral.IMV(EdCOD_CONS.Text);
  case Modelo of
    06,28,55,66:
    begin
      case CodCons of
        01: Txt := 'Comercial';
        02: Txt := 'Consumo pr�prio';
        03: Txt := 'Ilumina��o P�blica';
        04: Txt := 'Industrial';
        05: Txt := 'Poder P�blico';
        06: Txt := 'Residencial';
        07: Txt := 'Rural';
        08: Txt := 'Servi�o P�blico';
      end;
    end;
    29:
    begin
      if QrTbSPEDEFD028.State <> dsInactive then
        if QrTbSPEDEFD028.Locate('CodTxt', EdCOD_CONS.Text, []) then
          Txt := QrTbSPEDEFD028Nome.Value;
    end;
  end;
  //
  EdCOD_CONS_TXT.Text := Txt;
end;

procedure TFmEfdInnC500Cab.EdCOD_MODChange(Sender: TObject);
var
  Habilita: Boolean;
  COD_MOD: String;
begin
  COD_MOD := EdCOD_MOD.Text;
  Habilita := ( (COD_MOD = '55') or (COD_MOD = '66'));
  //
  EdCHV_DOCe.Enabled := Habilita;
  LaCHV_DOCe.Enabled := Habilita;
  EdCHV_DOCe_REF.Enabled := Habilita;
  LaCHV_DOCe_REF.Enabled := Habilita;
end;

procedure TFmEfdInnC500Cab.EdENER_INJETChange(Sender: TObject);
begin
  CalculaVL_DOC();
end;

procedure TFmEfdInnC500Cab.EdFIN_DOCeChange(Sender: TObject);
var
  FIN_DOCe: Integer;
  FIN_DOCe_TXT: String;
begin
  FIN_DOCe := EdFIN_DOCe.ValueVariant;
  case FIN_DOCe of
    1: FIN_DOCe_TXT := 'Normal';
    2: FIN_DOCe_TXT := 'Substitui��o';
    3: FIN_DOCe_TXT := 'Normal com ajuste';
    else FIN_DOCe_TXT := '?????';
  end;
  EdFIN_DOCe_TXT.ValueVariant := FIN_DOCe_TXT;
end;

procedure TFmEfdInnC500Cab.EdIND_DESTChange(Sender: TObject);
var
  IND_DEST: Integer;
  IND_DEST_TXT: String;
begin
  IND_DEST := EdIND_DEST.ValueVariant;
  case IND_DEST of
    1: IND_DEST_TXT := 'Contribuinte do ICMS';
    2: IND_DEST_TXT := 'Contribuinte Isento de Inscri��o no Cadastro de Contribuintes do ICMS';
    9: IND_DEST_TXT := 'N�o Contribuinte';
    else IND_DEST_TXT := '?????';
  end;
  EdIND_DEST_TXT.ValueVariant := IND_DEST_TXT;
end;

procedure TFmEfdInnC500Cab.EdOUTRAS_DEDChange(Sender: TObject);
begin
  CalculaVL_DOC();
end;

procedure TFmEfdInnC500Cab.EdVL_DAChange(Sender: TObject);
begin
  CalculaVL_DOC();
end;

procedure TFmEfdInnC500Cab.EdVL_DESCChange(Sender: TObject);
begin
  CalculaVL_DOC();
end;

procedure TFmEfdInnC500Cab.EdVL_FORNChange(Sender: TObject);
begin
  CalculaVL_DOC();
end;

procedure TFmEfdInnC500Cab.EdVL_SERV_NTChange(Sender: TObject);
begin
  CalculaVL_DOC();
end;

procedure TFmEfdInnC500Cab.EdVL_TERCChange(Sender: TObject);
begin
  CalculaVL_DOC();
end;

procedure TFmEfdInnC500Cab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdTerceiro.SetFocus;
end;

procedure TFmEfdInnC500Cab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FControle := 0;
  //
  // N�o pode informar!!!
  {
  UnDmkDAC_PF.AbreMySQLQuery0(QrMuniciDest, DModG.AllID_DB, [
  'SELECT mun.Codigo, mun.Nome ',
  'FROM dtb_munici mun ',
  'ORDER BY mun.Nome ',
  '']);
  EdCOD_MUN_DEST.ValueVariant := DModG.QrEmpresasCodMunici.Value;
  }
end;

procedure TFmEfdInnC500Cab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdInnC500Cab.ReopenTabelas();
var
  Ini, Fim, SQL_Periodo_Valido: String;
begin
  Ini := Geral.FDT(DmkPF.DatadeAnoMes(FAnoMes, 1, 0), 1);
  Fim := Geral.FDT(DmkPF.DatadeAnoMes(FAnoMes + 1, 1, -1), 1);
  SQL_Periodo_Valido :=
  'WHERE DataIni <= "' + Ini + '" ' + sLineBreak +
  'AND (DataFim >="' + Fim + '" OR DataFim<2)  ';

  //
  QrTerceiros.Open;
  // Se precisar IND_EMIT est� na tabela 16
  //QrTbSPEDEFD016.Open;
  // COD_MOD > QrTbSPEDEFD017
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD017, DModG.AllID_DB, [
  'SELECT *  ',
  'FROM tbspedcods017 ',
  SQL_Periodo_Valido,
  'AND CodTxt IN ("06", "28", "29", "55", "66") ',  // 55,66 > PVA v.1.03 PIS/COFINS
  'ORDER BY Nome ',
  '']);
  // COD_SIT >  QrTbSPEDEFD018
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD018, DModG.AllID_DB, [
  'SELECT *  ',
  'FROM tbspedcods018 ',
  SQL_Periodo_Valido,
  'ORDER BY Nome ',
  '']);
  // TbSPEDEFD024 x TbSPEDEFD025
  // Mudou para 130
  UnDmkDAC_PF.AbreMySQLQuery0(QrCST_ICMS, DModG.AllID_DB, [
 'SELECT *  ',
 'FROM tbspedcods130 ',
 SQL_Periodo_Valido,
 'ORDER BY Nome ',
 '']);
  // CFOP > QrTbSPEDEFD002
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD002, DModG.AllID_DB, [
  'SELECT *  ',
  'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
  SQL_Periodo_Valido,
  'ORDER BY Nome ',
  '']);
  // Classe de consumo de �gua > QrTbSPEDEFD028
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD028, DModG.AllID_DB, [
  'SELECT *  ',
  'FROM tbspedcods028 ',
  SQL_Periodo_Valido,
  'ORDER BY Nome ',
  '']);
  EdCOD_CONSRedefinido(Self);
  //
end;

(*
*)

end.
