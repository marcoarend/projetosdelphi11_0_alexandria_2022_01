unit EfdInnC001Ger;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Menus, ComCtrls, Grids,
  DBGrids, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, Variants,
  dmkDBGrid, frxClass, frxDBSet, dmkCheckGroup, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmEfdInnC001Ger = class(TForm)
    PainelDados: TPanel;
    DsEFD_C001: TDataSource;
    QrEFD_C001: TmySQLQuery;
    dmkPermissoes1: TdmkPermissoes;
    PMPeriodo: TPopupMenu;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    Itemns1: TMenuItem;
    Panel9: TPanel;
    Panel10: TPanel;
    DBGCab: TdmkDBGrid;
    Panel11: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel12: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    DBEdit1: TDBEdit;
    Panel3: TPanel;
    BtC500: TBitBtn;
    BtPeriodo: TBitBtn;
    Panel2: TPanel;
    BtSaida0: TBitBtn;
    PB1: TProgressBar;
    Panel16: TPanel;
    PMC500: TPopupMenu;
    GroupBox3: TGroupBox;
    Incluinovoperiodo1: TMenuItem;
    Excluiperiodoselecionado1: TMenuItem;
    QrEFD_C001NO_ENT: TWideStringField;
    QrEFD_C001MES_ANO: TWideStringField;
    QrEFD_C001ImporExpor: TSmallintField;
    QrEFD_C001AnoMes: TIntegerField;
    QrEFD_C001Empresa: TIntegerField;
    QrEFD_C001LinArq: TIntegerField;
    QrEFD_C001REG: TWideStringField;
    QrEFD_C001IND_MOV: TWideStringField;
    QrEfdInnC500Cab: TMySQLQuery;
    DsEfdInnC500Cab: TDataSource;
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    IncluiC500: TMenuItem;
    AlteraC500: TMenuItem;
    ExcluiC500: TMenuItem;
    dmkDBGrid1: TdmkDBGrid;
    TabSheet3: TTabSheet;
    BtC100: TBitBtn;
    PMC100: TPopupMenu;
    IncluiC1001: TMenuItem;
    AlteraC1001: TMenuItem;
    ExcluiC1001: TMenuItem;
    QrEFD_C100: TmySQLQuery;
    DsEFD_C100: TDataSource;
    dmkDBGrid2: TdmkDBGrid;
    QrEFD_C100NO_TERC: TWideStringField;
    QrEFD_C100ImporExpor: TSmallintField;
    QrEFD_C100AnoMes: TIntegerField;
    QrEFD_C100Empresa: TIntegerField;
    QrEFD_C100LinArq: TIntegerField;
    QrEFD_C100REG: TWideStringField;
    QrEFD_C100IND_OPER: TWideStringField;
    QrEFD_C100IND_EMIT: TWideStringField;
    QrEFD_C100COD_PART: TWideStringField;
    QrEFD_C100COD_MOD: TWideStringField;
    QrEFD_C100COD_SIT: TWideStringField;
    QrEFD_C100SER: TWideStringField;
    QrEFD_C100NUM_DOC: TIntegerField;
    QrEFD_C100CHV_NFE: TWideStringField;
    QrEFD_C100DT_DOC: TDateField;
    QrEFD_C100DT_E_S: TDateField;
    QrEFD_C100VL_DOC: TFloatField;
    QrEFD_C100IND_PGTO: TWideStringField;
    QrEFD_C100VL_DESC: TFloatField;
    QrEFD_C100VL_ABAT_NT: TFloatField;
    QrEFD_C100VL_MERC: TFloatField;
    QrEFD_C100IND_FRT: TWideStringField;
    QrEFD_C100VL_FRT: TFloatField;
    QrEFD_C100VL_SEG: TFloatField;
    QrEFD_C100VL_OUT_DA: TFloatField;
    QrEFD_C100VL_BC_ICMS: TFloatField;
    QrEFD_C100VL_ICMS: TFloatField;
    QrEFD_C100VL_BC_ICMS_ST: TFloatField;
    QrEFD_C100VL_ICMS_ST: TFloatField;
    QrEFD_C100VL_IPI: TFloatField;
    QrEFD_C100VL_PIS: TFloatField;
    QrEFD_C100VL_COFINS: TFloatField;
    QrEFD_C100VL_PIS_ST: TFloatField;
    QrEFD_C100VL_COFINS_ST: TFloatField;
    QrEFD_C100ParTipo: TIntegerField;
    QrEFD_C100ParCodi: TIntegerField;
    QrEFD_C100FatID: TIntegerField;
    QrEFD_C100FatNum: TIntegerField;
    QrEFD_C100ConfVal: TSmallintField;
    QrEFD_C100Terceiro: TIntegerField;
    QrEFD_C100Importado: TSmallintField;
    QrEFD_C100CST_ICMS: TWideStringField;
    QrEFD_C100CFOP: TWideStringField;
    QrEFD_C100ALIQ_ICMS: TFloatField;
    QrEFD_C100VL_RED_BC: TFloatField;
    N1: TMenuItem;
    IncluiitemNFC5001: TMenuItem;
    AlteraitemNFC500selecionada1: TMenuItem;
    ExcluiitemNFC500selecionada1: TMenuItem;
    QrEFD_C100Codigo: TIntegerField;
    QrEfdInnC500Its: TMySQLQuery;
    DsEfdInnC500Its: TDataSource;
    QrEfdInnC500ItsAnoMes: TIntegerField;
    QrEfdInnC500ItsEmpresa: TIntegerField;
    QrEfdInnC500ItsCodigo: TIntegerField;
    QrEfdInnC500ItsControle: TIntegerField;
    QrEfdInnC500ItsConta: TIntegerField;
    QrEfdInnC500ItsGraGruX: TIntegerField;
    QrEfdInnC500ItsQTD: TFloatField;
    QrEfdInnC500ItsUNID: TWideStringField;
    QrEfdInnC500ItsVL_ITEM: TFloatField;
    QrEfdInnC500ItsVL_DESC: TFloatField;
    QrEfdInnC500ItsCST_ICMS: TIntegerField;
    QrEfdInnC500ItsCFOP: TIntegerField;
    QrEfdInnC500ItsVL_BC_ICMS: TFloatField;
    QrEfdInnC500ItsALIQ_ICMS: TFloatField;
    QrEfdInnC500ItsVL_ICMS: TFloatField;
    QrEfdInnC500ItsVL_BC_ICMS_ST: TFloatField;
    QrEfdInnC500ItsALIQ_ST: TFloatField;
    QrEfdInnC500ItsVL_ICMS_ST: TFloatField;
    QrEfdInnC500ItsCST_PIS: TWideStringField;
    QrEfdInnC500ItsNAT_BC_CRED: TWideStringField;
    QrEfdInnC500ItsVL_BC_PIS: TFloatField;
    QrEfdInnC500ItsALIQ_PIS: TFloatField;
    QrEfdInnC500ItsVL_PIS: TFloatField;
    QrEfdInnC500ItsCOD_CTA: TWideStringField;
    QrEfdInnC500ItsCST_COFINS: TWideStringField;
    QrEfdInnC500ItsVL_BC_COFINS: TFloatField;
    QrEfdInnC500ItsALIQ_COFINS: TFloatField;
    QrEfdInnC500ItsVL_COFINS: TFloatField;
    QrEfdInnC500CabNO_TERC: TWideStringField;
    QrEfdInnC500CabAnoMes: TIntegerField;
    QrEfdInnC500CabEmpresa: TIntegerField;
    QrEfdInnC500CabCodigo: TIntegerField;
    QrEfdInnC500CabControle: TIntegerField;
    QrEfdInnC500CabIND_OPER: TWideStringField;
    QrEfdInnC500CabIND_EMIT: TWideStringField;
    QrEfdInnC500CabCOD_MOD: TWideStringField;
    QrEfdInnC500CabCOD_SIT: TWideStringField;
    QrEfdInnC500CabSER: TWideStringField;
    QrEfdInnC500CabSUB: TWideStringField;
    QrEfdInnC500CabCOD_CONS: TWideStringField;
    QrEfdInnC500CabNUM_DOC: TIntegerField;
    QrEfdInnC500CabDT_DOC: TDateField;
    QrEfdInnC500CabDT_E_S: TDateField;
    QrEfdInnC500CabVL_DOC: TFloatField;
    QrEfdInnC500CabVL_DESC: TFloatField;
    QrEfdInnC500CabVL_FORN: TFloatField;
    QrEfdInnC500CabVL_SERV_NT: TFloatField;
    QrEfdInnC500CabVL_TERC: TFloatField;
    QrEfdInnC500CabVL_DA: TFloatField;
    QrEfdInnC500CabVL_BC_ICMS: TFloatField;
    QrEfdInnC500CabVL_ICMS: TFloatField;
    QrEfdInnC500CabVL_BC_ICMS_ST: TFloatField;
    QrEfdInnC500CabVL_ICMS_ST: TFloatField;
    QrEfdInnC500CabCOD_INF: TWideStringField;
    QrEfdInnC500CabVL_PIS: TFloatField;
    QrEfdInnC500CabVL_COFINS: TFloatField;
    QrEfdInnC500CabTP_LIGACAO: TWideStringField;
    QrEfdInnC500CabCOD_GRUPO_TENSAO: TWideStringField;
    QrEfdInnC500CabTerceiro: TIntegerField;
    QrEfdInnC500CabImportado: TSmallintField;
    QrEfdInnC500CabCHV_DOCe: TWideStringField;
    QrEfdInnC500CabFIN_DOCe: TSmallintField;
    QrEfdInnC500CabCHV_DOCe_REF: TWideStringField;
    QrEfdInnC500CabIND_DEST: TSmallintField;
    QrEfdInnC500CabCOD_MUN_DEST: TIntegerField;
    QrEfdInnC500CabCOD_CTA: TWideStringField;
    QrEfdInnC500CabCOD_MOD_DOC_REF: TSmallintField;
    QrEfdInnC500CabHASH_DOC_REF: TWideStringField;
    QrEfdInnC500CabSER_DOC_REF: TWideStringField;
    QrEfdInnC500CabNUM_DOC_REF: TIntegerField;
    QrEfdInnC500CabMES_DOC_REF: TIntegerField;
    QrEfdInnC500CabENER_INJET: TFloatField;
    QrEfdInnC500CabOUTRAS_DED: TFloatField;
    DBGrid1: TDBGrid;
    QrEfdInnC500ItsNO_PRD_TAM_COR: TWideStringField;
    QrEfdInnC500ItsSIGLAUNIDMED: TWideStringField;
    Splitter1: TSplitter;
    QrEfdInnC500ItsVL_Outro: TFloatField;
    QrEfdInnC500ItsVL_OPR: TFloatField;
    QrEfdInnC500ItsVL_RED_BC: TFloatField;
    QrSum: TMySQLQuery;
    QrSumVL_BC_ICMS: TFloatField;
    QrSumVL_ICMS: TFloatField;
    QrSumVL_BC_ICMS_ST: TFloatField;
    QrSumVL_ICMS_ST: TFloatField;
    QrSumVL_RED_BC: TFloatField;
    QrSumVL_PIS: TFloatField;
    QrSumVL_COFINS: TFloatField;
    procedure BtC500Click(Sender: TObject);
    procedure BtSaida0Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPeriodoClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrEFD_C001BeforeClose(DataSet: TDataSet);
    procedure QrEFD_C001AfterScroll(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure QrEFD_C001AfterOpen(DataSet: TDataSet);
    procedure PMPeriodoPopup(Sender: TObject);
    procedure RGEmitenteClick(Sender: TObject);
    procedure PMC500Popup(Sender: TObject);
    procedure Incluinovoperiodo1Click(Sender: TObject);
    procedure Excluiperiodoselecionado1Click(Sender: TObject);
    procedure IncluiC500Click(Sender: TObject);
    procedure AlteraC500Click(Sender: TObject);
    procedure ExcluiC500Click(Sender: TObject);
    procedure BtC100Click(Sender: TObject);
    procedure IncluiC1001Click(Sender: TObject);
    procedure AlteraC1001Click(Sender: TObject);
    procedure ExcluiC1001Click(Sender: TObject);
    procedure IncluiitemNFC5001Click(Sender: TObject);
    procedure AlteraitemNFC500selecionada1Click(Sender: TObject);
    procedure QrEfdInnC500CabBeforeClose(DataSet: TDataSet);
    procedure QrEfdInnC500CabAfterScroll(DataSet: TDataSet);
    procedure ExcluiitemNFC500selecionada1Click(Sender: TObject);
  private
    { Private declarations }
    function  PeriodoJaExiste(ImporExpor, AnoMes, Empresa: Integer;
              Avisa: Boolean): Boolean;
    procedure InsUpdC100(SQLType: TSQLType);
    procedure InsUpdEfdInnC500Cab(SQLType: TSQLType);
    procedure InsUpdEfdInnC500Its(SQLType: TSQLType);
    //

  public
    { Public declarations }
    FEmpresa: Integer;
    FExpImpTXT, FEmprTXT: String;
    //
    procedure ReopenEFD_C001(AnoMes: Integer);
    procedure ReopenEFD_C100(LinArq: Integer);
    //
    procedure ReopenEfdInnC500Cab(Controle: Integer);
    procedure ReopenEfdInnC500Its(Conta: Integer);
    //
    procedure AtualizaTotaisC500(Controle: Integer);

  end;

var
  FmEfdInnC001Ger: TFmEfdInnC001Ger;

implementation

uses
  UnMyObjects, Module, MyDBCheck, ModuleGeral, ModProd, UCreate,
  CfgExpFile, UnGrade_Tabs, ModuleNFe_0000,
  Principal, Periodo, (*EFD_E100, EFD_E110, EFD_E111, EFD_E116,
  EFD_E112, EFD_E113, EFD_E115, EFD_C100*)EfdInnC500Cab, EfdInnC500Its;

{$R *.DFM}

const
  FFormatFloat = '00000';
  FImporExpor = 3; // Criar! N�o Mexer

procedure TFmEfdInnC001Ger.EdEmpresaChange(Sender: TObject);
begin
  if EdEmpresa.ValueVariant <> 0 then
  begin
    FEmpresa := DModG.QrEmpresasCodigo.Value;
    FEmprTXT := FormatFloat('0', FEmpresa);
  end else
  begin
    FEmpresa := 0;
    FEmprTXT := '0';
  end;
  BtPeriodo.Enabled := FEmpresa <> 0;
  ReopenEFD_C001(0);
end;

procedure TFmEfdInnC001Ger.ExcluiC1001Click(Sender: TObject);
begin
{ TODO : Exclus�o de C100 }
end;

procedure TFmEfdInnC001Ger.ExcluiC500Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da NF Tipo C500?') = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdinnc500cab', [
    'Controle'], ['='],
    [QrEfdInnC500CabControle.Value], '') then
    //
    ReopenEfdInnC500Cab(0);
  end;
end;

procedure TFmEfdInnC001Ger.ExcluiitemNFC500selecionada1Click(Sender: TObject);
var
  Controle, Conta: Integer;
begin
  if QrEfdInnC500Its.RecordCount > 0 then
  begin
    if UMyMod.ExcluiRegistroInt1('Confirma a retirada do item selecionado?',
    'EfdInnC500Its', 'Conta', QrEfdInnC500ItsConta.Value, Dmod.MyDB) = ID_YES then
    begin
      Controle := QrEfdInnC500ItsControle.Value;
      Conta := GOTOy.LocalizaPriorNextIntQr(QrEfdInnC500Its,
        QrEfdInnC500ItsConta, QrEfdInnC500ItsConta.Value);
      AtualizaTotaisC500(Controle);
      ReopenEfdInnC500Cab(Controle);
      ReopenEfdInnC500Its(Conta);
    end;
  end;
end;

procedure TFmEfdInnC001Ger.Excluiperiodoselecionado1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do per�odo selecionado?',
  'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipic001', [
    'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
    [QrEFD_C001ImporExpor.Value, QrEFD_C001AnoMes.Value,
    QrEFD_C001Empresa.Value], '') then
    ReopenEFD_C001(0);
  end;
end;

procedure TFmEfdInnC001Ger.IncluiC1001Click(Sender: TObject);
begin
  InsUpdC100(stIns);
end;

procedure TFmEfdInnC001Ger.IncluiC500Click(Sender: TObject);
begin
  InsUpdEfdInnC500Cab(stIns);
end;

procedure TFmEfdInnC001Ger.IncluiitemNFC5001Click(Sender: TObject);
begin
  InsUpdEfdInnC500Its(stIns);
end;

procedure TFmEfdInnC001Ger.Incluinovoperiodo1Click(Sender: TObject);
const
  LinArq = 0;
  REG = 'E001';
  IND_MOV = 1;
var
  Ano, Mes, Dia: Word;
  Cancelou: Boolean;
  AnoMes: Integer;
begin
  DecodeDate(Date, Ano, Mes, Dia);
  MLAGeral.EscolhePeriodo_MesEAno(TFmPeriodo, FmPeriodo, Mes, Ano, Cancelou, True, True);
  if not Cancelou then
  begin
    AnoMes := (Ano * 100) + Mes;
    //ShowMessage(FormatFloat('0', (Ano * 100) + Mes));
    if not PeriodoJaExiste(FImporExpor, AnoMes, FEmpresa, True) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efdicmsipic001', False, [
      'LinArq', 'REG', 'IND_MOV'], [
      'ImporExpor', 'AnoMes', 'Empresa'], [
      LinArq, REG, IND_MOV], [
      FImporExpor, AnoMes, FEmpresa], True) then
        ReopenEFD_C001(AnoMes);
    end;
    ReopenEFD_C001(AnoMes);
  end;
end;

procedure TFmEfdInnC001Ger.InsUpdC100(SQLType: TSQLType);
begin
(*
  if UmyMod.FormInsUpd_Cria(TFmEFD_C100, FmEFD_C100, afmoNegarComAviso,
  QrEFD_C100, SQLType) then
  begin
    FmEFD_C100.ImgTipo.SQLType := SQLType;
    FmEFD_C100.EdImporExpor.ValueVariant := QrEFD_C001ImporExpor.Value;
    FmEFD_C100.EdAnoMes.ValueVariant := QrEFD_C001AnoMes.Value;
    FmEFD_C100.EdEmpresa.ValueVariant := QrEFD_C001Empresa.Value;
    //
    if SQLType = stIns then
    begin
      FmEFD_C100.RGIND_OPER.ItemIndex := 0;
      FmEFD_C100.RGIND_EMIT.ItemIndex := 1;
      FmEFD_C100.TPDT_DOC.Date := Date;
      FmEFD_C100.TPDT_E_S.Date := Date;
    end else
    begin
    end;
    //
    FmEFD_C100.ShowModal;
    FmEFD_C100.Destroy;
  end;
*)
end;

procedure TFmEfdInnC001Ger.InsUpdEfdInnC500Cab(SQLType: TSQLType);
var
  Controle: Integer;
begin
  if DBCheck.CriaFm(TFmEfdInnC500Cab, FmEfdInnC500Cab, afmoNegarComAviso) then
  begin
    FmEfdInnC500Cab.FAnoMes := QrEFD_C001AnoMes.Value;
    FmEfdInnC500Cab.ReopenTabelas();
    FmEfdInnC500Cab.ImgTipo.SQLType := SQLType;
    FmEfdInnC500Cab.EdAnoMes.ValueVariant := QrEFD_C001AnoMes.Value;
    FmEfdInnC500Cab.EdEmpresa.ValueVariant := QrEFD_C001Empresa.Value;
    FmEfdInnC500Cab.EdCodigo.ValueVariant := 0; // n�o usa ainda

    //
    if SQLType = stIns then
    begin
      FmEfdInnC500Cab.RGIND_OPER.ItemIndex := 0;
      FmEfdInnC500Cab.RGIND_EMIT.ItemIndex := 1;
      //
      FmEfdInnC500Cab.EdCOD_SIT.Text := '00';
      FmEfdInnC500Cab.TPDT_DOC.Date := Date;
      FmEfdInnC500Cab.TPDT_E_S.Date := Date;
    end else
    begin
      FmEfdInnC500Cab.EdVL_BC_ICMS.Enabled    := True;
      FmEfdInnC500Cab.EdVL_ICMS.Enabled       := True;
      FmEfdInnC500Cab.EdVL_BC_ICMS_ST.Enabled := True;
      FmEfdInnC500Cab.EdVL_ICMS_ST.Enabled    := True;
      FmEfdInnC500Cab.EdVL_PIS.Enabled        := True;
      FmEfdInnC500Cab.EdVL_COFINS.Enabled     := True;

      FmEfdInnC500Cab.RGIND_OPER.ItemIndex := 0;
      FmEfdInnC500Cab.RGIND_EMIT.ItemIndex := 1;
      //
      FmEfdInnC500Cab.EdControle.ValueVariant      := QrEfdInnC500CabControle.Value;
      //FmEfdInnC500Cab.RGIND_OPER.ItemIndex         := QrEfdInnC500CabIND_OPER.Value;
      //FmEfdInnC500Cab.RGIND_EMIT.ItemIndex         := QrEfdInnC500CabIND_EMIT.Value;
      FmEfdInnC500Cab.EdTerceiro.ValueVariant      := QrEfdInnC500CabTerceiro.Value;
      FmEfdInnC500Cab.CBTerceiro.KeyValue          := QrEfdInnC500CabTerceiro.Value;
      FmEfdInnC500Cab.EdCOD_MOD.Text               := QrEfdInnC500CabCOD_MOD.Value;
      FmEfdInnC500Cab.EdCOD_SIT.Text               := QrEfdInnC500CabCOD_SIT.Value;
      FmEfdInnC500Cab.EdSER.Text                   := QrEfdInnC500CabSER.Value;
      FmEfdInnC500Cab.EdSUB.Text                   := QrEfdInnC500CabSUB.Value;
      FmEfdInnC500Cab.EdCOD_CONS.Text              := QrEfdInnC500CabCOD_CONS.Value;
      FmEfdInnC500Cab.EdNUM_DOC.ValueVariant       := QrEfdInnC500CabNUM_DOC.Value;
      FmEfdInnC500Cab.TPDT_DOC.Date                := QrEfdInnC500CabDT_DOC.Value;
      FmEfdInnC500Cab.TPDT_E_S.Date                := QrEfdInnC500CabDT_E_S.Value;
      FmEfdInnC500Cab.EdVL_DOC.ValueVariant        := QrEfdInnC500CabVL_DOC.Value;
      FmEfdInnC500Cab.EdVL_DESC.ValueVariant       := QrEfdInnC500CabVL_DESC.Value;
      FmEfdInnC500Cab.EdVL_FORN.ValueVariant       := QrEfdInnC500CabVL_FORN.Value;
      FmEfdInnC500Cab.EdVL_SERV_NT.ValueVariant    := QrEfdInnC500CabVL_SERV_NT.Value;
      FmEfdInnC500Cab.EdVL_TERC.ValueVariant       := QrEfdInnC500CabVL_TERC.Value;
      FmEfdInnC500Cab.EdVL_DA.ValueVariant         := QrEfdInnC500CabVL_DA.Value;
      FmEfdInnC500Cab.EdENER_INJET.ValueVariant    := QrEfdInnC500CabENER_INJET.Value;
      FmEfdInnC500Cab.EdOUTRAS_DED.ValueVariant    := QrEfdInnC500CabOUTRAS_DED.Value;
      FmEfdInnC500Cab.EdVL_BC_ICMS.ValueVariant    := QrEfdInnC500CabVL_BC_ICMS.Value;
      FmEfdInnC500Cab.EdVL_ICMS.ValueVariant       := QrEfdInnC500CabVL_ICMS.Value;
      FmEfdInnC500Cab.EdVL_BC_ICMS_ST.ValueVariant := QrEfdInnC500CabVL_BC_ICMS_ST.Value;
      FmEfdInnC500Cab.EdVL_ICMS_ST.ValueVariant    := QrEfdInnC500CabVL_ICMS_ST.Value;
      FmEfdInnC500Cab.EdVL_PIS.ValueVariant        := QrEfdInnC500CabVL_PIS.Value;
      FmEfdInnC500Cab.EdVL_COFINS.ValueVariant     := QrEfdInnC500CabVL_COFINS.Value;
      //FmEfdInnC500Cab.RGTP_LIGACAO.ItemIndex       := QrEfdInnC500CabTP_LIGACAO.Value;
      //FmEfdInnC500Cab.RGCOD_GRUPO_TENSAO.ItemIndex := QrEfdInnC500CabCOD_GRUPO_TENSAO.Value;
      FmEfdInnC500Cab.EdCHV_DOCe.ValueVariant        := QrEfdInnC500CabCHV_DOCe_REF.Value;
      FmEfdInnC500Cab.EdCHV_DOCe_REF.ValueVariant    := QrEfdInnC500CabCOD_CTA.Value;
      //FmEfdInnC500Cab.EdCOD_CTA.ValueVariant         := QrEfdInnC500CabHASH_DOC_REF.Value;
      //FmEfdInnC500Cab.EdHASH_DOC_REF.ValueVariant    := QrEfdInnC500CabSER_DOC_REF.Value;
      //FmEfdInnC500Cab.EdSER_DOC_REF.ValueVariant     := QrEfdInnC500Cab.Value;

      FmEfdInnC500Cab.EdFIN_DOCe.ValueVariant        := QrEfdInnC500CabFIN_DOCe.Value;
      //FmEfdInnC500Cab.EdIND_DEST.ValueVariant        := QrEfdInnC500CabIND_DEST.Value;
      //FmEfdInnC500Cab.EdCOD_MUN_DEST.ValueVariant    := QrEfdInnC500CabCOD_MUN_DEST.Value;
      //FmEfdInnC500Cab.EdCOD_MOD_DOC_REF.ValueVariant := QrEfdInnC500CabCOD_MOD_DOC_REF.Value;
      FmEfdInnC500Cab.EdNUM_DOC_REF.ValueVariant     := QrEfdInnC500CabNUM_DOC_REF.Value;
      FmEfdInnC500Cab.EdMES_DOC_REF.ValueVariant     := QrEfdInnC500CabMES_DOC_REF.Value;
      //
    end;
    //
    FmEfdInnC500Cab.ShowModal;
    Controle := FmEfdInnC500Cab.FControle;
    FmEfdInnC500Cab.Destroy;
    //
    if SQLType = stIns then
    begin
      if (Controle <> 0) and (Controle = QrEfdInnC500CabControle.Value) then
        InsUpdEfdInnC500Its(stIns);
    end;
  end;
end;

procedure TFmEfdInnC001Ger.InsUpdEfdInnC500Its(SQLType: TSQLType);
(*
begin
  if DBCheck.CriaFm(TFmEfdInnC500Its, FmEfdInnC500Its, afmoNegarComAviso) then
  begin
    FmEfdInnC500Its.ImgTipo.SQLType := SQLType;
    FmEfdInnC500Its.EdAnoMes.ValueVariant         := QrEFD_C001AnoMes.Value;
    FmEfdInnC500Its.EdEmpresa.ValueVariant        := QrEFD_C001Empresa.Value;
    FmEfdInnC500Its.EdC500.ValueVariant           := QrEfdInnC500CabLinArq.Value;
    FmEfdInnC500Its.EdCodigo.ValueVariant         := QrEfdInnC500CabCodigo.Value;
    //
    if SQLType = stIns then
    begin
      //
    end else
    begin
      FmEfdInnC500Its.EdControle.ValueVariant     := QrEfdIcmsIpiC500Controle.Value;
    end;
    //
    FmEfdInnC500Its.ShowModal;
    FmEfdInnC500Its.Destroy;
  end;
*)
var
  NewConta, Conta, GraGruX, Grandeza: Integer;
  CST_B, IPI_CST, PIS_CST, COFINS_CST: Integer;
  ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq, COFINSRec_pAliq, ICMSSTRec_pAliq: Double;
begin
  if DBCheck.CriaFm(TFmEfdInnC500Its, FmEfdInnC500Its, afmoNegarComAviso) then
  begin
    FmEfdInnC500Its.ImgTipo.SQLType := SQLType;
(*
    FmEfdInnC500Its.FData           := QrEfdInnNFsCabDT_E_S.Value;
    FmEfdInnC500Its.FRegrFiscal     := QrEfdInnNFsCabRegrFiscal.Value;
    //
    FmEfdInnC500Its.FQrCab                         := QrEfdInnNFsCab;
    FmEfdInnC500Its.FQrIts                         := QrEfdInnNFsIts;
    FmEfdInnC500Its.EdEmpresa.ValueVariant         := QrPQEEmpresa.Value;
    FmEfdInnC500Its.EdMovFatID.ValueVariant        := FEFDInnNFSMainFatID;
    FmEfdInnC500Its.EdMovFatNum.ValueVariant       := QrPQECodigo.Value;
    FmEfdInnC500Its.EdMovimCod.ValueVariant        := CO_MovimCod_ZERO; // N�o tem! QrPQEMovimCod.Value;
    FmEfdInnC500Its.EdMovimNiv.ValueVariant        := 0;
    FmEfdInnC500Its.EdMovimTwn.ValueVariant        := 0;
    FmEfdInnC500Its.EdControle.ValueVariant        := QrEfdInnNFsCabControle.Value;
    //FmEfdInnC500Its.FQrIts                         := QrVSInnNFs;
*)
    FmEfdInnC500Its.EdEmpresa.ValueVariant        := QrEfdInnC500CabEmpresa.Value;
    FmEfdInnC500Its.EdAnoMes.ValueVariant         := QrEfdInnC500CabAnoMes.Value;
    FmEfdInnC500Its.EdCodigo.ValueVariant         := QrEfdInnC500CabCodigo.Value;
    FmEfdInnC500Its.EdControle.ValueVariant       := QrEfdInnC500CabControle.Value;
    FmEfdInnC500Its.FData                         := QrEfdInnC500CabDT_E_S.Value;
    //
    if SQLType = stIns then
    begin

    end else
    begin
      FmEfdInnC500Its.EdControle.ValueVariant                      := QrEfdInnC500ItsControle.Value;
      FmEfdInnC500Its.EdConta.ValueVariant                         := QrEfdInnC500ItsConta.Value;

      FmEfdInnC500Its.EdGraGruX.ValueVariant                       := QrEfdInnC500ItsGraGruX.Value;
      FmEfdInnC500Its.CBGraGruX.KeyValue                           := QrEfdInnC500ItsGraGruX.Value;
      FmEfdInnC500Its.EdCFOP.Text                                  := Geral.FormataCFOP(Geral.FF0(QrEfdInnC500ItsCFOP.Value));
      FmEfdInnC500Its.EdQTD.ValueVariant                           := QrEfdInnC500ItsQTD.Value;
      FmEfdInnC500Its.EdVL_ITEM.ValueVariant                       := QrEfdInnC500ItsVL_ITEM.Value;
      FmEfdInnC500Its.EdVL_DESC.ValueVariant                       := QrEfdInnC500ItsVL_DESC.Value;

      FmEfdInnC500Its.EdCST_ICMS.ValueVariant                      := QrEfdInnC500ItsCST_ICMS.Value;
      FmEfdInnC500Its.EdVL_BC_ICMS.ValueVariant                    := QrEfdInnC500ItsVL_BC_ICMS.Value;
      FmEfdInnC500Its.EdALIQ_ICMS.ValueVariant                     := QrEfdInnC500ItsALIQ_ICMS.Value;
      FmEfdInnC500Its.EdVL_ICMS.ValueVariant                       := QrEfdInnC500ItsVL_ICMS.Value;
      FmEfdInnC500Its.EdVL_BC_ICMS_ST.ValueVariant                 := QrEfdInnC500ItsVL_BC_ICMS_ST.Value;
      FmEfdInnC500Its.EdALIQ_ST.ValueVariant                       := QrEfdInnC500ItsALIQ_ST.Value;
      FmEfdInnC500Its.EdVL_ICMS_ST.ValueVariant                    := QrEfdInnC500ItsVL_ICMS_ST.Value;

      FmEfdInnC500Its.EdCST_PIS.ValueVariant                       := QrEfdInnC500ItsCST_PIS.Value;
      FmEfdInnC500Its.EdVL_BC_PIS .ValueVariant                    := QrEfdInnC500ItsVL_BC_PIS.Value;
      FmEfdInnC500Its.EdALIQ_PIS.ValueVariant                      := QrEfdInnC500ItsALIQ_PIS.Value;
      FmEfdInnC500Its.EdVL_PIS.ValueVariant                        := QrEfdInnC500ItsVL_PIS.Value;
      FmEfdInnC500Its.EdCST_COFINS.ValueVariant                    := QrEfdInnC500ItsCST_COFINS.Value;
      FmEfdInnC500Its.EdVL_BC_COFINS.ValueVariant                  := QrEfdInnC500ItsVL_BC_COFINS.Value;
      FmEfdInnC500Its.EdALIQ_COFINS.ValueVariant                   := QrEfdInnC500ItsALIQ_COFINS.Value;
      FmEfdInnC500Its.EdVL_COFINS.ValueVariant                     := QrEfdInnC500ItsVL_COFINS.Value;
      //FmEfdInnC500Its.EdCOD_CTA.ValueVariant                       := QrEfdInnC500ItsCOD_CTA.Value;
      //FmEfdInnC500Its.EdVL_ABAT_NT.ValueVariant                    := QrEfdInnC500ItsVL_ABAT_NT.Value;
      //
      FmEfdInnC500Its.EdVL_Outro.ValueVariant                      := QrEfdInnC500ItsVL_Outro.Value;
      FmEfdInnC500Its.EdVL_OPR.ValueVariant                        := QrEfdInnC500ItsVL_OPR.Value;
      FmEfdInnC500Its.EdVL_RED_BC.ValueVariant                     := QrEfdInnC500ItsVL_RED_BC.Value;
(*
      FmEfdInnC500Its.EdAjusteVL_BC_ICMS.ValueVariant              := QrEfdInnC500ItsAjusteVL_BC_ICMS.Value;
      FmEfdInnC500Its.EdAjusteALIQ_ICMS.ValueVariant               := QrEfdInnC500ItsAjusteALIQ_ICMS.Value;
      FmEfdInnC500Its.EdAjusteVL_ICMS.ValueVariant                 := QrEfdInnC500ItsAjusteVL_ICMS.Value;
      FmEfdInnC500Its.EdAjusteVL_OUTROS.ValueVariant               := QrEfdInnC500ItsAjusteVL_OUTROS.Value;
*)
      FmEfdInnC500Its.EdNAT_BC_CRED.ValueVariant                   := QrEfdInnC500ItsNAT_BC_CRED.Value;
    end;
    FmEfdInnC500Its.ShowModal;
    NewConta := FmEfdInnC500Its.FNewConta;
    FmEfdInnC500Its.Destroy;
        //
    ReopenEfdInnC500Cab(QrEfdInnC500CabControle.Value);
    ReopenEfdInnC500Its(NewConta);
  end;
end;

function TFmEfdInnC001Ger.PeriodoJaExiste(ImporExpor, AnoMes, Empresa: Integer;
Avisa: Boolean): Boolean;
var
  Qry1: TmySQLQuery;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  Qry1.Close;
  Qry1.Database := Dmod.MyDB;
  UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
  'SELECT COUNT(*) Registros ',
  'FROM efdicmsipic001 ',
  'WHERE ImporExpor=' + FormatFloat('0', ImporExpor),
  'AND AnoMes=' + FormatFloat('0', AnoMes),
  'AND Empresa=' + FormatFloat('0', Empresa),
  '']);
  Result := Qry1.FieldByName('Registros').AsInteger > 0;
  if Result and Avisa then
  begin
    Geral.MensagemBox('O periodo selecionado j� existe!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmEfdInnC001Ger.PMPeriodoPopup(Sender: TObject);
begin
  Excluiperiodoselecionado1.Enabled :=
    (QrEFD_C001.State <> dsInactive) and
    (QrEFD_C001.RecordCount > 0)
  and
    (QrEfdInnC500Cab.State <> dsInactive) and
    (QrEfdInnC500Cab.RecordCount = 0);
end;

procedure TFmEfdInnC001Ger.PMC500Popup(Sender: TObject);
(*var
  Habilita: Boolean;
begin
  Habilita := (QrEFD_C001.State <> dsInactive) and (QrEFD_C001.RecordCount > 0);
  IncluiC500.Enabled := Habilita;
  //
  Habilita := Habilita and
    (QrEfdInnC500Cab.State <> dsInactive) and (QrEfdInnC500Cab.RecordCount > 0);
  AlteraC500.Enabled := Habilita;
  ExcluiC500.Enabled := Habilita and
    (QrEfdInnC500Cab.State <> dsInactive) and (QrEfdInnC500Cab.RecordCount > 0);
*)
begin
  MyObjects.HabilitaMenuItemCabUpd(AlteraC500, QrEfdInnC500Cab);
  MyObjects.HabilitaMenuItemCabDel(ExcluiC500, QrEfdInnC500Cab, QrEfdInnC500Its);
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiitemNFC5001, QrEfdInnC500Cab);
  MyObjects.HabilitaMenuItemItsDel(AlteraitemNFC500selecionada1, QrEfdInnC500Its);
  MyObjects.HabilitaMenuItemItsUpd(ExcluiitemNFC500selecionada1, QrEfdInnC500Its);
end;

procedure TFmEfdInnC001Ger.RGEmitenteClick(Sender: TObject);
begin
  ReopenEfdInnC500Cab(0);
end;

procedure TFmEfdInnC001Ger.ReopenEfdInnC500Cab(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnC500Cab, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)  ',
  'NO_TERC, c500.* ',
  'FROM efdinnc500cab c500  ',
  'LEFT JOIN entidades ent ON ent.Codigo=c500.Terceiro ',
  'WHERE c500.Empresa=' + FormatFloat('0', QrEFD_C001Empresa.Value),
  'AND c500.AnoMes=' + FormatFloat('0', QrEFD_C001AnoMes.Value),
  'ORDER BY DT_E_S DESC, Controle DESC ',
  '']);
  //
  QrEfdInnC500Cab.Locate('Controle', Controle, []);
end;

procedure TFmEfdInnC001Ger.ReopenEfdInnC500Its(Conta: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrEfdInnC500Its, Dmod.MyDB, [
  'SELECT vin.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM efdInnc500its     vin',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vin.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE vin.Controle=' + Geral.FF0(QrEfdInnC500CabControle.Value),
  '']);
  //
  QrEfdInnC500Its.Locate('Conta', Conta, []);
end;

procedure TFmEfdInnC001Ger.ReopenEFD_C001(AnoMes: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_C001, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT, ',
  'CONCAT(RIGHT(e001.AnoMes, 2), "/", ',
  'LEFT(LPAD(e001.AnoMes, 6, "0"), 4)) MES_ANO, e001.* ',
  'FROM efdicmsipic001 e001 ',
  'LEFT JOIN entidades ent ON ent.Codigo=e001.Empresa ',
  'WHERE e001.ImporExpor=' + FormatFloat('0', FImporExpor),
  'AND e001.Empresa=' + FormatFloat('0', FEmpresa),
  'ORDER BY e001.AnoMes DESC ',
  '']);
  //
  QrEFD_C001.Locate('AnoMes', AnoMes, []);
end;

procedure TFmEfdInnC001Ger.ReopenEFD_C100(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_C100, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)  ',
  'NO_TERC, c100.* ',
  'FROM efdicmsipic100 c100  ',
  'LEFT JOIN entidades ent ON ent.Codigo=c100.Terceiro ',
  'WHERE c100.ImporExpor=' + FormatFloat('0', QrEFD_C001ImporExpor.Value),
  'AND c100.Empresa=' + FormatFloat('0', QrEFD_C001Empresa.Value),
  'AND c100.AnoMes=' + FormatFloat('0', QrEFD_C001AnoMes.Value),
  '']);
  //
  QrEFD_C100.Locate('LinArq', LinArq, []);
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEfdInnC001Ger.BtC100Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMC100, BtC100);
end;

procedure TFmEfdInnC001Ger.BtC500Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMC500, BtC500);
end;

procedure TFmEfdInnC001Ger.BtSaida0Click(Sender: TObject);
begin
  //VAR_CADASTRO := Qr?.Value;
  Close;
end;

procedure TFmEfdInnC001Ger.AlteraC1001Click(Sender: TObject);
begin
  InsUpdC100(stUpd);
end;

procedure TFmEfdInnC001Ger.AlteraC500Click(Sender: TObject);
begin
  InsUpdEfdInnC500Cab(stUpd);
end;

procedure TFmEfdInnC001Ger.AlteraitemNFC500selecionada1Click(Sender: TObject);
begin
  InsUpdEfdInnC500Its(stUpd);
end;

procedure TFmEfdInnC001Ger.AtualizaTotaisC500(Controle: Integer);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrSum, Dmod.MyDB, [
  'SELECT SUM(VL_BC_ICMS) VL_BC_ICMS, ',
  'SUM(VL_ICMS) VL_ICMS, ',
  'SUM(VL_BC_ICMS_ST) VL_BC_ICMS_ST, ',
  'SUM(VL_ICMS_ST) VL_ICMS_ST, ',
  'SUM(VL_RED_BC) VL_RED_BC,',
  'SUM(VL_PIS) VL_PIS,',
  'SUM(VL_COFINS) VL_COFINS',
  'FROM efdinnc500its',
  'WHERE Controle=' + Geral.FF0(QrEfdInnC500CabControle.Value),
  '']);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'efdinnc500cab', False, [
  'VL_BC_ICMS', 'VL_ICMS',
  'VL_BC_ICMS_ST', 'VL_ICMS_ST',
  'VL_PIS', 'VL_COFINS'], [
  'Controle'], [
  QrSumVL_BC_ICMS.Value, QrSumVL_ICMS.Value,
  QrSumVL_BC_ICMS_ST.Value, QrSumVL_ICMS_ST.Value,
  QrSumVL_PIS.Value, QrSumVL_COFINS.Value], [
  Controle], True) then
  begin
    //
  end;
end;

procedure TFmEfdInnC001Ger.BtPeriodoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPeriodo, BtPeriodo);
end;

procedure TFmEfdInnC001Ger.FormCreate(Sender: TObject);
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FEmprTXT := '';
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 1;
  ImgTipo.SQLType := stLok;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmEfdInnC001Ger.SbNumeroClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Codigo(QrCadComItensCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEfdInnC001Ger.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEfdInnC001Ger.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrCadComItensCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmEfdInnC001Ger.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEfdInnC001Ger.QrEfdInnC500CabAfterScroll(DataSet: TDataSet);
begin
  ReopenEfdInnC500Its(0);
end;

procedure TFmEfdInnC001Ger.QrEfdInnC500CabBeforeClose(DataSet: TDataSet);
begin
  QrEfdInnC500Its.Close;
end;

procedure TFmEfdInnC001Ger.QrEFD_C001AfterOpen(DataSet: TDataSet);
begin
  BtC500.Enabled := QrEFD_C001.RecordCount > 0;
end;

procedure TFmEfdInnC001Ger.QrEFD_C001AfterScroll(DataSet: TDataSet);
begin
  ReopenEfdInnC500Cab(0);
end;

procedure TFmEfdInnC001Ger.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdInnC001Ger.SbQueryClick(Sender: TObject);
begin
{
  LocCod(QrCadComItensCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cadcomitens', Dmod.MyDB, CO_VAZIO));
}
end;

procedure TFmEfdInnC001Ger.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdInnC001Ger.QrEFD_C001BeforeClose(DataSet: TDataSet);
begin
  BtC500.Enabled := False;
  //
  QrEfdInnC500Cab.Close;
end;

end.

