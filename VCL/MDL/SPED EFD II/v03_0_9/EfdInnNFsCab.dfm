object FmEfdInnNFsCab: TFmEfdInnNFsCab
  Left = 339
  Top = 185
  Caption = 'SPE-ENTRA-002 :: Itens de NF-e de Entrada'
  ClientHeight = 627
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 496
    Width = 1008
    Height = 17
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar_: TCheckBox
      Left = 12
      Top = 0
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 1008
    Height = 57
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 0
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label1: TLabel
      Left = 540
      Top = 16
      Width = 46
      Height = 13
      Caption = 'Motorista:'
    end
    object SBMotorista: TSpeedButton
      Left = 892
      Top = 32
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBMotoristaClick
    end
    object LaPecas: TLabel
      Left = 96
      Top = 16
      Width = 33
      Height = 13
      Caption = 'Pe'#231'as:'
    end
    object LaPeso: TLabel
      Left = 180
      Top = 16
      Width = 27
      Height = 13
      Caption = 'Peso:'
    end
    object Label10: TLabel
      Left = 444
      Top = 16
      Width = 71
      Height = 13
      Caption = 'Valor total [F4]:'
    end
    object Label23: TLabel
      Left = 916
      Top = 16
      Width = 30
      Height = 13
      Caption = 'Placa:'
    end
    object LaAreaM2: TLabel
      Left = 272
      Top = 16
      Width = 39
      Height = 13
      Caption = #193'rea m'#178':'
      Enabled = False
    end
    object LaAreaP2: TLabel
      Left = 360
      Top = 16
      Width = 37
      Height = 13
      Caption = #193'rea ft'#178':'
      Enabled = False
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Conta'
      UpdCampo = 'Conta'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CBMotorista: TdmkDBLookupComboBox
      Left = 596
      Top = 32
      Width = 293
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsMotorista
      TabOrder = 7
      dmkEditCB = EdMotorista
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdMotorista: TdmkEditCB
      Left = 540
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBMotorista
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdPesoKg: TdmkEdit
      Left = 180
      Top = 32
      Width = 85
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdPecas: TdmkEdit
      Left = 96
      Top = 32
      Width = 81
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'PesoKg'
      UpdCampo = 'PesoKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdValorT: TdmkEdit
      Left = 444
      Top = 32
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ValorT'
      UpdCampo = 'ValorT'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdValorTChange
    end
    object EdPlaca: TdmkEdit
      Left = 916
      Top = 32
      Width = 80
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Placa'
      UpdCampo = 'Placa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdAreaM2: TdmkEditCalc
      Left = 268
      Top = 32
      Width = 84
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaM2'
      UpdCampo = 'AreaM2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      dmkEditCalcA = EdAreaP2
      CalcType = ctM2toP2
      CalcFrac = cfQuarto
    end
    object EdAreaP2: TdmkEditCalc
      Left = 356
      Top = 32
      Width = 84
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaP2'
      UpdCampo = 'AreaP2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      dmkEditCalcA = EdAreaM2
      CalcType = ctP2toM2
      CalcFrac = cfCento
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 201
        Height = 32
        Caption = 'NF-e de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 201
        Height = 32
        Caption = 'NF-e de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 201
        Height = 32
        Caption = 'NF-e de Entrada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 513
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 557
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 169
    Width = 1008
    Height = 327
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object PainelDados: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 120
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label17: TLabel
        Left = 64
        Top = 0
        Width = 65
        Height = 13
        Caption = 'Data entrada:'
      end
      object Label11: TLabel
        Left = 184
        Top = 0
        Width = 67
        Height = 13
        Caption = 'Data emiss'#227'o:'
      end
      object LaFornecedor: TLabel
        Left = 300
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Emitente:'
        Enabled = False
      end
      object LaTransportador: TLabel
        Left = 322
        Top = 80
        Width = 75
        Height = 13
        Caption = 'Transportadora:'
        Enabled = False
      end
      object LaCliInt: TLabel
        Left = 8
        Top = 39
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
        Enabled = False
      end
      object Label9: TLabel
        Left = 11
        Top = 0
        Width = 36
        Height = 13
        Caption = 'Pedido:'
      end
      object LaSitPedido: TLabel
        Left = 108
        Top = 0
        Width = 6
        Height = 13
        Caption = '0'
        Visible = False
      end
      object Label108: TLabel
        Left = 8
        Top = 80
        Width = 139
        Height = 13
        Caption = 'Indicador do tipo de frete [F4]'
        FocusControl = EdIND_FRT
      end
      object Label16: TLabel
        Left = 796
        Top = 0
        Width = 171
        Height = 13
        Caption = 'Indicador do tipo de pagamento [F4]'
        FocusControl = EdIND_PGTO
      end
      object Label18: TLabel
        Left = 445
        Top = 39
        Width = 54
        Height = 13
        Caption = '$ Produtos:'
      end
      object Label20: TLabel
        Left = 537
        Top = 39
        Width = 74
        Height = 13
        Caption = '$ Frete na NFe:'
      end
      object Label22: TLabel
        Left = 629
        Top = 39
        Width = 46
        Height = 13
        Caption = '$ Seguro:'
      end
      object Label28: TLabel
        Left = 721
        Top = 39
        Width = 75
        Height = 13
        Caption = '$ Desp. Acess.:'
      end
      object Label29: TLabel
        Left = 813
        Top = 39
        Width = 58
        Height = 13
        Caption = '$ Desconto:'
      end
      object Label7: TLabel
        Left = 905
        Top = 39
        Width = 53
        Height = 13
        Caption = '$ Total NF:'
        Enabled = False
      end
      object TPDT_E_S: TdmkEditDateTimePicker
        Left = 65
        Top = 16
        Width = 113
        Height = 21
        Date = 40060.000000000000000000
        Time = 0.661368159722769600
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPDT_DOC: TdmkEditDateTimePicker
        Left = 184
        Top = 16
        Width = 112
        Height = 21
        Date = 40060.000000000000000000
        Time = 0.661368159722769600
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdFornecedor: TdmkEditCB
        Left = 300
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFornecedorChange
        DBLookupComboBox = CBFornecedor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornecedor: TdmkDBLookupComboBox
        Left = 360
        Top = 16
        Width = 433
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOME_E_DOC_ENTIDADE'
        ListSource = DsFornecedores
        TabOrder = 4
        dmkEditCB = EdFornecedor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 67
        Top = 55
        Width = 374
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOME'
        ListSource = DsCI
        TabOrder = 8
        dmkEditCB = EdCliInt
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCliInt: TdmkEditCB
        Left = 8
        Top = 55
        Width = 55
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransportador: TdmkDBLookupComboBox
        Left = 381
        Top = 96
        Width = 452
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOME_E_DOC_ENTIDADE'
        ListSource = DsTransportadores
        TabOrder = 18
        dmkEditCB = EdTransportador
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTransportador: TdmkEditCB
        Left = 322
        Top = 96
        Width = 55
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransportador
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdPedido: TdmkEdit
        Left = 9
        Top = 16
        Width = 52
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdIND_FRT: TdmkEdit
        Left = 8
        Top = 96
        Width = 21
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '9'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '-1'
        QryCampo = 'IND_PGTO'
        UpdCampo = 'IND_PGTO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = -1
        ValWarn = False
        OnChange = EdIND_FRTChange
        OnKeyDown = EdIND_FRTKeyDown
      end
      object EdIND_FRT_TXT: TdmkEdit
        Left = 30
        Top = 96
        Width = 291
        Height = 21
        ReadOnly = True
        TabOrder = 16
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdIND_PGTO: TdmkEdit
        Left = 796
        Top = 16
        Width = 21
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '4'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '-1'
        QryCampo = 'IND_PGTO'
        UpdCampo = 'IND_PGTO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = -1
        ValWarn = False
        OnChange = EdIND_PGTOChange
        OnKeyDown = EdIND_PGTOKeyDown
      end
      object EdIND_PGTO_TXT: TdmkEdit
        Left = 818
        Top = 16
        Width = 179
        Height = 21
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdVL_MERC: TdmkEdit
        Left = 445
        Top = 55
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = CalculaValorTotalDocumento
      end
      object EdVL_FRT: TdmkEdit
        Left = 537
        Top = 55
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = CalculaValorTotalDocumento
      end
      object EdVL_SEG: TdmkEdit
        Left = 629
        Top = 55
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = CalculaValorTotalDocumento
      end
      object EdVL_OUT_DA: TdmkEdit
        Left = 721
        Top = 55
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = CalculaValorTotalDocumento
      end
      object EdVL_DESC: TdmkEdit
        Left = 809
        Top = 55
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = CalculaValorTotalDocumento
      end
      object EdVL_DOC: TdmkEdit
        Left = 905
        Top = 55
        Width = 90
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 14
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 120
      Width = 1008
      Height = 207
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object LaCHV_NFE: TLabel
        Left = 8
        Top = 26
        Width = 150
        Height = 13
        Caption = 'Chave de acesso da NF-e (VP):'
      end
      object LaCOD_MOD: TLabel
        Left = 424
        Top = 25
        Width = 38
        Height = 13
        Caption = 'Modelo:'
        Enabled = False
      end
      object LaSER: TLabel
        Left = 467
        Top = 25
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
        Enabled = False
      end
      object LaNUM_DOC: TLabel
        Left = 500
        Top = 26
        Width = 57
        Height = 13
        Caption = 'N'#250'mero NF:'
        Enabled = False
      end
      object SbNF_VP: TSpeedButton
        Left = 568
        Top = 38
        Width = 25
        Height = 25
        Caption = '<>'
        OnClick = SbNF_VPClick
      end
      object Label12: TLabel
        Left = 600
        Top = 25
        Width = 125
        Height = 13
        Caption = 'Atrelamento com XML BD:'
      end
      object Label26: TLabel
        Left = 8
        Top = 104
        Width = 210
        Height = 13
        Caption = 'C'#243'digo da situa'#231#227'o do documento fiscal [F4]'
        FocusControl = EdCOD_SIT
      end
      object Label4: TLabel
        Left = 9
        Top = 63
        Width = 55
        Height = 13
        Caption = '$ BC ICMS:'
      end
      object Label14: TLabel
        Left = 101
        Top = 63
        Width = 38
        Height = 13
        Caption = '$ ICMS:'
        Enabled = False
      end
      object Label15: TLabel
        Left = 193
        Top = 63
        Width = 72
        Height = 13
        Caption = '$ BC ICMS ST:'
      end
      object Label24: TLabel
        Left = 285
        Top = 63
        Width = 55
        Height = 13
        Caption = '$ ICMS ST:'
        Enabled = False
      end
      object Label27: TLabel
        Left = 377
        Top = 63
        Width = 25
        Height = 13
        Caption = '$ IPI:'
        Enabled = False
      end
      object Label32: TLabel
        Left = 469
        Top = 63
        Width = 29
        Height = 13
        Caption = '$ PIS:'
        Enabled = False
      end
      object Label35: TLabel
        Left = 561
        Top = 63
        Width = 51
        Height = 13
        Caption = '$ COFINS:'
        Enabled = False
      end
      object Label8: TLabel
        Left = 653
        Top = 63
        Width = 46
        Height = 13
        Caption = '$ PIS ST:'
        Enabled = False
      end
      object Label21: TLabel
        Left = 745
        Top = 63
        Width = 68
        Height = 13
        Caption = '$ COFINS ST:'
        Enabled = False
      end
      object SbChave: TSpeedButton
        Left = 384
        Top = 38
        Width = 25
        Height = 25
        Caption = '>'
        OnClick = SbChaveClick
      end
      object Label3: TLabel
        Left = 8
        Top = 148
        Width = 73
        Height = 13
        Caption = 'Movimenta'#231#227'o:'
      end
      object EdCHV_NFE: TdmkEdit
        Left = 8
        Top = 40
        Width = 373
        Height = 21
        MaxLength = 44
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCOD_MOD: TdmkEdit
        Left = 424
        Top = 40
        Width = 39
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        MaxLength = 2
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSER: TdmkEdit
        Left = 467
        Top = 40
        Width = 31
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        MaxLength = 3
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNUM_DOC: TdmkEdit
        Left = 500
        Top = 40
        Width = 66
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        MaxLength = 9
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNFe_StaLnk: TdmkEdit
        Left = 600
        Top = 40
        Width = 28
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        MaxLength = 9
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNFe_FatID: TdmkEdit
        Left = 632
        Top = 40
        Width = 39
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        MaxLength = 2
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNFe_FatNum: TdmkEdit
        Left = 675
        Top = 40
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        MaxLength = 3
        ReadOnly = True
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCOD_SIT: TdmkEdit
        Left = 8
        Top = 120
        Width = 21
        Height = 21
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '4'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '-1'
        QryCampo = 'IND_PGTO'
        UpdCampo = 'IND_PGTO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = -1
        ValWarn = False
        OnChange = EdCOD_SITChange
        OnKeyDown = EdCOD_SITKeyDown
      end
      object EdCOD_SIT_TXT: TdmkEdit
        Left = 30
        Top = 120
        Width = 803
        Height = 21
        ReadOnly = True
        TabOrder = 18
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkTipoNF: TdmkCheckBox
        Left = 8
        Top = 8
        Width = 162
        Height = 17
        Caption = 'Nota Fiscal Eletr'#244'nica (NF-e)'
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = CkTipoNFClick
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object EdVL_BC_ICMS: TdmkEdit
        Left = 9
        Top = 79
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdVL_ICMS: TdmkEdit
        Left = 101
        Top = 79
        Width = 90
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdVL_BC_ICMS_ST: TdmkEdit
        Left = 193
        Top = 79
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdVL_ICMS_ST: TdmkEdit
        Left = 285
        Top = 79
        Width = 90
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdVL_IPI: TdmkEdit
        Left = 377
        Top = 79
        Width = 90
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdVL_PIS: TdmkEdit
        Left = 469
        Top = 79
        Width = 90
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 13
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdVL_COFINS: TdmkEdit
        Left = 561
        Top = 79
        Width = 90
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 14
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdVL_PIS_ST: TdmkEdit
        Left = 653
        Top = 79
        Width = 90
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 15
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdVL_COFINS_ST: TdmkEdit
        Left = 745
        Top = 79
        Width = 90
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 16
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdRegrFiscal: TdmkEditCB
        Left = 88
        Top = 144
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 19
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBRegrFiscal
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBRegrFiscal: TdmkDBLookupComboBox
        Left = 148
        Top = 144
        Width = 685
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFisRegCad
        TabOrder = 20
        dmkEditCB = EdRegrFiscal
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 64
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 6
    object GroupBox1: TGroupBox
      Left = 100
      Top = 0
      Width = 908
      Height = 64
      Align = alClient
      Caption = ' Dados da Ficha RMP:'
      Enabled = False
      TabOrder = 0
      object Label5: TLabel
        Left = 96
        Top = 20
        Width = 38
        Height = 13
        Caption = 'Tipo ID:'
      end
      object Label13: TLabel
        Left = 264
        Top = 20
        Width = 32
        Height = 13
        Caption = 'IME-C:'
      end
      object Label25: TLabel
        Left = 12
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label2: TLabel
        Left = 180
        Top = 20
        Width = 54
        Height = 13
        Caption = 'ID Entrada:'
      end
      object EdMovFatID: TdmkEdit
        Left = 96
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 264
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEdit
        Left = 12
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
      end
      object EdMovFatNum: TdmkEdit
        Left = 180
        Top = 36
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object RGTpEntrd: TRadioGroup
      Left = 0
      Top = 0
      Width = 100
      Height = 64
      Align = alLeft
      Caption = ' Tipo de entrada: '
      ItemIndex = 0
      Items.Strings = (
        'Compra'
        'Remessa')
      TabOrder = 1
      OnClick = RGTpEntrdClick
    end
  end
  object QrMotorista: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 244
    Top = 40
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsMotorista: TDataSource
    DataSet = QrMotorista
    Left = 244
    Top = 88
  end
  object QrFornecedores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Tipo, ent.CNPJ, ent.CPF,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,'
      'CONCAT(IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome),'
      '  " - ", IF(ent.Tipo=0, ent.CNPJ, ent.CPF)) NOME_E_DOC_ENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece2="V"'
      'ORDER BY NOME'
      '')
    Left = 628
    Top = 8
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedoresNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrFornecedoresTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrFornecedoresCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrFornecedoresCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrFornecedoresNOME_E_DOC_ENTIDADE: TWideStringField
      FieldName = 'NOME_E_DOC_ENTIDADE'
      Size = 121
    end
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 628
    Top = 60
  end
  object QrTransportadores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Tipo, ent.CNPJ, ent.CPF,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,'
      'CONCAT(IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome),'
      '  " - ", IF(ent.Tipo=0, ent.CNPJ, ent.CPF)) NOME_E_DOC_ENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece3='#39'V'#39
      'ORDER BY NOME')
    Left = 68
    Top = 4
    object QrTransportadoresTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrTransportadoresCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTransportadoresCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTransportadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportadoresNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrTransportadoresNOME_E_DOC_ENTIDADE: TWideStringField
      FieldName = 'NOME_E_DOC_ENTIDADE'
      Size = 121
    end
  end
  object DsTransportadores: TDataSource
    DataSet = QrTransportadores
    Left = 96
    Top = 4
  end
  object QrCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NOME'
      'FROM entidades'
      'WHERE Cliente2='#39'V'#39
      'ORDER BY Nome')
    Left = 408
    Top = 4
    object QrCITipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCICNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrCICPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCINOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
  end
  object DsCI: TDataSource
    DataSet = QrCI
    Left = 408
    Top = 52
  end
  object Qr00_NFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, IDCtrl,'
      'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk,'
      'ide_mod, ide_serie, ide_nNF  '
      'FROM nfecaba '
      'WHERE FatID=53 '
      'AND Empresa=-11 '
      'AND ide_mod>0 '
      'AND ide_serie>0 '
      'AND ide_nNF>0 ')
    Left = 478
    Top = 9
    object Qr00_NFeCabAFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object Qr00_NFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object Qr00_NFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object Qr00_NFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object Qr00_NFeCabAAtrelaFatID: TIntegerField
      FieldName = 'AtrelaFatID'
      Required = True
    end
    object Qr00_NFeCabAAtrelaStaLnk: TSmallintField
      FieldName = 'AtrelaStaLnk'
      Required = True
    end
    object Qr00_NFeCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
      Required = True
    end
    object Qr00_NFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      Required = True
    end
    object Qr00_NFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Required = True
    end
    object Qr00_NFeCabAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object Qr00_NFeCabAAtrelaFatNum: TIntegerField
      FieldName = 'AtrelaFatNum'
      Required = True
    end
    object Qr00_NFeCabAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object Qr00_NFeCabAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
  end
  object QrFisRegCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.ModeloNF,'
      'frc.Nome, frc.Financeiro, imp.Nome NO_MODELO_NF,'
      'frc.TipoMov'
      'FROM fisregcad frc'
      'LEFT JOIN imprime imp ON imp.Codigo=frc.ModeloNF'
      'ORDER BY Nome')
    Left = 728
    Top = 8
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFisRegCadModeloNF: TIntegerField
      FieldName = 'ModeloNF'
      Required = True
    end
    object QrFisRegCadNO_MODELO_NF: TWideStringField
      FieldName = 'NO_MODELO_NF'
      Size = 100
    end
    object QrFisRegCadFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
    object QrFisRegCadTipoMov: TSmallintField
      FieldName = 'TipoMov'
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 728
    Top = 60
  end
end
