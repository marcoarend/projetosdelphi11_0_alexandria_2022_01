unit ModEFDII;

interface

uses
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables, Vcl.Menus,
  Windows, Messages, Graphics, Controls, Forms, Dialogs,
  UnDmkEnums, DBCtrls, Grids, DBGrids (*,
  ExtCtrls, Buttons, StdCtrls, ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus,
  UnDmkProcFunc, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, frxCross,
  dmkDBGridZTO, AppListas, UnGrl_Consts, UnComps_Vars, UnGrl_Geral, UnAppEnums,
  UnProjGroup_Consts, UnProjGroup_Vars, UnInternalConsts3*);

type
  TEFDTypLancto = (efdtplctIndef=0,  efdtplctEntrada=1, efdtplctFrete=2);
  TEFDJanela = (efdtjanIndef=0,  efdtjanCompraMP=1, efdtjanCompraUC=2);
  //
  TDmEFDII = class(TDataModule)
    QrEfdInnNFsCab: TMySQLQuery;
    QrEfdInnNFsCabNO_TER: TWideStringField;
    QrEfdInnNFsCabMovFatID: TIntegerField;
    QrEfdInnNFsCabMovFatNum: TIntegerField;
    QrEfdInnNFsCabMovimCod: TIntegerField;
    QrEfdInnNFsCabEmpresa: TIntegerField;
    QrEfdInnNFsCabControle: TIntegerField;
    QrEfdInnNFsCabTerceiro: TIntegerField;
    QrEfdInnNFsCabPecas: TFloatField;
    QrEfdInnNFsCabPesoKg: TFloatField;
    QrEfdInnNFsCabAreaM2: TFloatField;
    QrEfdInnNFsCabAreaP2: TFloatField;
    QrEfdInnNFsCabValorT: TFloatField;
    QrEfdInnNFsCabMotorista: TIntegerField;
    QrEfdInnNFsCabPlaca: TWideStringField;
    QrEfdInnNFsCabCOD_MOD: TSmallintField;
    QrEfdInnNFsCabCOD_SIT: TSmallintField;
    QrEfdInnNFsCabSER: TIntegerField;
    QrEfdInnNFsCabNUM_DOC: TIntegerField;
    QrEfdInnNFsCabCHV_NFE: TWideStringField;
    QrEfdInnNFsCabNFeStatus: TIntegerField;
    QrEfdInnNFsCabDT_DOC: TDateField;
    QrEfdInnNFsCabDT_E_S: TDateField;
    QrEfdInnNFsCabVL_DOC: TFloatField;
    QrEfdInnNFsCabIND_PGTO: TWideStringField;
    QrEfdInnNFsCabVL_DESC: TFloatField;
    QrEfdInnNFsCabVL_ABAT_NT: TFloatField;
    QrEfdInnNFsCabVL_MERC: TFloatField;
    QrEfdInnNFsCabIND_FRT: TWideStringField;
    QrEfdInnNFsCabVL_FRT: TFloatField;
    QrEfdInnNFsCabVL_SEG: TFloatField;
    QrEfdInnNFsCabVL_OUT_DA: TFloatField;
    QrEfdInnNFsCabVL_BC_ICMS: TFloatField;
    QrEfdInnNFsCabVL_ICMS: TFloatField;
    QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField;
    QrEfdInnNFsCabVL_ICMS_ST: TFloatField;
    QrEfdInnNFsCabVL_IPI: TFloatField;
    QrEfdInnNFsCabVL_PIS: TFloatField;
    QrEfdInnNFsCabVL_COFINS: TFloatField;
    QrEfdInnNFsCabVL_PIS_ST: TFloatField;
    QrEfdInnNFsCabVL_COFINS_ST: TFloatField;
    QrEfdInnNFsCabNFe_FatID: TIntegerField;
    QrEfdInnNFsCabNFe_FatNum: TIntegerField;
    QrEfdInnNFsCabNFe_StaLnk: TSmallintField;
    QrEfdInnNFsCabVSVmcWrn: TSmallintField;
    QrEfdInnNFsCabVSVmcObs: TWideStringField;
    QrEfdInnNFsCabVSVmcSeq: TWideStringField;
    QrEfdInnNFsCabVSVmcSta: TSmallintField;
    QrEfdInnNFsIts: TMySQLQuery;
    QrEfdInnNFsItsMovFatID: TIntegerField;
    QrEfdInnNFsItsMovFatNum: TIntegerField;
    QrEfdInnNFsItsMovimCod: TIntegerField;
    QrEfdInnNFsItsEmpresa: TIntegerField;
    QrEfdInnNFsItsMovimNiv: TIntegerField;
    QrEfdInnNFsItsMovimTwn: TIntegerField;
    QrEfdInnNFsItsControle: TIntegerField;
    QrEfdInnNFsItsConta: TIntegerField;
    QrEfdInnNFsItsGraGru1: TIntegerField;
    QrEfdInnNFsItsGraGruX: TIntegerField;
    QrEfdInnNFsItsprod_vProd: TFloatField;
    QrEfdInnNFsItsprod_vFrete: TFloatField;
    QrEfdInnNFsItsprod_vSeg: TFloatField;
    QrEfdInnNFsItsprod_vOutro: TFloatField;
    QrEfdInnNFsItsQTD: TFloatField;
    QrEfdInnNFsItsUNID: TWideStringField;
    QrEfdInnNFsItsVL_ITEM: TFloatField;
    QrEfdInnNFsItsVL_DESC: TFloatField;
    QrEfdInnNFsItsIND_MOV: TWideStringField;
    QrEfdInnNFsItsCST_ICMS: TIntegerField;
    QrEfdInnNFsItsCFOP: TIntegerField;
    QrEfdInnNFsItsCOD_NAT: TWideStringField;
    QrEfdInnNFsItsVL_BC_ICMS: TFloatField;
    QrEfdInnNFsItsALIQ_ICMS: TFloatField;
    QrEfdInnNFsItsVL_ICMS: TFloatField;
    QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField;
    QrEfdInnNFsItsALIQ_ST: TFloatField;
    QrEfdInnNFsItsVL_ICMS_ST: TFloatField;
    QrEfdInnNFsItsIND_APUR: TWideStringField;
    QrEfdInnNFsItsCST_IPI: TWideStringField;
    QrEfdInnNFsItsCOD_ENQ: TWideStringField;
    QrEfdInnNFsItsVL_BC_IPI: TFloatField;
    QrEfdInnNFsItsALIQ_IPI: TFloatField;
    QrEfdInnNFsItsVL_IPI: TFloatField;
    QrEfdInnNFsItsCST_PIS: TSmallintField;
    QrEfdInnNFsItsVL_BC_PIS: TFloatField;
    QrEfdInnNFsItsALIQ_PIS_p: TFloatField;
    QrEfdInnNFsItsQUANT_BC_PIS: TFloatField;
    QrEfdInnNFsItsALIQ_PIS_r: TFloatField;
    QrEfdInnNFsItsVL_PIS: TFloatField;
    QrEfdInnNFsItsCST_COFINS: TSmallintField;
    QrEfdInnNFsItsVL_BC_COFINS: TFloatField;
    QrEfdInnNFsItsALIQ_COFINS_p: TFloatField;
    QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField;
    QrEfdInnNFsItsALIQ_COFINS_r: TFloatField;
    QrEfdInnNFsItsVL_COFINS: TFloatField;
    QrEfdInnNFsItsCOD_CTA: TWideStringField;
    QrEfdInnNFsItsVL_ABAT_NT: TFloatField;
    QrEfdInnNFsItsDtCorrApo: TDateTimeField;
    QrEfdInnNFsItsxLote: TWideStringField;
    QrEfdInnNFsItsOri_IPIpIPI: TFloatField;
    QrEfdInnNFsItsOri_IPIvIPI: TFloatField;
    QrEfdInnNFsItsNO_PRD_TAM_COR: TWideStringField;
    QrEfdInnNFsItsSIGLAUNIDMED: TWideStringField;
    QrEfdInnNFsItsGerBxaEstq: TSmallintField;
    QrEfdInnNFsItsNCM: TWideStringField;
    QrEfdInnNFsItsUnidMed: TIntegerField;
    QrEfdInnNFsItsEx_TIPI: TWideStringField;
    QrEfdInnNFsItsGrandeza: TSmallintField;
    QrEfdInnNFsItsTipo_Item: TSmallintField;
    //procedure PMFiscalPopup(Sender: TObject);
    //procedure IncluiDocumento1Click(Sender: TObject);
  private
    { Private declarations }
    (*FMovFatID, FFrtFatID: Integer;
    FEFDJanela: TEFDJanela;
    FQrMaeCab: TmySQLQuery;
    FCabFld_MovimCod, FCabFld_MovFatNum: String;*)
  public
    { Public declarations }
{
    procedure ConfiguraJanelaMae(
              EFDJanela: TEFDJanela;
              MovFatID, FrtFatID: Integer;
              QrMaeCabb: TmySQLQuery;
              FCabFld_MovimCod, FCabFld_MovFatNum: String;
              GradeCab, GradeIts: TDBGrid);
}

(*
    function  DefineFatID(EFDTypLancto: TEFDTypLancto): Integer;
*)

    procedure ExcluiDocumento(MovFatID, MovFatNum: Integer; QrEfdInnNFsCab, QrEfdInnNFsIts: TmySQLQuery;
  QrEfdInnNFsCabControle: TIntegerField);
var

{
    procedure MostraFormEfdInnNFsCab(EFDTypLancto: TEFDTypLancto; SQLType: TSQLType;
  Mov_Codigo_Value, Mov_MovimCod_Value, Mov_Empresa_Value: Integer;
Mov_Pecas_Value, Mov_PesoKg_Value, Mov_AreaM2_Value, Mov_AreaP2_Value,
Mov_ValorT_Value: Double; Mov_Motorista_Value: Integer;
Mov_Placa_Value: String; ide_Mod: Integer; Mov_emi_serie_Value,
Mov_emi_subserie_Value, Mov_emi_nNF_Value: String;
Mov_DtCompra_Value, Mov_DtEntrada_Value: TDateTime; Mov_Fornecedor_Value,
Mov_Transporta_Value: Integer);
    procedure MostraFormEfdInnNFsIts(EFDTypLancto: TEFDTypLancto; SQLType: TSQLType;
  Mov_Codigo_Value, Mov_MovimCod_Value, Mov_Empresa_Value: Integer);
}
    procedure MostraFormEfdInnNFsIts(SQLType: TSQLType; QrEfdInnNFsCab, QrEfdInnNFsIts: TmySQLQuery);

    procedure ReopenEfdInnNFsIts_All(Controle, Conta: Integer; QrEfdInnNFsCab: TmySQLQuery);
    procedure ReopenEfdInnNFsCab(MovFatID, MovFatNum, Controle: Integer;
              QrEfdInnNFsCab: TmySQLQuery);
  end;

var
  DmEFDII: TDmEFDII;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TDmEFDII }

uses DmkGeral, Module, UMySQLModule, UnGOTOy, UnMyObjects, DmkDAC_PF, MyDBCheck,
  EfdInnNFsCab, EfdInnNFsIts,
  // BlueDerm
  VSInnCab, PQE, ModProd;

{
procedure TDmEFDII.ConfiguraJanelaMae(EFDJanela: TEFDJanela; MovFatID, FrtFatID: Integer;
  QrMaeCabb: TmySQLQuery; FCabFld_MovimCod, FCabFld_MovFatNum: String; GradeCab,
  GradeIts: TDBGrid);
begin
end;
}

{
function TDmEFDII.DefineFatID(EFDTypLancto: TEFDTypLancto): Integer;
const
  sProcName = 'TDmEFDII.DefineFatID()';
begin
  case EFDTypLancto of
    //efdtplctIndef=0,
    (*1*)efdtplctEntrada: Result := FMovFatID;
    (*2*)efdtplctFrete:   Result := FFrtFatID;
    else
    begin
      Result := -999999999;
      Geral.MB_Erro('"EFDTypLancto" n�o definido em ' + sProcName);
    end;
  end;
end;
}

procedure TDmEFDII.ExcluiDocumento(MovFatID, MovFatNum: Integer; QrEfdInnNFsCab, QrEfdInnNFsIts: TmySQLQuery;
  QrEfdInnNFsCabControle: TIntegerField);
var
  Controle: Integer;
begin
  if QrEfdInnNFsIts.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Exclus�o abortada! Documento com item!');
  end else
  begin
    if QrEfdInnNFsCab.RecordCount > 0 then
    begin
      if UMyMod.ExcluiRegistroInt1('Confirma a retirada do DF-e selecionado?',
      'EfdInnNFsCab', 'Controle', QrEfdInnNFsCabControle.Value, Dmod.MyDB) = ID_YES then
      begin
        Controle := GOTOy.LocalizaPriorNextIntQr(QrEfdInnNFsCab,
          QrEfdInnNFsCabControle, QrEfdInnNFsCabControle.Value);
        ReopenEfdInnNFsCab(MovFatID, MovFatNum, Controle, QrEfdInnNFsCab);
      end;
    end;
  end;
end;

(*procedure TDmEFDII.IncluiDocumento1Click(Sender: TObject);
begin
{
  case FEFDJanela of
    //efdtjanIndef=0,
    efdtjanCompraMP: FmVSInnCab.MostraFormEfdInnNFsCab(stIns);
    begin
    =1, efdtjanCompraUC=2);
}
end;
*)
(*
procedure TDmEFDII.MostraFormEfdInnNFsCab(EFDTypLancto: TEFDTypLancto; SQLType: TSQLType;
  Mov_Codigo_Value, Mov_MovimCod_Value, Mov_Empresa_Value: Integer;
Mov_Pecas_Value, Mov_PesoKg_Value, Mov_AreaM2_Value, Mov_AreaP2_Value,
Mov_ValorT_Value: Double; Mov_Motorista_Value: Integer;
Mov_Placa_Value: String; ide_Mod: Integer; Mov_emi_serie_Value,
Mov_emi_subserie_Value, Mov_emi_nNF_Value: String;
Mov_DtCompra_Value, Mov_DtEntrada_Value: TDateTime; Mov_Fornecedor_Value,
Mov_Transporta_Value: Integer);

var
  NewControle, Controle: Integer;
begin
{$IfDef sAllVS}
  NewControle := 0;
  Controle := QrEfdInnNFsCabControle.Value;
  //
  if DBCheck.CriaFm(TFmEfdInnNFsCab, FmEfdInnNFsCab, afmoNegarComAviso) then
  begin
    FmEfdInnNFsCab.ImgTipo.SQLType := SQLType;
    FmEfdInnNFsCab.FQrIts                         := QrEfdInnNFsCab;
    FmEfdInnNFsCab.EdMovFatID.ValueVariant        := DefineFatID(EFDTypLancto);
    FmEfdInnNFsCab.EdMovFatNum.ValueVariant       := Mov_Codigo_Value;
    FmEfdInnNFsCab.EdMovimCod.ValueVariant        := Mov_MovimCod_Value;
    FmEfdInnNFsCab.EdEmpresa.ValueVariant         := Mov_Empresa_Value;
    //FmEfdInnNFsCab.EdSerieFch.ValueVariant        := QrVSInnItsSerieFch.Value;
    //FmEfdInnNFsCab.EdFicha.ValueVariant           := QrVSInnItsFicha.Value;
    FmEfdInnNFsCab.EdControle.ValueVariant           := 0;
    if (SQLType = stIns) and (QrVSInnIts.RecordCount > 0) then
    begin
      FmEfdInnNFsCab.EdControle.ValueVariant      := 0;
      FmEfdInnNFsCab.EdPecas.ValueVariant         := Mov_Pecas_Value;
      FmEfdInnNFsCab.EdPesoKg.ValueVariant        := Mov_PesoKg_Value;
      FmEfdInnNFsCab.EdAreaM2.ValueVariant        := Mov_AreaM2_Value;
      FmEfdInnNFsCab.EdAreaP2.ValueVariant        := Mov_AreaP2_Value;
      FmEfdInnNFsCab.EdValorT.ValueVariant        := Mov_ValorT_Value;
      FmEfdInnNFsCab.EdMotorista.ValueVariant     := Mov_Motorista_Value;
      FmEfdInnNFsCab.CBMotorista.KeyValue         := Mov_Motorista_Value;
      FmEfdInnNFsCab.EdPlaca.ValueVariant         := Mov_Placa_Value;
      FmEfdInnNFsCab.EdCOD_SIT.ValueVariant       := 0; // Documento regular
      FmEfdInnNFsCab.EdCOD_MOD.ValueVariant       := 55;
      FmEfdInnNFsCab.EdSER.ValueVariant           := Mov_emi_serie_Value;
      FmEfdInnNFsCab.EdNUM_DOC.ValueVariant       := Mov_emi_nNF_Value;
      FmEfdInnNFsCab.EdCHV_NFE.ValueVariant       := '';
      //FmEfdInnNFsCab.EdNFeStatus.ValueVariant
      FmEfdInnNFsCab.TPDT_DOC.Date                := Mov_DtCompra_Value;
      FmEfdInnNFsCab.TPDT_E_S.Date                := Mov_DtEntrada_Value;

      FmEfdInnNFsCab.EdFornecedor.ValueVariant    := Mov_Fornecedor_Value;
      FmEfdInnNFsCab.CBFornecedor.KeyValue        := Mov_Fornecedor_Value;

      FmEfdInnNFsCab.EdTransportador.ValueVariant    := Mov_Transporta_Value;
      FmEfdInnNFsCab.CBTransportador.KeyValue        := Mov_Transporta_Value;

      FmEfdInnNFsCab.EdVL_DOC.ValueVariant        := Mov_ValorT_Value;
      FmEfdInnNFsCab.EdIND_PGTO.ValueVariant      := 1; //� Prazo
      FmEfdInnNFsCab.EdVL_DESC.ValueVariant       := 0.00;
      //FmEfdInnNFsCab.EdVL_ABAT_NT.ValueVariant    := 0.00;  // Abatimento n�o comercia
      FmEfdInnNFsCab.EdVL_MERC.ValueVariant       := Mov_ValorT_Value;
      //FmEfdInnNFsCab.EdIND_FRT.ValueVariant       := 1; // Contrata��o de frete por conta do destinat�rio
{
      FmEfdInnNFsCab.EdVL_FRT.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_SEG.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_OUT_DA.ValueVariant     := 0.00;
      FmEfdInnNFsCab.EdVL_BC_ICMS.ValueVariant    := Mov_ValorT.Value;;
      FmEfdInnNFsCab.EdVL_ICMS.ValueVariant       := 0.00;
      FmEfdInnNFsCab.EdVL_BC_ICMS_ST.ValueVariant := 0.00;
      FmEfdInnNFsCab.EdVL_ICMS_ST.ValueVariant    := 0.00;
      FmEfdInnNFsCab.EdVL_IPI.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_PIS.ValueVariant        := 0.00;
      FmEfdInnNFsCab.EdVL_COFINS.ValueVariant     := 0.00;
      FmEfdInnNFsCab.EdVL_PIS_ST.ValueVariant     := 0.00;
      FmEfdInnNFsCab.EdVL_COFINS_ST.ValueVariant  := 0.00;
}
      FmEfdInnNFsCab.EdNFe_FatID.ValueVariant         := 0;
      FmEfdInnNFsCab.EdNFe_FatNum.ValueVariant        := 0;
      FmEfdInnNFsCab.EdNFe_StaLnk.ValueVariant        := 0;
      //
    end else
    begin
      FmEfdInnNFsCab.EdControle.ValueVariant      := QrEfdInnNFsCabControle.Value;
      FmEfdInnNFsCab.EdPecas.ValueVariant         := QrEfdInnNFsCabPecas.Value;
      FmEfdInnNFsCab.EdPesoKg.ValueVariant        := QrEfdInnNFsCabPesoKg.Value;
      FmEfdInnNFsCab.EdAreaM2.ValueVariant        := QrEfdInnNFsCabAreaM2.Value;
      FmEfdInnNFsCab.EdAreaP2.ValueVariant        := QrEfdInnNFsCabAreaP2.Value;
      FmEfdInnNFsCab.EdValorT.ValueVariant        := QrEfdInnNFsCabValorT.Value;
      FmEfdInnNFsCab.EdMotorista.ValueVariant     := QrEfdInnNFsCabMotorista.Value;
      FmEfdInnNFsCab.CBMotorista.KeyValue         := QrEfdInnNFsCabMotorista.Value;
      FmEfdInnNFsCab.EdPlaca.ValueVariant         := QrEfdInnNFsCabPlaca.Value;
      FmEfdInnNFsCab.EdCOD_MOD.ValueVariant       := QrEfdInnNFsCabCOD_MOD.Value;
      FmEfdInnNFsCab.EdCOD_SIT.ValueVariant       := QrEfdInnNFsCabCOD_SIT.Value;
      FmEfdInnNFsCab.EdSER.ValueVariant           := QrEfdInnNFsCabSER.Value;
      FmEfdInnNFsCab.EdNUM_DOC.ValueVariant       := QrEfdInnNFsCabNUM_DOC.Value;
      FmEfdInnNFsCab.EdCHV_NFE.ValueVariant       := QrEfdInnNFsCabCHV_NFE.Value;
      //FmEfdInnNFsCab.EdNFeStatus.ValueVariant     := QrVSInnNFsCEdNFeStaab.Value;
      FmEfdInnNFsCab.TPDT_DOC.Date                := QrEfdInnNFsCabDT_DOC.Value;
      FmEfdInnNFsCab.TPDT_E_S.Date                := QrEfdInnNFsCabDT_E_S.Value;
      FmEfdInnNFsCab.EdVL_DOC.ValueVariant        := QrEfdInnNFsCabVL_DOC.Value;
      FmEfdInnNFsCab.EdIND_PGTO.ValueVariant      := QrEfdInnNFsCabIND_PGTO.Value;
      FmEfdInnNFsCab.EdVL_DESC.ValueVariant       := QrEfdInnNFsCabVL_DESC.Value;
      //FmEfdInnNFsCab.EdVL_ABAT_NT.ValueVariant    := 0.00;  // Abatimento n�o comercia
      FmEfdInnNFsCab.EdVL_MERC.ValueVariant       := QrEfdInnNFsCabVL_MERC.Value;
      FmEfdInnNFsCab.EdIND_FRT.ValueVariant       := QrEfdInnNFsCabIND_FRT.Value;
      FmEfdInnNFsCab.EdVL_FRT.ValueVariant        := QrEfdInnNFsCabVL_FRT.Value;
      FmEfdInnNFsCab.EdVL_SEG.ValueVariant        := QrEfdInnNFsCabVL_SEG.Value;
      FmEfdInnNFsCab.EdVL_OUT_DA.ValueVariant     := QrEfdInnNFsCabVL_OUT_DA.Value;
      FmEfdInnNFsCab.EdVL_BC_ICMS.ValueVariant    := QrEfdInnNFsCabVL_BC_ICMS.Value;
      FmEfdInnNFsCab.EdVL_ICMS.ValueVariant       := QrEfdInnNFsCabVL_ICMS.Value;
      FmEfdInnNFsCab.EdVL_BC_ICMS_ST.ValueVariant := QrEfdInnNFsCabVL_BC_ICMS_ST.Value;
      FmEfdInnNFsCab.EdVL_ICMS_ST.ValueVariant    := QrEfdInnNFsCabVL_ICMS_ST.Value;
      FmEfdInnNFsCab.EdVL_IPI.ValueVariant        := QrEfdInnNFsCabVL_IPI.Value;
      FmEfdInnNFsCab.EdVL_PIS.ValueVariant        := QrEfdInnNFsCabVL_PIS.Value;
      FmEfdInnNFsCab.EdVL_COFINS.ValueVariant     := QrEfdInnNFsCabVL_COFINS.Value;
      FmEfdInnNFsCab.EdVL_PIS_ST.ValueVariant     := QrEfdInnNFsCabVL_PIS_ST.Value;
      FmEfdInnNFsCab.EdVL_COFINS_ST.ValueVariant  := QrEfdInnNFsCabVL_COFINS.Value;
      FmEfdInnNFsCab.EdNFe_FatID.ValueVariant     := QrEfdInnNFsCabNFe_FatID.Value;
      FmEfdInnNFsCab.EdNFe_FatNum.ValueVariant    := QrEfdInnNFsCabNFe_FatNum.Value;
      FmEfdInnNFsCab.EdNFe_StaLnk.ValueVariant    := QrEfdInnNFsCabNFe_StaLnk.Value;
      //
    end;
    FmEfdInnNFsCab.ShowModal;
    NewControle := FmEfdInnNFsCab.FControle;
    FmEfdInnNFsCab.Destroy;
    //
    ReopenEfdInnNFsCab(NewControle);
    if (SQLType = stIns) and (NewControle > Controle) then
    begin
      if QrEfdInnNFsCabControle.Value = NewControle then
        MostraFormEfdInnNFsIts(stIns);
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;
*)

(*
procedure TDmEFDII.PMFiscalPopup(Sender: TObject);
begin
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiDocumento1, FQrMaeCab); //Mov_);
  MyObjects.HabilitaMenuItemItsUpd(AlteraDocumento1, QrEfdInnNFsCab);
  MyObjects.HabilitaMenuItemCabDel(ExcluiDocumento1, QrEfdInnNFsCab, QrEfdInnNFsIts);
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiItemdodocumento1, QrEfdInnNFsCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraoItemselecionadododocumento1, QrEfdInnNFsIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluioItemselecionadododocumento1, QrEfdInnNFsIts);
  //
end;
*)

procedure TDmEFDII.MostraFormEfdInnNFsIts(Empresa, FatID, FatNum: Integer;
SQLType: TSQLType; QrEfdInnNFsCab,
  QrEfdInnNFsIts: TmySQLQuery);
var
  NewConta, Conta, GraGru1: Integer;
  CST_B, IPI_CST, PIS_CST, COFINS_CST: Integer;
  ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq, COFINSRec_pAliq, ICMSSTRec_pAliq: Double;
begin
{$IfDef sAllVS}
  //NewConta := 0;
  //Conta := QrEfdInnNFsItsConta.Value;
  //
  ReopenEfdInnNFsCab();
  ReopenEfdInnNFsIts_Uni();
  //
  if DBCheck.CriaFm(TFmEfdInnNFsIts, FmEfdInnNFsIts, afmoNegarComAviso) then
  begin
    FmEfdInnNFsIts.ImgTipo.SQLType := SQLType;

    FmEfdInnNFsIts.FQrCab                         := QrEfdInnNFsCab;
    FmEfdInnNFsIts.FQrIts                         := QrEfdInnNFsIts;
    FmEfdInnNFsIts.EdEmpresa.ValueVariant         := QrVSInnCabEmpresa.Value;
    FmEfdInnNFsIts.EdMovFatID.ValueVariant        := FThisFatID;
    FmEfdInnNFsIts.EdMovFatNum.ValueVariant       := QrVSInnCabCodigo.Value;
    FmEfdInnNFsIts.EdMovimCod.ValueVariant        := QrVSInnCabMovimCod.Value;
    FmEfdInnNFsIts.EdMovimNiv.ValueVariant        := 0;
    FmEfdInnNFsIts.EdMovimTwn.ValueVariant        := 0;
    FmEfdInnNFsIts.EdControle.ValueVariant        := QrEfdInnNFsCabControle.Value;
    //FmEfdInnNFsIts.FQrIts                         := QrVSInnNFs;
    if SQLType = stIns then
    begin
      FmEfdInnNFsIts.EdConta.ValueVariant           := 0;

      GraGru1 := DmProd.ObtemGraGru1DeGraGruX(QrVSInnItsGraGruX.Value);

      FmEfdInnNFsIts.EdGraGruX.ValueVariant                       := QrVSInnItsGraGruX.Value;
      FmEfdInnNFsIts.CBGraGruX.KeyValue                           := QrVSInnItsGraGruX.Value;
      case QrVSInnItsGrandeza.Value of
        0: FmEfdInnNFsIts.EdQTD.ValueVariant                      := QrVSMovDifInfPecas.Value;
        2: FmEfdInnNFsIts.EdQTD.ValueVariant                      := QrVSMovDifInfPesoKg.Value;
        else Geral.MB_Aviso('Grandeza no cadastro grade da mat�ria-prima deve ser Pe�a ou Peso(kg)!');
      end;
      //FmEfdInnNFsIts.EdCFOP.Text                                  := '';
      if QrVSInnCabUF_Empresa.Value = QrVSInnCabUF_Fornece.Value then
        FmEfdInnNFsIts.EdCFOP.ValueVariant := Dmod.QrControleCreTrib_MP_CFOP_DE.Value
      else
        FmEfdInnNFsIts.EdCFOP.ValueVariant := Dmod.QrControleCreTrib_MP_CFOP_FE.Value;
      //
      FmEfdInnNFsIts.Edprod_vProd.ValueVariant                    := QrVSMovDifInfValorT.Value;
      FmEfdInnNFsIts.EdVL_ITEM.ValueVariant                       := QrVSMovDifInfValorT.Value;
      FmEfdInnNFsIts.EdVL_DESC.ValueVariant                       := 0.00;
      FmEfdInnNFsIts.EdIND_MOV.ValueVariant                       := '';
      {
      if Dmod.QrControleNaoRecupImposPQ.Value = 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
        'SELECT CST_B, IPI_CST, PIS_CST, COFINS_CST, ',
        'ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq,  ',
        'COFINSRec_pAliq  ',
        'FROM gragru1 ',
        'WHERE Nivel1=' + Geral.FF0(GraGru1),
        '']);
        //
        CST_B           := QrGraGru1CST_B.Value;
        IPI_CST         := QrGraGru1IPI_CST.Value;
        PIS_CST         := QrGraGru1PIS_CST.Value;
        COFINS_CST      := QrGraGru1COFINS_CST.Value;
        ICMSRec_pAliq   := QrGraGru1ICMSRec_pAliq.Value;
        ICMSSTRec_pAliq := 0.00;
        IPIRec_pAliq    := QrGraGru1IPIRec_pAliq.Value;
        PISRec_pAliq    := QrGraGru1PISRec_pAliq.Value;
        COFINSRec_pAliq := QrGraGru1COFINSRec_pAliq.Value;
      end else
      begin
      }
        CST_B           := Geral.IMV(Dmod.QrControleCreTrib_MP_ICMS_CST.VAlue);
        IPI_CST         := Geral.IMV(Dmod.QrControleCreTrib_MP_IPI_CST.VAlue);
        PIS_CST         := Geral.IMV(Dmod.QrControleCreTrib_MP_PIS_CST.VAlue);
        COFINS_CST      := Geral.IMV(Dmod.QrControleCreTrib_MP_COFINS_CST.VAlue);
        ICMSRec_pAliq   := Dmod.QrControleCreTrib_MP_ICMS_ALIQ.Value;
        ICMSSTRec_pAliq := Dmod.QrControleCreTrib_MP_ICMS_ALIQ_ST.Value;
        IPIRec_pAliq    := Dmod.QrControleCreTrib_MP_IPI_ALIQ.Value;
        PISRec_pAliq    := Dmod.QrControleCreTrib_MP_PIS_ALIQ.Value;
        COFINSRec_pAliq := Dmod.QrControleCreTrib_MP_COFINS_ALIQ.Value;
      {end;}
      FmEfdInnNFsIts.EdICMS_CST.ValueVariant                      := CST_B;
      //FmEfdInnNFsIts.EdCOD_NAT.ValueVariant                       := COD_NAT
      FmEfdInnNFsIts.EdVL_BC_ICMS.ValueVariant                    := QrVSMovDifInfValorT.Value;
      FmEfdInnNFsIts.EdALIQ_ICMS.ValueVariant                     := ICMSRec_pAliq;
      //FmEfdInnNFsIts.EdVL_ICMS.ValueVariant                       := VL_ICMS
      //FmEfdInnNFsIts.EdVL_BC_ICMS_ST.ValueVariant                 := VL_BC_ICMS_ST
      FmEfdInnNFsIts.EdALIQ_ST.ValueVariant                       := ICMSSTRec_pAliq;
      //FmEfdInnNFsIts.EdVL_ICMS_ST.ValueVariant                    := VL_ICMS_ST
      //FmEfdInnNFsIts.EdIND_APUR  .ValueVariant                    := IND_APUR
      FmEfdInnNFsIts.EdCST_IPI.ValueVariant                       := IPI_CST;
      //FmEfdInnNFsIts.EdCOD_ENQ   .ValueVariant                    := COD_ENQ
      //FmEfdInnNFsIts.EdVL_BC_IPI.ValueVariant                     := VL_BC_IPI
      FmEfdInnNFsIts.EdALIQ_IPI.ValueVariant                      := IPIRec_pAliq;
      //FmEfdInnNFsIts.EdVL_IPI.ValueVariant                        := VL_IPI
      FmEfdInnNFsIts.EdCST_PIS.ValueVariant                       := PIS_CST;
      //FmEfdInnNFsIts.EdVL_BC_PIS .ValueVariant                    := VL_BC_PIS
      FmEfdInnNFsIts.EdALIQ_PIS.ValueVariant                      := PISRec_pAliq;
      //FmEfdInnNFsIts.EdQUANT_BC_PIS.ValueVariant                  := QUANT_BC_PIS
      //FmEfdInnNFsIts.EdALIQ_PIS_r.ValueVariant                    := ALIQ_PIS_r
      //FmEfdInnNFsIts.EdVL_PIS.ValueVariant                        := VL_PIS
      FmEfdInnNFsIts.EdCST_COFINS.ValueVariant                    := COFINS_CST;
      //FmEfdInnNFsIts.EdVL_BC_COFINS.ValueVariant                  := VL_BC_COFINS
      FmEfdInnNFsIts.EdALIQ_COFINS.ValueVariant                   := COFINSRec_pAliq;
      //FmEfdInnNFsIts.EdQUANT_BC_COFINS.ValueVariant               := QUANT_BC_COFINS
      //FmEfdInnNFsIts.EdALIQ_COFINS_r.ValueVariant                 := ALIQ_COFINS_r
      //FmEfdInnNFsIts.EdVL_COFINS.ValueVariant                     := VL_COFINS
      //FmEfdInnNFsIts.EdCOD_CTA.ValueVariant                       := COD_CTA
      //FmEfdInnNFsIts.EdVL_ABAT_NT     .ValueVariant               := VL_ABAT_NT

    end else
    begin
      FmEfdInnNFsIts.EdConta.ValueVariant           := QrEfdInnNFsItsConta.Value;

      FmEfdInnNFsIts.EdGraGruX.ValueVariant                       := QrEfdInnNFsItsGraGruX.Value;
      FmEfdInnNFsIts.CBGraGruX.KeyValue                           := QrEfdInnNFsItsGraGruX.Value;
      FmEfdInnNFsIts.EdCFOP.Text                                  := Geral.FormataCFOP(Geral.FF0(QrEfdInnNFsItsCFOP.Value));
      //FmEfdInnNFsIts.EdCOD_NAT.ValueVariant                       :=
      FmEfdInnNFsIts.EdQTD.ValueVariant                           := QrEfdInnNFsItsQTD.Value;
      FmEfdInnNFsIts.EdIND_MOV.ValueVariant                       := QrEfdInnNFsItsIND_MOV.Value;
      FmEfdInnNFsIts.Edprod_vProd.ValueVariant                    := QrEfdInnNFsItsprod_vProd.Value;
      FmEfdInnNFsIts.Edprod_vFrete.ValueVariant                   := QrEfdInnNFsItsprod_vFrete.Value;
      FmEfdInnNFsIts.Edprod_vSeg.ValueVariant                     := QrEfdInnNFsItsprod_vSeg.Value;
      FmEfdInnNFsIts.Edprod_vOutro.ValueVariant                   := QrEfdInnNFsItsprod_vOutro.Value;
      FmEfdInnNFsIts.EdVL_ITEM.ValueVariant                       := QrEfdInnNFsItsVL_ITEM.Value;
      FmEfdInnNFsIts.EdVL_DESC.ValueVariant                       := QrEfdInnNFsItsVL_DESC.Value;
      FmEfdInnNFsIts.EdOri_IPIpIPI.ValueVariant                   := QrEfdInnNFsItsOri_IPIpIPI.Value;
      FmEfdInnNFsIts.EdOri_IPIvIPI.ValueVariant                   := QrEfdInnNFsItsOri_IPIvIPI.Value;
      FmEfdInnNFsIts.EdxLote.ValueVariant                         := QrEfdInnNFsItsxLote.Value;
      FmEfdInnNFsIts.EdICMS_CST.ValueVariant                      := QrEfdInnNFsItsCST_ICMS.Value;
      FmEfdInnNFsIts.EdVL_BC_ICMS.ValueVariant                    := QrEfdInnNFsItsVL_BC_ICMS.Value;
      FmEfdInnNFsIts.EdALIQ_ICMS.ValueVariant                     := QrEfdInnNFsItsALIQ_ICMS.Value;
      FmEfdInnNFsIts.EdVL_ICMS.ValueVariant                       := QrEfdInnNFsItsVL_ICMS.Value;
      FmEfdInnNFsIts.EdVL_BC_ICMS_ST.ValueVariant                 := QrEfdInnNFsItsVL_BC_ICMS_ST.Value;
      FmEfdInnNFsIts.EdALIQ_ST.ValueVariant                       := QrEfdInnNFsItsALIQ_ST.Value;
      FmEfdInnNFsIts.EdVL_ICMS_ST.ValueVariant                    := QrEfdInnNFsItsVL_ICMS_ST.Value;
      //FmEfdInnNFsIts.EdIND_APUR.ValueVariant                      := IND_APUR
      //FmEfdInnNFsIts.EdCOD_ENQ.ValueVariant                       := COD_ENQ


      FmEfdInnNFsIts.EdCST_IPI.ValueVariant                       := QrEfdInnNFsItsCST_IPI.Value;;
      FmEfdInnNFsIts.EdVL_BC_IPI.ValueVariant                     := QrEfdInnNFsItsVL_BC_IPI.Value;
      FmEfdInnNFsIts.EdALIQ_IPI.ValueVariant                      := QrEfdInnNFsItsALIQ_IPI.Value;
      FmEfdInnNFsIts.EdVL_IPI.ValueVariant                        := QrEfdInnNFsItsVL_IPI.Value;
      FmEfdInnNFsIts.EdCST_PIS.ValueVariant                       := QrEfdInnNFsItsCST_PIS.Value;
      FmEfdInnNFsIts.EdVL_BC_PIS .ValueVariant                    := QrEfdInnNFsItsVL_BC_PIS.Value;
      FmEfdInnNFsIts.EdALIQ_PIS.ValueVariant                      := QrEfdInnNFsItsALIQ_PIS_p.Value;
      //FmEfdInnNFsIts.EdQUANT_BC_PIS.ValueVariant                  := QrEfdInnNFsItsQUANT_BC_PIS.Value;
      //FmEfdInnNFsIts.EdALIQ_PIS_r.ValueVariant                    := QrEfdInnNFsItsALIQ_PIS_r.Value;
      FmEfdInnNFsIts.EdVL_PIS.ValueVariant                        := QrEfdInnNFsItsVL_PIS.Value;
      FmEfdInnNFsIts.EdCST_COFINS.ValueVariant                    := QrEfdInnNFsItsCST_COFINS.Value;
      FmEfdInnNFsIts.EdVL_BC_COFINS.ValueVariant                  := QrEfdInnNFsItsVL_BC_COFINS.Value;
      FmEfdInnNFsIts.EdALIQ_COFINS.ValueVariant                   := QrEfdInnNFsItsALIQ_COFINS_p.Value;
      //FmEfdInnNFsIts.EdQUANT_BC_COFINS.ValueVariant               := QrEfdInnNFsItsQUANT_BC_COFINS.Value;
      //FmEfdInnNFsIts.EdALIQ_COFINS_r.ValueVariant                 := QrEfdInnNFsItsALIQ_COFINS_r.Value;
      FmEfdInnNFsIts.EdVL_COFINS.ValueVariant                     := QrEfdInnNFsItsVL_COFINS.Value;
      //FmEfdInnNFsIts.EdCOD_CTA.ValueVariant                       := QrEfdInnNFsItsCOD_CTA.Value;
      //FmEfdInnNFsIts.EdVL_ABAT_NT.ValueVariant                    := QrEfdInnNFsItsVL_ABAT_NT.Value;
    end;
    FmEfdInnNFsIts.ShowModal;
    NewConta := FmEfdInnNFsIts.FConta;
    FmEfdInnNFsIts.Destroy;
        //
    ReopenEfdInnNFsIts_All(NewConta);
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TDmEFDII.ReopenEfdInnNFsCab(MovFatID, MovFatNum, Controle: Integer;
  QrEfdInnNFsCab: TmySQLQuery);
(*
var
  MovFatNum, MovimCod: Integer;
*)
begin
(*
  // VSInnCab: // Mov_Codigo.Value
  MovFatNum := FQrMaeCab.FieldByName(FCabFld_MovFatNum).AsInteger;
  MovimCod  := FQrMaeCab.FieldByName(FCabFld_MovimCod).AsInteger;
    //FMovFatID, FFrtFatID: Integer;
    //FQrMaeCab: TmySQLQuery;
    //FCabFld_MovimCod, FCabFld_MovFatNum: String;
*)
  UnDmkDAC_PF.AbremySQLQuery0(QrEfdInnNFsCab, Dmod.MyDB, [
  'SELECT vic.*, ',
  'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  ',
  'FROM efdinnnfscab vic ',
  'LEFT JOIN entidades ter ON ter.Codigo=vic.Terceiro ',
  'WHERE vic.MovFatID=' + Geral.FF0(MovFatID),
  'AND vic.MovFatNum=' + Geral.FF0(MovFatNum),
  //'AND vic.MovimCod=' + Geral.FF0(MovimCod), // N�o precisa
  ' ']);
  //
  QrEfdInnNFsCab.Locate('Controle', Controle, []);
end;

procedure TDmEFDII.ReopenEfdInnNFsIts_All(Controle, Conta: Integer;
  QrEfdInnNFsCab: TmySQLQuery);
begin
  UnDmkDAC_PF.AbremySQLQuery0(QrEfdInnNFsIts, Dmod.MyDB, [
  'SELECT vin.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM efdinnnfsits     vin',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vin.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE vin.Controle=' + Geral.FF0(QrEfdInnNFsCabControle.Value),
  '']);
  //
  QrEfdInnNFsIts.Locate('Conta', Conta, []);
end;


{
object QrEfdInnNFsCab: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT vin.*, '
    'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  '
    'FROM efdinnnfscab vin '
    'LEFT JOIN entidades ter ON ter.Codigo=vin.Terceiro '
    ' ')
  Left = 44
  Top = 6
  object QrEfdInnNFsCabNO_TER: TWideStringField
    FieldName = 'NO_TER'
    Size = 100
  end
  object QrEfdInnNFsCabMovFatID: TIntegerField
    FieldName = 'MovFatID'
    Required = True
  end
  object QrEfdInnNFsCabMovFatNum: TIntegerField
    FieldName = 'MovFatNum'
    Required = True
  end
  object QrEfdInnNFsCabMovimCod: TIntegerField
    FieldName = 'MovimCod'
    Required = True
  end
  object QrEfdInnNFsCabEmpresa: TIntegerField
    FieldName = 'Empresa'
    Required = True
  end
  object QrEfdInnNFsCabControle: TIntegerField
    FieldName = 'Controle'
    Required = True
  end
  object QrEfdInnNFsCabTerceiro: TIntegerField
    FieldName = 'Terceiro'
    Required = True
  end
  object QrEfdInnNFsCabPecas: TFloatField
    FieldName = 'Pecas'
    Required = True
  end
  object QrEfdInnNFsCabPesoKg: TFloatField
    FieldName = 'PesoKg'
    Required = True
  end
  object QrEfdInnNFsCabAreaM2: TFloatField
    FieldName = 'AreaM2'
    Required = True
  end
  object QrEfdInnNFsCabAreaP2: TFloatField
    FieldName = 'AreaP2'
    Required = True
  end
  object QrEfdInnNFsCabValorT: TFloatField
    FieldName = 'ValorT'
    Required = True
  end
  object QrEfdInnNFsCabMotorista: TIntegerField
    FieldName = 'Motorista'
    Required = True
  end
  object QrEfdInnNFsCabPlaca: TWideStringField
    FieldName = 'Placa'
  end
  object QrEfdInnNFsCabCOD_MOD: TSmallintField
    FieldName = 'COD_MOD'
    Required = True
  end
  object QrEfdInnNFsCabCOD_SIT: TSmallintField
    FieldName = 'COD_SIT'
    Required = True
  end
  object QrEfdInnNFsCabSER: TIntegerField
    FieldName = 'SER'
    Required = True
  end
  object QrEfdInnNFsCabNUM_DOC: TIntegerField
    FieldName = 'NUM_DOC'
    Required = True
  end
  object QrEfdInnNFsCabCHV_NFE: TWideStringField
    FieldName = 'CHV_NFE'
    Size = 44
  end
  object QrEfdInnNFsCabNFeStatus: TIntegerField
    FieldName = 'NFeStatus'
    Required = True
  end
  object QrEfdInnNFsCabDT_DOC: TDateField
    FieldName = 'DT_DOC'
    Required = True
  end
  object QrEfdInnNFsCabDT_E_S: TDateField
    FieldName = 'DT_E_S'
    Required = True
  end
  object QrEfdInnNFsCabVL_DOC: TFloatField
    FieldName = 'VL_DOC'
    Required = True
  end
  object QrEfdInnNFsCabIND_PGTO: TWideStringField
    FieldName = 'IND_PGTO'
    Size = 1
  end
  object QrEfdInnNFsCabVL_DESC: TFloatField
    FieldName = 'VL_DESC'
    Required = True
  end
  object QrEfdInnNFsCabVL_ABAT_NT: TFloatField
    FieldName = 'VL_ABAT_NT'
    Required = True
  end
  object QrEfdInnNFsCabVL_MERC: TFloatField
    FieldName = 'VL_MERC'
    Required = True
  end
  object QrEfdInnNFsCabIND_FRT: TWideStringField
    FieldName = 'IND_FRT'
    Size = 1
  end
  object QrEfdInnNFsCabVL_FRT: TFloatField
    FieldName = 'VL_FRT'
    Required = True
  end
  object QrEfdInnNFsCabVL_SEG: TFloatField
    FieldName = 'VL_SEG'
    Required = True
  end
  object QrEfdInnNFsCabVL_OUT_DA: TFloatField
    FieldName = 'VL_OUT_DA'
    Required = True
  end
  object QrEfdInnNFsCabVL_BC_ICMS: TFloatField
    FieldName = 'VL_BC_ICMS'
    Required = True
  end
  object QrEfdInnNFsCabVL_ICMS: TFloatField
    FieldName = 'VL_ICMS'
    Required = True
  end
  object QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField
    FieldName = 'VL_BC_ICMS_ST'
    Required = True
  end
  object QrEfdInnNFsCabVL_ICMS_ST: TFloatField
    FieldName = 'VL_ICMS_ST'
    Required = True
  end
  object QrEfdInnNFsCabVL_IPI: TFloatField
    FieldName = 'VL_IPI'
    Required = True
  end
  object QrEfdInnNFsCabVL_PIS: TFloatField
    FieldName = 'VL_PIS'
    Required = True
  end
  object QrEfdInnNFsCabVL_COFINS: TFloatField
    FieldName = 'VL_COFINS'
    Required = True
  end
  object QrEfdInnNFsCabVL_PIS_ST: TFloatField
    FieldName = 'VL_PIS_ST'
    Required = True
  end
  object QrEfdInnNFsCabVL_COFINS_ST: TFloatField
    FieldName = 'VL_COFINS_ST'
    Required = True
  end
  object QrEfdInnNFsCabNFe_FatID: TIntegerField
    FieldName = 'NFe_FatID'
    Required = True
  end
  object QrEfdInnNFsCabNFe_FatNum: TIntegerField
    FieldName = 'NFe_FatNum'
    Required = True
  end
  object QrEfdInnNFsCabNFe_StaLnk: TSmallintField
    FieldName = 'NFe_StaLnk'
    Required = True
  end
  object QrEfdInnNFsCabVSVmcWrn: TSmallintField
    FieldName = 'VSVmcWrn'
    Required = True
  end
  object QrEfdInnNFsCabVSVmcObs: TWideStringField
    FieldName = 'VSVmcObs'
    Size = 60
  end
  object QrEfdInnNFsCabVSVmcSeq: TWideStringField
    FieldName = 'VSVmcSeq'
    Size = 25
  end
  object QrEfdInnNFsCabVSVmcSta: TSmallintField
    FieldName = 'VSVmcSta'
    Required = True
  end
end
object DsEfdInnNFsCab: TDataSource
  DataSet = QrEfdInnNFsCab
  Left = 44
  Top = 56
end
object QrEfdInnNFsIts: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT vin.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  '

      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
      'e)), '
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, '
    'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, '
    'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, '
    'pgt.Tipo_Item) Tipo_Item '
    'FROM efdinnnfsits     vin'
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vin.GraGruX'
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
    'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2'
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip'
    'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ')
  Left = 44
  Top = 110
  object QrEfdInnNFsItsMovFatID: TIntegerField
    FieldName = 'MovFatID'
    Required = True
  end
  object QrEfdInnNFsItsMovFatNum: TIntegerField
    FieldName = 'MovFatNum'
    Required = True
  end
  object QrEfdInnNFsItsMovimCod: TIntegerField
    FieldName = 'MovimCod'
    Required = True
  end
  object QrEfdInnNFsItsEmpresa: TIntegerField
    FieldName = 'Empresa'
    Required = True
  end
  object QrEfdInnNFsItsMovimNiv: TIntegerField
    FieldName = 'MovimNiv'
    Required = True
  end
  object QrEfdInnNFsItsMovimTwn: TIntegerField
    FieldName = 'MovimTwn'
    Required = True
  end
  object QrEfdInnNFsItsControle: TIntegerField
    FieldName = 'Controle'
    Required = True
  end
  object QrEfdInnNFsItsConta: TIntegerField
    FieldName = 'Conta'
    Required = True
  end
  object QrEfdInnNFsItsGraGru1: TIntegerField
    FieldName = 'GraGru1'
    Required = True
  end
  object QrEfdInnNFsItsGraGruX: TIntegerField
    FieldName = 'GraGruX'
    Required = True
  end
  object QrEfdInnNFsItsprod_vProd: TFloatField
    FieldName = 'prod_vProd'
    Required = True
  end
  object QrEfdInnNFsItsprod_vFrete: TFloatField
    FieldName = 'prod_vFrete'
    Required = True
  end
  object QrEfdInnNFsItsprod_vSeg: TFloatField
    FieldName = 'prod_vSeg'
    Required = True
  end
  object QrEfdInnNFsItsprod_vOutro: TFloatField
    FieldName = 'prod_vOutro'
    Required = True
  end
  object QrEfdInnNFsItsQTD: TFloatField
    FieldName = 'QTD'
    Required = True
  end
  object QrEfdInnNFsItsUNID: TWideStringField
    FieldName = 'UNID'
    Size = 6
  end
  object QrEfdInnNFsItsVL_ITEM: TFloatField
    FieldName = 'VL_ITEM'
    Required = True
  end
  object QrEfdInnNFsItsVL_DESC: TFloatField
    FieldName = 'VL_DESC'
    Required = True
  end
  object QrEfdInnNFsItsIND_MOV: TWideStringField
    FieldName = 'IND_MOV'
    Size = 1
  end
  object QrEfdInnNFsItsCST_ICMS: TIntegerField
    FieldName = 'CST_ICMS'
  end
  object QrEfdInnNFsItsCFOP: TIntegerField
    FieldName = 'CFOP'
  end
  object QrEfdInnNFsItsCOD_NAT: TWideStringField
    FieldName = 'COD_NAT'
    Size = 10
  end
  object QrEfdInnNFsItsVL_BC_ICMS: TFloatField
    FieldName = 'VL_BC_ICMS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_ICMS: TFloatField
    FieldName = 'ALIQ_ICMS'
    Required = True
  end
  object QrEfdInnNFsItsVL_ICMS: TFloatField
    FieldName = 'VL_ICMS'
    Required = True
  end
  object QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField
    FieldName = 'VL_BC_ICMS_ST'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_ST: TFloatField
    FieldName = 'ALIQ_ST'
    Required = True
  end
  object QrEfdInnNFsItsVL_ICMS_ST: TFloatField
    FieldName = 'VL_ICMS_ST'
    Required = True
  end
  object QrEfdInnNFsItsIND_APUR: TWideStringField
    FieldName = 'IND_APUR'
    Size = 1
  end
  object QrEfdInnNFsItsCST_IPI: TWideStringField
    FieldName = 'CST_IPI'
    Size = 2
  end
  object QrEfdInnNFsItsCOD_ENQ: TWideStringField
    FieldName = 'COD_ENQ'
    Size = 3
  end
  object QrEfdInnNFsItsVL_BC_IPI: TFloatField
    FieldName = 'VL_BC_IPI'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_IPI: TFloatField
    FieldName = 'ALIQ_IPI'
    Required = True
  end
  object QrEfdInnNFsItsVL_IPI: TFloatField
    FieldName = 'VL_IPI'
    Required = True
  end
  object QrEfdInnNFsItsCST_PIS: TSmallintField
    FieldName = 'CST_PIS'
  end
  object QrEfdInnNFsItsVL_BC_PIS: TFloatField
    FieldName = 'VL_BC_PIS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_PIS_p: TFloatField
    FieldName = 'ALIQ_PIS_p'
    Required = True
  end
  object QrEfdInnNFsItsQUANT_BC_PIS: TFloatField
    FieldName = 'QUANT_BC_PIS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_PIS_r: TFloatField
    FieldName = 'ALIQ_PIS_r'
    Required = True
  end
  object QrEfdInnNFsItsVL_PIS: TFloatField
    FieldName = 'VL_PIS'
    Required = True
  end
  object QrEfdInnNFsItsCST_COFINS: TSmallintField
    FieldName = 'CST_COFINS'
  end
  object QrEfdInnNFsItsVL_BC_COFINS: TFloatField
    FieldName = 'VL_BC_COFINS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_COFINS_p: TFloatField
    FieldName = 'ALIQ_COFINS_p'
    Required = True
  end
  object QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField
    FieldName = 'QUANT_BC_COFINS'
    Required = True
  end
  object QrEfdInnNFsItsALIQ_COFINS_r: TFloatField
    FieldName = 'ALIQ_COFINS_r'
    Required = True
  end
  object QrEfdInnNFsItsVL_COFINS: TFloatField
    FieldName = 'VL_COFINS'
    Required = True
  end
  object QrEfdInnNFsItsCOD_CTA: TWideStringField
    FieldName = 'COD_CTA'
    Size = 255
  end
  object QrEfdInnNFsItsVL_ABAT_NT: TFloatField
    FieldName = 'VL_ABAT_NT'
    Required = True
  end
  object QrEfdInnNFsItsDtCorrApo: TDateTimeField
    FieldName = 'DtCorrApo'
    Required = True
  end
  object QrEfdInnNFsItsxLote: TWideStringField
    FieldName = 'xLote'
  end
  object QrEfdInnNFsItsOri_IPIpIPI: TFloatField
    FieldName = 'Ori_IPIpIPI'
    Required = True
  end
  object QrEfdInnNFsItsOri_IPIvIPI: TFloatField
    FieldName = 'Ori_IPIvIPI'
    Required = True
  end
  object QrEfdInnNFsItsNO_PRD_TAM_COR: TWideStringField
    FieldName = 'NO_PRD_TAM_COR'
    Size = 157
  end
  object QrEfdInnNFsItsSIGLAUNIDMED: TWideStringField
    FieldName = 'SIGLAUNIDMED'
    Size = 6
  end
  object QrEfdInnNFsItsGerBxaEstq: TSmallintField
    FieldName = 'GerBxaEstq'
  end
  object QrEfdInnNFsItsNCM: TWideStringField
    FieldName = 'NCM'
    Size = 10
  end
  object QrEfdInnNFsItsUnidMed: TIntegerField
    FieldName = 'UnidMed'
  end
  object QrEfdInnNFsItsEx_TIPI: TWideStringField
    FieldName = 'Ex_TIPI'
    Size = 3
  end
  object QrEfdInnNFsItsGrandeza: TSmallintField
    FieldName = 'Grandeza'
  end
  object QrEfdInnNFsItsTipo_Item: TSmallintField
    FieldName = 'Tipo_Item'
  end
end
object DsEfdInnNFsIts: TDataSource
  DataSet = QrEfdInnNFsIts
  Left = 44
  Top = 160
end
object QrGraGru1: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT CST_B, IPI_CST, PIS_CST, COFINS_CST, '
    'ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq,  '
    'COFINSRec_pAliq  '
    'FROM gragru1 '
    'WHERE Nivel1>0')
  Left = 44
  Top = 212
  object QrGraGru1CST_B: TSmallintField
    FieldName = 'CST_B'
    Required = True
  end
  object QrGraGru1IPI_CST: TSmallintField
    FieldName = 'IPI_CST'
    Required = True
  end
  object QrGraGru1PIS_CST: TSmallintField
    FieldName = 'PIS_CST'
    Required = True
  end
  object QrGraGru1COFINS_CST: TSmallintField
    FieldName = 'COFINS_CST'
    Required = True
  end
  object QrGraGru1ICMSRec_pAliq: TFloatField
    FieldName = 'ICMSRec_pAliq'
    Required = True
  end
  object QrGraGru1IPIRec_pAliq: TFloatField
    FieldName = 'IPIRec_pAliq'
    Required = True
  end
  object QrGraGru1PISRec_pAliq: TFloatField
    FieldName = 'PISRec_pAliq'
    Required = True
  end
  object QrGraGru1COFINSRec_pAliq: TFloatField
    FieldName = 'COFINSRec_pAliq'
    Required = True
  end
end
object PMFiscal: TPopupMenu
  OnPopup = PMFiscalPopup
  Left = 176
  Top = 4
  object IncluiDocumento1: TMenuItem
    Caption = '&Inclui Documento'
    OnClick = IncluiDocumento1Click
  end
  object AlteraDocumento1: TMenuItem
    Caption = '&Altera Documento'
  end
  object ExcluiDocumento1: TMenuItem
    Caption = '&Exclui Documento'
  end
  object N4: TMenuItem
    Caption = '-'
  end
  object IncluiItemdodocumento1: TMenuItem
    Caption = 'Inclui Item do documento'
  end
  object AlteraoItemselecionadododocumento1: TMenuItem
    Caption = 'Altera o  Item selecionado do documento'
  end
  object ExcluioItemselecionadododocumento1: TMenuItem
    Caption = 'Exclui o  Item selecionado do documento'
  end
end
}
end.
