unit EfdInnD500Cab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, Variants, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, UnDmkEnums, SPED_Listas;

type
  TFmEfdInnD500Cab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    BtOK: TBitBtn;
    Panel6: TPanel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrTerceiros: TmySQLQuery;
    DsTerceiros: TDataSource;
    QrTerceirosCodigo: TIntegerField;
    QrTerceirosNO_ENT: TWideStringField;
    Panel7: TPanel;
    QrTbSPEDEFD017: TmySQLQuery;
    QrTbSPEDEFD017CodTxt: TWideStringField;
    QrTbSPEDEFD017Nome: TWideStringField;
    DsTbSPEDEFD017: TDataSource;
    GroupBox3: TGroupBox;
    EdCOD_MOD: TdmkEditCB;
    CBCOD_MOD: TdmkDBLookupComboBox;
    QrTbSPEDEFD018: TmySQLQuery;
    DsTbSPEDEFD018: TDataSource;
    QrTbSPEDEFD018CodTxt: TWideStringField;
    QrTbSPEDEFD018Nome: TWideStringField;
    GroupBox4: TGroupBox;
    EdCOD_SIT: TdmkEditCB;
    CBCOD_SIT: TdmkDBLookupComboBox;
    Panel8: TPanel;
    Label2: TLabel;
    EdSER: TdmkEdit;
    EdSUB: TdmkEdit;
    Label7: TLabel;
    Label9: TLabel;
    EdNUM_DOC: TdmkEdit;
    Label10: TLabel;
    TPDT_DOC: TdmkEditDateTimePicker;
    Label11: TLabel;
    TPDT_A_P: TdmkEditDateTimePicker;
    Panel10: TPanel;
    Panel11: TPanel;
    RGTP_ASSINANTE: TdmkRadioGroup;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    EdVL_DOC: TdmkEdit;
    EdVL_DESC: TdmkEdit;
    EdVL_SERV: TdmkEdit;
    EdVL_SERV_NT: TdmkEdit;
    EdVL_TERC: TdmkEdit;
    EdVL_DA: TdmkEdit;
    EdVL_BC_ICMS: TdmkEdit;
    EdVL_ICMS: TdmkEdit;
    EdVL_PIS: TdmkEdit;
    EdVL_COFINS: TdmkEdit;
    QrCST_ICMS: TmySQLQuery;
    QrTbSPEDEFD002: TmySQLQuery;
    QrTbSPEDEFD002CodTxt: TWideStringField;
    QrTbSPEDEFD002Nome: TWideStringField;
    QrCST_ICMSCodTxt: TWideStringField;
    QrCST_ICMSNome: TWideStringField;
    DsCST_ICMS: TDataSource;
    DsTbSPEDEFD002: TDataSource;
    RGIND_OPER: TdmkRadioGroup;
    RGIND_EMIT: TdmkRadioGroup;
    Label4: TLabel;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label5: TLabel;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    EdControle: TdmkEdit;
    Label37: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdVL_SERVChange(Sender: TObject);
    procedure EdVL_SERV_NTChange(Sender: TObject);
    procedure EdVL_TERCChange(Sender: TObject);
    procedure EdVL_DAChange(Sender: TObject);
    procedure EdVL_DESCChange(Sender: TObject);
  private
    { Private declarations }
    function  COD_MOD_Permitido(): Boolean;
    function  COD_SIT_Permitido(): Boolean;
    procedure CalculaVL_DOC();
  public
    { Public declarations }
    FAnoMes, FControle: Integer;
    procedure ReopenTabelas();
  end;

  var
  FmEfdInnD500Cab: TFmEfdInnD500Cab;

implementation

uses UnMyObjects, Module, EfdInnD001Ger, UMySQLModule, ModuleGeral, UnDmkProcFunc,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmEfdInnD500Cab.BtOKClick(Sender: TObject);
const
  REG = 'D500';
  Importado = 0;
  //
  COD_INF = '';
var
  ImporExpor, AnoMes, Empresa, LinArq: Integer;
  //
  IND_OPER, IND_EMIT, COD_PART, COD_MOD, COD_SIT, SER, SUB, NUM_DOC,
  DT_DOC, DT_A_P, COD_CTA: String;
  VL_DOC, VL_DESC, VL_SERV, VL_SERV_NT, VL_TERC, VL_DA, VL_BC_ICMS, VL_ICMS,
  //COD_INF: String;
  VL_PIS, VL_COFINS: Double;
  TP_ASSINANTE: String;
  Codigo, Controle, Terceiro: Integer;
  SQLType: TSQLType;

begin
  SQLType  := ImgTipo.SQLtype;
  Codigo   := EdCodigo.ValueVariant;
  Controle := EdControle.ValueVariant;
  AnoMes   := EdAnoMes.ValueVariant;
  Empresa  := EdEmpresa.ValueVariant;
  //
  Terceiro := EdTerceiro.ValueVariant;
  if MyObjects.FIC(Terceiro = 0, EdTerceiro,
    'Informe o Fornecedor!') then
    Exit;
  //
  if MyObjects.FIC(RGIND_OPER.ItemIndex < 0, RGIND_OPER,
    'Informe o indicador do tipo de opera��o!') then
    Exit;
  //
  if MyObjects.FIC(RGIND_EMIT.ItemIndex < 0, RGIND_EMIT,
    'Informe o indicador do emitente do documento fiscal!') then
    Exit;
  //
  if not COD_MOD_Permitido() then
    Exit;
  //
  if not COD_SIT_Permitido() then
    Exit;
  //
  if MyObjects.FIC(EdNUM_DOC.ValueVariant < 1, EdNUM_DOC,
    'Informe o n�mero do documento!') then
    Exit;
  //
  if MyObjects.FIC(Int(TPDT_DOC.Date) > Geral.AAAAMM_To_Date(
    EdAnoMes.ValueVariant, qdmLast), TPDT_DOC, 'Data de emiss�o inv�lida!') then
    Exit;
  //
  if MyObjects.FIC(Int(TPDT_A_P.Date) > Geral.AAAAMM_To_Date(
    EdAnoMes.ValueVariant, qdmLast), TPDT_A_P, 'Data de entrada/sa�da inv�lida!') then
    Exit;
  if MyObjects.FIC(Int(TPDT_A_P.Date) < Geral.AAAAMM_To_Date(
    EdAnoMes.ValueVariant, qdmFirst), TPDT_A_P, 'Data de entrada/sa�da inv�lida!') then
    Exit;
  //
{
  if MyObjects.FIC(EdVL_DOC.ValueVariant < 0.01, EdVL_DOC,
    'Informe o valor total do documento fiscal!') then
    Exit;
  //
  if MyObjects.FIC(EdVL_SERV.ValueVariant < 0.01, EdVL_SERV,
    'Informe o valor total fornecido/consumido!') then
    Exit;
}
  //
  IND_OPER         := FormatFloat('0', RGIND_OPER.ItemIndex);
  IND_EMIT         := FormatFloat('0', RGIND_EMIT.ItemIndex);
  COD_PART         := FormatFloat('0', Terceiro);
  COD_MOD          := EdCOD_MOD.Text;
  COD_SIT          := EdCOD_SIT.Text;
  SER              := EdSER.Text;
  SUB              := EdSUB.Text;
  NUM_DOC          := EdNUM_DOC.ValueVariant;
  DT_DOC           := Geral.FDT(TPDT_DOC.Date, 1);
  DT_A_P           := Geral.FDT(TPDT_A_P.Date, 1);
  VL_DOC           := EdVL_DOC.ValueVariant;
  VL_DESC          := EdVL_DESC.ValueVariant;
  VL_SERV          := EdVL_SERV.ValueVariant;
  VL_SERV_NT       := EdVL_SERV_NT.ValueVariant;
  VL_TERC          := EdVL_TERC.ValueVariant;
  VL_DA            := EdVL_DA.ValueVariant;
  VL_BC_ICMS       := EdVL_BC_ICMS.ValueVariant;
  VL_ICMS          := EdVL_ICMS.ValueVariant;
  VL_PIS           := EdVL_PIS.ValueVariant;
  VL_COFINS        := EdVL_COFINS.ValueVariant;
  COD_CTA          := '';
  TP_ASSINANTE     := FormatFloat('0', RGTP_ASSINANTE.ItemIndex);
  //
  if RGTP_ASSINANTE.ItemIndex < 0 then
    TP_ASSINANTE := ' ';
  //
  Empresa := EdEmpresa.ValueVariant;
  //
  Controle := UMyMod.BPGS1I32('efdinnd500cab', 'Controle', '', '', tsPos, SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdinnd500cab', False, [
  'AnoMes', 'Empresa', 'Codigo',
  'IND_OPER', 'IND_EMIT', 'COD_MOD',
  'COD_SIT', 'SER', 'SUB',
  'NUM_DOC', 'DT_DOC', 'DT_A_P',
  'VL_DOC', 'VL_DESC', 'VL_SERV',
  'VL_SERV_NT', 'VL_TERC', 'VL_DA',
  'VL_BC_ICMS', 'VL_ICMS', 'COD_INF',
  'VL_PIS', 'VL_COFINS', 'COD_CTA',
  'TP_ASSINANTE', 'Terceiro', 'Importado'], [
  'Controle'], [
  AnoMes, Empresa, Codigo,
  IND_OPER, IND_EMIT, COD_MOD,
  COD_SIT, SER, SUB,
  NUM_DOC, DT_DOC, DT_A_P,
  VL_DOC, VL_DESC, VL_SERV,
  VL_SERV_NT, VL_TERC, VL_DA,
  VL_BC_ICMS, VL_ICMS, COD_INF,
  VL_PIS, VL_COFINS, COD_CTA,
  TP_ASSINANTE, Terceiro, Importado], [
  Controle], True) then
  begin
    FControle := Controle;
    FmEfdInnD001Ger.ReopenEfdInnd500Cab(Controle);
    Close;
  end;

(*
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'efdicmsipid500', False, [
  'REG', 'IND_OPER', 'IND_EMIT',
  'COD_PART', 'COD_MOD', 'COD_SIT',
  'SER', 'SUB', 
  'NUM_DOC', 'DT_DOC', 'DT_A_P',
  'VL_DOC', 'VL_DESC', 'VL_SERV',
  'VL_SERV_NT', 'VL_TERC', 'VL_DA',
  'VL_BC_ICMS', 'VL_ICMS', 'COD_INF',
  'VL_PIS', 'VL_COFINS', 'TP_ASSINANTE',
  'Terceiro', 'Importado'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  REG, IND_OPER, IND_EMIT,
  COD_PART, COD_MOD, COD_SIT,
  SER, SUB, 
  NUM_DOC, DT_DOC, DT_A_P,
  VL_DOC, VL_DESC, VL_SERV,
  VL_SERV_NT, VL_TERC, VL_DA,
  VL_BC_ICMS, VL_ICMS, COD_INF,
  VL_PIS, VL_COFINS, TP_ASSINANTE,
  Terceiro, Importado], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
    FmEfdInnD001Ger.ReopenEFD_D500(LinArq);
    Close;
  end;
*)
end;

procedure TFmEfdInnD500Cab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdInnD500Cab.CalculaVL_DOC;
var
  VL_DOC, VL_DESC, VL_SERV, VL_SERV_NT, VL_TERC, VL_DA, VL_BC_ICMS: Double;
begin
  VL_SERV          := EdVL_SERV.ValueVariant;
  VL_DA            := EdVL_DA.ValueVariant;
  VL_SERV_NT       := EdVL_SERV_NT.ValueVariant;
  VL_TERC          := EdVL_TERC.ValueVariant;

  VL_DESC          := EdVL_DESC.ValueVariant;
  //ENER_INJET       := EdENER_INJET.ValueVariant;
  //OUTRAS_DED       := EdOUTRAS_DED.ValueVariant;


// ////////////////////////////  Texto do Registro C500 !!! ////////////////////
//Campo 13 (VL_DOC) - Valida��o: O valor deste campo deve corresponder ao somat�rio dos campos VL_FORN, VL_DA,
//VL_SERV_NT e VL_TERC subtra�do do somat�rio de VL_DESC, ENER_INJET e OUTRAS_DED.

  VL_DOC           := (VL_SERV + VL_DA + VL_SERV_NT + VL_TERC) -
                      (VL_DESC (*+ ENER_INJET + OUTRAS_DED*));

  EdVL_DOC.ValueVariant := VL_DOC;
  //VL_BC_ICMS := ???
  //EdVL_BC_ICMS.ValueVariant := VL_BC_ICMS;
end;

function TFmEfdInnD500Cab.COD_MOD_Permitido(): Boolean;
var
  Modelo: Integer;
begin
  Modelo := Geral.IMV(EdCOD_MOD.Text);
  Result := Modelo in ([21,22]);
  if not Result then
    Geral.MensagemBox('C�digo do Modelo do Documento Fiscal inv�lido!',
    'Aviso', MB_OK+MB_ICONWARNING);
end;

function TFmEfdInnD500Cab.COD_SIT_Permitido(): Boolean;
var
  Sit: Integer;
begin
  Sit := Geral.IMV(EdCOD_SIT.Text);
  Result := (EdCOD_SIT.Text <> '') and (Sit in ([00,01,02,03,08]));
  if not Result then
    Geral.MensagemBox('C�digo da Situa��o do Documento Fiscal inv�lido!',
    'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmEfdInnD500Cab.EdVL_DAChange(Sender: TObject);
begin
  CalculaVL_DOC();
end;

procedure TFmEfdInnD500Cab.EdVL_DESCChange(Sender: TObject);
begin
  CalculaVL_DOC();
end;

procedure TFmEfdInnD500Cab.EdVL_SERVChange(Sender: TObject);
begin
  CalculaVL_DOC();
end;

procedure TFmEfdInnD500Cab.EdVL_SERV_NTChange(Sender: TObject);
begin
  CalculaVL_DOC();
end;

procedure TFmEfdInnD500Cab.EdVL_TERCChange(Sender: TObject);
begin
  CalculaVL_DOC();
end;

procedure TFmEfdInnD500Cab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdTerceiro.SetFocus;
end;

procedure TFmEfdInnD500Cab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmEfdInnD500Cab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdInnD500Cab.ReopenTabelas();
var
  Ini, Fim, SQL_Periodo_Valido: String;
begin
  Ini := Geral.FDT(DmkPF.DatadeAnoMes(FAnoMes, 1, 0), 1);
  Fim := Geral.FDT(DmkPF.DatadeAnoMes(FAnoMes + 1, 1, -1), 1);
  SQL_Periodo_Valido :=
  'WHERE DataIni <= "' + Ini + '" ' + sLineBreak +
  'AND (DataFim >="' + Fim + '" OR DataFim<2)  ';

  // Se precisar IND_EMIT est� na tabela 16
  //QrTbSPEDEFD016.Open;
  //
  QrTerceiros.Open;
  // Se precisar IND_EMIT est� na tabela 16
  //QrTbSPEDEFD016.Open;
  // COD_MOD > QrTbSPEDEFD017
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD017, DModG.AllID_DB, [
  'SELECT *  ',
  'FROM tbspedcods017 ',
  SQL_Periodo_Valido,
  'AND CodTxt IN ("21", "22") ',
  'ORDER BY Nome ',
  '']);
  // COD_SIT >  QrTbSPEDEFD018
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD018, DModG.AllID_DB, [
  'SELECT *  ',
  'FROM tbspedcods018 ',
  SQL_Periodo_Valido,
  'ORDER BY Nome ',
  '']);
  // TbSPEDEFD024 x TbSPEDEFD025
  // Mudou para 130
  UnDmkDAC_PF.AbreMySQLQuery0(QrCST_ICMS, DModG.AllID_DB, [
 'SELECT *  ',
 'FROM tbspedcods130 ',
 SQL_Periodo_Valido,
 'ORDER BY Nome ',
 '']);
  // CFOP > QrTbSPEDEFD002
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD002, DModG.AllID_DB, [
  'SELECT *  ',
  'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
  SQL_Periodo_Valido,
  'ORDER BY Nome ',
  '']);
end;

end.
