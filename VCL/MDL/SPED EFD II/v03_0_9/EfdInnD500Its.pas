unit EfdInnD500Its;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnInternalConsts,
  UnMsgInt, ExtCtrls, UnInternalConsts2, mySQLDbTables, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, ComCtrls, dmkMemo, dmkRadioGroup,
  dmkLabel, dmkImage, UnDmkEnums, DmkDAC_PF, dmkEditDateTimePicker, UnPQ_PF,
  dmkDBEdit, Vcl.Menus, UnMySQLCuringa, SPED_LIstas, UnGrl_Consts;

type
  TFmEfdInnD500Its = class(TForm)
    QrPQ1: TmySQLQuery;
    DsPQ1: TDataSource;
    QrLocalizaPQ: TmySQLQuery;
    Panel1: TPanel;
    Painel1: TPanel;
    PainelDados: TPanel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    SBProduto: TSpeedButton;
    QrPQ1NOMEPQ: TWideStringField;
    QrPQ1PQ: TIntegerField;
    Panel3: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label1: TLabel;
    QrPQCli: TmySQLQuery;
    QrPQCliCustoPadrao: TFloatField;
    QrPQCliMoedaPadrao: TIntegerField;
    QrPQCliNOMEMOEDAPADRAO: TWideStringField;
    DsPQCli: TDataSource;
    QrGraGru1: TmySQLQuery;
    QrGraGru1ICMSRec_pAliq: TFloatField;
    QrGraGru1PISRec_pAliq: TFloatField;
    QrGraGru1COFINSRec_pAliq: TFloatField;
    PMProduto: TPopupMenu;
    Cadastrodoproduto1: TMenuItem;
    Dadosfiscais1: TMenuItem;
    Panel6: TPanel;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    Panel7: TPanel;
    GBRecuImpost: TGroupBox;
    Panel2: TPanel;
    Label13: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    EdALIQ_ICMS: TdmkEdit;
    EdVL_ICMS: TdmkEdit;
    EdALIQ_PIS: TdmkEdit;
    EdVL_PIS: TdmkEdit;
    EdALIQ_COFINS: TdmkEdit;
    EdVL_COFINS: TdmkEdit;
    QrCFOP: TMySQLQuery;
    QrCFOPNome: TWideStringField;
    DsCFOP: TDataSource;
    QrCFOPCodigo: TLargeintField;
    Label476: TLabel;
    EdCST_ICMS: TdmkEdit;
    EdUCTextoB: TdmkEdit;
    Label478: TLabel;
    EdCST_PIS: TdmkEdit;
    EdUCTextoPIS_CST: TdmkEdit;
    Label480: TLabel;
    EdCST_COFINS: TdmkEdit;
    EdUCTextoCOFINS_CST: TdmkEdit;
    EdVL_BC_ICMS: TdmkEdit;
    EdVL_BC_PIS: TdmkEdit;
    EdVL_BC_COFINS: TdmkEdit;
    Label20: TLabel;
    EdVL_BC_ICMS_ST: TdmkEdit;
    EdALIQ_ST: TdmkEdit;
    EdVL_ICMS_ST: TdmkEdit;
    QrGraGruX: TMySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXEx_TIPI: TWideStringField;
    QrGraGruXGrandeza: TIntegerField;
    QrGraGruXTipo_Item: TIntegerField;
    DsGraGruX: TDataSource;
    Label66: TLabel;
    EdCFOP: TdmkEditCB;
    CBCFOP: TdmkDBLookupComboBox;
    SbCFOP: TSpeedButton;
    GroupBox1: TGroupBox;
    Panel9: TPanel;
    Label5: TLabel;
    EdAjusteVL_BC_ICMS: TdmkEdit;
    EdAjusteALIQ_ICMS: TdmkEdit;
    EdAjusteVL_ICMS: TdmkEdit;
    EdAjusteVL_OUTROS: TdmkEdit;
    Label8: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    EdAjusteVL_OPR: TdmkEdit;
    Label31: TLabel;
    Label32: TLabel;
    EdAjusteVL_RED_BC: TdmkEdit;
    Panel10: TPanel;
    Label27: TLabel;
    EdEmpresa: TdmkEdit;
    Label9: TLabel;
    EdAnoMes: TdmkEdit;
    Label15: TLabel;
    EdCodigo: TdmkEdit;
    Label3: TLabel;
    EdControle: TdmkEdit;
    Label11: TLabel;
    EdNAT_BC_CRED: TdmkEdit;
    EdNAT_BC_CRED_TXT: TdmkEdit;
    Label2: TLabel;
    EdConta: TdmkEdit;
    Label19: TLabel;
    EdQTD: TdmkEdit;
    DBEdit1: TDBEdit;
    Label18: TLabel;
    EdVL_ITEM: TdmkEdit;
    EdVL_Outro: TdmkEdit;
    Label17: TLabel;
    Label21: TLabel;
    EdVL_DESC: TdmkEdit;
    Label10: TLabel;
    EdVL_OPR: TdmkEdit;
    EdVL_RED_BC: TdmkEdit;
    Label4: TLabel;
    Label6: TLabel;
    EdVL_BC_ICMS_UF: TdmkEdit;
    Label7: TLabel;
    EdVL_ICMS_UF: TdmkEdit;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdVL_ITEMChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SBProdutoClick(Sender: TObject);
    procedure EdOri_IPIvIPIChange(Sender: TObject);
    procedure CkDtCorrApoClick(Sender: TObject);
    procedure QrPQCliCalcFields(DataSet: TDataSet);
    procedure EdALIQ_ICMSChange(Sender: TObject);
    procedure EdALIQ_PISChange(Sender: TObject);
    procedure EdALIQ_COFINSChange(Sender: TObject);
    procedure Cadastrodoproduto1Click(Sender: TObject);
    procedure Dadosfiscais1Click(Sender: TObject);
    procedure SbCFOPClick(Sender: TObject);
    procedure EdCST_ICMSChange(Sender: TObject);
    procedure EdCST_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCST_PISChange(Sender: TObject);
    procedure EdCST_COFINSChange(Sender: TObject);
    procedure EdCST_PISKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCST_COFINSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdVL_BC_ICMSChange(Sender: TObject);
    procedure EdVL_BC_PISChange(Sender: TObject);
    procedure EdVL_BC_COFINSChange(Sender: TObject);
    procedure EdCFOPChange(Sender: TObject);
    procedure EdVL_BC_COFINSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdVL_BC_PISKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdVL_BC_ICMSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNAT_BC_CREDChange(Sender: TObject);
    procedure EdNAT_BC_CREDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdVL_DESCChange(Sender: TObject);
    procedure EdVL_OutroChange(Sender: TObject);
    procedure EdVL_OPRChange(Sender: TObject);
  private
    { Private declarations }
    F_NAT_BC_CRED: MyArrayLista;
    //
    procedure ReopenPQCli();
    procedure CalculaRetornoImpostos(Qual: TRetornImpost);
    procedure ConfiguraImpostos(Insumo: Integer);
    //procedure CalculaC197();
  public
    { Public declarations }
    FData: TDateTime;
    FNewConta: Integer;
(*
    FQrCab, FQrIts: TmySQLQuery;
    FThisFatID, FThisFatNum, FThisMovimCod, FConta, FRegrFiscal: Integer;
*)
    //
    procedure CalculaOnEdit;
    //procedure MostraTodosPQs();
    procedure Pesos(Index: Integer);
    procedure ReopenPQ(Quais: TPartOuTodo);
    procedure ReopenCFOP();
    procedure ReopenGraGruX();
    function  ValorDaBC(): Double;

  end;

var
  FmEfdInnD500Its: TFmEfdInnD500Its;

implementation

uses UnMyObjects, PQE, Module, Principal, BlueDermConsts, UMySQLModule,
  UnAppPF, UnMoedas, UnGrade_Jan, ModProd, GetValor, UnFinanceiro, NFe_PF,
  ModuleNFe_0000, ModuleGeral, UnSPED_PF, UnSPED_Geral_PF, EfdInnD001Ger;

{$R *.DFM}


procedure TFmEfdInnD500Its.CalculaOnEdit();
var
  VL_ITEM, VL_Outro, VL_DESC, VL_OPR: Double;
begin
  VL_ITEM  := EdVL_ITEM.ValueVariant;
  VL_Outro := EdVL_Outro.ValueVariant;
  VL_DESC  := EdVL_DESC.ValueVariant;
  VL_OPR   := VL_ITEM + VL_Outro - VL_DESC;
  EdVL_OPR.ValueVariant := VL_OPR;
end;

procedure TFmEfdInnD500Its.CalculaRetornoImpostos(Qual: TRetornImpost);
const
  sProcName = 'FmPQEIts.CalculaRetornoImpostos()';
var
  pICMS,pPIS, pCofins, vICMS, vPIS, vCofins, ICMS_vBC, PISS_vBC, COFINS_vBC,
  _VL_OPR, AjusteVL_OUTROS, VL_ICMS_ST: Double;
  AjusteVL_OPR, BC: Double;
begin
(*
  if ImgTipo.SQLType = stIns then
  begin
    BC := EdVL_ITEM.ValueVariant - EdVL_DESC.ValueVariant;
    //
    EdVL_BC_ICMS.ValueVariant;
    EdVL_BC_PIS.ValueVariant;
    EdVL_BC_COFINS.ValueVariant;
  end;
*)
  ICMS_vBC   := EdVL_BC_ICMS.ValueVariant;
  PISS_vBC   := EdVL_BC_PIS.ValueVariant;
  COFINS_vBC := EdVL_BC_COFINS.ValueVariant;

  pICMS     := EdALIQ_ICMS.ValueVariant;
  pPIS      := EdALIQ_PIS.ValueVariant;
  pCofins   := EdALIQ_COFINS.ValueVariant;
  vICMS     := Geral.RoundC(ICMS_vBC * pICMS / 100, 2);
  vPIS      := Geral.RoundC(PISS_vBC * pPIS / 100, 2);
  vCofins   := Geral.RoundC(COFINS_vBC * pCOFINS / 100, 2);
  case Qual of
    //retimpostIndef=1,
    //retimpostNenhum: Exit;
    retimpostTodos:
    begin
      EdVL_ICMS.ValueVariant := vICMS;
      EdVL_PIS.ValueVariant := vPIS;
      EdVL_COFINS.ValueVariant := vCOFINS;
    end;
    retimpostICMS: EdVL_ICMS.ValueVariant := vICMS;
    retimpostPIS: EdVL_PIS.ValueVariant := vPIS;
    retimpostCOFINS: EdVL_COFINS.ValueVariant := vCOFINS;
    //retimpostIPI=7);
    else Geral.MB_Erro('TRetornImpost n�o implementado em ' + sProcName);
  end;
  //

(*  Parei aqui!
  AjusteVL_OUTROS := SPED_PF.FormulaVL_OUTROS(_VL_OPR, ICMS_vBC, VL_ICMS_ST,
  VL_RED_BC, IPI_vIPI);
*)

end;

procedure TFmEfdInnD500Its.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdInnD500Its.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FNewConta := 0;
  //
  F_NAT_BC_CRED := UnNFe_PF.ListaNAT_BC_CRED();
  //FQuaisInsumosMostra := TPartOuTodo.ptParcial;
  //
  ReopenGraGruX();
  ReopenCFOP();
  //
  EdUCTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV('0' + EdCST_ICMS.Text));
  EdUCTextoPIS_CST.Text := UFinanceiro.CST_PIS_Get(EdCST_PIS.Text);
  EdUCTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(EdCST_COFINS.Text);
end;

procedure TFmEfdInnD500Its.BtConfirmaClick(Sender: TObject);
var
  UNID, CST_PIS, NAT_BC_CRED, COD_CTA, CST_COFINS: String;
  AnoMes, Empresa, Codigo, Controle, Conta, GraGruX, CST_ICMS, CFOP: Integer;
  QTD, VL_ITEM, VL_DESC, VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST, ALIQ_ST,
  VL_ICMS_ST, VL_BC_PIS, ALIQ_PIS, VL_PIS, VL_BC_COFINS, ALIQ_COFINS, VL_COFINS,
  VL_Outro, VL_OPR, VL_RED_BC, VL_BC_ICMS_UF, VL_ICMS_UF: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  Conta          := EdConta.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  QTD            := EdQTD.ValueVariant;;
  UNID           := '';//EdUNID.ValueVariant;
  VL_ITEM        := EdVL_ITEM.ValueVariant;
  VL_DESC        := EdVL_DESC.ValueVariant;
  CST_ICMS       := EdCST_ICMS.ValueVariant;
  CFOP           := EdCFOP.ValueVariant;
  VL_BC_ICMS     := EdVL_BC_ICMS.ValueVariant;
  ALIQ_ICMS      := EdALIQ_ICMS.ValueVariant;
  VL_ICMS        := EdVL_ICMS.ValueVariant;
  VL_BC_ICMS_ST  := EdVL_BC_ICMS_ST.ValueVariant;
  ALIQ_ST        := EdALIQ_ST.ValueVariant;
  VL_ICMS_ST     := EdVL_ICMS_ST.ValueVariant;
  CST_PIS        := EdCST_PIS.ValueVariant;
  NAT_BC_CRED    := EdNAT_BC_CRED.ValueVariant;
  VL_BC_PIS      := EdVL_BC_PIS.ValueVariant;
  ALIQ_PIS       := EdALIQ_PIS.ValueVariant;
  VL_PIS         := EdVL_PIS.ValueVariant;
  COD_CTA        := ''; // EdCOD_CTA.ValueVariant;
  CST_COFINS     := EdCST_COFINS.ValueVariant;
  VL_BC_COFINS   := EdVL_BC_COFINS.ValueVariant;
  ALIQ_COFINS    := EdALIQ_COFINS.ValueVariant;
  VL_COFINS      := EdVL_COFINS.ValueVariant;
  VL_BC_ICMS_UF  := EdVL_BC_ICMS_UF.ValueVariant;
  VL_ICMS_UF     := EdVL_ICMS_UF.ValueVariant;
  //
  //
  VL_Outro       := EdVL_Outro.ValueVariant;
  VL_OPR         := EdVL_OPR.ValueVariant;
  VL_RED_BC      := EdVL_RED_BC.ValueVariant;
  VL_RED_BC      := EdVL_RED_BC.ValueVariant;
  //
  if DModG.ExigePisCofins() then
  begin
     if VL_PIS >= 0.01 then
       if MyObjects.FIC(Length(EdCST_PIS.Text) <> 2, EdCST_PIS,
       'Informe o CST do PIS com dois d�gitos!') then Exit;
     //
     if VL_COFINS >= 0.01 then
       if MyObjects.FIC(Length(EdCST_COFINS.Text) <> 2, EdCST_COFINS,
       'Informe o CST da COFINS com dois d�gitos!') then Exit;
     if MyObjects.FIC(NAT_BC_CRED = EmptyStr, EdNAT_BC_CRED,
     'Informe o C�digo da Base de C�lculo do Cr�dito!') then Exit;
  end;
  Conta := UMyMod.BPGS1I32('efdinnd500its', 'Conta', '', '', tsPos, SQLType, Conta);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdinnd500its', False, [
  'AnoMes', 'Empresa', 'Codigo', 'Controle',
  'GraGruX', 'QTD', 'UNID',
  'VL_ITEM', 'VL_DESC', 'CST_ICMS',
  'CFOP', 'VL_BC_ICMS', 'ALIQ_ICMS',
  'VL_ICMS', 'VL_BC_ICMS_ST', 'ALIQ_ST',
  'VL_ICMS_ST', 'CST_PIS', 'NAT_BC_CRED',
  'VL_BC_PIS', 'ALIQ_PIS', 'VL_PIS',
  'COD_CTA', 'CST_COFINS', 'VL_BC_COFINS',
  'ALIQ_COFINS', 'VL_COFINS',
  'VL_Outro', 'VL_OPR', 'VL_RED_BC',
  'VL_BC_ICMS_UF', 'VL_ICMS_UF'], [
  'Conta'], [
  AnoMes, Empresa, Codigo, Controle,
  GraGruX, QTD, UNID,
  VL_ITEM, VL_DESC, CST_ICMS,
  CFOP, VL_BC_ICMS, ALIQ_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, ALIQ_ST,
  VL_ICMS_ST, CST_PIS, NAT_BC_CRED,
  VL_BC_PIS, ALIQ_PIS, VL_PIS,
  COD_CTA, CST_COFINS, VL_BC_COFINS,
  ALIQ_COFINS, VL_COFINS,
  VL_Outro, VL_OPR, VL_RED_BC,
  VL_BC_ICMS_UF, VL_ICMS_UF], [
  Conta], True) then
  begin
    FNewConta := Controle;
    FmEfdInnD001Ger.AtualizaTotaisD500(Controle);
    // Parei aqui
    Close;
  end;
end;

procedure TFmEfdInnD500Its.EdOri_IPIvIPIChange(Sender: TObject);
begin
  CalculaOnEdit();
end;

procedure TFmEfdInnD500Its.EdALIQ_COFINSChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostCOFINS);
end;

procedure TFmEfdInnD500Its.EdALIQ_ICMSChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostICMS);
end;

procedure TFmEfdInnD500Its.EdALIQ_PISChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostPIS);
end;

procedure TFmEfdInnD500Its.EdVL_ITEMChange(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmEfdInnD500Its.EdVL_OPRChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostTodos);
end;

procedure TFmEfdInnD500Its.EdVL_OutroChange(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmEfdInnD500Its.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdInnD500Its.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdInnD500Its.EdCFOPChange(Sender: TObject);
var
  EhServico: Boolean;
  Empresa, CodInfoEmit, RegrFiscal, CRT_Emitido, CST_A_Emitido, CST_B_Emitido,
  CSOSN_Emitido, CFOP_Emitido: Integer;
  CFOP_Entrada: String; (*var ICMS_Aliq: Double;*)
  CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer;
  InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST: String;
  OriTES, EFD_II_C195, GenCtbD, GenCtbC: Integer;
  TES_ICMS, TES_IPI, TES_PIS, TES_COFINS, PVD_MadeBy: Integer;
  ICMSAliq, PISAliq, COFINSAliq: Double;
  FisRegGenCtbD, FisRegGenCtbC: Integer;
begin
{ N�o d�, pelo memos por enquanto!!!!
object QrNFeCabA: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT *  '
    'FROM  nfecaba '
    'WHERE FatID=53 '
    'AND FatNum =0 '
    'AND Empresa=-11 ')
  Left = 500
  Top = 296
end
object QrEfdInnNFsCab: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT * '
    'FROM efdinnnfscab'
    'WHERE Controle>0')
  Left = 666
  Top = 273
end
  if (ImgTipo.SQLType = stIns) and (FTpEntrd = 1) then
  begin
    Empresa       := QrEfdInnNFsCabEmpresa.Value;
    CodInfoEmit   := QrEfdInnNFsCabTerceiro.Value;
    PVD_MadeBy    := QrEfdInnNFsCabPVD_MadeBy.Value;
    RegrFiscal    := QrEfdInnNFsCabRegrFiscal.Value;
    EhServico     := QrEfdInnNFsCabEhServico.Value;
    CRT_Emitido   := QrNFeCabAemit_CRT.Value;
    CST_A_Emitido := QrEfdInnNFsCabCST_A_Emitido.Value;
    CST_B_Emitido := QrEfdInnNFsCabCST_B_Emitido.Value;
    CSOSN_Emitido := QrEfdInnNFsCabCSOSN_Emitido.Value;
    CFOP_Emitido  := QrEfdInnNFsCabCFOP_Emitido.Value;
    //
    if DmNFe_0000.ObtemNumeroCFOP_Entrada(
    // constantes de entrada
    Empresa, CodInfoEmit, PVD_MadeBy, RegrFiscal, EhServico,
    CRT_Emitido, CST_A_Emitido, CST_B_Emitido, CSOSN_Emitido,
    Geral.FormataCFOP(Geral.FF0(CFOP_Emitido)),
    // vari�veis de sa�da
    CFOP_Entrada, (*var ICMS_Aliq: Double;*)
    CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib,
    InnICMS_CST, InnIPI_CST, InnPIS_CST, InnCOFINS_CST,
    OriTES, EFD_II_C195, GenCtbD, GenCtbC,
    TES_ICMS, TES_IPI, TES_PIS, TES_COFINS,
    ICMSAliq, PISAliq, COFINSAliq, FisRegGenCtbD, FisRegGenCtbC);
    if Result then
    begin

    end;
  end;
}
end;

procedure TFmEfdInnD500Its.EdCST_COFINSChange(Sender: TObject);
begin
  EdUCTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(EdCST_COFINS.Text);
end;

procedure TFmEfdInnD500Its.EdCST_COFINSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCST_COFINS.Text := UFinanceiro.ListaDeCST_COFINS();
end;

procedure TFmEfdInnD500Its.EdVL_BC_COFINSChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostCOFINS);
end;

procedure TFmEfdInnD500Its.EdVL_BC_COFINSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Aliquota: Double;
begin
  if Key = VK_F4 then
  begin
    EdVL_BC_COFINS.ValueVariant := ValorDaBC();
    if SPED_Geral_PF.DefineAliquotaCOFINSPeloRegimeTributario(Aliquota) = True then
      EdALIQ_COFINS.ValueVariant := Aliquota;
  end;
end;

procedure TFmEfdInnD500Its.EdCST_ICMSChange(Sender: TObject);
begin
  //EdUCTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdCST_ICMS.Text));
  EdUCTextoB.Text := DmNFe_0000.ObtemDescricao_TabGov_1(EdCST_ICMS.ValueVariant,
    CO_NOME_tbspedefd_ICMS_CST, FData);
end;

procedure TFmEfdInnD500Its.EdCST_ICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', CO_NOME_tbspedefd_ICMS_CST, DModG.AllID_DB,
    ''(*Extra*), EdCST_ICMS, (*CBICMS_CST*)nil, dmktfInteger);
end;

procedure TFmEfdInnD500Its.EdVL_BC_ICMSChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostICMS);
end;

procedure TFmEfdInnD500Its.EdVL_BC_ICMSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdVL_BC_ICMS.ValueVariant := ValorDaBC();
end;

procedure TFmEfdInnD500Its.EdNAT_BC_CREDChange(Sender: TObject);
var
  NAT_BC_CRED: Byte;
begin
  EdNAT_BC_CRED_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_Txt2(spedNAT_BC_CRED,
    EdNAT_BC_CRED.ValueVariant, F_NAT_BC_CRED);
  //
end;

procedure TFmEfdInnD500Its.EdNAT_BC_CREDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdNAT_BC_CRED.Text := Geral.SelecionaItem(F_NAT_BC_CRED, 0,
      'SEL-LISTA-000 :: Indicador do tipo de pagamento',
      TitCols, Screen.Width)
  end;
end;

procedure TFmEfdInnD500Its.EdCST_PISChange(Sender: TObject);
begin
  EdUCTextoPIS_CST.Text := UFinanceiro.CST_PIS_Get(EdCST_PIS.Text);
end;

procedure TFmEfdInnD500Its.EdCST_PISKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCST_PIS.Text := UFinanceiro.ListaDeCST_PIS();
end;

procedure TFmEfdInnD500Its.EdVL_BC_PISChange(Sender: TObject);
begin
  CalculaRetornoImpostos(TRetornImpost.retimpostPIS);
end;

procedure TFmEfdInnD500Its.EdVL_BC_PISKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Aliquota: Double;
begin
  if Key = VK_F4 then
  begin
    EdVL_BC_PIS.ValueVariant := ValorDaBC();
    if SPED_Geral_PF.DefineAliquotaPISPeloRegimeTributario(Aliquota) = True then
      EdALIQ_PIS.ValueVariant := Aliquota;
  end;
end;

procedure TFmEfdInnD500Its.EdVL_DESCChange(Sender: TObject);
begin
  CalculaOnEdit;
end;

procedure TFmEfdInnD500Its.Pesos(Index: Integer);
begin
end;

procedure TFmEfdInnD500Its.QrPQCliCalcFields(DataSet: TDataSet);
begin
  QrPQCliNOMEMOEDAPADRAO.Value := UMoedas.ObtemNomeMoeda(QrPQCliMoedaPadrao.Value);
end;

procedure TFmEfdInnD500Its.ReopenCFOP();
begin
  //WHERE Codigo=CONCAT(SUBSTRING(1234, 1, 1), '.', SUBSTRING(1234, 2, 4))
  UnDmkDAC_PF.AbreMySQLQuery0(QrCFOP, Dmod.MyDB, [
  'SELECT CAST(CONCAT(SUBSTRING(cfp.Codigo, 1, 1), SUBSTRING(cfp.Codigo, 3, 3)) AS SIGNED) Codigo,',
  'cfp.Nome',
  'FROM cfop2003 cfp',
  'ORDER BY cfp.Nome',
  '']);
end;

procedure TFmEfdInnD500Its.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
begin
  SQL_AND := '';
  SQL_LFT := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  SQL_LFT,
  'WHERE ggx.Controle > 0 ',
  'AND pgt.Codigo=-11 > 0 ', // produtos D500
  SQL_AND,
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
  //Geral.MB_Teste(QrGraGruX.SQL.Text);
end;

procedure TFmEfdInnD500Its.ReopenPQ(Quais: TPartOuTodo);
var
  SQL_IQ: String;
begin
(*
  if Quais = ptIndef then
    UnDmkDAC_PF.AbreQuery(QrPQ1, Dmod.MyDB)
  else
  begin
    if Quais = ptTotal then
      SQL_IQ := ''
    else
      SQL_IQ := 'AND pfo.IQ=' + Geral.FF0(FmPQE.QrPQEIQ.Value);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPQ1, Dmod.MyDB, [
    'SELECT DISTINCT pq.Nome NOMEPQ, pfo.PQ ',
    'FROM pqfor pfo ',
    'LEFT JOIN pq pq ON pfo.PQ=pq.Codigo ',
    'WHERE pfo.CI=' + Geral.FF0(FmPQE.QrPQECI.Value),
    SQL_IQ,
    'AND pfo.Empresa=' + Geral.FF0(FEmpresa),
    'AND pq.Ativo = 1 ',
    'AND pq.GGXNiv2 > 0 ',
    'ORDER BY pq.Nome ',
    '']);
  end;
*)
end;

procedure TFmEfdInnD500Its.ReopenPQCli();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQCli, Dmod.MyDB, [
    'SELECT CustoPadrao, MoedaPadrao ',
    'FROM pqcli ',
    'WHERE PQ=' + Geral.FF0(QrPQ1PQ.Value),
    'AND CI=' + Geral.FF0(FmPQE.QrPQECI.Value),
    '']);
end;

procedure TFmEfdInnD500Its.ConfiguraImpostos(Insumo: Integer);
begin
{
  if Dmod.QrControleNaoRecupImposPQ.Value = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1, Dmod.MyDB, [
      'SELECT ICMSRec_pAliq, PISRec_pAliq,  ',
      'COFINSRec_pAliq  ',
      'FROM gragru1 ',
      'WHERE Nivel1=' + Geral.FF0(Insumo),
      '']);
    //
    EdRpICMS.ValueVariant   := QrGraGru1ICMSRec_pAliq.Value;
    EdRpPIS.ValueVariant    := QrGraGru1PISRec_pAliq.Value;
    EdRpCOFINS.ValueVariant := QrGraGru1COFINSRec_pAliq.Value;
  end else
  begin
    EdRpICMS.ValueVariant   := 0.00;
    EdRpPIS.ValueVariant    := 0.00;
    EdRpCOFINS.ValueVariant := 0.00;

    GBRecuImpost.Enabled := False;
    GBRecuImpost.Caption := 'Recupera��o de impostos: Op��es espec�ficas -> Geral - > N�o recupera impostos na entrada de insumos.!!!!!!!!!!!!!!';
  end;
}
end;

procedure TFmEfdInnD500Its.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmEfdInnD500Its.Dadosfiscais1Click(Sender: TObject);
var
  Insumo: Integer;
begin
{
  VAR_CADASTRO := 0;
  Insumo       := EdInsumo.ValueVariant;
  //
  Grade_Jan.MostraFormGraGruN(Insumo, 0, tpGraCusPrc, tsfDadosFiscais);
  //
  if BDC_PQCAD <> 0 then
    ConfiguraDados(BDC_PQCAD)
  else if VAR_CADASTRO <> 0 then
    ConfiguraDados(VAR_CADASTRO);
  //
  ConfiguraImpostos(Insumo);
}
end;

procedure TFmEfdInnD500Its.Cadastrodoproduto1Click(Sender: TObject);
var
  Insumo: Integer;
begin
{
  VAR_CADASTRO := 0;
  Insumo       := EdInsumo.ValueVariant;
  //
  FmPrincipal.CadastroPQ(Insumo);
  //
  if BDC_PQCAD <> 0 then
    ConfiguraDados(BDC_PQCAD)
  else if VAR_CADASTRO <> 0 then
    ConfiguraDados(VAR_CADASTRO);
  //
  ConfiguraImpostos(Insumo);
}
end;

(*
procedure TFmEfdInnNFsIts.CalculaC197();
var
  AjusteVL_OPR, AjusteVL_RED_BC: Double;
begin
  AjusteVL_OPR     := SPED_PF.FormulaVL_OPR(
                      Edprod_vProd.ValueVariant,
                      Edprod_vFrete.ValueVariant,
                      Edprod_vSeg.ValueVariant,
                      Edprod_vOutro.ValueVariant,
                      EdVL_DESC.ValueVariant);
  AjusteVL_RED_BC  := SPED_PF.FormulaVL_RED_BC(AjusteVL_OPR, VL_BC, pRedBC

  AjusteVL_OPR     := EdVL_Item.ValueVariant;
  AjusteVL_ICMS_ST := EdVL_ICMS_ST.ValueVariant;
end;
*)

procedure TFmEfdInnD500Its.SbCFOPClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormCFOP2003();
  if VAR_CADASTRO <> 0 then
  begin
    ReopenCFOP();
    UMyMod.SetaCodigoPesquisado(EdCFOP, CBCFOP, QrCFOP, VAR_CADASTRO);
  end;
end;

procedure TFmEfdInnD500Its.SBProdutoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProduto, SBProduto);
end;

function TFmEfdInnD500Its.ValorDaBC(): Double;
begin
  Result := EdVL_OPR.ValueVariant;
end;


(*
limitar CST e NAT_BC_CRED tanto de PIS como de COFINS nos registros C500 e D500
Fazer registros M200 e M600 (e M400 e M800)
*)

end.


