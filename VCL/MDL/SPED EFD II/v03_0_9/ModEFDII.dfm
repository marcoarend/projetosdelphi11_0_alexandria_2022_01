object DmEFDII: TDmEFDII
  Height = 480
  Width = 640
  PixelsPerInch = 96
  object QrEfdInnNFsCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vin.*, '
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  '
      'FROM efdinnnfscab vin '
      'LEFT JOIN entidades ter ON ter.Codigo=vin.Terceiro '
      ' ')
    Left = 28
    Top = 6
    object QrEfdInnNFsCabNO_TER: TWideStringField
      FieldName = 'NO_TER'
      Size = 100
    end
    object QrEfdInnNFsCabMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnNFsCabMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnNFsCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnNFsCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnNFsCabControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnNFsCabTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrEfdInnNFsCabPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrEfdInnNFsCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrEfdInnNFsCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrEfdInnNFsCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrEfdInnNFsCabValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrEfdInnNFsCabMotorista: TIntegerField
      FieldName = 'Motorista'
      Required = True
    end
    object QrEfdInnNFsCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrEfdInnNFsCabCOD_MOD: TSmallintField
      FieldName = 'COD_MOD'
      Required = True
    end
    object QrEfdInnNFsCabCOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
      Required = True
    end
    object QrEfdInnNFsCabSER: TIntegerField
      FieldName = 'SER'
      Required = True
    end
    object QrEfdInnNFsCabNUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Required = True
    end
    object QrEfdInnNFsCabCHV_NFE: TWideStringField
      FieldName = 'CHV_NFE'
      Size = 44
    end
    object QrEfdInnNFsCabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
      Required = True
    end
    object QrEfdInnNFsCabDT_DOC: TDateField
      FieldName = 'DT_DOC'
      Required = True
    end
    object QrEfdInnNFsCabDT_E_S: TDateField
      FieldName = 'DT_E_S'
      Required = True
    end
    object QrEfdInnNFsCabVL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Required = True
    end
    object QrEfdInnNFsCabIND_PGTO: TWideStringField
      FieldName = 'IND_PGTO'
      Size = 1
    end
    object QrEfdInnNFsCabVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
    end
    object QrEfdInnNFsCabVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Required = True
    end
    object QrEfdInnNFsCabVL_MERC: TFloatField
      FieldName = 'VL_MERC'
      Required = True
    end
    object QrEfdInnNFsCabIND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEfdInnNFsCabVL_FRT: TFloatField
      FieldName = 'VL_FRT'
      Required = True
    end
    object QrEfdInnNFsCabVL_SEG: TFloatField
      FieldName = 'VL_SEG'
      Required = True
    end
    object QrEfdInnNFsCabVL_OUT_DA: TFloatField
      FieldName = 'VL_OUT_DA'
      Required = True
    end
    object QrEfdInnNFsCabVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
    end
    object QrEfdInnNFsCabVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
    end
    object QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Required = True
    end
    object QrEfdInnNFsCabVL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Required = True
    end
    object QrEfdInnNFsCabVL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Required = True
    end
    object QrEfdInnNFsCabVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
    end
    object QrEfdInnNFsCabVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
    end
    object QrEfdInnNFsCabVL_PIS_ST: TFloatField
      FieldName = 'VL_PIS_ST'
      Required = True
    end
    object QrEfdInnNFsCabVL_COFINS_ST: TFloatField
      FieldName = 'VL_COFINS_ST'
      Required = True
    end
    object QrEfdInnNFsCabNFe_FatID: TIntegerField
      FieldName = 'NFe_FatID'
      Required = True
    end
    object QrEfdInnNFsCabNFe_FatNum: TIntegerField
      FieldName = 'NFe_FatNum'
      Required = True
    end
    object QrEfdInnNFsCabNFe_StaLnk: TSmallintField
      FieldName = 'NFe_StaLnk'
      Required = True
    end
    object QrEfdInnNFsCabVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
      Required = True
    end
    object QrEfdInnNFsCabVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrEfdInnNFsCabVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrEfdInnNFsCabVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
      Required = True
    end
  end
  object QrEfdInnNFsIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vin.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, '
      'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, '
      'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, '
      'pgt.Tipo_Item) Tipo_Item '
      'FROM efdinnnfsits     vin'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vin.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ')
    Left = 28
    Top = 58
    object QrEfdInnNFsItsMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnNFsItsMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnNFsItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnNFsItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnNFsItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrEfdInnNFsItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrEfdInnNFsItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnNFsItsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEfdInnNFsItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
    object QrEfdInnNFsItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrEfdInnNFsItsprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      Required = True
    end
    object QrEfdInnNFsItsprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Required = True
    end
    object QrEfdInnNFsItsprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Required = True
    end
    object QrEfdInnNFsItsprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrEfdInnNFsItsQTD: TFloatField
      FieldName = 'QTD'
      Required = True
    end
    object QrEfdInnNFsItsUNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrEfdInnNFsItsVL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      Required = True
    end
    object QrEfdInnNFsItsVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
    end
    object QrEfdInnNFsItsIND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
    object QrEfdInnNFsItsCST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrEfdInnNFsItsCFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrEfdInnNFsItsCOD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrEfdInnNFsItsVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      Required = True
    end
    object QrEfdInnNFsItsVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
    end
    object QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_ST: TFloatField
      FieldName = 'ALIQ_ST'
      Required = True
    end
    object QrEfdInnNFsItsVL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Required = True
    end
    object QrEfdInnNFsItsIND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrEfdInnNFsItsCST_IPI: TWideStringField
      FieldName = 'CST_IPI'
      Size = 2
    end
    object QrEfdInnNFsItsCOD_ENQ: TWideStringField
      FieldName = 'COD_ENQ'
      Size = 3
    end
    object QrEfdInnNFsItsVL_BC_IPI: TFloatField
      FieldName = 'VL_BC_IPI'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_IPI: TFloatField
      FieldName = 'ALIQ_IPI'
      Required = True
    end
    object QrEfdInnNFsItsVL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Required = True
    end
    object QrEfdInnNFsItsCST_PIS: TSmallintField
      FieldName = 'CST_PIS'
    end
    object QrEfdInnNFsItsVL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_PIS_p: TFloatField
      FieldName = 'ALIQ_PIS_p'
      Required = True
    end
    object QrEfdInnNFsItsQUANT_BC_PIS: TFloatField
      FieldName = 'QUANT_BC_PIS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_PIS_r: TFloatField
      FieldName = 'ALIQ_PIS_r'
      Required = True
    end
    object QrEfdInnNFsItsVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
    end
    object QrEfdInnNFsItsCST_COFINS: TSmallintField
      FieldName = 'CST_COFINS'
    end
    object QrEfdInnNFsItsVL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_COFINS_p: TFloatField
      FieldName = 'ALIQ_COFINS_p'
      Required = True
    end
    object QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField
      FieldName = 'QUANT_BC_COFINS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_COFINS_r: TFloatField
      FieldName = 'ALIQ_COFINS_r'
      Required = True
    end
    object QrEfdInnNFsItsVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
    end
    object QrEfdInnNFsItsCOD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEfdInnNFsItsVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Required = True
    end
    object QrEfdInnNFsItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
      Required = True
    end
    object QrEfdInnNFsItsxLote: TWideStringField
      FieldName = 'xLote'
    end
    object QrEfdInnNFsItsOri_IPIpIPI: TFloatField
      FieldName = 'Ori_IPIpIPI'
      Required = True
    end
    object QrEfdInnNFsItsOri_IPIvIPI: TFloatField
      FieldName = 'Ori_IPIvIPI'
      Required = True
    end
    object QrEfdInnNFsItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEfdInnNFsItsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrEfdInnNFsItsGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrEfdInnNFsItsNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrEfdInnNFsItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrEfdInnNFsItsEx_TIPI: TWideStringField
      FieldName = 'Ex_TIPI'
      Size = 3
    end
    object QrEfdInnNFsItsGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEfdInnNFsItsTipo_Item: TSmallintField
      FieldName = 'Tipo_Item'
    end
  end
end
