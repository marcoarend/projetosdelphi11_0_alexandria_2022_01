unit EFD_D001_v03_0_9;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Menus, ComCtrls, Grids,
  DBGrids, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, Variants,
  dmkDBGrid, frxClass, frxDBSet, dmkCheckGroup, DmkDAC_PF, dmkImage, UnDmkEnums,
  dmkDBGridZTO;

type
  TFmEFD_D001_v03_0_9 = class(TForm)
    PainelDados: TPanel;
    DsEFD_D001: TDataSource;
    QrEFD_D001: TmySQLQuery;
    dmkPermissoes1: TdmkPermissoes;
    PMPeriodo: TPopupMenu;
    PCPeriodos: TPageControl;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    Itemns1: TMenuItem;
    Panel9: TPanel;
    Panel10: TPanel;
    DBGCab: TdmkDBGrid;
    Panel11: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel12: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    DBEdit1: TDBEdit;
    Panel3: TPanel;
    BtD500: TBitBtn;
    BtPeriodo: TBitBtn;
    Panel2: TPanel;
    BtSaida0: TBitBtn;
    PB1: TProgressBar;
    Panel16: TPanel;
    PMD500: TPopupMenu;
    GroupBox3: TGroupBox;
    Incluinovoperiodo1: TMenuItem;
    Excluiperiodoselecionado1: TMenuItem;
    QrEFD_D500: TmySQLQuery;
    DsEFD_D500: TDataSource;
    PCServicos: TPageControl;
    TabSheet1: TTabSheet;
    IncluiD500: TMenuItem;
    AlteraD500: TMenuItem;
    ExcluiD500: TMenuItem;
    QrEFD_D001NO_ENT: TWideStringField;
    QrEFD_D001MES_ANO: TWideStringField;
    QrEFD_D001ImporExpor: TSmallintField;
    QrEFD_D001AnoMes: TIntegerField;
    QrEFD_D001Empresa: TIntegerField;
    QrEFD_D001LinArq: TIntegerField;
    QrEFD_D001REG: TWideStringField;
    QrEFD_D001IND_MOV: TWideStringField;
    QrEFD_D500NO_TERC: TWideStringField;
    QrEFD_D500ImporExpor: TSmallintField;
    QrEFD_D500AnoMes: TIntegerField;
    QrEFD_D500Empresa: TIntegerField;
    QrEFD_D500LinArq: TIntegerField;
    QrEFD_D500REG: TWideStringField;
    QrEFD_D500IND_OPER: TWideStringField;
    QrEFD_D500IND_EMIT: TWideStringField;
    QrEFD_D500COD_PART: TWideStringField;
    QrEFD_D500COD_MOD: TWideStringField;
    QrEFD_D500SER: TWideStringField;
    QrEFD_D500SUB: TWideStringField;
    QrEFD_D500NUM_DOC: TIntegerField;
    QrEFD_D500DT_DOC: TDateField;
    QrEFD_D500DT_A_P: TDateField;
    QrEFD_D500VL_DOC: TFloatField;
    QrEFD_D500VL_DESC: TFloatField;
    QrEFD_D500VL_SERV: TFloatField;
    QrEFD_D500VL_SERV_NT: TFloatField;
    QrEFD_D500VL_TERC: TFloatField;
    QrEFD_D500VL_DA: TFloatField;
    QrEFD_D500VL_BC_ICMS: TFloatField;
    QrEFD_D500VL_ICMS: TFloatField;
    QrEFD_D500COD_INF: TWideStringField;
    QrEFD_D500VL_PIS: TFloatField;
    QrEFD_D500VL_COFINS: TFloatField;
    QrEFD_D500COD_CTA: TWideStringField;
    QrEFD_D500TP_ASSINANTE: TWideStringField;
    QrEFD_D500Terceiro: TIntegerField;
    QrEFD_D500Importado: TSmallintField;
    QrEFD_D500CST_ICMS: TWideStringField;
    QrEFD_D500CFOP: TWideStringField;
    QrEFD_D500ALIQ_ICMS: TFloatField;
    QrEFD_D500VL_RED_BC: TFloatField;
    QrEFD_D500COD_SIT: TWideStringField;
    TabSheet3: TTabSheet;
    dmkDBGrid1: TdmkDBGrid;
    QrEFD_D100: TmySQLQuery;
    DsEFD_D100: TDataSource;
    dmkDBGridZTO1: TdmkDBGridZTO;
    BtD100: TBitBtn;
    PMD100: TPopupMenu;
    IncluinovaNFD1001: TMenuItem;
    AlteraNFD100selecionada1: TMenuItem;
    ExcluiNFD100selecionada1: TMenuItem;
    QrEFD_D100NO_TERC: TWideStringField;
    QrEFD_D100ImporExpor: TSmallintField;
    QrEFD_D100AnoMes: TIntegerField;
    QrEFD_D100Empresa: TIntegerField;
    QrEFD_D100LinArq: TIntegerField;
    QrEFD_D100REG: TWideStringField;
    QrEFD_D100IND_OPER: TWideStringField;
    QrEFD_D100IND_EMIT: TWideStringField;
    QrEFD_D100COD_PART: TWideStringField;
    QrEFD_D100COD_MOD: TWideStringField;
    QrEFD_D100COD_SIT: TWideStringField;
    QrEFD_D100SER: TWideStringField;
    QrEFD_D100SUB: TWideStringField;
    QrEFD_D100NUM_DOC: TIntegerField;
    QrEFD_D100CHV_CTE: TWideStringField;
    QrEFD_D100DT_DOC: TDateField;
    QrEFD_D100DT_A_P: TDateField;
    QrEFD_D100TP_CTE: TSmallintField;
    QrEFD_D100CHV_CTE_REF: TWideStringField;
    QrEFD_D100VL_DOC: TFloatField;
    QrEFD_D100VL_DESC: TFloatField;
    QrEFD_D100IND_FRT: TWideStringField;
    QrEFD_D100VL_SERV: TFloatField;
    QrEFD_D100VL_BC_ICMS: TFloatField;
    QrEFD_D100VL_ICMS: TFloatField;
    QrEFD_D100VL_NT: TFloatField;
    QrEFD_D100COD_INF: TWideStringField;
    QrEFD_D100COD_CTA: TWideStringField;
    QrEFD_D100Terceiro: TIntegerField;
    QrEFD_D100Importado: TSmallintField;
    QrEFD_D100Lk: TIntegerField;
    QrEFD_D100DataCad: TDateField;
    QrEFD_D100DataAlt: TDateField;
    QrEFD_D100UserCad: TIntegerField;
    QrEFD_D100UserAlt: TIntegerField;
    QrEFD_D100AlterWeb: TSmallintField;
    QrEFD_D100Ativo: TSmallintField;
    QrEFD_D100CST_ICMS: TIntegerField;
    QrEFD_D100CFOP: TIntegerField;
    QrEFD_D100ALIQ_ICMS: TFloatField;
    QrEFD_D100VL_RED_BC: TFloatField;
    procedure BtD500Click(Sender: TObject);
    procedure BtSaida0Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPeriodoClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrEFD_D001BeforeClose(DataSet: TDataSet);
    procedure QrEFD_D001AfterScroll(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure QrEFD_D001AfterOpen(DataSet: TDataSet);
    procedure PMPeriodoPopup(Sender: TObject);
    procedure RGEmitenteClick(Sender: TObject);
    procedure PMD500Popup(Sender: TObject);
    procedure Incluinovoperiodo1Click(Sender: TObject);
    procedure Excluiperiodoselecionado1Click(Sender: TObject);
    procedure IncluiD500Click(Sender: TObject);
    procedure AlteraD500Click(Sender: TObject);
    procedure ExcluiD500Click(Sender: TObject);
    procedure IncluinovaNFD1001Click(Sender: TObject);
    procedure AlteraNFD100selecionada1Click(Sender: TObject);
    procedure ExcluiNFD100selecionada1Click(Sender: TObject);
    procedure BtD100Click(Sender: TObject);
  private
    { Private declarations }
    function  PeriodoJaExiste(ImporExpor, AnoMes, Empresa: Integer;
              Avisa: Boolean): Boolean;
    procedure InsUpdD100(SQLType: TSQLType);
    procedure InsUpdD500(SQLType: TSQLType);
    //

  public
    { Public declarations }
    FEmpresa: Integer;
    FExpImpTXT, FEmprTXT: String;
    //
    procedure ReopenEFD_D001(AnoMes: Integer);
    procedure ReopenEFD_D100(LinArq: Integer);
    procedure ReopenEFD_D500(LinArq: Integer);
  end;

var
  FmEFD_D001_v03_0_9: TFmEFD_D001_v03_0_9;

implementation

uses UnMyObjects, Module, MyDBCheck, ModuleGeral, ModProd, UCreate,
CfgExpFile, UnGrade_Tabs, ModuleNFe_0000,
  Principal, Periodo, (*EFD_E100, EFD_E110, EFD_E111, EFD_E116,
  EFD_E112, EFD_E113, EFD_E115, EFD_D100,*) EFD_D500_v03_0_9;

{$R *.DFM}

const
  FFormatFloat = '00000';
  FImporExpor = 3; // Criar! N�o Mexer

procedure TFmEFD_D001_v03_0_9.EdEmpresaChange(Sender: TObject);
begin
  if EdEmpresa.ValueVariant <> 0 then
  begin
    FEmpresa := DModG.QrEmpresasCodigo.Value;
    FEmprTXT := FormatFloat('0', FEmpresa);
  end else
  begin
    FEmpresa := 0;
    FEmprTXT := '0';
  end;
  BtPeriodo.Enabled := FEmpresa <> 0;
  ReopenEFD_D001(0);
end;

procedure TFmEFD_D001_v03_0_9.ExcluiD500Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o da NF Tipo D500?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipid500', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [QrEFD_D500ImporExpor.Value, QrEFD_D500AnoMes.Value,
    QrEFD_D500Empresa.Value, QrEFD_D500LinArq.Value], '') then
    //
    ReopenEFD_D500(0);
  end;
end;

procedure TFmEFD_D001_v03_0_9.ExcluiNFD100selecionada1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o da NF Tipo D100?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipid100', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [QrEFD_D100ImporExpor.Value, QrEFD_D100AnoMes.Value,
    QrEFD_D100Empresa.Value, QrEFD_D100LinArq.Value], '') then
    //
    ReopenEFD_D100(0);
  end;
end;

procedure TFmEFD_D001_v03_0_9.Excluiperiodoselecionado1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do per�odo selecionado?',
  'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipid001', [
    'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
    [QrEFD_D001ImporExpor.Value, QrEFD_D001AnoMes.Value,
    QrEFD_D001Empresa.Value], '') then
    ReopenEFD_D001(0);
  end;
end;

procedure TFmEFD_D001_v03_0_9.IncluiD500Click(Sender: TObject);
begin
  InsUpdD500(stIns);
end;

procedure TFmEFD_D001_v03_0_9.IncluinovaNFD1001Click(Sender: TObject);
begin
  InsUpdD100(stIns);
end;

procedure TFmEFD_D001_v03_0_9.Incluinovoperiodo1Click(Sender: TObject);
const
  LinArq = 0;
  REG = 'E001';
  IND_MOV = 1;
var
  Ano, Mes, Dia: Word;
  Cancelou: Boolean;
  AnoMes: Integer;
begin
  DecodeDate(Date, Ano, Mes, Dia);
  MLAGeral.EscolhePeriodo_MesEAno(TFmPeriodo, FmPeriodo, Mes, Ano, Cancelou, True, True);
  if not Cancelou then
  begin
    AnoMes := (Ano * 100) + Mes;
    //ShowMessage(FormatFloat('0', (Ano * 100) + Mes));
    if not PeriodoJaExiste(FImporExpor, AnoMes, FEmpresa, True) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efdicmsipid001', False, [
      'LinArq', 'REG', 'IND_MOV'], [
      'ImporExpor', 'AnoMes', 'Empresa'], [
      LinArq, REG, IND_MOV], [
      FImporExpor, AnoMes, FEmpresa], True) then
        ReopenEFD_D001(AnoMes);
    end;
    ReopenEFD_D001(AnoMes);
  end;
end;

procedure TFmEFD_D001_v03_0_9.InsUpdD100(SQLType: TSQLType);
begin
{
  if UmyMod.FormInsUpd_Cria(TFmEFD_D100, FmEFD_D100, afmoNegarComAviso,
  QrEFD_D100, SQLType) then
  begin
    FmEFD_D100.FAnoMes := QrEFD_D001AnoMes.Value;
    FmEFD_D100.ReopenTabelas();
    FmEFD_D100.ImgTipo.SQLType := SQLType;
    FmEFD_D100.EdImporExpor.ValueVariant := QrEFD_D001ImporExpor.Value;
    FmEFD_D100.EdAnoMes.ValueVariant := QrEFD_D001AnoMes.Value;
    FmEFD_D100.EdEmpresa.ValueVariant := QrEFD_D001Empresa.Value;
    //
    if SQLType = stIns then
    begin
      FmEFD_D100.RGIND_OPER.ItemIndex := 0;
      FmEFD_D100.RGIND_EMIT.ItemIndex := 1;
      FmEFD_D100.TPDT_DOC.Date := Date;
      FmEFD_D100.TPDT_A_P.Date := Date;
    end else
    begin
    end;
    //
    FmEFD_D100.ShowModal;
    FmEFD_D100.Destroy;
  end;
}
end;

procedure TFmEFD_D001_v03_0_9.InsUpdD500(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmEFD_D500_v03_0_9, FmEFD_D500_v03_0_9, afmoNegarComAviso,
  QrEFD_D500, SQLType) then
  begin
    FmEFD_D500_v03_0_9.FAnoMes := QrEFD_D001AnoMes.Value;
    FmEFD_D500_v03_0_9.ReopenTabelas();
    FmEFD_D500_v03_0_9.ImgTipo.SQLType := SQLType;
    FmEFD_D500_v03_0_9.EdImporExpor.ValueVariant := QrEFD_D001ImporExpor.Value;
    FmEFD_D500_v03_0_9.EdAnoMes.ValueVariant := QrEFD_D001AnoMes.Value;
    FmEFD_D500_v03_0_9.EdEmpresa.ValueVariant := QrEFD_D001Empresa.Value;
    //
    if SQLType = stIns then
    begin
      FmEFD_D500_v03_0_9.RGIND_OPER.ItemIndex := 0;
      FmEFD_D500_v03_0_9.RGIND_EMIT.ItemIndex := 1;
      FmEFD_D500_v03_0_9.TPDT_DOC.Date := Date;
      FmEFD_D500_v03_0_9.TPDT_A_P.Date := Date;
    end else
    begin
    end;
    //
    FmEFD_D500_v03_0_9.ShowModal;
    FmEFD_D500_v03_0_9.Destroy;
  end;
end;

function TFmEFD_D001_v03_0_9.PeriodoJaExiste(ImporExpor, AnoMes, Empresa: Integer;
Avisa: Boolean): Boolean;
var
  Qry1: TmySQLQuery;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  Qry1.Close;
  Qry1.Database := Dmod.MyDB;
  UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
  'SELECT COUNT(*) Registros ',
  'FROM efdicmsipid001 ',
  'WHERE ImporExpor=' + FormatFloat('0', ImporExpor),
  'AND AnoMes=' + FormatFloat('0', AnoMes),
  'AND Empresa=' + FormatFloat('0', Empresa),
  '']);
  Result := Qry1.FieldByName('Registros').AsInteger > 0;
  if Result and Avisa then
  begin
    Geral.MensagemBox('O periodo selecionado j� existe!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmEFD_D001_v03_0_9.PMPeriodoPopup(Sender: TObject);
begin
  Excluiperiodoselecionado1.Enabled :=
    (QrEFD_D001.State <> dsInactive) and
    (QrEFD_D001.RecordCount > 0)
  and
    (QrEFD_D500.State <> dsInactive) and
    (QrEFD_D500.RecordCount = 0);
end;

procedure TFmEFD_D001_v03_0_9.PMD500Popup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEFD_D001.State <> dsInactive) and (QrEFD_D001.RecordCount > 0);
  IncluiD500.Enabled := Habilita;
  //
  Habilita := Habilita and
    (QrEFD_D500.State <> dsInactive) and (QrEFD_D500.RecordCount > 0);
  AlteraD500.Enabled := Habilita;
  ExcluiD500.Enabled := Habilita and
    (QrEFD_D500.State <> dsInactive) and (QrEFD_D500.RecordCount > 0);
end;

procedure TFmEFD_D001_v03_0_9.RGEmitenteClick(Sender: TObject);
begin
  ReopenEFD_D500(0);
end;

procedure TFmEFD_D001_v03_0_9.ReopenEFD_D001(AnoMes: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_D001, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT, ',
  'CONCAT(RIGHT(e001.AnoMes, 2), "/", ',
  'LEFT(LPAD(e001.AnoMes, 6, "0"), 4)) MES_ANO, e001.* ',
  'FROM efdicmsipid001 e001 ',
  'LEFT JOIN entidades ent ON ent.Codigo=e001.Empresa ',
  'WHERE e001.ImporExpor=' + FormatFloat('0', FImporExpor),
  'AND e001.Empresa=' + FormatFloat('0', FEmpresa),
  'ORDER BY e001.AnoMes DESC ',
  '']);
  //
  QrEFD_D001.Locate('AnoMes', AnoMes, []);
end;

procedure TFmEFD_D001_v03_0_9.ReopenEFD_D100(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_D100, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)  ',
  'NO_TERC, d100.* ',
  'FROM efdicmsipid100 d100  ',
  'LEFT JOIN entidades ent ON ent.Codigo=d100.Terceiro ',
  'WHERE d100.ImporExpor=' + FormatFloat('0', QrEFD_D001ImporExpor.Value),
  'AND d100.Empresa=' + FormatFloat('0', QrEFD_D001Empresa.Value),
  'AND d100.AnoMes=' + FormatFloat('0', QrEFD_D001AnoMes.Value),
  //'ORDER BY d100.?',
  '']);
  //
  QrEFD_D100.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_D001_v03_0_9.ReopenEFD_D500(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_D500, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)  ',
  'NO_TERC, d500.* ',
  'FROM efdicmsipid500 d500  ',
  'LEFT JOIN entidades ent ON ent.Codigo=d500.Terceiro ',
  'WHERE d500.ImporExpor=' + FormatFloat('0', QrEFD_D001ImporExpor.Value),
  'AND d500.Empresa=' + FormatFloat('0', QrEFD_D001Empresa.Value),
  'AND d500.AnoMes=' + FormatFloat('0', QrEFD_D001AnoMes.Value),
  //'ORDER BY d500.?',
  '']);
  //
  QrEFD_D500.Locate('LinArq', LinArq, []);
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEFD_D001_v03_0_9.AlteraNFD100selecionada1Click(Sender: TObject);
begin
  InsUpdD100(stUpd);
end;

procedure TFmEFD_D001_v03_0_9.BtD100Click(Sender: TObject);
begin
  PCServicos.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMD100, BtD100);
end;

procedure TFmEFD_D001_v03_0_9.BtD500Click(Sender: TObject);
begin
  PCServicos.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMD500, BtD500);
end;

procedure TFmEFD_D001_v03_0_9.BtSaida0Click(Sender: TObject);
begin
  //VAR_CADASTRO := Qr?.Value;
  Close;
end;

procedure TFmEFD_D001_v03_0_9.AlteraD500Click(Sender: TObject);
begin
  InsUpdD500(stUpd);
end;

procedure TFmEFD_D001_v03_0_9.BtPeriodoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPeriodo, BtPeriodo);
end;

procedure TFmEFD_D001_v03_0_9.FormCreate(Sender: TObject);
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FEmprTXT := '';
  PCPeriodos.Align := alClient;
  PCPeriodos.ActivePageIndex := 0;
  PCServicos.ActivePageIndex := 0;
  ImgTipo.SQLType := stLok;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmEFD_D001_v03_0_9.SbNumeroClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Codigo(QrCadComItensCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEFD_D001_v03_0_9.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEFD_D001_v03_0_9.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrCadComItensCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmEFD_D001_v03_0_9.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEFD_D001_v03_0_9.QrEFD_D001AfterOpen(DataSet: TDataSet);
begin
  BtD100.Enabled := QrEFD_D001.RecordCount > 0;
  BtD500.Enabled := QrEFD_D001.RecordCount > 0;
end;

procedure TFmEFD_D001_v03_0_9.QrEFD_D001AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_D100(0);
  ReopenEFD_D500(0);
end;

procedure TFmEFD_D001_v03_0_9.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_D001_v03_0_9.SbQueryClick(Sender: TObject);
begin
{
  LocCod(QrCadComItensCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cadcomitens', Dmod.MyDB, CO_VAZIO));
}
end;

procedure TFmEFD_D001_v03_0_9.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEFD_D001_v03_0_9.QrEFD_D001BeforeClose(DataSet: TDataSet);
begin
  BtD500.Enabled := False;
  //
  QrEFD_D100.Close;
  QrEFD_D500.Close;
end;

end.

