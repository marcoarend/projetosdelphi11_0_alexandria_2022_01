unit EfdIcmsIpiK100;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmEfdIcmsIpiK100 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    TPDT_INI: TdmkEditDateTimePicker;
    Label1: TLabel;
    TPDT_FIN: TdmkEditDateTimePicker;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdPeriApu: TdmkEdit;
    Label6: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEfdIcmsIpiK100: TFmEfdIcmsIpiK100;

implementation

uses UnMyObjects, Module, (*EFD_E001,*) UMySQLModule, SpedEfdIcmsIpi;

{$R *.DFM}

procedure TFmEfdIcmsIpiK100.BtOKClick(Sender: TObject);
var
  DT_INI, DT_FIN: String;
  ImporExpor, AnoMes, Empresa, PeriApu: Integer;
begin
  DT_INI := Geral.FDT(TPDT_INI.Date, 1);
  DT_FIN := Geral.FDT(TPDT_FIN.Date, 1);
  //
  ImporExpor := EdImporExpor.ValueVariant;
  AnoMes := EdAnoMes.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  //
  PeriApu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik100', 'PeriApu', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  ImgTipo.SQLType, 0, siPositivo, EdPeriApu);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'efdicmsipik100', False, [
(*
  'REG', 'DT_INI', 'DT_FIN'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
  REG, DT_INI, DT_FIN], [
*)
  'DT_INI', 'DT_FIN'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
  DT_INI, DT_FIN], [
  ImporExpor, AnoMes, Empresa, PeriApu], True) then
  begin
    DfSEII.ReopenK100(0);
    Close;
  end;
end;

procedure TFmEfdIcmsIpiK100.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpiK100.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiK100.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
