object FmEfdIcmsIpi1010: TFmEfdIcmsIpi1010
  Left = 339
  Top = 185
  Caption = 'SPE-1_OUT-010 :: Obrigatoriedade de Registros do Bloco 1'
  ClientHeight = 430
  ClientWidth = 763
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 763
    Height = 268
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 0
      Width = 763
      Height = 61
      Align = alTop
      Caption = ' Dados do Bloco: '
      Enabled = False
      TabOrder = 1
      object Label2: TLabel
        Left = 68
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ImporExpor:'
      end
      object Label4: TLabel
        Left = 132
        Top = 16
        Width = 42
        Height = 13
        Caption = 'AnoMes:'
      end
      object Label8: TLabel
        Left = 188
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label9: TLabel
        Left = 648
        Top = 16
        Width = 33
        Height = 13
        Caption = 'LinArq:'
      end
      object EdImporExpor: TdmkEdit
        Left = 68
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdAnoMes: TdmkEdit
        Left = 132
        Top = 32
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEdit
        Left = 188
        Top = 32
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdLinArq: TdmkEdit
        Left = 648
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 61
      Width = 763
      Height = 207
      Align = alClient
      Caption = ' Registro: '
      TabOrder = 2
      object CkIND_EXP: TCheckBox
        Left = 16
        Top = 16
        Width = 732
        Height = 17
        Caption = '1100 - Ocorreu averba'#231#227'o (conclus'#227'o) de exporta'#231#227'o no per'#237'odo.'
        TabOrder = 0
      end
      object CkIND_CCRF: TCheckBox
        Left = 16
        Top = 36
        Width = 732
        Height = 17
        Caption = 
          '1200 - Existem informa'#231#245'es acerca de cr'#233'ditos de ICMS a serem co' +
          'ntrolados, definidos pela Sefaz.'
        Enabled = False
        TabOrder = 1
      end
      object CkIND_USINA: TCheckBox
        Left = 16
        Top = 76
        Width = 732
        Height = 17
        Caption = 
          '1390 - Usinas de a'#231#250'car e/'#225'lcool - O estabelecimento '#233' produtor ' +
          'de a'#231#250'car e/ou '#225'lcool carburante com movimenta'#231#227'o e/ou estoque n' +
          'o per'#237'odo.'
        Enabled = False
        TabOrder = 2
      end
      object CkIND_COMB: TCheckBox
        Left = 16
        Top = 56
        Width = 732
        Height = 17
        Caption = 
          '1300 - '#201' comercio varejista de combust'#237'veis com movimenta'#231#227'o e/o' +
          'u estoque no per'#237'odo.'
        Enabled = False
        TabOrder = 3
      end
      object CkIND_EE: TCheckBox
        Left = 16
        Top = 116
        Width = 732
        Height = 17
        Caption = 
          '1500 - A empresa '#233' distribuidora de energia e ocorreu fornecimen' +
          'to de energia el'#233'trica para consumidores de outra UF.'
        Enabled = False
        TabOrder = 4
      end
      object CkIND_VA: TCheckBox
        Left = 16
        Top = 96
        Width = 732
        Height = 17
        Caption = 
          '1400 - Sendo o registro obrigat'#243'rio em sua Unidade de Federa'#231#227'o,' +
          ' existem informa'#231#245'es a serem prestadas neste registro.'
        Enabled = False
        TabOrder = 5
      end
      object CkIND_FORM: TCheckBox
        Left = 16
        Top = 156
        Width = 732
        Height = 17
        Caption = 
          '1700 - Foram emitidos documentos fiscais em papel no per'#237'odo em ' +
          'unidade da federa'#231#227'o que exija o controle de utiliza'#231#227'o de docum' +
          'entos fiscais.'
        Enabled = False
        TabOrder = 6
      end
      object CkIND_CART: TCheckBox
        Left = 16
        Top = 136
        Width = 732
        Height = 17
        Caption = '1600 - Realizou vendas com Cart'#227'o de Cr'#233'dito ou de d'#233'bito.'
        Enabled = False
        TabOrder = 7
      end
      object CkIND_AER: TCheckBox
        Left = 16
        Top = 176
        Width = 732
        Height = 17
        Caption = 
          '1800 - A empresa prestou servi'#231'os de transporte a'#233'reo de cargas ' +
          'e de passageiros.'
        Enabled = False
        TabOrder = 8
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 763
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 715
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 667
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 493
        Height = 32
        Caption = 'Obrigatoriedade de Registros do Bloco 1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 493
        Height = 32
        Caption = 'Obrigatoriedade de Registros do Bloco 1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 493
        Height = 32
        Caption = 'Obrigatoriedade de Registros do Bloco 1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 316
    Width = 763
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 759
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 360
    Width = 763
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 617
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 615
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
end
