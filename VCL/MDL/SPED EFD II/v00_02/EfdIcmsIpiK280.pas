unit EfdIcmsIpiK280;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup, dmkEditCalc, UnSPED_EFD_PF,
  UnDmkProcFunc;

type
  TFmEfdIcmsIpiK280 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdPeriApu: TdmkEdit;
    Label6: TLabel;
    EdKndTab: TdmkEdit;
    Label9: TLabel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXEx_TIPI: TWideStringField;
    DsGraGruX: TDataSource;
    Panel5: TPanel;
    EdGraGruX: TdmkEditCB;
    Label232: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    StaticText2: TStaticText;
    EdQTD_COR_POS: TdmkEdit;
    RGIND_EST: TdmkRadioGroup;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    DsEntidades: TDataSource;
    LaCOD_PART: TLabel;
    EdCOD_PART: TdmkEditCB;
    CBCOD_PART: TdmkDBLookupComboBox;
    EdKndCod: TdmkEdit;
    Label7: TLabel;
    EdKndNSU: TdmkEdit;
    Label8: TLabel;
    EdKndItm: TdmkEdit;
    Label10: TLabel;
    EdKndAID: TdmkEdit;
    Label1: TLabel;
    EdKndNiv: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    EdIDSeq1: TdmkEdit;
    QrGraGruXGrandeza: TIntegerField;
    QrGraGruXTipo_Item: TIntegerField;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    EdPecas: TdmkEdit;
    LaPecas: TLabel;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    TPDT_EST: TdmkEditDateTimePicker;
    Label2: TLabel;
    StaticText1: TStaticText;
    EdQTD_COR_NEG: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGIND_ESTClick(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdQTD_COR_POSRedefinido(Sender: TObject);
    procedure EdQTD_COR_NEGRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGruX();
    procedure InfoExtraQTD();
  public
    { Public declarations }
    FRegisPai, FRegisAvo: String;
    FID_SEK, FESTSTabSorc, FOriOpeProc, FOrigemIDKnd, FOriSPEDEFDKnd, FOriBalID,
    FOriKndReg: Integer;
  end;

  var
  FmEfdIcmsIpiK280: TFmEfdIcmsIpiK280;

implementation

uses UnMyObjects, Module, (*EfdIcmsIpiE001,*) UMySQLModule, DmkDAC_PF, MyListas,
  ModuleFin, UnFinanceiro, SpedEfdIcmsIpi;

{$R *.DFM}

procedure TFmEfdIcmsIpiK280.BtOKClick(Sender: TObject);
var
  DT_EST, COD_ITEM, IND_EST, COD_PART, RegisPai, RegisAvo: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq1, ID_SEK, DebCred, GraGruX, Grandeza, Entidade, Tipo_Item,
  ESTSTabSorc, OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd, OriBalID, OriKndReg,
  AM_CA: Integer;
  QTD_COR_POS, QTD_COR_NEG, Pecas, AreaM2, PesoKg: Double;
  SQLType: TSQLType;
  Data: TDateTime;
begin
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  PeriApu        := EdPeriApu.ValueVariant;
  if SQLType = stIns then
  begin
    KndTab         := Integer(TEstqSPEDTabSorc.estsNoSrc);
    KndCod         := PeriApu;
    KndNSU         := PeriApu;
    KndItm         := 0; // Abaixo
    KndAID         := Integer(TEstqMovimID.emidAjuste);
    KndNiv         := Integer(TEstqMovimNiv.eminSemNiv);
    IDSeq1         := 0; // Abaixo
    RegisPai       := 'NONE';
    RegisAvo       := 'NONE';
    ID_SEK         := Integer(TSPED_EFD_ComoLancou.seclLancadoManual);
    ESTSTabSorc    := Integer(TEstqSPEDTabSorc.estsNoSrc);
    OriOpeProc     := Integer(TOrigemOpeProc.oopND);
    OrigemIDKnd    := Integer(TOrigemIDKnd.oidk000ND);
    OriSPEDEFDKnd  := Integer(TOrigemSPEDEFDKnd.osekND);
    OriBalID       := Integer(TSPED_EFD_Bal.sebalIndef);
    OriKndReg      := Integer(TOrigemSPEDEFDKnd.osekND);
  end else
  begin
    KndTab         := EdKndTab.ValueVariant;
    KndCod         := EdKndCod.ValueVariant;
    KndNSU         := EdKndNSU.ValueVariant;
    KndItm         := EdKndItm.ValueVariant;
    KndAID         := EdKndAID.ValueVariant;
    KndNiv         := EdKndNiv.ValueVariant;
    IDSeq1         := EdIDSeq1.ValueVariant;
    RegisPai       := FRegisPai;
    RegisAvo       := FRegisAvo;
    ID_SEK         := FID_SEK;
    ESTSTabSorc    := FESTSTabSorc;
    OriOpeProc     := FOriOpeProc;
    OrigemIDKnd    := FOrigemIDKnd;
    OriSPEDEFDKnd  := FOriSPEDEFDKnd;
    OriBalID       := FOriBalID;
    OriKndReg      := FOriKndReg;
  end;
  Data           := TPDT_EST.Date;
  DT_EST         := Geral.FDT(Data, 1);
  GraGruX        := EdGraGruX.ValueVariant;
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD_COR_POS    := EdQTD_COR_POS.ValueVariant;
  QTD_COR_NEG    := EdQTD_COR_NEG.ValueVariant;
  IND_EST        := Geral.FF0(RGIND_EST.ItemIndex);
  COD_PART       := EdCOD_PART.ValueVariant;
  DebCred        := Integer(SPED_EFD_PF.DefineDebCred(QTD_COR_POS, QTD_COR_NEG));
  Grandeza       := QrGraGruXGrandeza.Value;
  Pecas          := EdPecas.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  Entidade       := EdCOD_PART.ValueVariant;
  Tipo_Item      := QrGraGruXTipo_Item.Value;
  //
  if COD_PART = '0' then
    COD_PART := '';
  //
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o c�digo do produto!') then
    Exit;
  if MyObjects.FIC(Trim(QrGraGruXSIGLAUNIDMED.Value) = '', EdGraGruX,
    'C�digo do produto sem sigla de unidade definida!') then Exit;
  if MyObjects.FIC((RGIND_EST.ItemIndex < 0) or (RGIND_EST.ItemIndex > 2), RGIND_EST,
    'Informe o indicador de propriedade/posse!') then Exit;
  if MyObjects.FIC((EdCOD_PART.ValueVariant <> 0) and (RGIND_EST.ItemIndex = 0), EdCOD_PART,
    'N�o informe o propriet�rio/possuidor quando for o informante!') then Exit;
  if MyObjects.FIC((EdCOD_PART.ValueVariant = 0) and (RGIND_EST.ItemIndex <> 0), EdCOD_PART,
    'Informe o propriet�rio/possuidor quando n�o for o informante!') then Exit;
  if MyObjects.FIC((QTD_COR_POS <> 0) and (QTD_COR_NEG <> 0), EdQTD_COR_POS,
  'Informe apenas a quantidade positiva ou a quantidade negativa!' + sLineBreak +
  'N�o � permitido informar ambas!') then Exit;
  if MyObjects.FIC((QTD_COR_POS = 0) and (QTD_COR_NEG = 0), EdQTD_COR_POS,
  'Informe a quantidade positiva ou a quantidade negativa!') then Exit;
  if MyObjects.FIC(QTD_COR_POS < 0, EdQTD_COR_POS,
  'N�o � permitido n�mero negativo!') then Exit;
  if MyObjects.FIC(QTD_COR_NEG < 0, EdQTD_COR_NEG,
  'N�o � permitido n�mero negativo!') then Exit;
  AM_CA := dmkPF.DataToAnoMes(Data);
  if MyObjects.FIC(AM_CA >= AnoMes, TPDT_EST,
  'Data da corre��o de apontamento deve ser inferior ao per�odo da escritura��o!'
  ) then Exit;
  //
  KndItm := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik280', 'KndItm', [
  'ImporExpor', 'AnoMes', 'KndTab'], [ImporExpor, AnoMes, KndTab],
  SQLType, KndItm, siPositivo, EdKndItm);
  //
  IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik280', 'IDSeq1', [
  'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
  ImporExpor, AnoMes, Empresa, PeriApu],
  SQLType, IDSeq1, siPositivo, EdIDSeq1);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdicmsipik280', False, [
  'KndAID', 'KndNiv', 'IDSeq1',
  'COD_ITEM', 'QTD_COR_POS', 'QTD_COR_NEG',
  'IND_EST', 'COD_PART', 'Grandeza',
  'Pecas', 'AreaM2', 'PesoKg',
  'Entidade', 'Tipo_Item', 'RegisPai',
  'RegisAvo', 'ESTSTabSorc', 'OriOpeProc',
  'OrigemIDKnd', 'OriSPEDEFDKnd', 'OriBalID',
  'OriKndReg', 'KndItm', 'DT_EST',
  'DebCred', 'GraGruX'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod', 'KndNSU'], [
  KndAID, KndNiv, IDSeq1,
  COD_ITEM, QTD_COR_POS, QTD_COR_NEG,
  IND_EST, COD_PART, Grandeza,
  Pecas, AreaM2, PesoKg,
  Entidade, Tipo_Item, RegisPai,
  RegisAvo, ESTSTabSorc, OriOpeProc,
  OrigemIDKnd, OriSPEDEFDKnd, OriBalID,
  OriKndReg, KndItm, DT_EST, DebCred, GraGruX], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod, KndNSU], True) then
  begin
    DfSEII.ReopenK280(KndTab, KndItm, 0, rkGrade);
    Close;
  end;
end;

procedure TFmEfdIcmsIpiK280.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpiK280.EdGraGruXRedefinido(Sender: TObject);
begin
  InfoExtraQTD();
end;

procedure TFmEfdIcmsIpiK280.EdQTD_COR_NEGRedefinido(Sender: TObject);
begin
  InfoExtraQTD();
end;

procedure TFmEfdIcmsIpiK280.EdQTD_COR_POSRedefinido(Sender: TObject);
begin
  InfoExtraQTD();
end;

procedure TFmEfdIcmsIpiK280.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiK280.FormCreate(Sender: TObject);
begin
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  ReopenGraGruX();
end;

procedure TFmEfdIcmsIpiK280.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdIcmsIpiK280.InfoExtraQTD();
var
  QTD: Double;
begin
  if (EdGraGruX.ValueVariant <> 0) and
  (
    (EdQTD_COR_POS.ValueVariant <> 0)
    OR
    (EdQTD_COR_NEG.ValueVariant <> 0)
  ) then
  begin
    EdPecas.ValueVariant  := 0;
    EdAreaM2.ValueVariant := 0;
    EdPesoKg.ValueVariant := 0;
    //
    QTD := EdQTD_COR_POS.ValueVariant - EdQTD_COR_NEG.ValueVariant;
    case TGrandezaUnidMed(QrGraGruXGrandeza.Value) of
      gumPeca:   EdPecas.ValueVariant  := QTD;
      gumAreaM2: EdAreaM2.ValueVariant := QTD;
      gumPesoKg: EdPesoKg.ValueVariant := QTD;
    end;
  end;
end;

procedure TFmEfdIcmsIpiK280.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
begin
  SQL_AND := '';
  SQL_LFT := '';
(*
  if TdmkAppID(CO_DMKID_APP) = dmkappB_L_U_E_D_E_R_M then
  begin
    SQL_AND := 'AND NOT (pqc.PQ IS NULL)';
    SQL_LFT := 'LEFT JOIN pqcli pqc ON pqc.PQ=gg1.Nivel1';
  end;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  SQL_LFT,
  'WHERE ggx.Controle > 0 ', //-900000 ',
  SQL_AND,
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

procedure TFmEfdIcmsIpiK280.RGIND_ESTClick(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (RGIND_EST.ItemIndex > 0) and (RGIND_EST.ItemIndex < 3);
  LaCOD_PART.Enabled := Habilita;
  EdCOD_PART.Enabled := Habilita;
  CBCOD_PART.Enabled := Habilita;
  if not Habilita then
  begin
    EdCOD_PART.ValueVariant := 0;
    CBCOD_PART.KeyValue     := 0;
  end;
end;

end.
