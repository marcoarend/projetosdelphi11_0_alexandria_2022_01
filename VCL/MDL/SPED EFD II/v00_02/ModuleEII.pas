unit ModuleEII;

interface

uses
  System.SysUtils, System.Classes;

type
  TDmEII = class(TDataModule)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DmEII: TDmEII;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

end.
