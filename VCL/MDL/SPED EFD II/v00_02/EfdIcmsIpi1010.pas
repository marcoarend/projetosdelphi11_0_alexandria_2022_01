unit EfdIcmsIpi1010;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  TFmEfdIcmsIpi1010 = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    GroupBox1: TGroupBox;
    CkIND_EXP: TCheckBox;
    CkIND_CCRF: TCheckBox;
    CkIND_USINA: TCheckBox;
    CkIND_COMB: TCheckBox;
    CkIND_EE: TCheckBox;
    CkIND_VA: TCheckBox;
    CkIND_FORM: TCheckBox;
    CkIND_CART: TCheckBox;
    CkIND_AER: TCheckBox;
    EdLinArq: TdmkEdit;
    Label9: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEfdIcmsIpi1010: TFmEfdIcmsIpi1010;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  EfdIcmsIpiE001, SpedEfdIcmsIpi;

{$R *.DFM}

procedure TFmEfdIcmsIpi1010.BtOKClick(Sender: TObject);
var
  REG, IND_EXP, IND_CCRF, IND_COMB, IND_USINA, IND_VA, IND_EE, IND_CART, IND_FORM, IND_AER: String;
  ImporExpor, AnoMes, Empresa, LinArq: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  LinArq         := EdLinArq.ValueVariant;
  //REG            := ;
  IND_EXP        := DmkPF.EscolhaDe2Str(CkIND_EXP.Checked, 'S', 'N');
  IND_CCRF       := DmkPF.EscolhaDe2Str(CkIND_CCRF.Checked, 'S', 'N');
  IND_COMB       := DmkPF.EscolhaDe2Str(CkIND_COMB.Checked, 'S', 'N');
  IND_USINA      := DmkPF.EscolhaDe2Str(CkIND_USINA.Checked, 'S', 'N');
  IND_VA         := DmkPF.EscolhaDe2Str(CkIND_VA.Checked, 'S', 'N');
  IND_EE         := DmkPF.EscolhaDe2Str(CkIND_EE.Checked, 'S', 'N');
  IND_CART       := DmkPF.EscolhaDe2Str(CkIND_CART.Checked, 'S', 'N');
  IND_FORM       := DmkPF.EscolhaDe2Str(CkIND_FORM.Checked, 'S', 'N');
  IND_AER        := DmkPF.EscolhaDe2Str(CkIND_AER.Checked, 'S', 'N');
  //
  //
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipi1010', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  ImgTipo.SQLType, LinArq, siPositivo, EdLinArq);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdicmsipi1010', False, [
  'LinArq', 'REG', 'IND_EXP',
  'IND_CCRF', 'IND_COMB', 'IND_USINA',
  'IND_VA', 'IND_EE', 'IND_CART',
  'IND_FORM', 'IND_AER'], [
  'ImporExpor', 'AnoMes', 'Empresa'], [
  LinArq, REG, IND_EXP,
  IND_CCRF, IND_COMB, IND_USINA,
  IND_VA, IND_EE, IND_CART,
  IND_FORM, IND_AER], [
  ImporExpor, AnoMes, Empresa], True) then
  begin
    DfSEII.Reopen1010();
    Close;
  end;
end;

procedure TFmEfdIcmsIpi1010.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpi1010.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpi1010.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmEfdIcmsIpi1010.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
