unit EfdIcmsIpiK210;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup, dmkEditCalc, UnDmkProcFunc,
  UnSPED_EFD_PF;

type
  TFmEfdIcmsIpiK210 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdPeriApu: TdmkEdit;
    Label6: TLabel;
    EdKndTab: TdmkEdit;
    Label9: TLabel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXEx_TIPI: TWideStringField;
    DsGraGruX: TDataSource;
    Panel5: TPanel;
    EdGraGruX: TdmkEditCB;
    Label232: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    StaticText2: TStaticText;
    EdQTD_ORI: TdmkEdit;
    EdKndCod: TdmkEdit;
    Label7: TLabel;
    EdKndNSU: TdmkEdit;
    Label8: TLabel;
    EdKndItm: TdmkEdit;
    Label10: TLabel;
    EdKndAID: TdmkEdit;
    Label1: TLabel;
    EdKndNiv: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    EdIDSeq1: TdmkEdit;
    QrGraGruXGrandeza: TIntegerField;
    QrGraGruXTipo_Item: TIntegerField;
    TPDT_INI_OS: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label13: TLabel;
    TPDT_FIN_OS: TdmkEditDateTimePicker;
    Label14: TLabel;
    EdMovimCod: TdmkEdit;
    EdTxt: TdmkEdit;
    DBText1: TDBText;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdMovimCodRedefinido(Sender: TObject);
    procedure EdMovimCodChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGruX();
  public
    { Public declarations }
    FID_SEK, FOriOpeProc, FOrigemIDKnd: Integer;
  end;

  var
  FmEfdIcmsIpiK210: TFmEfdIcmsIpiK210;

implementation

uses UnMyObjects, Module, (*EfdIcmsIpiE001,*) UMySQLModule, DmkDAC_PF, MyListas,
  ModuleFin, UnFinanceiro, SpedEfdIcmsIpi, UnVS_PF;

{$R *.DFM}

procedure TFmEfdIcmsIpiK210.BtOKClick(Sender: TObject);
var
  DT_INI_OS, DT_FIN_OS, COD_DOC_OS, COD_ITEM_ORI: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq1, ID_SEK, GraGruX, OriOpeProc, OrigemIDKnd, MovimCod: Integer;
  QTD_ORI: Double;
  SQLType: TSQLType;
  DataIni, DataFim: TDateTime;
begin
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  PeriApu        := EdPeriApu.ValueVariant;
  if SQLType = stIns then
  begin
    KndTab         := Integer(TEstqSPEDTabSorc.estsNoSrc);
    KndCod         := PeriApu;
    KndNSU         := PeriApu;
    KndItm         := 0; // Abaixo
    KndAID         := Integer(TEstqMovimID.emidAjuste);
    KndNiv         := Integer(TEstqMovimNiv.eminSemNiv);
    IDSeq1         := 0; // Abaixo
    ID_SEK         := Integer(TSPED_EFD_ComoLancou.seclLancadoManual);
    OriOpeProc     := Integer(TOrigemOpeProc.oopND);
    OrigemIDKnd    := Integer(TOrigemIDKnd.oidk000ND);
  end else
  begin
    KndTab         := EdKndTab.ValueVariant;
    KndCod         := EdKndCod.ValueVariant;
    KndNSU         := EdKndNSU.ValueVariant;
    KndItm         := EdKndItm.ValueVariant;
    KndAID         := EdKndAID.ValueVariant;
    KndNiv         := EdKndNiv.ValueVariant;
    IDSeq1         := EdIDSeq1.ValueVariant;
    ID_SEK         := FID_SEK;
    OriOpeProc     := FOriOpeProc;
    OrigemIDKnd    := FOrigemIDKnd;
  end;
  GraGruX        := EdGraGruX.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  DataIni        := Trunc(TPDT_INI_OS.Date);
  DataFim        := Trunc(TPDT_FIN_OS.Date);
  //
  if MyObjects.FIC(DataIni < 2, TPDT_INI_OS,
    'Informe a data inicial da Ordem de Servi�o! (IME-C)') then Exit;
  if MyObjects.FIC(MovimCod = 0, EdMovimCod,
    'Informe o c�digo da Ordem de Servi�o! (IME-C)') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o c�digo do produto!') then
    Exit;
  //ID_SEK         := ;
  DT_INI_OS      := Geral.FDT(TPDT_INI_OS.Date, 1);
  DT_FIN_OS      := Geral.FDT(TPDT_FIN_OS.Date, 1);
  COD_DOC_OS     := dmkPF.ITS_Null(MovimCod);
  COD_ITEM_ORI   := dmkPF.ITS_Null(GraGruX);
  QTD_ORI        := EdQTD_ORI.ValueVariant;
  //

  (*Campo 01 (REG) - Valor V�lido: [K210]*)
  //
  (*Campo 02 (DT_INI_OS) - Preenchimento: a data de in�cio dever� ser informada
  se existir ordem de servi�o, ainda que iniciada em per�odo de apura��o (K100)
  anterior.
  Valida��o: obrigat�rio se informado o campo COD_DOC_OS ou o campo DT_FIN_OS.
  A data informada deve ser menor ou igual a DT_FIN do registro K100.*)

  if not SPED_EFD_PF.ValidaDataInicialDentroDoAnoMesSPED(AnoMes, DataIni,
  DataFim, COD_DOC_OS) then Exit;

  (*Campo 03 (DT_FIN_OS) - Preenchimento: informar a data de conclus�o da ordem
  de servi�o. Ficar� em branco, caso a ordem de servi�o n�o seja conclu�da at� a
  data de encerramento do per�odo de apura��o.
  Valida��o: se preenchido, DT_FIN_OS deve estar compreendida no per�odo de
  apura��o do K100 e ser maior ou igual a DT_INI_OS.*)

  if not SPED_EFD_PF.ValidaDataFinalDentroDoAnoMesSPED(AnoMes, DataIni,
  DataFim, COD_DOC_OS) then Exit;

  (*Campo 04 (COD_DOC_OS) � Preenchimento: informar o c�digo da ordem de
  servi�o, caso exista.
  Valida��o: obrigat�rio se informado o campo DT_INI_OS.*)

  if not SPED_EFD_PF.ValidaOSDentroDoAnoMesSPED(AnoMes, DataIni,
  DataFim, COD_DOC_OS) then Exit;


  (*Campo 05 (COD_ITEM_ORI) - Valida��o: o c�digo do item de origem dever�
  existir no campo COD_ITEM do Registro 0200.*)

  if not SPED_EFD_PF.ValidaGGXEstahDentroDoAnoMesSPED(AnoMes, GraGruX,
  COD_DOC_OS) then Exit;

  (*Campo 06 (QTD_ORI) � Preenchimento: n�o � admitida quantidade negativa.*)

  if not SPED_EFD_PF.ValidaQtdeDentroDoAnoMesSPED(AnoMes, QTD_ORI, COD_DOC_OS,
  (*MaiorQueZero*)False) then Exit;

  //

  KndItm := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik210', 'KndItm', [
  'ImporExpor', 'AnoMes', 'KndTab'], [ImporExpor, AnoMes, KndTab],
  SQLType, KndItm, siPositivo, EdKndItm);
  //
  IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik210', 'IDSeq1', [
  'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
  ImporExpor, AnoMes, Empresa, PeriApu],
  SQLType, IDSeq1, siPositivo, EdIDSeq1);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdicmsipik210', False, [
  'KndAID', 'KndNiv', 'IDSeq1',
  'ID_SEK', 'DT_INI_OS', 'DT_FIN_OS',
  'COD_DOC_OS', 'COD_ITEM_ORI', 'QTD_ORI',
  'OriOpeProc', 'OrigemIDKnd', 'GraGruX'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'], [
  KndAID, KndNiv, IDSeq1,
  ID_SEK, DT_INI_OS, DT_FIN_OS,
  COD_DOC_OS, COD_ITEM_ORI, QTD_ORI,
  OriOpeProc, OrigemIDKnd, GraGruX], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True) then
  begin
    DfSEII.ReopenK210(IDSeq1);
    Close;
  end;
end;

procedure TFmEfdIcmsIpiK210.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpiK210.EdMovimCodChange(Sender: TObject);
begin
  EdTxt.Text := '';
end;

procedure TFmEfdIcmsIpiK210.EdMovimCodRedefinido(Sender: TObject);
begin
  VS_PF.VerificaSeExisteIMEC(EdTxt, EdMovimCod.ValueVariant);
end;

procedure TFmEfdIcmsIpiK210.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiK210.FormCreate(Sender: TObject);
begin
  TPDT_INI_OS.Date := 0;
  TPDT_FIN_OS.Date := 0;
  ReopenGraGruX();
end;

procedure TFmEfdIcmsIpiK210.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdIcmsIpiK210.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
begin
  SQL_AND := '';
  SQL_LFT := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  SQL_LFT,
  'WHERE ggx.Controle > 0 ',
  SQL_AND,
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

end.
