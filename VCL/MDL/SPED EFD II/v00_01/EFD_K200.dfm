object FmEFD_K200: TFmEFD_K200
  Left = 339
  Top = 185
  Caption = 'EFD-SPEDK-200 :: Estoque Escriturado'
  ClientHeight = 461
  ClientWidth = 701
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 701
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 653
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 605
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 345
        Height = 32
        Caption = 'Item de Estoque Escriturado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 345
        Height = 32
        Caption = 'Item de Estoque Escriturado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 345
        Height = 32
        Caption = 'Item de Estoque Escriturado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 391
    Width = 701
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 555
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 553
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 701
    Height = 299
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 701
      Height = 299
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 61
        Width = 701
        Height = 238
        Align = alClient
        Caption = ' Dados do item: '
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 697
          Height = 221
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label232: TLabel
            Left = 52
            Top = 4
            Width = 180
            Height = 13
            Caption = '03. Meu c'#243'digo (reduzido do produto):'
          end
          object LaCOD_PART: TLabel
            Left = 52
            Top = 175
            Width = 302
            Height = 13
            Caption = '06. Propriet'#225'rio/possuidor que n'#227'o seja o informante do arquivo:'
          end
          object EdGraGruX: TdmkEditCB
            Left = 52
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'COD_ITEM'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGraGruX
            IgnoraDBLookupComboBox = False
          end
          object CBGraGruX: TdmkDBLookupComboBox
            Left = 110
            Top = 20
            Width = 527
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_PRD_TAM_COR'
            ListSource = DsGraGruX
            TabOrder = 1
            dmkEditCB = EdGraGruX
            QryCampo = 'COD_ITEM'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object StaticText2: TStaticText
            Left = 52
            Top = 44
            Width = 469
            Height = 21
            AutoSize = False
            BorderStyle = sbsSunken
            Caption = '04. Quantidade do item'
            FocusControl = EdQTD
            TabOrder = 2
          end
          object EdQTD: TdmkEdit
            Left = 524
            Top = 44
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'QTD'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object RGIND_EST: TdmkRadioGroup
            Left = 52
            Top = 68
            Width = 585
            Height = 105
            Caption = ' 05. Indicador de propriedade/posse do item:'
            ItemIndex = 3
            Items.Strings = (
              '0- Item de propriedade do informante e em seu poder'
              '1- Item de propriedade do informante em posse de terceiros'
              '2- Item de propriedade de terceiros em posse do informante'
              '? - Indefinido')
            TabOrder = 4
            OnClick = RGIND_ESTClick
            QryCampo = 'IND_EST'
            UpdType = utYes
            OldValor = 0
          end
          object EdCOD_PART: TdmkEditCB
            Left = 52
            Top = 191
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'COD_PART'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCOD_PART
            IgnoraDBLookupComboBox = False
          end
          object CBCOD_PART: TdmkDBLookupComboBox
            Left = 109
            Top = 191
            Width = 528
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENT'
            ListSource = DsEntidades
            TabOrder = 6
            dmkEditCB = EdCOD_PART
            QryCampo = 'COD_PART'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 701
        Height = 61
        Align = alTop
        Caption = ' Dados do invent'#225'rio: '
        Enabled = False
        TabOrder = 1
        object Label3: TLabel
          Left = 68
          Top = 16
          Width = 56
          Height = 13
          Caption = 'ImporExpor:'
        end
        object Label4: TLabel
          Left = 132
          Top = 16
          Width = 42
          Height = 13
          Caption = 'AnoMes:'
        end
        object Label5: TLabel
          Left = 188
          Top = 16
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label6: TLabel
          Left = 236
          Top = 16
          Width = 28
          Height = 13
          Caption = 'K100:'
        end
        object Label2: TLabel
          Left = 528
          Top = 16
          Width = 100
          Height = 13
          Caption = '02. Data do estoque:'
        end
        object Label9: TLabel
          Left = 300
          Top = 16
          Width = 33
          Height = 13
          Caption = 'LinArq:'
        end
        object EdImporExpor: TdmkEdit
          Left = 68
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAnoMes: TdmkEdit
          Left = 132
          Top = 32
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdEmpresa: TdmkEdit
          Left = 188
          Top = 32
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdK100: TdmkEdit
          Left = 236
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object TPDT_EST: TdmkEditDateTimePicker
          Left = 528
          Top = 32
          Width = 112
          Height = 21
          Date = 40761.337560104170000000
          Time = 40761.337560104170000000
          TabOrder = 4
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdLinArq: TdmkEdit
          Left = 300
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 347
    Width = 701
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 697
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq,'
      'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle > -900000'
      'ORDER BY NO_PRD_TAM_COR')
    Left = 392
    Top = 184
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 166
    end
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGruXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGruXEx_TIPI: TWideStringField
      FieldName = 'Ex_TIPI'
      Size = 3
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 392
    Top = 232
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 456
    Top = 184
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 456
    Top = 232
  end
end
