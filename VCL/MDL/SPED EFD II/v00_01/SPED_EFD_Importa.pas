unit SPED_EFD_Importa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, ComCtrls, dmkEditDateTimePicker, DB, mySQLDbTables,
  dmkGeral, Grids, Math, DBGrids, dmkCheckGroup, Menus, dmkDBGridDAC,
  dmkDBGrid, frxClass, frxDBSet, UnDmkEnums, dmkImage, UnProjGroup_Consts;

type
  TFmSPED_EFD_Importa = class(TForm)
    PageControl0: TPageControl;
    TabSheet10: TTabSheet;
    Panel13: TPanel;
    Label83: TLabel;
    EdSPED_EFD_Path: TdmkEdit;
    SpeedButton7: TSpeedButton;
    Panel14: TPanel;
    SpeedButton1: TSpeedButton;
    QrLocNF: TmySQLQuery;
    PageControl1: TPageControl;
    TabSheet12: TTabSheet;
    MeImportado: TMemo;
    TabSheet13: TTabSheet;
    MeIgnor: TMemo;
    TabSheet14: TTabSheet;
    QrLocGGX: TmySQLQuery;
    QrLocGG1: TmySQLQuery;
    QrLocGG1GraGru1: TIntegerField;
    QrLocGG1Controle: TIntegerField;
    QrLocGG1NO_PRD_TAM_COR: TWideStringField;
    QrLocGG1PrdGrupTip: TIntegerField;
    QrLocPrdNom: TmySQLQuery;
    QrLocPrdNomGraGruX: TIntegerField;
    QrLocPrdTmp: TmySQLQuery;
    QrLocPrdTmpCU_CodUsu: TIntegerField;
    QrLocPrdTmpGraGru1: TIntegerField;
    QrLocPrdTmpGraGruX: TIntegerField;
    QrLocPrdTmpNO_PRD_TAM_COR: TWideStringField;
    QrLocPrdTmpPrdGrupTip: TIntegerField;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrLocPrdNomGraGru1: TIntegerField;
    QrLocNFFatID: TIntegerField;
    QrLocNFFatNum: TIntegerField;
    QrLocNFEmpresa: TIntegerField;
    QrLocNFIDCtrl: TIntegerField;
    Qr0000: TmySQLQuery;
    Qr0000Empresa: TIntegerField;
    Qr0000LinArq: TIntegerField;
    Qr0000REG: TWideStringField;
    Qr0000COD_VER: TIntegerField;
    Qr0000COD_FIN: TSmallintField;
    Qr0000DT_INI: TDateField;
    Qr0000DT_FIN: TDateField;
    Qr0000NOME: TWideStringField;
    Qr0000CNPJ: TWideStringField;
    Qr0000CPF: TWideStringField;
    Qr0000UF: TWideStringField;
    Qr0000IE: TWideStringField;
    Qr0000COD_MUN: TIntegerField;
    Qr0000IM: TWideStringField;
    Qr0000SUFRAMA: TWideStringField;
    Qr0000IND_PERFIL: TWideStringField;
    Qr0000IND_ATIV: TSmallintField;
    Qr0000AnoMes: TIntegerField;
    Ds0000: TDataSource;
    Ds0001: TDataSource;
    Qr0001: TmySQLQuery;
    Qr0005: TmySQLQuery;
    Ds0005: TDataSource;
    Qr0100: TmySQLQuery;
    Ds0100: TDataSource;
    Ds0150: TDataSource;
    Qr0150: TmySQLQuery;
    Qr0200: TmySQLQuery;
    Ds0200: TDataSource;
    Ds0400: TDataSource;
    Qr0400: TmySQLQuery;
    Qr0990: TmySQLQuery;
    Ds0990: TDataSource;
    DsC001: TDataSource;
    QrC001: TmySQLQuery;
    QrC100: TmySQLQuery;
    DsC100: TDataSource;
    DsC170: TDataSource;
    QrC170: TmySQLQuery;
    Qr0001AnoMes: TIntegerField;
    Qr0001Empresa: TIntegerField;
    Qr0001LinArq: TIntegerField;
    Qr0001REG: TWideStringField;
    Qr0001IND_MOV: TSmallintField;
    Qr0005AnoMes: TIntegerField;
    Qr0005Empresa: TIntegerField;
    Qr0005LinArq: TIntegerField;
    Qr0005REG: TWideStringField;
    Qr0005FANTASIA: TWideStringField;
    Qr0005CEP: TIntegerField;
    Qr0005END: TWideStringField;
    Qr0005NUM: TWideStringField;
    Qr0005COMPL: TWideStringField;
    Qr0005BAIRRO: TWideStringField;
    Qr0005FONE: TWideStringField;
    Qr0005FAX: TWideStringField;
    Qr0005EMAIL: TWideStringField;
    Qr0100AnoMes: TIntegerField;
    Qr0100Empresa: TIntegerField;
    Qr0100LinArq: TIntegerField;
    Qr0100NOME: TWideStringField;
    Qr0100CPF: TWideStringField;
    Qr0100CRC: TWideStringField;
    Qr0100CNPJ: TWideStringField;
    Qr0100CEP: TIntegerField;
    Qr0100END: TWideStringField;
    Qr0100NUM: TWideStringField;
    Qr0100COMPL: TWideStringField;
    Qr0100BAIRRO: TWideStringField;
    Qr0100FONE: TWideStringField;
    Qr0100FAX: TWideStringField;
    Qr0100EMAIL: TWideStringField;
    Qr0100COD_MUN: TIntegerField;
    Qr0100REG: TWideStringField;
    Qr0150AnoMes: TIntegerField;
    Qr0150Empresa: TIntegerField;
    Qr0150LinArq: TIntegerField;
    Qr0150REG: TWideStringField;
    Qr0150COD_PART: TWideStringField;
    Qr0150NOME: TWideStringField;
    Qr0150COD_PAIS: TIntegerField;
    Qr0150CNPJ: TWideStringField;
    Qr0150CPF: TWideStringField;
    Qr0150IE: TWideStringField;
    Qr0150COD_MUN: TIntegerField;
    Qr0150SUFRAMA: TWideStringField;
    Qr0150END: TWideStringField;
    Qr0150NUM: TWideStringField;
    Qr0150COMPL: TWideStringField;
    Qr0150BAIRRO: TWideStringField;
    Qr0400AnoMes: TIntegerField;
    Qr0400Empresa: TIntegerField;
    Qr0400LinArq: TIntegerField;
    Qr0400REG: TWideStringField;
    Qr0400COD_NAT: TWideStringField;
    Qr0400DESCR_NAT: TWideStringField;
    QrC190: TmySQLQuery;
    DsC190: TDataSource;
    DsC500: TDataSource;
    QrC500: TmySQLQuery;
    QrC590: TmySQLQuery;
    DsC590: TDataSource;
    QrC990: TmySQLQuery;
    DsC990: TDataSource;
    DsD001: TDataSource;
    QrD001: TmySQLQuery;
    QrD100: TmySQLQuery;
    DsD100: TDataSource;
    Qr0990AnoMes: TIntegerField;
    Qr0990Empresa: TIntegerField;
    Qr0990LinArq: TIntegerField;
    Qr0990REG: TWideStringField;
    Qr0990QTD_LIN_0: TIntegerField;
    QrC001AnoMes: TIntegerField;
    QrC001Empresa: TIntegerField;
    QrC001LinArq: TIntegerField;
    QrC001REG: TWideStringField;
    QrC001IND_MOV: TWideStringField;
    QrC170AnoMes: TIntegerField;
    QrC170Empresa: TIntegerField;
    QrC170LinArq: TIntegerField;
    QrC170C100: TIntegerField;
    QrC170REG: TWideStringField;
    QrC170NUM_ITEM: TIntegerField;
    QrC170COD_ITEM: TWideStringField;
    QrC170DESCR_COMPL: TWideStringField;
    QrC170QTD: TFloatField;
    QrC170UNID: TWideStringField;
    QrC170VL_ITEM: TFloatField;
    QrC170VL_DESC: TFloatField;
    QrC170IND_MOV: TWideStringField;
    QrC170CST_ICMS: TIntegerField;
    QrC170CFOP: TIntegerField;
    QrC170COD_NAT: TWideStringField;
    QrC170VL_BC_ICMS: TFloatField;
    QrC170ALIQ_ICMS: TFloatField;
    QrC170VL_ICMS: TFloatField;
    QrC170VL_BC_ICMS_ST: TFloatField;
    QrC170ALIQ_ST: TFloatField;
    QrC170VL_ICMS_ST: TFloatField;
    QrC170IND_APUR: TWideStringField;
    QrC170CST_IPI: TWideStringField;
    QrC170COD_ENQ: TWideStringField;
    QrC170VL_BC_IPI: TFloatField;
    QrC170ALIQ_IPI: TFloatField;
    QrC170VL_IPI: TFloatField;
    QrC170CST_PIS: TSmallintField;
    QrC170VL_BC_PIS: TFloatField;
    QrC170ALIQ_PIS_p: TFloatField;
    QrC170QUANT_BC_PIS: TFloatField;
    QrC170ALIQ_PIS_r: TFloatField;
    QrC170VL_PIS: TFloatField;
    QrC170CST_COFINS: TSmallintField;
    QrC170VL_BC_COFINS: TFloatField;
    QrC170ALIQ_COFINS_p: TFloatField;
    QrC170QUANT_BC_COFINS: TFloatField;
    QrC170ALIQ_COFINS_r: TFloatField;
    QrC170VL_COFINS: TFloatField;
    QrC170COD_CTA: TWideStringField;
    QrC190AnoMes: TIntegerField;
    QrC190Empresa: TIntegerField;
    QrC190LinArq: TIntegerField;
    QrC190C100: TIntegerField;
    QrC190REG: TWideStringField;
    QrC190CST_ICMS: TIntegerField;
    QrC190CFOP: TIntegerField;
    QrC190ALIQ_ICMS: TFloatField;
    QrC190VL_OPR: TFloatField;
    QrC190VL_BC_ICMS: TFloatField;
    QrC190VL_ICMS: TFloatField;
    QrC190VL_BC_ICMS_ST: TFloatField;
    QrC190VL_ICMS_ST: TFloatField;
    QrC190VL_RED_BC: TFloatField;
    QrC190VL_IPI: TFloatField;
    QrC190COD_OBS: TWideStringField;
    QrC590AnoMes: TIntegerField;
    QrC590Empresa: TIntegerField;
    QrC590LinArq: TIntegerField;
    QrC590C500: TIntegerField;
    QrC590REG: TWideStringField;
    QrC590CST_ICMS: TIntegerField;
    QrC590CFOP: TIntegerField;
    QrC590ALIQ_ICMS: TFloatField;
    QrC590VL_OPR: TFloatField;
    QrC590VL_BC_ICMS: TFloatField;
    QrC590VL_ICMS: TFloatField;
    QrC590VL_BC_ICMS_ST: TFloatField;
    QrC590VL_ICMS_ST: TFloatField;
    QrC590VL_RED_BC: TFloatField;
    QrC590COD_OBS: TWideStringField;
    QrC990AnoMes: TIntegerField;
    QrC990Empresa: TIntegerField;
    QrC990LinArq: TIntegerField;
    QrC990REG: TWideStringField;
    QrC990QTD_LIN_C: TIntegerField;
    DsD500: TDataSource;
    QrD500: TmySQLQuery;
    DsD190: TDataSource;
    QrD190: TmySQLQuery;
    DsD990: TDataSource;
    QrD990: TmySQLQuery;
    DsD590: TDataSource;
    QrD590: TmySQLQuery;
    DsE100: TDataSource;
    QrE100: TmySQLQuery;
    DsE001: TDataSource;
    QrE001: TmySQLQuery;
    QrD001AnoMes: TIntegerField;
    QrD001Empresa: TIntegerField;
    QrD001LinArq: TIntegerField;
    QrD001REG: TWideStringField;
    QrD001IND_MOV: TWideStringField;
    QrD190AnoMes: TIntegerField;
    QrD190Empresa: TIntegerField;
    QrD190LinArq: TIntegerField;
    QrD190D100: TIntegerField;
    QrD190REG: TWideStringField;
    QrD190CST_ICMS: TIntegerField;
    QrD190CFOP: TIntegerField;
    QrD190ALIQ_ICMS: TFloatField;
    QrD190VL_OPR: TFloatField;
    QrD190VL_BC_ICMS: TFloatField;
    QrD190VL_ICMS: TFloatField;
    QrD190VL_RED_BC: TFloatField;
    QrD190COD_OBS: TWideStringField;
    QrD590AnoMes: TIntegerField;
    QrD590Empresa: TIntegerField;
    QrD590LinArq: TIntegerField;
    QrD590D500: TIntegerField;
    QrD590REG: TWideStringField;
    QrD590CST_ICMS: TIntegerField;
    QrD590CFOP: TIntegerField;
    QrD590ALIQ_ICMS: TFloatField;
    QrD590VL_OPR: TFloatField;
    QrD590VL_BC_ICMS: TFloatField;
    QrD590VL_ICMS: TFloatField;
    QrD590VL_BC_ICMS_ST: TFloatField;
    QrD590VL_ICMS_ST: TFloatField;
    QrD590VL_RED_BC: TFloatField;
    QrD590COD_OBS: TWideStringField;
    QrD990AnoMes: TIntegerField;
    QrD990Empresa: TIntegerField;
    QrD990LinArq: TIntegerField;
    QrD990REG: TWideStringField;
    QrD990QTD_LIN_D: TIntegerField;
    QrE110: TmySQLQuery;
    DsE110: TDataSource;
    DsE990: TDataSource;
    QrE990: TmySQLQuery;
    QrH001: TmySQLQuery;
    DsH001: TDataSource;
    DsH005: TDataSource;
    QrH005: TmySQLQuery;
    QrH010: TmySQLQuery;
    DsH010: TDataSource;
    DsH990: TDataSource;
    QrH990: TmySQLQuery;
    QrE001AnoMes: TIntegerField;
    QrE001Empresa: TIntegerField;
    QrE001LinArq: TIntegerField;
    QrE001REG: TWideStringField;
    QrE001IND_MOV: TWideStringField;
    QrE100AnoMes: TIntegerField;
    QrE100Empresa: TIntegerField;
    QrE100LinArq: TIntegerField;
    QrE100REG: TWideStringField;
    QrE100DT_INI: TDateField;
    QrE100DT_FIN: TDateField;
    QrE110AnoMes: TIntegerField;
    QrE110Empresa: TIntegerField;
    QrE110LinArq: TIntegerField;
    QrE110E100: TIntegerField;
    QrE110REG: TWideStringField;
    QrE110VL_TOT_DEBITOS: TFloatField;
    QrE110VL_AJ_DEBITOS: TFloatField;
    QrE110VL_TOT_AJ_DEBITOS: TFloatField;
    QrE110VL_ESTORNOS_CRED: TFloatField;
    QrE110VL_TOT_CREDITOS: TFloatField;
    QrE110VL_AJ_CREDITOS: TFloatField;
    QrE110VL_TOT_AJ_CREDITOS: TFloatField;
    QrE110VL_ESTORNOS_DEB: TFloatField;
    QrE110VL_SLD_CREDOR_ANT: TFloatField;
    QrE110VL_SLD_APURADO: TFloatField;
    QrE110VL_TOT_DED: TFloatField;
    QrE110VL_ICMS_RECOLHER: TFloatField;
    QrE110DEB_ESP: TFloatField;
    QrE990AnoMes: TIntegerField;
    QrE990Empresa: TIntegerField;
    QrE990LinArq: TIntegerField;
    QrE990REG: TWideStringField;
    QrE990QTD_LIN_E: TIntegerField;
    Ds1001: TDataSource;
    Qr1990: TmySQLQuery;
    Ds1990: TDataSource;
    Qr1001: TmySQLQuery;
    QrH005AnoMes: TIntegerField;
    QrH005Empresa: TIntegerField;
    QrH005LinArq: TIntegerField;
    QrH005REG: TWideStringField;
    QrH005DT_INV: TDateField;
    QrH005VL_INV: TFloatField;
    QrH010AnoMes: TIntegerField;
    QrH010Empresa: TIntegerField;
    QrH010LinArq: TIntegerField;
    QrH010REG: TWideStringField;
    QrH010COD_ITEM: TWideStringField;
    QrH010UNID: TWideStringField;
    QrH010QTD: TFloatField;
    QrH010VL_UNIT: TFloatField;
    QrH010VL_ITEM: TFloatField;
    QrH010IND_PROP: TWideStringField;
    QrH010COD_PART: TWideStringField;
    QrH010TXT_COMPL: TWideStringField;
    QrH010COD_CTA: TWideStringField;
    QrH990AnoMes: TIntegerField;
    QrH990Empresa: TIntegerField;
    QrH990LinArq: TIntegerField;
    QrH990REG: TWideStringField;
    QrH990QTD_LIN_H: TIntegerField;
    Qr9001: TmySQLQuery;
    Ds9001: TDataSource;
    Ds9900: TDataSource;
    Qr9900: TmySQLQuery;
    Qr9990: TmySQLQuery;
    Ds9990: TDataSource;
    Ds9999: TDataSource;
    Qr9999: TmySQLQuery;
    Qr1001AnoMes: TIntegerField;
    Qr1001Empresa: TIntegerField;
    Qr1001LinArq: TIntegerField;
    Qr1001REG: TWideStringField;
    Qr1001IND_MOV: TSmallintField;
    Qr1990AnoMes: TIntegerField;
    Qr1990Empresa: TIntegerField;
    Qr1990LinArq: TIntegerField;
    Qr1990REG: TWideStringField;
    Qr1990QTD_LIN_1: TIntegerField;
    Qr9001AnoMes: TIntegerField;
    Qr9001Empresa: TIntegerField;
    Qr9001LinArq: TIntegerField;
    Qr9001REG: TWideStringField;
    Qr9001IND_MOV: TSmallintField;
    Qr9900AnoMes: TIntegerField;
    Qr9900Empresa: TIntegerField;
    Qr9900LinArq: TIntegerField;
    Qr9900REG: TWideStringField;
    Qr9900REG_BLC: TWideStringField;
    Qr9900QTD_REG_BLC: TIntegerField;
    Qr9990AnoMes: TIntegerField;
    Qr9990Empresa: TIntegerField;
    Qr9990LinArq: TIntegerField;
    Qr9990REG: TWideStringField;
    Qr9990QTD_LIN_9: TIntegerField;
    Qr9999AnoMes: TIntegerField;
    Qr9999Empresa: TIntegerField;
    Qr9999LinArq: TIntegerField;
    Qr9999REG: TWideStringField;
    Qr9999QTD_LIN: TIntegerField;
    Qr0190: TmySQLQuery;
    Ds0190: TDataSource;
    Qr0190AnoMes: TIntegerField;
    Qr0190Empresa: TIntegerField;
    Qr0190LinArq: TIntegerField;
    Qr0190REG: TWideStringField;
    Qr0190UNID: TWideStringField;
    Qr0190DESCR: TWideStringField;
    Qr0200AnoMes: TIntegerField;
    Qr0200Empresa: TIntegerField;
    Qr0200LinArq: TIntegerField;
    Qr0200REG: TWideStringField;
    Qr0200COD_ITEM: TWideStringField;
    Qr0200DESCR_ITEM: TWideStringField;
    Qr0200COD_BARRA: TWideStringField;
    Qr0200COD_ANT_ITEM: TWideStringField;
    Qr0200UNID_INV: TWideStringField;
    Qr0200TIPO_ITEM: TSmallintField;
    Qr0200COD_NCM: TWideStringField;
    Qr0200EX_IPI: TWideStringField;
    Qr0200COD_GEN: TSmallintField;
    Qr0200COD_LST: TIntegerField;
    Qr0200ALIQ_ICMS: TFloatField;
    QrVersao: TmySQLQuery;
    QrVersaoNUM_VER: TWideStringField;
    QrVersaoDT_INI: TDateField;
    QrVersaoDT_FIN: TDateField;
    QrErr0150: TmySQLQuery;
    DsErr0150: TDataSource;
    PageControl2: TPageControl;
    TabSheet7: TTabSheet;
    DBGrid5: TDBGrid;
    Panel7: TPanel;
    Panel15: TPanel;
    BtCarrega: TBitBtn;
    BtAtrela: TBitBtn;
    QrErr0150AnoMes: TIntegerField;
    QrErr0150Empresa: TIntegerField;
    QrErr0150LinArq: TIntegerField;
    QrErr0150Entidade: TIntegerField;
    QrErr0150REG: TWideStringField;
    QrErr0150COD_PART: TWideStringField;
    QrErr0150NOME: TWideStringField;
    QrErr0150COD_PAIS: TIntegerField;
    QrErr0150CNPJ: TWideStringField;
    QrErr0150CPF: TWideStringField;
    QrErr0150IE: TWideStringField;
    QrErr0150COD_MUN: TIntegerField;
    QrErr0150SUFRAMA: TWideStringField;
    QrErr0150END: TWideStringField;
    QrErr0150NUM: TWideStringField;
    QrErr0150COMPL: TWideStringField;
    QrErr0150BAIRRO: TWideStringField;
    QrErr0190: TmySQLQuery;
    DsErr0190: TDataSource;
    QrLocUM: TmySQLQuery;
    QrLocUMCodigo: TIntegerField;
    TabSheet16: TTabSheet;
    DBGrid7: TDBGrid;
    QrLocGGXControle: TIntegerField;
    QrErr0200: TmySQLQuery;
    DsErr0200: TDataSource;
    TabSheet17: TTabSheet;
    DBGrid8: TDBGrid;
    BtCadEnt: TBitBtn;
    Panel1: TPanel;
    CkNomeGGXIgual: TCheckBox;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EdEmp_CNPJ: TdmkEdit;
    EdEmp_Codigo: TdmkEdit;
    EdEmp_Nome: TdmkEdit;
    QrErrC170: TmySQLQuery;
    DsErrC170: TDataSource;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    QrItsC170: TmySQLQuery;
    QrItsC170AnoMes: TIntegerField;
    QrItsC170Empresa: TIntegerField;
    QrItsC170LinArq: TIntegerField;
    QrItsC170C100: TIntegerField;
    QrItsC170GraGruX: TIntegerField;
    QrItsC170REG: TWideStringField;
    QrItsC170NUM_ITEM: TIntegerField;
    QrItsC170COD_ITEM: TWideStringField;
    QrItsC170DESCR_COMPL: TWideStringField;
    QrItsC170QTD: TFloatField;
    QrItsC170UNID: TWideStringField;
    QrItsC170VL_ITEM: TFloatField;
    QrItsC170VL_DESC: TFloatField;
    QrItsC170IND_MOV: TWideStringField;
    QrItsC170CST_ICMS: TIntegerField;
    QrItsC170CFOP: TIntegerField;
    QrItsC170COD_NAT: TWideStringField;
    QrItsC170VL_BC_ICMS: TFloatField;
    QrItsC170ALIQ_ICMS: TFloatField;
    QrItsC170VL_ICMS: TFloatField;
    QrItsC170VL_BC_ICMS_ST: TFloatField;
    QrItsC170ALIQ_ST: TFloatField;
    QrItsC170VL_ICMS_ST: TFloatField;
    QrItsC170IND_APUR: TWideStringField;
    QrItsC170CST_IPI: TWideStringField;
    QrItsC170COD_ENQ: TWideStringField;
    QrItsC170VL_BC_IPI: TFloatField;
    QrItsC170ALIQ_IPI: TFloatField;
    QrItsC170VL_IPI: TFloatField;
    QrItsC170CST_PIS: TSmallintField;
    QrItsC170VL_BC_PIS: TFloatField;
    QrItsC170ALIQ_PIS_p: TFloatField;
    QrItsC170QUANT_BC_PIS: TFloatField;
    QrItsC170ALIQ_PIS_r: TFloatField;
    QrItsC170VL_PIS: TFloatField;
    QrItsC170CST_COFINS: TSmallintField;
    QrItsC170VL_BC_COFINS: TFloatField;
    QrItsC170ALIQ_COFINS_p: TFloatField;
    QrItsC170QUANT_BC_COFINS: TFloatField;
    QrItsC170ALIQ_COFINS_r: TFloatField;
    QrItsC170VL_COFINS: TFloatField;
    QrItsC170COD_CTA: TWideStringField;
    QrGGX: TmySQLQuery;
    QrGGXGerBxaEstq: TSmallintField;
    QrRecupera: TmySQLQuery;
    QrRecuperaIDCtrl: TIntegerField;
    CkSoTerceiros: TCheckBox;
    CkMPConsumido: TCheckBox;
    QrGGXPrdGrupTip: TIntegerField;
    CkUCConsumido: TCheckBox;
    QrGGXNivel2: TIntegerField;
    QrErr0150ImporExpor: TSmallintField;
    Qr0150ImporExpor: TSmallintField;
    Qr0190ImporExpor: TSmallintField;
    Qr0200ImporExpor: TSmallintField;
    QrC170ImporExpor: TSmallintField;
    Qr0000ImporExpor: TSmallintField;
    Qr0001ImporExpor: TSmallintField;
    Qr0005ImporExpor: TSmallintField;
    Qr0100ImporExpor: TSmallintField;
    Qr0400ImporExpor: TSmallintField;
    Qr0990ImporExpor: TSmallintField;
    QrC001ImporExpor: TSmallintField;
    QrC190ImporExpor: TSmallintField;
    QrC590ImporExpor: TSmallintField;
    QrC990ImporExpor: TSmallintField;
    QrD001ImporExpor: TSmallintField;
    QrD190ImporExpor: TSmallintField;
    QrD590ImporExpor: TSmallintField;
    QrD990ImporExpor: TSmallintField;
    QrE001ImporExpor: TSmallintField;
    QrE100ImporExpor: TSmallintField;
    QrE110ImporExpor: TSmallintField;
    QrE990ImporExpor: TSmallintField;
    QrH001AnoMes: TIntegerField;
    QrH001Empresa: TIntegerField;
    QrH001LinArq: TIntegerField;
    QrH001REG: TWideStringField;
    QrH001IND_MOV: TWideStringField;
    QrH001ImporExpor: TSmallintField;
    QrH005ImporExpor: TSmallintField;
    QrH010ImporExpor: TSmallintField;
    QrH990ImporExpor: TSmallintField;
    Qr1001ImporExpor: TSmallintField;
    Qr1990ImporExpor: TSmallintField;
    Qr9001ImporExpor: TSmallintField;
    Qr9900ImporExpor: TSmallintField;
    Qr9990ImporExpor: TSmallintField;
    Qr9999ImporExpor: TSmallintField;
    QrErrNFs: TmySQLQuery;
    DsErrNFs: TDataSource;
    DBGrid2: TDBGrid;
    Panel2: TPanel;
    QrErr0200ImporExpor: TSmallintField;
    QrErr0200AnoMes: TIntegerField;
    QrErr0200Empresa: TIntegerField;
    QrErr0200LinArq: TIntegerField;
    QrErr0200GraGruX: TIntegerField;
    QrErr0200REG: TWideStringField;
    QrErr0200COD_ITEM: TWideStringField;
    QrErr0200DESCR_ITEM: TWideStringField;
    QrErr0200COD_BARRA: TWideStringField;
    QrErr0200COD_ANT_ITEM: TWideStringField;
    QrErr0200UNID_INV: TWideStringField;
    QrErr0200TIPO_ITEM: TSmallintField;
    QrErr0200COD_NCM: TWideStringField;
    QrErr0200EX_IPI: TWideStringField;
    QrErr0200COD_GEN: TSmallintField;
    QrErr0200COD_LST: TIntegerField;
    QrErr0200ALIQ_ICMS: TFloatField;
    QrErrNFsLinArq: TIntegerField;
    QrErrNFsCOD_PART: TWideStringField;
    QrErrNFsCOD_MOD: TWideStringField;
    QrErrNFsCOD_SIT: TSmallintField;
    QrErrNFsSER: TWideStringField;
    QrErrNFsNUM_DOC: TIntegerField;
    QrErrNFsDT_DOC: TDateField;
    QrErrNFsDT_E_S: TDateField;
    QrErrNFsEntidade: TIntegerField;
    QrErrNFsNOME: TWideStringField;
    QrErrNFsDOC_PART: TWideStringField;
    QrErrNFsCOD_ITEM: TWideStringField;
    QrErrNFsQTD: TFloatField;
    QrErrNFsUNID: TWideStringField;
    QrErrNFsVL_ITEM: TFloatField;
    QrErr0200Receptor: TIntegerField;
    Panel3: TPanel;
    Label1: TLabel;
    EdErrEnt: TdmkEdit;
    Label2: TLabel;
    EdErrAM: TdmkEdit;
    SpeedButton2: TSpeedButton;
    frxErr_0200: TfrxReport;
    frxDsErr0200: TfrxDBDataset;
    SpeedButton3: TSpeedButton;
    frxDsErrNFs: TfrxDBDataset;
    RGConfig: TRadioGroup;
    QrE111: TmySQLQuery;
    DsE111: TDataSource;
    QrE111ImporExpor: TSmallintField;
    QrE111AnoMes: TIntegerField;
    QrE111Empresa: TIntegerField;
    QrE111LinArq: TIntegerField;
    QrE111E110: TIntegerField;
    QrE111REG: TWideStringField;
    QrE111COD_AJ_APUR: TWideStringField;
    QrE111DESCR_COMPL_AJ: TWideStringField;
    QrE111VL_AJ_APUR: TFloatField;
    QrErrs_: TmySQLQuery;
    DsErrs_: TDataSource;
    QrErrs_ImporExpor: TSmallintField;
    QrErrs_AnoMes: TIntegerField;
    QrErrs_Empresa: TIntegerField;
    QrErrs_LinArq: TIntegerField;
    QrErrs_REG: TWideStringField;
    QrErrs_Campo: TWideStringField;
    QrErrs_Erro: TSmallintField;
    QrG990: TmySQLQuery;
    QrG001: TmySQLQuery;
    DsG001: TDataSource;
    DsG990: TDataSource;
    QrG001ImporExpor: TSmallintField;
    QrG001AnoMes: TIntegerField;
    QrG001Empresa: TIntegerField;
    QrG001LinArq: TIntegerField;
    QrG001REG: TWideStringField;
    QrG001IND_MOV: TWideStringField;
    QrG990ImporExpor: TSmallintField;
    QrG990AnoMes: TIntegerField;
    QrG990Empresa: TIntegerField;
    QrG990LinArq: TIntegerField;
    QrG990REG: TWideStringField;
    QrG990QTD_LIN_G: TIntegerField;
    QrE116: TmySQLQuery;
    DsE116: TDataSource;
    QrE116ImporExpor: TSmallintField;
    QrE116AnoMes: TIntegerField;
    QrE116Empresa: TIntegerField;
    QrE116LinArq: TIntegerField;
    QrE116E110: TIntegerField;
    QrE116REG: TWideStringField;
    QrE116COD_OR: TWideStringField;
    QrE116VL_OR: TFloatField;
    QrE116DT_VCTO: TDateField;
    QrE116COD_REC: TWideStringField;
    QrE116NUM_PROC: TWideStringField;
    QrE116IND_PROC: TWideStringField;
    QrE116PROC: TWideStringField;
    QrE116TXT_COMPL: TWideStringField;
    QrE116MES_REF: TDateField;
    TabSheet1: TTabSheet;
    QrINN_0200: TmySQLQuery;
    QrINN_0200LinArq: TIntegerField;
    QrINN_0200Distancia: TIntegerField;
    QrINN_0200GraGruX: TIntegerField;
    QrINN_0200Nome: TWideStringField;
    QrINN_0200COD_ITEM: TWideStringField;
    QrINN_0200DESCR_ITEM: TWideStringField;
    QrINN_0200COD_ANT_ITEM: TWideStringField;
    QrGG1: TmySQLQuery;
    QrGG1Nome: TWideStringField;
    QrLoc0200: TmySQLQuery;
    QrLoc0200GraGruX: TIntegerField;
    QrINN_0200Levenshtein_Meu: TWideStringField;
    QrINN_0200Levenshtein_Inn: TWideStringField;
    PageControl3: TPageControl;
    TabSheet3: TTabSheet;
    DBGrid3: TDBGrid;
    QrAdver_0200: TmySQLQuery;
    DsAdver_0200: TDataSource;
    QrAdver_0200LinArq: TIntegerField;
    QrAdver_0200Distancia: TIntegerField;
    QrAdver_0200GraGruX: TIntegerField;
    QrAdver_0200Nome: TWideStringField;
    QrAdver_0200COD_ITEM: TWideStringField;
    QrAdver_0200DESCR_ITEM: TWideStringField;
    QrAdver_0200COD_ANT_ITEM: TWideStringField;
    QrAdver_0200Levenshtein_Meu: TWideStringField;
    QrAdver_0200Levenshtein_Inn: TWideStringField;
    QrAdver_0200NivelExclu: TIntegerField;
    frxSPEDEFD_IMPORT_003_00: TfrxReport;
    frxDsAdver_0200: TfrxDBDataset;
    Panel4: TPanel;
    SpeedButton4: TSpeedButton;
    QrLocCOD_PART: TmySQLQuery;
    QrLocCOD_PARTEntidade: TIntegerField;
    QrNFsA: TmySQLQuery;
    QrNFsAFatID: TIntegerField;
    QrNFsAFatNum: TIntegerField;
    QrNFsAEmpresa: TIntegerField;
    QrNFsAIDCtrl: TIntegerField;
    QrGGXNivel1: TIntegerField;
    QrLocUNID: TmySQLQuery;
    QrLocUNIDUnidMed: TIntegerField;
    QrLocUNIDGrandeza: TSmallintField;
    QrE112: TmySQLQuery;
    DsE112: TDataSource;
    QrE112ImporExpor: TSmallintField;
    QrE112AnoMes: TIntegerField;
    QrE112Empresa: TIntegerField;
    QrE112LinArq: TIntegerField;
    QrE112E111: TIntegerField;
    QrE112REG: TWideStringField;
    QrE112NUM_DA: TWideStringField;
    QrE112NUM_PROC: TWideStringField;
    QrE112IND_PROC: TWideStringField;
    QrE112PROC: TWideStringField;
    QrE112TXT_COMPL: TWideStringField;
    QrE113: TmySQLQuery;
    DsE113: TDataSource;
    QrE113ImporExpor: TSmallintField;
    QrE113AnoMes: TIntegerField;
    QrE113Empresa: TIntegerField;
    QrE113LinArq: TIntegerField;
    QrE113E111: TIntegerField;
    QrE113REG: TWideStringField;
    QrE113COD_PART: TWideStringField;
    QrE113COD_MOD: TWideStringField;
    QrE113SER: TWideStringField;
    QrE113SUB: TWideStringField;
    QrE113NUM_DOC: TIntegerField;
    QrE113DT_DOC: TDateField;
    QrE113COD_ITEM: TWideStringField;
    QrE113VL_AJ_ITEM: TFloatField;
    QrE115: TmySQLQuery;
    DsE115: TDataSource;
    QrE115ImporExpor: TSmallintField;
    QrE115AnoMes: TIntegerField;
    QrE115Empresa: TIntegerField;
    QrE115LinArq: TIntegerField;
    QrE115E110: TIntegerField;
    QrE115REG: TWideStringField;
    QrE115COD_INF_ADIC: TWideStringField;
    QrE115VL_INF_ADIC: TFloatField;
    QrE115DESCR_COMPL_AJ: TWideStringField;
    QrC500ImporExpor: TSmallintField;
    QrC500AnoMes: TIntegerField;
    QrC500Empresa: TIntegerField;
    QrC500LinArq: TIntegerField;
    QrC500REG: TWideStringField;
    QrC500IND_OPER: TWideStringField;
    QrC500IND_EMIT: TWideStringField;
    QrC500COD_PART: TWideStringField;
    QrC500COD_MOD: TWideStringField;
    QrC500COD_SIT: TWideStringField;
    QrC500SER: TWideStringField;
    QrC500SUB: TWideStringField;
    QrC500COD_CONS: TWideStringField;
    QrC500NUM_DOC: TIntegerField;
    QrC500DT_DOC: TDateField;
    QrC500DT_E_S: TDateField;
    QrC500VL_DOC: TFloatField;
    QrC500VL_DESC: TFloatField;
    QrC500VL_FORN: TFloatField;
    QrC500VL_SERV_NT: TFloatField;
    QrC500VL_TERC: TFloatField;
    QrC500VL_DA: TFloatField;
    QrC500VL_BC_ICMS: TFloatField;
    QrC500VL_ICMS: TFloatField;
    QrC500VL_BC_ICMS_ST: TFloatField;
    QrC500VL_ICMS_ST: TFloatField;
    QrC500COD_INF: TWideStringField;
    QrC500VL_PIS: TFloatField;
    QrC500VL_COFINS: TFloatField;
    QrC500TP_LIGACAO: TWideStringField;
    QrC500COD_GRUPO_TENSAO: TWideStringField;
    QrD500ImporExpor: TSmallintField;
    QrD500AnoMes: TIntegerField;
    QrD500Empresa: TIntegerField;
    QrD500LinArq: TIntegerField;
    QrD500REG: TWideStringField;
    QrD500IND_OPER: TWideStringField;
    QrD500IND_EMIT: TWideStringField;
    QrD500COD_PART: TWideStringField;
    QrD500COD_MOD: TWideStringField;
    QrD500COD_SIT: TWideStringField;
    QrD500SER: TWideStringField;
    QrD500SUB: TWideStringField;
    QrD500NUM_DOC: TIntegerField;
    QrD500DT_DOC: TDateField;
    QrD500DT_A_P: TDateField;
    QrD500VL_DOC: TFloatField;
    QrD500VL_DESC: TFloatField;
    QrD500VL_SERV: TFloatField;
    QrD500VL_SERV_NT: TFloatField;
    QrD500VL_TERC: TFloatField;
    QrD500VL_DA: TFloatField;
    QrD500VL_BC_ICMS: TFloatField;
    QrD500VL_ICMS: TFloatField;
    QrD500COD_INF: TWideStringField;
    QrD500VL_PIS: TFloatField;
    QrD500VL_COFINS: TFloatField;
    QrD500COD_CTA: TWideStringField;
    QrD500TP_ASSINANTE: TWideStringField;
    QrC100ImporExpor: TSmallintField;
    QrC100AnoMes: TIntegerField;
    QrC100Empresa: TIntegerField;
    QrC100LinArq: TIntegerField;
    QrC100REG: TWideStringField;
    QrC100IND_OPER: TWideStringField;
    QrC100IND_EMIT: TWideStringField;
    QrC100COD_PART: TWideStringField;
    QrC100COD_MOD: TWideStringField;
    QrC100COD_SIT: TWideStringField;
    QrC100SER: TWideStringField;
    QrC100NUM_DOC: TIntegerField;
    QrC100CHV_NFE: TWideStringField;
    QrC100DT_DOC: TDateField;
    QrC100DT_E_S: TDateField;
    QrC100VL_DOC: TFloatField;
    QrC100IND_PGTO: TWideStringField;
    QrC100VL_DESC: TFloatField;
    QrC100VL_ABAT_NT: TFloatField;
    QrC100VL_MERC: TFloatField;
    QrC100IND_FRT: TWideStringField;
    QrC100VL_FRT: TFloatField;
    QrC100VL_SEG: TFloatField;
    QrC100VL_OUT_DA: TFloatField;
    QrC100VL_BC_ICMS: TFloatField;
    QrC100VL_ICMS: TFloatField;
    QrC100VL_BC_ICMS_ST: TFloatField;
    QrC100VL_ICMS_ST: TFloatField;
    QrC100VL_IPI: TFloatField;
    QrC100VL_PIS: TFloatField;
    QrC100VL_COFINS: TFloatField;
    QrC100VL_PIS_ST: TFloatField;
    QrC100VL_COFINS_ST: TFloatField;
    QrC100ParTipo: TIntegerField;
    QrC100ParCodi: TIntegerField;
    QrC100FatID: TIntegerField;
    QrC100FatNum: TIntegerField;
    QrD100ImporExpor: TSmallintField;
    QrD100AnoMes: TIntegerField;
    QrD100Empresa: TIntegerField;
    QrD100LinArq: TIntegerField;
    QrD100REG: TWideStringField;
    QrD100IND_OPER: TWideStringField;
    QrD100IND_EMIT: TWideStringField;
    QrD100COD_PART: TWideStringField;
    QrD100COD_MOD: TWideStringField;
    QrD100COD_SIT: TWideStringField;
    QrD100SER: TWideStringField;
    QrD100SUB: TWideStringField;
    QrD100NUM_DOC: TIntegerField;
    QrD100CHV_CTE: TWideStringField;
    QrD100DT_DOC: TDateField;
    QrD100DT_A_P: TDateField;
    QrD100TP_CTE: TWideStringField;
    QrD100CHV_CTE_REF: TWideStringField;
    QrD100VL_DOC: TFloatField;
    QrD100VL_DESC: TFloatField;
    QrD100IND_FRT: TWideStringField;
    QrD100VL_SERV: TFloatField;
    QrD100VL_BC_ICMS: TFloatField;
    QrD100VL_ICMS: TFloatField;
    QrD100VL_NT: TFloatField;
    QrD100COD_INF: TWideStringField;
    QrD100COD_CTA: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel47: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtAtrelaClick(Sender: TObject);
    procedure QrErr0150BeforeClose(DataSet: TDataSet);
    procedure QrErr0150AfterOpen(DataSet: TDataSet);
    procedure BtCadEntClick(Sender: TObject);
    procedure EdEmp_CodigoChange(Sender: TObject);
    procedure MeImportadoChange(Sender: TObject);
    procedure QrErr0200AfterScroll(DataSet: TDataSet);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
  private
    { Private declarations }
    FCarregou: Boolean;
    FEmpresa, FAnoMes: Integer;
    //
    function  InsereNotaFiscal_C100_Cab(FatID, FatNum, Empresa: Integer): Boolean;
    function  InsereNotaFiscal_C170_I(FatID, FatNum, Empresa, GraGruX, Nivel1,
              nItem: Integer): Boolean;
    function  InsereNotaFiscal_C170_N(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function  InsereNotaFiscal_C170_O(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function  InsereNotaFiscal_C170_Q(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function  InsereNotaFiscal_C170_S(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    //
    function  ValidaPeriodoInteiroDeUmMes(const DtI, DtF: TDateTime): Boolean;
    function  DefineEntidadeCarregada(const CNPJCPF: String; const AvisaErros:
              Boolean): Boolean;
    function  ExcluiPeriodoSelecionado(Empresa: Integer): Boolean;
    procedure VerificaBtImporta();
    function QuantidadeDeCamposNaoConferem(Versao: Integer; REG: String;
             Campos: Integer): Boolean;
    function ImportaEstoque(): Boolean;
    procedure PreparaCarregamento();
    procedure ReopenErrNFs();

  public
    { Public declarations }
  end;

  var
  FmSPED_EFD_Importa: TFmSPED_EFD_Importa;


implementation

uses UnMyObjects, ModuleGeral, Module, NFeCabA_0000, MyDBCheck, UnGrade_Create,
  UMySQLModule,  UnInternalConsts, NatOper, ModProd, (*Sintegra_PesqPrd,*)
  UnGrade_Tabs, ModuleNFe_0000, EntidadesSel, Entidade2, SPED_EFD_II_Tabs,
  UnSPED_Geral, DmkDAC_PF;

{$R *.DFM}

const
  FImporExpor = 1; // 1 = Importa - N�o mexer!!!
  
procedure TFmSPED_EFD_Importa.BtAtrelaClick(Sender: TObject);
begin
  if MyObjects.FIC(QrErr0150COD_PAIS.Value=0, nil,
    'N�o ser� poss�vel atrelar a entidade!' + sLineBreak +
    'No arquivo a ser importado n�o foi definido o c�digo do pa�s!') then
    Exit;
  //
  if MyObjects.FIC(
    (Trim(QrErr0150CNPJ.Value) = '') and (Trim(QrErr0150CPF.Value) = '') and
    (QrErr0150COD_PAIS.Value = 1058), nil,
    'N�o ser� poss�vel atrelar a entidade!' + sLineBreak +
    'No arquivo a ser importado n�o foi definido o CNPJ/CPF do participante!') then
    Exit;
  //
  if DBCheck.CriaFm(TFmEntidadesSel, FmEntidadesSel, afmoLiberado) then
  begin
    FmEntidadesSel.ShowModal;
    if VAR_ENTIDADE <> 0 then
    begin
      if (
      (FmEntidadesSel.QrSelCodiPais.Value = 1058) and
       (Trim(FmEntidadesSel.QrSelCNPJ_CPF.Value) = '')
      ) or (
      (FmEntidadesSel.QrSelCodiPais.Value <> 1058) and
      (FmEntidadesSel.QrSelCodiPais.Value <> 0) and
      (Trim(FmEntidadesSel.QrSelCOD_PART.Value) = '')
      ) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'spedefd0150', False, [
        'Entidade'], [
        'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
        VAR_ENTIDADE], [
        QrErr0150ImporExpor.Value, QrErr0150AnoMes.Value,
        QrErr0150Empresa.Value, QrErr0150LinArq.Value], True) then
        begin
          // Se for estrangeiro...
          if QrErr0150COD_PAIS.Value <> 1058 then
          begin
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'entidades', False, [
            'COD_PART'], [
            'Codigo'], [
            QrErr0150COD_PART.Value], [
            VAR_ENTIDADE], True);
          end else
          //... caso contr�rio:
          begin
            if Trim(QrErr0150CNPJ.Value) <> '' then
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'entidades', False, [
              'CNPJ'], [
              'Codigo'], [
              QrErr0150CNPJ.Value], [
              VAR_ENTIDADE], True)
            else
            if Trim(QrErr0150CPF.Value) <> '' then
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'entidades', False, [
              'CPF'], [
              'Codigo'], [
              QrErr0150CPF.Value], [
              VAR_ENTIDADE], True);
          end;
        end;
        Qr0150.Close;
        UnDmkDAC_PF.AbreQuery(Qr0150, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
        //
        VerificaBtImporta();
      end else Geral.MensagemBox(
      'N�o ser� poss�vel atrelar a entidade!' + sLineBreak +
      'A combina��o C�digo do Pa�s X CNPJ/CPF X COD_PART n�o permite!' + sLineBreak+
      'O c�digo do pa�s deve estar definido e o CNPJ/CPF deve ser combin�vel!'
      + sLineBreak +
      'N�o altere o CNPJ/CPF no cadastro da entidade sem prud�ncia e sem per�cia!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
    //
    FmEntidadesSel.Destroy;
  end;
end;

procedure TFmSPED_EFD_Importa.BtCadEntClick(Sender: TObject);
var
  LinArq, Entidade, cUF: Integer;
  xLogr, nLogr, xRua, xUF: String;
begin
  if Geral.MensagemBox('Confirma o cadastro da entidade selecionada?' + sLineBreak +
  'Vale lembrar que a entidade j� pode estar cadastrada sem ter seu CNPJ/CPF no cadastro!' + sLineBreak +
  'Antes de cadastrar tenha certeza de ter pesquisado e n�o estar duplicando um cadastro!' + sLineBreak +
  'Deseja continuar assim mesmo?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION)
  = ID_YES then
  begin
    Geral.SeparaLogradouro(0, QrErr0150END.Value, xLogr, nLogr, xRua);
    cUF := Geral.IMV(Copy(FormatFloat('0', QrErr0150COD_MUN.Value), 1, 2));
    xUF := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(cUF);
    //
    if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
    begin
      FmEntidade2.Incluinovaentidade1Click(Self);
      FmEntidade2.FSetando := True;
      if Trim(QrErr0150CPF.Value) <> '' then
      begin
        FmEntidade2.RGTipo.ItemIndex          := 1;
        FmEntidade2.EdNome.Text               := QrErr0150NOME.Value;
        FmEntidade2.EdPCodiPais.ValueVariant  := QrErr0150COD_PAIS.Value;
        FmEntidade2.CBPCodiPais.KeyValue      := QrErr0150COD_PAIS.Value;
        FmEntidade2.EdPCodMunici.ValueVariant := QrErr0150COD_MUN.Value;
        FmEntidade2.CBPCodMunici.KeyValue     := QrErr0150COD_MUN.Value;
        if nLogr <> '' then
        begin
          FmEntidade2.EdPLograd.ValueVariant    := nLogr;
          FmEntidade2.CBPLograd.KeyValue        := nLogr;
        end;
        FmEntidade2.EdPRua.Text               := xRua;
        FmEntidade2.EdPNumero.Text            := QrErr0150NUM.Value;
        FmEntidade2.EdPCompl.Text             := QrErr0150COMPL.Value;
        FmEntidade2.EdPBairro.Text            := QrErr0150BAIRRO.Value;
        FmEntidade2.EdPUF.Text                := xUF;
      end else
      begin
        FmEntidade2.RGTipo.ItemIndex          := 0;
        FmEntidade2.EdRazaoSocial.Text        := QrErr0150NOME.Value;
        FmEntidade2.EdECodiPais.ValueVariant  := QrErr0150COD_PAIS.Value;
        FmEntidade2.CBECodiPais.KeyValue      := QrErr0150COD_PAIS.Value;
        FmEntidade2.EdECodMunici.ValueVariant := QrErr0150COD_MUN.Value;
        FmEntidade2.CBECodMunici.KeyValue     := QrErr0150COD_MUN.Value;
        if nLogr <> '' then
        begin
          FmEntidade2.EdELograd.ValueVariant    := nLogr;
          FmEntidade2.CBELograd.KeyValue        := nLogr;
        end;
        FmEntidade2.EdERua.Text               := xRua;
        FmEntidade2.EdENumero.Text            := QrErr0150NUM.Value;
        FmEntidade2.EdECompl.Text             := QrErr0150COMPL.Value;
        FmEntidade2.EdEBairro.Text            := QrErr0150BAIRRO.Value;
        FmEntidade2.EdEUF.Text                := xUF;
      end;
      //
      FmEntidade2.EdCNPJ.ValueVariant       := QrErr0150CNPJ.Value;
      FmEntidade2.EdIE.ValueVariant         := QrErr0150IE.Value;
      FmEntidade2.EdCPF.ValueVariant        := QrErr0150CPF.Value;
      FmEntidade2.EdSUFRAMA.ValueVariant    := QrErr0150SUFRAMA.Value;
      FmEntidade2.EdCOD_PART.ValueVariant   := QrErr0150COD_PART.Value;
      FmEntidade2.FSetando := False;
      FmEntidade2.ShowModal;
      FmEntidade2.Destroy;
    end;
    LinArq := QrErr0150LinArq.Value;
    QrErr0150.First;
    while not QrErr0150.Eof do
    begin
      Entidade := 0;
      if QrErr0150CNPJ.Value <> '' then
        DModG.ObtemEntidadeDeCNPJCFP(QrErr0150CNPJ.Value, Entidade)
      else
      if QrErr0150CPF.Value <> '' then
        DModG.ObtemEntidadeDeCNPJCFP(QrErr0150CPF.Value, Entidade)
      else
      if QrErr0150COD_PART.Value <> '' then
        DModG.ObtemEntidadeDeCOD_PART(QrErr0150COD_PART.Value, Entidade);
      //
      if Entidade <> 0 then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'spedefd0150', False, [
        'Entidade'], [
        'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
        Entidade], [
        QrErr0150ImporExpor.Value, QrErr0150AnoMes.Value,
        QrErr0150Empresa.Value, QrErr0150LinArq.Value], False);
      end;
      //
      QrErr0150.Next;
    end;
    VerificaBtImporta();
    QrErr0150.Locate('LinArq', LinArq, []);
  end;
end;

procedure TFmSPED_EFD_Importa.BtCarregaClick(Sender: TObject);
const
  Tit = 'Abertura de tabela na procedure BtCarregaClick()';
  //
{
  procedure IncluiProduto(CODPROD, DESCRICAO, UNIDADE, CODNCM: String; IPI_Alq: Double);
  const
    CST_A = '0';
    CST_B = '0';
    Prefixo = '';
    Nivel3 = 0;
    GraTamCad = 1;
    Peso = 0.000;
    PIS_AlqP = 0.00;
    COFINS_AlqP = 0.00;
    GraCorCad = 0;
    GraTamI = 1;
    GraCusPrc = 5; // Lista de Pre�os (pre�o m�dio)
  var
    Nivel2, Nivel1, CodUsu, PrdGrupTip, Fabricante, UnidMed: Integer;
    //
    Nome, (*Antigo,*) Referencia, NCM: String;
    SQLType: TSQLType;
    //Continua: Boolean;
    //
    //NCMS: array of String;
    //
    Controle, GraGruC, GraGru1, GraGruX, Entidade: Integer;
    CustoPreco: Double;
  begin
    Entidade := EdEmp_Codigo.ValueVariant;
    Nivel1 := Geral.IMV(CODPROD);
    Nivel2 := -4;
    //
    PrdGrupTip := -2;
    Referencia := '';
    Nome       := DESCRICAO;
    Fabricante := 0;
    //
    QrUnidMed.Close;
    QrUnidMed.Params[0].AsString := UNIDADE;
    UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    UnidMed := QrUnidMedCodigo.Value;
    NCM     := Geral.FormataNCM(CODNCM);
    CustoPreco := 0;
    //
    CodUsu := Nivel1;

    //
    //   GRAGRU1
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru1', False, [
    'Nivel3', 'Nivel2', 'CodUsu',
    'Nome', 'PrdGrupTip', 'GraTamCad',
    'UnidMed', 'CST_A', 'CST_B',
    'NCM', 'Peso', 'IPI_Alq',
    (*'IPI_CST', 'IPI_cEnq', 'IPI_vUnid',
    'IPI_TpTrib', 'TipDimens', 'PerCuztMin',
    'PerCuztMax', 'MedOrdem', 'PartePrinc',
    'InfAdProd', 'SiglaCustm', 'HowBxaEstq',
    'GerBxaEstq', 'PIS_CST',*) 'PIS_AlqP',
    (*'PIS_AlqV', 'PISST_AlqP', 'PISST_AlqV',
    'COFINS_CST',*) 'COFINS_AlqP', (*'COFINS_AlqV',
    'COFINSST_AlqP', 'COFINSST_AlqV', 'ICMS_modBC',
    'ICMS_modBCST', 'ICMS_pRedBC', 'ICMS_pRedBCST',
    'ICMS_pMVAST', 'ICMS_pICMSST', 'ICMS_Pauta',
    'ICMS_MaxTab', 'IPI_pIPI', 'cGTIN_EAN',
    'EX_TIPI', 'PIS_pRedBC', 'PISST_pRedBCST',
    'COFINS_pRedBC', 'COFINSST_pRedBCST', 'ICMSRec_pRedBC',
    'IPIRec_pRedBC', 'PISRec_pRedBC', 'COFINSRec_pRedBC',
    'ICMSRec_pAliq', 'IPIRec_pAliq', 'PISRec_pAliq',
    'COFINSRec_pAliq', 'ICMSRec_tCalc', 'IPIRec_tCalc',
    'PISRec_tCalc', 'COFINSRec_tCalc', 'ICMSAliqSINTEGRA',
    'ICMSST_BaseSINTEGRA', 'FatorClas', 'prod_indTot',*)
    'Referencia', 'Fabricante'], [
    'Nivel1'], [
    Nivel3, Nivel2, CodUsu,
    Nome, PrdGrupTip, GraTamCad,
    UnidMed, CST_A, CST_B,
    NCM, Peso, IPI_Alq,
    (*IPI_CST, IPI_cEnq, IPI_vUnid,
    IPI_TpTrib, TipDimens, PerCuztMin,
    PerCuztMax, MedOrdem, PartePrinc,
    InfAdProd, SiglaCustm, HowBxaEstq,
    GerBxaEstq, PIS_CST,*) PIS_AlqP,
    (*PIS_AlqV, PISST_AlqP, PISST_AlqV,
    COFINS_CST,*) COFINS_AlqP, (*COFINS_AlqV,
    COFINSST_AlqP, COFINSST_AlqV, ICMS_modBC,
    ICMS_modBCST, ICMS_pRedBC, ICMS_pRedBCST,
    ICMS_pMVAST, ICMS_pICMSST, ICMS_Pauta,
    ICMS_MaxTab, IPI_pIPI, cGTIN_EAN,
    EX_TIPI, PIS_pRedBC, PISST_pRedBCST,
    COFINS_pRedBC, COFINSST_pRedBCST, ICMSRec_pRedBC,
    IPIRec_pRedBC, PISRec_pRedBC, COFINSRec_pRedBC,
    ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq,
    COFINSRec_pAliq, ICMSRec_tCalc, IPIRec_tCalc,
    PISRec_tCalc, COFINSRec_tCalc, ICMSAliqSINTEGRA,
    ICMSST_BaseSINTEGRA, FatorClas, prod_indTot,*)
    Referencia, Fabricante], [
    Nivel1], True) then ;
    //
    ///  GRAGRUC
    Controle  := Nivel1;
(*
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruc', False, [
    'Nivel1', 'GraCorCad'], ['Controle'], [
    Nivel1, GraCorCad], [Controle], True) then ;
    //
*)
    //   GRAGRUX
    GraGruC := Nivel1;
    GraGru1 := Nivel1;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragrux', False, [
    'GraGruC', 'GraGru1', 'GraTamI'], ['Controle'], [
    GraGruC, GraGru1, GraTamI], [Controle], True) then ;
    //
    //   GRAGRUVAL = 5
    GraGruX := Controle;
    if CustoPreco >= 0.000001 then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
      'GraGruX', 'GraGru1', 'GraCusPrc', 'CustoPreco', 'Entidade'], [
      'Controle'], [GraGruX, GraGru1, GraCusPrc, CustoPreco, Entidade], [
      Controle], True) then ;
    end;
    //
  end;
  procedure LimparDadosTabela(SQL: WideString);
  begin
    MyObjects.Informa(LaAviso, True, 'Executando limpeza: ' + SQL);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(SQL);
    Dmod.QrUpd.ExecSQL;
  end;
}
var
  I, J, LinArq, Distancia, NivelExclu: Integer;
  Lista: TStringList;
  Levenshtein_Meu, Levenshtein_Inn: String;
  //
  // 0000
  REG, NOME, CNPJ, CPF, UF, IE, IM, SUFRAMA, IND_PERFIL: String;
  COD_VER, COD_FIN, COD_MUN, IND_ATIV: Integer;
  DT_INI, DT_FIN: String; //TDateTime;
  Empresa, Entidade, UnidMed, GraGruX: Integer;
  //  001
  CEP: Integer;
  FANTASIA, _END, NUM, COMPL, BAIRRO, FONE, FAX, EMAIL: String;
  //  005
  // 100
  CRC: String;
  // 0150
  COD_PART: String;
  COD_PAIS: Integer;
  // 0190
  UNID, DESCR: String;
  // 0200
  COD_ITEM, DESCR_ITEM, COD_BARRA, COD_ANT_ITEM, UNID_INV, TIPO_ITEM, COD_NCM,
  EX_IPI, COD_GEN, COD_LST: String;
  ALIQ_ICMS: Double;
  // 400
  COD_NAT, DESCR_NAT: String;
  // 990
  QTD_LIN_0: Integer;
  // C001
  IND_MOV: String;
  // C100
  IND_OPER, IND_EMIT, COD_MOD, SER, CHV_NFE, IND_PGTO, IND_FRT, COD_SIT: String;
  NUM_DOC: Integer;
  DT_DOC, DT_E_S: String; //TDateTime;
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST: Double;
  // C170
  DESCR_COMPL, IND_APUR, CST_IPI, COD_ENQ, COD_CTA: String;
  C100, NUM_ITEM, CST_ICMS, CFOP: Integer;
  QTD, VL_ITEM, ALIQ_ST, VL_BC_IPI, ALIQ_IPI, CST_PIS, VL_BC_PIS, ALIQ_PIS_p,
  QUANT_BC_PIS, ALIQ_PIS_r, CST_COFINS, VL_BC_COFINS, ALIQ_COFINS_p,
  QUANT_BC_COFINS, ALIQ_COFINS_r: Double;
  // C190
  VL_OPR, VL_RED_BC: Double;
  COD_OBS: String;
  // C500
  C500: Integer;
  SUB, TP_LIGACAO, COD_CONS, COD_INF, COD_GRUPO_TENSAO: String;
  VL_FORN, VL_SERV_NT, VL_TERC, VL_DA: Double;
  // C990
  QTD_LIN_C: Integer;
  // D100
  SUB_C, CHV_CTE, DT_A_P, CHV_CTE_REF: String;
  D100, TP_CTE: Integer;
  VL_SERV, VL_NT: Double;
  // D190
  // D500
  D500, TP_ASSINANTE: Integer;
  // D990
  QTD_LIN_D: Integer;
  //
  // E100
  E100: Integer;
  // E110
  E110: Integer;
  VL_TOT_DEBITOS, VL_AJ_DEBITOS, VL_TOT_AJ_DEBITOS, VL_ESTORNOS_CRED,
  VL_TOT_CREDITOS, VL_AJ_CREDITOS, VL_TOT_AJ_CREDITOS, VL_ESTORNOS_DEB,
  VL_SLD_CREDOR_ANT, VL_SLD_APURADO, VL_TOT_DED, VL_ICMS_RECOLHER,
  VL_SLD_CREDOR_TRANSPORTAR, DEB_ESP: Double;
  // E111
  COD_AJ_APUR, DESCR_COMPL_AJ: String;
  VL_AJ_APUR: Double;
  // E116
  COD_OR, DT_VCTO, COD_REC, NUM_PROC, IND_PROC, PROC, MES_REF: String;
  VL_OR: Double;
  // E990
  QTD_LIN_E: Integer;
  // G990
  QTD_LIN_G: Integer;
  // H005
  DT_INV: String;
  VL_INV: Double;
  // H010
  VL_UNIT: Double;
  IND_PROP, TXT_COMPL: String;
  // H990
  QTD_LIN_H: Integer;
  // 1990
  QTD_LIN_1: Integer;
  // 9900
  REG_BLC: String;
  QTD_REG_BLC: Integer;
  // 9990
  QTD_LIN_9: Integer;
  // 9999
  QTD_LIN: Integer;
  //
  DataIni, DataFim: TDateTime;
  MySQLQuery: TmySQLQuery;
  NomeTab: String;
begin
  BtCarrega.Enabled := False;
  if RGConfig.ItemIndex > 1 then
  begin
    Geral.MensagemBox(
    'Configura��o de carregamento e importa��o n�o implementado:' + sLineBreak +
    RGConfig.Items[RGConfig.ItemIndex] + sLineBreak + sLineBreak + 'Solicite � DERMATEK',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  FCarregou := False;
  Lista := TStringList.Create;
  try
    MeIgnor.Lines.Clear;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas tempor�rias');
    GradeCriar.RecriaTempTableNovo(ntrttSPEDINN_0200, DModG.QrUpdPID1, False);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando carregamento');
    Geral.MyExtractStrings(['|'], [' '], PChar(MeImportado.Lines[0]), Lista);
    COD_VER := Geral.IMV(Lista[01]);
    if MyObjects.FIC(not (COD_VER in ([3,4])), nil,
      'Vers�o de arquivo n�o implementada!' + sLineBreak +
      'Solicite implementa��o � DERMATEK!') then
      Exit;
    DataIni := Geral.ValidaDataBR(Lista[03], True, False);
    DataFim := Geral.ValidaDataBR(Lista[04], True, False);
    if not ValidaPeriodoInteiroDeUmMes(DataIni, DataFim) then
      Exit;
    CNPJ := Lista[06];
    if not DefineEntidadeCarregada(CNPJ, True) then
      Exit;
    if MyObjects.FIC(EdEmp_Codigo.ValueVariant = 0, EdEmp_Codigo, 'Empresa n�o definida!') then
      Exit;
    Empresa := EdEmp_Codigo.ValueVariant;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo dados duplic�veis');
    if not ExcluiPeriodoSelecionado(EdEmp_Codigo.ValueVariant) then
      Exit;
    //
    PB1.Position := 0;
    PB1.Max := MeImportado.Lines.Count;
    for I := 0 to MeImportado.Lines.Count - 1 do
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando linha n� ' +
      FormatFloat('0000', I) + ' = ' + MeImportado.Lines[I]);
      PB1.Position := PB1.Position + 1;
      LinArq := I;
      Lista.Clear;
      //
      Geral.MyExtractStrings(['|'], [' '], PChar(MeImportado.Lines[I]), Lista);
      //
      if Lista.Count > 0 then
      begin
        REG := Lista[00];
        if QuantidadeDeCamposNaoConferem(COD_VER, REG, Lista.Count) then
          Exit;
        if REG = '0000' then
        begin
          COD_FIN := Geral.IMV(Lista[02]);
          DT_INI     := Geral.FDT(DataIni, 1);
          DT_FIN     := Geral.FDT(DataFim, 1);
          NOME       := Lista[05];
          CNPJ       := Lista[06];
          CPF        := Lista[07];
          UF         := Lista[08];
          IE         := Lista[09];
          COD_MUN    := Geral.IMV(Lista[10]);
          IM         := Lista[11];
          SUFRAMA    := Lista[12];
          IND_PERFIL := Lista[13];
          IND_ATIV   := Geral.IMV(Lista[14]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0000', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq',
          'REG', 'COD_VER', 'COD_FIN',
          'DT_INI', 'DT_FIN', 'NOME',
          'CNPJ', 'CPF', 'UF',
          'IE', 'COD_MUN', 'IM',
          'SUFRAMA', 'IND_PERFIL', 'IND_ATIV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq,
          REG, COD_VER, COD_FIN,
          DT_INI, DT_FIN, NOME,
          CNPJ, CPF, UF,
          IE, COD_MUN, IM,
          SUFRAMA, IND_PERFIL, IND_ATIV], [
          ], True);
        end else
        if REG = '0001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = '0005' then
        begin
          FANTASIA      := Lista[01];
          CEP           := Geral.IMV(Lista[02]);
          _END          := Lista[03];
          NUM           := Lista[04];
          COMPL         := Lista[05];
          BAIRRO        := Lista[06];
          FONE          := Lista[07];
          FAX           := Lista[08];
          EMAIL         := Lista[09];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0005', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'FANTASIA',
          'CEP', 'END', 'NUM',
          'COMPL', 'BAIRRO', 'FONE',
          'FAX', 'EMAIL'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, FANTASIA,
          CEP, _END, NUM,
          COMPL, BAIRRO, FONE,
          FAX, EMAIL], [
          ], True);
        end else
        if REG = '0100' then
        begin
          NOME          := Lista[01];
          CPF           := Lista[02];
          CRC           := Lista[03];
          CNPJ          := Lista[04];
          CEP           := Geral.IMV(Lista[05]);
          _END          := Lista[06];
          NUM           := Lista[07];
          COMPL         := Lista[08];
          BAIRRO        := Lista[09];
          FONE          := Lista[10];
          FAX           := Lista[11];
          EMAIL         := Lista[12];
          COD_MUN       := Geral.IMV(Lista[13]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0100', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'NOME',
          'CPF', 'CRC', 'CNPJ',
          'CEP', 'END', 'NUM',
          'COMPL', 'BAIRRO', 'FONE',
          'FAX', 'EMAIL', 'COD_MUN'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, NOME,
          CPF, CRC, CNPJ,
          CEP, _END, NUM,
          COMPL, BAIRRO, FONE,
          FAX, EMAIL, COD_MUN], [
          ], True);
        end else
        if REG = '0150' then
        begin
          COD_PART  := Lista[01];
          NOME      := Lista[02];
          COD_PAIS  := Geral.IMV(Lista[03]);
          CNPJ      := Lista[04];
          CPF       := Lista[05];
          IE        := Lista[06];
          COD_MUN   := Geral.IMV(Lista[07]);
          SUFRAMA   := Lista[08];
          _END      := Lista[09];
          NUM       := Lista[10];
          COMPL     := Lista[11];
          BAIRRO    := Lista[12];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0150', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'COD_PART',
          'NOME', 'COD_PAIS', 'CNPJ',
          'CPF', 'IE', 'COD_MUN',
          'SUFRAMA', 'END', 'NUM',
          'COMPL', 'BAIRRO'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, COD_PART,
          NOME, COD_PAIS, CNPJ,
          CPF, IE, COD_MUN,
          SUFRAMA, _END, NUM,
          COMPL, BAIRRO], [
          ], True);
        end else
        if REG = '0190' then
        begin
          UNID      := Lista[01];
          DESCR     := Lista[02];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0190', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'UNID',
          'DESCR'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, UNID,
          DESCR], [
          ], True);
        end else
        if REG = '0200' then
        begin
          COD_ITEM      := Lista[01];
          DESCR_ITEM    := Lista[02];
          COD_BARRA     := Lista[03];
          COD_ANT_ITEM  := Lista[04];
          UNID_INV      := Lista[05];
          TIPO_ITEM     := Lista[06];
          COD_NCM       := Lista[07];
          EX_IPI        := Lista[08];
          COD_GEN       := Lista[09];
          COD_LST       := Lista[10];
          ALIQ_ICMS     := Geral.DMV(Lista[11]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0200', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'COD_ITEM',
          'DESCR_ITEM', 'COD_BARRA', 'COD_ANT_ITEM',
          'UNID_INV', 'TIPO_ITEM', 'COD_NCM',
          'EX_IPI', 'COD_GEN', 'COD_LST',
          'ALIQ_ICMS'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, COD_ITEM,
          DESCR_ITEM, COD_BARRA, COD_ANT_ITEM,
          UNID_INV, TIPO_ITEM, COD_NCM,
          EX_IPI, COD_GEN, COD_LST,
          ALIQ_ICMS], [
          ], True);
        end else
        if REG = '0400' then
        begin
          COD_NAT       := Lista[01];
          DESCR_NAT     := Lista[02];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0400', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'COD_NAT',
          'DESCR_NAT'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, COD_NAT,
          DESCR_NAT], [
          ], True);
        end else
        if REG = '0990' then
        begin
          QTD_LIN_0     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd0990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_0'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_0], [
          ], True);
        end else
        if REG = 'C001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdC001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = 'C100' then
        begin
          C100           := I; // Usa no C170 e C190 filhos
          IND_OPER       := Lista[01];
          IND_EMIT       := Lista[02];
          COD_PART       := Lista[03];
          COD_MOD        := Lista[04];
          COD_SIT        := Lista[05];
          SER            := Lista[06];
          NUM_DOC        := Geral.IMV(Lista[07]);
          CHV_NFE        := Lista[08];
          DT_DOC         := Geral.FDT(Geral.ValidaDataBR(Lista[09], True, False), 1);
          DT_E_S         := Geral.FDT(Geral.ValidaDataBR(Lista[10], True, False), 1);
          VL_DOC         := Geral.DMV(Lista[11]);
          IND_PGTO       := Lista[12];
          VL_DESC        := Geral.DMV(Lista[13]);
          VL_ABAT_NT     := Geral.DMV(Lista[14]);
          VL_MERC        := Geral.DMV(Lista[15]);
          IND_FRT        := Lista[16];
          VL_FRT         := Geral.DMV(Lista[17]);
          VL_SEG         := Geral.DMV(Lista[18]);
          VL_OUT_DA      := Geral.DMV(Lista[19]);
          VL_BC_ICMS     := Geral.DMV(Lista[20]);
          VL_ICMS        := Geral.DMV(Lista[21]);
          VL_BC_ICMS_ST  := Geral.DMV(Lista[22]);
          VL_ICMS_ST     := Geral.DMV(Lista[23]);
          VL_IPI         := Geral.DMV(Lista[24]);
          VL_PIS         := Geral.DMV(Lista[25]);
          VL_COFINS      := Geral.DMV(Lista[26]);
          VL_PIS_ST      := Geral.DMV(Lista[27]);
          VL_COFINS_ST   := Geral.DMV(Lista[28]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdC100', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_OPER',
          'IND_EMIT', 'COD_PART', 'COD_MOD',
          'COD_SIT', 'SER', 'NUM_DOC',
          'CHV_NFE', 'DT_DOC', 'DT_E_S',
          'VL_DOC', 'IND_PGTO', 'VL_DESC',
          'VL_ABAT_NT', 'VL_MERC', 'IND_FRT',
          'VL_FRT', 'VL_SEG', 'VL_OUT_DA',
          'VL_BC_ICMS', 'VL_ICMS', 'VL_BC_ICMS_ST',
          'VL_ICMS_ST', 'VL_IPI', 'VL_PIS',
          'VL_COFINS', 'VL_PIS_ST', 'VL_COFINS_ST'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_OPER,
          IND_EMIT, COD_PART, COD_MOD,
          COD_SIT, SER, NUM_DOC,
          CHV_NFE, DT_DOC, DT_E_S,
          VL_DOC, IND_PGTO, VL_DESC,
          VL_ABAT_NT, VL_MERC, IND_FRT,
          VL_FRT, VL_SEG, VL_OUT_DA,
          VL_BC_ICMS, VL_ICMS, VL_BC_ICMS_ST,
          VL_ICMS_ST, VL_IPI, VL_PIS,
          VL_COFINS, VL_PIS_ST, VL_COFINS_ST], [
          ], True);
        end else
        if REG = 'C170' then
        begin
          NUM_ITEM        := Geral.IMV(Lista[01]);
          COD_ITEM        := Lista[02];
          DESCR_COMPL     := Lista[03];
          QTD             := Geral.DMV(Lista[04]);
          UNID            := Lista[05];
          VL_ITEM         := Geral.DMV(Lista[06]);
          VL_DESC         := Geral.DMV(Lista[07]);
          IND_MOV         := Lista[08];
          CST_ICMS        := Geral.IMV(Lista[09]);
          CFOP            := Geral.IMV(Lista[10]);
          COD_NAT         := Lista[11];
          VL_BC_ICMS      := Geral.DMV(Lista[12]);
          ALIQ_ICMS       := Geral.DMV(Lista[13]);
          VL_ICMS         := Geral.DMV(Lista[14]);
          VL_BC_ICMS_ST   := Geral.DMV(Lista[15]);
          ALIQ_ST         := Geral.DMV(Lista[16]);
          VL_ICMS_ST      := Geral.DMV(Lista[17]);
          IND_APUR        := Lista[18];
          CST_IPI         := Lista[19];
          COD_ENQ         := Lista[20];
          VL_BC_IPI       := Geral.DMV(Lista[21]);
          ALIQ_IPI        := Geral.DMV(Lista[22]);
          VL_IPI          := Geral.DMV(Lista[23]);
          CST_PIS         := Geral.DMV(Lista[24]);
          VL_BC_PIS       := Geral.DMV(Lista[25]);
          ALIQ_PIS_p      := Geral.DMV(Lista[26]);
          QUANT_BC_PIS    := Geral.DMV(Lista[27]);
          ALIQ_PIS_r      := Geral.DMV(Lista[28]);
          VL_PIS          := Geral.DMV(Lista[29]);
          CST_COFINS      := Geral.DMV(Lista[30]);
          VL_BC_COFINS    := Geral.DMV(Lista[31]);
          ALIQ_COFINS_p   := Geral.DMV(Lista[32]);
          QUANT_BC_COFINS := Geral.DMV(Lista[33]);
          ALIQ_COFINS_r   := Geral.DMV(Lista[34]);
          VL_COFINS       := Geral.DMV(Lista[35]);
          COD_CTA         := Lista[36];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdC170', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'C100', 'REG',
          'NUM_ITEM', 'COD_ITEM', 'DESCR_COMPL',
          'QTD', 'UNID', 'VL_ITEM',
          'VL_DESC', 'IND_MOV', 'CST_ICMS',
          'CFOP', 'COD_NAT', 'VL_BC_ICMS',
          'ALIQ_ICMS', 'VL_ICMS', 'VL_BC_ICMS_ST',
          'ALIQ_ST', 'VL_ICMS_ST', 'IND_APUR',
          'CST_IPI', 'COD_ENQ', 'VL_BC_IPI',
          'ALIQ_IPI', 'VL_IPI', 'CST_PIS',
          'VL_BC_PIS', 'ALIQ_PIS_p', 'QUANT_BC_PIS',
          'ALIQ_PIS_r', 'VL_PIS', 'CST_COFINS',
          'VL_BC_COFINS', 'ALIQ_COFINS_p', 'QUANT_BC_COFINS',
          'ALIQ_COFINS_r', 'VL_COFINS', 'COD_CTA'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, C100, REG,
          NUM_ITEM, COD_ITEM, DESCR_COMPL,
          QTD, UNID, VL_ITEM,
          VL_DESC, IND_MOV, CST_ICMS,
          CFOP, COD_NAT, VL_BC_ICMS,
          ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST,
          ALIQ_ST, VL_ICMS_ST, IND_APUR,
          CST_IPI, COD_ENQ, VL_BC_IPI,
          ALIQ_IPI, VL_IPI, CST_PIS,
          VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
          ALIQ_PIS_r, VL_PIS, CST_COFINS,
          VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS,
          ALIQ_COFINS_r, VL_COFINS, COD_CTA], [
          ], True);
        end else
        if REG = 'C190' then
        begin
          CST_ICMS        := Geral.IMV(Lista[01]);
          CFOP            := Geral.IMV(Lista[02]);
          ALIQ_ICMS       := Geral.DMV(Lista[03]);
          VL_OPR          := Geral.DMV(Lista[04]);
          VL_BC_ICMS      := Geral.DMV(Lista[05]);
          VL_ICMS         := Geral.DMV(Lista[06]);
          VL_BC_ICMS_ST   := Geral.DMV(Lista[07]);
          VL_ICMS_ST      := Geral.DMV(Lista[08]);
          VL_RED_BC       := Geral.DMV(Lista[09]);
          VL_IPI          := Geral.DMV(Lista[10]);
          COD_OBS         := Lista[11];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdC190', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'C100', 'REG',
          'CST_ICMS', 'CFOP', 'ALIQ_ICMS',
          'VL_OPR', 'VL_BC_ICMS', 'VL_ICMS',
          'VL_BC_ICMS_ST', 'VL_ICMS_ST', 'VL_RED_BC',
          'VL_IPI', 'COD_OBS'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, C100, REG,
          CST_ICMS, CFOP, ALIQ_ICMS,
          VL_OPR, VL_BC_ICMS, VL_ICMS,
          VL_BC_ICMS_ST, VL_ICMS_ST, VL_RED_BC,
          VL_IPI, COD_OBS], [
          ], True);
        end else
        if REG = 'C500' then
        begin
          C500             := I;
          IND_OPER         := Lista[01];
          IND_EMIT         := Lista[02];
          COD_PART         := Lista[03];
          COD_MOD          := Lista[04];
          COD_SIT          := Lista[05];
          SER              := Lista[06];
          SUB              := Lista[07];
          COD_CONS         := Lista[08];
          NUM_DOC          := Geral.IMV(Lista[09]);
          DT_DOC           := Geral.FDT(Geral.ValidaDataBR(Lista[10], True, False), 1);
          DT_E_S           := Geral.FDT(Geral.ValidaDataBR(Lista[11], True, False), 1);
          VL_DOC           := Geral.DMV(Lista[12]);
          VL_DESC          := Geral.DMV(Lista[13]);
          VL_FORN          := Geral.DMV(Lista[14]);
          VL_SERV_NT       := Geral.DMV(Lista[15]);
          VL_TERC          := Geral.DMV(Lista[16]);
          VL_DA            := Geral.DMV(Lista[17]);
          VL_BC_ICMS       := Geral.DMV(Lista[18]);
          VL_ICMS          := Geral.DMV(Lista[19]);
          VL_BC_ICMS_ST    := Geral.DMV(Lista[20]);
          VL_ICMS_ST       := Geral.DMV(Lista[21]);
          COD_INF          := Lista[22];
          VL_PIS           := Geral.DMV(Lista[23]);
          VL_COFINS        := Geral.DMV(Lista[24]);
          TP_LIGACAO       := Lista[25];
          COD_GRUPO_TENSAO := Lista[26];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdC500', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_OPER',
          'IND_EMIT', 'COD_PART', 'COD_MOD',
          'COD_SIT', 'SER', 'SUB',
          'COD_CONS', 'NUM_DOC', 'DT_DOC',
          'DT_E_S', 'VL_DOC', 'VL_DESC',
          'VL_FORN', 'VL_SERV_NT', 'VL_TERC',
          'VL_DA', 'VL_BC_ICMS', 'VL_ICMS',
          'VL_BC_ICMS_ST', 'VL_ICMS_ST', 'COD_INF',
          'VL_PIS', 'VL_COFINS', 'TP_LIGACAO',
          'COD_GRUPO_TENSAO'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_OPER,
          IND_EMIT, COD_PART, COD_MOD,
          COD_SIT, SER, SUB,
          COD_CONS, NUM_DOC, DT_DOC,
          DT_E_S, VL_DOC, VL_DESC,
          VL_FORN, VL_SERV_NT, VL_TERC,
          VL_DA, VL_BC_ICMS, VL_ICMS,
          VL_BC_ICMS_ST, VL_ICMS_ST, COD_INF,
          VL_PIS, VL_COFINS, TP_LIGACAO,
          COD_GRUPO_TENSAO], [
          ], True);
        end else
        if REG = 'C590' then
        begin
          CST_ICMS         := Geral.IMV(Lista[01]);
          CFOP             := Geral.IMV(Lista[02]);
          ALIQ_ICMS        := Geral.IMV(Lista[03]);
          VL_OPR           := Geral.DMV(Lista[04]);
          VL_BC_ICMS       := Geral.DMV(Lista[05]);
          VL_ICMS          := Geral.DMV(Lista[06]);
          VL_BC_ICMS_ST    := Geral.DMV(Lista[07]);
          VL_ICMS_ST       := Geral.DMV(Lista[08]);
          VL_RED_BC        := Geral.DMV(Lista[09]);
          COD_OBS          := Lista[10];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdC590', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'C500', 'REG',
          'CST_ICMS', 'CFOP', 'ALIQ_ICMS',
          'VL_OPR', 'VL_BC_ICMS', 'VL_ICMS',
          'VL_BC_ICMS_ST', 'VL_ICMS_ST', 'VL_RED_BC',
          'COD_OBS'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, C500, REG,
          CST_ICMS, CFOP, ALIQ_ICMS,
          VL_OPR, VL_BC_ICMS, VL_ICMS,
          VL_BC_ICMS_ST, VL_ICMS_ST, VL_RED_BC,
          COD_OBS], [
          ], True);
        end else
        if REG = 'C990' then
        begin
          QTD_LIN_C     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdC990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_C'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_C], [
          ], True);
        end else
        if REG = 'D001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdD001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = 'D100' then
        begin
          D100          := I;
          IND_OPER      := Lista[01];
          IND_EMIT      := Lista[02];
          COD_PART      := Lista[03];
          COD_MOD       := Lista[04];
          COD_SIT       := Lista[05];
          SER           := Lista[06];
          SUB_C         := Lista[07];
          NUM_DOC       := Geral.IMV(Lista[08]);
          CHV_CTE       := Lista[09];
          DT_DOC        := Geral.FDT(Geral.ValidaDataBR(Lista[10], True, False), 1);
          DT_A_P        := Geral.FDT(Geral.ValidaDataBR(Lista[11], True, False), 1);
          TP_CTE        := Geral.IMV(Lista[12]);
          CHV_CTE_REF   := Lista[13];
          VL_DOC        := Geral.DMV(Lista[14]);
          VL_DESC       := Geral.DMV(Lista[15]);
          IND_FRT       := Lista[16];
          VL_SERV       := Geral.DMV(Lista[17]);
          VL_BC_ICMS    := Geral.DMV(Lista[18]);
          VL_ICMS       := Geral.DMV(Lista[19]);
          VL_NT         := Geral.DMV(Lista[20]);
          COD_INF       := Lista[21];
          COD_CTA       := Lista[22];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdD100', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_OPER',
          'IND_EMIT', 'COD_PART', 'COD_MOD',
          'COD_SIT', 'SER', 'SUB',
          'NUM_DOC', 'CHV_CTE', 'DT_DOC',
          'DT_A_P', 'TP_CTE', 'CHV_CTE_REF',
          'VL_DOC', 'VL_DESC', 'IND_FRT',
          'VL_SERV', 'VL_BC_ICMS', 'VL_ICMS',
          'VL_NT', 'COD_INF', 'COD_CTA'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_OPER,
          IND_EMIT, COD_PART, COD_MOD,
          COD_SIT, SER, SUB_C,
          NUM_DOC, CHV_CTE, DT_DOC,
          DT_A_P, TP_CTE, CHV_CTE_REF,
          VL_DOC, VL_DESC, IND_FRT,
          VL_SERV, VL_BC_ICMS, VL_ICMS,
          VL_NT, COD_INF, COD_CTA], [
          ], True);
        end else
        if REG = 'D190' then
        begin
          CST_ICMS      := Geral.IMV(Lista[01]);
          CFOP          := Geral.IMV(Lista[02]);
          ALIQ_ICMS     := Geral.DMV(Lista[03]);
          VL_OPR        := Geral.DMV(Lista[04]);
          VL_BC_ICMS    := Geral.DMV(Lista[05]);
          VL_ICMS       := Geral.DMV(Lista[06]);
          VL_RED_BC     := Geral.DMV(Lista[07]);
          COD_OBS       := Lista[08];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdD190', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'D100', 'REG',
          'CST_ICMS', 'CFOP', 'ALIQ_ICMS',
          'VL_OPR', 'VL_BC_ICMS', 'VL_ICMS',
          'VL_RED_BC', 'COD_OBS'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, D100, REG,
          CST_ICMS, CFOP, ALIQ_ICMS,
          VL_OPR, VL_BC_ICMS, VL_ICMS,
          VL_RED_BC, COD_OBS], [
          ], True);
        end else
        if REG = 'D500' then
        begin
          D500          := I;
          IND_OPER      := Lista[01];
          IND_EMIT      := Lista[02];
          COD_PART      := Lista[03];
          COD_MOD       := Lista[04];
          COD_SIT       := Lista[05];
          SER           := Lista[06];
          SUB           := Lista[07];
          NUM_DOC       := Geral.IMV(Lista[08]);
          DT_DOC        := Geral.FDT(Geral.ValidaDataBR(Lista[09], True, False), 1);
          DT_A_P        := Geral.FDT(Geral.ValidaDataBR(Lista[10], True, False), 1);
          VL_DOC        := Geral.DMV(Lista[11]);
          VL_DESC       := Geral.DMV(Lista[12]);
          VL_SERV       := Geral.DMV(Lista[13]);
          VL_SERV_NT    := Geral.DMV(Lista[14]);
          VL_TERC       := Geral.DMV(Lista[15]);
          VL_DA         := Geral.DMV(Lista[16]);
          VL_BC_ICMS    := Geral.DMV(Lista[17]);
          VL_ICMS       := Geral.DMV(Lista[18]);
          COD_INF       := Lista[19];
          VL_PIS        := Geral.DMV(Lista[20]);
          VL_COFINS     := Geral.DMV(Lista[21]);
          COD_CTA       := Lista[22];
          TP_ASSINANTE  := Geral.IMV(Lista[23]);
          //

          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdD500', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_OPER',
          'IND_EMIT', 'COD_PART', 'COD_MOD',
          'COD_SIT', 'SER', 'SUB',
          'NUM_DOC', 'DT_DOC', 'DT_A_P',
          'VL_DOC', 'VL_DESC', 'VL_SERV',
          'VL_SERV_NT', 'VL_TERC', 'VL_DA',
          'VL_BC_ICMS', 'VL_ICMS', 'COD_INF',
          'VL_PIS', 'VL_COFINS', 'COD_CTA',
          'TP_ASSINANTE'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_OPER,
          IND_EMIT, COD_PART, COD_MOD,
          COD_SIT, SER, SUB,
          NUM_DOC, DT_DOC, DT_A_P,
          VL_DOC, VL_DESC, VL_SERV,
          VL_SERV_NT, VL_TERC, VL_DA,
          VL_BC_ICMS, VL_ICMS, COD_INF,
          VL_PIS, VL_COFINS, COD_CTA,
          TP_ASSINANTE], [
          ], True);
        end else
        if REG = 'D590' then
        begin
          CST_ICMS      := Geral.IMV(Lista[01]);
          CFOP          := Geral.IMV(Lista[02]);
          ALIQ_ICMS     := Geral.DMV(Lista[03]);
          VL_OPR        := Geral.DMV(Lista[04]);
          VL_BC_ICMS    := Geral.DMV(Lista[05]);
          VL_ICMS       := Geral.DMV(Lista[06]);
          VL_BC_ICMS_ST := Geral.DMV(Lista[07]);
          VL_ICMS_ST    := Geral.DMV(Lista[08]);
          VL_RED_BC     := Geral.DMV(Lista[09]);
          COD_OBS       := Lista[10];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdD590', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'D500', 'REG',
          'CST_ICMS', 'CFOP', 'ALIQ_ICMS',
          'VL_OPR', 'VL_BC_ICMS', 'VL_ICMS',
          'VL_BC_ICMS_ST', 'VL_ICMS_ST', 'VL_RED_BC',
          'COD_OBS'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, D500, REG,
          CST_ICMS, CFOP, ALIQ_ICMS,
          VL_OPR, VL_BC_ICMS, VL_ICMS,
          VL_BC_ICMS_ST, VL_ICMS_ST, VL_RED_BC,
          COD_OBS], [
          ], True);
        end else
        if REG = 'D990' then
        begin
          QTD_LIN_D     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdD990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_D'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_D], [
          ], True);
        end else
        if REG = 'E001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdE001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = 'E100' then
        begin
          E100          := I;
          DT_INI        := Geral.FDT(Geral.ValidaDataBR(Lista[01], True, False), 1);
          DT_FIN        := Geral.FDT(Geral.ValidaDataBR(Lista[02], True, False), 1);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdE100', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'DT_INI',
          'DT_FIN'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, DT_INI,
          DT_FIN], [
          ], True);
        end else
        if REG = 'E110' then
        begin
          E110 := I;
          VL_TOT_DEBITOS     := Geral.DMV(Lista[01]);
          VL_AJ_DEBITOS      := Geral.DMV(Lista[02]);
          VL_TOT_AJ_DEBITOS  := Geral.DMV(Lista[03]);
          VL_ESTORNOS_CRED   := Geral.DMV(Lista[04]);
          VL_TOT_CREDITOS    := Geral.DMV(Lista[05]);
          VL_AJ_CREDITOS     := Geral.DMV(Lista[06]);
          VL_TOT_AJ_CREDITOS := Geral.DMV(Lista[07]);
          VL_ESTORNOS_DEB    := Geral.DMV(Lista[08]);
          VL_SLD_CREDOR_ANT  := Geral.DMV(Lista[09]);
          VL_SLD_APURADO     := Geral.DMV(Lista[10]);
          VL_TOT_DED         := Geral.DMV(Lista[11]);
          VL_ICMS_RECOLHER   := Geral.DMV(Lista[12]);
          VL_SLD_CREDOR_TRANSPORTAR := Geral.DMV(Lista[13]);
          DEB_ESP            := Geral.DMV(Lista[14]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdE110', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'E100', 'REG',
          'VL_TOT_DEBITOS', 'VL_AJ_DEBITOS', 'VL_TOT_AJ_DEBITOS',
          'VL_ESTORNOS_CRED', 'VL_TOT_CREDITOS', 'VL_AJ_CREDITOS',
          'VL_TOT_AJ_CREDITOS', 'VL_ESTORNOS_DEB', 'VL_SLD_CREDOR_ANT',
          'VL_SLD_APURADO', 'VL_TOT_DED', 'VL_ICMS_RECOLHER',
          'VL_SLD_CREDOR_TRANSPORTAR', 'DEB_ESP'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, E100, REG,
          VL_TOT_DEBITOS, VL_AJ_DEBITOS, VL_TOT_AJ_DEBITOS,
          VL_ESTORNOS_CRED, VL_TOT_CREDITOS, VL_AJ_CREDITOS,
          VL_TOT_AJ_CREDITOS, VL_ESTORNOS_DEB, VL_SLD_CREDOR_ANT,
          VL_SLD_APURADO, VL_TOT_DED, VL_ICMS_RECOLHER,
          VL_SLD_CREDOR_TRANSPORTAR, DEB_ESP], [
          ], True);
        end else
        if REG = 'E111' then
        begin
          COD_AJ_APUR        := Lista[01];
          DESCR_COMPL_AJ     := Lista[02];
          VL_AJ_APUR         := Geral.DMV(Lista[03]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdE111', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'E110', 'REG',
          'COD_AJ_APUR', 'DESCR_COMPL_AJ', 'VL_AJ_APUR'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, E110, REG,
          COD_AJ_APUR, DESCR_COMPL_AJ, VL_AJ_APUR], [
          ], True);
        end else
        if REG = 'E116' then
        begin
          COD_OR             := Lista[01];
          VL_OR              := Geral.DMV(Lista[02]);
          DT_VCTO            := Geral.FDT(Geral.ValidaDataBR(Lista[03], True, False), 1);
          COD_REC            := Lista[04];
          NUM_PROC           := Lista[05];
          IND_PROC           := Lista[06];
          PROC               := Lista[07];
          TXT_COMPL          := Lista[08];
          MES_REF            := Lista[09];
          if MES_REF = '' then
            MES_REF := Geral.FDT(Geral.ValidaDataBR('00000000', True, False), 1)
          else
            MES_REF := Geral.FDT(Geral.ValidaDataBR('01' + MES_REF, False, False), 1);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdE116', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq',
          'E110', 'REG', 'COD_OR',
          'VL_OR', 'DT_VCTO', 'COD_REC',
          'NUM_PROC', 'IND_PROC', 'PROC',
          'TXT_COMPL', 'MES_REF'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq,
          E110, REG, COD_OR,
          VL_OR, DT_VCTO, COD_REC,
          NUM_PROC, IND_PROC, PROC,
          TXT_COMPL, MES_REF], [
          ], True);
        end else
        if REG = 'E990' then
        begin
          QTD_LIN_E     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdE990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_E'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_E], [
          ], True);
        end else
        if REG = 'G001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdG001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = 'G990' then
        begin
          QTD_LIN_G     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdG990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_G'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_G], [
          ], True);
        end else
        if REG = 'H001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdH001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = 'H005' then
        begin
          DT_INV        := Geral.FDT(Geral.ValidaDataBR(Lista[01], True, False), 1);
          VL_INV        := Geral.DMV(Lista[02]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdH005', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'DT_INV',
          'VL_INV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, DT_INV,
          VL_INV], [
          ], True);
        end else
        if REG = 'H010' then
        begin
          COD_ITEM      := Lista[01];
          UNID          := Lista[02];
          QTD           := Geral.DMV(Lista[03]);
          VL_UNIT       := Geral.DMV(Lista[04]);
          VL_ITEM       := Geral.DMV(Lista[05]);
          IND_PROP      := Lista[06];
          COD_PART      := Lista[07];
          TXT_COMPL     := Lista[08];
          COD_CTA       := Lista[09];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdH010', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'COD_ITEM',
          'UNID', 'QTD', 'VL_UNIT',
          'VL_ITEM', 'IND_PROP', 'COD_PART',
          'TXT_COMPL', 'COD_CTA'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, COD_ITEM,
          UNID, QTD, VL_UNIT,
          VL_ITEM, IND_PROP, COD_PART,
          TXT_COMPL, COD_CTA], [
          ], True);
        end else
        if REG = 'H990' then
        begin
          QTD_LIN_H     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdH990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_H'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_H], [
          ], True);
        end else
        if REG = '1001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd1001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = '1990' then
        begin
          QTD_LIN_1     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd1990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_1'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_1], [
          ], True);
        end else
        if REG = '9001' then
        begin
          IND_MOV       := Lista[01];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd9001', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'IND_MOV'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, IND_MOV], [
          ], True);
        end else
        if REG = '9900' then
        begin
          REG_BLC       := Lista[01];
          QTD_REG_BLC   := Geral.IMV(Lista[02]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd9900', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'REG_BLC',
          'QTD_REG_BLC'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, REG_BLC,
          QTD_REG_BLC], [
          ], True);
        end else
        if REG = '9990' then
        begin
          QTD_LIN_9     := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd9990', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN_9'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN_9], [
          ], True);
        end else
        if REG = '9999' then
        begin
          QTD_LIN       := Geral.IMV(Lista[01]);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefd9999', False, [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'QTD_LIN'], [
          ], [
          FImporExpor, FAnoMes, Empresa, LinArq, REG, QTD_LIN], [
          ], True);
        end else
        begin
          MeIgnor.Lines.Add('Linha: ' + FormatFloat('0000', I) + ' Registro: ' + REG);
        end;
      end else Geral.MensagemBox('A linha ' + IntToStr(I+1)  +
      ' n�o possui texto algum!', 'Aviso', MB_OK+MB_ICONWARNING);
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregamento finalizado! Abrindo tabelas.');
    //
    // O "0" n�o porque QrErr usa na exporta��o e n�o na importa��o!
    for I := 1 to High(FTbs_SPED_EFD_II) (*- 1*) do
    begin
      MySQLQuery := nil;
      NomeTab := 'Qr' + Copy(FTbs_SPED_EFD_II[I], 8, 4);
      if (FindComponent(NomeTab) as TmySQLQuery) <> nil then
        UMyMod.ReabreQuery(FindComponent(NomeTab) as TmySQLQuery, Dmod.MyDB, [FImporExpor, FAnoMes, Empresa], Tit)
      else begin
        Geral.MensagemBox('N�o foi poss�vel lozalizar a Query "' + NomeTab +
        '". Avise a DERMATEK', 'ERRO', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando integridade do arquivo.');
    Qr9900.First;
    while not Qr9900.Eof do
    begin
      NomeTab := 'Qr' + Qr9900REG_BLC.Value;
      if (FindComponent(NomeTab) as TmySQLQuery) <> nil then
        MySQLQuery := FindComponent(NomeTab) as TmySQLQuery
      else begin
        MySQLQuery := nil;
        Geral.MensagemBox('N�o foi poss�vel lozalizar a Query "' + NomeTab +
        '". Avise a DERMATEK', 'ERRO', MB_OK+MB_ICONWARNING);
      end;
      if MySQLQuery <> nil then
      begin
        if MySQLQuery.RecordCount <> Qr9900QTD_REG_BLC.Value then
        begin
          Geral.MensagemBox('A quantidade de linhas informadas para o registro "'
          + Qr9900REG_BLC.Value + '" n�o confere com o informado no arquivo!',
          'Aviso', MB_OK+MB_ICONWARNING);
          Exit;
        end;
      end;
      //
      Qr9900.Next;
    end;
    //
    FCarregou := True;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando validade do arquivo.');
    //
    QrVersao.Close;
    QrVersao.Params[00].AsInteger := COD_VER;
    UnDmkDAC_PF.AbreQuery(QrVersao, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    if QrVersao.RecordCount > 0 then
    begin
      if (DataFim < QrVersaoDT_INI.Value) or (
      (DataIni > QrVersaoDT_FIN.Value) and
      (QrVersaoDT_FIN.Value > 0)) then
        Geral.MensagemBox('AVISO!' + sLineBreak +
        'A vers�o informada no arquivo (vers�o = ' + IntToStr(COD_VER) +
        ') n�o condiz com o per�odo informado no arquivo!' + sLineBreak +
        'Informe o respons�vel pelo aplicativo gerador do arquivo!',
        'Aviso', MB_OK+MB_ICONINFORMATION);
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando registros ignorados.');
    if MeIgnor.Lines.Count > 0 then
    begin
      Geral.MensagemBox('Carregamento cancelado!' + sLineBreak +
      'Existem ' + FormatFloat('0', MeIgnor.Lines.Count) +
      ' linhas no arquivo sem implementa��o no aplicativo!' + sLineBreak +
      'Informe a DERMATEK!', 'Aviso', MB_OK+MB_ICONWARNING);
      //
      Exit;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando cadastros das entidades.');
    PB1.Position := 0;
    PB1.Max := Qr0150.RecordCount;
    Qr0150.First;
    while not Qr0150.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando cadastros das entidades. ' +
      Qr0150CNPJ.Value + Qr0150CPF.Value + ' (' + Qr0150COD_PART.Value + ')');
      //
      Entidade := 0;
      if Qr0150CNPJ.Value <> '' then
        DModG.ObtemEntidadeDeCNPJCFP(Qr0150CNPJ.Value, Entidade)
      else
      if Qr0150CPF.Value <> '' then
        DModG.ObtemEntidadeDeCNPJCFP(Qr0150CPF.Value, Entidade)
      else
      if Qr0150COD_PART.Value <> '' then
        DModG.ObtemEntidadeDeCOD_PART(Qr0150COD_PART.Value, Entidade);
      //
      if Entidade <> 0 then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'spedefd0150', False, [
        'Entidade'], [
        'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
        Entidade], [
        Qr0150ImporExpor.Value, Qr0150AnoMes.Value,
        Qr0150Empresa.Value, Qr0150LinArq.Value], False);
      end;
      //
      Qr0150.Next;
    end;
    UMyMod.ReabreQuery(QrErr0150, Dmod.MyDB, [FImporExpor, FAnoMes, Empresa], Tit);
    if QrErr0150.RecordCount > 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Atrele as entidades caso existam, ou cadastre-as antes de continuar!');
      PageControl1.ActivePageIndex := 2;
      PageControl2.ActivePageIndex := 0;
      //
      Geral.MensagemBox('Carregamento cancelado!' + sLineBreak +
      'Existem ' + FormatFloat('0', QrErr0150.RecordCount) +
      ' entidades sem cadastro no aplicativo!' + sLineBreak +
      'Atrele as entidades caso existam, ou cadastre antes de continuar!',
      'Aviso', MB_OK+MB_ICONWARNING);
      //
      Exit;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando cadastros das unidades de medida.');
    PB1.Position := 0;
    PB1.Max := Qr0190.RecordCount;
    Qr0190.First;
    while not Qr0190.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
       'Verificando cadastros das unidades de medida. ' + Qr0190UNID.Value);
      //
      UnidMed := 0;
      if Qr0190UNID.Value <> '' then
      begin
        QrLocUM.Close;
        QrLocUM.Params[0].AsString := Qr0190UNID.Value;
        UnDmkDAC_PF.AbreQuery(QrLocUM, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
        //
        if QrLocUM.RecordCount > 0 then
          UnidMed := QrLocUMCodigo.Value;
      end;
      //
      if UnidMed <> 0 then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'spedefd0190', False, [
        'UnidMed'], [
        'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
        UnidMed], [
        Qr0190ImporExpor.Value, Qr0190AnoMes.Value,
        Qr0190Empresa.Value, Qr0190LinArq.Value], False);
      end;
      //
      Qr0190.Next;
    end;
    UMyMod.ReabreQuery(QrErr0190, Dmod.MyDB, [FImporExpor, FAnoMes, Empresa], Tit);
    if QrErr0190.RecordCount > 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Atrele as unidades de medida caso existam, ou cadastre-as antes de continuar!');
      PageControl1.ActivePageIndex := 2;
      PageControl2.ActivePageIndex := 1;
      //
      Geral.MensagemBox('Carregamento cancelado!' + sLineBreak +
      'Existem ' + FormatFloat('0', QrErr0190.RecordCount) +
      ' unidades de medida sem cadastro no aplicativo!' + sLineBreak +
      'Atrele as unidades de medida caso existam, ou cadastre antes de continuar!',
      'Aviso', MB_OK+MB_ICONWARNING);
      //
      Exit;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando cadastros dos produtos.');
    PB1.Position := 0;
    PB1.Max := Qr0200.RecordCount;
    Qr0200.First;
    while not Qr0200.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando cadastros dos produtos. ' +
      Qr0200COD_ITEM.Value + ' : ' + Qr0200DESCR_ITEM.Value);
      //
      GraGruX := 0;
      if Qr0200COD_ITEM.Value <> '' then
      begin
        QrLocGGX.Close;
        QrLocGGX.SQL.Clear;
        QrLocGGX.SQL.Add('SELECT ggx.Controle');
        QrLocGGX.SQL.Add('FROM gragrux ggx');
        QrLocGGX.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
        QrLocGGX.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
        QrLocGGX.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
        QrLocGGX.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
        QrLocGGX.SQL.Add('WHERE ggx.Controle=' + Qr0200COD_ITEM.Value);
        if CkNomeGGXIgual.Checked then
        begin
          // Usar Params[]  para evitar erro de '"'
          QrLocGGX.SQL.Add('AND gg1.Nome=:P0');
          QrLocGGX.Params[00].AsString := Qr0200DESCR_ITEM.Value;
        end;
        UnDmkDAC_PF.AbreQuery(QrLocGGX, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
        //
        if QrLocGGX.RecordCount > 0 then
        begin
          GraGruX  := QrLocGGXControle.Value;
          NivelExclu := 0;
        end else
          DmProd.ObtemNovoGraGruXdeExcluido(
            Geral.IMV(Geral.SoNumero_TT(Qr0200COD_ITEM.Value)),
            GraGruX, NivelExclu);
      end;
      //
      if GraGruX <> 0 then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'spedefd0200', False, [
        'GraGruX', 'NivelExclu'], [
        'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
        GraGruX, NivelExclu], [
        Qr0200ImporExpor.Value, Qr0200AnoMes.Value,
        Qr0200Empresa.Value, Qr0200LinArq.Value], False);
      end;
      //
      Qr0200.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Comparando cadastro de produtos.');
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO _spedinn_0200_');
    DModG.QrUpdPID1.SQL.Add('SELECT LinArq, 0 Distancia, GraGruX,');
    DModG.QrUpdPID1.SQL.Add('"" Nome, COD_ITEM, DESCR_ITEM , COD_ANT_ITEM, ');
    DModG.QrUpdPID1.SQL.Add('"" Levenshtein_Meu, "" Levenshtein_Inn, NivelExclu');
    DModG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.spedefd0200');
    DModG.QrUpdPID1.SQL.Add('WHERE ImporExpor=:P0');
    DModG.QrUpdPID1.SQL.Add('AND AnoMes=:P1');
    DModG.QrUpdPID1.SQL.Add('AND Empresa=:P2;');
    DModG.QrUpdPID1.SQL.Add('');
    DModG.QrUpdPID1.Params[00].AsInteger := FImporExpor;
    DModG.QrUpdPID1.Params[01].AsInteger := FAnoMes;
    DModG.QrUpdPID1.Params[02].AsInteger := Empresa;
    DModG.QrUpdPID1.ExecSQL;
    //
    QrINN_0200.Close;
    UnDmkDAC_PF.AbreQuery(QrINN_0200, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    PB1.Position := 0;
    PB1.Max := QrINN_0200.RecordCount;
    while not QrINN_0200.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Comparando cadastro do produto: ' +
        QrINN_0200COD_ITEM.Value);
      QrGG1.Close;
      QrGG1.Params[0].AsInteger := QrINN_0200GraGruX.Value;
      UnDmkDAC_PF.AbreQuery(QrGG1, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
      Levenshtein_Meu :=
        Copy(MLAGeral.SimplificaPalavras(QrGG1Nome.Value), 1, 120);
      Levenshtein_Inn :=
        Copy(MLAGeral.SimplificaPalavras(QrINN_0200DESCR_ITEM.Value), 1, 120);
      Distancia := MLAGeral.EditDistance_Levenshtein(
        Levenshtein_Meu, Levenshtein_Inn);
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_spedinn_0200_', False, [
      'Nome', 'Distancia', 'Levenshtein_Meu', 'Levenshtein_Inn'
      ], ['LinArq'], [
      QrGG1Nome.Value, Distancia, Levenshtein_Meu, Levenshtein_Inn
      ], [QrINN_0200LinArq.Value], False);
      //
      QrINN_0200.Next;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando exist�ncia de cadastro de produtos.');
    UMyMod.ReabreQuery(QrErr0200, Dmod.MyDB, [FImporExpor, FAnoMes, Empresa], Tit);
    if QrErr0200.RecordCount > 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Atrele os produtos caso existam, ou cadastre-os antes de continuar!');
      PageControl1.ActivePageIndex := 2;
      PageControl2.ActivePageIndex := 2;
      //
      Geral.MensagemBox('Carregamento cancelado!' + sLineBreak +
      'Existem ' + FormatFloat('0', QrErr0200.RecordCount) +
      ' produtos sem cadastro no aplicativo!' + sLineBreak +
      'Atrele os produtos caso existam, ou cadastre antes de continuar!',
      'Aviso', MB_OK+MB_ICONWARNING);
      //
      Exit;
    end;
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando itens de entrada (C170).');
    PB1.Position := 0;
    PB1.Max := QrC170.RecordCount;
    QrC170.First;
    while not QrC170.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando itens de entrada (C170). ' +
        QrC170COD_ITEM.Value);
      //
      GraGruX := 0;
      if QrC170COD_ITEM.Value <> '' then
      begin
(*
        QrLocGGX.Close;
        QrLocGGX.SQL.Clear;
        QrLocGGX.SQL.Add('SELECT ggx.Controle');
        QrLocGGX.SQL.Add('FROM gragrux ggx');
        QrLocGGX.SQL.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
        QrLocGGX.SQL.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
        QrLocGGX.SQL.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
        QrLocGGX.SQL.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
        QrLocGGX.SQL.Add('WHERE ggx.Controle=' + QrC170COD_ITEM.Value);
        UnDmkDAC_PF.AbreQuery(QrLocGGX, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
        //
        if QrLocGGX.RecordCount > 0 then
          GraGruX := QrLocGGXControle.Value;
*)
        QrLoc0200.Close;
        QrLoc0200.Params[00].AsInteger := FImporExpor;
        QrLoc0200.Params[01].AsInteger := FAnoMes;
        QrLoc0200.Params[02].AsInteger := Empresa;
        QrLoc0200.Params[03].AsString  := QrC170COD_ITEM.Value;
        UnDmkDAC_PF.AbreQuery(QrLoc0200, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
        //
        if QrLoc0200.RecordCount > 0 then
          GraGruX := QrLoc0200GraGruX.Value;
      end;
      //
      if GraGruX <> 0 then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'spedefdC170', False, [
        'GraGruX'], [
        'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
        GraGruX], [
        QrC170ImporExpor.Value, QrC170AnoMes.Value,
        QrC170Empresa.Value, QrC170LinArq.Value], False);
      end;
      //
      QrC170.Next;
    end;
    UMyMod.ReabreQuery(QrErrC170, Dmod.MyDB, [FImporExpor, FAnoMes, Empresa], Tit);
    if QrErrC170.RecordCount > 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Atrele os produtos caso existam, ou cadastre-os antes de continuar!');
      PageControl1.ActivePageIndex := 2;
      PageControl2.ActivePageIndex := 3;
      //
      Geral.MensagemBox('Carregamento cancelado!' + sLineBreak +
      'Existem ' + FormatFloat('0', QrErrC170.RecordCount) +
      ' produtos sem cadastro no aplicativo!' + sLineBreak +
      'Atrele os produtos caso existam, ou cadastre antes de continuar!',
      'Aviso', MB_OK+MB_ICONWARNING);
      //
      Exit;
    end;
    QrAdver_0200.Close;
    UnDmkDAC_PF.AbreQuery(QrAdver_0200, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    if QrAdver_0200.RecordCount > 0 then
      Geral.MensagemBox('Carregamento finalizado com advert�ncias!' + sLineBreak +
      'Existem ' + FormatFloat('0', QrAdver_0200.RecordCount) +
      ' produtos com nomes distantes!',
      'Aviso', MB_OK+MB_ICONWARNING);
        //
    PB1.Position := 0;
    //
    if ImportaEstoque() then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
    //
  finally
    if LaAviso1.Caption <> '' then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'CARREGAMENTO PARALISADO EM: ' + LaAviso1.Caption)
    else
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Carregamento finalizado!');
    //
    Screen.Cursor := crDefault;
    if Lista <> nil then
      Lista.Free;
  end;
end;

procedure TFmSPED_EFD_Importa.MeImportadoChange(Sender: TObject);
begin
  BtCarrega.Enabled := MeImportado.Lines.Count > 1;
end;

procedure TFmSPED_EFD_Importa.PreparaCarregamento();
begin
  MeImportado.Text := MLAGeral.LoadFileToText(EdSPED_EFD_Path.Text);
  PageControl1.ActivePageIndex := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Clique no bot�o carrega para ler arquivo!');
end;

function TFmSPED_EFD_Importa.DefineEntidadeCarregada(const CNPJCPF: String;
  const AvisaErros: Boolean): Boolean;
var
  Entidade: Integer;
  Doc, NomeEnt: String;
begin
  Result := False;
  Entidade := 0;
  NomeEnt := '';
  if (Length(CNPJCPF) = 14) and (Copy(CNPJCPF, 1, 3) = '000') then
    Doc := Copy(CNPJCPF, 4)
  else
    Doc := CNPJCPF;
  DModG.ObtemEntidadeDeCNPJCFP(Doc, Entidade);
  if Entidade <> 0 then
  begin
    if DModG.ReopenEndereco(Entidade) then
      NomeEnt := DModG.QrEnderecoNOME_ENT.Value;
    Result := NomeEnt <> '';
  end else begin
    if AvisaErros then
      Geral.MensagemBox('Entidade n�o definida para o CNPJ/CPF:' +
      sLineBreak + CNPJCPF, 'Aviso', MB_OK+MB_ICONWARNING);
  end;
  begin
    EdEmp_CNPJ.Text := Geral.FormataCNPJ_TT(CNPJCPF);
    EdEmp_Codigo.ValueVariant := Entidade;
    EdEmp_Nome.Text := NomeEnt;
  end;
end;

procedure TFmSPED_EFD_Importa.EdEmp_CodigoChange(Sender: TObject);
begin
  FEmpresa := EdEmp_Codigo.ValueVariant;
end;

function TFmSPED_EFD_Importa.ExcluiPeriodoSelecionado(Empresa: Integer): Boolean;
var
  EmpTXT, IE_TXT, AM_TXT: String;
  I: Integer;
begin
  Result := False;
  EmpTXT := FormatFloat('0', Empresa);
  AM_TXT := FormatFloat('0', FAnoMes);
  IE_TXT := FormatFloat('0', FImporExpor);
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT REG');
  Dmod.QrAux.SQL.Add('FROM spedefd0000');
  Dmod.QrAux.SQL.Add('WHERE ImporExpor=' + IE_TXT);
  Dmod.QrAux.SQL.Add('AND Empresa=' + EmpTXT);
  Dmod.QrAux.SQL.Add('AND AnoMes=' + AM_TXT);
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('UNION');
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('SELECT REG');
  Dmod.QrAux.SQL.Add('FROM spedefd0001');
  Dmod.QrAux.SQL.Add('WHERE ImporExpor=' + IE_TXT);
  Dmod.QrAux.SQL.Add('AND Empresa=' + EmpTXT);
  Dmod.QrAux.SQL.Add('AND AnoMes=' + AM_TXT);
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('UNION');
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('SELECT REG');
  Dmod.QrAux.SQL.Add('FROM spedefdc100');
  Dmod.QrAux.SQL.Add('WHERE ImporExpor=' + IE_TXT);
  Dmod.QrAux.SQL.Add('AND Empresa=' + EmpTXT);
  Dmod.QrAux.SQL.Add('AND AnoMes=' + AM_TXT);
  Dmod.QrAux.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SHOW TABLES');
    Dmod.QrAux.SQL.Add('FROM bluederm');
    Dmod.QrAux.SQL.Add('LIKE "spedefd%"');
    Dmod.QrAux.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    //
    if Dmod.QrAux.RecordCount - FNot_SPED_EFD_II <> FQtd_SPED_EFD_II then
    begin
      Geral.MensagemBox(
      'Importa��o cancelada! Quantidade de tabelas desatualizadas!' + sLineBreak +
      'AVISE A DERMATEK!', 'Aviso', MB_OK+MB_ICONWARNING);
    end else begin
      if Geral.MensagemBox(
      'J� existem dados para este per�odo!' + sLineBreak +
      'Os dados j� existentes ser�o exclu�dos! Deseja continuar assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Screen.Cursor := crHourGlass;
        try
          // Por preven��o excluir do "0" mesmo que n�o use!
          for I := 0 to High(FTbs_SPED_EFD_II) (*- 1*) do
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('DELETE FROM ' + FTbs_SPED_EFD_II[I]);
            Dmod.QrUpd.SQL.Add('WHERE ImporExpor=' + IE_TXT);
            Dmod.QrUpd.SQL.Add('AND Empresa=' + EmpTXT);
            Dmod.QrUpd.SQL.Add('AND AnoMes=' + AM_TXT);
            Dmod.QrUpd.ExecSQL;
          end;
          Result := True;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
    // Evitar exclus�o errada!
    if (GRADE_TABS_PARTIPO_0004 <> 0) and (FAnoMes <> 0) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo dados duplic�veis no estoque');
      QrRecupera.Close;
      QrRecupera.Params[00].AsInteger := VAR_FATID_0251;
      QrRecupera.Params[01].AsInteger := GRADE_TABS_PARTIPO_0004;
      QrRecupera.Params[02].AsInteger := FAnoMes;
      UnDmkDAC_PF.AbreQuery(QrRecupera, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0');
      Dmod.QrUpd.SQL.Add('AND Empresa=:P1');
      Dmod.QrUpd.SQL.Add('AND partipo=:P2');
      Dmod.QrUpd.SQL.Add('AND parcodi=:P3');
      Dmod.QrUpd.Params[00].AsInteger := VAR_FATID_0251;
      Dmod.QrUpd.Params[01].AsInteger := Empresa;
      Dmod.QrUpd.Params[02].AsInteger := GRADE_TABS_PARTIPO_0004;
      Dmod.QrUpd.Params[03].AsInteger := FAnoMes;
      Dmod.QrUpd.ExecSQL;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo Notas Ficais existentes');
      QrNFsA.Close;
{
SELECT FatID, FatNum, Empresa
FROM nfecaba
WHERE FatID=:P0
AND EFD_INN_AnoMes=:P1
AND EFD_INN_Empresa=:P2
}
      QrNFsA.Params[00].AsInteger := VAR_FATID_0251;
      QrNFsA.Params[01].AsInteger := FAnoMes;
      QrNFsA.Params[02].AsInteger := Empresa;
      UnDmkDAC_PF.AbreQuery(QrNFsA, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
      PB1.Position := 0;
      PB1.Max := QrNFsA.RecordCount;
      while not QrNFsA.Eof do
      begin
        PB1.Position := PB1.Position + 1;
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo Nota Fiscal - ID = ' +
          FormatFloat('000000000', QrNFsAIDCtrl.Value));
        DmNFe_0000.ExcluiNfe(0, QrNFsAFatID.Value, QrNFsAFatNum.Value,
          QrNFsAEmpresa.Value, True, True);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'livres', False, [
        'Codigo', 'Tabela'], [], [
        QrNFsAIDCtrl.Value, 'nfecaba'], [
        ], False);
        //
        QrNFsA.Next;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Recuperando c�digos "StqMovItsA.IDCtrl"');
      PB1.Position := 0;
      PB1.Max := QrRecupera.RecordCount;
      QrRecupera.First;
      while not QrRecupera.Eof do
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'Recuperando c�digos "StqMovItsA.IDCtrl" ' + FormatFloat('0',
          QrRecuperaIDCtrl.Value));
        PB1.Position := PB1.Position + 1;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'livres', False, [
        'Codigo', 'Tabela'], [], [
        QrRecuperaIDCtrl.Value, 'stqmovitsa'], [
        ], False);
        //
        QrRecupera.Next;
      end;
    end;
  end else
    Result := True;
end;

procedure TFmSPED_EFD_Importa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSPED_EFD_Importa.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType       := stLok;
  CkNomeGGXIgual.Enabled := VAR_USUARIO = -1;
  //
  FCarregou := False;
  FAnoMes := 0;
  //
  PageControl0.ActivePageIndex := 0;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  //
  QrINN_0200.DataBase := DModG.MyPID_DB;
  QrAdver_0200.DataBase := DModG.MyPID_DB;
end;

procedure TFmSPED_EFD_Importa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmSPED_EFD_Importa.ImportaEstoque(): Boolean;
const
  FatID = VAR_FATID_0251;
  Tipo = VAR_FATID_0251;
  ParTipo = GRADE_TABS_PARTIPO_0004;  // Para poder exclu�r em massa
  StqCenCad = 1;
  OriCnta = 1;
  OriPart = 1;
  FatorClas = 1;
  QuemUsou = 0;
  Retorno = 0;
  DebCtrl = -1;
  SMIMultIns = -1;
  GrupoBal = 0;
  AntQtde = 0;
var
  (*
  IND_OPER, IND_EMIT, COD_PART, COD_MOD, SER: String;
  COD_SIT, NUM_DOC: Integer;
  *)
  //
  DataHora: String;
  FatNum, OriCodi, OriCtrl, Empresa, GraGruX, ParCodi, IDCtrl, Baixa, nItem,
  UnidMed: Integer;
  Qtde, Pecas, Peso, AreaM2, AreaP2, CustoAll, ValorAll: Double;
  Corrige: Boolean;
begin
  // N�o carregar
  if RGConfig.ItemIndex = 0 then
  begin
    Result := True;
    Exit;
  end else
    Result := False;
  if not CkSoTerceiros.Checked then
  begin
    Geral.MensagemBox('Importa��o de Notas Fiscais pr�prias n�o implementado!' +
    sLineBreak + 'Solicite a implementa��o � DERMATEK!',
    'Informa��o', MB_OK+MB_ICONINFORMATION);
    Exit;
  end;
(* N�o existe inser��o de NFs. S� no estoque
  if not CkRecriarNFs.Checked then
  begin
    if Geral.MensagemBox(
    'As notas fiscais j� inseridas anteriormente n�o ter�o seus dados corrigidos!' +
    sLineBreak + 'Deseja continuar assim mesmo?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
      Exit;
  end;
*)
  Screen.Cursor := crHourGlass;
  try
    PB1.Position := 0;
    PB1.Max := QrC100.RecordCount;
    QrC100.First;
    while not QrC100.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Registro C100 Linha ' +
      FormatFloat('0', QrC100LinArq.Value) + ' NF S�rie ' +
      QrC100SER.Value + ' N�mero ' + FormatFloat('000000', QrC100NUM_DOC.Value)
      + ' - Verificando');
      //
      (*  �ndice no arquivo (emitidos por terceiros)
      IND_OPER := QrC100IND_OPER.Value;
      IND_EMIT := QrC100IND_EMIT.Value;
      COD_PART := QrC100COD_PART.Value;
      COD_MOD  := QrC100COD_MOD.Value;
      COD_SIT  := QrC100COD_SIT.Value;
      SER      := QrC100SER.Value;
      NUM_DOC  := QrC100NUM_DOC.Value;
      *)
      //
      if QrC100IND_EMIT.Value = '1' then
      begin
        Empresa := QrC100Empresa.Value;
        FatNum := DModG.BuscaProximoInteiro('nfecaba', 'FatNum', 'FatID', FatID);
        if InsereNotaFiscal_C100_Cab(FatID, FatNum, Empresa) then
        begin
          QrItsC170.Close;
          QrItsC170.Params[00].AsInteger := QrC100ImporExpor.Value;
          QrItsC170.Params[01].AsInteger := QrC100AnoMes.Value;
          QrItsC170.Params[02].AsInteger := QrC100Empresa.Value;
          QrItsC170.Params[03].AsInteger := QrC100LinArq.Value;
          UnDmkDAC_PF.AbreQuery(QrItsC170, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
          if QrItsC170.RecordCount = 0 then
            Geral.MensagemBox('Registro C100 Linha ' + FormatFloat('0',
            QrC100LinArq.Value)
            + sLineBreak + 'S�rie da NF: ' + QrC100SER.Value
            + sLineBreak + 'N�mero da NF:' + FormatFloat('000000', QrC100NUM_DOC.Value)
            + sLineBreak + 'C�digo do participante: ' + QrC100COD_PART.Value
            + sLineBreak + 'N�o h� item nesta NF!', 'Aviso', MB_OK+MB_ICONWARNING)
          else begin
            QrItsC170.First;
            while not QrItsC170.Eof do
            begin
              MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Registro C100 Linha ' +
              FormatFloat('0', QrC100LinArq.Value) + ' NF S�rie ' +
              QrC100SER.Value + ' N�mero ' + FormatFloat('000000',
              QrC100NUM_DOC.Value) + ' inclu�ndo no estoque item da linha ' +
              FormatFloat('0', QrItsC170LinArq.Value));
              GraGruX    := QrItsC170GraGruX.Value;
              QrGGX.Close;
              QrGGX.Params[00].AsInteger := GraGruX;
              UnDmkDAC_PF.AbreQuery(QrGGX, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
              // String
              DataHora   := Geral.FDT(QrC100DT_E_S.Value, 1);
              // Integer
              OriCodi    := FatNum;
              OriCtrl    := QrItsC170LinArq.Value;
              Empresa    := QrItsC170Empresa.Value;
              ParCodi    := QrC100AnoMes.Value; // Para poder exclu�r em massa
              // Double
              Qtde       := QrItsC170QTD.Value;
              Pecas      := 0; // Define abaixo
              Peso       := 0; // Define abaixo
              AreaM2     := 0; // Define abaixo
              AreaP2     := 0; // Define abaixo
              CustoAll   := QrItsC170VL_ITEM.Value;
              ValorAll   := 0;
{
SELECT UnidMed
FROM spedefd0190
WHERE ImporExpor=:P0
AND AnoMes=:P1
AND Empresa=:P2
AND UNID=:P3
}
              QrLocUNID.Close;
              QrLocUNID.Params[00].AsInteger := FImporExpor;
              QrLocUNID.Params[01].AsInteger := FAnoMes;
              QrLocUNID.Params[02].AsInteger := Empresa;
              QrLocUNID.Params[03].AsString  := QrItsC170UNID.Value;
              UnDmkDAC_PF.AbreQuery(QrLocUNID, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
              UnidMed    := QrLocUNIDUnidMed.Value;
              // Corrigir pelo arquivo  ????
              Corrige := False;
              case QrLocUNIDGrandeza.Value of
(*Pe�a          *) 0: Pecas := Qtde;
(*�rea (m�)     *) 1: AreaM2 := Qtde;
(*Peso (kg)     *) 2: Peso := Qtde;
(*Volume ( m�)  *) 3: Corrige := True;
(*Linear (m)    *) 4: Corrige := True;
(*? ? ? (outros)*) 5: Corrige := True;
(*�rea (ft�)    *) 6: AreaP2 := Qtde;
(*Peso (t)      *) 7: Peso := Qtde;
(*Volume (m�)   *) 8: Corrige := True;
              end;
              if Corrige then
              begin
                Pecas      := 0; // Define abaixo
                Peso       := 0; // Define abaixo
                AreaM2     := 0; // Define abaixo
                AreaP2     := 0; // Define abaixo
                //
                case QrGGXGerBxaEstq.Value of
                  //0: ? ? ?
                  1: Pecas := Qtde;       // Pe�a
                  2: AreaM2 := Qtde;      // m�
                  3: Peso := Qtde;        // kg
                  4: Peso := Qtde;        // t (ton)
                  5: AreaP2 := Qtde;      // ft�
                end;
              end;
              //
              if (QrGGXPrdGrupTip.Value = - 2) and (QrGGXNivel2.Value <> - 2)
              and CkUCConsumido.Checked then
                Baixa := 0
              else
              if (QrGGXPrdGrupTip.Value = - 1) and CkMPConsumido.Checked then
                Baixa := 0
              else begin
                if QrItsC170IND_MOV.Value = '0' then
                  Baixa := 1
                else
                  Baixa := 0;
              end;
              //
              nItem := QrItsC170NUM_ITEM.Value;
              if InsereNotaFiscal_C170_I(
                 FatID, FatNum, Empresa, GraGruX, QrGGXNivel1.Value, nItem) then
              if InsereNotaFiscal_C170_N(FatID, FatNum, Empresa, nItem) then
              if InsereNotaFiscal_C170_O(FatID, FatNum, Empresa, nItem) then
              if InsereNotaFiscal_C170_Q(FatID, FatNum, Empresa, nItem) then
              if InsereNotaFiscal_C170_S(FatID, FatNum, Empresa, nItem) then
              begin
                IDCtrl := UMyMod.BuscaEmLivreY_Def('stqmovitsa', 'IDCtrl', stIns, 0);
                UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
                CO_DATA_HORA_SMI, 'Tipo', 'OriCodi',
                'OriCtrl', 'OriCnta', 'OriPart',
                'Empresa', 'StqCenCad', 'GraGruX',
                'Qtde', 'Pecas', 'Peso',
                'AreaM2', 'AreaP2', 'FatorClas',
                'QuemUsou', 'Retorno', 'ParTipo',
                'ParCodi', 'DebCtrl', 'SMIMultIns',
                'CustoAll', 'ValorAll', 'GrupoBal',
                'Baixa', 'AntQtde', 'UnidMed'], [
                'IDCtrl'], [
                DataHora, Tipo, OriCodi,
                OriCtrl, OriCnta, OriPart,
                Empresa, StqCenCad, GraGruX,
                Qtde, Pecas, Peso,
                AreaM2, AreaP2, FatorClas,
                QuemUsou, Retorno, ParTipo,
                ParCodi, DebCtrl, SMIMultIns,
                CustoAll, ValorAll, GrupoBal,
                Baixa, AntQtde, UnidMed], [
                IDCtrl], False);
              end;
              //
              QrItsC170.Next;
            end;
          end;
        end;
      end else begin
        if not CkSoTerceiros.Checked then
          Geral.MensagemBox(
          'Importa��o de Notas Fiscais pr�prias n�o implementado!' +
          sLineBreak + 'Solicite a implementa��o � DERMATEK!',
          'Informa��o', MB_OK+MB_ICONINFORMATION);
      end;
      QrC100.Next;
    end;
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmSPED_EFD_Importa.InsereNotaFiscal_C100_Cab(FatID, FatNum, Empresa: Integer): Boolean;
const
  Importado = 2;
var
  IDCtrl: Integer;
  Q0, Q1, Q2, Qn, QSim, QNao: Integer;
  // NFeCabA
  CodInfoDest, CodInfoEmit, ide_tpNF, ide_mod, Status, ide_serie, ide_nNF,
  ide_indPag, modFrete, EFD_INN_AnoMes, EFD_INN_Empresa, EFD_INN_LinArq,
  Codigo: Integer;
  COD_SIT, NFG_Serie, Id, ide_dEmi, ide_dSaiEnt, DataFiscal, COD_MOD(*, DataE*): String;
  ICMSTot_vNF, ICMSTot_vDesc, VL_ABAT_NT, ICMSTot_vProd, ICMSTot_vFrete,
  ICMSTot_vSeg, ICMSTot_vOutro, ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,
  ICMSTot_vST, ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
  RetTrib_vRetPIS, RetTrib_vRetCOFINS: Double;
{
  // NFeItsI
  nItem, MeuID, Nivel1, GraGruX, prod_CFOP: Integer;
  prod_qCom, prod_vUnCom, prod_vProd, prod_qTrib, prod_vUnTrib: Double;
  // StqMovItsa
  DataHora: String;
  Tipo, OriCodi, OriCtrl, ParCodi: Integer;
  Qtde, CustoAll: Double;
  // PQX
  ParTipo, StqMovIts, OrigemCodi, OrigemCtrl: Integer;
}
begin
  ide_tpNF := Geral.IMV(QrC100IND_OPER.Value);
  //
  CodInfoDest := Empresa;
  QrLocCOD_PART.Close;
  QrLocCOD_PART.Params[00].AsInteger := QrC100ImporExpor.Value;
  QrLocCOD_PART.Params[01].AsInteger := QrC100AnoMes.Value;
  QrLocCOD_PART.Params[02].AsInteger := QrC100Empresa.Value;
  QrLocCOD_PART.Params[03].AsString  := QrC100COD_PART.Value;
  UnDmkDAC_PF.AbreQuery(QrLocCOD_PART, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  CodInfoEmit := QrLocCOD_PARTEntidade.Value;
  //
  COD_MOD := QrC100COD_MOD.Value;
  ide_mod := Geral.IMV(COD_MOD);
  COD_SIT := QrC100COD_SIT.Value;
  Status := SPED_Geral.Obtem_NFe_Status_de_SPED_COD_SIT(Geral.IMV(COD_SIT));
  NFG_Serie := QrC100SER.Value;
  ide_serie := Geral.IMV(NFG_Serie);
  ide_nNF := QrC100NUM_DOC.Value;
  Id := QrC100CHV_NFE.Value;
  DataFiscal := Geral.FDT(QrC100DT_DOC.Value, 1);
  ide_dEmi := Geral.FDT(QrC100DT_DOC.Value - 2, 1);
  ide_dSaiEnt := ide_dEmi;
  ICMSTot_vNF := QrC100VL_DOC.Value;
  ide_indPag := SPED_Geral.Obtem_NFe_indPag_de_SPED_IND_PGTO(QrC100IND_PGTO.Value);
  ICMSTot_vDesc := QrC100VL_DESC.Value;
  VL_ABAT_NT := QrC100VL_ABAT_NT.Value;
  ICMSTot_vProd := QrC100VL_MERC.Value;
  modFrete := SPED_Geral.Obtem_NFe_modFrete_de_SPED_IND_FRT(QrC100IND_FRT.Value);
  ICMSTot_vFrete := QrC100VL_FRT.Value;
  ICMSTot_vSeg := QrC100VL_SEG.Value;
  ICMSTot_vOutro := QrC100VL_OUT_DA.Value;
  ICMSTot_vBC := QrC100VL_BC_ICMS.Value;
  ICMSTot_vICMS := QrC100VL_ICMS.Value;
  ICMSTot_vBCST := QrC100VL_BC_ICMS_ST.Value;
  ICMSTot_vST := QrC100VL_ICMS_ST.Value;
  ICMSTot_vIPI := QrC100VL_IPI.Value;
  ICMSTot_vPIS := QrC100VL_PIS.Value;
  ICMSTot_vCOFINS := QrC100VL_COFINS.Value;
  RetTrib_vRetPIS := QrC100VL_PIS_ST.Value;
  RetTrib_vRetCOFINS := QrC100VL_COFINS_ST.Value;
  // := QrC100ImporExpor.Value;
  EFD_INN_AnoMes := QrC100AnoMes.Value;
  EFD_INN_Empresa := QrC100Empresa.Value;
  EFD_INN_LinArq := QrC100LinArq.Value;
  //
  IDCtrl := DModG.BuscaProximoInteiro_A('nfecaba', 'IDCtrl');
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False, [
  'IDCtrl', (*'LoteEnv', 'versao',*)
  'Id', (*'ide_cUF', 'ide_cNF',
  'ide_natOp',*) 'ide_indPag', 'ide_mod',
  'ide_serie', 'ide_nNF', 'ide_dEmi',
  'ide_dSaiEnt', 'ide_tpNF', (*'ide_cMunFG',
  'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
  'ide_tpAmb', 'ide_finNFe', 'ide_procEmi',
  'ide_verProc', 'emit_CNPJ', 'emit_CPF',
  'emit_xNome', 'emit_xFant', 'emit_xLgr',
  'emit_nro', 'emit_xCpl', 'emit_xBairro',
  'emit_cMun', 'emit_xMun', 'emit_UF',
  'emit_CEP', 'emit_cPais', 'emit_xPais',
  'emit_fone', 'emit_IE', 'emit_IEST',
  'emit_IM', 'emit_CNAE', 'dest_CNPJ',
  'dest_CPF', 'dest_xNome', 'dest_xLgr',
  'dest_nro', 'dest_xCpl', 'dest_xBairro',
  'dest_cMun', 'dest_xMun', 'dest_UF',
  'dest_CEP', 'dest_cPais', 'dest_xPais',
  'dest_fone', 'dest_IE', 'dest_ISUF',*)
  'ICMSTot_vBC', 'ICMSTot_vICMS', 'ICMSTot_vBCST',
  'ICMSTot_vST', 'ICMSTot_vProd', 'ICMSTot_vFrete',
  'ICMSTot_vSeg', 'ICMSTot_vDesc', (*'ICMSTot_vII',*)
  'ICMSTot_vIPI', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
  'ICMSTot_vOutro', 'ICMSTot_vNF', (*'ISSQNtot_vServ',
  'ISSQNtot_vBC', 'ISSQNtot_vISS', 'ISSQNtot_vPIS',
  'ISSQNtot_vCOFINS',*) 'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS',
  (*'RetTrib_vRetCSLL', 'RetTrib_vBCIRRF', 'RetTrib_vIRRF',
  'RetTrib_vBCRetPrev', 'RetTrib_vRetPrev',*) 'ModFrete',
  (*'Transporta_CNPJ', 'Transporta_CPF', 'Transporta_XNome',
  'Transporta_IE', 'Transporta_XEnder', 'Transporta_XMun',
  'Transporta_UF', 'RetTransp_vServ', 'RetTransp_vBCRet',
  'RetTransp_PICMSRet', 'RetTransp_vICMSRet', 'RetTransp_CFOP',
  'RetTransp_CMunFG', 'VeicTransp_Placa', 'VeicTransp_UF',
  'VeicTransp_RNTC', 'Cobr_Fat_nFat', 'Cobr_Fat_vOrig',
  'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq', 'InfAdic_InfAdFisco',
  'InfAdic_InfCpl', 'Exporta_UFEmbarq', 'Exporta_XLocEmbarq',
  'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',*)
  'Status', (*'infProt_Id', 'infProt_tpAmb',
  'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
  'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
  'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
  'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
  'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
  'infCanc_xJust', '_Ativo_', 'FisRegCad',
  'CartEmiss', 'TabelaPrc', 'CondicaoPg',
  'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',
  'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
  'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
  'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
  'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
  'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
  'DataFiscal', (*'protNFe_versao', 'retCancNFe_versao',
  'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta', 'SINTEGRA_ExpNat',
  'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta', 'SINTEGRA_ExpConhNum',
  'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip', 'SINTEGRA_ExpPais',
  'SINTEGRA_ExpAverDta',*) 'CodInfoEmit', 'CodInfoDest',
  (*'CriAForca', 'ide_hSaiEnt', 'ide_dhCont',
  'ide_xJust', 'emit_CRT', 'dest_email',
  'Vagao', 'Balsa', 'CodInfoTrsp',
  'OrdemServ', 'Situacao', 'Antigo',*)
  'NFG_Serie', (*'NF_ICMSAlq', 'NF_CFOP',*)
  'Importado', (*'NFG_SubSerie', 'NFG_ValIsen',
  'NFG_NaoTrib', 'NFG_Outros',*) 'COD_MOD',
  'COD_SIT', 'VL_ABAT_NT', 'EFD_INN_AnoMes',
  'EFD_INN_Empresa', 'EFD_INN_LinArq'], [
  'FatID', 'FatNum', 'Empresa'], [
  IDCtrl, (*LoteEnv, versao,*)
  Id, (*ide_cUF, ide_cNF,
  ide_natOp,*) ide_indPag, ide_mod,
  ide_serie, ide_nNF, ide_dEmi,
  ide_dSaiEnt, ide_tpNF, (*ide_cMunFG,
  ide_tpImp, ide_tpEmis, ide_cDV,
  ide_tpAmb, ide_finNFe, ide_procEmi,
  ide_verProc, emit_CNPJ, emit_CPF,
  emit_xNome, emit_xFant, emit_xLgr,
  emit_nro, emit_xCpl, emit_xBairro,
  emit_cMun, emit_xMun, emit_UF,
  emit_CEP, emit_cPais, emit_xPais,
  emit_fone, emit_IE, emit_IEST,
  emit_IM, emit_CNAE, dest_CNPJ,
  dest_CPF, dest_xNome, dest_xLgr,
  dest_nro, dest_xCpl, dest_xBairro,
  dest_cMun, dest_xMun, dest_UF,
  dest_CEP, dest_cPais, dest_xPais,
  dest_fone, dest_IE, dest_ISUF,*)
  ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,
  ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete,
  ICMSTot_vSeg, ICMSTot_vDesc, (*ICMSTot_vII,*)
  ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
  ICMSTot_vOutro, ICMSTot_vNF, (*ISSQNtot_vServ,
  ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,
  ISSQNtot_vCOFINS,*) RetTrib_vRetPIS, RetTrib_vRetCOFINS,
  (*RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,
  RetTrib_vBCRetPrev, RetTrib_vRetPrev,*) ModFrete,
  (*Transporta_CNPJ, Transporta_CPF, Transporta_XNome,
  Transporta_IE, Transporta_XEnder, Transporta_XMun,
  Transporta_UF, RetTransp_vServ, RetTransp_vBCRet,
  RetTransp_PICMSRet, RetTransp_vICMSRet, RetTransp_CFOP,
  RetTransp_CMunFG, VeicTransp_Placa, VeicTransp_UF,
  VeicTransp_RNTC, Cobr_Fat_nFat, Cobr_Fat_vOrig,
  Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,
  InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq,
  Compra_XNEmp, Compra_XPed, Compra_XCont,*)
  Status, (*infProt_Id, infProt_tpAmb,
  infProt_verAplic, infProt_dhRecbto, infProt_nProt,
  infProt_digVal, infProt_cStat, infProt_xMotivo,
  infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
  infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,
  infCanc_cStat, infCanc_xMotivo, infCanc_cJust,
  infCanc_xJust, _Ativo_, FisRegCad,
  CartEmiss, TabelaPrc, CondicaoPg,
  FreteExtra, SegurExtra, ICMSRec_pRedBC,
  ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
  IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
  IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
  PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
  COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
  DataFiscal, (*protNFe_versao, retCancNFe_versao,
  SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_ExpNat,
  SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum,
  SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais,
  SINTEGRA_ExpAverDta,*) CodInfoEmit, CodInfoDest,
  (*CriAForca, ide_hSaiEnt, ide_dhCont,
  ide_xJust, emit_CRT, dest_email,
  Vagao, Balsa, CodInfoTrsp,
  OrdemServ, Situacao, Antigo,*)
  NFG_Serie, (*NF_ICMSAlq, NF_CFOP,*)
  Importado, (*NFG_SubSerie, NFG_ValIsen,
  NFG_NaoTrib, NFG_Outros,*) COD_MOD,
  COD_SIT, VL_ABAT_NT, EFD_INN_AnoMes,
  EFD_INN_Empresa, EFD_INN_LinArq], [
  FatID, FatNum, Empresa], True);
end;

function TFmSPED_EFD_Importa.InsereNotaFiscal_C170_I(FatID, FatNum,
  Empresa, GraGruX, Nivel1, nItem: Integer): Boolean;
var
  // NFeItsI
  prod_uCom, prod_uTrib: String;
  MeuID, prod_CFOP: Integer;
  prod_qCom, prod_vUnCom, prod_vProd, prod_vDesc, prod_qTrib, prod_vUnTrib: Double;
begin
  //prod_CFOP := Geral.IMV(Geral.SoNumero_TT(QrItsC170CFOP.Value));
  prod_CFOP := QrItsC170CFOP.Value;
  prod_qCom := QrItsC170QTD.Value;
  prod_vProd := QrItsC170VL_ITEM.Value;
  if prod_qCom = 0 then
    prod_vUnCom := 0
  else
    prod_vUnCom := prod_vProd / prod_qCom;
  prod_uCom := QrItsC170UNID.Value;
  prod_qTrib := prod_qCom;
  prod_uTrib := prod_uCom;
  prod_vUnTrib := prod_vUnCom;
  prod_vDesc := QrItsC170VL_DESC.Value;
  MeuID := QrItsC170LinArq.Value;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
  (*'prod_cProd', 'prod_cEAN', 'prod_xProd',
  'prod_NCM', 'prod_EXTIPI', 'prod_genero',*)
  'prod_CFOP', 'prod_uCom', 'prod_qCom',
  'prod_vUnCom', 'prod_vProd', (*'prod_cEANTrib',*)
  'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
  (*'prod_vFrete', 'prod_vSeg',*) 'prod_vDesc',
  (*'Tem_IPI', '_Ativo_', 'InfAdCuztm',
  'EhServico', 'ICMSRec_pRedBC', 'ICMSRec_vBC',
  'ICMSRec_pAliq', 'ICMSRec_vICMS', 'IPIRec_pRedBC',
  'IPIRec_vBC', 'IPIRec_pAliq', 'IPIRec_vIPI',
  'PISRec_pRedBC', 'PISRec_vBC', 'PISRec_pAliq',
  'PISRec_vPIS', 'COFINSRec_pRedBC', 'COFINSRec_vBC',
  'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*) 'MeuID',
  'Nivel1', (*'prod_vOutro', 'prod_indTot',
  'prod_xPed', 'prod_nItemPed',*) 'GraGruX'(*,
  'UnidMedCom', 'UnidMedTrib'*)], [
  'FatID', 'FatNum', 'Empresa', 'nItem'], [
  (*prod_cProd, prod_cEAN, prod_xProd,
  prod_NCM, prod_EXTIPI, prod_genero,*)
  prod_CFOP, prod_uCom, prod_qCom,
  prod_vUnCom, prod_vProd, (*prod_cEANTrib,*)
  prod_uTrib, prod_qTrib, prod_vUnTrib,
  (*prod_vFrete, prod_vSeg,*) prod_vDesc,
  (*Tem_IPI, _Ativo_, InfAdCuztm,
  EhServico, ICMSRec_pRedBC, ICMSRec_vBC,
  ICMSRec_pAliq, ICMSRec_vICMS, IPIRec_pRedBC,
  IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI,
  PISRec_pRedBC, PISRec_vBC, PISRec_pAliq,
  PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
  COFINSRec_pAliq, COFINSRec_vCOFINS,*) MeuID,
  Nivel1, (*prod_vOutro, prod_indTot,
  prod_xPed, prod_nItemPed,*) GraGruX(*,
  UnidMedCom, UnidMedTrib*)], [
  FatID, FatNum, Empresa, nItem], True);
end;

function TFmSPED_EFD_Importa.InsereNotaFiscal_C170_N(FatID, FatNum,
  Empresa, nItem: Integer): Boolean;
var
  ICMS_Orig, ICMS_CST: Integer;
  COD_NAT: String;
  ICMS_vBC, ICMS_pICMS, ICMS_vICMS, ICMS_vBCST, ICMS_pICMSST, ICMS_vICMSST: Double;
begin
  ICMS_Orig :=  QrItsC170CST_ICMS.Value div 100;
  ICMS_CST := QrItsC170CST_ICMS.Value mod 100;
  COD_NAT := QrItsC170COD_NAT.Value;

  ICMS_vBC := QrItsC170VL_BC_ICMS.Value;
  ICMS_pICMS := QrItsC170ALIQ_ICMS.Value;
  ICMS_vICMS := QrItsC170VL_ICMS.Value;

  ICMS_vBCST := QrItsC170VL_BC_ICMS_ST.Value;
  ICMS_pICMSST := QrItsC170ALIQ_ST.Value;
  ICMS_vICMSST := QrItsC170VL_ICMS_ST.Value;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsn', False, [
  'ICMS_Orig', 'ICMS_CST', (*'ICMS_modBC',
  'ICMS_pRedBC',*) 'ICMS_vBC', 'ICMS_pICMS',
  'ICMS_vICMS', (*'ICMS_modBCST', 'ICMS_pMVAST',
  'ICMS_pRedBCST',*) 'ICMS_vBCST', 'ICMS_pICMSST',
  'ICMS_vICMSST', (*'_Ativo_', 'ICMS_CSOSN',
  'ICMS_UFST', 'ICMS_pBCOp', 'ICMS_vBCSTRet',
  'ICMS_vICMSSTRet', 'ICMS_motDesICMS', 'ICMS_pCredSN',
  'ICMS_vCredICMSSN',*) 'COD_NAT'], [
  'FatID', 'FatNum', 'Empresa', 'nItem'], [
  ICMS_Orig, ICMS_CST, (*ICMS_modBC,
  ICMS_pRedBC,*) ICMS_vBC, ICMS_pICMS,
  ICMS_vICMS, (*ICMS_modBCST, ICMS_pMVAST,
  ICMS_pRedBCST,*) ICMS_vBCST, ICMS_pICMSST,
  ICMS_vICMSST, (*_Ativo_, ICMS_CSOSN,
  ICMS_UFST, ICMS_pBCOp, ICMS_vBCSTRet,
  ICMS_vICMSSTRet, ICMS_motDesICMS, ICMS_pCredSN,
  ICMS_vCredICMSSN,*) COD_NAT], [
  FatID, FatNum, Empresa, nItem], True);
end;

function TFmSPED_EFD_Importa.InsereNotaFiscal_C170_O(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
var
  IPI_CST, IPI_cEnq: String;
  IPI_vBC, IPI_pIPI, IPI_vIPI: Double;
begin
  IPI_CST := QrItsC170CST_IPI.Value;
  IPI_cEnq := QrItsC170COD_ENQ.Value;
  IPI_vBC := QrItsC170VL_BC_IPI.Value;
  IPI_pIPI := QrItsC170ALIQ_IPI.Value;
  IPI_vIPI := QrItsC170VL_IPI.Value;
  //
  if (IPI_vBC >= 0.01) or (IPI_pIPI >= 0.01) or (IPI_vIPI >= 0.01) then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitso', False, [
    (*'IPI_clEnq', 'IPI_CNPJProd', 'IPI_cSelo',
    'IPI_qSelo',*) 'IPI_cEnq', 'IPI_CST',
    'IPI_vBC', (*'IPI_qUnid', 'IPI_vUnid',*)
    'IPI_pIPI', 'IPI_vIPI'(*, '_Ativo_'*)], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    (*IPI_clEnq, IPI_CNPJProd, IPI_cSelo,
    IPI_qSelo,*) IPI_cEnq, IPI_CST,
    IPI_vBC, (*IPI_qUnid, IPI_vUnid,*)
    IPI_pIPI, IPI_vIPI(*, _Ativo_*)], [
    FatID, FatNum, Empresa, nItem], True);
  end else
    Result := True;
end;

function TFmSPED_EFD_Importa.InsereNotaFiscal_C170_Q(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
var
  PIS_CST: Integer;
  PIS_vBC, PIS_pPIS, PIS_vPIS, PIS_qBCProd, PIS_vAliqProd: Double;
begin
  PIS_CST := QrItsC170CST_PIS.Value;
  PIS_vBC := QrItsC170VL_BC_PIS.Value;
  PIS_pPIS := QrItsC170ALIQ_PIS_p.Value;
  PIS_qBCProd := QrItsC170QUANT_BC_PIS.Value;
  PIS_vAliqProd := QrItsC170ALIQ_PIS_r.Value;
  PIS_vPIS := QrItsC170VL_PIS.Value;
  //
  if (PIS_vBC >= 0.01) or (PIS_pPIS >= 0.01) or (PIS_qBCProd >= 0.01)
  or (PIS_vAliqProd >= 0.01) or (PIS_vPIS >= 0.01) then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsq', False, [
    'PIS_CST', 'PIS_vBC', 'PIS_pPIS',
    'PIS_vPIS', 'PIS_qBCProd', 'PIS_vAliqProd'(*,
    '_Ativo_', 'PIS_fatorBC'*)], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    PIS_CST, PIS_vBC, PIS_pPIS,
    PIS_vPIS, PIS_qBCProd, PIS_vAliqProd(*,
    _Ativo_, PIS_fatorBC*)], [
    FatID, FatNum, Empresa, nItem], True);
  end else
    Result := True;
end;

function TFmSPED_EFD_Importa.InsereNotaFiscal_C170_S(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
var
  COFINS_CST: Integer;
  COFINS_vBC, COFINS_pCOFINS, COFINS_vCOFINS, COFINS_qBCProd, COFINS_vAliqProd: Double;
begin
  COFINS_CST := QrItsC170CST_COFINS.Value;
  COFINS_vBC := QrItsC170VL_BC_COFINS.Value;
  COFINS_pCOFINS := QrItsC170ALIQ_COFINS_p.Value;
  COFINS_qBCProd := QrItsC170QUANT_BC_COFINS.Value;
  COFINS_vAliqProd := QrItsC170ALIQ_COFINS_r.Value;
  COFINS_vCOFINS := QrItsC170VL_COFINS.Value;
  //
  if (COFINS_vBC >= 0.01) or (COFINS_pCOFINS >= 0.01) or (COFINS_qBCProd >= 0.01)
  or (COFINS_vAliqProd >= 0.01) or (COFINS_vCOFINS >= 0.01) then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsq', False, [
    'COFINS_CST', 'COFINS_vBC', 'COFINS_pCOFINS',
    'COFINS_vCOFINS', 'COFINS_qBCProd', 'COFINS_vAliqProd'(*,
    '_Ativo_', 'COFINS_fatorBC'*)], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    COFINS_CST, COFINS_vBC, COFINS_pCOFINS,
    COFINS_vCOFINS, COFINS_qBCProd, COFINS_vAliqProd(*,
    _Ativo_, COFINS_fatorBC*)], [
    FatID, FatNum, Empresa, nItem], True);
  end else
    Result := True;
end;

procedure TFmSPED_EFD_Importa.QrErr0150AfterOpen(DataSet: TDataSet);
begin
  BtAtrela.Enabled := QrErr0150.RecordCount > 0;
  BtCadEnt.Enabled := QrErr0150.RecordCount > 0;
end;

procedure TFmSPED_EFD_Importa.QrErr0150BeforeClose(DataSet: TDataSet);
begin
  BtAtrela.Enabled := False;
  BtCadEnt.Enabled := False;
  QrErrNFs.Close;
end;

procedure TFmSPED_EFD_Importa.QrErr0200AfterScroll(DataSet: TDataSet);
begin
  ReopenErrNFs();
end;

function TFmSPED_EFD_Importa.QuantidadeDeCamposNaoConferem(Versao: Integer;
  REG: String; Campos: Integer): Boolean;
var
  Campo: Integer;
begin
  case Versao of
    3,4:
    begin
      if REG = '0000' then Campo := 15 else
      if REG = '0001' then Campo := 02 else
      if REG = '0005' then Campo := 10 else
      if REG = '0100' then Campo := 14 else
      if REG = '0150' then Campo := 13 else
      if REG = '0190' then Campo := 03 else
      if REG = '0200' then Campo := 12 else
      if REG = '0400' then Campo := 03 else
      if REG = '0990' then Campo := 02 else
      if REG = 'C001' then Campo := 02 else
      if REG = 'C100' then Campo := 29 else
      if REG = 'C170' then Campo := 37 else
      if REG = 'C190' then Campo := 12 else
      if REG = 'C500' then Campo := 27 else
      if REG = 'C590' then Campo := 11 else
      if REG = 'C990' then Campo := 02 else
      if REG = 'D001' then Campo := 02 else
      if REG = 'D100' then Campo := 23 else
      if REG = 'D190' then Campo := 09 else
      if REG = 'D500' then Campo := 24 else
      if REG = 'D590' then Campo := 11 else
      if REG = 'D990' then Campo := 02 else
      if REG = 'E001' then Campo := 02 else
      if REG = 'E100' then Campo := 03 else
      if REG = 'E110' then Campo := 15 else
      if REG = 'E111' then Campo := 04 else
      if REG = 'E116' then
      begin
        case Versao of
          3: Campo := 09;
          4: Campo := 10;
          else Campo := 0;
        end;
      end else
      if REG = 'E990' then Campo := 02 else
      if Copy(REG, 1, 1) = 'G' then
      begin
        case Versao of 
          4:
          begin
            if REG = 'G001' then Campo := 02 else
            if REG = 'G990' then Campo := 02 else
          end;
          else Campo := 0;
        end;
      end else
      if REG = 'H001' then Campo := 02 else
      if REG = 'H001' then Campo := 02 else
      if REG = 'H005' then Campo := 03 else
      if REG = 'H010' then Campo := 10 else
      if REG = 'H990' then Campo := 02 else
      if REG = '1001' then Campo := 02 else
      if REG = '1990' then Campo := 02 else
      if REG = '9001' then Campo := 02 else
      if REG = '9900' then Campo := 03 else
      if REG = '9990' then Campo := 02 else
      if REG = '9999' then Campo := 02 else
      Campo := 0;
    end;
    else Campo := 0;
  end;
  //
  Result := Campos <> Campo;
  if Result then
    Geral.MensagemBox('A quantidade de campos para o registro "' + REG +
    '" n�o confere para a vers�o ' + FormatFloat('000', Versao) + sLineBreak +
    'Quantidade de campos esperada:    ' + FormatFloat('00', Campo) + sLineBreak +
    'Quantidade de campos encontrada: ' + FormatFloat('00', Campos),
    'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmSPED_EFD_Importa.ReopenErrNFs();
begin
  QrErrNFs.Close;
  QrErrNFs.SQL.Clear;
  QrErrNFs.SQL.Add('SELECT c100.LinArq, c100.COD_PART, c100.COD_MOD,');
  QrErrNFs.SQL.Add('c100.COD_SIT, c100.SER, c100.NUM_DOC, c100.DT_DOC,');
  QrErrNFs.SQL.Add('c100.DT_E_S, _0150.Entidade, _0150.NOME, ');
  QrErrNFs.SQL.Add('CONCAT(_0150.CNPJ,_0150.CPF) DOC_PART,');
  QrErrNFs.SQL.Add('c170.COD_ITEM, c170.QTD, c170.UNID, c170.VL_ITEM');
  QrErrNFs.SQL.Add('FROM spedefdc170 c170');
  QrErrNFs.SQL.Add('LEFT JOIN spedefdc100 c100 ON c100.LinArq=c170.C100');
  QrErrNFs.SQL.Add('  AND c100.ImporExpor=:P0');
  QrErrNFs.SQL.Add('  AND c100.AnoMes=:P1');
  QrErrNFs.SQL.Add('  AND c100.Empresa=:P2');
  QrErrNFs.SQL.Add('LEFT JOIN spedefd0150 _0150 ON _0150.COD_PART=c100.COD_PART');
  QrErrNFs.SQL.Add('  AND _0150.ImporExpor=:P3');
  QrErrNFs.SQL.Add('  AND _0150.AnoMes=:P4');
  QrErrNFs.SQL.Add('  AND _0150.Empresa=:P5');
  QrErrNFs.SQL.Add('WHERE c170.ImporExpor=:P6');
  QrErrNFs.SQL.Add('AND c170.AnoMes=:P7');
  QrErrNFs.SQL.Add('AND c170.Empresa=:P8');
  QrErrNFs.SQL.Add('AND c170.COD_ITEM=:P9');

  QrErrNFs.Params[00].AsInteger := FImporExpor;
  QrErrNFs.Params[01].AsInteger := QrErr0200AnoMes.Value;
  QrErrNFs.Params[02].AsInteger := QrErr0200Empresa.Value;

  QrErrNFs.Params[03].AsInteger := FImporExpor;
  QrErrNFs.Params[04].AsInteger := QrErr0200AnoMes.Value;
  QrErrNFs.Params[05].AsInteger := QrErr0200Empresa.Value;

  QrErrNFs.Params[06].AsInteger := FImporExpor;
  QrErrNFs.Params[07].AsInteger := QrErr0200AnoMes.Value;
  QrErrNFs.Params[08].AsInteger := QrErr0200Empresa.Value;

  QrErrNFs.Params[09].AsString  := QrErr0200COD_ITEM.Value;
  UnDmkDAC_PF.AbreQuery(QrErrNFs, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmSPED_EFD_Importa.SpeedButton1Click(Sender: TObject);
begin
  PreparaCarregamento();
end;

procedure TFmSPED_EFD_Importa.SpeedButton2Click(Sender: TObject);
const
  Tit = 'Reabertura de tabelas com erros de importa��o';
var
  Entidade, AnoMes: Integer;
begin
  Entidade := EdErrEnt.ValueVariant;
  AnoMes := EdErrAM.ValueVariant;
  //
  UMyMod.ReabreQuery(QrErr0150, Dmod.MyDB, [FImporExpor, AnoMes, Entidade], Tit);
  if QrErr0150.RecordCount > 0 then
  begin
    PageControl2.ActivePageIndex := 0;
    Exit;
  end;
  //
  UMyMod.ReabreQuery(QrErr0190, Dmod.MyDB, [FImporExpor, AnoMes, Entidade], Tit);
  if QrErr0190.RecordCount > 0 then
  begin
    PageControl2.ActivePageIndex := 1;
    Exit;
  end;
  //
  UMyMod.ReabreQuery(QrErr0200, Dmod.MyDB, [FImporExpor, AnoMes, Entidade], Tit);
  if QrErr0200.RecordCount > 0 then
  begin
    PageControl2.ActivePageIndex := 2;
    Exit;
  end;
  //
  UMyMod.ReabreQuery(QrErrC170, Dmod.MyDB, [FImporExpor, AnoMes, Entidade], Tit);
  if QrErrC170.RecordCount > 0 then
  begin
    PageControl2.ActivePageIndex := 3;
    Exit;
  end;
  //
end;

procedure TFmSPED_EFD_Importa.SpeedButton3Click(Sender: TObject);
begin
  if PageControl2.ActivePageIndex = 2 then
    MyObjects.frxMostra(frxErr_0200, 'Erros de importa��o de produtos')
  else
    Geral.MensagemBox('Relat�rio n�o implementado para:' + sLineBreak +
    TTabSheet(PageControl2.ActivePage).Caption + sLineBreak +
    'Para uma impress�o de emerg�ncia clique na grade desejada e pressione a tecla "Prnt Scrn"',
    'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmSPED_EFD_Importa.SpeedButton4Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxSPEDEFD_IMPORT_003_00,
    'Advert�ncias de produtos em importa��o SPED_EFD');
end;

procedure TFmSPED_EFD_Importa.SpeedButton7Click(Sender: TObject);
begin
  if MyObjects.DefineArquivo1(Self, EdSPED_EFD_Path) then
    PreparaCarregamento();
end;

function TFmSPED_EFD_Importa.ValidaPeriodoInteiroDeUmMes(const DtI, DtF:
TDateTime): Boolean;
var
  //AnoIa, MesIa, DiaIa,
  AnoIi, MesIi, DiaIi,
  AnoFf, AnoFd, MesFf, MesFd, DiaFf, DiaFd: Word;
begin
  FAnoMes := 0;
  Result := False;
  DecodeDate(DtI, AnoIi, MesIi, DiaIi);
  //DecodeDate(DtI-1, AnoIi, MesIi, DiaIi);
  DecodeDate(DtF, AnoFf, MesFf, DiaFf);
  DecodeDate(DtF + 1, AnoFd, MesFd, DiaFd);
  FAnoMes := AnoIi * 100 + MesIi;
  //
  if DiaIi <> 1 then
    Geral.MensagemBox(
    'Carregamento cancelado! Dia inicial deve ser o primeiro dia (dia 1) do m�s,'
    + sLineBreak +
    'mesmo que as atividades tenham se iniciado ap�s o dia primeiro!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else if DiaFd <> 1 then
    Geral.MensagemBox(
    'Carregamento cancelado! Dia final deve ser o �ltimo dia do m�s,'
    + sLineBreak +
    'mesmo que as atividades tenham sido finalizados antes do �ltimo dia do m�s!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else if MesIi <> MesFf then
    Geral.MensagemBox(
    'Carregamento cancelado! O per�odo deve ser de um �nico m�s!' + sLineBreak +
    'Data inicial selecionada:  ' + Geral.FDT(DtI, 3) + sLineBreak +
    'Data Final selecionada:   ' + Geral.FDT(DtF, 3),
    'Aviso', MB_OK+MB_ICONWARNING)
  else if FAnoMes < 200001 then
    Geral.MensagemBox(
    'Carregamento cancelado! O per�odo deve ser de um ano ap�s 2000!' + sLineBreak +
    'Data inicial selecionada:  ' + Geral.FDT(DtI, 3) + sLineBreak +
    'Data Final selecionada:   ' + Geral.FDT(DtF, 3),
    'Aviso', MB_OK+MB_ICONWARNING)
  else
    Result := True;
end;

procedure TFmSPED_EFD_Importa.VerificaBtImporta();
const
  Tit = 'Abertura de tabelas a procura de falta de cadastro';
begin
  UMyMod.ReabreQuery(QrErr0150, Dmod.MyDB, [FImporExpor, FAnoMes, FEmpresa], Tit);
  UMyMod.ReabreQuery(QrErr0190, Dmod.MyDB, [FImporExpor, FAnoMes, FEmpresa], Tit);
  UMyMod.ReabreQuery(QrErr0200, Dmod.MyDB, [FImporExpor, FAnoMes, FEmpresa], Tit);
{
  BtImporta.Enabled := FCarregou and (FEmpresa <> 0) and (FAnoMes <> 0) and
    (QrErr0150.State <> dsInactive) and (QrErr0150.RecordCount = 0) and
    (QrErr0190.State <> dsInactive) and (QrErr0190.RecordCount = 0) and
    (QrErr0200.State <> dsInactive) and (QrErr0200.RecordCount = 0)
    ;
}
end;
{
ADAPTADOR FLANGE 60MM X 2''
  //fazer QrErrNFs para C170 e D e etc?
 { TODO : colocar produtos em grade para comparar Nomes! }
end.

