object FmEFD_D500: TFmEFD_D500
  Left = 339
  Top = 185
  Caption = 'EFD-SPEDD-500 :: Comunica'#231#227'o e Telecomunica'#231#227'o'
  ClientHeight = 672
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 405
        Height = 32
        Caption = 'Comunica'#231#227'o e Telecomunica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 405
        Height = 32
        Caption = 'Comunica'#231#227'o e Telecomunica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 405
        Height = 32
        Caption = 'Comunica'#231#227'o e Telecomunica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 602
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 561
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 145
        Height = 53
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object BtOK: TBitBtn
          Tag = 14
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtOKClick
        end
      end
      object GroupBox2: TGroupBox
        Left = 145
        Top = 0
        Width = 491
        Height = 53
        Align = alClient
        Caption = ' Dados do Per'#237'odo: '
        Enabled = False
        TabOrder = 1
        object Label3: TLabel
          Left = 112
          Top = 12
          Width = 56
          Height = 13
          Caption = 'ImporExpor:'
        end
        object Label4: TLabel
          Left = 176
          Top = 12
          Width = 42
          Height = 13
          Caption = 'AnoMes:'
        end
        object Label5: TLabel
          Left = 232
          Top = 12
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label6: TLabel
          Left = 280
          Top = 12
          Width = 33
          Height = 13
          Caption = 'LinArq:'
        end
        object EdImporExpor: TdmkEdit
          Left = 112
          Top = 28
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ImporExpor'
          UpdCampo = 'ImporExpor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAnoMes: TdmkEdit
          Left = 176
          Top = 28
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'AnoMes'
          UpdCampo = 'AnoMes'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdEmpresa: TdmkEdit
          Left = 232
          Top = 28
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Empresa'
          UpdCampo = 'Empresa'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdLinArq: TdmkEdit
          Left = 280
          Top = 28
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'LinArq'
          UpdCampo = 'LinArq'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 510
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 469
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 510
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 469
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 510
        Align = alClient
        Caption = ' Dados da Nota Fiscal: '
        TabOrder = 0
        ExplicitHeight = 469
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 408
            Top = 4
            Width = 57
            Height = 13
            Caption = 'Fornecedor:'
          end
          object RGIND_OPER: TdmkRadioGroup
            Left = 0
            Top = 0
            Width = 172
            Height = 46
            Align = alLeft
            Caption = ' Indicador do Tipo de Opera'#231#227'o: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              '0 - Entrada'
              '1 - Sa'#237'da')
            TabOrder = 0
            QryCampo = 'IND_OPER'
            UpdType = utYes
            OldValor = 0
          end
          object RGIND_EMIT: TdmkRadioGroup
            Left = 172
            Top = 0
            Width = 232
            Height = 46
            Align = alLeft
            Caption = ' Indicador do Emitente do Documento Fiscal: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              '0 - Emiss'#227'o pr'#243'pria'
              '1 - Terceiros')
            TabOrder = 1
            QryCampo = 'IND_EMIT'
            UpdType = utYes
            OldValor = 0
          end
          object EdTerceiro: TdmkEditCB
            Left = 408
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Terceiro'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBTerceiro
            IgnoraDBLookupComboBox = False
          end
          object CBTerceiro: TdmkDBLookupComboBox
            Left = 464
            Top = 20
            Width = 309
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NO_ENT'
            ListSource = DsTerceiros
            TabOrder = 3
            dmkEditCB = EdTerceiro
            QryCampo = 'Terceiro'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object Panel7: TPanel
          Left = 2
          Top = 61
          Width = 780
          Height = 44
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object GroupBox3: TGroupBox
            Left = 0
            Top = 0
            Width = 388
            Height = 44
            Align = alLeft
            Caption = ' C'#243'digo do Modelo do Documento Fiscal: '
            TabOrder = 0
            object EdCOD_MOD: TdmkEditCB
              Left = 4
              Top = 16
              Width = 29
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'COD_MOD'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              DBLookupComboBox = CBCOD_MOD
              IgnoraDBLookupComboBox = False
            end
            object CBCOD_MOD: TdmkDBLookupComboBox
              Left = 35
              Top = 16
              Width = 344
              Height = 21
              KeyField = 'CodTxt'
              ListField = 'Nome'
              ListSource = DsTbSPEDEFD017
              TabOrder = 1
              dmkEditCB = EdCOD_MOD
              QryCampo = 'COD_MOD'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object GroupBox4: TGroupBox
            Left = 388
            Top = 0
            Width = 388
            Height = 44
            Align = alLeft
            Caption = ' C'#243'digo da Situa'#231#227'o do Documento Fiscal: '
            TabOrder = 1
            object EdCOD_SIT: TdmkEditCB
              Left = 4
              Top = 16
              Width = 29
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'COD_SIT'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              DBLookupComboBox = CBCOD_SIT
              IgnoraDBLookupComboBox = False
            end
            object CBCOD_SIT: TdmkDBLookupComboBox
              Left = 35
              Top = 16
              Width = 344
              Height = 21
              KeyField = 'CodTxt'
              ListField = 'Nome'
              ListSource = DsTbSPEDEFD018
              TabOrder = 1
              dmkEditCB = EdCOD_SIT
              QryCampo = 'COD_SIT'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
        object Panel8: TPanel
          Left = 2
          Top = 105
          Width = 780
          Height = 40
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object Label2: TLabel
            Left = 4
            Top = 0
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object Label7: TLabel
            Left = 40
            Top = 0
            Width = 22
            Height = 13
            Caption = 'Sub:'
          end
          object Label9: TLabel
            Left = 72
            Top = 0
            Width = 47
            Height = 13
            Caption = 'N'#186' da NF:'
          end
          object Label10: TLabel
            Left = 140
            Top = 0
            Width = 82
            Height = 13
            Caption = 'Data de emiss'#227'o:'
          end
          object Label11: TLabel
            Left = 256
            Top = 0
            Width = 71
            Height = 13
            Caption = 'Data sai/entra:'
          end
          object EdSER: TdmkEdit
            Left = 4
            Top = 16
            Width = 32
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SER'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSUB: TdmkEdit
            Left = 40
            Top = 16
            Width = 28
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SUB'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdNUM_DOC: TdmkEdit
            Left = 72
            Top = 16
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'NUM_DOC'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object TPDT_DOC: TdmkEditDateTimePicker
            Left = 140
            Top = 16
            Width = 112
            Height = 21
            Date = 40769.049742349540000000
            Time = 40769.049742349540000000
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DT_DOC'
            UpdCampo = 'DT_DOC'
            UpdType = utYes
          end
          object TPDT_A_P: TdmkEditDateTimePicker
            Left = 256
            Top = 16
            Width = 112
            Height = 21
            Date = 40769.049742349540000000
            Time = 40769.049742349540000000
            TabOrder = 4
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DT_A_P'
            UpdCampo = 'DT_E_S'
            UpdType = utYes
          end
        end
        object Panel10: TPanel
          Left = 2
          Top = 145
          Width = 780
          Height = 264
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
          object Panel11: TPanel
            Left = 416
            Top = 0
            Width = 364
            Height = 264
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object RGTP_ASSINANTE: TdmkRadioGroup
              Left = 0
              Top = 0
              Width = 364
              Height = 264
              Align = alClient
              Caption = ' C'#243'digo do Tipo de Assinante: '
              Enabled = False
              Items.Strings = (
                '0 - Nenhum'
                '1 - Comercial/Industrial'
                '2 - Poder P'#250'blico'
                '3 - Residencial/Pessoa f'#237'sica'
                '4 - P'#250'blico'
                '5 - Semi-P'#250'blico'
                '6 - Outros')
              TabOrder = 0
              QryCampo = 'TP_ASSINANTE'
              UpdType = utYes
              OldValor = 0
            end
          end
          object GroupBox5: TGroupBox
            Left = 0
            Top = 0
            Width = 416
            Height = 264
            Align = alLeft
            Caption = ' Valores: '
            TabOrder = 1
            object Panel9: TPanel
              Left = 2
              Top = 15
              Width = 416
              Height = 247
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label12: TLabel
                Left = 8
                Top = 8
                Width = 145
                Height = 13
                Caption = 'Valor total do documento fiscal'
              end
              object Label13: TLabel
                Left = 8
                Top = 32
                Width = 109
                Height = 13
                Caption = 'Valor total do desconto'
              end
              object Label14: TLabel
                Left = 8
                Top = 56
                Width = 150
                Height = 13
                Caption = 'Valor total fornecido/consumido'
              end
              object Label15: TLabel
                Left = 8
                Top = 80
                Width = 246
                Height = 13
                Caption = 'Valor  total  dos  servi'#231'os  n'#227'o-tributados  pelo ICMS'
              end
              object Label16: TLabel
                Left = 8
                Top = 104
                Width = 193
                Height = 13
                Caption = 'Valor total cobrado em nome de terceiros'
              end
              object Label17: TLabel
                Left = 8
                Top = 128
                Width = 309
                Height = 13
                Caption = 'Valor total de despesas acess'#243'rias indicadas no documento fiscal'
              end
              object Label18: TLabel
                Left = 8
                Top = 152
                Width = 216
                Height = 13
                Caption = 'Valor acumulado da base de c'#225'lculo do ICMS'
              end
              object Label19: TLabel
                Left = 8
                Top = 176
                Width = 123
                Height = 13
                Caption = 'Valor acumulado do ICMS'
              end
              object Label22: TLabel
                Left = 8
                Top = 200
                Width = 59
                Height = 13
                Caption = 'Valor do PIS'
              end
              object Label23: TLabel
                Left = 8
                Top = 224
                Width = 81
                Height = 13
                Caption = 'Valor da COFINS'
              end
              object EdVL_DOC: TdmkEdit
                Left = 332
                Top = 4
                Width = 76
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'VL_DOC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdVL_DESC: TdmkEdit
                Left = 332
                Top = 28
                Width = 76
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'VL_DESC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdVL_SERV: TdmkEdit
                Left = 332
                Top = 52
                Width = 76
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'VL_SERV'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdVL_SERV_NT: TdmkEdit
                Left = 332
                Top = 76
                Width = 76
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'VL_SERV_NT'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdVL_TERC: TdmkEdit
                Left = 332
                Top = 100
                Width = 76
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'VL_TERC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdVL_DA: TdmkEdit
                Left = 332
                Top = 124
                Width = 76
                Height = 21
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'VL_DA'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdVL_BC_ICMS: TdmkEdit
                Left = 332
                Top = 148
                Width = 76
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'VL_BC_ICMS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdVL_ICMS: TdmkEdit
                Left = 332
                Top = 172
                Width = 76
                Height = 21
                Alignment = taRightJustify
                TabOrder = 7
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'VL_ICMS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdVL_PIS: TdmkEdit
                Left = 332
                Top = 196
                Width = 76
                Height = 21
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'VL_PIS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdVL_COFINS: TdmkEdit
                Left = 332
                Top = 220
                Width = 76
                Height = 21
                Alignment = taRightJustify
                TabOrder = 9
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'VL_COFINS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
        end
        object GroupBox6: TGroupBox
          Left = 2
          Top = 409
          Width = 780
          Height = 99
          Align = alClient
          Caption = ' Recupera'#231#227'o de ICMS: '
          TabOrder = 4
          ExplicitHeight = 58
          object Label24: TLabel
            Left = 8
            Top = 16
            Width = 24
            Height = 13
            Caption = 'CST:'
          end
          object Label25: TLabel
            Left = 644
            Top = 16
            Width = 41
            Height = 13
            Caption = 'Aliquota:'
          end
          object Label26: TLabel
            Left = 692
            Top = 16
            Width = 52
            Height = 13
            Caption = '$ Red. BC:'
          end
          object Label27: TLabel
            Left = 8
            Top = 56
            Width = 31
            Height = 13
            Caption = 'CFOP:'
          end
          object EdCST_ICMS: TdmkEditCB
            Left = 8
            Top = 32
            Width = 36
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 3
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'CST_ICMS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            DBLookupComboBox = CBCST_ICMS
            IgnoraDBLookupComboBox = False
          end
          object CBCST_ICMS: TdmkDBLookupComboBox
            Left = 43
            Top = 32
            Width = 594
            Height = 21
            KeyField = 'CodTxt'
            ListField = 'Nome'
            ListSource = DsCST_ICMS
            TabOrder = 1
            dmkEditCB = EdCST_ICMS
            QryCampo = 'CST_ICMS'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdALIQ_ICMS: TdmkEdit
            Left = 644
            Top = 32
            Width = 44
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ALIQ_ICMS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdVL_RED_BC: TdmkEdit
            Left = 692
            Top = 32
            Width = 76
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'VL_RED_BC'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCFOP: TdmkEditCB
            Left = 8
            Top = 72
            Width = 40
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 4
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'CFOP'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            DBLookupComboBox = CBCFOP
            IgnoraDBLookupComboBox = False
          end
          object CBCFOP: TdmkDBLookupComboBox
            Left = 47
            Top = 72
            Width = 722
            Height = 21
            KeyField = 'CodTxt'
            ListField = 'Nome'
            ListSource = DsTbSPEDEFD002
            TabOrder = 5
            dmkEditCB = EdCFOP
            QryCampo = 'CFOP'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 558
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitTop = 517
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrTerceiros: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades ent '
      'ORDER BY NO_ENT'
      '')
    Left = 620
    Top = 156
    object QrTerceirosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTerceirosNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsTerceiros: TDataSource
    DataSet = QrTerceiros
    Left = 620
    Top = 204
  end
  object QrTbSPEDEFD017: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM tbspedefd017'
      'ORDER BY Nome')
    Left = 620
    Top = 252
    object QrTbSPEDEFD017CodTxt: TWideStringField
      FieldName = 'CodTxt'
    end
    object QrTbSPEDEFD017Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsTbSPEDEFD017: TDataSource
    DataSet = QrTbSPEDEFD017
    Left = 620
    Top = 300
  end
  object QrTbSPEDEFD018: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM tbspedefd018'
      'ORDER BY Nome')
    Left = 620
    Top = 344
    object QrTbSPEDEFD018CodTxt: TWideStringField
      FieldName = 'CodTxt'
    end
    object QrTbSPEDEFD018Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsTbSPEDEFD018: TDataSource
    DataSet = QrTbSPEDEFD018
    Left = 620
    Top = 392
  end
  object QrCST_ICMS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT(t024.CodTxt, t025.CodTxt) CST, '
      't024.Nome NO_CST_A, t025.Nome NO_CST_B'
      'FROM tbspedefd024 t024'
      'INNER JOIN tbspedefd025 t025'
      'ORDER BY t024.Nome, t025.Nome')
    Left = 720
    Top = 156
    object QrCST_ICMSCodTxt: TWideStringField
      DisplayWidth = 60
      FieldName = 'CodTxt'
      Size = 60
    end
    object QrCST_ICMSNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object QrTbSPEDEFD002: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT CodTxt, Nome'
      'FROM tbspedefd002'
      'ORDER BY Nome')
    Left = 720
    Top = 252
    object QrTbSPEDEFD002CodTxt: TWideStringField
      FieldName = 'CodTxt'
    end
    object QrTbSPEDEFD002Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCST_ICMS: TDataSource
    DataSet = QrCST_ICMS
    Left = 720
    Top = 204
  end
  object DsTbSPEDEFD002: TDataSource
    DataSet = QrTbSPEDEFD002
    Left = 720
    Top = 300
  end
end
