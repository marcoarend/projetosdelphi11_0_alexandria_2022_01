unit SPEDEFDEnce;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkCheckBox,
  MyDBCheck, dmkMemo, UnComps_Vars;

type
  TFmSPEDEFDEnce = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrSPEDEFDEnce: TmySQLQuery;
    DsSPEDEFDEnce: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdAnoMes: TdmkEdit;
    Label1: TLabel;
    DBEdAnoMes: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdNome: TdmkEdit;
    Label9: TLabel;
    CkMovimVS: TdmkCheckBox;
    CkGerLibEFD: TdmkCheckBox;
    CkEnvioEFD: TdmkCheckBox;
    CkEncerraEFD: TdmkCheckBox;
    QrSPEDEFDEnceAnoMes: TIntegerField;
    QrSPEDEFDEnceEmpresa: TIntegerField;
    QrSPEDEFDEnceNome: TWideStringField;
    QrSPEDEFDEnceMovimVS: TSmallintField;
    QrSPEDEFDEnceGerLibEFD: TSmallintField;
    QrSPEDEFDEnceEnvioEFD: TSmallintField;
    QrSPEDEFDEnceEncerraEFD: TSmallintField;
    QrSPEDEFDEnceLk: TIntegerField;
    QrSPEDEFDEnceDataCad: TDateField;
    QrSPEDEFDEnceDataAlt: TDateField;
    QrSPEDEFDEnceUserCad: TIntegerField;
    QrSPEDEFDEnceUserAlt: TIntegerField;
    QrSPEDEFDEnceAlterWeb: TSmallintField;
    QrSPEDEFDEnceAtivo: TSmallintField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    dmkCheckBox1: TDBCheckBox;
    dmkCheckBox2: TDBCheckBox;
    dmkCheckBox3: TDBCheckBox;
    dmkCheckBox4: TDBCheckBox;
    DBEdit3: TDBEdit;
    QrSPEDEFDEnceNO_EMPRESA: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSPEDEFDEnceAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSPEDEFDEnceBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, AnoMes: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmSPEDEFDEnce: TFmSPEDEFDEnce;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, Periodo, UnSPED_EFD_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSPEDEFDEnce.LocCod(Atual, AnoMes: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, AnoMes);
end;

procedure TFmSPEDEFDEnce.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSPEDEFDEnceAnoMes.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSPEDEFDEnce.DefParams;
begin
  VAR_GOTOTABELA := 'spedefdicmsipience';
  VAR_GOTOMYSQLTABLE := QrSPEDEFDEnce;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := 'AnoMes';
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT see.*, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA  ');
  VAR_SQLx.Add('FROM spedefdicmsipience see ');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=see.Empresa ');
  VAR_SQLx.Add('WHERE see.AnoMes<>0 ');
  //
  VAR_SQL1.Add('AND see.AnoMes=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND see.Nome Like :P0');
  //
end;

procedure TFmSPEDEFDEnce.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSPEDEFDEnce.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSPEDEFDEnce.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSPEDEFDEnce.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSPEDEFDEnce.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSPEDEFDEnce.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSPEDEFDEnce.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSPEDEFDEnce.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSPEDEFDEnce.BtAlteraClick(Sender: TObject);
begin
  EdEmpresa.Enabled := False;
  CBEmpresa.Enabled := False;
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrSPEDEFDEnce, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'spedefdicmsipience');
  DModG.SetaFilialdeEntidade(QrSPEDEFDEnceEmpresa.Value, EdEmpresa, CBEmpresa);
end;

procedure TFmSPEDEFDEnce.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSPEDEFDEnceAnoMes.Value;
  Close;
end;

procedure TFmSPEDEFDEnce.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  AnoMes, Empresa, MovimVS, GerLibEFD, EnvioEFD, EncerraEFD: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  AnoMes         := EdAnoMes.ValueVariant;
  if MyObjects.FIC(AnoMes=0, EdAnoMes, 'Informe o ano e m�s!') then  Exit;
  Empresa        := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Empresa=0, EdEmpresa, 'Informe a empresa!') then  Exit;
  Empresa        :=  DModG.ObtemEntidadeDeFilial(Empresa);
  Nome           := EdNome.Text;
  MovimVS        := Geral.BoolToInt(CkMovimVS   .Checked);
  GerLibEFD      := Geral.BoolToInt(CkGerLibEFD .Checked);
  EnvioEFD       := Geral.BoolToInt(CkEnvioEFD  .Checked);
  EncerraEFD     := Geral.BoolToInt(CkEncerraEFD.Checked);
  //
  //AnoMes := UMyMod.BPGS1I32('spedefdicmsipience', 'AnoMes', 'Empresa', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'spedefdicmsipience', False, [
  'Nome', 'MovimVS', 'GerLibEFD',
  'EnvioEFD', 'EncerraEFD'], [
  'AnoMes', 'Empresa'], [
  Nome, MovimVS, GerLibEFD,
  EnvioEFD, EncerraEFD], [
  AnoMes, Empresa], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    //
    VAR_Data_SPEDEFDEnce_MovimVS := SPED_EFD_PF.SPEDEFDEnce_DataMinMovim('MovimVS');
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(AnoMes, AnoMes);
  end;
end;

procedure TFmSPEDEFDEnce.BtDesisteClick(Sender: TObject);
var
  AnoMes: Integer;
begin
  AnoMes := EdAnoMes.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(AnoMes, Dmod.MyDB, 'spedefdicmsipience', 'AnoMes');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmSPEDEFDEnce.BtIncluiClick(Sender: TObject);
var
  Ano, Mes, Dia: Word;
  Cancelou, Continua: Boolean;
  AnoMes: Integer;
begin
  EdEmpresa.Enabled := True;
  CBEmpresa.Enabled := True;
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  MLAGeral.EscolhePeriodo_MesEAno(TFmPeriodo, FmPeriodo, Mes, Ano, Cancelou, True, True);
  if not Cancelou then
  begin
    AnoMes := (Ano * 100) + Mes;
    UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrSPEDEFDEnce, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'spedefdicmsipience');
    EdAnoMes.ValueVariant := AnoMes;
  end;
end;

procedure TFmSPEDEFDEnce.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmSPEDEFDEnce.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSPEDEFDEnceAnoMes.Value, LaRegistro.Caption);
end;

procedure TFmSPEDEFDEnce.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSPEDEFDEnce.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrSPEDEFDEnceAnoMes.Value, LaRegistro.Caption);
end;

procedure TFmSPEDEFDEnce.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSPEDEFDEnce.QrSPEDEFDEnceAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSPEDEFDEnce.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSPEDEFDEnce.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSPEDEFDEnceAnoMes.Value,
  CuringaLoc.CriaForm('AnoMes', CO_NOME, 'spedefdicmsipience', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSPEDEFDEnce.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSPEDEFDEnce.QrSPEDEFDEnceBeforeOpen(DataSet: TDataSet);
begin
  QrSPEDEFDEnceAnoMes.DisplayFormat := FFormatFloat;
end;

end.

