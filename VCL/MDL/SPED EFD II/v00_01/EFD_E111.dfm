object FmEFD_E111: TFmEFD_E111
  Left = 339
  Top = 185
  Caption = 'EFD-SPEDE-111 :: Ajuste/Benef'#237'cio/Incentivo da Apura'#231#227'o do ICMS'
  ClientHeight = 349
  ClientWidth = 701
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 701
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 653
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 605
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 588
        Height = 32
        Caption = 'Ajuste/Benef'#237'cio/Incentivo da Apura'#231#227'o do ICMS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 588
        Height = 32
        Caption = 'Ajuste/Benef'#237'cio/Incentivo da Apura'#231#227'o do ICMS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 588
        Height = 32
        Caption = 'Ajuste/Benef'#237'cio/Incentivo da Apura'#231#227'o do ICMS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 279
    Width = 701
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 555
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 553
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 701
    Height = 187
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 701
      Height = 187
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 61
        Width = 701
        Height = 126
        Align = alClient
        Caption = ' Dados do ajuste/benef'#237'cio/incentivo da apura'#231#227'o do ICMS: '
        TabOrder = 0
        object Label7: TStaticText
          Left = 56
          Top = 20
          Width = 469
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 
            '02. C'#243'digo do ajuste da apura'#231#227'o e dedu'#231#227'o, conforme a tabela in' +
            'dicada no item 5.1.1.'
          FocusControl = EdCOD_AJ_APUR
          TabOrder = 3
        end
        object Label8: TStaticText
          Left = 56
          Top = 44
          Width = 585
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '03. Descri'#231#227'o complementar do ajuste da apura'#231#227'o.'
          FocusControl = EdDESCR_COMPL_AJ
          TabOrder = 4
        end
        object Label10: TStaticText
          Left = 56
          Top = 92
          Width = 469
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '04. Valor do ajuste da apura'#231#227'o'
          FocusControl = EdVL_AJ_APUR
          TabOrder = 5
        end
        object EdCOD_AJ_APUR: TdmkEdit
          Left = 528
          Top = 20
          Width = 112
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'COD_AJ_APUR'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdDESCR_COMPL_AJ: TdmkEdit
          Left = 56
          Top = 68
          Width = 585
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'DESCR_COMPL_AJ'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdVL_AJ_APUR: TdmkEdit
          Left = 528
          Top = 92
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_AJ_APUR'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 701
        Height = 61
        Align = alTop
        Caption = ' Dados do intervalo: '
        Enabled = False
        TabOrder = 1
        object Label3: TLabel
          Left = 68
          Top = 16
          Width = 56
          Height = 13
          Caption = 'ImporExpor:'
        end
        object Label4: TLabel
          Left = 132
          Top = 16
          Width = 42
          Height = 13
          Caption = 'AnoMes:'
        end
        object Label5: TLabel
          Left = 188
          Top = 16
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label6: TLabel
          Left = 236
          Top = 16
          Width = 28
          Height = 13
          Caption = 'E110:'
        end
        object Label1: TLabel
          Left = 412
          Top = 16
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
        end
        object Label2: TLabel
          Left = 528
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Data final:'
        end
        object Label9: TLabel
          Left = 300
          Top = 16
          Width = 33
          Height = 13
          Caption = 'LinArq:'
        end
        object EdImporExpor: TdmkEdit
          Left = 68
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdAnoMes: TdmkEdit
          Left = 132
          Top = 32
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdEmpresa: TdmkEdit
          Left = 188
          Top = 32
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdE110: TdmkEdit
          Left = 236
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object TPDT_INI: TdmkEditDateTimePicker
          Left = 412
          Top = 32
          Width = 112
          Height = 21
          Date = 40761.337560104170000000
          Time = 40761.337560104170000000
          TabOrder = 4
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPDT_FIN: TdmkEditDateTimePicker
          Left = 528
          Top = 32
          Width = 112
          Height = 21
          Date = 40761.337560104170000000
          Time = 40761.337560104170000000
          TabOrder = 5
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdLinArq: TdmkEdit
          Left = 300
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 235
    Width = 701
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 697
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
end
