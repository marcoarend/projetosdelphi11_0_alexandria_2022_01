object FmSPED_EFD_Importa: TFmSPED_EFD_Importa
  Left = 339
  Top = 185
  Caption = 'SPE-EFDII-001 :: Importa'#231#227'o de Arquivo SPED EFD ICMS/IPI'
  ClientHeight = 691
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl0: TPageControl
    Left = 0
    Top = 105
    Width = 1008
    Height = 541
    ActivePage = TabSheet10
    Align = alClient
    TabOrder = 0
    object TabSheet10: TTabSheet
      Caption = 'Carregar dados do arquivo'
      object Panel13: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 49
        Align = alTop
        ParentBackground = False
        TabOrder = 0
        object Label83: TLabel
          Left = 8
          Top = 4
          Width = 166
          Height = 13
          Caption = 'Arquivo magn'#233'tico a ser importado:'
        end
        object SpeedButton7: TSpeedButton
          Left = 946
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton7Click
        end
        object SpeedButton1: TSpeedButton
          Left = 966
          Top = 20
          Width = 21
          Height = 21
          Caption = '>'
          OnClick = SpeedButton1Click
        end
        object EdSPED_EFD_Path: TdmkEdit
          Left = 8
          Top = 20
          Width = 937
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = 
            'C:\_MLArend\Clientes\Ideal - MJ Novaes\SPED\EFD\SPED EFD Enviado' +
            's\A_20100101_20100131.TXT'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 
            'C:\_MLArend\Clientes\Ideal - MJ Novaes\SPED\EFD\SPED EFD Enviado' +
            's\A_20100101_20100131.TXT'
          ValWarn = False
        end
      end
      object Panel14: TPanel
        Left = 0
        Top = 49
        Width = 1000
        Height = 464
        Align = alClient
        ParentBackground = False
        TabOrder = 1
        object PageControl1: TPageControl
          Left = 1
          Top = 1
          Width = 998
          Height = 462
          ActivePage = TabSheet12
          Align = alClient
          TabOrder = 0
          object TabSheet12: TTabSheet
            Caption = ' Carregamento'
            object MeImportado: TMemo
              Left = 0
              Top = 0
              Width = 990
              Height = 340
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              WordWrap = False
              OnChange = MeImportadoChange
            end
            object Panel15: TPanel
              Left = 0
              Top = 384
              Width = 990
              Height = 50
              Align = alBottom
              ParentBackground = False
              TabOrder = 1
              object BtCarrega: TBitBtn
                Left = 20
                Top = 4
                Width = 90
                Height = 40
                Caption = '&Carrega'
                Enabled = False
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtCarregaClick
              end
            end
            object Panel1: TPanel
              Left = 0
              Top = 340
              Width = 990
              Height = 44
              Align = alBottom
              ParentBackground = False
              TabOrder = 2
              object CkNomeGGXIgual: TCheckBox
                Left = 344
                Top = 4
                Width = 245
                Height = 17
                Caption = 'Nome do produto deve ser igual ao cadastrado.'
                Enabled = False
                TabOrder = 0
              end
              object CkSoTerceiros: TCheckBox
                Left = 344
                Top = 24
                Width = 221
                Height = 17
                Caption = 'Importar somente notas fiscais de terceiros.'
                Checked = True
                State = cbChecked
                TabOrder = 1
              end
              object CkMPConsumido: TCheckBox
                Left = 604
                Top = 4
                Width = 225
                Height = 17
                Caption = 'Considerar mat'#233'ria-prima como consumido.'
                Checked = True
                State = cbChecked
                TabOrder = 2
              end
              object CkUCConsumido: TCheckBox
                Left = 604
                Top = 24
                Width = 329
                Height = 17
                Caption = 'Considerar Uso e Consumo que n'#227'o for insumo como consumido.'
                Checked = True
                State = cbChecked
                TabOrder = 3
              end
              object RGConfig: TRadioGroup
                Left = 1
                Top = 1
                Width = 336
                Height = 42
                Align = alLeft
                Caption = ' Carregamento e importa'#231#227'o: '
                Columns = 2
                Enabled = False
                ItemIndex = 0
                Items.Strings = (
                  'Apenas carregar o arquivo'
                  'Carregar e importar')
                TabOrder = 4
              end
            end
          end
          object TabSheet13: TTabSheet
            Caption = ' Ignorados no carregamento '
            ImageIndex = 1
            object MeIgnor: TMemo
              Left = 0
              Top = 0
              Width = 990
              Height = 434
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
          object TabSheet14: TTabSheet
            Caption = ' Erros no carregamento '
            ImageIndex = 2
            object PageControl2: TPageControl
              Left = 0
              Top = 33
              Width = 990
              Height = 401
              ActivePage = TabSheet2
              Align = alClient
              TabOrder = 0
              ExplicitHeight = 458
              object TabSheet7: TTabSheet
                Caption = '0150: Entidades n'#227'o localizadas'
                object DBGrid5: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 982
                  Height = 380
                  Align = alClient
                  DataSource = DsErr0150
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                end
                object Panel7: TPanel
                  Left = 0
                  Top = 380
                  Width = 982
                  Height = 50
                  Align = alBottom
                  ParentBackground = False
                  TabOrder = 1
                  object BtAtrela: TBitBtn
                    Left = 8
                    Top = 4
                    Width = 90
                    Height = 40
                    Caption = '&Atrela'
                    Enabled = False
                    NumGlyphs = 2
                    TabOrder = 0
                    OnClick = BtAtrelaClick
                  end
                  object BtCadEnt: TBitBtn
                    Left = 100
                    Top = 4
                    Width = 90
                    Height = 40
                    Caption = '&Cadastra'
                    Enabled = False
                    NumGlyphs = 2
                    TabOrder = 1
                    OnClick = BtCadEntClick
                  end
                end
              end
              object TabSheet16: TTabSheet
                Caption = '0190: Unidades de medida n'#227'o localizadas'
                ImageIndex = 1
                object DBGrid7: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 982
                  Height = 430
                  Align = alClient
                  DataSource = DsErr0190
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                end
              end
              object TabSheet17: TTabSheet
                Caption = '0200: Produtos n'#227'o localizados'
                ImageIndex = 2
                object DBGrid8: TDBGrid
                  Left = 0
                  Top = 229
                  Width = 982
                  Height = 201
                  Align = alClient
                  DataSource = DsErrNFs
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'LinArq'
                      Width = 40
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_PART'
                      Width = 112
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_MOD'
                      Title.Caption = 'MOD'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SER'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NUM_DOC'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DT_DOC'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DT_E_S'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Entidade'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NOME'
                      Width = 200
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DOC_PART'
                      Width = 112
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_ITEM'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QTD'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'UNIT'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_ITEM'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_SIT'
                      Visible = True
                    end>
                end
                object DBGrid2: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 982
                  Height = 209
                  Align = alTop
                  DataSource = DsErr0200
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Receptor'
                      Title.Caption = 'C'#243'd. substituto'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_ITEM'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DESCR_ITEM'
                      Width = 393
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'UNID_INV'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'TIPO_ITEM'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_NCM'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'EX_IPI'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_GEN'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_LST'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ALIQ_ICMS'
                      Visible = True
                    end>
                end
                object Panel2: TPanel
                  Left = 0
                  Top = 209
                  Width = 982
                  Height = 20
                  Align = alTop
                  Caption = 'Notas Fiscais que tem o produto selecionado acima'
                  ParentBackground = False
                  TabOrder = 2
                end
              end
              object TabSheet2: TTabSheet
                Caption = 'C170: Itens n'#227'o localizados'
                ImageIndex = 3
                object DBGrid1: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 982
                  Height = 373
                  Align = alClient
                  DataSource = DsErrC170
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                end
              end
            end
            object Panel3: TPanel
              Left = 0
              Top = 0
              Width = 990
              Height = 33
              Align = alTop
              ParentBackground = False
              TabOrder = 1
              object Label1: TLabel
                Left = 16
                Top = 12
                Width = 44
                Height = 13
                Caption = 'Empresa:'
              end
              object Label2: TLabel
                Left = 140
                Top = 12
                Width = 154
                Height = 13
                Caption = 'Ano / m'#234's no formato AAAAMM:'
              end
              object SpeedButton2: TSpeedButton
                Left = 346
                Top = 8
                Width = 21
                Height = 21
                Caption = '>'
                OnClick = SpeedButton2Click
              end
              object SpeedButton3: TSpeedButton
                Left = 382
                Top = 8
                Width = 63
                Height = 21
                Caption = 'Imprimir'
                OnClick = SpeedButton3Click
              end
              object EdErrEnt: TdmkEdit
                Left = 64
                Top = 8
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '-11'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = -11
                ValWarn = False
              end
              object EdErrAM: TdmkEdit
                Left = 296
                Top = 8
                Width = 45
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 6
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '201103'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 201103
                ValWarn = False
              end
            end
          end
          object TabSheet1: TTabSheet
            Caption = ' Advert'#234'ncias no carregamento '
            ImageIndex = 3
            object PageControl3: TPageControl
              Left = 0
              Top = 33
              Width = 990
              Height = 401
              ActivePage = TabSheet3
              Align = alClient
              TabOrder = 0
              ExplicitHeight = 458
              object TabSheet3: TTabSheet
                Caption = 'Produtos com nomes distantes'
                object DBGrid3: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 982
                  Height = 373
                  Align = alClient
                  DataSource = DsAdver_0200
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'LinArq'
                      Title.Caption = 'Linha'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Distancia'
                      Title.Caption = 'Dist'#226'ncia'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Reduzido'
                      Width = 65
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Nome'
                      Title.Caption = 'Nome do produto'
                      Visible = True
                    end>
                end
              end
            end
            object Panel4: TPanel
              Left = 0
              Top = 0
              Width = 990
              Height = 33
              Align = alTop
              ParentBackground = False
              TabOrder = 1
              object SpeedButton4: TSpeedButton
                Left = 2
                Top = 4
                Width = 63
                Height = 21
                Caption = 'Imprimir'
                OnClick = SpeedButton4Click
              end
            end
          end
        end
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 646
    Width = 1008
    Height = 45
    Align = alBottom
    Caption = ' Dados da Empresa: '
    TabOrder = 1
    object Label11: TLabel
      Left = 8
      Top = 20
      Width = 30
      Height = 13
      Caption = 'CNPJ:'
    end
    object Label12: TLabel
      Left = 164
      Top = 20
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label13: TLabel
      Left = 264
      Top = 20
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object EdEmp_CNPJ: TdmkEdit
      Left = 40
      Top = 16
      Width = 112
      Height = 21
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdEmp_Codigo: TdmkEdit
      Left = 204
      Top = 16
      Width = 56
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEmp_CodigoChange
    end
    object EdEmp_Nome: TdmkEdit
      Left = 300
      Top = 16
      Width = 697
      Height = 21
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        Visible = False
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        Visible = False
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 534
        Height = 32
        Caption = 'Importa'#231#227'o de Arquivo SPED EFD ICMS/IPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 534
        Height = 32
        Caption = 'Importa'#231#227'o de Arquivo SPED EFD ICMS/IPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 534
        Height = 32
        Caption = 'Importa'#231#227'o de Arquivo SPED EFD ICMS/IPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel47: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object QrLocNF: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, IDCtrl'
      'FROM nfecaba'
      'WHERE FatID IN (51,151)'
      'AND Empresa=:P0'
      'AND ide_mod=:P1'
      'AND ide_serie=:P2'
      'AND ide_nNF=:P3'
      'AND CodinfoEmit=:P4')
    Left = 44
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrLocNFFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLocNFFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrLocNFEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrLocNFIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object QrLocGGX: TMySQLQuery
    Database = Dmod.MyDB
    Left = 940
    Top = 392
    object QrLocGGXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrLocGG1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.PrdGrupTip'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE gg1.Nivel1 =:P0')
    Left = 100
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocGG1GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrLocGG1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocGG1NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrLocGG1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
  end
  object QrLocPrdNom: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_CodUsu, ggx.GraGru1, ggx.Controle GraGruX, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.PrdGrupTip'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE gg1.Nome = :P0')
    Left = 128
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPrdNomGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocPrdNomGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
  end
  object QrLocPrdTmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_CodUsu, ggx.GraGru1, ggx.Controle GraGruX, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.PrdGrupTip'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE gg1.NomeTemp = :P0')
    Left = 156
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPrdTmpCU_CodUsu: TIntegerField
      FieldName = 'CU_CodUsu'
      Required = True
    end
    object QrLocPrdTmpGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrLocPrdTmpGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocPrdTmpNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrLocPrdTmpPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM unidmed'
      'WHERE Sigla=:P0')
    Left = 184
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object Qr0000: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd0000'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 456
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr0000ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr0000AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr0000Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr0000LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr0000REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr0000COD_VER: TIntegerField
      FieldName = 'COD_VER'
    end
    object Qr0000COD_FIN: TSmallintField
      FieldName = 'COD_FIN'
    end
    object Qr0000DT_INI: TDateField
      FieldName = 'DT_INI'
    end
    object Qr0000DT_FIN: TDateField
      FieldName = 'DT_FIN'
    end
    object Qr0000NOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object Qr0000CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object Qr0000CPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object Qr0000UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object Qr0000IE: TWideStringField
      FieldName = 'IE'
      Size = 14
    end
    object Qr0000COD_MUN: TIntegerField
      FieldName = 'COD_MUN'
    end
    object Qr0000IM: TWideStringField
      FieldName = 'IM'
      Size = 255
    end
    object Qr0000SUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
    object Qr0000IND_PERFIL: TWideStringField
      FieldName = 'IND_PERFIL'
      Size = 1
    end
    object Qr0000IND_ATIV: TSmallintField
      FieldName = 'IND_ATIV'
    end
  end
  object Ds0000: TDataSource
    DataSet = Qr0000
    Left = 484
    Top = 212
  end
  object Ds0001: TDataSource
    DataSet = Qr0001
    Left = 484
    Top = 240
  end
  object Qr0001: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd0001'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 456
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr0001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr0001AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr0001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr0001LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr0001REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr0001IND_MOV: TSmallintField
      FieldName = 'IND_MOV'
    end
  end
  object Qr0005: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd0005'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 456
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr0005ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr0005AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr0005Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr0005LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr0005REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr0005FANTASIA: TWideStringField
      FieldName = 'FANTASIA'
      Size = 60
    end
    object Qr0005CEP: TIntegerField
      FieldName = 'CEP'
    end
    object Qr0005END: TWideStringField
      FieldName = 'END'
      Size = 60
    end
    object Qr0005NUM: TWideStringField
      FieldName = 'NUM'
      Size = 10
    end
    object Qr0005COMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object Qr0005BAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object Qr0005FONE: TWideStringField
      FieldName = 'FONE'
      Size = 10
    end
    object Qr0005FAX: TWideStringField
      FieldName = 'FAX'
      Size = 10
    end
    object Qr0005EMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 255
    end
  end
  object Ds0005: TDataSource
    DataSet = Qr0005
    Left = 484
    Top = 268
  end
  object Qr0100: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd0100'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 456
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr0100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr0100AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr0100Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr0100LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr0100REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr0100NOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object Qr0100CPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object Qr0100CRC: TWideStringField
      FieldName = 'CRC'
      Size = 15
    end
    object Qr0100CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object Qr0100CEP: TIntegerField
      FieldName = 'CEP'
    end
    object Qr0100END: TWideStringField
      FieldName = 'END'
      Size = 60
    end
    object Qr0100NUM: TWideStringField
      FieldName = 'NUM'
      Size = 10
    end
    object Qr0100COMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object Qr0100BAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object Qr0100FONE: TWideStringField
      FieldName = 'FONE'
      Size = 10
    end
    object Qr0100FAX: TWideStringField
      FieldName = 'FAX'
      Size = 10
    end
    object Qr0100EMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 255
    end
    object Qr0100COD_MUN: TIntegerField
      FieldName = 'COD_MUN'
    end
  end
  object Ds0100: TDataSource
    DataSet = Qr0100
    Left = 484
    Top = 296
  end
  object Ds0150: TDataSource
    DataSet = Qr0150
    Left = 484
    Top = 324
  end
  object Qr0150: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd0150'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 456
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr0150ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr0150AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr0150Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr0150LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr0150REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr0150COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object Qr0150NOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object Qr0150COD_PAIS: TIntegerField
      FieldName = 'COD_PAIS'
    end
    object Qr0150CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object Qr0150CPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object Qr0150IE: TWideStringField
      FieldName = 'IE'
      Size = 14
    end
    object Qr0150COD_MUN: TIntegerField
      FieldName = 'COD_MUN'
    end
    object Qr0150SUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
    object Qr0150END: TWideStringField
      FieldName = 'END'
      Size = 60
    end
    object Qr0150NUM: TWideStringField
      FieldName = 'NUM'
      Size = 10
    end
    object Qr0150COMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object Qr0150BAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
  end
  object Qr0200: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd0200'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 456
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr0200ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr0200AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr0200Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr0200LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr0200REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr0200COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object Qr0200DESCR_ITEM: TWideStringField
      FieldName = 'DESCR_ITEM'
      Size = 255
    end
    object Qr0200COD_BARRA: TWideStringField
      FieldName = 'COD_BARRA'
      Size = 255
    end
    object Qr0200COD_ANT_ITEM: TWideStringField
      FieldName = 'COD_ANT_ITEM'
      Size = 60
    end
    object Qr0200UNID_INV: TWideStringField
      FieldName = 'UNID_INV'
      Size = 6
    end
    object Qr0200TIPO_ITEM: TSmallintField
      FieldName = 'TIPO_ITEM'
    end
    object Qr0200COD_NCM: TWideStringField
      FieldName = 'COD_NCM'
      Size = 8
    end
    object Qr0200EX_IPI: TWideStringField
      FieldName = 'EX_IPI'
      Size = 3
    end
    object Qr0200COD_GEN: TSmallintField
      FieldName = 'COD_GEN'
    end
    object Qr0200COD_LST: TIntegerField
      FieldName = 'COD_LST'
    end
    object Qr0200ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
  end
  object Ds0200: TDataSource
    DataSet = Qr0200
    Left = 484
    Top = 380
  end
  object Ds0400: TDataSource
    DataSet = Qr0400
    Left = 484
    Top = 408
  end
  object Qr0400: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd0400'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 456
    Top = 408
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr0400ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr0400AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr0400Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr0400LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr0400REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr0400COD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object Qr0400DESCR_NAT: TWideStringField
      FieldName = 'DESCR_NAT'
      Size = 255
    end
  end
  object Qr0990: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd0990'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 456
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr0990ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr0990AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr0990Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr0990LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr0990REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr0990QTD_LIN_0: TIntegerField
      FieldName = 'QTD_LIN_0'
    end
  end
  object Ds0990: TDataSource
    DataSet = Qr0990
    Left = 484
    Top = 436
  end
  object DsC001: TDataSource
    DataSet = QrC001
    Left = 540
    Top = 212
  end
  object QrC001: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdc001'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 512
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrC001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrC001AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrC001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrC001LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrC001REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrC001IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
  end
  object QrC100: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdc100'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 512
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrC100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
      Origin = 'spedefdc100.ImporExpor'
    end
    object QrC100AnoMes: TIntegerField
      FieldName = 'AnoMes'
      Origin = 'spedefdc100.AnoMes'
    end
    object QrC100Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'spedefdc100.Empresa'
    end
    object QrC100LinArq: TIntegerField
      FieldName = 'LinArq'
      Origin = 'spedefdc100.LinArq'
    end
    object QrC100REG: TWideStringField
      FieldName = 'REG'
      Origin = 'spedefdc100.REG'
      Size = 4
    end
    object QrC100IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Origin = 'spedefdc100.IND_OPER'
      Size = 1
    end
    object QrC100IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Origin = 'spedefdc100.IND_EMIT'
      Size = 1
    end
    object QrC100COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Origin = 'spedefdc100.COD_PART'
      Size = 60
    end
    object QrC100COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Origin = 'spedefdc100.COD_MOD'
      Size = 2
    end
    object QrC100COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Origin = 'spedefdc100.COD_SIT'
      Size = 2
    end
    object QrC100SER: TWideStringField
      FieldName = 'SER'
      Origin = 'spedefdc100.SER'
      Size = 3
    end
    object QrC100NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Origin = 'spedefdc100.NUM_DOC'
    end
    object QrC100CHV_NFE: TWideStringField
      FieldName = 'CHV_NFE'
      Origin = 'spedefdc100.CHV_NFE'
      Size = 44
    end
    object QrC100DT_DOC: TDateField
      FieldName = 'DT_DOC'
      Origin = 'spedefdc100.DT_DOC'
    end
    object QrC100DT_E_S: TDateField
      FieldName = 'DT_E_S'
      Origin = 'spedefdc100.DT_E_S'
    end
    object QrC100VL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Origin = 'spedefdc100.VL_DOC'
    end
    object QrC100IND_PGTO: TWideStringField
      FieldName = 'IND_PGTO'
      Origin = 'spedefdc100.IND_PGTO'
      Size = 1
    end
    object QrC100VL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Origin = 'spedefdc100.VL_DESC'
    end
    object QrC100VL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Origin = 'spedefdc100.VL_ABAT_NT'
    end
    object QrC100VL_MERC: TFloatField
      FieldName = 'VL_MERC'
      Origin = 'spedefdc100.VL_MERC'
    end
    object QrC100IND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Origin = 'spedefdc100.IND_FRT'
      Size = 1
    end
    object QrC100VL_FRT: TFloatField
      FieldName = 'VL_FRT'
      Origin = 'spedefdc100.VL_FRT'
    end
    object QrC100VL_SEG: TFloatField
      FieldName = 'VL_SEG'
      Origin = 'spedefdc100.VL_SEG'
    end
    object QrC100VL_OUT_DA: TFloatField
      FieldName = 'VL_OUT_DA'
      Origin = 'spedefdc100.VL_OUT_DA'
    end
    object QrC100VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Origin = 'spedefdc100.VL_BC_ICMS'
    end
    object QrC100VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Origin = 'spedefdc100.VL_ICMS'
    end
    object QrC100VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Origin = 'spedefdc100.VL_BC_ICMS_ST'
    end
    object QrC100VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Origin = 'spedefdc100.VL_ICMS_ST'
    end
    object QrC100VL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Origin = 'spedefdc100.VL_IPI'
    end
    object QrC100VL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Origin = 'spedefdc100.VL_PIS'
    end
    object QrC100VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Origin = 'spedefdc100.VL_COFINS'
    end
    object QrC100VL_PIS_ST: TFloatField
      FieldName = 'VL_PIS_ST'
      Origin = 'spedefdc100.VL_PIS_ST'
    end
    object QrC100VL_COFINS_ST: TFloatField
      FieldName = 'VL_COFINS_ST'
      Origin = 'spedefdc100.VL_COFINS_ST'
    end
    object QrC100ParTipo: TIntegerField
      FieldName = 'ParTipo'
      Origin = 'spedefdc100.ParTipo'
    end
    object QrC100ParCodi: TIntegerField
      FieldName = 'ParCodi'
      Origin = 'spedefdc100.ParCodi'
    end
    object QrC100FatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'spedefdc100.FatID'
    end
    object QrC100FatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'spedefdc100.FatNum'
    end
  end
  object DsC100: TDataSource
    DataSet = QrC100
    Left = 540
    Top = 240
  end
  object DsC170: TDataSource
    DataSet = QrC170
    Left = 540
    Top = 268
  end
  object QrC170: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdc170'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 512
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrC170ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrC170AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrC170Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrC170LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrC170C100: TIntegerField
      FieldName = 'C100'
    end
    object QrC170REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrC170NUM_ITEM: TIntegerField
      FieldName = 'NUM_ITEM'
    end
    object QrC170COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrC170DESCR_COMPL: TWideStringField
      FieldName = 'DESCR_COMPL'
      Size = 255
    end
    object QrC170QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrC170UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrC170VL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
    end
    object QrC170VL_DESC: TFloatField
      FieldName = 'VL_DESC'
    end
    object QrC170IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
    object QrC170CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrC170CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrC170COD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrC170VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrC170ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrC170VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrC170VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
    end
    object QrC170ALIQ_ST: TFloatField
      FieldName = 'ALIQ_ST'
    end
    object QrC170VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
    end
    object QrC170IND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrC170CST_IPI: TWideStringField
      FieldName = 'CST_IPI'
      Size = 2
    end
    object QrC170COD_ENQ: TWideStringField
      FieldName = 'COD_ENQ'
      Size = 3
    end
    object QrC170VL_BC_IPI: TFloatField
      FieldName = 'VL_BC_IPI'
    end
    object QrC170ALIQ_IPI: TFloatField
      FieldName = 'ALIQ_IPI'
    end
    object QrC170VL_IPI: TFloatField
      FieldName = 'VL_IPI'
    end
    object QrC170CST_PIS: TSmallintField
      FieldName = 'CST_PIS'
    end
    object QrC170VL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
    end
    object QrC170ALIQ_PIS_p: TFloatField
      FieldName = 'ALIQ_PIS_p'
    end
    object QrC170QUANT_BC_PIS: TFloatField
      FieldName = 'QUANT_BC_PIS'
    end
    object QrC170ALIQ_PIS_r: TFloatField
      FieldName = 'ALIQ_PIS_r'
    end
    object QrC170VL_PIS: TFloatField
      FieldName = 'VL_PIS'
    end
    object QrC170CST_COFINS: TSmallintField
      FieldName = 'CST_COFINS'
    end
    object QrC170VL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
    end
    object QrC170ALIQ_COFINS_p: TFloatField
      FieldName = 'ALIQ_COFINS_p'
    end
    object QrC170QUANT_BC_COFINS: TFloatField
      FieldName = 'QUANT_BC_COFINS'
    end
    object QrC170ALIQ_COFINS_r: TFloatField
      FieldName = 'ALIQ_COFINS_r'
    end
    object QrC170VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
    end
    object QrC170COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
  end
  object QrC190: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdc190'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 512
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrC190ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrC190AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrC190Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrC190LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrC190C100: TIntegerField
      FieldName = 'C100'
    end
    object QrC190REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrC190CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrC190CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrC190ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrC190VL_OPR: TFloatField
      FieldName = 'VL_OPR'
    end
    object QrC190VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrC190VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrC190VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
    end
    object QrC190VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
    end
    object QrC190VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
    object QrC190VL_IPI: TFloatField
      FieldName = 'VL_IPI'
    end
    object QrC190COD_OBS: TWideStringField
      FieldName = 'COD_OBS'
      Size = 6
    end
  end
  object DsC190: TDataSource
    DataSet = QrC190
    Left = 540
    Top = 296
  end
  object DsC500: TDataSource
    DataSet = QrC500
    Left = 540
    Top = 324
  end
  object QrC500: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdc500'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 512
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrC500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
      Origin = 'spedefdc500.ImporExpor'
    end
    object QrC500AnoMes: TIntegerField
      FieldName = 'AnoMes'
      Origin = 'spedefdc500.AnoMes'
    end
    object QrC500Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'spedefdc500.Empresa'
    end
    object QrC500LinArq: TIntegerField
      FieldName = 'LinArq'
      Origin = 'spedefdc500.LinArq'
    end
    object QrC500REG: TWideStringField
      FieldName = 'REG'
      Origin = 'spedefdc500.REG'
      Size = 4
    end
    object QrC500IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Origin = 'spedefdc500.IND_OPER'
      Size = 1
    end
    object QrC500IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Origin = 'spedefdc500.IND_EMIT'
      Size = 1
    end
    object QrC500COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Origin = 'spedefdc500.COD_PART'
      Size = 60
    end
    object QrC500COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Origin = 'spedefdc500.COD_MOD'
      Size = 2
    end
    object QrC500COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Origin = 'spedefdc500.COD_SIT'
      Size = 2
    end
    object QrC500SER: TWideStringField
      FieldName = 'SER'
      Origin = 'spedefdc500.SER'
      Size = 4
    end
    object QrC500SUB: TWideStringField
      FieldName = 'SUB'
      Origin = 'spedefdc500.SUB'
      Size = 3
    end
    object QrC500COD_CONS: TWideStringField
      FieldName = 'COD_CONS'
      Origin = 'spedefdc500.COD_CONS'
      Size = 2
    end
    object QrC500NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Origin = 'spedefdc500.NUM_DOC'
    end
    object QrC500DT_DOC: TDateField
      FieldName = 'DT_DOC'
      Origin = 'spedefdc500.DT_DOC'
    end
    object QrC500DT_E_S: TDateField
      FieldName = 'DT_E_S'
      Origin = 'spedefdc500.DT_E_S'
    end
    object QrC500VL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Origin = 'spedefdc500.VL_DOC'
    end
    object QrC500VL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Origin = 'spedefdc500.VL_DESC'
    end
    object QrC500VL_FORN: TFloatField
      FieldName = 'VL_FORN'
      Origin = 'spedefdc500.VL_FORN'
    end
    object QrC500VL_SERV_NT: TFloatField
      FieldName = 'VL_SERV_NT'
      Origin = 'spedefdc500.VL_SERV_NT'
    end
    object QrC500VL_TERC: TFloatField
      FieldName = 'VL_TERC'
      Origin = 'spedefdc500.VL_TERC'
    end
    object QrC500VL_DA: TFloatField
      FieldName = 'VL_DA'
      Origin = 'spedefdc500.VL_DA'
    end
    object QrC500VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Origin = 'spedefdc500.VL_BC_ICMS'
    end
    object QrC500VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Origin = 'spedefdc500.VL_ICMS'
    end
    object QrC500VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Origin = 'spedefdc500.VL_BC_ICMS_ST'
    end
    object QrC500VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Origin = 'spedefdc500.VL_ICMS_ST'
    end
    object QrC500COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Origin = 'spedefdc500.COD_INF'
      Size = 6
    end
    object QrC500VL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Origin = 'spedefdc500.VL_PIS'
    end
    object QrC500VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Origin = 'spedefdc500.VL_COFINS'
    end
    object QrC500TP_LIGACAO: TWideStringField
      FieldName = 'TP_LIGACAO'
      Origin = 'spedefdc500.TP_LIGACAO'
      Size = 1
    end
    object QrC500COD_GRUPO_TENSAO: TWideStringField
      FieldName = 'COD_GRUPO_TENSAO'
      Origin = 'spedefdc500.COD_GRUPO_TENSAO'
      Size = 2
    end
  end
  object QrC590: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdc590'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 512
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrC590ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrC590AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrC590Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrC590LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrC590C500: TIntegerField
      FieldName = 'C500'
    end
    object QrC590REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrC590CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrC590CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrC590ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrC590VL_OPR: TFloatField
      FieldName = 'VL_OPR'
    end
    object QrC590VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrC590VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrC590VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
    end
    object QrC590VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
    end
    object QrC590VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
    object QrC590COD_OBS: TWideStringField
      FieldName = 'COD_OBS'
      Size = 6
    end
  end
  object DsC590: TDataSource
    DataSet = QrC590
    Left = 540
    Top = 352
  end
  object QrC990: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdc990'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 512
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrC990ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrC990AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrC990Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrC990LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrC990REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrC990QTD_LIN_C: TIntegerField
      FieldName = 'QTD_LIN_C'
    end
  end
  object DsC990: TDataSource
    DataSet = QrC990
    Left = 540
    Top = 380
  end
  object DsD001: TDataSource
    DataSet = QrD001
    Left = 596
    Top = 212
  end
  object QrD001: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdd001'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 568
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrD001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrD001AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrD001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrD001LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrD001REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrD001IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
  end
  object QrD100: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdd100'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 568
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrD100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
      Origin = 'spedefdd100.ImporExpor'
    end
    object QrD100AnoMes: TIntegerField
      FieldName = 'AnoMes'
      Origin = 'spedefdd100.AnoMes'
    end
    object QrD100Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'spedefdd100.Empresa'
    end
    object QrD100LinArq: TIntegerField
      FieldName = 'LinArq'
      Origin = 'spedefdd100.LinArq'
    end
    object QrD100REG: TWideStringField
      FieldName = 'REG'
      Origin = 'spedefdd100.REG'
      Size = 4
    end
    object QrD100IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Origin = 'spedefdd100.IND_OPER'
      Size = 1
    end
    object QrD100IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Origin = 'spedefdd100.IND_EMIT'
      Size = 1
    end
    object QrD100COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Origin = 'spedefdd100.COD_PART'
      Size = 60
    end
    object QrD100COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Origin = 'spedefdd100.COD_MOD'
      Size = 2
    end
    object QrD100COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Origin = 'spedefdd100.COD_SIT'
      Size = 2
    end
    object QrD100SER: TWideStringField
      FieldName = 'SER'
      Origin = 'spedefdd100.SER'
      Size = 4
    end
    object QrD100SUB: TWideStringField
      FieldName = 'SUB'
      Origin = 'spedefdd100.SUB'
      Size = 3
    end
    object QrD100NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Origin = 'spedefdd100.NUM_DOC'
    end
    object QrD100CHV_CTE: TWideStringField
      FieldName = 'CHV_CTE'
      Origin = 'spedefdd100.CHV_CTE'
      Size = 44
    end
    object QrD100DT_DOC: TDateField
      FieldName = 'DT_DOC'
      Origin = 'spedefdd100.DT_DOC'
    end
    object QrD100DT_A_P: TDateField
      FieldName = 'DT_A_P'
      Origin = 'spedefdd100.DT_A_P'
    end
    object QrD100TP_CTE: TWideStringField
      FieldName = 'TP_CTE'
      Origin = 'spedefdd100.TP_CTE'
      Size = 1
    end
    object QrD100CHV_CTE_REF: TWideStringField
      FieldName = 'CHV_CTE_REF'
      Origin = 'spedefdd100.CHV_CTE_REF'
      Size = 44
    end
    object QrD100VL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Origin = 'spedefdd100.VL_DOC'
    end
    object QrD100VL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Origin = 'spedefdd100.VL_DESC'
    end
    object QrD100IND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Origin = 'spedefdd100.IND_FRT'
      Size = 1
    end
    object QrD100VL_SERV: TFloatField
      FieldName = 'VL_SERV'
      Origin = 'spedefdd100.VL_SERV'
    end
    object QrD100VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Origin = 'spedefdd100.VL_BC_ICMS'
    end
    object QrD100VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Origin = 'spedefdd100.VL_ICMS'
    end
    object QrD100VL_NT: TFloatField
      FieldName = 'VL_NT'
      Origin = 'spedefdd100.VL_NT'
    end
    object QrD100COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Origin = 'spedefdd100.COD_INF'
      Size = 6
    end
    object QrD100COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Origin = 'spedefdd100.COD_CTA'
      Size = 255
    end
  end
  object DsD100: TDataSource
    DataSet = QrD100
    Left = 596
    Top = 240
  end
  object DsD500: TDataSource
    DataSet = QrD500
    Left = 596
    Top = 296
  end
  object QrD500: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdd500'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 568
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrD500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
      Origin = 'spedefdd500.ImporExpor'
    end
    object QrD500AnoMes: TIntegerField
      FieldName = 'AnoMes'
      Origin = 'spedefdd500.AnoMes'
    end
    object QrD500Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'spedefdd500.Empresa'
    end
    object QrD500LinArq: TIntegerField
      FieldName = 'LinArq'
      Origin = 'spedefdd500.LinArq'
    end
    object QrD500REG: TWideStringField
      FieldName = 'REG'
      Origin = 'spedefdd500.REG'
      Size = 4
    end
    object QrD500IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Origin = 'spedefdd500.IND_OPER'
      Size = 1
    end
    object QrD500IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Origin = 'spedefdd500.IND_EMIT'
      Size = 1
    end
    object QrD500COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Origin = 'spedefdd500.COD_PART'
      Size = 60
    end
    object QrD500COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Origin = 'spedefdd500.COD_MOD'
      Size = 2
    end
    object QrD500COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Origin = 'spedefdd500.COD_SIT'
      Size = 2
    end
    object QrD500SER: TWideStringField
      FieldName = 'SER'
      Origin = 'spedefdd500.SER'
      Size = 4
    end
    object QrD500SUB: TWideStringField
      FieldName = 'SUB'
      Origin = 'spedefdd500.SUB'
      Size = 3
    end
    object QrD500NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Origin = 'spedefdd500.NUM_DOC'
    end
    object QrD500DT_DOC: TDateField
      FieldName = 'DT_DOC'
      Origin = 'spedefdd500.DT_DOC'
    end
    object QrD500DT_A_P: TDateField
      FieldName = 'DT_A_P'
      Origin = 'spedefdd500.DT_A_P'
    end
    object QrD500VL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Origin = 'spedefdd500.VL_DOC'
    end
    object QrD500VL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Origin = 'spedefdd500.VL_DESC'
    end
    object QrD500VL_SERV: TFloatField
      FieldName = 'VL_SERV'
      Origin = 'spedefdd500.VL_SERV'
    end
    object QrD500VL_SERV_NT: TFloatField
      FieldName = 'VL_SERV_NT'
      Origin = 'spedefdd500.VL_SERV_NT'
    end
    object QrD500VL_TERC: TFloatField
      FieldName = 'VL_TERC'
      Origin = 'spedefdd500.VL_TERC'
    end
    object QrD500VL_DA: TFloatField
      FieldName = 'VL_DA'
      Origin = 'spedefdd500.VL_DA'
    end
    object QrD500VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Origin = 'spedefdd500.VL_BC_ICMS'
    end
    object QrD500VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Origin = 'spedefdd500.VL_ICMS'
    end
    object QrD500COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Origin = 'spedefdd500.COD_INF'
      Size = 6
    end
    object QrD500VL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Origin = 'spedefdd500.VL_PIS'
    end
    object QrD500VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Origin = 'spedefdd500.VL_COFINS'
    end
    object QrD500COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Origin = 'spedefdd500.COD_CTA'
      Size = 255
    end
    object QrD500TP_ASSINANTE: TWideStringField
      FieldName = 'TP_ASSINANTE'
      Origin = 'spedefdd500.TP_ASSINANTE'
      Size = 1
    end
  end
  object DsD190: TDataSource
    DataSet = QrD190
    Left = 596
    Top = 268
  end
  object QrD190: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdd190'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 568
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrD190ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrD190AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrD190Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrD190LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrD190D100: TIntegerField
      FieldName = 'D100'
    end
    object QrD190REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrD190CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrD190CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrD190ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrD190VL_OPR: TFloatField
      FieldName = 'VL_OPR'
    end
    object QrD190VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrD190VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrD190VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
    object QrD190COD_OBS: TWideStringField
      FieldName = 'COD_OBS'
      Size = 6
    end
  end
  object DsD990: TDataSource
    DataSet = QrD990
    Left = 596
    Top = 352
  end
  object QrD990: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdd990'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 568
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrD990ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrD990AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrD990Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrD990LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrD990REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrD990QTD_LIN_D: TIntegerField
      FieldName = 'QTD_LIN_D'
    end
  end
  object DsD590: TDataSource
    DataSet = QrD590
    Left = 596
    Top = 324
  end
  object QrD590: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdd590'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 568
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrD590ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrD590AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrD590Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrD590LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrD590D500: TIntegerField
      FieldName = 'D500'
    end
    object QrD590REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrD590CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrD590CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrD590ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrD590VL_OPR: TFloatField
      FieldName = 'VL_OPR'
    end
    object QrD590VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrD590VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrD590VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
    end
    object QrD590VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
    end
    object QrD590VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
    object QrD590COD_OBS: TWideStringField
      FieldName = 'COD_OBS'
      Size = 6
    end
  end
  object DsE100: TDataSource
    DataSet = QrE100
    Left = 652
    Top = 240
  end
  object QrE100: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefde100'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 624
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrE100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE100AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE100Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE100LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE100REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE100DT_INI: TDateField
      FieldName = 'DT_INI'
    end
    object QrE100DT_FIN: TDateField
      FieldName = 'DT_FIN'
    end
  end
  object DsE001: TDataSource
    DataSet = QrE001
    Left = 652
    Top = 212
  end
  object QrE001: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefde001'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 624
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrE001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE001AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE001LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE001REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE001IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
  end
  object QrE110: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefde110'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 624
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrE110ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE110AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE110Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE110LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE110E100: TIntegerField
      FieldName = 'E100'
    end
    object QrE110REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE110VL_TOT_DEBITOS: TFloatField
      FieldName = 'VL_TOT_DEBITOS'
    end
    object QrE110VL_AJ_DEBITOS: TFloatField
      FieldName = 'VL_AJ_DEBITOS'
    end
    object QrE110VL_TOT_AJ_DEBITOS: TFloatField
      FieldName = 'VL_TOT_AJ_DEBITOS'
    end
    object QrE110VL_ESTORNOS_CRED: TFloatField
      FieldName = 'VL_ESTORNOS_CRED'
    end
    object QrE110VL_TOT_CREDITOS: TFloatField
      FieldName = 'VL_TOT_CREDITOS'
    end
    object QrE110VL_AJ_CREDITOS: TFloatField
      FieldName = 'VL_AJ_CREDITOS'
    end
    object QrE110VL_TOT_AJ_CREDITOS: TFloatField
      FieldName = 'VL_TOT_AJ_CREDITOS'
    end
    object QrE110VL_ESTORNOS_DEB: TFloatField
      FieldName = 'VL_ESTORNOS_DEB'
    end
    object QrE110VL_SLD_CREDOR_ANT: TFloatField
      FieldName = 'VL_SLD_CREDOR_ANT'
    end
    object QrE110VL_SLD_APURADO: TFloatField
      FieldName = 'VL_SLD_APURADO'
    end
    object QrE110VL_TOT_DED: TFloatField
      FieldName = 'VL_TOT_DED'
    end
    object QrE110VL_ICMS_RECOLHER: TFloatField
      FieldName = 'VL_ICMS_RECOLHER'
    end
    object QrE110VL_SLD_CREDOR_TRANSPORTAR: TFloatField
      FieldName = 'VL_SLD_CREDOR_TRANSPORTAR'
    end
    object QrE110DEB_ESP: TFloatField
      FieldName = 'DEB_ESP'
    end
  end
  object DsE110: TDataSource
    DataSet = QrE110
    Left = 652
    Top = 268
  end
  object DsE990: TDataSource
    DataSet = QrE990
    Left = 652
    Top = 436
  end
  object QrE990: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefde990'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 624
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrE990ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE990AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE990Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE990LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE990REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE990QTD_LIN_E: TIntegerField
      FieldName = 'QTD_LIN_E'
    end
  end
  object QrH001: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdh001'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 680
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrH001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrH001AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrH001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrH001LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrH001REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrH001IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
  end
  object DsH001: TDataSource
    DataSet = QrH001
    Left = 708
    Top = 212
  end
  object DsH005: TDataSource
    DataSet = QrH005
    Left = 708
    Top = 240
  end
  object QrH005: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdh005'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 680
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrH005ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrH005AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrH005Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrH005LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrH005REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrH005DT_INV: TDateField
      FieldName = 'DT_INV'
    end
    object QrH005VL_INV: TFloatField
      FieldName = 'VL_INV'
    end
  end
  object QrH010: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdh010'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 680
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrH010ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrH010AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrH010Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrH010LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrH010REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrH010COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrH010UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrH010QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrH010VL_UNIT: TFloatField
      FieldName = 'VL_UNIT'
    end
    object QrH010VL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
    end
    object QrH010IND_PROP: TWideStringField
      FieldName = 'IND_PROP'
      Size = 1
    end
    object QrH010COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrH010TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
    object QrH010COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
  end
  object DsH010: TDataSource
    DataSet = QrH010
    Left = 708
    Top = 268
  end
  object DsH990: TDataSource
    DataSet = QrH990
    Left = 708
    Top = 296
  end
  object QrH990: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdh990'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 680
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrH990ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrH990AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrH990Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrH990LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrH990REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrH990QTD_LIN_H: TIntegerField
      FieldName = 'QTD_LIN_H'
    end
  end
  object Ds1001: TDataSource
    DataSet = Qr1001
    Left = 820
    Top = 212
  end
  object Qr1990: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd1990'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 792
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr1990ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr1990AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr1990Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr1990LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr1990REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr1990QTD_LIN_1: TIntegerField
      FieldName = 'QTD_LIN_1'
    end
  end
  object Ds1990: TDataSource
    DataSet = Qr1990
    Left = 820
    Top = 240
  end
  object Qr1001: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd1001'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 792
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr1001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr1001AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr1001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr1001LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr1001REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr1001IND_MOV: TSmallintField
      FieldName = 'IND_MOV'
    end
  end
  object Qr9001: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd9001'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 848
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr9001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr9001AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr9001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr9001LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr9001REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr9001IND_MOV: TSmallintField
      FieldName = 'IND_MOV'
    end
  end
  object Ds9001: TDataSource
    DataSet = Qr9001
    Left = 876
    Top = 212
  end
  object Ds9900: TDataSource
    DataSet = Qr9900
    Left = 876
    Top = 240
  end
  object Qr9900: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd9900'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 848
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr9900ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr9900AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr9900Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr9900LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr9900REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr9900REG_BLC: TWideStringField
      FieldName = 'REG_BLC'
      Size = 4
    end
    object Qr9900QTD_REG_BLC: TIntegerField
      FieldName = 'QTD_REG_BLC'
    end
  end
  object Qr9990: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd9990'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 848
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr9990ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr9990AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr9990Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr9990LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr9990REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr9990QTD_LIN_9: TIntegerField
      FieldName = 'QTD_LIN_9'
    end
  end
  object Ds9990: TDataSource
    DataSet = Qr9990
    Left = 876
    Top = 268
  end
  object Ds9999: TDataSource
    DataSet = Qr9999
    Left = 876
    Top = 296
  end
  object Qr9999: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd9999'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 848
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr9999ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr9999AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr9999Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr9999LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr9999REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr9999QTD_LIN: TIntegerField
      FieldName = 'QTD_LIN'
    end
  end
  object Qr0190: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd0190'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 456
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr0190ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr0190AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr0190Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr0190LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr0190REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr0190UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object Qr0190DESCR: TWideStringField
      FieldName = 'DESCR'
      Size = 255
    end
  end
  object Ds0190: TDataSource
    DataSet = Qr0190
    Left = 484
    Top = 352
  end
  object QrVersao: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdvers'
      'WHERE COD_VER=:P0')
    Left = 940
    Top = 504
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVersaoNUM_VER: TWideStringField
      FieldName = 'NUM_VER'
      Size = 4
    end
    object QrVersaoDT_INI: TDateField
      FieldName = 'DT_INI'
    end
    object QrVersaoDT_FIN: TDateField
      FieldName = 'DT_FIN'
    end
  end
  object QrErr0150: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrErr0150AfterOpen
    BeforeClose = QrErr0150BeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd0150'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'AND Entidade=0'
      'ORDER BY LinArq')
    Left = 940
    Top = 476
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrErr0150ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrErr0150AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrErr0150Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrErr0150LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrErr0150Entidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrErr0150REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrErr0150COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrErr0150NOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrErr0150COD_PAIS: TIntegerField
      FieldName = 'COD_PAIS'
    end
    object QrErr0150CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrErr0150CPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrErr0150IE: TWideStringField
      FieldName = 'IE'
      Size = 14
    end
    object QrErr0150COD_MUN: TIntegerField
      FieldName = 'COD_MUN'
    end
    object QrErr0150SUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
    object QrErr0150END: TWideStringField
      FieldName = 'END'
      Size = 60
    end
    object QrErr0150NUM: TWideStringField
      FieldName = 'NUM'
      Size = 10
    end
    object QrErr0150COMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrErr0150BAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
  end
  object DsErr0150: TDataSource
    DataSet = QrErr0150
    Left = 968
    Top = 476
  end
  object QrErr0190: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrErr0150AfterOpen
    BeforeClose = QrErr0150BeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM spedefd0190'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'AND UnidMed=0'
      'ORDER BY LinArq')
    Left = 940
    Top = 448
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object DsErr0190: TDataSource
    DataSet = QrErr0190
    Left = 968
    Top = 448
  end
  object QrLocUM: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM unidmed'
      'WHERE Sigla=:P0'
      'ORDER BY Codigo')
    Left = 968
    Top = 504
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocUMCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrErr0200: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrErr0150AfterOpen
    BeforeClose = QrErr0150BeforeClose
    AfterScroll = QrErr0200AfterScroll
    SQL.Strings = (
      'SELECT ggd.Receptor, _0200.*'
      'FROM spedefd0200 _0200'
      'LEFT JOIN gragrudel ggd ON ggd.Excluido=_0200.COD_ITEM'
      'WHERE _0200.ImporExpor=:P0'
      'AND _0200.AnoMes=:P1'
      'AND _0200.Empresa=:P2'
      'AND _0200.GraGruX=0'
      'ORDER BY _0200.LinArq'
      ''
      '/*'
      'SELECT *'
      'FROM spedefd0200'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'AND GraGruX=0'
      'ORDER BY LinArq'
      '*/'
      '')
    Left = 912
    Top = 420
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrErr0200Receptor: TIntegerField
      FieldName = 'Receptor'
    end
    object QrErr0200ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrErr0200AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrErr0200Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrErr0200LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrErr0200GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrErr0200REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrErr0200COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrErr0200DESCR_ITEM: TWideStringField
      FieldName = 'DESCR_ITEM'
      Size = 255
    end
    object QrErr0200COD_BARRA: TWideStringField
      FieldName = 'COD_BARRA'
      Size = 255
    end
    object QrErr0200COD_ANT_ITEM: TWideStringField
      FieldName = 'COD_ANT_ITEM'
      Size = 60
    end
    object QrErr0200UNID_INV: TWideStringField
      FieldName = 'UNID_INV'
      Size = 6
    end
    object QrErr0200TIPO_ITEM: TSmallintField
      FieldName = 'TIPO_ITEM'
    end
    object QrErr0200COD_NCM: TWideStringField
      FieldName = 'COD_NCM'
      Size = 8
    end
    object QrErr0200EX_IPI: TWideStringField
      FieldName = 'EX_IPI'
      Size = 3
    end
    object QrErr0200COD_GEN: TSmallintField
      FieldName = 'COD_GEN'
    end
    object QrErr0200COD_LST: TIntegerField
      FieldName = 'COD_LST'
    end
    object QrErr0200ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
  end
  object DsErr0200: TDataSource
    DataSet = QrErr0200
    Left = 940
    Top = 420
  end
  object QrErrC170: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrErr0150AfterOpen
    BeforeClose = QrErr0150BeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdc170'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'AND GraGruX=0'
      'ORDER BY LinArq')
    Left = 940
    Top = 364
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object DsErrC170: TDataSource
    DataSet = QrErrC170
    Left = 968
    Top = 364
  end
  object QrItsC170: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdc170'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'AND C100=:P3'
      'ORDER BY LinArq')
    Left = 940
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsC170AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrItsC170Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrItsC170LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrItsC170C100: TIntegerField
      FieldName = 'C100'
    end
    object QrItsC170GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrItsC170REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrItsC170NUM_ITEM: TIntegerField
      FieldName = 'NUM_ITEM'
    end
    object QrItsC170COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrItsC170DESCR_COMPL: TWideStringField
      FieldName = 'DESCR_COMPL'
      Size = 255
    end
    object QrItsC170QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrItsC170UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrItsC170VL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
    end
    object QrItsC170VL_DESC: TFloatField
      FieldName = 'VL_DESC'
    end
    object QrItsC170IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
    object QrItsC170CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrItsC170CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrItsC170COD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrItsC170VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrItsC170ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrItsC170VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrItsC170VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
    end
    object QrItsC170ALIQ_ST: TFloatField
      FieldName = 'ALIQ_ST'
    end
    object QrItsC170VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
    end
    object QrItsC170IND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrItsC170CST_IPI: TWideStringField
      FieldName = 'CST_IPI'
      Size = 2
    end
    object QrItsC170COD_ENQ: TWideStringField
      FieldName = 'COD_ENQ'
      Size = 3
    end
    object QrItsC170VL_BC_IPI: TFloatField
      FieldName = 'VL_BC_IPI'
    end
    object QrItsC170ALIQ_IPI: TFloatField
      FieldName = 'ALIQ_IPI'
    end
    object QrItsC170VL_IPI: TFloatField
      FieldName = 'VL_IPI'
    end
    object QrItsC170CST_PIS: TSmallintField
      FieldName = 'CST_PIS'
    end
    object QrItsC170VL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
    end
    object QrItsC170ALIQ_PIS_p: TFloatField
      FieldName = 'ALIQ_PIS_p'
    end
    object QrItsC170QUANT_BC_PIS: TFloatField
      FieldName = 'QUANT_BC_PIS'
    end
    object QrItsC170ALIQ_PIS_r: TFloatField
      FieldName = 'ALIQ_PIS_r'
    end
    object QrItsC170VL_PIS: TFloatField
      FieldName = 'VL_PIS'
    end
    object QrItsC170CST_COFINS: TSmallintField
      FieldName = 'CST_COFINS'
    end
    object QrItsC170VL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
    end
    object QrItsC170ALIQ_COFINS_p: TFloatField
      FieldName = 'ALIQ_COFINS_p'
    end
    object QrItsC170QUANT_BC_COFINS: TFloatField
      FieldName = 'QUANT_BC_COFINS'
    end
    object QrItsC170ALIQ_COFINS_r: TFloatField
      FieldName = 'ALIQ_COFINS_r'
    end
    object QrItsC170VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
    end
    object QrItsC170COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
  end
  object QrGGX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.PrdGrupTip, gg1.GerBxaEstq, gg1.Nivel2, gg1.Nivel1'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggx.Controle=:P0')
    Left = 968
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGGXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGGXPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGGXNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGGXNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
  end
  object QrRecupera: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IDCtrl'
      'FROM stqmovitsa'
      'WHERE Tipo=:P0'
      'AND partipo=:P1'
      'AND parcodi=:P2')
    Left = 968
    Top = 392
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrRecuperaIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object QrErrNFs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT c100.LinArq, c100.COD_PART, c100.COD_MOD,'
      'c100.COD_SIT, c100.SER, c100.NUM_DOC, c100.DT_DOC,'
      'c100.DT_E_S, _0150.Entidade, _0150.NOME, '
      'CONCAT(_0150.CNPJ,_0150.CPF) DOC_PART,'
      'c170.COD_ITEM, c170.QTD, c170.UNID, c170.VL_ITEM'
      'FROM spedefdc170 c170'
      'LEFT JOIN spedefdc100 c100 ON c100.LinArq=c170.C100'
      '  AND c100.ImporExpor=1'
      '  AND c100.AnoMes=201103'
      '  AND c100.Empresa=-11'
      'LEFT JOIN spedefd0150 _0150 ON _0150.COD_PART=c100.COD_PART'
      '  AND _0150.ImporExpor=1'
      '  AND _0150.AnoMes=201103'
      '  AND _0150.Empresa=-11'
      'WHERE c170.ImporExpor=1'
      'AND c170.AnoMes=201103'
      'AND c170.Empresa=-11'
      'AND c170.COD_ITEM="-201"')
    Left = 828
    Top = 420
    object QrErrNFsLinArq: TIntegerField
      FieldName = 'LinArq'
      Required = True
    end
    object QrErrNFsCOD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrErrNFsCOD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrErrNFsCOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
    end
    object QrErrNFsSER: TWideStringField
      FieldName = 'SER'
      Size = 3
    end
    object QrErrNFsNUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrErrNFsDT_DOC: TDateField
      FieldName = 'DT_DOC'
    end
    object QrErrNFsDT_E_S: TDateField
      FieldName = 'DT_E_S'
    end
    object QrErrNFsEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrErrNFsNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrErrNFsDOC_PART: TWideStringField
      FieldName = 'DOC_PART'
      Size = 25
    end
    object QrErrNFsCOD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrErrNFsQTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrErrNFsUNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrErrNFsVL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsErrNFs: TDataSource
    DataSet = QrErrNFs
    Left = 856
    Top = 420
  end
  object frxErr_0200: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.407328206020000000
    ReportOptions.LastChange = 39949.407328206020000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 800
    Top = 420
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsErr0200
        DataSetName = 'frxDsErr0200'
      end
      item
        DataSet = frxDsErrNFs
        DataSetName = 'frxDsErrNFs'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader5: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 71.811062680000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1009.133858267717000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 18.897650000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LISTA DE ERROS DE IMPORTA'#199#195'O (Registro 0200)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date][Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 854.173780000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 37.795300000000000000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'EMPRESA: [frxDsErr0200."Empresa"]  PER'#205'ODO: [frxDsErr0200."AnoMe' +
              's"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo item')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo NCM')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 449.764070000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'EX IPI')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'G'#234'nero')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'd. servi'#231'o')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Aliq. ICMS')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 60.472480000000000000
          Width = 188.976353540000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 56.692950000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd. Item')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 60.472480000000000000
          Width = 56.692950000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd. Item')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015762680000000000
        Top = 151.181200000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsErr0200
        DataSetName = 'frxDsErr0200'
        RowCount = 0
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'UNID_INV'
          DataSet = frxDsErr0200
          DataSetName = 'frxDsErr0200'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErr0200."UNID_INV"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'TIPO_ITEM'
          DataSet = frxDsErr0200
          DataSetName = 'frxDsErr0200'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErr0200."TIPO_ITEM"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'COD_NCM'
          DataSet = frxDsErr0200
          DataSetName = 'frxDsErr0200'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErr0200."COD_NCM"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 449.764070000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'EX_IPI'
          DataSet = frxDsErr0200
          DataSetName = 'frxDsErr0200'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErr0200."EX_IPI"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'COD_GEN'
          DataSet = frxDsErr0200
          DataSetName = 'frxDsErr0200'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErr0200."COD_GEN"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'COD_LST'
          DataSet = frxDsErr0200
          DataSetName = 'frxDsErr0200'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErr0200."COD_LST"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'ALIQ_ICMS'
          DataSet = frxDsErr0200
          DataSetName = 'frxDsErr0200'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErr0200."ALIQ_ICMS"]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 7.559060000000000000
          Width = 188.976353540000000000
          Height = 11.338582680000000000
          DataField = 'DESCR_ITEM'
          DataSet = frxDsErr0200
          DataSetName = 'frxDsErr0200'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErr0200."DESCR_ITEM"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 56.692950000000000000
          Height = 11.338582680000000000
          DataField = 'Receptor'
          DataSet = frxDsErr0200
          DataSetName = 'frxDsErr0200'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErr0200."Receptor"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 7.559060000000000000
          Width = 56.692950000000000000
          Height = 11.338582680000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsErr0200
          DataSetName = 'frxDsErr0200'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErr0200."COD_ITEM"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 22.677180000000000000
          Width = 22.677165354330710000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Mod')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 56.692950000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'LinArq')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 22.677180000000000000
          Width = 94.488250000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd. Part')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 22.677180000000000000
          Width = 22.677165354330710000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Top = 22.677180000000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'N'#186' NF')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 241.889920000000000000
          Top = 22.677180000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data doc.')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Top = 22.677180000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data doc.')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 22.677180000000000000
          Width = 37.795275590551180000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Entidade')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 22.677180000000000000
          Width = 268.346456690000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Participante')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299630000000000000
          Top = 22.677180000000000000
          Width = 94.488250000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 740.787880000000000000
          Top = 22.677180000000000000
          Width = 94.488250000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd. Item')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 835.276130000000000000
          Top = 22.677180000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 891.969080000000000000
          Top = 22.677180000000000000
          Width = 37.795300000000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 929.764380000000000000
          Top = 22.677180000000000000
          Width = 79.370078740000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor do item')
          ParentFont = False
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 279.685220000000000000
        Width = 1009.134510000000000000
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338582680000000000
        Top = 207.874150000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsErrNFs
        DataSetName = 'frxDsErrNFs'
        RowCount = 0
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Width = 56.692950000000000000
          Height = 11.338582680000000000
          DataField = 'LinArq'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrNFs."LinArq"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Width = 94.488250000000000000
          Height = 11.338582680000000000
          DataField = 'COD_PART'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrNFs."COD_PART"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 22.677165354330710000
          Height = 11.338582680000000000
          DataField = 'COD_MOD'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrNFs."COD_MOD"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Width = 22.677165354330710000
          Height = 11.338582680000000000
          DataField = 'SER'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrNFs."SER"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataField = 'NUM_DOC'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErrNFs."NUM_DOC"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 241.889920000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'DT_DOC'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrNFs."DT_DOC"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'DT_E_S'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrNFs."DT_E_S"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Width = 37.795275590551180000
          Height = 11.338582680000000000
          DataField = 'Entidade'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrNFs."Entidade"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 268.346456692913400000
          Height = 11.338582680000000000
          DataField = 'NOME'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrNFs."NOME"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299630000000000000
          Width = 94.488250000000000000
          Height = 11.338582680000000000
          DataField = 'DOC_PART'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrNFs."DOC_PART"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 740.787880000000000000
          Width = 94.488250000000000000
          Height = 11.338582680000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrNFs."COD_ITEM"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 835.276130000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataField = 'QTD'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErrNFs."QTD"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 891.969080000000000000
          Width = 37.795300000000000000
          Height = 11.338582680000000000
          DataField = 'UNIT'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrNFs."UNIT"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 929.764380000000000000
          Width = 79.370078740000000000
          Height = 11.338582680000000000
          DataField = 'VL_ITEM'
          DataSet = frxDsErrNFs
          DataSetName = 'frxDsErrNFs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErrNFs."VL_ITEM"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsErr0200: TfrxDBDataset
    UserName = 'frxDsErr0200'
    CloseDataSource = False
    DataSet = QrErr0200
    BCDToCurrency = False
    DataSetOptions = []
    Left = 968
    Top = 420
  end
  object frxDsErrNFs: TfrxDBDataset
    UserName = 'frxDsErrNFs'
    CloseDataSource = False
    FieldAliases.Strings = (
      'LinArq=LinArq'
      'COD_PART=COD_PART'
      'COD_MOD=COD_MOD'
      'COD_SIT=COD_SIT'
      'SER=SER'
      'NUM_DOC=NUM_DOC'
      'DT_DOC=DT_DOC'
      'DT_E_S=DT_E_S'
      'Entidade=Entidade'
      'NOME=NOME'
      'DOC_PART=DOC_PART'
      'COD_ITEM=COD_ITEM'
      'QTD=QTD'
      'UNIT=UNIT'
      'VL_ITEM=VL_ITEM')
    DataSource = DsErrNFs
    BCDToCurrency = False
    DataSetOptions = []
    Left = 884
    Top = 420
  end
  object QrE111: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefde111'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 624
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrE111ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE111AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE111Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE111LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE111E110: TIntegerField
      FieldName = 'E110'
    end
    object QrE111REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE111COD_AJ_APUR: TWideStringField
      FieldName = 'COD_AJ_APUR'
      Size = 8
    end
    object QrE111DESCR_COMPL_AJ: TWideStringField
      FieldName = 'DESCR_COMPL_AJ'
      Size = 255
    end
    object QrE111VL_AJ_APUR: TFloatField
      FieldName = 'VL_AJ_APUR'
    end
  end
  object DsE111: TDataSource
    DataSet = QrE111
    Left = 652
    Top = 296
  end
  object QrErrs_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefderrs'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 400
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrErrs_ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrErrs_AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrErrs_Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrErrs_LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrErrs_REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrErrs_Campo: TWideStringField
      FieldName = 'Campo'
      Size = 50
    end
    object QrErrs_Erro: TSmallintField
      FieldName = 'Erro'
    end
  end
  object DsErrs_: TDataSource
    DataSet = QrErrs_
    Left = 428
    Top = 212
  end
  object QrG990: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdg990'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 736
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrG990ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrG990AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrG990Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrG990LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrG990REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrG990QTD_LIN_G: TIntegerField
      FieldName = 'QTD_LIN_G'
    end
  end
  object QrG001: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdg001'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 736
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrG001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrG001AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrG001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrG001LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrG001REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrG001IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
  end
  object DsG001: TDataSource
    DataSet = QrG001
    Left = 764
    Top = 212
  end
  object DsG990: TDataSource
    DataSet = QrG990
    Left = 764
    Top = 240
  end
  object QrE116: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefde116'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 624
    Top = 408
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrE116ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE116AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE116Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE116LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE116E110: TIntegerField
      FieldName = 'E110'
    end
    object QrE116REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE116COD_OR: TWideStringField
      FieldName = 'COD_OR'
      Size = 3
    end
    object QrE116VL_OR: TFloatField
      FieldName = 'VL_OR'
    end
    object QrE116DT_VCTO: TDateField
      FieldName = 'DT_VCTO'
    end
    object QrE116COD_REC: TWideStringField
      FieldName = 'COD_REC'
      Size = 255
    end
    object QrE116NUM_PROC: TWideStringField
      FieldName = 'NUM_PROC'
      Size = 15
    end
    object QrE116IND_PROC: TWideStringField
      FieldName = 'IND_PROC'
      Size = 1
    end
    object QrE116PROC: TWideStringField
      FieldName = 'PROC'
      Size = 255
    end
    object QrE116TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
    object QrE116MES_REF: TDateField
      FieldName = 'MES_REF'
    end
  end
  object DsE116: TDataSource
    DataSet = QrE116
    Left = 652
    Top = 408
  end
  object QrINN_0200: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _spedinn_0200_')
    Left = 780
    Top = 12
    object QrINN_0200LinArq: TIntegerField
      FieldName = 'LinArq'
      Origin = '_spedinn_0200_.LinArq'
    end
    object QrINN_0200Distancia: TIntegerField
      FieldName = 'Distancia'
      Origin = '_spedinn_0200_.Distancia'
    end
    object QrINN_0200GraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = '_spedinn_0200_.GraGruX'
    end
    object QrINN_0200Nome: TWideStringField
      FieldName = 'Nome'
      Origin = '_spedinn_0200_.Nome'
      Size = 120
    end
    object QrINN_0200COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Origin = '_spedinn_0200_.COD_ITEM'
      Size = 60
    end
    object QrINN_0200DESCR_ITEM: TWideStringField
      FieldName = 'DESCR_ITEM'
      Origin = '_spedinn_0200_.DESCR_ITEM'
      Size = 255
    end
    object QrINN_0200COD_ANT_ITEM: TWideStringField
      FieldName = 'COD_ANT_ITEM'
      Origin = '_spedinn_0200_.COD_ANT_ITEM'
      Size = 60
    end
    object QrINN_0200Levenshtein_Meu: TWideStringField
      FieldName = 'Levenshtein_Meu'
      Origin = '_spedinn_0200_.Levenshtein_Meu'
      Size = 120
    end
    object QrINN_0200Levenshtein_Inn: TWideStringField
      FieldName = 'Levenshtein_Inn'
      Origin = '_spedinn_0200_.Levenshtein_Inn'
      Size = 120
    end
  end
  object QrGG1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nome'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggx.Controle=:P0')
    Left = 808
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGG1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object QrLoc0200: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraGruX '
      'FROM spedefd0200'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'AND COD_ITEM=:P3'
      '')
    Left = 836
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrLoc0200GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrAdver_0200: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _spedinn_0200_'
      'WHERE Distancia > 0'
      'OR NivelExclu > 0'
      '')
    Left = 104
    Top = 324
    object QrAdver_0200LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrAdver_0200Distancia: TIntegerField
      FieldName = 'Distancia'
    end
    object QrAdver_0200GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrAdver_0200Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrAdver_0200COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrAdver_0200DESCR_ITEM: TWideStringField
      FieldName = 'DESCR_ITEM'
      Size = 255
    end
    object QrAdver_0200COD_ANT_ITEM: TWideStringField
      FieldName = 'COD_ANT_ITEM'
      Size = 60
    end
    object QrAdver_0200Levenshtein_Meu: TWideStringField
      FieldName = 'Levenshtein_Meu'
      Size = 120
    end
    object QrAdver_0200Levenshtein_Inn: TWideStringField
      FieldName = 'Levenshtein_Inn'
      Size = 120
    end
    object QrAdver_0200NivelExclu: TIntegerField
      FieldName = 'NivelExclu'
    end
  end
  object DsAdver_0200: TDataSource
    DataSet = QrAdver_0200
    Left = 132
    Top = 324
  end
  object frxSPEDEFD_IMPORT_003_00: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //        '
      'end.')
    Left = 160
    Top = 324
    Datasets = <
      item
        DataSet = frxDsAdver_0200
        DataSetName = 'frxDsAdver_0200'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 71.811062680000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1009.134510000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ADVERT'#202'NCIAS EM IMPORTA'#199#195'O DE SPED-EFD')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315033860000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'CADASTRO DE PRODUTOS')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 60.472480000000000000
          Width = 83.149616060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Dist'#226'ncia Levenstein')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 60.472480000000000000
          Width = 75.590556060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 37.795256060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Linha')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Top = 60.472480000000000000
          Width = 60.472436060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'N'#237'vel exclus'#227'o')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007876460000000000
        Top = 272.126160000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 60.472472680000000000
        ParentFont = False
        Top = 151.181200000000000000
        Width = 680.315400000000000000
        DataSet = frxDsAdver_0200
        DataSetName = 'frxDsAdver_0200'
        RowCount = 0
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 11.338590000000000000
          Width = 548.031806060000000000
          Height = 11.338582680000000000
          DataField = 'Nome'
          DataSet = frxDsAdver_0200
          DataSetName = 'frxDsAdver_0200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'courier New'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAdver_0200."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 75.590556060000000000
          Height = 11.338582680000000000
          DataField = 'GraGruX'
          DataSet = frxDsAdver_0200
          DataSetName = 'frxDsAdver_0200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAdver_0200."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795256060000000000
          Height = 11.338582680000000000
          DataField = 'LinArq'
          DataSet = frxDsAdver_0200
          DataSetName = 'frxDsAdver_0200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAdver_0200."LinArq"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 22.677180000000000000
          Width = 548.031806060000000000
          Height = 11.338582680000000000
          DataField = 'DESCR_ITEM'
          DataSet = frxDsAdver_0200
          DataSetName = 'frxDsAdver_0200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'courier New'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAdver_0200."DESCR_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 132.283506060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome cadastrado no aplicativo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 132.283506060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome constante no arquivo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 34.015770000000000000
          Width = 548.031806060000000000
          Height = 11.338582680000000000
          DataField = 'Levenshtein_Meu'
          DataSet = frxDsAdver_0200
          DataSetName = 'frxDsAdver_0200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'courier New'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAdver_0200."Levenshtein_Meu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 45.354360000000000000
          Width = 548.031806060000000000
          Height = 11.338582680000000000
          DataField = 'Levenshtein_Inn'
          DataSet = frxDsAdver_0200
          DataSetName = 'frxDsAdver_0200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'courier New'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAdver_0200."Levenshtein_Inn"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Top = 34.015770000000000000
          Width = 132.283506060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Simplifica'#231#227'o do nome cadastrado:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 132.283506060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Simplifica'#231#227'o do nome importado:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 83.149616060000000000
          Height = 11.338582680000000000
          DataField = 'Distancia'
          DataSet = frxDsAdver_0200
          DataSetName = 'frxDsAdver_0200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAdver_0200."Distancia"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 536.693260000000000000
          Width = 60.472436060000000000
          Height = 11.338582680000000000
          DataField = 'NivelExclu'
          DataSet = frxDsAdver_0200
          DataSetName = 'frxDsAdver_0200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAdver_0200."NivelExclu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsAdver_0200: TfrxDBDataset
    UserName = 'frxDsAdver_0200'
    CloseDataSource = False
    DataSet = QrAdver_0200
    BCDToCurrency = False
    DataSetOptions = []
    Left = 188
    Top = 324
  end
  object QrLocCOD_PART: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Entidade'
      'FROM spedefd0150'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'AND COD_PART=:P3')
    Left = 320
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrLocCOD_PARTEntidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object QrNFsA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, IDCtrl'
      'FROM nfecaba'
      'WHERE EFD_INN_AnoMes <> 0'
      'AND EFD_INN_Empresa <> 0'
      'AND FatID=:P0'
      'AND EFD_INN_AnoMes=:P1'
      'AND EFD_INN_Empresa=:P2')
    Left = 180
    Top = 488
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFsAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFsAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFsAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFsAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object QrLocUNID: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT s190.UnidMed, unm.Grandeza'
      'FROM spedefd0190 s190'
      'LEFT JOIN unidmed unm ON unm.Codigo=s190.UnidMed'
      'WHERE s190.ImporExpor=:P0'
      'AND s190.AnoMes=:P1'
      'AND s190.Empresa=:P2'
      'AND s190.UNID=:P3'
      '')
    Left = 320
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrLocUNIDUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrLocUNIDGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
  end
  object QrE112: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefde112'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 624
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrE112ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE112AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE112Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE112LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE112E111: TIntegerField
      FieldName = 'E111'
    end
    object QrE112REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE112NUM_DA: TWideStringField
      FieldName = 'NUM_DA'
      Size = 255
    end
    object QrE112NUM_PROC: TWideStringField
      FieldName = 'NUM_PROC'
      Size = 15
    end
    object QrE112IND_PROC: TWideStringField
      FieldName = 'IND_PROC'
      Size = 1
    end
    object QrE112PROC: TWideStringField
      FieldName = 'PROC'
      Size = 255
    end
    object QrE112TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
  end
  object DsE112: TDataSource
    DataSet = QrE112
    Left = 652
    Top = 324
  end
  object QrE113: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefde113'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 624
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrE113ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE113AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE113Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE113LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE113E111: TIntegerField
      FieldName = 'E111'
    end
    object QrE113REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE113COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrE113COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrE113SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrE113SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrE113NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrE113DT_DOC: TDateField
      FieldName = 'DT_DOC'
    end
    object QrE113COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrE113VL_AJ_ITEM: TFloatField
      FieldName = 'VL_AJ_ITEM'
    end
  end
  object DsE113: TDataSource
    DataSet = QrE113
    Left = 652
    Top = 352
  end
  object QrE115: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM spedefde115'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      'ORDER BY LinArq')
    Left = 624
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrE115ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE115AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE115Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE115LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE115E110: TIntegerField
      FieldName = 'E110'
    end
    object QrE115REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE115COD_INF_ADIC: TWideStringField
      FieldName = 'COD_INF_ADIC'
      Size = 8
    end
    object QrE115VL_INF_ADIC: TFloatField
      FieldName = 'VL_INF_ADIC'
    end
    object QrE115DESCR_COMPL_AJ: TWideStringField
      FieldName = 'DESCR_COMPL_AJ'
      Size = 255
    end
  end
  object DsE115: TDataSource
    DataSet = QrE115
    Left = 652
    Top = 380
  end
end
