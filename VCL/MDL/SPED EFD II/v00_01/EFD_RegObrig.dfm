object FmEFD_RegObrig: TFmEFD_RegObrig
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: ??? ??? ???'
  ClientHeight = 629
  ClientWidth = 1022
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1022
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 974
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 926
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 151
        Height = 32
        Caption = '??? ??? ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 151
        Height = 32
        Caption = '??? ??? ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 151
        Height = 32
        Caption = '??? ??? ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1022
    Height = 456
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1022
      Height = 456
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 1022
        Height = 49
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label27: TLabel
          Left = 8
          Top = 7
          Width = 116
          Height = 13
          Caption = 'Arquivo a ser carregado:'
        end
        object SpeedButton8: TSpeedButton
          Left = 401
          Top = 23
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton8Click
        end
        object SbCarrega: TSpeedButton
          Left = 425
          Top = 23
          Width = 21
          Height = 21
          Caption = '...'
          Visible = False
        end
        object EdCaminho: TdmkEdit
          Left = 8
          Top = 23
          Width = 389
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = 'C:\SPED EFD PC\Registro 0000.xlsx'
          QryCampo = 'DirNFeGer'
          UpdCampo = 'DirNFeGer'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 'C:\SPED EFD PC\Registro 0000.xlsx'
          ValWarn = False
        end
        object RGTabela: TRadioGroup
          Left = 456
          Top = 0
          Width = 566
          Height = 49
          Align = alRight
          Caption = ' Tabela: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'Indefinido'
            'SPED ICMS / IPI'
            'SPED Contribuik'#231#245'es')
          TabOrder = 1
          OnClick = RGTabelaClick
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 49
        Width = 1022
        Height = 407
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitHeight = 366
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 1022
          Height = 407
          Align = alClient
          TabOrder = 0
          ExplicitHeight = 366
          object PageControl1: TPageControl
            Left = 2
            Top = 15
            Width = 1018
            Height = 390
            ActivePage = TabSheet3
            Align = alClient
            TabOrder = 0
            ExplicitHeight = 349
            object TabSheet1: TTabSheet
              Caption = 'Arquivo carregado'
              object Grade1: TdmkStringGrid
                Left = 0
                Top = 0
                Width = 1010
                Height = 362
                Align = alClient
                DefaultRowHeight = 21
                TabOrder = 0
                ExplicitHeight = 321
              end
            end
            object TabSheet2: TTabSheet
              Caption = 'Blocos definidos'
              ImageIndex = 1
              object Memo1: TMemo
                Left = 0
                Top = 0
                Width = 1010
                Height = 362
                Align = alClient
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                WantReturns = False
                WordWrap = False
                ExplicitHeight = 321
              end
              object SGBlocos: TStringGrid
                Left = 0
                Top = 0
                Width = 1010
                Height = 362
                Align = alClient
                ColCount = 19
                DefaultRowHeight = 21
                RowCount = 2
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
                TabOrder = 1
                ExplicitLeft = 56
                ExplicitTop = 28
                ExplicitHeight = 321
              end
            end
            object TabSheet3: TTabSheet
              Caption = 'Campos definidos'
              ImageIndex = 2
              object SGCampos: TStringGrid
                Left = 0
                Top = 0
                Width = 1010
                Height = 362
                Align = alClient
                ColCount = 19
                DefaultRowHeight = 21
                RowCount = 2
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
                TabOrder = 0
                ExplicitLeft = 56
                ExplicitTop = 28
                ExplicitHeight = 321
                ColWidths = (
                  64
                  64
                  64
                  64
                  64
                  64
                  64
                  64
                  64
                  64
                  64
                  64
                  64
                  64
                  64
                  64
                  64
                  64
                  64)
              end
            end
            object TabSheet4: TTabSheet
              Caption = 'Avisos'
              ImageIndex = 3
              object MeAvisos: TMemo
                Left = 0
                Top = 0
                Width = 1010
                Height = 362
                Align = alClient
                Lines.Strings = (
                  'Memo2')
                TabOrder = 0
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 504
    Width = 1022
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1018
      Height = 21
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object PB1: TProgressBar
      Left = 2
      Top = 36
      Width = 1018
      Height = 17
      Align = alBottom
      TabOrder = 1
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1022
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 876
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 874
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtCarrega: TBitBtn
        Left = 16
        Top = 7
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Carrega'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtCarregaClick
      end
      object BtInsBlcs: TBitBtn
        Tag = 14
        Left = 144
        Top = 7
        Width = 185
        Height = 40
        Caption = 'Insere Blocos'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtInsBlcsClick
      end
      object BtInsFlds: TBitBtn
        Tag = 14
        Left = 332
        Top = 7
        Width = 185
        Height = 40
        Caption = 'Insere Campos'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtInsFldsClick
      end
    end
  end
end
