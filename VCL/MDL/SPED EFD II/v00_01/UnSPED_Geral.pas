unit UnSPED_Geral;

interface

uses
  Windows, Forms, Controls, StdCtrls, ComCtrls, ExtCtrls, Messages, SysUtils,
  Classes, Graphics, Dialogs, Menus, UnMsgInt, UnInternalConsts, UnMLAGeral,
  UnInternalConsts2, dmkGeral, Variants, StrUtils, mySQLDbTables;

type
  TIndicadorPreenchimento = (ipcIndefinido=0, ipcNaoPreencher=1,
    ipcCondicional=2, ipcObrigatorio=3);
  TIndicadorDoTipoDeOperacao = (itoNaoAplicavel, itoEntrada, itoSaida, itoAmbos);
  TFinalidadeEmissaoDocFiscal = (fedfNormal, fedfComplementar, fedfAjuste);
  TUnSPED_Geral = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }

    // Indicador de pagamento # Indicador da forma de pagamento
    function Obtem_NFe_indPag_de_SPED_IND_PGTO(IND_PGTO: String): Integer;
    function Obtem_SPED_IND_PGTO_de_NFe_indPag(indPag: Integer): String;

    // Indicador do tipo de frete # Modalidade do frete
    function Obtem_NFe_modFrete_de_SPED_IND_FRT(IND_FRT: String): Integer;
    function Obtem_SPED_IND_FRT_de_NFe_modFrete(modFrete: Integer): String;

    // Status da NFe # C�digo da situa��o do documento fiscal
    function Obtem_NFe_Status_de_SPED_COD_SIT(COD_SIT: Integer): Integer;
    function Obtem_SPED_COD_SIT_de_NFe_Status(const IND_OPER, IND_EMIT:
             String; const Status: Integer; const DtEmissao, DtSaiEntra,
             DtFiscal: TDateTime; const COD_MOD, SER: String;
             const NUM_DOC, FatID, FatNum, CodInfoEmit, CodInfoDest: Integer;
             const Finalidade: TFinalidadeEmissaoDocFiscal; AnoMesCompetencia:
             Integer; var Situacao: Integer; MeErros: TMemo): Boolean;
    function DefineREG_de_Modelo_de_NF(ide_Mod: Integer; COD_MOD: String): Variant;
    function VDom(const Bloco, Registro, Campo, Valor: String; MeErros: TMemo): Boolean;
    function ObtemTextoDeCodTxtTbSPED(TbSPEDEFD, CodTxt: String): String;
  end;


var
  SPED_Geral: TUnSPED_Geral;

implementation

uses UnMyObjects, Module, MyDBCheck,
  ModuleNFe_0000, ModuleGeral, DmkDAC_PF
 ;

{ TUnSPED_Geral }

function TUnSPED_Geral.DefineREG_de_Modelo_de_NF(ide_Mod: Integer; COD_MOD: String): Variant;
var
  Modelo: Integer;
begin
  Result := Null;
  Modelo := 0;
  if ide_Mod <> 0 then
    Modelo := ide_mod
  else
  begin
    if Geral.SoNumero_TT(COD_MOD) = COD_MOD then
      Modelo := Geral.IMV(COD_MOD);
  end;
  case Modelo of
    0:
    begin
      if COD_MOD = '1B' then
        Result := 'C100'
      else
      if COD_MOD = '2D' then
        Result := 'C400'
      else
      if COD_MOD = '8B' then
        Result := 'D100'
    end;
    01: Result := 'C100';
    02: Result := 'C300';

    04: Result := 'C100';

    06: Result := 'C500';
    07: Result := 'D100';
    08: Result := 'D100';
    09: Result := 'D100';
    10: Result := 'D100';
    11: Result := 'D100';

    13: Result := 'D300';
    14: Result := 'D300';
    15: Result := 'D300';
    16: Result := 'D300';

    18: Result := 'D400';

    21: Result := 'D500';
    22: Result := 'D500';

    26: Result := 'D100';
    27: Result := 'D100';
    28: Result := 'C500';
    29: Result := 'C500';

    55: Result := 'C100';
    57: Result := 'D100';
  end;
end;

function TUnSPED_Geral.ObtemTextoDeCodTxtTbSPED(TbSPEDEFD,
  CodTxt: String): String;
var
  Qry: TmySQLQuery;
begin
  Result := '';
  Qry := TmySQLQuery.Create(DModG);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.AllID_DB, [
    'SELECT Nome ',
    'FROM ' + TbSPEDEFD,
    'WHERE CodTxt="' + CodTxt + '" ',
    '']);
    Result := Qry.FieldByName('Nome').AsString;
  finally
    Qry.Free;
  end;
end;

function TUnSPED_Geral.Obtem_NFe_indPag_de_SPED_IND_PGTO(
  IND_PGTO: String): Integer;
begin
// Indicador de pagamento # Indicador da forma de pagamento
  // � vista
  if IND_PGTO = '0' then Result := 0
  else
  // � Prazo
  if IND_PGTO = '1' then Result := 1
  else
  // Sem Pagamento = outros ?
  if IND_PGTO = '9' then Result := 2
  else
  // Erro
  begin
    Result := 2;
    Geral.MB_ERRO('Indicador da forma de pagamento n�o implementado: ' +
    IND_PGTO + sLineBreak + 'Avise a Dermatek!');
  end;
end;

function TUnSPED_Geral.Obtem_NFe_modFrete_de_SPED_IND_FRT(
  IND_FRT: String): Integer;
begin
// Indicador do tipo de frete # Modalidade do frete
  // Por conta de terceiros
  if IND_FRT = '0' then Result := 2
  else
  // Por conta do emitente
  if IND_FRT = '1' then Result := 0
  else
  // Por conta do destinat�rio / remetente
  if IND_FRT = '2' then Result := 1
  else
  // Sem frete
  if IND_FRT = '9' then Result := 9
  else
  // Erro
  begin
    Result := 9;
    Geral.MB_ERRO('Indicador do tipo de frete n�o implementado: ' +
    IND_FRT + sLineBreak + 'Avise a Dermatek!');
  end;
end;

function TUnSPED_Geral.Obtem_NFe_Status_de_SPED_COD_SIT(
  COD_SIT: Integer): Integer;
begin
// Status da NFe # C�digo da situa��o do documento fiscal
  case COD_SIT of
    00, 01: Result := 100;
    02, 03: Result := 101;
        04: Result := 110;
        05: Result := 102;
        06,
    07, 08: Result := 100;
    else
    begin
      Result := -1; //  N�o existe!
      Geral.MB_ERRO('C�digo da situa��o do documento fiscal n�o implementado: ' +
      FormatFloat('0', COD_SIT) + sLineBreak + 'Avise a Dermatek!');
    end;
  end;
end;

function TUnSPED_Geral.Obtem_SPED_COD_SIT_de_NFe_Status(const IND_OPER, IND_EMIT:
             String; const Status: Integer; const DtEmissao, DtSaiEntra,
             DtFiscal: TDateTime; const COD_MOD, SER: String;
             const NUM_DOC, FatID, FatNum, CodInfoEmit, CodInfoDest: Integer;
             const Finalidade: TFinalidadeEmissaoDocFiscal; AnoMesCompetencia:
             Integer; var Situacao: Integer; MeErros: TMemo): Boolean;
// Status da NFe # C�digo da situa��o do documento fiscal
{
Documento Extempor�neo:
http://www1.receita.fazenda.gov.br/faq/sped-fiscal.htm
=======================
Opera��o de entrada
    Pergunta: Quando um documento fiscal de entrada de mercadorias ou aquisi��o
              de servi�os � considerado extempor�neo?(45)
    Resposta: � o documento de entrada de mercadorias ou aquisi��o de servi�os
              que foi escriturado fora do per�odo de apura��o em que normalmente
              deveria ter sido registrado. Como exemplo, temos a emiss�o de um
              documento em 31/01/2009 e entrada efetiva no estabelecimento em
              01/02/2009. Neste caso, este documento deve ser escriturado como
              documento regular no per�odo de apura��o de fevereiro de 2009.
              Caso seja escriturado em per�odo posterior a fevereiro de 2009,
              observado o prazo decadencial, ser� considerado extempor�neo.
Opera��o de sa�da
    Pergunta: Quando um documento fiscal de sa�da de mercadorias ou presta��o de
              servi�os � considerado extempor�neo?(46)
    Resposta: � o documento de sa�da de mercadorias ou presta��o de servi�os que
              foi escriturado fora do per�odo de apura��o em que normalmente
              deveria ter sido registrado, utilizando como base a data de
              emiss�o do documento fiscal ou a data da efetiva sa�da da
              mercadoria ou presta��o do servi�o, conforme disposto na
              legisla��o estadual.
=======================
Decadencial: Decad�ncia, fora do prazo estipulado.
}
{
vers�o=1.0
00|Documento regular|01012009|
01|Documento regular extempor�neo|01012009|
02|Documento cancelado|01012009|
03|Documento cancelado extempor�neo|01012009|
04|NFe ou CT-e denegada|01012009|
05|NFe ou CT-e Numera��o inutilizada|01012009|
06|Documento Fiscal Complementar|01012009|
07|Documento Fiscal Complementar extempor�neo|01012009|
08|Documento Fiscal emitido com base em Regime Especial ou Norma Espec�fica|01012009|
}
var
  Extemporaneo, EhEntrada(*, EhEmissaoPropria*): Boolean;
  Ano, Mes, Dia: Word;
  AnoMesI, AnoMesF, AnoMesE, Meses: Integer;
  Continua: Boolean;
begin
  Situacao := 98; // for�ar a gerar erro (usar 98 porque 99 j� � o padr�o de registros n�o setados)
  Result := True;
  EhEntrada := IND_OPER = '0';
//  EhEmissaoPropria := IND_EMIT = '0';
  //
  Continua := CodInfoEmit <> CodInfoDest;
  if not Continua then
  begin
    Geral.MensagemArray(MB_ICONERROR,
    'NF inv�lida! Emitende n�o pode ser o mesmo que o dest./rem.',
    ['Modelo de NF:', 'S�rie da NF:', 'N�mero da NF:',
    'Emiss�o', 'Entrada / sa�da', 'Data Fiscal:', 'FatID:', 'FatNum:', 'Status:'],
    [COD_MOD, SER, FormatFloat('0', NUM_DOC), Geral.FDT(DtEmissao, 3),
    Geral.FDT(DtSaiEntra, 3), Geral.FDT(DtFiscal, 3), FormatFloat('0', FAtID)
    + ' - ' + DmNFe_0000.NomeFatID_NFe(FatID), FormatFloat('0', FatNum),
    FormatFloat('0', Status)], MeErros);
    //
    Result := False;
    Exit;
  end else begin
    case Status of
      100,101,102,110,301: Continua := True;
      else Continua := False;
    end;
    if not Continua then
    begin
      Geral.MensagemArray(MB_ICONERROR, 'NF sem status v�lido!',
      ['Modelo de NF:', 'S�rie da NF:', 'N�mero da NF:',
      'Emiss�o', 'Entrada / sa�da', 'Data Fiscal:', 'FatID:', 'FatNum:', 'Status:'],
      [COD_MOD, SER, FormatFloat('0', NUM_DOC), Geral.FDT(DtEmissao, 3),
      Geral.FDT(DtSaiEntra, 3), Geral.FDT(DtFiscal, 3), FormatFloat('0', FAtID)
      + ' - ' + DmNFe_0000.NomeFatID_NFe(FatID), FormatFloat('0', FatNum),
      FormatFloat('0', Status)], MeErros);
      //
      Result := False;
      Exit;
    end;
  end;
  //
  DecodeDate(DtEmissao, Ano, Mes, Dia);
  AnoMesI := Ano * 100 + Mes;
  //
  DecodeDate(DtSaiEntra, Ano, Mes, Dia);
  AnoMesF := Ano * 100 + Mes;
  //
  if AnoMesF > AnoMesI then
    AnoMesE := AnoMesF
  else
    AnoMesE := AnoMesI;
  Meses := AnoMesCompetencia - AnoMesE;
  if Meses < 0 then
  begin
    Geral.MensagemArray(MB_ICONWARNING, 'Data de emiss�o ou entrada ou sa�da maior que a data fiscal!',
    ['Modelo de NF:', 'S�rie da NF:', 'N�mero da NF:',
    'Emiss�o', 'Entrada / sa�da', 'Data Fiscal:', 'FatID:', 'FatNum:'],
    [COD_MOD, SER, FormatFloat('0', NUM_DOC), Geral.FDT(DtEmissao, 3),
    Geral.FDT(DtSaiEntra, 3), Geral.FDT(DtFiscal, 3), FormatFloat('0', FAtID)
    + ' - ' + DmNFe_0000.NomeFatID_NFe(FatID), FormatFloat('0', FatNum)], MeErros);
  end;
  if EhEntrada then
    Extemporaneo := Meses > 1
  else
    Extemporaneo := Meses > 0;
  //
  if Finalidade = fedfComplementar then // se n�o for nf normal ( complementar) e de ajuste?
    Situacao := 06
  else
  begin
    case Status of
      //vers�o=1.0
           100: Situacao := 00; //|Documento regular|01012009|
                              //01|Documento regular extempor�neo|01012009|
           101: Situacao := 02; //|Documento cancelado|01012009|
                              //03|Documento cancelado extempor�neo|01012009|
      110, 301: Situacao := 04; //|NFe ou CT-e denegada|01012009|
           102: Situacao := 05; //|NFe ou CT-e Numera��o inutilizada|01012009|
      (*Feito acima*)         //06|Documento Fiscal Complementar|01012009|
                              //07|Documento Fiscal Complementar extempor�neo|01012009|
      (* ??? Parei aqui *)    //08|Documento Fiscal emitido com base em Regime Especial ou Norma Espec�fica|01012009|
      // Regime especial e Norma espec�fica � o caso de NF sobre cupom!
    end;
  end;
  if Extemporaneo and (Situacao in ([00,02,06])) then
    Situacao := Situacao + 1;
end;

function TUnSPED_Geral.Obtem_SPED_IND_FRT_de_NFe_modFrete(
  modFrete: Integer): String;
begin
// Indicador do tipo de frete # Modalidade do frete
  case modFrete of
    0: Result := '1';
    1: Result := '2';
    // Por conta de terceiros
    2: Result := '0';
    9: Result := '9';
    else
    begin
      Result := '9';
      Geral.MB_ERRO('Modalidade do frete n�o implementado: ' + FormatFloat(
      '0', modFrete) + sLineBreak + 'Avise a Dermatek!');
    end;
  end;
end;

function TUnSPED_Geral.Obtem_SPED_IND_PGTO_de_NFe_indPag(
  indPag: Integer): String;
begin
// Indicador de pagamento # Indicador da forma de pagamento
  case indPag of
    // � vista
    0: Result := '0';
    // � Prazo
    1: Result := '1';
    // Sem Pagamento = outros ?
    //2: Result := '9'; at� 30/06/2012
    2: Result := '2';
    //
    // Erro
    else begin
      Result := '8';
      Geral.MB_ERRO('Forma de pagamento n�o implementada: ' +
      FormatFloat('0', indPag) + sLineBreak + 'Avise a Dermatek!');
    end;
  end;
end;

function TUnSPED_Geral.VDom(const Bloco, Registro, Campo, Valor: String;
  MeErros: TMemo): Boolean;
const
  Dominios: array [0..3] of String = ('0.0000.REG', '0.0000.COD_FIN',
  '0.0000.IND_PERFIL', '0.0000.IND_ATIV');
  //
  _0_0000_01: array [0..00] of String = ('0000');
  _0_0000_03: array [0..01] of String = ('0', '1');
  _0_0000_14: array [0..02] of String = ('A', 'B', 'C');
  _0_0000_15: array [0..01] of String = ('0', '1');
var
  Lista: Integer;
  Item, Aviso: String;
begin
  Result := False;
  Item := Bloco + '.' + Registro + '.' + Campo;
  Lista := AnsiIndexStr(Item, Dominios);
  if Lista > -1 then
  begin
    case Lista of
      00: Result := AnsiMatchStr(Valor, _0_0000_01);
      01: Result := AnsiMatchStr(Valor, _0_0000_03);
      02: Result := AnsiMatchStr(Valor, _0_0000_14);
      03: Result := AnsiMatchStr(Valor, _0_0000_15);
      //
      else Geral.MB_Erro('Dominio SPED-EFD "' + Item + '" inv�lido! [1]');
    end;
  end else Result := True;
    //Geral.MB_Aviso('Dominio SPED-EFD "' + Item + '" inv�lido! [2]');
  if not Result then
  begin
    Aviso :=
    'SPED-EFD item #' + Item + '": ' + 'Valor inv�lido: "' + Valor + '"';
    if MeErros <> nil then
      MeErros.Lines.Add(Aviso)
    else
      Geral.MB_Aviso(Aviso);
  end;
end;

end.

