unit EFD_E110;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask;

type
  TFmEFD_E110 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdE100: TdmkEdit;
    Label6: TLabel;
    Label1: TLabel;
    TPDT_INI: TdmkEditDateTimePicker;
    TPDT_FIN: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label7: TStaticText;
    EdVL_TOT_DEBITOS: TdmkEdit;
    Label8: TStaticText;
    EdVL_AJ_DEBITOS: TdmkEdit;
    Label9: TStaticText;
    EdVL_TOT_AJ_DEBITOS: TdmkEdit;
    Label10: TStaticText;
    EdVL_ESTORNOS_CRED: TdmkEdit;
    Label11: TStaticText;
    EdVL_TOT_CREDITOS: TdmkEdit;
    Label12: TStaticText;
    EdVL_AJ_CREDITOS: TdmkEdit;
    Label13: TStaticText;
    EdVL_TOT_AJ_CREDITOS: TdmkEdit;
    Label14: TStaticText;
    EdVL_ESTORNOS_DEB: TdmkEdit;
    Label15: TStaticText;
    EdVL_SLD_CREDOR_ANT: TdmkEdit;
    Label16: TStaticText;
    EdVL_SLD_APURADO: TdmkEdit;
    Label17: TStaticText;
    EdVL_TOT_DED: TdmkEdit;
    Label18: TStaticText;
    EdVL_ICMS_RECOLHER: TdmkEdit;
    Label19: TStaticText;
    EdVL_SLD_CREDOR_TRANSPORTAR: TdmkEdit;
    Label20: TStaticText;
    EdDEB_ESP: TdmkEdit;
    EdLinArq: TdmkEdit;
    Label21: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEFD_E110: TFmEFD_E110;

implementation

uses UnMyObjects, Module, EFD_E001, UMySQLModule;

{$R *.DFM}

procedure TFmEFD_E110.BtOKClick(Sender: TObject);
const
  REG = 'E110';
var
  E100, ImporExpor, AnoMes, Empresa, LinArq: Integer;
  VL_TOT_DEBITOS,
  VL_AJ_DEBITOS, VL_TOT_AJ_DEBITOS, VL_ESTORNOS_CRED,
  VL_TOT_CREDITOS, VL_AJ_CREDITOS, VL_TOT_AJ_CREDITOS,
  VL_ESTORNOS_DEB, VL_SLD_CREDOR_ANT, VL_SLD_APURADO,
  VL_TOT_DED, VL_ICMS_RECOLHER, VL_SLD_CREDOR_TRANSPORTAR,
  DEB_ESP: Double;
begin
  E100 := EdE100.ValueVariant; 
  VL_TOT_DEBITOS :=        EdVL_TOT_DEBITOS.ValueVariant;
  VL_AJ_DEBITOS :=         EdVL_AJ_DEBITOS.ValueVariant;
  VL_TOT_AJ_DEBITOS :=     EdVL_TOT_AJ_DEBITOS.ValueVariant;
  VL_ESTORNOS_CRED :=      EdVL_ESTORNOS_CRED.ValueVariant;
  VL_TOT_CREDITOS :=       EdVL_TOT_CREDITOS.ValueVariant;
  VL_AJ_CREDITOS :=        EdVL_AJ_CREDITOS.ValueVariant;
  VL_TOT_AJ_CREDITOS :=    EdVL_TOT_AJ_CREDITOS.ValueVariant;
  VL_ESTORNOS_DEB :=       EdVL_ESTORNOS_DEB.ValueVariant;
  VL_SLD_CREDOR_ANT :=     EdVL_SLD_CREDOR_ANT.ValueVariant;
  VL_SLD_APURADO :=        EdVL_SLD_APURADO.ValueVariant;
  VL_TOT_DED :=            EdVL_TOT_DED.ValueVariant;
  VL_ICMS_RECOLHER :=      EdVL_ICMS_RECOLHER.ValueVariant;
  VL_SLD_CREDOR_TRANSPORTAR :=    EdVL_SLD_CREDOR_TRANSPORTAR.ValueVariant;
  DEB_ESP :=               EdDEB_ESP.ValueVariant;
  //
  ImporExpor := EdImporExpor.ValueVariant;
  AnoMes := EdAnoMes.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  LinArq := EdLinArq.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'efd_e110', False, [
  'E100', 'REG', 'VL_TOT_DEBITOS',
  'VL_AJ_DEBITOS', 'VL_TOT_AJ_DEBITOS', 'VL_ESTORNOS_CRED',
  'VL_TOT_CREDITOS', 'VL_AJ_CREDITOS', 'VL_TOT_AJ_CREDITOS',
  'VL_ESTORNOS_DEB', 'VL_SLD_CREDOR_ANT', 'VL_SLD_APURADO',
  'VL_TOT_DED', 'VL_ICMS_RECOLHER', 'VL_SLD_CREDOR_TRANSPORTAR',
  'DEB_ESP'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  E100, REG, VL_TOT_DEBITOS,
  VL_AJ_DEBITOS, VL_TOT_AJ_DEBITOS, VL_ESTORNOS_CRED,
  VL_TOT_CREDITOS, VL_AJ_CREDITOS, VL_TOT_AJ_CREDITOS,
  VL_ESTORNOS_DEB, VL_SLD_CREDOR_ANT, VL_SLD_APURADO,
  VL_TOT_DED, VL_ICMS_RECOLHER, VL_SLD_CREDOR_TRANSPORTAR,
  DEB_ESP], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
    FmEFD_E001.ReopenEFD_E100(LinArq);
    Close;
  end;
end;

procedure TFmEFD_E110.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_E110.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_E110.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
