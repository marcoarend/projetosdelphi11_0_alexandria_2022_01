unit EFD_H020;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup;

type
  TFmEFD_H020 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdH010: TdmkEdit;
    Label6: TLabel;
    TPDT_INV: TdmkEditDateTimePicker;
    Label2: TLabel;
    EdLinArq: TdmkEdit;
    Label9: TLabel;
    Panel5: TPanel;
    StaticText2: TStaticText;
    EdBC_ICMS: TdmkEdit;
    StaticText3: TStaticText;
    EdVL_ICMS: TdmkEdit;
    Label1: TLabel;
    QrTbSPEDEFD130: TmySQLQuery;
    QrTbSPEDEFD130CodTxt: TWideStringField;
    QrTbSPEDEFD130Nome: TWideStringField;
    DsTbSPEDEFD130: TDataSource;
    EdCST_ICMS: TdmkEditCB;
    CBCST_ICMS: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FAnoMes: Integer;
    //
    procedure ReopenTabelas();
  end;

  var
  FmEFD_H020: TFmEFD_H020;

implementation

uses UnMyObjects, Module, EFD_E001, UMySQLModule, DmkDAC_PF, MyListas,
  ModuleGeral, UnDmkProcFunc;

{$R *.DFM}

procedure TFmEFD_H020.BtOKClick(Sender: TObject);
const
  REG = 'H020';
var
  ImporExpor, AnoMes, Empresa, H010, LinArq, CST_ICMS: Integer;
  BC_ICMS, VL_ICMS: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  H010           := EdH010.ValueVariant;
  LinArq         := EdLinArq.ValueVariant;
  //REG            := ;
  CST_ICMS       := EdCST_ICMS.ValueVariant;
  BC_ICMS        := EdBC_ICMS.ValueVariant;
  VL_ICMS        := EdVL_ICMS.ValueVariant;
  //
  if MyObjects.FIC(CST_ICMS = 0, EdCST_ICMS, 'Informe o CST!') then
    Exit;
  //
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_h020', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  ImgTipo.SQLType, LinArq, siPositivo, EdLinArq);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efd_h020', False, [
  'H010', 'REG', 'CST_ICMS',
  'BC_ICMS', 'VL_ICMS'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  H010, REG, CST_ICMS,
  BC_ICMS, VL_ICMS], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
    FmEFD_E001.ReopenEFD_H020(LinArq);
    Close;
  end;
end;

procedure TFmEFD_H020.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_H020.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_H020.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEFD_H020.ReopenTabelas();
var
  Ini, Fim, SQL_Periodo_Valido: String;
begin
  //FAnoMes := EdAnoMes.ValueVariant;
  Ini := Geral.FDT(DmkPF.DatadeAnoMes(FAnoMes, 1, 0), 1);
  Fim := Geral.FDT(DmkPF.DatadeAnoMes(FAnoMes + 1, 1, -1), 1);
  SQL_Periodo_Valido :=
  'WHERE DataIni <= "' + Ini + '" ' + sLineBreak +
  'AND (DataFim >="' + Fim + '" OR DataFim<2)  ';
  // CST
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD130, DModG.AllID_DB, [
 'SELECT *  ',
 'FROM tbspedefd130 ',
 SQL_Periodo_Valido,
 'ORDER BY Nome ',
 '']);
end;

end.
