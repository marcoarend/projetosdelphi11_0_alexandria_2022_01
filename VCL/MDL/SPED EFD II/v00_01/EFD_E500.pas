unit EFD_E500;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmEFD_E500 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    TPDT_INI: TdmkEditDateTimePicker;
    Label1: TLabel;
    TPDT_FIN: TdmkEditDateTimePicker;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdLinArq: TdmkEdit;
    Label6: TLabel;
    RGIND_APUR: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEFD_E500: TFmEFD_E500;

implementation

uses UnMyObjects, Module, EFD_E001, UMySQLModule;

{$R *.DFM}

procedure TFmEFD_E500.BtOKClick(Sender: TObject);
const
  REG = 'E500';
var
  DT_INI, DT_FIN, IND_APUR: String;
  ImporExpor, AnoMes, Empresa, LinArq: Integer;
begin
  DT_INI := Geral.FDT(TPDT_INI.Date, 1);
  DT_FIN := Geral.FDT(TPDT_FIN.Date, 1);
  //
  if MyObjects.FIC(RGIND_APUR.ItemIndex < 0, nil, 'Informe o indicador de per�odo!') then Exit;
  IND_APUR := RGIND_APUR.Items[RGIND_APUR.ItemIndex][1];
  ImporExpor := EdImporExpor.ValueVariant;
  AnoMes := EdAnoMes.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  //
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_e500', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  ImgTipo.SQLType, 0, siPositivo, EdLinArq);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'efd_e500', False, [
  'REG', 'IND_APUR', 'DT_INI', 'DT_FIN'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  REG, IND_APUR, DT_INI, DT_FIN], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
    FmEFD_E001.ReopenEFD_E500(LinArq);
    Close;
  end;
end;

procedure TFmEFD_E500.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_E500.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_E500.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
