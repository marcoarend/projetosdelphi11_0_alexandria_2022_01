unit UnSPED_EFD_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) dmkEdit, dmkRadioGroup,
  dmkMemo, AdvToolBar, dmkCheckGroup, UnDmkProcFunc, TypInfo, UnMyObjects,
  dmkEditDateTimePicker;

//const
  //FImporExpor = 2; // 2 = Exporta - N�o mexer!!!

type
  TEstagioVS_SPED = (evsspedNone=0, evsspedEncerraVS=1, evsspedGeraSPED=2,
                     evsspedExportaSPED=3);
  TCorrApoFormaLctoPosNegSPED = (caflpnIndef=0, caflpnMovNormEsquecido=1,
                               caflpnDesfazimentoMov=2);
  TUnSPED_EFD_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AvisoItemJaImportado(MeAviso: TMemo; IDItem, GraGruX: Integer;
              REG: String);
    procedure AvisoQtdeZero(MeAviso: TMemo; IDItem, GraGruX, MovimCod, Controle:
              Integer; REG: String);
    procedure BaixaLayoutSPED(Memo: TMemo);
    function  DefineDatasPeriodoSPED(const Empresa, AnoMes: Integer;
              const Registro: String; const LastDtIni, LastDtFim: TDateTime;
              var NewDtIni, NewDtFim: TDateTime): Boolean;
    function  DefineDatasPeriCorrApoSPED(const Empresa: Integer; const Registro:
              String; const DtLancto, DtCorrApo: TDateTime;
              var ThatDtIni, ThatDtFim: TDateTime): Boolean;
    function  DefineQuantidadePositivaOuNegativa(const regOri, REG: String;
              const ValAntes, ValNovo: Double; const FormaLcto:
              TCorrApoFormaLctoPosNegSPED; var QTD_COR_POS, QTD_COR_NEG:
              Double): Boolean;
    function  EncerraMesSPED(ImporExpor, AnoMes, Empresa: Integer; Campo:
              String): Boolean;
    procedure MostraFormSPEDEFDEnce();

              //

    function  DataFimObrigatoria(DtHrFimOpe, DiaFim: TDateTime): TDateTime;
    function  ErrGrandeza(TabSrc, GGxSrc, GGXDst: String): Boolean;
    function  InsereItemAtual_K210(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, Controle: Integer; const DataIni,
              DataFim, DtMovim, DtCorrApo: TDateTime; const GraGruX, ClientMO,
              FornecMO, EntiSitio: Integer; const Qtde: Double; const
              OrigemOpeProc: TOrigemOpeProc; const OriIDKnd: TOrigemIDKnd;
              const DiaFim: TDateTime; const TipoPeriodoFiscal:
              TTipoPeriodoFiscal; const MeAviso: TMemo; var IDSeq1: Integer;
              Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K215(const ImporExpor, AnoMes, Empresa, PeriApu,
              IDSeq1: Integer; Data, DtMovim, DtCorrApo: TDateTime; GraGruX:
              Integer; Qtde: Double; COD_INS_SUBST: String; ID_Item, MovimID,
              Codigo, MovimCod, Controle, ClientMO, FornecMO, EntiSitio:
              Integer; OrigemOpeProc: TOrigemOpeProc; OriIDKnd: TOrigemIDKnd;
              OriESTSTabSorc: TEstqSPEDTabSorc; TipoPeriodoFiscal:
              TTipoPeriodoFiscal; MeAviso: TMemo): Boolean;
              //...
    function  InsereItemAtual_K230(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, Controle:
              Integer; const DataIni, DataFim, DtMovim, DtCorrApo: TDateTime;
              const GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde:
              Double; const OrigemOpeProc: TOrigemOpeProc; DiaFim: TDateTime;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso: TMemo;
              var IDSeq1: Integer; Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K235(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1:
              Integer; Data, DtCorrApo:
              TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
              ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
              EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
              TEstqSPEDTabSorc; const TipoPeriodoFiscal:
              TTipoPeriodoFiscal; const MeAviso: TMemo): Boolean;
    function  InsereItemAtual_K250(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, Controle:
              Integer; const DtHrFimOpe, DtMovim, DtCorrApo: TDateTime; const
              GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde:
              Double; const OrigemOpeProc: TOrigemOpeProc; DiaFim: TDateTime;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso:
              TMemo; var IDSeq1: Integer; Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K255(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1:
              Integer; Data, DtCorrApo:
              TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
              ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
              EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
              TEstqSPEDTabSorc; const TipoPeriodoFiscal:
              TTipoPeriodoFiscal; const MeAviso: TMemo): Boolean;
    function  InsereItemAtual_K260(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, Controle:
              Integer; const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd,
              DtCorrApoCons: TDateTime; const GraGruX(*, CtrlDst, CtrlBxa*),
              ClientMO, FornecMO, EntiSitio: Integer; const QtdeProd, QtdeCons:
              Double; const OrigemOpeProc: TOrigemOpeProc; const TipoPeriodoFiscal:
              TTipoPeriodoFiscal; const MeAviso: TMemo; var IDSeq1: Integer;
              const Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K265(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1:
              Integer; Data, DtCorrApo: TDateTime;
              GraGruX: Integer; QtdeCons, QtdeRet: Double; COD_INS_SUBST:
              String; ID_Item, MovimID, Codigo, MovimCod, Controle: Integer;
              OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso:
              TMemo): Boolean;
    function  InsereItemAtual_K270(const ImporExpor, AnoMes, Empresa, PeriApu:
              Integer; const RegOri: String; const Data, DtCorrApo:
              TDateTime; const MovimID, Codigo, MovimCod, Controle, GraGruX:
              Integer; const Qtde: Double; const SPED_EFD_KndRegOrigem:
              TSPED_EFD_KndRegOrigem; OrigemOpeProc: TOrigemOpeProc;
              OrigemIDKnd: TOrigemIDKnd; const OriSPEDEFDKnd: TOrigemSPEDEFDKnd;
              MeAviso: TMemo; var IDSeq1: Integer): Boolean;
    function  InsereItemAtual_K275(const ImporExpor, AnoMes, Empresa, PeriApu:
              Integer; RegOri: String;  MovimID, Codigo, MovimCod,
              Controle, GraGruX, IDSeq1: Integer; Qtde: Double;
              COD_INS_SUBST: String; SPED_EFD_KndRegOrigem:
              TSPED_EFD_KndRegOrigem; ESTSTabSorc: TEstqSPEDTabSorc;
              OrigemOpeProc: TOrigemOpeProc; OriIDKnd: TOrigemIDKnd;
              const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd; MeAviso: TMemo): Boolean;
    function  InsereItensAtuais_K280_Old(BalTab: TEstqSPEDTabSorc;
              TipoPeriodoFiscal: TTipoPeriodoFiscal;
              ImporExpor, AnoMes, Empresa, K100: Integer; const RegPai,
              RegAvo: String; const DataSPED, DtCorrApo: TDateTime;
              const MovimID, Codigo, MovimCod, Controle, GraGruX, ClientMO,
              FornecMO, EntiSitio: Integer; LocalKnd: TSPEDLocalKnd;
              const Qtde: Double; const OrigemBalID: TSPED_EFD_Bal;
              const SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
              const OriESTSTabSorc: TEstqSPEDTabSorc; const OriOrigemOpeProc:
              TOrigemOpeProc; const OriOrigemIDKnd: TOrigemIDKnd;
              const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd;
              const Agrega: Boolean; var LinArq: Integer): Boolean;
    function  InsereItensAtuais_K280_New(BalTab: TEstqSPEDTabSorc;
              TipoPeriodoFiscal: TTipoPeriodoFiscal;
              ImporExpor, AnoMes, Empresa, K100_PeriApu: Integer; const RegPai,
              RegAvo: String; const DataSPED, DtCorrApo: TDateTime;
              const MovimID, Codigo, MovimCod, Controle, GraGruX, ClientMO,
              FornecMO, EntiSitio: Integer; LocalKnd: TSPEDLocalKnd;
              const Qtde: Double; const OrigemBalID: TSPED_EFD_Bal;
              const SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
              const OriESTSTabSorc: TEstqSPEDTabSorc; const OriOrigemOpeProc:
              TOrigemOpeProc; const OriOrigemIDKnd: TOrigemIDKnd;
              const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd;
              const Agrega: Boolean; const MeAviso: TMemo): Boolean;
    function  InsereItemAtual_K290(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, CodDocOP(*, Controle*): Integer; const
              DataIni, DataFim, (*DtMovim,*) DtCorrApo: TDateTime; const (*GraGruX,*)
              ClientMO, FornecMO, EntiSitio: Integer; (*const Qtde: Double;*)
              const OrigemOpeProc: TOrigemOpeProc; const DiaFim: TDateTime;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo; var IDSeq1: Integer):
              Boolean;
    function  InsereItemAtual_K291(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
              JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod,
              PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
              GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod,
              Controle, ClientMO, FornecMO, EntiSitio: Integer; OrigemOpeProc:
              TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc; const
              TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
    function  InsereItemAtual_K292(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
              JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod,
              PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
              GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod,
              Controle, ClientMO, FornecMO, EntiSitio: Integer; OrigemOpeProc:
              TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc; const
              TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
    function  InsereItemAtual_K300(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, CodDocOP(*, Controle*): Integer; const
              DataIni, DataFim, DtMovim, DtCorrApo: TDateTime; const (*GraGruX,*)
              ClientMO, FornecMO, EntiSitio: Integer; (*const Qtde: Double;*)
              const OrigemOpeProc: TOrigemOpeProc; const DiaFim: TDateTime;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo; var IDSeq1: Integer):
              Boolean;
    function  InsereItemAtual_K301(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
              JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod,
              PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
              GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod,
              Controle, ClientMO, FornecMO,
              EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
              TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
              const TabSorc: TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
    function  InsereItemAtual_K302(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
              JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod,
              PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
              GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod,
              Controle, ClientMO, FornecMO, EntiSitio: Integer; OrigemOpeProc:
              TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc; const
              TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
              // ...


    function  LiberaAcaoVS_SPED(ImporExpor, AnoMes, Empresa: Integer; EstagioVS_SPED:
              TEstagioVS_SPED): Boolean;
    procedure MostraFormEFD_C001();
    procedure MostraFormEFD_D001();
    //procedure MostraFormEFD_E001();
    procedure MostraFormSPED_EFD_Exporta();
    //
    procedure MostraFormEfdIcmsIpiE001();

    //
    function  ObtemIND_ESTeCOD_PART(const Empresa, ClientMO, Terceiro, GraGruX:
              Integer; var IND_EST, COD_PART: String; var Entidade: Integer):
              String;
    function  ObtemSPED_EFD_KndRegOrigem(const REG: String; var Origem:
              TSPED_EFD_KndRegOrigem): Boolean;
    function  ObtemTipoPeriodoRegistro(Empresa: Integer; Registro, ProcOri: String):
              Integer;
    function  ObtemPeriodoSPEDdeData(Data: TDateTime; TipoPeriodoFiscal:
              TTipoPeriodoFiscal): Integer;
    function  ObtemDatasDePeriodoSPED(const Ano, Mes, Periodo: Integer;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; var DataIni, DataFim:
              TDateTime): Boolean;
    function  ObtemTipoDeProducaoSPED(EntiEmp: Integer): Integer;
    procedure ReopenTbSpedEfdXXX(Query: TmySQLQuery; DtIni, DtFim: TDateTime;
              TabName, FldOrd: String);
    function  SPEDEFDEnce_AnoMes(Campo: String): Integer;
    function  SPEDEFDEnce_DataMinMovim(Campo: String): TDateTime;
    function  SPEDEFDEnce_Periodo(Campo: String): Integer;
    function  SPEDEFDEnce_DataUltimoInventario(Empresa: Integer): TDateTime;
    function  SQLPeriodoFiscal(TipoPeriodoFiscal: TTipoPeriodoFiscal; CampoPsq,
              CampoRes: String; VirgulaFinal: Boolean): String;
    function  VerificaGGX_SPED(GraGruX, KndCod, KndNSU, KndItm: Integer;
              REG: String): Boolean;

    function  DefineDebCred(Valor: Double): TDebCred; overload;
    function  DefineDebCred(ValPos, ValNeg: Double): TDebCred; overload;
    procedure ConfigDtasMinMaxAnoMesSPED(AnoMes: Integer; TPDT_SPED:
              TdmkEditDateTimePicker);
    function  ValidaDataItemDentroDoAnoMesSPED(AnoMes: Integer; Data, DtIniOP,
              DtFimOP: TDateTime; OrdemDeServico: String): Boolean;
    function  ValidaDataDentroDoAnoMesSPED(AnoMes: Integer; Data:
              TDateTime; OrdemDeServico: String; Obrigatorio: Boolean): Boolean;
    function  ValidaDataInicialDentroDoAnoMesSPED(AnoMes: Integer; DataIni,
              DataFim: TDateTime; OrdemDeServico: String): Boolean;
    function  ValidaDataFinalDentroDoAnoMesSPED(AnoMes: Integer; DataIni,
              DataFim: TDateTime; OrdemDeServico: String): Boolean;
    function  ValidaOSDentroDoAnoMesSPED(AnoMes: Integer; DataIni,
              DataFim: TDateTime; OrdemDeServico: String): Boolean;
    function  ValidaQtdeDentroDoAnoMesSPED(AnoMes: Integer; Qtde: Double;
              OrdemDeServico: String; MaiorQueZero: Boolean): Boolean;
    function  ValidaGGXEstahDentroDoAnoMesSPED(AnoMes: Integer; GraGruX:
              Integer; OrdemDeServico: String): Boolean;
    function  ValidaGGXOri_DifereGGXDstDentroDoAnoMesSPED(AnoMes, GGXOri,
              GGXDst: Integer; OrdemDeServico: String): Boolean;
    function  ValidaExigenciaValorMaiorQueZeroOuZero(AnoMes: Integer; DataIni,
              DataFim: TDateTime; Qtd: Double; OrdemDeServico: String): Boolean;
    function  ValidaCOD_INS_SUBST_DentroDoAnoMesSPED(AnoMes, GraGruX: Integer;
              COD_INS_SUBST, COD_DOC_OS: String): Boolean;
    procedure MostraFormSpedEfdIcmsIpiClaProd(ImporExpor, AnoMes, Empresa,
              LinArq: Integer; Registro: String; Data: TDateTime);
    procedure MostraFormSpedefdIcmsIpiBalancos(ImporExpor, AnoMes, Empresa,
              LinArq: Integer; Registro: String; Data: TDateTime);
  end;
var
  SPED_EFD_PF: TUnSPED_EFD_PF;

implementation

uses UnMLAGeral, MyDBCheck, DmkDAC_PF, Module, ModuleGeral, UnDmkWeb, Restaura2,
  SPED_EFD_Exporta_v03_0_1, (*EFD_C001, EFD_D001, EFD_E001,*) UMySQLModule, UnGrade_PF,
  EfdIcmsIpiE001_v03_0_1, SPEDEFDEnce_v03_0_1,
  SpedEfdIcmsipiBalancos_v03_0_1, SpedEfdIcmsIpiClaProd_v03_0_1,
  ModAppGraG1, ModVS;


{ TUnSPED_EFD_PF }

procedure TUnSPED_EFD_PF.AvisoItemJaImportado(MeAviso: TMemo; IDItem, GraGruX:
  Integer; REG: String);
begin
  if MeAviso <> nil then
    MeAviso.Lines.Add('Item j� importado! > ID: ' + Geral.FF0(IDItem) +
    ' Reduzido: ' + Geral.FF0(GraGruX) + ' ' + REG);
end;

procedure TUnSPED_EFD_PF.AvisoQtdeZero(MeAviso: TMemo; IDItem, GraGruX,
  MovimCod, Controle: Integer; REG: String);
begin
  if MeAviso <> nil then
    MeAviso.Lines.Add('Item com quantidade zerada! > ID: ' + Geral.FF0(IDItem) +
    ' Reduzido: ' + Geral.FF0(GraGruX) + ' ' + REG + ' IME-C ' + Geral.FF0(
    MovimCod) + ' IME-I ' + Geral.FF0(Controle));
end;

procedure TUnSPED_EFD_PF.BaixaLayoutSPED(Memo: TMemo);
var
  Res: Boolean;
  ArqNome: String;
  Versao: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT VerSPEDLay ',
      'FROM spedefdicmsipictrl ',
      '']);
    Res := DmkWeb.VerificaAtualizacaoVersao2(True, True, CO_WEBID_SPEDLAY,
             'Layout Sped', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value),
             Qry.FieldByName('VerSPEDLay').AsInteger, 0, DModG.ObtemAgora(), Memo,
             dtSQLs, Versao, ArqNome, False);
    if (Res) and (VAR_DOWNLOAD_OK) then
    begin
      //C:\Projetos\Delphi 2007\Aplicativos\Auxiliares\DermaBK\Restaura.pas
      Application.CreateForm(TFmRestaura2, FmRestaura2);
      FmRestaura2.Show;
      FmRestaura2.EdBackFile.Text := CO_DIR_RAIZ_DMK + '\' + ArqNome + '.SQL';
      //FmRestaura2.BeRestore.DatabaseName := DMod.MyDB.DatabaseName;
      FmRestaura2.FDB := DModG.AllID_DB;
      FmRestaura2.BtDiretoClick(Self);
      FmRestaura2.Close;
      //
      //Atualiza vers�o no banco de dados
      UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
        'UPDATE spedefdicmsipictrl  ',
        'SET VerSPEDLay=' + Geral.FF0(Versao),
        '']);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnSPED_EFD_PF.ConfigDtasMinMaxAnoMesSPED(AnoMes: Integer;
  TPDT_SPED: TdmkEditDateTimePicker);
var
  DtIni, DtFim: TDateTime;
begin
  TPDT_SPED.MinDate := 0;
  TPDT_SPED.MaxDate := 0;
  if AnoMes > 0 then
  begin
    DtIni := dmkPF.DatadeAnoMes(AnoMes, 1, 0);
    DtFim := IncMonth(DtIni, 1) - 1;
    TPDT_SPED.Date := DtFim;
    TPDT_SPED.MinDate := DtIni;
    TPDT_SPED.MaxDate := DtFim;
  end;
end;

function TUnSPED_EFD_PF.DataFimObrigatoria(DtHrFimOpe,
  DiaFim: TDateTime): TDateTime;
begin
  if DtHrFimOpe < 2 then
    Result :=  DiaFim
  else
  if DtHrFimOpe > DiaFim then
    Result :=  DiaFim
  else
    Result := DtHrFimOpe;
end;

function TUnSPED_EFD_PF.DefineDatasPeriCorrApoSPED(const Empresa: Integer;
  const Registro: String; const DtLancto, DtCorrApo: TDateTime;
  var ThatDtIni, ThatDtFim: TDateTime): Boolean;
const
  sProcName = 'TUnSPED_EFD_PF.DefineDatasPeriCorrApoSPED()';
var
  TipoPer, PeriAtu, PeriAnt, PerLine, DiaIni, DiaFim: Integer;
  Dia, Mes, Ano: Word;
begin
  TipoPer := ObtemTipoPeriodoRegistro(Empresa, Registro, sProcName);
  PeriAtu := ObtemPeriodoSPEDdeData(DtLancto, TTipoPeriodoFiscal(TipoPer));
  PeriAnt := ObtemPeriodoSPEDdeData(DtCorrApo, TTipoPeriodoFiscal(TipoPer));
  PerLine := PeriAtu - PeriAnt;
  if not PerLine in ([1,2]) then
    Geral.MB_Aviso('Diferen�a de tempo pass�vel de recusa!');
  case TTipoPeriodoFiscal(TipoPer) of
    spedperDecendial:
    begin
      DecodeDate(DtCorrApo, Ano, Mes, Dia);
      case Dia of
        00..10:
        begin
          ThatDtIni := EncodeDate(Ano, Mes, 1);
          ThatDtFim := EncodeDate(Ano, Mes, 10);
        end;
        11..20:
        begin
          ThatDtIni := EncodeDate(Ano, Mes, 11);
          ThatDtFim := EncodeDate(Ano, Mes, 20);
        end;
        21..31:
        begin
          ThatDtIni := EncodeDate(Ano, Mes, 21);
          ThatDtFim := IncMonth(ThatDtIni, 1);
          DecodeDate(ThatDtFim, Ano, Mes, Dia);
          ThatDtFim := EncodeDate(Ano, Mes, 1) - 1;
        end;
      end;
    end;
    spedperMensal:
    begin
      DecodeDate(DtCorrApo, Ano, Mes, Dia);
      ThatDtIni := EncodeDate(Ano, Mes, 1);
      ThatDtFim := IncMonth(ThatDtIni, 1) - 1;
    end;
  end;
end;

function TUnSPED_EFD_PF.DefineDatasPeriodoSPED(const Empresa, AnoMes: Integer;
  const Registro: String; const LastDtIni, LastDtFim: TDateTime; var NewDtIni,
  NewDtFim: TDateTime): Boolean;
const
  sProcName = 'TUnSPED_EFD_PF.DefineDatasPeriodoSPED()';
  //
  procedure OverPeriodo();
  begin
    Geral.MB_Aviso('N�o h� mais per�odos dispon�veis para o m�s selecionado!');
    NewDtIni := 0;
    NewDtFim := 0;
  end;
var
  Periodo, AnoAM, MesAM, Incremento: Integer;
  AnoPe, MesPe, DiaPe: word;
begin
  Geral.AnoMesToAnoEMes(AnoMes, AnoAM, MesAM);
  NewDtIni := Geral.AnoMesToData(AnoMes, 1);
  NewDtFim := IncMonth(NewDtIni) - 1;
  Result := False;
  //
  Periodo := ObtemTipoPeriodoRegistro(Empresa, Registro, sProcName);
{
  DModG.ReopenParamsEmp(Empresa);
  Periodo := 0;
  Incremento := 0;
  //
  if Registro = 'E100' then
    Periodo := DModG.QrParamsEmpSPED_EFD_Peri_E100.Value
  else
  if Registro = 'E500' then
    Periodo := DModG.QrParamsEmpSPED_EFD_Peri_E500.Value
  else
  if Registro = 'K100' then
    Periodo := DModG.QrParamsEmpSPED_EFD_Peri_K100.Value;
  //
  if Periodo < 1 then
  begin
    Geral.MB_Erro(
    'Tipo de per�odo indefinido em "TUnSPED_EFD_PF.DefineDatasPeriodoSPED()"');
    Exit;
  end;
}
  case TTipoPeriodoFiscal(Periodo) of
    TTipoPeriodoFiscal.spedperDecendial:
    begin
      if LastDtIni = 0 then
      begin
        NewDtIni := Geral.AnoMesToData(AnoMes, 1);
        NewDtFim := NewDtIni + 9;
      end else
      begin
        DecodeDate(LastDtFim, AnoPe, MesPe, DiaPe);
        DiaPe := DiaPe + 1;
        if DiaPe > 21 then
          OverPeriodo()
        else
        begin
          NewDtIni := EncodeDate(AnoPe, MesPe, DiaPe);
          NewDtFim := NewDtIni + 9;
          if DiaPe >= 20  then
            NewDtFim :=
              Geral.AnoMesToData(Geral.IncrementaMes_AnoMes(AnoMes, 1), 1) - 1;
        end;
      end;
    end;
    TTipoPeriodoFiscal.spedperMensal:
    begin
      if LastDtIni = 0 then
      begin
        NewDtIni := Geral.AnoMesToData(AnoMes, 1);
        NewDtFim := Geral.AnoMesToData(AnoMes + 1, 1) - 1;
      end else
        OverPeriodo();
    end;
    else begin
    Geral.MB_Erro(
    'Tipo de per�odo n�o implementado em "TUnSPED_EFD_PF.DefineDatasPeriodoSPED()"');
    Exit;
    end;
  end;
end;

function TUnSPED_EFD_PF.DefineDebCred(ValPos, ValNeg: Double): TDebCred;
begin
  if (ValPos <> 0) and (ValNeg <> 0) then
    Result    := TDebCred.debcredAmbos
  else
  if ValPos > 0 then
    Result    := TDebCred.debcredCred
  else
  if ValNeg > 0 then
    Result    := TDebCred.debcredDeb
  else
    Result    := TDebCred.debcredND;
end;

function TUnSPED_EFD_PF.DefineDebCred(Valor: Double): TDebCred;
begin
  if Valor >= 0 then
    Result    := TDebCred.debcredCred
  else
    Result    := TDebCred.debcredDeb;
end;

function TUnSPED_EFD_PF.DefineQuantidadePositivaOuNegativa(const RegOri, REG:
  String; const ValAntes, ValNovo: Double; const FormaLcto:
  TCorrApoFormaLctoPosNegSPED; var QTD_COR_POS, QTD_COR_NEG: Double): Boolean;
  //
  procedure Aviso();
  begin
    Geral.MB_Aviso('Falta de implementa��o:' + sLineBreak +
    'Fun��o: ' + 'SPED_EFD_PF.DefineQuantidadePositivaOuNegativa()' + sLineBreak +
    // GetEnumName(TypeInfo(TOrigemIDKnd),Integer(OrigemIDKnd)))
    'Forma de corre��o: ' + GetEnumName(TypeInfo(TCorrApoFormaLctoPosNegSPED), Integer(FormaLcto)) + sLineBreak +
    'Registro de origem: ' + RegOri + sLineBreak +
    'Registro de corre��o: ' + REG + sLineBreak +
    'Quantidade anterior: ' + Geral.FFT(ValAntes, 3, siNegativo) + sLineBreak +
    'Quantidade nova: ' + Geral.FFT(ValNovo, 3, siNegativo) + sLineBreak +
    'Quantidade corre��o: ' + Geral.FFT(ValNovo - ValAntes, 3, siNegativo) + sLineBreak +
    'QTD_COR_POS: ' + Geral.FFT(QTD_COR_POS, 3, siNegativo) + sLineBreak +
    'QTD_COR_NEG: ' + Geral.FFT(QTD_COR_NEG, 3, siNegativo) + sLineBreak +
    sLineBreak);
  end;
var
  ValCorApo: Double;
begin
  Result      := False;
  QTD_COR_NEG := 0;
  QTD_COR_POS := 0;
  if (ValNovo = 0) and (ValAntes = 0) then
  begin
    Result := True;
    Exit;
  end;
  ValCorApo   := ValNovo - ValAntes;
  case FormaLcto of
    //caflpnIndef=0,
    caflpnMovNormEsquecido:
    begin
(*
Boa tarde,

  Esqueci de lan�ar no SPED EFD do m�s anterior uma reclassifica��o de
  produto de classe A para B. No SPED atual vou lan�a-lo no K270/K275.
  Devo lan�ar as quantidades no QTD_COR_POS ou no QTD_COR_NEG?
  Devo lan�ar no K280 tamb�m?
  // Resposta
Prezado(a) Contribuinte,

se o erro apontado seria objeto de informa��o no registro K220 no per�odo
da ocorr�ncia, ent�o a corre��o ser� efetuada nos registros K270/K275,
conforme est� descrito no Guia Pr�tico. Se o produto A est� quantificado em
n�mero maior que o correto, lan�ar quantidade negativa. Por consequ�ncia, o
"B" est� menor, lan�ar quantidade positiva.

O K280 dever� ser corrigido tamb�m, considerando que o estoque final de um
per�odo � o estoque inicial do per�odo subsequente, e, portanto,
influenciar� no estoque final escriturado dos per�odos subsequentes.
*)
      if RegOri = 'K210' then
      begin
        if REG = 'K270' then QTD_COR_NEG := ValCorApo else
        //if REG = 'K275' then QTD_COR_POS := ? else
        Aviso();
      end
      else
      if RegOri = 'K215' then
      begin
        //if REG = 'K270' then QTD_COR_NEG := ? else
        if REG = 'K275' then QTD_COR_POS := ValCorApo else
        Aviso();
      end
      else
      if RegOri = 'K220' then
      begin
        if REG = 'K270' then QTD_COR_NEG := ValCorApo else
        if REG = 'K275' then QTD_COR_POS := ValCorApo else
        Aviso();
      end
      else
      if (RegOri = 'K230')
      or (RegOri = 'K250') then
      begin
        if REG = 'K270' then QTD_COR_POS := ValCorApo else
        //if REG = 'K275' then QTD_COR_NEG := ValCorApo else
        Aviso();
      end
      else
      if (RegOri = 'K235')
      or (RegOri = 'K255') then
      begin
        if REG = 'K275' then QTD_COR_NEG := ValCorApo else
        //if REG = 'K275' then QTD_COR_NEG := ValCorApo else
        Aviso();
      end
      else
      if (RegOri = 'K260') then
      begin
        if (REG = 'K270') then
        begin
          if ValCorApo < 0 then
            QTD_COR_NEG := - ValCorApo
          else
            QTD_COR_POS := ValCorApo;
        end else
        //if REG = 'K275' then QTD_COR_NEG := ValCorApo else
        Aviso();
      end
      else
      if (RegOri = 'K291')
      or (RegOri = 'K301') then
      begin
        if (REG = 'K270') then QTD_COR_POS := ValCorApo
        else
        Aviso();
      end
      else
      if (RegOri = 'K292')
      or (RegOri = 'K302') then
      begin
        if (REG = 'K270') then QTD_COR_NEG := ValCorApo
        else
        Aviso();
      end
      else Aviso();
    end;
    //caflpnDesfazimentoMov:
    else Aviso();
  end;
  if (QTD_COR_NEG < 0) or (QTD_COR_POS < 0) then Aviso() else
  if (QTD_COR_NEG = 0) and (QTD_COR_POS = 0) then Aviso() else
  Result := True;
end;

function TUnSPED_EFD_PF.EncerraMesSPED(ImporExpor, AnoMes, Empresa: Integer;
  Campo: String): Boolean;
const
  Valor = 1;
var
  Qry: TmySQLQuery;
  Nome: String;
  MovimVS, GerLibEFD, EnvioEFD: Integer;
  SQLType: TSQLType;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM spedefdicmsipience ',
    'WHERE ImporExpor=' + Geral.FF0(AnoMes),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
    if Qry.RecordCount > 0 then
      SQLType      := stUpd
    else
      SQLType      := stIns;
    //AnoMes         := ;
    //Empresa        := ;
    //Nome           := ;
    //MovimVS        := ;
    //GerLibEFD      := ;
    //EnvioEFD       := ;

    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'spedefdicmsipience', False, [
    Campo], [
    'ImporExpor', 'AnoMes', 'Empresa'], [
    Valor], [
    ImporExpor, AnoMes, Empresa], True);
  finally
    Qry.Free;
  end;
end;

function TUnSPED_EFD_PF.ErrGrandeza(TabSrc, GGxSrc, GGXDst: String): Boolean;
  function ParteSQL(GGXStr: String): String;
  begin
    Result := Geral.ATS([
    'SELECT ggx.GraGru1 Nivel1, ggx.Controle Reduzido, ',
    'CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_GG1, med.Sigla, med.Grandeza, ',
    'xco.Grandeza xco_Grandeza,  ',
    'CAST(IF(xco.Grandeza > 0, xco.Grandeza,  ',
    '  IF((ggx.GraGruY<2048 OR  ',
    '  xco.CouNiv2<>1), 2,  ',
    '  IF(xco.CouNiv2=1, 1, -1))) AS DECIMAL(11,0))  dif_Grandeza ',
    'FROM ' + TabSrc + ' ses ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=ses.' + GGXStr,
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    nv1 ON nv1.Codigo=xco.CouNiv1  ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    nv2 ON nv2.Codigo=xco.CouNiv2  ',
    'WHERE (IF(xco.Grandeza>0, xco.Grandeza, ',
    '  IF((ggx.GraGruY<2048 OR xco.CouNiv2<>1), 2, ',
    '    IF(xco.CouNiv2=1, 1, -1))) <> med.Grandeza) ',
    '']);
  end;
var
  SQLSrc, SQLDst, SQLUni: String;
begin
  SQLSrc := '';
  SQLDst := '';
  SQLUni := '';
  if GGxSrc <> '' then
    SQLSrc := ParteSQL(GGXSrc);
  if (GGxSrc <> '') and (GGXDst <> '') then
    SQLUni := 'UNION ';
  if GGxDst <> '' then
    SQLDst := ParteSQL(GGXDst);
  Result := DfModAppGraG1.GrandesasInconsistentes([
  ' DROP TABLE IF EXISTS _couros_err_grandeza_; ',
  'CREATE TABLE _couros_err_grandeza_ ',
  ' ',
  SQLSrc,
  SQLUni,
  SQLDst,
  '; ',
  'SELECT * FROM _couros_err_grandeza_ ',
  ''], DModG.MyPID_DB);
end;

function TUnSPED_EFD_PF.InsereItemAtual_K210(const ImporExpor, AnoMes, Empresa,
  PeriApu, MovimID, Codigo, MovimCod, Controle: Integer; const DataIni, DataFim,
  DtMovim, DtCorrApo: TDateTime; const GraGruX, ClientMO, FornecMO,
  EntiSitio: Integer; const Qtde: Double; const OrigemOpeProc: TOrigemOpeProc;
  const OriIDKnd: TOrigemIDKnd; const DiaFim: TDateTime; const TipoPeriodoFiscal:
  TTipoPeriodoFiscal; const MeAviso: TMemo; var IDSeq1: Integer;
  Agrega: Boolean): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik210';
  REG      = 'K210';
  //OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_INI_OS, DT_FIN_OS, COD_DOC_OS, COD_ITEM_ORI: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, OriOpeProc, OrigemIDKnd:
  Integer;
  QTD_ORI: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0;//Integer(MovimNiv);
  //
  IDSeq1         := 0;
  //ID_SEK         := ;
  DT_INI_OS      := Geral.FDT(DataIni, 1);
  if Int(DataFim) > DiaFim then
    DT_FIN_OS    := '0000-00-00'
  else
    DT_FIN_OS    := Geral.FDT(DataFim, 1);
  COD_DOC_OS     := Geral.FF0(MovimCod);
  COD_ITEM_ORI   := Geral.FF0(GraGruX);
  QTD_ORI        := Qtde;
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  OrigemIDKnd    := Integer(OriIDKnd);
  //
  if DtCorrApo > 1 then
  begin
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OriIDKnd, TOrigemSPEDEFDKnd.osekProducao, MeAviso, IDSeq1); // Item de origem
    //
    InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo, MovimCod,
    Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
    OriIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_INI_OS', 'DT_FIN_OS',
    'COD_DOC_OS', 'COD_ITEM_ORI', 'QTD_ORI',
    'GraGruX', 'OriOpeProc', 'OrigemIDKnd'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_INI_OS, DT_FIN_OS,
    COD_DOC_OS, COD_ITEM_ORI, QTD_ORI,
    GraGruX, OriOpeProc, OrigemIDKnd], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TUnSPED_EFD_PF.InsereItemAtual_K215(const ImporExpor, AnoMes, Empresa,
  PeriApu, IDSeq1: Integer; Data, DtMovim, DtCorrApo: TDateTime; GraGruX:
  Integer; Qtde: Double; COD_INS_SUBST: String; ID_Item, MovimID, Codigo,
  MovimCod, Controle, ClientMO, FornecMO, EntiSitio: Integer; OrigemOpeProc:
  TOrigemOpeProc; OriIDKnd: TOrigemIDKnd; OriESTSTabSorc: TEstqSPEDTabSorc;
  TipoPeriodoFiscal: TTipoPeriodoFiscal; MeAviso: TMemo):
  Boolean;
const
  TabDst   = 'efdicmsipik215';
  REG      = 'K215';
  RegPai   = 'K210';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  COD_ITEM_DES: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, ESTSTabSorc,
  OriOpeProc, OrigemIDKnd: Integer;
  QTD_DES: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  Result := False;
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  COD_ITEM_DES   := Geral.FF0(GraGruX);
  QTD_DES        := Qtde;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  OrigemIDKnd    := Integer(OriIDKnd);
  //
  if DtCorrApo > 1 then
  begin
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K275(ImporExpor, AnoMes, Empresa, PeriApu, REG,
    MovimID, Codigo, MovimCod, Controle,
    GraGruX, (*LinArqPai*)IDSeq1, Qtde, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
    OriESTSTabSorc, OrigemOpeProc, OriIDKnd, TOrigemSPEDEFDKnd.osekProducao, MeAviso);
    //
    SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
    OriIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'COD_ITEM_DES', 'QTD_DES',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc',
    'OrigemIDKnd'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, COD_ITEM_DES, QTD_DES,
    GraGruX, ESTSTabSorc, OriOpeProc,
    OrigemIDKnd], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnSPED_EFD_PF.InsereItemAtual_K230(const ImporExpor, AnoMes, Empresa,
  PeriApu, MovimID, Codigo, MovimCod,
  Controle: Integer; const DataIni, DataFim, DtMovim, DtCorrApo: TDateTime;
  const GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde: Double;
  const OrigemOpeProc: TOrigemOpeProc; DiaFim: TDateTime;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso: TMemo;
  var IDSeq1: Integer; Agrega: Boolean): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik230';
  REG      = 'K230';
  //Controle = 0;
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_INI_OP, DT_FIN_OP, COD_DOC_OP, COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, OriOpeProc: Integer;
  QTD_ENC: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  Result         := False;
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  DT_INI_OP      := Geral.FDT(DataIni, 1);
  if Int(DataFim) > DiaFim then
    DT_FIN_OP    := '0000-00-00'
  else
    DT_FIN_OP    := Geral.FDT(DataFim, 1);
  COD_DOC_OP     := Geral.FF0(MovimCod);
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD_ENC        := Qtde;
  //
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
    //
    SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_INI_OP', 'DT_FIN_OP',
    'COD_DOC_OP', 'COD_ITEM', 'QTD_ENC',
    'GraGruX', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_INI_OP, DT_FIN_OP,
    COD_DOC_OP, COD_ITEM, QTD_ENC,
    GraGruX, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TUnSPED_EFD_PF.InsereItemAtual_K235(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1: Integer; Data,
  DtCorrApo: TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
  ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc;
  OriESTSTabSorc: TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
  const MeAviso: TMemo): Boolean;
const
  TabDst   = 'efdicmsipik235';
  REG      = 'K235';
  RegPai   = 'K230';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
(*
var
  DT_SAIDA, COD_ITEM: String;
  ImporExpor, AnoMes, Empresa, LinArq, K100, K2X0: Integer;
  QTD: Double;
  SQLType: TSQLType;
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
*)
var
  DT_SAIDA, COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq2, ESTSTabSorc, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  DT_SAIDA       := Geral.FDT(Data, 1);
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K275(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, MovimID, Codigo, MovimCod, Controle,
    GraGruX, (*LinArqPai*)IDSeq1, Qtde, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
    OriESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    MeAviso); // item de destino
    //
    //
    SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, Data, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(nil, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'DT_SAIDA',
    'COD_ITEM', 'QTD', 'COD_INS_SUBST',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, DT_SAIDA,
    COD_ITEM, QTD, COD_INS_SUBST,
    GraGruX, ESTSTabSorc, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnSPED_EFD_PF.InsereItemAtual_K250(const ImporExpor, AnoMes, Empresa, PeriApu, MovimID, Codigo, MovimCod,
  Controle: Integer; const DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde: Double;
  const OrigemOpeProc: TOrigemOpeProc; DiaFim: TDateTime;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso: TMemo;
  var IDSeq1: Integer; Agrega: Boolean): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik250';
  REG      = 'K250';
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_PROD, COD_ITEM, COD_DOC_OP: String;
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  Data: TDateTime;
begin
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  Result         := False;
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  Data           := DataFimObrigatoria(DtHrFimOpe, DiaFim);
  DT_PROD        := Geral.FDT(Data, 1);
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD            := Qtde;
  COD_DOC_OP     := Geral.FF0(MovimCod);
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
    //
    SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_PROD', 'COD_ITEM',
    'QTD', 'COD_DOC_OP', 'GraGruX',
    'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_PROD, COD_ITEM,
    QTD, COD_DOC_OP, GraGruX,
    OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TUnSPED_EFD_PF.InsereItemAtual_K255(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1: Integer; Data,
  DtCorrApo: TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
  ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc;
  OriESTSTabSorc: TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
  const MeAviso: TMemo): Boolean;
const
  TabDst   = 'efdicmsipik255';
  REG      = 'K255';
  RegPai   = 'K250';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_CONS, COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq2, ESTSTabSorc, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  DT_CONS        := Geral.FDT(Data, 1);
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K275(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, MovimID, Codigo, MovimCod, Controle,
    GraGruX, (*LinArqPai*)IDSeq1, Qtde, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
    OriESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso); // item de destino
    //
    //
    SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, Data, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'DT_CONS',
    'COD_ITEM', 'QTD', 'COD_INS_SUBST',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, DT_CONS,
    COD_ITEM, QTD, COD_INS_SUBST,
    GraGruX, ESTSTabSorc, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnSPED_EFD_PF.InsereItemAtual_K260(const ImporExpor, AnoMes, Empresa,
  PeriApu, MovimID, Codigo, MovimCod, Controle: Integer; const DtHrAberto,
  DtHrFimOpe, DtMovim, DtCorrApoProd, DtCorrApoCons: TDateTime; const GraGruX,
  ClientMO, FornecMO, EntiSitio: Integer; const QtdeProd, QtdeCons: Double;
  const OrigemOpeProc: TOrigemOpeProc;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso: TMemo;
  var IDSeq1: Integer; const Agrega: Boolean): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik260';
  REG      = 'K260';
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  COD_OP_OS, COD_ITEM, DT_SAIDA, DT_RET: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, OriOpeProc: Integer;
  QTD_SAIDA, QTD_RET: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result         := False;
  if DtCorrApoProd = -1 then
    Geral.MB_Aviso('DtCorrApoProd = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  if DtCorrApoCons = -1 then
    Geral.MB_Aviso('DtCorrApoCons = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  COD_OP_OS      := Geral.FF0(MovimCod);
  COD_ITEM       := Geral.FF0(GraGruX);
  DT_SAIDA       := Geral.FDT(DtHrAberto, 1);
  QTD_SAIDA      := QtdeCons;
  DT_RET         := Geral.FDT(DtHrFimOpe, 1);
  QTD_RET        := QtdeProd;
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApoProd > 1 then
  begin
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    //QtdPos e Neg no mesmo registro n�o pode!
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, DtMovim, DtCorrApoProd, MovimID, Codigo,
    MovimCod, Controle,  GraGruX, QtdeProd, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
    //
    if Result then
    begin
      //
    //
      SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
      TipoPeriodoFiscal, ImporExpor, AnoMes,
      Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApoProd, MovimID, Codigo, MovimCod,
      Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
      QtdeProd, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
      OrigemIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
      //
      Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, DtMovim, DtCorrApoCons, MovimID, Codigo,
      MovimCod, Controle,  GraGruX, -QtdeCons, SPED_EFD_KndRegOrigem,
      OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
      //
    //
      SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
      TipoPeriodoFiscal, ImporExpor, AnoMes,
      Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApoCons, MovimID, Codigo, MovimCod,
      Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
      -QtdeCons, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
      OrigemIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
      //
    end;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'COD_OP_OS', 'COD_ITEM',
    'DT_SAIDA', 'QTD_SAIDA', 'DT_RET',
    'QTD_RET', 'GraGruX', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, COD_OP_OS, COD_ITEM,
    DT_SAIDA, QTD_SAIDA, DT_RET,
    QTD_RET, GraGruX, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TUnSPED_EFD_PF.InsereItemAtual_K265(ImporExpor, AnoMes, Empresa,
  PeriApu, IDSeq1: Integer; Data, DtCorrApo: TDateTime; GraGruX: Integer;
  QtdeCons, QtdeRet: Double; COD_INS_SUBST: String; ID_Item, MovimID, Codigo,
  MovimCod, Controle: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
  TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso:
  TMemo): Boolean;
const
  TabDst   = 'efdicmsipik265';
  REG      = 'K265';
  RegPai   = 'K260';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, ESTSTabSorc, OriOpeProc: Integer;
  QTD_CONS, QTD_RET: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  //
  SQLType        := stNil;
  //ImporExpor     := ;
  //AnoMes         := ;
  //Empresa        := ;
  //PeriApu        := ;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD_CONS       := QtdeCons;
  QTD_RET        := QtdeRet;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    if QTD_CONS <> 0 then
      Result := InsereItemAtual_K275(ImporExpor, AnoMes, Empresa,
      PeriApu, REG, MovimID, Codigo, MovimCod, Controle,
      GraGruX, (*LinArqPai*)IDSeq1, QTD_CONS, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
      OriESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso); // item de destino
    //
    if QTD_RET <> 0 then
      Result := InsereItemAtual_K275(ImporExpor, AnoMes, Empresa,
      PeriApu, REG, MovimID, Codigo, MovimCod, Controle,
      GraGruX, (*LinArqPai*)IDSeq1, QTD_RET, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
      OriESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso); // item de destino
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'COD_ITEM',
    'QTD_CONS', 'QTD_RET', 'GraGruX',
    'ESTSTabSorc', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, COD_ITEM,
    QTD_CONS, QTD_RET, GraGruX,
    ESTSTabSorc, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnSPED_EFD_PF.InsereItemAtual_K270(const ImporExpor, AnoMes, Empresa,
  PeriApu: Integer; const RegOri: String; const Data,
  DtCorrApo: TDateTime; const MovimID, Codigo, MovimCod, Controle,
  GraGruX: Integer; const Qtde: Double;
  const SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  OrigemOpeProc: TOrigemOpeProc; OrigemIDKnd: TOrigemIDKnd;
  const OriSPEDEFDKnd: TOrigemSPEDEFDKnd; MeAviso: TMemo; var IDSeq1: Integer): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik270';
  REG      = 'K270';
  ValAntes = 0;
  TabSorc  = TEstqSPEDTabSorc.estsVMI;
var
  DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM, ORIGEM, RegisPai: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  OriOpeProc, KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv: Integer;
  QTD_COR_POS, QTD_COR_NEG, ValNovo: Double;
  SQLType: TSQLType;
  //
  ThatDtIni, ThatDtFim: TDateTime;
  SQL_POS_NEG: String;
begin
  IDSeq1         := 0;
  //SQLType        := ImgTipo.SQLType?;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  SPED_EFD_PF.DefineDatasPeriCorrApoSPED(Empresa, 'K100', Data, DtCorrApo, ThatDtIni, ThatDtFim);
  //MovimID        := ;
  //Codigo         := ;
  //MovimCod       := ;
  DT_INI_AP      := Geral.FDT(ThatDtIni, 1);
  DT_FIN_AP      := Geral.FDT(ThatDtFim, 1);
  COD_OP_OS      := Geral.FF0(MovimCod);
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD_COR_POS    := 0;
  QTD_COR_NEG    := 0;
  RegisPai       := RegOri;
  ValNovo := Qtde;
  SPED_EFD_PF.DefineQuantidadePositivaOuNegativa(RegOri, REG, ValAntes, ValNovo,
    caflpnMovNormEsquecido, QTD_COR_POS, QTD_COR_NEG);
  ORIGEM         := Geral.FF0(Integer(SPED_EFD_KndRegOrigem));
  //GraGruX        := ; GGXOri
  OriOpeProc     := Integer(OrigemOpeProc);
  if QTD_COR_POS > 0 then
    SQL_POS_NEG := 'AND QTD_COR_POS > 0 '
  else
    SQL_POS_NEG := 'AND QTD_COR_NEG > 0 ';
  //
  VerificaGGX_SPED(GraGruX, KndCod, KndNSU, KndItm, REG);
  UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
  'SELECT * ',
  'FROM efdicmsipik270 ',
  'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
  'AND AnoMes=' + Geral.FF0(AnoMes),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND PeriApu=' + Geral.FF0(PeriApu),
  'AND KndTab=' + Geral.FF0(KndTab),
  'AND KndCod=' + Geral.FF0(KndCod),
  'AND KndNSU=' + Geral.FF0(KndNSU),
  'AND KndItm=' + Geral.FF0(KndItm),
  'AND GraGruX=' + Geral.FF0(GraGruX),
  'AND ORIGEM="' + ORIGEM + '"',
  '']);
  //Geral.MB_SQL(Self, DmModVS.QrX999);
  IDSeq1 := 0;
  if DmModVS.QrX999.RecordCount > 0 then
  begin
    AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
    Exit;
  end else
  begin
    IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
    'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
    ImporExpor, AnoMes, Empresa, PeriApu],
    stIns, IDSeq1, siPositivo, nil);
    //
    SQLType := stIns;
  end;
    //
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
  'IDSeq1', 'KndAID', 'KndNiv',
  'DT_INI_AP', 'DT_FIN_AP', 'COD_OP_OS',
  'COD_ITEM', 'QTD_COR_POS', 'QTD_COR_NEG',
  'ORIGEM', 'GraGruX', 'OriOpeProc',
  'OrigemIDKnd', 'OriSPEDEFDKnd', 'RegisPai'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'], [
  IDSeq1, KndAID, KndNiv,
  DT_INI_AP, DT_FIN_AP, COD_OP_OS,
  COD_ITEM, QTD_COR_POS, QTD_COR_NEG,
  ORIGEM, GraGruX, OriOpeProc,
  OrigemIDKnd, OriSPEDEFDKnd, RegisPai], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True);
end;

function TUnSPED_EFD_PF.InsereItemAtual_K275(const ImporExpor, AnoMes, Empresa,
  PeriApu: Integer; RegOri: String; MovimID, Codigo, MovimCod, Controle,
  GraGruX, IDSeq1: Integer; Qtde: Double; COD_INS_SUBST: String;
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem; ESTSTabSorc: TEstqSPEDTabSorc;
  OrigemOpeProc: TOrigemOpeProc; OriIDKnd: TOrigemIDKnd;
  const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd; MeAviso: TMemo): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik275';
  REG      = 'K275';
  ValAntes = 0;
  TabSorc  = TEstqSPEDTabSorc.estsVMI;
var
  COD_ITEM, ORIGEM, RegisPai: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  IDSeq2, KndTab, KndCod, KndNSU, KndItm,
  KndAID, KndNiv, OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd: Integer;
  QTD_COR_POS, QTD_COR_NEG: Double;
  SQLType: TSQLType;
  //
  ValNovo: Double;
begin
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //IDSeq1         := ;
  IDSeq2         := 0;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //ID_SEK         := ;
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD_COR_POS    := 0;
  QTD_COR_NEG    := 0;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  //ESTSTabSorc    := ;
  ORIGEM         := Geral.FF0(Integer(SPED_EFD_KndRegOrigem));
  OriOpeProc     := Integer(OrigemOpeProc);
  OrigemIDKnd    := Integer(OriIDKnd);
  OriSPEDEFDKnd  := Integer(OrigemSPEDEFDKnd);
  RegisPai       := RegOri;
  ValNovo := Qtde;
  SPED_EFD_PF.DefineQuantidadePositivaOuNegativa(RegOri, REG, ValAntes, ValNovo,
    caflpnMovNormEsquecido, QTD_COR_POS, QTD_COR_NEG);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
  'SELECT * ',
  'FROM efdicmsipik275 ',
  'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
  'AND AnoMes=' + Geral.FF0(AnoMes),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND PeriApu=' + Geral.FF0(PeriApu),
  'AND KndTab=' + Geral.FF0(KndTab),
  'AND KndCod=' + Geral.FF0(KndCod),
  'AND KndNSU=' + Geral.FF0(KndNSU),
  'AND KndItm=' + Geral.FF0(KndItm),
  'AND GraGruX=' + Geral.FF0(GraGruX),
  '']);
  IDSeq2 := 0;
  if DmModVS.QrX999.RecordCount > 0 then
  begin
    AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
    Exit;
  end else
  begin
    IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
    'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
    ImporExpor, AnoMes, Empresa, PeriApu],
    stIns, IDSeq2, siPositivo, nil);
    //
    SQLType := stIns;
  end;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
  'IDSeq1', 'IDSeq2', 'KndAID',
  'KndNiv', 'ID_SEK', 'COD_ITEM',
  'QTD_COR_POS', 'QTD_COR_NEG', 'COD_INS_SUBST',
  'GraGruX', 'ESTSTabSorc', 'ORIGEM',
  'OriOpeProc', 'OrigemIDKnd', 'OriSPEDEFDKnd',
  'RegisPai'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'], [
  IDSeq1, IDSeq2, KndAID,
  KndNiv, ID_SEK, COD_ITEM,
  QTD_COR_POS, QTD_COR_NEG, COD_INS_SUBST,
  GraGruX, ESTSTabSorc, ORIGEM,
  OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd,
  RegisPai], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True);
end;

function TUnSPED_EFD_PF.InsereItemAtual_K290(const ImporExpor, AnoMes, Empresa,
  PeriApu, MovimID, Codigo, MovimCod, CodDocOP(*, Controle*): Integer; const
  DataIni, DataFim, (*DtMovim,*) DtCorrApo: TDateTime; const ClientMO, FornecMO,
  EntiSitio: Integer; const OrigemOpeProc: TOrigemOpeProc; const DiaFim:
  TDateTime; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
  const TabSorc: TEstqSPEDTabSorc; const MeAviso: TMemo;
  var IDSeq1: Integer): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik290';
  REG      = 'K290';
var
  DT_INI_OP, DT_FIN_OP, COD_DOC_OP: String;
  //ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1, ID_SEK,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, OriOpeProc: Integer;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  Result         := False;
  //
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := 0(*Controle*);
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  DT_INI_OP      := Geral.FDT(DataIni, 1);
  if Int(DataFim) > DiaFim then
    DT_FIN_OP    := '0000-00-00'
  else
    DT_FIN_OP    := Geral.FDT(DataFim, 1);
  COD_DOC_OP     := Geral.FF0(CodDocOP);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    // N�o existe registro K270 e K280 para para K290!
    (*
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
    //
    SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, Agrega=True, MeAviso);
    //
    *)
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND COD_DOC_OP="' + Geral.FF0(CodDocOP) + '"',
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      MeAviso.Lines.Add('K290 j� importado > KndNSU: ' + Geral.FF0(KndItm));
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_INI_OP', 'DT_FIN_OP',
    'COD_DOC_OP', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_INI_OP, DT_FIN_OP,
    COD_DOC_OP, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TUnSPED_EFD_PF.InsereItemAtual_K291(ImporExpor, AnoMes, Empresa,
  PeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
  PaiMovCod, PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
  GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod, Controle, ClientMO,
  FornecMO, EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
  TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
  TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
const
  TabDst   = 'efdicmsipik291';
  REG      = 'K291';
  //RegPai   = 'K290';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
var
  COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  ESTSTabSorc,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  if Qtde = 0 then
  begin
    AvisoQtdeZero(MeAviso, KndItm, GraGruX, MovimCod, Controle, REG);
    Exit;
  end;
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  //
  if DtCorrApo > 1 then
  begin
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
    //
    SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'COD_ITEM',
    'QTD', 'GraGruX', 'OriOpeProc',
    'JmpMovID', 'JmpMovCod', 'JmpNivel1',
    'JmpNivel2', 'PaiMovID', 'PaiMovCod',
    'PaiNivel1', 'PaiNivel2'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, COD_ITEM,
    QTD, GraGruX, OriOpeProc,
    JmpMovID, JmpMovCod, JmpNivel1,
    JmpNivel2, PaiMovID, PaiMovCod,
    PaiNivel1, PaiNivel2], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnSPED_EFD_PF.InsereItemAtual_K292(ImporExpor, AnoMes, Empresa,
  PeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1,
JmpNivel2, PaiMovID, PaiMovCod,
PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime; GraGruX: Integer;
  Qtde: Double; MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc;
  OriESTSTabSorc: TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
  const TabSorc: TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
const
  TabDst   = 'efdicmsipik292';
  REG      = 'K292';
  //RegPai   = 'K290';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
var
  COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  ESTSTabSorc,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  if Qtde = 0 then
  begin
    AvisoQtdeZero(MeAviso, KndItm, GraGruX, MovimCod, Controle, REG);
    Exit;
  end;
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  //
  if DtCorrApo > 1 then
  begin
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
    //
    SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'COD_ITEM',
    'QTD', 'GraGruX', 'OriOpeProc',
    'JmpMovID', 'JmpMovCod', 'JmpNivel1',
    'JmpNivel2', 'PaiMovID', 'PaiMovCod',
    'PaiNivel1', 'PaiNivel2'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, COD_ITEM,
    QTD, GraGruX, OriOpeProc,
    JmpMovID, JmpMovCod, JmpNivel1,
    JmpNivel2, PaiMovID, PaiMovCod,
    PaiNivel1, PaiNivel2], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnSPED_EFD_PF.InsereItemAtual_K300(const ImporExpor, AnoMes, Empresa,
  PeriApu, MovimID, Codigo, MovimCod, CodDocOP: Integer; const DataIni, DataFim,
  DtMovim,  DtCorrApo: TDateTime; const ClientMO, FornecMO, EntiSitio: Integer;
  const OrigemOpeProc: TOrigemOpeProc; const DiaFim: TDateTime;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc: TEstqSPEDTabSorc;
  const MeAviso: TMemo; var IDSeq1: Integer): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik300';
  REG      = 'K300';
var
  DT_PROD: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, OriOpeProc: Integer;
  SQLType: TSQLType;
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  Result         := False;
  //
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := 0(*Controle*);
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  //K250 >> DT_PROD        := DataFimObrigatoria(DtHrFimOpe, DiaFim);
  DT_PROD        := Geral.FDT(DtMovim, 1);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    // N�o existe registro K270 e K280 para para K300!
    (*
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
    //
    SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, Agrega=True, MeAviso);
    //
    *)
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND DT_PROD="' + DT_PROD + '"',
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      MeAviso.Lines.Add('K300 j� importado > KndNSU: ' + Geral.FF0(KndNSU));
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_PROD', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_PROD, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TUnSPED_EFD_PF.InsereItemAtual_K301(ImporExpor, AnoMes, Empresa,
  PeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
  PaiMovCod, PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
  GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod, Controle, ClientMO,
  FornecMO, EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
  TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
  TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
const
  TabDst   = 'efdicmsipik301';
  REG      = 'K301';
  //RegPai   = 'K300';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
var
  COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  ESTSTabSorc,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  if Qtde = 0 then
  begin
    AvisoQtdeZero(MeAviso, KndItm, GraGruX, MovimCod, Controle, REG);
    Exit;
  end;
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  //
  if DtCorrApo > 1 then
  begin
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
    //
    SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'COD_ITEM',
    'QTD', 'GraGruX', 'OriOpeProc',
    'JmpMovID', 'JmpMovCod', 'JmpNivel1',
    'JmpNivel2', 'PaiMovID', 'PaiMovCod',
    'PaiNivel1', 'PaiNivel2'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, COD_ITEM,
    QTD, GraGruX, OriOpeProc,
    JmpMovID, JmpMovCod, JmpNivel1,
    JmpNivel2, PaiMovID, PaiMovCod,
    PaiNivel1, PaiNivel2], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnSPED_EFD_PF.InsereItemAtual_K302(ImporExpor, AnoMes, Empresa,
  PeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1,
JmpNivel2, PaiMovID, PaiMovCod,
PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime; GraGruX: Integer;
  Qtde: Double; MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc;
  OriESTSTabSorc: TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
  const TabSorc: TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
const
  TabDst   = 'efdicmsipik302';
  REG      = 'K302';
  //RegPai   = 'K300';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
var
  COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  ESTSTabSorc,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  if Qtde = 0 then
  begin
    AvisoQtdeZero(MeAviso, KndItm, GraGruX, MovimCod, Controle, REG);
    Exit;
  end;
  if DtCorrApo = -1 then
    Geral.MB_Aviso('DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  //
  if DtCorrApo > 1 then
  begin
    if not SPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
    //
    SPED_EFD_PF.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'COD_ITEM',
    'QTD', 'GraGruX', 'OriOpeProc',
    'JmpMovID', 'JmpMovCod', 'JmpNivel1',
    'JmpNivel2', 'PaiMovID', 'PaiMovCod',
    'PaiNivel1', 'PaiNivel2'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, COD_ITEM,
    QTD, GraGruX, OriOpeProc,
    JmpMovID, JmpMovCod, JmpNivel1,
    JmpNivel2, PaiMovID, PaiMovCod,
    PaiNivel1, PaiNivel2], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnSPED_EFD_PF.InsereItensAtuais_K280_New(BalTab: TEstqSPEDTabSorc;
  TipoPeriodoFiscal: TTipoPeriodoFiscal; ImporExpor, AnoMes, Empresa,
  K100_PeriApu: Integer; const RegPai, RegAvo: String; const DataSPED,
  DtCorrApo: TDateTime; const MovimID, Codigo, MovimCod, Controle, GraGruX,
  ClientMO, FornecMO, EntiSitio: Integer; LocalKnd: TSPEDLocalKnd;
  const Qtde: Double; const OrigemBalID: TSPED_EFD_Bal;
  const SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  const OriESTSTabSorc: TEstqSPEDTabSorc;
  const OriOrigemOpeProc: TOrigemOpeProc; const OriOrigemIDKnd: TOrigemIDKnd;
  const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd; const Agrega: Boolean;
  const MeAviso: TMemo): Boolean;
  //
  function Escolhe(A, B: Integer): Integer;
  begin
    if A <> 0 then
      Result := A
    else
      Result := B;
  end;
const
  sProcName = '"UnSPED_EFD_PF.InsereItensAtuais_K280_New()"';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik280';
  REG      = 'K280';
  ValAntes = 0;
{
var
  COD_ITEM, IND_EST, COD_PART, DT_EST: String;
  BalID, BalCod, BalNum, BalItm, BalEnt, Grandeza, Entidade, Tipo_Item, ESTSTabSorc,
  OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd: Integer;
  QTD_COR_POS, QTD_COR_NEG, Pecas, AreaM2, PesoKg: Double;
  SQLType: TSQLType;
  OriGrandeza: TGrandezaUnidMed;
  DtAtu: TDateTime;
  AMIni, AMFim, AMAtu, Local: Integer;
  SQL_POS_NEG: String;
  DebCred: TDebCred;
begin
  // erro se periodo for decendial
  if TipoPeriodoFiscal <> TTipoPeriodoFiscal.spedperMensal then
  begin
    Geral.MB_Aviso('Per�odo de apura��o n�o implementado para o registro K280!'
    + sLineBreak + 'Avise a Dermatek!');
    //
    Exit;
  end;
  //
  SQLType        := stIns;
  Result         := False;
  COD_ITEM       := Geral.FF0(GraGruX);
  //
  if Qtde >= 0 then
    DebCred := TDebCred.debcredCred
  else
    DebCred := TDebCred.debcredDeb;
  case DebCred of
    TDebCred.debcredCred:
    begin
      QTD_COR_POS  := Qtde;
      SQL_POS_NEG := 'AND QTD_COR_POS > 0 ';
    end;
    TDebCred.debcredDeb:
    begin
      QTD_COR_NEG  := -Qtde;
      SQL_POS_NEG := 'AND QTD_COR_POS < 0 ';
    end;
    else
    begin
      QTD_COR_POS    := 0;
      QTD_COR_NEG    := 0;
      SQL_POS_NEG := 'AND QTD_COR_??? >?< 0 ';
    end;
  end;
  //
  //IND_EST        := ;
  //COD_PART       := ;
  Local := 0;
  case LocalKnd of
    (*0*)slkND: ; // Zero!
    (*1*)slkQualquer:    Local := Escolhe(EntiSitio, FornecMO);
    (*2*)slkForcaFornec: Local := FornecMO;
    (*3*)slkForcaSitio:  Local := EntiSitio;
    (*4*)slkPrefFornec:  Local := Escolhe(FornecMO, EntiSitio);
    (*5*)slkPrefSitio:   Local := Escolhe(EntiSitio, FornecMO);
    (*?*)else Geral.MB_Erro(
    '"LocalKnd" n�o implementado em "UnSPED_EFD_PF.InsereItensAtuais_K280()"');
  end;
  if Local = 0 then Geral.MB_Erro(
    '"Local" indefinido em "UnSPED_EFD_PF.InsereItensAtuais_K280()"' +
    sLineBreak + 'MovimID: ' + Geral.FF0(MovimID) +
    sLineBreak + 'Codigo: ' + Geral.FF0(Codigo) +
    sLineBreak + 'IME-C: ' + Geral.FF0(MovimCod) +
    sLineBreak + 'IME-I: ' + Geral.FF0(Controle) +
    sLineBreak + 'Reduzido: ' + Geral.FF0(GraGruX) +
    sLineBreak + 'ClientMO: ' + Geral.FF0(ClientMO) +
    sLineBreak + 'FornecMO: ' + Geral.FF0(FornecMO) +
    sLineBreak + 'EntiSitio: ' + Geral.FF0(EntiSitio)+
    sLineBreak);
  SPED_EFD_PF.ObtemIND_ESTeCOD_PART(Empresa, ClientMO, Local, GraGruX,
    IND_EST, COD_PART, Entidade);
  BalID          := MovimID;
  BalCod         := Codigo;
  BalNum         := MovimCod;
  BalItm         := Controle;
  BalEnt         := Geral.IMV(COD_PART);
  Grade_PF.ObtemGrandezaDeGraGruX(GraGruX, OriGrandeza, Tipo_Item);
  Grandeza       := Integer(OriGrandeza);
  Pecas          := 0;
  AreaM2         := 0;
  PesoKg         := 0;
  case OriGrandeza of
    (*0*)gumPeca   : Pecas  := Qtde;
    (*1*)gumAreaM2 : AreaM2 := Qtde;
    (*2*)gumPesoKG : PesoKg := Qtde;
    else Geral.MB_Aviso(
    'Grandeza inesperada em "FmSPED_EFD_K2XX.InsereItensAtuais_K280()"');
  end;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OriOrigemOpeProc);
  OrigemIDKnd    := Integer(OriOrigemIDKnd);
  OriSPEDEFDKnd  := Integer(OrigemSPEDEFDKnd);
  //
  AMIni := DmkPF.DataToAnoMes(DtCorrApo);
  AMFim := DmkPF.DataToAnoMes(DataSPED);
  AMAtu := AMINi;
  while AMAtu < AMFim do
  begin
    DtAtu          := IncMonth(Geral.AnoMesToData(AMAtu, 1), 1) - 1;
    DT_EST         := Geral.FDT(DtAtu, 1);
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND DT_EST="' + DT_EST + '"',
    'AND BalID=' + Geral.FF0(BalID),
    'AND BalNum=' + Geral.FF0(BalNum),
    'AND BalItm=' + Geral.FF0(BalItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    'AND DebCred=' + Geral.FF0(Integer(DebCred)),
    '']);
    if DmModVS.QrX999.RecordCount > 0 then
      Exit;
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    begin
      LinArq := 0;
      LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'LinArq', [
      (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
      stIns, LinArq, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'REG', 'RegisPai', 'RegisAvo',
    'K100', 'DT_EST', 'COD_ITEM',
    'QTD_COR_POS', 'QTD_COR_NEG', 'IND_EST',
    'COD_PART', 'BalTab', 'BalID',
    'BalCod', 'BalNum',
    'BalItm', 'BalEnt', 'GraGruX',
    'Grandeza', 'Pecas', 'AreaM2',
    'PesoKg', 'Entidade', 'Tipo_Item',
    'ESTSTabSorc', 'OriOpeProc', 'OrigemIDKnd',
    'OriSPEDEFDKnd', 'DebCred'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
    REG, RegPai, RegAvo,
    K100, DT_EST, COD_ITEM,
    QTD_COR_POS, QTD_COR_NEG, IND_EST,
    COD_PART, Integer(BalTab), BalID,
    BalCod, BalNum,
    BalItm, BalEnt, GraGruX,
    Grandeza, Pecas, AreaM2,
    PesoKg, Entidade, Tipo_Item,
    ESTSTabSorc, OriOpeProc, OrigemIDKnd,
    OriSPEDEFDKnd, Integer(DebCred)], [
    ImporExpor, AnoMes, Empresa, LinArq], True);
    //
    AMAtu := dmkPF.IncrementaAnoMes(AMAtu, 1);
  end;
}
var
  DT_EST, COD_ITEM, IND_EST, COD_PART, RegisPai, RegisAvo: String;
  //ImporExpor, AnoMes, Empresa, GraGruX,
  PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, DebCred, Grandeza,
  Entidade, Tipo_Item, ESTSTabSorc, OriOpeProc, OrigemIDKnd,
  OriSPEDEFDKnd, OriBalID, OriKndReg: Integer;
  QTD_COR_POS, QTD_COR_NEG, Pecas, AreaM2, PesoKg: Double;
  SQLType: TSQLType;
  //
  DebiCredi: TDebCred;
  OriGrandeza: TGrandezaUnidMed;
  AMIni, AMFim, AMAtu, Local, IDSeq1: Integer;
  DtAtu: TDateTime;
  //
  procedure Msg(Texto: String);
  begin
    Geral.MB_Erro(Texto +
    sLineBreak + sProcName +
    sLineBreak + 'MovimID: ' + Geral.FF0(MovimID) +
    sLineBreak + 'Codigo: ' + Geral.FF0(Codigo) +
    sLineBreak + 'IME-C: ' + Geral.FF0(MovimCod) +
    sLineBreak + 'IME-I: ' + Geral.FF0(Controle) +
    sLineBreak + 'Reduzido: ' + Geral.FF0(GraGruX) +
    sLineBreak + 'ClientMO: ' + Geral.FF0(ClientMO) +
    sLineBreak + 'FornecMO: ' + Geral.FF0(FornecMO) +
    sLineBreak + 'EntiSitio: ' + Geral.FF0(EntiSitio)+
    sLineBreak);
  end;
begin
  Result         := False;
  // erro se periodo for decendial
  if TipoPeriodoFiscal <> TTipoPeriodoFiscal.spedperMensal then
  begin
    Geral.MB_Aviso('Per�odo de apura��o n�o implementado para o registro K280!'
    + sLineBreak + 'Avise a Dermatek!');
    //
    Exit;
  end;
  SQLType        := stNil;
  //ImporExpor     := ;    Acima
  //AnoMes         := ;    Acima
  //Empresa        := ;    Acima
  PeriApu        := K100_PeriApu;
  KndTab         := Integer(BalTab);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := MovimID;
  KndNiv         := 0;  // ??????  > n�o tem!
  DT_EST         := ''; // Abaixo
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD_COR_POS    := 0;  // Abaixo
  QTD_COR_NEG    := 0;  // Abaixo
  IND_EST        := ''; // Abaixo SPED_EFD_PF.ObtemIND_ESTeCOD_PART
  COD_PART       := ''; // Abaixo SPED_EFD_PF.ObtemIND_ESTeCOD_PART
  DebCred        := 0;  // Abaixo
  //GraGruX        := ;    Acima
  Grandeza       := 0;  // Abaixo ObtemGrandezaDeGraGruX(
  Pecas          := 0;  // Abaixo
  AreaM2         := 0;  // Abaixo
  PesoKg         := 0;  // Abaixo
  Entidade       := 0;  // Abaixo SPED_EFD_PF.ObtemIND_ESTeCOD_PART
  Tipo_Item      := 0;  // Abaixo ObtemGrandezaDeGraGruX(
  RegisPai       := RegPai;
  RegisAvo       := RegAvo;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OriOrigemOpeProc);
  OrigemIDKnd    := Integer(OriOrigemIDKnd);
  OriSPEDEFDKnd  := Integer(OrigemSPEDEFDKnd);
  OriBalID       := Integer(OrigemBalID);
  OriKndReg      := Integer(SPED_EFD_KndRegOrigem);
  //
  if Qtde >= 0 then
    DebiCredi    := TDebCred.debcredCred
  else
    DebiCredi    := TDebCred.debcredDeb;
  DebCred        := Integer(DebiCredi);
  QTD_COR_POS    := 0;
  QTD_COR_NEG    := 0;
  case DebiCredi of
    TDebCred.debcredCred: QTD_COR_POS  := Qtde;
    TDebCred.debcredDeb:  QTD_COR_NEG  := -Qtde;
  end;
  //
  Local := 0;
  case LocalKnd of
    (*0*)slkND: ; // Zero!
    (*1*)slkQualquer:    Local := Escolhe(EntiSitio, FornecMO);
    (*2*)slkForcaFornec: Local := FornecMO;
    (*3*)slkForcaSitio:  Local := EntiSitio;
    (*4*)slkPrefFornec:  Local := Escolhe(FornecMO, EntiSitio);
    (*5*)slkPrefSitio:   Local := Escolhe(EntiSitio, FornecMO);
    (*?*)else Msg(
    '"LocalKnd" n�o implementado!' +
    GetEnumName(TypeInfo(TSPEDLocalKnd), Integer(LocalKnd)));
  end;
  if Local = 0 then Msg(
    '"Local" indefinido!');
  SPED_EFD_PF.ObtemIND_ESTeCOD_PART(Empresa, ClientMO, Local, GraGruX,
    IND_EST, COD_PART, Entidade);
  Grade_PF.ObtemGrandezaDeGraGruX(GraGruX, OriGrandeza, Tipo_Item);
  Grandeza       := Integer(OriGrandeza);
  case OriGrandeza of
    (*0*)gumPeca   : Pecas  := Qtde;
    (*1*)gumAreaM2 : AreaM2 := Qtde;
    (*2*)gumPesoKG : PesoKg := Qtde;
    else Msg(
    'Grandeza inesperada!' +
    sLineBreak + 'Grandeza: ' + Geral.FF0(Integer(OriGrandeza)) + ' >> ' +
    GetEnumName(TypeInfo(TGrandezaUnidMed), Integer(OriGrandeza)));
  end;
  //
  AMIni := DmkPF.DataToAnoMes(DtCorrApo);
  AMFim := DmkPF.DataToAnoMes(DataSPED);
  AMAtu := AMINi;
  while AMAtu < AMFim do
  begin
    DtAtu          := IncMonth(Geral.AnoMesToData(AMAtu, 1), 1) - 1;
    DT_EST         := Geral.FDT(DtAtu, 1);
(*
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND DT_EST="' + DT_EST + '"',
    'AND BalID=' + Geral.FF0(BalID),
    'AND BalNum=' + Geral.FF0(BalNum),
    'AND BalItm=' + Geral.FF0(BalItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    'AND DebCred=' + Geral.FF0(Integer(DebCred)),
    '']);
*)
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND DT_EST="' + DT_EST + '"',
    'AND DebCred=' + Geral.FF0(Integer(DebCred)),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      if MeAviso <> nil then
      AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
{
      LinArq := 0;
      LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'LinArq', [
      (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
      stIns, LinArq, siPositivo, nil);
      //
}
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'COD_ITEM',
    'QTD_COR_POS', 'QTD_COR_NEG', 'IND_EST',
    'COD_PART', 'Grandeza',
    'Pecas', 'AreaM2', 'PesoKg',
    'Entidade', 'Tipo_Item', 'RegisPai',
    'RegisAvo', 'ESTSTabSorc', 'OriOpeProc',
    'OrigemIDKnd', 'OriSPEDEFDKnd', 'OriBalID',
    'OriKndReg', 'IDSeq1', 'ID_SEK'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm', 'DT_EST',
    'DebCred', 'GraGruX'], [
    KndAID, KndNiv, COD_ITEM,
    QTD_COR_POS, QTD_COR_NEG, IND_EST,
    COD_PART, Grandeza,
    Pecas, AreaM2, PesoKg,
    Entidade, Tipo_Item, RegisPai,
    RegisAvo, ESTSTabSorc, OriOpeProc,
    OrigemIDKnd, OriSPEDEFDKnd, OriBalID,
    OriKndReg, IDSeq1, ID_SEK], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm, DT_EST,
    DebCred, GraGruX], True);
    //
    AMAtu := dmkPF.IncrementaAnoMes(AMAtu, 1);
  end;
end;

function TUnSPED_EFD_PF.InsereItensAtuais_K280_Old(BalTab: TEstqSPEDTabSorc;
  TipoPeriodoFiscal: TTipoPeriodoFiscal; ImporExpor, AnoMes, Empresa, K100:
  Integer; const RegPai, RegAvo: String; const DataSPED, DtCorrApo: TDateTime;
  const MovimID, Codigo, MovimCod, Controle, GraGruX, ClientMO, FornecMO,
  EntiSitio: Integer; LocalKnd: TSPEDLocalKnd; const Qtde: Double; const
  OrigemBalID: TSPED_EFD_Bal; const SPED_EFD_KndRegOrigem:
  TSPED_EFD_KndRegOrigem; const OriESTSTabSorc: TEstqSPEDTabSorc; const
  OriOrigemOpeProc: TOrigemOpeProc; const OriOrigemIDKnd: TOrigemIDKnd; const
  OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd; const Agrega: Boolean; var LinArq:
  Integer): Boolean;
  //
  function Escolhe(A, B: Integer): Integer;
  begin
    if A <> 0 then
      Result := A
    else
      Result := B;
  end;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efd_k280';
  REG      = 'K280';
  ValAntes = 0;
var
  COD_ITEM, IND_EST, COD_PART, DT_EST: String;
  (*ImporExpor, AnoMes, Empresa,*)
  BalID, BalCod, BalNum, BalItm, BalEnt, Grandeza, Entidade, Tipo_Item, ESTSTabSorc,
  OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd: Integer;
  QTD_COR_POS, QTD_COR_NEG, Pecas, AreaM2, PesoKg: Double;
  SQLType: TSQLType;
  OriGrandeza: TGrandezaUnidMed;
  DtAtu: TDateTime;
  AMIni, AMFim, AMAtu, Local: Integer;
  SQL_POS_NEG: String;
  DebCred: TDebCred;
begin
  // erro se periodo for decendial
  if TipoPeriodoFiscal <> TTipoPeriodoFiscal.spedperMensal then
  begin
    Geral.MB_Aviso('Per�odo de apura��o n�o implementado para o registro K280!'
    + sLineBreak + 'Avise a Dermatek!');
    //
    Exit;
  end;
  //
  SQLType        := stIns;
  Result         := False;
  //ImporExpor     := ImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  COD_ITEM       := Geral.FF0(GraGruX);
  //
  if Qtde >= 0 then
    DebCred := TDebCred.debcredCred
  else
    DebCred := TDebCred.debcredDeb;
  case DebCred of
    TDebCred.debcredCred:
    begin
      QTD_COR_POS  := Qtde;
      SQL_POS_NEG := 'AND QTD_COR_POS > 0 ';
    end;
    TDebCred.debcredDeb:
    begin
      QTD_COR_NEG  := -Qtde;
      SQL_POS_NEG := 'AND QTD_COR_POS < 0 ';
    end;
    else
    begin
      QTD_COR_POS    := 0;
      QTD_COR_NEG    := 0;
      SQL_POS_NEG := 'AND QTD_COR_??? >?< 0 ';
    end;
  end;
  //
  //IND_EST        := ;
  //COD_PART       := ;
  Local := 0;
  case LocalKnd of
    (*0*)slkND: ; // Zero!
    (*1*)slkQualquer:    Local := Escolhe(EntiSitio, FornecMO);
    (*2*)slkForcaFornec: Local := FornecMO;
    (*3*)slkForcaSitio:  Local := EntiSitio;
    (*4*)slkPrefFornec:  Local := Escolhe(FornecMO, EntiSitio);
    (*5*)slkPrefSitio:   Local := Escolhe(EntiSitio, FornecMO);
    (*?*)else Geral.MB_Erro(
    '"LocalKnd" n�o implementado em "UnSPED_EFD_PF.InsereItensAtuais_K280()"');
  end;
(*
"Local" indefinido em "UnSPED_EFD_PF.InsereItensAtuais_K280()"
MovimID: 24
Codigo: 21
IME-C: 1671
IME-I: 0
Reduzido: 138
ClientMO: -11
FornecMO: 0
EntiSitio: 0
*)
  if Local = 0 then Geral.MB_Erro(
    '"Local" indefinido em "UnSPED_EFD_PF.InsereItensAtuais_K280()"' +
    sLineBreak + 'MovimID: ' + Geral.FF0(MovimID) +
    sLineBreak + 'Codigo: ' + Geral.FF0(Codigo) +
    sLineBreak + 'IME-C: ' + Geral.FF0(MovimCod) +
    sLineBreak + 'IME-I: ' + Geral.FF0(Controle) +
    sLineBreak + 'Reduzido: ' + Geral.FF0(GraGruX) +
    sLineBreak + 'ClientMO: ' + Geral.FF0(ClientMO) +
    sLineBreak + 'FornecMO: ' + Geral.FF0(FornecMO) +
    sLineBreak + 'EntiSitio: ' + Geral.FF0(EntiSitio)+
    sLineBreak);
  //SPED_EFD_PF.ObtemIND_ESTeCOD_PART(Empresa, ClientMO, FornecMO, GraGruX,
    //IND_EST, COD_PART, Entidade);
  SPED_EFD_PF.ObtemIND_ESTeCOD_PART(Empresa, ClientMO, Local, GraGruX,
    IND_EST, COD_PART, Entidade);
  BalID          := MovimID;
  BalCod         := Codigo;
  BalNum         := MovimCod;
  BalItm         := Controle;
  BalEnt         := Geral.IMV(COD_PART);
  Grade_PF.ObtemGrandezaDeGraGruX(GraGruX, OriGrandeza, Tipo_Item);
  Grandeza       := Integer(OriGrandeza);
  Pecas          := 0;
  AreaM2         := 0;
  PesoKg         := 0;
  case OriGrandeza of
    (*0*)gumPeca   : Pecas  := Qtde;
    (*1*)gumAreaM2 : AreaM2 := Qtde;
    (*2*)gumPesoKG : PesoKg := Qtde;
    else Geral.MB_Aviso(
    'Grandeza inesperada em "FmSPED_EFD_K2XX.InsereItensAtuais_K280()"');
  end;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OriOrigemOpeProc);
  OrigemIDKnd    := Integer(OriOrigemIDKnd);
  OriSPEDEFDKnd  := Integer(OrigemSPEDEFDKnd);
  //
  AMIni := DmkPF.DataToAnoMes(DtCorrApo);
  AMFim := DmkPF.DataToAnoMes(DataSPED);
  AMAtu := AMINi;
  while AMAtu < AMFim do
  begin
    DtAtu          := IncMonth(Geral.AnoMesToData(AMAtu, 1), 1) - 1;
    DT_EST         := Geral.FDT(DtAtu, 1);
(*    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND K100=' + Geral.FF0(K100),
    'AND RegisPai="' + RegPai + '"',
    'AND DT_EST="' + DT_EST + '"',
    'AND ESTSTabSorc=' + Geral.FF0(ESTSTabSorc),
    'AND OriOpeProc=' + Geral.FF0(OriOpeProc),
    'AND OrigemIDKnd=' + Geral.FF0(BalItm),
    'AND OriSPEDEFDKnd=' + Geral.FF0(OriSPEDEFDKnd),
    'AND BalID=' + Geral.FF0(BalID),
    'AND BalNum=' + Geral.FF0(BalNum),
    'AND BalItm=' + Geral.FF0(BalItm),
    SQL_POS_NEG,
    '']);*)
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    //'AND K100=' + Geral.FF0(K100),
    //'AND RegisPai="' + RegPai + '"',
    'AND DT_EST="' + DT_EST + '"',
    //'AND ESTSTabSorc=' + Geral.FF0(ESTSTabSorc),
    //'AND OriOpeProc=' + Geral.FF0(OriOpeProc),
    //'AND OrigemIDKnd=' + Geral.FF0(BalItm),
    //'AND OriSPEDEFDKnd=' + Geral.FF0(OriSPEDEFDKnd),
    'AND BalID=' + Geral.FF0(BalID),
    'AND BalNum=' + Geral.FF0(BalNum),
    'AND BalItm=' + Geral.FF0(BalItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    'AND DebCred=' + Geral.FF0(Integer(DebCred)),
    '']);
    if DmModVS.QrX999.RecordCount > 0 then
      Exit;
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    (*
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      LinArq  := DmModVS.QrX999.FieldByName('LinArq').AsInteger;
      SQLType := stUpd;
      if Agrega then
      begin
        QTD_COR_POS := QTD_COR_POS + DmModVS.QrX999.FieldByName('QTD_COR_POS').AsFloat;
        QTD_COR_NEG := QTD_COR_NEG + DmModVS.QrX999.FieldByName('QTD_COR_NEG').AsFloat;
      end;
    end else
    *)
    begin
      LinArq := 0;
      LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'LinArq', [
      (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
      stIns, LinArq, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'REG', 'RegisPai', 'RegisAvo',
    'K100', 'DT_EST', 'COD_ITEM',
    'QTD_COR_POS', 'QTD_COR_NEG', 'IND_EST',
    'COD_PART', 'BalTab', 'BalID',
    'BalCod', 'BalNum',
    'BalItm', 'BalEnt', 'GraGruX',
    'Grandeza', 'Pecas', 'AreaM2',
    'PesoKg', 'Entidade', 'Tipo_Item',
    'ESTSTabSorc', 'OriOpeProc', 'OrigemIDKnd',
    'OriSPEDEFDKnd', 'DebCred'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
    REG, RegPai, RegAvo,
    K100, DT_EST, COD_ITEM,
    QTD_COR_POS, QTD_COR_NEG, IND_EST,
    COD_PART, Integer(BalTab), BalID,
    BalCod, BalNum,
    BalItm, BalEnt, GraGruX,
    Grandeza, Pecas, AreaM2,
    PesoKg, Entidade, Tipo_Item,
    ESTSTabSorc, OriOpeProc, OrigemIDKnd,
    OriSPEDEFDKnd, Integer(DebCred)], [
    ImporExpor, AnoMes, Empresa, LinArq], True);
    //
    AMAtu := dmkPF.IncrementaAnoMes(AMAtu, 1);
  end;
end;

function TUnSPED_EFD_PF.LiberaAcaoVS_SPED(ImporExpor, AnoMes, Empresa: Integer;
  EstagioVS_SPED: TEstagioVS_SPED): Boolean;
var
  FldPre, FldPos, AskPre, AskPos: String;
  Continua: Boolean;
  AMPre, AMPos: Integer;
begin
  Result := False;
  AMPre := 0;
  AMPos := 0;
  case EstagioVS_SPED of
    TEstagioVS_SPED.evsspedEncerraVS:
    begin
      FldPre := 'MovimVS';
      FldPos := 'GerLibEFD';
      AskPre :=
      'O movimento de couros ainda n�o foi encerrado para o per�odo selecionado.'
      + sLineBreak +
      'Ap�s o encerramento os novos lan�amentos feitos com data anterior ao per�odo ser�o lan�ados em SPED posterior como ajuste!'
      + sLineBreak + 'Deseja encerrar o per�odo?';
    end;
    TEstagioVS_SPED.evsspedGeraSPED:
    begin
      FldPre := 'GerLibEFD';
      FldPos := 'EnvioEFD';
      AskPre :=
      'A gera��o do SPED ainda n�o foi liberada para o per�odo selecionado.'
      + sLineBreak + 'Deseja liberar?';
    end;
    TEstagioVS_SPED.evsspedExportaSPED:
    begin
      FldPre := 'EnvioEFD';
      FldPos := 'EncerraEFD';
      AskPre :=
      'A exporta��o do SPED ainda n�o foi liberada para o per�odo selecionado.'
      + sLineBreak + 'Deseja liberar?';
    end;
    //TEstagioVS_SPED.evsspedNone:
    else
    begin
      FldPre := '';
      FldPos := '';
      AskPre := '';
      AskPos := '';
    end;
  end;
  //
  AMPre := SPEDEFDEnce_AnoMes(FldPre);
  AMPos := SPEDEFDEnce_AnoMes(FldPos);
  //
  if AMPre < AnoMes then
  begin
    if Geral.MB_Pergunta(AskPre) = ID_Yes then
    begin
      Continua := SPED_EFD_PF.EncerraMesSPED(ImporExpor, AnoMes, Empresa, FldPre);
      if Continua then
      begin
        // reconfirmar que criou!
        AMPre := SPEDEFDEnce_AnoMes(FldPre);
        Continua := AMPre >= AnoMes;
      end;
    end else
      Continua := False;
  end;
  if Continua then
  begin
    Continua := AMPos < AnoMes;
(*
    if not Continua then
    begin
      if Geral.MB_Pergunta(AskPos) = ID_Yes then
      begin
        Continua := SPED_EFD_PF.EncerraMesSPED(AnoMes, Empresa, FldPos);
        if Continua then
        begin
          // reconfirmar que criou!
          AMPos := SPEDEFDEnce_AnoMes(FldPos);
          Continua := AMPos >= AnoMes;
        end;
      end;
    end;
*)
  end;
  // Ini 2019-01-04
  if not Continua then
  begin
    case EstagioVS_SPED of
      TEstagioVS_SPED.evsspedEncerraVS: ;
      TEstagioVS_SPED.evsspedGeraSPED:  ;
      TEstagioVS_SPED.evsspedExportaSPED: Continua := AMPre = AnoMes;
      else ;
    end;
  end;
  // Fim 2019-01-04
  Result := Continua;
end;

procedure TUnSPED_EFD_PF.MostraFormEfdIcmsIpiE001();
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiE001_v03_0_1, FmEfdIcmsIpiE001_v03_0_1, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiE001_v03_0_1.ShowModal;
    FmEfdIcmsIpiE001_v03_0_1.Destroy;
  end;
end;

procedure TUnSPED_EFD_PF.MostraFormEFD_C001();
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "C001"');
(*
  if DBCheck.CriaFm(TFmEFD_C001, FmEFD_C001, afmoNegarComAviso) then
  begin
    FmEFD_C001.ShowModal;
    FmEFD_C001.Destroy;
  end;
*)
end;

procedure TUnSPED_EFD_PF.MostraFormEFD_D001();
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "D001"');
(*
  if DBCheck.CriaFm(TFmEFD_D001, FmEFD_D001, afmoNegarComAviso) then
  begin
    FmEFD_D001.ShowModal;
    FmEFD_D001.Destroy;
  end;
*)
end;

(*
procedure TUnSPED_EFD_PF.MostraFormEFD_E001();
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "E001"');
  if DBCheck.CriaFm(TFmEFD_E001, FmEFD_E001, afmoNegarComAviso) then
  begin
    FmEFD_E001.ShowModal;
    FmEFD_E001.Destroy;
  end;
end;
*)

procedure TUnSPED_EFD_PF.MostraFormSPEDEFDEnce();
begin
  if DBCheck.CriaFm(TFmSPEDEFDEnce_v03_0_1, FmSPEDEFDEnce_v03_0_1, afmoNegarComAviso) then
  begin
    FmSPEDEFDEnce_v03_0_1.ShowModal;
    FmSPEDEFDEnce_v03_0_1.Destroy;
  end;
end;

procedure TUnSPED_EFD_PF.MostraFormSpedefdIcmsIpiBalancos(ImporExpor, AnoMes,
  Empresa, LinArq: Integer; Registro: String; Data: TDateTime);
var
  Ano, Mes: Word;
begin
  if DBCheck.CriaFm(TFmSpedefdIcmsIpiBalancos_v03_0_1, FmSpedefdIcmsIpiBalancos_v03_0_1, afmoNegarComAviso) then
  begin
    FmSpedefdIcmsIpiBalancos_v03_0_1.FImporExpor := ImporExpor;
    FmSpedefdIcmsIpiBalancos_v03_0_1.FAnoMes     := AnoMes;
    FmSpedefdIcmsIpiBalancos_v03_0_1.FEmpresa    := Empresa;
    FmSpedefdIcmsIpiBalancos_v03_0_1.FRegPai     := LinArq;
    FmSpedefdIcmsIpiBalancos_v03_0_1.FData       := Data;
    //
    dmkPF.AnoMesDecode(AnoMes, Ano, Mes);
    FmSpedefdIcmsIpiBalancos_v03_0_1.CBMes.ItemIndex := Mes - 1;
    FmSpedefdIcmsIpiBalancos_v03_0_1.CBAno.Text      := Geral.FF0(Ano);
    FmSpedefdIcmsIpiBalancos_v03_0_1.TPData.Date     := Data;
    //
    if Registro = 'H010' then
    begin
      FmSpedefdIcmsIpiBalancos_v03_0_1.PCRegistro.ActivePageIndex := 1;
      FmSpedefdIcmsIpiBalancos_v03_0_1.LaData.Visible             := False;
      FmSpedefdIcmsIpiBalancos_v03_0_1.TPData.Visible             := False;
      FmSpedefdIcmsIpiBalancos_v03_0_1.PnPeriodo.Enabled          := True;
    end else
    if Registro = 'K200' then
    begin
      FmSpedefdIcmsIpiBalancos_v03_0_1.PCRegistro.ActivePageIndex := 2;
      FmSpedefdIcmsIpiBalancos_v03_0_1.LaData.Visible             := True;
      FmSpedefdIcmsIpiBalancos_v03_0_1.TPData.Visible             := True;
      FmSpedefdIcmsIpiBalancos_v03_0_1.PnPeriodo.Enabled          := False;
    end else
      FmSpedefdIcmsIpiBalancos_v03_0_1.PCRegistro.ActivePageIndex := 0;
    //
    if FmSpedefdIcmsIpiBalancos_v03_0_1.PCRegistro.ActivePageIndex > 0 then
      FmSpedefdIcmsIpiBalancos_v03_0_1.ShowModal
    else
      Geral.MB_Erro('Registro n�o implementdo em balan�o de SPED EFD: ' +
      Registro);
    FmSpedefdIcmsIpiBalancos_v03_0_1.Destroy;
  end;
end;

procedure TUnSPED_EFD_PF.MostraFormSpedEfdIcmsIpiClaProd(ImporExpor, AnoMes,
  Empresa, LinArq: Integer; Registro: String; Data: TDateTime);
begin
var
  Ano, Mes: Word;
begin
  if DBCheck.CriaFm(TFmSpedEfdIcmsIpiClaProd_v03_0_1, FmSpedEfdIcmsIpiClaProd_v03_0_1, afmoNegarComAviso) then
  begin
    FmSpedEfdIcmsIpiClaProd_v03_0_1.FImporExpor := ImporExpor;
    FmSpedEfdIcmsIpiClaProd_v03_0_1.FAnoMes     := AnoMes;
    FmSpedEfdIcmsIpiClaProd_v03_0_1.FEmpresa    := Empresa;
    FmSpedEfdIcmsIpiClaProd_v03_0_1.FPeriApu    := LinArq;
    FmSpedEfdIcmsIpiClaProd_v03_0_1.FData       := Data;
    //
    dmkPF.AnoMesDecode(AnoMes, Ano, Mes);
    FmSpedEfdIcmsIpiClaProd_v03_0_1.CBMes.ItemIndex := Mes - 1;
    FmSpedEfdIcmsIpiClaProd_v03_0_1.CBAno.Text      := Geral.FF0(Ano);
    FmSpedEfdIcmsIpiClaProd_v03_0_1.TPData.Date     := Data;
    //
    if Registro = 'K220' then
    begin
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PCRegistro.ActivePageIndex := 1;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.LaData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.TPData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PnPeriodo.Enabled          := True;
    end else
    if Registro = 'K230' then
    begin
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PCRegistro.ActivePageIndex := 2;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.LaData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.TPData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PnPeriodo.Enabled          := True;
    end else
(*
    if Registro = 'K200' then
    begin
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PCRegistro.ActivePageIndex := 2;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.LaData.Visible             := True;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.TPData.Visible             := True;
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PnPeriodo.Enabled          := False;
    end else
*)
      FmSpedEfdIcmsIpiClaProd_v03_0_1.PCRegistro.ActivePageIndex := 0;
    //
    if FmSpedEfdIcmsIpiClaProd_v03_0_1.PCRegistro.ActivePageIndex > 0 then
      FmSpedEfdIcmsIpiClaProd_v03_0_1.ShowModal
    else
      Geral.MB_Erro('Registro n�o implementdo em SPED EFD K2XX: ' +
      Registro);
    FmSpedEfdIcmsIpiClaProd_v03_0_1.Destroy;
  end;
end;

end;

procedure TUnSPED_EFD_PF.MostraFormSPED_EFD_Exporta();
begin
  if DBCheck.CriaFm(TFmSPED_EFD_Exporta_v03_0_1, FmSPED_EFD_Exporta_v03_0_1, afmoNegarComAviso) then
  begin
    FmSPED_EFD_Exporta_v03_0_1.EdEmpresa.ValueVariant := 1;
    FmSPED_EFD_Exporta_v03_0_1.CBEmpresa.KeyValue := 1;
    FmSPED_EFD_Exporta_v03_0_1.RGCOD_FIN.ItemIndex := 1;
    FmSPED_EFD_Exporta_v03_0_1.RGPreenche.ItemIndex := 2;
    FmSPED_EFD_Exporta_v03_0_1.EdMes.ValueVariant := Date - 21;
    FmSPED_EFD_Exporta_v03_0_1.ShowModal;
    FmSPED_EFD_Exporta_v03_0_1.Destroy;
  end;
end;

function TUnSPED_EFD_PF.ObtemTipoDeProducaoSPED(EntiEmp: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SPED_EFD_Producao ',
      'FROM paramsemp ',
      'WHERE Codigo=' + Geral.FF0(EntiEmp),
      '']);
      //
    Result := Qry.FieldByName('SPED_EFD_Producao').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnSPED_EFD_PF.ObtemTipoPeriodoRegistro(Empresa: Integer;
  Registro, ProcOri: String): Integer;
const
  sProcName = 'TUnSPED_EFD_PF.ObtemTipoPeriodoRegistro()';
begin
  DModG.ReopenParamsEmp(Empresa);
  Result := 0;
  //
  if Registro = 'E100' then
    Result := DModG.QrParamsEmpSPED_EFD_Peri_E100.Value
  else
  if Registro = 'E500' then
    Result := DModG.QrParamsEmpSPED_EFD_Peri_E500.Value
  else
  if Registro = 'K100' then
    Result := DModG.QrParamsEmpSPED_EFD_Peri_K100.Value
  else
  begin
    Geral.MB_Erro(
    'Tipo de per�odo indefinido em "' + sProcName + '"' +
    sLineBreak +
    'Origem: "' + ProcOri + '"');
    Exit;
  end;
  //
  if Result < 1 then
  begin
    Geral.MB_Erro(
    'Tipo de per�odo n�o definido na filial para:' + sLineBreak +
    'Empresa: ' + Geral.FF0(Empresa) + sLineBreak +
    'Registro: ' + Registro + sLineBreak +
    'procedimento: "' + sProcName + '"' + sLineBreak +
    'Origem: "' + ProcOri + '"' + sLineBreak +
    'Configure em Op��es >> Filiais na guia SPED EFD ');
    Exit;
  end;
end;

function TUnSPED_EFD_PF.ObtemDatasDePeriodoSPED(const Ano, Mes, Periodo: Integer;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; var DataIni, DataFim:
  TDateTime): Boolean;
begin
  Result := False;
  case TipoPeriodoFiscal of
    //spedperIndef=0,
    (*1*)spedperDiario:
    begin
      DataIni := EncodeDate(Ano, Mes, Periodo);
      DataFim := DataIni;
      Result  := True;
    end;
    {
    (*2*)spedperSemanal:
    begin
      Errado? semana pode comecar de 1 a 7!
      DataIni :=       DataIni := EncodeDate(Ano, Mes, Periodo);
      DataFim := DataIni;

      DataFim := DataIni;
      Result  := EncodeDate(Ano, Mes, (Periodo * 7) - 6);
    end;
    }
    (*3*)spedperDecendial:
    begin
      DataIni := EncodeDate(Ano, Mes, (Periodo * 10) - 9);
      if Periodo = 3 then
        DataFim := IncMonth(EncodeDate(Ano, Mes, 1), 1) - 1
      else
        DataFim := DataIni + 9;
      Result  := True;
    end;
    {
    (*4*)spedperQuinzenal:
    begin
      Meses := Geral.Periodo2000(Data);
      DecodeDate(Data, Ano, Mes, Dia);
      Periodos := (Dia div 15) + 1;
      if Periodos > 2 then
        Periodos := 2;
      Result := (Meses - 1) * 2;
      Result := Result + Periodos;
    end;
    }
    (*5*)spedperMensal:
    begin
      DataIni := EncodeDate(Ano, Mes, 1);
      DataFim := IncMonth(EncodeDate(Ano, Mes, 1), 1) - 1;
      Result  := True;
    end;
    else Geral.MB_Erro('Tipo de periodo n�o imlementado!');
  end;
end;

function TUnSPED_EFD_PF.ObtemIND_ESTeCOD_PART(const Empresa, ClientMO, Terceiro,
  GraGruX: Integer; var IND_EST, COD_PART: String; var Entidade: Integer): String;
begin
  Result   := '';
  IND_EST  := '';
  COD_PART := '';
  Entidade := 0;
  // o produto pertence a empresa declarante
  if ClientMO  = Empresa then
  begin
    if Terceiro = Empresa then
    //Da empresa em seu poder
    begin
      IND_EST  := Geral.FF0(Integer(spedipiePropInfPoderInf));//=0,
      COD_PART := '';
      Entidade := Empresa;
    end else
    //Da empresa com terceiros
    begin
      IND_EST  := Geral.FF0(Integer(spedipiePropInfPoderTer)); //=1,
      COD_PART := Geral.FF0(Terceiro);
      Entidade := Terceiro;
      if Terceiro = 0 then
        Result := 'COD_PART: ' + Geral.FF0(Terceiro) +
        ' Da empresa com terceiro indefinido! Empresa: ' + Geral.FF0(Empresa) +
        ' cliente de servi�o: ' + Geral.FF0(ClientMO);
    end;
  end else
  // o produto N�O pertence a empresa declarante
  begin
    if Terceiro = Empresa then
    //De terceiros c/ a empresa
    begin
      IND_EST := Geral.FF0(Integer(spedipiePropTerPoderInf)); //=2,
      COD_PART := Geral.FF0(Terceiro);
      Entidade := Terceiro;
      if (Terceiro = 0) then
        Result := 'COD_PART: ' + Geral.FF0(Terceiro) +
        ' De terceiro indefinido! Empresa: ' + Geral.FF0(Empresa) +
        ' cliente de servi�o: ' + Geral.FF0(ClientMO);
    end else
    // De terceiros com terceiros
    begin
      IND_EST  := '-';
      COD_PART := '0';
      Entidade := Terceiro;
      //
      //MeAviso.Lines.Add('COD_ITEM: ' + COD_ITEM +
      Result := 'COD_ITEM: ' + Geral.FF0(GraGruX) +
      ' De terceiros com terceiros! COD_PART: ' + Geral.FF0(Terceiro);
    end;
  end;
end;

function TUnSPED_EFD_PF.ObtemPeriodoSPEDdeData(Data: TDateTime;
  TipoPeriodoFiscal: TTipoPeriodoFiscal): Integer;
var
  Meses, Periodo, Dias, Semanas, Periodos: Integer;
  Dia, Mes, Ano: Word;
begin
  Result := 0;
{///////////////////////////////////////////////////////////////////////////////
Per�odo
                http://www31.receita.fazenda.gov.br/SicalcWeb/calcpfPer_odo.html
                extraido em 13/05/2017
Para as receitas que a aplica��o poder� efetuar o c�lculo de acr�scimos legais
  (multa de mora e juros), essa informa��o ser� obrigat�ria. Para as demais, a
  aplica��o solicitar�, em seu lugar, o "Per�odo de Apura��o".
Para tipo de per�odo igual a "Exerc�cio", dever� ser informado o ano
  correspondente, com quatro algarismos. N�o dever� ser informado o ano-base e
  sim o exerc�cio da declara��o.
Para tipo de per�odo igual a "Trimestral", dever� ser informado o trimestre
  correspondente, no formato TAAAA, onde T � igual ao n�mero do trimestre
  ( 1,2,3 ou 4) e AAAA � igual ao ano com quatro algarismos.
Para tipo de per�odo igual a "Mensal", dever� ser informado o m�s
  correspondente, no formato MMAAAA, onde MM � igual ao n�mero do m�s com dois
  algarismos(01 a 12) e AAAA � igual ao ano com quatro algarismos.
Para tipo de per�odo igual a "Quinzenal", dever� ser informada a quinzena
  correspondente, no formato QMMAAAA, onde Q � igual ao n�mero da quinzena
  (1 ou 2), MM � igual ao n�mero do m�s com dois algarismos (01 a 12) e AAAA �
  igual ao ano com quatro algarismos.
Para tipo de per�odo igual a "Decendial", dever� ser informado o dec�ndio
  correspondente, no formato DMMAAAA, onde D � igual ao n�mero do dec�ndio
  (1, 2 ou 3), MM � igual ao n�mero do m�s com dois algarismos (01 a 12) e AAAA
  � igual ao ano com quatro algarismos.
Para tipo de per�odo igual a "Semanal", dever� ser informada a semana
  correspondente, no formato SMMAAAA, onde S � igual ao n�mero da semana
  (1 a 5), MM � igual ao n�mero do m�s com dois algarismos (01 a 12) e AAAA �
  igual ao ano com quatro algarismos.
  OBS: Para os tributos de periodicidade semanal, que a pr�pria p�gina apresenta
    os valores dos acr�scimos legais, quando devidos, e do respectivo
    vencimento, a semana se inicia no domingo e termina no s�bado. Portanto, a
    primeira semana de um determinado m�s � aquela onde aparece o primeiro
    s�bado, mesmo que seja o primeiro dia do m�s.
Para tipo de per�odo igual a "Di�rio", informe o dia correspondente, no formato
  DDMMAAAA, onde DD � igual ao dia do m�s (01 a 31), MM � igual ao n�mero do m�s
  com dois algarismos (01 a 12) e AAAA � igual ao ano com quatro algarismos.e
///////////////////////////////////////////////////////////////////////////////}
  case TipoPeriodoFiscal of
    //spedperIndef=0,
    (*1*)spedperDiario   : Result := Trunc(Data) - Trunc(EncodeDate(1999, 12, 31));
    (*2*)spedperSemanal  :
    begin
      Dias := Trunc(Data) - Trunc(EncodeDate(2000, 1, 1));
      Result := (Dias div 7) + 1;
    end;
    (*3*)spedperDecendial:
    begin
      Meses := Geral.Periodo2000(Data);
      DecodeDate(Data, Ano, Mes, Dia);
      Semanas := ((Dia-1) div 10) + 1;
      if Semanas > 3 then
        Semanas := 3;
      Result := (Meses - 1) * 3;
      Result := Result + Semanas;
    end;
    (*4*)spedperQuinzenal:
    begin
      Meses := Geral.Periodo2000(Data);
      DecodeDate(Data, Ano, Mes, Dia);
      Periodos := (Dia div 15) + 1;
      if Periodos > 2 then
        Periodos := 2;
      Result := (Meses - 1) * 2;
      Result := Result + Periodos;
    end;
    (*5*)spedperMensal: Result := Geral.Periodo2000(Data);
    else Geral.MB_Erro('Tipo de periodo n�o imlementado!');
  end;
end;

function TUnSPED_EFD_PF.ObtemSPED_EFD_KndRegOrigem(const REG: String; var
  Origem: TSPED_EFD_KndRegOrigem): Boolean;
const
  sprocName = 'ObtemSPED_EFD_KndRegOrigem()';
begin
  Result := False;
  //if (REG = 'K200') (***************) then Origem := sek2708oriK200  ->> N�o existe!
  if (REG = 'K230') or (REG = 'K235') then Origem := sek2708oriK230K235 else
  if (REG = 'K250') or (REG = 'K255') then Origem := sek2708oriK250K255 else
  if (REG = 'K210') or (REG = 'K215') then Origem := sek2708oriK210K215 else
  if (REG = 'K260') or (REG = 'K265') then Origem := sek2708oriK260K265 else
  if (REG = 'K260') or (REG = 'K265') then Origem := sek2708oriK260K265 else
  if (REG = 'K220') (***************) then Origem := sek2708oriK220 else
  //if (REG = 'K290') (***************) then Origem := sek2708oriK290  ->> N�o existe!
  if (REG = 'K291') (***************) then Origem := sek2708oriK291 else
  if (REG = 'K292') (***************) then Origem := sek2708oriK292 else
  //if (REG = 'K300') (***************) then Origem := sek2708oriK300  ->> N�o existe!
  if (REG = 'K301') (***************) then Origem := sek2708oriK301 else
  if (REG = 'K302') (***************) then Origem := sek2708oriK302 else
  Origem := sek2708oriIndef;
  //
  Result := Integer(Origem) > 0;
  if not Result then
    Geral.MB_ERRO('"REG" n�o implementado em ' + sProcName);
end;

procedure TUnSPED_EFD_PF.ReopenTbSpedEfdXXX(Query: TmySQLQuery; DtIni,
  DtFim: TDateTime; TabName, FldOrd: String);
var
  Ini, Fim: String;
begin
  Ini := Geral.FDT(DtIni, 1);
  Fim := Geral.FDT(DtFim, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.AllID_DB, [
  'SELECT * ',
  'FROM ' + TabName,
  'WHERE DataIni <= "' + Ini + '" ',
  'AND (  ',
  '  DataFim >= "' + Fim + '" ',
  '  OR ',
  '  DataFim < "1900-01-01" ',
  ') ',
  'ORDER BY ' + FldOrd,
  '']);
end;

function TUnSPED_EFD_PF.SPEDEFDEnce_AnoMes(Campo: String): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(AnoMes) AnoMes ',
    'FROM spedefdicmsipience ',
    'WHERE ' + Campo + '=1 ', // 1=Feito
    '']);
    Result := Qry.FieldByName('AnoMes').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnSPED_EFD_PF.SPEDEFDEnce_DataMinMovim(Campo: String): TDateTime;
var
  AnoA, MesA, DiaA: Word;
begin
(*
  AnoMes := SPEDEFDEnce_AnoMes(Campo);
  //
  if AnoMes <> 0 then
    Result := Geral.AnoMesToData(AnoMes, 1);
*)
  if (not DModG.SoUmaEmpresaLogada(True))
  or (MyObjects.FIC(VAR_LIB_EMPRESA_SEL = 0, nil,
  'Empresa logada n�o definida!')) then
  begin
    DecodeDate(DModG.ObtemAgora(), AnoA, MesA, DiaA);
    Result := EncodeDate(AnoA, 1, 1);
    //
    Exit;
  end;
  //
  Result := SPEDEFDEnce_DataUltimoInventario(VAR_LIB_EMPRESA_SEL) + 1;
end;

function TUnSPED_EFD_PF.SPEDEFDEnce_DataUltimoInventario(Empresa: Integer):
  TDateTime;
var
  Agora: TDateTime;
  Qry: TmySQLQuery;
  AnoA, MesA, DiaA, AnoI, MesI, DiaI: Word;
  DataX: TDateTime;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Agora := DModG.ObtemAgora();
    DecodeDate(Agora, AnoA, MesA, DiaA);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(DT_INV) UltDT_INV',
    'FROM efdicmsipih005',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    '']);
    DataX := Qry.FieldByName('UltDT_INV').AsDateTime;
    DecodeDate(DataX, AnoI, MesI, DiaI);
    if AnoI < (AnoA - 1) then
    begin
      DataX := EncodeDate(AnoA - 1, 12, 31);
      if Int(Agora) - Int(DataX) < 60 then
        DataX := IncMonth(DataX, -12);
    end;
    //
    Result := DataX;
  finally
    Qry.Free;
  end;
end;

function TUnSPED_EFD_PF.SPEDEFDEnce_Periodo(Campo: String): Integer;
var
  AnoMes: Integer;
begin
  AnoMes := SPEDEFDEnce_AnoMes(Campo);
  //
  if AnoMes <> 0 then
    Result := Geral.AnoMesToPeriodo(AnoMes)
  else
    Result := 0;
end;

function TUnSPED_EFD_PF.SQLPeriodoFiscal(TipoPeriodoFiscal: TTipoPeriodoFiscal;
  CampoPsq, CampoRes: String; VirgulaFinal: Boolean): String;
var
  Virgula: String;
begin
  if VirgulaFinal then
    Virgula := ', '
  else
    Virgula := ' ';
  //
  case TipoPeriodoFiscal of
    //spedperIndef=0, spedperDiario=1, spedperSemanal=2,
    spedperDecendial: Result := Geral.ATS([
      'CASE ',
      '  WHEN ' + CampoPsq + ' = 0 THEN 0 ',
      '  WHEN DAYOFMONTH(' + CampoPsq + ') < 11 THEN 1 ',
      '  WHEN DAYOFMONTH(' + CampoPsq + ') < 21 THEN 2 ',
      '  ELSE 3 END ' + CampoRes]);
    //spedperQuinzenal=4,
    spedperMensal: Result := Geral.ATS([
      'CASE ',
      '  WHEN ' + CampoPsq + ' = 0 THEN 0 ',
      '  ELSE 1 END ' + CampoRes]);
    else Result := Geral.ATS([
      'CASE ',
      '  WHEN ' + CampoPsq + ' = 0 THEN #ERR ',
      '  ELSE #ERR END ' + CampoRes]);
  end;
  Result := Result + Virgula;
end;

function TUnSPED_EFD_PF.ValidaCOD_INS_SUBST_DentroDoAnoMesSPED(AnoMes,
  GraGruX: Integer; COD_INS_SUBST, COD_DOC_OS: String): Boolean;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K235]*)
  (*Campo 05 (COD_INS_SUBST) � Preenchimento: informar o c�digo do item
  componente/insumo que estava previsto para ser consumido no Registro 0210 e
  que foi substitu�do pelo COD_ITEM deste registro.
  Valida��o: o c�digo do insumo substitu�do deve existir no Registro 0210 para o
  mesmo produto resultante � K230/0200.
  O tipo do componente/insumo (campo TIPO_ITEM do Registro 0200) deve ser igual
  a 00, 01, 02, 03, 04, 05 ou 10.*)

  //  Implementar???
end;

function TUnSPED_EFD_PF.ValidaDataDentroDoAnoMesSPED(AnoMes: Integer;
  Data: TDateTime; OrdemDeServico: String; Obrigatorio: Boolean): Boolean;
var
  DtAMIni, DtAMFim: TDateTime;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K220]*)
  (*Campo 02 (DT_MOV) -: Valida��o: a data deve estar compreendida no per�odo
  informado nos campos DT_INI e DT_FIN do Registro K100.*)
  if Obrigatorio and (Data < 2) then
  begin
    Geral.MB_Aviso('Informe a data!');
    Result := False;
    Exit;
  end;
  if Data > 2 then
  begin
    dmkPF.DatasIniFimDoPeriodo(AnoMes, DtAMIni, DtAMFim);
    if (Data < DtAMIni) or (Data > DtAMFim) then
    begin
      Geral.MB_Aviso('Data deve ser dentro do per�odo!');
      Result := False;
      Exit;
    end;
  end;
end;

function TUnSPED_EFD_PF.ValidaDataFinalDentroDoAnoMesSPED(AnoMes: Integer;
  DataIni, DataFim: TDateTime; OrdemDeServico: String): Boolean;
var
  DtAMIni, DtAMFim: TDateTime;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K210]*)
  //
  (*Campo 03 (DT_FIN_OS) - Preenchimento: informar a data de conclus�o da ordem
  de servi�o. Ficar� em branco, caso a ordem de servi�o n�o seja conclu�da at� a
  data de encerramento do per�odo de apura��o.
  Valida��o: se preenchido, DT_FIN_OS deve estar compreendida no per�odo de
  apura��o do K100 e ser maior ou igual a DT_INI_OS.*)
  if DataFim > 2 then
  begin
    dmkPF.DatasIniFimDoPeriodo(AnoMes, DtAMIni, DtAMFim);
    if (DataFim < DtAMIni) or (DataFim > DtAMFim) then
    begin
      Geral.MB_Aviso('Data final quando informada deve ser dentro do per�odo!');
      Result := False;
      Exit;
    end else
    if DataIni > DataFim then
    begin
      Geral.MB_Aviso('Data final deve ser maior ou igual a data inicial!');
      Result := False;
      Exit;
    end;
  end;
end;

function TUnSPED_EFD_PF.ValidaDataInicialDentroDoAnoMesSPED(AnoMes: Integer;
  DataIni, DataFim: TDateTime; OrdemDeServico: String): Boolean;
var
  DtAMIni, DtAMFim: TDateTime;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K210]*)
  //
  (*Campo 02 (DT_INI_OS) - Preenchimento: a data de in�cio dever� ser informada
  se existir ordem de servi�o, ainda que iniciada em per�odo de apura��o (K100)
  anterior.
  Valida��o: obrigat�rio se informado o campo COD_DOC_OS ou o campo DT_FIN_OS.
  A data informada deve ser menor ou igual a DT_FIN do registro K100.*)
  dmkPF.DatasIniFimDoPeriodo(AnoMes, DtAMIni, DtAMFim);
  if (Trim(OrdemDeServico) <> '') or (DataFim >= 2) then
  begin
    if DataIni < 2 then
    begin
      Geral.MB_Aviso('Data inicial deve ser informada!');
      Result := False;
      Exit;
    end;
  end;
  if (DataIni > DataFim) and (DataFim >= 2) then
  begin
    Geral.MB_Aviso('Data inicial deve ser menor ou igual a data final!');
    Result := False;
    Exit;
  end;
  if (DataIni > DtAMFim) then
  begin
    Geral.MB_Aviso('Data inicial deve ser menor ou igual a data final do per�odo!');
    Result := False;
    Exit;
  end;
end;

function TUnSPED_EFD_PF.ValidaDataItemDentroDoAnoMesSPED(AnoMes: Integer;
  Data, DtIniOP, DtFimOP: TDateTime; OrdemDeServico: String): Boolean;
var
  DtAMIni, DtAMFim: TDateTime;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K235]*)
  (*Campo 02 (DT_SA�DA) - Valida��o: a data deve estar compreendida no per�odo
  da ordem de produ��o, se existente, campos DT_INI_OP e DT_FIN_OP do Registro
  K230. Se DT_FIN_OP do Registro K230 � Itens Produzidos estiver em branco, o
  campo DT_SA�DA dever� ser maior que o campo DT_INI_OP do Registro K230 e menor
  ou igual a DT_FIN do Registro K100. E em qualquer hip�tese a data deve estar
  compreendida no per�odo de apura��o � K100.*)
  dmkPF.DatasIniFimDoPeriodo(AnoMes, DtAMIni, DtAMFim);

  if (DtIniOP > 2) and (Data < DtIniOP) then
  begin
    Geral.MB_Aviso(
    'A data deve ser maior ou igual a data inicial do registro pai!');
    Result := False;
    Exit;
  end;

  if (DtFimOP > 2) and (Data > DtFimOP) then
  begin
    Geral.MB_Aviso(
    'A data deve ser menor ou igual a data final do registro pai!');
    Result := False;
    Exit;
  end;

  if (Data < DtAMIni) or (Data > DtAMFim) then
  begin
    Geral.MB_Aviso(
    'A data deve estar dentro do per�odo SPED!');
    Result := False;
    Exit;
  end;
end;

function TUnSPED_EFD_PF.ValidaExigenciaValorMaiorQueZeroOuZero(AnoMes: Integer;
  DataIni, DataFim: TDateTime; Qtd: Double; OrdemDeServico: String): Boolean;
var
  DtAMIni, DtAMFim: TDateTime;
  A1, A2, A3, A4: Boolean;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K230]*)
  (*Campo 06 (QTD_ENC) � Preenchimento: n�o � admitida quantidade negativa.
  Valida��o:
    a) deve ser maior que zero quando: os campos DT_INI_OP e DT_FIN_OP estiverem
    preenchidos e compreendidos no per�odo do Registro K100 ou todos os tr�s
    campos DT_FIN_OP, DT_INI_OP e COD_DOC_OP n�o estiverem preenchidos;
    b) deve ser igual a zero quando o campo DT_FIN_OP estiver preenchido e for
    menor que o campo DT_INI do Registro 0000.*)
  dmkPF.DatasIniFimDoPeriodo(AnoMes, DtAMIni, DtAMFim);

  A1 := (DataIni > 2) and (DataFim > 2);
  A2 := (DataIni >= DtAMIni) and (DataIni <= DtAMFim) and
        (DataFim >= DtAMIni) and (DataFim <= DtAMFim);
  //
  A3 := (DataIni < 2) and (DataFim < 2) and (Trim(OrdemDeServico) = '');
  A4 := (A1 and A2) or A3;
  //
  if A4 and (Qtd <= 0) then
  begin
    Geral.MB_Aviso('N�o � admitida quantidade negativa ou zerada!');
    Result := False;
    Exit;
  end;
  //
  if ((DataFim > 2) and (DataFim < DtAMIni)) and (Qtd <> 0) then
  begin
    Geral.MB_Aviso(
    'Quantidade deve ser igual a zero quando o campo DT_FIN_OP estiver preenchido e for menor que o campo DT_INI do Registro 0000.!');
    Result := False;
    Exit;
  end;
end;

function TUnSPED_EFD_PF.ValidaGGXEstahDentroDoAnoMesSPED(AnoMes,
  GraGruX: Integer; OrdemDeServico: String): Boolean;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K210]*)
  //
  (*Campo 05 (COD_ITEM_ORI) - Valida��o: o c�digo do item de origem dever�
  existir no campo COD_ITEM do Registro 0200.*)

  (*////////////// � feiro s� depois na exporta��o!!!??? //////////////*)
end;

function TUnSPED_EFD_PF.ValidaGGXOri_DifereGGXDstDentroDoAnoMesSPED(AnoMes,
  GGXOri, GGXDst: Integer; OrdemDeServico: String): Boolean;
begin
  Result := not MyObjects.FIC(GGXOri = GGXDst, nil,
  'Reduzido deve ser diferente do reduzido origem / destino!');
end;

function TUnSPED_EFD_PF.ValidaOSDentroDoAnoMesSPED(AnoMes: Integer; DataIni,
  DataFim: TDateTime; OrdemDeServico: String): Boolean;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K210]*)
  //
  (*Campo 04 (COD_DOC_OS) � Preenchimento: informar o c�digo da ordem de
  servi�o, caso exista.
  Valida��o: obrigat�rio se informado o campo DT_INI_OS.*)
  if DataIni > 2 then
  begin
    if Trim(OrdemDeServico) = '' then
    begin
      Geral.MB_Aviso(
      'Ordem de servi�o deve ser informada quando for informada a data inicial do per�odo!');
      Result := False;
    end;
  end;
end;

function TUnSPED_EFD_PF.ValidaQtdeDentroDoAnoMesSPED(AnoMes: Integer;
  Qtde: Double; OrdemDeServico: String; MaiorQueZero: Boolean): Boolean;
begin
  Result := True;
  (*Campo 01 (REG) - Valor V�lido: [K210]*)
  (*Campo 06 (QTD_ORI) � Preenchimento: n�o � admitida quantidade negativa.*)
  if Qtde < 0 then
  begin
    Geral.MB_Aviso('N�o � admitida quantidade negativa!');
    Result := False;
  end;
  if MaiorQueZero and (Qtde = 0) then
  begin
    Geral.MB_Aviso('N�o � admitida quantidade zerada!');
    Result := False;
  end;
end;

function TUnSPED_EFD_PF.VerificaGGX_SPED(GraGruX, KndCod, KndNSU,
  KndItm: Integer; REG: String): Boolean;
begin
  if GraGruX = 0 then
  begin
    Result := False;
    Geral.MB_Erro('O reduzido ficou indefinido no registro ' + REG + sLineBreak +
    'ID Codigo: ' + Geral.FF0(KndCod) + sLineBreak +
    'IME-C: ' + Geral.FF0(KndNSU) + sLineBreak +
    'IME-I: ' + Geral.FF0(KndItm) + sLineBreak +
    '');
  end else Result := True;
end;

end.


