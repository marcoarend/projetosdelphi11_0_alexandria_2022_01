object FmSPED_EFD_Exporta_IfNotGera: TFmSPED_EFD_Exporta_IfNotGera
  Left = 339
  Top = 185
  Caption = 'SPE-D_EFD-002 :: Exporta'#231#227'o de Arquivo SPED-EFD (TXT)'
  ClientHeight = 705
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 529
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 112
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 23
        Height = 13
        Caption = 'Filial:'
      end
      object LaMes: TLabel
        Left = 640
        Top = 3
        Width = 23
        Height = 13
        Caption = 'M'#234's:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 573
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdMes: TdmkEdit
        Left = 640
        Top = 19
        Width = 82
        Height = 21
        Alignment = taCenter
        TabOrder = 2
        FormatType = dmktfMesAno
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = True
        PermiteNulo = False
        ValueVariant = Null
        ValWarn = False
        OnChange = EdMesChange
      end
      object RGCOD_FIN: TRadioGroup
        Left = 8
        Top = 44
        Width = 573
        Height = 40
        Caption = ' Finalidade do arquivo: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '? - N'#227'o definido'
          '0 - Remessa de arquivo original'
          '1 - Remessa de arquivo substituto')
        TabOrder = 3
        OnClick = RGCOD_FINClick
      end
      object CkCorrigeCriadosAForca: TCheckBox
        Left = 12
        Top = 88
        Width = 193
        Height = 17
        Caption = 'Tenta corrigir itens criados a for'#231'a.'
        TabOrder = 4
      end
      object RGPreenche: TRadioGroup
        Left = 584
        Top = 44
        Width = 413
        Height = 40
        Caption = ' Preenchimento do arquivo: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '? - N'#227'o definido'
          'N'#227'o (vazio)'
          'Sim (informar dados)')
        TabOrder = 5
        OnClick = RGCOD_FINClick
      end
      object CkRecriaRegAotmaticos: TCheckBox
        Left = 727
        Top = 20
        Width = 194
        Height = 17
        Caption = 'Recria registros autom'#225'ticos.'
        Checked = True
        State = cbChecked
        TabOrder = 6
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 112
      Width = 1008
      Height = 417
      ActivePage = TabSheet4
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Registros a serem exportados '
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 389
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object TVBlocos: TTreeView
            Left = 1
            Top = 28
            Width = 998
            Height = 360
            Align = alClient
            Indent = 19
            MultiSelect = True
            TabOrder = 0
            OnClick = TVBlocosClick
            OnKeyDown = TVBlocosKeyDown
            Items.NodeData = {
              03010000001E0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000
              00000000000100}
          end
          object Panel5: TPanel
            Left = 1
            Top = 1
            Width = 998
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object SpeedButton1: TSpeedButton
              Left = 4
              Top = 2
              Width = 105
              Height = 22
              Caption = 'Bloco K - estoque'
              OnClick = SpeedButton1Click
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Texto Gerado '
        ImageIndex = 1
        object MeGerado: TMemo
          Left = 0
          Top = 0
          Width = 1000
          Height = 389
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          WordWrap = False
          OnKeyUp = MeGeradoKeyUp
          OnMouseDown = MeGeradoMouseDown
          OnMouseUp = MeGeradoMouseUp
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Erros e Avisos '
        ImageIndex = 2
        object MeErros: TMemo
          Left = 0
          Top = 0
          Width = 1000
          Height = 389
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          WordWrap = False
          OnKeyUp = MeGeradoKeyUp
          OnMouseDown = MeGeradoMouseDown
          OnMouseUp = MeGeradoMouseUp
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' Documentos sem defini'#231#227'o de Modelo Fiscal '
        ImageIndex = 3
        object Label4: TLabel
          Left = 0
          Top = 0
          Width = 1000
          Height = 18
          Align = alTop
          Alignment = taCenter
          Caption = 'Duplo clique na linha abre a janela para edi'#231#227'o da NF'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitWidth = 373
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 18
          Width = 1000
          Height = 371
          Align = alClient
          DataSource = DsErrMod
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'DataFiscal'
              Title.Caption = 'Data fiscal'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Terceiro'
              Width = 47
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TERCEIRO'
              Title.Caption = 'Nome do Cliente / Fornecedor'
              Width = 314
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ide_dEmi'
              Title.Caption = 'Emiss'#227'o'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ide_serie'
              Title.Caption = 'S'#233'ie'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ide_nNF'
              Title.Caption = 'N'#186' N.F.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMSTot_vNF'
              Title.Caption = 'Valor NF'
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        Visible = False
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        Visible = False
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 492
        Height = 32
        Caption = 'Exporta'#231#227'o de Arquivo SPED-EFD (TXT)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 492
        Height = 32
        Caption = 'Exporta'#231#227'o de Arquivo SPED-EFD (TXT)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 492
        Height = 32
        Caption = 'Exporta'#231#227'o de Arquivo SPED-EFD (TXT)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel47: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 634
    Width = 1008
    Height = 71
    Align = alBottom
    TabOrder = 3
    object Panel16: TPanel
      Left = 2
      Top = 0
      Width = 1004
      Height = 21
      Align = alBottom
      ParentBackground = False
      TabOrder = 0
      object StatusBar: TStatusBar
        Left = 1
        Top = 1
        Width = 1002
        Height = 19
        Panels = <
          item
            Text = ' Posi'#231#227'o do cursor no texto gerado:'
            Width = 192
          end
          item
            Width = 100
          end
          item
            Text = ' Arquivo salvo:'
            Width = 96
          end
          item
            Width = 50
          end>
      end
    end
    object PainelConfirma: TPanel
      Left = 2
      Top = 21
      Width = 1004
      Height = 48
      Align = alBottom
      TabOrder = 1
      object Label2: TLabel
        Left = 152
        Top = 4
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
        Enabled = False
      end
      object Label3: TLabel
        Left = 268
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Data final:'
        Enabled = False
      end
      object Panel2: TPanel
        Left = 872
        Top = 1
        Width = 131
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object TPDataIni: TdmkEditDateTimePicker
        Left = 152
        Top = 19
        Width = 113
        Height = 21
        Time = 0.499141331019927700
        Enabled = False
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        LastDayTimePicker = TPDataFim
        DatePurpose = dmkdpNone
      end
      object TPDataFim: TdmkEditDateTimePicker
        Left = 268
        Top = 19
        Width = 113
        Height = 21
        Time = 0.499141331019927700
        Enabled = False
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object BtErros: TBitBtn
        Left = 460
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Erros'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtErrosClick
      end
    end
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pem.SPED_EFD_IND_PERFIL, pem.SPED_EFD_IND_ATIV,'
      'pem.SPED_EFD_CadContador, pem.SPED_EFD_CRCContador,'
      'pem.SPED_EFD_EscriContab, pem.SPED_EFD_EnderContab,'
      'SPED_EFD_Path,'
      'IF(ctd.Tipo=0, ctd.RazaoSocial, ctd.Nome) NO_CTD,'
      'ctd.CPF CPF_CTD, ctb.CNPJ CNPJ_CTB,'
      'IF(ctb.Tipo=0, ctb.RazaoSocial, ctb.Nome) NO_CTB'
      'FROM paramsemp pem'
      'LEFT JOIN entidades ctd ON ctd.Codigo=pem.SPED_EFD_CadContador'
      'LEFT JOIN entidades ctb ON ctb.Codigo=pem.SPED_EFD_EscriContab'
      'WHERE pem.Codigo=:P0'
      ''
      '')
    Left = 44
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsEmpSPED_EFD_IND_PERFIL: TWideStringField
      FieldName = 'SPED_EFD_IND_PERFIL'
      Size = 1
    end
    object QrParamsEmpSPED_EFD_IND_ATIV: TSmallintField
      FieldName = 'SPED_EFD_IND_ATIV'
    end
    object QrParamsEmpSPED_EFD_CadContador: TIntegerField
      FieldName = 'SPED_EFD_CadContador'
    end
    object QrParamsEmpSPED_EFD_CRCContador: TWideStringField
      FieldName = 'SPED_EFD_CRCContador'
      Size = 15
    end
    object QrParamsEmpSPED_EFD_EscriContab: TIntegerField
      FieldName = 'SPED_EFD_EscriContab'
    end
    object QrParamsEmpSPED_EFD_EnderContab: TSmallintField
      FieldName = 'SPED_EFD_EnderContab'
    end
    object QrParamsEmpSPED_EFD_Path: TWideStringField
      FieldName = 'SPED_EFD_Path'
      Size = 255
    end
    object QrParamsEmpNO_CTD: TWideStringField
      FieldName = 'NO_CTD'
      Size = 100
    end
    object QrParamsEmpNO_CTB: TWideStringField
      FieldName = 'NO_CTB'
      Size = 100
    end
    object QrParamsEmpCPF_CTD: TWideStringField
      FieldName = 'CPF_CTD'
      Size = 18
    end
    object QrParamsEmpCNPJ_CTB: TWideStringField
      FieldName = 'CNPJ_CTB'
      Size = 18
    end
    object QrParamsEmpSPED_EFD_DtFiscal: TSmallintField
      FieldName = 'SPED_EFD_DtFiscal'
    end
    object QrParamsEmpSPED_EFD_ID_0200: TSmallintField
      FieldName = 'SPED_EFD_ID_0200'
    end
    object QrParamsEmpSPED_EFD_ID_0150: TSmallintField
      FieldName = 'SPED_EFD_ID_0150'
    end
  end
  object QrEmpresa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo, ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, '
      'IE, ECodMunici, PCodMunici, NIRE, SUFRAMA,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOME_ENT, '
      'IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJ_CPF, '
      'IF(en.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF, '
      'IF(en.Tipo=0, en.Fantasia, en.Apelido) NO_2_ENT, '
      'IF(en.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD, '
      'IF(en.Tipo=0, en.ERua, en.PRua) RUA, '
      'IF(en.Tipo=0, en.ECompl, en.PCompl) COMPL,'
      'IF(en.Tipo=0, en.EBairro, en.PBairro) BAIRRO,'
      'IF(en.Tipo=0, en.ETe1, en.PTe1) TE1,'
      'IF(en.Tipo=0, en.EFax, en.PFax) FAX,'
      'IF(en.Tipo=0, en.EEmail, en.PEmail) EMAIL,'
      'en.Codigo'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      ''
      'WHERE en.Codigo=:P0'
      '')
    Left = 44
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmpresaTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmpresaENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEmpresaPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrEmpresaELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrEmpresaPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrEmpresaECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEmpresaPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrEmpresaNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrEmpresaCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEmpresaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEmpresaIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmpresaECodMunici: TIntegerField
      FieldName = 'ECodMunici'
    end
    object QrEmpresaPCodMunici: TIntegerField
      FieldName = 'PCodMunici'
    end
    object QrEmpresaNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEmpresaSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
    object QrEmpresaNO_2_ENT: TWideStringField
      FieldName = 'NO_2_ENT'
      Size = 60
    end
    object QrEmpresaNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEmpresaRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEmpresaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEmpresaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEmpresaTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEmpresaFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEmpresaEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
  end
  object QrVersao: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COD_VER'
      'FROM spedefdvers'
      'WHERE (DT_FIN >=:P0)'
      'OR (DT_INI <:P1 AND DT_FIN<2)'
      ' ')
    Left = 44
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrVersaoCodTxt: TWideStringField
      FieldName = 'CodTxt'
      Size = 60
    end
  end
  object QrCampos: TMySQLQuery
    Database = Dmod.MyDB
    Left = 216
    Top = 64
    object QrCamposBloco: TWideStringField
      FieldName = 'Bloco'
      Origin = 'spedefdflds.Bloco'
      Size = 1
    end
    object QrCamposRegistro: TWideStringField
      FieldName = 'Registro'
      Origin = 'spedefdflds.Registro'
      Size = 4
    end
    object QrCamposNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'spedefdflds.Numero'
    end
    object QrCamposVersaoIni: TIntegerField
      FieldName = 'VersaoIni'
      Origin = 'spedefdflds.VersaoIni'
    end
    object QrCamposVersaoFim: TIntegerField
      FieldName = 'VersaoFim'
      Origin = 'spedefdflds.VersaoFim'
    end
    object QrCamposCampo: TWideStringField
      FieldName = 'Campo'
      Origin = 'spedefdflds.Campo'
      Size = 50
    end
    object QrCamposTipo: TWideStringField
      FieldName = 'Tipo'
      Origin = 'spedefdflds.Tipo'
      Size = 1
    end
    object QrCamposTam: TSmallintField
      FieldName = 'Tam'
      Origin = 'spedefdflds.Tam'
    end
    object QrCamposTObrig: TSmallintField
      FieldName = 'TObrig'
      Origin = 'spedefdflds.TObrig'
    end
    object QrCamposDecimais: TSmallintField
      FieldName = 'Decimais'
      Origin = 'spedefdflds.Decimais'
    end
    object QrCamposDescrLin1: TWideStringField
      FieldName = 'DescrLin1'
      Origin = 'spedefdflds.DescrLin1'
      Size = 80
    end
    object QrCamposDescrLin2: TWideStringField
      FieldName = 'DescrLin2'
      Origin = 'spedefdflds.DescrLin2'
      Size = 80
    end
    object QrCamposDescrLin3: TWideStringField
      FieldName = 'DescrLin3'
      Origin = 'spedefdflds.DescrLin3'
      Size = 80
    end
    object QrCamposDescrLin4: TWideStringField
      FieldName = 'DescrLin4'
      Origin = 'spedefdflds.DescrLin4'
      Size = 80
    end
    object QrCamposDescrLin5: TWideStringField
      FieldName = 'DescrLin5'
      Origin = 'spedefdflds.DescrLin5'
      Size = 80
    end
    object QrCamposDescrLin6: TWideStringField
      FieldName = 'DescrLin6'
      Origin = 'spedefdflds.DescrLin6'
      Size = 80
    end
    object QrCamposDescrLin7: TWideStringField
      FieldName = 'DescrLin7'
      Origin = 'spedefdflds.DescrLin7'
      Size = 80
    end
    object QrCamposDescrLin8: TWideStringField
      FieldName = 'DescrLin8'
      Origin = 'spedefdflds.DescrLin8'
      Size = 80
    end
    object QrCamposCObrig: TWideStringField
      FieldName = 'CObrig'
      Origin = 'spedefdflds.CObrig'
      Size = 2
    end
    object QrCamposEObrig: TWideStringField
      FieldName = 'EObrig'
      Origin = 'spedefdflds.CObrig'
      Size = 2
    end
    object QrCamposSObrig: TWideStringField
      FieldName = 'SObrig'
      Origin = 'spedefdflds.CObrig'
      Size = 2
    end
  end
  object QrEnder: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo, ENumero, PNumero, ELograd, PLograd, '
      'ECEP, PCEP, IE, ECodMunici, PCodMunici,'
      ' NIRE, SUFRAMA, ECodiPais, PCodiPais,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOME_ENT, '
      'IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJ_CPF, '
      'IF(en.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF, '
      'IF(en.Tipo=0, en.Fantasia, en.Apelido) NO_2_ENT, '
      'IF(en.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD, '
      'IF(en.Tipo=0, en.ERua, en.PRua) RUA, '
      'IF(en.Tipo=0, en.ECompl, en.PCompl) COMPL,'
      'IF(en.Tipo=0, en.EBairro, en.PBairro) BAIRRO,'
      'IF(en.Tipo=0, en.ETe1, en.PTe1) TE1,'
      'IF(en.Tipo=0, en.EFax, en.PFax) FAX,'
      'IF(en.Tipo=0, en.EEmail, en.PEmail) EMAIL,'
      'en.Codigo'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      ''
      'WHERE en.Codigo=:P0'
      '')
    Left = 260
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnderTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEnderENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEnderPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrEnderELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrEnderPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrEnderECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEnderPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrEnderIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEnderECodMunici: TIntegerField
      FieldName = 'ECodMunici'
    end
    object QrEnderPCodMunici: TIntegerField
      FieldName = 'PCodMunici'
    end
    object QrEnderNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEnderSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
    object QrEnderNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrEnderCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEnderNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEnderNO_2_ENT: TWideStringField
      FieldName = 'NO_2_ENT'
      Size = 60
    end
    object QrEnderNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEnderRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEnderCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEnderBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEnderTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEnderFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEnderEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrEnderCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnderECodiPais: TIntegerField
      FieldName = 'ECodiPais'
    end
    object QrEnderPCodiPais: TIntegerField
      FieldName = 'PCodiPais'
    end
  end
  object QrSelEnt: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT DISTINCT CodInfoEmit Entidade'
      'FROM nfecaba'
      'WHERE ide_tpAmb<>2 '
      'AND Empresa=:P0'
      'AND CodInfoEmit <>:P1'
      'AND DataFiscal BETWEEN :P2 AND :P3'
      'AND Status IN (100,101,102,110,301)'
      ''
      'UNION'
      ''
      'SELECT DISTINCT CodInfoDest Entidade'
      'FROM nfecaba'
      'WHERE ide_tpAmb<>2 '
      'AND Empresa=:P0'
      'AND CodInfoEmit <>:P1'
      'AND DataFiscal BETWEEN :P2 AND :P3'
      'AND Status IN (100,101,102,110,301)'
      ''
      'ORDER BY Entidade')
    Left = 272
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSelEntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrUniMedi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT nfei.prod_uTrib'
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.EMpresa'
      'WHERE  nfea.ide_tpAmb<>2 '
      'AND nfea.Status IN (100,101,102,110,301)'
      'AND nfea.Empresa=:P0'
      'AND nfea.DataFiscal BETWEEN :P1 AND :P2'
      'ORDER BY prod_uTrib')
    Left = 300
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrUniMediNoUnidMed: TWideStringField
      FieldName = 'NoUnidMed'
      Size = 6
    end
  end
  object QrForca: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfea.DataFiscal, nfea.CriAForca, '
      'nfei.FatID, nfei.FatNum, nfei.Empresa, '
      'nfei.nItem, nfei.GraGruX GGX_NfeI, '
      'nfei.prod_uTrib, smia.GraGruX GGX_SMIA'
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.EMpresa'
      'LEFT JOIN stqmovitsa smia ON smia.Tipo=nfei.FatID'
      '     AND smia.OriCodi=nfei.FatNum'
      '     AND smia.Empresa=nfei.EMpresa'
      '     AND smia.OriCtrl=nfei.MeuID'
      'WHERE nfea.CriAForca = 1'
      'AND smia.GraGruX <> 0'
      'AND (smia.GraGruX <> nfei.GraGruX'
      '     OR nfei.prod_uTrib = '#39#39
      '     )')
    Left = 328
    Top = 64
    object QrForcaDataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrForcaCriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
    object QrForcaFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrForcaFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrForcaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrForcanItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrForcaGGX_NfeI: TIntegerField
      FieldName = 'GGX_NfeI'
    end
    object QrForcaprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrForcaGGX_SMIA: TIntegerField
      FieldName = 'GGX_SMIA'
    end
  end
  object QrGGX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.UnidMed,'
      'unm.Sigla'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 356
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGGXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGGXSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
  end
  object QrCorrige001: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa, '
      'nfei.nItem, nfei.prod_cProd '
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.Empresa'
      'WHERE nfea.Empresa=nfea.CodInfoEmit'
      'AND GraGruX=0'
      'AND prod_cProd <> '#39#39)
    Left = 44
    Top = 484
    object QrCorrige001FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCorrige001FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCorrige001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCorrige001nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrCorrige001prod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
  end
  object QrCorrige1x3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa, '
      'nfei.nItem, nfei.Nivel1'
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.Empresa'
      'WHERE nfei.FatID IN (103,113)'
      'AND GraGruX=0'
      'AND Nivel1 < 0')
    Left = 44
    Top = 528
    object QrCorrige1x3FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCorrige1x3FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCorrige1x3Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCorrige1x3nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrCorrige1x3Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
  object QrReduzidos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT nfei.GraGruX'
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.EMpresa'
      'WHERE nfea.ide_tpAmb<>2 '
      'AND Status IN (100,101,102,110,301)'
      'AND nfea.Empresa=:P0'
      'AND nfea.DataFiscal BETWEEN :P1 AND :P2'
      'AND GraGruX <> 0'
      'ORDER BY GraGruX')
    Left = 468
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrReduzidosGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrSPEDEFDIcmsIpiErrs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM SPEDEFDErrs'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      ''
      ''
      '')
    Left = 44
    Top = 392
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrProduto2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.cGTIN_EAN , gg1.NCM,'
      'gg1.EX_TIPI, gg1.COD_LST, gg1.SPEDEFD_ALIQ_ICMS,'
      'unm.SIGLA, pgt.Tipo_Item'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle')
    Left = 192
    Top = 456
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProduto2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProduto2NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrProduto2cGTIN_EAN: TWideStringField
      FieldName = 'cGTIN_EAN'
      Size = 14
    end
    object QrProduto2NCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrProduto2EX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Size = 3
    end
    object QrProduto2COD_LST: TWideStringField
      FieldName = 'COD_LST'
      Size = 4
    end
    object QrProduto2SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrProduto2Tipo_Item: TSmallintField
      FieldName = 'Tipo_Item'
    end
    object QrProduto2SPEDEFD_ALIQ_ICMS: TFloatField
      FieldName = 'SPEDEFD_ALIQ_ICMS'
    end
    object QrProduto2prod_CEST: TIntegerField
      FieldName = 'prod_CEST'
    end
    object QrProduto2GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
  end
  object QrCabA_C: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'FatID, FatNum, Empresa, IDCtrl, Id,  ide_indPag, ide_mod, '
      'ide_serie, ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF,'
      'ide_tpAmb, ide_finNFe, emit_CNPJ, emit_CPF, '
      'dest_CNPJ, dest_CPF, '
      'ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST, '
      'ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete, '
      'ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII, '
      'ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS, '
      'ICMSTot_vOutro, ICMSTot_vNF, ISSQNtot_vServ, '
      'ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS, '
      'ISSQNtot_vCOFINS, RetTrib_vRetPIS, RetTrib_vRetCOFINS, '
      'RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF, '
      'RetTrib_vBCRetPrev, RetTrib_vRetPrev, ModFrete, '
      'Cobr_Fat_nFat, Cobr_Fat_vOrig, '
      'Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco, '
      'InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq, '
      'Compra_XNEmp, Compra_XPed, Compra_XCont, '
      ''
      'Status, infProt_Id, '
      'infProt_cStat, infCanc_cStat, '
      ''
      'ICMSRec_pRedBC, '
      'ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS, '
      'IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq, '
      'IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC, '
      'PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC, '
      'COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS, '
      
        'DataFiscal, SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_E' +
        'xpNat, '
      'SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum, '
      'SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais, '
      'SINTEGRA_ExpAverDta, CodInfoEmit, CodInfoDest, '
      'CriAForca, ide_hSaiEnt, ide_dhCont, '
      'emit_CRT, dest_email, '
      'Vagao, Balsa, CodInfoTrsp, '
      'OrdemServ, Situacao, Antigo, '
      'NFG_Serie, NF_ICMSAlq, NF_CFOP, '
      'Importado, NFG_SubSerie, NFG_ValIsen, '
      'NFG_NaoTrib, NFG_Outros, COD_MOD, COD_SIT, VL_ABAT_NT'
      'FROM nfecaba nfea '
      'WHERE ide_tpAmb<>2'
      'AND Status IN (100,101,102,110,301)'
      'AND  ('
      '  COD_MOD IN ('#39'01'#39','#39'1B'#39','#39'04'#39','#39'55'#39')'
      '  OR ide_mod IN (1,55)'
      ')'
      'AND Empresa=:P0'
      'AND DataFiscal BETWEEN :P1 AND :P2'
      '')
    Left = 120
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCabA_CFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCabA_CFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCabA_CEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCabA_CIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCabA_CId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrCabA_Cide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrCabA_Cide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrCabA_Cide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrCabA_Cide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrCabA_Cide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrCabA_Cide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrCabA_Cide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrCabA_Cide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrCabA_Cide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrCabA_Cemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrCabA_Cemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrCabA_Cdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrCabA_Cdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrCabA_CICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrCabA_CICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrCabA_CICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrCabA_CICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrCabA_CICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrCabA_CICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrCabA_CICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrCabA_CICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrCabA_CICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrCabA_CICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrCabA_CICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrCabA_CICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrCabA_CICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrCabA_CICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrCabA_CISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
    end
    object QrCabA_CISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
    end
    object QrCabA_CISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
    end
    object QrCabA_CISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
    end
    object QrCabA_CISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
    end
    object QrCabA_CRetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
    end
    object QrCabA_CRetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
    end
    object QrCabA_CRetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
    end
    object QrCabA_CRetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
    end
    object QrCabA_CRetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
    end
    object QrCabA_CRetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
    end
    object QrCabA_CRetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
    end
    object QrCabA_CModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrCabA_CCobr_Fat_nFat: TWideStringField
      FieldName = 'Cobr_Fat_nFat'
      Size = 60
    end
    object QrCabA_CCobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
    end
    object QrCabA_CCobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
    end
    object QrCabA_CCobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
    end
    object QrCabA_CInfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCabA_CInfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCabA_CExporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Size = 2
    end
    object QrCabA_CExporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Size = 60
    end
    object QrCabA_CCompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrCabA_CCompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrCabA_CCompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
    object QrCabA_CStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrCabA_CinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrCabA_CinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrCabA_CinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrCabA_CDataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrCabA_CSINTEGRA_ExpDeclNum: TWideStringField
      FieldName = 'SINTEGRA_ExpDeclNum'
      Size = 11
    end
    object QrCabA_CSINTEGRA_ExpDeclDta: TDateField
      FieldName = 'SINTEGRA_ExpDeclDta'
    end
    object QrCabA_CSINTEGRA_ExpNat: TWideStringField
      FieldName = 'SINTEGRA_ExpNat'
      Size = 1
    end
    object QrCabA_CSINTEGRA_ExpRegNum: TWideStringField
      FieldName = 'SINTEGRA_ExpRegNum'
      Size = 12
    end
    object QrCabA_CSINTEGRA_ExpRegDta: TDateField
      FieldName = 'SINTEGRA_ExpRegDta'
    end
    object QrCabA_CSINTEGRA_ExpConhNum: TWideStringField
      FieldName = 'SINTEGRA_ExpConhNum'
      Size = 16
    end
    object QrCabA_CSINTEGRA_ExpConhDta: TDateField
      FieldName = 'SINTEGRA_ExpConhDta'
    end
    object QrCabA_CSINTEGRA_ExpConhTip: TWideStringField
      FieldName = 'SINTEGRA_ExpConhTip'
      Size = 2
    end
    object QrCabA_CSINTEGRA_ExpPais: TWideStringField
      FieldName = 'SINTEGRA_ExpPais'
      Size = 4
    end
    object QrCabA_CSINTEGRA_ExpAverDta: TDateField
      FieldName = 'SINTEGRA_ExpAverDta'
    end
    object QrCabA_CCodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrCabA_CCodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object QrCabA_CCriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
    object QrCabA_Cide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrCabA_Cide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrCabA_Cemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
    object QrCabA_Cdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrCabA_CVagao: TWideStringField
      FieldName = 'Vagao'
    end
    object QrCabA_CBalsa: TWideStringField
      FieldName = 'Balsa'
    end
    object QrCabA_CCodInfoTrsp: TIntegerField
      FieldName = 'CodInfoTrsp'
    end
    object QrCabA_COrdemServ: TIntegerField
      FieldName = 'OrdemServ'
    end
    object QrCabA_CSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrCabA_CAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrCabA_CNFG_Serie: TWideStringField
      FieldName = 'NFG_Serie'
      Size = 3
    end
    object QrCabA_CNF_ICMSAlq: TFloatField
      FieldName = 'NF_ICMSAlq'
    end
    object QrCabA_CNF_CFOP: TWideStringField
      FieldName = 'NF_CFOP'
      Size = 4
    end
    object QrCabA_CImportado: TSmallintField
      FieldName = 'Importado'
    end
    object QrCabA_CNFG_SubSerie: TWideStringField
      FieldName = 'NFG_SubSerie'
      Size = 3
    end
    object QrCabA_CNFG_ValIsen: TFloatField
      FieldName = 'NFG_ValIsen'
    end
    object QrCabA_CNFG_NaoTrib: TFloatField
      FieldName = 'NFG_NaoTrib'
    end
    object QrCabA_CNFG_Outros: TFloatField
      FieldName = 'NFG_Outros'
    end
    object QrCabA_CCOD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrCabA_CCOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
    end
    object QrCabA_CVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
    end
  end
  object QrCorrige002: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodInfoEmit, CodInfoDest,'
      'ide_tpNF, DataFiscal,'
      'FatID, FatNum, Empresa, IDCtrl, ide_mod, '
      'ide_serie, ide_nNF, ide_dEmi, ide_dSaiEnt, '
      'ide_finNFe, emit_CNPJ, emit_CPF, Status, '
      'NFG_Serie, NFG_SubSerie, COD_MOD'
      'FROM nfecaba nfea '
      'WHERE ide_tpAmb<>2'
      'AND COD_SIT in (98,99)'
      'AND Empresa=:P0'
      'AND DataFiscal BETWEEN :P1 AND :P2')
    Left = 44
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCorrige002FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCorrige002FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCorrige002Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCorrige002IDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCorrige002ide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrCorrige002ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrCorrige002ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrCorrige002ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrCorrige002ide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrCorrige002ide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrCorrige002emit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrCorrige002emit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrCorrige002Status: TIntegerField
      FieldName = 'Status'
    end
    object QrCorrige002NFG_Serie: TWideStringField
      FieldName = 'NFG_Serie'
      Size = 3
    end
    object QrCorrige002NFG_SubSerie: TWideStringField
      FieldName = 'NFG_SubSerie'
      Size = 3
    end
    object QrCorrige002COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrCorrige002CodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrCorrige002ide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrCorrige002DataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrCorrige002CodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
  end
  object QrItsI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'ORDER BY nItem')
    Left = 120
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsIFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrItsIFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrItsIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrItsInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrItsIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrItsIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrItsIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrItsIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrItsIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrItsIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrItsIMeuID: TIntegerField
      FieldName = 'MeuID'
    end
    object QrItsIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrItsIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
  end
  object QrItsN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 188
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsNnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrItsNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrItsNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrItsNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrItsNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrItsNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrItsNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrItsNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrItsNCOD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrItsNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrItsNICMS_vICMSOp: TFloatField
      FieldName = 'ICMS_vICMSOp'
    end
    object QrItsNICMS_pDif: TFloatField
      FieldName = 'ICMS_pDif'
    end
    object QrItsNICMS_vICMSDif: TFloatField
      FieldName = 'ICMS_vICMSDif'
    end
    object QrItsNICMS_vICMSDeson: TFloatField
      FieldName = 'ICMS_vICMSDeson'
    end
    object QrItsNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrItsNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrItsNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrItsNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrItsNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrItsNICMS_CSOSN: TIntegerField
      FieldName = 'ICMS_CSOSN'
    end
    object QrItsNICMS_UFST: TWideStringField
      FieldName = 'ICMS_UFST'
      Size = 2
    end
    object QrItsNICMS_pBCOp: TFloatField
      FieldName = 'ICMS_pBCOp'
    end
    object QrItsNICMS_vBCSTRet: TFloatField
      FieldName = 'ICMS_vBCSTRet'
    end
    object QrItsNICMS_vICMSSTRet: TFloatField
      FieldName = 'ICMS_vICMSSTRet'
    end
    object QrItsNICMS_motDesICMS: TSmallintField
      FieldName = 'ICMS_motDesICMS'
    end
    object QrItsNICMS_pCredSN: TFloatField
      FieldName = 'ICMS_pCredSN'
    end
    object QrItsNICMS_vCredICMSSN: TFloatField
      FieldName = 'ICMS_vCredICMSSN'
    end
  end
  object QrStqMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Baixa '
      'FROM stqmovitsa'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1'
      'AND Empresa=:P2'
      'AND GraGruX=:P3'
      ''
      'UNION'
      ''
      'SELECT Baixa '
      'FROM stqmovitsb'
      'WHERE Tipo=:P4'
      'AND OriCodi=:P5'
      'AND Empresa=:P6'
      'AND GraGruX=:P7'
      '')
    Left = 704
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end>
    object QrStqMovItsBaixa: TSmallintField
      FieldName = 'Baixa'
      Required = True
    end
  end
  object QrItsO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 216
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsOIND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrItsOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrItsOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrItsOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
    end
    object QrItsOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrItsOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrItsOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrItsOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
  end
  object QrItsQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsq'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 244
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsQPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrItsQPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
    end
    object QrItsQPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
    end
    object QrItsQPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
    end
    object QrItsQPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
    end
    object QrItsQPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
    end
  end
  object QrItsS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitss'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 272
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsSCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrItsSCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrItsSCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
    end
    object QrItsSCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
    end
    object QrItsSCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
    end
    object QrItsSCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
    end
  end
  object QrGruI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT n.ICMS_Orig, n.ICMS_CST, i.ICMSRec_pAliq, '
      
        'SUM(i.ICMSRec_vBC) ICMSRec_vBC, SUM(i.ICMSRec_vICMS) ICMSRec_vIC' +
        'MS, '
      'i.prod_CFOP, SUM(i.prod_vProd + '
      'i.prod_vFrete + i.prod_vSeg + i.prod_vOutro) Valor,'
      'SUM(i.prod_vProd) prod_vProd, SUM(o.IPI_vIPI) IPI_vIPI    '
      'FROM nfeitsi i'
      'LEFT JOIN nfeitsn n ON n.FatID=i.FatID '
      '  AND n.FatNum=i.FatNum AND n.Empresa=i.Empresa'
      '  AND n.nItem=i.nItem'
      'LEFT JOIN nfeitso o ON o.FatID=i.FatID '
      '  AND o.FatNum=i.FatNum AND o.Empresa=i.Empresa'
      '  AND o.nItem=i.nItem'
      'WHERE i.FatID=:P0'
      'AND i.FatNum=:P1'
      'AND i.Empresa=:P2'
      'GROUP BY n.ICMS_Orig, n.ICMS_CST, i.ICMSRec_pAliq, i.prod_CFOP')
    Left = 120
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGruInCST: TSmallintField
      FieldName = 'nCST'
    end
    object QrGruInCFOP: TIntegerField
      FieldName = 'nCFOP'
    end
    object QrGruIValor: TFloatField
      FieldName = 'Valor'
    end
    object QrGruIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrGruIIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrGruIICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrGruIICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrGruIICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrGruIICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrGruIpICMS: TFloatField
      FieldName = 'pICMS'
    end
  end
  object QrCorrI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa, nfei.nItem, nfei.G' +
        'raGruX'
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.EMpresa'
      'WHERE prod_uTrib=""'
      'AND  nfea.Empresa=:P0'
      'AND nfea.DataFiscal BETWEEN :P1 AND :P2')
    Left = 384
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCorrIFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCorrIFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCorrIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCorrInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrCorrIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrErrCFOP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfi.FatID, nfi.FatNum, nfi.Empresa,  '
      'nfi.nItem, nfi.prod_CFOP, t002.* '
      'FROM nfeItsi nfi '
      'LEFT JOIN nfecaba nfa ON nfa.FatID=nfi.FatID '
      '     AND nfa.FatNum=nfi.FatNum '
      '     AND nfa.Empresa=nfi.Empresa '
      'LEFT JOIN tbspedefd002 t002 ON t002.Codigo=nfi.prod_CFOP '
      'WHERE nfa.DataFiscal BETWEEN "2010-01-01" AND "2010-01-31" '
      'AND t002.Codigo IS NULL ')
    Left = 412
    Top = 268
    object QrErrCFOPFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrErrCFOPFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrErrCFOPEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrErrCFOPnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrErrCFOPprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrErrCFOPCodTxt: TWideStringField
      FieldName = 'CodTxt'
      Required = True
    end
    object QrErrCFOPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrErrCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrErrCFOPDataIni: TDateField
      FieldName = 'DataIni'
      Required = True
    end
    object QrErrCFOPDataFim: TDateField
      FieldName = 'DataFim'
    end
  end
  object QrCFOP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM tbspedefd002'
      'WHERE Codigo="9999"')
    Left = 380
    Top = 312
  end
  object QrAllI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      
        'SUM(i.ICMSRec_vBC) ICMSRec_vBC, SUM(i.ICMSRec_vICMS) ICMSRec_vIC' +
        'MS, '
      'SUM(i.prod_vProd + '
      'i.prod_vFrete + i.prod_vSeg + i.prod_vOutro) Valor,'
      'SUM(i.prod_vProd) prod_vProd, SUM(o.IPI_vIPI) IPI_vIPI    '
      'FROM nfeitsi i'
      'LEFT JOIN nfeitsn n ON n.FatID=i.FatID '
      '  AND n.FatNum=i.FatNum AND n.Empresa=i.Empresa'
      '  AND n.nItem=i.nItem'
      'LEFT JOIN nfeitso o ON o.FatID=i.FatID '
      '  AND o.FatNum=i.FatNum AND o.Empresa=i.Empresa'
      '  AND o.nItem=i.nItem'
      'WHERE i.FatID=:P0'
      'AND i.FatNum=:P1'
      'AND i.Empresa=:P2')
    Left = 120
    Top = 392
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrAllIValor: TFloatField
      FieldName = 'Valor'
    end
    object QrAllIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrAllIIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrAllIICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrAllIICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrAllIICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrAllIICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
  end
  object QrBlocos: TMySQLQuery
    SQL.Strings = (
      'SELECT * '
      'FROM _spedefd_blcs_'
      'ORDER BY Ordem, Bloco'
      '')
    Left = 628
    Top = 264
    object QrBlocosBloco: TWideStringField
      FieldName = 'Bloco'
      Size = 1
    end
    object QrBlocosOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrBlocosAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object QrErrMod: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfa.DataFiscal, nfa.FatID, nfa.FatNum, nfa.Empresa, '
      
        'nfa.ide_mod, nfa.ide_serie, nfa.ide_nNF, nfa.ide_dEmi, nfa.ICMST' +
        'ot_vNF, '
      
        'IF(nfa.CodInfoEmit=:P0, nfa.CodInfoDest, nfa.CodInfoEmit) Tercei' +
        'ro,'
      
        'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, nfa.IDCtr' +
        'l'
      'FROM nfecaba nfa'
      'LEFT JOIN entidades ent ON ent.Codigo='
      '     IF(CodInfoEmit=:P1, CodInfoDest, CodInfoEmit)'
      'WHERE ide_tpAmb<>2'
      'AND Status IN (100,101,102,110,301)'
      'AND DataFiscal BETWEEN :P2 AND :P3'
      'AND EFD_EXP_REG='#39#39
      'ORDER BY DataFiscal, ide_dEmi, NO_TERCEIRO')
    Left = 468
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrErrModDataFiscal: TDateField
      FieldName = 'DataFiscal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrErrModFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrErrModFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrErrModEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrErrModide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrErrModide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrErrModide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrErrModide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrErrModICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrErrModTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrErrModNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrErrModIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object DsErrMod: TDataSource
    DataSet = QrErrMod
    Left = 492
    Top = 268
  end
  object QrEFD_E100: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E100BeforeClose
    AfterScroll = QrEFD_E100AfterScroll
    SQL.Strings = (
      'SELECT * FROM efd_e100'
      'WHERE ImporExpor=3'
      'AND AnoMes=201001'
      'AND Empresa=-11')
    Left = 420
    Top = 408
    object QrEFD_E100DT_INI: TDateField
      FieldName = 'DT_INI'
    end
    object QrEFD_E100DT_FIN: TDateField
      FieldName = 'DT_FIN'
    end
    object QrEFD_E100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E100AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E100Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E100LinArq: TIntegerField
      FieldName = 'LinArq'
    end
  end
  object QrEFD_E110: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E110BeforeClose
    AfterScroll = QrEFD_E110AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e110 e110 '
      'WHERE e110.ImporExpor=3 '
      'AND e110.Empresa=-11 '
      'AND e110.AnoMes=201001'
      'AND e110.LinArq>0')
    Left = 420
    Top = 456
    object QrEFD_E110ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E110AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E110Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E110LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E110E100: TIntegerField
      FieldName = 'E100'
    end
    object QrEFD_E110REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E110VL_TOT_DEBITOS: TFloatField
      FieldName = 'VL_TOT_DEBITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_AJ_DEBITOS: TFloatField
      FieldName = 'VL_AJ_DEBITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_TOT_AJ_DEBITOS: TFloatField
      FieldName = 'VL_TOT_AJ_DEBITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_TOT_CREDITOS: TFloatField
      FieldName = 'VL_TOT_CREDITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_ESTORNOS_CRED: TFloatField
      FieldName = 'VL_ESTORNOS_CRED'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_AJ_CREDITOS: TFloatField
      FieldName = 'VL_AJ_CREDITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_TOT_AJ_CREDITOS: TFloatField
      FieldName = 'VL_TOT_AJ_CREDITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_ESTORNOS_DEB: TFloatField
      FieldName = 'VL_ESTORNOS_DEB'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_SLD_CREDOR_ANT: TFloatField
      FieldName = 'VL_SLD_CREDOR_ANT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_SLD_APURADO: TFloatField
      FieldName = 'VL_SLD_APURADO'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_TOT_DED: TFloatField
      FieldName = 'VL_TOT_DED'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_ICMS_RECOLHER: TFloatField
      FieldName = 'VL_ICMS_RECOLHER'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_SLD_CREDOR_TRANSPORTAR: TFloatField
      FieldName = 'VL_SLD_CREDOR_TRANSPORTAR'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110DEB_ESP: TFloatField
      FieldName = 'DEB_ESP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object QrEFD_E111: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E111BeforeClose
    AfterScroll = QrEFD_E111AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e111 e111'
      'WHERE e111.ImporExpor=3'
      'AND e111.Empresa=-11'
      'AND e111.AnoMes=201001'
      'AND e111.LinArq>0'
      '')
    Left = 780
    Top = 244
    object QrEFD_E111ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E111AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E111Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E111LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E111E110: TIntegerField
      FieldName = 'E110'
    end
    object QrEFD_E111REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E111COD_AJ_APUR: TWideStringField
      FieldName = 'COD_AJ_APUR'
      Size = 8
    end
    object QrEFD_E111DESCR_COMPL_AJ: TWideStringField
      FieldName = 'DESCR_COMPL_AJ'
      Size = 255
    end
    object QrEFD_E111VL_AJ_APUR: TFloatField
      FieldName = 'VL_AJ_APUR'
    end
  end
  object QrEFD_E115: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e115 e115'
      'WHERE e115.ImporExpor=3'
      'AND e115.Empresa=-11'
      'AND e115.AnoMes=201001'
      'AND e115.LinArq>0'
      '')
    Left = 864
    Top = 244
    object QrEFD_E115ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E115AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E115Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E115LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E115E110: TIntegerField
      FieldName = 'E110'
    end
    object QrEFD_E115REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E115COD_INF_ADIC: TWideStringField
      FieldName = 'COD_INF_ADIC'
      Size = 8
    end
    object QrEFD_E115VL_INF_ADIC: TFloatField
      FieldName = 'VL_INF_ADIC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E115DESCR_COMPL_AJ: TWideStringField
      FieldName = 'DESCR_COMPL_AJ'
      Size = 255
    end
  end
  object QrEFD_E116: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e116 e116'
      'WHERE e116.ImporExpor=3'
      'AND e116.Empresa=-11'
      'AND e116.AnoMes=201001'
      'AND e116.LinArq>0'
      '')
    Left = 892
    Top = 244
    object QrEFD_E116ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E116AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E116Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E116LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E116E110: TIntegerField
      FieldName = 'E110'
    end
    object QrEFD_E116REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E116COD_OR: TWideStringField
      FieldName = 'COD_OR'
      Size = 3
    end
    object QrEFD_E116VL_OR: TFloatField
      FieldName = 'VL_OR'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEFD_E116DT_VCTO: TDateField
      FieldName = 'DT_VCTO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrEFD_E116COD_REC: TWideStringField
      FieldName = 'COD_REC'
      Size = 255
    end
    object QrEFD_E116NUM_PROC: TWideStringField
      FieldName = 'NUM_PROC'
      Size = 15
    end
    object QrEFD_E116IND_PROC: TWideStringField
      FieldName = 'IND_PROC'
      Size = 1
    end
    object QrEFD_E116PROC: TWideStringField
      FieldName = 'PROC'
      Size = 255
    end
    object QrEFD_E116TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
    object QrEFD_E116MES_REF: TDateField
      FieldName = 'MES_REF'
    end
  end
  object QrEFD_E112: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e112 e112'
      'WHERE e112.ImporExpor=3'
      'AND e112.Empresa=-11'
      'AND e112.AnoMes=201001'
      'AND e112.LinArq>0'
      '')
    Left = 808
    Top = 244
    object QrEFD_E112ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E112AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E112Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E112LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E112E111: TIntegerField
      FieldName = 'E111'
    end
    object QrEFD_E112REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E112NUM_DA: TWideStringField
      FieldName = 'NUM_DA'
      Size = 255
    end
    object QrEFD_E112NUM_PROC: TWideStringField
      FieldName = 'NUM_PROC'
      Size = 15
    end
    object QrEFD_E112IND_PROC: TWideStringField
      FieldName = 'IND_PROC'
      Size = 1
    end
    object QrEFD_E112PROC: TWideStringField
      FieldName = 'PROC'
      Size = 255
    end
    object QrEFD_E112TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
  end
  object QrEFD_E113: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e113 e113'
      'WHERE e113.ImporExpor=3'
      'AND e113.Empresa=-11'
      'AND e113.AnoMes=201001'
      'AND e113.LinArq>0'
      '')
    Left = 836
    Top = 244
    object QrEFD_E113ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E113AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E113Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E113LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E113E111: TIntegerField
      FieldName = 'E111'
    end
    object QrEFD_E113REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E113COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_E113COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrEFD_E113SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEFD_E113SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEFD_E113NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrEFD_E113DT_DOC: TDateField
      FieldName = 'DT_DOC'
    end
    object QrEFD_E113COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_E113VL_AJ_ITEM: TFloatField
      FieldName = 'VL_AJ_ITEM'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object QrEFD_D100: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E100BeforeClose
    SQL.Strings = (
      'SELECT * FROM efd_d100'
      'WHERE ImporExpor<>2'
      'AND AnoMes=201001'
      'AND Empresa=-11')
    Left = 340
    Top = 408
    object QrEFD_D100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_D100AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_D100Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_D100LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_D100REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_D100IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Size = 1
    end
    object QrEFD_D100IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Size = 1
    end
    object QrEFD_D100COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_D100COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrEFD_D100COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Size = 2
    end
    object QrEFD_D100SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEFD_D100SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEFD_D100NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrEFD_D100CHV_CTE: TWideStringField
      FieldName = 'CHV_CTE'
      Size = 44
    end
    object QrEFD_D100DT_DOC: TDateField
      FieldName = 'DT_DOC'
    end
    object QrEFD_D100DT_A_P: TDateField
      FieldName = 'DT_A_P'
    end
    object QrEFD_D100TP_CTE: TSmallintField
      FieldName = 'TP_CTE'
      Required = True
    end
    object QrEFD_D100CHV_CTE_REF: TWideStringField
      FieldName = 'CHV_CTE_REF'
      Size = 44
    end
    object QrEFD_D100VL_DOC: TFloatField
      FieldName = 'VL_DOC'
    end
    object QrEFD_D100VL_DESC: TFloatField
      FieldName = 'VL_DESC'
    end
    object QrEFD_D100IND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEFD_D100VL_SERV: TFloatField
      FieldName = 'VL_SERV'
    end
    object QrEFD_D100VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrEFD_D100VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrEFD_D100VL_NT: TFloatField
      FieldName = 'VL_NT'
    end
    object QrEFD_D100COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrEFD_D100COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEFD_D100Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEFD_D100Importado: TSmallintField
      FieldName = 'Importado'
    end
    object QrEFD_D100Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_D100DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_D100DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_D100UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_D100UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_D100AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_D100Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_D100CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrEFD_D100CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrEFD_D100ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrEFD_D100VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
  end
  object QrBlcs: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM spedefdblcs'
      'WHERE Implementd <> 0'
      'ORDER BY Ordem, Registro')
    Left = 356
    Top = 92
    object QrBlcsBloco: TWideStringField
      FieldName = 'Bloco'
      Size = 1
    end
    object QrBlcsRegistro: TWideStringField
      FieldName = 'Registro'
      Size = 4
    end
    object QrBlcsNivel: TSmallintField
      FieldName = 'Nivel'
    end
    object QrBlcsRegisPai: TWideStringField
      FieldName = 'RegisPai'
      Size = 4
    end
    object QrBlcsOcorAciNiv: TIntegerField
      FieldName = 'OcorAciNiv'
    end
    object QrBlcsOcorEstNiv: TIntegerField
      FieldName = 'OcorEstNiv'
    end
    object QrBlcsImplementd: TSmallintField
      FieldName = 'Implementd'
    end
    object QrBlcsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrBlcsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrNiv2: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Registro '
      'FROM _spedefd_blcs_'
      'WHERE Nivel=2'
      'AND Implementd <> 0'
      'AND ('
      '  Bloco = "C"'
      '  OR'
      '  Bloco = "D"'
      ')')
    Left = 384
    Top = 92
    object QrNiv2Registro: TWideStringField
      FieldName = 'Registro'
      Size = 4
    end
  end
  object QrEFD_C500: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) '
      'NO_TERC, c500.*'
      'FROM efd_c500 c500 '
      'LEFT JOIN entidades ent ON ent.Codigo=c500.Terceiro'
      'WHERE c500.ImporExpor=3 '
      'AND c500.Empresa=-11 '
      'AND c500.AnoMes=201001'
      '')
    Left = 264
    Top = 456
    object QrEFD_C500NO_TERC: TWideStringField
      FieldName = 'NO_TERC'
      Size = 100
    end
    object QrEFD_C500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_C500AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_C500Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_C500LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_C500REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_C500IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Size = 1
    end
    object QrEFD_C500IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Size = 1
    end
    object QrEFD_C500COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_C500COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrEFD_C500COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Size = 2
    end
    object QrEFD_C500SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEFD_C500SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEFD_C500COD_CONS: TWideStringField
      FieldName = 'COD_CONS'
      Size = 2
    end
    object QrEFD_C500NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrEFD_C500DT_DOC: TDateField
      FieldName = 'DT_DOC'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_C500DT_E_S: TDateField
      FieldName = 'DT_E_S'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_C500VL_DOC: TFloatField
      FieldName = 'VL_DOC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_DESC: TFloatField
      FieldName = 'VL_DESC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_FORN: TFloatField
      FieldName = 'VL_FORN'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_SERV_NT: TFloatField
      FieldName = 'VL_SERV_NT'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_TERC: TFloatField
      FieldName = 'VL_TERC'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_DA: TFloatField
      FieldName = 'VL_DA'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrEFD_C500VL_PIS: TFloatField
      FieldName = 'VL_PIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500TP_LIGACAO: TWideStringField
      FieldName = 'TP_LIGACAO'
      Size = 1
    end
    object QrEFD_C500COD_GRUPO_TENSAO: TWideStringField
      FieldName = 'COD_GRUPO_TENSAO'
      Size = 2
    end
    object QrEFD_C500Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEFD_C500Importado: TSmallintField
      FieldName = 'Importado'
    end
    object QrEFD_C500CST_ICMS: TWideStringField
      FieldName = 'CST_ICMS'
      Size = 3
    end
    object QrEFD_C500CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrEFD_C500ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_C500VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object QrEFD_D500: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E100BeforeClose
    SQL.Strings = (
      'SELECT * FROM efd_d500'
      'WHERE ImporExpor<>2'
      'AND AnoMes=201001'
      'AND Empresa=-11')
    Left = 340
    Top = 456
    object QrEFD_D500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_D500AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_D500Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_D500LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_D500REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_D500IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Size = 1
    end
    object QrEFD_D500IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Size = 1
    end
    object QrEFD_D500COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_D500COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrEFD_D500COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Size = 2
    end
    object QrEFD_D500SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEFD_D500SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEFD_D500NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrEFD_D500DT_DOC: TDateField
      FieldName = 'DT_DOC'
    end
    object QrEFD_D500DT_A_P: TDateField
      FieldName = 'DT_A_P'
    end
    object QrEFD_D500VL_DOC: TFloatField
      FieldName = 'VL_DOC'
    end
    object QrEFD_D500VL_DESC: TFloatField
      FieldName = 'VL_DESC'
    end
    object QrEFD_D500VL_SERV: TFloatField
      FieldName = 'VL_SERV'
    end
    object QrEFD_D500VL_SERV_NT: TFloatField
      FieldName = 'VL_SERV_NT'
    end
    object QrEFD_D500VL_TERC: TFloatField
      FieldName = 'VL_TERC'
    end
    object QrEFD_D500VL_DA: TFloatField
      FieldName = 'VL_DA'
    end
    object QrEFD_D500VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrEFD_D500VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrEFD_D500COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrEFD_D500VL_PIS: TFloatField
      FieldName = 'VL_PIS'
    end
    object QrEFD_D500VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
    end
    object QrEFD_D500COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEFD_D500TP_ASSINANTE: TWideStringField
      FieldName = 'TP_ASSINANTE'
      Size = 1
    end
    object QrEFD_D500Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrEFD_D500Importado: TSmallintField
      FieldName = 'Importado'
    end
    object QrEFD_D500CST_ICMS: TWideStringField
      FieldName = 'CST_ICMS'
      Size = 3
    end
    object QrEFD_D500CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrEFD_D500ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrEFD_D500VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
  end
  object QrEFD_E500: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E100BeforeClose
    AfterScroll = QrEFD_E100AfterScroll
    SQL.Strings = (
      'SELECT * FROM efd_e100'
      'WHERE ImporExpor=3'
      'AND AnoMes=201001'
      'AND Empresa=-11')
    Left = 496
    Top = 408
    object QrEFD_E500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E500AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E500Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E500LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E500IND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrEFD_E500DT_INI: TDateField
      FieldName = 'DT_INI'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_E500DT_FIN: TDateField
      FieldName = 'DT_FIN'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object QrEFD_E520: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E520BeforeClose
    AfterScroll = QrEFD_E520AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e520 E520 ')
    Left = 496
    Top = 456
    object QrEFD_E520ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E520AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E520Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E520LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E520E500: TIntegerField
      FieldName = 'E500'
    end
    object QrEFD_E520REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E520VL_SD_ANT_IPI: TFloatField
      FieldName = 'VL_SD_ANT_IPI'
    end
    object QrEFD_E520VL_DEB_IPI: TFloatField
      FieldName = 'VL_DEB_IPI'
    end
    object QrEFD_E520VL_CRED_IPI: TFloatField
      FieldName = 'VL_CRED_IPI'
    end
    object QrEFD_E520VL_OD_IPI: TFloatField
      FieldName = 'VL_OD_IPI'
    end
    object QrEFD_E520VL_OC_IPI: TFloatField
      FieldName = 'VL_OC_IPI'
    end
    object QrEFD_E520VL_SC_IPI: TFloatField
      FieldName = 'VL_SC_IPI'
    end
    object QrEFD_E520VL_SD_IPI: TFloatField
      FieldName = 'VL_SD_IPI'
    end
  end
  object QrEFD_E530: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e520 E520 ')
    Left = 496
    Top = 504
    object QrEFD_E530ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E530AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E530Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E530LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E530E520: TIntegerField
      FieldName = 'E520'
    end
    object QrEFD_E530REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E530IND_AJ: TWideStringField
      FieldName = 'IND_AJ'
      Size = 1
    end
    object QrEFD_E530VL_AJ: TFloatField
      FieldName = 'VL_AJ'
    end
    object QrEFD_E530COD_AJ: TWideStringField
      FieldName = 'COD_AJ'
      Size = 3
    end
    object QrEFD_E530IND_DOC: TWideStringField
      FieldName = 'IND_DOC'
      Size = 1
    end
    object QrEFD_E530NUM_DOC: TWideStringField
      FieldName = 'NUM_DOC'
      Size = 255
    end
    object QrEFD_E530DESCR_AJ: TWideStringField
      FieldName = 'DESCR_AJ'
      Size = 255
    end
  end
  object QrEFD_H005: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrEFD_H005AfterScroll
    SQL.Strings = (
      'SELECT * FROM efd_h005')
    Left = 584
    Top = 408
    object QrEFD_H005ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_H005AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_H005Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_H005LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_H005REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_H005DT_INV: TDateField
      FieldName = 'DT_INV'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_H005VL_INV: TFloatField
      FieldName = 'VL_INV'
    end
    object QrEFD_H005MOT_INV: TWideStringField
      FieldName = 'MOT_INV'
      Size = 2
    end
    object QrEFD_H005Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_H005DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_H005DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_H005UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_H005UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_H005AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_H005Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrEFD_H010: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_H010BeforeClose
    AfterScroll = QrEFD_H010AfterScroll
    SQL.Strings = (
      'SELECT h010.* '
      'FROM efd_h010 h010'
      'WHERE h010.ImporExpor=3'
      'AND h010.Empresa=-11'
      'AND h010.AnoMes=201401'
      'AND h010.H005=1'
      '')
    Left = 584
    Top = 456
    object QrEFD_H010ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_H010AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_H010Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_H010H005: TIntegerField
      FieldName = 'H005'
    end
    object QrEFD_H010LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_H010REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_H010COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_H010UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrEFD_H010QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrEFD_H010VL_UNIT: TFloatField
      FieldName = 'VL_UNIT'
      DisplayFormat = '#,###,###,##0.000000'
    end
    object QrEFD_H010VL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_H010IND_PROP: TWideStringField
      FieldName = 'IND_PROP'
      Size = 1
    end
    object QrEFD_H010COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_H010TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
    object QrEFD_H010COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEFD_H010VL_ITEM_IR: TFloatField
      FieldName = 'VL_ITEM_IR'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_H010Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_H010DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_H010DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_H010UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_H010UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_H010AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_H010Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrEFD_H020: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM efd_h020')
    Left = 584
    Top = 504
    object QrEFD_H020ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_H020AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_H020Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_H020H010: TIntegerField
      FieldName = 'H010'
    end
    object QrEFD_H020LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_H020REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_H020CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrEFD_H020BC_ICMS: TFloatField
      FieldName = 'BC_ICMS'
    end
    object QrEFD_H020VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrEFD_H020Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_H020DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_H020DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_H020UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_H020UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_H020AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_H020Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrProduto: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.cGTIN_EAN , gg1.NCM,'
      'gg1.EX_TIPI, gg1.COD_LST, gg1.SPEDEFD_ALIQ_ICMS,'
      'unm.SIGLA, pgt.Tipo_Item'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle')
    Left = 496
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdutoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProdutoNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrProdutocGTIN_EAN: TWideStringField
      FieldName = 'cGTIN_EAN'
      Size = 14
    end
    object QrProdutoNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrProdutoEX_TIPI: TWideStringField
      FieldName = 'EX_TIPI'
      Size = 3
    end
    object QrProdutoCOD_LST: TWideStringField
      FieldName = 'COD_LST'
      Size = 4
    end
    object QrProdutoSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrProdutoTipo_Item: TSmallintField
      FieldName = 'Tipo_Item'
    end
    object QrProdutoSPEDEFD_ALIQ_ICMS: TFloatField
      FieldName = 'SPEDEFD_ALIQ_ICMS'
    end
  end
  object QrEFD_K100: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K100BeforeClose
    AfterScroll = QrEFD_K100AfterScroll
    SQL.Strings = (
      'SELECT h010.* '
      'FROM efd_h010 h010'
      'WHERE h010.ImporExpor=3'
      'AND h010.Empresa=-11'
      'AND h010.AnoMes=201401'
      'AND h010.H005=1'
      '')
    Left = 684
    Top = 408
    object QrEFD_K100DT_INI: TDateField
      FieldName = 'DT_INI'
    end
    object QrEFD_K100DT_FIN: TDateField
      FieldName = 'DT_FIN'
    end
    object QrEFD_K100PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
  end
  object QrEFD_K200: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM efdicmsipik200 '
      'WHERE ImporExpor=3'
      'AND AnoMes=201812'
      'AND Empresa=-11'
      'AND PeriApu=1')
    Left = 684
    Top = 456
    object QrEFD_K200ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K200AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K200Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K200PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrEFD_K200KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrEFD_K200KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrEFD_K200KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrEFD_K200KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrEFD_K200KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrEFD_K200KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrEFD_K200IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrEFD_K200DT_EST: TDateField
      FieldName = 'DT_EST'
    end
    object QrEFD_K200COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K200QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrEFD_K200IND_EST: TWideStringField
      FieldName = 'IND_EST'
      Size = 1
    end
    object QrEFD_K200COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_K200GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K200Grandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrEFD_K200Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEFD_K200AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEFD_K200PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrEFD_K200Entidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrEFD_K200Tipo_Item: TIntegerField
      FieldName = 'Tipo_Item'
    end
    object QrEFD_K200Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K200DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K200DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K200UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K200UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K200AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K200Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_K200ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K200AWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrEFD_K200AWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
  end
  object QrEFD_K220: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'CONCAT(gg1o.Nome, '
      'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), '
      'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) '
      'NO_PRD_TAM_COR_ORI, '
      'CONCAT(gg1d.Nome, '
      'IF(gtid.PrintTam=0, "", CONCAT(" ", gtid.Nome)), '
      'IF(gccd.PrintCor=0,"", CONCAT(" ", gccd.Nome))) '
      'NO_PRD_TAM_COR_DEST, '
      'K220.* '
      'FROM efd_K220 K220 '
      'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K220.COD_ITEM_ORI '
      'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC '
      'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad '
      'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI '
      'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 '
      'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed '
      ' '
      'LEFT JOIN gragrux    ggxd ON ggxd.Controle=K220.COD_ITEM_DEST '
      'LEFT JOIN gragruc    ggcd ON ggcd.Controle=ggxd.GraGruC '
      'LEFT JOIN gracorcad  gccd ON gccd.Codigo=ggcd.GraCorCad '
      'LEFT JOIN gratamits  gtid ON gtid.Controle=ggxd.GraTamI '
      'LEFT JOIN gragru1    gg1d ON gg1d.Nivel1=ggxd.GraGru1 '
      'LEFT JOIN unidmed    unmd ON unmd.Codigo=gg1d.UnidMed '
      'WHERE k220.ImporExpor=3'
      'AND k220.Empresa=-11'
      'AND k220.AnoMes=201601'
      'AND k220.K100=1'
      'ORDER BY k220.DT_MOV, COD_ITEM_ORI, COD_ITEM_DEST ')
    Left = 684
    Top = 504
    object QrEFD_K220DT_MOV: TDateField
      FieldName = 'DT_MOV'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_K220COD_ITEM_ORI: TWideStringField
      FieldName = 'COD_ITEM_ORI'
      Size = 60
    end
    object QrEFD_K220COD_ITEM_DEST: TWideStringField
      FieldName = 'COD_ITEM_DEST'
      Size = 60
    end
    object QrEFD_K220QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrEFD_K220ID_SEK: TIntegerField
      FieldName = 'ID_SEK'
    end
  end
  object QrEFD_K230: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K230BeforeClose
    AfterScroll = QrEFD_K230AfterScroll
    Left = 752
    Top = 408
    object QrEFD_K230ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K230AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K230Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K230LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K230REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K230DT_INI_OP: TDateField
      FieldName = 'DT_INI_OP'
    end
    object QrEFD_K230K100: TIntegerField
      FieldName = 'K100'
    end
    object QrEFD_K230DT_FIN_OP: TDateField
      FieldName = 'DT_FIN_OP'
    end
    object QrEFD_K230COD_DOC_OP: TWideStringField
      FieldName = 'COD_DOC_OP'
      Size = 30
    end
    object QrEFD_K230COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K230QTD_ENC: TFloatField
      FieldName = 'QTD_ENC'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrEFD_K230Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K230DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K230DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K230UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K230UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K230AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K230Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_K230ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K230Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K230MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K230MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
  end
  object QrEFD_K235: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR,'
      'k235.*'
      'FROM efd_k235 k235'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k235.COD_ITEM'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE k235.ImporExpor=3'
      'AND k235.Empresa=-11'
      'AND k235.AnoMes=201601'
      'AND k235.K230=1')
    Left = 752
    Top = 456
    object QrEFD_K235DT_SAIDA: TDateField
      FieldName = 'DT_SAIDA'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_K235COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K235QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_K235COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrEFD_K235ID_SEK: TIntegerField
      FieldName = 'ID_SEK'
    end
  end
  object QrEFD_K250: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K250BeforeClose
    AfterScroll = QrEFD_K250AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'IF(k250.DT_PROD = 0, "", '
      'DATE_FORMAT(k250.DT_PROD, "%d/%m/%Y")) DT_PROD_Txt,'
      'K250.* '
      'FROM efd_K250 k250 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K250.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k250.ImporExpor=3'
      'AND k250.Empresa=-11'
      'AND k250.AnoMes=201601'
      'AND k250.K100=1'
      'ORDER BY DT_PROD, COD_DOC_OP, COD_ITEM ')
    Left = 752
    Top = 504
    object QrEFD_K250DT_PROD: TDateField
      FieldName = 'DT_PROD'
    end
    object QrEFD_K250COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K250QTD: TFloatField
      FieldName = 'QTD'
    end
  end
  object QrEFD_K255: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR,'
      'k235.*'
      'FROM efd_k235 k235'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k235.COD_ITEM'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE k235.ImporExpor=3'
      'AND k235.Empresa=-11'
      'AND k235.AnoMes=201601'
      'AND k235.K230=1')
    Left = 752
    Top = 552
    object QrEFD_K255DT_CONS: TDateField
      FieldName = 'DT_CONS'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_K255COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K255QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_K255COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrEFD_K255ID_SEK: TIntegerField
      FieldName = 'ID_SEK'
    end
  end
  object QrGraGru1Cons: TMySQLQuery
    Database = Dmod.MyDB
    Left = 844
    Top = 408
    object QrGraGru1ConsQtd_Comp: TFloatField
      FieldName = 'Qtd_Comp'
    end
    object QrGraGru1ConsPerda: TFloatField
      FieldName = 'Perda'
    end
    object QrGraGru1ConsGGX_CONSU: TIntegerField
      FieldName = 'GGX_CONSU'
    end
  end
  object QrPsq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 844
    Top = 456
  end
  object QrEFD_1010: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_1010')
    Left = 924
    Top = 408
    object QrEFD_1010ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_1010AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_1010Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_1010LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_1010REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_1010IND_EXP: TWideStringField
      FieldName = 'IND_EXP'
      Size = 1
    end
    object QrEFD_1010IND_CCRF: TWideStringField
      FieldName = 'IND_CCRF'
      Size = 1
    end
    object QrEFD_1010IND_COMB: TWideStringField
      FieldName = 'IND_COMB'
      Size = 1
    end
    object QrEFD_1010IND_USINA: TWideStringField
      FieldName = 'IND_USINA'
      Size = 1
    end
    object QrEFD_1010IND_VA: TWideStringField
      FieldName = 'IND_VA'
      Size = 1
    end
    object QrEFD_1010IND_EE: TWideStringField
      FieldName = 'IND_EE'
      Size = 1
    end
    object QrEFD_1010IND_CART: TWideStringField
      FieldName = 'IND_CART'
      Size = 1
    end
    object QrEFD_1010IND_FORM: TWideStringField
      FieldName = 'IND_FORM'
      Size = 1
    end
    object QrEFD_1010IND_AER: TWideStringField
      FieldName = 'IND_AER'
      Size = 1
    end
    object QrEFD_1010Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_1010DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_1010DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_1010UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_1010UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_1010AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_1010Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrEnt: TMySQLQuery
    Database = Dmod.MyDB
    Left = 220
    Top = 356
  end
  object QrNFeEFD_C170: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM nfeefd_c170 '
      'WHERE FatID=51 '
      'AND FatNum=3 '
      'AND Empresa=-11 '
      'AND nItem=1')
    Left = 924
    Top = 456
    object QrNFeEFD_C170FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEFD_C170FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEFD_C170Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEFD_C170nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeEFD_C170GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNFeEFD_C170COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrNFeEFD_C170DESCR_COMPL: TWideStringField
      FieldName = 'DESCR_COMPL'
      Size = 255
    end
    object QrNFeEFD_C170QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrNFeEFD_C170UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrNFeEFD_C170VL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
    end
    object QrNFeEFD_C170VL_DESC: TFloatField
      FieldName = 'VL_DESC'
    end
    object QrNFeEFD_C170IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
    object QrNFeEFD_C170CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrNFeEFD_C170CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrNFeEFD_C170COD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrNFeEFD_C170VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrNFeEFD_C170ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrNFeEFD_C170VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrNFeEFD_C170VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
    end
    object QrNFeEFD_C170ALIQ_ST: TFloatField
      FieldName = 'ALIQ_ST'
    end
    object QrNFeEFD_C170VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
    end
    object QrNFeEFD_C170IND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrNFeEFD_C170CST_IPI: TWideStringField
      FieldName = 'CST_IPI'
      Size = 2
    end
    object QrNFeEFD_C170COD_ENQ: TWideStringField
      FieldName = 'COD_ENQ'
      Size = 3
    end
    object QrNFeEFD_C170VL_BC_IPI: TFloatField
      FieldName = 'VL_BC_IPI'
    end
    object QrNFeEFD_C170ALIQ_IPI: TFloatField
      FieldName = 'ALIQ_IPI'
    end
    object QrNFeEFD_C170VL_IPI: TFloatField
      FieldName = 'VL_IPI'
    end
    object QrNFeEFD_C170CST_PIS: TSmallintField
      FieldName = 'CST_PIS'
    end
    object QrNFeEFD_C170VL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
    end
    object QrNFeEFD_C170ALIQ_PIS_p: TFloatField
      FieldName = 'ALIQ_PIS_p'
    end
    object QrNFeEFD_C170QUANT_BC_PIS: TFloatField
      FieldName = 'QUANT_BC_PIS'
    end
    object QrNFeEFD_C170ALIQ_PIS_r: TFloatField
      FieldName = 'ALIQ_PIS_r'
    end
    object QrNFeEFD_C170VL_PIS: TFloatField
      FieldName = 'VL_PIS'
    end
    object QrNFeEFD_C170CST_COFINS: TSmallintField
      FieldName = 'CST_COFINS'
    end
    object QrNFeEFD_C170VL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
    end
    object QrNFeEFD_C170ALIQ_COFINS_p: TFloatField
      FieldName = 'ALIQ_COFINS_p'
    end
    object QrNFeEFD_C170QUANT_BC_COFINS: TFloatField
      FieldName = 'QUANT_BC_COFINS'
    end
    object QrNFeEFD_C170ALIQ_COFINS_r: TFloatField
      FieldName = 'ALIQ_COFINS_r'
    end
    object QrNFeEFD_C170VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
    end
    object QrNFeEFD_C170COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrNFeEFD_C170Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFeEFD_C170DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFeEFD_C170DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFeEFD_C170UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFeEFD_C170UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFeEFD_C170AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFeEFD_C170Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM unidmed'
      'WHERE UPPER(SIGLA)=:P0'
      'ORDER BY Codigo'
      '')
    Left = 384
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object Qr0220: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT med.Sigla, c170.UNID, SUM(itsi.prod_qTrib) prod_qTrib,'
      'SUM(c170.QTD) QTD,  '
      'SUM(itsi.prod_qTrib) / SUM(c170.QTD) FATOR'
      'FROM nfeefd_c170 c170'
      'LEFT JOIN nfecaba caba ON'
      '  caba.FatID = c170.FatID'
      '  AND caba.FatNum=c170.FatNum'
      '  AND caba.Empresa=c170.Empresa'
      'LEFT JOIN nfeitsi itsi ON'
      '  itsi.FatID = c170.FatID'
      '  AND itsi.FatNum=c170.FatNum'
      '  AND itsi.Empresa=c170.Empresa'
      '  AND itsi.nItem=c170.nItem'
      'LEFT JOIN gragrux ggx ON ggx.Controle=c170.GraGrux'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE caba.DataFiscal BETWEEN "2017-01-01" AND "2017-01-31"'
      'AND UPPER(med.Sigla) <> UPPER(c170.UNID)'
      'AND c170.GraGruX=4'
      'GROUP BY med.Sigla, c170.UNID')
    Left = 844
    Top = 504
    object Qr0220Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object Qr0220UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object Qr0220prod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object Qr0220QTD: TFloatField
      FieldName = 'QTD'
    end
    object Qr0220FATOR: TFloatField
      FieldName = 'FATOR'
    end
  end
end
