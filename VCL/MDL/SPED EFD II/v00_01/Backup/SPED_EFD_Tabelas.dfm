object FmSPED_EFD_Tabelas: TFmSPED_EFD_Tabelas
  Left = 339
  Top = 185
  Caption = 'SPE-D_EFD-000 :: Download de Tabelas SPED-EFD'
  ClientHeight = 570
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 408
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 0
      Top = 0
      Width = 784
      Height = 16
      Align = alTop
      Alignment = taCenter
      Caption = 'Lista de erros no download e importa'#231#227'o:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 265
    end
    object Panel16: TPanel
      Left = 0
      Top = 307
      Width = 784
      Height = 101
      Align = alBottom
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 8
        Top = 4
        Width = 41
        Height = 13
        Caption = '..........'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LaAviso2: TLabel
        Left = 8
        Top = 44
        Width = 41
        Height = 13
        Caption = '..........'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PB1: TProgressBar
        Left = 8
        Top = 20
        Width = 760
        Height = 17
        TabOrder = 0
      end
      object StatusBar: TStatusBar
        Left = 0
        Top = 82
        Width = 784
        Height = 19
        Panels = <
          item
            Text = ' Posi'#231#227'o do cursor no texto gerado:'
            Width = 192
          end
          item
            Width = 100
          end
          item
            Text = ' Arquivo salvo:'
            Width = 96
          end
          item
            Width = 50
          end>
      end
      object PB2: TProgressBar
        Left = 8
        Top = 60
        Width = 760
        Height = 17
        TabOrder = 2
      end
    end
    object Memo1: TMemo
      Left = 0
      Top = 16
      Width = 599
      Height = 291
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object Panel3: TPanel
      Left = 599
      Top = 16
      Width = 185
      Height = 291
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object BtBacen_Pais: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 169
        Height = 40
        Caption = '&Atualiza PAISES (BACEN )'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtBacen_PaisClick
      end
      object BtDTB_Munici: TBitBtn
        Tag = 14
        Left = 8
        Top = 48
        Width = 169
        Height = 40
        Caption = '&Atualiza MUNIC'#205'PIOS (DTB)'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtDTB_MuniciClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 404
        Height = 32
        Caption = 'Download de Tabelas SPED-EFD'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 404
        Height = 32
        Caption = 'Download de Tabelas SPED-EFD'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 404
        Height = 32
        Caption = 'Download de Tabelas SPED-EFD'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 456
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label3: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 500
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object CkForcaCad: TCheckBox
        Left = 184
        Top = 16
        Width = 381
        Height = 17
        Caption = 
          'For'#231'a o recadastramento dos registros mesmo que j'#225' estejam cadas' +
          'trados.'
        TabOrder = 0
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProtocolVersion = pv1_0
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 156
    Top = 124
  end
  object QrTabelas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SHOW TABLES '
      'LIKE "tbspedefd%"')
    Left = 532
    Top = 136
  end
  object QrVersao: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Versao FROM spedefdtabt ')
    Left = 348
    Top = 200
    object QrVersaoVersao: TWideStringField
      FieldName = 'Versao'
    end
  end
  object QrDupl: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT CodTxt'
      'FROM tbspedefd001'
      'WHERE CodTxt="1"')
    Left = 424
    Top = 80
    object QrDuplCodTxt: TWideStringField
      FieldName = 'CodTxt'
    end
  end
  object QrSorc: TmySQLQuery
    Database = DModG.AllID_DB
    Left = 676
    Top = 64
  end
  object QrDest: TmySQLQuery
    Database = DModG.AllID_DB
    Left = 704
    Top = 64
  end
end
