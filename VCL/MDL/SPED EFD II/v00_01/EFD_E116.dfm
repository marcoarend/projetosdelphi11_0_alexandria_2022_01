object FmEFD_E116: TFmEFD_E116
  Left = 339
  Top = 185
  Caption = 
    'EFD-SPEDE-116 :: Obriga'#231#245'es do ICMS a Recolher - Opera'#231#245'es Pr'#243'pr' +
    'ias'
  ClientHeight = 561
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 660
        Height = 32
        Caption = 'Obriga'#231#245'es do ICMS a Recolher - Opera'#231#245'es Pr'#243'prias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 660
        Height = 32
        Caption = 'Obriga'#231#245'es do ICMS a Recolher - Opera'#231#245'es Pr'#243'prias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 660
        Height = 32
        Caption = 'Obriga'#231#245'es do ICMS a Recolher - Opera'#231#245'es Pr'#243'prias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 491
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 466
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 399
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitHeight = 374
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 399
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 374
      object GroupBox1: TGroupBox
        Left = 0
        Top = 61
        Width = 784
        Height = 338
        Align = alClient
        Caption = ' Dados do ajuste/benef'#237'cio/incentivo da apura'#231#227'o do ICMS: '
        TabOrder = 0
        ExplicitHeight = 313
        object Label7: TStaticText
          Left = 12
          Top = 20
          Width = 741
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 
            '02. C'#243'digo da obriga'#231#227'o a recolher, conforme a tabela indicada n' +
            'o item 5.4.'
          TabOrder = 0
        end
        object Label8: TStaticText
          Left = 12
          Top = 68
          Width = 593
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '03. Valor da obriga'#231#227'o a recolher [F4]'
          TabOrder = 3
        end
        object Label10: TStaticText
          Left = 12
          Top = 92
          Width = 625
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '04. Data de vencimento da obriga'#231#227'o'
          FocusControl = EdVL_OR
          TabOrder = 5
        end
        object EdVL_OR: TdmkEdit
          Left = 640
          Top = 68
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_OR'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdVL_ORKeyDown
        end
        object TPDT_VCTO: TdmkEditDateTimePicker
          Left = 640
          Top = 92
          Width = 112
          Height = 21
          Date = 40761.337560104170000000
          Time = 40761.337560104170000000
          TabOrder = 6
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DT_VCTO'
          UpdType = utYes
        end
        object StaticText1: TStaticText
          Left = 12
          Top = 116
          Width = 625
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 
            '05. C'#243'digo de receita referente '#224' obriga'#231#227'o, pr'#243'prio da unidade ' +
            'da federa'#231#227'o, conforme legisla'#231#227'o estadual.'
          FocusControl = EdCOD_REC
          TabOrder = 7
        end
        object EdCOD_REC: TdmkEdit
          Left = 640
          Top = 116
          Width = 112
          Height = 21
          TabOrder = 8
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'COD_REC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object StaticText2: TStaticText
          Left = 12
          Top = 140
          Width = 625
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 
            '06. N'#250'mero do processo ou auto de infra'#231#227'o ao qual aobriga'#231#227'o es' +
            't'#225' vinculada, se houver.'
          FocusControl = EdNUM_PROC
          TabOrder = 9
        end
        object EdNUM_PROC: TdmkEdit
          Left = 640
          Top = 140
          Width = 112
          Height = 21
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'NUM_PROC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RGIND_PROC: TdmkRadioGroup
          Left = 12
          Top = 164
          Width = 741
          Height = 41
          Caption = ' 07. Indicador da origem do processo: '
          Columns = 5
          ItemIndex = 0
          Items.Strings = (
            ' - N'#227'o tem'
            '0 - SEFAZ'
            '1 - Justi'#231'a Federal'
            '2 - Justi'#231'a Estadual'
            '9 - Outros')
          TabOrder = 11
          QryCampo = 'IND_PROC'
          UpdType = utYes
          OldValor = 0
        end
        object StaticText3: TStaticText
          Left = 12
          Top = 208
          Width = 741
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '08. Descri'#231#227'o resumida do processo que embasou o lan'#231'amento.'
          FocusControl = EdPROC
          TabOrder = 12
        end
        object EdPROC: TdmkEdit
          Left = 12
          Top = 232
          Width = 741
          Height = 21
          TabOrder = 13
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'PROC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object StaticText4: TStaticText
          Left = 12
          Top = 256
          Width = 741
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '09. Descri'#231#227'o complementar das obriga'#231#245'es a recolher.'
          FocusControl = EdTXT_COMPL
          TabOrder = 14
        end
        object EdTXT_COMPL: TdmkEdit
          Left = 12
          Top = 280
          Width = 741
          Height = 21
          TabOrder = 15
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'TXT_COMPL'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object StaticText5: TStaticText
          Left = 12
          Top = 304
          Width = 625
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '10. M'#234's de refer'#234'ncia (selecione o dia 1'#186' do m'#234's de refer'#234'ncia).'
          TabOrder = 16
        end
        object TPMES_REF: TdmkEditDateTimePicker
          Left = 640
          Top = 304
          Width = 112
          Height = 21
          Date = 40761.337560104170000000
          Time = 40761.337560104170000000
          TabOrder = 17
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DT_VCTO'
          UpdType = utYes
        end
        object EdCOD_OR: TdmkEditCB
          Left = 12
          Top = 44
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'COD_OR'
          UpdCampo = 'COD_OR'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCOD_OR
          IgnoraDBLookupComboBox = False
        end
        object CBCOD_OR: TdmkDBLookupComboBox
          Left = 68
          Top = 44
          Width = 685
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsTbSpedEfd005
          TabOrder = 2
          dmkEditCB = EdCOD_OR
          QryCampo = 'COD_OR'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object StaticText6: TStaticText
          Left = 608
          Top = 68
          Width = 28
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = ' [F4]'
          TabOrder = 18
        end
        object StObCOD_OR: TStaticText
          Left = 756
          Top = 44
          Width = 21
          Height = 21
          Alignment = taCenter
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 'E'
          Color = clRed
          FocusControl = EdVL_OR
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 19
          Transparent = False
        end
        object StaticText7: TStaticText
          Left = 756
          Top = 68
          Width = 21
          Height = 21
          Alignment = taCenter
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 'E'
          Color = clRed
          FocusControl = EdVL_OR
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 20
          Transparent = False
        end
        object StaticText8: TStaticText
          Left = 756
          Top = 92
          Width = 21
          Height = 21
          Alignment = taCenter
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 'E'
          Color = clRed
          FocusControl = EdVL_OR
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 21
          Transparent = False
        end
        object StaticText9: TStaticText
          Left = 756
          Top = 116
          Width = 21
          Height = 21
          Alignment = taCenter
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 'E'
          Color = clRed
          FocusControl = EdVL_OR
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 22
          Transparent = False
        end
        object StaticText10: TStaticText
          Left = 760
          Top = 304
          Width = 21
          Height = 21
          Alignment = taCenter
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 'E'
          Color = clRed
          FocusControl = EdVL_OR
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 23
          Transparent = False
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 61
        Align = alTop
        Caption = ' Dados do intervalo: '
        Enabled = False
        TabOrder = 1
        object Label3: TLabel
          Left = 68
          Top = 16
          Width = 56
          Height = 13
          Caption = 'ImporExpor:'
        end
        object Label4: TLabel
          Left = 132
          Top = 16
          Width = 42
          Height = 13
          Caption = 'AnoMes:'
        end
        object Label5: TLabel
          Left = 188
          Top = 16
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label6: TLabel
          Left = 300
          Top = 16
          Width = 28
          Height = 13
          Caption = 'E110:'
        end
        object Label1: TLabel
          Left = 428
          Top = 16
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
        end
        object Label2: TLabel
          Left = 544
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Data final:'
        end
        object Label9: TLabel
          Left = 364
          Top = 16
          Width = 33
          Height = 13
          Caption = 'LinArq:'
        end
        object Label11: TLabel
          Left = 236
          Top = 16
          Width = 28
          Height = 13
          Caption = 'E100:'
        end
        object EdImporExpor: TdmkEdit
          Left = 68
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAnoMes: TdmkEdit
          Left = 132
          Top = 32
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdEmpresa: TdmkEdit
          Left = 188
          Top = 32
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdE110: TdmkEdit
          Left = 300
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object TPDT_INI: TdmkEditDateTimePicker
          Left = 428
          Top = 32
          Width = 112
          Height = 21
          Date = 40761.337560104170000000
          Time = 40761.337560104170000000
          TabOrder = 4
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPDT_FIN: TdmkEditDateTimePicker
          Left = 544
          Top = 32
          Width = 112
          Height = 21
          Date = 40761.337560104170000000
          Time = 40761.337560104170000000
          TabOrder = 5
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdLinArq: TdmkEdit
          Left = 364
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdE100: TdmkEdit
          Left = 236
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 447
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitTop = 422
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrTbSpedEfd005: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT *  '
      'FROM tbspedefd005 '
      'WHERE DataIni <= "2014-01-01" '
      'AND (  '
      '  DataFim >= "2014-01-31" '
      '  OR '
      '  DataFim < "1900-01-01" '
      ') '
      'ORDER BY Nome ')
    Left = 368
    Top = 308
    object QrTbSpedEfd005CodTxt: TWideStringField
      FieldName = 'CodTxt'
    end
    object QrTbSpedEfd005Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTbSpedEfd005Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrTbSpedEfd005DataIni: TDateField
      FieldName = 'DataIni'
    end
    object QrTbSpedEfd005DataFim: TDateField
      FieldName = 'DataFim'
    end
  end
  object DsTbSpedEfd005: TDataSource
    DataSet = QrTbSpedEfd005
    Left = 368
    Top = 352
  end
end
