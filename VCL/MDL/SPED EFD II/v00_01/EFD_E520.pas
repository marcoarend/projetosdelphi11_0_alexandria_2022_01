unit EFD_E520;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums;

type
  TFmEFD_E520 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdE500: TdmkEdit;
    Label6: TLabel;
    Label1: TLabel;
    TPDT_INI: TdmkEditDateTimePicker;
    TPDT_FIN: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label7: TStaticText;
    EdVL_SD_ANT_IPI: TdmkEdit;
    Label8: TStaticText;
    EdVL_DEB_IPI: TdmkEdit;
    Label9: TStaticText;
    EdVL_CRED_IPI: TdmkEdit;
    Label10: TStaticText;
    EdVL_OD_IPI: TdmkEdit;
    Label11: TStaticText;
    EdVL_OC_IPI: TdmkEdit;
    Label12: TStaticText;
    EdVL_SC_IPI: TdmkEdit;
    Label13: TStaticText;
    EdVL_SD_IPI: TdmkEdit;
    EdLinArq: TdmkEdit;
    Label21: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEFD_E520: TFmEFD_E520;

implementation

uses UnMyObjects, Module, EFD_E001, UMySQLModule;

{$R *.DFM}

procedure TFmEFD_E520.BtOKClick(Sender: TObject);
const
  REG = 'E520';
var
  ImporExpor, AnoMes, Empresa, LinArq, E500: Integer;
  VL_SD_ANT_IPI, VL_DEB_IPI, VL_CRED_IPI, VL_OD_IPI, VL_OC_IPI, VL_SC_IPI,
  VL_SD_IPI: Double;
  SQLType: TSQLType;
begin
  SQLType               := ImgTipo.SQLType;
  E500                  := EdE500.ValueVariant;
  VL_SD_ANT_IPI         := EdVL_SD_ANT_IPI.ValueVariant;
  VL_DEB_IPI            := EdVL_DEB_IPI.ValueVariant;
  VL_CRED_IPI           := EdVL_CRED_IPI.ValueVariant;
  VL_OD_IPI             := EdVL_OD_IPI.ValueVariant;
  VL_OC_IPI             := EdVL_OC_IPI.ValueVariant;
  VL_SC_IPI             := EdVL_SC_IPI.ValueVariant;
  VL_SD_IPI             := EdVL_SD_IPI.ValueVariant;
  //
  ImporExpor := EdImporExpor.ValueVariant;
  AnoMes := EdAnoMes.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  LinArq := EdLinArq.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'efd_e520', False, [
  'REG', 'VL_SD_ANT_IPI', 'VL_DEB_IPI',
  'VL_CRED_IPI', 'VL_OD_IPI', 'VL_OC_IPI',
  'VL_SC_IPI', 'VL_SD_IPI'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'E500'], [
  REG, VL_SD_ANT_IPI, VL_DEB_IPI,
  VL_CRED_IPI, VL_OD_IPI, VL_OC_IPI,
  VL_SC_IPI, VL_SD_IPI], [
  ImporExpor, AnoMes, Empresa, LinArq, E500], True) then
  begin
    FmEFD_E001.ReopenEFD_E500(LinArq);
    Close;
  end;
end;

procedure TFmEFD_E520.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_E520.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_E520.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
