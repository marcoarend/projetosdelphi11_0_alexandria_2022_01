unit Sintegra_Arq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, ComCtrls, dmkEditDateTimePicker, DB, mySQLDbTables,
  dmkGeral, Grids, Math, DBGrids, dmkCheckGroup, Menus, dmkDBGridDAC, dmkDBGrid,
  UnDmkProcFunc, Variants, UnDmkEnums, UnGrade_Jan;

type
  TFmSintegra_Arq = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    QrEnti: TmySQLQuery;
    QrEntiNO_ENT: TWideStringField;
    QrEntiCNPJ_CPF: TWideStringField;
    QrEntiIE_RG: TWideStringField;
    QrEntixCidade: TWideStringField;
    QrEntiFax: TWideStringField;
    QrEntixUF: TWideStringField;
    QrEntiRua: TWideStringField;
    QrEntiNumero: TFloatField;
    QrEntiCompl: TWideStringField;
    QrEntiBairro: TWideStringField;
    QrEntiCEP: TLargeintField;
    QrEntiLograd: TWideStringField;
    QrEntiLOGRADOURO: TWideStringField;
    QrCabA: TmySQLQuery;
    QrCabAide_tpNF: TSmallintField;
    QrCabAide_dEmi: TDateField;
    QrCabAide_dSaiEnt: TDateField;
    QrCabAemit_UF: TWideStringField;
    QrCabAdest_UF: TWideStringField;
    QrCabAide_mod: TSmallintField;
    QrCabAide_serie: TIntegerField;
    QrCabAide_nNF: TIntegerField;
    QrCabAFatID: TIntegerField;
    QrCabAFatNum: TIntegerField;
    QrCabAEmpresa: TIntegerField;
    QrSubN: TmySQLQuery;
    QrSubNprod_CFOP: TIntegerField;
    QrSubNICMS_pICMS: TFloatField;
    QrSubNprod_vProd: TFloatField;
    QrSubNICMS_vBC: TFloatField;
    QrSubNICMS_vICMS: TFloatField;
    QrCabAStatus: TIntegerField;
    QrDtaFis: TmySQLQuery;
    QrCabADataFiscal: TDateField;
    QrSubO: TmySQLQuery;
    QrItsI: TmySQLQuery;
    QrItsIprod_CFOP: TIntegerField;
    QrItsInItem: TIntegerField;
    QrItsN: TmySQLQuery;
    QrItsNnItem: TIntegerField;
    QrItsNICMS_CST: TSmallintField;
    QrItsO: TmySQLQuery;
    QrItsOnItem: TIntegerField;
    QrItsIprod_cProd: TWideStringField;
    QrItsIprod_qCom: TFloatField;
    QrItsIprod_vProd: TFloatField;
    QrItsIprod_vDesc: TFloatField;
    QrItsNICMS_vBC: TFloatField;
    QrItsNICMS_pICMS: TFloatField;
    QrItsNICMS_vICMS: TFloatField;
    QrItsNICMS_vBCST: TFloatField;
    QrItsNICMS_vICMSST: TFloatField;
    QrItsOIPI_vIPI: TFloatField;
    QrItsNICMS_Orig: TSmallintField;
    QrProd_: TmySQLQuery;
    QrProd_Sigla: TWideStringField;
    QrProd_NCM: TWideStringField;
    QrProd_IPI_Alq: TFloatField;
    QrProd_ICMSAliqSINTEGRA: TFloatField;
    QrProd_ICMS_pRedBC: TFloatField;
    QrProd_ICMS_pRedBCST: TFloatField;
    QrProd_Nivel1: TIntegerField;
    QrProd_Nome: TWideStringField;
    QrItsINivel1: TIntegerField;
    QrProd_ICMSST_BaseSINTEGRA: TFloatField;
    QrExpA: TmySQLQuery;
    QrExpASINTEGRA_ExpDeclNum: TWideStringField;
    QrExpASINTEGRA_ExpDeclDta: TDateField;
    QrExpASINTEGRA_ExpNat: TWideStringField;
    QrExpASINTEGRA_ExpRegNum: TWideStringField;
    QrExpASINTEGRA_ExpRegDta: TDateField;
    QrExpASINTEGRA_ExpConhNum: TWideStringField;
    QrExpASINTEGRA_ExpConhDta: TDateField;
    QrExpASINTEGRA_ExpConhTip: TWideStringField;
    QrExpASINTEGRA_ExpPais: TWideStringField;
    QrExpASINTEGRA_ExpAverDta: TDateField;
    QrExpAide_mod: TSmallintField;
    QrExpAide_serie: TIntegerField;
    QrExpAide_nNF: TIntegerField;
    QrExpAide_dEmi: TDateField;
    QrEntiCNPJ: TWideStringField;
    QrEntiIE: TWideStringField;
    QrSubOprod_CFOP: TIntegerField;
    QrSubOICMS_pICMS: TFloatField;
    QrSubOprod_vProd: TFloatField;
    QrSubOIPI_vBC: TFloatField;
    QrSubOIPI_vIPI: TFloatField;
    QrCabAIE: TWideStringField;
    QrCabACNPJ_CPF: TWideStringField;
    QrCabAUF: TWideStringField;
    QrCabAemit_CNPJ: TWideStringField;
    QrCabAdest_CNPJ: TWideStringField;
    QrCabAICMSTot_vFrete: TFloatField;
    QrCabAICMSTot_vSeg: TFloatField;
    QrCabAICMSTot_vDesc: TFloatField;
    QrCabAICMSTot_vOutro: TFloatField;
    QrCabAICMSTot_vPIS: TFloatField;
    QrCabAICMSTot_vCOFINS: TFloatField;
    QrCabAISSQNtot_vServ: TFloatField;
    DsCabA: TDataSource;
    QrCabAEhOEmitente: TLargeintField;
    DsDtaFis: TDataSource;
    QrEnt: TmySQLQuery;
    QrEntUFx: TWideStringField;
    QrCabACodInfoEmit: TIntegerField;
    QrProd_prod_cProd: TWideStringField;
    QrProd_prod_xProd: TWideStringField;
    QrCabAIDCtrl: TIntegerField;
    QrLocod: TmySQLQuery;
    QrLocodCodUsu: TIntegerField;
    QrPrds: TmySQLQuery;
    DsPrds: TDataSource;
    DsItsi: TDataSource;
    DsSubN: TDataSource;
    QrItsIprod_xProd: TWideStringField;
    QrItsIprod_NCM: TWideStringField;
    QrItsIprod_uCom: TWideStringField;
    QrLocodNome: TWideStringField;
    QrLocodNCM: TWideStringField;
    QrLocodSigla: TWideStringField;
    QrLocodIPI_Alq: TFloatField;
    QrLocodICMSAliqSINTEGRA: TFloatField;
    QrLocodICMS_pRedBC: TFloatField;
    QrLocodICMS_pRedBCST: TFloatField;
    QrLocodICMSST_BaseSINTEGRA: TFloatField;
    QrPrdsCodigo: TWideStringField;
    QrPrdsSigla: TWideStringField;
    QrPrdsNCM: TWideStringField;
    QrPrdsIPI_Alq: TFloatField;
    QrPrdsICMSAliqSINTEGRA: TFloatField;
    QrPrdsICMS_pRedBC: TFloatField;
    QrPrdsICMS_pRedBCST: TFloatField;
    QrPrdsICMSST_BaseSINTEGRA: TFloatField;
    QrPrdsNome: TWideStringField;
    QrPrdsItens: TIntegerField;
    QrPrdsAtivo: TSmallintField;
    PageControl3: TPageControl;
    TabSheet6: TTabSheet;
    Panel1: TPanel;
    PnBase: TPanel;
    PnConfig: TPanel;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    RG_IdNatOpInfo: TRadioGroup;
    RG_Finalidade: TRadioGroup;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    CkRecriarForca: TCheckBox;
    CkConsertaNome: TCheckBox;
    Panel3: TPanel;
    LaAviso: TLabel;
    PB1: TProgressBar;
    MeGerado: TMemo;
    Panel6: TPanel;
    Grade: TStringGrid;
    MeAvisos: TMemo;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label6: TLabel;
    Panel5: TPanel;
    Button1: TButton;
    DBGrid1: TDBGrid;
    TabSheet2: TTabSheet;
    Label7: TLabel;
    DBGrid2: TDBGrid;
    DBGrid6: TDBGrid;
    TabSheet3: TTabSheet;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    DBGrid4: TDBGrid;
    TabSheet4: TTabSheet;
    DBGrid3: TDBGrid;
    TabSheet7: TTabSheet;
    QrSintegra10: TmySQLQuery;
    DsSintegra10: TDataSource;
    DsR10: TDataSource;
    QrR10: TmySQLQuery;
    PainelConfirma: TPanel;
    BtGera: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel4: TPanel;
    StatusBar: TStatusBar;
    Panel7: TPanel;
    BtCompara: TBitBtn;
    Panel8: TPanel;
    BitBtn2: TBitBtn;
    QrR11: TmySQLQuery;
    QrSintegra11: TmySQLQuery;
    DsSintegra11: TDataSource;
    DsR11: TDataSource;
    QrR50: TmySQLQuery;
    QrSintegra50: TmySQLQuery;
    DsSintegra50: TDataSource;
    DsR50: TDataSource;
    QrR51: TmySQLQuery;
    QrSintegra51: TmySQLQuery;
    DsSintegra51: TDataSource;
    DsR51: TDataSource;
    QrSintegra54: TmySQLQuery;
    QrR54: TmySQLQuery;
    DsR54: TDataSource;
    DsSintegra54: TDataSource;
    QrR53: TmySQLQuery;
    DsR53: TDataSource;
    DsSintegra53: TDataSource;
    QrSintegra53: TmySQLQuery;
    QrSintegra70: TmySQLQuery;
    QrR70: TmySQLQuery;
    DsSintegra70: TDataSource;
    DsR70: TDataSource;
    QrR71: TmySQLQuery;
    QrSintegra71: TmySQLQuery;
    DsSintegra71: TDataSource;
    DsR71: TDataSource;
    QrR74: TmySQLQuery;
    QrSintegra74: TmySQLQuery;
    DsSintegra74: TDataSource;
    DsR74: TDataSource;
    QrR75: TmySQLQuery;
    QrSintegra75: TmySQLQuery;
    DsSintegra75: TDataSource;
    DsR75: TDataSource;
    QrSintegra85: TmySQLQuery;
    DsSintegra85: TDataSource;
    QrR85: TmySQLQuery;
    DsR85: TDataSource;
    QrSintegra86: TmySQLQuery;
    DsSintegra86: TDataSource;
    DsR86: TDataSource;
    QrR86: TmySQLQuery;
    QrSintegra88: TmySQLQuery;
    QrR88: TmySQLQuery;
    DsR88: TDataSource;
    DsSintegra88: TDataSource;
    QrSintegra90: TmySQLQuery;
    DsSintegra90: TDataSource;
    DsR90: TDataSource;
    QrR90: TmySQLQuery;
    Panel10: TPanel;
    CGTipos: TdmkCheckGroup;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrR50TIPO: TWideStringField;
    QrR50CNPJ: TWideStringField;
    QrR50INSCEST: TWideStringField;
    QrR50EMISSAO: TDateField;
    QrR50UF: TWideStringField;
    QrR50MODELO: TWideStringField;
    QrR50SERIE: TWideStringField;
    QrR50NUMNF: TWideStringField;
    QrR50CODNATOP: TWideStringField;
    QrR50EMITENTE: TWideStringField;
    QrR50VALORTOT: TWideStringField;
    QrR50BASEICMS: TWideStringField;
    QrR50VALORICMS: TWideStringField;
    QrR50VLRISENTO: TWideStringField;
    QrR50OUTROS: TWideStringField;
    QrR50ALIQUOTA: TWideStringField;
    QrR50SITUACAO: TWideStringField;
    QrSintegra50TIPO: TWideStringField;
    QrSintegra50CNPJ: TWideStringField;
    QrSintegra50INSCEST: TWideStringField;
    QrSintegra50EMISSAO: TDateField;
    QrSintegra50UF: TWideStringField;
    QrSintegra50MODELO: TWideStringField;
    QrSintegra50SERIE: TWideStringField;
    QrSintegra50NUMNF: TWideStringField;
    QrSintegra50CODNATOP: TWideStringField;
    QrSintegra50EMITENTE: TWideStringField;
    QrSintegra50VALORTOT: TWideStringField;
    QrSintegra50BASEICMS: TWideStringField;
    QrSintegra50VALORICMS: TWideStringField;
    QrSintegra50VLRISENTO: TWideStringField;
    QrSintegra50OUTROS: TWideStringField;
    QrSintegra50ALIQUOTA: TWideStringField;
    QrSintegra50SITUACAO: TWideStringField;
    Panel11: TPanel;
    GradeMy: TDBGrid;
    Label8: TLabel;
    Label9: TLabel;
    GradeCo: TDBGrid;
    QrSubNprod_vNF: TFloatField;
    QrTotN: TmySQLQuery;
    QrTotNprod_vProd: TFloatField;
    QrTotNprod_vNF: TFloatField;
    QrTotNICMS_vBC: TFloatField;
    QrTotNICMS_vICMS: TFloatField;
    QrCabAICMSTot_vNF: TFloatField;
    QrCabAICMSTot_vBC: TFloatField;
    QrCabAICMSTot_vICMS: TFloatField;
    QrCabAICMSTot_vBCST: TFloatField;
    QrCabAICMSTot_vST: TFloatField;
    QrCabAICMSTot_vProd: TFloatField;
    QrCabAICMSTot_vII: TFloatField;
    QrCabAICMSTot_vIPI: TFloatField;
    QrCabACodInfoDest: TIntegerField;
    QrNatOper: TmySQLQuery;
    QrNatOperCodEnt: TWideStringField;
    Panel12: TPanel;
    DBGrid5: TDBGrid;
    Label10: TLabel;
    QrDifere: TmySQLQuery;
    QrDifereTipoReg: TSmallintField;
    QrDifereItemReg: TIntegerField;
    QrDifereCampo: TWideStringField;
    QrDifereValMy: TWideStringField;
    QrDifereValCo: TWideStringField;
    QrDifereAtivo: TSmallintField;
    DsDifere: TDataSource;
    Memo1: TMemo;
    BtExclCo: TBitBtn;
    PmExclCo: TPopupMenu;
    Excluiitematualdaminhatabela1: TMenuItem;
    Memo2: TMemo;
    QrSintegra51TIPO: TWideStringField;
    QrSintegra51CNPJ: TWideStringField;
    QrSintegra51INSCEST: TWideStringField;
    QrSintegra51EMISSAO: TDateField;
    QrSintegra51UF: TWideStringField;
    QrSintegra51SERIE: TWideStringField;
    QrSintegra51NUMNF: TWideStringField;
    QrSintegra51CODNATOP: TWideStringField;
    QrSintegra51TOTALNF: TWideStringField;
    QrSintegra51VLRTOTIPI: TWideStringField;
    QrSintegra51VLRISENTO: TWideStringField;
    QrSintegra51OUTROS: TWideStringField;
    QrSintegra51BRANCOS: TWideStringField;
    QrSintegra51SITUACAO: TWideStringField;
    QrR51TIPO: TWideStringField;
    QrR51CNPJ: TWideStringField;
    QrR51INSCEST: TWideStringField;
    QrR51EMISSAO: TDateField;
    QrR51UF: TWideStringField;
    QrR51SERIE: TWideStringField;
    QrR51NUMNF: TWideStringField;
    QrR51CODNATOP: TWideStringField;
    QrR51TOTALNF: TWideStringField;
    QrR51VLRTOTIPI: TWideStringField;
    QrR51VLRISENTO: TWideStringField;
    QrR51OUTROS: TWideStringField;
    QrR51BRANCOS: TWideStringField;
    QrR51SITUACAO: TWideStringField;
    QrLocNFa: TmySQLQuery;
    QrLocNFaFatID: TIntegerField;
    QrLocNFaFatNum: TIntegerField;
    QrLocNFaEmpresa: TIntegerField;
    QrLocNFaIDCtrl: TIntegerField;
    QrLocNFaide_dEmi: TDateField;
    QrLocNFaemit_xNome: TWideStringField;
    QrLocNFaICMSTot_vProd: TFloatField;
    QrLocNFaICMSTot_vNF: TFloatField;
    QrSubOprod_vNF: TFloatField;
    QrTotO: TmySQLQuery;
    QrTotOprod_vProd: TFloatField;
    QrTotOprod_vNF: TFloatField;
    QrTotOIPI_vBC: TFloatField;
    QrTotOIPI_vIPI: TFloatField;
    QrR54TIPO: TWideStringField;
    QrR54CNPJ: TWideStringField;
    QrR54MODELO: TWideStringField;
    QrR54SERIE: TWideStringField;
    QrR54NUMNF: TWideStringField;
    QrR54CODNATOP: TWideStringField;
    QrR54SITTRIB: TWideStringField;
    QrR54NUMITEM: TWideStringField;
    QrR54CODPROD: TWideStringField;
    QrR54QTDADE: TWideStringField;
    QrR54VLRPROD: TWideStringField;
    QrR54VLRDESC: TWideStringField;
    QrR54BASECALC: TWideStringField;
    QrR54BASESUBTRI: TWideStringField;
    QrR54VLRIPI: TWideStringField;
    QrR54ALIQICMS: TWideStringField;
    QrSintegra54TIPO: TWideStringField;
    QrSintegra54CNPJ: TWideStringField;
    QrSintegra54MODELO: TWideStringField;
    QrSintegra54SERIE: TWideStringField;
    QrSintegra54NUMNF: TWideStringField;
    QrSintegra54CODNATOP: TWideStringField;
    QrSintegra54SITTRIB: TWideStringField;
    QrSintegra54NUMITEM: TWideStringField;
    QrSintegra54CODPROD: TWideStringField;
    QrSintegra54QTDADE: TWideStringField;
    QrSintegra54VLRPROD: TWideStringField;
    QrSintegra54VLRDESC: TWideStringField;
    QrSintegra54BASECALC: TWideStringField;
    QrSintegra54BASESUBTRI: TWideStringField;
    QrSintegra54VLRIPI: TWideStringField;
    QrSintegra54ALIQICMS: TWideStringField;
    QrSintegra53TIPO: TWideStringField;
    QrSintegra53CNPJ: TWideStringField;
    QrSintegra53INSCEST: TWideStringField;
    QrSintegra53EMISSAO: TDateField;
    QrSintegra53UF: TWideStringField;
    QrSintegra53MODELO: TWideStringField;
    QrSintegra53SERIE: TWideStringField;
    QrSintegra53NUMERO: TWideStringField;
    QrSintegra53CFOP: TWideStringField;
    QrSintegra53EMITENTE: TWideStringField;
    QrSintegra53BASECALC: TWideStringField;
    QrSintegra53ICMSRETIDO: TWideStringField;
    QrSintegra53DESPACESS: TWideStringField;
    QrSintegra53SITUACAO: TWideStringField;
    QrSintegra53CODANTEC: TWideStringField;
    QrSintegra53BRANCOS: TWideStringField;
    QrR53TIPO: TWideStringField;
    QrR53CNPJ: TWideStringField;
    QrR53INSCEST: TWideStringField;
    QrR53EMISSAO: TDateField;
    QrR53UF: TWideStringField;
    QrR53MODELO: TWideStringField;
    QrR53SERIE: TWideStringField;
    QrR53NUMERO: TWideStringField;
    QrR53CFOP: TWideStringField;
    QrR53EMITENTE: TWideStringField;
    QrR53BASECALC: TWideStringField;
    QrR53ICMSRETIDO: TWideStringField;
    QrR53DESPACESS: TWideStringField;
    QrR53SITUACAO: TWideStringField;
    QrR53CODANTEC: TWideStringField;
    QrR53BRANCOS: TWideStringField;
    DsCampos: TDataSource;
    TbCampos: TmySQLTable;
    GradeCampos: TdmkDBGridDAC;
    TbCamposCampo: TWideStringField;
    TbCamposAtivo: TSmallintField;
    Splitter1: TSplitter;
    QrItsOIPI_vBC: TFloatField;
    Ck_54_900: TCheckBox;
    Panel9: TPanel;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    QrSintegra70TIPO: TWideStringField;
    QrSintegra70CNPJ: TWideStringField;
    QrSintegra70INSCEST: TWideStringField;
    QrSintegra70EMISSAO: TDateField;
    QrSintegra70UF: TWideStringField;
    QrSintegra70MODELO: TWideStringField;
    QrSintegra70SERIE: TWideStringField;
    QrSintegra70SUBSERIE: TWideStringField;
    QrSintegra70NUMNF: TWideStringField;
    QrSintegra70CODNATOP: TWideStringField;
    QrSintegra70VALORTOT: TWideStringField;
    QrSintegra70BASEICMS: TWideStringField;
    QrSintegra70VALORICMS: TWideStringField;
    QrSintegra70VLRISENTO: TWideStringField;
    QrSintegra70OUTROS: TWideStringField;
    QrSintegra70CIF_FOB: TWideStringField;
    QrSintegra70SITUACAO: TWideStringField;
    QrR70TIPO: TWideStringField;
    QrR70CNPJ: TWideStringField;
    QrR70INSCEST: TWideStringField;
    QrR70EMISSAO: TDateField;
    QrR70UF: TWideStringField;
    QrR70MODELO: TWideStringField;
    QrR70SERIE: TWideStringField;
    QrR70SUBSERIE: TWideStringField;
    QrR70NUMNF: TWideStringField;
    QrR70CODNATOP: TWideStringField;
    QrR70VALORTOT: TWideStringField;
    QrR70BASEICMS: TWideStringField;
    QrR70VALORICMS: TWideStringField;
    QrR70VLRISENTO: TWideStringField;
    QrR70OUTROS: TWideStringField;
    QrR70CIF_FOB: TWideStringField;
    QrR70SITUACAO: TWideStringField;
    QrSintegra71TIPO: TWideStringField;
    QrSintegra71CNPJ: TWideStringField;
    QrSintegra71INSCEST: TWideStringField;
    QrSintegra71EMISSAOC: TDateField;
    QrSintegra71UF: TWideStringField;
    QrSintegra71MODELOC: TWideStringField;
    QrSintegra71SERIEC: TWideStringField;
    QrSintegra71SUBSERIE: TWideStringField;
    QrSintegra71NUMEROC: TWideStringField;
    QrSintegra71UFREM: TWideStringField;
    QrSintegra71CNPJREM: TWideStringField;
    QrSintegra71INSCESTREM: TWideStringField;
    QrSintegra71EMISSAON: TDateField;
    QrSintegra71MODELON: TWideStringField;
    QrSintegra71SERIEN: TWideStringField;
    QrSintegra71NUMNF: TWideStringField;
    QrSintegra71TOTALNF: TWideStringField;
    QrSintegra71BRANCOS: TWideStringField;
    QrR71TIPO: TWideStringField;
    QrR71CNPJ: TWideStringField;
    QrR71INSCEST: TWideStringField;
    QrR71EMISSAOC: TDateField;
    QrR71UF: TWideStringField;
    QrR71MODELOC: TWideStringField;
    QrR71SERIEC: TWideStringField;
    QrR71SUBSERIE: TWideStringField;
    QrR71NUMEROC: TWideStringField;
    QrR71UFREM: TWideStringField;
    QrR71CNPJREM: TWideStringField;
    QrR71INSCESTREM: TWideStringField;
    QrR71EMISSAON: TDateField;
    QrR71MODELON: TWideStringField;
    QrR71SERIEN: TWideStringField;
    QrR71NUMNF: TWideStringField;
    QrR71TOTALNF: TWideStringField;
    QrR71BRANCOS: TWideStringField;
    QrSintegra74TIPO: TWideStringField;
    QrSintegra74DATA: TDateField;
    QrSintegra74CODIPROD: TWideStringField;
    QrSintegra74QTDADE: TWideStringField;
    QrSintegra74VLRPROD: TWideStringField;
    QrSintegra74CODPOSSEM: TWideStringField;
    QrSintegra74CNPJ: TWideStringField;
    QrSintegra74INSCEST: TWideStringField;
    QrSintegra74UF: TWideStringField;
    QrSintegra74BRANCOS: TWideStringField;
    QrR74TIPO: TWideStringField;
    QrR74DATA: TDateField;
    QrR74CODIPROD: TWideStringField;
    QrR74QTDADE: TWideStringField;
    QrR74VLRPROD: TWideStringField;
    QrR74CODPOSSEM: TWideStringField;
    QrR74CNPJ: TWideStringField;
    QrR74INSCEST: TWideStringField;
    QrR74UF: TWideStringField;
    QrR74BRANCOS: TWideStringField;
    QrSintegra75TIPO: TWideStringField;
    QrSintegra75DATAINI: TDateField;
    QrSintegra75DATAFIN: TDateField;
    QrSintegra75CODPROD: TWideStringField;
    QrSintegra75CODNCM: TWideStringField;
    QrSintegra75DESCRICAO: TWideStringField;
    QrSintegra75UNIDADE: TWideStringField;
    QrSintegra75ALIQIPI: TWideStringField;
    QrSintegra75ALIQICMS: TWideStringField;
    QrSintegra75REDBASEICM: TWideStringField;
    QrSintegra75BASESUBTRI: TWideStringField;
    QrR75TIPO: TWideStringField;
    QrR75DATAINI: TDateField;
    QrR75DATAFIN: TDateField;
    QrR75CODPROD: TWideStringField;
    QrR75CODNCM: TWideStringField;
    QrR75DESCRICAO: TWideStringField;
    QrR75UNIDADE: TWideStringField;
    QrR75ALIQIPI: TWideStringField;
    QrR75ALIQICMS: TWideStringField;
    QrR75REDBASEICM: TWideStringField;
    QrR75BASESUBTRI: TWideStringField;
    QrSintegra85TIPO: TWideStringField;
    QrSintegra85DECLARACAO: TWideStringField;
    QrSintegra85DATADECLAR: TDateField;
    QrSintegra85NATUREZA: TWideStringField;
    QrSintegra85REGISTRO: TWideStringField;
    QrSintegra85DATAREG: TDateField;
    QrSintegra85CONHECIM: TWideStringField;
    QrSintegra85DATACONHEC: TDateField;
    QrSintegra85TIPOCONHEC: TWideStringField;
    QrSintegra85PAIS: TWideStringField;
    QrSintegra85RESERVADO: TWideStringField;
    QrSintegra85DATAAVERB: TDateField;
    QrSintegra85NUMNFEXP: TWideStringField;
    QrSintegra85EMISSAO: TDateField;
    QrSintegra85MODELO: TWideStringField;
    QrSintegra85SERIE: TWideStringField;
    QrSintegra85BRANCOS: TWideStringField;
    QrR85TIPO: TWideStringField;
    QrR85DECLARACAO: TWideStringField;
    QrR85DATADECLAR: TDateField;
    QrR85NATUREZA: TWideStringField;
    QrR85REGISTRO: TWideStringField;
    QrR85DATAREG: TDateField;
    QrR85CONHECIM: TWideStringField;
    QrR85DATACONHEC: TDateField;
    QrR85TIPOCONHEC: TWideStringField;
    QrR85PAIS: TWideStringField;
    QrR85RESERVADO: TWideStringField;
    QrR85DATAAVERB: TDateField;
    QrR85NUMNFEXP: TWideStringField;
    QrR85EMISSAO: TDateField;
    QrR85MODELO: TWideStringField;
    QrR85SERIE: TWideStringField;
    QrR85BRANCOS: TWideStringField;
    QrSintegra86TIPO: TWideStringField;
    QrSintegra86REGEXPORT: TWideStringField;
    QrSintegra86DATAREG: TDateField;
    QrSintegra86CNPJREM: TWideStringField;
    QrSintegra86INSCEST: TWideStringField;
    QrSintegra86UF: TWideStringField;
    QrSintegra86NUMNF: TWideStringField;
    QrSintegra86EMISSAO: TDateField;
    QrSintegra86MODELO: TWideStringField;
    QrSintegra86SERIE: TWideStringField;
    QrSintegra86CODPROD: TWideStringField;
    QrSintegra86QTDADE: TWideStringField;
    QrSintegra86VLRUNIT: TWideStringField;
    QrSintegra86VLRPROD: TWideStringField;
    QrSintegra86RELACIONA: TWideStringField;
    QrSintegra86BRANCOS: TWideStringField;
    QrR86TIPO: TWideStringField;
    QrR86REGEXPORT: TWideStringField;
    QrR86DATAREG: TDateField;
    QrR86CNPJREM: TWideStringField;
    QrR86INSCEST: TWideStringField;
    QrR86UF: TWideStringField;
    QrR86NUMNF: TWideStringField;
    QrR86EMISSAO: TDateField;
    QrR86MODELO: TWideStringField;
    QrR86SERIE: TWideStringField;
    QrR86CODPROD: TWideStringField;
    QrR86QTDADE: TWideStringField;
    QrR86VLRUNIT: TWideStringField;
    QrR86VLRPROD: TWideStringField;
    QrR86RELACIONA: TWideStringField;
    QrR86BRANCOS: TWideStringField;
    QrSintegra88TIPO: TWideStringField;
    QrSintegra88SUBTIPO: TWideStringField;
    QrSintegra88CNPJ: TWideStringField;
    QrSintegra88MODELO: TWideStringField;
    QrSintegra88SERIE: TWideStringField;
    QrSintegra88NUMERO: TWideStringField;
    QrSintegra88CFOP: TWideStringField;
    QrSintegra88CST: TWideStringField;
    QrSintegra88NUMITEM: TWideStringField;
    QrSintegra88CODPROD: TWideStringField;
    QrSintegra88NUMSER: TWideStringField;
    QrSintegra88BRANCOS: TWideStringField;
    QrR88TIPO: TWideStringField;
    QrR88SUBTIPO: TWideStringField;
    QrR88CNPJ: TWideStringField;
    QrR88MODELO: TWideStringField;
    QrR88SERIE: TWideStringField;
    QrR88NUMERO: TWideStringField;
    QrR88CFOP: TWideStringField;
    QrR88CST: TWideStringField;
    QrR88NUMITEM: TWideStringField;
    QrR88CODPROD: TWideStringField;
    QrR88NUMSER: TWideStringField;
    QrR88BRANCOS: TWideStringField;
    CkGera75x54: TCheckBox;
    QrSintegra10TIPO: TWideStringField;
    QrSintegra10CNPJ: TWideStringField;
    QrSintegra10INSCEST: TWideStringField;
    QrSintegra10NCONTRIB: TWideStringField;
    QrSintegra10MUNICIPIO: TWideStringField;
    QrSintegra10UF: TWideStringField;
    QrSintegra10FAX: TWideStringField;
    QrSintegra10DATAINI: TDateField;
    QrSintegra10DATAFIN: TDateField;
    QrSintegra10CODCONV: TWideStringField;
    QrSintegra10CODNAT: TWideStringField;
    QrSintegra10CODFIN: TWideStringField;
    QrR10TIPO: TWideStringField;
    QrR10CNPJ: TWideStringField;
    QrR10INSCEST: TWideStringField;
    QrR10NCONTRIB: TWideStringField;
    QrR10MUNICIPIO: TWideStringField;
    QrR10UF: TWideStringField;
    QrR10FAX: TWideStringField;
    QrR10DATAINI: TDateField;
    QrR10DATAFIN: TDateField;
    QrR10CODCONV: TWideStringField;
    QrR10CODNAT: TWideStringField;
    QrR10CODFIN: TWideStringField;
    QrSintegra11TIPO: TWideStringField;
    QrSintegra11LOGRADOURO: TWideStringField;
    QrSintegra11NUMERO: TWideStringField;
    QrSintegra11COMPLEMENT: TWideStringField;
    QrSintegra11BAIRRO: TWideStringField;
    QrSintegra11CEP: TWideStringField;
    QrSintegra11CONTATO: TWideStringField;
    QrSintegra11TELEFONE: TWideStringField;
    QrR11TIPO: TWideStringField;
    QrR11LOGRADOURO: TWideStringField;
    QrR11NUMERO: TWideStringField;
    QrR11COMPLEMENT: TWideStringField;
    QrR11BAIRRO: TWideStringField;
    QrR11CEP: TWideStringField;
    QrR11CONTATO: TWideStringField;
    QrR11TELEFONE: TWideStringField;
    TabSheet8: TTabSheet;
    PageControl4: TPageControl;
    TabSheet9: TTabSheet;
    DBGSMIC2: TdmkDBGrid;
    TabSheet11: TTabSheet;
    DBGExport2: TdmkDBGrid;
    DBGListErr1: TDBGrid;
    Splitter2: TSplitter;
    TabSheet10: TTabSheet;
    Panel13: TPanel;
    Label83: TLabel;
    EdSINTEGRA_Path: TdmkEdit;
    SpeedButton7: TSpeedButton;
    Panel14: TPanel;
    Panel15: TPanel;
    BtCarrega: TBitBtn;
    SpeedButton1: TSpeedButton;
    BtImporta: TBitBtn;
    QrLoad_S50: TmySQLQuery;
    GroupBox2: TGroupBox;
    EdEmp_CNPJ: TdmkEdit;
    Label11: TLabel;
    EdEmp_Codigo: TdmkEdit;
    Label12: TLabel;
    Label13: TLabel;
    EdEmp_Nome: TdmkEdit;
    QrLoad_S50TIPO: TSmallintField;
    QrLoad_S50CNPJ: TWideStringField;
    QrLoad_S50IE: TWideStringField;
    QrLoad_S50EMISSAO: TDateField;
    QrLoad_S50UF: TWideStringField;
    QrLoad_S50MODELO: TWideStringField;
    QrLoad_S50SERIE: TWideStringField;
    QrLoad_S50NUMNF: TWideStringField;
    QrLoad_S50CFOP: TWideStringField;
    QrLoad_S50EMITENTE: TWideStringField;
    QrLoad_S50ValorNF: TFloatField;
    QrLoad_S50BCICMS: TFloatField;
    QrLoad_S50ValICMS: TFloatField;
    QrLoad_S50ValIsento: TFloatField;
    QrLoad_S50ValNCICMS: TFloatField;
    QrLoad_S50AliqICMS: TFloatField;
    QrLoad_S50Terceiro: TIntegerField;
    QrLoad_S50Ativo: TSmallintField;
    QrLocNF: TmySQLQuery;
    PageControl5: TPageControl;
    TabSheet12: TTabSheet;
    GridS: TStringGrid;
    MeImportado: TMemo;
    TabSheet13: TTabSheet;
    MeIgnor: TMemo;
    TabSheet14: TTabSheet;
    MeErros: TMemo;
    TabSheet15: TTabSheet;
    MeNaoImport: TMemo;
    Panel16: TPanel;
    PB2: TProgressBar;
    LaAviso2: TLabel;
    QrLocGGX: TmySQLQuery;
    QrLocGGXGraGru1: TIntegerField;
    QrLocGGXControle: TIntegerField;
    QrLocGGXNO_PRD_TAM_COR: TWideStringField;
    RGComoImporta: TRadioGroup;
    QrLocGG1: TmySQLQuery;
    QrLocGG1GraGru1: TIntegerField;
    QrLocGG1Controle: TIntegerField;
    QrLocGG1NO_PRD_TAM_COR: TWideStringField;
    QrLocGGXPrdGrupTip: TIntegerField;
    QrLocGG1PrdGrupTip: TIntegerField;
    QrLocPrdNom: TmySQLQuery;
    QrLocPrdNomGraGruX: TIntegerField;
    QrLocPrdTmp: TmySQLQuery;
    QrLocPrdTmpCU_CodUsu: TIntegerField;
    QrLocPrdTmpGraGru1: TIntegerField;
    QrLocPrdTmpGraGruX: TIntegerField;
    QrLocPrdTmpNO_PRD_TAM_COR: TWideStringField;
    QrLocPrdTmpPrdGrupTip: TIntegerField;
    QrLoad_S54: TmySQLQuery;
    QrLoad_S54TIPO: TWideStringField;
    QrLoad_S54CNPJ: TWideStringField;
    QrLoad_S54MODELO: TWideStringField;
    QrLoad_S54SERIE: TWideStringField;
    QrLoad_S54NUMNF: TWideStringField;
    QrLoad_S54CFOP: TWideStringField;
    QrLoad_S54CST: TWideStringField;
    QrLoad_S54NUMITEM: TWideStringField;
    QrLoad_S54CODPROD: TWideStringField;
    QrLoad_S54QTDADE: TFloatField;
    QrLoad_S54VLRPROD: TFloatField;
    QrLoad_S54VLRDESC: TFloatField;
    QrLoad_S54BASECALC: TFloatField;
    QrLoad_S54BASESUBTRI: TFloatField;
    QrLoad_S54VLRIPI: TFloatField;
    QrLoad_S54ALIQICMS: TFloatField;
    QrLoad_S54Ativo: TSmallintField;
    QrLoad_S75: TmySQLQuery;
    QrLoad_S75GraGruX: TIntegerField;
    QrLoad_S75TIPO: TWideStringField;
    QrLoad_S75DATAINI: TDateField;
    QrLoad_S75DATAFIN: TDateField;
    QrLoad_S75CODPROD: TWideStringField;
    QrLoad_S75CODNCM: TWideStringField;
    QrLoad_S75DESCRICAO: TWideStringField;
    QrLoad_S75UNIDADE: TWideStringField;
    QrLoad_S75ALIQIPI: TFloatField;
    QrLoad_S75ALIQICMS: TFloatField;
    QrLoad_S75REDBASEICM: TFloatField;
    QrLoad_S75BASESUBTRI: TFloatField;
    QrLoad_S75Ativo: TSmallintField;
    QrLocPrdNomGraGru1: TIntegerField;
    CkRecriarNFs: TCheckBox;
    QrLocNFFatID: TIntegerField;
    QrLocNFFatNum: TIntegerField;
    QrLocNFEmpresa: TIntegerField;
    QrLocNFIDCtrl: TIntegerField;
    QrLoad_S75Nivel1: TIntegerField;
    QrSum_S54: TmySQLQuery;
    QrSum_S54VLRPROD: TFloatField;
    QrSum_S50: TmySQLQuery;
    QrSum_S50ValorNF: TFloatField;
    QrLoad_S70: TmySQLQuery;
    QrLoad_S70TIPO: TWideStringField;
    QrLoad_S70CNPJ: TWideStringField;
    QrLoad_S70INSCEST: TWideStringField;
    QrLoad_S70EMISSAO: TDateField;
    QrLoad_S70UF: TWideStringField;
    QrLoad_S70MODELO: TWideStringField;
    QrLoad_S70SERIE: TWideStringField;
    QrLoad_S70SUBSERIE: TWideStringField;
    QrLoad_S70NUMNF: TWideStringField;
    QrLoad_S70CFOP: TWideStringField;
    QrLoad_S70VALORTOT: TFloatField;
    QrLoad_S70BASEICMS: TFloatField;
    QrLoad_S70VALORICMS: TFloatField;
    QrLoad_S70VLRISENTO: TFloatField;
    QrLoad_S70OUTROS: TFloatField;
    QrLoad_S70CIF_FOB: TSmallintField;
    QrLoad_S70SITUACAO: TWideStringField;
    QrLoad_S70Ativo: TSmallintField;
    QrLoad_S70Transporta: TIntegerField;
    CkExclEstqMP: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtGeraClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrEntiCalcFields(DataSet: TDataSet);
    procedure MeGeradoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure MeGeradoMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MeGeradoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure GradeDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure Button1Click(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtComparaClick(Sender: TObject);
    procedure DBGrid5DblClick(Sender: TObject);
    procedure BtExclCoClick(Sender: TObject);
    procedure Excluiitematualdaminhatabela1Click(Sender: TObject);
    procedure GradeMyDblClick(Sender: TObject);
    procedure QrDifereAfterOpen(DataSet: TDataSet);
    procedure GradeCamposAfterReopenAfterSQLExec(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure MeImportadoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MeImportadoMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MeImportadoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtCarregaClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure EdEmp_CNPJChange(Sender: TObject);
  private
    { Private declarations }
    //FxLinha: String;
    FQtd10, FQtd11, FQtd50, FQtd51, FQtd53, FQtd54, FQtd55, FQtd56, FQtd57,
    FQtd60, FQtd61, FQtd70, FQtd71, FQtd74, FQtd75, FQtd76, FQtd77, FQtd85,
    FQtd86, FQtd90, FQtd99, FQtd00: Integer;
    F_Casas: Byte;
    FcReg, F_Tam, F_PosI, F_PosF,
    FMeGerado_SelCol, FMeGerado_SelLin,
    FMeImportado_SelCol, FMeImportado_SelLin,
    FnLinha: Integer;
    F_Fmt: Char;
    FtxtLin, FxLinha: String;
    F_Obrigatorio: Boolean;
    FEmpresa_TXT: String;
    FSintegra10, FSintegra11,
    FSintegra50, FSintegra51, FSintegra53, FSintegra54, FSintegra70,
    FSintegra71, FSintegra74, FSintegra75, FSintegra85, FSintegra86,
    FSintegra88, FSintegra90, FSintegraPM, FSintegraEC, FSintegraEr: String;
    FS50Load, FS54Load, FS70Load, FS75Load: String;
    procedure InfoPosMeGerado();
    procedure InfoPosMeImportado();
{
    procedure InfoPosMeImportado2(Line: Integer);
    function AddItemX(Tam, PosI, PosF: Integer; Filler, Valor: String;
             AjustaAMenor: TAjustaAMenor = aamNone): Boolean;
}
    function LimpaValor(Valor: String): String;
    function AddItemX(Registro, Campo: Integer; SubTipo: Char; Valor: String;
             AjustaAMenor: TAjustaAMenor = aamNone): Boolean;
    function ValorPositivo(const ValInfo: Double; var ValOut: Double): Boolean;
    function DadosNFAtual(): String;
    function DefineDadosEntidadeNF(var CNPJ_CPF, IE, Data, UF, EmitenteNF, Situacao:
             String): Boolean;
    function DefineCFOPEntrada(EmitenteNF: String; const CFOP_Inn: Integer;
             var CFOP_Out: Integer): Boolean;
    function AdicionaLinhaAoMeGerado(const Texto: String; var Contador: Integer): Boolean;
    //
    function IncluiRegistro10(): Boolean;
    function IncluiRegistro11(): Boolean;
    function IncluiRegistro50(): Boolean;
    function IncluiRegistro51(): Boolean;
    function IncluiRegistro53(): Boolean;
    function IncluiRegistro54(): Boolean;
    function IncluiRegistro55(): Boolean;
    function IncluiRegistro56(): Boolean;
    function IncluiRegistro57(): Boolean;
    function IncluiRegistro60M(): Boolean;
    function IncluiRegistro60A(): Boolean;
    function IncluiRegistro60D(): Boolean;
    function IncluiRegistro60I(): Boolean;
    function IncluiRegistro60R(): Boolean;
    function IncluiRegistro61(): Boolean;
    function IncluiRegistro61R(): Boolean;
    function IncluiRegistro70(): Boolean;
    function IncluiRegistro71(): Boolean;
    function IncluiRegistro74(): Boolean;
    function IncluiRegistro75(): Boolean;
    function IncluiRegistro76(): Boolean;
    function IncluiRegistro77(): Boolean;
    function IncluiRegistro85(): Boolean;
    function IncluiRegistro86(): Boolean;
    function IncluiRegistro90(): Boolean;
    //
    procedure OrganizaGrades();
    procedure LocalizaNF(FatID, IDCtrl: Integer);
    procedure ReopenDifere(Filtra: Boolean);
    procedure SetaTodosCampos(Ativo: Integer);
    function  DefSerie(Serie: Integer): String;
{
    function  DefineEntidade(const CNPJCPF: String; const AvisaErros: Boolean;
              var Entidade: Integer; var Nome: String): Boolean;
    function  DefineReduzido(const CODPROD: String; const AvisaErros: Boolean;
              var Reduzido: Integer; var Nome: String): Boolean;
    }          
  public
    { Public declarations }
    FExportarItem: Boolean;
  end;

  var
  FmSintegra_Arq: TFmSintegra_Arq;

implementation

uses UnMyObjects, ModuleGeral, Module, NFeCabA_0000, MyDBCheck, UnGrade_Create,
UMySQLModule, UnInternalConsts, EntradaCab, NatOper, ModProd, Sintegra_PesqPrd,
  ModuleNFe_0000, Principal, UnGrade_Tabs, UnSintegra;

{$R *.DFM}

function TFmSintegra_Arq.AddItemX(Registro, Campo: Integer; SubTipo: Char; Valor: String;
AjustaAMenor: TAjustaAMenor = aamNone): Boolean;
  function Sai(): Boolean;
  var
    Txt: String;
    I: Integer;
  begin
    //Result := False;
    //
    Txt := '';
    for I := 1 to Length(FTxtLin) do
      Txt := Txt + MLAGeral.CharToLetraMaiuscula(Ord(FTxtLin[I]));
    //
    MeGerado.Lines.Add(Txt);
    Result := True;
  end;

  function Mensagem(): String;
  begin
    Result := #13#10 + 'Function: "TFmSintegra_Arq.AddItemX()"' + #13#10 +
    'Linha: ' + FxLinha + #13#10 +
    'Registro: ' + IntToStr(FcReg) + #13#10 +
    'Posi��o inicial: ' + IntToStr(F_PosI) + #13#10 +
    'Posi��o Final: ' + IntToStr(F_PosF) + #13#10 +
    'Tamanho campo: ' + IntToStr(F_Tam) + #13#10 +
    'Texto campo: ' + Valor + #13#10 + #13#10 +
    'Campo: ' + USintegra.ObtemDescricaoDeCampo(FcReg, F_PosI, SubTipo);
  end;
var
  xVal: String;
  tI, tT: Integer;
begin
  Result  := False;
  USintegra.CoordenadasCampo(Registro, Campo, SubTipo, F_Tam, F_PosI, F_PosF, F_Fmt,
    F_Casas, F_Obrigatorio);
  if F_Obrigatorio and (LimpaValor(Valor) = '') then
  begin
    Geral.MensagemBox('Valor � obrigat�rio:' +
    Mensagem, 'Erro', MB_OK+MB_ICONERROR);
    //  desmarcar! 2011-02-25
    Sai();
    Exit;
  end;
  //FxReg   := Copy(FtxtLin, 1, 2);
  FnLinha := MeGerado.Lines.Count + 1;
  FxLinha := FormatFloat('000', FnLinha);
  //
  tI := Length(FtxtLin);
  if tI <> F_PosI - 1 then
  begin
    Geral.MensagemBox('Tamanho inicial n�o permite posi��o inicial:' +
    Mensagem, 'Erro', MB_OK+MB_ICONERROR);
    Sai();
    Exit;
  end;
  // verifica se o tamanho confere com a posi��o final - inicial
  if F_Tam <> F_PosF - F_PosI + 1 then
  begin
    Geral.MensagemBox('Tamanho de campo n�o confere com posi��o inicial e final:' +
    Mensagem, 'Erro', MB_OK+MB_ICONERROR);
    Sai();
    Exit;
  end;
  // verifica se o tamanho do texto a adicionar � igual ou inferior ao m�ximo
  xVal := Valor;
  tT := Length(xVal);
  // Ajusta tamanho do campo quando maior que o m�ximo
  if (AjustaAMenor <> aamNone) and (tT > F_Tam) then
  begin
    case AjustaAMenor of
      aamTxt:
      begin
        xVal := Copy(xVal, 1, F_Tam);
        tT := Length(xVal);
      end;
      aamNum:
      begin
        while tT > F_Tam do
        begin
          xVal := Copy(xVal, 2);
          tT := Length(xVal);
        end;
      end;
    end;
  end;
  if F_Tam < tT then
  begin
    Geral.MensagemBox('Tamanho do valor maior que o permitido para o campo:' +
    Mensagem, 'Erro', MB_OK+MB_ICONERROR);
    (*
    desmarcar! 2011-02-25 teste cfop
    xVal := Copy(xVal, 1, F_Tam);
    *)
    Sai();
    Exit;
  end;
  // Ajusta tamanho do campo quando menor que o m�ximo
  if F_Fmt = 'X' then
  begin
    // Texto. Preenchimento com espa�os em branco ap�s texto
    while tT < F_Tam do
    begin
      xVal := xVal + ' ';
      tT := Length(xVal);
    end;
  end else
  if F_Fmt = 'N' then
  begin
    // N�meros. Preenchimento com zeros antes do n�mero
    while tT < F_Tam do
    begin
      xVal := '0' + xVal;
      tT := Length(xVal);
    end;
  end else
  if F_Fmt = 'D' then
  begin
    // Data. Campo deve ter tamanho exato.
    if F_Tam <> tT then
    begin
      Geral.MensagemBox('Tamanho fixo de data diferente do permitido para o campo:' +
      Mensagem, 'Erro', MB_OK+MB_ICONERROR);
      Sai();
      Exit;
    end;
  end
  else
  begin
    Geral.MensagemBox('Filler n�o implementado: "' + F_Fmt + '"' +
    Mensagem, 'Erro', MB_OK+MB_ICONERROR);
    Exit;
    Sai();
  end;
  // Adiciona campo ao registro
  FtxtLin := FtxtLin + xVal;
  if F_PosF <> Length(FtxtLin) then
  begin
    Geral.MensagemBox('Tamanho da Linha maior que o permitido ap�s adicionar o campo:' +
    Mensagem, 'Erro', MB_OK+MB_ICONERROR);
    Exit;
    Sai();
  end;
  //
  Result := True;
  //
end;

function TFmSintegra_Arq.AdicionaLinhaAoMeGerado(const Texto: String; var Contador: Integer): Boolean;
var
  Txt: String;
  I, T: Integer;
  Dta: TDateTime;
  Ano, Mes, Dia: Integer;
  // 10
  TIPO, CNPJ, INSCEST, NCONTRIB, MUNICIPIO, UF, FAX, DATAINI, DATAFIN, CODCONV,
  CODNAT, CODFIN: String;
  // 11
  LOGRADOURO, NUMERO, COMPLEMENT, BAIRRO, CEP, CONTATO, TELEFONE: String;
  // 50
  EMISSAO, MODELO, SERIE, NUMNF, CODNATOP, EMITENTE, VALORTOT, BASEICMS,
  VALORICMS, VLRISENTO, OUTROS, ALIQUOTA, SITUACAO: String;
  // 51
  TOTALNF, VLRTOTIPI, BRANCOS: String;
  // 53
  CFOP, BASECALC, ICMSRETIDO, DESPACESS, CODANTEC: String;
  // 54
  SITTRIB, NUMITEM, CODPROD, QTDADE, VLRPROD, VLRDESC, BASESUBTRI, VLRIPI,
  ALIQICMS: String;
  // 70
  SUBSERIE, CIF_FOB: String;
  // 71
  EMISSAOC, MODELOC, SERIEC, NUMEROC, UFREM, CNPJREM, INSCESTREM, EMISSAON,
  MODELON, SERIEN: String;
  // 74
  DATA, CODIPROD, CODPOSSEM: String;
  // 75
  CODNCM, DESCRICAO, UNIDADE, ALIQIPI, REDBASEICM: String;
  // 85
  DECLARACAO, DATADECLAR, NATUREZA, REGISTRO, DATAREG, CONHECIM, DATACONHEC,
  TIPOCONHEC, PAIS, RESERVADO, DATAAVERB, NUMNFEXP: String;
  // 86
  REGEXPORT, VLRUNIT, RELACIONA: String;
  // 88
  //SUBTIPO, CST, NUMSER: String;
  // 90
  TIPOREG, TOTREG, TIPOREG1, TOTREG1, TIPOREG2, TOTREG2, TIPOREG3, TOTREG3,
  TIPOREG4, TOTREG4, TIPOREG5, TOTREG5, TIPOREG6, TOTREG6, TIPOREG7, TOTREG7,
  TIPOREG8, TOTREG8, BRANCO, NUMTIP90: String;
begin
  Result := False;
  //
  T := Length(Texto);
  if T <> 126 then
  begin
    Geral.MensagemBox('Tamanho inv�lido para a linha ' + FxLinha + '.' + #13#10
    + 'Tamanho da linha: ' + IntToStr(T) + #13#10 +
    'Tamanho correto: 126', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //
  Txt := '';
  for I := 1 to Length(Texto) do
    Txt := Txt + MLAGeral.CharToLetraMaiuscula(Ord(Texto[I]));
  //
  T := Length(Txt);
  if T <> 126 then
  begin
    Geral.MensagemBox('Tamanho inv�lido para a linha ' + FxLinha +
    ' ap�s transformar texto em mai�sculas. Ser� utilizado o texto original!' + #13#10
    + 'Tamanho da linha: ' + IntToStr(T) + #13#10 +
    'Tamanho correto: 126', 'Erro', MB_OK+MB_ICONERROR);
    Txt := Texto;
  end;
  //
  MeGerado.Lines.Add(Txt);
  Contador := Contador + 1;
  Result := True;
  //
  // Tabela
  TIPO      := Copy(Txt, 001, 002);
  case Geral.IMV(TIPO) of
    10:
    begin
      CNPJ      := Copy(Txt, 003, 014);
      INSCEST   := Copy(Txt, 017, 014);
      NCONTRIB  := Copy(Txt, 031, 035);
      MUNICIPIO := Copy(Txt, 066, 030);
      UF        := Copy(Txt, 096, 002);
      FAX       := Copy(Txt, 098, 010);
      DATAINI   := Copy(Txt, 108, 008);
      DATAFIN   := Copy(Txt, 116, 008);
      CODCONV   := Copy(Txt, 124, 001);
      CODNAT    := Copy(Txt, 125, 001);
      CODFIN    := Copy(Txt, 126, 001);
      // DATAINI
      Ano  := Geral.IMV(Copy(DATAINI, 1, 4));
      Mes  := Geral.IMV(Copy(DATAINI, 5, 2));
      Dia  := Geral.IMV(Copy(DATAINI, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      DATAINI := Geral.FDT(Dta, 1);
      // DATAFIN
      Ano  := Geral.IMV(Copy(DATAFIN, 1, 4));
      Mes  := Geral.IMV(Copy(DATAFIN, 5, 2));
      Dia  := Geral.IMV(Copy(DATAFIN, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      DATAFIN := Geral.FDT(Dta, 1);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra10, False, [
      'TIPO', 'CNPJ', 'INSCEST',
      'NCONTRIB', 'MUNICIPIO', 'UF',
      'FAX', 'DATAINI', 'DATAFIN',
      'CODCONV', 'CODNAT', 'CODFIN'], [
      ], [
      TIPO, CNPJ, INSCEST,
      NCONTRIB, MUNICIPIO, UF,
      FAX, DATAINI, DATAFIN,
      CODCONV, CODNAT, CODFIN], [
      ], False) then ;
    end;
    11:
    begin
      LOGRADOURO := Copy(Txt, 003, 034);
      NUMERO     := Copy(Txt, 037, 005);
      COMPLEMENT := Copy(Txt, 042, 022);
      BAIRRO     := Copy(Txt, 064, 015);
      CEP        := Copy(Txt, 079, 008);
      CONTATO    := Copy(Txt, 087, 028);
      TELEFONE   := Copy(Txt, 115, 012);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra11, False, [
      'TIPO', 'LOGRADOURO', 'NUMERO',
      'COMPLEMENT', 'BAIRRO', 'CEP',
      'CONTATO', 'TELEFONE'], [
      ], [
      TIPO, LOGRADOURO, NUMERO,
      COMPLEMENT, BAIRRO, CEP,
      CONTATO, TELEFONE], [
      ], False) then ;
    end;
    50:
    begin
      CNPJ      := Copy(Txt, 003, 014);
      INSCEST   := Copy(Txt, 017, 014);
      EMISSAO   := Copy(Txt, 031, 008);
      UF        := Copy(Txt, 039, 002);
      MODELO    := Copy(Txt, 041, 002);
      SERIE     := Copy(Txt, 043, 003);
      NUMNF     := Copy(Txt, 046, 006);
      CODNATOP  := Copy(Txt, 052, 004);
      EMITENTE  := Copy(Txt, 056, 001);
      VALORTOT  := Copy(Txt, 057, 013);
      BASEICMS  := Copy(Txt, 070, 013);
      VALORICMS := Copy(Txt, 083, 013);
      VLRISENTO := Copy(Txt, 096, 013);
      OUTROS    := Copy(Txt, 109, 013);
      ALIQUOTA  := Copy(Txt, 122, 004);
      SITUACAO  := Copy(Txt, 126, 001);
      //
      Ano  := Geral.IMV(Copy(EMISSAO, 1, 4));
      Mes  := Geral.IMV(Copy(EMISSAO, 5, 2));
      Dia  := Geral.IMV(Copy(EMISSAO, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      EMISSAO := Geral.FDT(Dta, 1);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra50, False, [
      'TIPO', 'CNPJ', 'INSCEST',
      'EMISSAO', 'UF', 'MODELO',
      'SERIE', 'NUMNF', 'CODNATOP',
      'EMITENTE', 'VALORTOT', 'BASEICMS',
      'VALORICMS', 'VLRISENTO', 'OUTROS',
      'ALIQUOTA', 'SITUACAO'], [
      ], [
      TIPO, CNPJ, INSCEST,
      EMISSAO, UF, MODELO,
      SERIE, NUMNF, CODNATOP,
      EMITENTE, VALORTOT, BASEICMS,
      VALORICMS, VLRISENTO, OUTROS,
      ALIQUOTA, SITUACAO], [
      ], False) then ;
    end;
    51:
    begin
      CNPJ      := Copy(Txt, 003, 014);
      INSCEST   := Copy(Txt, 017, 014);
      EMISSAO   := Copy(Txt, 031, 008);
      UF        := Copy(Txt, 039, 002);
      SERIE     := Copy(Txt, 041, 003);
      NUMNF     := Copy(Txt, 044, 006);
      CODNATOP  := Copy(Txt, 050, 004);
      TOTALNF   := Copy(Txt, 054, 013);
      VLRTOTIPI := Copy(Txt, 067, 013);
      VLRISENTO := Copy(Txt, 080, 013);
      OUTROS    := Copy(Txt, 093, 013);
      BRANCOS   := Copy(Txt, 106, 020);
      SITUACAO  := Copy(Txt, 126, 001);
      //
      Ano  := Geral.IMV(Copy(EMISSAO, 1, 4));
      Mes  := Geral.IMV(Copy(EMISSAO, 5, 2));
      Dia  := Geral.IMV(Copy(EMISSAO, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      EMISSAO := Geral.FDT(Dta, 1);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra51, False, [
      'TIPO', 'CNPJ', 'INSCEST',
      'EMISSAO', 'UF', 'SERIE',
      'NUMNF', 'CODNATOP', 'TOTALNF',
      'VLRTOTIPI', 'VLRISENTO', 'OUTROS',
      'BRANCOS', 'SITUACAO'], [
      ], [
      TIPO, CNPJ, INSCEST,
      EMISSAO, UF, SERIE,
      NUMNF, CODNATOP, TOTALNF,
      VLRTOTIPI, VLRISENTO, OUTROS,
      BRANCOS, SITUACAO], [
      ], False) then ;
    end;
    53:
    begin
      CNPJ      := Copy(Txt, 003, 014);
      INSCEST   := Copy(Txt, 017, 014);
      EMISSAO   := Copy(Txt, 031, 008);
      UF        := Copy(Txt, 039, 002);
      MODELO    := Copy(Txt, 041, 002);
      SERIE     := Copy(Txt, 043, 003);
      NUMERO    := Copy(Txt, 046, 006);
      CFOP      := Copy(Txt, 052, 004);
      EMITENTE  := Copy(Txt, 056, 001);
      BASECALC  := Copy(Txt, 057, 013);
      ICMSRETIDO:= Copy(Txt, 070, 013);
      DESPACESS := Copy(Txt, 083, 013);
      SITUACAO  := Copy(Txt, 096, 001);
      CODANTEC  := Copy(Txt, 097, 001);
      BRANCOS   := Copy(Txt, 098, 029);
      //
      Ano  := Geral.IMV(Copy(EMISSAO, 1, 4));
      Mes  := Geral.IMV(Copy(EMISSAO, 5, 2));
      Dia  := Geral.IMV(Copy(EMISSAO, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      EMISSAO := Geral.FDT(Dta, 1);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra53, False, [
      'TIPO', 'CNPJ', 'INSCEST',
      'EMISSAO', 'UF', 'MODELO',
      'SERIE', 'NUMERO', 'CFOP',
      'EMITENTE', 'BASECALC', 'ICMSRETIDO',
      'DESPACESS', 'SITUACAO', 'CODANTEC',
      'BRANCOS'], [
      ], [
      TIPO, CNPJ, INSCEST,
      EMISSAO, UF, MODELO,
      SERIE, NUMERO, CFOP,
      EMITENTE, BASECALC, ICMSRETIDO,
      DESPACESS, SITUACAO, CODANTEC,
      BRANCOS], [
      ], False) then ;
    end;
    54:
    begin
      CNPJ      := Copy(Txt, 003, 014);
      MODELO    := Copy(Txt, 017, 002);
      SERIE     := Copy(Txt, 019, 003);
      NUMNF     := Copy(Txt, 022, 006);
      CODNATOP  := Copy(Txt, 028, 004);
      SITTRIB   := Copy(Txt, 032, 003);
      NUMITEM   := Copy(Txt, 035, 003);
      CODPROD   := Copy(Txt, 038, 014);
      QTDADE    := Copy(Txt, 052, 011);
      VLRPROD   := Copy(Txt, 063, 012);
      VLRDESC   := Copy(Txt, 075, 012);
      BASECALC  := Copy(Txt, 087, 012);
      BASESUBTRI:= Copy(Txt, 099, 012);
      VLRIPI    := Copy(Txt, 111, 012);
      ALIQICMS  := Copy(Txt, 123, 004);
      //
      Ano  := Geral.IMV(Copy(EMISSAO, 1, 4));
      Mes  := Geral.IMV(Copy(EMISSAO, 5, 2));
      Dia  := Geral.IMV(Copy(EMISSAO, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      EMISSAO := Geral.FDT(Dta, 1);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra54, False, [
      'TIPO', 'CNPJ', 'MODELO',
      'SERIE', 'NUMNF', 'CODNATOP',
      'SITTRIB', 'NUMITEM', 'CODPROD',
      'QTDADE', 'VLRPROD', 'VLRDESC',
      'BASECALC', 'BASESUBTRI', 'VLRIPI',
      'ALIQICMS'], [
      ], [
      TIPO, CNPJ, MODELO,
      SERIE, NUMNF, CODNATOP,
      SITTRIB, NUMITEM, CODPROD,
      QTDADE, VLRPROD, VLRDESC,
      BASECALC, BASESUBTRI, VLRIPI,
      ALIQICMS], [
      ], False) then ;
    end;
    70:
    begin
      CNPJ      := Copy(Txt, 003, 014);
      INSCEST   := Copy(Txt, 017, 014);
      EMISSAO   := Copy(Txt, 031, 008);
      UF        := Copy(Txt, 039, 002);
      MODELO    := Copy(Txt, 041, 002);
      SERIE     := Copy(Txt, 043, 001);
      SUBSERIE  := Copy(Txt, 044, 002);
      NUMNF     := Copy(Txt, 046, 006);
      CODNATOP  := Copy(Txt, 052, 004);
      VALORTOT  := Copy(Txt, 056, 013);
      BASEICMS  := Copy(Txt, 069, 014);
      VALORICMS := Copy(Txt, 083, 014);
      VLRISENTO := Copy(Txt, 097, 014);
      OUTROS    := Copy(Txt, 111, 014);
      CIF_FOB   := Copy(Txt, 125, 001);
      SITUACAO  := Copy(Txt, 126, 001);
      //
      Ano  := Geral.IMV(Copy(EMISSAO, 1, 4));
      Mes  := Geral.IMV(Copy(EMISSAO, 5, 2));
      Dia  := Geral.IMV(Copy(EMISSAO, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      EMISSAO := Geral.FDT(Dta, 1);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra70, False, [
      'TIPO', 'CNPJ', 'INSCEST',
      'EMISSAO', 'UF', 'MODELO',
      'SERIE', 'SUBSERIE', 'NUMNF',
      'CODNATOP', 'VALORTOT', 'BASEICMS',
      'VALORICMS', 'VLRISENTO', 'OUTROS',
      'CIF_FOB', 'SITUACAO'], [
      ], [
      TIPO, CNPJ, INSCEST,
      EMISSAO, UF, MODELO,
      SERIE, SUBSERIE, NUMNF,
      CODNATOP, VALORTOT, BASEICMS,
      VALORICMS, VLRISENTO, OUTROS,
      CIF_FOB, SITUACAO], [
      ], False) then ;
    end;
    71:
    begin
      CNPJ       := Copy(Txt, 003, 014);
      INSCEST    := Copy(Txt, 017, 014);
      EMISSAOC   := Copy(Txt, 031, 008);
      UF         := Copy(Txt, 039, 002);
      MODELOC    := Copy(Txt, 041, 002);
      SERIEC     := Copy(Txt, 043, 001);
      SUBSERIE   := Copy(Txt, 044, 002);
      NUMEROC    := Copy(Txt, 046, 006);
      UFREM      := Copy(Txt, 052, 002);
      CNPJREM    := Copy(Txt, 054, 014);
      INSCESTREM := Copy(Txt, 068, 014);
      EMISSAON   := Copy(Txt, 082, 008);
      MODELON    := Copy(Txt, 090, 002);
      SERIEN     := Copy(Txt, 092, 003);
      NUMNF      := Copy(Txt, 095, 006);
      TOTALNF    := Copy(Txt, 101, 014);
      BRANCOS    := Copy(Txt, 115, 012);
      //
      Ano  := Geral.IMV(Copy(EMISSAOC, 1, 4));
      Mes  := Geral.IMV(Copy(EMISSAOC, 5, 2));
      Dia  := Geral.IMV(Copy(EMISSAOC, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      EMISSAOC := Geral.FDT(Dta, 1);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra71, False, [
      'TIPO', 'CNPJ', 'INSCEST',
      'EMISSAOC', 'UF', 'MODELOC',
      'SERIEC', 'SUBSERIE', 'NUMEROC',
      'UFREM', 'CNPJREM', 'INSCESTREM',
      'EMISSAON', 'MODELON', 'SERIEN',
      'NUMNF', 'TOTALNF', 'BRANCOS'], [
      ], [
      TIPO, CNPJ, INSCEST,
      EMISSAOC, UF, MODELOC,
      SERIEC, SUBSERIE, NUMEROC,
      UFREM, CNPJREM, INSCESTREM,
      EMISSAON, MODELON, SERIEN,
      NUMNF, TOTALNF, BRANCOS], [
      ], False) then ;
    end;
    74:
    begin
      DATA      := Copy(Txt, 003, 008);
      CODIPROD  := Copy(Txt, 011, 014);
      QTDADE    := Copy(Txt, 025, 013);
      VLRPROD   := Copy(Txt, 038, 013);
      CODPOSSEM := Copy(Txt, 051, 001);
      CNPJ      := Copy(Txt, 052, 014);
      INSCEST   := Copy(Txt, 066, 014);
      UF        := Copy(Txt, 080, 002);
      BRANCOS   := Copy(Txt, 045, 082);
      //
      Ano  := Geral.IMV(Copy(DATA, 1, 4));
      Mes  := Geral.IMV(Copy(DATA, 5, 2));
      Dia  := Geral.IMV(Copy(DATA, 7, 2));
      if (Ano=0) then
        Dta:= 0
      else
        Dta:= EncodeDate(Ano, Mes, dia);
      DATA := Geral.FDT(Dta, 1);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra74, False, [
      'TIPO', 'DATA', 'CODIPROD',
      'QTDADE', 'VLRPROD', 'CODPOSSEM',
      'CNPJ', 'INSCEST', 'UF',
      'BRANCOS'], [
      ], [
      TIPO, DATA, CODIPROD,
      QTDADE, VLRPROD, CODPOSSEM,
      CNPJ, INSCEST, UF,
      BRANCOS], [
      ], False) then ;
    end;
    75:
    begin
      DATAINI    := Copy(Txt, 003, 008);
      DATAFIN    := Copy(Txt, 011, 008);
      CODPROD    := Copy(Txt, 019, 014);
      CODNCM     := Copy(Txt, 033, 008);
      DESCRICAO  := Copy(Txt, 053, 041);
      UNIDADE    := Copy(Txt, 094, 006);
      ALIQIPI    := Copy(Txt, 100, 005);
      ALIQICMS   := Copy(Txt, 105, 004);
      REDBASEICM := Copy(Txt, 109, 005);
      BASESUBTRI := Copy(Txt, 114, 013);
      //
      Ano  := Geral.IMV(Copy(DATAINI, 1, 4));
      Mes  := Geral.IMV(Copy(DATAINI, 5, 2));
      Dia  := Geral.IMV(Copy(DATAINI, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      DATAINI := Geral.FDT(Dta, 1);
      //
      Ano  := Geral.IMV(Copy(DATAFIN, 1, 4));
      Mes  := Geral.IMV(Copy(DATAFIN, 5, 2));
      Dia  := Geral.IMV(Copy(DATAFIN, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      DATAFIN := Geral.FDT(Dta, 1);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra75, False, [
      'TIPO', 'DATAINI', 'DATAFIN',
      'CODPROD', 'CODNCM', 'DESCRICAO',
      'UNIDADE', 'ALIQIPI', 'ALIQICMS',
      'REDBASEICM', 'BASESUBTRI'], [
      ], [
      TIPO, DATAINI, DATAFIN,
      CODPROD, CODNCM, DESCRICAO,
      UNIDADE, ALIQIPI, ALIQICMS,
      REDBASEICM, BASESUBTRI], [
      ], False) then ;
    end;
    85:
    begin
      DECLARACAO := Copy(Txt, 003, 011);
      DATADECLAR := Copy(Txt, 014, 008);
      NATUREZA   := Copy(Txt, 022, 001);
      REGISTRO   := Copy(Txt, 023, 012);
      DATAREG    := Copy(Txt, 035, 008);
      CONHECIM   := Copy(Txt, 043, 016);
      DATACONHEC := Copy(Txt, 059, 008);
      TIPOCONHEC := Copy(Txt, 067, 002);
      PAIS       := Copy(Txt, 069, 004);
      RESERVADO  := Copy(Txt, 073, 008);
      DATAAVERB  := Copy(Txt, 081, 008);
      NUMNFEXP   := Copy(Txt, 089, 006);
      EMISSAO    := Copy(Txt, 095, 008);
      MODELO     := Copy(Txt, 103, 002);
      SERIE      := Copy(Txt, 105, 003);
      BRANCOS    := Copy(Txt, 108, 019);
      // DATADECLAR
      Ano  := Geral.IMV(Copy(DATADECLAR, 1, 4));
      Mes  := Geral.IMV(Copy(DATADECLAR, 5, 2));
      Dia  := Geral.IMV(Copy(DATADECLAR, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      DATADECLAR := Geral.FDT(Dta, 1);
      // DATAREG
      Ano  := Geral.IMV(Copy(DATAREG, 1, 4));
      Mes  := Geral.IMV(Copy(DATAREG, 5, 2));
      Dia  := Geral.IMV(Copy(DATAREG, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      DATAREG := Geral.FDT(Dta, 1);
      // DATACONHEC
      Ano  := Geral.IMV(Copy(DATACONHEC, 1, 4));
      Mes  := Geral.IMV(Copy(DATACONHEC, 5, 2));
      Dia  := Geral.IMV(Copy(DATACONHEC, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      DATACONHEC := Geral.FDT(Dta, 1);
      // DATAAVERB
      Ano  := Geral.IMV(Copy(DATAAVERB, 1, 4));
      Mes  := Geral.IMV(Copy(DATAAVERB, 5, 2));
      Dia  := Geral.IMV(Copy(DATAAVERB, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      DATAAVERB := Geral.FDT(Dta, 1);
      // EMISSAO
      Ano  := Geral.IMV(Copy(EMISSAO, 1, 4));
      Mes  := Geral.IMV(Copy(EMISSAO, 5, 2));
      Dia  := Geral.IMV(Copy(EMISSAO, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      EMISSAO := Geral.FDT(Dta, 1);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra85, False, [
      'TIPO', 'DECLARACAO', 'DATADECLAR',
      'NATUREZA', 'REGISTRO', 'DATAREG',
      'CONHECIM', 'DATACONHEC', 'TIPOCONHEC',
      'PAIS', 'RESERVADO', 'DATAAVERB',
      'NUMNFEXP', 'EMISSAO', 'MODELO',
      'SERIE', 'BRANCOS'], [
      ], [
      TIPO, DECLARACAO, DATADECLAR,
      NATUREZA, REGISTRO, DATAREG,
      CONHECIM, DATACONHEC, TIPOCONHEC,
      PAIS, RESERVADO, DATAAVERB,
      NUMNFEXP, EMISSAO, MODELO,
      SERIE, BRANCOS], [
      ], False) then ;
    end;
    86:
    begin
      REGEXPORT := Copy(Txt, 003, 012);
      DATAREG   := Copy(Txt, 015, 008);
      CNPJREM   := Copy(Txt, 023, 014);
      INSCEST   := Copy(Txt, 037, 014);
      UF        := Copy(Txt, 051, 002);
      NUMNF     := Copy(Txt, 053, 006);
      EMISSAO   := Copy(Txt, 059, 008);
      MODELO    := Copy(Txt, 067, 002);
      SERIE     := Copy(Txt, 069, 003);
      CODPROD   := Copy(Txt, 072, 014);
      QTDADE    := Copy(Txt, 086, 011);
      VLRUNIT   := Copy(Txt, 097, 012);
      VLRPROD   := Copy(Txt, 109, 012);
      RELACIONA := Copy(Txt, 121, 001);
      BRANCOS   := Copy(Txt, 122, 005);
      //
      Ano  := Geral.IMV(Copy(DATAREG, 1, 4));
      Mes  := Geral.IMV(Copy(DATAREG, 5, 2));
      Dia  := Geral.IMV(Copy(DATAREG, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      DATAREG := Geral.FDT(Dta, 1);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra86, False, [
      'TIPO', 'REGEXPORT', 'DATAREG',
      'CNPJREM', 'INSCEST', 'UF',
      'NUMNF', 'EMISSAO', 'MODELO',
      'SERIE', 'CODPROD', 'QTDADE',
      'VLRUNIT', 'VLRPROD', 'RELACIONA',
      'BRANCOS'], [
      ], [
      TIPO, REGEXPORT, DATAREG,
      CNPJREM, INSCEST, UF,
      NUMNF, EMISSAO, MODELO,
      SERIE, CODPROD, QTDADE,
      VLRUNIT, VLRPROD, RELACIONA,
      BRANCOS], [
      ], False) then ;
    end;
    88:
    begin
      // N�o existe no manual!
{
      SUBTIPO   := Copy(Txt, 0xx, 0xx);
      CNPJ      := Copy(Txt, 0xx, 0xx);
      MODELO    := Copy(Txt, 0xx, 0xx);
      SERIE     := Copy(Txt, 0xx, 0xx);
      NUMERO    := Copy(Txt, 0xx, 0xx);
      CFOP      := Copy(Txt, 0xx, 0xx);
      CST       := Copy(Txt, 0xx, 0xx);
      NUMITEM   := Copy(Txt, 0xx, 0xx);
      CODPROD   := Copy(Txt, 0xx, 0xx);
      NUMSER    := Copy(Txt, 0xx, 0xx);
      BRANCOS   := Copy(Txt, 0xx, 0xx);
      //
      Ano  := Geral.IMV(Copy(EMISSAO, 1, 4));
      Mes  := Geral.IMV(Copy(EMISSAO, 5, 2));
      Dia  := Geral.IMV(Copy(EMISSAO, 7, 2));
      if (Ano=0) then
        Dta := 0
      else
        Dta := EncodeDate(Ano, Mes, dia);
      EMISSAO := Geral.FDT(Dta, 1);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra88, False, [
      'TIPO', 'SUBTIPO', 'CNPJ',
      'MODELO', 'SERIE', 'NUMERO',
      'CFOP', 'CST', 'NUMITEM',
      'CODPROD', 'NUMSER', 'BRANCOS'], [
      ], [
      TIPO, SUBTIPO, CNPJ,
      MODELO, SERIE, NUMERO,
      CFOP, CST, NUMITEM,
      CODPROD, NUMSER, BRANCOS], [
      ], False) then ;
}
    end;
    90:
    begin
      CNPJ      := Copy(Txt, 003, 014);
      INSCEST   := Copy(Txt, 017, 014);
      TIPOREG   := Copy(Txt, 031, 002);
      TOTREG    := Copy(Txt, 033, 008);
      TIPOREG1  := Copy(Txt, 041, 002);
      TOTREG1   := Copy(Txt, 043, 008);
      TIPOREG2  := Copy(Txt, 051, 002);
      TOTREG2   := Copy(Txt, 053, 008);
      TIPOREG3  := Copy(Txt, 061, 002);
      TOTREG3   := Copy(Txt, 063, 008);
      TIPOREG4  := Copy(Txt, 071, 002);
      TOTREG4   := Copy(Txt, 073, 008);
      TIPOREG5  := Copy(Txt, 081, 002);
      TOTREG5   := Copy(Txt, 083, 008);
      TIPOREG6  := Copy(Txt, 091, 002);
      TOTREG6   := Copy(Txt, 093, 008);
      TIPOREG7  := Copy(Txt, 101, 002);
      TOTREG7   := Copy(Txt, 103, 008);
      TIPOREG8  := Copy(Txt, 111, 002);
      TOTREG8   := Copy(Txt, 113, 008);
      BRANCO    := Copy(Txt, 121, 004);
      NUMTIP90  := Copy(Txt, 125, 001);
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegra90, False, [
      'TIPO', 'CNPJ', 'INSCEST',
      'TIPOREG', 'TOTREG', 'TIPOREG1',
      'TOTREG1', 'TIPOREG2', 'TOTREG2',
      'TIPOREG3', 'TOTREG3', 'TIPOREG4',
      'TOTREG4', 'TIPOREG5', 'TOTREG5',
      'TIPOREG6', 'TOTREG6', 'TIPOREG7',
      'TOTREG7', 'TIPOREG8', 'TOTREG8',
      'BRANCO', 'NUMTIP90'], [
      ], [
      TIPO, CNPJ, INSCEST,
      TIPOREG, TOTREG, TIPOREG1,
      TOTREG1, TIPOREG2, TOTREG2,
      TIPOREG3, TOTREG3, TIPOREG4,
      TOTREG4, TIPOREG5, TOTREG5,
      TIPOREG6, TOTREG6, TIPOREG7,
      TOTREG7, TIPOREG8, TOTREG8,
      BRANCO, NUMTIP90], [
      ], False) then ;
    end;
  end;
end;

procedure TFmSintegra_Arq.BitBtn1Click(Sender: TObject);
begin
  SetaTodosCampos(1);
end;

procedure TFmSintegra_Arq.BitBtn3Click(Sender: TObject);
begin
  SetaTodosCampos(0);
end;

procedure TFmSintegra_Arq.BitBtn4Click(Sender: TObject);
begin
  ReopenDifere(True);
end;

procedure TFmSintegra_Arq.BtComparaClick(Sender: TObject);
var
  I, Tipo: Integer;
  QrA, QrB: TmySQLQuery;
  procedure Compara(MyQr, QrCo: TmySQLQuery; MyDs, CoDs: TDataSource);
  var
    TipoReg, ItemReg, J: Integer;
    Campo, ValMy, ValCo: String;
    Continua: Boolean;
    xMy, xCo: String;
  begin
    MyQr.Database := DModG.MyPID_DB;
    MyQr.Close;
    MyQr.Open;
    GradeMy.DataSource := MyDs;
    //
    QrCo.Database := Dmod.MyDB;
    QrCo.Close;
    QrCo.Open;
    GradeCo.DataSource := CoDs;
    //
    if MyQr.RecordCount > QrCo.RecordCount then
    begin
      QrA := MyQr;
      QrB := QrCo;
    end
    else begin
      QrA := QrCo;
      QrB := MyQr;
    end;
    //
    for J := 0 to QrA.FieldCount - 1 do
    begin
      Campo   := QrA.Fields[J].FieldName;
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegraEC, False, [
      'Campo'], ['Ativo'], [Campo], [1], False) then (*Exit*);
    end;
    //
    QrB.First;
    QrA.First;
    while not QrA.Eof do
    begin
      Update;
      Application.ProcessMessages;
      //
      for J := 0 to QrA.FieldCount - 1 do
      begin
        if QrB.Fields[J].Value <> QrA.Fields[J].Value then
        begin
          TipoReg := Geral.IMV(Copy(QrA.Name, Length(QrA.Name) -1));
          ItemReg := QrA.RecNo;
          Campo   := QrA.Fields[J].FieldName;
          ValMy   := Geral.VariantToString(MyQr.Fields[J].Value);
          ValCo   := Geral.VariantToString(QrCo.Fields[J].Value);
          //
          if (Tipo = 54) and (Campo = 'CODPROD') then
          begin
             xMy := Geral.SoNumero_TT(ValMy);
             xCo := Geral.SoNumero_TT(ValCo);
             if (xMy = ValMy) and (xCo = ValCo) and (Geral.IMV(xMy) = Geral.IMV(xCo)) then
               Continua := False
             else
               Continua := True;  
          end else Continua := True;
          if Continua then
            if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSintegraEr, False, [
            'TipoReg', 'ItemReg', 'Campo',
            'ValMy', 'ValCo'], [
            ], [
            TipoReg, ItemReg, Campo,
            ValMy, ValCo], [
            ], False) then (*Exit*);
        end;
      end;
      QrB.Next;
      QrA.Next;
      //
      //Sleep(1000);
    end;
  end;
begin
  Screen.Cursor := crHourGlass;
  try
    Memo1.Lines.Clear;
    FSintegraEC := GradeCriar.RecriaTempTableNovo(ntrttSintegraEC, DmodG.QrUpdPID1, False);
    FSintegraEr := GradeCriar.RecriaTempTableNovo(ntrttSintegraEr, DmodG.QrUpdPID1, False);
    QrDifere.Close;
    for I := 0 to CGTipos.Items.Count - 1 do
    begin
      if CGTipos.Checked[I] then
      begin
        Tipo := Geral.IMV(CGTipos.Items[I]);
        case Tipo of
          10:  Compara(QrSintegra10, QrR10, DsSintegra10, DsR10);
          11:  Compara(QrSintegra11, QrR11, DsSintegra11, DsR11);
          50:  Compara(QrSintegra50, QrR50, DsSintegra50, DsR50);
          51:  Compara(QrSintegra51, QrR51, DsSintegra51, DsR51);
          53:  Compara(QrSintegra53, QrR53, DsSintegra53, DsR53);
          54:  Compara(QrSintegra54, QrR54, DsSintegra54, DsR54);
          70:  Compara(QrSintegra70, QrR70, DsSintegra70, DsR70);
          71:  Compara(QrSintegra71, QrR71, DsSintegra71, DsR71);
          74:  Compara(QrSintegra74, QrR74, DsSintegra74, DsR74);
          75:  Compara(QrSintegra75, QrR75, DsSintegra75, DsR75);
          85:  Compara(QrSintegra85, QrR85, DsSintegra85, DsR85);
          86:  Compara(QrSintegra86, QrR86, DsSintegra86, DsR86);
          88:  Compara(QrSintegra88, QrR88, DsSintegra88, DsR88);
          90:  Compara(QrSintegra90, QrR90, DsSintegra90, DsR90);
          else Geral.MensagemBox('Tipo n�o iplementado! AVISE A DERMATEK!',
          'Aviso', MB_OK+MB_ICONWARNING);
        end;
      end;
    end;
    TbCampos.Open;
    ReopenDifere(False);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSintegra_Arq.BtExclCoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclCo, BtExclCo);
end;

procedure TFmSintegra_Arq.BtGeraClick(Sender: TObject);
  procedure ReabreCabA(dIni, dFim, Ordem: String);
  begin
    QrCabA.Close;
    QrCabA.SQL.Clear;
{
    QrCabA.SQL.Add('SELECT IF(nfea.ide_tpEmis=0, nfea.dest_CNPJ,');
    QrCabA.SQL.Add('  IF(nfea.emit_CNPJ=:Pa, IF(nfea.dest_CNPJ <> "",');
    QrCabA.SQL.Add('  nfea.dest_CNPJ, nfea.dest_CPF),');
    QrCabA.SQL.Add('  nfea.emit_CNPJ)) CNPJ_CPF,');
    QrCabA.SQL.Add('IF(nfea.ide_tpEmis=0, nfea.dest_IE,');
    QrCabA.SQL.Add('  IF(nfea.emit_CNPJ=:Pa, nfea.dest_IE,');
    QrCabA.SQL.Add('  nfea.emit_IE)) IE,');
    QrCabA.SQL.Add('IF(nfea.ide_tpEmis=0, nfea.dest_UF,');
    QrCabA.SQL.Add('  IF(nfea.emit_CNPJ=:Pa, nfea.dest_UF,');
    QrCabA.SQL.Add('  nfea.emit_UF)) UF,');
    QrCabA.SQL.Add('IF(nfea.emit_CNPJ=:Pa, 1, 0) EhOEmitente,');
    QrCabA.SQL.Add('nfea.*');
    QrCabA.SQL.Add('FROM nfecaba nfea');
    QrCabA.SQL.Add('WHERE (');
    QrCabA.SQL.Add('  (nfea.emit_CNPJ=:Pa) OR (nfea.dest_CNPJ=:Pa)');
    QrCabA.SQL.Add(') AND (nfea.DataFiscal BETWEEN :Pb AND :Pc)');
    QrCabA.SQL.Add('ORDER BY ' + Ordem);
}

{
    QrCabA.SQL.Add('SELECT IF(nfea.ide_tpEmis=0, nfea.dest_CNPJ,');
    QrCabA.SQL.Add('  IF(nfea.emit_CNPJ="' + CNPJ + '", IF(nfea.dest_CNPJ <> "",');
    QrCabA.SQL.Add('  nfea.dest_CNPJ, nfea.dest_CPF),');
    QrCabA.SQL.Add('  nfea.emit_CNPJ)) CNPJ_CPF,');
    QrCabA.SQL.Add('IF(nfea.ide_tpEmis=0, nfea.dest_IE,');
    QrCabA.SQL.Add('  IF(nfea.emit_CNPJ="' + CNPJ + '", nfea.dest_IE,');
    QrCabA.SQL.Add('  nfea.emit_IE)) IE,');
    QrCabA.SQL.Add('IF(nfea.ide_tpEmis=0, nfea.dest_UF,');
    QrCabA.SQL.Add('  IF(nfea.emit_CNPJ="' + CNPJ + '", nfea.dest_UF,');
    QrCabA.SQL.Add('  nfea.emit_UF)) UF,');
    QrCabA.SQL.Add('IF(nfea.emit_CNPJ="' + CNPJ + '", 1, 0) EhOEmitente,');
    QrCabA.SQL.Add(' nfea.*');
    QrCabA.SQL.Add('FROM nfecaba nfea');
    QrCabA.SQL.Add('WHERE (');
    QrCabA.SQL.Add(' (Status IN (100,101,102,301,302)) OR (nfea.emit_CNPJ<>"' + CNPJ + '"))');
    QrCabA.SQL.Add('AND (');
    QrCabA.SQL.Add('  (nfea.emit_CNPJ="' + CNPJ + '") OR (nfea.dest_CNPJ="' + CNPJ + '")');
    QrCabA.SQL.Add(') AND (nfea.DataFiscal BETWEEN "' + dIni + '" AND "' + dFim + '")');
    QrCabA.SQL.Add('ORDER BY nfea.DataFiscal');
    QrCabA.ParamByName('Pa').AsString := CNPJ;
    QrCabA.ParamByName('Pb').AsString := dIni;
    QrCabA.ParamByName('Pc').AsString := dFim;
}
    QrCabA.SQL.Add('SELECT');
    QrCabA.SQL.Add('');
    QrCabA.SQL.Add('IF(nfea.CodInfoEmit=' + FEmpresa_TXT + ',  IF(nfea.dest_CNPJ <> "",');
    QrCabA.SQL.Add('  nfea.dest_CNPJ, nfea.dest_CPF),');
    QrCabA.SQL.Add('  IF(nfea.emit_CNPJ <> "", nfea.emit_CNPJ,');
    QrCabA.SQL.Add('  nfea.emit_CPF))  CNPJ_CPF,');
    QrCabA.SQL.Add('');
    QrCabA.SQL.Add('IF(nfea.CodInfoEmit=' + FEmpresa_TXT + ',  nfea.dest_IE,  nfea.emit_IE) IE,');
    QrCabA.SQL.Add('');
    QrCabA.SQL.Add('IF(nfea.CodInfoEmit=' + FEmpresa_TXT + ',  nfea.dest_UF,  nfea.emit_UF) UF,');
    QrCabA.SQL.Add('');
    QrCabA.SQL.Add('IF(nfea.CodInfoEmit=' + FEmpresa_TXT + ', 1, 0) EhOEmitente,');
    QrCabA.SQL.Add('nfea.*');
    QrCabA.SQL.Add('');
    QrCabA.SQL.Add('FROM nfecaba nfea');
    QrCabA.SQL.Add('WHERE (');
    QrCabA.SQL.Add(' (Status IN (100,101,102,301,302)) OR (nfea.CodInfoEmit<>' + FEmpresa_TXT + '))');
    QrCabA.SQL.Add('AND (');
    QrCabA.SQL.Add('  (nfea.CodInfoEmit=' + FEmpresa_TXT + ') OR (nfea.CodInfoDest=' + FEmpresa_TXT + ')');
    QrCabA.SQL.Add(') AND (nfea.DataFiscal BETWEEN "' + dIni + '" AND "' + dFim + '")');
    QrCabA.SQL.Add('ORDER BY nfea.DataFiscal');
    QrCabA.Open;
  end;
var
  CNPJ, dIni, dFim, Arq, Dir, Arquivo: String;
  Continua: Boolean;
begin
  MeAvisos.Lines.Clear;
  FQtd10 := 0; FQtd11 := 0; FQtd50 := 0; FQtd51 := 0; FQtd53 := 0; FQtd54 := 0;
  FQtd55 := 0; FQtd56 := 0; FQtd57 := 0; FQtd60 := 0; FQtd61 := 0; FQtd70 := 0;
  FQtd71 := 0; FQtd74 := 0; FQtd75 := 0; FQtd76 := 0; FQtd77 := 0; FQtd85 := 0;
  FQtd86 := 0; FQtd90 := 0; FQtd99 := 0; FQtd00 := 0;
  //
  if EdEmpresa.ValueVariant = 0 then
  begin
    Geral.MensagemBox('Informe a Empresa!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  FEmpresa_TXT := FormatFloat('0', DModG.QrEmpresasCodigo.Value);
  //
  FSintegra10 := GradeCriar.RecriaTempTableNovo(ntrttSintegra10, DmodG.QrUpdPID1, False);
  FSintegra11 := GradeCriar.RecriaTempTableNovo(ntrttSintegra11, DmodG.QrUpdPID1, False);
  FSintegra50 := GradeCriar.RecriaTempTableNovo(ntrttSintegra50, DmodG.QrUpdPID1, False);
  FSintegra51 := GradeCriar.RecriaTempTableNovo(ntrttSintegra51, DmodG.QrUpdPID1, False);
  FSintegra53 := GradeCriar.RecriaTempTableNovo(ntrttSintegra53, DmodG.QrUpdPID1, False);
  FSintegra54 := GradeCriar.RecriaTempTableNovo(ntrttSintegra54, DmodG.QrUpdPID1, False);
  FSintegra70 := GradeCriar.RecriaTempTableNovo(ntrttSintegra70, DmodG.QrUpdPID1, False);
  FSintegra71 := GradeCriar.RecriaTempTableNovo(ntrttSintegra71, DmodG.QrUpdPID1, False);
  FSintegra74 := GradeCriar.RecriaTempTableNovo(ntrttSintegra74, DmodG.QrUpdPID1, False);
  FSintegra75 := GradeCriar.RecriaTempTableNovo(ntrttSintegra75, DmodG.QrUpdPID1, False);
  FSintegra85 := GradeCriar.RecriaTempTableNovo(ntrttSintegra85, DmodG.QrUpdPID1, False);
  FSintegra86 := GradeCriar.RecriaTempTableNovo(ntrttSintegra86, DmodG.QrUpdPID1, False);
  FSintegra88 := GradeCriar.RecriaTempTableNovo(ntrttSintegra88, DmodG.QrUpdPID1, False);
  FSintegra90 := GradeCriar.RecriaTempTableNovo(ntrttSintegra90, DmodG.QrUpdPID1, False);
  FSintegraPM := GradeCriar.RecriaTempTableNovo(ntrttSintegraPM, DmodG.QrUpdPID1, False);
  //
  QrDtaFis.Close;
  QrDtaFis.Open;
  if QrDtaFis.RecordCount > 0 then
    Geral.MensagemBox('Existem ' + FormatFloat('0', QrDtaFis.RecordCount) +
    ' NFs com a data fiscal indefinida!', 'Erro', MB_OK+MB_ICONERROR);
  //
  if CkConsertaNome.Checked then
  begin
    MyObjects.Informa(LaAviso, True, 'Corrigindo nomes de uso e consumo');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE gragru1 gg1, PQ pq');
    Dmod.QrUpd.SQL.Add('SET gg1.Nome=pq.Nome');
    Dmod.QrUpd.SQL.Add('WHERE pq.Codigo=gg1.Nivel1');
    Dmod.QrUpd.SQL.Add('AND gg1.PrdGrupTip=-2');
    Dmod.QrUpd.ExecSQL;
    MyObjects.Informa(LaAviso, False, 'Nomes de uso e consumo corrigidos.');
  end;
  QrEnti.Close;
  QrEnti.Params[0].AsInteger := DModG.QrEmpresasCodigo.Value;
  QrEnti.Open;
  //
  DModG.ReopenParamsEmp(DModG.QrEmpresasCodigo.Value);
  //
  CNPJ := Geral.SoNumero_TT(QrEntiCNPJ_CPF.Value);
  dIni := Geral.FDT(TPDataIni.Date, 1);
  dFim := Geral.FDT(TPDataFim.Date, 1);
  //
  MeGerado.Lines.Clear;
  if not IncluiRegistro10() then Exit;
  if not IncluiRegistro11() then Exit;
  //
  if (DModG.QrParamsEmpReg50.Value = 1)
  or (DModG.QrParamsEmpReg51.Value = 1)
  or (DModG.QrParamsEmpReg53.Value = 1)
  then begin
    ReabreCabA(dIni, dFim, 'nfea.DataFiscal');
    {
    QrCabA.Close;
    QrCabA.SQL.Clear;
    QrCabA.SQL.Add('SELECT IF(nfea.ide_tpEmis=0, nfea.dest_CNPJ,');
    QrCabA.SQL.Add('  IF(nfea.emit_CNPJ=:Pa, IF(nfea.dest_CNPJ <> "",');
    QrCabA.SQL.Add('  nfea.dest_CNPJ, nfea.dest_CPF),');
    QrCabA.SQL.Add('  nfea.emit_CNPJ)) CNPJ_CPF,');
    QrCabA.SQL.Add('IF(nfea.ide_tpEmis=0, nfea.dest_IE,');
    QrCabA.SQL.Add('  IF(nfea.emit_CNPJ=:Pa, nfea.dest_IE,');
    QrCabA.SQL.Add('  nfea.emit_IE)) IE,');
    QrCabA.SQL.Add('IF(nfea.ide_tpEmis=0, nfea.dest_UF,');
    QrCabA.SQL.Add('  IF(nfea.emit_CNPJ=:Pa, nfea.dest_UF,');
    QrCabA.SQL.Add('  nfea.emit_UF)) UF,');
    QrCabA.SQL.Add('nfea.*');
    QrCabA.SQL.Add('FROM nfecaba nfea');
    QrCabA.SQL.Add('WHERE (');
    QrCabA.SQL.Add('  (nfea.emit_CNPJ=:Pa) OR (nfea.dest_CNPJ=:Pa)');
    QrCabA.SQL.Add(') AND (nfea.DataFiscal BETWEEN :Pb AND :Pc)');
    QrCabA.SQL.Add('ORDER BY nfea.DataFiscal');
    QrCabA.SQL.Add('');
    QrCabA.SQL.Add('');
    QrCabA.Params[00].AsString  := CNPJ;
    QrCabA.Params[01].AsString  := CNPJ;
    QrCabA.Params[02].AsString  := CNPJ;
    QrCabA.Params[03].AsString  := dIni;
    QrCabA.Params[04].AsString  := dFim;
    QrCabA.Open;
    }
    if DModG.QrParamsEmpReg50.Value = 1 then
      if not IncluiRegistro50() then Exit;
    if DModG.QrParamsEmpReg51.Value = 1 then
      if not IncluiRegistro51() then Exit;
    if DModG.QrParamsEmpReg53.Value = 1 then
      if not IncluiRegistro53() then Exit;
  end;
  if (DModG.QrParamsEmpReg54.Value = 1)
  or (DModG.QrParamsEmpReg56.Value = 1) then
  begin
    ReabreCabA(dIni, dFim, 'CNPJ_CPF, nfea.ide_serie, nfea.ide_nNF');
    {
    QrCabA.Close;
    QrCabA.SQL.Clear;
    QrCabA.SQL.Add('SELECT IF(nfea.ide_tpEmis=0, nfea.dest_CNPJ,');
    QrCabA.SQL.Add('  IF(nfea.emit_CNPJ=:Pa, IF(nfea.dest_CNPJ <> "",');
    QrCabA.SQL.Add('  nfea.dest_CNPJ, nfea.dest_CPF),');
    QrCabA.SQL.Add('  nfea.emit_CNPJ)) CNPJ_CPF,');
    QrCabA.SQL.Add('IF(nfea.ide_tpEmis=0, nfea.dest_IE,');
    QrCabA.SQL.Add('  IF(nfea.emit_CNPJ=:Pa, nfea.dest_IE,');
    QrCabA.SQL.Add('  nfea.emit_IE)) IE,');
    QrCabA.SQL.Add('IF(nfea.ide_tpEmis=0, nfea.dest_UF,');
    QrCabA.SQL.Add('  IF(nfea.emit_CNPJ=:Pa, nfea.dest_UF,');
    QrCabA.SQL.Add('  nfea.emit_UF)) UF,');
    QrCabA.SQL.Add('nfea.*');
    QrCabA.SQL.Add('FROM nfecaba nfea');
    QrCabA.SQL.Add('WHERE (');
    QrCabA.SQL.Add('  (nfea.emit_CNPJ=:Pa) OR (nfea.dest_CNPJ=:Pa)');
    QrCabA.SQL.Add(') AND (nfea.DataFiscal BETWEEN :Pb AND :Pc)');
    QrCabA.SQL.Add('ORDER BY CNPJ_CPF, nfea.ide_serie, nfea.ide_nNF');
    QrCabA.SQL.Add('');
    QrCabA.SQL.Add('');
    QrCabA.Params[00].AsString  := CNPJ;
    QrCabA.Params[01].AsString  := CNPJ;
    QrCabA.Params[02].AsString  := CNPJ;
    QrCabA.Params[03].AsString  := dIni;
    QrCabA.Params[04].AsString  := dFim;
    QrCabA.Open;
    }
    if DModG.QrParamsEmpReg54.Value = 1 then
      if not IncluiRegistro54() then Exit;
    if DModG.QrParamsEmpReg56.Value = 1 then
      if not IncluiRegistro56() then Exit;
  end;
  //
  if DModG.QrParamsEmpReg55.Value = 1 then
    if not IncluiRegistro55() then Exit;
  //
  if DModG.QrParamsEmpReg57.Value = 1 then
    if not IncluiRegistro57() then Exit;
  //
  if DModG.QrParamsEmpReg60M.Value = 1 then
    if not IncluiRegistro60M() then Exit;
  //
  if DModG.QrParamsEmpReg60A.Value = 1 then
    if not IncluiRegistro60A() then Exit;
  //
  if DModG.QrParamsEmpReg60D.Value = 1 then
    if not IncluiRegistro60D() then Exit;
  //
  if DModG.QrParamsEmpReg60I.Value = 1 then
    if not IncluiRegistro60I() then Exit;
  //
  if DModG.QrParamsEmpReg60R.Value = 1 then
    if not IncluiRegistro60R() then Exit;
  //
  if DModG.QrParamsEmpReg61.Value = 1 then
    if not IncluiRegistro61() then Exit;
  //
  if DModG.QrParamsEmpReg61R.Value = 1 then
    if not IncluiRegistro61R() then Exit;
  //
  if DModG.QrParamsEmpReg70.Value = 1 then
    if not IncluiRegistro70() then Exit;
  //
  if DModG.QrParamsEmpReg71.Value = 1 then
    if not IncluiRegistro71() then Exit;
  //
  if DModG.QrParamsEmpReg74.Value = 1 then
    if not IncluiRegistro74() then Exit;
  //
  if DModG.QrParamsEmpReg75.Value = 1 then
  begin
{
    QrProd.Close;
    QrProd.Params[00].AsString  := CNPJ;
    QrProd.Params[01].AsString  := CNPJ;
    QrProd.Params[02].AsString  := dIni;
    QrProd.Params[03].AsString  := dFim;
    QrProd.Open;
}
    QrPrds.Close;
    QrPrds.Database := DModG.MyPID_DB;
    QrPrds.SQL.Clear;
    QrPrds.SQL.Add('SELECT sPM.*');
    QrPrds.SQL.Add('FROM sintegrapm sPM');
    QrPrds.SQL.Add('ORDER BY Codigo');
    QrPrds.SQL.Add('');
    QrPrds.Open;
    //
    if not IncluiRegistro75() then Exit;
  end;
  //
  if DModG.QrParamsEmpReg76.Value = 1 then
    if not IncluiRegistro76() then Exit;
  //
  if DModG.QrParamsEmpReg77.Value = 1 then
    if not IncluiRegistro77() then Exit;
  //
  if (DModG.QrParamsEmpReg85.Value = 1) then
  begin
    QrExpA.Close;
    QrExpA.Params[00].AsString  := CNPJ;
    QrExpA.Params[01].AsString  := CNPJ;
    QrExpA.Params[02].AsString  := dIni;
    QrExpA.Params[03].AsString  := dFim;
    QrExpA.Open;
    //
    if not IncluiRegistro85() then Exit;
  end;
  //
  if (DModG.QrParamsEmpReg86.Value = 1) then
  begin
    (*
    QrExpI.Close;
    QrExpI.Params[00].AsString  := CNPJ;
    QrExpI.Params[01].AsString  := CNPJ;
    QrExpI.Params[02].AsString  := dIni;
    QrExpI.Params[03].AsString  := dFim;
    QrExpI.Open;
    *)//
    if not IncluiRegistro86() then Exit;
  end;
  if DModG.QrParamsEmpReg90.Value = 1 then
    if not IncluiRegistro90() then Exit;
  //
  MyObjects.Informa(LaAviso, False, '...');
  //
  dIni := FormatDateTime('YYYYMMDD', TPDataIni.Date);
  dFim := FormatDateTime('YYYYMMDD', TPDataFim.Date);
  Arq := CNPJ + '_' + dIni + '_' + dFim;
  Dir := DModG.QrParamsEmpSINTEGRA_Path.Value;
  if Trim(Dir) = '' then
    Dir := 'C:\Dermatek\SINTEGRA';
  Arquivo := dmkPF.CaminhoArquivo(Dir, Arq, 'txt');
  if FileExists(Arquivo) then
    Continua := Application.MessageBox(PChar('O arquivo "' + Arquivo +
    ' j� existe. Deseja sobrescrev�-lo?'), 'Pergunta', MB_YESNOCANCEL+
    MB_ICONQUESTION) = ID_YES
  else
    Continua := True;
  if Continua then
  begin
    //if MLAGeral.SalvaTextoEmArquivo(Arquivo, MeGerado.Text, True) then
    // Banco do Brasil CNAB 240 exige s� #10 como quebra de linha!!!
    if MLAGeral.ExportaMemoToFileExt(MeGerado, Arquivo, True, False, True, 1310, Null) then
    begin
      StatusBar.Panels[3].Text := Arquivo;
      //
      Geral.MensagemBox(PChar('O arquivo magn�tico foi salvo como "' +
      Arquivo + '". '), 'Mensagem', MB_OK+MB_ICONINFORMATION);
      {
      Arquivo + '". Voc� pode copiar o caminho da caixa de edi��o na aba "' +
      'Arquivos" aba "Gerado"!'), 'Mensagem', MB_OK+MB_ICONINFORMATION);
      }
      //
    end;
  end;
end;

procedure TFmSintegra_Arq.BtImportaClick(Sender: TObject);
  function Inclui_S50(): Boolean;
  const
    FatID     = 151;
    Status    = 100;
    infProt_cStat = 100;
    infCanc_cStat = 0;
    CriAForca = 0;
    Importado = 1;
    ide_tpAmb = 1;
  var
    FatNum, IDCtrl, Filial, Empresa: Integer;
    Id, ide_dEmi, ide_dSaiEnt, ide_hSaiEnt, NFG_Serie: String;
    ide_mod, ide_serie, ide_nNF, ide_tpNF: Integer;
    ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST, ICMSTot_vST, ICMSTot_vProd,
    ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII, ICMSTot_vIPI,
    ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro, ICMSTot_vNF: Double;
    ModFrete, RetTransp_CMunFG: Integer;
    RetTransp_vServ, RetTransp_vBCRet, RetTransp_PICMSRet, RetTransp_vICMSRet: Double;
    RetTransp_CFOP, DataFiscal: String;
    CodInfoEmit, CodInfoDest, CodInfoTrsp: Integer;
    NF_ICMSAlq: Double;
    NF_CFOP: String;
    //
    prod_cProd, prod_xProd, prod_CFOP, prod_uCom: String;
    prod_qCom, prod_vUnCom, prod_vProd: Double;
    MeuID, Nivel1, nItem: Integer;
    //
    GraGruX, OriCnta, OriPart, Tipo, OriCodi, OriCtrl, StqCenCad, Baixa: Integer;
    DataHora, GraGruX_Txt: String;
    Qtde, Pecas, Peso, CustoAll, Fator: Double;
    ICMS_vBC, ICMS_pICMS: Double;
    //
    Continua: Boolean;
    Fat_ID: Integer;
  begin
    Baixa := 1;
    if MyObjects.FIC(EdEmp_Codigo.ValueVariant = 0, EdEmp_Codigo, 'Empresa n�o definida!') then
      Exit;
    Empresa := EdEmp_Codigo.ValueVariant;
    //

    Id := '';//
  {
    if not NFeXMLGeren.VerificaChaveAcesso(Id, True) then
      Exit;
  }
    ide_dEmi           := Geral.FDT(QrLoad_S50EMISSAO.Value - 2, 1);
    ide_dSaiEnt        := Geral.FDT(QrLoad_S50EMISSAO.Value, 1);
    ide_hSaiEnt        := '00:00:00';
    ide_mod            := Geral.IMV(QrLoad_S50MODELO.Value);
    NFG_Serie          := QrLoad_S50SERIE.Value;
    ide_Serie          := Geral.IMV(Geral.SoNumero_TT(NFG_Serie));
    ide_nNF            := Geral.IMV(QrLoad_S50NUMNF.Value);
    if (QrLoad_S50EMITENTE.Value = 'P') then
      ide_tpNF         := 0
    else
      ide_tpNF         := 1;
    //
    QrSum_S54.Close;
    QrSum_S54.Params[00].AsString := QrLoad_S50CNPJ.Value;
    QrSum_S54.Params[01].AsString := QrLoad_S50Modelo.Value;
    QrSum_S54.Params[02].AsString := QrLoad_S50Serie.Value;
    QrSum_S54.Params[03].AsString := QrLoad_S50NUMNF.Value;
    QrSum_S54.Open;
    //
    QrSum_S50.Close;
    QrSum_S50.Params[00].AsString := QrLoad_S50CNPJ.Value;
    QrSum_S50.Params[01].AsString := QrLoad_S50Modelo.Value;
    QrSum_S50.Params[02].AsString := QrLoad_S50Serie.Value;
    QrSum_S50.Params[03].AsString := QrLoad_S50NUMNF.Value;
    QrSum_S50.Open;
    //
    ICMSTot_vBC        := QrLoad_S50BCICMS.Value;
    ICMSTot_vICMS      := QrLoad_S50ValICMS.Value;
    ICMSTot_vBCST      := 0;
    ICMSTot_vST        := 0;
    ICMSTot_vProd      := QrSum_S54VLRPROD.Value;
    ICMSTot_vFrete     := 0;
    ICMSTot_vSeg       := 0;
    ICMSTot_vDesc      := 0;
    ICMSTot_vII        := 0;
    ICMSTot_vIPI       := 0;
    ICMSTot_vPIS       := 0;
    ICMSTot_vCOFINS    := 0;
    ICMSTot_vOutro     := 0;
    ICMSTot_vNF        := QrSum_S50ValorNF.Value;
    //
    ModFrete           := 2;
    //
    RetTransp_vServ    := 0;
    RetTransp_vBCRet   := 0;
    RetTransp_PICMSRet := 0;
    RetTransp_vICMSRet := 0;
    RetTransp_CFOP     := '';
    RetTransp_CMunFG   := 0;
    //
    DataFiscal         := Geral.FDT(QrLoad_S50EMISSAO.Value, 1);
    CodInfoEmit        := QrLoad_S50Terceiro.Value;
    CodInfoDest        := Empresa;
    CodInfoTrsp        := 0;
    //
    NF_ICMSAlq         := QrLoad_S50AliqICMS.Value;
    NF_CFOP            := QrLoad_S50CFOP.Value;
    //
  (*
  SELECT CodInfoEmit
  FROM nfecaba
  WHERE FatID IN (51,151)
  AND Empresa=-11
  AND ide_mod=1
  AND ide_serie=1
  AND ide_nNF=1
  AND CodinfoEmit > 0
  *)
    QrLocNF.Close;
    QrLocNF.Params[00].AsInteger := Empresa;
    QrLocNF.Params[01].AsInteger := ide_mod;
    QrLocNF.Params[02].AsInteger := ide_serie;
    QrLocNF.Params[03].AsInteger := ide_nNF;
    QrLocNF.Params[04].AsInteger := CodInfoEmit;
    QrLocNF.Open;
    if QrLocNF.RecordCount > 0 then
    begin
      if CkRecriarNFs.Checked then
      begin
        FatNum := QrLocNFFatNum.Value;
        IDCtrl := QrLocNFIDCtrl.Value;
        Fat_ID := QrLocNFFatID.Value;
        DmNFe_0000.ExcluiNfe(0(*Status*), Fat_ID, FatNum, Empresa, True);
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa ');
        Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1');
        Dmod.QrUpd.Params[00].AsInteger := Fat_ID;
        Dmod.QrUpd.Params[01].AsInteger := FatNum;
        UMyMod.ExecutaQuery(Dmod.QrUpd);
        //
        Continua := True;
      end else
      begin
        Continua := False;
        MeNaoImport.Lines.Add('NF: ' + FormatFloat('000000', ide_nNF) + ' Serie: ' +
        FormatFloat('000', ide_serie) + ' Modelo: ' + FormatFloat('00', ide_mod) +
        ' j� cadastrada para a empresa ' + FormatFloat('0', Empresa) +
        ' emitida pelo fornecdor ' + FormatFloat('000000', CodInfoEmit));
        PageControl5.ActivePageIndex := 3;
        PageControl5.Update;
        Application.ProcessMessages;
      end;
    end else
    begin
      IDCtrl := DModG.BuscaProximoInteiro('nfecaba', 'IDCtrl', '', 0);
      FatNum := UMyMod.BuscaEmLivreY_Def('pqe', 'Codigo', stIns, 0, nil, True);
      Continua := True;
    end;
    if Continua then
    begin
      MyObjects.Informa(LaAviso2, True,
        'Importando NF: ' + FormatFloat('000000', ide_nNF) +
        ' Serie: ' + FormatFloat('000', ide_serie) +
        ' Modelo: ' + FormatFloat('00', ide_mod) +
        ' emitida pelo fornecdor ' + FormatFloat('000000', CodInfoEmit));
      //
      //Result :=
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False, [
      'IDCtrl', (*'LoteEnv', 'versao',*)
      'Id', (*'ide_cUF', 'ide_cNF',
      'ide_natOp', 'ide_indPag',*) 'ide_mod',
      'ide_serie', 'ide_nNF', 'ide_dEmi',
      'ide_dSaiEnt', 'ide_hSaiEnt', 'ide_tpNF',
      (*'ide_cMunFG', 'ide_tpImp', 'ide_tpEmis',
      'ide_cDV',*) 'ide_tpAmb', (*'ide_finNFe',
      'ide_procEmi', 'ide_verProc', 'ide_dhCont',
      'ide_xJust', 'emit_CNPJ', 'emit_CPF',
      'emit_xNome', 'emit_xFant', 'emit_xLgr',
      'emit_nro', 'emit_xCpl', 'emit_xBairro',
      'emit_cMun', 'emit_xMun', 'emit_UF',
      'emit_CEP', 'emit_cPais', 'emit_xPais',
      'emit_fone', 'emit_IE', 'emit_IEST',
      'emit_IM', 'emit_CNAE', 'emit_CRT',
      'dest_CNPJ', 'dest_CPF', 'dest_xNome',
      'dest_xLgr', 'dest_nro', 'dest_xCpl',
      'dest_xBairro', 'dest_cMun', 'dest_xMun',
      'dest_UF', 'dest_CEP', 'dest_cPais',
      'dest_xPais', 'dest_fone', 'dest_IE',
      'dest_ISUF', 'dest_email',*) 'ICMSTot_vBC',
      'ICMSTot_vICMS', 'ICMSTot_vBCST', 'ICMSTot_vST',
      'ICMSTot_vProd', 'ICMSTot_vFrete', 'ICMSTot_vSeg',
      'ICMSTot_vDesc', 'ICMSTot_vII', 'ICMSTot_vIPI',
      'ICMSTot_vPIS', 'ICMSTot_vCOFINS', 'ICMSTot_vOutro',
      'ICMSTot_vNF', (*'ISSQNtot_vServ', 'ISSQNtot_vBC',
      'ISSQNtot_vISS', 'ISSQNtot_vPIS', 'ISSQNtot_vCOFINS',
      'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS', 'RetTrib_vRetCSLL',
      'RetTrib_vBCIRRF', 'RetTrib_vIRRF', 'RetTrib_vBCRetPrev',
      'RetTrib_vRetPrev',*) 'ModFrete', (*'Transporta_CNPJ',
      'Transporta_CPF', 'Transporta_XNome', 'Transporta_IE',
      'Transporta_XEnder', 'Transporta_XMun', 'Transporta_UF',*)
      'RetTransp_vServ', 'RetTransp_vBCRet', 'RetTransp_PICMSRet',
      'RetTransp_vICMSRet', 'RetTransp_CFOP', 'RetTransp_CMunFG',
      (*'VeicTransp_Placa', 'VeicTransp_UF', 'VeicTransp_RNTC',
      'Vagao', 'Balsa', 'Cobr_Fat_nFat',
      'Cobr_Fat_vOrig', 'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq',
      'InfAdic_InfAdFisco', 'InfAdic_InfCpl', 'Exporta_UFEmbarq',
      'Exporta_XLocEmbarq', 'Compra_XNEmp', 'Compra_XPed',
      'Compra_XCont',*) 'Status', (*'protNFe_versao',
      'infProt_Id', 'infProt_tpAmb', 'infProt_verAplic',
      'infProt_dhRecbto', 'infProt_nProt', 'infProt_digVal',*)
      'infProt_cStat', (*'infProt_xMotivo', 'retCancNFe_versao',
      'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
      'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',*)
      'infCanc_cStat', (*'infCanc_xMotivo', 'infCanc_cJust',
      'infCanc_xJust', '_Ativo_', 'FisRegCad',
      'CartEmiss', 'TabelaPrc', 'CondicaoPg',
      'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',
      'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
      'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
      'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
      'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
      'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
      'DataFiscal', (*'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta',
      'SINTEGRA_ExpNat', 'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta',
      'SINTEGRA_ExpConhNum', 'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip',
      'SINTEGRA_ExpPais', 'SINTEGRA_ExpAverDta',*) 'CodInfoEmit',
      'CodInfoDest', 'CriAForca', 'CodInfoTrsp',
      (*'OrdemServ', 'Situacao', 'Antigo',*)
      'NFG_Serie', 'NF_ICMSAlq', 'NF_CFOP', 'Importado'], [
      'FatID', 'FatNum', 'Empresa'], [
      IDCtrl, (*LoteEnv, versao,*)
      Id, (*ide_cUF, ide_cNF,
      ide_natOp, ide_indPag,*) ide_mod,
      ide_serie, ide_nNF, ide_dEmi,
      ide_dSaiEnt, ide_hSaiEnt, ide_tpNF,
      (*ide_cMunFG, ide_tpImp, ide_tpEmis,
      ide_cDV,*) ide_tpAmb, (*ide_finNFe,
      ide_procEmi, ide_verProc, ide_dhCont,
      ide_xJust, emit_CNPJ, emit_CPF,
      emit_xNome, emit_xFant, emit_xLgr,
      emit_nro, emit_xCpl, emit_xBairro,
      emit_cMun, emit_xMun, emit_UF,
      emit_CEP, emit_cPais, emit_xPais,
      emit_fone, emit_IE, emit_IEST,
      emit_IM, emit_CNAE, emit_CRT,
      dest_CNPJ, dest_CPF, dest_xNome,
      dest_xLgr, dest_nro, dest_xCpl,
      dest_xBairro, dest_cMun, dest_xMun,
      dest_UF, dest_CEP, dest_cPais,
      dest_xPais, dest_fone, dest_IE,
      dest_ISUF, dest_email,*) ICMSTot_vBC,
      ICMSTot_vICMS, ICMSTot_vBCST, ICMSTot_vST,
      ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg,
      ICMSTot_vDesc, ICMSTot_vII, ICMSTot_vIPI,
      ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro,
      ICMSTot_vNF, (*ISSQNtot_vServ, ISSQNtot_vBC,
      ISSQNtot_vISS, ISSQNtot_vPIS, ISSQNtot_vCOFINS,
      RetTrib_vRetPIS, RetTrib_vRetCOFINS, RetTrib_vRetCSLL,
      RetTrib_vBCIRRF, RetTrib_vIRRF, RetTrib_vBCRetPrev,
      RetTrib_vRetPrev,*) ModFrete, (*Transporta_CNPJ,
      Transporta_CPF, Transporta_XNome, Transporta_IE,
      Transporta_XEnder, Transporta_XMun, Transporta_UF,*)
      RetTransp_vServ, RetTransp_vBCRet, RetTransp_PICMSRet,
      RetTransp_vICMSRet, RetTransp_CFOP, RetTransp_CMunFG,
      (*VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
      Vagao, Balsa, Cobr_Fat_nFat,
      Cobr_Fat_vOrig, Cobr_Fat_vDesc, Cobr_Fat_vLiq,
      InfAdic_InfAdFisco, InfAdic_InfCpl, Exporta_UFEmbarq,
      Exporta_XLocEmbarq, Compra_XNEmp, Compra_XPed,
      Compra_XCont,*) Status, (*protNFe_versao,
      infProt_Id, infProt_tpAmb, infProt_verAplic,
      infProt_dhRecbto, infProt_nProt, infProt_digVal,*)
      infProt_cStat, (*infProt_xMotivo, retCancNFe_versao,
      infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
      infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,*)
      infCanc_cStat, (*infCanc_xMotivo, infCanc_cJust,
      infCanc_xJust, _Ativo_, FisRegCad,
      CartEmiss, TabelaPrc, CondicaoPg,
      FreteExtra, SegurExtra, ICMSRec_pRedBC,
      ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
      IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
      IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
      PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
      COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
      DataFiscal, (*SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta,
      SINTEGRA_ExpNat, SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta,
      SINTEGRA_ExpConhNum, SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip,
      SINTEGRA_ExpPais, SINTEGRA_ExpAverDta,*) CodInfoEmit,
      CodInfoDest, CriAForca, CodInfoTrsp,
      (*OrdemServ, Situacao, Antigo,*)
      NFG_Serie, NF_ICMSAlq, NF_CFOP, Importado], [
      FatID, FatNum, Empresa], True) then ;
      //if Result then
      //begin
  (*
        qVol  := QrNotaFiscQTDADE.Value;
        esp   := QrNotaFiscESPECIE.Value;
        marca := QrNotaFiscMARCA.Value;
        nVol  := QrNotaFiscNUMPROD.Value;
        pesoB := QrNotaFiscPESOBRUT.Value;
        pesoL := QrNotaFiscPESOLIQ.Value;
        FControle := FControle + 1;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabxvol', False, [
        'qVol', 'esp', 'marca',
        'nVol', 'pesoL', 'pesoB'], [
        'FatID', 'FatNum', 'Empresa', 'Controle'], [
        qVol, esp, marca,
        nVol, pesoL, pesoB], [
        FatID, FatNum, Empresa, FControle], True) then ;
        //
  *)
        QrLoad_S54.Close;
  (*
  SELECT * FROM load_s54
  WHERE CNPJ=:P0
  AND Serie=:P1
  AND NUMNF=:P2
  *)
        QrLoad_S54.Params[00].AsString := QrLoad_S50CNPJ.Value;
        QrLoad_S54.Params[01].AsString := QrLoad_S50Modelo.Value;
        QrLoad_S54.Params[02].AsString := QrLoad_S50Serie.Value;
        QrLoad_S54.Params[03].AsString := QrLoad_S50NUMNF.Value;
        QrLoad_S54.Open;
        if QrLoad_S54.RecordCount = 0 then
        begin
          MeNaoImport.Lines.Add('NF sem produtos > NF: ' +
          FormatFloat('000000', ide_nNF) + ' Serie: ' +
          FormatFloat('000', ide_serie) + ' Modelo: ' + FormatFloat('00', ide_mod) +
          ' emitida contra a empresa ' + FormatFloat('0', Empresa) +
          ' emitida pelo fornecdor ' + FormatFloat('000000', CodInfoEmit));
          Exit;
        end;
        //
        while not QrLoad_S54.Eof do
        begin
          //nItem := 0;
          //try
          nItem          := Geral.IMV(QrLoad_S54NUMITEM.Value);
          QrLoad_S75.Close;
          QrLoad_S75.Params[0].AsString := QrLoad_S54CODPROD.Value;
          QrLoad_S75.Open;
          if QrLoad_S75.RecordCount = 0 then
          begin
            Geral.MensagemBox('Produto n�o localizado:' + QrLoad_S54CODPROD.Value,
            'Aviso', MB_OK+MB_ICONWARNING);
            Result := False;
            Exit;
          end;
          //
          prod_cProd     := FormatFloat('0', QrLoad_S75GraGruX.Value);
          prod_xProd     := QrLoad_S75DESCRICAO.Value;
          prod_CFOP      := Geral.SoNumero_TT(QrLoad_S54CFOP.Value);
          prod_uCom      := QrLoad_S75Unidade.Value;
          prod_qCom      := QrLoad_S54QTDADE.Value;
          prod_vProd     := QrLoad_S54VLRPROD.Value;
          if prod_qCom > 0 then
            prod_vUnCom  := prod_vProd / prod_qCom
          else
            prod_vUnCom  := 0;
          MeuID          := 0;
          GraGruX        := QrLoad_S75GraGruX.Value;
          Nivel1         := QrLoad_S75Nivel1.Value;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
          'prod_cProd', (*'prod_cEAN',*) 'prod_xProd',
          (*'prod_NCM', 'prod_EXTIPI', 'prod_genero',*)
          'prod_CFOP', 'prod_uCom', 'prod_qCom',
          'prod_vUnCom', 'prod_vProd', (*'prod_cEANTrib',
          'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
          'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
          'prod_vOutro', 'prod_indTot', 'prod_xPed',
          'prod_nItemPed', 'Tem_IPI', '_Ativo_',
          'InfAdCuztm', 'EhServico', 'ICMSRec_pRedBC',
          'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
          'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
          'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
          'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
          'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
          'MeuID', 'Nivel1'], [
          'FatID', 'FatNum', 'Empresa', 'nItem'], [
          prod_cProd, (*prod_cEAN,*) prod_xProd,
          (*prod_NCM, prod_EXTIPI, prod_genero,*)
          prod_CFOP, prod_uCom, prod_qCom,
          prod_vUnCom, prod_vProd, (*prod_cEANTrib,
          prod_uTrib, prod_qTrib, prod_vUnTrib,
          prod_vFrete, prod_vSeg, prod_vDesc,
          prod_vOutro, prod_indTot, prod_xPed,
          prod_nItemPed, Tem_IPI, _Ativo_,
          InfAdCuztm, EhServico, ICMSRec_pRedBC,
          ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
          IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
          IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
          PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
          COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
          MeuID, Nivel1], [
          FatID, FatNum, Empresa, nItem], True, '', False, True) then ;
          //begin
            ICMS_vBC := QrLoad_S54BASECALC.Value;
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsn', False, [
            (*'ICMS_Orig', 'ICMS_CST', 'ICMS_modBC',
            'ICMS_pRedBC',*) 'ICMS_vBC', 'ICMS_pICMS'(*,
            'ICMS_vICMS', 'ICMS_modBCST', 'ICMS_pMVAST',
            'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
            'ICMS_vICMSST', '_Ativo_', 'ICMS_CSOSN',
            'ICMS_UFST', 'ICMS_pBCOp', 'ICMS_vBCSTRet',
            'ICMS_vICMSSTRet', 'ICMS_motDesICMS', 'ICMS_pCredSN',
            'ICMS_vCredICMSSN'*)], [
            'FatID', 'FatNum', 'Empresa', 'nItem'], [
            (*ICMS_Orig, ICMS_CST, ICMS_modBC,
            ICMS_pRedBC,*) ICMS_vBC, ICMS_pICMS(*,
            ICMS_vICMS, ICMS_modBCST, ICMS_pMVAST,
            ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
            ICMS_vICMSST, _Ativo_, ICMS_CSOSN,
            ICMS_UFST, ICMS_pBCOp, ICMS_vBCSTRet,
            ICMS_vICMSSTRet, ICMS_motDesICMS, ICMS_pCredSN,
            ICMS_vCredICMSSN*)], [
            FatID, FatNum, Empresa, nItem], True) then ;
            //begin
              DataHora  := DataFiscal;
              Tipo      := FatID;
              OriCodi   := FatNum;
              OriCtrl   := QrLoad_S54.RecNo;
              OriCnta   := 0;
              OriPart   := 0;
              StqCenCad := 1;
              Qtde      := prod_qCom;
              Pecas     := 0;
              Peso      := 0;
              CustoAll  := prod_vProd;
              IDCtrl    := UMyMod.BuscaEmLivreY_Def('stqmovitsa', 'IDCtrl', stIns, 0);
              //
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
              'DataHora', 'Tipo', 'OriCodi',
              'OriCtrl', 'OriCnta', 'OriPart',
              'Empresa', 'StqCenCad', 'GraGruX',
              'Qtde', 'Pecas', 'Peso',
              (*'AreaM2', 'AreaP2', 'FatorClas',
              'QuemUsou', 'Retorno', 'ParTipo',
              'ParCodi', 'DebCtrl', 'SMIMultIns',*)
              'CustoAll', (*'ValorAll', 'GrupoBal',*)
              'Baixa'], [
              'IDCtrl'], [
              DataHora, Tipo, OriCodi,
              OriCtrl, OriCnta, OriPart,
              Empresa, StqCenCad, GraGruX,
              Qtde, Pecas, Peso,
              (*AreaM2, AreaP2, FatorClas,
              QuemUsou, Retorno, ParTipo,
              ParCodi, DebCtrl, SMIMultIns,*)
              CustoAll, (*ValorAll, GrupoBal,*)
              Baixa], [
              IDCtrl], False) then ;
            //end;
          //end;
          //
          {
          except
             Memo1.Lines.Add('Erro nfeitsi > ' + FormatFloat('0', FatNum) + '#' + FormatFloat('0', nItem));
             raise;
          end;
          }
          QrLoad_S54.Next;
        end;
      //end;
    end;
  end;
  function Inclui_S70(): Boolean;
  const
    FatID     = 151;
    Status    = 100;
    infProt_cStat = 100;
    infCanc_cStat = 0;
    CriAForca = 0;
    Importado = 1;
    ide_tpAmb = 1;
  var
    FatNum, IDCtrl, Filial, Empresa: Integer;
    Id, ide_dEmi, ide_dSaiEnt, ide_hSaiEnt, NFG_Serie: String;
    ide_mod, ide_serie, ide_nNF, ide_tpNF: Integer;
    ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST, ICMSTot_vST, ICMSTot_vProd,
    ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII, ICMSTot_vIPI,
    ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro, ICMSTot_vNF: Double;
    ModFrete, RetTransp_CMunFG: Integer;
    RetTransp_vServ, RetTransp_vBCRet, RetTransp_PICMSRet, RetTransp_vICMSRet: Double;
    RetTransp_CFOP, DataFiscal: String;
    CodInfoEmit, CodInfoDest, CodInfoTrsp: Integer;
    NF_ICMSAlq: Double;
    NF_CFOP: String;
    //
    prod_cProd, prod_xProd, prod_CFOP, prod_uCom: String;
    prod_qCom, prod_vUnCom, prod_vProd: Double;
    MeuID, Nivel1, nItem: Integer;
    //
    GraGruX, OriCnta, OriPart, Tipo, OriCodi, OriCtrl, StqCenCad, Baixa: Integer;
    DataHora, GraGruX_Txt: String;
    Qtde, Pecas, Peso, CustoAll, Fator: Double;
    ICMS_vBC, ICMS_pICMS: Double;
    //
    Continua: Boolean;
    Fat_ID: Integer;
    NFG_SubSerie: String;
  begin
    Baixa := 1;
    if MyObjects.FIC(EdEmp_Codigo.ValueVariant = 0, EdEmp_Codigo, 'Empresa n�o definida!') then
      Exit;
    Empresa := EdEmp_Codigo.ValueVariant;
    //

    Id := '';//
  {
    if not NFeXMLGeren.VerificaChaveAcesso(Id, True) then
      Exit;
  }
    ide_dEmi           := Geral.FDT(QrLoad_S70EMISSAO.Value - 2, 1);
    ide_dSaiEnt        := Geral.FDT(QrLoad_S70EMISSAO.Value, 1);
    ide_hSaiEnt        := '00:00:00';
    ide_mod            := Geral.IMV(QrLoad_S70MODELO.Value);
    NFG_Serie          := QrLoad_S70SERIE.Value;
    NFG_SubSerie       := QrLoad_S70SUBSERIE.Value;
    ide_Serie          := Geral.IMV(Geral.SoNumero_TT(NFG_Serie));
    ide_nNF            := Geral.IMV(QrLoad_S70NUMNF.Value);
    ide_tpNF           := 1;//
    //
    ICMSTot_vBC        := QrLoad_S70BASEICMS.Value;
    ICMSTot_vICMS      := QrLoad_S70VALORICMS.Value;
    ICMSTot_vBCST      := 0;
    ICMSTot_vST        := 0;
    ICMSTot_vProd      := QrSum_S54VLRPROD.Value;
    ICMSTot_vFrete     := 0;
    ICMSTot_vSeg       := 0;
    ICMSTot_vDesc      := 0;
    ICMSTot_vII        := 0;
    ICMSTot_vIPI       := 0;
    ICMSTot_vPIS       := 0;
    ICMSTot_vCOFINS    := 0;
    ICMSTot_vOutro     := 0;
    ICMSTot_vNF        := QrLoad_S70VALORTOT.Value;
    //
    ModFrete           := 2;
    //
    RetTransp_vServ    := 0;
    RetTransp_vBCRet   := 0;
    RetTransp_PICMSRet := 0;
    RetTransp_vICMSRet := 0;
    RetTransp_CFOP     := '';
    RetTransp_CMunFG   := 0;
    //
    DataFiscal         := Geral.FDT(QrLoad_S70EMISSAO.Value, 1);
    CodInfoEmit        := QrLoad_S70Transporta.Value;
    CodInfoDest        := Empresa;
    CodInfoTrsp        := 0;
    //
    NF_ICMSAlq         := 0;
    NF_CFOP            := QrLoad_S70CFOP.Value;
    //
  (*
  SELECT CodInfoEmit
  FROM nfecaba
  WHERE FatID IN (51,151)
  AND Empresa=-11
  AND ide_mod=1
  AND ide_serie=1
  AND ide_nNF=1
  AND CodinfoEmit > 0
  *)
    QrLocNF.Close;
    QrLocNF.Params[00].AsInteger := Empresa;
    QrLocNF.Params[01].AsInteger := ide_mod;
    QrLocNF.Params[02].AsInteger := ide_serie;
    QrLocNF.Params[03].AsInteger := ide_nNF;
    QrLocNF.Params[04].AsInteger := CodInfoEmit;
    QrLocNF.Open;
    if QrLocNF.RecordCount > 0 then
    begin
      if CkRecriarNFs.Checked then
      begin
        FatNum := QrLocNFFatNum.Value;
        IDCtrl := QrLocNFIDCtrl.Value;
        Fat_ID := QrLocNFFatID.Value;
        DmNFe_0000.ExcluiNfe(0(*Status*), Fat_ID, FatNum, Empresa, True);
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa ');
        Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1');
        Dmod.QrUpd.Params[00].AsInteger := Fat_ID;
        Dmod.QrUpd.Params[01].AsInteger := FatNum;
        UMyMod.ExecutaQuery(Dmod.QrUpd);
        //
        Continua := True;
      end else
      begin
        Continua := False;
        MeNaoImport.Lines.Add('NF: ' + FormatFloat('000000', ide_nNF) + ' Serie: ' +
        FormatFloat('000', ide_serie) + ' Modelo: ' + FormatFloat('00', ide_mod) +
        ' j� cadastrada para a empresa ' + FormatFloat('0', Empresa) +
        ' emitida pelo fornecdor ' + FormatFloat('000000', CodInfoEmit));
        PageControl5.ActivePageIndex := 3;
        PageControl5.Update;
        Application.ProcessMessages;
      end;
    end else
    begin
      IDCtrl := DModG.BuscaProximoInteiro('nfecaba', 'IDCtrl', '', 0);
      FatNum := UMyMod.BuscaEmLivreY_Def('pqe', 'Codigo', stIns, 0, nil, True);
      Continua := True;
    end;
    if Continua then
    begin
      MyObjects.Informa(LaAviso2, True,
        'Importando NF: ' + FormatFloat('000000', ide_nNF) +
        ' Serie: ' + FormatFloat('000', ide_serie) +
        ' Modelo: ' + FormatFloat('00', ide_mod) +
        ' emitida pelo fornecdor ' + FormatFloat('000000', CodInfoEmit));
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False, [
      'IDCtrl', (*'LoteEnv', 'versao',*)
      'Id', (*'ide_cUF', 'ide_cNF',
      'ide_natOp', 'ide_indPag',*) 'ide_mod',
      'ide_serie', 'ide_nNF', 'ide_dEmi',
      'ide_dSaiEnt', 'ide_hSaiEnt', 'ide_tpNF',
      (*'ide_cMunFG', 'ide_tpImp', 'ide_tpEmis',
      'ide_cDV',*) 'ide_tpAmb', (*'ide_finNFe',
      'ide_procEmi', 'ide_verProc', 'ide_dhCont',
      'ide_xJust', 'emit_CNPJ', 'emit_CPF',
      'emit_xNome', 'emit_xFant', 'emit_xLgr',
      'emit_nro', 'emit_xCpl', 'emit_xBairro',
      'emit_cMun', 'emit_xMun', 'emit_UF',
      'emit_CEP', 'emit_cPais', 'emit_xPais',
      'emit_fone', 'emit_IE', 'emit_IEST',
      'emit_IM', 'emit_CNAE', 'emit_CRT',
      'dest_CNPJ', 'dest_CPF', 'dest_xNome',
      'dest_xLgr', 'dest_nro', 'dest_xCpl',
      'dest_xBairro', 'dest_cMun', 'dest_xMun',
      'dest_UF', 'dest_CEP', 'dest_cPais',
      'dest_xPais', 'dest_fone', 'dest_IE',
      'dest_ISUF', 'dest_email',*) 'ICMSTot_vBC',
      'ICMSTot_vICMS', 'ICMSTot_vBCST', 'ICMSTot_vST',
      'ICMSTot_vProd', 'ICMSTot_vFrete', 'ICMSTot_vSeg',
      'ICMSTot_vDesc', 'ICMSTot_vII', 'ICMSTot_vIPI',
      'ICMSTot_vPIS', 'ICMSTot_vCOFINS', 'ICMSTot_vOutro',
      'ICMSTot_vNF', (*'ISSQNtot_vServ', 'ISSQNtot_vBC',
      'ISSQNtot_vISS', 'ISSQNtot_vPIS', 'ISSQNtot_vCOFINS',
      'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS', 'RetTrib_vRetCSLL',
      'RetTrib_vBCIRRF', 'RetTrib_vIRRF', 'RetTrib_vBCRetPrev',
      'RetTrib_vRetPrev',*) 'ModFrete', (*'Transporta_CNPJ',
      'Transporta_CPF', 'Transporta_XNome', 'Transporta_IE',
      'Transporta_XEnder', 'Transporta_XMun', 'Transporta_UF',*)
      'RetTransp_vServ', 'RetTransp_vBCRet', 'RetTransp_PICMSRet',
      'RetTransp_vICMSRet', 'RetTransp_CFOP', 'RetTransp_CMunFG',
      (*'VeicTransp_Placa', 'VeicTransp_UF', 'VeicTransp_RNTC',
      'Vagao', 'Balsa', 'Cobr_Fat_nFat',
      'Cobr_Fat_vOrig', 'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq',
      'InfAdic_InfAdFisco', 'InfAdic_InfCpl', 'Exporta_UFEmbarq',
      'Exporta_XLocEmbarq', 'Compra_XNEmp', 'Compra_XPed',
      'Compra_XCont',*) 'Status', (*'protNFe_versao',
      'infProt_Id', 'infProt_tpAmb', 'infProt_verAplic',
      'infProt_dhRecbto', 'infProt_nProt', 'infProt_digVal',*)
      'infProt_cStat', (*'infProt_xMotivo', 'retCancNFe_versao',
      'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
      'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',*)
      'infCanc_cStat', (*'infCanc_xMotivo', 'infCanc_cJust',
      'infCanc_xJust', '_Ativo_', 'FisRegCad',
      'CartEmiss', 'TabelaPrc', 'CondicaoPg',
      'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',
      'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
      'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
      'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
      'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
      'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
      'DataFiscal', (*'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta',
      'SINTEGRA_ExpNat', 'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta',
      'SINTEGRA_ExpConhNum', 'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip',
      'SINTEGRA_ExpPais', 'SINTEGRA_ExpAverDta',*) 'CodInfoEmit',
      'CodInfoDest', 'CriAForca', 'CodInfoTrsp',
      (*'OrdemServ', 'Situacao', 'Antigo',*)
      'NFG_Serie', 'NF_ICMSAlq', 'NF_CFOP',
      'Importado', 'NFG_SubSerie'], [
      'FatID', 'FatNum', 'Empresa'], [
      IDCtrl, (*LoteEnv, versao,*)
      Id, (*ide_cUF, ide_cNF,
      ide_natOp, ide_indPag,*) ide_mod,
      ide_serie, ide_nNF, ide_dEmi,
      ide_dSaiEnt, ide_hSaiEnt, ide_tpNF,
      (*ide_cMunFG, ide_tpImp, ide_tpEmis,
      ide_cDV,*) ide_tpAmb, (*ide_finNFe,
      ide_procEmi, ide_verProc, ide_dhCont,
      ide_xJust, emit_CNPJ, emit_CPF,
      emit_xNome, emit_xFant, emit_xLgr,
      emit_nro, emit_xCpl, emit_xBairro,
      emit_cMun, emit_xMun, emit_UF,
      emit_CEP, emit_cPais, emit_xPais,
      emit_fone, emit_IE, emit_IEST,
      emit_IM, emit_CNAE, emit_CRT,
      dest_CNPJ, dest_CPF, dest_xNome,
      dest_xLgr, dest_nro, dest_xCpl,
      dest_xBairro, dest_cMun, dest_xMun,
      dest_UF, dest_CEP, dest_cPais,
      dest_xPais, dest_fone, dest_IE,
      dest_ISUF, dest_email,*) ICMSTot_vBC,
      ICMSTot_vICMS, ICMSTot_vBCST, ICMSTot_vST,
      ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg,
      ICMSTot_vDesc, ICMSTot_vII, ICMSTot_vIPI,
      ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro,
      ICMSTot_vNF, (*ISSQNtot_vServ, ISSQNtot_vBC,
      ISSQNtot_vISS, ISSQNtot_vPIS, ISSQNtot_vCOFINS,
      RetTrib_vRetPIS, RetTrib_vRetCOFINS, RetTrib_vRetCSLL,
      RetTrib_vBCIRRF, RetTrib_vIRRF, RetTrib_vBCRetPrev,
      RetTrib_vRetPrev,*) ModFrete, (*Transporta_CNPJ,
      Transporta_CPF, Transporta_XNome, Transporta_IE,
      Transporta_XEnder, Transporta_XMun, Transporta_UF,*)
      RetTransp_vServ, RetTransp_vBCRet, RetTransp_PICMSRet,
      RetTransp_vICMSRet, RetTransp_CFOP, RetTransp_CMunFG,
      (*VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
      Vagao, Balsa, Cobr_Fat_nFat,
      Cobr_Fat_vOrig, Cobr_Fat_vDesc, Cobr_Fat_vLiq,
      InfAdic_InfAdFisco, InfAdic_InfCpl, Exporta_UFEmbarq,
      Exporta_XLocEmbarq, Compra_XNEmp, Compra_XPed,
      Compra_XCont,*) Status, (*protNFe_versao,
      infProt_Id, infProt_tpAmb, infProt_verAplic,
      infProt_dhRecbto, infProt_nProt, infProt_digVal,*)
      infProt_cStat, (*infProt_xMotivo, retCancNFe_versao,
      infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
      infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,*)
      infCanc_cStat, (*infCanc_xMotivo, infCanc_cJust,
      infCanc_xJust, _Ativo_, FisRegCad,
      CartEmiss, TabelaPrc, CondicaoPg,
      FreteExtra, SegurExtra, ICMSRec_pRedBC,
      ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
      IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
      IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
      PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
      COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
      DataFiscal, (*SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta,
      SINTEGRA_ExpNat, SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta,
      SINTEGRA_ExpConhNum, SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip,
      SINTEGRA_ExpPais, SINTEGRA_ExpAverDta,*) CodInfoEmit,
      CodInfoDest, CriAForca, CodInfoTrsp,
      (*OrdemServ, Situacao, Antigo,*)
      NFG_Serie, NF_ICMSAlq, NF_CFOP,
      Importado, NFG_SubSerie], [
      FatID, FatNum, Empresa], True);
      if Result then
      begin
        //
      end;
    end;
  end;
begin
  MeNaoImport.Lines.Clear;
  //
  QrLoad_S50.Close;
  QrLoad_S50.Open;
  PB2.Position := 0;
  PB2.Max := QrLoad_S50.RecordCount;
  while not QrLoad_S50.Eof do
  begin
    PB2.Position := PB2.Position + 1;
    Inclui_S50();
    QrLoad_S50.Next;
  end;
  //
  QrLoad_S70.Close;
  QrLoad_S70.Open;
  PB2.Position := 0;
  PB2.Max := QrLoad_S70.RecordCount;
  while not QrLoad_S70.Eof do
  begin
    PB2.Position := PB2.Position + 1;
    Inclui_S70();
    QrLoad_S70.Next;
  end;
  if CkExclEstqMP.Checked then
    Dmod.ExcluiItensEstqNFsCouroImportadas();
end;

procedure TFmSintegra_Arq.BtCarregaClick(Sender: TObject);
  procedure IncluiProduto(CODPROD, DESCRICAO, UNIDADE, CODNCM: String; IPI_Alq: Double);
  const
    CST_A = '0';
    CST_B = '0';
    Prefixo = '';
    Nivel3 = 0;
    GraTamCad = 1;
    Peso = 0.000;
    PIS_AlqP = 0.00;
    COFINS_AlqP = 0.00;
    GraCorCad = 0;
    GraTamI = 1;
    GraCusPrc = 5; // Lista de Pre�os (pre�o m�dio)
  var
    Nivel2, Nivel1, CodUsu, PrdGrupTip, Fabricante, UnidMed: Integer;
    //
    Nome, (*Antigo,*) Referencia, NCM: String;
    SQLType: TSQLType;
    //Continua: Boolean;
    //
    //NCMS: array of String;
    //
    Controle, GraGruC, GraGru1, GraGruX, Entidade: Integer;
    CustoPreco: Double;
  begin
    Entidade := EdEmp_Codigo.ValueVariant;
    Nivel1 := Geral.IMV(CODPROD);
    Nivel2 := -4;
    //
    PrdGrupTip := -2;
    Referencia := '';
    Nome       := DESCRICAO;
    Fabricante := 0;
    //
    {
    QrUnidMed.Close;
    QrUnidMed.Params[0].AsString := UNIDADE;
    QrUnidMed.Open;
    UnidMed := QrUnidMedCodigo.Value;
    }
    UnidMed := DmProd.ObtemUnidMedDeSigla(UNIDADE);
    NCM     := Geral.FormataNCM(CODNCM);
    CustoPreco := 0;
  {
    TbProdutosQtEstoque.Value,
    TbProdutosQtPeas.Value,
    TbProdutosIPI.Value,
    TbProdutosICMS.Value,
    TbProdutosICMSLC.Value,
    TbProdutosCodTrib.Value,
    TbProdutosEstoqMin.Value,
    TbProdutosOrigem.Value,
    TbProdutosLocal.Value,
    TbProdutosDataEnt.Value,
    TbProdutosCoeficient.Value,
    TbProdutosD04UKEY.Value
  }
    //
    CodUsu := Nivel1;

    {  teste
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, '_produtos', False, [
    'Codigo', 'CodPesq', 'Descricao',
    'Fabricante', 'Grupo', 'Tipo',
    'Medida', 'VlrUni', 'QtEstoque',
    'QtPeas', 'IPI', 'ICMS',
    'ICMSLC', 'ClasFisc', 'CodTrib',
    'EstoqMin', 'Origem', 'Local',
    'DataEnt', 'Coeficient', 'D04UKEY'], [
    ], [
    TbProdutosCodigo.Value, TbProdutosCodPesq.Value, TbProdutosDESCRIO.Value,
    TbProdutosFabricante.Value, TbProdutosGrupo.Value, TbProdutosTipo.Value,
    TbProdutosMedida.Value, TbProdutosVlrUni.Value, TbProdutosQtEstoque.Value,
    TbProdutosQtPeas.Value, TbProdutosIPI.Value, TbProdutosICMS.Value,
    TbProdutosICMSLC.Value, TbProdutosClasFisc.Value, TbProdutosCodTrib.Value,
    TbProdutosEstoqMin.Value, TbProdutosOrigem.Value, TbProdutosLocal.Value,
    TbProdutosDataEnt.Value, TbProdutosCoeficient.Value, TbProdutosD04UKEY.Value], [
    ], False) then ;
    }
    //
    //   GRAGRU1
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragru1', False, [
    'Nivel3', 'Nivel2', 'CodUsu',
    'Nome', 'PrdGrupTip', 'GraTamCad',
    'UnidMed', 'CST_A', 'CST_B',
    'NCM', 'Peso', 'IPI_Alq',
    (*'IPI_CST', 'IPI_cEnq', 'IPI_vUnid',
    'IPI_TpTrib', 'TipDimens', 'PerCuztMin',
    'PerCuztMax', 'MedOrdem', 'PartePrinc',
    'InfAdProd', 'SiglaCustm', 'HowBxaEstq',
    'GerBxaEstq', 'PIS_CST',*) 'PIS_AlqP',
    (*'PIS_AlqV', 'PISST_AlqP', 'PISST_AlqV',
    'COFINS_CST',*) 'COFINS_AlqP', (*'COFINS_AlqV',
    'COFINSST_AlqP', 'COFINSST_AlqV', 'ICMS_modBC',
    'ICMS_modBCST', 'ICMS_pRedBC', 'ICMS_pRedBCST',
    'ICMS_pMVAST', 'ICMS_pICMSST', 'ICMS_Pauta',
    'ICMS_MaxTab', 'IPI_pIPI', 'cGTIN_EAN',
    'EX_TIPI', 'PIS_pRedBC', 'PISST_pRedBCST',
    'COFINS_pRedBC', 'COFINSST_pRedBCST', 'ICMSRec_pRedBC',
    'IPIRec_pRedBC', 'PISRec_pRedBC', 'COFINSRec_pRedBC',
    'ICMSRec_pAliq', 'IPIRec_pAliq', 'PISRec_pAliq',
    'COFINSRec_pAliq', 'ICMSRec_tCalc', 'IPIRec_tCalc',
    'PISRec_tCalc', 'COFINSRec_tCalc', 'ICMSAliqSINTEGRA',
    'ICMSST_BaseSINTEGRA', 'FatorClas', 'prod_indTot',*)
    'Referencia', 'Fabricante'], [
    'Nivel1'], [
    Nivel3, Nivel2, CodUsu,
    Nome, PrdGrupTip, GraTamCad,
    UnidMed, CST_A, CST_B,
    NCM, Peso, IPI_Alq,
    (*IPI_CST, IPI_cEnq, IPI_vUnid,
    IPI_TpTrib, TipDimens, PerCuztMin,
    PerCuztMax, MedOrdem, PartePrinc,
    InfAdProd, SiglaCustm, HowBxaEstq,
    GerBxaEstq, PIS_CST,*) PIS_AlqP,
    (*PIS_AlqV, PISST_AlqP, PISST_AlqV,
    COFINS_CST,*) COFINS_AlqP, (*COFINS_AlqV,
    COFINSST_AlqP, COFINSST_AlqV, ICMS_modBC,
    ICMS_modBCST, ICMS_pRedBC, ICMS_pRedBCST,
    ICMS_pMVAST, ICMS_pICMSST, ICMS_Pauta,
    ICMS_MaxTab, IPI_pIPI, cGTIN_EAN,
    EX_TIPI, PIS_pRedBC, PISST_pRedBCST,
    COFINS_pRedBC, COFINSST_pRedBCST, ICMSRec_pRedBC,
    IPIRec_pRedBC, PISRec_pRedBC, COFINSRec_pRedBC,
    ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq,
    COFINSRec_pAliq, ICMSRec_tCalc, IPIRec_tCalc,
    PISRec_tCalc, COFINSRec_tCalc, ICMSAliqSINTEGRA,
    ICMSST_BaseSINTEGRA, FatorClas, prod_indTot,*)
    Referencia, Fabricante], [
    Nivel1], True) then ;
    //
    ///  GRAGRUC
    Controle  := Nivel1;
(*
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruc', False, [
    'Nivel1', 'GraCorCad'], ['Controle'], [
    Nivel1, GraCorCad], [Controle], True) then ;
    //
*)
    //   GRAGRUX
    GraGruC := Nivel1;
    GraGru1 := Nivel1;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragrux', False, [
    'GraGruC', 'GraGru1', 'GraTamI'], ['Controle'], [
    GraGruC, GraGru1, GraTamI], [Controle], True) then ;
    //
    //   GRAGRUVAL = 5
    GraGruX := Controle;
    if CustoPreco >= 0.000001 then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
      'GraGruX', 'GraGru1', 'GraCusPrc', 'CustoPreco', 'Entidade'], [
      'Controle'], [GraGruX, GraGru1, GraCusPrc, CustoPreco, Entidade], [
      Controle], True) then ;
    end;
    //
  end;
  procedure LimparDadosTabela(SQL: WideString);
  begin
    MyObjects.Informa(LaAviso, True, 'Executando limpeza: ' + SQL);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(SQL);
    Dmod.QrUpd.ExecSQL;
  end;
var
  I, Tipo, Fornecedor, Codigo, EUF, PUF, GraGruX, Nivel1, CIF_FOB: Integer;
  Emitente, CNPJ, IE, Data, Modelo, Serie, NumNF, CFOP, CST, Emissao, UF, NomeForn,
  RazaoSocial, Fantasia: String;
  ValorNF, BCICMS, ValICMS, ValIsento, ValNCICMS, AliqICMS: Double;
  NUMITEM, CODPROD, NomeProd: String;
  QTDADE, VLRPROD, VLRDESC, BASECALC, BASESUBTRI, VLRIPI, ALIQIPI, REDBASEICM: Double;
  DATAINI, DATAFIN, CODNCM, DESCRICAO, UNIDADE, INSCEST, SUBSERIE, SITUACAO: String;
  VALORTOT, BASEICMS, VALORICMS, VLRISENTO, OUTROS: Double;
  Transporta: Integer;
  NomeTrsp: String;
begin
  Screen.Cursor := crHourGlass;
  try
    BtImporta.Enabled := False;
    MeIgnor.Lines.Clear;
    MeErros.Lines.Clear;
    FS50Load := GradeCriar.RecriaTempTableNovo(ntrttS50Load, DmodG.QrUpdPID1, False);
    FS54Load := GradeCriar.RecriaTempTableNovo(ntrttS54Load, DmodG.QrUpdPID1, False);
    FS70Load := GradeCriar.RecriaTempTableNovo(ntrttS70Load, DmodG.QrUpdPID1, False);
    FS75Load := GradeCriar.RecriaTempTableNovo(ntrttS75Load, DmodG.QrUpdPID1, False);
    for I := 1 to MeImportado.Lines.Count - 1 do
    begin
      //InfoPosMeImportado2(I);
      USintegra.InfoPosMeImportado2(MeImportado, GridS, I,
        F_Tam, F_PosI, F_PosF, F_Fmt, F_Casas, F_Obrigatorio);
      //
      Tipo := Geral.IMV(GridS.Cells[01,01]);
      case Tipo of
        10:
        begin
          EdEmp_CNPJ.ValueVariant := GridS.Cells[01,02];
        end;
        50:
        begin
          Emitente := Uppercase(GridS.Cells[01,10]);
          if Emitente = 'T' then
          begin
            CNPJ   := GridS.Cells[01,02];
            IE     := GridS.Cells[01,03];
            Data   := GridS.Cells[02,04];
            UF     := GridS.Cells[01,05];
            Modelo := GridS.Cells[01,06];
            Serie  := GridS.Cells[01,07];
            NumNF  := GridS.Cells[01,08];
            CFOP   := GridS.Cells[01,09];
            //
            ValorNF   := Geral.DMV(GridS.Cells[02,11]);
            BCICMS    := Geral.DMV(GridS.Cells[02,12]);
            ValICMS   := Geral.DMV(GridS.Cells[02,13]);
            ValIsento := Geral.DMV(GridS.Cells[02,14]);
            ValNCICMS := Geral.DMV(GridS.Cells[02,15]);
            AliqICMS  := Geral.DMV(GridS.Cells[02,16]);
            //
            //dmkEdit1.Text := CNPJ;
            //
            Emissao := Geral.FDT(Geral.ValidaDataBR(Data, False, False), 1);
            //
            if DModG.DefineEntidade(CNPJ, False, Fornecedor, NomeForn) then
            begin
              UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'load_s50', False, [
              'TIPO', 'CNPJ', 'IE',
              'EMISSAO', 'UF', 'MODELO',
              'SERIE', 'NUMNF', 'CFOP',
              'EMITENTE', 'ValorNF', 'BCICMS',
              'ValICMS', 'ValIsento', 'ValNCICMS',
              'AliqICMS', 'Terceiro'], [
              ], [
              TIPO, CNPJ, IE,
              EMISSAO, UF, MODELO,
              SERIE, NUMNF, CFOP,
              EMITENTE, ValorNF, BCICMS,
              ValICMS, ValIsento, ValNCICMS,
              AliqICMS, Fornecedor], [
              ], False);
            end else
            begin
              Pagecontrol5.ActivePageIndex := 2;
              Pagecontrol5.Update;
              Application.ProcessMessages;
              //
              if NomeForn = '' then
                MeErros.Lines.Add('Linha ' + FormatFloat('0000', I) +
                ' CNPJ/CPF cad. incompleto "' +
                CNPJ + '" NF: ' + NumNF + ' de ' + Data)
              else
                MeErros.Lines.Add('Linha ' + FormatFloat('0000', I) +
                ' CNPJ/CPF n�o cadastrado: "' +
                CNPJ + '" NF: ' + NumNF + ' de ' + Data);
              //
              Fantasia := '   CNPJ: ' + CNPJ + ' NF: ' + NumNF + ' de ' + Data;
              EUF := Geral.GetCodigoUF_da_SiglaUF(UF);
              PUF := EUF;
              // tirar, s� da problema!
              if Length(Geral.SoNumero_TT(CNPJ)) > 11 then
              begin
                Codigo := UMyMod.BuscaEmLivreY_Def('entidades', 'Codigo', stIns, 0);
                UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
                'CodUsu', 'RazaoSocial', 'CNPJ', 'IE', 'Tipo', 'EUF', 'PUF'], [
                'Codigo'], [
                Codigo, Fantasia, CNPJ, IE, 0, EUF, PUF], [
                Codigo], True);
              end else begin
                Codigo := UMyMod.BuscaEmLivreY_Def('entidades', 'Codigo', stIns, 0);
                UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
                'CodUsu', 'Nome', 'CPF', 'Tipo', 'EUF', 'PUF'], [
                'Codigo'], [
                Codigo, Fantasia, Copy(CNPJ, 4), 1, EUF, PUF], [
                Codigo], True);
              end;
            end;
          end else begin
            MeIgnor.Lines.Add('Linha ' + FormatFloat('0000', I) + ' registro tipo 50 Emitente = "' + Emitente + '" n�o importado!');
          end;
        end;
        54:
        begin
          CNPJ       := GridS.Cells[01,02];
          Modelo     := GridS.Cells[01,03];
          Serie      := GridS.Cells[01,04];
          NumNF      := GridS.Cells[01,05];
          CFOP       := GridS.Cells[01,06];
          CST        := GridS.Cells[01,07];
          NUMITEM    := GridS.Cells[01,08];
          CODPROD    := GridS.Cells[01,09];
          QTDADE     := Geral.DMV(GridS.Cells[02,10]);
          VLRPROD    := Geral.DMV(GridS.Cells[02,11]);
          VLRDESC    := Geral.DMV(GridS.Cells[02,12]);
          BASECALC   := Geral.DMV(GridS.Cells[02,13]);
          BASESUBTRI := Geral.DMV(GridS.Cells[02,14]);
          VLRIPI     := Geral.DMV(GridS.Cells[02,15]);
          ALIQICMS   := Geral.DMV(GridS.Cells[02,16]);
          //  Cuidado! apenas temp
          //GraGruX    := 0;//Geral.IMV(CodProd);
{
          if DefineReduzido(TRIM(CODPROD), False, GraGruX, NomeProd) then
          begin
}
            UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'load_s54', False, [
            'TIPO', 'CNPJ', 'MODELO',
            'SERIE', 'NUMNF', 'CFOP',
            'CST', 'NUMITEM', 'CODPROD',
            'QTDADE', 'VLRPROD', 'VLRDESC',
            'BASECALC', 'BASESUBTRI', 'VLRIPI',
            'ALIQICMS'], [
            ], [
            TIPO, CNPJ, MODELO,
            SERIE, NUMNF, CFOP,
            CST, NUMITEM, CODPROD,
            QTDADE, VLRPROD, VLRDESC,
            BASECALC, BASESUBTRI, VLRIPI,
            ALIQICMS], [
            ], False);
{
          end else
          begin
            Pagecontrol5.ActivePageIndex := 2;
            Pagecontrol5.Update;
            Application.ProcessMessages;
            //
            MeErros.Lines.Add('Linha ' + FormatFloat('0000', I) +
            ' Produto n�o cadastrado: "' +
            CODPROD + '" NF: ' + NumNF + ' de ' + Data);
            //
          end;
}
        end;
        70:
        begin
          CNPJ      := GridS.Cells[01,02];
          INSCEST   := GridS.Cells[01,03];
          EMISSAO   := Geral.FDT(Geral.ValidaDataBR(GridS.Cells[02,04], False, False), 1);
          UF        := GridS.Cells[01,05];
          MODELO    := GridS.Cells[01,06];
          SERIE     := GridS.Cells[01,07];
          SUBSERIE  := GridS.Cells[01,08];
          NUMNF     := GridS.Cells[01,09];
          CFOP      := GridS.Cells[01,10];
          VALORTOT  := Geral.DMV(GridS.Cells[02,11]);
          BASEICMS  := Geral.DMV(GridS.Cells[02,12]);
          VALORICMS := Geral.DMV(GridS.Cells[02,13]);
          VLRISENTO := Geral.DMV(GridS.Cells[02,14]);
          OUTROS    := Geral.DMV(GridS.Cells[02,15]);
          CIF_FOB   := Geral.IMV(GridS.Cells[01,16]);
          SITUACAO  := GridS.Cells[01,17];
          //
          if DModG.DefineEntidade(CNPJ, False, Transporta, NomeTrsp) then
          begin
            UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'load_s70', False, [
            'TIPO', 'CNPJ', 'INSCEST',
            'EMISSAO', 'UF', 'MODELO',
            'SERIE', 'SUBSERIE', 'NUMNF',
            'CFOP', 'VALORTOT', 'BASEICMS',
            'VALORICMS', 'VLRISENTO', 'OUTROS',
            'CIF_FOB', 'SITUACAO', 'Transporta'], [
            ], [
            TIPO, CNPJ, INSCEST,
            EMISSAO, UF, MODELO,
            SERIE, SUBSERIE, NUMNF,
            CFOP, VALORTOT, BASEICMS,
            VALORICMS, VLRISENTO, OUTROS,
            CIF_FOB, SITUACAO, Transporta], [
            ], False);
          end else
          begin
            Pagecontrol5.ActivePageIndex := 2;
            Pagecontrol5.Update;
            Application.ProcessMessages;
            //
            if NomeForn = '' then
              MeErros.Lines.Add('Linha ' + FormatFloat('0000', I) +
              ' CNPJ/CPF cad. incompleto "' +
              CNPJ + '" NF: ' + NumNF + ' de ' + Data)
            else
              MeErros.Lines.Add('Linha ' + FormatFloat('0000', I) +
              ' CNPJ/CPF n�o cadastrado: "' +
              CNPJ + '" NF: ' + NumNF + ' de ' + Data);
            //
            Fantasia := '   CNPJ: ' + CNPJ + ' NF: ' + NumNF + ' de ' + Data;
            EUF := Geral.GetCodigoUF_da_SiglaUF(UF);
            PUF := EUF;
            // tirar s� da problema!
            if Length(Geral.SoNumero_TT(CNPJ)) > 11 then
            begin
              Codigo := UMyMod.BuscaEmLivreY_Def('entidades', 'Codigo', stIns, 0);
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
              'CodUsu', 'RazaoSocial', 'CNPJ', 'IE', 'Tipo', 'EUF', 'PUF'], [
              'Codigo'], [
              Codigo, Fantasia, CNPJ, IE, 0, EUF, PUF], [
              Codigo], True);
            end else begin
              Codigo := UMyMod.BuscaEmLivreY_Def('entidades', 'Codigo', stIns, 0);
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
              'CodUsu', 'Nome', 'CPF', 'Tipo', 'EUF', 'PUF'], [
              'Codigo'], [
              Codigo, Fantasia, Copy(CNPJ, 4), 1, EUF, PUF], [
              Codigo], True);
            end;
          end;
        end;
        75:
        begin
          DATAINI    := Geral.FDT(Geral.ValidaDataBR(GridS.Cells[02,02], False, False), 1);
          DATAFIN    := Geral.FDT(Geral.ValidaDataBR(GridS.Cells[02,03], False, False), 1);
          CODPROD    := GridS.Cells[01,04];
          CODNCM     := GridS.Cells[01,05];
          DESCRICAO  := Trim(GridS.Cells[01,06]);
          UNIDADE    := GridS.Cells[01,07];
          AliqIPI    := Geral.DMV(GridS.Cells[02,08]);
          AliqICMS   := Geral.DMV(GridS.Cells[02,09]);
          REDBASEICM := Geral.DMV(GridS.Cells[02,10]);
          BASESUBTRI := Geral.DMV(GridS.Cells[02,11]);
          //
          QrLocPrdNom.Close;
          QrLocPrdNom.Params[0].AsString := DESCRICAO;
          QrLocPrdNom.Open;
          GraGruX := QrLocPrdNomGraGruX.Value;
          Nivel1  := QrLocPrdNomGraGru1.Value;
          //
          {
          if (GraGruX = 0) and (DESCRICAO <> '') then
          begin
            QrLocPrdTmp.Close;
            QrLocPrdTmp.Params[0].AsString := DESCRICAO;
            QrLocPrdTmp.Open;
            GraGruX := QrLocPrdTmpGraGruX.Value;
          end;
          }
          //
          if GraGruX = 0 then
          begin
            Nivel1 := 0;
            {
            if (Geral.IMV(CODPROD) < 85) then
            begin
              IncluiProduto(CODPROD, DESCRICAO, UNIDADE, CODNCM, AliqIPI);
            end;
            }
            if DBCheck.CriaFm(TFmSintegra_PesqPrd, FmSintegra_PesqPrd, afmoLiberado) then
            begin
              FmSintegra_PesqPrd.EdNaoLoc_TXT.Text := DESCRICAO;
              FmSintegra_PesqPrd.EdNaoLoc_COD.Text := CODPROD;
              //
              FmSintegra_PesqPrd.EdPesq.Text   := '%' + DESCRICAO + '%';
              FmSintegra_PesqPrd.ShowModal;
              GraGruX := FmSintegra_PesqPrd.FGraGruX;
              Nivel1  := FmSintegra_PesqPrd.FNivel1;
              FmSintegra_PesqPrd.Destroy;
            end;
            if GraGruX <> 0 then
            begin
              MeErros.Lines.Add('Linha ' + FormatFloat('0000', I) +
              ' Produto n�o localizado: "' +
              CODPROD + '" Descri��o: "' + DESCRICAO + '"');
              // ShowMessage(DESCRICAO);
              if Nivel1 <> 0 then
                UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
                'NomeTemp'], ['Nivel1'], [DESCRICAO], [
                Nivel1], False);
            end;
          end;
          //

          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'load_s75', False, [
          'TIPO', 'DATAINI', 'DATAFIN',
          'CODPROD', 'GraGruX', 'Nivel1',
          'CODNCM', 'DESCRICAO', 'UNIDADE',
          'ALIQIPI', 'ALIQICMS', 'REDBASEICM',
          'BASESUBTRI'], [
          ], [
          TIPO, DATAINI, DATAFIN,
          CODPROD, GraGruX, Nivel1,
          CODNCM, DESCRICAO, UNIDADE,
          ALIQIPI, ALIQICMS,
          REDBASEICM, BASESUBTRI], [
          ], False);
        end;
        else
        begin
          MeIgnor.Lines.Add('Linha ' + FormatFloat('0000', I) + ' registro tipo ' + IntToStr(Tipo) + ' n�o importado!');
        end;
      end;
    end;
    BtImporta.Enabled :=
      (*(MeErros.Lines.Count = 0) and*) (EdEmp_Codigo.ValueVariant <> 0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSintegra_Arq.BtNenhumClick(Sender: TObject);
begin
  CGTipos.Value := 0;
end;

function TFmSintegra_Arq.IncluiRegistro10(): Boolean;
const
  cEstrutura = '3';
var
  IdNatOpInfo, Finalidade: String;
begin
  Result := False;
  FtxtLin := '';
  MyObjects.Informa(LaAviso, True, 'Inclu�ndo registro do tipo 10');
  if DModG.QrParamsEmpReg10.Value = 0 then
  begin
    Geral.MensagemBox('O registro tipo 10 � obrigat�rio!' +
    #13#10 + 'O arquivo gerado poder� ser rejeitado!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Result := True;
    Exit;
  end;
  IdNatOpInfo := Geral.SoNumero_TT(RG_IdNatOpInfo.Items[RG_IdNatOpInfo.ItemIndex][1]);
  if IdNatOpInfo <> '3' then
  begin
    Geral.MensagemBox('A "Identifica��o da natureza das opera��es informadas" selecionada n�o tem implementa��o! AVISE A DERMATEK',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Finalidade := Geral.SoNumero_TT(RG_Finalidade.Items[RG_Finalidade.ItemIndex][1]);
  if Finalidade <> '1' then
  begin
    Geral.MensagemBox('A "Finalidade da apresenta��o do arquivo magn�tico" selecionada n�o tem implementa��o! AVISE A DERMATEK',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
{
9  -  R E G I S T R O   T I P O   1 0
MESTRE DO ESTABELECIMENTO

N�	Denomina��o do Campo	Conte�do	Tamanho	Posi��o	Formato
01	Tipo	"10"	02	1	2	N
}
  FcReg := 10;
  if not AddItemX(FcReg, 01, ' ', IntToStr(FcReg)) then Exit;
//02	CGC/MF	CGC/MF do estabelecimento informante	14	3	16	N
  if not AddItemX(FcReg, 02, ' ', Geral.SoNumero_TT(QrEntiCNPJ_CPF.Value)) then Exit;
//03	Inscri��o Estadual	Inscri��o estadual do estabelecimento informante	14	17	30	X
  if not AddItemX(FcReg, 03, ' ', Geral.SoNumero_TT(QrEntiIE_RG.Value)) then Exit;
//04	Nome do Contribuinte	Nome comercial (raz�o social / denomina��o) do contribuinte	35	31	65	X
  if not AddItemX(FcReg, 04, ' ', QrEntiNO_ENT.Value, aamTxt) then Exit;
//05	Munic�pio	Munic�pio onde est� domiciliado o estabelecimento informante	30	66	95	X
  if not AddItemX(FcReg, 05, ' ', QrEntixCidade.Value, aamTxt) then Exit;
//06	Unidade da Federa��o	Unidade da Federa��o referente ao Munic�pio	2	96	97	X
  if not AddItemX(FcReg, 06, ' ', QrEntixUF.Value) then Exit;
//07	Fax	N�mero do fax do estabelecimento informante	10	98	107	N
  if not AddItemX(FcReg, 07, ' ', Geral.SoNumero_TT(DModG.QrParamsEmpFisFax.Value), aamNum) then Exit;
//08	Data Inicial	A data do in�cio do per�odo referente �s informa��es prestadas	8	108	115	N
  if not AddItemX(FcReg, 08, ' ', FormatDateTime('YYYYMMDD', TPDataIni.Date)) then Exit;
//09	Data Final	A data do fim do per�odo referente �s informa��es prestadas	8	116	123	N
  if not AddItemX(FcReg, 09, ' ', FormatDateTime('YYYYMMDD', TPDataFim.Date)) then Exit;
{
Reda��o original do campo 10, efeitos at� 31.12.02.
10	C�digo da identifica��o da estrutura do arquivo magn�tico entregue	C�digo da identifica��o do Conv�nio utilizado no arquivo magn�tico, conforme tabela abaixo	1	124	124	X
Nova reda��o ao campo 10 pelo Conv. ICMS 142/02, efeitos a partir de 01.01.03.
10	C�digo da identifica��o do Conv�nio
C�digo da identifica��o da estrutura do arquivo magn�tico entregue, conforme tabela abaixo	1	124	124	X
}
  if not AddItemX(FcReg, 10, ' ', cEstrutura) then Exit;
//11	C�digo da identifica��o da natureza das opera��es informadas
//C�digo da identifica��o da natureza das opera��es informadas, conforme tabela abaixo	1	125	125	X
  if not AddItemX(FcReg, 11, ' ', IdNatOpInfo) then Exit;
//12	C�digo da finalidade do arquivo magn�tico
//C�digo do finalidade utilizado no arquivo magn�tico, conforme tabela abaixo	1	126	126	X
  if not AddItemX(FcReg, 12, ' ', Finalidade) then Exit;
{
9.1 - OBSERVA��ES:
 Nova reda��o dada ao subitem 9.1.1 pelo Conv. ICMS 19/04, efeitos a partir de 01.07.04.
9.1.1 - Tabela para preenchimento do campo 10:
TABELA DE C�DIGO DE IDENTIFICA��O DA ESTRUTURA DO ARQUIVO MAGN�TICO ENTREGUE
C�digo	Descri��o do c�digo de identifica��o da estrutura do arquivo
1	Estrutura conforme Conv�nio ICMS 57/95, na vers�o estabelecida pelo Conv�nio ICMS 31/99 e com as altera��es promovidas at� o Conv�nio ICMS 30/02.
2	Estrutura conforme Conv�nio ICMS 57/95, na vers�o estabelecida pelo Conv�nio ICMS 69/02 e com as altera��es promovidas pelo Conv�nio ICMS 142/02.
3	Estrutura conforme Conv�nio ICMS 57/95, com as altera��es promovidas pelo Conv�nio ICMS 76/03.
 Reda��o anterior dada ao subitem 9.1.1 pelo Conv. ICMS 142/02, efeitos de 01.01.03 a 30.06.04.
9.1.1 - Tabela para preenchimento do campo 10:
TABELA DE C�DIGO DE IDENTIFICA��O DA ESTRUTURA DO ARQUIVO MAGN�TICO ENTREGUE
C�digo	Descri��o do c�digo de identifica��o da estrutura do arquivo
1	Estrutura conforme Conv�nio ICMS 57/95 na vers�o do Conv�nio ICMS 31/99
2	Estrutura conforme Conv�nio ICMS 57/95 na vers�o atual

Reda��o anterior dada pelo Conv 69/02 (n�o produziu efeitos devido ao Conv. ICMS 142/02).
09.1.1 - Tabela para preenchimento do campo 10:
TABELA DE C�DIGO DA IDENTIFICA��O DO CONV�NIO
C�digo	Descri��o do c�digo de identifica��o do Conv�nio
1	Conv�nio ICMS 31/99
2	Conv�nio ICMS xx/02
Reda��o original, efeitos at� 31.12.02.
9.1.1 - Tabela para preenchimento do campo 10:
TABELA DE C�DIGO DA IDENTIFICA��O DO CONV�NIO
C�digo	Descri��o do c�digo de identifica��o do Conv�nio
1	Conv�nio 31/99
Acrescido o subitem 9.1.1.1 pelo Conv. ICMS 39/00, efeitos a partir de 01.08.00.
9.1.1.1 - o contribuinte dever� entregar o arquivo magn�tico atualizado de acordo com a vers�o mais recente do Conv�nio 57/95;
9.1.2 - Tabela para preenchimento do campo 11
Tabela para C�digo da identifica��o da natureza das opera��es informadas
C�digo	Descri��o do c�digo da natureza das opera��es
1	Interestaduais somente opera��es sujeitas ao regime de Substitui��o Tribut�ria
2	Interestaduais - opera��es com ou sem Substitui��o Tribut�ria
3	Totalidade das opera��es do informante
 Nova reda��o dada pelo Conv. 69/02, com efeitos a partir de 01.01.03
9.1.3 - Tabela para preenchimento do campo 12:
TABELA DE FINALIDADES DA APRESENTA��O DO ARQUIVO MAGN�TICO
C�digo	Descri��o da finalidade
1	Normal
2	Retifica��o total de arquivo: substitui��o total de informa��es prestadas pelo contribuinte referentes a este per�odo
3	Retifica��o aditiva de arquivo: acr�scimo de informa��o n�o inclu�da em arquivos j� apresentados
5	Desfazimento: arquivo de informa��o referente a opera��es/presta��es n�o efetivadas . Neste caso, o arquivo dever� conter, al�m dos registros tipo 10 e tipo 90, apenas os registros
Referentes as opera��es/presta��es n�o efetivadas
Reda��o original com efeitos de 28.06.95 a 31.12.02
9.1.3 - Tabela para preenchimento do campo 12:
TABELA DE FINALIDADES DA APRESENTA��O DO ARQUIVO MAGN�TICO
C�digo	Descri��o da finalidade
1	Normal
2	Retifica��o total de arquivo: substitui��o total de informa��es prestadas pelo contribuinte referentes a este per�odo
3	Retifica��o aditiva de arquivo: acr�scimo de informa��o n�o inclu�da em arquivos j� apresentados
4	Retifica��o corretiva de arquivo: substitui��o de informa��o relativa a documento j� informado
5	Desfazimento: arquivo de informa��o referente a opera��es/presta��es n�o efetivadas . Neste caso, o arquivo dever� conter, al�m dos registros tipo 10 e tipo 90, apenas os registros referentes �s opera��es/presta��es n�o efetivadas
 Acrescido subitem 9.1.4 pelo Conv. 69/02, efeitos a partir de 01.01.03
9.1.4 - No caso de "Retifica��o corretiva de arquivo: substitui��o de informa��o relativa a documento j� informado" prevista nas vers�es anteriores do Conv�nio 57/95, dever� ser enviado novo arquivo completo, utilizando a "Retifica��o total de arquivo" (c�digo 2).
Acrescido subitem 9.1.5 pelo Conv. 18/04, efeitos a partir de 01.07.04
9.1.5 - Fica a crit�rio de cada Unidade da Federa��o a ado��o do C�digo 3 do subitem 9.1.3 - Retifica��o aditiva de arquivo: acr�scimo de informa��o n�o inclu�da em arquivo j� apresentado;
}
  Result := AdicionaLinhaAoMeGerado(FTxtLin, FQtd10);
end;

function TFmSintegra_Arq.IncluiRegistro11(): Boolean;
begin
  Result := False;
  FtxtLin := '';
  MyObjects.Informa(LaAviso, True, 'Inclu�ndo registro do tipo 11');
  if DModG.QrParamsEmpReg11.Value = 0 then
  begin
    Geral.MensagemBox('O registro tipo 11 � obrigat�rio!' +
    #13#10 + 'O arquivo gerado poder� ser rejeitado!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Result := True;
    Exit;
  end;
{
1 0  -  R E G I S T R O   T I P O   1 1
{
Dados Complementares do Informante
N�	Denomina��o do Campo	Conte�do	Tamanho	Posi��o	Formato
}
//01	Tipo	"11"	02	1	2	N
  FcReg := 11;
  FtxtLin := '';
  if not AddItemX(11, 01, ' ', IntToStr(FcReg)) then Exit;
  //
//02	Logradouro	Logradouro	34	3	36	X
  if not AddItemX(FcReg, 02, ' ', QrEntiLOGRADOURO.Value, aamTxt) then Exit;
//03	N�mero	N�mero	5	37	41	N
  if not AddItemX(FcReg, 03, ' ', FormatFloat('0', QrEntiNumero.Value), aamTxt) then Exit;
//04	Complemento	Complemento	22	42	63	X
  if not AddItemX(FcReg, 04, ' ', QrEntiCompl.Value, aamTxt) then Exit;
//05	Bairro	Bairro	15	64	78	X
  if not AddItemX(FcReg, 05, ' ', QrEntiBairro.Value, aamTxt) then Exit;
//06	CEP	C�digo de Endere�amento Postal	8	79	86	N
  if not AddItemX(FcReg, 06, ' ', FormatFloat('0', QrEntiCEP.Value)) then Exit;
//07	Nome do Contato	Pessoa respons�vel para contatos	28	87	114	X
  if not AddItemX(FcReg, 07, ' ', DModG.QrParamsEmpFisContNom.Value, aamTxt) then Exit;
//08	Telefone	N�mero dos telefones para contatos	12	115	126	N
  if not AddItemX(FcReg, 08, ' ', Geral.SoNumero_TT(DModG.QrParamsEmpFisContTel.Value), aamNum) then Exit;
  //
  Result := AdicionaLinhaAoMeGerado(FTxtLin, FQtd11);
end;

function TFmSintegra_Arq.IncluiRegistro50(): Boolean;
var
  CNPJ_CPF, IE, Data, UF, EmitenteNF, Situacao,
  xTotal, xBC, xICMS, xIsen, xOutr, xAliq: String;
  vTotal, vBC, vICMS, vIsen, vOutr, vAliq: Double;
  CFOP: Integer;
begin
  PageControl1.ActivePageIndex := 1;
  Result := False;
  MyObjects.Informa(LaAviso, True, 'Inclu�ndo registros do tipo 50');
  QrCabA.First;
  while not QrCabA.Eof do
  begin
{
Nova reda��o dada ao subitem 11.1.4 pelo Conv. ICMS 39/00, efeitos a partir de 01.08.00:
11.1.4 - No caso de documentos com mais de uma al�quota de ICMS e/ou mais de um C�digo Fiscal de Opera��o - CFOP,
deve ser gerado para cada combina��o de �al�quota� e �CFOP� um registro tipo 50,
com valores nos campos monet�rios (11, 12, 13, 14 e 15) correspondendo � soma dos itens que comp�e o mesmo,
de tal forma que as somas dos valores dos campos monet�rios dos diversos registros que representam uma mesma nota fiscal,
corresponder�o aos valores totais da mesma;
Reda��o original, efeitos at� 31.07.00:
11.1.4 - No caso de documentos com mais de uma al�quota de ICMS, deve ser gerado um registro por al�quota;
neste caso, os valores dos CAMPOS 11, 12, 13, 14 e 15 referem-se � soma das opera��es da al�quota informada no registro;
}
    QrTotN.Close;
    QrTotN.Params[00].AsInteger := QrCabAFatID.Value;
    QrTotN.Params[01].AsInteger := QrCabAFatNum.Value;
    QrTotN.Params[02].AsInteger := QrCabAEmpresa.Value;
    QrTotN.Open;
    //
    QrSubN.Close;
    QrSubN.Params[00].AsInteger := QrCabAFatID.Value;
    QrSubN.Params[01].AsInteger := QrCabAFatNum.Value;
    QrSubN.Params[02].AsInteger := QrCabAEmpresa.Value;
    QrSubN.Open;
    QrSubN.First;
    while not QrSubN.Eof do
    begin
      if not DefineDadosEntidadeNF(CNPJ_CPF, IE, Data, UF, EmitenteNF, Situacao) then Exit;
      if not DefineCFOPEntrada(EmitenteNF, QrSubNprod_CFOP.Value, CFOP) then Exit;
      //
      // Total da nota
      if (QrTotNprod_vNF.Value > 0) and (QrCabAStatus.Value = 100) then
      begin
        vTotal := (QrSubNprod_vNF.Value / QrTotNprod_vNF.Value) * QrCabAICMSTot_vNF.Value;
      end else
        vTotal := 0;
      if (Length(QrCabACNPJ_CPF.Value) > 11) and (QrCabAStatus.Value = 100) then
      begin
        // Base de C�lculo
        if QrTotNICMS_vBC.Value = QrCabAICMSTot_vBC.Value then
          vBC := QrSubNICMS_vBC.Value
        else begin
          if QrTotNprod_vNF.Value > 0 then
          begin
            vBC := (QrSubNprod_vNF.Value / QrTotNprod_vNF.Value) * QrCabAICMSTot_vBC.Value;
          end else
            vBC := 0;
        end;
        vAliq  := QrSubNICMS_pICMS.Value;
        // Valor do ICMS
        if QrTotNICMS_vICMS.Value = QrCabAICMSTot_vICMS.Value then
          vICMS  := QrSubNICMS_vICMS.Value
        else begin
          if QrTotNprod_vNF.Value > 0 then
          begin
            vICMS := (QrSubNprod_vNF.Value / QrTotNprod_vNF.Value) * QrCabAICMSTot_vICMS.Value;
          end else
            vICMS := 0;
        end;
        // valor isento ou n�o tributado
        vIsen  := 0; // ??? Parei Aqui
        // Outras (valor que n�o confira d�bito ou cr�dito de ICMS)
        vOutr := 0;
        //if not ValorPositivo(vTotal - vBC, vOutr) then Exit; // ??? Parei Aqui
      end else begin
        // Pessoa F�sica!
        Memo2.Lines.Add('Venda para pessoa f�sica! NF = ' +
        IntToStr(QrCabAide_nNF.Value));
        //, 'Aviso', MB_OK+MB_ICONWARNING);
        vBC   := 0;
        vAliq := 0;
        vICMS := 0;
        vIsen := 0;
        vOutr := 0;
      end;
{
11 - REGISTRO TIPO 50
Nota Fiscal, modelo 1 ou 1-A (c�digo 01), quanto ao ICMS, a crit�rio de cada UF, Nota Fiscal do Produtor, modelo 4 (c�digo 04),
Nota Fiscal/Conta de Energia El�trica, modelo 6 (c�digo 06),
Nota Fiscal de Servi�o de Comunica��o, modelo 21 (c�digo 21),
Nota Fiscal de Servi�os de Telecomunica��es, modelo 22 (c�digo 22)�
Nota Fiscal Eletr�nica, modelo 55 (c�digo 55).
Reda��o anterior dada ao cabe�alho do item 11 pelo Conv. ICMS 76/03, efeitos de 16.10.03 a 28.03.06.
11 - REGISTRO TIPO 50
Nota Fiscal, modelo 1 ou 1-A (c�digo 01), quanto ao ICMS, a crit�rio de cada UF, Nota Fiscal do Produtor, modelo 4 (c�digo 04),
Nota Fiscal/Conta de Energia El�trica, modelo 6 (c�digo 06),
Nota Fiscal de Servi�o de Comunica��o, modelo 21 (c�digo 21),
Nota Fiscal de Servi�os de Telecomunica��es, modelo 22 (c�digo 22).
Reda��o anterior dada pelo Conv 69/02, efeitos de 01.01.03 a 15.10.03
11 - REGISTRO TIPO 50
NOTA FISCAL, MODELO 1 OU 1-A (c�digo 01) , QUANTO AO ICMS
NOTA FISCAL/CONTA DE ENERGIA EL�TRICA, MODELO 6 (c�digo 06),
NOTA FISCAL DE SERVI�O DE COMUNICA��O, MODELO 21
NOTA FISCAL DE SERVI�OS DE TELECOMUNICA��ES, MODELO 22 (c�digo 22)
}
      Result := False;
      FtxtLin := '';
{
1 1  -  R E G I S T R O   T I P O   5 0
Dados Complementares do Informante
N�	Denomina��o do Campo	Conte�do	Tamanho	Posi��o	Formato
}
    //01	Tipo	"50"	02	1	2	N
      FcReg := 50;
      FtxtLin := '';
      if not AddItemX(FcReg, 01, ' ' , IntToStr(FcReg)) then Exit;
      //
    //02	CNPJ	CNPJ do remetente nas entradas e do destinat�rio nas sa�das	14	3	16	N
      if not AddItemX(FcReg, 02, ' ', CNPJ_CPF) then Exit;
    //03	Inscri��o Estadual	Inscri��o Estadual do remetente nas entradas e do destinat�rio nas sa�das	14	17	30	X
      if not AddItemX(FcReg, 03, ' ', IE) then Exit;
    //04	Data de emiss�o ou recebimento	Data de emiss�o na sa�da ou de recebimento na entrada	8	31	38	N
      if not AddItemX(FcReg, 04, ' ', Data) then Exit;
    //05	Unidade da Federa��o	Sigla da unidade da Federa��o do remetente nas entradas e do destinat�rio nas sa�das 	2	39	40	X
      if not AddItemX(FcReg, 05, ' ', UF) then Exit;
    //06	Modelo	C�digo do modelo da nota fiscal	2	41	42	N
      if not AddItemX(FcReg, 06, ' ', FormatFloat('0', QrCabAide_mod.Value)) then Exit;
    //07	S�rie	S�rie da nota fiscal	3	43	45	X
      if not AddItemX(FcReg, 07, ' ', DefSerie(QrCabAide_serie.Value)) then Exit;
    //08	N�mero	N�mero da nota fiscal	6	46	51	N
      if not AddItemX(FcReg, 08, ' ', FormatFloat('0', QrCabAide_nNF.Value), aamNum) then Exit;
    //09	CFOP	C�digo Fiscal de Opera��o e Presta��o	4	52	55	N
      if not AddItemX(FcReg, 09, ' ', FormatFloat('0', CFOP)) then Exit;
    //10	Emitente	Emitente da Nota Fiscal (P-pr�prio/T-terceiros)	1	56	56	X
      if not AddItemX(FcReg, 10, ' ', EmitenteNF) then Exit;
    //11	Valor Total	Valor total da nota fiscal (com 2 decimais)	13	57	69	N
      xTotal := Geral.FTX(vTotal, 11, 2, siPositivo);
      if not AddItemX(FcReg, 11, ' ', xTotal) then Exit;
    //12	Base de C�lculo do ICMS	Base de C�lculo do ICMS (com 2 decimais)	13	70	82	N
      xBC := Geral.FTX(vBC, 11, 2, siPositivo);
      if not AddItemX(FcReg, 12, ' ', xBC) then Exit;
    //13	Valor do ICMS	Montante do imposto (com 2 decimais)	13	83	95	N
      xICMS := Geral.FTX(vICMS, 11, 2, siPositivo);
      if not AddItemX(FcReg, 13, ' ', xICMS) then Exit;
    //14	Isenta ou n�o-tributada	Valor amparado por isen��o ou n�o incid�ncia (com 2 decimais)	13	96	108	N
      xIsen := Geral.FTX(vIsen, 11, 2, siPositivo);
      if not AddItemX(FcReg, 14, ' ', xIsen) then Exit;
    //15	Outras	Valor que n�o confira d�bito ou cr�dito do ICMS (com 2 decimais)	13	109	121	N
      xOutr := Geral.FTX(vOutr, 11, 2, siPositivo);
      if not AddItemX(FcReg, 15, ' ', xOutr) then Exit;
    //16	Al�quota	Al�quota do ICMS (com 2 decimais)	4	122	125	N
      xAliq := Geral.FTX(vAliq, 2, 2, siPositivo);
      if not AddItemX(FcReg, 16, ' ', xAliq) then Exit;
{Nova reda��o ao campo 17 pelo Conv. ICMS 142/02, efeitos a partir de 01.01.03.
17	Situa��o	Situa��o da Nota Fiscal	1	126	126	X
Reda��o anterior do campo 17 dada pelo Conv. ICMS 69/02 (n�o produziu efeitos devido ao Conv. ICMS 142/02).
17	Situa��o	Situa��o da nota fiscal Quanto ao cancelamento	1	126	126	X
}
      if not AddItemX(FcReg, 17, ' ', Situacao) then Exit;
{
11.1 - OBSERVA��ES
Nova reda��o ao item 11.1.1 pelo Conv. ICMS 111/08, efeitos a partir de 01.10.08.
11.1.1 - Este registro dever� ser composto por contribuinte do ICMS, obedecendo a sistem�tica semelhante � da escritura��o dos livros Registro de Entradas e Registro de Sa�das, mesmo quando desobrigado de escritur�-los.
Reda��o original, efeitos at� 30.09.08.
11.1.1 - Este registro dever� ser composto por contribuinte do ICMS, obedecendo a sistem�tica semelhante � da escritura��o dos livros Registro de Entradas e Registro de Sa�das;
11.1.2 - Nas opera��es decorrente das vendas de produtos agropecu�rios, inclusive caf� em gr�o, efetuadas pelo Banco do Brasil S.A., em leil�o na bolsa de mercadorias, em nome de produtores (Conv. ICMS 46/94 de 29 de mar�o de 1994 e Conv. ICMS 132/95 de 11 de dezembro de 1995), os CAMPOS 02, 03 e 05 devem conter os dados do emitente da Nota Fiscal, devendo a cada registro Tipo 50 corresponder um registro Tipo 71, com os dados dos estabelecimentos remetente e destinat�rio;
Acrescido o subitem 11.1.2A, pelo Conv. ICMS 142/02, efeitos a partir de 01.01.03.
11.1.2A - Nas opera��es decorrentes de servi�os de telecomunica��es ou comunica��es o registro dever� ser composto apenas na aquisi��o;
Revigorado com nova reda��o o subitem 11.1.3, pelo Conv. ICMS 76/03, efeitos a partir de 16.10.03.
11.1.3 - Em se tratando de Nota Fiscal/Conta de Energia El�trica, Nota Fiscal de Servi�os de Comunica��o e de Telecomunica��o, o registro dever� ser composto apenas na entrada de energia el�trica ou aquisi��o de servi�os de comunica��o e telecomunica��es;
Revogado pelo Conv. 69/02, efeitos de 01.01.03 at� 15.10.03
11.1.3 - Em se tratando de Nota Fiscal/Conta de Energia El�trica e Nota Fiscal de Servi�o de Telecomunica��es, o registro dever� ser composto apenas na entrada de energia ou aquisi��o de servi�o de telecomunica��es;
Nova reda��o dada ao subitem 11.1.4 pelo Conv. ICMS 39/00, efeitos a partir de 01.08.00.
11.1.4 - No caso de documentos com mais de uma al�quota de ICMS e/ou mais de um C�digo Fiscal de Opera��o - CFOP, deve ser gerado para cada combina��o de �al�quota� e �CFOP� um registro tipo 50, com valores nos campos monet�rios (11, 12, 13, 14 e 15) correspondendo � soma dos itens que comp�e o mesmo, de tal forma que as somas dos valores dos campos monet�rios dos diversos registros que representam uma mesma nota fiscal, corresponder�o aos valores totais da mesma;
Reda��o original, efeitos at� 31.07.00.
11.1.4 - No caso de documentos com mais de uma al�quota de ICMS, deve ser gerado um registro por al�quota; neste caso, os valores dos CAMPOS 11, 12, 13, 14 e 15 referem-se � soma das opera��es da al�quota informada no registro;
11.1.5 - CAMPO 02
Nova reda��o dada ao subitem 11.1.5.1 pelo Conv. ICMS 76/03, efeitos a partir de 16.10.03
11.1.5.1 - Em se tratando de pessoas n�o obrigadas � inscri��o no CNPJ/MF, preencher com o CPF;
Reda��o original, efeitos at� 15.10.03
11.1.5.1 - Em se tratando de pessoas n�o obrigadas � inscri��o no CGC/MF, preencher com o CPF.
11.1.5.2 - Tratando-se de opera��es com o exterior ou com pessoa f�sica n�o inscrita no CPF zerar o campo;
11.1.6 - CAMPO 03
11.1.6.1 - Tratando-se de opera��es com o exterior ou com pessoas n�o obrigadas � inscri��o estadual, o campo assumir� o conte�do "ISENTO";
11.1.6.2 - Na hip�tese de registro referente a fornecimento feito por produtor agropecu�rio, em que seja obrigat�ria a emiss�o de Nota Fiscal de Entrada, a unidade da Federa��o poder� dispor sobre qual informa��o pretende neste campo;
11.1.7 - CAMPO 05 - Tratando-se de opera��es com o exterior, colocar "EX";
11.1.8 - CAMPO 06 - Preencher conforme c�digos da tabela de modelos de documentos fiscais, do subitem 3.3;
11.1.9 - CAMPO 07
11.1.9.1. Em se tratando de documento sem seria��o deixar em branco as tr�s posi��es.
11.1.9.2 - No caso de Nota Fiscal, modelo 1 e 1-A (c�digo 01), preencher com o algarismo designativo da s�rie ( "1", "2" etc..) deixando em branco as posi��es n�o significativas.
11.1.9.3 - Em se tratando de documentos com seria��o indicada por letra, preencher com a respectiva letra (B, C ou E). No caso de documentos fiscais de "S�rie �nica" preencher com a letra U.
11.1.9.4 - Em se tratando dos documentos fiscais de s�rie indicada por letra seguida da express�o "�nica" ( "S�rie B-�nica" , "S�rie C-�nica ou S�rie E-�nica"), preencher com a respectiva letra (B , C ou E) na primeira posi��o e com a letra U na segunda posi��o, deixando em branco a posi��o n�o significativa.
Nova reda��o dada pelo Conv. 69/02, com efeitos a partir de 01.01.03
11.1.9.5 - No caso de documento fiscal de "S�rie �nica" seguida por algarismo ar�bico ( "S�rie �nica 1", "S�rie �nica 2" etc...) preencher com a letra U na primeira posi��o, e o algarismo respectivo dever� ser indicado nas posi��es subseq�entes.
Reda��o original com efeitos de 28.06.95 a 31.12.02
11.1.9.5 - No caso de documento fiscal de "S�rie �nica" seguida por algarismo ar�bico ( "S�rie �nica 1", "S�rie �nica 2" etc...) preencher com a letra U na primeira posi��o, deixando em branco as posi��es n�o significativas. O algarismo respectivo dever� ser indicado no campo Subs�rie.
Acrescido o subitem 11.1.9A pelo Conv. ICMS 12/06, efeitos a partir de 29.03.06.
11.1.9A - CAMPO 08 - Se o n�mero do documento fiscal tiver mais de 6 d�gitos, preencher com os 6 �ltimos d�gitos;
Nova reda��o dada pelo Conv. 69/02, com efeitos a partir de 01.01.03
11.1.10 - CAMPO 10 - Preencher com "P" se nota fiscal emitida pelo contribuinte informante (pr�prio) ou "T", se emitida por terceiros.
Reda��o original com efeitos de 28.06.95 a 31.12.02
11.1.10 - CAMPO 08
Revogado os subitens 11.1.10.1 a 11.1.10.4, pelo Conv. ICMS 76/03, efeitos a partir de 16.10.03.
11.1.10.1 - Em se tratando de documento fiscal sem subseria��o deixar em branco as duas posi��es.
11.1.10.2 - No caso de Nota Fiscal, modelo 1 e 1-A (c�digo 01), preencher com brancos."
11.1.10.3 - No caso de subs�rie designada por algarismo aposto � letra indicativa da s�rie ( "S�rie B Subs�rie 1", "S�rie B Subs�rie 2" ou "S�rie B-1", "S�rie B-2" etc..) ou de documento fiscal de s�rie �nica com subs�rie designada por algarismo ( "S�rie �nica 1", "S�rie �nica 2" etc...), preencher com o algarismo de subs�rie ( "1", "2" etc...) deixando em branco a posi��o n�o significativa.
11.1.10.4 No caso de subseria��o de documentos fiscais de s�ries "A-�nica", "B-�nica", "C-�nica" e "E-�nica", colocar "U" na primeira posi��o e o n�mero da subs�rie na segunda posi��o;
Nova reda��o dada pelo Conv. ICMS 69/02, efeitos a partir de 01.01.03.
11.1.11 - CAMPO 09 e 16 - Ver observa��o 11.1.4;
Reda��o anterior com efeitos de 28.06.95 a 31.12.02
11.1.11 - CAMPO 10 e 16 - Ver observa��o 11.1.4;
Reda��o original, efeitos at� 31.07.00.
11.1.11 - CAMPO 10 - Um registro para cada CFOP do Documento Fiscal, sendo que constar�, no registro, CFOP igual ao lan�ado no livro fiscal respectivo.
11.1.12 - CAMPO 12 - Base de C�lculo do ICMS
11.1.12.1 - Colocar o valor da base de c�lculo do ICMS, quando n�o se tratar de opera��o ou presta��o com substitui��o tribut�ria;
11.1.12.2 - Quando se tratar de opera��o ou presta��o com substitui��o tribut�ria deve-se:
11.1.12.2.1 - colocar o valor da base de c�lculo ICMS pr�prio, quando se tratar de opera��o de sa�da e o informante for o substituto tribut�rio;
11.1.12.2.2 - zerar o campo quando o informante n�o for o substituto tribut�rio.
11.1.13 - CAMPO 13 - Valor do ICMS
11.1.13.1 - Colocar o valor do ICMS, quando n�o se tratar de opera��o com substitui��o tribut�ria;
11.1.13.2 - Quando se tratar de opera��o com substitui��o tribut�ria deve-se:
11.1.13.2.1 - colocar o valor do ICMS pr�prio, quando se tratar de opera��o de sa�da e o informante for o substituto tribut�rio;
11.1.13.2.2 - zerar o campo quando o informante n�o for o substituto tribut�rio.
Nova reda��o dada ao subitem 11.1.14 pelo Conv. ICMS 42/09, efeitos a partir de 09.07.09.
11.1.14 - CAMPO 17 - Preencher o campo de acordo com a tabela abaixo:
Situa��o	Conte�do do Campo
Documento Fiscal Normal	N
Documento Fiscal Cancelado	S
Lan�amento Extempor�neo de Documento Fiscal Normal	E
Lan�amento Extempor�neo de Documento Fiscal Cancelado	X
Documento com USO DENEGADO - exclusivamente para uso dos emitentes de Nota Fiscal Eletr�nica - Modelo 55 e Conhecimento de Transporte Eletr�nico, Modelo 57.	2
Documento com USO inutilizado - exclusivamente para uso dos emitentes de Nota Fiscal Eletr�nica - Modelo 55 e Conhecimento de Transporte Eletr�nico, Modelo 57.	4
Reda��o anterior dada ao subitem 11.1.14 pelo Conv. ICMS 12/06, efeitos de 29.03.06 a 08.07.09.
11.1.14 - CAMPO 17 - Preencher o campo de acordo com a tabela abaixo:
Situa��o	Conte�do do Campo
Documento Fiscal Normal	N
Documento Fiscal Cancelado	S
Lan�amento Extempor�neo de Documento Fiscal Normal	E
Lan�amento Extempor�neo de Documento Fiscal Cancelado	X
Documento com USO DENEGADO - exclusivamente para uso dos emitentes de Nota Fiscal Eletr�nica - Modelo 55	2
Documento com USO inutilizado - exclusivamente para uso dos emitentes de Nota Fiscal Eletr�nica - Modelo 55	4
Reda��o anterior dada ao subitem 11.1.14 pelo Conv. ICMS 142/02, efeitos de 01.01.03 a 28.03.06.
11.1.14 - CAMPO 17 - Preencher o campo de acordo com a tabela abaixo:
Situa��o	Conte�do do Campo
Documento Fiscal Normal	N
Documento Fiscal Cancelado	S
Lan�amento Extempor�neo de Documento Fiscal Normal	E
Lan�amento Extempor�neo de Documento Fiscal Cancelado	X
O campo 17 deve ser preenchido conforme os seguintes crit�rios:
l 	com "N", para lan�amento normal de documento fiscal n�o cancelado;
l 	com "S", para lan�amento de documento regularmente cancelado;
l 	com "E", para Lan�amento Extempor�neo de Documento Fiscal n�o cancelado;"
l 	com "X", para Lan�amento Extempor�neo de Documento Fiscal cancelado;
Reda��o original, efeitos at� 31.12.02.
11.1.14 - CAMPO 17 - Preencher com "S", se se tratar de documento fiscal regularmente cancelado e com "N", caso contr�rio.
11.1.15 - o registro das antigas Notas Fiscais, modelo 1, s�ries A, B, C ou U, e modelo 3, s�rie E, somente poder� se referir a emiss�es anteriores a 01 de mar�o de 1996.
Acrescido o subitem 11.1.16, pelo Conv. ICMS 12/05, efeitos a partir de 05/04/05.
11.1.16 - Nos documentos fiscais emitidos em opera��es ou presta��es tamb�m registradas em equipamento Emissor de Cupom Fiscal - ECF os campos 11 a 16 devem ser zerados, n�o devendo ser informados registros tipo 54.
Nova reda��o dada ao item 12 pelo Conv. ICMS 69/02, efeitos a partir de 01.01.03.
}
      if not AdicionaLinhaAoMeGerado(FTxtLin, FQtd50) then Exit;
      QrSubN.Next;
    end;
    QrCabA.Next;
  end;
  Result := True;
end;

function TFmSintegra_Arq.IncluiRegistro51(): Boolean;
var
  CNPJ_CPF, IE, Data, UF, EmitenteNF, Situacao,
  xTotal, xIPI, xIsen, xOutr: String;
  vTotal, vBC, vIPI, vIsen, vOutr: Double;
  CFOP: Integer;
begin
  {
  Geral.MensagemBox(
  'O registro tipo 51 n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
  (*
   Parei aqui!!!
   � pela al�quota de ICMS ou de IPI???
  *)
  }
  Result := False;
  MyObjects.Informa(LaAviso, True, 'Inclu�ndo registros do tipo 51');
  QrCabA.First;
  while not QrCabA.Eof do
  begin
{
12.1.6 - CAMPO 08 - Valem as observa��es do subitem 11.1.4;

Nova reda��o dada ao subitem 11.1.4 pelo Conv. ICMS 39/00, efeitos a partir de 01.08.00:
11.1.4 - No caso de documentos com mais de uma al�quota de ICMS e/ou mais de um C�digo Fiscal de Opera��o - CFOP,
deve ser gerado para cada combina��o de �al�quota� e �CFOP� um registro tipo 50,
com valores nos campos monet�rios (11, 12, 13, 14 e 15) correspondendo � soma dos itens que comp�e o mesmo,
de tal forma que as somas dos valores dos campos monet�rios dos diversos registros que representam uma mesma nota fiscal,
corresponder�o aos valores totais da mesma;
Reda��o original, efeitos at� 31.07.00:
11.1.4 - No caso de documentos com mais de uma al�quota de ICMS, deve ser gerado um registro por al�quota;
neste caso, os valores dos CAMPOS 11, 12, 13, 14 e 15 referem-se � soma das opera��es da al�quota informada no registro;
}
    QrSubO.Close;
    QrSubO.Params[00].AsInteger := QrCabAFatID.Value;
    QrSubO.Params[01].AsInteger := QrCabAFatNum.Value;
    QrSubO.Params[02].AsInteger := QrCabAEmpresa.Value;
    QrSubO.Open;
    //
    QrTotO.Close;
    QrTotO.Params[00].AsInteger := QrCabAFatID.Value;
    QrTotO.Params[01].AsInteger := QrCabAFatNum.Value;
    QrTotO.Params[02].AsInteger := QrCabAEmpresa.Value;
    QrTotO.Open;
    //
    QrSubO.First;
    while not QrSubO.Eof do
    begin
      if not DefineDadosEntidadeNF(CNPJ_CPF, IE, Data, UF, EmitenteNF, Situacao) then Exit;
      if not DefineCFOPEntrada(EmitenteNF, QrSubOprod_CFOP.Value, CFOP) then Exit;
      //
{
        vTotal := QrSubOprod_vProd.Value;
        vBC    := QrSubOIPI_vBC.Value;
        //vAliq  := QrSubOIPI_pIPI.Value;
        vIPI   := QrSubOIPI_vIPI.Value;
        if not ValorPositivo(vTotal - vBC, vIsen) then Exit; // ??? Parei Aqui
        vOutr  := 0; // ??? Parei Aqui
}
      {
      if QrCabAide_nNF.Value = 20050 then
        ShowMessage('Ver valores!');
      }
      // Total da nota
      if (QrTotOprod_vNF.Value > 0) and (QrCabAStatus.Value = 100) then
      begin
        vTotal := (QrSubOprod_vNF.Value / QrTotOprod_vNF.Value) * QrCabAICMSTot_vNF.Value;
      end else
        vTotal := 0;
(*
      if (Length(QrCabACNPJ_CPF.Value) > 11) and (QrCabAStatus.Value = 100) then
      begin
*)
        // Base de C�lculo
        (*//if QrTotOIPI_vBC.Value = QrCabAICMSTot_vBC.Value then
          vBC := QrSubOIPI_vBC.Value
        else begin
          if QrTotOprod_vNF.Value > 0 then
          begin
            vBC := (QrSubOprod_vNF.Value / QrTotOprod_vNF.Value) * QrCabAICMSTot_vBC.Value;
          end else
            vBC := 0;
        end;
        *)
          vBC := QrSubOIPI_vBC.Value;
          vIPI  := QrSubOIPI_vIPI.Value;
        //vAliq  := QrSubOICMS_pIPI.Value;
        // Valor do ICMS
{
        if QrTotOIPI_vIPI.Value = QrCabAICMSTot_vICMS.Value then
          vIPI  := QrSubOIPI_vIPI.Value
        else begin
          if QrTotOprod_vNF.Value > 0 then
          begin
            vIPI := (QrSubOprod_vNF.Value / QrTotOprod_vNF.Value) * QrCabAICMSTot_vICMS.Value;
          end else
            vIPI := 0;
        end;
}
        // valor isento ou n�o tributado
        //vIsen  := 0; // ??? Parei Aqui
        // Outras (valor que n�o confira d�bito ou cr�dito de ICMS)
        vOutr := 0;
        if not ValorPositivo(vTotal - vBC - vIPI, vIsen) then Exit; // ??? Parei Aqui
(*
      end else begin
        // Pessoa F�sica!
        vBC   := 0;
        //vAliq := 0;
        vIPI  := 0;
        vIsen := 0;
        vOutr := 0;
      end;
*)
{
1 2  -  R E G I S T R O   T I P O   5 1
TOTAL DE NOTA FISCAL QUANTO AO IPI
N�	Denomina��o do Campo	Conte�do	Tamanho	Posi��o	Formato
01	Tipo	"51"	2	1	2	N
}
      FcReg := 51;
      FtxtLin := '';
      if not AddItemX(FcReg, 01, ' ', IntToStr(FcReg)) then Exit;
//02	CNPJ	CNPJ do remetente nas entradas e do destinat�rio nas sa�das	14	3	16	N
      if not AddItemX(FcReg, 02, ' ', CNPJ_CPF) then Exit;
//03	Inscri��o Estadual	Inscri��o Estadual do remetente nas entradas e do destinat�rio nas sa�das	14	17	30	X
      if not AddItemX(FcReg, 03, ' ', IE) then Exit;
//04	Data de emiss�o/recebimento	Data de emiss�o na sa�da ou recebimento na entrada	8	31	38	N
      if not AddItemX(FcReg, 04, ' ', Data) then Exit;
//05	Unidade da Federa��o	Sigla da unidade da Federa��o do remetente nas entradas e do destinat�rio nas sa�das	2	39	40	X
      if not AddItemX(FcReg, 05, ' ' , UF) then Exit;
//06	S�rie	S�rie da nota fiscal	3	41	43	X
      if not AddItemX(FcReg, 06, ' ', DefSerie(QrCabAide_serie.Value)) then Exit;
//07	N�mero	N�mero da nota fiscal	6	44	49	N
      if not AddItemX(FcReg, 07, ' ', FormatFloat('0', QrCabAide_nNF.Value), aamNum) then Exit;
//08	CFOP	C�digo Fiscal de Opera��o e Presta��o	4	50	53	N
      if not AddItemX(FcReg, 08, ' ', FormatFloat('0', CFOP)) then Exit;
//09	Valor Total	Valor total da nota fiscal (com 2 decimais)	13	54	66	N
      xTotal := Geral.FTX(vTotal, 11, 2, siPositivo);
      if not AddItemX(FcReg, 09, ' ', xTotal) then Exit;
//10	Valor do IPI	Montante do IPI (com 2 decimais)	13	67	79	N
      xIPI := Geral.FTX(vIPI, 11, 2, siPositivo);
      if not AddItemX(FcReg, 10, ' ', xIPI) then Exit;
//11	Isenta ou n�o-tributada - IPI	Valor amparado por isen��o ou n�o incid�ncia do IPI (com 2 decimais)	13	80	92	N
      xIsen := Geral.FTX(vIsen, 11, 2, siPositivo);
      if not AddItemX(FcReg, 11, ' ', xIsen) then Exit;
//12	Outras - IPI	Valor que n�o confira d�bito ou cr�dito do IPI (com 2 decimais)	13	93	105	N
      xOutr := Geral.FTX(vOutr, 11, 2, siPositivo);
      if not AddItemX(FcReg, 12, ' ', xOutr) then Exit;
//13	Brancos	Brancos	20	106	125	X Nova reda��o ao campo 14 pelo Conv. ICMS 142/02, efeitos a partir de 01.01.03.
      if not AddItemX(FcReg, 13, ' ', '') then Exit;
{
14	Situa��o	Situa��o da Nota Fiscal	1	126	126	X Reda��o anterior do campo 14 dada pelo Conv. ICMS 69/02 (n�o produziu efeitos devido ao Conv. ICMS 142/02).
14	Situa��o	Situa��o do documento fiscal quanto ao cancelamento	1	126	126	X
}
      if not AddItemX(FcReg, 14, ' ', Situacao) then Exit;
{
12.1 - OBSERVA��ES:
12.1.1 - Este registro dever� ser composto somente por contribuintes do IPI, obedecendo a sistem�tica semelhante � da escritura��o dos livros Registro de Entradas e Registro de Sa�das;
12.1.2 - CAMPO 02 - Valem as observa��es do subitem 11.1.5;
12.1.3 - CAMPO 03 - Valem as observa��es do subitem 11.1.6;
12.1.4 - CAMPO 05 - Valem as observa��es do subitem 11.1.7;
12.1.5 - CAMPO 06 - Valem as observa��es do subitem 11.1.9;
Nova reda��o ao subitem 12.1.6 pelo Conv. ICMS 142/02, efeitos a partir de 01.01.03.
12.1.6 - CAMPO 08 - Valem as observa��es do subitem 11.1.4;
Reda��o anterior do subitem 12.1.6 dada pelo Conv. ICMS 69/02 (n�o produziu efeitos devido ao Conv. ICMS 142/02).
12.1.6 - CAMPO 09 - Valem as observa��es do subitem 11.1.11;
Nova reda��o ao subitem 12.1.7 pelo Conv. ICMS 142/02, efeitos a partir de 01.01.03.
12.1.7 - CAMPO 14 - Valem as observa��es do subitem 11.1.14.
Reda��o anterior do subitem 12.1.7 dada pelo Conv. ICMS 69/02 (n�o produziu efeitos devido ao Conv. ICMS 142/02).
12.1.7 - CAMPO 15 - Valem as observa��es do subitem 11.1.14;
}
      if not AdicionaLinhaAoMeGerado(FTxtLin, FQtd51) then Exit;
      QrSubO.Next;
    end;
    QrCabA.Next;
  end;
  Result := True;
end;

function TFmSintegra_Arq.IncluiRegistro53(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 53 n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro54: Boolean;
var
  xCST, CNPJ_CPF, IE, Data, UF, EmitenteNF, Situacao, CodProd: String;
  nItem, Incremento: Integer;
  PIS_COFINS,
  vQuant, vValor, vDesco, vBC_ICMS, vBCST_ICMS, vIPI, pICMS(*, vBC_IPI, vBCST_IPI*): Double;
  xQuant, xValor, xDesco, xBC_ICMS, xBCST_ICMS, xIPI, xICMS: String;
  //
  Sigla, NCM, Nome: String;
  IPI_Alq, ICMSAliqSINTEGRA, ICMS_pRedBC, ICMS_pRedBCST, ICMSST_BaseSINTEGRA: Double;
  function CriaLinha(CFOP: Integer): Boolean;
  begin
    Result := False;
{
1 4  -  R E G I S T R O   T I P O   5 4
PRODUTO
N�	Denomina��o do Campo	Conte�do	Tamanho	Posi��o	Formato
01	Tipo	"54"	2	1	2	N
}
      FcReg := 54;
      FtxtLin := '';
      if not AddItemX(FcReg, 001, ' ' , IntToStr(FcReg)) then Exit;
      //
//02	CNPJ	CNPJ do remetente nas entradas e do destinat�rio nas sa�das	14	3	16	N
      if not AddItemX(FcReg, 02, ' ', CNPJ_CPF) then Exit;
//03	Modelo	C�digo do modelo da nota fiscal	2	17	18	N
      if not AddItemX(FcReg, 03, ' ', FormatFloat('0', QrCabAide_mod.Value)) then Exit;
//04	S�rie	S�rie da nota fiscal 	3	19	21	X
      if not AddItemX(FcReg, 04, ' ', DefSerie(QrCabAide_serie.Value)) then Exit;
//05	N�mero	N�mero da nota fiscal	6	22	27	N
      if not AddItemX(FcReg, 05, ' ', FormatFloat('0', QrCabAide_nNF.Value), aamNum) then Exit;
//06	CFOP	C�digo Fiscal de Opera��o e Presta��o	4	28	31	N
      if not AddItemX(FcReg, 06, ' ', FormatFloat('0', CFOP)) then Exit;
{
Nova reda��o dada ao campo 7 do item 14 - Registro 54 pelo Conv. ICMS 12/05, efeitos a partir de 05.04.05.
07	CST	C�digo da Situa��o Tribut�ria	3	32	34	X
Reda��o anterior do campo 7 do item 14 - Registro Tipo 54 dada pelo Conv. ICMS 69/02, efeitos de 01.01.03 a 04/04/05
07	CST	C�digo da Situa��o Tribut�ria	3	32	34
}
      if not AddItemX(FcReg, 07, ' ' , xCST) then Exit;
//08	N�mero do Item	N�mero de ordem do item na nota fiscal	3	35	37	N
      if not AddItemX(FcReg, 08, ' ', FormatFloat('0', nItem + Incremento)) then Exit;
//09	C�digo do Produto ou Servi�o	C�digo do produto ou servi�o do informante	14	38	51	X
      if not AddItemX(FcReg, 09, ' ', CodProd, aamNum) then Exit;
//10	Quantidade	Quantidade do produto (com 3 decimais)	11	52	62	N
      xQuant := Geral.FTX(vQuant, 8, 3, siPositivo);
      if not AddItemX(FcReg, 10, ' ', xQuant) then Exit;
//11	Valor do Produto	Valor bruto do produto (valor unit�rio multiplicado por quantidade) - com 2 decimais	12	63	74	N
      xValor := Geral.FTX(vValor, 10, 2, siPositivo);
      if not AddItemX(FcReg, 11, ' ', xValor) then Exit;
//12	Valor do Desconto / Despesa Acess�ria 	Valor do Desconto Concedido no item (com 2 decimais).	12	75	86	N
      xDesco := Geral.FTX(vDesco, 10, 2, siPositivo);
      if not AddItemX(FcReg, 12, ' ', xDesco) then Exit;
//13	Base de C�lculo do ICMS	Base de c�lculo do ICMS (com 2 decimais)	12	87	98	N
      xBC_ICMS := Geral.FTX(vBC_ICMS, 10, 2, siPositivo);
      if not AddItemX(FcReg, 13, ' ', xBC_ICMS) then Exit;
//14	Base de C�lculo do ICMS para Substitui��o Tribut�ria
//    Base de c�lculo do ICMS de reten��o na Substitui��o Tribut�ria (com 2 decimais)	12	99	110	N
      xBCST_ICMS := Geral.FTX(vBCST_ICMS, 10, 2, siPositivo);
      if not AddItemX(FcReg, 14, ' ', xBCST_ICMS) then Exit;
//15	Valor do IPI	Valor do IPI (com 2 decimais)	12	111	122	N
      xIPI := Geral.FTX(vIPI, 10, 2, siPositivo);
      if not AddItemX(FcReg, 15, ' ', xIPI) then Exit;
//16	Al�quota do ICMS	Al�quota Utilizada no C�lculo do ICMS (com 2 decimais)	4	123	126	N
      xICMS := Geral.FTX(pICMS, 2, 2, siPositivo);
      if not AddItemX(FcReg, 16, ' ', xICMS) then Exit;
      //
      Result := AdicionaLinhaAoMeGerado(FTxtLin, FQtd54);
      //
  end;
var
  CFOP, Controle: Integer;
begin
  Result := False;
  MyObjects.Informa(LaAviso, True, 'Inclu�ndo registros do tipo 54');
  Controle := 0;
  QrCabA.First;
  while not QrCabA.Eof do
  begin
    //if QrCabAide_nNF.Value = 882 then
      //ShowMessage('Ver valores!');
    
    QrItsI.Close;
    QrItsI.Params[00].AsInteger := QrCabAFatID.Value;
    QrItsI.Params[01].AsInteger := QrCabAFatNum.Value;
    QrItsI.Params[02].AsInteger := QrCabAEmpresa.Value;
    QrItsI.Open;
    //
    QrItsN.Close;
    QrItsN.Params[00].AsInteger := QrCabAFatID.Value;
    QrItsN.Params[01].AsInteger := QrCabAFatNum.Value;
    QrItsN.Params[02].AsInteger := QrCabAEmpresa.Value;
    QrItsN.Open;
    //
    QrItsO.Close;
    QrItsO.Params[00].AsInteger := QrCabAFatID.Value;
    QrItsO.Params[01].AsInteger := QrCabAFatNum.Value;
    QrItsO.Params[02].AsInteger := QrCabAEmpresa.Value;
    QrItsO.Open;
    //
    while not QrItsI.Eof do
    begin
      if QrItsIprod_vProd.Value > 0 then
      begin
        if QrItsI.RecNo = 1 then
        begin
          if QrItsInItem.Value = 0 then
            Incremento := 1
          else
            Incremento := 0;
        end;
        //Incremento := QrItsI.RecNo;
        if not DefineDadosEntidadeNF(CNPJ_CPF, IE, Data, UF, EmitenteNF, Situacao) then Exit;
        if not DefineCFOPEntrada(EmitenteNF, QrItsIprod_CFOP.Value, CFOP) then Exit;
        nItem := QrItsInItem.Value;
        if not QrItsN.Locate('nItem', nItem, []) then
        begin
          MeAvisos.Lines.Add(
          'N�o foi poss�vel localizar o registro do ICMS do item ' + FormatFloat(
          '0', nItem) + ' da NF ' + FormatFloat('000000', QrCabAide_nNF.Value) +
          '.');//, 'Erro', MB_OK+MB_ICONERROR);
          //Screen.Cursor := crDefault;
          //Exit;
        end;
        if not QrItsO.Locate('nItem', nItem, []) then
        begin

        end else begin
        end;
        vQuant     := QrItsIprod_qCom.Value;
        vValor     := QrItsIprod_vProd.Value;
        vDesco     := QrItsIprod_vDesc.Value;
        //
        vBC_ICMS   := QrItsNICMS_vBC.Value;
        vBCST_ICMS := QrItsNICMS_vBCST.Value;
        //vBC_IPI    := QrItsOIPI_vBC.Value;
        //vBCST_IPI  := QrItsNICMS_vBCST.Value;
        vIPI       := QrItsOIPI_vIPI.Value;
        pICMS      := QrItsNICMS_pICMS.Value;
        if (Length(QrCabACNPJ_CPF.Value) > 11) and (QrCabAStatus.Value = 100) then
        begin
        end else begin
          // Pessoa F�sica!
          // Se n�o fizer d� aviso de "N�o encontrado registro tipo 50 correspondente" no sintegra
          vBC_ICMS   := 0;
          pICMS      := 0;
        end;
        //
        if QrItsINivel1.Value = 0 then
        begin
          CodProd             := QrItsIprod_cProd.Value;
          Sigla               := QrItsIprod_uCom.Value;
          NCM                 := Geral.SoNumero_TT(QrItsIprod_NCM.Value);
          IPI_Alq             := 0; // Parei aqui!!! ver!!!
          ICMSAliqSINTEGRA    := 0; // Parei aqui!!! ver!!!
          ICMS_pRedBC         := 0; // Parei aqui!!! ver!!!
          ICMS_pRedBCST       := 0; // Parei aqui!!! ver!!!
          ICMSST_BaseSINTEGRA := 0; // Parei aqui!!! ver!!!
          Nome                := QrItsIprod_xProd.Value;
          //
          if Trim(CodProd) = '' then
            CodProd := 'N/D';
          //if Trim(NCM) = '' then
            //NCM := '999999999';
        end else begin
          QrLocod.Close;
          QrLocod.Params[0].AsInteger := QrItsINivel1.Value;
          QrLocod.Open;
          CodProd             := FormatFloat('0', QrLocodCodUsu.Value);
          Sigla               := QrLocodSigla.Value;
          NCM                 := Geral.SoNumero_TT(QrLocodNCM.Value);
          IPI_Alq             := QrLocodIPI_Alq.Value;
          ICMSAliqSINTEGRA    := QrLocodICMSAliqSINTEGRA.Value;
          ICMS_pRedBC         := QrLocodICMS_pRedBC.Value;
          ICMS_pRedBCST       := QrLocodICMS_pRedBCST.Value;
          ICMSST_BaseSINTEGRA := QrLocodICMSST_BaseSINTEGRA.Value;
          Nome                := QrLocodNome.Value;
        end;
        if (CodProd = '0') then
          Geral.MensagemBox('Cuidado! Produto com c�digo = "' + CodProd + '"!',
          'Aviso', MB_OK+MB_ICONWARNING);
  {
        UMyMod.SQLReplace(DmodG.QrUpdPID1, 'SINTEGRAPM', [
       'Ativo'], ['Codigo'], [1], [CodProd], False);
  }
  {
        UMyMod.SQLIns_ON_DUPLICATE_KEY(DmodG.QrUpdPID1, 'SINTEGRAPM', False, [
        'Ativo'], ['Codigo'], ['Ativo'], [1], [CodProd], [1], False);
  }
        // para gerar um 75 para cada 54
        if CkGera75x54.Checked then
          Controle := Controle + 1;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(DmodG.QrUpdPID1, 'SINTEGRAPM', False, [
        'Sigla', 'NCM', 'IPI_Alq', //'vBC_IPI', 'vBCST_IPI',
        'ICMSAliqSINTEGRA', 'ICMS_pRedBC', 'ICMS_pRedBCST',
        'ICMSST_BaseSINTEGRA', 'Nome', 'Ativo'
        ], ['Controle', 'Codigo'], ['Ativo'], [
        Sigla, NCM, IPI_Alq, //vBC_IPI, vBCST_IPI,
        ICMSAliqSINTEGRA, ICMS_pRedBC, ICMS_pRedBCST,
        ICMSST_BaseSINTEGRA, Nome, 1
        ], [Controle, CodProd], [1], False, 'Itens', 1);

        //

        xCST       := FormatFloat('0', QrItsNICMS_Orig.Value) +
                      FormatFloat('00', QrItsNICMS_CST.Value);
        //
        if not CriaLinha(CFOP) then Exit;
        //
      end;  
      QrItsI.Next;
    end;
    //
    vQuant     := 0;
    vDesco     := 0;
    xCST       := '';
    CodProd    := '';
    vBC_ICMS   := 0;
    vBCST_ICMS := 0;
    vIPI       := 0;
    pICMS      := 0;
    if Ck_54_900.Checked then
    begin
      if QrCabAICMSTot_vFrete.Value > 0 then
      begin
        nItem      := 991;
        vValor     := QrCabAICMSTot_vFrete.Value;
        if not CriaLinha(CFOP) then Exit;
      end;
      //
      if QrCabAICMSTot_vSeg.Value > 0 then
      begin
        nItem      := 992;
        vValor     := QrCabAICMSTot_vSeg.Value;
        if not CriaLinha(CFOP) then Exit;
      end;
      //
      PIS_COFINS := QrCabAICMSTot_vPIS.Value + QrCabAICMSTot_vCOFINS.Value;
      if PIS_COFINS > 0 then
      begin
        nItem      := 993;
        vValor     := PIS_COFINS;
        if not CriaLinha(CFOP) then Exit;
      end;
      { ???
      if QrCabAICMSTot_v.Value > 0 then
      begin
        nItem      := 997;
        vValor     := QrCabAICMSTot_v.Value;
        if not CriaLinha() then Exit;
      end;
      }
      if QrCabAISSQNtot_vServ.Value > 0 then
      begin
        nItem      := 998;
        vValor     := QrCabAISSQNtot_vServ.Value;
        if not CriaLinha(CFOP) then Exit;
      end;
      if QrCabAICMSTot_vOutro.Value > 0 then
      begin
        nItem      := 999;
        vValor     := QrCabAICMSTot_vOutro.Value;
        if not CriaLinha(CFOP) then Exit;
      end;
    end;
    //
    QrCabA.Next;
  end;
  //
  Result := True;
  //
end;

function TFmSintegra_Arq.IncluiRegistro55(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 55 n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro56(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 56 n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro57(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 57 n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro60A(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 60A n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro60D(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 60D n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro60I(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 60I n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro60M(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 60M n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro60R(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 60R n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro61(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 61 n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro61R(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 61R n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro70(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 70 n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro71(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 71 n�o est� implementado! Solicite � DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro74(): Boolean;
  procedure ItemNaoExportado(CodMot, TxtMot: String);
  const
    Exportado = 0;
  var
    My_Idx: Integer;
  begin
    FExportarItem := False;
    My_Idx := DmProd.QrSMIC2My_Idx.Value;
    UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stUpd, '_smic2_', False, [
      'Exportado'], ['My_Idx'], [Exportado], [My_Idx], False);
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stIns, 'listerr1', False, [
    'Codigo', 'Texto1'], ['My_Idx'], [
    CodMot, TxtMot], [My_Idx], False);
    //
  end;
const
  PGT_CodUsu  = 0;
  PGT_Codigo  = 0;
  GG1_CodUsu  = 0;
  GG1_Codigo  = 0;
  GraCusPrc   = 0;
  SoPositivos = True;
var
  //
  CNPJ, IE, UF, Terceiro, NomeReduzido, NCM: String;
  PT, SitProd, Exportados, Empresa, Controle: Integer;
  CodProd, xQtde, xValr, xSitProd, Sigla, Nome: String;
  IPI_Alq, ICMSAliqSINTEGRA, ICMS_pRedBC, ICMS_pRedBCST, ICMSST_BaseSINTEGRA: Double;
begin
  Empresa := DModG.QrEmpresasCodigo.Value;
  //
  DmProd.GeraEstoqueEm(Empresa, TPDataFim.Date, testqSINTEGRA,
  PGT_CodUsu, PGT_Codigo, GG1_CodUsu, GG1_Codigo, GraCusPrc, SoPositivos, LaAviso);
  //
  Result := False;
{
19A  -  R E G I S T R O   T I P O   7 4
REGISTRO DE INVENT�RIO
}

  FcReg := 74;
  MyObjects.Informa(LaAviso, True, 'Inclu�ndo registros do tipo 74');
  //
  Exportados := 0;
  Controle   := 0;
  //
  DmProd.QrSMIC2.First;
  while not DmProd.QrSMIC2.Eof do
  begin
    FExportarItem := True;
    // Pr�prio X Terceiro:
    PT := 0;
    if DmProd.QrSMIC2Empresa.Value = (*DModG.QrEmpresasCodigo.Value*) Empresa then PT := 10;
    if DmProd.QrSMIC2EntiSitio.Value = (*DModG.QrEmpresasCodigo.Value*) Empresa then PT := PT + 1;
    //
    case PT of
      // de terceiros com terceiros (inv�lido)
      0: SitProd := 0;
      // de terceiros com a empresa (3)
      1: SitProd := 3;
      // da empresa com terceiros (2)
      10,11:
      begin
        // case (4) em transito, ou (5) inaproveit�vel
        case DmProd.QrSMIC2SitProd.Value of
          1,2,3: if PT = 10 then
            SitProd := 2
          else
            SitProd := 1;
          4,5: SitProd := DmProd.QrSMIC2SitProd.Value;
          else SitProd := -1; // desconhecido
        end;
      end;
      else SitProd := -1; // desconhecido!
    end;
    case SitProd of
      -1: ItemNaoExportado('037.001.-01', 'Produto de terceiro em poder de terceiro');
       0: ItemNaoExportado('037.001.000', 'Situa��o do produto n�o definida (se � pr�prio e com quem est�)');
       1..5: (* Nada *);
       else
         ItemNaoExportado('037.001.' + FormatFloat('000', Sitprod), 'Situa��o do produto desconhecida');
     end;
    //
    case SitProd of
      1:
      begin
        CNPJ := '00000000000000';
        IE   := '              ';
        UF   := DmProd.QrSMIC2EMP_NO_UF.Value;
      end;
      2:
      begin
        CNPJ := DmProd.QrSMIC2EMP_CNPJ.Value;
        IE   := Geral.SoNumero_TT(DmProd.QrSMIC2EMP_IE.Value);
        UF   := DmProd.QrSMIC2EMP_NO_UF.Value;
        Terceiro := IntToStr(DmProd.QrSMIC2Empresa.Value);
      end;
      3:
      begin
        CNPJ := DmProd.QrSMIC2ENS_CNPJ.Value;
        IE   := Geral.SoNumero_TT(DmProd.QrSMIC2ENS_IE.Value);
        UF   := DmProd.QrSMIC2ENS_NO_UF.Value;
        Terceiro := IntToStr(DmProd.QrSMIC2EntiSitio.Value);
      end;
      else
      begin
        CNPJ := '';
        IE   := '';
        UF   := '';
      end;
    end;
    if Length(CNPJ) <> 14 then
      ItemNaoExportado('038.014.000', 'CNPJ de terceiro (c�digo = ' + Terceiro + ') n�o definido');
    if Length(IE) = 0     then
      ItemNaoExportado('052.020.000', 'I.E. de terceiro (c�digo = ' + Terceiro + ') n�o definido');
    if Length(UF) <> 2    then
      ItemNaoExportado('072.002.000', 'UF de terceiro (c�digo = ' + Terceiro + ') n�o definido');
    //
    //  Parei aqui Ver se faz pelo GraGru1 ou pelo graGruX!
    if DmProd.QrSMIC2GraGruX.Value = 0 then
      ItemNaoExportado('017.020.000', 'Produto sem c�digo de reduzido');
    if DmProd.QrSMIC2QTDE.Value <= 0 then
      ItemNaoExportado('074.021.000', 'Quantidade do item zerado ou negativo');
    if DmProd.QrSMIC2PrcCusUni.Value <= 0 then
    begin
      if DmProd.QrSMIC2GraCusPrc.Value = 0 then
      begin
        if DmProd.QrSMIC2.FieldByName('GraCusPrc').AsString = '' then // C�digo nulo
          ItemNaoExportado('095.017.001', 'Pre�o n�o definido na lista')
        else
          ItemNaoExportado('095.017.001', 'Lista de pre�os n�o definida');
      end else
        ItemNaoExportado('095.017.000', 'Valor unit�rio do item zerado ou negativo');
    end;
    //
    NomeReduzido := DmProd.FormaNomeProduto(DmProd.QrSMIC2NO_PRD.Value, DmProd.QrSMIC2NO_TAM.Value,
      DmProd.QrSMIC2NO_COR.Value, DmProd.QrSMIC2PrintTam.Value, DmProd.QrSMIC2PrintCor.Value);
    //
    if Trim(NomeReduzido) = '' then
      ItemNaoExportado('206.080.000', 'Nome do item n�o definido');
    //
    NCM := Geral.SoNumero_TT(DmProd.QrSMIC2NCM.Value);
    if Length(NCM) < 7 then
      ItemNaoExportado('290.010.000', 'NCM inv�lido: ' + DmProd.QrSMIC2NCM.Value);
    //
    if Trim(DmProd.QrSMIC2SIGLA.Value) = '' then
      ItemNaoExportado('330.003.000', 'Unidade de medida n�o definida');
    //
    //
    if FExportarItem then
    begin
      FtxtLin := '';
      CodProd := FormatFloat('0', DmProd.QrSMIC2GraGruX.Value);
      if CodProd = '0' then
        CodProd := 'N/D';
      //
      //N�	Denomina��o do Campo	Conte�do	Tamanho	Posi��o	Formato
      //01	Tipo	"74"	2	1	2	N
      if not AddItemX(FcReg, 01, ' ', IntToStr(FcReg)) then Exit;
      //02	Data do Invent�rio 	Data do Invent�rio no formato AAAAMMDD 	8	3	10	N
      if not AddItemX(FcReg, 02, ' ', FormatDateTime('YYYYMMDD', TPDataFim.Date)) then Exit;
      //03	C�digo do Produto 	C�digo do produto do informante	14	11	24	X
      if not AddItemX(FcReg, 03, ' ', CodProd, aamNum) then Exit;
      //04	Quantidade	Quantidade do produto (com 3 decimais)	13	25	37	N
      xQtde := Geral.FTX(DmProd.QrSMIC2Qtde.Value, 10, 3, siPositivo);
      if not AddItemX(FcReg, 04, ' ', xQtde) then Exit;
      //05	Valor do Produto	Valor bruto do produto (valor unit�rio multiplicado por quantidade) - com 2 decimais	13	38	50	N
      xValr := Geral.FTX(DmProd.QrSMIC2ValorTot.Value, 11, 2, siPositivo);
      if not AddItemX(FcReg, 05, ' ', xValr) then Exit;
      //06	C�digo de Posse das Mercadorias Inventariadas	C�digo de Posse das Mercadorias Inventariadas, conforme tabela abaixo	1	51	51	X
      xSitProd := FormatFloat('0', SitProd);
      if not AddItemX(FcReg, 06, ' ', xSitProd) then Exit;
      //07	CNPJ do Possuidor / Propriet�rio	CNPJ do Possuidor da Mercadoria de propriedade do Informante, ou do propriet�rio da Mercadoria em poder do Informante	14	52	65	N
      if not AddItemX(FcReg, 07, ' ', CNPJ) then Exit;
      //08	Inscri��o Estadual do Possuidor / Propriet�rio	Inscri��o Estadual do Possuidor da Mercadoria de propriedade do Informante, ou do propriet�rio da Mercadoria em poder do Informante	14	66	79	X
      if not AddItemX(FcReg, 08, ' ', IE) then Exit;
      //09	UF do Possuidor/ Propriet�rio	Unidade da Federa��o do Possuidor da Mercadoria de propriedade do Informante, ou do propriet�rio da Mercadoria em poder do Informante	2	80	81	X
      if not AddItemX(FcReg, 09, ' ', UF) then Exit;
      //10	Brancos	 	45	82	126	X
      if not AddItemX(FcReg, 10, ' ', ' ') then Exit;
      //
      if not AdicionaLinhaAoMeGerado(FTxtLin, FQtd74) then
        Exit
      else
        Exportados := Exportados + 1;
      //
      QrLocod.Close;
      QrLocod.Params[0].AsInteger := QrItsINivel1.Value;
      QrLocod.Open;
      //CodProd             := j� defino acima
      Sigla               := DmProd.QrSMIC2SIGLA.Value;
      NCM                 := Geral.SoNumero_TT(DmProd.QrSMIC2NCM.Value);
      IPI_Alq             := 0; // Parei aqui!!! ver!!!
      ICMSAliqSINTEGRA    := 0; // Parei aqui!!! ver!!!
      ICMS_pRedBC         := 0; // Parei aqui!!! ver!!!
      ICMS_pRedBCST       := 0; // Parei aqui!!! ver!!!
      ICMSST_BaseSINTEGRA := 0; // Parei aqui!!! ver!!!
      Nome                :=  NomeReduzido;
      //
      if CkGera75x54.Checked then
        Controle := Controle + 1;
      UMyMod.SQLIns_ON_DUPLICATE_KEY(DmodG.QrUpdPID1, 'SINTEGRAPM', False, [
      'Sigla', 'NCM', 'IPI_Alq', //'vBC_IPI', 'vBCST_IPI',
      'ICMSAliqSINTEGRA', 'ICMS_pRedBC', 'ICMS_pRedBCST',
      'ICMSST_BaseSINTEGRA', 'Nome', 'Ativo'
      ], ['Controle', 'Codigo'], ['Ativo'], [
      Sigla, NCM, IPI_Alq, //vBC_IPI, vBCST_IPI,
      ICMSAliqSINTEGRA, ICMS_pRedBC, ICMS_pRedBCST,
      ICMSST_BaseSINTEGRA, Nome, 1
      ], [Controle, CodProd], [1], False, 'Itens', 1);
      //
    end else
      MeAvisos.Lines.Add(
      'N�o foi poss�vel exportar o registro n� ' + FormatFloat(
      '0', DmProd.QrSMIC2.RecNo) + ' da Tabela de Estoque.');
    DmProd.QrSMIC2.Next;
  end;
  DmProd.ReopenExport2(Exportados, nil, False);
  Result := True;
end;

function TFmSintegra_Arq.IncluiRegistro75: Boolean;
var
  cProd, xProd: String;
begin
  Result := False;
{
20  -  R E G I S T R O   T I P O   7 5
C�DIGO DE PRODUTO OU SERVI�O
}

  FcReg := 75;
  MyObjects.Informa(LaAviso, True, 'Inclu�ndo registros do tipo 75');
  QrPrds.First;
  while not QrPrds.Eof do
  begin
    FtxtLin := '';
    //
    {
    if QrPrdsNivel1.Value <> 0 then
    begin
      cProd := FormatFloat('0', QrPrdsNivel1.Value);
      xProd := QrPrdsNome.Value;
    end else begin
      cProd := QrPrdsprod_cProd.Value;
      xProd := QrPrdsprod_xProd.Value;
    end;
    }
    //cProd := FormatFloat('0', QrPrdsCodigo.Value);
    cProd := QrPrdsCodigo.Value;
    if cProd = '' then
      cProd := 'N/D';
    xProd := QrPrdsNome.Value;

    //
//N�	Denomina��o do Campo	Conte�do	Tamanho	Posi��o	Formato
//01	Tipo	"75"	2	1	2	N
    if not AddItemX(FcReg, 01, ' ', IntToStr(FcReg)) then Exit;
//02	Data Inicial	Data inicial do per�odo de validade das informa��es	8	3	10	N
    if not AddItemX(FcReg, 02, ' ', FormatDateTime('YYYYMMDD', TPDataIni.Date)) then Exit;
//03	Data Final	Data final do per�odo de validade das informa��es	8	11	18	N
    if not AddItemX(FcReg, 03, ' ', FormatDateTime('YYYYMMDD', TPDataFim.Date)) then Exit;
//04	C�digo do Produto ou Servi�o	C�digo do produto ou servi�o utilizado pelo contribuinte	14	19	32	X
    if not AddItemX(FcReg, 04, ' ', cProd) then Exit;
//05	C�digo NCM	Codifica��o da Nomenclatura Comum do Mercosul	8	33	40	X
    if not AddItemX(FcReg, 05, ' ', Geral.SoNumero_TT(QrPrdsNCM.Value)) then Exit;
//06	Descri��o	Descri��o do produto ou servi�o	53	41	93	X
    if not AddItemX(FcReg, 06, ' ', xProd, aamTxt) then Exit;
//07	Unidade de Medida de Comercializa��o 	Unidade de medida de comercializa��o do produto ( un, kg, mt, m3, sc, frd, kWh, etc..)	6	94	99	X
    if not AddItemX(FcReg, 07, ' ', QrPrdsSigla.Value) then Exit;
//08	Al�quota do IPI	Al�quota do IPI do produto (com 2 decimais)	5	100	104	N
    if not AddItemX(FcReg, 08, ' ', Geral.FTX(QrPrdsIPI_Alq.Value, 3, 2, siPositivo)) then Exit;
//09	Al�quota do ICMS	Al�quota do ICMS aplic�vel a mercadoria ou servi�o nas opera��es ou presta��es internas ou naquelas que se tiverem iniciado no exterior (com 2 decimais)	4	105	108	N
    if not AddItemX(FcReg, 09, ' ', Geral.FTX(QrPrdsICMSAliqSINTEGRA.Value, 2, 2, siPositivo)) then Exit;
//10	Redu��o da Base de C�lculo do ICMS	% de Redu��o na base de c�lculo do ICMS, nas opera��es internas (com 2 decimais)	5	109	113	N
    if not AddItemX(FcReg, 10, ' ', Geral.FTX(QrPrdsICMS_pRedBC.Value, 3, 2, siPositivo)) then Exit;
//11	Base de C�lculo do ICMS de Substitui��o Tribut�ria	Base de C�lculo do ICMS de substitui��o tribut�ria (com 2 decimais)	13	114	126	N
    if not AddItemX(FcReg, 11, ' ', Geral.FTX(QrPrdsICMSST_BaseSINTEGRA.Value, 11, 2, siPositivo)) then Exit;
    //
    if not AdicionaLinhaAoMeGerado(FTxtLin, FQtd75) then Exit;
    //
    QrPrds.Next;
  end;
  //
  Result := True;
  //
end;

function TFmSintegra_Arq.IncluiRegistro76(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 76 n�o est� implementado! Avise a DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro77(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 77 n�o est� implementado! Avise a DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro85(): Boolean;
begin
  Result := False;
{
20C  -  R E G I S T R O   T I P O   8 5
INFORMA��ES DE EXPORTA��ES
}

  MyObjects.Informa(LaAviso, True, 'Inclu�ndo registros do tipo 85');
  FcReg := 85;
  QrExpA.First;
  while not QrExpA.Eof do
  begin
    FtxtLin := '';
//N�	Denomina��o do Campo	Conte�do	Tamanho	Posi��o	Formato
//01	Tipo	"85"	02	01	02	X
    if not AddItemX(FcReg, 01, ' ', IntToStr(FcReg)) then Exit;
{Nova reda��o dada ao campo 2 do item 20C - Registro Tipo 85 pelo Conv. ICMS 70/07, efeitos a partir de 12.07.07.
02	Declara��o de Exporta��o/Declara��o Simplificada de  Exporta��o	N� da Declara��o de Exporta��o/ N� Declara��o Simplificada de  Exporta��o	    11	03	13	  N
Reda��o original, efeitos at� 11.07.07.
02	Declara��o de Exporta��o	N� da Declara��o de Exporta��o	11	03	13	N}
    if not AddItemX(FcReg, 02, ' ', Geral.SoNumero_TT(QrExpASINTEGRA_ExpDeclNum.Value)) then Exit;
//03	Data da Declara��o	Data da Declara��o de Exporta��o (AAAAMMDD)	08	14	21	N
    if not AddItemX(FcReg, 03, ' ', FormatDateTime('YYYYMMDD', QrExpASINTEGRA_ExpDeclDta.Value)) then Exit;
{Nova reda��o dada ao campo 4 do item 20C - Registro Tipo 85 pelo Conv. ICMS 70/07, efeitos a partir de 12.07.07.
04	Natureza da Exporta��o	Preencher com:
"1" - Exporta��o Direta
"2" - Exporta��o Indireta
"3" - Exporta��o Direta- Regime Simplificado
"4" - Exporta��o Indireta- Regime Simplificado	01	22	22	X
Reda��o anterior dada ao campo 4 do item 20C - Registro Tipo 85 pelo Conv. ICMS 15/05, efeitos de 01.07.05 a 11.07.07.
04	Natureza da Exporta��o	Preencher com:
"1" - Exporta��o Direta
"2" - Exporta��o Indireta	01	22	22	X
Reda��o original, efeitos de 01.01.05 a 30.06.05.
04	Averba��o	Informa��o quanto � averba��o do Despacho de Exporta��o. (Preencher com "S"- SIM ou "N" - N�o)	01	22	22	X}
    if not AddItemX(FcReg, 04, ' ', Geral.SoNumero_TT(QrExpASINTEGRA_ExpNat.Value)) then Exit;
//05	Registro de Exporta��o	N� do registro de Exporta��o	12	23	34	N
    if not AddItemX(FcReg, 05, ' ', Geral.SoNumero_TT(QrExpASINTEGRA_ExpRegNum.Value)) then Exit;
//06	Data do Registro	Data do Registro de Exporta��o (AAAAMMDD)	08	35	42	N
    if not AddItemX(FcReg, 06, ' ', FormatDateTime('YYYYMMDD', QrExpASINTEGRA_ExpRegDta.Value)) then Exit;
//07	Conhecimento de embarque	N� do conhecimento de embarque	16	43	58	X
    if not AddItemX(FcReg, 07, ' ', QrExpASINTEGRA_ExpConhNum.Value) then Exit;
//08	Data do conhecimento	Data do conhecimento de embarque (AAAAMMDD)	08	59	66	N
    if not AddItemX(FcReg, 08, ' ', FormatDateTime('YYYYMMDD', QrExpASINTEGRA_ExpConhDta.Value)) then Exit;
//09	Tipo do Conhecimento	Informa��o do tipo de conhecimento de transporte (Preencher conforme tabela de tipo de documento de carga do SISCOMEX - anexa)	02	67	68	N
    if not AddItemX(FcReg, 09, ' ', Geral.SoNumero_TT(QrExpASINTEGRA_ExpConhTip.Value)) then Exit;
//10	Pa�s	C�digo do pa�s de destino da mercadoria (Preencher conforme tabela do SISCOMEX)	04	69	72	N
    if not AddItemX(FcReg, 10, ' ', Geral.SoNumero_TT(QrExpASINTEGRA_ExpPais.Value)) then Exit;
{Nova reda��o dada ao campo 11 do item 20C - Registro Tipo 85 pelo Conv. ICMS 15/05, efeitos a partir de 01.07.05.
11	Reservado	Preencher com zeros	08	73	80	N
Reda��o original, efeitos de 01.01.05 a 30.06.05.
11	Comprovante de Exporta��o	N�mero do Comprovante de Exporta��o	08	73	80	N
Nova reda��o dada ao campo 12 do item 20C - Registro Tipo 85 pelo Conv. ICMS 15/05, efeitos a partir de 01.07.05.}
    if not AddItemX(FcReg, 11, ' ', '0') then Exit;
{12	Data da Averba��o da Declara��o de Exporta��o	Data da averba��o da Declara��o de exporta��o (AAAAMMDD)	08	81	88	N
Reda��o original, efeitos de 01.01.05 a 30.06.05.
12	Data do comprovante de exporta��o	Data do comprovante de exporta��o (AAAAMMDD)	08	81	88	N}
    if not AddItemX(FcReg, 12, ' ', FormatDateTime('YYYYMMDD', QrExpASINTEGRA_ExpAverDta.Value)) then Exit;
{Nova reda��o dada ao campo 13 do item 20C - Registro Tipo 85 pelo Conv. ICMS 15/05, efeitos a partir de 01.07.05.
13	Nota Fiscal de Exporta��o	N�mero de Nota Fiscal de Exporta��o emitida pelo Exportador	06	89	94	N
Reda��o original, efeitos de 01.01.05 a 30.06.05.
13	Nota Fiscal de Exporta��o	N�mero de Nota Fiscal de Exporta��o emitida pela Comercial Exportadora ou "Trading Company"	06	89	94	N}
    if not AddItemX(FcReg, 13, ' ', FormatFloat('0', QrExpAide_nNF.Value)) then Exit;
//14	Data da emiss�o	Data da emiss�o da NF de exporta��o / revenda (AAAAMMDD)	08	95	102	N
    if not AddItemX(FcReg, 14, ' ', FormatDateTime('YYYYMMDD', QrExpAide_dEmi.Value)) then Exit;
//15	Modelo	C�digo do modelo da NF	02	103	104	N
    if not AddItemX(FcReg, 15, ' ', FormatFloat('0', QrExpAide_mod.Value)) then Exit;
//16	S�rie	S�rie da Nota Fiscal	03	105	107	N
    if not AddItemX(FcReg, 16, ' ', DefSerie(QrExpAide_serie.Value)) then Exit;
//17	Brancos	Brancos	19	108	126	X
    if not AddItemX(FcReg, 17, ' ', ' ') then Exit;
    //
    if not AdicionaLinhaAoMeGerado(FTxtLin, FQtd85) then Exit;
    //
    QrExpA.Next;
  end;
  //
  Result := True;
  //
end;

function TFmSintegra_Arq.IncluiRegistro86(): Boolean;
begin
  Result := False;
  Geral.MensagemBox(
  'O registro tipo 86 n�o est� implementado! Avise a DERMATEK!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
end;

function TFmSintegra_Arq.IncluiRegistro90(): Boolean;
const
  tamRep = 90; // 31 at� 120
var
  CNPJ, IE, TxtTipos, Txt: String;
  I: Integer;
begin
  Result := False;
  MyObjects.Informa(LaAviso, True, 'Inclu�ndo registro do tipo 90');
{
21  -  R E G I S T R O   T I P O   9 0
TOTALIZA��O DO ARQUIVO
}
  CNPJ := Geral.SoNumero_TT(QrEntiCNPJ_CPF.Value);
  IE   := Geral.SoNumero_TT(QrEntiIE_RG.Value);
  //
  FcReg := 90;
  TxtTipos := ''; // Tipo = 99
  if FQtd50  > 0 then TxtTipos := TxtTipos + '50' + Geral.FTX(FQtd50, 8, 0, siPositivo);
  if FQtd51  > 0 then TxtTipos := TxtTipos + '51' + Geral.FTX(FQtd51, 8, 0, siPositivo);
  if FQtd53  > 0 then TxtTipos := TxtTipos + '53' + Geral.FTX(FQtd53, 8, 0, siPositivo);
  if FQtd54  > 0 then TxtTipos := TxtTipos + '54' + Geral.FTX(FQtd54, 8, 0, siPositivo);
  if FQtd55  > 0 then TxtTipos := TxtTipos + '55' + Geral.FTX(FQtd55, 8, 0, siPositivo);
  if FQtd56  > 0 then TxtTipos := TxtTipos + '56' + Geral.FTX(FQtd56, 8, 0, siPositivo);
  if FQtd57  > 0 then TxtTipos := TxtTipos + '57' + Geral.FTX(FQtd57, 8, 0, siPositivo);
  if FQtd60  > 0 then TxtTipos := TxtTipos + '60' + Geral.FTX(FQtd60, 8, 0, siPositivo);
  if FQtd61  > 0 then TxtTipos := TxtTipos + '60' + Geral.FTX(FQtd61, 8, 0, siPositivo);
  if FQtd70  > 0 then TxtTipos := TxtTipos + '70' + Geral.FTX(FQtd70, 8, 0, siPositivo);
  if FQtd71  > 0 then TxtTipos := TxtTipos + '71' + Geral.FTX(FQtd71, 8, 0, siPositivo);
  if FQtd74  > 0 then TxtTipos := TxtTipos + '74' + Geral.FTX(FQtd74, 8, 0, siPositivo);
  if FQtd75  > 0 then TxtTipos := TxtTipos + '75' + Geral.FTX(FQtd75, 8, 0, siPositivo);
  if FQtd76  > 0 then TxtTipos := TxtTipos + '76' + Geral.FTX(FQtd76, 8, 0, siPositivo);
  if FQtd77  > 0 then TxtTipos := TxtTipos + '77' + Geral.FTX(FQtd77, 8, 0, siPositivo);
  if FQtd85  > 0 then TxtTipos := TxtTipos + '85' + Geral.FTX(FQtd85, 8, 0, siPositivo);
  if FQtd86  > 0 then TxtTipos := TxtTipos + '86' + Geral.FTX(FQtd86, 8, 0, siPositivo);
  // Se der um n�mero inteiro, igual ser� necess�rio mais uma linha por causa do registro tipo 99
  FQtd90 := 1 + (Length(TxtTipos) div tamRep);
  FQtd99 :=
    FQtd10 + FQtd11 + FQtd50 + FQtd51 + FQtd53 + FQtd54 + FQtd55 + FQtd56 +
    FQtd57 + FQtd60 + FQtd61 + FQtd70 + FQtd71 + FQtd74 + FQtd75 + FQtd76 +
    FQtd77 + FQtd85 + FQtd86 + FQtd90;
  //
  TxtTipos := TxtTipos + '99' + Geral.FTX(FQtd99, 8, 0, siPositivo);
  for I := 1 to FQtd90 do
  begin
    FtxtLin := '';
    Txt := Copy(TxtTipos, 1 + ((I - 1) * tamRep), tamRep);
    if I = FQtd90 then
      while Length(Txt) < tamRep do
        Txt := Txt + ' ';
//N�	Denomina��o do Campo	Conte�do	Tamanho	Posi��o	Formato
//01	Tipo	"90"	02	01	02	X
    if not AddItemX(FcReg, 01, ' ', IntToStr(FcReg)) then Exit;
//02	CGC/MF	CGC/MF do informante	14	3	16	N
    if not AddItemX(FcReg, 02, ' ', CNPJ) then Exit;
//03	Inscri��o Estadual	Inscri��o Estadual do informante	14	17	30	X
    if not AddItemX(FcReg, 03, ' ', IE) then Exit;
//04	Tipo a ser totalizado	Tipo de registro que ser� totalizado pelo pr�ximo campo	2	31	32	N
//05	Total de registros	Total de registros do tipo informado no campo anterior	8	33	40	N
//...	......	............	.....	.....	......	...
    FTxtLin := FTxtLin + Txt;
    // Brancos
    if not AddItemX(FcReg, 22, ' ', '') then Exit;
//06	N�mero de registros tipo 90	 	1	126	126	N
    if not AddItemX(FcReg, 23, ' ', IntToStr(FQtd90)) then Exit;
    {
    FQtd10, FQtd11, FQtd50, FQtd51, FQtd53, FQtd54, FQtd55, FQtd56, FQtd57,
    FQtd60M, FQtd60A, FQtd60D,  FQtd60I, FQtd61, FQtd61R, FQtd70, FQtd71,
    FQtd74, FQtd75, FQtd76, FQtd77, FQtd85, FQtd86, FQtd90: Integer;
    }
    //
    if not AdicionaLinhaAoMeGerado(FTxtLin, FQtd00) then Exit;
    //
  end;
  Result := True;
end;

procedure TFmSintegra_Arq.InfoPosMeGerado();
var
  L, C: Integer;
  MudouLinha: Boolean;
  Linha: String;
  //
  Registro, Campo: Integer;
  SubTipo: Char;
  DescriCampo, ConteudoCampo, ValorCampo: String;
begin
  MudouLinha := False;
  L := Geral.RichRow(MeGerado) + 1;
  C := Geral.RichCol(MeGerado) + 1;
  StatusBar.Panels[1].Text := Format('L: %3d   C: %3d', [L, C]);
  //
  if (L <> FMeGerado_SelLin) then
  begin
    FMeGerado_SelLin := L;
    MudouLinha    := True;
  end;
  //
  if (C <> FMeGerado_SelCol) then
  begin
    FMeGerado_SelCol := C;
  end;
  if MudouLinha then
  begin
    Linha := MeGerado.Lines[L - 1];
    //
    Registro := Geral.IMV(Copy(Linha, 1, 2));
    case Registro of
      60, 61: SubTipo := Linha[3];
      else SubTipo := #0;
    end;
    MyObjects.LimpaGrade(Grade, 1, 1, True);
    //
    Campo := 1;
    while Campo > 0 do
    begin
      USintegra.CoordenadasCampo(Registro, Campo, SubTipo, F_Tam, F_PosI, F_PosF, F_Fmt,
      F_Casas, F_Obrigatorio);
      if F_PosI > 0 then
      begin
        Grade.RowCount := Campo + 1;
        DescriCampo := USintegra.ObtemDescricaoDeCampo(Registro, F_PosI, SubTipo);
        ConteudoCampo := Copy(Linha, F_PosI, F_Tam);
        if F_Fmt = 'X' then
          ValorCampo := ''
        else
        if F_Fmt = 'N' then
        begin
          if F_Casas = 0 then
            ValorCampo := ''
          else
            ValorCampo := Geral.FFT(
              Geral.DMV(ConteudoCampo) / Power(10, F_Casas), F_Casas, siPositivo);
        end else
        if F_Fmt = 'D' then
        begin
          case F_Casas of
            0: ValorCampo := Copy(ConteudoCampo, 7, 2) + '/' +
                             Copy(ConteudoCampo, 5, 2) + '/' +
                             Copy(ConteudoCampo, 1, 4);
            1: ValorCampo := Copy(ConteudoCampo, 1, 2) + '/' +
                             Copy(ConteudoCampo, 3, 4);
          end;
        end else  Geral.MensagemBox('Formata��o n�o definida: "' + F_Fmt +
        '". Avise a Dermatek', 'Erro', MB_OK+MB_ICONERROR);
        //end;
        //

        Grade.Cells[00,Campo] := FormatFloat('00', Campo);
        Grade.Cells[01,Campo] := ConteudoCampo;
        Grade.Cells[02,Campo] := ValorCampo;
        Grade.Cells[03,Campo] := FormatFloat('000', F_Tam);
        Grade.Cells[04,Campo] := FormatFloat('000', F_PosI);
        Grade.Cells[05,Campo] := FormatFloat('000', F_PosF);
        Grade.Cells[06,Campo] := F_Fmt;
        Grade.Cells[07,Campo] := IntToStr(F_Casas);
        Grade.Cells[08,Campo] := DescriCampo;
        //
        Campo := Campo + 1;
      end else
        Campo := 0;
    end;
  end;
end;

procedure TFmSintegra_Arq.InfoPosMeImportado();
begin
  USintegra.InfoPosMeImportado(MeImportado, GridS, StatusBar,
    FMeImportado_SelLin, FMeImportado_SelCol, F_Tam, F_PosI, F_PosF, F_Fmt,
    F_Casas, F_Obrigatorio);
end;

{
procedure TFmSintegra_Arq.InfoPosMeImportado2(Line: Integer);
var
  Linha: String;
  //
  Registro, Campo: Integer;
  SubTipo: Char;
  DescriCampo, ConteudoCampo, ValorCampo: String;
begin
    Linha := MeImportado.Lines[Line - 1];
    //
    Registro := Geral.IMV(Copy(Linha, 1, 2));
    case Registro of
      60, 61: SubTipo := Linha[3];
      else SubTipo := #0;
    end;
    MyObjects.LimpaGrade(GridS, 1, 1, True);
    //
    Campo := 1;
    while Campo > 0 do
    begin
      USintegra.CoordenadasCampo(Registro, Campo, SubTipo, F_Tam, F_PosI, F_PosF, F_Fmt,
      F_Casas, F_Obrigatorio);
      if F_PosI > 0 then
      begin
        GridS.RowCount := Campo + 1;
        DescriCampo := USintegra.ObtemDescricaoDeCampo(Registro, F_PosI, SubTipo);
        ConteudoCampo := Copy(Linha, F_PosI, F_Tam);
        if F_Fmt = 'X' then
          ValorCampo := ''
        else
        if F_Fmt = 'N' then
        begin
          if F_Casas = 0 then
            ValorCampo := ''
          else
            ValorCampo := Geral.FFT(
              Geral.DMV(ConteudoCampo) / Power(10, F_Casas), F_Casas, False);
        end else
        if F_Fmt = 'D' then
        begin
          case F_Casas of
            0: ValorCampo := Copy(ConteudoCampo, 7, 2) + '/' +
                             Copy(ConteudoCampo, 5, 2) + '/' +
                             Copy(ConteudoCampo, 1, 4);
            1: ValorCampo := Copy(ConteudoCampo, 1, 2) + '/' +
                             Copy(ConteudoCampo, 3, 4);
          end;
        end else  Geral.MensagemBox('Formata��o n�o definida: "' + F_Fmt +
        '". Avise a Dermatek', 'Erro', MB_OK+MB_ICONERROR);
        //end;
        //

        GridS.Cells[00,Campo] := FormatFloat('00', Campo);
        GridS.Cells[01,Campo] := ConteudoCampo;
        GridS.Cells[02,Campo] := ValorCampo;
        GridS.Cells[03,Campo] := FormatFloat('000', F_Tam);
        GridS.Cells[04,Campo] := FormatFloat('000', F_PosI);
        GridS.Cells[05,Campo] := FormatFloat('000', F_PosF);
        GridS.Cells[06,Campo] := F_Fmt;
        GridS.Cells[07,Campo] := IntToStr(F_Casas);
        GridS.Cells[08,Campo] := DescriCampo;
        //
        Campo := Campo + 1;
      end else
        Campo := 0;
    end;
    GridS.Update;
    Application.ProcessMessages;
//  end;
end;
}

function TFmSintegra_Arq.LimpaValor(Valor: String): String;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(Valor) do
    if (Valor[I] <> ' ') and (Valor[I] <> '0') then
      Result := Result + Valor[I];
end;

procedure TFmSintegra_Arq.LocalizaNF(FatID, IDCtrl: Integer);
begin
  case FatID of
    VAR_FATID_0001: // Faturamento
    begin
      if DBCheck.CriaFm(TFmNFeCabA_0000, FmNFeCabA_0000, afmoNegarComAviso) then
      begin
        FmNFeCabA_0000.LocCod(IDCtrl, IDCtrl);
        FmNFeCabA_0000.ShowModal;
        FmNFeCabA_0000.Destroy;
      end;
    end;
    VAR_FATID_0013, VAR_FATID_0113, VAR_FATID_0213: // entrada Mat�ria-prima
      Grade_Jan.CriaFormEntradaCab(tnfMateriaPrima, True, 0);
    VAR_FATID_0051, VAR_FATID_0151, VAR_FATID_0251: // entrada de uso e consumo
      Grade_Jan.CriaFormEntradaCab(tnfUsoEConsumo, True, 0);
    else Geral.MensagemBox('FatID n�o implemetado: ' + IntToStr(FatID),
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmSintegra_Arq.MeGeradoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  InfoPosMeGerado();
end;

procedure TFmSintegra_Arq.MeGeradoMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  InfoPosMeGerado();
end;

procedure TFmSintegra_Arq.MeGeradoMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  InfoPosMeGerado();
end;

procedure TFmSintegra_Arq.MeImportadoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  InfoPosMeImportado();
end;

procedure TFmSintegra_Arq.MeImportadoMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  InfoPosMeImportado();
end;

procedure TFmSintegra_Arq.MeImportadoMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  InfoPosMeImportado();
end;

procedure TFmSintegra_Arq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSintegra_Arq.BtTodosClick(Sender: TObject);
begin
  CGTipos.Value := 0;
end;

procedure TFmSintegra_Arq.Button1Click(Sender: TObject);
begin
// fazer
end;

function TFmSintegra_Arq.DadosNFAtual(): String;
begin
  Result := #13#10 +
  'Emitente: ' + QrCabAemit_CNPJ.Value + #13#10 +
  'Destinat�rio: ' + QrCabAdest_CNPJ.Value + #13#10 +
  'Nota Fiscal: ' + FormatFloat('000000', QrCabAide_nNF.Value) + #13#10 +
  'Data emiss�o: ' + Geral.FDT(QrCabAide_dEmi.Value, 2) + #13#10 +
  'Status da NF: ' + FormatFloat('000', QrCabAStatus.Value);
end;

procedure TFmSintegra_Arq.DBGrid2DblClick(Sender: TObject);
begin
  LocalizaNF(QrCabAFatID.Value, QrCabAIDCtrl.Value);
end;

procedure TFmSintegra_Arq.DBGrid5DblClick(Sender: TObject);
  procedure SetaGrades(QrMy, QrCo: TmySQLQuery; DsMy, DsCo: TDataSource);
  var
    I: Integer;
  begin
    QrMy.First;
    QrCo.First;
    for I := 2 to QrDifereItemReg.Value do
    begin
      QrMy.Next;
      QrCo.Next;
    end;
    GradeMy.DataSource := DsMy;
    GradeCo.DataSource := DsCo;
  end;
begin
  case QrDifereTipoReg.Value of
    50:  SetaGrades(QrSintegra50, QrR50, DsSintegra50, DsR50);
    51:  SetaGrades(QrSintegra51, QrR51, DsSintegra51, DsR51);
    53:  SetaGrades(QrSintegra53, QrR53, DsSintegra53, DsR53);
    54:  SetaGrades(QrSintegra54, QrR54, DsSintegra54, DsR54);
    70:  SetaGrades(QrSintegra70, QrR70, DsSintegra70, DsR70);
    71:  SetaGrades(QrSintegra71, QrR71, DsSintegra71, DsR71);
    74:  SetaGrades(QrSintegra74, QrR74, DsSintegra74, DsR74);
    75:  SetaGrades(QrSintegra75, QrR75, DsSintegra75, DsR75);
    85:  SetaGrades(QrSintegra85, QrR85, DsSintegra85, DsR85);
    86:  SetaGrades(QrSintegra86, QrR86, DsSintegra86, DsR86);
    88:  SetaGrades(QrSintegra88, QrR88, DsSintegra88, DsR88);
    90:  SetaGrades(QrSintegra90, QrR90, DsSintegra90, DsR90);
  end;
end;

function TFmSintegra_Arq.DefineCFOPEntrada(EmitenteNF: String;
  const CFOP_Inn: Integer; var CFOP_Out: Integer): Boolean;
var
  TXT_A, TXT_B, Msg: String;
  procedure ConfiguraCFOP;
  begin
    if DBCheck.CriaFm(TFmNatOper, FmNatOper, afmoNegarComAviso) then
    begin
      FmNatOper.BtIncluiClick(Self);
      FmNatOper.ShowModal;
      FmNatOper.Destroy;
    end;
  end;
begin
  CFOP_Out := CFOP_Inn;
  Result   := True;
  if EmitenteNF = 'T' then // terceiros
  begin
    TXT_A := FormatFloat('0,000', CFOP_Inn);
    TXT_B := FormatFloat('0000', CFOP_Inn);
    QrNatOper.Close;
    QrNatOper.Params[00].AsString := TXT_A;
    QrNatOper.Params[01].AsString := TXT_B;
    QrNatOper.Open;
    if QrNatOper.RecordCount = 0 then
    begin
      Result := False;
      Msg := 'CFOP de entrada n�o cadastrado para o CFOP ' + TXT_A;
      {
      Geral.MensagemBox('CFOP de entrada n�o cadastrado para o CFOP ' +
      TXT_A, 'Aviso', MB_OK+MB_ICONWARNING);
      }
      VAR_CADTEXTO := TXT_A;
      MLAGeral.MessageDlgCheck(Msg, mtConfirmation, [mbOK], 0, mrOK,
      True, True, 'Desejo configurar agora o CFOP de entrada n� ' +
      TXT_A, @ConfiguraCFOP);
      //
      QrNatOper.Close;
      QrNatOper.Params[00].AsString := TXT_A;
      QrNatOper.Params[01].AsString := TXT_B;
      QrNatOper.Open;
      //
      if QrNatOper.RecordCount > 0 then
         Result := DefineCFOPEntrada(EmitenteNF, CFOP_Inn, CFOP_Out);
    end else
    if Trim(QrNatOperCodEnt.Value) = '' then
    begin
      Result := False;
      Geral.MensagemBox('CFOP de entrada em branco para o CFOP ' +
      TXT_A, 'Aviso', MB_OK+MB_ICONWARNING);
    end else
    begin
      CFOP_Out := Geral.IMV(Geral.SoNumero_TT(QrNatOperCodEnt.Value));
    end;
  end else begin
    if CFOP_Out > 9999 then
    begin
      TXT_A := FormatFloat('0000', CFOP_Inn);
      CFOP_Out := Geral.IMV(Copy(FormatFloat('0000', CFOP_Out), 1, 4));
      Memo2.Lines.Add('CFOP Alterado de ' + TXT_A + ' para ' +
      FormatFloat('0000', CFOP_Out));
      //, 'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
end;

function TFmSintegra_Arq.DefineDadosEntidadeNF(var CNPJ_CPF, IE, Data, UF,
EmitenteNF, Situacao: String): Boolean;
begin
  Result := False;
{
11.1.5 - CAMPO 02 - CNPJ
Nova reda��o dada ao subitem 11.1.5.1 pelo Conv. ICMS 76/03, efeitos a partir de 16.10.03
11.1.5.1 - Em se tratando de pessoas n�o obrigadas � inscri��o no CNPJ/MF, preencher com o CPF;
Reda��o original, efeitos at� 15.10.03
11.1.5.1 - Em se tratando de pessoas n�o obrigadas � inscri��o no CGC/MF, preencher com o CPF.
11.1.5.2 - Tratando-se de opera��es com o exterior ou com pessoa f�sica n�o inscrita no CPF zerar o campo;
}
(*  if ((QrCabAdest_CNPJ.Value <> '') and (QrCabAdest_CNPJ.Value = QrEntiCNPJ_CPF.Value))
  or ((QrCabAdest_CPF.Value  <> '') and (QrCabAdest_CPF.Value  = QrEntiCNPJ_CPF.Value)) then // Entradas
  begin
    // CNPJ do fornecedor
    if QrCabAemit_CNPJ.Value <> '' then
      CNPJ_CPF := Geral.SoNumero_TT(QrCabAemit_CNPJ.Value)
    else
      CNPJ_CPF := Geral.SoNumero_TT(QrCabAemit_CPF.Value);
    //
*)
  CNPJ_CPF := Geral.SoNumero_TT(QrCabACNPJ_CPF.Value);
{
11.1.6 - CAMPO 03
11.1.6.1 - Tratando-se de opera��es com o exterior ou com pessoas n�o obrigadas � inscri��o estadual, o campo assumir� o conte�do "ISENTO";
11.1.6.2 - Na hip�tese de registro referente a fornecimento feito por produtor agropecu�rio, em que seja obrigat�ria a emiss�o de Nota Fiscal de Entrada, a unidade da Federa��o poder� dispor sobre qual informa��o pretende neste campo;
}
  IE := Geral.SoNumero_TT(QrCabAIE.Value);
  if IE = '' then IE := 'ISENTO';
  UF := QrCabAUF.Value;
  if UF = '' then
  begin
    QrEnt.Close;
    QrEnt.Params[0].AsInteger := QrCabACodInfoEmit.Value;
    QrEnt.Open;
    //
    UF := QrEntUFx.Value
  end;
(*
    // Inscri��o Estadual do remetente nas entradas
    IE := QrCabAemit_IE.Value;
    if IE = '' then IE := 'ISENTO';
    //
    //
    //11.1.7 - CAMPO 05 - Tratando-se de opera��es com o exterior, colocar "EX";
    // Sigla da unidade da Federa��o do remetente nas entradas
    UF := QrCabAemit_UF.Value;
  end else begin  // sa�das
    // CNPJ
    if QrCabAdest_CNPJ.Value <> '' then
      CNPJ_CPF := Geral.SoNumero_TT(QrCabAdest_CNPJ.Value)
    else
      CNPJ_CPF := Geral.SoNumero_TT(QrCabAdest_CPF.Value);
    //
    // Inscri��o Estadual do destinat�rio nas sa�das
    IE := QrCabAdest_IE.Value;
    if IE = '' then IE := 'ISENTO';
    //
    //11.1.7 - CAMPO 05 - Tratando-se de opera��es com o exterior, colocar "EX";
    // Sigla da unidade da Federa��o do destinat�rio nas sa�das
    UF := QrCabAdest_UF.Value;
  end;
*)
  //
  // Data de recebimento na entrada ou de emiss�o na sa�da
  Data := FormatDateTime('YYYYMMDD', QrCabADataFiscal.Value);
  //
  // Alterado 2011-02-27
  //if QrCabAEmpresa.Value < 0 then
  if QrCabACodInfoEmit.Value = DModG.QrEmpresasCodigo.Value then
    EmitenteNF := 'P'
  else
  if QrCabACodInfoDest.Value = DModG.QrEmpresasCodigo.Value then
    EmitenteNF := 'T'
  else
  begin
    Geral.MensagemBox('Tipo de emitente indefinido! AVISE A DERMATEK',
    'Aviso', MB_OK+MB_ICONWARNING);
    EmitenteNF := '?';
  end;
  //

  if QrCabAEhOEmitente.Value = 1 then
  begin
    case QrCabAStatus.Value of
      100: Situacao := 'N'; // Normal
      101: Situacao := 'S'; // Cancelada
      102: Situacao := '4'; // NFe Inutilizada
      //?: Situacao := 'E'; // Extempor�neo Normal     Parei Aqui!
      //?: Situacao := 'X'; // Extempor�neo Cancelado  Parei Aqui!
      301..
      302: Situacao := '2'; // NFe Denegada
      else begin
        Situacao := '?';
        Geral.MensagemBox('Situa��o de NF desconhecida!' + #13#10 +
        DadosNFAtual(), 'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
    end;   // Parei aqui! Fazer outras situa��es como?
  end else Situacao := 'N';
  Result := True;
end;

{
function TFmSintegra_Arq.DefineReduzido(const CODPROD: String;
  const AvisaErros: Boolean; var Reduzido: Integer; var Nome: String): Boolean;
var
  Doc: String;
  Codigo: Integer;
begin
  Result := False;
  Reduzido := 0;
  Codigo := Geral.IMV(Trim(CODPROD));
  case RGComoImporta.ItemIndex of
    0:
    begin
      QrLocGGX.Close;
      QrLocGGX.Params[0].AsInteger := Codigo;
      QrLocGGX.Open;
      if QrLocGGX.RecordCount > 0 then
      begin
        Reduzido := QrLocGGXControle.Value;
        Nome     := QrLocGGXNO_PRD_TAM_COR.Value;
      end else begin
        if AvisaErros then
          Geral.MensagemBox('Reduzido n�o localizado para o produto:' +
          #13#10 + CODPROD, 'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
    1:
    begin
      QrLocGGX.Close;
      QrLocGGX.Params[0].AsInteger := Codigo;
      QrLocGGX.Open;
      //
(*
    if QrProdPrdGrupTip.Value < 0 then
      CodProd := FormatFloat('0', QrProdGraGru1.Value)
    else
      CodProd := FormatFloat('0', QrProdGraGruX.Value);
*)
      QrLocGG1.Close;
      QrLocGG1.Params[0].AsInteger := Codigo;
      QrLocGG1.Open;
      //
      if Codigo = 4937 then
//    4295          84389090EIXO DE DIRECAO PL
//    4937          85362000DISJUNTOR WEG MPW25 20-25A 10045310
        ShowMessage(IntToStr(Codigo));

      if (QrLocGGXPrdGrupTip.Value > 0) and (QrLocGG1PrdGrupTip.Value < 0) then
      begin
      (*
      if QrLocGG1.RecordCount > 0 then
      begin
        Reduzido := QrLocGG1Controle.Value;
        Nome     := QrLocGG1NO_PRD_TAM_COR.Value;
      end else begin
        if AvisaErros then
          Geral.MensagemBox('Reduzido n�o localizado para o produto:' +
          #13#10 + CODPROD, 'Aviso', MB_OK+MB_ICONWARNING);
      end;
      *)
        ShowMessage(IntToStr(Codigo));
        MeNaoImport.Lines.Add('GG1 > ' + FormatFloat('0', QrLocGG1GraGru1.Value) + ' ' + QrLocGG1NO_PRD_TAM_COR.Value);
        MeNaoImport.Lines.Add('GGX > ' + FormatFloat('0', QrLocGGXControle.Value) + ' ' + QrLocGGXNO_PRD_TAM_COR.Value);
      end else begin
        if QrLocGGXPrdGrupTip.Value > 0 then
          MeNaoImport.Lines.Add('GGX > ' + FormatFloat('0', QrLocGGXControle.Value) + ' ' + QrLocGGXNO_PRD_TAM_COR.Value)
        else
        if QrLocGG1PrdGrupTip.Value < 0 then
        begin
          Reduzido := QrLocGG1GraGru1.Value;
          MeNaoImport.Lines.Add('GG1 > ' + FormatFloat('0', QrLocGG1GraGru1.Value) + ' ' + QrLocGG1NO_PRD_TAM_COR.Value)
        end else
          MeNaoImport.Lines.Add('NONONONONONONONONONMONO OK');
      end;
    end;
    else Geral.MensagemBox(
    'Considera��o de c�digo no carregamento n�o implementado!', 'Aviso',
    MB_OK+MB_ICONWARNING);
  end;
  Result := Reduzido <> 0;
end;
}

function TFmSintegra_Arq.DefSerie(Serie: Integer): String;
begin
  Result := FormatFloat('0', Serie);
  if Result = '0' then
    Result := ' ';
end;

procedure TFmSintegra_Arq.EdEmp_CNPJChange(Sender: TObject);
var
  Empresa: Integer;
  NomeEmp: String;
begin
  if DModG.DefineEntidade(EdEmp_CNPJ.Text, True, Empresa, NomeEmp) then
  begin
    EdEmp_Codigo.ValueVariant := Empresa;
    EdEmp_Nome.Text := NomeEmp;
  end;
end;

procedure TFmSintegra_Arq.Excluiitematualdaminhatabela1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  I: Integer;
begin
  if Geral.MensagemBox(
  'Confirma a exclus�o do item selecionada da "Minha tabela"?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Qry := TmySQLQuery(GradeMy.DataSource.DataSet);
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('DELETE FROM ' + Copy(Qry.Name, 3));
    DModG.QrUpdPID1.SQL.Add('WHERE ' + Qry.Fields[0].FieldName +' = ' +
      Geral.VariavelToString(Qry.Fields[0].AsVariant));
    for I := 1 to Qry.Fields.Count - 1 do
    DModG.QrUpdPID1.SQL.Add('AND ' + Qry.Fields[I].FieldName +' = ' +
      Geral.VariavelToString(Qry.Fields[I].AsVariant));
    DModG.QrUpdPID1.ExecSQL;
    Qry.Close;
    Qry.Open;
  end;
end;

procedure TFmSintegra_Arq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  OrganizaGrades();
end;

procedure TFmSintegra_Arq.FormCreate(Sender: TObject);
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FMeGerado_SelCol := 0;
  FMeGerado_SelLin := 0;
  // Definir antes da chamada da cria��o do form
  //DModG.ReopenEmpresas(?)
  //
  TPDataIni.Date := Geral.PrimeiroDiaDoMes(IncMonth(Date, -1));
  TPDataFim.Date := Geral.PrimeiroDiaDoMes(Date) -1;
  //
  MyObjects.LimpaGrade(Grade, 1, 1, True);
  Grade.Cells[00,00] := 'N�';
  Grade.Cells[01,00] := 'Conte�do do campo';
  Grade.Cells[02,00] := 'Formatado';
  Grade.Cells[03,00] := 'Tam';
  Grade.Cells[04,00] := 'Ini';
  Grade.Cells[05,00] := 'Fim';
  Grade.Cells[06,00] := 'F';
  Grade.Cells[07,00] := 'D';
  Grade.Cells[08,00] := 'Descri��o do Campo';
  //
  Grade.ColWidths[00] := 024;
  Grade.ColWidths[01] := 140;
  Grade.ColWidths[02] := 080;
  Grade.ColWidths[03] := 028;
  Grade.ColWidths[04] := 028;
  Grade.ColWidths[05] := 028;
  Grade.ColWidths[06] := 018;
  Grade.ColWidths[07] := 018;
  Grade.ColWidths[08] := 999;
  //
  MyObjects.LimpaGrade(GridS, 1, 1, True);
  GridS.Cells[00,00] := 'N�';
  GridS.Cells[01,00] := 'Conte�do do campo';
  GridS.Cells[02,00] := 'Formatado';
  GridS.Cells[03,00] := 'Tam';
  GridS.Cells[04,00] := 'Ini';
  GridS.Cells[05,00] := 'Fim';
  GridS.Cells[06,00] := 'F';
  GridS.Cells[07,00] := 'D';
  GridS.Cells[08,00] := 'Descri��o do Campo';
  //
  GridS.ColWidths[00] := 024;
  GridS.ColWidths[01] := 140;
  GridS.ColWidths[02] := 080;
  GridS.ColWidths[03] := 028;
  GridS.ColWidths[04] := 028;
  GridS.ColWidths[05] := 028;
  GridS.ColWidths[06] := 018;
  GridS.ColWidths[07] := 018;
  GridS.ColWidths[08] := 999;
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl4.ActivePageIndex := 0;
  PageControl5.ActivePageIndex := 0;
  //
  QrDifere.Database      := DModG.MyPID_DB;
  QrSintegra10.Database := DModG.MyPID_DB;
  QrSintegra50.Database := DModG.MyPID_DB;
  QrSintegra51.Database := DModG.MyPID_DB;
  QrSintegra53.Database := DModG.MyPID_DB;
  QrSintegra54.Database := DModG.MyPID_DB;
  QrSintegra70.Database := DModG.MyPID_DB;
  QrSintegra71.Database := DModG.MyPID_DB;
  QrSintegra74.Database := DModG.MyPID_DB;
  QrSintegra75.Database := DModG.MyPID_DB;
  QrSintegra85.Database := DModG.MyPID_DB;
  QrSintegra86.Database := DModG.MyPID_DB;
  QrSintegra88.Database := DModG.MyPID_DB;
  QrSintegra90.Database := DModG.MyPID_DB;
  //
  QrLoad_S50.Database    := DModG.MyPID_DB;
  //
  DBGSMIC2.DataSource    := DmProd.DsSMIC2;
  DBGExport2.DataSource  := DmProd.DsExport2;
  DBGListErr1.DataSource := DmProd.DsListErr1;
end;

procedure TFmSintegra_Arq.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
  OrganizaGrades();
end;

procedure TFmSintegra_Arq.GradeCamposAfterReopenAfterSQLExec(Sender: TObject);
begin
  ReopenDifere(True);
end;

procedure TFmSintegra_Arq.GradeDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeGeral(Grade, ACol, ARow, Rect, State,
  {
  Grade.Cells[00,00] := 'N�';
  Grade.Cells[01,00] := 'Conte�do do campo';
  Grade.Cells[02,00] := 'Formatado';
  Grade.Cells[03,00] := 'Tam';
  Grade.Cells[04,00] := 'Ini';
  Grade.Cells[05,00] := 'Fim';
  Grade.Cells[06,00] := 'F';
  Grade.Cells[07,00] := 'D';
  Grade.Cells[08,00] := 'Descri��o do Campo';
  }
  [taCenter, taLeftJustify, taRightJustify, taCenter, taCenter, taCenter,
  taCenter, taCenter, taLeftJustify], False);
end;

procedure TFmSintegra_Arq.GradeMyDblClick(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
  QrY := TmySQLQuery(GradeMy.DataSource.DataSet);
  //
  QrLocNFa.Close;
  QrLocNFa.Params[00].AsString := Qry.FieldByName('NUMNF').AsString;
  QrLocNFa.Params[01].AsString := Qry.FieldByName('CNPJ').AsString;
  QrLocNFa.Open;
  //
  case QrLocNFa.RecordCount of
    0: Geral.MensagemBox('A NF n�o foi localizada!', 'Aviso', MB_OK+MB_ICONWARNING);
    1: LocalizaNF(QrLocNFaFatID.Value, QrLocNFaIDCtrl.Value);
    else Geral.MensagemBox(
    'Foi localizado mais de uma NF!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmSintegra_Arq.OrganizaGrades();
begin
  if GradeMy.Height <> GradeCo.Height then
    GradeMy.Height := (GradeMy.Height + GradeCo.Height) div 2;
{
  begin
    while GradeMy.Height > GradeCo.Height do
      GradeMy.Height := GradeMy.Height - 1;
  end else
  begin
    while GradeMy.Height < GradeCo.Height do
      GradeMy.Height := GradeMy.Height + 1;
  end;
}
end;

procedure TFmSintegra_Arq.QrDifereAfterOpen(DataSet: TDataSet);
begin
  Label10.Caption := 'Diferen�as encontradas: ' + IntToStr(QrDifere.RecordCount);
end;

procedure TFmSintegra_Arq.QrEntiCalcFields(DataSet: TDataSet);
begin
  QrEntiLOGRADOURO.Value := '';
  if QrEntiLograd.Value <> '' then
    QrEntiLOGRADOURO.Value := QrEntiLograd.Value + ' ';
  if QrEntiRua.Value <> '' then
    QrEntiLOGRADOURO.Value := QrEntiLOGRADOURO.Value + QrEntiRua.Value + ', ';
  {
  if QrEntiNumero.Value <> 0 then
    QrEntiLOGRADOURO.Value := QrEntiLOGRADOURO.Value +
    FormatFloat('0', QrEntiNumero.Value)
  else
    QrEntiLOGRADOURO.Value := QrEntiLOGRADOURO.Value + 'S/N';
  }
end;

procedure TFmSintegra_Arq.ReopenDifere(Filtra: Boolean);
var
  Itens: Integer;
begin
  QrDifere.Close;
  QrDifere.SQL.Clear;
  QrDifere.SQL.Add('SELECT *');
  QrDifere.SQL.Add('FROM sintegraer');
  if Filtra then
  begin
    Itens := 0;
    TbCampos.First;
    while not TbCampos.Eof do
    begin
      if TbCamposAtivo.Value = 0 then
      begin
        if Itens = 0 then
          QrDifere.SQL.Add('WHERE Campo <> "' + TbCamposCampo.Value + '"')
        else
          QrDifere.SQL.Add('AND Campo <> "' + TbCamposCampo.Value + '"');
        //
        Itens := Itens + 1;
      end;
      //
      TbCampos.Next;
    end;
  end;
  QrDifere.Open;
end;

procedure TFmSintegra_Arq.SetaTodosCampos(Ativo: Integer);
begin
  TbCampos.First;
  while not TbCampos.Eof do
  begin
    TbCampos.Edit;
    TbCamposAtivo.Value := Ativo;
    TbCampos.Post;
    TbCampos.Next;
  end;
  //
  ReopenDifere(True);
end;

procedure TFmSintegra_Arq.SpeedButton1Click(Sender: TObject);
begin
  MeImportado.Text := MLAGeral.LoadFileToText(EdSINTEGRA_Path.Text);
end;

procedure TFmSintegra_Arq.SpeedButton7Click(Sender: TObject);
begin
  if MyObjects.DefineArquivo1(Self, EdSINTEGRA_Path) then
  begin
    MeImportado.Text := MLAGeral.LoadFileToText(EdSINTEGRA_Path.Text);
  end;
end;

function TFmSintegra_Arq.ValorPositivo(const ValInfo: Double; var ValOut: Double): Boolean;
begin
  if ValInfo <= -0.01 then
  begin
    Geral.MensagemBox('Valor negativo! Erro de c�lculo!', 'Aviso',
    MB_OK+MB_ICONERROR);
    ValOut := 0;
    Result := False;
  end else begin
    ValOut := ValInfo;
    Result := True;
  end;
end;

{
Valor � obrigat�rio:
Function: "TFmSintegra_Arq.AddItemX()"
Linha: 237
Registro: 54
Posi��o inicial: 35
Posi��o Final: 37
Tamanho campo: 3
Texto campo: 0

Campo: N�mero de ordem do item na nota fiscal}
end.
