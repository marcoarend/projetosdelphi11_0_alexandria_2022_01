unit UnSintegra;
interface

uses
  Windows, Forms, Controls, StdCtrls, ComCtrls, ExtCtrls, Messages, SysUtils,
  Classes, Graphics, Dialogs, Menus, UnMsgInt, UnInternalConsts, UnMLAGeral,
  UnInternalConsts2, dmkGeral, Grids, math, UnDmkEnums;

type
  TUnSintegra = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  CoordenadasCampo(const Registro, Campo: Integer; const SubTipo:
              Char; var Tam, Ini, Fim: Integer; var Fmt: Char; var Deci:
              Byte; var Obrigatorio: Boolean): Boolean;
    procedure InfoPosMeImportado(MeImportado: TMemo; GridS: TStringGrid;
              StatusBar: TStatusBar; MeImportado_SelLin, MeImportado_SelCol,
              Tam, PosI, PosF: Integer; var Fmt: Char; var Casas:
              Byte; var Obrigatorio: Boolean);
    procedure InfoPosMeImportado2(MeImportado: TMemo;
              GridS: TStringGrid; Line: Integer;
              var Tam, PosI, PosF: Integer; var Fmt: Char; var Casas:
              Byte; var Obrigatorio: Boolean);
    function  ObtemDescricaoDeCampo(nReg, PosI: Integer; SubTipo: Char): String;
    function  ObtemStatusDaSituacao(Situacao: String): Integer;
    procedure CriaNFe(ImporExpor, AnoMes, Empresa, LinArq: Integer);
    procedure ExcluiPeriodoSintegra(ImporExpor, AnoMes, Empresa: Integer);
  end;

var
  USintegra: TUnSintegra;

implementation

uses UnMyObjects, UnGrade_Tabs, UMySQLModule, ModProd, ModuleGeral, Module;


{ TUnSintegra }

function TUnSintegra.CoordenadasCampo(const Registro, Campo: Integer;
  const SubTipo: Char; var Tam, Ini, Fim: Integer; var Fmt: Char;
  var Deci: Byte; var Obrigatorio: Boolean): Boolean;
var
  T, I, F, D, O: Integer;
  X: Char;
begin
  T(*Tam*) := 0;
  I(*Ini*) := 0;
  F(*Fim*) := 0;
  X(*Fmt*) := #0;
  O(*Obrigatorio*) := 1;
  D        := 0;
  case Registro of
    10:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo de registro';
        02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 1; end; // CGC/MF do contribuinte';
        03:	begin T := 014;	I := 017;	F := 030; X := 'X'; D := 0; O := 1; end; // Inscri��o Estadual do contribuinte';
        04:	begin T := 035;	I := 031;	F := 065; X := 'X'; D := 0; O := 1; end; // Nome do contribuinte';
        05:	begin T := 030;	I := 066;	F := 095; X := 'X'; D := 0; O := 1; end; // Munic�pio do contribuinte';
        06:	begin T := 002;	I := 096;	F := 097; X := 'X'; D := 0; O := 1; end; // UF do contribuinte';
        07:	begin T := 010;	I := 098;	F := 107; X := 'N'; D := 0; O := 0; end; // Fax do contribuinte';
        08:	begin T := 008;	I := 108;	F := 115; X := 'D'; D := 0; O := 1; end; // Data inicial do per�odo';
        09:	begin T := 008;	I := 116;	F := 123; X := 'D'; D := 0; O := 1; end; // Data final do per�odo';
        10:	begin T := 001;	I := 124;	F := 124; X := 'X'; D := 0; O := 1; end; // C�digo da identifica��o do conv�nio';
        11:	begin T := 001;	I := 125;	F := 125; X := 'X'; D := 0; O := 1; end; // C�digo da identifica��o da natureza das opera��es informadas';
        12:	begin T := 001;	I := 126;	F := 126; X := 'X'; D := 0; O := 1; end; // C�digo da finalidade do arquivo magn�tico';
      end;
    end;
    11:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo de registro
        02:	begin T := 034;	I := 003;	F := 036; X := 'X'; D := 0; O := 1; end; // Logradouro
        03:	begin T := 005;	I := 037;	F := 041; X := 'N'; D := 0; O := 0; end; // N�mero
        04:	begin T := 022;	I := 042;	F := 063; X := 'X'; D := 0; O := 0; end; // Complemento
        05:	begin T := 015;	I := 064;	F := 078; X := 'X'; D := 0; O := 1; end; // Bairro
        06:	begin T := 008;	I := 079;	F := 086; X := 'N'; D := 0; O := 1; end; // CEP
        07:	begin T := 028;	I := 087;	F := 114; X := 'X'; D := 0; O := 1; end; // Nome do Contato
        08:	begin T := 012;	I := 115;	F := 126; X := 'N'; D := 0; O := 1; end; // Telefone
      end;
    end;
    50:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo de registro';
        02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 2; end; // CNPJ do remetente nas entradas e do destinat�rio nas sa�das';
        03:	begin T := 014;	I := 017;	F := 030; X := 'X'; D := 0; O := 1; end; // Inscri��o Estadual do remetente nas entradas e do destinat�rio nas sa�das';
        04:	begin T := 008;	I := 031;	F := 038; X := 'D'; D := 0; O := 1; end; // Data de emiss�o na sa�da ou de recebimento na entrada';
        05:	begin T := 002;	I := 039;	F := 040; X := 'X'; D := 0; O := 1; end; // Sigla da unidade da Federa��o do remetente nas entradas e do destinat�rio nas sa�das ';
        06:	begin T := 002;	I := 041;	F := 042; X := 'N'; D := 0; O := 1; end; // C�digo do modelo da nota fiscal';
        07:	begin T := 003;	I := 043;	F := 045; X := 'X'; D := 0; O := 0; end; // S�rie da nota fiscal';
        08:	begin T := 006;	I := 046;	F := 051; X := 'N'; D := 0; O := 1; end; // N�mero da nota fiscal';
        09:	begin T := 004;	I := 052;	F := 055; X := 'N'; D := 0; O := 1; end; // C�digo Fiscal de Opera��o e Presta��o';
        10:	begin T := 001;	I := 056;	F := 056; X := 'X'; D := 0; O := 1; end; // Emitente da Nota Fiscal (P-pr�prio/T-terceiros)';
        11:	begin T := 013;	I := 057;	F := 069; X := 'N'; D := 2; O := 2; end; // Valor total da nota fiscal (com 2 decimais)';
        12:	begin T := 013;	I := 070;	F := 082; X := 'N'; D := 2; O := 2; end; // Base de C�lculo do ICMS (com 2 decimais)';
        13:	begin T := 013;	I := 083;	F := 095; X := 'N'; D := 2; O := 2; end; // Montante do imposto (com 2 decimais)';
        14:	begin T := 013;	I := 096;	F := 108; X := 'N'; D := 2; O := 0; end; // Valor amparado por isen��o ou n�o incid�ncia (com 2 decimais)';
        15:	begin T := 013;	I := 109;	F := 121; X := 'N'; D := 2; O := 0; end; // Valor que n�o confira d�bito ou cr�dito do ICMS (com 2 decimais)';
        16:	begin T := 004;	I := 122;	F := 125; X := 'N'; D := 2; O := 0; end; // Al�quota do ICMS (com 2 decimais)';
        17:	begin T := 001;	I := 126;	F := 126; X := 'X'; D := 0; O := 1; end; // Situa��o da Nota Fiscal';
      end;
    end;
    51:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 1; end; // CNPJ do remetente nas entradas e do destinat�rio nas sa�das
        03:	begin T := 014;	I := 017;	F := 030; X := 'X'; D := 0; O := 1; end; // Inscri��o Estadual do remetente nas entradas e do destinat�rio nas sa�das
        04:	begin T := 008;	I := 031;	F := 038; X := 'D'; D := 0; O := 1; end; // Data de emiss�o na sa�da ou recebimento na entrada
        05:	begin T := 002;	I := 039;	F := 040; X := 'X'; D := 0; O := 1; end; // Sigla da unidade da Federa��o do remetente nas entradas e do destinat�rio nas sa�das
        06:	begin T := 003;	I := 041;	F := 043; X := 'X'; D := 0; O := 0; end; // S�rie da nota fiscal
        07:	begin T := 006;	I := 044;	F := 049; X := 'N'; D := 0; O := 1; end; // N�mero da nota fiscal
        08:	begin T := 004;	I := 050;	F := 053; X := 'N'; D := 0; O := 1; end; // C�digo Fiscal de Opera��o e Presta��o
        09:	begin T := 013;	I := 054;	F := 066; X := 'N'; D := 2; O := 2; end; // Valor total da nota fiscal (com 2 decimais)
        10:	begin T := 013;	I := 067;	F := 079; X := 'N'; D := 2; O := 0; end; // Montante do IPI (com 2 decimais)
        11:	begin T := 013;	I := 080;	F := 092; X := 'N'; D := 2; O := 0; end; // Valor amparado por isen��o ou n�o incid�ncia do IPI (com 2 decimais)
        12:	begin T := 013;	I := 093;	F := 105; X := 'N'; D := 2; O := 0; end; // Valor que n�o confira d�bito ou cr�dito do IPI (com 2 decimais)
        13:	begin T := 020;	I := 106;	F := 125; X := 'X'; D := 0; O := 0; end; // Brancos
        14:	begin T := 001;	I := 126;	F := 126; X := 'X'; D := 0; O := 1; end; // Situa��o da Nota Fiscal
      end;
    end;
    53:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 1; end; // CNPJ do contribuinte Substitu�do
        03:	begin T := 014;	I := 017;	F := 030; X := 'X'; D := 0; O := 1; end; // Inscri��o Estadual do Contribuinte substitu�do
        04:	begin T := 008;	I := 031;	F := 038; X := 'D'; D := 0; O := 1; end; // Data de emiss�o na sa�da ou recebimento na entrada
        05:	begin T := 002;	I := 039;	F := 040; X := 'X'; D := 0; O := 1; end; // Sigla da unidade da Federa��o do contribuinte substitu�do
        06:	begin T := 002;	I := 041;	F := 042; X := 'N'; D := 0; O := 1; end; // C�digo do modelo da nota fiscal
        07:	begin T := 003;	I := 043;	F := 045; X := 'X'; D := 0; O := 0; end; // S�rie da nota fiscal
        08:	begin T := 006;	I := 046;	F := 051; X := 'N'; D := 0; O := 1; end; // N�mero da nota fiscal
        09:	begin T := 004;	I := 052;	F := 055; X := 'N'; D := 0; O := 1; end; // C�digo Fiscal de Opera��o e Presta��o
        10:	begin T := 001;	I := 056;	F := 056; X := 'X'; D := 0; O := 1; end; // Emitente da Nota Fiscal (P-pr�prio/T-terceiros)
        11:	begin T := 013;	I := 057;	F := 069; X := 'N'; D := 2; O := 0; end; // Base de c�lculo de reten��o do ICMS (com 2 decimais)
        12:	begin T := 013;	I := 070;	F := 082; X := 'N'; D := 2; O := 0; end; // ICMS retido pelo substituto (com 2 decimais)
        13:	begin T := 013;	I := 083;	F := 095; X := 'N'; D := 2; O := 0; end; // Soma das despesas acess�rias (frete, seguro e outras - com 2 decimais)
        14:	begin T := 001;	I := 096;	F := 096; X := 'X'; D := 0; O := 1; end; // Situa��o da Nota Fiscal
        15:	begin T := 001;	I := 097;	F := 097; X := 'X'; D := 0; O := 1; end; // C�digo que identifica o tipo da Antecipa��o Tribut�ria
        16:	begin T := 029;	I := 098;	F := 126; X := 'X'; D := 0; O := 0; end; // Brancos
      end;
    end;
    54:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 2; end; // CNPJ do remetente nas entradas e do destinat�rio nas sa�das
        03:	begin T := 002;	I := 017;	F := 018; X := 'N'; D := 0; O := 1; end; // C�digo do modelo da nota fiscal
        04:	begin T := 003;	I := 019;	F := 021; X := 'X'; D := 0; O := 0; end; // S�rie da nota fiscal
        05:	begin T := 006;	I := 022;	F := 027; X := 'N'; D := 0; O := 1; end; // N�mero da nota fiscal
        06:	begin T := 004;	I := 028;	F := 031; X := 'N'; D := 0; O := 1; end; // C�digo Fiscal de Opera��o e Presta��o
        07:	begin T := 003;	I := 032;	F := 034; X := 'X'; D := 0; O := 0; end; // C�digo da Situa��o Tribut�ria
        08:	begin T := 003;	I := 035;	F := 037; X := 'N'; D := 0; O := 2; end;{1} //N�mero de ordem do item na nota fiscal
        09:	begin T := 014;	I := 038;	F := 051; X := 'X'; D := 0; O := 0; end; // C�digo do produto ou servi�o do informante
        10:	begin T := 011;	I := 052;	F := 062; X := 'N'; D := 3; O := 0; end; // Quantidade do produto (com 3 decimais)
        11:	begin T := 012;	I := 063;	F := 074; X := 'N'; D := 2; O := 2; end; // Valor bruto do produto (valor unit�rio multiplicado por quantidade) - com 2 decimais
        12:	begin T := 012;	I := 075;	F := 086; X := 'N'; D := 2; O := 0; end; // Valor do Desconto Concedido no item (com 2 decimais).
        13:	begin T := 012;	I := 087;	F := 098; X := 'N'; D := 2; O := 0; end; // Base de c�lculo do ICMS (com 2 decimais)
        14:	begin T := 012;	I := 099;	F := 110; X := 'N'; D := 2; O := 0; end; // Base de c�lculo do ICMS de reten��o na Substitui��o Tribut�ria (com 2 decimais)
        15:	begin T := 012;	I := 111;	F := 122; X := 'N'; D := 2; O := 0; end; // Valor do IPI (com 2 decimais)
        16:	begin T := 004;	I := 123;	F := 126; X := 'N'; D := 2; O := 0; end; // Al�quota Utilizada no C�lculo do ICMS (com 2 decimais)
      end;
    end;
    55:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 1; end; // CNPJ do contribuinte Substituto tribut�rio
        03:	begin T := 014;	I := 017;	F := 030; X := 'X'; D := 0; O := 1; end; // Inscri��o Estadual na Unidade da Federa��o destinat�ria) do contribuinte substituto tribut�rio
        04:	begin T := 008;	I := 031;	F := 038; X := 'D'; D := 0; O := 1; end; // Data do pagamento do documento de Arrecada��o
        05:	begin T := 002;	I := 039;	F := 040; X := 'X'; D := 0; O := 1; end; // Sigla da unidade da Federa��o do contribuinte substituto tribut�rio
        06:	begin T := 002;	I := 041;	F := 042; X := 'N'; D := 0; O := 1; end; // Sigla da unidade da Federa��o de destino (favorecida)
        07:	begin T := 003;	I := 043;	F := 045; X := 'X'; D := 0; O := 1; end; // C�digo do Banco onde foi efetuado o recolhimento
        08:	begin T := 004;	I := 046;	F := 049; X := 'N'; D := 0; O := 1; end; // Ag�ncia onde foi efetuado o recolhimento
        09:	begin T := 020;	I := 050;	F := 069; X := 'X'; D := 0; O := 1; end; // N�mero de autentica��o banc�ria do documento de arrecada��o
        10:	begin T := 013;	I := 070;	F := 082; X := 'N'; D := 2; O := 1; end; // Valor recolhido (com 2 decimais)
        11:	begin T := 008;	I := 083;	F := 090; X := 'D'; D := 0; O := 1; end; // Data do vencimento do ICMS substitu�do
        12:	begin T := 006;	I := 091;	F := 096; X := 'D'; D := 1; O := 1; end; // M�s e ano referente � ocorr�ncia do fato gerador, formato MMAAAA
        13:	begin T := 030;	I := 097;	F := 126; X := 'X'; D := 0; O := 1; end; // Preencher com o conte�do do campo 15 da GNRE
      end;
    end;
    56:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 1; end; // CNPJ ou CPF do adquirente
        03:	begin T := 002;	I := 017;	F := 018; X := 'N'; D := 0; O := 1; end; // C�digo do modelo da nota fiscal
        04:	begin T := 003;	I := 019;	F := 021; X := 'X'; D := 0; O := 0; end; // S�rie da nota fiscal
        05:	begin T := 006;	I := 022;	F := 027; X := 'N'; D := 0; O := 1; end; // N�mero da nota fiscal
        06:	begin T := 004;	I := 028;	F := 031; X := 'N'; D := 0; O := 1; end; // C�digo Fiscal de Opera��o e Presta��o
        07:	begin T := 003;	I := 032;	F := 034; X := 'X'; D := 0; O := 1; end; // C�digo da Situa��o Tribut�ria
        08:	begin T := 003;	I := 035;	F := 037; X := 'N'; D := 0; O := 1; end; // N�mero de ordem do item na nota fiscal
        09:	begin T := 014;	I := 038;	F := 051; X := 'X'; D := 0; O := 1; end; // C�digo do produto ou servi�o do informante
        10:	begin T := 001;	I := 052;	F := 052; X := 'N'; D := 0; O := 1; end; // Tipo de opera��o: 1 - venda para concession�ria; 2- "Faturamento Direto" - Conv�nio ICMS 51/00; 3 - Venda direta; 0 � Outras
        11:	begin T := 014;	I := 053;	F := 066; X := 'N'; D := 0; O := 1; end; // CNPJ da concession�ria
        12:	begin T := 004;	I := 067;	F := 070; X := 'N'; D := 2; O := 0; end; // Al�quota do IPI (com 2 decimais)
        13:	begin T := 017;	I := 071;	F := 087; X := 'X'; D := 0; O := 1; end; // C�digo do Chassi do ve�culo
        14:	begin T := 039;	I := 088;	F := 126; X := 'X'; D := 0; O := 0; end; // Brancos
      end;
    end;
    57:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 1; end; // CNPJ do remetente nas entradas e do destinat�rio nas sa�das
        03:	begin T := 014;	I := 017;	F := 030; X := 'X'; D := 0; O := 1; end; // Inscri��o Estadual do Contribuinte
        04:	begin T := 002;	I := 031;	F := 032; X := 'N'; D := 0; O := 1; end; // C�digo do modelo da nota fiscal
        05:	begin T := 003;	I := 033;	F := 035; X := 'X'; D := 0; O := 0; end; // S�rie da nota fiscal
        06:	begin T := 006;	I := 036;	F := 041; X := 'N'; D := 0; O := 1; end; // N�mero da nota fiscal
        07:	begin T := 004;	I := 042;	F := 045; X := 'N'; D := 0; O := 1; end; // C�digo Fiscal de Opera��o e Presta��o
        08:	begin T := 003;	I := 046;	F := 048; X := 'X'; D := 0; O := 1; end; // C�digo da Situa��o Tribut�ria
        09:	begin T := 003;	I := 049;	F := 051; X := 'N'; D := 0; O := 1; end; // N�mero de ordem do item na nota fiscal
        10:	begin T := 014;	I := 052;	F := 065; X := 'X'; D := 0; O := 1; end; // C�digo do produto do informante
        11:	begin T := 020;	I := 066;	F := 085; X := 'X'; D := 0; O := 1; end; // N�mero do lote de fabrica��o do produto
        12:	begin T := 041;	I := 086;	F := 126; X := 'X'; D := 0; O := 0; end; // Brancos
      end;
    end;
    60:
    begin
      case Ord(SubTipo) of
        Ord('M'):
        begin
          case Campo of
            01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo 60
            02:	begin T := 001;	I := 003;	F := 003; X := 'X'; D := 0; O := 1; end; // Subtipo "M"
            03:	begin T := 008;	I := 004;	F := 011; X := 'D'; D := 0; O := 1; end; // Data de emiss�o dos documentos fiscais
            04:	begin T := 020;	I := 012;	F := 031; X := 'X'; D := 0; O := 1; end; // N�mero de s�rie de fabrica��o do equipamento
            05:	begin T := 003;	I := 032;	F := 034; X := 'N'; D := 0; O := 1; end; // N�mero atribu�do pelo estabelecimento ao equipamento
            06:	begin T := 002;	I := 035;	F := 036; X := 'X'; D := 0; O := 1; end; // C�digo do modelo do documento fiscal
            07:	begin T := 006;	I := 037;	F := 042; X := 'N'; D := 0; O := 1; end; // N�mero do primeiro documento fiscal emitido no dia (N�mero do Contador de Ordem de Opera��o - COO)
            08:	begin T := 006;	I := 043;	F := 048; X := 'N'; D := 0; O := 1; end; // N�mero do �ltimo documento fiscal emitido no dia (N�mero do Contador de Ordem de Opera��o - COO)
            09:	begin T := 006;	I := 049;	F := 054; X := 'N'; D := 0; O := 1; end; // N�mero do Contador de Redu��o Z (CRZ)
            10:	begin T := 003;	I := 055;	F := 057; X := 'N'; D := 0; O := 1; end; // Valor acumulado no Contador de Rein�cio de Opera��o (CRO)
            11:	begin T := 016;	I := 058;	F := 073; X := 'N'; D := 2; O := 1; end; // Valor acumulado no totalizador de Venda Bruta
            12:	begin T := 016;	I := 074;	F := 089; X := 'N'; D := 2; O := 1; end; // Valor acumulado no Totalizador Geral
            13:	begin T := 037;	I := 090;	F := 126; X := 'X'; D := 0; O := 0; end; // Brancos
          end;
        end;
        Ord('A'):
        begin
          case Campo of
            01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo 60
            02:	begin T := 001;	I := 003;	F := 003; X := 'X'; D := 0; O := 1; end; // "A"
            03:	begin T := 008;	I := 004;	F := 011; X := 'D'; D := 0; O := 1; end; // Data de emiss�o dos documentos fiscais
            04:	begin T := 020;	I := 012;	F := 031; X := 'X'; D := 0; O := 1; end; // N�mero de s�rie de fabrica��o do equipamento
            05:	begin T := 004;	I := 032;	F := 035; X := 'X'; D := 0; O := 0; end; // Identificador da Situa��o Tribut�ria / Al�quota do ICMS
            06:	begin T := 012;	I := 036;	F := 047; X := 'N'; D := 2; O := 0; end; // Valor acumulado no final do dia no totalizador parcial da situa��o tribut�ria / al�quota indicada no campo 05 (com 2 decimais)
            07:	begin T := 079;	I := 048;	F := 126; X := 'X'; D := 0; O := 0; end; // Brancos
          end;
        end;
        Ord('D'):
        begin
          case Campo of
            01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
            02:	begin T := 001;	I := 003;	F := 003; X := 'X'; D := 0; O := 1; end; // "D"
            03:	begin T := 008;	I := 004;	F := 011; X := 'D'; D := 0; O := 1; end; // Data de emiss�o dos documentos fiscais
            04:	begin T := 020;	I := 012;	F := 031; X := 'X'; D := 0; O := 1; end; // N�mero de s�rie de fabrica��o do equipamento
            05:	begin T := 014;	I := 032;	F := 045; X := 'X'; D := 0; O := 1; end; // C�digo da mercadoria/produto ou servi�o do informante
            06:	begin T := 013;	I := 046;	F := 058; X := 'N'; D := 3; O := 1; end; // Quantidade comercializada da mercadoria/produto no dia (com 3 decimais)
            07:	begin T := 016;	I := 059;	F := 074; X := 'N'; D := 2; O := 1; end; // Valor l�quido (valor bruto diminu�do dos descontos) da mercadoria/produto acumulado no dia (com 2 decimais)
            08:	begin T := 016;	I := 075;	F := 090; X := 'N'; D := 2; O := 1; end; // Base de c�lculo do ICMS - valor acumulado no dia (com 2 decimais)
            09:	begin T := 004;	I := 091;	F := 094; X := 'X'; D := 0; O := 0; end; // Identificador da Situa��o Tribut�ria / Al�quota do ICMS (com 2 decimais)
            10:	begin T := 013;	I := 095;	F := 107; X := 'N'; D := 2; O := 1; end; // Montante do imposto
            11:	begin T := 019;	I := 108;	F := 126; X := 'X'; D := 0; O := 0; end; // Brancos
          end;
        end;
        Ord('I'):
        begin
          case Campo of
            01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
            02:	begin T := 001;	I := 003;	F := 003; X := 'X'; D := 0; O := 1; end; // "I"
            03:	begin T := 008;	I := 004;	F := 011; X := 'D'; D := 0; O := 1; end; // Data de emiss�o do documento fiscal
            04:	begin T := 020;	I := 012;	F := 031; X := 'X'; D := 0; O := 1; end; // N�mero de s�rie de fabrica��o do equipamento
            05:	begin T := 002;	I := 032;	F := 033; X := 'X'; D := 0; O := 1; end; // C�digo do modelo do documento fiscal
            06:	begin T := 006;	I := 034;	F := 039; X := 'N'; D := 0; O := 1; end; // N�mero do Contador de Ordem de Opera��o (COO)
            07:	begin T := 003;	I := 040;	F := 042; X := 'N'; D := 0; O := 1; end; // N�mero de Ordem do item no Documento Fiscal
            08:	begin T := 014;	I := 043;	F := 056; X := 'X'; D := 0; O := 1; end; // C�digo da mercadoria/produto ou servi�o do informante
            09:	begin T := 013;	I := 057;	F := 069; X := 'N'; D := 3; O := 1; end; // Quantidade da mercadoria/produto (com 3 decimais)
            10:	begin T := 013;	I := 070;	F := 082; X := 'N'; D := 2; O := 1; end; // Valor l�quido (valor bruto diminu�do do desconto) da mercadoria/produto (com 2 decimais)
            11:	begin T := 012;	I := 083;	F := 094; X := 'N'; D := 2; O := 1; end; // Base de C�lculo do ICMS do Item (com 2 decimais)
            12:	begin T := 004;	I := 095;	F := 098; X := 'X'; D := 0; O := 0; end; // Identificador da Situa��o Tribut�ria / Al�quota do ICMS (com 2 decimais)
            13:	begin T := 012;	I := 099;	F := 110; X := 'N'; D := 2; O := 0; end; // Montante do Imposto (2 decimais)
            14:	begin T := 016;	I := 111;	F := 126; X := 'X'; D := 0; O := 0; end; // Brancos
          end;
        end;
        Ord('R'):
        begin
          case Campo of
            01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
            02:	begin T := 001;	I := 003;	F := 003; X := 'X'; D := 0; O := 1; end; // "R"
            03:	begin T := 006;	I := 004;	F := 009; X := 'D'; D := 1; O := 1; end; // M�s e Ano de emiss�o dos documentos fiscais
            04:	begin T := 014;	I := 010;	F := 023; X := 'X'; D := 0; O := 1; end; // C�digo da mercadoria/produto ou servi�o do informante
            05:	begin T := 013;	I := 024;	F := 036; X := 'N'; D := 3; O := 1; end; // Quantidade da mercadoria/produto no m�s (com 3 decimais)
            06:	begin T := 016;	I := 037;	F := 052; X := 'N'; D := 2; O := 1; end; // Valor l�quido (valor bruto diminu�do do desconto) da mercadoria/produto ou servi�o acumulado no m�s (com 2 decimais)
            07:	begin T := 016;	I := 053;	F := 068; X := 'N'; D := 2; O := 1; end; // Base de c�lculo do ICMS - valor acumulado no m�s (com 2 decimais)
            08:	begin T := 004;	I := 069;	F := 072; X := 'X'; D := 0; O := 0; end; // Identificador da Situa��o Tribut�ria / Al�quota do ICMS (com 2 decimais)
            09:	begin T := 054;	I := 073;	F := 126; X := 'X'; D := 0; O := 0; end; // Brancos
          end;
        end;
      end;
    end;
    61:
    begin
      case Ord(SubTipo) of
        Ord(' '):
        begin
          case Campo of
            01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
            02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 0; end; // Brancos
            03:	begin T := 014;	I := 017;	F := 030; X := 'X'; D := 0; O := 0; end; // Brancos
            04:	begin T := 008;	I := 031;	F := 038; X := 'D'; D := 0; O := 1; end; // Data de emiss�o do(s) documento(s) fiscal(is)
            05:	begin T := 002;	I := 039;	F := 040; X := 'X'; D := 0; O := 1; end; // Modelo do(s) documento(s) fiscal(is)
            06:	begin T := 003;	I := 041;	F := 043; X := 'X'; D := 0; O := 0; end; // S�rie do(s) documento(s) fiscal(is)
            07:	begin T := 002;	I := 044;	F := 045; X := 'X'; D := 0; O := 1; end; // Subs�rie do(s) documento(s) fiscal(is)
            08:	begin T := 006;	I := 046;	F := 051; X := 'N'; D := 0; O := 1; end; // N�mero do primeiro documento fiscal emitido no dia do mesmo modelo, s�rie e subs�rie
            09:	begin T := 006;	I := 052;	F := 057; X := 'N'; D := 0; O := 1; end; // N�mero do �ltimo documento fiscal emitido no dia do mesmo modelo, s�rie e subs�rie
            10:	begin T := 013;	I := 058;	F := 070; X := 'N'; D := 2; O := 1; end; // Valor total do(s) documento(s) fiscal(is)/Movimento di�rio (com 2 decimais)
            11:	begin T := 013;	I := 071;	F := 083; X := 'N'; D := 2; O := 1; end; // Base de c�lculo do(s) documento(s) fiscal(is)/Total di�rio (com 2 decimais)
            12:	begin T := 012;	I := 084;	F := 095; X := 'N'; D := 2; O := 1; end; // Valor do Montante do Imposto/Total di�rio (com 2 decimais)
            13:	begin T := 013;	I := 096;	F := 108; X := 'N'; D := 2; O := 0; end; // Valor amparado por isen��o ou n�o-incid�ncia/Total di�rio (com 2 decimais)
            14:	begin T := 013;	I := 109;	F := 121; X := 'N'; D := 2; O := 0; end; // Valor que n�o confira d�bito ou cr�dito de ICMS/Total di�rio (com 2 decimais)
            15:	begin T := 004;	I := 122;	F := 125; X := 'N'; D := 2; O := 0; end; // Al�quota do ICMS (com 2 decimais)
            16:	begin T := 001;	I := 126;	F := 126; X := 'X'; D := 0; O := 0; end; // Branco
          end;
        end;
        Ord('R'):
        begin
          case Campo of
            01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
            02:	begin T := 001;	I := 003;	F := 003; X := 'X'; D := 0; O := 1; end; // "R"
            03:	begin T := 006;	I := 004;	F := 009; X := 'D'; D := 1; O := 1; end; // M�s e Ano de emiss�o dos documentos fiscais
            04:	begin T := 014;	I := 010;	F := 023; X := 'X'; D := 0; O := 1; end; // C�digo do produto do informante
            05:	begin T := 013;	I := 024;	F := 036; X := 'N'; D := 3; O := 1; end; // Quantidade do produto acumulada vendida no m�s (com 3 decimais)
            06:	begin T := 016;	I := 037;	F := 052; X := 'N'; D := 2; O := 1; end; // Valor bruto do produto - valor acumulado da venda do produto no m�s (com 2 decimais)
            07:	begin T := 016;	I := 053;	F := 068; X := 'N'; D := 2; O := 1; end; // Base de c�lculo do ICMS do valor acumulado no m�s (com 2 decimais)
            08:	begin T := 004;	I := 069;	F := 072; X := 'X'; D := 2; O := 0; end; // Al�quota do ICMS do produto
            09:	begin T := 054;	I := 073;	F := 126; X := 'X'; D := 0; O := 0; end; // Preencher posi��es com espa�os em branco
          end;
        end;
      end;
    end;
    70:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 1; end; // CNPJ do emitente do documento, no caso de aquisi��o de servi�o; CNPJ do tomador do servi�o, no caso de emiss�o do documento
        03:	begin T := 014;	I := 017;	F := 030; X := 'X'; D := 0; O := 1; end; // Inscri��o Estadual do emitente do documento, no caso de aquisi��o de servi�o; Inscri��o Estadual do tomador do servi�o, no caso de emiss�o do documento
        04:	begin T := 008;	I := 031;	F := 038; X := 'D'; D := 0; O := 1; end; // Data de emiss�o para o prestador, ou data de utiliza��o do servi�o para o tomador
        05:	begin T := 002;	I := 039;	F := 040; X := 'X'; D := 0; O := 1; end; // Sigla da unidade da Federa��o do emitente do documento, no caso de aquisi��o de servi�o, ou do tomador do servi�o, no caso de emiss�o do documento
        06:	begin T := 002;	I := 041;	F := 042; X := 'N'; D := 0; O := 1; end; // C�digo do modelo do documento fiscal
        07:	begin T := 001;	I := 043;	F := 043; X := 'X'; D := 0; O := 0; end; // S�rie do documento
        08:	begin T := 002;	I := 044;	F := 045; X := 'X'; D := 0; O := 0; end; // Subs�rie do documento
        09:	begin T := 006;	I := 046;	F := 051; X := 'N'; D := 0; O := 1; end; // N�mero do documento
        10:	begin T := 004;	I := 052;	F := 055; X := 'N'; D := 0; O := 1; end; // C�digo Fiscal de Opera��o e Presta��o - Um registro para cada CFOP do documento fiscal
        11:	begin T := 013;	I := 056;	F := 068; X := 'N'; D := 2; O := 1; end; // Valor total do documento fiscal (com 2 decimais)
        12:	begin T := 014;	I := 069;	F := 082; X := 'N'; D := 2; O := 1; end; // Base de c�lculo do ICMS (com duas decimais)
        13:	begin T := 014;	I := 083;	F := 096; X := 'N'; D := 2; O := 1; end; // Montante do imposto (com duas decimais)
        14:	begin T := 014;	I := 097;	F := 110; X := 'N'; D := 2; O := 0; end; // Valor amparado por isen��o ou n�o incid�ncia (com duas decimais)
        15:	begin T := 014;	I := 111;	F := 124; X := 'N'; D := 2; O := 0; end; // Valor que n�o confira d�bito ou cr�dito do ICMS (com duas decimais)
        16:	begin T := 001;	I := 125;	F := 125; X := 'N'; D := 0; O := 0; end; // Modalidade do frete - "1" - CIF, "2" - FOB ou "0" - OUTR0S (a op��o "0" - OUTROS nos casos em que n�o se aplica a informa��o de cl�usula CIF ou FOB)
        17:	begin T := 001;	I := 126;	F := 126; X := 'X'; D := 0; O := 1; end; // Situa��o do documento fiscal
      end;
    end;
    71:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 1; end; // CNPJ do tomador do servi�o
        03:	begin T := 014;	I := 017;	F := 030; X := 'X'; D := 0; O := 1; end; // Inscri��o estadual do tomador do servi�o
        04:	begin T := 008;	I := 031;	F := 038; X := 'D'; D := 0; O := 1; end; // Data de emiss�o do conhecimento
        05:	begin T := 002;	I := 039;	F := 040; X := 'X'; D := 0; O := 1; end; // Unidade da Federa��o do tomador do servi�o
        06:	begin T := 002;	I := 041;	F := 042; X := 'N'; D := 0; O := 1; end; // Modelo de Conhecimento
        07:	begin T := 001;	I := 043;	F := 043; X := 'X'; D := 0; O := 0; end; // S�rie do conhecimento
        08:	begin T := 002;	I := 044;	F := 045; X := 'X'; D := 0; O := 0; end; // Subs�rie do conhecimento
        09:	begin T := 006;	I := 046;	F := 051; X := 'N'; D := 0; O := 1; end; // N�mero do conhecimento
        10:	begin T := 002;	I := 052;	F := 053; X := 'X'; D := 0; O := 1; end; // Unidade da Federa��o do remetente, se o destinat�rio for o tomador ou unidade da Federa��o do destinat�rio, se o remetente for o tomador
        11:	begin T := 014;	I := 054;	F := 067; X := 'N'; D := 0; O := 1; end; // CNPJ do remetente, se o destinat�rio for o tomador ou CNPJ do destinat�rio, se o remetente for o tomador
        12:	begin T := 014;	I := 068;	F := 081; X := 'X'; D := 0; O := 1; end; // Inscri��o Estadual do remetente, se o destinat�rio for o tomador ou Inscri��o Estadual do destinat�rio, se o remetente for o tomador
        13:	begin T := 008;	I := 082;	F := 089; X := 'D'; D := 0; O := 1; end; // Data de emiss�o da nota fiscal que acoberta a carga transportada
        14:	begin T := 002;	I := 090;	F := 091; X := 'X'; D := 0; O := 1; end; // Modelo da nota fiscal que acoberta a carga transportada
        15:	begin T := 003;	I := 092;	F := 094; X := 'X'; D := 0; O := 0; end; // S�rie da nota fiscal que acoberta a carga transportada
        16:	begin T := 006;	I := 095;	F := 100; X := 'N'; D := 0; O := 1; end; // N�mero da nota fiscal que acoberta a carga transportada
        17:	begin T := 014;	I := 101;	F := 114; X := 'N'; D := 2; O := 1; end; // Valor total da nota fiscal que acoberta a carga transportada (com duas decimais)
        18:	begin T := 012;	I := 115;	F := 126; X := 'X'; D := 0; O := 0; end; // Brancos
      end;
    end;
    74:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 008;	I := 003;	F := 010; X := 'D'; D := 0; O := 1; end; // Data do Invent�rio no formato AAAAMMDD
        03:	begin T := 014;	I := 011;	F := 024; X := 'X'; D := 0; O := 1; end; // C�digo do produto do informante
        04:	begin T := 013;	I := 025;	F := 037; X := 'N'; D := 3; O := 1; end; // Quantidade do produto (com 3 decimais)
        05:	begin T := 013;	I := 038;	F := 050; X := 'N'; D := 2; O := 1; end; // Valor bruto do produto (valor unit�rio multiplicado por quantidade) - com 2 decimais
        06:	begin T := 001;	I := 051;	F := 051; X := 'X'; D := 0; O := 1; end; // C�digo de Posse das Mercadorias Inventariadas, conforme tabela abaixo
        07:	begin T := 014;	I := 052;	F := 065; X := 'N'; D := 0; O := 0; end; // CNPJ do Possuidor da Mercadoria de propriedade do Informante, ou do propriet�rio da Mercadoria em poder do Informante
        08:	begin T := 014;	I := 066;	F := 079; X := 'X'; D := 0; O := 0; end; // Inscri��o Estadual do Possuidor da Mercadoria de propriedade do Informante, ou do propriet�rio da Mercadoria em poder do Informante
        09:	begin T := 002;	I := 080;	F := 081; X := 'X'; D := 0; O := 1; end; // Unidade da Federa��o do Possuidor da Mercadoria de propriedade do Informante, ou do propriet�rio da Mercadoria em poder do Informante
        10:	begin T := 045;	I := 082;	F := 126; X := 'X'; D := 0; O := 0; end; // Brancos
      end;
    end;
    75:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 008;	I := 003;	F := 010; X := 'D'; D := 0; O := 1; end; // Data inicial do per�odo de validade das informa��es
        03:	begin T := 008;	I := 011;	F := 018; X := 'D'; D := 0; O := 1; end; // Data final do per�odo de validade das informa��es
        04:	begin T := 014;	I := 019;	F := 032; X := 'X'; D := 0; O := 1; end; // C�digo do produto ou servi�o utilizado pelo contribuinte
        05:	begin T := 008;	I := 033;	F := 040; X := 'X'; D := 0; O := 2; end; // Codifica��o da Nomenclatura Comum do Mercosul
        06:	begin T := 053;	I := 041;	F := 093; X := 'X'; D := 0; O := 1; end; // Descri��o do produto ou servi�o
        07:	begin T := 006;	I := 094;	F := 099; X := 'X'; D := 0; O := 1; end; // Unidade de medida de comercializa��o do produto ( un, kg, mt, m3, sc, frd, kWh, etc..)
        08:	begin T := 005;	I := 100;	F := 104; X := 'N'; D := 2; O := 0; end; // Al�quota do IPI do produto (com 2 decimais)
        09:	begin T := 004;	I := 105;	F := 108; X := 'N'; D := 2; O := 0; end; // Al�quota do ICMS aplic�vel a mercadoria ou servi�o nas opera��es ou presta��es internas ou naquelas que se tiverem iniciado no exterior (com 2 decimais)
        10:	begin T := 005;	I := 109;	F := 113; X := 'N'; D := 2; O := 0; end; // % de Redu��o na base de c�lculo do ICMS, nas opera��es internas (com 2 decimais)
        11:	begin T := 013;	I := 114;	F := 126; X := 'N'; D := 2; O := 0; end; // Base de C�lculo do ICMS de substitui��o tribut�ria (com 2 decimais)
      end;
    end;
    76:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 1; end; // CNPJ/CPF do tomador do servi�o
        03:	begin T := 014;	I := 017;	F := 030; X := 'X'; D := 0; O := 1; end; // Inscri��o Estadual do do tomador do servi�o
        04:	begin T := 002;	I := 031;	F := 032; X := 'N'; D := 0; O := 1; end; // C�digo do modelo da nota fiscal
        05:	begin T := 002;	I := 033;	F := 034; X := 'X'; D := 0; O := 0; end; // S�rie da nota fiscal
        06:	begin T := 002;	I := 035;	F := 036; X := 'X'; D := 0; O := 0; end; // Subs�rie da nota fiscal
        07:	begin T := 010;	I := 037;	F := 046; X := 'N'; D := 0; O := 1; end; // N�mero da nota fiscal
        08:	begin T := 004;	I := 047;	F := 050; X := 'N'; D := 0; O := 1; end; // C�digo Fiscal de Opera��o e Presta��o
        09:	begin T := 001;	I := 051;	F := 051; X := 'N'; D := 0; O := 1; end; // C�digo da identifica��o do tipo de receita, conforme tabela abaixo
        10:	begin T := 008;	I := 052;	F := 059; X := 'D'; D := 0; O := 1; end; // Data de emiss�o na sa�da ou de Recebimento na entrada
        11:	begin T := 002;	I := 060;	F := 061; X := 'X'; D := 0; O := 1; end; // Sigla da Unidade da Federa��o do Remetente nas entradas e do destinat�rio nas sa�das
        12:	begin T := 013;	I := 062;	F := 074; X := 'N'; D := 2; O := 1; end; // Valor total da nota fiscal (com 2 decimais)
        13:	begin T := 013;	I := 075;	F := 087; X := 'N'; D := 2; O := 1; end; // Base de C�lculo do ICMS (com 2 decimais)
        14:	begin T := 012;	I := 088;	F := 099; X := 'N'; D := 2; O := 1; end; // Montante do imposto (com 2 decimais)
        15:	begin T := 012;	I := 100;	F := 111; X := 'N'; D := 2; O := 1; end; // Valor amparado por isen��o ou n�o-incid�ncia (com 2 decimais)
        16:	begin T := 012;	I := 112;	F := 123; X := 'N'; D := 2; O := 1; end; // Valor que n�o confira d�bito ou Cr�dito do ICMS (com 2 decimais)
        17:	begin T := 002;	I := 124;	F := 125; X := 'N'; D := 2; O := 0; end; // Al�quota do ICMS (valor inteiro)
        18:	begin T := 001;	I := 126;	F := 126; X := 'X'; D := 0; O := 1; end; // Situa��o da nota fiscal
      end;
    end;
    77:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 1; end; // CNPJ/CPF do tomador do servi�o
        03:	begin T := 002;	I := 017;	F := 018; X := 'N'; D := 0; O := 1; end; // C�digo do modelo da nota fiscal
        04:	begin T := 002;	I := 019;	F := 020; X := 'X'; D := 0; O := 0; end; // S�rie da nota fiscal
        05:	begin T := 002;	I := 021;	F := 022; X := 'X'; D := 0; O := 0; end; // Subs�rie da nota fiscal
        06:	begin T := 010;	I := 023;	F := 032; X := 'N'; D := 0; O := 1; end; // N�mero da nota fiscal
        07:	begin T := 004;	I := 033;	F := 036; X := 'N'; D := 0; O := 1; end; // C�digo Fiscal de Opera��o e Presta��o
        08:	begin T := 001;	I := 037;	F := 037; X := 'N'; D := 0; O := 1; end; // C�digo da identifica��o do tipo de receita, conforme tabela abaixo
        09:	begin T := 003;	I := 038;	F := 040; X := 'N'; D := 0; O := 1; end; // N�mero de ordem do item na nota fiscal
        10:	begin T := 011;	I := 041;	F := 051; X := 'X'; D := 0; O := 1; end; // C�digo do servi�o do informante
        11:	begin T := 013;	I := 052;	F := 064; X := 'N'; D := 3; O := 1; end; // Quantidade do servi�o (com 3 decimais)
        12:	begin T := 012;	I := 065;	F := 076; X := 'N'; D := 2; O := 1; end; // Valor bruto do servi�o (valor unit�rio multiplicado por Quantidade) - com 2 decimais
        13:	begin T := 012;	I := 077;	F := 088; X := 'N'; D := 2; O := 0; end; // Valor do Desconto Concedido no item (com 2 decimais).
        14:	begin T := 012;	I := 089;	F := 100; X := 'N'; D := 2; O := 1; end; // Base de c�lculo do ICMS (com 2 decimais)
        15:	begin T := 002;	I := 101;	F := 102; X := 'N'; D := 0; O := 0; end; // Al�quota Utilizada no C�lculo do ICMS (valor inteiro)
        16:	begin T := 014;	I := 103;	F := 116; X := 'N'; D := 0; O := 1; end; // CNPJ/MF da operadora de destino
        17:	begin T := 010;	I := 117;	F := 126; X := 'N'; D := 0; O := 1; end; // C�digo que designa o usu�rio final na rede do informante
      end;
    end;
    85:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 011;	I := 003;	F := 013; X := 'N'; D := 0; O := 1; end; // N� da Declara��o de Exporta��o/ N� Declara��o Simplificada de  Exporta��o
        03:	begin T := 008;	I := 014;	F := 021; X := 'D'; D := 0; O := 1; end; // Data da Declara��o de Exporta��o (AAAAMMDD)
        04:	begin T := 001;	I := 022;	F := 022; X := 'X'; D := 0; O := 1; end; // Preencher com: "1" - Exporta��o Direta "2" - Exporta��o Indireta "3" - Exporta��o Direta- Regime Simplificado "4" - Exporta��o Indireta- Regime Simplificado
        05:	begin T := 012;	I := 023;	F := 034; X := 'N'; D := 0; O := 1; end; // N� do registro de Exporta��o
        06:	begin T := 008;	I := 035;	F := 042; X := 'D'; D := 0; O := 1; end; // Data do Registro de Exporta��o (AAAAMMDD)
        07:	begin T := 016;	I := 043;	F := 058; X := 'X'; D := 0; O := 1; end; // N� do conhecimento de embarque
        08:	begin T := 008;	I := 059;	F := 066; X := 'D'; D := 0; O := 1; end; // Data do conhecimento de embarque (AAAAMMDD)
        09:	begin T := 002;	I := 067;	F := 068; X := 'N'; D := 0; O := 1; end; // Informa��o do tipo de conhecimento de transporte (Preencher conforme tabela de tipo de documento de carga do SISCOMEX - anexa)
        10:	begin T := 004;	I := 069;	F := 072; X := 'N'; D := 0; O := 1; end; // C�digo do pa�s de destino da mercadoria (Preencher conforme tabela do SISCOMEX)
        11:	begin T := 008;	I := 073;	F := 080; X := 'N'; D := 0; O := 0; end; // Preencher com zeros
        12:	begin T := 008;	I := 081;	F := 088; X := 'D'; D := 0; O := 1; end; // Data da averba��o da Declara��o de exporta��o (AAAAMMDD)
        13:	begin T := 006;	I := 089;	F := 094; X := 'N'; D := 0; O := 1; end; // N�mero de Nota Fiscal de Exporta��o emitida pelo Exportador
        14:	begin T := 008;	I := 095;	F := 102; X := 'D'; D := 0; O := 1; end; // Data da emiss�o da NF de exporta��o / revenda (AAAAMMDD)
        15:	begin T := 002;	I := 103;	F := 104; X := 'N'; D := 0; O := 1; end; // C�digo do modelo da NF
        16:	begin T := 003;	I := 105;	F := 107; X := 'N'; D := 0; O := 0; end; // S�rie da Nota Fiscal
        17:	begin T := 019;	I := 108;	F := 126; X := 'X'; D := 0; O := 0; end; // Brancos
      end;
    end;
    86:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 012;	I := 003;	F := 014; X := 'N'; D := 0; O := 1; end; // N� do registro de Exporta��o
        03:	begin T := 008;	I := 015;	F := 022; X := 'D'; D := 0; O := 1; end; // Data do Registro de Exporta��o (AAAAMMDD)
        04:	begin T := 014;	I := 023;	F := 036; X := 'N'; D := 0; O := 1; end; // CNPJ do contribuinte Produtor/Industrial/Fabricante que promoveu a remessa com fim espec�fico
        05:	begin T := 014;	I := 037;	F := 050; X := 'X'; D := 0; O := 1; end; // Inscri��o Estadual do contribuinte Produtor/Industrial/Fabricante que promoveu a remessa com fim espec�fico
        06:	begin T := 002;	I := 051;	F := 052; X := 'X'; D := 0; O := 1; end; // Unidade da Federa��o do Produtor/Industrial/Fabricante que promoveu remessa com fim espec�fico
        07:	begin T := 006;	I := 053;	F := 058; X := 'N'; D := 0; O := 1; end; // N� da Nota Fiscal de remessa com fim espec�fico de exporta��o recebida
        08:	begin T := 008;	I := 059;	F := 066; X := 'D'; D := 0; O := 1; end; // Data de emiss�o da Nota Fiscal da remessa com fim espec�fico (AAAAMMMDD)
        09:	begin T := 002;	I := 067;	F := 068; X := 'N'; D := 0; O := 1; end; // C�digo do modelo do documento fiscal
        10:	begin T := 003;	I := 069;	F := 071; X := 'N'; D := 0; O := 0; end; // S�rie da Nota Fiscal
        11:	begin T := 014;	I := 072;	F := 085; X := 'X'; D := 0; O := 1; end; // C�digo do produto adotado no registro tipo 75 quando do registro de entrada da Nota Fiscal de remessa com fim espec�fico
        12:	begin T := 011;	I := 086;	F := 096; X := 'N'; D := 3; O := 1; end; // Quantidade, efetivamente exportada, do produto declarado na Nota Fiscal de remessa com fim espec�fico recebida (com tr�s decimais)
        13:	begin T := 012;	I := 097;	F := 108; X := 'N'; D := 2; O := 1; end; // Valor unit�rio do produto (com duas decimais)
        14:	begin T := 012;	I := 109;	F := 120; X := 'N'; D := 2; O := 1; end; // Valor total do produto (valor unit�rio multiplicado pela quantidade) - com 2 decimais
        15:	begin T := 001;	I := 121;	F := 121; X := 'N'; D := 0; O := 1; end; // Preencher conforme tabela de c�digos de relacionamento entre Registro de Exporta��o e Nota Fiscal de remessa com fim espec�fico - Tabela A
        16:	begin T := 005;	I := 122;	F := 126; X := 'X'; D := 0; O := 0; end; // Brancos
      end;
    end;
    90:
    begin
      case Campo of
        01:	begin T := 002;	I := 001;	F := 002; X := 'N'; D := 0; O := 1; end; // Tipo
        02:	begin T := 014;	I := 003;	F := 016; X := 'N'; D := 0; O := 1; end; // CGC/MF do informante
        03:	begin T := 014;	I := 017;	F := 030; X := 'X'; D := 0; O := 1; end; // Inscri��o Estadual do informante
        04:	begin T := 002;	I := 031;	F := 032; X := 'N'; D := 0; O := 1; end; // Tipo de registro que ser� totalizado pelo pr�ximo campo
        05:	begin T := 008;	I := 033;	F := 040; X := 'N'; D := 0; O := 1; end; // Total de registros do tipo informado no campo anterior
       // ADAPTA��O
        06:	begin T := 002;	I := 041;	F := 042; X := 'X'; D := 0; O := 0; end; // Tipo de registro que ser� totalizado pelo pr�ximo campo
        07:	begin T := 008;	I := 043;	F := 050; X := 'X'; D := 0; O := 0; end; // Total de registros do tipo informado no campo anterior
        08:	begin T := 002;	I := 051;	F := 052; X := 'X'; D := 0; O := 0; end; // Tipo de registro que ser� totalizado pelo pr�ximo campo
        09:	begin T := 008;	I := 053;	F := 060; X := 'X'; D := 0; O := 0; end; // Total de registros do tipo informado no campo anterior
        10:	begin T := 002;	I := 061;	F := 062; X := 'X'; D := 0; O := 0; end; // Tipo de registro que ser� totalizado pelo pr�ximo campo
        11:	begin T := 008;	I := 063;	F := 070; X := 'X'; D := 0; O := 0; end; // Total de registros do tipo informado no campo anterior
        12:	begin T := 002;	I := 071;	F := 072; X := 'X'; D := 0; O := 0; end; // Tipo de registro que ser� totalizado pelo pr�ximo campo
        13:	begin T := 008;	I := 073;	F := 080; X := 'X'; D := 0; O := 0; end; // Total de registros do tipo informado no campo anterior
        14:	begin T := 002;	I := 081;	F := 082; X := 'X'; D := 0; O := 0; end; // Tipo de registro que ser� totalizado pelo pr�ximo campo
        15:	begin T := 008;	I := 083;	F := 090; X := 'X'; D := 0; O := 0; end; // Total de registros do tipo informado no campo anterior
        16:	begin T := 002;	I := 091;	F := 092; X := 'X'; D := 0; O := 0; end; // Tipo de registro que ser� totalizado pelo pr�ximo campo
        17:	begin T := 008;	I := 093;	F := 100; X := 'X'; D := 0; O := 0; end; // Total de registros do tipo informado no campo anterior
        18:	begin T := 002;	I := 101;	F := 102; X := 'X'; D := 0; O := 0; end; // Tipo de registro que ser� totalizado pelo pr�ximo campo
        19:	begin T := 008;	I := 103;	F := 110; X := 'X'; D := 0; O := 0; end; // Total de registros do tipo informado no campo anterior
        20:	begin T := 002;	I := 111;	F := 112; X := 'X'; D := 0; O := 0; end; // Tipo de registro que ser� totalizado pelo pr�ximo campo
        21:	begin T := 008;	I := 113;	F := 120; X := 'X'; D := 0; O := 0; end; // Total de registros do tipo informado no campo anterior
        22:	begin T := 005;	I := 121;	F := 125; X := 'X'; D := 0; O := 0; end; // Brancos
        // FIM ADAPTA��O
        23:	begin T := 001;	I := 126;	F := 126; X := 'N'; D := 0; O := 1; end; // N�mero de registros tipo 90
      end;
    end;
  end;
  Tam  := T;
  Ini  := I;
  Fim  := F;
  Fmt  := X;
  Deci := D;
  Obrigatorio := O = 1;
  Result := True;
end;

procedure TUnSintegra.CriaNFe(ImporExpor, AnoMes, Empresa, LinArq: Integer);
const
  Importado = 1;
var
  ide_serie, ide_mod, Versao, IDCtrl, FatID, FatNum, ide_nNF,
  CriAForca,   CodInfoDest, CodInfoEmit, GraGruX: Integer;
  Id, ide_verProc, ide_natOp, ide_tpNF, ide_dEmi, ide_dSaiEnt, DataFiscal,
  ide_indPag, emit_CNPJ, emit_CPF, emit_IE, dest_CNPJ, dest_CPF, dest_IE: String;
  ICMSTot_vICMS, ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc,
  ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro, ICMSTot_vNF:
  Double;
  // Itens
  MeuID, Nivel1, nItem: Integer;
  prod_CFOP, prod_uCom, prod_uTrib: String;
  prod_qCom, prod_vUnCom, prod_vProd, prod_qTrib, prod_vUnTrib: Double;
  //
  CFOP, CFOPa, CFOPb, CFOPc, CFOPd, CFOPe: String;
  //
  NFG_Serie, NF_CFOP, NFG_SubSerie: String;
  NF_ICMSAlq, NFG_ValIsen, NFG_NaoTrib, NFG_Outros, ICMSTot_vBC,
  ICMSRec_vICMS, ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq: Double;
  EFD_INN_AnoMes, EFD_INN_Empresa, EFD_INN_LinArq, UnidMedCom, UnidMedTrib: Integer;

  // nfeitsn
  ICMS_Orig, ICMS_CST, ICMS_modBC, ICMS_modBCST, Status, Tabela: Integer;
  ICMS_pRedBC, ICMS_vBC, ICMS_pICMS, ICMS_vICMS, ICMS_pMVAST, ICMS_pRedBCST,
  ICMS_vBCST, ICMS_pICMSST, ICMS_vICMSST: Double;
  //
  Ano, Mes, Dia: Word;
begin
  //
  Tabela := MyObjects.SelRadioGroup('Tipo de Entrada', 'Tipo de entrada',
  ['Mat�ria-prima','Uso e consumo'], 1);
  if Tabela = 0 then
  begin
    Tabela := 2;
    FatID := VAR_FATID_0113;
    FatNum := UMyMod.BuscaEmLivreY_Def('MPInIts', 'Conta', stIns, 0, nil, True);
  end else
  begin
    Tabela := 1;
    FatID := VAR_FATID_0151;
    FatNum := UMyMod.BuscaEmLivreY_Def('pqe', 'Codigo', stIns, 0, nil, True);
  end;
  //
  DmProd.QrSINTEGR_50.Close;
  DmProd.QrSINTEGR_50.Params[00].AsInteger := ImporExpor;
  DmProd.QrSINTEGR_50.Params[01].AsInteger := AnoMes;
  DmProd.QrSINTEGR_50.Params[02].AsInteger := Empresa;
  DmProd.QrSINTEGR_50.Params[03].AsInteger := LinArq;
  DmProd.QrSINTEGR_50.Open;
  //
  if DmProd.QrSINTEGR_50EMITENTE.Value = 'P' (* Pr�prio *) then
  begin
    CodInfoDest                 := DmProd.QrSINTEGR_50Terceiro.Value;
    CodInfoEmit                 := Empresa;
  end else begin
    CodInfoDest                 := Empresa;
    CodInfoEmit                 := DmProd.QrSINTEGR_50Terceiro.Value;
  end;

  Id                          := '';
  ide_verProc                 := '';
  ide_natOp                   := '';
  ide_mod                     := Geral.IMV(DmProd.QrSINTEGR_50MODELO.Value);
  ide_serie                   := Geral.IMV(DmProd.QrSINTEGR_50SERIE.Value);
  ide_tpNF                    := '1';
  ide_indPag                  := '1';
  ide_nNF                     := Geral.IMV(DmProd.QrSINTEGR_50NUMNF.Value);
  DecodeDate(DmProd.QrSINTEGR_50EMISSAO.Value, Ano, Mes, Dia);
  if Dia < 3 then
    ide_dEmi := Geral.FDT(DmProd.QrSINTEGR_50EMISSAO.Value, 1)
  else
    ide_dEmi := Geral.FDT(DmProd.QrSINTEGR_50EMISSAO.Value - 2, 1);
  ide_dSaiEnt                 := ide_dEmi;
  versao                      := 0;
  DataFiscal                  := Geral.FDT(DmProd.QrSINTEGR_50EMISSAO.Value, 1);
  //
  DModG.ObtemCNPJ_CPF_IE_DeEntidade(CodInfoEmit, emit_CNPJ, emit_CPF, emit_IE);
  DModG.ObtemCNPJ_CPF_IE_DeEntidade(CodInfoDest, dest_CNPJ, dest_CPF, dest_IE);
  //
  ICMSTot_vProd               := DmProd.QrSINTEGR_50ValorNF.Value(*QrSumPQEItsValorItem.Value*);
  ICMSTot_vFrete              := 0(*QrPQE_2_Frete.Value*);
  ICMSTot_vSeg                := 0(*QrPQE_2_Seguro.Value*);
  ICMSTot_vDesc               := 0(*QrPQE_2_Desconto.Value*);
  ICMSTot_vIPI                := 0(*QrPQE_2_IPI.Value*);
  ICMSTot_vPIS                := 0(*QrPQE_2_PIS.Value*);
  ICMSTot_vCOFINS             := 0(*QrPQE_2_COFINS.Value*);
  ICMSTot_vOutro              := 0(*QrPQE_2_Outros.Value*);
  ICMSTot_vNF                 := DmProd.QrSINTEGR_50ValorNF.Value;
  ICMSTot_vICMS               := DmProd.QrSINTEGR_50ValICMS.Value;
  ICMSTot_vBC                 := DmProd.QrSINTEGR_50BCICMS.Value;
  NFG_Serie := DmProd.QrSINTEGR_50SERIE.Value;
  NF_ICMSAlq := DmProd.QrSINTEGR_50AliqICMS.Value;
  NF_CFOP := DmProd.QrSINTEGR_50CFOP.Value;
  NFG_SubSerie := '';
  NFG_ValIsen := DmProd.QrSINTEGR_50ValIsento.Value;
  NFG_NaoTrib := DmProd.QrSINTEGR_50ValNCICMS.Value;
  NFG_Outros := 0;  // ???
  EFD_INN_AnoMes := DmProd.QrSINTEGR_50AnoMes.Value;
  EFD_INN_Empresa := DmProd.QrSINTEGR_50Empresa.Value;
  EFD_INN_LinArq := DmProd.QrSINTEGR_50LinArq.Value;
  //
  Status := USintegra.ObtemStatusDaSituacao(DmProd.QrSINTEGR_50Situacao.Value);
  //
  IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);

  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False,[
'IDCtrl', (*'LoteEnv', 'versao',*)
'Id', (*'ide_cUF', 'ide_cNF',*)
'ide_natOp', 'ide_indPag', 'ide_mod',
'ide_serie', 'ide_nNF', 'ide_dEmi',
'ide_dSaiEnt', 'ide_tpNF', (*'ide_cMunFG',
'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
'ide_tpAmb', 'ide_finNFe', 'ide_procEmi',*)
'ide_verProc', 'emit_CNPJ', 'emit_CPF',
(*'emit_xNome', 'emit_xFant', 'emit_xLgr',
'emit_nro', 'emit_xCpl', 'emit_xBairro',
'emit_cMun', 'emit_xMun', 'emit_UF',
'emit_CEP', 'emit_cPais', 'emit_xPais',
'emit_fone',*) 'emit_IE', (*'emit_IEST',
'emit_IM', 'emit_CNAE',*) 'dest_CNPJ',
'dest_CPF', (*'dest_xNome', 'dest_xLgr',
'dest_nro', 'dest_xCpl', 'dest_xBairro',
'dest_cMun', 'dest_xMun', 'dest_UF',
'dest_CEP', 'dest_cPais', 'dest_xPais',
'dest_fone',*) 'dest_IE', (*'dest_ISUF',*)
'ICMSTot_vBC', 'ICMSTot_vICMS', (*'ICMSTot_vBCST',
'ICMSTot_vST',*) 'ICMSTot_vProd', 'ICMSTot_vFrete',
'ICMSTot_vSeg', 'ICMSTot_vDesc', (*'ICMSTot_vII',*)
'ICMSTot_vIPI', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
'ICMSTot_vOutro', 'ICMSTot_vNF', (*'ISSQNtot_vServ',
'ISSQNtot_vBC', 'ISSQNtot_vISS', 'ISSQNtot_vPIS',
'ISSQNtot_vCOFINS', 'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS',
'RetTrib_vRetCSLL', 'RetTrib_vBCIRRF', 'RetTrib_vIRRF',
'RetTrib_vBCRetPrev', 'RetTrib_vRetPrev', 'ModFrete',
'Transporta_CNPJ', 'Transporta_CPF', 'Transporta_XNome',
'Transporta_IE', 'Transporta_XEnder', 'Transporta_XMun',
'Transporta_UF', 'RetTransp_vServ', 'RetTransp_vBCRet',
'RetTransp_PICMSRet', 'RetTransp_vICMSRet', 'RetTransp_CFOP',
'RetTransp_CMunFG', 'VeicTransp_Placa', 'VeicTransp_UF',
'VeicTransp_RNTC', 'Cobr_Fat_nFat', 'Cobr_Fat_vOrig',
'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq', 'InfAdic_InfAdFisco',
'InfAdic_InfCpl', 'Exporta_UFEmbarq', 'Exporta_XLocEmbarq',
'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',*)
'Status', (*'infProt_Id', 'infProt_tpAmb',
'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
'infCanc_xJust', '_Ativo_', 'FisRegCad',
'CartEmiss', 'TabelaPrc', 'CondicaoPg',
'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',
'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
'DataFiscal', (*'protNFe_versao', 'retCancNFe_versao',
'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta', 'SINTEGRA_ExpNat',
'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta', 'SINTEGRA_ExpConhNum',
'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip', 'SINTEGRA_ExpPais',
'SINTEGRA_ExpAverDta',*) 'CodInfoEmit', 'CodInfoDest',
'CriAForca', (*'ide_hSaiEnt', 'ide_dhCont',
'ide_xJust', 'emit_CRT', 'dest_email',
'Vagao', 'Balsa', 'CodInfoTrsp',
'OrdemServ', 'Situacao', 'Antigo',*)
'NFG_Serie', 'NF_ICMSAlq', 'NF_CFOP',
'Importado', 'NFG_SubSerie', 'NFG_ValIsen',
'NFG_NaoTrib', 'NFG_Outros', (*'COD_MOD',
'COD_SIT', 'VL_ABAT_NT',*) 'EFD_INN_AnoMes',
'EFD_INN_Empresa', 'EFD_INN_LinArq'], [
'FatID', 'FatNum', 'Empresa'], [
IDCtrl, (*LoteEnv, versao,*)
Id, (*ide_cUF, ide_cNF,*)
ide_natOp, ide_indPag, ide_mod,
ide_serie, ide_nNF, ide_dEmi,
ide_dSaiEnt, ide_tpNF, (*ide_cMunFG,
ide_tpImp, ide_tpEmis, ide_cDV,
ide_tpAmb, ide_finNFe, ide_procEmi,*)
ide_verProc, emit_CNPJ, emit_CPF,
(*emit_xNome, emit_xFant, emit_xLgr,
emit_nro, emit_xCpl, emit_xBairro,
emit_cMun, emit_xMun, emit_UF,
emit_CEP, emit_cPais, emit_xPais,
emit_fone,*) emit_IE, (*emit_IEST,
emit_IM, emit_CNAE,*) dest_CNPJ,
dest_CPF, (*dest_xNome, dest_xLgr,
dest_nro, dest_xCpl, dest_xBairro,
dest_cMun, dest_xMun, dest_UF,
dest_CEP, dest_cPais, dest_xPais,
dest_fone,*) dest_IE, (*dest_ISUF,*)
ICMSTot_vBC, ICMSTot_vICMS, (*ICMSTot_vBCST,
ICMSTot_vST,*) ICMSTot_vProd, ICMSTot_vFrete,
ICMSTot_vSeg, ICMSTot_vDesc, (*ICMSTot_vII,*)
ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
ICMSTot_vOutro, ICMSTot_vNF, (*ISSQNtot_vServ,
ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,
ISSQNtot_vCOFINS, RetTrib_vRetPIS, RetTrib_vRetCOFINS,
RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,
RetTrib_vBCRetPrev, RetTrib_vRetPrev, ModFrete,
Transporta_CNPJ, Transporta_CPF, Transporta_XNome,
Transporta_IE, Transporta_XEnder, Transporta_XMun,
Transporta_UF, RetTransp_vServ, RetTransp_vBCRet,
RetTransp_PICMSRet, RetTransp_vICMSRet, RetTransp_CFOP,
RetTransp_CMunFG, VeicTransp_Placa, VeicTransp_UF,
VeicTransp_RNTC, Cobr_Fat_nFat, Cobr_Fat_vOrig,
Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,
InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq,
Compra_XNEmp, Compra_XPed, Compra_XCont,*)
Status, (*infProt_Id, infProt_tpAmb,
infProt_verAplic, infProt_dhRecbto, infProt_nProt,
infProt_digVal, infProt_cStat, infProt_xMotivo,
infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,
infCanc_cStat, infCanc_xMotivo, infCanc_cJust,
infCanc_xJust, _Ativo_, FisRegCad,
CartEmiss, TabelaPrc, CondicaoPg,
FreteExtra, SegurExtra, ICMSRec_pRedBC,
ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
DataFiscal, (*protNFe_versao, retCancNFe_versao,
SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_ExpNat,
SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum,
SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais,
SINTEGRA_ExpAverDta,*) CodInfoEmit, CodInfoDest,
CriAForca, (*ide_hSaiEnt, ide_dhCont,
ide_xJust, emit_CRT, dest_email,
Vagao, Balsa, CodInfoTrsp,
OrdemServ, Situacao, Antigo,*)
NFG_Serie, NF_ICMSAlq, NF_CFOP,
Importado, NFG_SubSerie, NFG_ValIsen,
NFG_NaoTrib, NFG_Outros, (*COD_MOD,
COD_SIT, VL_ABAT_NT,*) EFD_INN_AnoMes,
EFD_INN_Empresa, EFD_INN_LinArq], [
FatID, FatNum, Empresa], True) then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'sintegr_50', False, [
    'Tabela', 'FatID', 'FatNum'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
    Tabela, FatID, FatNum], [
    ImporExpor, AnoMes, Empresa, LinArq], True);

{
    QrPQEIts_2_.Close;
    QrPQEIts_2_.Params[0].AsInteger := QrPQE_2_FatNum.Value;
    QrPQEIts_2_.Open;
    nItem := 0;
    while not QrPQEIts_2_.Eof do
    begin
      nItem        := nItem + 1;
      MeuID        := QrPQEIts_2_Controle.Value;
      Nivel1       := QrPQEIts_2_Insumo.Value;
      GraGruX      := DmProd.ObtemPrimeiroGraGruXdeGraGru1(Nivel1, FatID,
        FatNum, nItem, QrPQEIts_2_NOMEPQ.Value);
      prod_CFOP    := QrPQEIts_2_prod_CFOP.Value;
      prod_uTrib   := prod_uTrib;
      prod_qCom    := QrPQEIts_2_TotalPeso.Value;
      prod_qTrib   := QrPQEIts_2_TotalPeso.Value;
      if QrPQEIts_2_TotalPeso.Value > 0 then
        prod_vUnCom  := QrPQEIts_2_ValorItem.Value / QrPQEIts_2_TotalPeso.Value
      else prod_vUnCom  := 0;
      prod_vUnTrib := prod_vUnCom;
      prod_vProd   := QrPQEIts_2_ValorItem.Value;
      //
      DmProd.Obtem_prod_uTribdeGraGru1(Nivel1, prod_uCom, UnidMedCom);
      UnidMedTrib := UnidMedCom;
      //
      ObtemValorRecImpostos(QrSintegr_50ValorNF.Value, prod_vProd,
      QrSintegr_50BCICMS.Value, QrSintegr_50ValICMS.Value,
      QrSintegr_50ValIsento.Value, QrSintegr_50ValNCICMS.Value,
      QrSintegr_50AliqICMS.Value,
      ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
(*'prod_cProd', 'prod_cEAN', 'prod_xProd',
'prod_NCM', 'prod_EXTIPI', 'prod_genero',*)
'prod_CFOP', 'prod_uCom', 'prod_qCom',
'prod_vUnCom', 'prod_vProd', (*'prod_cEANTrib',*)
'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
(*'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
'Tem_IPI', '_Ativo_', 'InfAdCuztm',
'EhServico',*) 'ICMSRec_pRedBC', 'ICMSRec_vBC',
'ICMSRec_pAliq', 'ICMSRec_vICMS', (*'IPIRec_pRedBC',
'IPIRec_vBC', 'IPIRec_pAliq', 'IPIRec_vIPI',
'PISRec_pRedBC', 'PISRec_vBC', 'PISRec_pAliq',
'PISRec_vPIS', 'COFINSRec_pRedBC', 'COFINSRec_vBC',
'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)  'MeuID',
'Nivel1', (*'prod_vOutro', 'prod_indTot',
'prod_xPed', 'prod_nItemPed',*) 'GraGruX',
'UnidMedCom', 'UnidMedTrib'], [
'FatID', 'FatNum', 'Empresa', 'nItem'], [
(*prod_cProd, prod_cEAN, prod_xProd,
prod_NCM, prod_EXTIPI, prod_genero,*)
prod_CFOP, prod_uCom, prod_qCom,
prod_vUnCom, prod_vProd, (*prod_cEANTrib,*)
prod_uTrib, prod_qTrib, prod_vUnTrib,
(*prod_vFrete, prod_vSeg, prod_vDesc,
Tem_IPI, _Ativo_, InfAdCuztm,
EhServico,*) ICMSRec_pRedBC, ICMSRec_vBC,
ICMSRec_pAliq, ICMSRec_vICMS, (*IPIRec_pRedBC,
IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI,
PISRec_pRedBC, PISRec_vBC, PISRec_pAliq,
PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
COFINSRec_pAliq, COFINSRec_vCOFINS,*)MeuID,
Nivel1, (*prod_vOutro, prod_indTot,
prod_xPed, prod_nItemPed,*) GraGruX,
UnidMedCom, UnidMedTrib], [
FatID, FatNum, Empresa, nItem], True) then
      begin
        ICMS_Orig := QrPQEIts_2_ICMS_Orig.Value;
        ICMS_CST  := QrPQEIts_2_ICMS_Orig.Value;
        ICMS_modBC := QrPQEIts_2_ICMS_modBC.Value;
        ICMS_pRedBC := QrPQEIts_2_ICMS_pRedBC.Value;
        ICMS_vBC := QrPQEIts_2_ICMS_vBC.Value;
        ICMS_pICMS := QrPQEIts_2_ICMS_pICMS.Value;
        ICMS_vICMS := QrPQEIts_2_ICMS_vICMS.Value;
        ICMS_modBCST := QrPQEIts_2_ICMS_modBCST.Value;
        ICMS_pMVAST := QrPQEIts_2_ICMS_pMVAST.Value;
        ICMS_pRedBCST := QrPQEIts_2_ICMS_pRedBCST.Value;
        ICMS_vBCST := QrPQEIts_2_ICMS_vBCST.Value;
        ICMS_pICMSST := QrPQEIts_2_ICMS_pICMSST.Value;
        ICMS_vICMSST := QrPQEIts_2_ICMS_vICMSST.Value;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsn', False, [
'ICMS_Orig', 'ICMS_CST', 'ICMS_modBC',
'ICMS_pRedBC', 'ICMS_vBC', 'ICMS_pICMS',
'ICMS_vICMS', 'ICMS_modBCST', 'ICMS_pMVAST',
'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
'ICMS_vICMSST'(*, '_Ativo_', 'ICMS_CSOSN',
'ICMS_UFST', 'ICMS_pBCOp', 'ICMS_vBCSTRet',
'ICMS_vICMSSTRet', 'ICMS_motDesICMS', 'ICMS_pCredSN',
'ICMS_vCredICMSSN', 'COD_NAT'*)], [
'FatID', 'FatNum', 'Empresa', 'nItem'], [
ICMS_Orig, ICMS_CST, ICMS_modBC,
ICMS_pRedBC, ICMS_vBC, ICMS_pICMS,
ICMS_vICMS, ICMS_modBCST, ICMS_pMVAST,
ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
ICMS_vICMSST(*, _Ativo_, ICMS_CSOSN,
ICMS_UFST, ICMS_pBCOp, ICMS_vBCSTRet,
ICMS_vICMSSTRet, ICMS_motDesICMS, ICMS_pCredSN,
ICMS_vCredICMSSN, COD_NAT*)], [
FatID, FatNum, Empresa, nItem], True) then
        begin
          // Fazer IPI, PIS e COFINS?
        end;
      end;
      //
      QrPQEIts_2_.Next;
    end;
}
    //
  end;
//  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TUnSintegra.ExcluiPeriodoSintegra(ImporExpor, AnoMes,
  Empresa: Integer);
  procedure ExcluiItensTabela(Tabela: String);
  begin
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'DELETE FROM ' + Lowercase(Tabela),
    'WHERE ImporExpor=' + FormatFloat('0', ImporExpor),
    'AND AnoMes=' + FormatFloat('0', AnoMes),
    'AND Empresa=' + FormatFloat('0', Empresa)]);
  end;
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  //
  ExcluiItensTabela('SINTEGR_75');
  ExcluiItensTabela('SINTEGR_70');
  ExcluiItensTabela('SINTEGR_54');
  ExcluiItensTabela('SINTEGR_50');
  ExcluiItensTabela('SINTEGR_11');
  ExcluiItensTabela('SINTEGR_10');
  //
  Screen.Cursor := MyCursor;
end;

procedure TUnSintegra.InfoPosMeImportado(MeImportado: TMemo; GridS: TStringGrid;
              StatusBar: TStatusBar; MeImportado_SelLin, MeImportado_SelCol,
              Tam, PosI, PosF: Integer; var Fmt: Char; var Casas:
              Byte; var Obrigatorio: Boolean);
var
  L, C: Integer;
  MudouLinha: Boolean;
  Linha: String;
  //
  Registro, Campo: Integer;
  SubTipo: Char;
  DescriCampo, ConteudoCampo, ValorCampo: String;
begin
  MudouLinha := False;
  L := Geral.RichRow(MeImportado) + 1;
  C := Geral.RichCol(MeImportado) + 1;
  StatusBar.Panels[1].Text := Format('L: %3d   C: %3d', [L, C]);
  //
  if (L <> MeImportado_SelLin) then
  begin
    //MeImportado_SelLin := L;
    MudouLinha    := True;
  end;
  //
  if (C <> MeImportado_SelCol) then
  begin
    //MeImportado_SelCol := C;
  end;
  if MudouLinha then
  begin
    Linha := MeImportado.Lines[L - 1];
    //
    Registro := Geral.IMV(Copy(Linha, 1, 2));
    case Registro of
      60, 61: SubTipo := Linha[3];
      else SubTipo := #0;
    end;
    MyObjects.LimpaGrade(GridS, 1, 1, True);
    //
    Campo := 1;
    while Campo > 0 do
    begin
      USintegra.CoordenadasCampo(Registro, Campo, SubTipo, Tam, PosI, PosF, Fmt,
      Casas, Obrigatorio);
      if PosI > 0 then
      begin
        GridS.RowCount := Campo + 1;
        DescriCampo := USintegra.ObtemDescricaoDeCampo(Registro, PosI, SubTipo);
        ConteudoCampo := Copy(Linha, PosI, Tam);
        if Fmt = 'X' then
          ValorCampo := ''
        else
        if Fmt = 'N' then
        begin
          if Casas = 0 then
            ValorCampo := ''
          else
            ValorCampo := Geral.FFT(
              Geral.DMV(ConteudoCampo) / Power(10, Casas), Casas, siPositivo);
        end else
        if Fmt = 'D' then
        begin
          case Casas of
            0: ValorCampo := Copy(ConteudoCampo, 7, 2) + '/' +
                             Copy(ConteudoCampo, 5, 2) + '/' +
                             Copy(ConteudoCampo, 1, 4);
            1: ValorCampo := Copy(ConteudoCampo, 1, 2) + '/' +
                             Copy(ConteudoCampo, 3, 4);
          end;
        end else  Geral.MensagemBox('Formata��o n�o definida: "' + Fmt +
        '". Avise a Dermatek', 'Erro', MB_OK+MB_ICONERROR);
        //end;
        //

        GridS.Cells[00,Campo] := FormatFloat('00', Campo);
        GridS.Cells[01,Campo] := ConteudoCampo;
        GridS.Cells[02,Campo] := ValorCampo;
        GridS.Cells[03,Campo] := FormatFloat('000', Tam);
        GridS.Cells[04,Campo] := FormatFloat('000', PosI);
        GridS.Cells[05,Campo] := FormatFloat('000', PosF);
        GridS.Cells[06,Campo] := Fmt;
        GridS.Cells[07,Campo] := IntToStr(Casas);
        GridS.Cells[08,Campo] := DescriCampo;
        //
        Campo := Campo + 1;
      end else
        Campo := 0;
    end;
  end;
end;

procedure TUnSintegra.InfoPosMeImportado2(MeImportado: TMemo;
GridS: TStringGrid; Line: Integer;
var Tam, POsI, POsF: Integer; var Fmt: Char;
var Casas: Byte; var Obrigatorio: Boolean);
var
  Linha: String;
  //
  Registro, Campo: Integer;
  SubTipo: Char;
  DescriCampo, ConteudoCampo, ValorCampo: String;
begin
  Linha := MeImportado.Lines[Line - 1];
  //
  Registro := Geral.IMV(Copy(Linha, 1, 2));
  case Registro of
    60, 61: SubTipo := Linha[3];
    else SubTipo := #0;
  end;
  MyObjects.LimpaGrade(GridS, 1, 1, True);
  //
  Campo := 1;
  while Campo > 0 do
  begin
    CoordenadasCampo(Registro, Campo, SubTipo, Tam, PosI, PosF, Fmt,
    Casas, Obrigatorio);
    if PosI > 0 then
    begin
      GridS.RowCount := Campo + 1;
      DescriCampo := ObtemDescricaoDeCampo(Registro, PosI, SubTipo);
      ConteudoCampo := Copy(Linha, PosI, Tam);
      if Fmt = 'X' then
        ValorCampo := ''
      else
      if Fmt = 'N' then
      begin
        if Casas = 0 then
          ValorCampo := ''
        else
          ValorCampo := Geral.FFT(
            Geral.DMV(ConteudoCampo) / Power(10, Casas), Casas, siPositivo);
      end else
      if Fmt = 'D' then
      begin
        case Casas of
          0: ValorCampo := Copy(ConteudoCampo, 7, 2) + '/' +
                           Copy(ConteudoCampo, 5, 2) + '/' +
                           Copy(ConteudoCampo, 1, 4);
          1: ValorCampo := Copy(ConteudoCampo, 1, 2) + '/' +
                           Copy(ConteudoCampo, 3, 4);
        end;
      end else  Geral.MensagemBox('Formata��o n�o definida: "' + Fmt +
      '". Avise a Dermatek', 'Erro', MB_OK+MB_ICONERROR);
      //end;
      //

      GridS.Cells[00,Campo] := FormatFloat('00', Campo);
      GridS.Cells[01,Campo] := ConteudoCampo;
      GridS.Cells[02,Campo] := ValorCampo;
      GridS.Cells[03,Campo] := FormatFloat('000', Tam);
      GridS.Cells[04,Campo] := FormatFloat('000', PosI);
      GridS.Cells[05,Campo] := FormatFloat('000', PosF);
      GridS.Cells[06,Campo] := Fmt;
      GridS.Cells[07,Campo] := IntToStr(Casas);
      GridS.Cells[08,Campo] := DescriCampo;
      //
      Campo := Campo + 1;
    end else
      Campo := 0;
  end;
  GridS.Update;
  Application.ProcessMessages;
end;

function TUnSintegra.ObtemDescricaoDeCampo(nReg, PosI: Integer;
  SubTipo: Char): String;
begin
  Result := '?????';
  case nReg of
    10:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'CGC/MF do contribuinte';
      017: Result := 'Inscri��o Estadual do contribuinte';
      031: Result := 'Nome do contribuinte';
      066: Result := 'Munic�pio do contribuinte';
      096: Result := 'UF do contribuinte';
      098: Result := 'Fax do contribuinte';
      108: Result := 'Data inicial do per�odo';
      116: Result := 'Data final do per�odo';
      124: Result := 'C�digo da identifica��o do conv�nio';
      125: Result := 'C�digo da identifica��o da natureza das opera��es informadas';
      126: Result := 'C�digo da finalidade do arquivo magn�tico';
    end;
    11:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'Logradouro';
      037: Result := 'N�mero';
      042: Result := 'Complemento';
      064: Result := 'Bairro';
      079: Result := 'CEP';
      087: Result := 'Nome do Contato';
      115: Result := 'Telefone';
    end;
    50:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'CNPJ do remetente nas entradas e do destinat�rio nas sa�das';
      017: Result := 'Inscri��o Estadual do remetente nas entradas e do destinat�rio nas sa�das';
      031: Result := 'Data de emiss�o na sa�da ou de recebimento na entrada';
      039: Result := 'Sigla da unidade da Federa��o do remetente nas entradas e do destinat�rio nas sa�das ';
      041: Result := 'C�digo do modelo da nota fiscal';
      043: Result := 'S�rie da nota fiscal';
      046: Result := 'N�mero da nota fiscal';
      052: Result := 'C�digo Fiscal de Opera��o e Presta��o';
      056: Result := 'Emitente da Nota Fiscal (P-pr�prio/T-terceiros)';
      057: Result := 'Valor total da nota fiscal (com 2 decimais)';
      070: Result := 'Base de C�lculo do ICMS (com 2 decimais)';
      083: Result := 'Montante do imposto (com 2 decimais)';
      096: Result := 'Valor amparado por isen��o ou n�o incid�ncia (com 2 decimais)';
      109: Result := 'Valor que n�o confira d�bito ou cr�dito do ICMS (com 2 decimais)';
      122: Result := 'Al�quota do ICMS (com 2 decimais)';
      126: Result := 'Situa��o da Nota Fiscal';
    end;
    51:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'CNPJ do remetente nas entradas e do destinat�rio nas sa�das';
      017: Result := 'Inscri��o Estadual do remetente nas entradas e do destinat�rio nas sa�das';
      031: Result := 'Data de emiss�o na sa�da ou recebimento na entrada';
      039: Result := 'Sigla da unidade da Federa��o do remetente nas entradas e do destinat�rio nas sa�das';
      041: Result := 'S�rie da nota fiscal';
      044: Result := 'N�mero da nota fiscal';
      050: Result := 'C�digo Fiscal de Opera��o e Presta��o';
      054: Result := 'Valor total da nota fiscal (com 2 decimais)';
      067: Result := 'Montante do IPI (com 2 decimais)';
      080: Result := 'Valor amparado por isen��o ou n�o incid�ncia do IPI (com 2 decimais)';
      093: Result := 'Valor que n�o confira d�bito ou cr�dito do IPI (com 2 decimais)';
      106: Result := 'Brancos';
      126: Result := 'Situa��o da Nota Fiscal';
    end;
    53:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'CNPJ do contribuinte Substitu�do';
      017: Result := 'Inscri��o Estadual do Contribuinte substitu�do';
      031: Result := 'Data de emiss�o na sa�da ou recebimento na entrada';
      039: Result := 'Sigla da unidade da Federa��o do contribuinte substitu�do';
      041: Result := 'C�digo do modelo da nota fiscal';
      043: Result := 'S�rie da nota fiscal';
      046: Result := 'N�mero da nota fiscal';
      052: Result := 'C�digo Fiscal de Opera��o e Presta��o';
      056: Result := 'Emitente da Nota Fiscal (P-pr�prio/T-terceiros)';
      057: Result := 'Base de c�lculo de reten��o do ICMS (com 2 decimais)';
      070: Result := 'ICMS retido pelo substituto (com 2 decimais)';
      083: Result := 'Soma das despesas acess�rias (frete, seguro e outras - com 2 decimais)';
      096: Result := 'Situa��o da Nota Fiscal';
      097: Result := 'C�digo que identifica o tipo da Antecipa��o Tribut�ria';
      098: Result := 'Brancos';
    end;
    54:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'CNPJ do remetente nas entradas e do destinat�rio nas sa�das';
      017: Result := 'C�digo do modelo da nota fiscal';
      019: Result := 'S�rie da nota fiscal';
      022: Result := 'N�mero da nota fiscal';
      028: Result := 'C�digo Fiscal de Opera��o e Presta��o';
      032: Result := 'C�digo da Situa��o Tribut�ria';
      035: Result := 'N�mero de ordem do item na nota fiscal';
      038: Result := 'C�digo do produto ou servi�o do informante';
      052: Result := 'Quantidade do produto (com 3 decimais)';
      063: Result := 'Valor bruto do produto (valor unit�rio multiplicado por quantidade) - com 2 decimais';
      075: Result := 'Valor do Desconto Concedido no item (com 2 decimais).';
      087: Result := 'Base de c�lculo do ICMS (com 2 decimais)';
      099: Result := 'Base de c�lculo do ICMS de reten��o na Substitui��o Tribut�ria (com 2 decimais)';
      111: Result := 'Valor do IPI (com 2 decimais)';
      123: Result := 'Al�quota Utilizada no C�lculo do ICMS (com 2 decimais)';
    end;
    55:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'CNPJ do contribuinte Substituto tribut�rio';
      017: Result := 'Inscri��o Estadual na Unidade da Federa��o destinat�ria) do contribuinte substituto tribut�rio';
      031: Result := 'Data do pagamento do documento de Arrecada��o';
      039: Result := 'Sigla da unidade da Federa��o do contribuinte substituto tribut�rio';
      041: Result := 'Sigla da unidade da Federa��o de destino (favorecida)';
      043: Result := 'C�digo do Banco onde foi efetuado o recolhimento';
      046: Result := 'Ag�ncia onde foi efetuado o recolhimento';
      050: Result := 'N�mero de autentica��o banc�ria do documento de arrecada��o';
      070: Result := 'Valor recolhido (com 2 decimais)';
      083: Result := 'Data do vencimento do ICMS substitu�do';
      091: Result := 'M�s e ano referente � ocorr�ncia do fato gerador, formato MMAAAA';
      097: Result := 'Preencher com o conte�do do campo 15 da GNRE';
    end;
    56:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'CNPJ ou CPF do adquirente';
      017: Result := 'C�digo do modelo da nota fiscal';
      019: Result := 'S�rie da nota fiscal';
      022: Result := 'N�mero da nota fiscal';
      028: Result := 'C�digo Fiscal de Opera��o e Presta��o';
      032: Result := 'C�digo da Situa��o Tribut�ria';
      035: Result := 'N�mero de ordem do item na nota fiscal';
      038: Result := 'C�digo do produto ou servi�o do informante';
      052: Result := 'Tipo de opera��o: 1 - venda para concession�ria; 2- "Faturamento Direto" - Conv�nio ICMS 51/00; 3 - Venda direta; 0 � Outras';
      053: Result := 'CNPJ da concession�ria';
      067: Result := 'Al�quota do IPI (com 2 decimais)';
      071: Result := 'C�digo do Chassi do ve�culo';
      088: Result := 'Brancos';
    end;
    57:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'CNPJ do remetente nas entradas e do destinat�rio nas sa�das';
      017: Result := 'Inscri��o Estadual do Contribuinte';
      031: Result := 'C�digo do modelo da nota fiscal';
      033: Result := 'S�rie da nota fiscal';
      036: Result := 'N�mero da nota fiscal';
      042: Result := 'C�digo Fiscal de Opera��o e Presta��o';
      046: Result := 'C�digo da Situa��o Tribut�ria';
      049: Result := 'N�mero de ordem do item na nota fiscal';
      052: Result := 'C�digo do produto do informante';
      066: Result := 'N�mero do lote de fabrica��o do produto';
      086: Result := 'Brancos';
    end;
    60:
    begin
      case Ord(SubTipo) of
        Ord('M'):
        begin
          case PosI of
            001: Result := 'Tipo de registro';
            003: Result := '"M"';
            004: Result := 'Data de emiss�o dos documentos fiscais';
            012: Result := 'N�mero de s�rie de fabrica��o do equipamento';
            032: Result := 'N�mero atribu�do pelo estabelecimento ao equipamento';
            035: Result := 'C�digo do modelo do documento fiscal';
            037: Result := 'N�mero do primeiro documento fiscal emitido no dia (N�mero do Contador de Ordem de Opera��o - COO)';
            043: Result := 'N�mero do �ltimo documento fiscal emitido no dia (N�mero do Contador de Ordem de Opera��o - COO)';
            049: Result := 'N�mero do Contador de Redu��o Z (CRZ)';
            055: Result := 'Valor acumulado no Contador de Rein�cio de Opera��o (CRO)';
            058: Result := 'Valor acumulado no totalizador de Venda Bruta';
            074: Result := 'Valor acumulado no Totalizador Geral';
            090: Result := 'Brancos';
          end;
        end;
        Ord('A'):
        begin
          case PosI of
            001: Result := 'Tipo de registro';
            003: Result := '"A"';
            004: Result := 'Data de emiss�o dos documentos fiscais';
            012: Result := 'N�mero de s�rie de fabrica��o do equipamento';
            032: Result := 'Identificador da Situa��o Tribut�ria / Al�quota do ICMS';
            036: Result := 'Valor acumulado no final do dia no totalizador parcial da situa��o tribut�ria / al�quota indicada no campo 05 (com 2 decimais)';
            048: Result := 'Brancos';
          end;
        end;
        Ord('D'):
        begin
          case PosI of
            001: Result := 'Tipo de registro';
            003: Result := '"D"';
            004: Result := 'Data de emiss�o dos documentos fiscais';
            012: Result := 'N�mero de s�rie de fabrica��o do equipamento';
            032: Result := 'C�digo da mercadoria/produto ou servi�o do informante';
            046: Result := 'Quantidade comercializada da mercadoria/produto no dia (com 3 decimais)';
            059: Result := 'Valor l�quido (valor bruto diminu�do dos descontos) da mercadoria/produto acumulado no dia (com 2 decimais)';
            075: Result := 'Base de c�lculo do ICMS - valor acumulado no dia (com 2 decimais)';
            091: Result := 'Identificador da Situa��o Tribut�ria / Al�quota do ICMS (com 2 decimais)';
            095: Result := 'Montante do imposto';
            108: Result := 'Brancos';
          end;
        end;
        Ord('I'):
        begin
          case PosI of
            001: Result := 'Tipo de registro';
            003: Result := '"I"';
            004: Result := 'Data de emiss�o do documento fiscal';
            012: Result := 'N�mero de s�rie de fabrica��o do equipamento';
            032: Result := 'C�digo do modelo do documento fiscal';
            034: Result := 'N�mero do Contador de Ordem de Opera��o (COO)';
            040: Result := 'N�mero de Ordem do item no Documento Fiscal';
            043: Result := 'C�digo da mercadoria/produto ou servi�o do informante';
            057: Result := 'Quantidade da mercadoria/produto (com 3 decimais)';
            070: Result := 'Valor l�quido (valor bruto diminu�do do desconto) da mercadoria/produto (com 2 decimais)';
            083: Result := 'Base de C�lculo do ICMS do Item (com 2 decimais)';
            095: Result := 'Identificador da Situa��o Tribut�ria / Al�quota do ICMS (com 2 decimais)';
            099: Result := 'Montante do Imposto (2 decimais)';
            111: Result := 'Brancos';
          end;
        end;
        Ord('R'):
        begin
          case PosI of
            001: Result := 'Tipo de registro';
            003: Result := '"R"';
            004: Result := 'M�s e Ano de emiss�o dos documentos fiscais';
            010: Result := 'C�digo da mercadoria/produto ou servi�o do informante';
            024: Result := 'Quantidade da mercadoria/produto no m�s (com 3 decimais)';
            037: Result := 'Valor l�quido (valor bruto diminu�do do desconto) da mercadoria/produto ou servi�o acumulado no m�s (com 2 decimais)';
            053: Result := 'Base de c�lculo do ICMS - valor acumulado no m�s (com 2 decimais)';
            069: Result := 'Identificador da Situa��o Tribut�ria / Al�quota do ICMS (com 2 decimais)';
            073: Result := 'Brancos';
          end;
        end;
      end;
    end;
    61:
    begin
      case Ord(SubTipo) of
        Ord(' '):
        begin
          case PosI of
            001: Result := 'Tipo de registro';
            003: Result := 'Brancos';
            017: Result := 'Brancos';
            031: Result := 'Data de emiss�o do(s) documento(s) fiscal(is)';
            039: Result := 'Modelo do(s) documento(s) fiscal(is)';
            041: Result := 'S�rie do(s) documento(s) fiscal(is)';
            044: Result := 'Subs�rie do(s) documento(s) fiscal(is)';
            046: Result := 'N�mero do primeiro documento fiscal emitido no dia do mesmo modelo, s�rie e subs�rie';
            052: Result := 'N�mero do �ltimo documento fiscal emitido no dia do mesmo modelo, s�rie e subs�rie';
            058: Result := 'Valor total do(s) documento(s) fiscal(is)/Movimento di�rio (com 2 decimais)';
            071: Result := 'Base de c�lculo do(s) documento(s) fiscal(is)/Total di�rio (com 2 decimais)';
            084: Result := 'Valor do Montante do Imposto/Total di�rio (com 2 decimais)';
            096: Result := 'Valor amparado por isen��o ou n�o-incid�ncia/Total di�rio (com 2 decimais)';
            109: Result := 'Valor que n�o confira d�bito ou cr�dito de ICMS/Total di�rio (com 2 decimais)';
            122: Result := 'Al�quota do ICMS (com 2 decimais)';
            126: Result := 'Brancos';
          end;
        end;
      end;
      case Ord(SubTipo) of
        Ord('R'):
        begin
          case PosI of
            001: Result := 'Tipo de registro';
            003: Result := '"R"';
            004: Result := 'M�s e Ano de emiss�o dos documentos fiscais';
            010: Result := 'C�digo do produto do informante';
            024: Result := 'Quantidade do produto acumulada vendida no m�s (com 3 decimais)';
            037: Result := 'Valor bruto do produto - valor acumulado da venda do produto no m�s (com 2 decimais)';
            053: Result := 'Base de c�lculo do ICMS do valor acumulado no m�s (com 2 decimais)';
            069: Result := 'Al�quota do ICMS do produto';
            073: Result := 'Preencher posi��es com espa�os em branco';
          end;
        end;
      end;
    end;
    70:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'CNPJ do emitente do documento, no caso de aquisi��o de servi�o; CNPJ do tomador do servi�o, no caso de emiss�o do documento';
      017: Result := 'Inscri��o Estadual do emitente do documento, no caso de aquisi��o de servi�o; Inscri��o Estadual do tomador do servi�o, no caso de emiss�o do documento';
      031: Result := 'Data de emiss�o para o prestador, ou data de utiliza��o do servi�o para o tomador';
      039: Result := 'Sigla da unidade da Federa��o do emitente do documento, no caso de aquisi��o de servi�o, ou do tomador do servi�o, no caso de emiss�o do documento';
      041: Result := 'C�digo do modelo do documento fiscal';
      043: Result := 'S�rie do documento';
      044: Result := 'Subs�rie do documento';
      046: Result := 'N�mero do documento';
      052: Result := 'C�digo Fiscal de Opera��o e Presta��o - Um registro para cada CFOP do documento fiscal';
      056: Result := 'Valor total do documento fiscal (com 2 decimais)';
      069: Result := 'Base de c�lculo do ICMS (com duas decimais)';
      083: Result := 'Montante do imposto (com duas decimais)';
      097: Result := 'Valor amparado por isen��o ou n�o incid�ncia (com duas decimais)';
      111: Result := 'Valor que n�o confira d�bito ou cr�dito do ICMS (com duas decimais)';
      125: Result := 'Modalidade do frete - "1" - CIF, "2" - FOB ou "0" - OUTR0S (a op��o "0" - OUTROS nos casos em que n�o se aplica a informa��o de cl�usula CIF ou FOB)';
      126: Result := 'Situa��o do documento fiscal';
    end;
    71:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'CNPJ do tomador do servi�o';
      017: Result := 'Inscri��o estadual do tomador do servi�o';
      031: Result := 'Data de emiss�o do conhecimento';
      039: Result := 'Unidade da Federa��o do tomador do servi�o';
      041: Result := 'Modelo de Conhecimento';
      043: Result := 'S�rie do conhecimento';
      044: Result := 'Subs�rie do conhecimento';
      046: Result := 'N�mero do conhecimento';
      052: Result := 'Unidade da Federa��o do remetente, se o destinat�rio for o tomador ou unidade da Federa��o do destinat�rio, se o remetente for o tomador';
      054: Result := 'CNPJ do remetente, se o destinat�rio for o tomador ou CNPJ do destinat�rio, se o remetente for o tomador';
      068: Result := 'Inscri��o Estadual do remetente, se o destinat�rio for o tomador ou Inscri��o Estadual do destinat�rio, se o remetente for o tomador';
      082: Result := 'Data de emiss�o da nota fiscal que acoberta a carga transportada';
      090: Result := 'Modelo da nota fiscal que acoberta a carga transportada';
      092: Result := 'S�rie da nota fiscal que acoberta a carga transportada';
      095: Result := 'N�mero da nota fiscal que acoberta a carga transportada';
      101: Result := 'Valor total da nota fiscal que acoberta a carga transportada (com duas decimais)';
      115: Result := 'Brancos';
    end;
    74:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'Data do Invent�rio no formato AAAAMMDD';
      011: Result := 'C�digo do produto do informante';
      025: Result := 'Quantidade do produto (com 3 decimais)';
      038: Result := 'Valor bruto do produto (valor unit�rio multiplicado por quantidade) - com 2 decimais';
      051: Result := 'C�digo de Posse das Mercadorias Inventariadas, conforme tabela abaixo';
      052: Result := 'CNPJ do Possuidor da Mercadoria de propriedade do Informante, ou do propriet�rio da Mercadoria em poder do Informante';
      066: Result := 'Inscri��o Estadual do Possuidor da Mercadoria de propriedade do Informante, ou do propriet�rio da Mercadoria em poder do Informante';
      080: Result := 'Unidade da Federa��o do Possuidor da Mercadoria de propriedade do Informante, ou do propriet�rio da Mercadoria em poder do Informante';
      082: Result := 'Brancos';
    end;
    75:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'Data inicial do per�odo de validade das informa��es';
      011: Result := 'Data final do per�odo de validade das informa��es';
      019: Result := 'C�digo do produto ou servi�o utilizado pelo contribuinte';
      033: Result := 'Codifica��o da Nomenclatura Comum do Mercosul';
      041: Result := 'Descri��o do produto ou servi�o';
      094: Result := 'Unidade de medida de comercializa��o do produto ( un, kg, mt, m3, sc, frd, kWh, etc..)';
      100: Result := 'Al�quota do IPI do produto (com 2 decimais)';
      105: Result := 'Al�quota do ICMS aplic�vel a mercadoria ou servi�o nas opera��es ou presta��es internas ou naquelas que se tiverem iniciado no exterior (com 2 decimais)';
      109: Result := '% de Redu��o na base de c�lculo do ICMS, nas opera��es internas (com 2 decimais)';
      114: Result := 'Base de C�lculo do ICMS de substitui��o tribut�ria (com 2 decimais)';
    end;
    76:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'CNPJ/CPF do tomador do servi�o';
      017: Result := 'Inscri��o Estadual do do tomador do servi�o';
      031: Result := 'C�digo do modelo da nota fiscal';
      033: Result := 'S�rie da nota fiscal';
      035: Result := 'Subs�rie da nota fiscal';
      037: Result := 'N�mero da nota fiscal';
      047: Result := 'C�digo Fiscal de Opera��o e Presta��o';
      051: Result := 'C�digo da identifica��o do tipo de receita, conforme tabela abaixo';
      052: Result := 'Data de emiss�o na sa�da ou de Recebimento na entrada';
      060: Result := 'Sigla da Unidade da Federa��o do Remetente nas entradas e do destinat�rio nas sa�das';
      062: Result := 'Valor total da nota fiscal (com 2 decimais)';
      075: Result := 'Base de C�lculo do ICMS (com 2 decimais)';
      088: Result := 'Montante do imposto (com 2 decimais)';
      100: Result := 'Valor amparado por isen��o ou n�o-incid�ncia (com 2 decimais)';
      112: Result := 'Valor que n�o confira d�bito ou Cr�dito do ICMS (com 2 decimais)';
      124: Result := 'Al�quota do ICMS (valor inteiro)';
      126: Result := 'Situa��o da nota fiscal';
    end;
    77:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'CNPJ/CPF do tomador do servi�o';
      017: Result := 'C�digo do modelo da nota fiscal';
      019: Result := 'S�rie da nota fiscal';
      021: Result := 'Subs�rie da nota fiscal';
      023: Result := 'N�mero da nota fiscal';
      033: Result := 'C�digo Fiscal de Opera��o e Presta��o';
      037: Result := 'C�digo da identifica��o do tipo de receita, conforme tabela abaixo';
      038: Result := 'N�mero de ordem do item na nota fiscal';
      041: Result := 'C�digo do servi�o do informante';
      052: Result := 'Quantidade do servi�o (com 3 decimais)';
      065: Result := 'Valor bruto do servi�o (valor unit�rio multiplicado por Quantidade) - com 2 decimais';
      077: Result := 'Valor do Desconto Concedido no item (com 2 decimais).';
      089: Result := 'Base de c�lculo do ICMS (com 2 decimais)';
      101: Result := 'Al�quota Utilizada no C�lculo do ICMS (valor inteiro)';
      103: Result := 'CNPJ/MF da operadora de destino';
      117: Result := 'C�digo que designa o usu�rio final na rede do informante';
    end;
    85:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'N� da Declara��o de Exporta��o/ N� Declara��o Simplificada de  Exporta��o';
      014: Result := 'Data da Declara��o de Exporta��o (AAAAMMDD)';
      022: Result := 'Preencher com: "1" - Exporta��o Direta "2" - Exporta��o Indireta "3" - Exporta��o Direta- Regime Simplificado "4" - Exporta��o Indireta- Regime Simplificado';
      023: Result := 'N� do registro de Exporta��o';
      035: Result := 'Data do Registro de Exporta��o (AAAAMMDD)';
      043: Result := 'N� do conhecimento de embarque';
      059: Result := 'Data do conhecimento de embarque (AAAAMMDD)';
      067: Result := 'Informa��o do tipo de conhecimento de transporte (Preencher conforme tabela de tipo de documento de carga do SISCOMEX - anexa)';
      069: Result := 'C�digo do pa�s de destino da mercadoria (Preencher conforme tabela do SISCOMEX)';
      073: Result := 'Preencher com zeros';
      081: Result := 'Data da averba��o da Declara��o de exporta��o (AAAAMMDD)';
      089: Result := 'N�mero de Nota Fiscal de Exporta��o emitida pelo Exportador';
      095: Result := 'Data da emiss�o da NF de exporta��o / revenda (AAAAMMDD)';
      103: Result := 'C�digo do modelo da NF';
      105: Result := 'S�rie da Nota Fiscal';
      108: Result := 'Brancos';
    end;
    86:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'N� do registro de Exporta��o';
      015: Result := 'Data do Registro de Exporta��o (AAAAMMDD)';
      023: Result := 'CNPJ do contribuinte Produtor/Industrial/Fabricante que promoveu a remessa com fim espec�fico';
      037: Result := 'Inscri��o Estadual do contribuinte Produtor/Industrial/Fabricante que promoveu a remessa com fim espec�fico';
      051: Result := 'Unidade da Federa��o do Produtor/Industrial/Fabricante que promoveu remessa com fim espec�fico';
      053: Result := 'N� da Nota Fiscal de remessa com fim espec�fico de exporta��o recebida';
      059: Result := 'Data de emiss�o da Nota Fiscal da remessa com fim espec�fico (AAAAMMMDD)';
      067: Result := 'C�digo do modelo do documento fiscal';
      069: Result := 'S�rie da Nota Fiscal';
      072: Result := 'C�digo do produto adotado no registro tipo 75 quando do registro de entrada da Nota Fiscal de remessa com fim espec�fico';
      086: Result := 'Quantidade, efetivamente exportada, do produto declarado na Nota Fiscal de remessa com fim espec�fico recebida (com tr�s decimais)';
      097: Result := 'Valor unit�rio do produto (com duas decimais)';
      109: Result := 'Valor total do produto (valor unit�rio multiplicado pela quantidade) - com 2 decimais';
      121: Result := 'Preencher conforme tabela de c�digos de relacionamento entre Registro de Exporta��o e Nota Fiscal de remessa com fim espec�fico - Tabela A';
      122: Result := 'Brancos';
    end;
    90:
    case PosI of
      001: Result := 'Tipo de registro';
      003: Result := 'CGC/MF do informante';
      017: Result := 'Inscri��o Estadual do informante';
      031: Result := 'Tipo de registro que ser� totalizado pelo pr�ximo campo';
      033: Result := 'Total de registros do tipo informado no campo anterior';
      // ADAPTA��O
      041: Result := 'Tipo de registro que ser� totalizado pelo pr�ximo campo';
      043: Result := 'Total de registros do tipo informado no campo anterior';
      051: Result := 'Tipo de registro que ser� totalizado pelo pr�ximo campo';
      053: Result := 'Total de registros do tipo informado no campo anterior';
      061: Result := 'Tipo de registro que ser� totalizado pelo pr�ximo campo';
      063: Result := 'Total de registros do tipo informado no campo anterior';
      071: Result := 'Tipo de registro que ser� totalizado pelo pr�ximo campo';
      073: Result := 'Total de registros do tipo informado no campo anterior';
      081: Result := 'Tipo de registro que ser� totalizado pelo pr�ximo campo';
      083: Result := 'Total de registros do tipo informado no campo anterior';
      091: Result := 'Tipo de registro que ser� totalizado pelo pr�ximo campo';
      093: Result := 'Total de registros do tipo informado no campo anterior';
      101: Result := 'Tipo de registro que ser� totalizado pelo pr�ximo campo';
      103: Result := 'Total de registros do tipo informado no campo anterior';
      111: Result := 'Tipo de registro que ser� totalizado pelo pr�ximo campo';
      113: Result := 'Total de registros do tipo informado no campo anterior';
      121: Result := 'Brancos';
      // FIM ADAPTA��O
      126: Result := 'N�mero de registros tipo 90';
    end;
  end;
end;

function TUnSintegra.ObtemStatusDaSituacao(Situacao: String): Integer;
begin
  if Situacao = 'N' then
    Result := 100
  else
  if Situacao = 'S' then
    Result := 101
  else
  if Situacao = 'E' then
    Result := 100
  else
  if Situacao = 'X' then
    Result := 101
  else
  if Situacao = '2' then
    Result := 301
  else
  if Situacao = '4' then
    Result := 102
  else
    Result := 0
end;

end.
