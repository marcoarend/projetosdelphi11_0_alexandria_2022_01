object FmImportaSintegraNew: TFmImportaSintegraNew
  Left = 339
  Top = 185
  Caption = 'SIN-TEGRA-001 :: Novo Per'#237'odo SINTEGRA'
  ClientHeight = 628
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 273
        Height = 32
        Caption = 'Novo Per'#237'odo Sintegra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 273
        Height = 32
        Caption = 'Novo Per'#237'odo Sintegra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 273
        Height = 32
        Caption = 'Novo Per'#237'odo Sintegra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 558
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImporta: TBitBtn
        Left = 104
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Importa'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
      end
      object BtCarrega: TBitBtn
        Left = 12
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Carrega'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtCarregaClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 435
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 435
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 435
        Align = alClient
        TabOrder = 0
        object Panel13: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 178
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 1
            Top = 128
            Width = 778
            Height = 49
            Align = alBottom
            Caption = ' Dados da Empresa: '
            Enabled = False
            TabOrder = 1
            object Label11: TLabel
              Left = 8
              Top = 24
              Width = 30
              Height = 13
              Caption = 'CNPJ:'
            end
            object Label12: TLabel
              Left = 164
              Top = 24
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
            end
            object Label13: TLabel
              Left = 264
              Top = 24
              Width = 31
              Height = 13
              Caption = 'Nome:'
            end
            object EdEmp_CNPJ: TdmkEdit
              Left = 40
              Top = 20
              Width = 112
              Height = 21
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdEmp_CNPJChange
            end
            object EdEmp_Codigo: TdmkEdit
              Left = 204
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdEmp_Nome: TdmkEdit
              Left = 300
              Top = 20
              Width = 469
              Height = 21
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object GroupBox3: TGroupBox
            Left = 1
            Top = 1
            Width = 778
            Height = 127
            Align = alClient
            Caption = ' Arquivo magn'#233'tico a ser importado: '
            TabOrder = 0
            object SpeedButton7: TSpeedButton
              Left = 438
              Top = 32
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton7Click
            end
            object SpeedButton1: TSpeedButton
              Left = 458
              Top = 32
              Width = 21
              Height = 21
              Caption = '>'
              OnClick = SpeedButton1Click
            end
            object LaMes: TLabel
              Left = 484
              Top = 15
              Width = 23
              Height = 13
              Caption = 'M'#234's:'
            end
            object Label2: TLabel
              Left = 540
              Top = 16
              Width = 55
              Height = 13
              Caption = 'Data inicial:'
              Enabled = False
            end
            object Label3: TLabel
              Left = 656
              Top = 16
              Width = 48
              Height = 13
              Caption = 'Data final:'
              Enabled = False
            end
            object Label1: TLabel
              Left = 8
              Top = 16
              Width = 97
              Height = 13
              Caption = 'Caminho do arquivo:'
            end
            object Label4: TLabel
              Left = 8
              Top = 60
              Width = 23
              Height = 13
              Caption = 'Filial:'
            end
            object EdSINTEGRA_Path: TdmkEdit
              Left = 8
              Top = 32
              Width = 429
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'C:\_MLArend\Clientes\Ideal - MJ Novaes\2010 01\LFS00100110.TXT'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'C:\_MLArend\Clientes\Ideal - MJ Novaes\2010 01\LFS00100110.TXT'
              ValWarn = False
            end
            object TPDataIni: TdmkEditDateTimePicker
              Left = 540
              Top = 31
              Width = 113
              Height = 21
              Date = 0.499141331019927700
              Time = 0.499141331019927700
              Enabled = False
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              LastDayTimePicker = TPDataFim
            end
            object TPDataFim: TdmkEditDateTimePicker
              Left = 656
              Top = 31
              Width = 113
              Height = 21
              Date = 0.499141331019927700
              Time = 0.499141331019927700
              Enabled = False
              TabOrder = 2
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object EdEmpresa: TdmkEditCB
              Left = 36
              Top = 56
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEmpresa
              IgnoraDBLookupComboBox = False
            end
            object CBEmpresa: TdmkDBLookupComboBox
              Left = 92
              Top = 56
              Width = 677
              Height = 21
              KeyField = 'Filial'
              ListField = 'NOMEFILIAL'
              ListSource = DModG.DsEmpresas
              TabOrder = 4
              dmkEditCB = EdEmpresa
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdMes: TdmkEdit
              Left = 484
              Top = 31
              Width = 54
              Height = 21
              Alignment = taCenter
              TabOrder = 5
              FormatType = dmktfMesAno
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfLong
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = True
              PermiteNulo = False
              ValueVariant = Null
              ValWarn = False
              OnChange = EdMesChange
            end
            object RGEmitente: TRadioGroup
              Left = 8
              Top = 80
              Width = 761
              Height = 41
              Caption = ' Tipo de Emitente: '
              Columns = 4
              ItemIndex = 0
              Items.Strings = (
                'Nenhuma'
                'T - Emitidas por terceiros'
                'P - Emitidas pelo estabalecimento'
                'Todas NFs do arquivo')
              TabOrder = 6
            end
          end
        end
        object PageControl5: TPageControl
          Left = 2
          Top = 193
          Width = 780
          Height = 240
          ActivePage = TabSheet13
          Align = alClient
          TabOrder = 1
          object TabSheet12: TTabSheet
            Caption = ' Carregamento'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GridS: TStringGrid
              Left = 184
              Top = 0
              Width = 588
              Height = 212
              Align = alRight
              ColCount = 9
              DefaultColWidth = 20
              DefaultRowHeight = 18
              RowCount = 2
              TabOrder = 0
              OnDrawCell = GridSDrawCell
              ColWidths = (
                20
                105
                44
                20
                20
                20
                20
                20
                20)
            end
            object MeImportado: TMemo
              Left = 0
              Top = 0
              Width = 184
              Height = 212
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              ScrollBars = ssVertical
              TabOrder = 1
              WordWrap = False
              OnChange = MeImportadoChange
              OnKeyUp = MeImportadoKeyUp
              OnMouseDown = MeImportadoMouseDown
              OnMouseUp = MeImportadoMouseUp
            end
          end
          object TabSheet13: TTabSheet
            Caption = ' Ignorados no carregamento '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object MeIgnor: TMemo
              Left = 0
              Top = 0
              Width = 772
              Height = 212
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
          object TabSheet14: TTabSheet
            Caption = ' Erros no carregamento '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 249
            object MeErros: TMemo
              Left = 0
              Top = 0
              Width = 772
              Height = 248
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              ExplicitHeight = 249
            end
          end
          object TabSheet15: TTabSheet
            Caption = ' N'#227'o importados '
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 249
            object MeNaoImport: TMemo
              Left = 0
              Top = 0
              Width = 772
              Height = 248
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              ExplicitHeight = 249
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 483
    Width = 784
    Height = 75
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 58
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object StatusBar: TStatusBar
        Left = 0
        Top = 39
        Width = 780
        Height = 19
        Panels = <
          item
            Text = ' Posi'#231#227'o do cursor no texto gerado:'
            Width = 192
          end
          item
            Width = 100
          end
          item
            Text = ' Arquivo salvo:'
            Width = 96
          end
          item
            Width = 50
          end>
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 1
      end
    end
  end
  object QrLocPrdNom: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_CodUsu, ggx.GraGru1, ggx.Controle GraGruX, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.PrdGrupTip'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE gg1.Nome = :P0')
    Left = 748
    Top = 508
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPrdNomGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocPrdNomGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
  end
end
