unit ImportaSintegraGer;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Menus, ComCtrls, Grids,
  DBGrids, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, Variants,
  dmkDBGrid, frxClass, frxDBSet, dmkCheckGroup, DmkDAC_PF, dmkImage, UnDmkEnums,
  UnGrade_Jan;

type
  TFmImportaSintegraGer = class(TForm)
    PainelDados: TPanel;
    DsSINTEGR_10: TDataSource;
    QrSINTEGR_10: TmySQLQuery;
    dmkPermissoes1: TdmkPermissoes;
    PMPeriodo: TPopupMenu;
    QrSINTEGR_50: TmySQLQuery;
    DsSINTEGR_50: TDataSource;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    PMExclui: TPopupMenu;
    Panel7: TPanel;
    Itemns1: TMenuItem;
    Periodoselecionado1: TMenuItem;
    ItemSelecionado2: TMenuItem;
    QrSEI: TmySQLQuery;
    QrSEINivel1: TIntegerField;
    QrSEINO_PRD: TWideStringField;
    QrSEIPrdGrupTip: TIntegerField;
    QrSEIUnidMed: TIntegerField;
    QrSEINO_PGT: TWideStringField;
    QrSEISIGLA: TWideStringField;
    QrSEINO_TAM: TWideStringField;
    QrSEINO_COR: TWideStringField;
    QrSEIGraCorCad: TIntegerField;
    QrSEIGraGruC: TIntegerField;
    QrSEIGraGru1: TIntegerField;
    QrSEIGraTamI: TIntegerField;
    QrSEIControle: TIntegerField;
    QrSEIImporExpor: TSmallintField;
    QrSEIAnoMes: TIntegerField;
    QrSEIEmpresa: TIntegerField;
    QrSEIGraGruX: TIntegerField;
    QrSEISit_Prod: TSmallintField;
    QrSEITerceiro: TIntegerField;
    QrSEIEstqIniQtd: TFloatField;
    QrSEIEstqIniPrc: TFloatField;
    QrSEIEstqIniVal: TFloatField;
    QrSEIComprasQtd: TFloatField;
    QrSEIComprasPrc: TFloatField;
    QrSEIComprasVal: TFloatField;
    QrSEIConsumoQtd: TFloatField;
    QrSEIConsumoPrc: TFloatField;
    QrSEIConsumoVal: TFloatField;
    QrSEIEstqFimQtd: TFloatField;
    QrSEIEstqFimPrc: TFloatField;
    QrSEIEstqFimVal: TFloatField;
    QrSEINO_ENT: TWideStringField;
    QrSEICNPJ_CPF: TWideStringField;
    QrSEIIE: TWideStringField;
    QrSEIEUF: TSmallintField;
    QrSEIPUF: TSmallintField;
    QrSEITipo: TSmallintField;
    QrSEIPrintTam: TSmallintField;
    QrSEIPrintCor: TSmallintField;
    QrSEINCM: TWideStringField;
    Panel10: TPanel;
    DBGCab: TdmkDBGrid;
    GroupBox1: TGroupBox;
    CGAgrup0: TdmkCheckGroup;
    frxDsSPEDEstqIts: TfrxDBDataset;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel12: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    DBEdit1: TDBEdit;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtNF: TBitBtn;
    BtPeriodo: TBitBtn;
    Panel2: TPanel;
    BtSaida0: TBitBtn;
    BtSeqAcoes: TBitBtn;
    QrSINTEGR_10NO_ENT: TWideStringField;
    QrSINTEGR_10MES_ANO: TWideStringField;
    QrSINTEGR_10ImporExpor: TSmallintField;
    QrSINTEGR_10AnoMes: TIntegerField;
    QrSINTEGR_10Empresa: TIntegerField;
    QrSINTEGR_10TIPO: TWideStringField;
    QrSINTEGR_10CNPJ: TWideStringField;
    QrSINTEGR_10INSCEST: TWideStringField;
    QrSINTEGR_10NCONTRIB: TWideStringField;
    QrSINTEGR_10MUNICIPIO: TWideStringField;
    QrSINTEGR_10UF: TWideStringField;
    QrSINTEGR_10FAX: TWideStringField;
    QrSINTEGR_10DATAINI: TDateField;
    QrSINTEGR_10DATAFIN: TDateField;
    QrSINTEGR_10CODCONV: TWideStringField;
    QrSINTEGR_10CODNAT: TWideStringField;
    QrSINTEGR_10CODFIN: TWideStringField;
    QrSINTEGR_50ImporExpor: TSmallintField;
    QrSINTEGR_50AnoMes: TIntegerField;
    QrSINTEGR_50Empresa: TIntegerField;
    QrSINTEGR_50LinArq: TIntegerField;
    QrSINTEGR_50TIPO: TSmallintField;
    QrSINTEGR_50CNPJ: TWideStringField;
    QrSINTEGR_50IE: TWideStringField;
    QrSINTEGR_50EMISSAO: TDateField;
    QrSINTEGR_50UF: TWideStringField;
    QrSINTEGR_50MODELO: TWideStringField;
    QrSINTEGR_50SERIE: TWideStringField;
    QrSINTEGR_50NUMNF: TWideStringField;
    QrSINTEGR_50CFOP: TWideStringField;
    QrSINTEGR_50EMITENTE: TWideStringField;
    QrSINTEGR_50ValorNF: TFloatField;
    QrSINTEGR_50BCICMS: TFloatField;
    QrSINTEGR_50ValICMS: TFloatField;
    QrSINTEGR_50ValIsento: TFloatField;
    QrSINTEGR_50ValNCICMS: TFloatField;
    QrSINTEGR_50AliqICMS: TFloatField;
    QrSINTEGR_50Terceiro: TIntegerField;
    QrSINTEGR_50Tabela: TSmallintField;
    QrSINTEGR_50FatID: TIntegerField;
    QrSINTEGR_50FatNum: TIntegerField;
    QrLocPQE: TmySQLQuery;
    QrLocPQECodigo: TIntegerField;
    QrPQE_1_: TmySQLQuery;
    DsPQE_1_: TDataSource;
    QrLocMPc: TmySQLQuery;
    QrLocMPcConta: TIntegerField;
    QrLocEMP: TmySQLQuery;
    QrLocEMPCodigo: TIntegerField;
    QrSINTEGR_50ACHOU_TXT: TWideStringField;
    QrLocPQEValorNF: TFloatField;
    PMSeqAcoes0: TPopupMenu;
    N50T_SeqAcoes_1: TMenuItem;
    N50T_SeqAcoes_2: TMenuItem;
    QrExclUni: TmySQLQuery;
    QrExclUniFatID: TIntegerField;
    QrExclUniFatNum: TIntegerField;
    QrExclUniStatus: TIntegerField;
    QrExclUniEmpresa: TIntegerField;
    QrExclMul: TmySQLQuery;
    QrExclMulFatID: TIntegerField;
    QrExclMulFatNum: TIntegerField;
    QrExclMulEmpresa: TIntegerField;
    N50T_SeqAcoes_3: TMenuItem;
    PB1: TProgressBar;
    RGEmitente: TRadioGroup;
    QrSINTEGR_50ConfVal: TSmallintField;
    QrExclMulinfProt_cStat: TIntegerField;
    QrSumPQEIts: TmySQLQuery;
    QrPQE_1_FatNum: TIntegerField;
    QrPQE_1_dtEmi: TDateField;
    QrPQE_1_dtSaiEnt: TDateField;
    QrPQE_1_DataFiscal: TDateField;
    QrPQE_1_ValTot: TFloatField;
    QrPQE_1_Empresa: TIntegerField;
    QrPQE_1_CodInfoDest: TIntegerField;
    QrPQE_1_CodInfoEmit: TIntegerField;
    QrPQE_1_Id: TWideStringField;
    QrPQE_1_ide_mod: TSmallintField;
    QrPQE_1_Serie: TIntegerField;
    QrPQE_1_nNF: TIntegerField;
    QrPQE_1_Frete: TFloatField;
    QrPQE_1_Seguro: TFloatField;
    QrPQE_1_Desconto: TFloatField;
    QrPQE_1_IPI: TFloatField;
    QrPQE_1_PIS: TFloatField;
    QrPQE_1_COFINS: TFloatField;
    QrPQE_1_Outros: TFloatField;
    QrPQEIts_2_: TmySQLQuery;
    QrPQEIts_2_CUSTOITEM: TFloatField;
    QrPQEIts_2_VALORKG: TFloatField;
    QrPQEIts_2_TOTALKGBRUTO: TFloatField;
    QrPQEIts_2_CUSTOKG: TFloatField;
    QrPQEIts_2_PRECOKG: TFloatField;
    QrPQEIts_2_Codigo: TIntegerField;
    QrPQEIts_2_Controle: TIntegerField;
    QrPQEIts_2_Conta: TIntegerField;
    QrPQEIts_2_Volumes: TIntegerField;
    QrPQEIts_2_PesoVB: TFloatField;
    QrPQEIts_2_PesoVL: TFloatField;
    QrPQEIts_2_ValorItem: TFloatField;
    QrPQEIts_2_IPI: TFloatField;
    QrPQEIts_2_RIPI: TFloatField;
    QrPQEIts_2_CFin: TFloatField;
    QrPQEIts_2_ICMS: TFloatField;
    QrPQEIts_2_RICMS: TFloatField;
    QrPQEIts_2_TotalCusto: TFloatField;
    QrPQEIts_2_Insumo: TIntegerField;
    QrPQEIts_2_TotalPeso: TFloatField;
    QrPQEIts_2_NOMEPQ: TWideStringField;
    QrPQEIts_2_prod_cProd: TWideStringField;
    QrPQEIts_2_prod_cEAN: TWideStringField;
    QrPQEIts_2_prod_xProd: TWideStringField;
    QrPQEIts_2_prod_NCM: TWideStringField;
    QrPQEIts_2_prod_EX_TIPI: TWideStringField;
    QrPQEIts_2_prod_genero: TSmallintField;
    QrPQEIts_2_prod_CFOP: TWideStringField;
    QrPQEIts_2_prod_uCom: TWideStringField;
    QrPQEIts_2_prod_qCom: TFloatField;
    QrPQEIts_2_prod_vUnCom: TFloatField;
    QrPQEIts_2_prod_vProd: TFloatField;
    QrPQEIts_2_prod_cEANTrib: TWideStringField;
    QrPQEIts_2_prod_uTrib: TWideStringField;
    QrPQEIts_2_prod_qTrib: TFloatField;
    QrPQEIts_2_prod_vUnTrib: TFloatField;
    QrPQEIts_2_prod_vFrete: TFloatField;
    QrPQEIts_2_prod_vSeg: TFloatField;
    QrPQEIts_2_prod_vDesc: TFloatField;
    QrPQEIts_2_ICMS_Orig: TSmallintField;
    QrPQEIts_2_ICMS_CST: TSmallintField;
    QrPQEIts_2_ICMS_modBC: TSmallintField;
    QrPQEIts_2_ICMS_pRedBC: TFloatField;
    QrPQEIts_2_ICMS_vBC: TFloatField;
    QrPQEIts_2_ICMS_pICMS: TFloatField;
    QrPQEIts_2_ICMS_vICMS: TFloatField;
    QrPQEIts_2_ICMS_modBCST: TSmallintField;
    QrPQEIts_2_ICMS_pMVAST: TFloatField;
    QrPQEIts_2_ICMS_pRedBCST: TFloatField;
    QrPQEIts_2_ICMS_vBCST: TFloatField;
    QrPQEIts_2_ICMS_pICMSST: TFloatField;
    QrPQEIts_2_ICMS_vICMSST: TFloatField;
    QrPQEIts_2_IPI_cEnq: TWideStringField;
    QrPQEIts_2_IPI_CST: TSmallintField;
    QrPQEIts_2_IPI_vBC: TFloatField;
    QrPQEIts_2_IPI_qUnid: TFloatField;
    QrPQEIts_2_IPI_vUnid: TFloatField;
    QrPQEIts_2_IPI_pIPI: TFloatField;
    QrPQEIts_2_IPI_vIPI: TFloatField;
    QrPQEIts_2_PIS_CST: TSmallintField;
    QrPQEIts_2_PIS_vBC: TFloatField;
    QrPQEIts_2_PIS_pPIS: TFloatField;
    QrPQEIts_2_PIS_vPIS: TFloatField;
    QrPQEIts_2_PIS_qBCProd: TFloatField;
    QrPQEIts_2_PIS_vAliqProd: TFloatField;
    QrPQEIts_2_PISST_vBC: TFloatField;
    QrPQEIts_2_PISST_pPIS: TFloatField;
    QrPQEIts_2_PISST_qBCProd: TFloatField;
    QrPQEIts_2_PISST_vAliqProd: TFloatField;
    QrPQEIts_2_PISST_vPIS: TFloatField;
    QrPQEIts_2_COFINS_CST: TSmallintField;
    QrPQEIts_2_COFINS_vBC: TFloatField;
    QrPQEIts_2_COFINS_pCOFINS: TFloatField;
    QrPQEIts_2_COFINS_qBCProd: TFloatField;
    QrPQEIts_2_COFINS_vAliqProd: TFloatField;
    QrPQEIts_2_COFINS_vCOFINS: TFloatField;
    QrPQEIts_2_COFINSST_vBC: TFloatField;
    QrPQEIts_2_COFINSST_pCOFINS: TFloatField;
    QrPQEIts_2_COFINSST_qBCProd: TFloatField;
    QrPQEIts_2_COFINSST_vAliqProd: TFloatField;
    QrPQEIts_2_COFINSST_vCOFINS: TFloatField;
    QrSumPQEItsValorItem: TFloatField;
    QrPQE_2_: TmySQLQuery;
    QrPQE_2_FatNum: TIntegerField;
    QrPQE_2_dtEmi: TDateField;
    QrPQE_2_dtSaiEnt: TDateField;
    QrPQE_2_DataFiscal: TDateField;
    QrPQE_2_ValTot: TFloatField;
    QrPQE_2_Empresa: TIntegerField;
    QrPQE_2_CodInfoDest: TIntegerField;
    QrPQE_2_CodInfoEmit: TIntegerField;
    QrPQE_2_Id: TWideStringField;
    QrPQE_2_ide_mod: TSmallintField;
    QrPQE_2_Serie: TIntegerField;
    QrPQE_2_nNF: TIntegerField;
    QrPQE_2_Frete: TFloatField;
    QrPQE_2_Seguro: TFloatField;
    QrPQE_2_Desconto: TFloatField;
    QrPQE_2_IPI: TFloatField;
    QrPQE_2_PIS: TFloatField;
    QrPQE_2_COFINS: TFloatField;
    QrPQE_2_Outros: TFloatField;
    QrNFeCabA: TmySQLQuery;
    DsNFeCabA: TDataSource;
    QrNFeCabAide_nNF: TIntegerField;
    QrNFeCabAide_dEmi: TDateField;
    QrNFeCabAide_dSaiEnt: TDateField;
    QrNFeCabAICMSTot_vNF: TFloatField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAEmpresa: TIntegerField;
    QrNFEItsI: TmySQLQuery;
    DsNFEItsI: TDataSource;
    QrNFEItsINO_GG1: TWideStringField;
    QrNFEItsIFatID: TIntegerField;
    QrNFEItsIFatNum: TIntegerField;
    QrNFEItsIEmpresa: TIntegerField;
    QrNFEItsInItem: TIntegerField;
    QrNFEItsIprod_cProd: TWideStringField;
    QrNFEItsIprod_cEAN: TWideStringField;
    QrNFEItsIprod_xProd: TWideStringField;
    QrNFEItsIprod_NCM: TWideStringField;
    QrNFEItsIprod_EXTIPI: TWideStringField;
    QrNFEItsIprod_genero: TSmallintField;
    QrNFEItsIprod_CFOP: TIntegerField;
    QrNFEItsIprod_uCom: TWideStringField;
    QrNFEItsIprod_qCom: TFloatField;
    QrNFEItsIprod_vUnCom: TFloatField;
    QrNFEItsIprod_vProd: TFloatField;
    QrNFEItsIprod_cEANTrib: TWideStringField;
    QrNFEItsIprod_uTrib: TWideStringField;
    QrNFEItsIprod_qTrib: TFloatField;
    QrNFEItsIprod_vUnTrib: TFloatField;
    QrNFEItsIprod_vFrete: TFloatField;
    QrNFEItsIprod_vSeg: TFloatField;
    QrNFEItsIprod_vDesc: TFloatField;
    QrNFEItsITem_IPI: TSmallintField;
    QrNFEItsI_Ativo_: TSmallintField;
    QrNFEItsIInfAdCuztm: TIntegerField;
    QrNFEItsIEhServico: TIntegerField;
    QrNFEItsILk: TIntegerField;
    QrNFEItsIDataCad: TDateField;
    QrNFEItsIDataAlt: TDateField;
    QrNFEItsIUserCad: TIntegerField;
    QrNFEItsIUserAlt: TIntegerField;
    QrNFEItsIAlterWeb: TSmallintField;
    QrNFEItsIAtivo: TSmallintField;
    QrNFEItsIICMSRec_pRedBC: TFloatField;
    QrNFEItsIICMSRec_vBC: TFloatField;
    QrNFEItsIICMSRec_pAliq: TFloatField;
    QrNFEItsIICMSRec_vICMS: TFloatField;
    QrNFEItsIIPIRec_pRedBC: TFloatField;
    QrNFEItsIIPIRec_vBC: TFloatField;
    QrNFEItsIIPIRec_pAliq: TFloatField;
    QrNFEItsIIPIRec_vIPI: TFloatField;
    QrNFEItsIPISRec_pRedBC: TFloatField;
    QrNFEItsIPISRec_vBC: TFloatField;
    QrNFEItsIPISRec_pAliq: TFloatField;
    QrNFEItsIPISRec_vPIS: TFloatField;
    QrNFEItsICOFINSRec_pRedBC: TFloatField;
    QrNFEItsICOFINSRec_vBC: TFloatField;
    QrNFEItsICOFINSRec_pAliq: TFloatField;
    QrNFEItsICOFINSRec_vCOFINS: TFloatField;
    QrNFEItsIMeuID: TIntegerField;
    QrNFEItsINivel1: TIntegerField;
    QrNFEItsIprod_vOutro: TFloatField;
    QrNFEItsIprod_indTot: TSmallintField;
    QrNFEItsIprod_xPed: TWideStringField;
    QrNFEItsIprod_nItemPed: TIntegerField;
    QrNFEItsIGraGruX: TIntegerField;
    QrNFEItsIUnidMedCom: TIntegerField;
    QrNFEItsIUnidMedTrib: TIntegerField;
    PMNF: TPopupMenu;
    IncluiNF1: TMenuItem;
    AlteraNF1: TMenuItem;
    QrSINTEGR_50Situacao: TWideStringField;
    QrSINTEGR_54: TmySQLQuery;
    QrSINTEGR_54ImporExpor: TSmallintField;
    QrSINTEGR_54AnoMes: TIntegerField;
    QrSINTEGR_54Empresa: TIntegerField;
    QrSINTEGR_54LinArq: TIntegerField;
    QrSINTEGR_54TIPO: TWideStringField;
    QrSINTEGR_54CNPJ: TWideStringField;
    QrSINTEGR_54MODELO: TWideStringField;
    QrSINTEGR_54SERIE: TWideStringField;
    QrSINTEGR_54NUMNF: TWideStringField;
    QrSINTEGR_54CFOP: TWideStringField;
    QrSINTEGR_54CST: TWideStringField;
    QrSINTEGR_54NUMITEM: TWideStringField;
    QrSINTEGR_54CODPROD: TWideStringField;
    QrSINTEGR_54QTDADE: TFloatField;
    QrSINTEGR_54VLRPROD: TFloatField;
    QrSINTEGR_54VLRDESC: TFloatField;
    QrSINTEGR_54BASECALC: TFloatField;
    QrSINTEGR_54BASESUBTRI: TFloatField;
    QrSINTEGR_54VLRIPI: TFloatField;
    QrSINTEGR_54ALIQICMS: TFloatField;
    DsSINTEGR_54: TDataSource;
    Incluinovoperiodo1: TMenuItem;
    Excluiperiodoselecionado1: TMenuItem;
    QrNFeCabAIDCtrl: TIntegerField;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Panel9: TPanel;
    PageControl4: TPageControl;
    TabSheet4: TTabSheet;
    DBGAtrelado: TDBGrid;
    Panel13: TPanel;
    Panel14: TPanel;
    DBGrid1: TDBGrid;
    TabSheet5: TTabSheet;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    Splitter1: TSplitter;
    Label4: TLabel;
    DBG_s50: TdmkDBGrid;
    dmkDBGrid1: TdmkDBGrid;
    Panel16: TPanel;
    Panel15: TPanel;
    DBGrid4: TDBGrid;
    TabSheet7: TTabSheet;
    Panel17: TPanel;
    dmkDBGrid2: TdmkDBGrid;
    QrSINTEGR_70: TmySQLQuery;
    DsSINTEGR_70: TDataSource;
    QrSINTEGR_70ImporExpor: TSmallintField;
    QrSINTEGR_70AnoMes: TIntegerField;
    QrSINTEGR_70Empresa: TIntegerField;
    QrSINTEGR_70LinArq: TIntegerField;
    QrSINTEGR_70TIPO: TWideStringField;
    QrSINTEGR_70CNPJ: TWideStringField;
    QrSINTEGR_70INSCEST: TWideStringField;
    QrSINTEGR_70EMISSAO: TDateField;
    QrSINTEGR_70UF: TWideStringField;
    QrSINTEGR_70MODELO: TWideStringField;
    QrSINTEGR_70SERIE: TWideStringField;
    QrSINTEGR_70SUBSERIE: TWideStringField;
    QrSINTEGR_70NUMNF: TWideStringField;
    QrSINTEGR_70CFOP: TWideStringField;
    QrSINTEGR_70VALORTOT: TFloatField;
    QrSINTEGR_70BASEICMS: TFloatField;
    QrSINTEGR_70VALORICMS: TFloatField;
    QrSINTEGR_70VLRISENTO: TFloatField;
    QrSINTEGR_70OUTROS: TFloatField;
    QrSINTEGR_70CIF_FOB: TSmallintField;
    QrSINTEGR_70SITUACAO: TWideStringField;
    QrSINTEGR_70Transporta: TIntegerField;
    PMSeqAcoes1: TPopupMenu;
    N70T_SeqAcoes_2: TMenuItem;
    N70T_SeqAcoes_1: TMenuItem;
    QrSINTEGR_10Stat50T: TSmallintField;
    QrSINTEGR_10Stat50P: TSmallintField;
    QrSINTEGR_10Stat70T: TSmallintField;
    N50T_SeqAcoes_4: TMenuItem;
    procedure BtNFClick(Sender: TObject);
    procedure BtSaida0Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPeriodoClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrSINTEGR_10BeforeClose(DataSet: TDataSet);
    procedure QrSINTEGR_10AfterScroll(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure QrSINTEGR_10AfterOpen(DataSet: TDataSet);
    procedure Itemns1Click(Sender: TObject);
    procedure PMPeriodoPopup(Sender: TObject);
    procedure ItemSelecionado2Click(Sender: TObject);
    procedure QrSINTEGR_50BeforeClose(DataSet: TDataSet);
    procedure frxSPEDEFD_PRINT_000_01GetValue(const VarName: string;
      var Value: Variant);
    procedure BtSeqAcoesClick(Sender: TObject);
    procedure QrSINTEGR_50AfterScroll(DataSet: TDataSet);
    procedure QrSINTEGR_50CalcFields(DataSet: TDataSet);
    procedure DBG_s50DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure N50T_SeqAcoes_1Click(Sender: TObject);
    procedure PMSeqAcoes0Popup(Sender: TObject);
    procedure N50T_SeqAcoes_3Click(Sender: TObject);
    procedure N50T_SeqAcoes_2Click(Sender: TObject);
    procedure RGEmitenteClick(Sender: TObject);
    procedure QrNFeCabABeforeClose(DataSet: TDataSet);
    procedure QrNFeCabAAfterScroll(DataSet: TDataSet);
    procedure QrSINTEGR_50AfterOpen(DataSet: TDataSet);
    procedure PMNFPopup(Sender: TObject);
    procedure IncluiNF1Click(Sender: TObject);
    procedure AlteraNF1Click(Sender: TObject);
    procedure Incluinovoperiodo1Click(Sender: TObject);
    procedure Excluiperiodoselecionado1Click(Sender: TObject);
    procedure N70T_SeqAcoes_1Click(Sender: TObject);
    procedure N70T_SeqAcoes_2Click(Sender: TObject);
    procedure N50T_SeqAcoes_4Click(Sender: TObject);
    procedure PMSeqAcoes1Popup(Sender: TObject);
  private
    { Private declarations }
    //FExportarItem: Boolean;
    {
    function VeriStr(Tam, Item: Integer; Texto: String): String;
    }
    function DefineDataFim(): TDateTime;
    function PertenceAoGrupoGG2(): Boolean;
    function PertenceAoGrupoPGT(): Boolean;
    function PertenceAoGrupoPRD(): Boolean;
    //
    procedure CriaNFeCabA_151(Codigo: Integer);
    procedure ReopenAtrelado();
    procedure DefineDatasMinEMax(const MesesAMenos, MesesAMais: Integer; var
              DataMin, DataMax: String);
    procedure IncrementaStatus(Campo: String;
              Status, ImporExpor, AnoMes, Empresa: Integer);
    procedure ObtemValorRecImpostos(const ValItsNF,
              BCICMS, ValICMS, ValIsento, ValNCICMS, AliqICMS: Double;
              var ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS:
              Double);

  public
    { Public declarations }
    FEmpresa_Int, FAnoMes_Int: Integer;
    FEmpresa_Txt, FAnoMes_Txt: String;
    //
    procedure ReopenSINTEGR_10(AnoMes: Integer);
    procedure ReopenSINTEGR_50(LinArq: Integer);
    procedure ReopenSINTEGR_70(LinArq: Integer);
    //
    procedure ReopenEFD_D100(LinArq: Integer);
    //
  end;

var
  FmImportaSintegraGer: TFmImportaSintegraGer;

implementation

uses UnMyObjects, Module, MyDBCheck, ModuleGeral, ModProd, UCreate,
CfgExpFile, ImportaSintegraNew, UnGrade_Tabs, ModuleNFe_0000,
  Principal, UnSintegra, UnSPED_Geral;

{$R *.DFM}

const
  FFormatFloat = '00000';
  FImporExpor_Int = 1; // Importar. N�o mexer!
  FImporExpor_Txt = '1'; // Importar. N�o mexer!

procedure TFmImportaSintegraGer.EdEmpresaChange(Sender: TObject);
begin
  if EdEmpresa.ValueVariant <> 0 then
  begin
    FEmpresa_Int := DModG.QrEmpresasCodigo.Value;
    FEmpresa_Txt := FormatFloat('0', FEmpresa_Int);
  end else
  begin
    FEmpresa_Int := 0;
    FEmpresa_Txt := '';
  end;
  BtPeriodo.Enabled := FEmpresa_Int <> 0;
  ReopenSINTEGR_10(0);
end;

procedure TFmImportaSintegraGer.Excluiperiodoselecionado1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do per�odo selecionado?',
  'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
  begin
    USintegra.ExcluiPeriodoSintegra(QrSINTEGR_10ImporExpor.Value,
      QrSINTEGR_10AnoMes.Value, QrSINTEGR_10Empresa.Value);
    //
    ReopenSINTEGR_10(0);
  end;
end;

procedure TFmImportaSintegraGer.N50T_SeqAcoes_1Click(
  Sender: TObject);
const
  Status = 1;
var
  DataMin, DataMax, TotRec: String;
  Tabela, FatID, FatNum, ImporExpor, AnoMes, Empresa, LinArq(*, ConfVal*): Integer;
  Achou: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    DefineDatasMinEMax(1, 1, DataMin, DataMax);
    //
    PB1.Max := QrSINTEGR_50.RecordCount;
    TotRec := FormatFloat('0', QrSINTEGR_50.RecordCount);
    //
    QrSINTEGR_50.First;
    while not QrSINTEGR_50.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Atrelando item ' +
      FormatFloat('0', QrSINTEGR_50.RecNo) + ' / ' + TotRec);
      Achou := False;
      //
      ImporExpor := QrSINTEGR_50ImporExpor.Value;
      AnoMes := QrSINTEGR_50AnoMes.Value;
      Empresa := QrSINTEGR_50Empresa.Value;
      LinArq := QrSINTEGR_50LinArq.Value;
      //ConfVal := 0;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrLocPQE, Dmod.MyDB, [
      'SELECT Codigo, ValorNF FROM PQE ',
      'WHERE IQ=' + FormatFloat('0', QrSINTEGR_50Terceiro.Value),
      'AND NF=' + QrSINTEGR_50NUMNF.Value,
      'AND Data BETWEEN "' + DataMin + '" AND "' + DataMax + '"'
      ]);
      if QrLocPQE.RecordCount = 1 then
      begin
{
        if QrLocPQEValorNF.Value = QrSINTEGR_50ValorNF.Value then
          ConfVal := 1;
}
        //
        Tabela := GRADE_TABS_PARTIPO_0001;
        FatID := VAR_FATID_0151;
        FatNum := QrLocPQECodigo.Value;
        //
        Achou := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'sintegr_50', False, [
        'Tabela', 'FatID', 'FatNum'], [
        'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
        Tabela,
        FatID, FatNum], [
        ImporExpor, AnoMes, Empresa, LinArq], True);
      end;
      if not Achou then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLocMPc, Dmod.MyDB, [
        'SELECT its.Conta ',
        'FROM mpinits its ',
        'LEFT JOIN MPIn mpi ON mpi.Controle=its.Controle',
        'WHERE its.EmitNFAvul=0 ',
        'AND mpi.Procedencia=' + FormatFloat('0', QrSINTEGR_50Terceiro.Value),
        'AND its.NF=' + QrSINTEGR_50NUMNF.Value,
        'AND its.Emissao BETWEEN "' + DataMin + '" AND "' + DataMax + '"'
        ]);
        if QrLocMPc.RecordCount = 1 then
        begin
          Tabela := GRADE_TABS_PARTIPO_0002;
          FatID := VAR_FATID_0113;
          FatNum := QrLocMPcConta.Value;
          //
          Achou := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'sintegr_50', False, [
          'Tabela', 'FatID', 'FatNum'], [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
          Tabela,
          FatID, FatNum], [
          ImporExpor, AnoMes, Empresa, LinArq], True);
        end;
      end;
      if not Achou then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLocEMP, Dmod.MyDB, [
        'SELECT Codigo ',
        'FROM entimp ',
        'WHERE Codigo=' + FormatFloat('0', QrSINTEGR_50Terceiro.Value)
        ]);
        if QrLocEMP.RecordCount > 0 then
        begin
          Tabela := GRADE_TABS_PARTIPO_0002;
          FatID := VAR_FATID_0113;
          FatNum := 0; // N�o tem nota associada
          //
          Achou := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'sintegr_50', False, [
          'Tabela', 'FatID', 'FatNum'], [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
          Tabela,
          FatID, FatNum], [
          ImporExpor, AnoMes, Empresa, LinArq], True);
        end;
      end;
      if not Achou then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLocEMP, Dmod.MyDB, [
        'SELECT Procedencia Codigo ',
        'FROM mpin ',
        'WHERE Procedencia=' + FormatFloat('0', QrSINTEGR_50Terceiro.Value)
        ]);
        if QrLocEMP.RecordCount > 0 then
        begin
          Tabela := GRADE_TABS_PARTIPO_0002;
          FatID := VAR_FATID_0113;
          FatNum := 0; // N�o tem nota associada
          //
          Achou := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'sintegr_50', False, [
          'Tabela', 'FatID', 'FatNum'], [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
          Tabela,
          FatID, FatNum], [
          ImporExpor, AnoMes, Empresa, LinArq], True);
        end;
      end;
      if not Achou then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLocEMP, Dmod.MyDB, [
        'SELECT Fornece Codigo ',
        'FROM mpinits ',
        'WHERE Fornece=' + FormatFloat('0', QrSINTEGR_50Terceiro.Value)
        ]);
        if QrLocEMP.RecordCount > 0 then
        begin
          Tabela := GRADE_TABS_PARTIPO_0002;
          FatID := VAR_FATID_0113;
          FatNum := 0; // N�o tem nota associada
          //
          Achou := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'sintegr_50', False, [
          'Tabela', 'FatID', 'FatNum'], [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
          Tabela,
          FatID, FatNum], [
          ImporExpor, AnoMes, Empresa, LinArq], True);
        end;
      end;
      if not Achou then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLocEMP, Dmod.MyDB, [
        'SELECT EmitNFAvul Codigo ',
        'FROM mpinits ',
        'WHERE EmitNFAvul=' + FormatFloat('0', QrSINTEGR_50Terceiro.Value)
        ]);
        if QrLocEMP.RecordCount > 0 then
        begin
          Tabela := GRADE_TABS_PARTIPO_0002;
          FatID := VAR_FATID_0113;
          FatNum := 0; // N�o tem nota associada
          //
          Achou := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'sintegr_50', False, [
          'Tabela', 'FatID', 'FatNum'], [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
          Tabela,
          FatID, FatNum], [
          ImporExpor, AnoMes, Empresa, LinArq], True);
        end;
      end;
      if not Achou then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLocEMP, Dmod.MyDB, [
        'SELECT IQ Codigo ',
        'FROM PQE ',
        'WHERE IQ=' + FormatFloat('0', QrSINTEGR_50Terceiro.Value)
        ]);
        if QrLocEMP.RecordCount > 0 then
        begin
          Tabela := GRADE_TABS_PARTIPO_0001;
          FatID := VAR_FATID_0151;
          FatNum := 0; // N�o tem nota associada
          //
          //Achou :=
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'sintegr_50', False, [
          'Tabela', 'FatID', 'FatNum'], [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
          Tabela,
          FatID, FatNum], [
          ImporExpor, AnoMes, Empresa, LinArq], True);
        end;
      end;
      //
      QrSINTEGR_50.Next;
    end;
    //
    ImporExpor := QrSINTEGR_10ImporExpor.Value;
    AnoMes := QrSINTEGR_10AnoMes.Value;
    Empresa := QrSINTEGR_10Empresa.Value;
    IncrementaStatus('Stat50T', Status, ImporExpor, AnoMes, Empresa);
    //
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;

  { TODO : Fazer count de tipos! }
  (*
  SELECT IF(FatNum=0, 0, 1) TemFatNum,
  FatID, COUNT(LinArq) Itens
  FROM sintegr_50
  WHERE ImporExpor=1
  AND AnoMes=201001
  AND EMpresa=-11
  GROUP BY FatID, TemFatNum
  ORDER BY FatID, TemFatNum
  *)

end;

procedure TFmImportaSintegraGer.N50T_SeqAcoes_2Click(Sender: TObject);
const
  Status = 2;
var
  DataMin, DataMax: String;
  ImporExpor, AnoMes, Empresa: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    DefineDatasMinEMax(0, 0, DataMin, DataMax);
    ImporExpor := QrSINTEGR_10ImporExpor.Value;
    AnoMes := QrSINTEGR_10AnoMes.Value;
    Empresa := QrSINTEGR_10Empresa.Value;
    //
    //if CkRecriarPQ.Checked then Excluir criadas a for�a de PQE
    begin
      PB1.Position := 0;
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Pesquisando notas de uso e consumo a serem excluidas');
      QrExclMul.Close;
      QrExclMul.Params[00].AsInteger := VAR_FATID_0151;
      QrExclMul.Params[01].AsInteger := Empresa;
      QrExclMul.Params[02].AsString  := DataMin;
      QrExclMul.Params[03].AsString  := DataMax;
      QrExclMul.Open;
      //
      if QrExclMul.RecordCount > 0 then
      begin
        PB1.Max := QrExclMul.RecordCount;
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Excluindo notas de uso e consumo geradas a for�a');
        //
        while not QrExclMul.Eof do
        begin
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
          DmNFe_0000.ExcluiNfe(QrExclMulinfProt_cStat.Value, QrExclMulFatID.Value,
            QrExclMulFatNum.Value, QrExclMulEmpresa.Value, True);
          //
          QrExclMul.Next;
        end;
      end;
    end;

    //if CkRecriarMP.Checked then Excluir criadas a forca de MPIn
    begin
      PB1.Position := 0;
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Pesquisando notas de mat�ria-prima a serem excluidas');
      QrExclMul.Close;
      QrExclMul.Params[00].AsInteger := VAR_FATID_0113;
      QrExclMul.Params[01].AsInteger := Empresa;
      QrExclMul.Params[02].AsString  := DataMin;
      QrExclMul.Params[03].AsString  := DataMax;
      QrExclMul.Open;
      //
      if QrExclMul.RecordCount > 0 then
      begin
        PB1.Max := QrExclMul.RecordCount;
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Excluindo notas de mat�ria-prima geradas a for�a');
        while not QrExclMul.Eof do
        begin
          PB1.Position := PB1.Position + 1;
          DmNFe_0000.ExcluiNfe(QrExclMulinfProt_cStat.Value, QrExclMulFatID.Value,
            QrExclMulFatNum.Value, QrExclMulEmpresa.Value, True);
          //
          QrExclMul.Next;
        end;
      end;
    end;
    //
    IncrementaStatus('Stat50T', Status, ImporExpor, AnoMes, Empresa);
    //
    PB1.POsition := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaSintegraGer.N50T_SeqAcoes_3Click(Sender: TObject);
const
  Status = 3;
var
  DataMin, DataMax: String;
  ImporExpor, AnoMes, Empresa: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    DefineDatasMinEMax(1, 1, DataMin, DataMax);
    //
    QrSINTEGR_50.First;
    while not QrSINTEGR_50.Eof do
    begin
      if QrSINTEGR_50Tabela.Value = 1 then  // PQE
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLocPQE, Dmod.MyDB, [
        'SELECT Codigo, ValorNF FROM PQE ',
        'WHERE IQ=' + FormatFloat('0', QrSINTEGR_50Terceiro.Value),
        'AND NF=' + QrSINTEGR_50NUMNF.Value,
        'AND Data BETWEEN "' + DataMin + '" AND "' + DataMax + '"'
        ]);
        if QrLocPQE.RecordCount = 1 then
          CriaNFeCabA_151(QrLocPQECodigo.Value);
      end;
      //
      QrSINTEGR_50.Next;
    end;
    //
    ImporExpor := QrSINTEGR_10ImporExpor.Value;
    AnoMes := QrSINTEGR_10AnoMes.Value;
    Empresa := QrSINTEGR_10Empresa.Value;
    IncrementaStatus('Stat50T', Status, ImporExpor, AnoMes, Empresa);
    //
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaSintegraGer.N50T_SeqAcoes_4Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    //
    // ????
    //
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaSintegraGer.N70T_SeqAcoes_1Click(Sender: TObject);
const
  Status = 1;
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o de todos conhecimentos de frete '
  + #13#10 + 'importados para a empresa no per�odo selecionado?', Dmod.QrUpd,
  'efd_d100', ['ImporExpor', 'Empresa', 'AnoMes', 'Importado'],
  ['=','=','=','>'], [FImporExpor_Txt, FEmpresa_Txt, FAnoMes_Txt, 0], '') then
  begin
    IncrementaStatus('Stat70T', Status, FImporExpor_Int, FAnoMes_Int, FEmpresa_Int);
    ReopenEFD_D100(0);
  end;
end;

procedure TFmImportaSintegraGer.N70T_SeqAcoes_2Click(Sender: TObject);
  function CriaConhecimentoDeFrete(ImporExpor, AnoMes, Empresa: Integer): Boolean;
  const
    Importado = 1;
    REG = 'D100';
    IND_OPER = '0'; // Aquisi��o
    IND_EMIT = '1'; // Terceiros
    CHV_CTE = '';
    TP_CTE = '';
    CHV_CTE_REF = '';
    VL_DESC = 0.00;
    COD_INF = '';
    COD_CTA = '';
  var
    Status, LinArq, Terceiro, COD_SIT, NUM_DOC: Integer;
    COD_PART, COD_MOD, SER, SUB, DT_DOC, DT_A_P, IND_FRT: String;
    DtEmissao, DtSaiEntra, DtFiscal: TDateTime;
    VL_DOC, VL_SERV, VL_BC_ICMS, VL_ICMS, VL_NT: Double;
    //
    CST_ICMS, CFOP: Integer;
    ALIQ_ICMS, VL_RED_BC: Double;
  begin
    Terceiro := QrSINTEGR_70Transporta.Value;
    COD_PART := FormatFloat('0', Terceiro);
    COD_MOD := QrSINTEGR_70MODELO.Value;


    SER := QrSINTEGR_70SERIE.Value;
    SUB := QrSINTEGR_70SUBSERIE.Value;
    NUM_DOC := Geral.IMV(QrSINTEGR_70NUMNF.Value);
    DT_DOC := Geral.FDT(QrSINTEGR_70EMISSAO.Value, 1);

    Status := USintegra.ObtemStatusDaSituacao(QrSINTEGR_70Situacao.Value);
    DtEmissao := QrSINTEGR_70EMISSAO.Value;
    DtSaiEntra := QrSINTEGR_70EMISSAO.Value;
    DtFiscal := QrSINTEGR_70EMISSAO.Value;

    SPED_Geral.Obtem_SPED_COD_SIT_de_NFe_Status(IND_OPER, IND_EMIT,
      Status, DtEmissao, DtSaiEntra, DtFiscal, COD_MOD, SER, NUM_DOC, VAR_FATID_1901,
      NUM_DOC, Terceiro, FEmpresa_Int, fedfNormal, FAnoMes_Int, COD_SIT, nil);

    DT_A_P := DT_DOC;
    VL_DOC := QrSINTEGR_70VALORTOT.Value;
    IND_FRT := FormatFloat('0', QrSINTEGR_70CIF_FOB.Value); // � igual!! Aleluia!!!
    VL_SERV := QrSINTEGR_70VALORTOT.Value;
    VL_BC_ICMS := QrSINTEGR_70BASEICMS.Value;
    VL_ICMS := QrSINTEGR_70VALORICMS.Value;
    VL_NT := QrSINTEGR_70VLRISENTO.Value;
    //
    CST_ICMS := 0; // N�o tem!
    CFOP := Geral.IMV(QrSINTEGR_70CFOP.Value);
    if VL_BC_ICMS > 0 then
      ALIQ_ICMS := (Round((VL_ICMS / VL_BC_ICMS) * 10000)) / 100
    else
      ALIQ_ICMS := 0;
    VL_RED_BC := 0; // Isentos?? Outros??
    // Risco de chave duplicada se for feiro lan�amento manual
    //LinArq := QrSINTEGR_70LinArq.Value;
    LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_d100', 'LinArq', [
    'ImporExpor', 'AnoMes', 'Empresa'], [ImporExpor, AnoMes, Empresa],
      stIns, 0, siPositivo, nil);
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efd_d100', False,[
    'REG', 'IND_OPER', 'IND_EMIT',
    'COD_PART', 'COD_MOD', 'COD_SIT',
    'SER', 'SUB', 'NUM_DOC',
    'CHV_CTE', 'DT_DOC', 'DT_A_P',
    'TP_CTE', 'CHV_CTE_REF', 'VL_DOC',
    'VL_DESC', 'IND_FRT', 'VL_SERV',
    'VL_BC_ICMS', 'VL_ICMS', 'VL_NT',
    'COD_INF', 'COD_CTA', 'Terceiro',
    'Importado', 'CST_ICMS', 'CFOP',
    'ALIQ_ICMS', 'VL_RED_BC'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
    REG, IND_OPER, IND_EMIT,
    COD_PART, COD_MOD, COD_SIT,
    SER, SUB, NUM_DOC,
    CHV_CTE, DT_DOC, DT_A_P,
    TP_CTE, CHV_CTE_REF, VL_DOC,
    VL_DESC, IND_FRT, VL_SERV,
    VL_BC_ICMS, VL_ICMS, VL_NT,
    COD_INF, COD_CTA, Terceiro,
    Importado, CST_ICMS, CFOP,
    ALIQ_ICMS, VL_RED_BC], [
    ImporExpor, AnoMes, Empresa, LinArq], True);
  end;
const
  Status = 2;
begin
  PB1.Position := 0;
  PB1.Max := QrSINTEGR_70.RecordCount;
  QrSINTEGR_70.First;
  while not QrSINTEGR_70.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Incluindo registro ' +
      FormatFloat('0', QrSINTEGR_70.RecNo));
    CriaConhecimentoDeFrete(FImporExpor_Int, FAnoMes_Int, FEmpresa_Int);
    //
    QrSINTEGR_70.Next;
  end;
  IncrementaStatus('Stat70T', Status, FImporExpor_Int, FAnoMes_Int, FEmpresa_Int);
  PB1.Position := 0; 
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmImportaSintegraGer.ObtemValorRecImpostos(const ValItsNF,
  BCICMS, ValICMS, ValIsento, ValNCICMS, AliqICMS: Double;
  var ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS: Double);
var
  Dif, Fator: Double;
begin
  Dif := BCICMS - ValItsNF;
  if (BCICMS < 0.01) and (AliqICMS < 0.01) and (ValICMS < 0.01) then
  begin
    ICMSRec_pRedBC := 0;
    ICMSRec_vBC := 0;
    ICMSRec_pAliq := 0;
    ICMSRec_vICMS := 0;
  end else
  if Dif < 0.01 then
  begin
    ICMSRec_pRedBC := 0;
    ICMSRec_vBC := BCICMS;
    ICMSRec_pAliq := AliqICMS;
    ICMSRec_vICMS := ValICMS;
  end else begin
    if BCICMS > 0 then
      Fator := ValItsNF / BCICMS
    else
      Fator := 0;
    //
    ICMSRec_pRedBC := Int((1 - Fator) * 100);
    ICMSRec_vBC := Geral.RoundC(Fator * BCICMS, 2);
    ICMSRec_pAliq := AliqICMS;
    ICMSRec_vICMS := Geral.RoundC(Fator * ValICMS, 2);
  end;
end;

procedure TFmImportaSintegraGer.IncluiNF1Click(Sender: TObject);
begin
  USintegra.CriaNFe(QrSINTEGR_50ImporExpor.Value, QrSINTEGR_50AnoMes.Value,
    QrSINTEGR_50Empresa.Value, QrSINTEGR_50LinArq.Value);
  ReopenSINTEGR_50(QrSINTEGR_50LinArq.Value);
end;

procedure TFmImportaSintegraGer.Incluinovoperiodo1Click(Sender: TObject);
begin
  if MyObjects.CriaForm_AcessoTotal(TFmImportaSintegraNew, FmImportaSintegraNew) then
  begin
    FmImportaSintegraNew.ShowModal;
    ReopenSINTEGR_10(FmImportaSintegraNew.FAnoMes);
    FmImportaSintegraNew.Destroy;
  end;
end;

procedure TFmImportaSintegraGer.IncrementaStatus(Campo: String;
Status, ImporExpor, AnoMes,
  Empresa: Integer);
begin
  if QrSINTEGR_10.FieldByName(Campo).AsInteger = Status -1 then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'sintegr_10', False, [
    Campo], [
    'ImporExpor', 'AnoMes', 'Empresa'], [
    Status], [
    ImporExpor, AnoMes, Empresa], True);
    //
    ReopenSINTEGR_10(QrSINTEGR_10AnoMes.Value);
  end;
end;

procedure TFmImportaSintegraGer.Itemns1Click(Sender: TObject);
begin
{
  PageControl1.ActivePageIndex := 0;
  if UmyMod.FormInsUpd_Cria(TFmSPEDEstqIts, FmSPEDEstqIts, afmoNegarComAviso,
  QrSINTEGR_50, stIns) then
  begin
    FmSPEDEstqIts.RGSit_Prod.ItemIndex := 1;
    FmSPEDEstqIts.ShowModal;
    FmSPEDEstqIts.Destroy;
  end;
}
end;

procedure TFmImportaSintegraGer.ItemSelecionado2Click(Sender: TObject);
begin
(*
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrSINTEGR_50, TDBGrid(DBGIts),
  'spedestqits', ['Controle'], ['Controle'], istPergunta, '');
  ReopenSINTEGR_50(0, 0, 0);
*)
end;

function TFmImportaSintegraGer.PertenceAoGrupoGG2: Boolean;
begin
  case PageControl1.ActivePageIndex of
    0: Result := Geral.IntInConjunto(2, CGAgrup0.Value);
    else Result := False;
  end;
end;

function TFmImportaSintegraGer.PertenceAoGrupoPGT(): Boolean;
begin
  case PageControl1.ActivePageIndex of
    0: Result := Geral.IntInConjunto(4, CGAgrup0.Value);
    else Result := False;
  end;
end;

function TFmImportaSintegraGer.PertenceAoGrupoPRD(): Boolean;
begin
  case PageControl1.ActivePageIndex of
    0: Result := Geral.IntInConjunto(1, CGAgrup0.Value);
    else Result := False;
  end;
end;

procedure TFmImportaSintegraGer.PMExcluiPopup(Sender: TObject);
begin
  Periodoselecionado1.Enabled :=
    (QrSINTEGR_10.State <> dsInactive)
    and
    (QrSINTEGR_10.RecordCount > 0)
    and
    (QrSINTEGR_50.RecordCount = 0);
  //
end;

procedure TFmImportaSintegraGer.PMPeriodoPopup(Sender: TObject);
begin
  Excluiperiodoselecionado1.Enabled :=
    (QrSINTEGR_10.State <> dsInactive) and
    (QrSINTEGR_10.RecordCount > 0) and
    (QrSINTEGR_10Stat50T.Value < 4 (* ? *));
  Itemns1.Enabled :=
    (QrSINTEGR_10.State <> dsInactive) and
    (QrSINTEGR_10.RecordCount > 0);
end;

procedure TFmImportaSintegraGer.PMNFPopup(Sender: TObject);
begin
  IncluiNF1.Enabled := QrNFeCabA.RecordCount = 0;
  AlteraNF1.Enabled := QrNFeCabA.RecordCount = 1;
end;

procedure TFmImportaSintegraGer.PMSeqAcoes0Popup(Sender: TObject);
begin
  N50T_SeqAcoes_1.Enabled := QrSINTEGR_10Stat50T.Value = 0;
  N50T_SeqAcoes_2.Enabled := QrSINTEGR_10Stat50T.Value = 1;
  N50T_SeqAcoes_3.Enabled := QrSINTEGR_10Stat50T.Value = 2;
  //N50T_SeqAcoes_4.Enabled := QrSINTEGR_10Status.Value = 3;
end;

procedure TFmImportaSintegraGer.PMSeqAcoes1Popup(Sender: TObject);
begin
  N70T_SeqAcoes_1.Enabled := QrSINTEGR_10Stat70T.Value = 0;
  N70T_SeqAcoes_2.Enabled := QrSINTEGR_10Stat70T.Value = 1;
end;

procedure TFmImportaSintegraGer.ReopenSINTEGR_50(LinArq: Integer);
begin
  QrSINTEGR_50.Close;
  QrSINTEGR_50.Params[00].AsInteger := QrSINTEGR_10ImporExpor.Value;
  QrSINTEGR_50.Params[01].AsInteger := QrSINTEGR_10Empresa.Value;
  QrSINTEGR_50.Params[02].AsInteger := QrSINTEGR_10AnoMes.Value;
  QrSINTEGR_50.Params[03].AsString  := RGEmitente.Items[RGEmitente.ItemIndex][1];
  QrSINTEGR_50.Open;
  //
  QrSINTEGR_50.Locate('LinArq', LinArq, []);
end;

procedure TFmImportaSintegraGer.ReopenSINTEGR_70(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSINTEGR_70, Dmod.MyDB, [
  'SELECT s70.* ',
  'FROM sintegr_70 s70 ',
  'WHERE s70.ImporExpor=' + FImporExpor_Txt,
  'AND s70.Empresa=' + FEmpresa_Txt,
  'AND s70.AnoMes=' + FAnoMes_Txt,
  'ORDER BY LinArq ',
  '']);
end;

procedure TFmImportaSintegraGer.RGEmitenteClick(Sender: TObject);
begin
  ReopenSINTEGR_50(0);
end;

procedure TFmImportaSintegraGer.ReopenAtrelado();
begin
  QrPQE_1_.Close;
  if QrSINTEGR_50FatID.Value  in
  ([VAR_FATID_0051, VAR_FATID_0151, VAR_FATID_0251]) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPQE_1_, Dmod.MyDB, [
    'SELECT Codigo FatNum, DataE dtEmi, DataS dtSaiEnt,  ',
    'Data DataFiscal, ValorNF ValTot, CI Empresa,  ',
    'CI CodInfoDest, IQ CodInfoEmit, refNFe Id,  ',
    'modNF ide_mod, Serie,  NF nNF, Frete, Seguro, ',
    'Desconto, IPI, PIS, COFINS, Outros',
    'FROM PQE ',
    'WHERE Codigo=' + FormatFloat('0', QrSINTEGR_50FatNum.Value),
    'AND CI=' + FormatFloat('0', QrSINTEGR_10Empresa.Value),
    '']);
    //
    DBGAtrelado.DataSource := DsPQE_1_;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabA, Dmod.MyDB, [
  'SELECT FatID, FatNum, Empresa, IDCtrl, ',
  'ide_nNF, ide_dEmi, ide_dSaiEnt, ICMSTot_vNF ',
  'FROM nfecaba ',
  ' ',
  'WHERE EFD_INN_AnoMes <> 0 ',
  'AND EFD_INN_Empresa <> 0 ',
  'AND EFD_INN_LinArq <> 0 ',
  ' ',
  'AND EFD_INN_AnoMes=' + FormatFloat('0', QrSINTEGR_50AnoMes.Value),
  'AND EFD_INN_Empresa=' + FormatFloat('0', QrSINTEGR_50Empresa.Value),
  'AND EFD_INN_LinArq=' + FormatFloat('0', QrSINTEGR_50LinArq.Value),
  '']);
  //
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSINTEGR_54, Dmod.MyDB, [
  'SELECT s54.* ',
  'FROM sintegr_54 s54 ',
  'WHERE s54.ImporExpor=' + FormatFloat('0', QrSINTEGR_50ImporExpor.Value),
  'AND s54.AnoMes=' + FormatFloat('0', QrSINTEGR_50AnoMes.Value),
  'AND s54.Empresa=' + FormatFloat('0', QrSINTEGR_50Empresa.Value),
  'AND s54.CNPJ="' + QrSINTEGR_50CNPJ.Value + '"',
  'AND s54.MODELO="' + QrSINTEGR_50MODELO.Value + '"',
  'AND s54.SERIE="' + QrSINTEGR_50SERIE.Value + '"',
  'AND s54.NUMNF="' + QrSINTEGR_50NUMNF.Value + '"',
  '']);
end;

procedure TFmImportaSintegraGer.ReopenEFD_D100(LinArq: Integer);
begin
  // 
end;

procedure TFmImportaSintegraGer.ReopenSINTEGR_10(AnoMes: Integer);
begin
  QrSINTEGR_10.Close;
  if FEmpresa_Int <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSINTEGR_10, Dmod.MyDB, [
    'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT,',
    'CONCAT(RIGHT(sec.AnoMes, 2), "/",',
    'LEFT(LPAD(sec.AnoMes, 6, "0"), 4)) MES_ANO, sec.*',
    'FROM sintegr_10 sec',
    'LEFT JOIN entidades ent ON ent.Codigo=sec.Empresa',
    'WHERE sec.ImporExpor=' + FImporExpor_Txt,
    'AND sec.Empresa=' + FEmpresa_Txt,
    'ORDER BY AnoMes DESC']);
    //
    if AnoMes > 0 then
      QrSINTEGR_10.Locate('AnoMes', AnoMes, []);
  end;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmImportaSintegraGer.BtNFClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNF, BtNF);
end;

procedure TFmImportaSintegraGer.BtSaida0Click(Sender: TObject);
begin
  //VAR_CADASTRO := Qr?.Value;
  Close;
end;

procedure TFmImportaSintegraGer.DBG_s50DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if Column.FieldName = 'ACHOU_TXT' then
  begin
    if QrSINTEGR_50FatNum.Value > 0 then
      Cor := clBlue
    else
      Cor := clRed;
    with DBG_s50.Canvas do
    begin
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

function TFmImportaSintegraGer.DefineDataFim(): TDateTime;
var
  Ano, Mes: Word;
  DtStr: String;
begin
  DtStr := FormatFloat('000000', QrSINTEGR_10AnoMes.Value);
  Ano := Geral.IMV(Copy(DtStr, 1, 4));
  Mes := Geral.IMV(Copy(DtStr, 5, 2));
  Result := EncodeDate(Ano, Mes, 1);
  Result := IncMonth(Result, 1) - 1;
end;

procedure TFmImportaSintegraGer.DefineDatasMinEMax(const MesesAMenos,
MesesAMais: Integer; var DataMin, DataMax: String);
begin
  DataMin := Geral.FDT(IncMonth(QrSINTEGR_10DATAINI.Value, - MesesAMenos), 1);
  DataMax := Geral.FDT(IncMonth(QrSINTEGR_10DATAFIN.Value, + MesesAMais), 1);
end;

procedure TFmImportaSintegraGer.BtSeqAcoesClick(Sender: TObject);
begin
  case PageControl2.ActivePageIndex of
    0:
    begin
      case RGEmitente.ItemIndex of
        0: MyObjects.MostraPopUpDeBotao(PMSeqAcoes0, BtSeqAcoes);
        1: Geral.MensagemBox('Sequ�ncia de a��es n�o implementada!',
           'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
    1:
    begin
      MyObjects.MostraPopUpDeBotao(PMSeqAcoes1, BtSeqAcoes);
    end;
  end;
end;

procedure TFmImportaSintegraGer.CriaNFeCabA_151(Codigo: Integer);
const
  Importado = 1;
var
  ide_serie, ide_mod, Versao, IDCtrl, FatID, FatNum, Empresa, ide_nNF,
  CriAForca,   CodInfoDest, CodInfoEmit, GraGruX: Integer;
  Id, ide_verProc, ide_natOp, ide_tpNF, ide_dEmi, ide_dSaiEnt, DataFiscal,
  ide_indPag, emit_CNPJ, emit_CPF, emit_IE, dest_CNPJ, dest_CPF, dest_IE: String;
  ICMSTot_vICMS, ICMSTot_vProd, ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc,
  ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro, ICMSTot_vNF:
  Double;
  // Itens
  MeuID, Nivel1, nItem: Integer;
  prod_CFOP, prod_uCom, prod_uTrib: String;
  prod_qCom, prod_vUnCom, prod_vProd, prod_qTrib, prod_vUnTrib: Double;
  //
  CFOP, CFOPa, CFOPb, CFOPc, CFOPd, CFOPe: String;
  //
  NFG_Serie, NF_CFOP, NFG_SubSerie: String;
  NF_ICMSAlq, NFG_ValIsen, NFG_NaoTrib, NFG_Outros, ICMSTot_vBC,
  ICMSRec_vICMS, ICMSRec_vICMSST, ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_vBCST,
  ICMSRec_pAliq: Double;
  EFD_INN_AnoMes, EFD_INN_Empresa, EFD_INN_LinArq, UnidMedCom, UnidMedTrib: Integer;

  // nfeitsn
  ICMS_Orig, ICMS_CST, ICMS_modBC, ICMS_modBCST, Status: Integer;
  ICMS_pRedBC, ICMS_vBC, ICMS_pICMS, ICMS_vICMS, ICMS_pMVAST, ICMS_pRedBCST,
  ICMS_vBCST, ICMS_pICMSST, ICMS_vICMSST: Double;
  //
  SumBC, SumVl: Double;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando dados fiscais para uso e consumo');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQE_2_, Dmod.MyDB, [
  'SELECT Codigo FatNum, DataE dtEmi, DataS dtSaiEnt,  ',
  'Data DataFiscal, ValorNF ValTot, CI Empresa,  ',
  'CI CodInfoDest, IQ CodInfoEmit, refNFe Id,  ',
  'modNF ide_mod, Serie,  NF nNF, Frete, Seguro, ',
  'Desconto, IPI, PIS, COFINS, Outros',
  'FROM PQE ',
  'WHERE Codigo=' + FormatFloat('0', Codigo),
  '']);
  //
  FatID                       := VAR_FATID_0151;
  FatNum                      := QrPQE_2_FatNum.Value;
  Empresa                     := QrPQE_2_Empresa.Value;
  CodInfoDest                 := QrPQE_2_CodInfoDest.Value;
  CodInfoEmit                 := QrPQE_2_CodInfoEmit.Value;
  Id                          := QrPQE_2_Id.Value;
  ide_verProc                 := '';
  ide_natOp                   := '';
  ide_mod                     := QrPQE_2_ide_mod.Value;
  ide_serie                   := QrPQE_2_Serie.Value;
  ide_tpNF                    := '1';
  ide_indPag                  := '1';
  ide_nNF                     := QrPQE_2_nNF.Value;
  ide_dEmi                    := Geral.FDT(QrPQE_2_dtEmi.Value, 1);
  ide_dSaiEnt                 := Geral.FDT(QrPQE_2_dtSaiEnt.Value, 1);
  versao                      := 0;

  // Pegar do SINTEGRA!
  DataFiscal                  := Geral.FDT(QrSINTEGR_50EMISSAO.Value, 1);
  if QrPQE_2_dtEmi.Value < 2 then ide_dEmi := DataFiscal;
  if QrPQE_2_dtSaiEnt.Value < 2 then ide_dSaiEnt := DataFiscal;
  //
  DModG.ObtemCNPJ_CPF_IE_DeEntidade(CodInfoEmit, emit_CNPJ, emit_CPF, emit_IE);
  dest_CNPJ                   := DModG.QrEmpresasCNPJ_CPF.Value;
  dest_CPF                    := '';
  dest_IE                     := DModG.QrEmpresasIE.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPQEIts, Dmod.MyDB, [
  'SELECT SUM(ValorItem) ValorItem ',
  'FROM pqeits ',
  'WHERE Codigo=' + FormatFloat('0', FatNum),
  '']);
  // Pegar do PQE /PQEIts
  ICMSTot_vProd               := QrSumPQEItsValorItem.Value;
  ICMSTot_vFrete              := QrPQE_2_Frete.Value;
  ICMSTot_vSeg                := QrPQE_2_Seguro.Value;
  ICMSTot_vDesc               := QrPQE_2_Desconto.Value;
  ICMSTot_vIPI                := QrPQE_2_IPI.Value;
  ICMSTot_vPIS                := QrPQE_2_PIS.Value;
  ICMSTot_vCOFINS             := QrPQE_2_COFINS.Value;
  ICMSTot_vOutro              := QrPQE_2_Outros.Value;
  // Pegar do SINTEGRA
  ICMSTot_vNF                 := QrSINTEGR_50ValorNF.Value;
  ICMSTot_vICMS               := QrSINTEGR_50ValICMS.Value;
  ICMSTot_vBC                 := QrSINTEGR_50BCICMS.Value;
  ICMSRec_vICMS               := QrSINTEGR_50ValICMS.Value;
  ICMSRec_vBC                 := QrSINTEGR_50BCICMS.Value;
  ICMSRec_vICMSST             := 0;
  ICMSRec_vBCST               := 0;
  NFG_Serie := QrSINTEGR_50SERIE.Value;
  NF_ICMSAlq := QrSINTEGR_50AliqICMS.Value;
  NF_CFOP := QrSINTEGR_50CFOP.Value;
  NFG_SubSerie := '';
  NFG_ValIsen := QrSINTEGR_50ValIsento.Value;
  NFG_NaoTrib := QrSINTEGR_50ValNCICMS.Value;
  NFG_Outros := 0;  // ???
  EFD_INN_AnoMes := QrSINTEGR_50AnoMes.Value;
  EFD_INN_Empresa := QrSINTEGR_50Empresa.Value;
  EFD_INN_LinArq := QrSINTEGR_50LinArq.Value;
  //
  Status := USintegra.ObtemStatusDaSituacao(QrSINTEGR_50Situacao.Value);
  //
  IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);

  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False,[
'IDCtrl', (*'LoteEnv', 'versao',*)
'Id', (*'ide_cUF', 'ide_cNF',*)
'ide_natOp', 'ide_indPag', 'ide_mod',
'ide_serie', 'ide_nNF', 'ide_dEmi',
'ide_dSaiEnt', 'ide_tpNF', (*'ide_cMunFG',
'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
'ide_tpAmb', 'ide_finNFe', 'ide_procEmi',*)
'ide_verProc', 'emit_CNPJ', 'emit_CPF',
(*'emit_xNome', 'emit_xFant', 'emit_xLgr',
'emit_nro', 'emit_xCpl', 'emit_xBairro',
'emit_cMun', 'emit_xMun', 'emit_UF',
'emit_CEP', 'emit_cPais', 'emit_xPais',
'emit_fone',*) 'emit_IE', (*'emit_IEST',
'emit_IM', 'emit_CNAE',*) 'dest_CNPJ',
'dest_CPF', (*'dest_xNome', 'dest_xLgr',
'dest_nro', 'dest_xCpl', 'dest_xBairro',
'dest_cMun', 'dest_xMun', 'dest_UF',
'dest_CEP', 'dest_cPais', 'dest_xPais',
'dest_fone',*) 'dest_IE', (*'dest_ISUF',*)
'ICMSTot_vBC', 'ICMSTot_vICMS', (*'ICMSTot_vBCST',
'ICMSTot_vST',*) 'ICMSTot_vProd', 'ICMSTot_vFrete',
'ICMSTot_vSeg', 'ICMSTot_vDesc', (*'ICMSTot_vII',*)
'ICMSTot_vIPI', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
'ICMSTot_vOutro', 'ICMSTot_vNF', (*'ISSQNtot_vServ',
'ISSQNtot_vBC', 'ISSQNtot_vISS', 'ISSQNtot_vPIS',
'ISSQNtot_vCOFINS', 'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS',
'RetTrib_vRetCSLL', 'RetTrib_vBCIRRF', 'RetTrib_vIRRF',
'RetTrib_vBCRetPrev', 'RetTrib_vRetPrev', 'ModFrete',
'Transporta_CNPJ', 'Transporta_CPF', 'Transporta_XNome',
'Transporta_IE', 'Transporta_XEnder', 'Transporta_XMun',
'Transporta_UF', 'RetTransp_vServ', 'RetTransp_vBCRet',
'RetTransp_PICMSRet', 'RetTransp_vICMSRet', 'RetTransp_CFOP',
'RetTransp_CMunFG', 'VeicTransp_Placa', 'VeicTransp_UF',
'VeicTransp_RNTC', 'Cobr_Fat_nFat', 'Cobr_Fat_vOrig',
'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq', 'InfAdic_InfAdFisco',
'InfAdic_InfCpl', 'Exporta_UFEmbarq', 'Exporta_XLocEmbarq',
'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',*)
'Status', (*'infProt_Id', 'infProt_tpAmb',
'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
'infCanc_xJust', '_Ativo_', 'FisRegCad',
'CartEmiss', 'TabelaPrc', 'CondicaoPg',
'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',*)
'ICMSRec_vBC', (*'ICMSRec_pAliq',*) 'ICMSRec_vICMS',(*
'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
'DataFiscal', (*'protNFe_versao', 'retCancNFe_versao',
'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta', 'SINTEGRA_ExpNat',
'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta', 'SINTEGRA_ExpConhNum',
'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip', 'SINTEGRA_ExpPais',
'SINTEGRA_ExpAverDta',*) 'CodInfoEmit', 'CodInfoDest',
'CriAForca', (*'ide_hSaiEnt', 'ide_dhCont',
'ide_xJust', 'emit_CRT', 'dest_email',
'Vagao', 'Balsa', 'CodInfoTrsp',
'OrdemServ', 'Situacao', 'Antigo',*)
'NFG_Serie', 'NF_ICMSAlq', 'NF_CFOP',
'Importado', 'NFG_SubSerie', 'NFG_ValIsen',
'NFG_NaoTrib', 'NFG_Outros', (*'COD_MOD',
'COD_SIT', 'VL_ABAT_NT',*) 'EFD_INN_AnoMes',
'EFD_INN_Empresa', 'EFD_INN_LinArq',
'ICMSRec_vBCST', 'ICMSRec_vICMSST'], [
'FatID', 'FatNum', 'Empresa'], [
IDCtrl, (*LoteEnv, versao,*)
Id, (*ide_cUF, ide_cNF,*)
ide_natOp, ide_indPag, ide_mod,
ide_serie, ide_nNF, ide_dEmi,
ide_dSaiEnt, ide_tpNF, (*ide_cMunFG,
ide_tpImp, ide_tpEmis, ide_cDV,
ide_tpAmb, ide_finNFe, ide_procEmi,*)
ide_verProc, emit_CNPJ, emit_CPF,
(*emit_xNome, emit_xFant, emit_xLgr,
emit_nro, emit_xCpl, emit_xBairro,
emit_cMun, emit_xMun, emit_UF,
emit_CEP, emit_cPais, emit_xPais,
emit_fone,*) emit_IE, (*emit_IEST,
emit_IM, emit_CNAE,*) dest_CNPJ,
dest_CPF, (*dest_xNome, dest_xLgr,
dest_nro, dest_xCpl, dest_xBairro,
dest_cMun, dest_xMun, dest_UF,
dest_CEP, dest_cPais, dest_xPais,
dest_fone,*) dest_IE, (*dest_ISUF,*)
ICMSTot_vBC, ICMSTot_vICMS, (*ICMSTot_vBCST,
ICMSTot_vST,*) ICMSTot_vProd, ICMSTot_vFrete,
ICMSTot_vSeg, ICMSTot_vDesc, (*ICMSTot_vII,*)
ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
ICMSTot_vOutro, ICMSTot_vNF, (*ISSQNtot_vServ,
ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,
ISSQNtot_vCOFINS, RetTrib_vRetPIS, RetTrib_vRetCOFINS,
RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,
RetTrib_vBCRetPrev, RetTrib_vRetPrev, ModFrete,
Transporta_CNPJ, Transporta_CPF, Transporta_XNome,
Transporta_IE, Transporta_XEnder, Transporta_XMun,
Transporta_UF, RetTransp_vServ, RetTransp_vBCRet,
RetTransp_PICMSRet, RetTransp_vICMSRet, RetTransp_CFOP,
RetTransp_CMunFG, VeicTransp_Placa, VeicTransp_UF,
VeicTransp_RNTC, Cobr_Fat_nFat, Cobr_Fat_vOrig,
Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,
InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq,
Compra_XNEmp, Compra_XPed, Compra_XCont,*)
Status, (*infProt_Id, infProt_tpAmb,
infProt_verAplic, infProt_dhRecbto, infProt_nProt,
infProt_digVal, infProt_cStat, infProt_xMotivo,
infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,
infCanc_cStat, infCanc_xMotivo, infCanc_cJust,
infCanc_xJust, _Ativo_, FisRegCad,
CartEmiss, TabelaPrc, CondicaoPg,
FreteExtra, SegurExtra, ICMSRec_pRedBC,*)
ICMSRec_vBC, (*ICMSRec_pAliq,*) ICMSRec_vICMS,
(*IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
DataFiscal, (*protNFe_versao, retCancNFe_versao,
SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_ExpNat,
SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum,
SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais,
SINTEGRA_ExpAverDta,*) CodInfoEmit, CodInfoDest,
CriAForca, (*ide_hSaiEnt, ide_dhCont,
ide_xJust, emit_CRT, dest_email,
Vagao, Balsa, CodInfoTrsp,
OrdemServ, Situacao, Antigo,*)
NFG_Serie, NF_ICMSAlq, NF_CFOP,
Importado, NFG_SubSerie, NFG_ValIsen,
NFG_NaoTrib, NFG_Outros, (*COD_MOD,
COD_SIT, VL_ABAT_NT,*) EFD_INN_AnoMes,
EFD_INN_Empresa, EFD_INN_LinArq,
ICMSRec_vBCST, ICMSRec_vICMSST], [
FatID, FatNum, Empresa], True) then
  begin
    QrPQEIts_2_.Close;
    QrPQEIts_2_.Params[0].AsInteger := QrPQE_2_FatNum.Value;
    QrPQEIts_2_.Open;
    nItem := 0;
    // Teste
    SumBC := 0;
    SumVl := 0;
    //
    while not QrPQEIts_2_.Eof do
    begin
      nItem        := nItem + 1;
      MeuID        := QrPQEIts_2_Controle.Value;
      Nivel1       := QrPQEIts_2_Insumo.Value;
      DmProd.ObtemPrimeiroGraGruXdeGraGru1(FatID, FatNum, nItem,
        QrPQEIts_2_NOMEPQ.Value, True, Nivel1, GraGruX);
      prod_CFOP    := QrPQEIts_2_prod_CFOP.Value;
      prod_uTrib   := prod_uTrib;
      prod_qCom    := QrPQEIts_2_TotalPeso.Value;
      prod_qTrib   := QrPQEIts_2_TotalPeso.Value;
      if QrPQEIts_2_TotalPeso.Value > 0 then
        prod_vUnCom  := QrPQEIts_2_ValorItem.Value / QrPQEIts_2_TotalPeso.Value
      else prod_vUnCom  := 0;
      prod_vUnTrib := prod_vUnCom;
      prod_vProd   := QrPQEIts_2_ValorItem.Value;
      //
      DmProd.Obtem_prod_uTribdeGraGru1(Nivel1, prod_uCom, UnidMedCom);
      UnidMedTrib := UnidMedCom;
      //
{     Parei Aqui! Teste Tirar
      if QrPQEIts_2_Codigo.Value = 231 then
        ShowMessage('231');
}
      ObtemValorRecImpostos(prod_vProd,
      QrSintegr_50BCICMS.Value, QrSintegr_50ValICMS.Value,
      QrSintegr_50ValIsento.Value, QrSintegr_50ValNCICMS.Value,
      QrSintegr_50AliqICMS.Value,
      ICMSRec_pRedBC, ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS);
      ICMSRec_vBCST := 0;
      ICMSRec_vICMSST := 0;
      //
      SumBC := SumBC + ICMSRec_vBC;
      SumVl := SumVl + ICMSRec_vICMS;
      if QrPQEIts_2_.RecordCount = QrPQEIts_2_.RecNo then
      begin
        if SumBC <> QrSintegr_50BCICMS.Value then
          ICMSRec_vBC := ICMSRec_vBC - (SumBC - QrSintegr_50BCICMS.Value);
        if SumVl <> QrSintegr_50ValICMS.Value then
          ICMSRec_vICMS := ICMSRec_vICMS - (SumVl - QrSintegr_50ValICMS.Value);
      end;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
(*'prod_cProd', 'prod_cEAN', 'prod_xProd',
'prod_NCM', 'prod_EXTIPI', 'prod_genero',*)
'prod_CFOP', 'prod_uCom', 'prod_qCom',
'prod_vUnCom', 'prod_vProd', (*'prod_cEANTrib',*)
'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
(*'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
'Tem_IPI', '_Ativo_', 'InfAdCuztm',
'EhServico',*) 'ICMSRec_pRedBC', 'ICMSRec_vBC',
'ICMSRec_pAliq', 'ICMSRec_vICMS', (*'IPIRec_pRedBC',
'IPIRec_vBC', 'IPIRec_pAliq', 'IPIRec_vIPI',
'PISRec_pRedBC', 'PISRec_vBC', 'PISRec_pAliq',
'PISRec_vPIS', 'COFINSRec_pRedBC', 'COFINSRec_vBC',
'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)  'MeuID',
'Nivel1', (*'prod_vOutro', 'prod_indTot',
'prod_xPed', 'prod_nItemPed',*) 'GraGruX',
'UnidMedCom', 'UnidMedTrib',
'ICMSRec_vBCST', 'ICMSRec_vICMSST'], [
'FatID', 'FatNum', 'Empresa', 'nItem'], [
(*prod_cProd, prod_cEAN, prod_xProd,
prod_NCM, prod_EXTIPI, prod_genero,*)
prod_CFOP, prod_uCom, prod_qCom,
prod_vUnCom, prod_vProd, (*prod_cEANTrib,*)
prod_uTrib, prod_qTrib, prod_vUnTrib,
(*prod_vFrete, prod_vSeg, prod_vDesc,
Tem_IPI, _Ativo_, InfAdCuztm,
EhServico,*) ICMSRec_pRedBC, ICMSRec_vBC,
ICMSRec_pAliq, ICMSRec_vICMS, (*IPIRec_pRedBC,
IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI,
PISRec_pRedBC, PISRec_vBC, PISRec_pAliq,
PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
COFINSRec_pAliq, COFINSRec_vCOFINS,*)MeuID,
Nivel1, (*prod_vOutro, prod_indTot,
prod_xPed, prod_nItemPed,*) GraGruX,
UnidMedCom, UnidMedTrib,
ICMSRec_vBCST, ICMSRec_vICMSST], [
FatID, FatNum, Empresa, nItem], True) then
      begin
        ICMS_Orig := QrPQEIts_2_ICMS_Orig.Value;
        ICMS_CST  := QrPQEIts_2_ICMS_Orig.Value;
        ICMS_modBC := QrPQEIts_2_ICMS_modBC.Value;
        ICMS_pRedBC := QrPQEIts_2_ICMS_pRedBC.Value;
        ICMS_vBC := QrPQEIts_2_ICMS_vBC.Value;
        ICMS_pICMS := QrPQEIts_2_ICMS_pICMS.Value;
        ICMS_vICMS := QrPQEIts_2_ICMS_vICMS.Value;
        ICMS_modBCST := QrPQEIts_2_ICMS_modBCST.Value;
        ICMS_pMVAST := QrPQEIts_2_ICMS_pMVAST.Value;
        ICMS_pRedBCST := QrPQEIts_2_ICMS_pRedBCST.Value;
        ICMS_vBCST := QrPQEIts_2_ICMS_vBCST.Value;
        ICMS_pICMSST := QrPQEIts_2_ICMS_pICMSST.Value;
        ICMS_vICMSST := QrPQEIts_2_ICMS_vICMSST.Value;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsn', False, [
'ICMS_Orig', 'ICMS_CST', 'ICMS_modBC',
'ICMS_pRedBC', 'ICMS_vBC', 'ICMS_pICMS',
'ICMS_vICMS', 'ICMS_modBCST', 'ICMS_pMVAST',
'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
'ICMS_vICMSST'(*, '_Ativo_', 'ICMS_CSOSN',
'ICMS_UFST', 'ICMS_pBCOp', 'ICMS_vBCSTRet',
'ICMS_vICMSSTRet', 'ICMS_motDesICMS', 'ICMS_pCredSN',
'ICMS_vCredICMSSN', 'COD_NAT'*)], [
'FatID', 'FatNum', 'Empresa', 'nItem'], [
ICMS_Orig, ICMS_CST, ICMS_modBC,
ICMS_pRedBC, ICMS_vBC, ICMS_pICMS,
ICMS_vICMS, ICMS_modBCST, ICMS_pMVAST,
ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
ICMS_vICMSST(*, _Ativo_, ICMS_CSOSN,
ICMS_UFST, ICMS_pBCOp, ICMS_vBCSTRet,
ICMS_vICMSSTRet, ICMS_motDesICMS, ICMS_pCredSN,
ICMS_vCredICMSSN, COD_NAT*)], [
FatID, FatNum, Empresa, nItem], True) then
        begin
          // Fazer IPI, PIS e COFINS?
        end;
      end;
      //
      QrPQEIts_2_.Next;
    end;
    //
    QrPQE_2_.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmImportaSintegraGer.AlteraNF1Click(Sender: TObject);
begin
  Grade_Jan.MostraFormEntrada(QrNFeCabAFatID.Value, QrNFeCabAIDCtrl.Value);
  ReopenSINTEGR_50(QrSINTEGR_50LinArq.Value);
end;

procedure TFmImportaSintegraGer.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmImportaSintegraGer.BtPeriodoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPeriodo, BtPeriodo);
end;

procedure TFmImportaSintegraGer.FormCreate(Sender: TObject);
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FEmpresa_Txt := '';
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  //
end;

procedure TFmImportaSintegraGer.SbNumeroClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Codigo(QrCadComItensCodigo.Value, LaRegistro.Caption);
end;

procedure TFmImportaSintegraGer.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmImportaSintegraGer.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrCadComItensCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmImportaSintegraGer.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmImportaSintegraGer.QrNFeCabAAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeItsI, Dmod.MyDB, [
  'SELECT gg1.Nome NO_GG1, nfei.* ',
  'FROM nfeitsi nfei ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=nfei.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'WHERE FatID=' + FormatFloat('0', QrNFeCabAFatID.Value),
  'AND FatNum=' + FormatFloat('0', QrNFeCabAFatNum.Value),
  'AND Empresa=' + FormatFloat('0', QrNFeCabAEmpresa.Value),
  '']);
end;

procedure TFmImportaSintegraGer.QrNFeCabABeforeClose(DataSet: TDataSet);
begin
  QrNFeItsI.Close;
end;

procedure TFmImportaSintegraGer.QrSINTEGR_10AfterOpen(DataSet: TDataSet);
begin
  //BtInclui.Enabled := True;
  BtExclui.Enabled := True;
  //BtAltera.Enabled := True;
end;

procedure TFmImportaSintegraGer.QrSINTEGR_10AfterScroll(DataSet: TDataSet);
begin
  FAnoMes_Int := QrSINTEGR_10AnoMes.Value;
  FAnoMes_Txt := FormatFloat('0', FAnoMes_Int);
  ReopenSINTEGR_50(0);
  ReopenSINTEGR_70(0);
  //
  BtSeqAcoes.Enabled := True;
end;

procedure TFmImportaSintegraGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmImportaSintegraGer.SbQueryClick(Sender: TObject);
begin
{
  LocCod(QrCadComItensCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cadcomitens', Dmod.MyDB, CO_VAZIO));
}
end;

{
function TFmImportaSintegraGer.VeriStr(Tam, Item: Integer; Texto: String): String;
begin
  Result := Texto;
  if Length(Result) <> Tam then
  Geral.MensagemBox('O item com tamanho inv�lido: ' + #13#10 +
  'Item n�mero: ' + FormatFloat('0', Item) + #13#10 +
  'Tamanho esperado: ' + FormatFloat('0', Tam) + #13#10 +
  'Tamanho do texto informado: ' + FormatFloat('0', Length(Result)) + #13#10 +
  'Texto informado: "' + Texto + '"', 'Erro', MB_OK+MB_ICONERROR);
end;
}

procedure TFmImportaSintegraGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImportaSintegraGer.frxSPEDEFD_PRINT_000_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VFR_EMPRESA' then Value := MLAGeral.CampoReportTxt(CBEmpresa.Text, 'TODAS')
  else if VarName = 'VARF_PERIODO' then Value := QrSINTEGR_10MES_ANO.Value
  else if VarName = 'VARF_GG2_VISIBLE' then Value := PertenceAoGrupoGG2()
  else if VarName = 'VARF_PGT_VISIBLE' then Value := PertenceAoGrupoPGT()
  else if VarName = 'VARF_PRD_VISIBLE' then Value := PertenceAoGrupoPrd()
  else if VarName = 'DATA_EM_TXT' then Value := Geral.FDT(DefineDataFim(), 2)
{
  else if VarName = 'VFR_NO_PGT' then Value := MLAGeral.CampoReportTxt(CBPrdGrupTip.Text, 'TODOS')
  else if VarName = 'VFR_NO_PRD' then Value := MLAGeral.CampoReportTxt(CBGraGru1.Text, 'TODOS')
  else if VarName = 'DATAHORA_TXT' then Value := Geral.FDT(FAgora, 8)
  else if VarName = 'PERIODO_TXT' then Value := MLAGeral.PeriodoImp(
    TPDataIni1.Date, TPDataFim1.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'VARF_PGT_VISIBLE' then Value := PertenceAoGrupoPGT()
  else if VarName = 'VARF_PRD_VISIBLE' then Value := PertenceAoGrupoPrd()
  else if VarName = 'VFR_LISTA_PRECO' then Value := CBGraCusPrc.Text
  //
  else if VarName = 'PERIODO_TXT_5' then Value := MLAGeral.PeriodoImp(
    TPDataIni5.Date, TPDataFim5.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'VFR_FILTRO_5' then
  begin
   Value := 'TUDO';
    case CBFiltro5.Value of
      //0: CBFiltro5.SetMaxValue; // Tudo
      1: Value := 'Baixa correta';
      2: Value := 'Sem baixa';
      3: Value := 'Baixa correta ou sem baixa';
      4: Value := 'Erro baixa';
      5: Value := 'Baixa correta ou incorreta';
      6: Value := 'Baixa incorreta ou sem baixa';
      7: ; //Tudo
    end;
  end;
}
end;

procedure TFmImportaSintegraGer.QrSINTEGR_10BeforeClose(DataSet: TDataSet);
begin
  FAnoMes_Int := 0;
  FAnoMes_Txt := '';
  BtExclui.Enabled := False;
  //BtAltera.Enabled := False;
  BtSeqAcoes.Enabled := False;
  //
  QrSINTEGR_50.Close;
  QrSINTEGR_70.Close;
end;

procedure TFmImportaSintegraGer.QrSINTEGR_50AfterOpen(DataSet: TDataSet);
begin
  BtNF.Enabled := QrSINTEGR_50.RecordCount > 0;
end;

procedure TFmImportaSintegraGer.QrSINTEGR_50AfterScroll(DataSet: TDataSet);
begin
  ReopenAtrelado();
end;

procedure TFmImportaSintegraGer.QrSINTEGR_50BeforeClose(DataSet: TDataSet);
begin
  BtNF.Enabled := False;
  QrNFeCabA.Close;
  QrSINTEGR_54.Close;
end;

procedure TFmImportaSintegraGer.QrSINTEGR_50CalcFields(DataSet: TDataSet);
begin
  if QrSINTEGR_50FatNum.Value <> 0 then
    QrSINTEGR_50ACHOU_TXT.Value := 'SIM'
  else
    QrSINTEGR_50ACHOU_TXT.Value := 'N�O'
end;

end.

