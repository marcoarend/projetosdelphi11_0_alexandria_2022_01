unit Sintegra_PesqPrd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables;

type
  TFmSintegra_PesqPrd = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TdmkEdit;
    DBGrid1: TDBGrid;
    QrLocPrdNom: TmySQLQuery;
    QrLocPrdNomGraGruX: TIntegerField;
    DsLocPrdNom: TDataSource;
    QrLocPrdNomCU_CodUsu: TIntegerField;
    QrLocPrdNomGraGru1: TIntegerField;
    QrLocPrdNomNO_PRD_TAM_COR: TWideStringField;
    QrLocPrdNomPrdGrupTip: TIntegerField;
    EdNaoLoc_TXT: TEdit;
    Label2: TLabel;
    EdNaoLoc_COD: TEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure QrLocPrdNomBeforeClose(DataSet: TDataSet);
    procedure QrLocPrdNomAfterOpen(DataSet: TDataSet);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FGraGruX, FNivel1: Integer;
  end;

  var
  FmSintegra_PesqPrd: TFmSintegra_PesqPrd;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmSintegra_PesqPrd.BtOKClick(Sender: TObject);
begin
  FGraGruX := QrLocPrdNomGraGruX.Value;
  FNivel1  := QrLocPrdNomGraGru1.Value;
  if FGraGruX <> 0 then
    Close;
  Close;
end;

procedure TFmSintegra_PesqPrd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSintegra_PesqPrd.DBGrid1DblClick(Sender: TObject);
begin
  FGraGruX := QrLocPrdNomGraGruX.Value;
  FNivel1  := QrLocPrdNomGraGru1.Value;
  if FGraGruX <> 0 then
    Close;
end;

procedure TFmSintegra_PesqPrd.EdPesqChange(Sender: TObject);
begin
  QrLocPrdNom.Close;
  QrLocPrdNom.Params[0].AsString := EdPesq.Text;
  QrLocPrdNom.Open;
  //
end;

procedure TFmSintegra_PesqPrd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSintegra_PesqPrd.FormCreate(Sender: TObject);
begin
  FGraGruX := 0;
  FNivel1  := 0;
end;

procedure TFmSintegra_PesqPrd.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmSintegra_PesqPrd.QrLocPrdNomAfterOpen(DataSet: TDataSet);
begin
  BtOK.Enabled := QrLocPrdNom.RecordCount > 0;
end;

procedure TFmSintegra_PesqPrd.QrLocPrdNomBeforeClose(DataSet: TDataSet);
begin
  BtOK.Enabled := False;
end;

end.
