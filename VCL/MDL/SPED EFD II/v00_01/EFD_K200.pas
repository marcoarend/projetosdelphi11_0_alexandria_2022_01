unit EFD_K200;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup;

type
  TFmEFD_K200 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdK100: TdmkEdit;
    Label6: TLabel;
    TPDT_EST: TdmkEditDateTimePicker;
    Label2: TLabel;
    EdLinArq: TdmkEdit;
    Label9: TLabel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXEx_TIPI: TWideStringField;
    DsGraGruX: TDataSource;
    Panel5: TPanel;
    EdGraGruX: TdmkEditCB;
    Label232: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    StaticText2: TStaticText;
    EdQTD: TdmkEdit;
    RGIND_EST: TdmkRadioGroup;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    DsEntidades: TDataSource;
    LaCOD_PART: TLabel;
    EdCOD_PART: TdmkEditCB;
    CBCOD_PART: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGIND_ESTClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGruX();
  public
    { Public declarations }
    FBalID, FBalNum, FBalItm, FBalEnt: Integer;
  end;

  var
  FmEFD_K200: TFmEFD_K200;

implementation

uses UnMyObjects, Module, EFD_E001, UMySQLModule, DmkDAC_PF, MyListas,
  ModuleFin, UnFinanceiro;

{$R *.DFM}

procedure TFmEFD_K200.BtOKClick(Sender: TObject);
const
  REG = 'K200';
var
  UNID, IND_PROP, TXT_COMPL, COD_CTA: String;
  //
  GraGruX: Integer;
var
  DT_EST, COD_ITEM, IND_EST, COD_PART: String;
  ImporExpor, AnoMes, Empresa, LinArq, K100, BalID, BalNum, BalItm, BalEnt: Integer;
  QTD: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  LinArq         := EdLinArq.ValueVariant;
  K100           := EdK100.ValueVariant;
  DT_EST         := Geral.FDT(TPDT_EST.Date, 1);
  COD_ITEM       := Geral.FF0(GraGruX);
  QTD            := EdQTD.ValueVariant;
  IND_EST        := Geral.FF0(RGIND_EST.ItemIndex);
  COD_PART       := EdCOD_PART.ValueVariant;
  //
  GraGruX        := EdGraGruX.ValueVariant;
  //
  if SQLType = stIns then
  begin
    BalID          := Integer(TSPED_EFD_Bal.sebalEFD_K200);
    BalNum         := K100;
    BalItm         := LinArq;
    BalEnt         := EdCOD_PART.ValueVariant;
  end else
  begin
    BalID          := FBalID;
    BalNum         := FBalNum;
    BalItm         := FBalItm;
    BalEnt         := FBalEnt;
  end;
  //
  if COD_PART = '0' then
    COD_PART := '';
  //
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o c�digo do produto!') then
    Exit;
  if MyObjects.FIC(Trim(UNID) = '', EdGraGruX,
    'C�digo do produto sem sigla de unidade definida!') then Exit;
  if MyObjects.FIC((RGIND_EST.ItemIndex < 0) or (RGIND_EST.ItemIndex > 2), RGIND_EST,
    'Informe o indicador de propriedade/posse!') then Exit;
  if MyObjects.FIC((EdCOD_PART.ValueVariant <> 0) and (RGIND_EST.ItemIndex = 0), EdCOD_PART,
    'N�o informe o propriet�rio/possuidor quando for o informante!') then Exit;
  if MyObjects.FIC((EdCOD_PART.ValueVariant = 0) and (RGIND_EST.ItemIndex <> 0), EdCOD_PART,
    'Informe o propriet�rio/possuidor quando n�o for o informante!') then Exit;
  //
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_k200', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  ImgTipo.SQLType, LinArq, siPositivo, EdLinArq);
  //
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efd_k200', False, [
  'REG', 'K100', 'DT_EST',
  'COD_ITEM', 'QTD', 'IND_EST',
  'COD_PART', 'BalID', 'BalNum',
  'BalItm', 'BalEnt'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  REG, K100, DT_EST,
  COD_ITEM, QTD, IND_EST,
  COD_PART, BalID, BalNum,
  BalItm, BalEnt], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
    // Nao tem totais
    //FmEFD_E001.AtualizaValoresK100deK200();
    FmEFD_E001.ReopenEFD_K200(LinArq, rkGrade);
    Close;
  end;
end;

procedure TFmEFD_K200.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_K200.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_K200.FormCreate(Sender: TObject);
begin
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  FBalID  := 0;
  FBalNum := 0;
  FBalItm := 0;
  FBalEnt := 0;
  ReopenGraGruX();
end;

procedure TFmEFD_K200.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEFD_K200.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
begin
  SQL_AND := '';
  SQL_LFT := '';
  if TdmkAppID(CO_DMKID_APP) = dmkappB_L_U_E_D_E_R_M then
  begin
    SQL_AND := 'AND NOT (pqc.PQ IS NULL)';
    SQL_LFT := 'LEFT JOIN pqcli pqc ON pqc.PQ=gg1.Nivel1';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  SQL_LFT,
  'WHERE ggx.Controle > -900000 ',
  SQL_AND,
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

procedure TFmEFD_K200.RGIND_ESTClick(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (RGIND_EST.ItemIndex > 0) and (RGIND_EST.ItemIndex < 3);
  LaCOD_PART.Enabled := Habilita;
  EdCOD_PART.Enabled := Habilita;
  CBCOD_PART.Enabled := Habilita;
  if not Habilita then
  begin
    EdCOD_PART.ValueVariant := 0;
    CBCOD_PART.KeyValue     := 0;
  end;
end;

end.
