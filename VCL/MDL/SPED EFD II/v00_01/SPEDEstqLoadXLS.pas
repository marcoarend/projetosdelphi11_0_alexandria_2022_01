unit SPEDEstqLoadXLS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls, Grids,
  ComObj, ComCtrls, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  DBGrids, dmkDBGridDAC, frxClass, frxDBSet, Menus, UnDmkEnums;

type
  TFmSPEDEstqLoadXLS = class(TForm)
    PainelConfirma: TPanel;
    BtAbrir: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    OpenDialog1: TOpenDialog;
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    Grade1: TStringGrid;
    BtSalva: TBitBtn;
    QrXcelCfgCab: TmySQLQuery;
    DsXcelCfgCab: TDataSource;
    QrXcelCfgCabCodigo: TIntegerField;
    QrXcelCfgCabCodUsu: TIntegerField;
    QrXcelCfgCabNome: TWideStringField;
    QrXcelCfgCabLinTit: TIntegerField;
    QrXcelCfgCabLinIni: TIntegerField;
    QrXcelCfgIts: TmySQLQuery;
    QrXcelCfgItsCodigo: TIntegerField;
    QrXcelCfgItsControle: TIntegerField;
    QrXcelCfgItsColuna: TWideStringField;
    QrXcelCfgItsNome: TWideStringField;
    QrXcelCfgItsDataType: TSmallintField;
    QrXcelCfgItsCasas: TSmallintField;
    QrXcelCfgItsTabSource: TWideStringField;
    QrXcelCfgItsFldSource: TWideStringField;
    QrXcelCfgItsTabDest: TWideStringField;
    QrXcelCfgItsFldDest: TWideStringField;
    QrXcelCfgItsID_TabFld: TIntegerField;
    QrXcelCfgItsNO_TabFld: TWideStringField;
    QrXcelCfgItsNO_DataType: TWideStringField;
    Grade0: TStringGrid;
    TabSheet3: TTabSheet;
    MeErros: TMemo;
    TabSheet1: TTabSheet;
    QrMeses: TmySQLQuery;
    DsMeses: TDataSource;
    QrItens: TmySQLQuery;
    DsItens: TDataSource;
    QrMesesAnoMes: TIntegerField;
    QrMesesEstqIniQtd: TFloatField;
    QrMesesEstqIniPrc: TFloatField;
    QrMesesEstqIniVal: TFloatField;
    QrMesesComprasQtd: TFloatField;
    QrMesesComprasPrc: TFloatField;
    QrMesesComprasVal: TFloatField;
    QrMesesConsumoQtd: TFloatField;
    QrMesesConsumoPrc: TFloatField;
    QrMesesConsumoVal: TFloatField;
    QrMesesEstqFimQtd: TFloatField;
    QrMesesEstqFimPrc: TFloatField;
    QrMesesEstqFimVal: TFloatField;
    QrMesesAtivo: TSmallintField;
    QrMesesMES_ANO: TWideStringField;
    QrIts: TmySQLQuery;
    QrItsLinArq: TIntegerField;
    QrItsLinPla: TIntegerField;
    QrItsAnoMes: TIntegerField;
    QrItsEmpresa: TIntegerField;
    QrItsSeqLin: TIntegerField;
    QrItsGraGruX: TIntegerField;
    QrItsSit_Prod: TSmallintField;
    QrItsTerceiro: TIntegerField;
    QrItsEstqIniQtd: TFloatField;
    QrItsEstqIniPrc: TFloatField;
    QrItsEstqIniVal: TFloatField;
    QrItsComprasQtd: TFloatField;
    QrItsComprasPrc: TFloatField;
    QrItsComprasVal: TFloatField;
    QrItsConsumoQtd: TFloatField;
    QrItsConsumoPrc: TFloatField;
    QrItsConsumoVal: TFloatField;
    QrItsEstqFimQtd: TFloatField;
    QrItsEstqFimPrc: TFloatField;
    QrItsEstqFimVal: TFloatField;
    QrItsAtivo: TSmallintField;
    QrItsNome: TWideStringField;
    QrErros: TmySQLQuery;
    QrErrosLinArq: TIntegerField;
    TabSheet4: TTabSheet;
    Panel6: TPanel;
    Panel4: TPanel;
    DBGrid1: TdmkDBGridDAC;
    Panel5: TPanel;
    DBGrid2: TDBGrid;
    QrErrosErro: TIntegerField;
    Panel7: TPanel;
    QrErrosNO_ERRO: TWideStringField;
    DsErros: TDataSource;
    DBGrid3: TDBGrid;
    QrItsPlanilha: TWideStringField;
    QrItsCelula: TWideStringField;
    QrErrosLinPla: TIntegerField;
    QrErrosPlanilha: TWideStringField;
    QrErrosCelula: TWideStringField;
    QrAntNew: TmySQLQuery;
    QrAntNewEstqFimQtd: TFloatField;
    QrAntNewEstqFimVal: TFloatField;
    QrAntOld: TmySQLQuery;
    QrAntOldEstqFimQtd: TFloatField;
    QrAntOldEstqFimVal: TFloatField;
    BtVincula: TBitBtn;
    QrErrosNome: TWideStringField;
    BtConfere: TBitBtn;
    QrLocGGX: TmySQLQuery;
    QrLocGGXGraGruX: TIntegerField;
    BtImprime2: TBitBtn;
    frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_01: TfrxReport;
    frxDsErros: TfrxDBDataset;
    QrErr2: TmySQLQuery;
    QrErr2Erro: TIntegerField;
    QrErr2Nome: TWideStringField;
    QrErr2Its: TmySQLQuery;
    QrErr2ItsLinArq: TIntegerField;
    QrErr2ItsErro: TIntegerField;
    QrErr2ItsLinPla: TIntegerField;
    QrErr2ItsPlanilha: TWideStringField;
    QrErr2ItsCelula: TWideStringField;
    QrErr2ItsNome: TWideStringField;
    GroupBox1: TGroupBox;
    CkErros: TCheckBox;
    CkAdver: TCheckBox;
    PMImprime2: TPopupMenu;
    Listadeavisos1: TMenuItem;
    Agruparportiponomedoproduto1: TMenuItem;
    frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_02: TfrxReport;
    frxDsErr2: TfrxDBDataset;
    frxDsErr2Its: TfrxDBDataset;
    QrErr2NO_ERRO: TWideStringField;
    Apenaslistarprodutos1: TMenuItem;
    QrErr2GraGruX: TIntegerField;
    frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_03: TfrxReport;
    QrProds: TmySQLQuery;
    frxDsProds: TfrxDBDataset;
    QrProdsGraGruX: TIntegerField;
    QrProdsNome: TWideStringField;
    QrProdsSEQ: TIntegerField;
    Panel8: TPanel;
    PB1: TProgressBar;
    LaAviso2: TLabel;
    LaAviso1: TLabel;
    Panel9: TPanel;
    Label1: TLabel;
    EdArquivo: TEdit;
    Label2: TLabel;
    EdXcelCfgCab: TdmkEditCB;
    CBXcelCfgCab: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    RGModeloPla_: TRadioGroup;
    QrItensItens: TLargeintField;
    QrItensGraGruX: TIntegerField;
    QrItensSit_Prod: TSmallintField;
    QrItensTerceiro: TIntegerField;
    QrItensEstqIniQtd: TFloatField;
    QrItensEstqIniPrc: TFloatField;
    QrItensEstqIniVal: TFloatField;
    QrItensComprasQtd: TFloatField;
    QrItensComprasPrc: TFloatField;
    QrItensComprasVal: TFloatField;
    QrItensConsumoQtd: TFloatField;
    QrItensConsumoPrc: TFloatField;
    QrItensConsumoVal: TFloatField;
    QrItensEstqFimQtd: TFloatField;
    QrItensEstqFimPrc: TFloatField;
    QrItensEstqFimVal: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure BtSalvaClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdArquivoChange(Sender: TObject);
    procedure EdXcelCfgCabChange(Sender: TObject);
    procedure QrXcelCfgCabBeforeClose(DataSet: TDataSet);
    procedure QrXcelCfgCabAfterScroll(DataSet: TDataSet);
    procedure MeErrosChange(Sender: TObject);
    procedure CkErrosClick(Sender: TObject);
    procedure QrErrosCalcFields(DataSet: TDataSet);
    procedure BtVinculaClick(Sender: TObject);
    procedure QrErrosAfterScroll(DataSet: TDataSet);
    procedure QrErrosBeforeClose(DataSet: TDataSet);
    procedure BtConfereClick(Sender: TObject);
    procedure QrErrosAfterOpen(DataSet: TDataSet);
    procedure BtImprime2Click(Sender: TObject);
    procedure frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_01GetValue(
      const VarName: string; var Value: Variant);
    procedure QrErr2BeforeClose(DataSet: TDataSet);
    procedure QrErr2AfterScroll(DataSet: TDataSet);
    procedure Listadeavisos1Click(Sender: TObject);
    procedure Agruparportiponomedoproduto1Click(Sender: TObject);
    procedure QrErr2CalcFields(DataSet: TDataSet);
    procedure Apenaslistarprodutos1Click(Sender: TObject);
    procedure QrProdsCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FColunas: array of Integer;
    FTitulos: array of String;
    FFldIdx: array of Integer;
    FFldNom: array of String;
    FUsaFld: array of Boolean;
    FFormat: array of Integer;
    function DefineColunas(): Boolean;
    function  DefineVariantDeTexto(Planilha, Celula: String;
              Formato: Integer; Texto: String): Variant;
    procedure HabilitaAbertura();
    function ItensNaoTemErro(): Boolean;
    procedure ReopenErros(LinArq: Integer);
    procedure VerificaAbertura();
    function Xls_To_StringGrid(AGrid, xGrid: TStringGrid; AXLSFile: string): Boolean;
    function NomeDeErro(Erro: Integer): String;
  public
    { Public declarations }
    FEmpresa, FSit_Prod, FTerceiro: Integer;
    FNomes: array of String;
  end;

  var
  FmSPEDEstqLoadXLS: TFmSPEDEstqLoadXLS;

implementation

uses UnMyObjects, Module, UCreate, ModuleGeral, UnInternalConsts,
  UMySQLModule, XcelCfgCab, MyDBCheck, UnGrade_Create, GraGruRSel, SPEDEstqGer,
  DmkDAC_PF;

const
  FID_TabFld = 15;

{$R *.DFM}

procedure TFmSPEDEstqLoadXLS.BtConfereClick(Sender: TObject);
begin
  VerificaAbertura();
end;

procedure TFmSPEDEstqLoadXLS.BtImprime2Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime2, BtImprime2);
end;

procedure TFmSPEDEstqLoadXLS.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSPEDEstqLoadXLS.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSPEDEstqLoadXLS.FormCreate(Sender: TObject);
var
  CfgCod: Integer;
begin
  PageControl1.ActivePageIndex := 0;
  FEmpresa := 0;
  FSit_Prod := 0;
  FTerceiro := 0;
  SetLength(FNomes, FID_TabFld + 1);
  FNomes[00] := '';
  FNomes[01] := 'Sequ�ncia (linha)';
  FNomes[02] := 'Reduzido do produto';
  FNomes[03] := 'Nome do produto';
  FNomes[04] := 'Pre�o m�dio inicial';
  FNomes[05] := 'Estoque inicial (kg)';
  FNomes[06] := 'Estoque inicial (R$)';
  FNomes[07] := 'Pre�o m�dio compras';
  FNomes[08] := 'Compras (kg)';
  FNomes[09] := 'Compras (R$)';
  FNomes[10] := 'Pre�o m�dio consumo';
  FNomes[11] := 'Consumo (R$)';
  FNomes[12] := 'Consumo (kg)';
  FNomes[13] := 'Pre�o m�dio final';
  FNomes[14] := 'Estoque final (kg)';
  FNomes[15] := 'Estoque final (R$)';
  //
  UnDmkDAC_PF.AbreQuery(QrXcelCfgCab, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  Grade0.Cells[0,0] := 'Seq';
  Grade0.Cells[1,0] := 'Lin';
  Grade0.Cells[2,0] := 'Ano';
  Grade0.Cells[3,0] := 'M�s';
  Grade0.Cells[4,0] := 'Planilha';
  //
  QrMeses.Close;
  QrMeses.Database := DModG.MyPID_DB;
  //
  QrItens.Close;
  QrItens.Database := DModG.MyPID_DB;
  //
  QrIts.Close;
  QrIts.Database := DModG.MyPID_DB;
  //
  QrErros.Close;
  QrErros.Database := DModG.MyPID_DB;
  //
  QrAntNew.Close;
  QrAntNew.Database := DModG.MyPID_DB;
  //
  QrErr2.Close;
  QrErr2.Database := DModG.MyPID_DB;
  //
  QrErr2Its.Close;
  QrErr2Its.Database := DModG.MyPID_DB;
  //
  QrProds.Close;
  QrProds.Database := DModG.MyPID_DB;
  //
  EdArquivo.Text := Geral.ReadAppKey('SPEDESTQ\Arquivo', Application.Title,
    ktString, '', HKEY_LOCAL_MACHINE);
  CfgCod := Geral.ReadAppKey('SPEDESTQ\Config', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  if CfgCod <> 0 then
  begin
    EdXcelCfgCab.ValueVariant := CfgCod;
    CBXcelCfgCab.KeyValue     := CfgCod;
  end;
end;

procedure TFmSPEDEstqLoadXLS.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmSPEDEstqLoadXLS.frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_01GetValue(
  const VarName: string; var Value: Variant);
begin
  if VarName = 'VFR_EMPRESA' then Value := '???????'
  else
  if VarName = 'VAR_TIPO_REL' then
  begin
    if CkErros.Checked and CkAdver.Checked then
      Value := 'ERROS E ADVERT�NCIAS'
    else if CkErros.Checked then
      Value := 'ERROS'
    else if CkAdver.Checked then
      Value := 'ADVERT�NCIAS'
    else
      Value := '?????';
  end;
end;

procedure TFmSPEDEstqLoadXLS.SpeedButton1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
    EdArquivo.Text := OpenDialog1.FileName;
end;

procedure TFmSPEDEstqLoadXLS.SpeedButton2Click(Sender: TObject);
var
  I(*, K*): Integer;
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmXcelCfgCab, FmXcelCfgCab, afmoNegarComAviso) then
  begin
    //FmXcelCfgCab.FCodigos := FCodigos;
    SetLength(FmXcelCfgCab.FNomes,  FID_TabFld + 1);
    for I := 0 to FID_TabFld do
      FmXcelCfgCab.FNomes[I] := FNomes[I];
    FmXcelCfgCab.FListaTabFld := UCriar.RecriaTempTableNovo(
      ntrtt_CodNom, DmodG.QrUpdPID1, False, 1, '_tab_fld_');
    UCriar.InsereRegistrosCodNom(DModG.QrUpdPID1, FmXcelCfgCab.FListaTabFld, FNomes);
    FmXcelCfgCab.ShowModal;
    FmXcelCfgCab.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodigoPesquisado(
        EdXcelCfgCab, CBXcelCfgCab, QrXcelCfgCab, VAR_CADASTRO);
  end;
end;

procedure TFmSPEDEstqLoadXLS.VerificaAbertura();
begin
  if ItensNaoTemErro() then
  begin
    BtSalva.Enabled := True;
    BtAbrir.Enabled := False;
    PageControl1.ActivePageIndex := 3;
  end else PageControl1.ActivePageIndex := 2;
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM _spedestq_sum_;');
  DModG.QrUpdPID1.SQL.Add('INSERT INTO _spedestq_sum_');
  DModG.QrUpdPID1.SQL.Add('SELECT AnoMes, SUM(EstqIniQtd) EstqIniQtd,');
  DModG.QrUpdPID1.SQL.Add('SUM(EstqIniPrc*EstqIniQtd)/ SUM(EstqIniQtd) EstqIniPrc,');
  DModG.QrUpdPID1.SQL.Add('SUM(EstqIniVal) EstqIniVal, SUM(ComprasQtd) ComprasQtd,');
  DModG.QrUpdPID1.SQL.Add('SUM(ComprasPrc*ComprasQtd)/ SUM(ComprasQtd) ComprasPrc,');
  DModG.QrUpdPID1.SQL.Add('SUM(ComprasVal) ComprasVal, SUM(ConsumoQtd) ConsumoQtd,');
  DModG.QrUpdPID1.SQL.Add('SUM(ConsumoPrc*ConsumoQtd)/ SUM(ConsumoQtd) ConsumoPrc,');
  DModG.QrUpdPID1.SQL.Add('SUM(ConsumoVal) ConsumoVal, SUM(EstqFimQtd) EstqFimQtd,');
  DModG.QrUpdPID1.SQL.Add('SUM(EstqFimPrc*EstqFimQtd)/ SUM(EstqFimQtd) EstqFimPrc,');
  DModG.QrUpdPID1.SQL.Add('SUM(EstqFimVal) EstqFimVal, 0 Ativo');
  DModG.QrUpdPID1.SQL.Add('FROM _spedestq_its_');
  DModG.QrUpdPID1.SQL.Add('GROUP BY AnoMes');
  DModG.QrUpdPID1.SQL.Add('ORDER BY AnoMes;');
  DModG.QrUpdPID1.ExecSQL;
  //
  QrMeses.Close;
  UnDmkDAC_PF.AbreQuery(QrMeses, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  BtConfere.Enabled := False;
  MyObjects.Informa(LaAviso1, False, '...');
end;

procedure TFmSPEDEstqLoadXLS.Agruparportiponomedoproduto1Click(Sender: TObject);
begin
  QrErr2.Close;
  QrErr2.SQL.Clear;
  if CkErros.Checked or CkAdver.Checked then
  begin
    QrErr2.SQL.Add('SELECT DISTINCT Erro, GraGruX, Nome');
    QrErr2.SQL.Add('FROM _spedestq_err_');
    if CkErros.Checked and CkAdver.Checked then
       (*nada de where ou seja: todos erros e advert�ncias!*)
    else if CkErros.Checked then
      QrErr2.SQL.Add('WHERE Erro >0')
    else if CkAdver.Checked then
      QrErr2.SQL.Add('WHERE Erro <0');
    //
    QrErr2.SQL.Add('ORDER BY Erro DESC, GraGruX, Nome');
    UnDmkDAC_PF.AbreQuery(QrErr2, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    //MLAGeral.LeMeuSQLy(QrErr2, '', nil, False, True);
    MyObjects.frxMostra(frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_02,
      'Erros / advert�ncias de importa��o de Excel');
  end else Geral.MensagemBox('Defina os avisos de importa��o antes de imprimir!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmSPEDEstqLoadXLS.Apenaslistarprodutos1Click(Sender: TObject);
begin
  QrProds.Close;
  UnDmkDAC_PF.AbreQuery(QrProds, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  MyObjects.frxMostra(frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_03,
    'Lista de produtos sem c�digo em importa��o de Excel');
end;

procedure TFmSPEDEstqLoadXLS.BtAbrirClick(Sender: TObject);
const
  FaltaInfoCfg = 'Informe a configura��o!';
var
  XcelCfgCab: Integer;
  //Arquivo: String;
  Erros: String;
  //Continua: Boolean;
begin
  MyObjects.Informa(LaAviso1, True, 'Recriando arquivo tempor�rio');
  GradeCriar.RecriaTempTableNovo(ntrttSPEDESTQ_ITS, DModG.QrUpdPID1, False);
  GradeCriar.RecriaTempTableNovo(ntrttSPEDESTQ_SUM, DModG.QrUpdPID1, False);
  GradeCriar.RecriaTempTableNovo(ntrttSPEDESTQ_ERR, DModG.QrUpdPID1, False);
  MyObjects.Informa(LaAviso1, True, 'Verificando configura��es');
  MeErros.Lines.Clear;
  if XcelCfgCab = 0 then
  begin
    MyObjects.Informa(LaAviso1, True, FaltaInfoCfg);
    Geral.MensagemBox(FaltaInfoCfg, 'Aviso', MB_OK+MB_ICONWARNING);
    EdXcelCfgCab.SetFocus;
    Exit;
  end (*else XcelCfgCab := QrXcelCfgCabCodigo.Value*);
  if not DefineColunas() then
    Exit;
  PB1.Position := 0;
  XcelCfgCab := Geral.IMV(EdXcelCfgCab.Text);
  Erros := '';
  if QrXcelCfgCabLinIni.Value < 1 then
    Erros := Erros + '-> Linha: Linha inicial inv�lida' +Chr(13) +Chr(10);
  if Erros <> '' then
  begin
     Erros := 'A configura��o selecionada n�o ' +
     'est� cadastrada corretamente para importa��o de estoque em ' +
     'formato excel. Abaixo est� listado o que falta configurar para uma ' +
     'importa��o com o m�nimo de dados necess�rios para haver importa��o!' +
     #13#10 + #13#10 + Erros + #13#10 + #13#10 +
     'Esta configura��o deve ser corrigida.';
     Geral.MensagemBox(Erros, 'Erro', MB_OK+MB_ICONERROR);
     LaAviso1.Caption := '� necess�rio corrigir a configura��o!';
     Exit;
  end;
  Geral.WriteAppKey('SPEDESTQ\Config', Application.Title,
    XcelCfgCab, ktInteger, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('SPEDESTQ\Arquivo', Application.Title,
    EdArquivo.Text, ktString, HKEY_LOCAL_MACHINE);
  //
(*
  Continua := False;
  case RGModeloPla.ItemIndex of
    0: Geral.MensagemBox('Selecione o modelo de planilha!', 'Aviso', MB_OK+MB_ICONWARNING);
    1: Continua := Xls_To_StringGrid_IDEAL_2006(Grade1, Grade0, EdArquivo.Text);
    2: Continua := Xls_To_StringGrid_IDEAL_2011(Grade1, Grade0, EdArquivo.Text);
  end;
  if Continua then
*)
    if Xls_To_StringGrid(Grade1, Grade0, EdArquivo.Text) then
      VerificaAbertura();
end;

function TFmSPEDEstqLoadXLS.Xls_To_StringGrid(AGrid, xGrid: TStringGrid; AXLSFile: string): Boolean;
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
  //
  XlSLinhaInicial = 3;
  //
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  Row, col, k, r, Larg, m, n: Integer;
  ColWid: array[0..255] of Integer;
  //
  Z, B, WS, Ano, Mes: Integer;
  Nome_PLA, Celula, Planilha, TitCell, TitCfg, TxtCell: String;
  I, J: Integer;
  //
  Nome, SeqEGGX: String;
  LinArq, LinPla, SeqLin, AnoMes, Coluna, GraGruX: Integer;
  EstqIniQtd, EstqIniPrc, EstqIniVal, ComprasQtd, ComprasPrc, ComprasVal,
  ConsumoQtd, ConsumoPrc, ConsumoVal, EstqFimQtd, EstqFimPrc, EstqFimVal: Double;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  MyObjects.Informa(LaAviso1, True, 'Criando aplica��o Excel. Isso pode levar alguns segundos');
  XLApp := CreateOleObject('Excel.Application');
  try
    MyObjects.LimpaGrade(AGrid, 1, 1, True);
    MyObjects.LimpaGrade(xGrid, 1, 1, True);
    XLApp.Visible := False;
    MyObjects.Informa(LaAviso1, True, 'Abrindo arquivo excel selecionado');
    XLApp.Workbooks.Open(AXLSFile);
    k := 0;
    Z := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets.Count;
    for B := Z downto 1 do
    begin
      MyObjects.Informa(LaAviso1, True, 'Selecionando planilha do arquivo excel');
      XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[B].Select;
      Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[B];
      Nome_PLA := Sheet.Name;
      if Geral.ObtemAnoMesDeMesStrAno(Nome_PLA, Ano, Mes) then
      begin
        Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
        Row := XLApp.ActiveCell.Row;
        //y := XLApp.ActiveCell.Column;
        QrXcelCfgIts.Last;
        Col := Geral.IMV(MLAGeral.LetraToNumero(QrXcelCfgItsColuna.Value));
        //
        AGrid.RowCount := AGrid.RowCount + Row;
        xGrid.RowCount := xGrid.RowCount + Row;
        if AGrid.ColCount < Col + 1 then
          AGrid.ColCount := Col + 1;
        PB1.Position := 0;
        PB1.Max := Row * (Col + 1);
        MyObjects.Informa(LaAviso1, True, 'Lendo planilha "' + Nome_PLA + '" > ' + FormatFloat('0', B) + '/' +
        FormatFloat('0', Z) + ' do arquivo excel');
        Update;
        RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[Row, Col]].Value;
        WS := WS + k;
        k := 1;
        m := Round(CharWid * 3);
        for n := 0 to 255 do ColWid[n] := m;
        repeat
          Update;
          Application.ProcessMessages;
          AGrid.Cells[00, (WS + k)] := FormatFloat('000', (WS + k));
          //
          xGrid.Cells[00, (WS + k)] := FormatFloat('000', (WS + k));
          xGrid.Cells[01, (WS + k)] := FormatFloat('000', (k));
          xGrid.Cells[02, (WS + k)] := FormatFloat('0000', Ano);
          xGrid.Cells[03, (WS + k)] := FormatFloat('00', Mes);
          xGrid.Cells[04, (WS + k)] := Nome_PLA;
          for r := 1 to Col do
          begin
            PB1.Position := PB1.Position + 1;
            MyObjects.Informa(LaAviso2, True, 'Lendo c�lula ' +
              MLAGeral.ColRowToCelula(r, k));
            Update;
            Application.ProcessMessages;
            //
            AGrid.Cells[r, (WS + K)] := RangeMatrix[K, R];
            Larg := Round((Length(RangeMatrix[K, R]) + 1) * CharWid);
            if Larg > ColWid[r] then
              ColWid[r] := Larg;
          end;
          Inc(k, 1);
          AGrid.RowCount := WS + K + 2;
        until k > Row;
        RangeMatrix := Unassigned;
      end else Geral.MensagemBox('N�o foi poss�vel obter o ano e o m�s da pasta: ' +
      #13#10 + Nome_PLA + #13#10 + 'Esta pasta n�o ser� importada!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
    (*Application.MessageBox(PChar('Os dados foram importados com sucesso do '+
      'arquivo: '+Chr(13)+Chr(10)+EdArquivo.Text), 'Informa��o',
      MB_OK+MB_ICONINFORMATION);*)
    for n := 0 to AGrid.ColCount -1 do
      AGrid.ColWidths[n] := ColWid[n];
    //
    //  Le dados solicitados da Grade1 e grava na Grade 2
    //
    m := Round(CharWid * 3);
    for n := 0 to 255 do ColWid[n] := m;
    //
    MyObjects.Informa(LaAviso1, True, 'Gerando tabela provis�ria');
    PB1.Position := 0;
    Z := AGrid.RowCount;
    PB1.Max := Z;
    for I := 1 to Z do
    begin
      LinArq := Geral.IMV(xGrid.Cells[00, I]);
      LinPla := Geral.IMV(xGrid.Cells[01, I]);
      AnoMes := Geral.IMV(xGrid.Cells[02, I]) * 100 +
                Geral.IMV(xGrid.Cells[03, I]);
      if (LinArq > 0) and (AnoMes > 0) then
      begin
        if (LinPla > 0) and (LinPla = QrXcelCfgCabLinTit.Value) then
        begin
          // Vai comparar o t�tulo do excell com o informado no Blue Derm!
          for J := 1 to FID_TabFld do
          begin
            TitCell := AnsiLowercase(Trim(AGrid.Cells[J, LinPla]));
            TitCfg  := AnsiLowercase(Trim(FFldNom[J]));
            if FUsaFld[J] and (TitCell <> TitCfg) then
            begin
              Celula := MLAGeral.ColRowToCelula(J, I);
              Planilha := xGrid.Cells[04, I];
              Geral.MensagemArray(MB_ICONWARNING, 'Diferen�a de t�tulo',
              ['Planilha: ', 'C�lula: ', 'T�tulo que consta n� c�lula: ',
              'T�tulo esperado: '], [Planilha, Celula, TitCell,
              TitCfg], MeErros);
            end;
          end;
        end else
        if LinPla >= QrXcelCfgCabLinIni.Value then
        begin
          // Dados a serem registrados
          SeqLin := 0;
          GraGruX := 0;
          Nome := '';
          EstqIniPrc := 0;
          EstqIniQtd := 0;
          EstqIniVal := 0;
          ComprasPrc := 0;
          ComprasQtd := 0;
          ComprasVal := 0;
          ConsumoPrc := 0;
          ConsumoVal := 0;
          ConsumoQtd := 0;
          EstqFimPrc := 0;
          EstqFimQtd := 0;
          EstqFimVal := 0;
          // Verificar se � uma linha v�lida
          SeqEGGX :=
            Trim(AGrid.Cells[FColunas[01], I]) +  // SeqLin
            Trim(AGrid.Cells[FColunas[02], I]);   // GraGruX
          if (SeqEGGX <> '') and (SeqEGGX = Geral.SoNumero_TT(SeqEGGX)) then
          begin
            for J := 1 to FID_TabFld do
            begin
              Coluna := FColunas[J];
              if Coluna > 0 then
              begin
                Celula := MLAGeral.ColRowToCelula(J, LinPla);
                Planilha := xGrid.Cells[04, I];
                TxtCell := Trim(AGrid.Cells[Coluna, I]);
                case J of
                  01: // 'Sequ�ncia (linha)';
                    SeqLin := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  02: // 'Reduzido do produto';
                    GraGruX := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  03: // 'Nome do produto';
                    Nome := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  04: // 'Pre�o m�dio inicial';
                    EstqIniPrc := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  05: // 'Estoque inicial (kg)';
                    EstqIniQtd := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  06: // 'Estoque inicial (R$)';
                    EstqIniVal := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  07: // 'Pre�o m�dio compras';
                    ComprasPrc := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  08: // 'Compras (kg)';
                    ComprasQtd := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  09: // 'Compras (R$)';
                    ComprasVal := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  10: // 'Pre�o m�dio consumo';
                    ConsumoPrc := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  11: // 'Consumo (R$)';
                    ConsumoVal := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  12: // 'Consumo (kg)';
                    ConsumoQtd := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  13: // 'Pre�o m�dio final';
                    EstqFimPrc := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  14: // 'Estoque final (kg)';
                    EstqFimQtd := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  15: // 'Estoque final (R$)';
                    EstqFimVal := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                end;
              end;
            end;
            if GraGruX = 0 then
            begin
              QrLocGGX.Close;
              QrLocGGX.Params[0].AsString := Nome;
              UnDmkDAC_PF.AbreQuery(QrLocGGX, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
              //
              if QrLocGGX.RecordCount > 0 then
                GraGruX := QrLocGGXGraGruX.Value;
            end;
            UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_spedestq_its_', False, [
            'LinPla', 'AnoMes', 'Empresa',
            'SeqLin', 'GraGruX', 'Sit_Prod',
            'Terceiro', 'EstqIniQtd', 'EstqIniPrc',
            'EstqIniVal', 'ComprasQtd', 'ComprasPrc',
            'ComprasVal', 'ConsumoQtd', 'ConsumoPrc',
            'ConsumoVal', 'EstqFimQtd', 'EstqFimPrc',
            'EstqFimVal', 'Nome', 'Planilha', 'Celula'], [
            'LinArq'], [
            LinPla, AnoMes, FEmpresa,
            SeqLin, GraGruX, FSit_Prod,
            FTerceiro, EstqIniQtd, EstqIniPrc,
            EstqIniVal, ComprasQtd, ComprasPrc,
            ComprasVal, ConsumoQtd, ConsumoPrc,
            ConsumoVal, EstqFimQtd, EstqFimPrc,
            EstqFimVal, Nome, Planilha, Celula], [
            LinArq], False);
          end;
        end;
      end;
    end;
    //
    //
    MyObjects.Informa(LaAviso2, False, '...');
  finally
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
end;

{
function TFmSPEDEstqLoadXLS.Xls_To_StringGrid_IDEAL_2011(AGrid, xGrid: TStringGrid;
  AXLSFile: string): Boolean;
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
  //
  XlSLinhaInicial = 3;
  XlSLastCol = 13;
  //
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r, Larg, m, n: Integer;
  ColWid: array[0..255] of Integer;
  //
  Z, B, WS, Ano, Mes: Integer;
  Nome_PLA, Celula, Planilha, TitCell, TitCfg, TxtCell: String;
  I, J: Integer;
  //
  Nome, SeqEGGX: String;
  LinArq, LinPla, SeqLin, AnoMes, Coluna, GraGruX: Integer;
  EstqIniQtd, EstqIniPrc, EstqIniVal, ComprasQtd, ComprasPrc, ComprasVal,
  ConsumoQtd, ConsumoPrc, ConsumoVal, EstqFimQtd, EstqFimPrc, EstqFimVal: Double;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  MyObjects.Informa(LaAviso1, True, 'Criando aplica��o Excel. Isso pode levar alguns segundos');
  XLApp := CreateOleObject('Excel.Application');
  try
    MyObjects.LimpaGrade(AGrid, 1, 1, True);
    MyObjects.LimpaGrade(xGrid, 1, 1, True);
    XLApp.Visible := False;
    MyObjects.Informa(LaAviso1, True, 'Abrindo arquivo excel selecionado');
    XLApp.Workbooks.Open(AXLSFile);
    k := 0;
    Z := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets.Count;
    for B := Z downto 1 do
    begin
      MyObjects.Informa(LaAviso1, True, 'Selecionando planilha do arquivo excel');
      XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[B].Select;
      Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[B];
      Nome_PLA := Sheet.Name;
      if Geral.ObtemAnoMesDeMesStrAno(Nome_PLA, Ano, Mes) then
      begin
        Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
        x := XLApp.ActiveCell.Row;
        y := XLApp.ActiveCell.Column;
        AGrid.RowCount := AGrid.RowCount + x;
        xGrid.RowCount := xGrid.RowCount + x;
        if AGrid.ColCount < y + 1 then
          AGrid.ColCount := y + 1;
        PB1.Position := 0;
        PB1.Max := x * (y + 1);
        MyObjects.Informa(LaAviso1, True, 'Lendo planilha "' + Nome_PLA + '" > ' + FormatFloat('0', B) + '/' +
        FormatFloat('0', Z) + ' do arquivo excel');
        Update;
        RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;
        WS := WS + k;
        k := 1;
        m := Round(CharWid * 3);
        for n := 0 to 255 do ColWid[n] := m;
        repeat
          Update;
          Application.ProcessMessages;
          AGrid.Cells[00, (WS + k)] := FormatFloat('000', (WS + k));
          //
          xGrid.Cells[00, (WS + k)] := FormatFloat('000', (WS + k));
          xGrid.Cells[01, (WS + k)] := FormatFloat('000', (k));
          xGrid.Cells[02, (WS + k)] := FormatFloat('0000', Ano);
          xGrid.Cells[03, (WS + k)] := FormatFloat('00', Mes);
          xGrid.Cells[04, (WS + k)] := Nome_PLA;
          for r := 1 to (*y*) XLSLastCol do
          begin
            PB1.Position := PB1.Position + 1;
            MyObjects.Informa(LaAviso2, True, 'Lendo c�lula ' +
              MLAGeral.ColRowToCelula(r, k));
            Update;
            Application.ProcessMessages;
            //
            AGrid.Cells[r, (WS + K)] := RangeMatrix[K, R];
            Larg := Round((Length(RangeMatrix[K, R]) + 1) * CharWid);
            if Larg > ColWid[r] then
              ColWid[r] := Larg;
          end;
          Inc(k, 1);
          AGrid.RowCount := WS + K + 2;
        until k > x;
        RangeMatrix := Unassigned;
      end else Geral.MensagemBox('N�o foi poss�vel obter o ano e o m�s da pasta: ' +
      #13#10 + Nome_PLA + #13#10 + 'Esta pasta n�o ser� importada!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
    (*Application.MessageBox(PChar('Os dados foram importados com sucesso do '+
      'arquivo: '+Chr(13)+Chr(10)+EdArquivo.Text), 'Informa��o',
      MB_OK+MB_ICONINFORMATION);*)
    for n := 0 to AGrid.ColCount -1 do
      AGrid.ColWidths[n] := ColWid[n];
    //
    //  Le dados solicitados da Grade1 e grava na Grade 2
    //
    m := Round(CharWid * 3);
    for n := 0 to 255 do ColWid[n] := m;
    //
    MyObjects.Informa(LaAviso1, True, 'Gerando tabela provis�ria');
    PB1.Position := 0;
    Z := AGrid.RowCount;
    PB1.Max := Z;
    for I := 1 to Z do
    begin
      LinArq := Geral.IMV(xGrid.Cells[00, I]);
      LinPla := Geral.IMV(xGrid.Cells[01, I]);
      AnoMes := Geral.IMV(xGrid.Cells[02, I]) * 100 +
                Geral.IMV(xGrid.Cells[03, I]);
      if (LinArq > 0) and (AnoMes > 0) then
      begin
        if (LinPla > 0) and (LinPla = QrXcelCfgCabLinTit.Value) then
        begin
          // Vai comparar o t�tulo do excell com o informado no Blue Derm!
          for J := 1 to FID_TabFld do
          begin
            TitCell := AnsiLowercase(Trim(AGrid.Cells[J, LinPla]));
            TitCfg  := AnsiLowercase(Trim(FFldNom[J]));
            if FUsaFld[J] and (TitCell <> TitCfg) then
            begin
              Celula := MLAGeral.ColRowToCelula(J, I);
              Planilha := xGrid.Cells[04, I];
              Geral.MensagemArray(MB_ICONWARNING, 'Diferen�a de t�tulo',
              ['Planilha: ', 'C�lula: ', 'T�tulo que consta n� c�lula: ',
              'T�tulo esperado: '], [Planilha, Celula, TitCell,
              TitCfg], MeErros);
            end;
          end;
        end else
        if LinPla >= QrXcelCfgCabLinIni.Value then
        begin
          // Dados a serem registrados
          SeqLin := 0;
          GraGruX := 0;
          Nome := '';
          EstqIniPrc := 0;
          EstqIniQtd := 0;
          EstqIniVal := 0;
          ComprasPrc := 0;
          ComprasQtd := 0;
          ComprasVal := 0;
          ConsumoPrc := 0;
          ConsumoVal := 0;
          ConsumoQtd := 0;
          EstqFimPrc := 0;
          EstqFimQtd := 0;
          EstqFimVal := 0;
          // Verificar se � uma linha v�lida
          SeqEGGX :=
            Trim(AGrid.Cells[FColunas[01], I]) +  // SeqLin
            Trim(AGrid.Cells[FColunas[02], I]);   // GraGruX
          if (SeqEGGX <> '') and (SeqEGGX = Geral.SoNumero_TT(SeqEGGX)) then
          begin
            for J := 1 to FID_TabFld do
            begin
              Coluna := FColunas[J];
              if Coluna > 0 then
              begin
                Celula := MLAGeral.ColRowToCelula(J, LinPla);
                Planilha := xGrid.Cells[04, I];
                TxtCell := Trim(AGrid.Cells[Coluna, I]);
                case J of
                  01: // 'Sequ�ncia (linha)';
                    SeqLin := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  02: // 'Reduzido do produto';
                    GraGruX := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  03: // 'Nome do produto';
                    Nome := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  04: // 'Pre�o m�dio inicial';
                    EstqIniPrc := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  05: // 'Estoque inicial (kg)';
                    EstqIniQtd := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  06: // 'Estoque inicial (R$)';
                    EstqIniVal := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  07: // 'Pre�o m�dio compras';
                    ComprasPrc := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  08: // 'Compras (kg)';
                    ComprasQtd := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  09: // 'Compras (R$)';
                    ComprasVal := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
(*
                  10: // 'Pre�o m�dio consumo';
                    ConsumoPrc := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
*)
                  10: // 'Consumo (R$)';
                    ConsumoVal := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  11: // 'Consumo (kg)';
                    ConsumoQtd := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
(*
                  13: // 'Pre�o m�dio final';
                    EstqFimPrc := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
*)
                  12: // 'Estoque final (kg)';
                    EstqFimQtd := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                  13: // 'Estoque final (R$)';
                    EstqFimVal := DefineVariantDeTexto(Planilha, Celula, FFormat[Coluna], TxtCell);
                end;
              end;
            end;
            //
            (* Calcular pre�o m�dio pois n�o � informado no arquivo *)
            if ConsumoQtd > 0 then
              ConsumoPrc := ConsumoVal / ConsumoQtd
            else ConsumoQtd := 0;
            //
            if EstqFimQtd > 0 then
              EstqFimPrc := EstqFimVal / EstqFimQtd
            else EstqFimQtd := 0;
            //
            if GraGruX = 0 then
            begin
              QrLocGGX.Close;
              QrLocGGX.Params[0].AsString := Nome;
              UnDmkDAC_PF.AbreQuery(QrLocGGX, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
              //
              if QrLocGGX.RecordCount > 0 then
                GraGruX := QrLocGGXGraGruX.Value;
            end;
            UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_spedestq_its_', False, [
            'LinPla', 'AnoMes', 'Empresa',
            'SeqLin', 'GraGruX', 'Sit_Prod',
            'Terceiro', 'EstqIniQtd', 'EstqIniPrc',
            'EstqIniVal', 'ComprasQtd', 'ComprasPrc',
            'ComprasVal', 'ConsumoQtd', 'ConsumoPrc',
            'ConsumoVal', 'EstqFimQtd', 'EstqFimPrc',
            'EstqFimVal', 'Nome', 'Planilha', 'Celula'], [
            'LinArq'], [
            LinPla, AnoMes, FEmpresa,
            SeqLin, GraGruX, FSit_Prod,
            FTerceiro, EstqIniQtd, EstqIniPrc,
            EstqIniVal, ComprasQtd, ComprasPrc,
            ComprasVal, ConsumoQtd, ConsumoPrc,
            ConsumoVal, EstqFimQtd, EstqFimPrc,
            EstqFimVal, Nome, Planilha, Celula], [
            LinArq], False);
          end;
        end;
      end;
    end;
    //
    //
    MyObjects.Informa(LaAviso2, False, '...');
  finally
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TFmSPEDEstqLoadXLS.HabilitaAbertura();
begin
  BtAbrir.Enabled :=
    (EdXcelCfgCab.ValueVariant <> 0) and
    (Trim(EdArquivo.Text) <> '') and
    FileExists(EdArquivo.Text);
end;

function TFmSPEDEstqLoadXLS.ItensNaoTemErro(): Boolean;
  procedure InsereErro(Erro: Integer);
  var
    LinArq, LinPla, GraGruX: Integer;
    Planilha, Celula, Nome: String;
  begin
    LinArq := QrItsLinArq.Value;
    LinPla := QrItsLinPla.Value;
    Planilha := QrItsPlanilha.Value;
    Celula := QrItsCelula.Value;
    Nome := QrItsNome.Value;
    GraGruX := QrItsGraGruX.Value;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_spedestq_err_', False, [
    'LinArq', 'Erro', 'LinPla', 'Planilha', 'Celula', 'Nome', 'GraGruX'], [], [
    LinArq, Erro, LinPla, Planilha, Celula, Nome, GraGruX], [], False);
  end;
var
  Diferenca: Double;
begin
  Result := False;
  MyObjects.Informa(LaAviso1, True, 'Verificando erros nos registros');
  GradeCriar.RecriaTempTableNovo(ntrttSPEDESTQ_ERR, DModG.QrUpdPID1, False);
  QrIts.Close;
  UnDmkDAC_PF.AbreQuery(QrIts, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  while not QrIts.Eof do
  begin
    MyObjects.Informa(LaAviso1, True, 'Verificando erros no registro ' +
      FormatFloat('000', QrIts.RecNo) + ' de ' +
      FormatFloat('000', QrIts.RecordCount));
    if QrItsGraGruX.Value = 0 then
      InsereErro(9);
    //
    Diferenca := (QrItsEstqIniQtd.Value + QrItsComprasQtd.Value -
      QrItsConsumoQtd.Value);
    //
    if Diferenca <> QrItsEstqFimQtd.Value then
    begin
      if (QrItsEstqIniQtd.Value = 0) and
         (QrItsComprasQtd.Value = 0) and
         (QrItsConsumoQtd.Value = 0) then
        InsereErro(-1)
      else
        InsereErro(1)
    end;
    if (QrItsEstqIniVal.Value + QrItsComprasVal.Value -
    QrItsConsumoVal.Value) <> QrItsEstqFimVal.Value then
    begin
      if (QrItsEstqIniVal.Value = 0) and
         (QrItsComprasVal.Value = 0) and
         (QrItsConsumoVal.Value = 0) then
        InsereErro(-3)
      else
        InsereErro(-2)
    end;
    //
    if QrItsGraGruX.Value <> 0 then
    begin
      QrAntNew.Close;
      QrAntNew.Params[0].AsInteger := QrItsGraGruX.Value;
      QrAntNew.Params[0].AsInteger := Geral.IncrementaMes_AnoMes(QrItsAnoMes.Value, -1);
      UnDmkDAC_PF.AbreQuery(QrAntNew, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
      if QrAntNew.RecordCount > 0 then
      begin
        if Int(QrAntNewEstqFimQtd.Value * 1000) <> Int(QrItsEstqFimQtd.Value * 1000) then
          InsereErro(7);
        if Int(QrAntNewEstqFimVal.Value * 1000) <> Int(QrItsEstqFimVal.Value * 1000) then
          InsereErro(6)
      end else
      begin
        QrAntOld.Close;
        QrAntOld.Params[0].AsInteger := QrItsGraGruX.Value;
        QrAntOld.Params[0].AsInteger := Geral.IncrementaMes_AnoMes(QrItsAnoMes.Value, -1);
        UnDmkDAC_PF.AbreQuery(QrAntOld, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
        if QrAntOld.RecordCount > 0 then
        begin
          if Int(QrAntOldEstqFimQtd.Value * 1000) <> Int(QrItsEstqFimQtd.Value * 1000) then
            InsereErro(5);
          if Int(QrAntOldEstqFimVal.Value * 1000) <> Int(QrItsEstqFimVal.Value * 1000) then
            InsereErro(4)
        end;
      end;
    end;
    //
    QrIts.Next;
  end;
  // For�ar reaberura de Erros
  CKErros.Checked := False;
  CKAdver.Checked := False;
  CkErros.Checked := True;
  //
  if QrErros.RecordCount > 0 then
    Geral.MensagemBox('Existem ' + FormatFloat('0', QrErros.RecordCount) +
    ' erros que devem ser corrigidos antes de salvar os dados abertos!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else
    Result := True;  
end;

procedure TFmSPEDEstqLoadXLS.Listadeavisos1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_01,
    'Erros / advert�ncias de importa��o de Excel');
end;

procedure TFmSPEDEstqLoadXLS.MeErrosChange(Sender: TObject);
begin
  if MeErros.Text <> '' then
    PageControl1.ActivePageIndex := 1;
end;

function TFmSPEDEstqLoadXLS.NomeDeErro(Erro: Integer): String;
var
  s: String;
begin
  s := '';
  case Erro of
    -3: s := 'Movimento de valor no per�odo + anterior zerado com diferen�a no saldo final';
    -2: s := 'Movimento de valor no per�odo + anterior n�o zerado com diferen�a no saldo final';
    -1: s := 'Movimento de quantidade no per�odo + anterior zerado com diferen�a no saldo final';
     1: s := 'Movimento de quantidade no per�odo + anterior n�o zerado com diferen�a no saldo final';
     4: s := 'Saldo de valor final do per�odo anterior na base da dados n�o confere com inicial do per�odo atual';
     5: s := 'Saldo de quantidade final do per�odo anterior na base da dados n�o confere com inicial do per�odo atual';
     6: s := 'Saldo de valor final do per�odo anterior no arquivo excel n�o confere com inicial do per�odo atual';
     7: s := 'Saldo de quantidade final do per�odo anterior no arquivo excel n�o confere com inicial do per�odo atual';
     9: s := 'Produto sem reduzido informado no arquivo excel';
  end;
  if Erro > 0 then
    s := 'ERRO: ' + s
  else
    s := 'AVISO: ' + s;
  Result := s;  
end;

procedure TFmSPEDEstqLoadXLS.QrErr2AfterScroll(DataSet: TDataSet);
begin
  QrErr2Its.Close;
  QrErr2Its.Params[00].AsInteger := QrErr2Erro.Value;
  QrErr2Its.Params[01].AsString  := QrErr2Nome.Value;
  UnDmkDAC_PF.AbreQuery(QrErr2Its, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmSPEDEstqLoadXLS.QrErr2BeforeClose(DataSet: TDataSet);
begin
  QrErr2Its.Close;
end;

procedure TFmSPEDEstqLoadXLS.QrErr2CalcFields(DataSet: TDataSet);
begin
  QrErr2NO_ERRO.Value := NomeDeErro(QrErr2Erro.Value);
end;

procedure TFmSPEDEstqLoadXLS.QrErrosAfterOpen(DataSet: TDataSet);
begin
  BtImprime2.Enabled := QrErros.RecordCount > 0;
end;

procedure TFmSPEDEstqLoadXLS.QrErrosAfterScroll(DataSet: TDataSet);
begin
  BtVincula.Enabled := QrErrosErro.Value = 9;
end;

procedure TFmSPEDEstqLoadXLS.QrErrosBeforeClose(DataSet: TDataSet);
begin
  BtVincula.Enabled := False;
  BtImprime2.Enabled := False;
end;

procedure TFmSPEDEstqLoadXLS.QrErrosCalcFields(DataSet: TDataSet);
begin
  QrErrosNO_ERRO.Value := NomeDeErro(QrErrosErro.Value);
end;

procedure TFmSPEDEstqLoadXLS.QrProdsCalcFields(DataSet: TDataSet);
begin
  QrProdsSEQ.Value := QrProds.RecNo;
end;

procedure TFmSPEDEstqLoadXLS.QrXcelCfgCabAfterScroll(DataSet: TDataSet);
begin
  QrXcelCfgIts.Close;
  QrXcelCfgIts.Params[0].AsInteger := QrXcelCfgCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrXcelCfgIts, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
end;

procedure TFmSPEDEstqLoadXLS.QrXcelCfgCabBeforeClose(DataSet: TDataSet);
begin
  QrXcelCfgIts.Close;
end;

procedure TFmSPEDEstqLoadXLS.ReopenErros(LinArq: Integer);
begin
  QrErros.Close;
  QrErros.SQL.Clear;
  if CkErros.Checked or CkAdver.Checked then
  begin
    QrErros.SQL.Add('SELECT * FROM _spedestq_err_');
    if CkErros.Checked and CkAdver.Checked then
       (*nada de where ou seja: todos erros e advert�ncias!*)
    else if CkErros.Checked then
      QrErros.SQL.Add('WHERE Erro >0')
    else if CkAdver.Checked then
      QrErros.SQL.Add('WHERE Erro <0');
    //
    QrErros.SQL.Add('ORDER BY Erro DESC, LinArq');
    UnDmkDAC_PF.AbreQuery(QrErros, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    //MLAGeral.LeMeuSQLy(QrErros, '', nil, False, True);
  end;
end;

procedure TFmSPEDEstqLoadXLS.BtSalvaClick(Sender: TObject);
const
  Sit_Prod = 1; // Da empresa em seu poder.
  Terceiro = 0; //
var
  ImporExpor, AnoMes, Empresa, Controle, GraGruX: Integer;
  EstqIniQtd, EstqIniPrc, EstqIniVal, ComprasQtd, ComprasPrc, ComprasVal,
  ConsumoQtd, ConsumoPrc, ConsumoVal, EstqFimQtd, EstqFimPrc, EstqFimVal: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    QrItens.Close;
    UnDmkDAC_PF.AbreQuery(QrItens, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    QrItens.First;
    ImporExpor := FmSPEDEstqGer.QrSPEDEstqCabImporExpor.Value;
    AnoMes := FmSPEDEstqGer.QrSPEDEstqCabAnoMes.Value;
    Empresa := FmSPEDEstqGer.QrSPEDEstqCabEmpresa.Value;
    while not QrItens.Eof do
    begin
      GraGruX := QrItensGraGruX.Value;
      EstqIniQtd := QrItensEstqIniQtd.Value;
      EstqIniPrc := QrItensEstqIniPrc.Value;
      EstqIniVal := QrItensEstqIniVal.Value;
      ComprasQtd := QrItensComprasQtd.Value;
      ComprasPrc := QrItensComprasPrc.Value;
      ComprasVal := QrItensComprasVal.Value;
      ConsumoQtd := QrItensConsumoQtd.Value;
      ConsumoPrc := QrItensConsumoPrc.Value;
      ConsumoVal := QrItensConsumoVal.Value;
      EstqFimQtd := QrItensEstqFimQtd.Value;
      EstqFimPrc := QrItensEstqFimPrc.Value;
      EstqFimVal := QrItensEstqFimVal.Value;
      //
      Controle := UMyMod.BuscaEmLivreY_Def_SPED_EFD('spedestqits', 'Controle',
        stIns, 0);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedestqits', False, [
      'ImporExpor', 'AnoMes', 'Empresa',
      'GraGruX', 'Sit_Prod', 'Terceiro',
      'EstqIniQtd', 'EstqIniPrc', 'EstqIniVal',
      'ComprasQtd', 'ComprasPrc', 'ComprasVal',
      'ConsumoQtd', 'ConsumoPrc', 'ConsumoVal',
      'EstqFimQtd', 'EstqFimPrc', 'EstqFimVal'
      ], [
      'Controle'], [
      ImporExpor, AnoMes, Empresa,
      GraGruX, Sit_Prod, Terceiro,
      EstqIniQtd, EstqIniPrc, EstqIniVal,
      ComprasQtd, ComprasPrc, ComprasVal,
      ConsumoQtd, ConsumoPrc, ConsumoVal,
      EstqFimQtd, EstqFimPrc, EstqFimVal
      ], [
      Controle], True);
      //
      QrItens.Next;
    end;
    FmSPEDEstqGer.ReopenSPEDEstqIts(0, 0, 0);
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSPEDEstqLoadXLS.BtVinculaClick(Sender: TObject);
var
  GraGruX: Integer;
  MD5, Soundex, Nome: String;
begin
  if DBCheck.CriaFm(TFmGraGruRSel, FmGraGruRSel, afmoNegarComAviso) then
  begin
    VAR_CADASTRO := 0;
    FmGraGruRSel.EdNomeX.Text := QrErrosNome.Value;
    FmGraGruRSel.ShowModal;
    if VAR_CADASTRO <> 0  then
    begin
      Screen.Cursor := crHourGlass;
      try
        GraGruX := VAR_CADASTRO;
        MD5 := FmGraGruRSel.EdMD5.Text;
        Soundex := FmGraGruRSel.QrSoundexSoundex.Value;
        Nome := QrErrosNome.Value;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruvinc', False, [
        'GraGruX', 'MD5', 'Soundex', 'Nome'], [], [
        GraGruX, MD5, Soundex, Nome], [], False) then
        begin
          DModG.QrUpdPID1.SQL.Clear;
          DModG.QrUpdPID1.SQL.Add('UPDATE _spedestq_its_ SET ');
          DModG.QrUpdPID1.SQL.Add('GraGruX=:P0');
          DModG.QrUpdPID1.SQL.Add('WHERE Nome=:P1');
          DModG.QrUpdPID1.Params[00].AsInteger := GraGruX;
          DModG.QrUpdPID1.Params[01].AsString  := Nome;
          DModG.QrUpdPID1.ExecSQL;
        end;
        BtConfere.Enabled := True;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
    FmGraGruRSel.Destroy;
  end;
end;

procedure TFmSPEDEstqLoadXLS.CkErrosClick(Sender: TObject);
begin
  ReopenErros(0);
end;

function TFmSPEDEstqLoadXLS.DefineColunas(): Boolean;
var
  I, Col: Integer;
begin
  Result := False;
  try
    SetLength(FColunas, FID_TabFld + 1);
    SetLength(FTitulos, FID_TabFld + 1);
    SetLength(FFldIdx, FID_TabFld + 1);
    SetLength(FFldNom, FID_TabFld + 1);
    SetLength(FUsaFld, FID_TabFld + 1);
    SetLength(FFormat, FID_TabFld + 1);
    for I := 0 to FID_TabFld do
    begin
      FColunas[I] := 0;
      FTitulos[I] := '';
      FFldIdx[I] := 0;
      FFldNom[I] := '';
      FUsaFld[I] := False;
      FFormat[I] := 0;
    end;
    //
    QrXcelCfgIts.First;
    while not QrXcelCfgIts.Eof do
    begin
      Col := Geral.IMV(MLAGeral.LetraToNumero(QrXcelCfgItsColuna.Value));
      FColunas[QrXcelCfgItsID_TabFld.Value] := Col;
      FTitulos[QrXcelCfgItsID_TabFld.Value] := QrXcelCfgItsNome.Value;
      FFldIdx[Col] := QrXcelCfgItsID_TabFld.Value;
      FFldNom[Col] := QrXcelCfgItsNome.Value;
      FUsaFld[Col] := True;
      FFormat[Col] := QrXcelCfgItsDataType.Value;
      //
      Grade1.Cells[Col, 0] := QrXcelCfgItsNome.Value;
      //
      QrXcelCfgIts.Next;
    end;
    //
    Result := True;
  finally
    if not Result then
      Geral.MensagemBox('N�o foi possivel definir as posi��es das colunas!',
      'Erro', MB_OK+MB_ICONERROR);
  end;
end;

function TFmSPEDEstqLoadXLS.DefineVariantDeTexto(Planilha, Celula: String;
  Formato: Integer; Texto: String): Variant;
begin
  MyObjects.Informa(LaAviso2, True, 'Validadndo c�lula ' + Celula +
    ' da planilha ' + Planilha);
  case TAllFormat(Formato) of
    dmktfNone: Result := Texto;
    dmktfString: Result := Texto;
    dmktfDouble: Result := Geral.DMV(Texto);
    dmktfInteger: Result := Geral.IMV(Texto);
    dmktfLongint: Result := Geral.IMV(Texto);
    dmktfInt64: Result := Geral.I64(Texto);
    // Falta Testar Date, Time, DateTime, MesAno e AAAAMM!
    dmktfDate: Result := Geral.ValidaDataBR(Texto, True, False);
    dmktfTime: Result := Geral.ValidaDataBR(Texto, True, False);
    dmktfDateTime: Result := Geral.ValidaDataBR(Texto, True, False);
    dmktfMesAno: Result := Geral.ValidaDataBR('01/' + Texto, True, False);
    dmktf_AAAAMM: Result := Geral.IMV(Texto);
    //
    dmktfUnknown: Result := Texto;
  end;
end;

procedure TFmSPEDEstqLoadXLS.EdArquivoChange(Sender: TObject);
begin
  HabilitaAbertura();
end;

procedure TFmSPEDEstqLoadXLS.EdXcelCfgCabChange(Sender: TObject);
begin
  HabilitaAbertura();
end;

end.
