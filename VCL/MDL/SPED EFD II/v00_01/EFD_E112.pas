unit EFD_E112;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, dmkRadioGroup, UnDmkEnums;

type
  TFmEFD_E112 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdE111: TdmkEdit;
    Label6: TLabel;
    Label1: TLabel;
    TPDT_INI: TdmkEditDateTimePicker;
    TPDT_FIN: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label7: TStaticText;
    EdNUM_DA: TdmkEdit;
    Label8: TStaticText;
    EdLinArq: TdmkEdit;
    Label9: TLabel;
    RGIND_PROC: TdmkRadioGroup;
    StaticText3: TStaticText;
    EdPROC: TdmkEdit;
    StaticText4: TStaticText;
    EdTXT_COMPL: TdmkEdit;
    EdNUM_PROC: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEFD_E112: TFmEFD_E112;

implementation

uses UnMyObjects, Module, EFD_E001, UMySQLModule;

{$R *.DFM}

procedure TFmEFD_E112.BtOKClick(Sender: TObject);
const
  REG = 'E112';
var
  E111, ImporExpor, AnoMes, Empresa, LinArq(*, MES_REF*): Integer;
  NUM_DA, NUM_PROC, IND_PROC, PROC, TXT_COMPL: String;
//  VL_OR: Double;
begin
  E111 := EdE111.ValueVariant;
  //
  NUM_DA := EdNUM_DA.Text;
  NUM_PROC := EdNUM_PROC.Text;
  IND_PROC := RGIND_PROC.Items[RGIND_PROC.ItemIndex][1];
  PROC := EdPROC.Text;
  TXT_COMPL := EdTXT_COMPL.Text;
  //
  ImporExpor := EdImporExpor.ValueVariant;
  AnoMes := EdAnoMes.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  //
  LinArq := EdLinArq.ValueVariant;
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_E112', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  ImgTipo.SQLType, LinArq, siPositivo, EdLinArq);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'efd_E112', False, [
  'REG', 'NUM_DA', 'NUM_PROC',
  'IND_PROC', 'PROC', 'TXT_COMPL'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'E111'], [
  REG, NUM_DA, NUM_PROC,
  IND_PROC, PROC, TXT_COMPL], [
  ImporExpor, AnoMes, Empresa, E111, LinArq], True) then
  begin
    FmEFD_E001.ReopenEFD_E112(LinArq);
    Close;
  end;
end;

procedure TFmEFD_E112.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_E112.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_E112.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
