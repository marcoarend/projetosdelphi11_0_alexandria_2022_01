 1 => Tabelas Globais

 11 => 3.1.1 - Tabela Vers�o do Leiaute - Sped Fiscal
  8 => 4.4.6- Tabela de C�digos dos Totalizadores Parciais da REDU��O Z
  5 => 5.4 - Tabela de C�digos das Obriga��es do ICMS a Recolher
  2 => Tabela C�digo Fiscal de Opera��o e Presta��o - CFOP
  4 => Tabela de Munc�pios do IBGE
  6 => Tabela de Pa�ses do IBGE
 13 => Tabela de Pa�ses SISCOMEX
  1 => Tabela de Produtos para Combust�veis / Solvente
  3 => Tabela G�nero do Item de Mercadoria/Servi�o
 12 => Tipo de Conhecimento de Transporte Eletr�nico
  7 => Tipo do Conhecimento de Transporte
  9 => UF C�digo IBGE - Sigla
 10 => UF Sigla

 11 => Tabelas da Bahia

190 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
245 => 5.2 - Tabela de Informa��es Adicionais da Apura��o - Valores Declarat�rios - BA
408 => 5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS - BA
189 => Tabela de C�digos de Receita

 21 => Tabelas da Para�ba

 46 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
 47 => 5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios
 48 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
125 => 5.4- Tabela de C�digos das Obriga��es  de ICMS a Recolher
 49 => 5.5-  Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS
195 => Tabela de C�digos de Receita

 8 => Tabelas de Alagoas

 33 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
 34 => 5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios
 35 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
196 => Tabela de C�digos de Receita

 3 => Tabelas de Caracter�sticas dos Documentos

 18 => 4.1.2- Tabela Situa��o do Documento
 16 => Tabela de Indicador de Emitente
 17 => Tabela Documentos Fiscais do ICMS

 6 => Tabelas de Classes de Consumo

 31 =>  4.4.1- Tabela Classifica��o de Itens de Energia El�trica, Servi�os de Comunica��o e Telecomunica��o
 28 => 4.4.2- Tabela Classes de Consumo de �gua Canalizada
 29 => 4.4.3- Tabela Classes de Consumo de G�s Canalizado
 30 => 4.4.4- Tabela Classes de Consumo dos Servi�os de Comunica��o e Telecomunica��o
 32 => 4.4.5 - Tabela - Faixa de Consumo de Energia El�trica

15 => Tabelas de Goi�s

 37 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
 38 => 5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios
 39 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
 40 => 5.5-  Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS
187 => Tabela de C�digos de Receita

19 => Tabelas de Minas Gerais

 43 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
 44 => 5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios
 45 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
248 => 5.5-  Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS - MG
194 => Tabela de C�digos de Receita
249 => Tabela de Itens UF �ndice de Participa��o dos Munic�pios MG

22 => Tabelas de Pernambuco

212 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS

 29 => Tabelas de Rond�nia

 53 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
179 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
185 => Tabela de C�digos de Receita

28 => Tabelas de Roraima

30 => Tabelas de Santa Catarina

 54 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
 55 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
183 => Tabela de C�digos de Receita

31 => Tabelas de S�o Paulo

247 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS - SP
598 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal SP
246 => Tabela de Itens UF �ndice de Participa��o dos Munic�pios

32 => Tabelas de Sergipe

 56 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
 57 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
 58 => 5.5-  Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS
182 => Tabela de C�digos de Receita

5 => Tabelas de Situa��o Tribut�ria

130 => Tabela C�digo da Situa��o Tribut�ria - CST (ICMS)
 23 => Tabela C�digo de Situa��o Tribut�ria da COFINS
 27 => Tabela C�digo de Situa��o Tribut�ria do PIS
 26 => Tabela C�digo de Tributa��o do IPI - CST_IPI

33 => Tabelas de Tocantins

 59 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
 60 => 5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios

 7 => Tabelas do Acre

10 => Tabelas do Amap�

9 => Tabelas do Amazonas

220 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
221 => 5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios - AM
222 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal - AM
188 => Tabela de C�digos de Receita

12 => Tabelas do Cear�

 36 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
197 => Tabela de C�digos de Receita

13 => Tabelas do Distrito Federal

14 => Tabelas do Esp�rito Santo

171 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
172 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
193 => Tabela de C�digos de Receita
555 => Tabela de Itens UF �ndice de Participa��o dos Munic�pios ES

4 => Tabelas do IPI

 20 => 4.5.1- Tabela de C�digos da Classe de Enquadramento do IPI
 22 => 4.5.2- Tabela de C�digo de Selo de Controle
 19 => 4.5.4 - Tabela C�digo de Ajuste da Apura��o do IPI
 21 => Tabela C�digo de Enquadramento Legal do IPI

16 => Tabelas do Maranh�o

122 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
121 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal

17 => Tabelas do Mato Grosso

131 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
170 => 5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios

18 => Tabelas do Mato Grosso do Sul

 42 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
 41 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
192 => Tabela de C�digos de Receita

20 => Tabelas do Par�

123 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
124 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
186 => Tabela de C�digos de Receita

23 => Tabelas do Paran�

 50 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
 51 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
191 => Tabela de C�digos de Receita

24 => Tabelas do Piau�

127 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS

25 => Tabelas do Rio de Janeiro

 52 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
129 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
244 => 5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS - RJ

26 => Tabelas do Rio Grande do Norte

126 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
128 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
223 => 5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS - RN
224 => Tabela de C�digos de Receita RN

27 => Tabelas do Rio Grande do Sul

173 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
180 => 5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios
176 => 5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal
175 => 5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS
184 => Tabela de C�digos de Receita

2 => Tabelas Gen�ricas - ICMS

 15 => 5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS
 14 => 5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS
181 => Tabela de C�digos de Receita - GNRE


http://www.sped.fazenda.gov.br/spedtabelas/appconsulta/obterTabelaExterna.aspx?idPacote=1&idTabela=11



SPEDEFDTabT


