object FmEFD_E113: TFmEFD_E113
  Left = 339
  Top = 185
  Caption = 'EFD-SPEDE-113 :: Identifica'#231#227'o dos Documentos Fiscais'
  ClientHeight = 325
  ClientWidth = 694
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 694
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 784
    object GB_R: TGroupBox
      Left = 646
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 736
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 598
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 688
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 460
        Height = 32
        Caption = 'Identifica'#231#227'o dos Documentos Fiscais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 460
        Height = 32
        Caption = 'Identifica'#231#227'o dos Documentos Fiscais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 460
        Height = 32
        Caption = 'Identifica'#231#227'o dos Documentos Fiscais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 255
    Width = 694
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 466
    ExplicitWidth = 784
    object PnSaiDesis: TPanel
      Left = 548
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 638
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 546
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 636
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 694
    Height = 163
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 784
    ExplicitHeight = 374
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 694
      Height = 163
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 784
      ExplicitHeight = 374
      object GroupBox1: TGroupBox
        Left = 0
        Top = 61
        Width = 694
        Height = 102
        Align = alClient
        Caption = ' Dados: '
        TabOrder = 0
        ExplicitWidth = 784
        ExplicitHeight = 313
        object Label7: TStaticText
          Left = 16
          Top = 20
          Width = 546
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '02. C'#243'digo do participante (cliente/fornecedor)'
          FocusControl = EdCOD_PART
          TabOrder = 2
        end
        object Label8: TStaticText
          Left = 16
          Top = 44
          Width = 109
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '03. Modelo da N.F.'
          TabOrder = 3
        end
        object EdCOD_PART: TdmkEdit
          Left = 568
          Top = 20
          Width = 112
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'COD_PART'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object StaticText3: TStaticText
          Left = 16
          Top = 68
          Width = 137
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '07. Data da emiss'#227'o da NF'
          TabOrder = 4
        end
        object EdCOD_MOD: TdmkEdit
          Left = 128
          Top = 44
          Width = 40
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'COD_MOD'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdSER: TdmkEdit
          Left = 276
          Top = 44
          Width = 40
          Height = 21
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'SER'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object StaticText1: TStaticText
          Left = 172
          Top = 44
          Width = 101
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '04. S'#233'rie da N.F.'
          TabOrder = 6
        end
        object EdSUB: TdmkEdit
          Left = 436
          Top = 44
          Width = 40
          Height = 21
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'SUB'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object StaticText2: TStaticText
          Left = 320
          Top = 44
          Width = 113
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '05. Subs'#233'rie da N.F.'
          TabOrder = 8
        end
        object EdNUM_DOC: TdmkEdit
          Left = 616
          Top = 44
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'NUM_DOC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object StaticText5: TStaticText
          Left = 480
          Top = 44
          Width = 133
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '06. Numero da N.F.'
          TabOrder = 10
        end
        object TPDT_DOC: TdmkEditDateTimePicker
          Left = 156
          Top = 68
          Width = 100
          Height = 21
          Date = 40761.337560104170000000
          Time = 40761.337560104170000000
          TabOrder = 11
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DT_DOC'
          UpdType = utYes
        end
        object StaticText6: TStaticText
          Left = 260
          Top = 68
          Width = 97
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '08. C'#243'digo do item:'
          TabOrder = 12
        end
        object EdCOD_ITEM: TdmkEdit
          Left = 360
          Top = 68
          Width = 45
          Height = 21
          TabOrder = 13
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'COD_ITEM'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object StaticText7: TStaticText
          Left = 408
          Top = 68
          Width = 193
          Height = 21
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = '09. Valor do ajuste para opera'#231#227'o/item'
          TabOrder = 14
        end
        object EdVL_AJ_ITEM: TdmkEdit
          Left = 604
          Top = 68
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 15
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'VL_AJ_ITEM'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 694
        Height = 61
        Align = alTop
        Caption = ' Dados do intervalo: '
        Enabled = False
        TabOrder = 1
        ExplicitWidth = 784
        object Label3: TLabel
          Left = 68
          Top = 16
          Width = 56
          Height = 13
          Caption = 'ImporExpor:'
        end
        object Label4: TLabel
          Left = 132
          Top = 16
          Width = 42
          Height = 13
          Caption = 'AnoMes:'
        end
        object Label5: TLabel
          Left = 188
          Top = 16
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label6: TLabel
          Left = 236
          Top = 16
          Width = 28
          Height = 13
          Caption = 'E111:'
        end
        object Label1: TLabel
          Left = 412
          Top = 16
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
        end
        object Label2: TLabel
          Left = 528
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Data final:'
        end
        object Label9: TLabel
          Left = 300
          Top = 16
          Width = 33
          Height = 13
          Caption = 'LinArq:'
        end
        object EdImporExpor: TdmkEdit
          Left = 68
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdAnoMes: TdmkEdit
          Left = 132
          Top = 32
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdEmpresa: TdmkEdit
          Left = 188
          Top = 32
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdE111: TdmkEdit
          Left = 236
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object TPDT_INI: TdmkEditDateTimePicker
          Left = 412
          Top = 32
          Width = 112
          Height = 21
          Date = 40761.337560104170000000
          Time = 40761.337560104170000000
          TabOrder = 4
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPDT_FIN: TdmkEditDateTimePicker
          Left = 528
          Top = 32
          Width = 112
          Height = 21
          Date = 40761.337560104170000000
          Time = 40761.337560104170000000
          TabOrder = 5
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdLinArq: TdmkEdit
          Left = 300
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 211
    Width = 694
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitTop = 422
    ExplicitWidth = 784
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 690
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 780
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
end
