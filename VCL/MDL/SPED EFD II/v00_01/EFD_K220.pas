unit EFD_K220;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup, AppListas;

type
  TFmEFD_K220 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdK100: TdmkEdit;
    Label6: TLabel;
    TPDT_MOV: TdmkEditDateTimePicker;
    Label2: TLabel;
    EdLinArq: TdmkEdit;
    Label9: TLabel;
    QrGraGruX1: TmySQLQuery;
    QrGraGruX1Controle: TIntegerField;
    QrGraGruX1NO_PRD_TAM_COR: TWideStringField;
    QrGraGruX1GraGru1: TIntegerField;
    QrGraGruX1SIGLAUNIDMED: TWideStringField;
    QrGraGruX1GerBxaEstq: TSmallintField;
    QrGraGruX1NCM: TWideStringField;
    QrGraGruX1UnidMed: TIntegerField;
    QrGraGruX1Ex_TIPI: TWideStringField;
    DsGraGruX1: TDataSource;
    Panel5: TPanel;
    EdCOD_ITEM_ORI: TdmkEditCB;
    Label232: TLabel;
    CBCOD_ITEM_ORI: TdmkDBLookupComboBox;
    StaticText2: TStaticText;
    EdQTD: TdmkEdit;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    DsEntidades: TDataSource;
    EdCOD_ITEM_DEST: TdmkEditCB;
    Label1: TLabel;
    CBCOD_ITEM_DEST: TdmkDBLookupComboBox;
    EdMovimID: TdmkEdit;
    EdMovimID_Txt: TdmkEdit;
    Label7: TLabel;
    QrGraGruX2: TmySQLQuery;
    QrGraGruX2Controle: TIntegerField;
    QrGraGruX2NO_PRD_TAM_COR: TWideStringField;
    QrGraGruX2GraGru1: TIntegerField;
    QrGraGruX2SIGLAUNIDMED: TWideStringField;
    QrGraGruX2GerBxaEstq: TSmallintField;
    QrGraGruX2NCM: TWideStringField;
    QrGraGruX2UnidMed: TIntegerField;
    QrGraGruX2Ex_TIPI: TWideStringField;
    DsGraGruX2: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdMovimIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMovimIDChange(Sender: TObject);
  private
    { Private declarations }
    //FArrTxtMovimID: MyArrayLista;
    procedure ReopenGraGruX();

  public
    { Public declarations }
    FBalID, FBalNum, FBalItm, FBalEnt: Integer;
    FID_SEK: TSPED_EFD_Bal;
  end;

  var
  FmEFD_K220: TFmEFD_K220;

implementation

uses UnMyObjects, Module, EFD_E001, UMySQLModule, DmkDAC_PF, MyListas,
  ModuleFin, UnFinanceiro;

{$R *.DFM}

procedure TFmEFD_K220.BtOKClick(Sender: TObject);
const
  REG = 'K220';
var
  ImporExpor, AnoMes, Empresa, K100, LinArq, MovimID: Integer;
  SQLType: TSQLType;
var
  DT_MOV, COD_ITEM_ORI, COD_ITEM_DEST: String;
  QTD: Double;
  ID_SEK: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  LinArq         := EdLinArq.ValueVariant;
  K100           := EdK100.ValueVariant;
  //
  MovimID        := EdMovimID.ValueVariant;
  DT_MOV         := Geral.FDT(TPDT_MOV.Date, 1);
  COD_ITEM_ORI   := (EdCOD_ITEM_ORI.ValueVariant);
  COD_ITEM_DEST  := (EdCOD_ITEM_DEST.ValueVariant);
  QTD            := EdQTD.ValueVariant;
  //IND_EST        := Geral.FF0(RGIND_EST.ItemIndex);
  //COD_PART       := EdCOD_PART.ValueVariant;
  if SQLType = stIns then
  begin
    ID_SEK         := Integer(TSPED_EFD_Bal.sebalEFD_K200);
  end else
  begin
    ID_SEK         := Integer(FID_SEK);
  end;
  //Geral.MB_SQL(Self, QrX999);
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_k220', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  ImgTipo.SQLType, LinArq, siPositivo, EdLinArq);
  //
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efd_k220', False, [
  'REG', 'K100', 'DT_MOV',
  'COD_ITEM_ORI', 'COD_ITEM_DEST', 'QTD',
  'MovimID', 'ID_SEK'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  REG, K100, DT_MOV,
  COD_ITEM_ORI, COD_ITEM_DEST, QTD,
  MovimID, ID_SEK], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
    // Nao tem totais
    //FmEFD_E001.AtualizaValoresK100deK220();
    FmEFD_E001.ReopenEFD_K220(LinArq);
    Close;
  end;
end;

procedure TFmEFD_K220.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_K220.EdMovimIDChange(Sender: TObject);
var
  MovimID: Integer;
begin
  MovimID := EdMovimID.ValueVariant;
  EdMovimID_Txt.text := sEstqMovimID_FRENDLY[MovimID];
end;

procedure TFmEFD_K220.EdMovimIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if (Key = VK_F4) then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdMovimID.Text := Geral.SelecionaItemLista(sEstqMovimID_FRENDLY, 0,
    'SEL-LISTA-000 :: Sele��o do ID do movimento', TitCols, Screen.Width)
  end;
end;

procedure TFmEFD_K220.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_K220.FormCreate(Sender: TObject);
begin
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  FBalID  := 0;
  FBalNum := 0;
  FBalItm := 0;
  FBalEnt := 0;
  ReopenGraGruX();
end;

procedure TFmEFD_K220.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEFD_K220.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
  procedure Abre(Qry: TmySQLQuery);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
    'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
    SQL_LFT,
    'WHERE ggx.Controle > -900000 ',
    SQL_AND,
    'ORDER BY NO_PRD_TAM_COR ',
    '']);
  end;
begin
  SQL_AND := '';
  SQL_LFT := '';
  if TdmkAppID(CO_DMKID_APP) = dmkappB_L_U_E_D_E_R_M then
  begin
    SQL_AND := 'AND NOT (pqc.PQ IS NULL)';
    SQL_LFT := 'LEFT JOIN pqcli pqc ON pqc.PQ=gg1.Nivel1';
    //
    Abre(QrGraGruX1);
    Abre(QrGraGruX2);
  end;
end;

end.
