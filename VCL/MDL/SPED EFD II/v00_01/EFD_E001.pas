unit EFD_E001;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Menus, ComCtrls, Grids,
  DBGrids, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, Variants,
  dmkDBGrid, frxClass, frxDBSet, dmkCheckGroup, DmkDAC_PF, dmkImage, UnDmkEnums,
  dmkDBGridZTO, UnAppPF, UnProjGroup_Vars, AppListas, UnProjGroup_Consts;

type
  TReopenKind = (rkGrade=0, rkPrint=1);
  TFmEFD_E001 = class(TForm)
    PainelDados: TPanel;
    DsEFD_E001: TDataSource;
    QrEFD_E001: TmySQLQuery;
    dmkPermissoes1: TdmkPermissoes;
    PMPeriodo: TPopupMenu;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    PME111: TPopupMenu;
    Panel7: TPanel;
    Itemns1: TMenuItem;
    Panel9: TPanel;
    Panel10: TPanel;
    DBGCab: TdmkDBGrid;
    Panel11: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel12: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    DBEdit1: TDBEdit;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida0: TBitBtn;
    PB1: TProgressBar;
    Panel16: TPanel;
    PME100: TPopupMenu;
    Incluinovoperiodo1: TMenuItem;
    Excluiperiodoselecionado1: TMenuItem;
    QrEFD_E001NO_ENT: TWideStringField;
    QrEFD_E001MES_ANO: TWideStringField;
    QrEFD_E001ImporExpor: TSmallintField;
    QrEFD_E001AnoMes: TIntegerField;
    QrEFD_E001Empresa: TIntegerField;
    QrEFD_E001LinArq: TIntegerField;
    QrEFD_E001REG: TWideStringField;
    QrEFD_E001IND_MOV: TWideStringField;
    QrEFD_E100: TmySQLQuery;
    DsEFD_E100: TDataSource;
    QrEFD_E100DT_INI: TDateField;
    QrEFD_E100DT_FIN: TDateField;
    QrEFD_E110: TmySQLQuery;
    DsEFD_E110: TDataSource;
    Incluinovointervalo1: TMenuItem;
    Alteraintervaloselecionado1: TMenuItem;
    Excluiintervaloselecionado1: TMenuItem;
    QrEFD_E110ImporExpor: TSmallintField;
    QrEFD_E110AnoMes: TIntegerField;
    QrEFD_E110Empresa: TIntegerField;
    QrEFD_E110LinArq: TIntegerField;
    QrEFD_E110E100: TIntegerField;
    QrEFD_E110REG: TWideStringField;
    QrEFD_E110VL_TOT_DEBITOS: TFloatField;
    QrEFD_E110VL_AJ_DEBITOS: TFloatField;
    QrEFD_E110VL_TOT_AJ_DEBITOS: TFloatField;
    QrEFD_E110VL_ESTORNOS_CRED: TFloatField;
    QrEFD_E110VL_TOT_CREDITOS: TFloatField;
    QrEFD_E110VL_AJ_CREDITOS: TFloatField;
    QrEFD_E110VL_TOT_AJ_CREDITOS: TFloatField;
    QrEFD_E110VL_ESTORNOS_DEB: TFloatField;
    QrEFD_E110VL_SLD_CREDOR_ANT: TFloatField;
    QrEFD_E110VL_SLD_APURADO: TFloatField;
    QrEFD_E110VL_TOT_DED: TFloatField;
    QrEFD_E110VL_ICMS_RECOLHER: TFloatField;
    QrEFD_E110VL_SLD_CREDOR_TRANSPORTAR: TFloatField;
    QrEFD_E110DEB_ESP: TFloatField;
    QrEFD_E100ImporExpor: TSmallintField;
    QrEFD_E100AnoMes: TIntegerField;
    QrEFD_E100Empresa: TIntegerField;
    QrEFD_E100LinArq: TIntegerField;
    QrEFD_E100REG: TWideStringField;
    QrEFD_E111: TmySQLQuery;
    DsEFD_E111: TDataSource;
    QrEFD_E111ImporExpor: TSmallintField;
    QrEFD_E111AnoMes: TIntegerField;
    QrEFD_E111Empresa: TIntegerField;
    QrEFD_E111LinArq: TIntegerField;
    QrEFD_E111E110: TIntegerField;
    QrEFD_E111REG: TWideStringField;
    QrEFD_E111COD_AJ_APUR: TWideStringField;
    QrEFD_E111DESCR_COMPL_AJ: TWideStringField;
    QrEFD_E111VL_AJ_APUR: TFloatField;
    QrEFD_E116: TmySQLQuery;
    DsEFD_E116: TDataSource;
    QrEFD_E116ImporExpor: TSmallintField;
    QrEFD_E116AnoMes: TIntegerField;
    QrEFD_E116Empresa: TIntegerField;
    QrEFD_E116LinArq: TIntegerField;
    QrEFD_E116E110: TIntegerField;
    QrEFD_E116REG: TWideStringField;
    QrEFD_E116COD_OR: TWideStringField;
    QrEFD_E116VL_OR: TFloatField;
    QrEFD_E116DT_VCTO: TDateField;
    QrEFD_E116COD_REC: TWideStringField;
    QrEFD_E116NUM_PROC: TWideStringField;
    QrEFD_E116IND_PROC: TWideStringField;
    QrEFD_E116PROC: TWideStringField;
    QrEFD_E116TXT_COMPL: TWideStringField;
    QrEFD_E116MES_REF: TDateField;
    Incluinovoajuste1: TMenuItem;
    Alteraajusteselecionado1: TMenuItem;
    Excluiajusteselecionado1: TMenuItem;
    PMObrigacoes: TPopupMenu;
    IncluinovaobrigaodoICMSarecolher1: TMenuItem;
    Alteraobrigaoselecionada1: TMenuItem;
    Excluiobrigaoselecionada1: TMenuItem;
    QrEFD_E112: TmySQLQuery;
    DsEFD_E112: TDataSource;
    QrEFD_E113: TmySQLQuery;
    DsEFD_E113: TDataSource;
    QrEFD_E115: TmySQLQuery;
    DsEFD_E115: TDataSource;
    QrEFD_E115ImporExpor: TSmallintField;
    QrEFD_E115AnoMes: TIntegerField;
    QrEFD_E115Empresa: TIntegerField;
    QrEFD_E115LinArq: TIntegerField;
    QrEFD_E115E110: TIntegerField;
    QrEFD_E115REG: TWideStringField;
    QrEFD_E115COD_INF_ADIC: TWideStringField;
    QrEFD_E115VL_INF_ADIC: TFloatField;
    QrEFD_E115DESCR_COMPL_AJ: TWideStringField;
    QrEFD_E112ImporExpor: TSmallintField;
    QrEFD_E112AnoMes: TIntegerField;
    QrEFD_E112Empresa: TIntegerField;
    QrEFD_E112LinArq: TIntegerField;
    QrEFD_E112E111: TIntegerField;
    QrEFD_E112REG: TWideStringField;
    QrEFD_E112NUM_DA: TWideStringField;
    QrEFD_E112NUM_PROC: TWideStringField;
    QrEFD_E112IND_PROC: TWideStringField;
    QrEFD_E112PROC: TWideStringField;
    QrEFD_E112TXT_COMPL: TWideStringField;
    QrEFD_E113ImporExpor: TSmallintField;
    QrEFD_E113AnoMes: TIntegerField;
    QrEFD_E113Empresa: TIntegerField;
    QrEFD_E113LinArq: TIntegerField;
    QrEFD_E113E111: TIntegerField;
    QrEFD_E113REG: TWideStringField;
    QrEFD_E113COD_PART: TWideStringField;
    QrEFD_E113COD_MOD: TWideStringField;
    QrEFD_E113SER: TWideStringField;
    QrEFD_E113SUB: TWideStringField;
    QrEFD_E113NUM_DOC: TIntegerField;
    QrEFD_E113DT_DOC: TDateField;
    QrEFD_E113COD_ITEM: TWideStringField;
    QrEFD_E113VL_AJ_ITEM: TFloatField;
    N1: TMenuItem;
    E113Informaesadicionais1: TMenuItem;
    E1121: TMenuItem;
    Incluinovainformaoadicional1: TMenuItem;
    Alterainformaoadicionalselecionada1: TMenuItem;
    Excluiinformaoadicionalselecionada1: TMenuItem;
    Incluinovaidentificaodedocumentofiscal1: TMenuItem;
    Alteraidentificaoselecionada1: TMenuItem;
    Excluiidentificaoselecionada1: TMenuItem;
    PMValDecltorio: TPopupMenu;
    Incluinovovalordeclaratrio1: TMenuItem;
    Alteravalordeclaratrioselecionado1: TMenuItem;
    Excluivalordeclaratrioselecionado1: TMenuItem;
    PCEx00: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    TabSheet7: TTabSheet;
    GroupBox1: TGroupBox;
    dmkDBGrid1: TdmkDBGrid;
    PCE100: TPageControl;
    TabSheet3: TTabSheet;
    Panel4: TPanel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label7: TStaticText;
    Label8: TStaticText;
    Label9: TStaticText;
    Label10: TStaticText;
    Label11: TStaticText;
    Label12: TStaticText;
    Label13: TStaticText;
    Label14: TStaticText;
    Label15: TStaticText;
    Label16: TStaticText;
    Label17: TStaticText;
    Label18: TStaticText;
    Label19: TStaticText;
    Label20: TStaticText;
    TabSheet4: TTabSheet;
    Panel5: TPanel;
    Splitter2: TSplitter;
    Label25: TLabel;
    DBGrid1: TDBGrid;
    Panel14: TPanel;
    Label21: TLabel;
    Splitter1: TSplitter;
    Label22: TLabel;
    DBGrid4: TDBGrid;
    DBGrid6: TDBGrid;
    TabSheet5: TTabSheet;
    Panel15: TPanel;
    Label23: TLabel;
    DBGrid5: TDBGrid;
    TabSheet6: TTabSheet;
    Panel8: TPanel;
    Label24: TLabel;
    DBGrid2: TDBGrid;
    Panel13: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Panel17: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    Panel18: TPanel;
    GroupBox3: TGroupBox;
    dmkDBGrid2: TdmkDBGridZTO;
    PME500: TPopupMenu;
    IncluinovointervaloIPI1: TMenuItem;
    AlteraintervaloIPIselecionado1: TMenuItem;
    ExcluiintervaloIPIselecionado1: TMenuItem;
    QrEFD_E500: TmySQLQuery;
    DsEFD_E500: TDataSource;
    QrEFD_E500ImporExpor: TSmallintField;
    QrEFD_E500AnoMes: TIntegerField;
    QrEFD_E500Empresa: TIntegerField;
    QrEFD_E500LinArq: TIntegerField;
    QrEFD_E500REG: TWideStringField;
    QrEFD_E500DT_INI: TDateField;
    QrEFD_E500DT_FIN: TDateField;
    QrEFD_E500IND_APUR: TWideStringField;
    QrEFD_E500NO_IND_APUR: TWideStringField;
    QrEFD_E510: TmySQLQuery;
    DsEFD_E510: TDataSource;
    QrEFD_E520: TmySQLQuery;
    DsEFD_E520: TDataSource;
    QrEFD_E530: TmySQLQuery;
    DsEFD_E530: TDataSource;
    QrEFD_E510ImporExpor: TSmallintField;
    QrEFD_E510AnoMes: TIntegerField;
    QrEFD_E510Empresa: TIntegerField;
    QrEFD_E510LinArq: TIntegerField;
    QrEFD_E510E500: TIntegerField;
    QrEFD_E510REG: TWideStringField;
    QrEFD_E510CFOP: TIntegerField;
    QrEFD_E510CST_IPI: TWideStringField;
    QrEFD_E510VL_CONT_IPI: TFloatField;
    QrEFD_E510VL_BC_IPI: TFloatField;
    QrEFD_E510VL_IPI: TFloatField;
    QrEFD_E510Lk: TIntegerField;
    QrEFD_E510DataCad: TDateField;
    QrEFD_E510DataAlt: TDateField;
    QrEFD_E510UserCad: TIntegerField;
    QrEFD_E510UserAlt: TIntegerField;
    QrEFD_E510AlterWeb: TSmallintField;
    QrEFD_E510Ativo: TSmallintField;
    QrEFD_E520ImporExpor: TSmallintField;
    QrEFD_E520AnoMes: TIntegerField;
    QrEFD_E520Empresa: TIntegerField;
    QrEFD_E520LinArq: TIntegerField;
    QrEFD_E520E500: TIntegerField;
    QrEFD_E520REG: TWideStringField;
    QrEFD_E520VL_SD_ANT_IPI: TFloatField;
    QrEFD_E520VL_DEB_IPI: TFloatField;
    QrEFD_E520VL_CRED_IPI: TFloatField;
    QrEFD_E520VL_OD_IPI: TFloatField;
    QrEFD_E520VL_OC_IPI: TFloatField;
    QrEFD_E520VL_SC_IPI: TFloatField;
    QrEFD_E520VL_SD_IPI: TFloatField;
    QrEFD_E520Lk: TIntegerField;
    QrEFD_E520DataCad: TDateField;
    QrEFD_E520DataAlt: TDateField;
    QrEFD_E520UserCad: TIntegerField;
    QrEFD_E520UserAlt: TIntegerField;
    QrEFD_E520AlterWeb: TSmallintField;
    QrEFD_E520Ativo: TSmallintField;
    QrEFD_E530ImporExpor: TSmallintField;
    QrEFD_E530AnoMes: TIntegerField;
    QrEFD_E530Empresa: TIntegerField;
    QrEFD_E530LinArq: TIntegerField;
    QrEFD_E530E520: TIntegerField;
    QrEFD_E530REG: TWideStringField;
    QrEFD_E530IND_AJ: TWideStringField;
    QrEFD_E530VL_AJ: TFloatField;
    QrEFD_E530COD_AJ: TWideStringField;
    QrEFD_E530IND_DOC: TWideStringField;
    QrEFD_E530NUM_DOC: TWideStringField;
    QrEFD_E530DESCR_AJ: TWideStringField;
    PCE500: TPageControl;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    Panel19: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel20: TPanel;
    Label26: TLabel;
    dmkDBGridZTO2: TdmkDBGridZTO;
    dmkDBGridZTO3: TdmkDBGridZTO;
    PME530: TPopupMenu;
    IncluiajustedaapuracaodoIPI1: TMenuItem;
    AlteraajustedaapuracaodoIPIselecionado1: TMenuItem;
    ExcluiajustedaapuracaodoIPIselecionado1: TMenuItem;
    TabSheet10: TTabSheet;
    Panel21: TPanel;
    Panel22: TPanel;
    BtE110: TBitBtn;
    BtE111: TBitBtn;
    BtE116: TBitBtn;
    BtE115: TBitBtn;
    Panel23: TPanel;
    BtE520: TBitBtn;
    BtE530: TBitBtn;
    GroupBox4: TGroupBox;
    dmkDBGridZTO4: TdmkDBGridZTO;
    QrEFD_H005: TmySQLQuery;
    DsEFD_H005: TDataSource;
    Panel24: TPanel;
    BtH010: TBitBtn;
    BtH005: TBitBtn;
    BtH020: TBitBtn;
    PMH005: TPopupMenu;
    Incluidatadeinventrio1: TMenuItem;
    AlteraInventrio1: TMenuItem;
    Excluiinventrio1: TMenuItem;
    QrEFD_H005ImporExpor: TSmallintField;
    QrEFD_H005AnoMes: TIntegerField;
    QrEFD_H005Empresa: TIntegerField;
    QrEFD_H005LinArq: TIntegerField;
    QrEFD_H005REG: TWideStringField;
    QrEFD_H005DT_INV: TDateField;
    QrEFD_H005VL_INV: TFloatField;
    QrEFD_H005MOT_INV: TWideStringField;
    QrEFD_H005NO_MOT_INV: TWideStringField;
    QrEFD_H005Lk: TIntegerField;
    QrEFD_H005DataCad: TDateField;
    QrEFD_H005DataAlt: TDateField;
    QrEFD_H005UserCad: TIntegerField;
    QrEFD_H005UserAlt: TIntegerField;
    QrEFD_H005AlterWeb: TSmallintField;
    QrEFD_H005Ativo: TSmallintField;
    Panel25: TPanel;
    DBGH010: TdmkDBGridZTO;
    Panel26: TPanel;
    Label27: TLabel;
    dmkDBGridZTO6: TdmkDBGridZTO;
    Label28: TLabel;
    QrEFD_H010: TmySQLQuery;
    DsEFD_H010: TDataSource;
    QrEFD_H020: TmySQLQuery;
    DsEFD_H020: TDataSource;
    QrEFD_H020ImporExpor: TSmallintField;
    QrEFD_H020AnoMes: TIntegerField;
    QrEFD_H020Empresa: TIntegerField;
    QrEFD_H020H010: TIntegerField;
    QrEFD_H020LinArq: TIntegerField;
    QrEFD_H020REG: TWideStringField;
    QrEFD_H020CST_ICMS: TIntegerField;
    QrEFD_H020BC_ICMS: TFloatField;
    QrEFD_H020VL_ICMS: TFloatField;
    QrEFD_H020Lk: TIntegerField;
    QrEFD_H020DataCad: TDateField;
    QrEFD_H020DataAlt: TDateField;
    QrEFD_H020UserCad: TIntegerField;
    QrEFD_H020UserAlt: TIntegerField;
    QrEFD_H020AlterWeb: TSmallintField;
    QrEFD_H020Ativo: TSmallintField;
    QrEFD_H010ImporExpor: TSmallintField;
    QrEFD_H010AnoMes: TIntegerField;
    QrEFD_H010Empresa: TIntegerField;
    QrEFD_H010H005: TIntegerField;
    QrEFD_H010LinArq: TIntegerField;
    QrEFD_H010REG: TWideStringField;
    QrEFD_H010COD_ITEM: TWideStringField;
    QrEFD_H010UNID: TWideStringField;
    QrEFD_H010QTD: TFloatField;
    QrEFD_H010VL_UNIT: TFloatField;
    QrEFD_H010VL_ITEM: TFloatField;
    QrEFD_H010IND_PROP: TWideStringField;
    QrEFD_H010COD_PART: TWideStringField;
    QrEFD_H010TXT_COMPL: TWideStringField;
    QrEFD_H010COD_CTA: TWideStringField;
    QrEFD_H010Lk: TIntegerField;
    QrEFD_H010DataCad: TDateField;
    QrEFD_H010DataAlt: TDateField;
    QrEFD_H010UserCad: TIntegerField;
    QrEFD_H010UserAlt: TIntegerField;
    QrEFD_H010AlterWeb: TSmallintField;
    QrEFD_H010Ativo: TSmallintField;
    PMH010: TPopupMenu;
    Importadebalano1: TMenuItem;
    Incluiitem1: TMenuItem;
    Alteraitem1: TMenuItem;
    Excluiitemns1: TMenuItem;
    QrEFD_H010VL_ITEM_IR: TFloatField;
    QrEFD_H010NO_PRD_TAM_COR: TWideStringField;
    QrEFD_H010NO_PART: TWideStringField;
    QrEFD_H010BalID: TIntegerField;
    QrEFD_H010BalNum: TIntegerField;
    QrEFD_H010BalItm: TIntegerField;
    PMH020: TPopupMenu;
    Incluiinformaocomplementar1: TMenuItem;
    Alterainformaocomplementar1: TMenuItem;
    Excluiinformaocomplementar1: TMenuItem;
    TabSheet11: TTabSheet;
    QrEFD_K100: TmySQLQuery;
    DsEFD_K100: TDataSource;
    Panel27: TPanel;
    PMK100: TPopupMenu;
    IncluinovointervalodeProduoeEstoque1: TMenuItem;
    AlteraintervalodeProduoeEstoque1: TMenuItem;
    ExcluiintervalodeProduoeEstoque1: TMenuItem;
    BtK200: TBitBtn;
    PMK200: TPopupMenu;
    ImportaK200_1: TMenuItem;
    IncluiK200_1: TMenuItem;
    AlteraK200_1: TMenuItem;
    ExcluiK200_1: TMenuItem;
    QrEFD_K200: TmySQLQuery;
    DsEFD_K200: TDataSource;
    BtK220: TBitBtn;
    BtK230: TBitBtn;
    BtVerifica: TBitBtn;
    QrEFD_H010BalEnt: TIntegerField;
    QrEFD_K100ImporExpor: TSmallintField;
    QrEFD_K100AnoMes: TIntegerField;
    QrEFD_K100Empresa: TIntegerField;
    QrEFD_K100LinArq: TIntegerField;
    QrEFD_K100REG: TWideStringField;
    QrEFD_K100DT_INI: TDateField;
    QrEFD_K100DT_FIN: TDateField;
    QrEFD_K100Lk: TIntegerField;
    QrEFD_K100DataCad: TDateField;
    QrEFD_K100DataAlt: TDateField;
    QrEFD_K100UserCad: TIntegerField;
    QrEFD_K100UserAlt: TIntegerField;
    QrEFD_K100AlterWeb: TSmallintField;
    QrEFD_K100Ativo: TSmallintField;
    QrEFD_K200ImporExpor: TSmallintField;
    QrEFD_K200AnoMes: TIntegerField;
    QrEFD_K200Empresa: TIntegerField;
    QrEFD_K200LinArq: TIntegerField;
    QrEFD_K200REG: TWideStringField;
    QrEFD_K200K100: TIntegerField;
    QrEFD_K200DT_EST: TDateField;
    QrEFD_K200COD_ITEM: TWideStringField;
    QrEFD_K200QTD: TFloatField;
    QrEFD_K200IND_EST: TWideStringField;
    QrEFD_K200COD_PART: TWideStringField;
    QrEFD_K200BalID: TIntegerField;
    QrEFD_K200BalNum: TIntegerField;
    QrEFD_K200BalItm: TIntegerField;
    QrEFD_K200Lk: TIntegerField;
    QrEFD_K200DataCad: TDateField;
    QrEFD_K200DataAlt: TDateField;
    QrEFD_K200UserCad: TIntegerField;
    QrEFD_K200UserAlt: TIntegerField;
    QrEFD_K200AlterWeb: TSmallintField;
    QrEFD_K200Ativo: TSmallintField;
    QrEFD_K200BalEnt: TIntegerField;
    QrEFD_K200NO_PART: TWideStringField;
    QrEFD_K200NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K200NO_IND_EST: TWideStringField;
    Label29: TLabel;
    Label30: TLabel;
    PMK220: TPopupMenu;
    ImportaK220_1: TMenuItem;
    IncluiK220_1: TMenuItem;
    AlteraK220_1: TMenuItem;
    ExcluiK220_1: TMenuItem;
    PCK200: TPageControl;
    TabSheet12: TTabSheet;
    TabSheet13: TTabSheet;
    DBGK200: TdmkDBGridZTO;
    DsEFD_K220: TDataSource;
    QrEFD_K220: TmySQLQuery;
    DBGK220: TdmkDBGridZTO;
    QrEFD_K220NO_PRD_TAM_COR_ORI: TWideStringField;
    QrEFD_K220NO_PRD_TAM_COR_DEST: TWideStringField;
    QrEFD_K220ImporExpor: TSmallintField;
    QrEFD_K220AnoMes: TIntegerField;
    QrEFD_K220Empresa: TIntegerField;
    QrEFD_K220LinArq: TIntegerField;
    QrEFD_K220REG: TWideStringField;
    QrEFD_K220K100: TIntegerField;
    QrEFD_K220DT_MOV: TDateField;
    QrEFD_K220COD_ITEM_ORI: TWideStringField;
    QrEFD_K220COD_ITEM_DEST: TWideStringField;
    QrEFD_K220QTD: TFloatField;
    QrEFD_K220Lk: TIntegerField;
    QrEFD_K220DataCad: TDateField;
    QrEFD_K220DataAlt: TDateField;
    QrEFD_K220UserCad: TIntegerField;
    QrEFD_K220UserAlt: TIntegerField;
    QrEFD_K220AlterWeb: TSmallintField;
    QrEFD_K220Ativo: TSmallintField;
    QrEFD_K220MovimID: TIntegerField;
    QrEFD_K220ID_SEK: TSmallintField;
    PMK230: TPopupMenu;
    ImportaK230_1: TMenuItem;
    IncluiK230_1: TMenuItem;
    AlteraK230_1: TMenuItem;
    ExcluiK230_1: TMenuItem;
    QrEFD_K230: TmySQLQuery;
    DsEFD_K230: TDataSource;
    QrEFD_K230NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K230ImporExpor: TSmallintField;
    QrEFD_K230AnoMes: TIntegerField;
    QrEFD_K230Empresa: TIntegerField;
    QrEFD_K230LinArq: TIntegerField;
    QrEFD_K230REG: TWideStringField;
    QrEFD_K230DT_INI_OP: TDateField;
    QrEFD_K230K100: TIntegerField;
    QrEFD_K230DT_FIN_OP: TDateField;
    QrEFD_K230COD_DOC_OP: TWideStringField;
    QrEFD_K230COD_ITEM: TWideStringField;
    QrEFD_K230QTD_ENC: TFloatField;
    QrEFD_K230Lk: TIntegerField;
    QrEFD_K230DataCad: TDateField;
    QrEFD_K230DataAlt: TDateField;
    QrEFD_K230UserCad: TIntegerField;
    QrEFD_K230UserAlt: TIntegerField;
    QrEFD_K230AlterWeb: TSmallintField;
    QrEFD_K230Ativo: TSmallintField;
    QrEFD_K230DT_FIN_OP_Txt: TWideStringField;
    QrEFD_K230Grandeza: TSmallintField;
    QrEFD_K230Sigla: TWideStringField;
    QrEFD_K250: TmySQLQuery;
    DsEFD_K250: TDataSource;
    QrEFD_K235: TmySQLQuery;
    QrEFD_K235Grandeza: TSmallintField;
    QrEFD_K235Sigla: TWideStringField;
    QrEFD_K235NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K235ImporExpor: TSmallintField;
    QrEFD_K235AnoMes: TIntegerField;
    QrEFD_K235Empresa: TIntegerField;
    QrEFD_K235LinArq: TIntegerField;
    QrEFD_K235REG: TWideStringField;
    QrEFD_K235DT_SAIDA: TDateField;
    QrEFD_K235COD_ITEM: TWideStringField;
    QrEFD_K235QTD: TFloatField;
    QrEFD_K235COD_INS_SUBST: TWideStringField;
    QrEFD_K235Lk: TIntegerField;
    QrEFD_K235DataCad: TDateField;
    QrEFD_K235DataAlt: TDateField;
    QrEFD_K235UserCad: TIntegerField;
    QrEFD_K235UserAlt: TIntegerField;
    QrEFD_K235AlterWeb: TSmallintField;
    QrEFD_K235Ativo: TSmallintField;
    QrEFD_K235K230: TIntegerField;
    QrEFD_K235ID_SEK: TSmallintField;
    DsEFD_K235: TDataSource;
    QrEFD_K255: TmySQLQuery;
    QrEFD_K255Grandeza: TSmallintField;
    QrEFD_K255Sigla: TWideStringField;
    QrEFD_K255NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K255ImporExpor: TSmallintField;
    QrEFD_K255AnoMes: TIntegerField;
    QrEFD_K255Empresa: TIntegerField;
    QrEFD_K255LinArq: TIntegerField;
    QrEFD_K255REG: TWideStringField;
    QrEFD_K255DT_CONS: TDateField;
    QrEFD_K255COD_ITEM: TWideStringField;
    QrEFD_K255QTD: TFloatField;
    QrEFD_K255COD_INS_SUBST: TWideStringField;
    QrEFD_K255Lk: TIntegerField;
    QrEFD_K255DataCad: TDateField;
    QrEFD_K255DataAlt: TDateField;
    QrEFD_K255UserCad: TIntegerField;
    QrEFD_K255UserAlt: TIntegerField;
    QrEFD_K255AlterWeb: TSmallintField;
    QrEFD_K255Ativo: TSmallintField;
    QrEFD_K255K250: TIntegerField;
    QrEFD_K255ID_SEK: TSmallintField;
    DsEFD_K255: TDataSource;
    TabSheet16: TTabSheet;
    PCK235: TPageControl;
    TabSheet17: TTabSheet;
    TabSheet18: TTabSheet;
    DBGK230: TdmkDBGridZTO;
    Splitter3: TSplitter;
    DBGK235: TdmkDBGridZTO;
    DBGK250: TdmkDBGridZTO;
    Splitter4: TSplitter;
    DBGK255: TdmkDBGridZTO;
    K2301: TMenuItem;
    K2351: TMenuItem;
    K2501: TMenuItem;
    K2551: TMenuItem;
    N2: TMenuItem;
    odos1: TMenuItem;
    Vaiparajanelademovimento1: TMenuItem;
    QrEFD_K255ID_Item: TLargeintField;
    QrEFD_K235ID_Item: TLargeintField;
    QrEFD_K250Grandeza: TSmallintField;
    QrEFD_K250Sigla: TWideStringField;
    QrEFD_K250NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K250DT_PROD_Txt: TWideStringField;
    QrEFD_K250ImporExpor: TSmallintField;
    QrEFD_K250AnoMes: TIntegerField;
    QrEFD_K250Empresa: TIntegerField;
    QrEFD_K250LinArq: TIntegerField;
    QrEFD_K250REG: TWideStringField;
    QrEFD_K250K100: TIntegerField;
    QrEFD_K250DT_PROD: TDateField;
    QrEFD_K250COD_ITEM: TWideStringField;
    QrEFD_K250QTD: TFloatField;
    QrEFD_K250Lk: TIntegerField;
    QrEFD_K250DataCad: TDateField;
    QrEFD_K250DataAlt: TDateField;
    QrEFD_K250UserCad: TIntegerField;
    QrEFD_K250UserAlt: TIntegerField;
    QrEFD_K250AlterWeb: TSmallintField;
    QrEFD_K250Ativo: TSmallintField;
    QrEFD_K250ID_SEK: TSmallintField;
    QrEFD_K250MovimID: TIntegerField;
    QrEFD_K250Codigo: TIntegerField;
    QrEFD_K250MovimCod: TIntegerField;
    QrEFD_K250COD_DOC_OP: TWideStringField;
    PMImprime: TPopupMenu;
    frxEFD_SPEDE_K200_001: TfrxReport;
    frxDsEFD_K200: TfrxDBDataset;
    QrEFD_K200Sigla: TWideStringField;
    QrEFD_K200GraGruX: TIntegerField;
    QrEFD_K200Grandeza: TIntegerField;
    QrEFD_K200Pecas: TFloatField;
    QrEFD_K200AreaM2: TFloatField;
    QrEFD_K200PesoKg: TFloatField;
    frxEFD_SPEDE_K220_001: TfrxReport;
    frxDsEFD_K220: TfrxDBDataset;
    QrEFD_K220Codigo: TIntegerField;
    QrEFD_K220MovimCod: TIntegerField;
    QrEFD_K220CtrlDst: TIntegerField;
    QrEFD_K220GGXDst: TIntegerField;
    QrEFD_K220GGXOri: TIntegerField;
    QrEFD_K255MovimID: TIntegerField;
    QrEFD_K255Codigo: TIntegerField;
    QrEFD_K255MovimCod: TIntegerField;
    QrEFD_K255Controle: TIntegerField;
    QrEFD_K235MovimID: TIntegerField;
    QrEFD_K235Codigo: TIntegerField;
    QrEFD_K235MovimCod: TIntegerField;
    QrEFD_K235Controle: TIntegerField;
    BlocoKRegistrosK2201: TMenuItem;
    BlocoKRegistrosK2001: TMenuItem;
    QrEFD_K220Sigla: TWideStringField;
    frxEFD_SPEDE_K230_001: TfrxReport;
    frxDsEFD_K230: TfrxDBDataset;
    frxDsEFD_K235: TfrxDBDataset;
    frxDsEFD_K250: TfrxDBDataset;
    frxDsEFD_K255: TfrxDBDataset;
    ProduoK230aK2551: TMenuItem;
    QrEFD_K220ESOMIEM: TIntegerField;
    TabSheet14: TTabSheet;
    QrEFD_K_ConfG: TmySQLQuery;
    DsEFD_K_ConfG: TDataSource;
    dmkDBGridZTO7: TdmkDBGridZTO;
    frxEFD_SPEDE_K_VERIF_001: TfrxReport;
    frxDsEFD_K_ConfG: TfrxDBDataset;
    Panel28: TPanel;
    BtDifGera: TBitBtn;
    BtDifImprime: TBitBtn;
    QrEFD_K_ConfGGraGruX: TIntegerField;
    QrEFD_K_ConfGSdo_Ini: TFloatField;
    QrEFD_K_ConfGCompra: TFloatField;
    QrEFD_K_ConfGProducao: TFloatField;
    QrEFD_K_ConfGEntrouClasse: TFloatField;
    QrEFD_K_ConfGConsumo: TFloatField;
    QrEFD_K_ConfGSaiuClasse: TFloatField;
    QrEFD_K_ConfGVenda: TFloatField;
    QrEFD_K_ConfGFinal: TFloatField;
    QrEFD_K_ConfGDiferenca: TFloatField;
    QrEFD_K_ConfGSigla: TWideStringField;
    QrEFD_K_ConfGNO_PRD_TAM_COR: TWideStringField;
    QrEFD_K_ConfGGrandeza: TSmallintField;
    QrPsq01: TmySQLQuery;
    Splitter5: TSplitter;
    DsPsq01: TDataSource;
    QrEFD_K_ConfGIndevido: TFloatField;
    QrEFD_K_ConfGSubProduto: TFloatField;
    BitBtn1: TBitBtn;
    QrEFD_K_ConfGErrEmpresa: TFloatField;
    frxErrEmpresa: TfrxReport;
    QrErrEmpresa: TmySQLQuery;
    frxDsErrEmpresa: TfrxDBDataset;
    N3: TMenuItem;
    Erroempresaitens1: TMenuItem;
    QrErrEmpresaCodigo: TIntegerField;
    QrErrEmpresaMovimID: TIntegerField;
    QrErrEmpresaMovimCod: TIntegerField;
    QrErrEmpresaEmpresa: TIntegerField;
    QrErrEmpresaGraGruX: TIntegerField;
    QrErrEmpresaPecas: TFloatField;
    QrErrEmpresaAreaM2: TFloatField;
    QrErrEmpresaPesoKg: TFloatField;
    QrErrEmpresaGrandeza: TSmallintField;
    QrErrEmpresaNO_PRD_TAM_COR: TWideStringField;
    QrErrEmpresaSrcMovID: TIntegerField;
    QrErrEmpresaSrcNivel1: TIntegerField;
    QrErrEmpresaSrcNivel2: TIntegerField;
    QrEFD_K_ConfGETE: TFloatField;
    QrEFD_K_ConfGBalancoIndev: TFloatField;
    Panel29: TPanel;
    BtPeriodo: TBitBtn;
    Panel30: TPanel;
    Panel31: TPanel;
    dmkDBGridZTO5: TdmkDBGridZTO;
    BtK100: TBitBtn;
    Panel33: TPanel;
    BtE500: TBitBtn;
    Panel32: TPanel;
    BtE100: TBitBtn;
    QrEFD_K_ConfGESTSTabSorc: TIntegerField;
    Panel34: TPanel;
    DBGPsq01: TdmkDBGridZTO;
    DBGPsq02: TdmkDBGridZTO;
    QrPsq02: TmySQLQuery;
    DsPsq02: TDataSource;
    TabSheet15: TTabSheet;
    Panel35: TPanel;
    Panel36: TPanel;
    Bt1010: TBitBtn;
    QrEFD_1010: TmySQLQuery;
    QrEFD_1010ImporExpor: TSmallintField;
    QrEFD_1010AnoMes: TIntegerField;
    QrEFD_1010Empresa: TIntegerField;
    QrEFD_1010LinArq: TIntegerField;
    QrEFD_1010REG: TWideStringField;
    QrEFD_1010IND_EXP: TWideStringField;
    QrEFD_1010IND_CCRF: TWideStringField;
    QrEFD_1010IND_COMB: TWideStringField;
    QrEFD_1010IND_USINA: TWideStringField;
    QrEFD_1010IND_VA: TWideStringField;
    QrEFD_1010IND_EE: TWideStringField;
    QrEFD_1010IND_CART: TWideStringField;
    QrEFD_1010IND_FORM: TWideStringField;
    QrEFD_1010IND_AER: TWideStringField;
    QrEFD_1010Lk: TIntegerField;
    QrEFD_1010DataCad: TDateField;
    QrEFD_1010DataAlt: TDateField;
    QrEFD_1010UserCad: TIntegerField;
    QrEFD_1010UserAlt: TIntegerField;
    QrEFD_1010AlterWeb: TSmallintField;
    QrEFD_1010Ativo: TSmallintField;
    Panel37: TPanel;
    CkIND_EXP: TDBCheckBox;
    CkIND_CCRF: TDBCheckBox;
    CkIND_COMB: TDBCheckBox;
    CkIND_USINA: TDBCheckBox;
    CkIND_VA: TDBCheckBox;
    CkIND_EE: TDBCheckBox;
    CkIND_CART: TDBCheckBox;
    CkIND_FORM: TDBCheckBox;
    CkIND_AER: TDBCheckBox;
    DsEFD_1010: TDataSource;
    TabSheet19: TTabSheet;
    QrEFD_K210: TmySQLQuery;
    DsEFD_K210: TDataSource;
    QrEFD_K215: TmySQLQuery;
    DsEFD_K215: TDataSource;
    frxDsEFD_K215: TfrxDBDataset;
    frxDsEFD_K210: TfrxDBDataset;
    QrEFD_K215Grandeza: TSmallintField;
    QrEFD_K215Sigla: TWideStringField;
    QrEFD_K215NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K215ImporExpor: TSmallintField;
    QrEFD_K215AnoMes: TIntegerField;
    QrEFD_K215Empresa: TIntegerField;
    QrEFD_K215LinArq: TIntegerField;
    QrEFD_K215REG: TWideStringField;
    QrEFD_K215K210: TIntegerField;
    QrEFD_K215COD_ITEM_DES: TWideStringField;
    QrEFD_K215QTD_DES: TFloatField;
    QrEFD_K215ID_Item: TLargeintField;
    QrEFD_K215MovimID: TIntegerField;
    QrEFD_K215Codigo: TIntegerField;
    QrEFD_K215MovimCod: TIntegerField;
    QrEFD_K215Controle: TIntegerField;
    QrEFD_K215GraGruX: TIntegerField;
    QrEFD_K215ESTSTabSorc: TIntegerField;
    QrEFD_K215Lk: TIntegerField;
    QrEFD_K215DataCad: TDateField;
    QrEFD_K215DataAlt: TDateField;
    QrEFD_K215UserCad: TIntegerField;
    QrEFD_K215UserAlt: TIntegerField;
    QrEFD_K215AlterWeb: TSmallintField;
    QrEFD_K215Ativo: TSmallintField;
    QrEFD_K210Grandeza: TSmallintField;
    QrEFD_K210Sigla: TWideStringField;
    QrEFD_K210NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K210DT_FIN_OS_Txt: TWideStringField;
    QrEFD_K210ImporExpor: TSmallintField;
    QrEFD_K210AnoMes: TIntegerField;
    QrEFD_K210Empresa: TIntegerField;
    QrEFD_K210LinArq: TIntegerField;
    QrEFD_K210REG: TWideStringField;
    QrEFD_K210K100: TIntegerField;
    QrEFD_K210ID_SEK: TSmallintField;
    QrEFD_K210MovimID: TIntegerField;
    QrEFD_K210Codigo: TIntegerField;
    QrEFD_K210MovimCod: TIntegerField;
    QrEFD_K210DT_INI_OS: TDateField;
    QrEFD_K210DT_FIN_OS: TDateField;
    QrEFD_K210COD_DOC_OS: TWideStringField;
    QrEFD_K210COD_ITEM_ORI: TWideStringField;
    QrEFD_K210QTD_ORI: TFloatField;
    QrEFD_K210GraGruX: TIntegerField;
    QrEFD_K210Lk: TIntegerField;
    QrEFD_K210DataCad: TDateField;
    QrEFD_K210DataAlt: TDateField;
    QrEFD_K210UserCad: TIntegerField;
    QrEFD_K210UserAlt: TIntegerField;
    QrEFD_K210AlterWeb: TSmallintField;
    QrEFD_K210Ativo: TSmallintField;
    QrEFD_K_ConfGSaiuDesmonte: TFloatField;
    QrEFD_K_ConfGEntrouDesmonte: TFloatField;
    QrEFD_K230OriOpeProc: TSmallintField;
    QrEFD_K250OriOpeProc: TSmallintField;
    QrEFD_K210OriOpeProc: TSmallintField;
    Excluisegmentointeiroimportado1: TMenuItem;
    TabSheet20: TTabSheet;
    QrEFD_K270_230: TmySQLQuery;
    DsEFD_K270_230: TDataSource;
    DBGK270_230: TdmkDBGridZTO;
    Splitter7: TSplitter;
    dmkDBGridZTO11: TdmkDBGridZTO;
    QrEFD_K270_230Grandeza: TSmallintField;
    QrEFD_K270_230Sigla: TWideStringField;
    QrEFD_K270_230NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K270_230ImporExpor: TSmallintField;
    QrEFD_K270_230AnoMes: TIntegerField;
    QrEFD_K270_230Empresa: TIntegerField;
    QrEFD_K270_230LinArq: TIntegerField;
    QrEFD_K270_230REG: TWideStringField;
    QrEFD_K270_230K100: TIntegerField;
    QrEFD_K270_230ID_SEK: TSmallintField;
    QrEFD_K270_230MovimID: TIntegerField;
    QrEFD_K270_230Codigo: TIntegerField;
    QrEFD_K270_230MovimCod: TIntegerField;
    QrEFD_K270_230DT_INI_AP: TDateField;
    QrEFD_K270_230DT_FIN_AP: TDateField;
    QrEFD_K270_230COD_OP_OS: TWideStringField;
    QrEFD_K270_230COD_ITEM: TWideStringField;
    QrEFD_K270_230QTD_COR_POS: TFloatField;
    QrEFD_K270_230QTD_COR_NEG: TFloatField;
    QrEFD_K270_230ORIGEM: TWideStringField;
    QrEFD_K270_230GraGruX: TIntegerField;
    QrEFD_K270_230Lk: TIntegerField;
    QrEFD_K270_230DataCad: TDateField;
    QrEFD_K270_230DataAlt: TDateField;
    QrEFD_K270_230UserCad: TIntegerField;
    QrEFD_K270_230UserAlt: TIntegerField;
    QrEFD_K270_230AlterWeb: TSmallintField;
    QrEFD_K270_230Ativo: TSmallintField;
    QrEFD_K275_235: TmySQLQuery;
    QrEFD_K275_235Grandeza: TSmallintField;
    QrEFD_K275_235Sigla: TWideStringField;
    QrEFD_K275_235NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K275_235ImporExpor: TSmallintField;
    QrEFD_K275_235AnoMes: TIntegerField;
    QrEFD_K275_235Empresa: TIntegerField;
    QrEFD_K275_235LinArq: TIntegerField;
    QrEFD_K275_235REG: TWideStringField;
    QrEFD_K275_235K270: TIntegerField;
    QrEFD_K275_235ID_SEK: TSmallintField;
    QrEFD_K275_235COD_ITEM: TWideStringField;
    QrEFD_K275_235QTD_COR_POS: TFloatField;
    QrEFD_K275_235QTD_COR_NEG: TFloatField;
    QrEFD_K275_235COD_INS_SUBST: TWideStringField;
    QrEFD_K275_235ID_Item: TLargeintField;
    QrEFD_K275_235MovimID: TIntegerField;
    QrEFD_K275_235Codigo: TIntegerField;
    QrEFD_K275_235MovimCod: TIntegerField;
    QrEFD_K275_235Controle: TIntegerField;
    QrEFD_K275_235GraGruX: TIntegerField;
    QrEFD_K275_235ESTSTabSorc: TIntegerField;
    QrEFD_K275_235Lk: TIntegerField;
    QrEFD_K275_235DataCad: TDateField;
    QrEFD_K275_235DataAlt: TDateField;
    QrEFD_K275_235UserCad: TIntegerField;
    QrEFD_K275_235UserAlt: TIntegerField;
    QrEFD_K275_235AlterWeb: TSmallintField;
    QrEFD_K275_235Ativo: TSmallintField;
    DsEFD_K275_235: TDataSource;
    Splitter8: TSplitter;
    Label31: TLabel;
    DBGK270_220: TdmkDBGridZTO;
    QrEFD_K270_220: TmySQLQuery;
    DsEFD_K270_220: TDataSource;
    QrEFD_K275_220: TmySQLQuery;
    DsEFD_K275_220: TDataSource;
    QrEFD_K270_220NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K270_220Sigla: TWideStringField;
    QrEFD_K270_220ImporExpor: TSmallintField;
    QrEFD_K270_220AnoMes: TIntegerField;
    QrEFD_K270_220Empresa: TIntegerField;
    QrEFD_K270_220LinArq: TIntegerField;
    QrEFD_K270_220REG: TWideStringField;
    QrEFD_K270_220K100: TIntegerField;
    QrEFD_K270_220ID_SEK: TSmallintField;
    QrEFD_K270_220MovimID: TIntegerField;
    QrEFD_K270_220Codigo: TIntegerField;
    QrEFD_K270_220MovimCod: TIntegerField;
    QrEFD_K270_220DT_INI_AP: TDateField;
    QrEFD_K270_220DT_FIN_AP: TDateField;
    QrEFD_K270_220COD_OP_OS: TWideStringField;
    QrEFD_K270_220COD_ITEM: TWideStringField;
    QrEFD_K270_220QTD_COR_POS: TFloatField;
    QrEFD_K270_220QTD_COR_NEG: TFloatField;
    QrEFD_K270_220ORIGEM: TWideStringField;
    QrEFD_K270_220GraGruX: TIntegerField;
    QrEFD_K270_220Lk: TIntegerField;
    QrEFD_K270_220DataCad: TDateField;
    QrEFD_K270_220DataAlt: TDateField;
    QrEFD_K270_220UserCad: TIntegerField;
    QrEFD_K270_220UserAlt: TIntegerField;
    QrEFD_K270_220AlterWeb: TSmallintField;
    QrEFD_K270_220Ativo: TSmallintField;
    QrEFD_K275_220NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K275_220Sigla: TWideStringField;
    QrEFD_K275_220ImporExpor: TSmallintField;
    QrEFD_K275_220AnoMes: TIntegerField;
    QrEFD_K275_220Empresa: TIntegerField;
    QrEFD_K275_220LinArq: TIntegerField;
    QrEFD_K275_220REG: TWideStringField;
    QrEFD_K275_220K270: TIntegerField;
    QrEFD_K275_220ID_SEK: TSmallintField;
    QrEFD_K275_220COD_ITEM: TWideStringField;
    QrEFD_K275_220QTD_COR_POS: TFloatField;
    QrEFD_K275_220QTD_COR_NEG: TFloatField;
    QrEFD_K275_220COD_INS_SUBST: TWideStringField;
    QrEFD_K275_220ID_Item: TLargeintField;
    QrEFD_K275_220MovimID: TIntegerField;
    QrEFD_K275_220Codigo: TIntegerField;
    QrEFD_K275_220MovimCod: TIntegerField;
    QrEFD_K275_220Controle: TIntegerField;
    QrEFD_K275_220GraGruX: TIntegerField;
    QrEFD_K275_220ESTSTabSorc: TIntegerField;
    QrEFD_K275_220Lk: TIntegerField;
    QrEFD_K275_220DataCad: TDateField;
    QrEFD_K275_220DataAlt: TDateField;
    QrEFD_K275_220UserCad: TIntegerField;
    QrEFD_K275_220UserAlt: TIntegerField;
    QrEFD_K275_220AlterWeb: TSmallintField;
    QrEFD_K275_220Ativo: TSmallintField;
    QrEFD_K275_220ORIGEM: TWideStringField;
    DBGK275_220: TdmkDBGridZTO;
    frxDsEFD_K270_230: TfrxDBDataset;
    frxDsEFD_K275_235: TfrxDBDataset;
    frxDsEFD_K270_220: TfrxDBDataset;
    frxDsEFD_K275_220: TfrxDBDataset;
    QrEFD_K270_250: TmySQLQuery;
    DsEFD_K270_250: TDataSource;
    QrEFD_K275_255: TmySQLQuery;
    DsEFD_K275_255: TDataSource;
    QrEFD_K270_250Grandeza: TSmallintField;
    QrEFD_K270_250Sigla: TWideStringField;
    QrEFD_K270_250NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K270_250ImporExpor: TSmallintField;
    QrEFD_K270_250AnoMes: TIntegerField;
    QrEFD_K270_250Empresa: TIntegerField;
    QrEFD_K270_250LinArq: TIntegerField;
    QrEFD_K270_250REG: TWideStringField;
    QrEFD_K270_250K100: TIntegerField;
    QrEFD_K270_250ID_SEK: TSmallintField;
    QrEFD_K270_250MovimID: TIntegerField;
    QrEFD_K270_250Codigo: TIntegerField;
    QrEFD_K270_250MovimCod: TIntegerField;
    QrEFD_K270_250DT_INI_AP: TDateField;
    QrEFD_K270_250DT_FIN_AP: TDateField;
    QrEFD_K270_250COD_OP_OS: TWideStringField;
    QrEFD_K270_250COD_ITEM: TWideStringField;
    QrEFD_K270_250QTD_COR_POS: TFloatField;
    QrEFD_K270_250QTD_COR_NEG: TFloatField;
    QrEFD_K270_250ORIGEM: TWideStringField;
    QrEFD_K270_250GraGruX: TIntegerField;
    QrEFD_K270_250Lk: TIntegerField;
    QrEFD_K270_250DataCad: TDateField;
    QrEFD_K270_250DataAlt: TDateField;
    QrEFD_K270_250UserCad: TIntegerField;
    QrEFD_K270_250UserAlt: TIntegerField;
    QrEFD_K270_250AlterWeb: TSmallintField;
    QrEFD_K270_250Ativo: TSmallintField;
    QrEFD_K275_255Grandeza: TSmallintField;
    QrEFD_K275_255Sigla: TWideStringField;
    QrEFD_K275_255NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K275_255ImporExpor: TSmallintField;
    QrEFD_K275_255AnoMes: TIntegerField;
    QrEFD_K275_255Empresa: TIntegerField;
    QrEFD_K275_255LinArq: TIntegerField;
    QrEFD_K275_255REG: TWideStringField;
    QrEFD_K275_255K270: TIntegerField;
    QrEFD_K275_255ID_SEK: TSmallintField;
    QrEFD_K275_255COD_ITEM: TWideStringField;
    QrEFD_K275_255QTD_COR_POS: TFloatField;
    QrEFD_K275_255QTD_COR_NEG: TFloatField;
    QrEFD_K275_255COD_INS_SUBST: TWideStringField;
    QrEFD_K275_255ID_Item: TLargeintField;
    QrEFD_K275_255MovimID: TIntegerField;
    QrEFD_K275_255Codigo: TIntegerField;
    QrEFD_K275_255MovimCod: TIntegerField;
    QrEFD_K275_255Controle: TIntegerField;
    QrEFD_K275_255GraGruX: TIntegerField;
    QrEFD_K275_255ESTSTabSorc: TIntegerField;
    QrEFD_K275_255Lk: TIntegerField;
    QrEFD_K275_255DataCad: TDateField;
    QrEFD_K275_255DataAlt: TDateField;
    QrEFD_K275_255UserCad: TIntegerField;
    QrEFD_K275_255UserAlt: TIntegerField;
    QrEFD_K275_255AlterWeb: TSmallintField;
    QrEFD_K275_255Ativo: TSmallintField;
    TabSheet21: TTabSheet;
    dmkDBGridZTO13: TdmkDBGridZTO;
    Splitter9: TSplitter;
    dmkDBGridZTO14: TdmkDBGridZTO;
    QrEFD_K270_210: TmySQLQuery;
    DsEFD_K270_210: TDataSource;
    QrEFD_K270_210Grandeza: TSmallintField;
    QrEFD_K270_210Sigla: TWideStringField;
    QrEFD_K270_210NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K270_210ImporExpor: TSmallintField;
    QrEFD_K270_210AnoMes: TIntegerField;
    QrEFD_K270_210Empresa: TIntegerField;
    QrEFD_K270_210LinArq: TIntegerField;
    QrEFD_K270_210REG: TWideStringField;
    QrEFD_K270_210K100: TIntegerField;
    QrEFD_K270_210ID_SEK: TSmallintField;
    QrEFD_K270_210MovimID: TIntegerField;
    QrEFD_K270_210Codigo: TIntegerField;
    QrEFD_K270_210MovimCod: TIntegerField;
    QrEFD_K270_210DT_INI_AP: TDateField;
    QrEFD_K270_210DT_FIN_AP: TDateField;
    QrEFD_K270_210COD_OP_OS: TWideStringField;
    QrEFD_K270_210COD_ITEM: TWideStringField;
    QrEFD_K270_210QTD_COR_POS: TFloatField;
    QrEFD_K270_210QTD_COR_NEG: TFloatField;
    QrEFD_K270_210ORIGEM: TWideStringField;
    QrEFD_K270_210GraGruX: TIntegerField;
    QrEFD_K270_210Lk: TIntegerField;
    QrEFD_K270_210DataCad: TDateField;
    QrEFD_K270_210DataAlt: TDateField;
    QrEFD_K270_210UserCad: TIntegerField;
    QrEFD_K270_210UserAlt: TIntegerField;
    QrEFD_K270_210AlterWeb: TSmallintField;
    QrEFD_K270_210Ativo: TSmallintField;
    QrEFD_K275_215: TmySQLQuery;
    DsEFD_K275_215: TDataSource;
    QrEFD_K275_215NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K275_215Sigla: TWideStringField;
    QrEFD_K275_215ImporExpor: TSmallintField;
    QrEFD_K275_215AnoMes: TIntegerField;
    QrEFD_K275_215Empresa: TIntegerField;
    QrEFD_K275_215LinArq: TIntegerField;
    QrEFD_K275_215REG: TWideStringField;
    QrEFD_K275_215K270: TIntegerField;
    QrEFD_K275_215ID_SEK: TSmallintField;
    QrEFD_K275_215COD_ITEM: TWideStringField;
    QrEFD_K275_215QTD_COR_POS: TFloatField;
    QrEFD_K275_215QTD_COR_NEG: TFloatField;
    QrEFD_K275_215COD_INS_SUBST: TWideStringField;
    QrEFD_K275_215ID_Item: TLargeintField;
    QrEFD_K275_215MovimID: TIntegerField;
    QrEFD_K275_215Codigo: TIntegerField;
    QrEFD_K275_215MovimCod: TIntegerField;
    QrEFD_K275_215Controle: TIntegerField;
    QrEFD_K275_215GraGruX: TIntegerField;
    QrEFD_K275_215ESTSTabSorc: TIntegerField;
    QrEFD_K275_215Lk: TIntegerField;
    QrEFD_K275_215DataCad: TDateField;
    QrEFD_K275_215DataAlt: TDateField;
    QrEFD_K275_215UserCad: TIntegerField;
    QrEFD_K275_215UserAlt: TIntegerField;
    QrEFD_K275_215AlterWeb: TSmallintField;
    QrEFD_K275_215Ativo: TSmallintField;
    PageControl2: TPageControl;
    TabSheet22: TTabSheet;
    TabSheet23: TTabSheet;
    dmkDBGridZTO8: TdmkDBGridZTO;
    Splitter6: TSplitter;
    dmkDBGridZTO9: TdmkDBGridZTO;
    dmkDBGridZTO15: TdmkDBGridZTO;
    Splitter10: TSplitter;
    dmkDBGridZTO16: TdmkDBGridZTO;
    TabSheet24: TTabSheet;
    DBGK260: TdmkDBGridZTO;
    DBGK265: TdmkDBGridZTO;
    Splitter11: TSplitter;
    QrEFD_K260: TmySQLQuery;
    QrEFD_K265: TmySQLQuery;
    QrEFD_K265Grandeza: TSmallintField;
    QrEFD_K265Sigla: TWideStringField;
    QrEFD_K265NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K265ImporExpor: TSmallintField;
    QrEFD_K265AnoMes: TIntegerField;
    QrEFD_K265Empresa: TIntegerField;
    QrEFD_K265LinArq: TIntegerField;
    QrEFD_K265REG: TWideStringField;
    QrEFD_K265K260: TIntegerField;
    QrEFD_K265ID_SEK: TSmallintField;
    QrEFD_K265COD_ITEM: TWideStringField;
    QrEFD_K265QTD_CONS: TFloatField;
    QrEFD_K265QTD_RET: TFloatField;
    QrEFD_K265ID_Item: TLargeintField;
    QrEFD_K265MovimID: TIntegerField;
    QrEFD_K265Codigo: TIntegerField;
    QrEFD_K265MovimCod: TIntegerField;
    QrEFD_K265Controle: TIntegerField;
    QrEFD_K265GraGruX: TIntegerField;
    QrEFD_K265ESTSTabSorc: TIntegerField;
    QrEFD_K265Lk: TIntegerField;
    QrEFD_K265DataCad: TDateField;
    QrEFD_K265DataAlt: TDateField;
    QrEFD_K265UserCad: TIntegerField;
    QrEFD_K265UserAlt: TIntegerField;
    QrEFD_K265AlterWeb: TSmallintField;
    QrEFD_K265Ativo: TSmallintField;
    QrEFD_K265OriOpeProc: TSmallintField;
    QrEFD_K260Grandeza: TSmallintField;
    QrEFD_K260Sigla: TWideStringField;
    QrEFD_K260NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K260DT_RET_Txt: TWideStringField;
    QrEFD_K260ImporExpor: TSmallintField;
    QrEFD_K260AnoMes: TIntegerField;
    QrEFD_K260Empresa: TIntegerField;
    QrEFD_K260LinArq: TIntegerField;
    QrEFD_K260REG: TWideStringField;
    QrEFD_K260K100: TIntegerField;
    QrEFD_K260ID_SEK: TSmallintField;
    QrEFD_K260MovimID: TIntegerField;
    QrEFD_K260Codigo: TIntegerField;
    QrEFD_K260MovimCod: TIntegerField;
    QrEFD_K260COD_OP_OS: TWideStringField;
    QrEFD_K260COD_ITEM: TWideStringField;
    QrEFD_K260DT_SAIDA: TDateField;
    QrEFD_K260QTD_SAIDA: TFloatField;
    QrEFD_K260DT_RET: TDateField;
    QrEFD_K260QTD_RET: TFloatField;
    QrEFD_K260GraGruX: TIntegerField;
    QrEFD_K260Lk: TIntegerField;
    QrEFD_K260DataCad: TDateField;
    QrEFD_K260DataAlt: TDateField;
    QrEFD_K260UserCad: TIntegerField;
    QrEFD_K260UserAlt: TIntegerField;
    QrEFD_K260AlterWeb: TSmallintField;
    QrEFD_K260Ativo: TSmallintField;
    QrEFD_K260OriOpeProc: TSmallintField;
    DsEFD_K260: TDataSource;
    DsEFD_K265: TDataSource;
    K2601: TMenuItem;
    K2651: TMenuItem;
    QrEFD_K_ConfGProduReforma: TFloatField;
    QrEFD_K_ConfGInsumReforma: TFloatField;
    TabSheet25: TTabSheet;
    DBGK280: TdmkDBGridZTO;
    QrEFD_K280: TmySQLQuery;
    DsEFD_K280: TDataSource;
    QrEFD_K280NO_PART: TWideStringField;
    QrEFD_K280Sigla: TWideStringField;
    QrEFD_K280NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K280NO_IND_EST: TWideStringField;
    QrEFD_K280ImporExpor: TSmallintField;
    QrEFD_K280AnoMes: TIntegerField;
    QrEFD_K280Empresa: TIntegerField;
    QrEFD_K280LinArq: TIntegerField;
    QrEFD_K280REG: TWideStringField;
    QrEFD_K280K100: TIntegerField;
    QrEFD_K280COD_ITEM: TWideStringField;
    QrEFD_K280QTD_COR_POS: TFloatField;
    QrEFD_K280QTD_COR_NEG: TFloatField;
    QrEFD_K280IND_EST: TWideStringField;
    QrEFD_K280COD_PART: TWideStringField;
    QrEFD_K280BalID: TIntegerField;
    QrEFD_K280BalNum: TIntegerField;
    QrEFD_K280BalItm: TIntegerField;
    QrEFD_K280BalEnt: TIntegerField;
    QrEFD_K280GraGruX: TIntegerField;
    QrEFD_K280Grandeza: TIntegerField;
    QrEFD_K280Pecas: TFloatField;
    QrEFD_K280AreaM2: TFloatField;
    QrEFD_K280PesoKg: TFloatField;
    QrEFD_K280Entidade: TIntegerField;
    QrEFD_K280Tipo_Item: TIntegerField;
    QrEFD_K280Lk: TIntegerField;
    QrEFD_K280DataCad: TDateField;
    QrEFD_K280DataAlt: TDateField;
    QrEFD_K280UserCad: TIntegerField;
    QrEFD_K280UserAlt: TIntegerField;
    QrEFD_K280AlterWeb: TSmallintField;
    QrEFD_K280Ativo: TSmallintField;
    QrEFD_K280ESTSTabSorc: TIntegerField;
    QrEFD_K280OriOpeProc: TSmallintField;
    QrEFD_K280OrigemIDKnd: TIntegerField;
    QrEFD_K280DT_EST: TDateField;
    ExcluiK200_280_1: TMenuItem;
    N4: TMenuItem;
    BtK280: TBitBtn;
    Label32: TLabel;
    Label33: TLabel;
    PMK280: TPopupMenu;
    ExcluiK280_1: TMenuItem;
    ExcluiK220_280_1: TMenuItem;
    ExcluiK230_280_1: TMenuItem;
    QrEFD_K280NO_OriSPEDEFDKnd: TWideStringField;
    QrEFD_K280RegisPai: TWideStringField;
    Excluiregistrosd1: TMenuItem;
    QrEFD_K280RegisAvo: TWideStringField;
    QrEFD_K270_260: TmySQLQuery;
    DsEFD_K270_260: TDataSource;
    QrEFD_K270_260Grandeza: TSmallintField;
    QrEFD_K270_260Sigla: TWideStringField;
    QrEFD_K270_260NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K270_260ImporExpor: TSmallintField;
    QrEFD_K270_260AnoMes: TIntegerField;
    QrEFD_K270_260Empresa: TIntegerField;
    QrEFD_K270_260LinArq: TIntegerField;
    QrEFD_K270_260REG: TWideStringField;
    QrEFD_K270_260K100: TIntegerField;
    QrEFD_K270_260ID_SEK: TSmallintField;
    QrEFD_K270_260MovimID: TIntegerField;
    QrEFD_K270_260Codigo: TIntegerField;
    QrEFD_K270_260MovimCod: TIntegerField;
    QrEFD_K270_260DT_INI_AP: TDateField;
    QrEFD_K270_260DT_FIN_AP: TDateField;
    QrEFD_K270_260COD_OP_OS: TWideStringField;
    QrEFD_K270_260COD_ITEM: TWideStringField;
    QrEFD_K270_260QTD_COR_POS: TFloatField;
    QrEFD_K270_260QTD_COR_NEG: TFloatField;
    QrEFD_K270_260ORIGEM: TWideStringField;
    QrEFD_K270_260GraGruX: TIntegerField;
    QrEFD_K270_260Lk: TIntegerField;
    QrEFD_K270_260DataCad: TDateField;
    QrEFD_K270_260DataAlt: TDateField;
    QrEFD_K270_260UserCad: TIntegerField;
    QrEFD_K270_260UserAlt: TIntegerField;
    QrEFD_K270_260AlterWeb: TSmallintField;
    QrEFD_K270_260Ativo: TSmallintField;
    TabSheet26: TTabSheet;
    dmkDBGridZTO10: TdmkDBGridZTO;
    Splitter12: TSplitter;
    dmkDBGridZTO12: TdmkDBGridZTO;
    QrEFD_K275_265: TmySQLQuery;
    DsEFD_K275_265: TDataSource;
    QrEFD_K275_265Grandeza: TSmallintField;
    QrEFD_K275_265Sigla: TWideStringField;
    QrEFD_K275_265NO_PRD_TAM_COR: TWideStringField;
    QrEFD_K275_265ImporExpor: TSmallintField;
    QrEFD_K275_265AnoMes: TIntegerField;
    QrEFD_K275_265Empresa: TIntegerField;
    QrEFD_K275_265LinArq: TIntegerField;
    QrEFD_K275_265REG: TWideStringField;
    QrEFD_K275_265K270: TIntegerField;
    QrEFD_K275_265ID_SEK: TSmallintField;
    QrEFD_K275_265COD_ITEM: TWideStringField;
    QrEFD_K275_265QTD_COR_POS: TFloatField;
    QrEFD_K275_265QTD_COR_NEG: TFloatField;
    QrEFD_K275_265COD_INS_SUBST: TWideStringField;
    QrEFD_K275_265ID_Item: TLargeintField;
    QrEFD_K275_265MovimID: TIntegerField;
    QrEFD_K275_265Codigo: TIntegerField;
    QrEFD_K275_265MovimCod: TIntegerField;
    QrEFD_K275_265Controle: TIntegerField;
    QrEFD_K275_265GraGruX: TIntegerField;
    QrEFD_K275_265ESTSTabSorc: TIntegerField;
    QrEFD_K275_265Lk: TIntegerField;
    QrEFD_K275_265DataCad: TDateField;
    QrEFD_K275_265DataAlt: TDateField;
    QrEFD_K275_265UserCad: TIntegerField;
    QrEFD_K275_265UserAlt: TIntegerField;
    QrEFD_K275_265AlterWeb: TSmallintField;
    QrEFD_K275_265Ativo: TSmallintField;
    N5: TMenuItem;
    ExcluirTODASTABELASK1: TMenuItem;
    QrEFD_K280DebCred: TSmallintField;
    procedure BtSaida0Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPeriodoClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrEFD_E001BeforeClose(DataSet: TDataSet);
    procedure QrEFD_E001AfterScroll(DataSet: TDataSet);
    procedure PME111Popup(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure QrEFD_E001AfterOpen(DataSet: TDataSet);
    procedure PMPeriodoPopup(Sender: TObject);
    procedure BtImprime1Click(Sender: TObject);
    procedure RGEmitenteClick(Sender: TObject);
    procedure PME100Popup(Sender: TObject);
    procedure Incluinovoperiodo1Click(Sender: TObject);
    procedure Excluiperiodoselecionado1Click(Sender: TObject);
    procedure QrEFD_E100BeforeClose(DataSet: TDataSet);
    procedure QrEFD_E100AfterScroll(DataSet: TDataSet);
    procedure Incluinovointervalo1Click(Sender: TObject);
    procedure Alteraintervaloselecionado1Click(Sender: TObject);
    procedure Excluiintervaloselecionado1Click(Sender: TObject);
    procedure QrEFD_E110AfterScroll(DataSet: TDataSet);
    procedure Incluinovoajuste1Click(Sender: TObject);
    procedure Alteraajusteselecionado1Click(Sender: TObject);
    procedure IncluinovaobrigaodoICMSarecolher1Click(Sender: TObject);
    procedure Alteraobrigaoselecionada1Click(Sender: TObject);
    procedure PMObrigacoesPopup(Sender: TObject);
    procedure Excluiajusteselecionado1Click(Sender: TObject);
    procedure Excluiobrigaoselecionada1Click(Sender: TObject);
    procedure QrEFD_E111BeforeClose(DataSet: TDataSet);
    procedure QrEFD_E111AfterScroll(DataSet: TDataSet);
    procedure Incluinovainformaoadicional1Click(Sender: TObject);
    procedure Alterainformaoadicionalselecionada1Click(Sender: TObject);
    procedure Excluiinformaoadicionalselecionada1Click(Sender: TObject);
    procedure Excluiidentificaoselecionada1Click(Sender: TObject);
    procedure Incluinovaidentificaodedocumentofiscal1Click(Sender: TObject);
    procedure Alteraidentificaoselecionada1Click(Sender: TObject);
    procedure PMValDecltorioPopup(Sender: TObject);
    procedure Incluinovovalordeclaratrio1Click(Sender: TObject);
    procedure Alteravalordeclaratrioselecionado1Click(Sender: TObject);
    procedure Excluivalordeclaratrioselecionado1Click(Sender: TObject);
    procedure QrEFD_E110BeforeClose(DataSet: TDataSet);
    procedure IncluinovointervaloIPI1Click(Sender: TObject);
    procedure AlteraintervaloIPIselecionado1Click(Sender: TObject);
    procedure ExcluiintervaloIPIselecionado1Click(Sender: TObject);
    procedure QrEFD_E500AfterScroll(DataSet: TDataSet);
    procedure QrEFD_E500BeforeClose(DataSet: TDataSet);
    procedure QrEFD_E520BeforeClose(DataSet: TDataSet);
    procedure QrEFD_E520AfterScroll(DataSet: TDataSet);
    procedure IncluiajustedaapuracaodoIPI1Click(Sender: TObject);
    procedure AlteraajustedaapuracaodoIPIselecionado1Click(Sender: TObject);
    procedure ExcluiajustedaapuracaodoIPIselecionado1Click(Sender: TObject);
    procedure PME530Popup(Sender: TObject);
    procedure PME500Popup(Sender: TObject);
    procedure BtE100Click(Sender: TObject);
    procedure BtE500Click(Sender: TObject);
    procedure BtE520Click(Sender: TObject);
    procedure BtE110Click(Sender: TObject);
    procedure BtE111Click(Sender: TObject);
    procedure BtE530Click(Sender: TObject);
    procedure BtE115Click(Sender: TObject);
    procedure BtE116Click(Sender: TObject);
    procedure QrEFD_E100AfterOpen(DataSet: TDataSet);
    procedure QrEFD_E500AfterOpen(DataSet: TDataSet);
    procedure QrEFD_E520AfterOpen(DataSet: TDataSet);
    procedure BtH005Click(Sender: TObject);
    procedure Incluidatadeinventrio1Click(Sender: TObject);
    procedure AlteraInventrio1Click(Sender: TObject);
    procedure Excluiinventrio1Click(Sender: TObject);
    procedure PMH005Popup(Sender: TObject);
    procedure QrEFD_H005AfterScroll(DataSet: TDataSet);
    procedure QrEFD_H005BeforeClose(DataSet: TDataSet);
    procedure QrEFD_H010BeforeClose(DataSet: TDataSet);
    procedure QrEFD_H010AfterScroll(DataSet: TDataSet);
    procedure Incluiitem1Click(Sender: TObject);
    procedure Alteraitem1Click(Sender: TObject);
    procedure Excluiitemns1Click(Sender: TObject);
    procedure QrEFD_H005AfterOpen(DataSet: TDataSet);
    procedure QrEFD_H010AfterOpen(DataSet: TDataSet);
    procedure BtH010Click(Sender: TObject);
    procedure Importadebalano1Click(Sender: TObject);
    procedure Incluiinformaocomplementar1Click(Sender: TObject);
    procedure Alterainformaocomplementar1Click(Sender: TObject);
    procedure Excluiinformaocomplementar1Click(Sender: TObject);
    procedure BtH020Click(Sender: TObject);
    procedure PMH020Popup(Sender: TObject);
    procedure PMH010Popup(Sender: TObject);
    procedure BtK100Click(Sender: TObject);
    procedure IncluinovointervalodeProduoeEstoque1Click(Sender: TObject);
    procedure AlteraintervalodeProduoeEstoque1Click(Sender: TObject);
    procedure ExcluiintervalodeProduoeEstoque1Click(Sender: TObject);
    procedure PMK100Popup(Sender: TObject);
    procedure ImportaK200_1Click(Sender: TObject);
    procedure IncluiK200_1Click(Sender: TObject);
    procedure AlteraK200_1Click(Sender: TObject);
    procedure ExcluiK200_1Click(Sender: TObject);
    procedure BtK200Click(Sender: TObject);
    procedure QrEFD_K100AfterOpen(DataSet: TDataSet);
    procedure QrEFD_K100BeforeClose(DataSet: TDataSet);
    procedure QrEFD_K100AfterScroll(DataSet: TDataSet);
    procedure PMK220Popup(Sender: TObject);
    procedure PMK200Popup(Sender: TObject);
    procedure ImportaK220_1Click(Sender: TObject);
    procedure IncluiK220_1Click(Sender: TObject);
    procedure AlteraK220_1Click(Sender: TObject);
    procedure ExcluiK220_1Click(Sender: TObject);
    procedure BtK220Click(Sender: TObject);
    procedure BtK230Click(Sender: TObject);
    procedure ImportaK230_1Click(Sender: TObject);
    procedure IncluiK230_1Click(Sender: TObject);
    procedure AlteraK230_1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QrEFD_K230AfterScroll(DataSet: TDataSet);
    procedure QrEFD_K250AfterScroll(DataSet: TDataSet);
    procedure QrEFD_K250BeforeClose(DataSet: TDataSet);
    procedure QrEFD_K230BeforeClose(DataSet: TDataSet);
    procedure PCK200Change(Sender: TObject);
    procedure K2301Click(Sender: TObject);
    procedure K2351Click(Sender: TObject);
    procedure K2501Click(Sender: TObject);
    procedure K2551Click(Sender: TObject);
    procedure odos1Click(Sender: TObject);
    procedure Vaiparajanelademovimento1Click(Sender: TObject);
    procedure DBGK250DblClick(Sender: TObject);
    procedure DBGK230DblClick(Sender: TObject);
    procedure frxEFD_SPEDE_K200_001GetValue(const VarName: string;
      var Value: Variant);
    procedure SbImprimeClick(Sender: TObject);
    procedure BlocoKRegistrosK2001Click(Sender: TObject);
    procedure BlocoKRegistrosK2201Click(Sender: TObject);
    procedure ProduoK230aK2551Click(Sender: TObject);
    procedure BtVerificaClick(Sender: TObject);
    procedure BtDifGeraClick(Sender: TObject);
    procedure BtDifImprimeClick(Sender: TObject);
    procedure dmkDBGridZTO7CellClick(Column: TColumn);
    procedure DBGPsq02DblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Erroempresaitens1Click(Sender: TObject);
    procedure Bt1010Click(Sender: TObject);
    procedure QrEFD_K210AfterScroll(DataSet: TDataSet);
    procedure QrEFD_K210BeforeClose(DataSet: TDataSet);
    procedure dmkDBGridZTO8DblClick(Sender: TObject);
    procedure Excluisegmentointeiroimportado1Click(Sender: TObject);
    procedure QrEFD_K270_230BeforeClose(DataSet: TDataSet);
    procedure QrEFD_K270_230AfterScroll(DataSet: TDataSet);
    procedure DBGK270_230DblClick(Sender: TObject);
    procedure QrEFD_K270_220AfterScroll(DataSet: TDataSet);
    procedure QrEFD_K270_220BeforeClose(DataSet: TDataSet);
    procedure QrEFD_K270_250AfterScroll(DataSet: TDataSet);
    procedure QrEFD_K270_250BeforeClose(DataSet: TDataSet);
    procedure QrEFD_K270_210BeforeClose(DataSet: TDataSet);
    procedure QrEFD_K270_210AfterScroll(DataSet: TDataSet);
    procedure QrEFD_K260AfterScroll(DataSet: TDataSet);
    procedure QrEFD_K260BeforeClose(DataSet: TDataSet);
    procedure K2601Click(Sender: TObject);
    procedure K2651Click(Sender: TObject);
    procedure PMK230Popup(Sender: TObject);
    procedure ExcluiK200_280_1Click(Sender: TObject);
    procedure BtK280Click(Sender: TObject);
    procedure ExcluiK280_1Click(Sender: TObject);
    procedure PMK280Popup(Sender: TObject);
    procedure ExcluiK220_280_1Click(Sender: TObject);
    procedure ExcluiK230_280_1Click(Sender: TObject);
    procedure Excluiregistrosd1Click(Sender: TObject);
    procedure QrEFD_K270_260AfterScroll(DataSet: TDataSet);
    procedure QrEFD_K270_260BeforeClose(DataSet: TDataSet);
    procedure ExcluirTODASTABELASK1Click(Sender: TObject);
  private
    { Private declarations }
    FColumnSel: String;
    //FExportarItem: Boolean;
    function  PeriodoJaExiste(ImporExpor, AnoMes, Empresa: Integer;
              Avisa: Boolean): Boolean;
    procedure InsUpdE100(SQLType: TSQLType);
    procedure InsUpdE111(SQLType: TSQLType);
    procedure InsUpdE112(SQLType: TSQLType);
    procedure InsUpdE113(SQLType: TSQLType);
    procedure InsUpdE115(SQLType: TSQLType);
    procedure InsUpdE116(SQLType: TSQLType);
    procedure InsUpdE500(SQLType: TSQLType);
    procedure InsUpdE530(SQLType: TSQLType);
    procedure InsUpdH005(SQLType: TSQLType);
    procedure InsUpdH010(SQLType: TSQLType);
    procedure InsUpdH020(SQLType: TSQLType);
    procedure InsUpdK100(SQLType: TSQLType);
    procedure InsUpdK200(SQLType: TSQLType);
    procedure InsUpdK220(SQLType: TSQLType);
    procedure InsUpdK230(SQLType: TSQLType);
    //
    procedure JanelaDoMovimento();
    //
    procedure MostraFormEFD_E110();
    procedure MostraFormEFD_E520();
    //
    procedure MostraFormEFD_1010();
    //
    procedure ExcluiItensdeTabela(Tabela: String);
    procedure ExcluiItensK7X(ImporExpor, AnoMes, Empresa: Integer;
              SohEspecifico: TSPED_EFD_K270_08_Origem);
    procedure ExcluiItensNormaisDeProducao();
    procedure ExcluiItensK27XParcialTipoEFD(OriSPEDEFDKnd: TOrigemSPEDEFDKnd);
    procedure ExcluiItensK280ParcialTipoEFD(OriSPEDEFDKnd: TOrigemSPEDEFDKnd);
    procedure ExcluiRegsitrosImportados_Estoque();
    procedure ExcluiRegsitrosImportados_Classe();
    procedure ExcluiRegsitrosImportados_Producao();

    //
  public
    { Public declarations }
    FEmpresa: Integer;
    FExpImpTXT, FEmprTXT: String;
    //
    procedure AtualizaValoresE520deE530();
    procedure AtualizaValoresH005deH010();
    procedure ReopenEFD_E001(AnoMes: Integer);
    procedure ReopenEFD_E100(LinArq: Integer);
    procedure ReopenEFD_E110();
    procedure ReopenEFD_E111(LinArq: Integer);
    procedure ReopenEFD_E112(LinArq: Integer);
    procedure ReopenEFD_E113(LinArq: Integer);
    procedure ReopenEFD_E115(LinArq: Integer);
    procedure ReopenEFD_E116(LinArq: Integer);
    procedure ReopenEFD_E500(LinArq: Integer);
    procedure ReopenEFD_E510(LinArq: Integer);
    procedure ReopenEFD_E520();
    procedure ReopenEFD_E530(LinArq: Integer);
    procedure ReopenEFD_H005(LinArq: Integer);
    procedure ReopenEFD_H010(LinArq: Integer);
    procedure ReopenEFD_H020(LinArq: Integer);
    procedure ReopenEFD_K100(LinArq: Integer);
    procedure ReopenEFD_K200(LinArq: Integer; ReopenKind: TReopenKind);
    procedure ReopenEFD_K210(LinArq: Integer);
    procedure ReopenEFD_K215(LinArq: Integer);
    procedure ReopenEFD_K220(LinArq: Integer);
    procedure ReopenEFD_K230(LinArq: Integer);
    procedure ReopenEFD_K235(LinArq: Integer);
    procedure ReopenEFD_K250(LinArq: Integer);
    procedure ReopenEFD_K255(LinArq: Integer);
    procedure ReopenEFD_K260(LinArq: Integer);
    procedure ReopenEFD_K265(LinArq: Integer);
    procedure ReopenEFD_K270_210(LinArq: Integer);
    procedure ReopenEFD_K270_220(LinArq: Integer);
    procedure ReopenEFD_K270_230(LinArq: Integer);
    procedure ReopenEFD_K270_250(LinArq: Integer);
    procedure ReopenEFD_K270_260(LinArq: Integer);
    procedure ReopenEFD_K275_215(LinArq: Integer);
    procedure ReopenEFD_K275_220(LinArq: Integer);
    procedure ReopenEFD_K275_235(LinArq: Integer);
    procedure ReopenEFD_K275_255(LinArq: Integer);
    procedure ReopenEFD_K275_265(LinArq: Integer);
    procedure ReopenEFD_K280(LinArq: Integer; ReopenKind: TReopenKind);

    procedure ReopenEFD_K_ConfG(GraGruX: Integer);
    procedure ReopenEFD_1010();
    //
    procedure VerificaBlocoK();
  end;

var
  FmEFD_E001: TFmEFD_E001;

implementation

uses UnMyObjects, Module, MyDBCheck, ModuleGeral, ModProd, UCreate,
  UnDmkProcFunc, CfgExpFile, UnGrade_Tabs, ModuleNFe_0000, UnSPED_EFD_PF,
  SPED_Listas, Principal, Periodo, UnALL_Jan,
  EFD_E100, EFD_E110, EFD_E111, EFD_E116, EFD_E112, EFD_E113, EFD_E115,
  EFD_E500, EFD_E520, EFD_E530, EFD_H005, EFD_H010, EFD_H020, EFD_K100,
  EFD_K200, EFD_K220, EFD_1010,
  UnVS_PF, PQx, SPED_EFD_Exporta;

{$R *.DFM}

const
  FFormatFloat = '00000';
  FImporExpor = 3; // Criar! N�o Mexer

procedure TFmEFD_E001.EdEmpresaChange(Sender: TObject);
begin
  if EdEmpresa.ValueVariant <> 0 then
  begin
    FEmpresa := DModG.QrEmpresasCodigo.Value;
    FEmprTXT := Geral.FF0(FEmpresa);
  end else
  begin
    FEmpresa := 0;
    FEmprTXT := '0';
  end;
  BtPeriodo.Enabled := FEmpresa <> 0;
  ReopenEFD_E001(0);
end;

procedure TFmEFD_E001.Erroempresaitens1Click(Sender: TObject);
var
  SQL_Periodo: String;
  DiaIni, DiaFim: TDateTime;
begin
  DiaIni      := Geral.AnoMesToData(QrEFD_E001AnoMes.Value, 1);
  DiaFim      := IncMonth(DiaIni, 1) -1;
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataHora ', DiaIni, DiaFim, True, True);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrEmpresa, Dmod.MyDB, [
 'SELECT vmi.SrcMovID, vmi.SrcNivel1, vmi.SrcNivel2, ',
 'vmi.Codigo, vmi.MovimID, vmi.MovimCod, vmi.Empresa,  ',
 'vmi.GraGruX, Pecas, AreaM2, PesoKg, med.Grandeza, ',
 'CONCAT(gg1.Nome, IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
 'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) NO_PRD_TAM_COR ',
 'FROM ' + CO_SEL_TAB_VMI + ' vmi   ',
 'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX   ',
 'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1   ',
 'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed   ',
 'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
 'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
 'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
 SQL_Periodo,
 'AND Empresa>-11 ', // N�o � uma empresa!
 '']);
 //
  MyObjects.frxDefineDataSets(frxErrEmpresa, [
  DModG.frxDsDono,
  frxDsErrEmpresa
  ]);
  //
  MyObjects.frxMostra(frxErrEmpresa, 'Itens erros de empresa');
end;

procedure TFmEFD_E001.ExcluiajustedaapuracaodoIPIselecionado1Click(
  Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o do ajuste selecionado?',
  Dmod.QrUpd, 'efd_e530', ['ImporExpor', 'AnoMes', 'Empresa', 'E520', 'LinArq'
  ], ['=','=','=','=','='], [QrEFD_E530ImporExpor.Value, QrEFD_E530AnoMes.Value,
  QrEFD_E530Empresa.Value, QrEFD_E530E520.Value, QrEFD_E530LinArq.Value], '') then
  begin
    AtualizaValoresE520deE530();
    ReopenEFD_E500(QrEFD_E500LinArq.Value);
  end;
end;

procedure TFmEFD_E001.Excluiajusteselecionado1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o do ajuste selecionado?',
  Dmod.QrUpd, 'efd_e111', ['ImporExpor', 'AnoMes', 'Empresa', 'E110', 'LinArq'
  ], ['=','=','=','=','='], [QrEFD_E111ImporExpor.Value, QrEFD_E111AnoMes.Value,
  QrEFD_E111Empresa.Value, QrEFD_E111E110.Value, QrEFD_E111LinArq.Value], '') then
    ReopenEFD_E111(QrEFD_E111LinArq.Value);
end;

procedure TFmEFD_E001.Excluiidentificaoselecionada1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o da identifica��o de documento selecionada?',
  Dmod.QrUpd, 'efd_e113', ['ImporExpor', 'AnoMes', 'Empresa', 'E111', 'LinArq'
  ], ['=','=','=','=','='], [QrEFD_E113ImporExpor.Value, QrEFD_E113AnoMes.Value,
  QrEFD_E113Empresa.Value, QrEFD_E113E111.Value, QrEFD_E113LinArq.Value], '') then
    ReopenEFD_E113(QrEFD_E113LinArq.Value);
end;

procedure TFmEFD_E001.Excluiinformaoadicionalselecionada1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o da informa��o adicional selecionada?',
  Dmod.QrUpd, 'efd_e112', ['ImporExpor', 'AnoMes', 'Empresa', 'E111', 'LinArq'
  ], ['=','=','=','=','='], [QrEFD_E112ImporExpor.Value, QrEFD_E112AnoMes.Value,
  QrEFD_E112Empresa.Value, QrEFD_E112E111.Value, QrEFD_E112LinArq.Value], '') then
    ReopenEFD_E112(QrEFD_E112LinArq.Value);
end;

procedure TFmEFD_E001.Excluiinformaocomplementar1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o da informa��o adicional selecionada?',
  Dmod.QrUpd, 'efd_h020', ['ImporExpor', 'AnoMes', 'Empresa', 'H010', 'LinArq'
  ], ['=','=','=','=','='], [QrEFD_H020ImporExpor.Value, QrEFD_H020AnoMes.Value,
  QrEFD_H020Empresa.Value, QrEFD_H020H010.Value, QrEFD_H020LinArq.Value], '') then
    ReopenEFD_H020(QrEFD_H020LinArq.Value);
end;

procedure TFmEFD_E001.ExcluiintervalodeProduoeEstoque1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do intervalo e seus valores?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
(*
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_k110', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [QrEFD_K110ImporExpor.Value, QrEFD_K110AnoMes.Value,
    QrEFD_K110Empresa.Value, QrEFD_K110LinArq.Value], '') then
    //
*)
(*
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_k100', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [QrEFD_K100ImporExpor.Value, QrEFD_K100AnoMes.Value,
    QrEFD_K100Empresa.Value, QrEFD_K100LinArq.Value], '') then
*)
    //
    ReopenEFD_K100(0);
  end;
end;

procedure TFmEFD_E001.ExcluiintervaloIPIselecionado1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do intervalo e seus valores?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_e110', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [QrEFD_E110ImporExpor.Value, QrEFD_E110AnoMes.Value,
    QrEFD_E110Empresa.Value, QrEFD_E110LinArq.Value], '') then
    //
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_e500', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [QrEFD_E500ImporExpor.Value, QrEFD_E500AnoMes.Value,
    QrEFD_E500Empresa.Value, QrEFD_E500LinArq.Value], '') then
    //
    ReopenEFD_E500(0);
  end;

end;

procedure TFmEFD_E001.Excluiintervaloselecionado1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do intervalo e seus valores?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_e110', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [QrEFD_E110ImporExpor.Value, QrEFD_E110AnoMes.Value,
    QrEFD_E110Empresa.Value, QrEFD_E110LinArq.Value], '') then
    //
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_e100', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [QrEFD_E100ImporExpor.Value, QrEFD_E100AnoMes.Value,
    QrEFD_E100Empresa.Value, QrEFD_E100LinArq.Value], '') then
    //
    ReopenEFD_E100(0);
  end;
end;

procedure TFmEFD_E001.Excluiinventrio1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do invent�rio selecionado?') = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_h005', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=', '='],
    [QrEFD_H005ImporExpor.Value, QrEFD_H005AnoMes.Value,
    QrEFD_H005Empresa.Value, QrEFD_H005LinArq.Value], '') then
    ReopenEFD_H005(0);
  end;
end;

procedure TFmEFD_E001.Excluiitemns1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEFD_H010, TDBGrid(DBGH010),
    'efd_h010', ['ImporExpor', 'AnoMes', 'Empresa', 'H005', 'LinArq'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'H005', 'LinArq'], istPergunta, '');
  AtualizaValoresH005deH010();
end;

procedure TFmEFD_E001.ExcluiItensDeTabela(Tabela: String);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'DELETE FROM ' + Tabela +
  ' WHERE ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value) +
  ' AND AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value) +
  ' AND Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value) +
  ';']);
end;

procedure TFmEFD_E001.ExcluiItensK27XParcialTipoEFD(
  OriSPEDEFDKnd: TOrigemSPEDEFDKnd);
begin
  if QrEFD_K100.RecordCount > 0 then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_k275', [
    'ImporExpor', 'AnoMes', 'Empresa', 'OriSPEDEFDKnd'],
    ['=','=','=','='],
    [QrEFD_K100ImporExpor.Value, QrEFD_K100AnoMes.Value,
    QrEFD_K100Empresa.Value, Integer(OriSPEDEFDKnd)], '') then
    begin
      if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_k270', [
      'ImporExpor', 'AnoMes', 'Empresa', 'OriSPEDEFDKnd'],
      ['=','=','=','='],
      [QrEFD_K100ImporExpor.Value, QrEFD_K100AnoMes.Value,
      QrEFD_K100Empresa.Value, Integer(OriSPEDEFDKnd)], '') then
      begin
        ReopenEFD_K270_210(0);
        ReopenEFD_K270_220(0);
        ReopenEFD_K270_230(0);
        ReopenEFD_K270_250(0);
        ReopenEFD_K270_260(0);
      end;
    end;
  end;
end;

procedure TFmEFD_E001.ExcluiItensK280ParcialTipoEFD(
  OriSPEDEFDKnd: TOrigemSPEDEFDKnd);
begin
  if QrEFD_K100.RecordCount > 0 then
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_k280', [
    'ImporExpor', 'AnoMes', 'Empresa', 'OriSPEDEFDKnd'],
    ['=','=','=','='],
    [QrEFD_K100ImporExpor.Value, QrEFD_K100AnoMes.Value,
    QrEFD_K100Empresa.Value, Integer(OriSPEDEFDKnd)], '') then
      ReopenEFD_K280(0, rkGrade);
end;

procedure TFmEFD_E001.ExcluiItensK7X(ImporExpor, AnoMes, Empresa: Integer;
  SohEspecifico: TSPED_EFD_K270_08_Origem);
const
  sProcName = 'FmEFD_E001.ExcluiItensK7X()';
var
  StrOri: String;
begin
  StrOri := Geral.FF0(Integer(SohEspecifico));
  if Integer(SohEspecifico) <> 0 then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_k270', [
    'ImporExpor', 'AnoMes', 'Empresa', 'ORIGEM'], [
    '=','=','=','='],
    [ImporExpor, AnoMes, Empresa, StrOri], '') then
    //
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_k275', [
    'ImporExpor', 'AnoMes', 'Empresa', 'ORIGEM'], [
    '=','=','=','='],
    [ImporExpor, AnoMes, Empresa, StrOri], '') then
    //
    case SohEspecifico of
      (*1*)sek2708oriK230K235: ReopenEFD_K270_230(0);
      (*2*)sek2708oriK250K255: ReopenEFD_K270_250(0);
      (*3*)sek2708oriK210K215: ReopenEFD_K270_210(0);
      (*4*)sek2708oriK260K265: ReopenEFD_K270_260(0);
      (*5*)sek2708oriK220:     ReopenEFD_K270_220(0);
      else Geral.MB_Aviso('Reopen n�o implementado em ' + sProcName);
    end;
  end else
  begin
    //if Geral.MB_Pergunta(
    //'Confirma a exclus�o de todos itens corre��o de apontamento do periodo selecionado?'
    //) = ID_YES then
    begin
      if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_k270', [
      'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
      [ImporExpor, AnoMes, Empresa], '') then
      //
      if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_k275', [
      'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
      [ImporExpor, AnoMes, Empresa], '') then
      //
      ReopenEFD_K270_210(0);
      ReopenEFD_K270_220(0);
      ReopenEFD_K270_230(0);
      ReopenEFD_K270_250(0);
      ReopenEFD_K270_260(0);
    end;
  end;
end;

procedure TFmEFD_E001.ExcluiItensNormaisDeProducao();
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o de todos os registros de produ��o?') = ID_YES then
  begin
    ExcluiItensDeTabela('efd_k210');
    ExcluiItensDeTabela('efd_k215');
    ExcluiItensDeTabela('efd_k230');
    ExcluiItensDeTabela('efd_k235');
    ExcluiItensDeTabela('efd_k250');
    ExcluiItensDeTabela('efd_k255');
    ExcluiItensDeTabela('efd_k260');
    ExcluiItensDeTabela('efd_k265');
    //
    ExcluiItensK7X(QrEFD_E001ImporExpor.Value, QrEFD_E001AnoMes.Value,
      QrEFD_E001Empresa.Value, sek2708oriK210K215);
    ExcluiItensK7X(QrEFD_E001ImporExpor.Value, QrEFD_E001AnoMes.Value,
      QrEFD_E001Empresa.Value, sek2708oriK230K235);
    ExcluiItensK7X(QrEFD_E001ImporExpor.Value, QrEFD_E001AnoMes.Value,
      QrEFD_E001Empresa.Value, sek2708oriK250K255);
    //
    ReopenEFD_K210(0);
    ReopenEFD_K230(0);
    ReopenEFD_K250(0);
    ReopenEFD_K260(0);
    ReopenEFD_K270_210(0);
    ReopenEFD_K270_230(0);
    ReopenEFD_K270_250(0);
    ReopenEFD_K280(0, rkGrade);
  end;
end;

procedure TFmEFD_E001.ExcluiK200_1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEFD_K200, TDBGrid(DBGK200),
    'efd_k200', ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'], istPergunta, '');
end;

procedure TFmEFD_E001.ExcluiK200_280_1Click(Sender: TObject);
begin
  ExcluiRegsitrosImportados_Estoque();
end;

procedure TFmEFD_E001.ExcluiK220_1Click(Sender: TObject);
  procedure ExcluiSelecionados();
    procedure ExcluiItemAtual(Reabre: Boolean);
    begin
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrEFD_K220, 'efd_k220',
      ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'],
      ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'], Reabre, '');
    end;
  var
    q: TSelType;
    n, c: Integer;
  begin
    DBCheck.Quais_Selecionou(QrEFD_K220, TDBGrid(DBGK220), q);
    case q of
      istAtual: ExcluiItemAtual(True);
      istSelecionados:
      begin
        with DBGK220.DataSource.DataSet do
        for n := 0 to DBGK220.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBGK220.SelectedRows.Items[n]));
          ExcluiItemAtual(False);
        end;
        ReopenEFD_K220(0);
        ReopenEFD_K270_220(0);
      end;
      istTodos:
      begin
        QrEFD_K220.First;
        while not QrEFD_K220.Eof do
        begin
          ExcluiItemAtual(False);
          //
          QrEFD_K220.Next;
        end;
        ExcluiItensDeTabela('efd_k23subprd');
        ExcluiItensDeTabela('efd_k25subprd');
        ExcluiItensK7X(QrEFD_E001ImporExpor.Value, QrEFD_E001AnoMes.Value,
          QrEFD_E001Empresa.Value, sek2708oriK220);
        ReopenEFD_K220(0);
        ReopenEFD_K270_220(0);
      end;
    end;
  end;
begin
{
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEFD_K220, TDBGrid(DBGK220),
    'efd_k220', ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'], istPergunta, '');
  //  Problema aqui! excluindo tudo sempre! mesmo que escolhe soh um.
  //  Resolver no futuro com procedure ExcluiSelecionados(); acima!
  ExcluiItensDeTabela('efd_k23subprd');
  ExcluiItensDeTabela('efd_k25subprd');
}
  ExcluiSelecionados();
end;

procedure TFmEFD_E001.ExcluiK220_280_1Click(Sender: TObject);
begin
  ExcluiRegsitrosImportados_Classe();
end;

procedure TFmEFD_E001.ExcluiK230_280_1Click(Sender: TObject);
begin
  ExcluiRegsitrosImportados_Producao();
end;

procedure TFmEFD_E001.ExcluiK280_1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEFD_K280, TDBGrid(DBGK280),
    'efd_k280', ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'], istPergunta, '');
end;

procedure TFmEFD_E001.Excluiobrigaoselecionada1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o da obriga�ao selecionada?',
  Dmod.QrUpd, 'efd_e116', ['ImporExpor', 'AnoMes', 'Empresa', 'E110', 'LinArq'
  ], ['=','=','=','=','='], [QrEFD_E116ImporExpor.Value, QrEFD_E116AnoMes.Value,
  QrEFD_E116Empresa.Value, QrEFD_E116E110.Value, QrEFD_E116LinArq.Value], '') then
    ReopenEFD_E116(QrEFD_E116LinArq.Value);
end;

procedure TFmEFD_E001.Excluiperiodoselecionado1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do per�odo selecionado?',
  'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_e001', [
    'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
    [QrEFD_E001ImporExpor.Value, QrEFD_E001AnoMes.Value,
    QrEFD_E001Empresa.Value], '') then
    ReopenEFD_E001(0);
  end;
end;

procedure TFmEFD_E001.Excluiregistrosd1Click(Sender: TObject);
begin
  ExcluiRegsitrosImportados_Estoque();
  ExcluiRegsitrosImportados_Classe();
  ExcluiRegsitrosImportados_Producao();
end;

procedure TFmEFD_E001.ExcluiRegsitrosImportados_Classe();
begin
  if Geral.MB_Pergunta('Confirma a exclus�o de todos itens de classe?') =
  ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_k220', [
    'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
    [QrEFD_K220ImporExpor.Value, QrEFD_K220AnoMes.Value,
    QrEFD_K220Empresa.Value], '') then
    begin
      ReopenEFD_K220(0); //, rkGrade);
      //
      ExcluiItensK27XParcialTipoEFD(TOrigemSPEDEFDKnd.osekClasse);
      ExcluiItensK280ParcialTipoEFD(TOrigemSPEDEFDKnd.osekClasse);
    end;
  end;
end;

procedure TFmEFD_E001.ExcluiRegsitrosImportados_Estoque();
begin
  if Geral.MB_Pergunta('Confirma a exclus�o de todos itens de estoque?') =
  ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_k200', [
    'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
    [QrEFD_K200ImporExpor.Value, QrEFD_K200AnoMes.Value,
    QrEFD_K200Empresa.Value], '') then
    begin
      ReopenEFD_K200(0, rkGrade);
      //
      ExcluiItensK27XParcialTipoEFD(TOrigemSPEDEFDKnd.osekEstoque);
      ExcluiItensK280ParcialTipoEFD(TOrigemSPEDEFDKnd.osekEstoque);
    end;
  end;
end;

procedure TFmEFD_E001.ExcluiRegsitrosImportados_Producao();
begin
  ExcluiItensNormaisDeProducao();
  ExcluiItensK27XParcialTipoEFD(osekProducao);
  ExcluiItensK280ParcialTipoEFD(osekProducao);
end;

procedure TFmEFD_E001.ExcluirTODASTABELASK1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  if Geral.MB_Pergunta('Deseja EXCLUIR TODAS TABELAS EFD_K... e SPEDEFDK...?')=
  ID_YES then
  begin
    if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DROP TABLE IF EXISTS SPEDEFDK001; ',
    'DROP TABLE IF EXISTS SPEDEFDK100; ',
    'DROP TABLE IF EXISTS SPEDEFDK200; ',
    'DROP TABLE IF EXISTS SPEDEFDK210; ',
    'DROP TABLE IF EXISTS SPEDEFDK215; ',
    'DROP TABLE IF EXISTS SPEDEFDK220; ',
    'DROP TABLE IF EXISTS SPEDEFDK230; ',
    'DROP TABLE IF EXISTS SPEDEFDK235; ',
    'DROP TABLE IF EXISTS SPEDEFDK250; ',
    'DROP TABLE IF EXISTS SPEDEFDK255; ',
    'DROP TABLE IF EXISTS SPEDEFDK260; ',
    'DROP TABLE IF EXISTS SPEDEFDK265; ',
    'DROP TABLE IF EXISTS SPEDEFDK270; ',
    'DROP TABLE IF EXISTS SPEDEFDK275; ',
    'DROP TABLE IF EXISTS SPEDEFDK280; ',
    'DROP TABLE IF EXISTS SPEDEFDK990; ',
    ' ',
    'DROP TABLE IF EXISTS EFD_K100; ',
    'DROP TABLE IF EXISTS EFD_K200; ',
    'DROP TABLE IF EXISTS EFD_K210; ',
    'DROP TABLE IF EXISTS EFD_K215; ',
    'DROP TABLE IF EXISTS EFD_K220; ',
    'DROP TABLE IF EXISTS EFD_K230; ',
    'DROP TABLE IF EXISTS EFD_K235; ',
    'DROP TABLE IF EXISTS EFD_K23SubPrd; ',
    'DROP TABLE IF EXISTS EFD_K250; ',
    'DROP TABLE IF EXISTS EFD_K255; ',
    'DROP TABLE IF EXISTS EFD_K260; ',
    'DROP TABLE IF EXISTS EFD_K265; ',
    'DROP TABLE IF EXISTS EFD_K270; ',
    'DROP TABLE IF EXISTS EFD_K275; ',
    'DROP TABLE IF EXISTS EFD_K280; ',
    'DROP TABLE IF EXISTS EFD_K25SubPrd; ',
    'DROP TABLE IF EXISTS EFD_K_ConfG; ',
    '']) then
      Geral.MB_Info('SQL de DROP TABLE executada sem erros!');
  end;
  ALL_Jan.MostraFormVerifiDB();
  //
  Close;
end;

procedure TFmEFD_E001.Excluisegmentointeiroimportado1Click(Sender: TObject);
  procedure ExcluiSegmento(Segmento: Integer);
  var
    AnoMes: Integer;
    //
    procedure ExcluiSegmentoDeTabela(Tabela: String);
    begin
      Dmod.MyDB.Execute('DELETE FROM ' + Tabela +
      ' WHERE AnoMes=' + Geral.FF0(AnoMes) +
      ' AND OriOpeProc=' + Geral.FF0(Segmento));
    end;
  begin
    AnoMes := QrEFD_E001AnoMes.Value;
    ExcluiSegmentoDeTabela('efd_k210');
    ExcluiSegmentoDeTabela('efd_k215');
    ExcluiSegmentoDeTabela('efd_k230');
    ExcluiSegmentoDeTabela('efd_k235');
    ExcluiSegmentoDeTabela('efd_k250');
    ExcluiSegmentoDeTabela('efd_k255');
  end;
var
  Segmento, Item: Integer;
begin
  Item := MyObjects.SelRadioGroup('Exclus�o', 'Segmento', sSPED_KPROD_SEGM, -1);
  if Item >= 0 then
  begin
    Screen.Cursor := crHourGlass;
    try
      Segmento := Item + 1;
      ExcluiSegmento(Segmento);
    finally
      Screen.Cursor := crDefault;
    end;
    Geral.MB_Info('Exclus�o executada!');
  end;
end;

procedure TFmEFD_E001.Excluivalordeclaratrioselecionado1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a declaral��o de valor selacionada?',
  Dmod.QrUpd, 'efd_e115', ['ImporExpor', 'AnoMes', 'Empresa', 'E110', 'LinArq'
  ], ['=','=','=','=','='], [QrEFD_E115ImporExpor.Value, QrEFD_E115AnoMes.Value,
  QrEFD_E115Empresa.Value, QrEFD_E115E110.Value, QrEFD_E115LinArq.Value], '') then
    ReopenEFD_E115(QrEFD_E115LinArq.Value);
end;

procedure TFmEFD_E001.Importadebalano1Click(Sender: TObject);
begin
  AppPF.MostraFormSPED_EFD_X999_Balancos(QrEFD_H005ImporExpor.Value,
    QrEFD_H005AnoMes.Value, QrEFD_H005Empresa.Value, QrEFD_H005LinArq.Value,
    'H010', QrEFD_H005DT_INV.Value);
  AtualizaValoresH005deH010();
end;

procedure TFmEFD_E001.ImportaK200_1Click(Sender: TObject);
begin
  AppPF.MostraFormSPED_EFD_X999_Balancos(QrEFD_K100ImporExpor.Value,
    QrEFD_K100AnoMes.Value, QrEFD_K100Empresa.Value, QrEFD_K100LinArq.Value,
    'K200', QrEFD_K100DT_FIN.Value);
  ReopenEFD_K200(0, rkGrade);
  ReopenEFD_K280(0, rkGrade);
end;

procedure TFmEFD_E001.ImportaK220_1Click(Sender: TObject);
begin
  AppPF.MostraFormSPED_EFD_K2XX(QrEFD_K100ImporExpor.Value,
    QrEFD_K100AnoMes.Value, QrEFD_K100Empresa.Value, QrEFD_K100LinArq.Value,
    'K220', QrEFD_K100DT_FIN.Value);
  ReopenEFD_K220(0);
  ReopenEFD_K270_220(0);
  ReopenEFD_K280(0, rkGrade);
end;

procedure TFmEFD_E001.ImportaK230_1Click(Sender: TObject);
begin
  AppPF.MostraFormSPED_EFD_K2XX(QrEFD_K100ImporExpor.Value,
    QrEFD_K100AnoMes.Value, QrEFD_K100Empresa.Value, QrEFD_K100LinArq.Value,
    'K230', QrEFD_K100DT_FIN.Value);
  ReopenEFD_K210(0);
  ReopenEFD_K230(0);
  ReopenEFD_K250(0);
  ReopenEFD_K260(0);
  ReopenEFD_K270_210(0);
  ReopenEFD_K270_230(0);
  ReopenEFD_K270_250(0);
  ReopenEFD_K280(0, rkGrade);
end;

procedure TFmEFD_E001.IncluiajustedaapuracaodoIPI1Click(Sender: TObject);
begin
  InsUpdE530(stIns);
end;

procedure TFmEFD_E001.Incluidatadeinventrio1Click(Sender: TObject);
begin
  InsUpdH005(stIns);
end;

procedure TFmEFD_E001.Incluiinformaocomplementar1Click(Sender: TObject);
begin
  InsUpdH020(stIns);
end;

procedure TFmEFD_E001.Incluiitem1Click(Sender: TObject);
begin
  InsUpdH010(stIns);
end;

procedure TFmEFD_E001.IncluiK200_1Click(Sender: TObject);
begin
  InsUpdK200(stIns);
end;

procedure TFmEFD_E001.IncluiK220_1Click(Sender: TObject);
begin
  InsUpdK220(stIns);
end;

procedure TFmEFD_E001.IncluiK230_1Click(Sender: TObject);
begin
  InsUpdK230(stIns);
end;

procedure TFmEFD_E001.Incluinovaidentificaodedocumentofiscal1Click(
  Sender: TObject);
begin
  InsUpdE113(stIns);
end;

procedure TFmEFD_E001.Incluinovainformaoadicional1Click(Sender: TObject);
begin
  InsUpdE112(stIns);
end;

procedure TFmEFD_E001.IncluinovaobrigaodoICMSarecolher1Click(Sender: TObject);
begin
  InsUpdE116(stIns);
end;

procedure TFmEFD_E001.Incluinovoajuste1Click(Sender: TObject);
begin
  InsUpdE111(stIns);
end;

procedure TFmEFD_E001.Incluinovointervalo1Click(Sender: TObject);
begin
  InsUpdE100(stIns);
end;

procedure TFmEFD_E001.IncluinovointervalodeProduoeEstoque1Click(
  Sender: TObject);
begin
  InsUpdK100(stIns);
end;

procedure TFmEFD_E001.IncluinovointervaloIPI1Click(Sender: TObject);
begin
  InsUpdE500(stIns);
end;

procedure TFmEFD_E001.Incluinovoperiodo1Click(Sender: TObject);
const
  LinArq = 0;
  REG = 'E001';
  IND_MOV = 1;
var
  Ano, Mes, Dia: Word;
  Cancelou, Continua: Boolean;
  AnoMes, MovimVS: Integer;
begin
  Continua := False;
  DecodeDate(Date, Ano, Mes, Dia);
  MLAGeral.EscolhePeriodo_MesEAno(TFmPeriodo, FmPeriodo, Mes, Ano, Cancelou, True, True);
  if not Cancelou then
  begin
    AnoMes := (Ano * 100) + Mes;
    //ShowMessage(Geral.FF0((Ano * 100) + Mes));
    if not PeriodoJaExiste(FImporExpor, AnoMes, FEmpresa, True) then
    begin
      if not SPED_EFD_PF.LiberaAcaoVS_SPED(AnoMes, FEmpresa,
      TEstagioVS_SPED.evsspedEncerraVS) then
        Exit;
(*
      MovimVS := VS_PF.SPEDEFDEnce_AnoMes('MovimVS');
      if MovimVS < AnoMes then
      begin
        if Geral.MB_Pergunta(
        'O movimento de couros ainda n�o foi encerrado para o per�odo selecionado.'
        + sLineBreak + 'Deseja encerrar o per�odo?') = ID_Yes then
        begin
          Continua := SPED_EFD_PF.EncerraMesSPED(AnoMes, FEmpresa, 'MovimVS');
          if Continua then
          begin
            // reconfirmar que criou!
            MovimVS := VS_PF.SPEDEFDEnce_AnoMes('MovimVS');
            Continua := MovimVS >= AnoMes;
          end;
        end;
      end else
        Continua := True;
      if Continua then
      begin
*)
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efd_e001', False, [
        'LinArq', 'REG', 'IND_MOV'], [
        'ImporExpor', 'AnoMes', 'Empresa'], [
        LinArq, REG, IND_MOV], [
        FImporExpor, AnoMes, FEmpresa], True) then
          ReopenEFD_E001(AnoMes);
      //end;
    end;
    ReopenEFD_E001(AnoMes);
  end;
end;

procedure TFmEFD_E001.Incluinovovalordeclaratrio1Click(Sender: TObject);
begin
  InsUpdE115(stIns);
end;

procedure TFmEFD_E001.InsUpdE100(SQLType: TSQLType);
const
  Registro = 'E100';
var
  Ini, Fim: TDateTime;
  Ano, Mes: Integer;
begin
  if UmyMod.FormInsUpd_Cria(TFmEFD_E100, FmEFD_E100, afmoNegarComAviso,
  QrEFD_E100, SQLType) then
  begin
    if SQLType = stIns then
    begin
      if not SPED_EFD_PF.DefineDatasPeriodoSPED(FEmpresa,
      QrEFD_E001AnoMes.Value, 'E100',
      QrEFD_E100DT_INI.Value, QrEFD_E100DT_FIN.Value, Ini, Fim) then ;
    end else
    begin
      Ini := QrEFD_E100DT_INI.Value;
      Fim := QrEFD_E100DT_FIN.Value;
    end;
    FmEFD_E100.ImgTipo.SQLType := SQLType;
    FmEFD_E100.EdImporExpor.ValueVariant := QrEFD_E001ImporExpor.Value;
    FmEFD_E100.EdAnoMes.ValueVariant := QrEFD_E001AnoMes.Value;
    FmEFD_E100.EdEmpresa.ValueVariant := QrEFD_E001Empresa.Value;
    //
    FmEFD_E100.TPDT_INI.Date := Ini;
    FmEFD_E100.TPDT_FIN.Date := Fim;
    //
    FmEFD_E100.ShowModal;
    FmEFD_E100.Destroy;
  end;
end;

procedure TFmEFD_E001.InsUpdE111(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEFD_E111, FmEFD_E111, afmoNegarComAviso) then
  begin
    FmEFD_E111.ImgTipo.SQLType := SQLType;
    //
    FmEFD_E111.TPDT_INI.Date := QrEFD_E100DT_INI.Value;
    FmEFD_E111.TPDT_FIN.Date := QrEFD_E100DT_FIN.Value;
    //
    FmEFD_E111.EdImporExpor.ValueVariant := QrEFD_E110ImporExpor.Value;
    FmEFD_E111.EdAnoMes.ValueVariant := QrEFD_E110AnoMes.Value;
    FmEFD_E111.EdEmpresa.ValueVariant := QrEFD_E110Empresa.Value;
    FmEFD_E111.EdE110.ValueVariant := QrEFD_E110LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEFD_E111.EdLinArq.ValueVariant := QrEFD_E111LinArq.Value;
      //
      FmEFD_E111.EdCOD_AJ_APUR.ValueVariant := QrEFD_E111COD_AJ_APUR.Value;
      FmEFD_E111.EdDESCR_COMPL_AJ.ValueVariant := QrEFD_E111DESCR_COMPL_AJ.Value;
      FmEFD_E111.EdVL_AJ_APUR.ValueVariant := QrEFD_E111VL_AJ_APUR.Value;
    end;
    //
    FmEFD_E111.ShowModal;
    FmEFD_E111.Destroy;
  end;
end;

procedure TFmEFD_E001.InsUpdE112(SQLType: TSQLType);
  function DefineItem_IND_PROC(x: String): Integer;
  begin
    if x = '0' then Result := 1 else
    if x = '1' then Result := 2 else
    if x = '2' then Result := 3 else
    if x = '9' then Result := 4 else
                    Result := 0;
  end;
begin
  if DBCheck.CriaFm(TFmEFD_E112, FmEFD_E112, afmoNegarComAviso) then
  begin
    FmEFD_E112.ImgTipo.SQLType := SQLType;
    //
    FmEFD_E112.TPDT_INI.Date := QrEFD_E100DT_INI.Value;
    FmEFD_E112.TPDT_FIN.Date := QrEFD_E100DT_FIN.Value;
    //
    FmEFD_E112.EdImporExpor.ValueVariant := QrEFD_E111ImporExpor.Value;
    FmEFD_E112.EdAnoMes.ValueVariant := QrEFD_E111AnoMes.Value;
    FmEFD_E112.EdEmpresa.ValueVariant := QrEFD_E111Empresa.Value;
    FmEFD_E112.EdE111.ValueVariant := QrEFD_E111LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEFD_E112.EdLinArq.ValueVariant := QrEFD_E112LinArq.Value;
      //
      FmEFD_E112.EdNUM_DA.Text := QrEFD_E112NUM_DA.Value;
      FmEFD_E112.EdNUM_PROC.Text := QrEFD_E112NUM_PROC.Value;
      FmEFD_E112.RGIND_PROC.ItemIndex := DefineItem_IND_PROC(QrEFD_E112IND_PROC.Value);
      FmEFD_E112.EdPROC.Text := QrEFD_E112PROC.Value;
      FmEFD_E112.EdTXT_COMPL.Text := QrEFD_E112TXT_COMPL.Value;
    end;
    //
    FmEFD_E112.ShowModal;
    FmEFD_E112.Destroy;
  end;
end;

procedure TFmEFD_E001.InsUpdE113(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEFD_E113, FmEFD_E113, afmoNegarComAviso) then
  begin
    FmEFD_E113.ImgTipo.SQLType := SQLType;
    //
    FmEFD_E113.TPDT_INI.Date := QrEFD_E100DT_INI.Value;
    FmEFD_E113.TPDT_FIN.Date := QrEFD_E100DT_FIN.Value;
    //
    FmEFD_E113.EdImporExpor.ValueVariant := QrEFD_E111ImporExpor.Value;
    FmEFD_E113.EdAnoMes.ValueVariant := QrEFD_E111AnoMes.Value;
    FmEFD_E113.EdEmpresa.ValueVariant := QrEFD_E111Empresa.Value;
    FmEFD_E113.EdE111.ValueVariant := QrEFD_E111LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEFD_E113.EdLinArq.ValueVariant := QrEFD_E113LinArq.Value;
      //
      FmEFD_E113.EdCOD_PART.Text := QrEFD_E113COD_PART.Value;
      FmEFD_E113.EdCOD_MOD.Text := QrEFD_E113COD_MOD.Value;
      FmEFD_E113.EdSER.Text := QrEFD_E113SER.Value;
      FmEFD_E113.EdSUB.Text := QrEFD_E113SUB.Value;
      FmEFD_E113.EdNUM_DOC.ValueVariant := QrEFD_E113NUM_DOC.Value;
      FmEFD_E113.TPDT_DOC.Date := QrEFD_E113DT_DOC.Value;
      FmEFD_E113.EdCOD_ITEM.Text := QrEFD_E113COD_ITEM.Value;
      FmEFD_E113.EdVL_AJ_ITEM.ValueVariant := QrEFD_E113VL_AJ_ITEM.Value;
    end;
    //
    FmEFD_E113.ShowModal;
    FmEFD_E113.Destroy;
  end;
end;

procedure TFmEFD_E001.InsUpdE115(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEFD_E115, FmEFD_E115, afmoNegarComAviso) then
  begin
    FmEFD_E115.ImgTipo.SQLType := SQLType;
    //
    FmEFD_E115.TPDT_INI.Date := QrEFD_E100DT_INI.Value;
    FmEFD_E115.TPDT_FIN.Date := QrEFD_E100DT_FIN.Value;
    //
    FmEFD_E115.EdImporExpor.ValueVariant := QrEFD_E110ImporExpor.Value;
    FmEFD_E115.EdAnoMes.ValueVariant := QrEFD_E110AnoMes.Value;
    FmEFD_E115.EdEmpresa.ValueVariant := QrEFD_E110Empresa.Value;
    FmEFD_E115.EdE110.ValueVariant := QrEFD_E110LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEFD_E115.EdLinArq.ValueVariant := QrEFD_E115LinArq.Value;
      //
      FmEFD_E115.EdCOD_INF_ADIC.Text := QrEFD_E115COD_INF_ADIC.Value;
      FmEFD_E115.EdVL_INF_ADIC.ValueVariant := QrEFD_E115VL_INF_ADIC.Value;
      FmEFD_E115.EdDESCR_COMPL_AJ.Text := QrEFD_E115DESCR_COMPL_AJ.Value;
    end;
    //
    FmEFD_E115.ShowModal;
    FmEFD_E115.Destroy;
  end;
end;

procedure TFmEFD_E001.InsUpdE116(SQLType: TSQLType);
  function DefineItem_IND_PROC(x: String): Integer;
  begin
    if x = '0' then Result := 1 else
    if x = '1' then Result := 2 else
    if x = '2' then Result := 3 else
    if x = '9' then Result := 4 else
                    Result := 0;
  end;
const
  TabName = 'TbSpedEfd005';
  FldOrde = 'Nome';
var
  Ano, Mes, Dia: Word;
begin
  if DBCheck.CriaFm(TFmEFD_E116, FmEFD_E116, afmoNegarComAviso) then
  begin
    FmEFD_E116.ImgTipo.SQLType := SQLType;
    //
    FmEFD_E116.TPDT_INI.Date := QrEFD_E100DT_INI.Value;
    FmEFD_E116.TPDT_FIN.Date := QrEFD_E100DT_FIN.Value;
    //
    SPED_EFD_PF.ReopenTbSpedEfdXXX(FmEFD_E116.QrTbSpedEfd005,
    QrEFD_E100DT_INI.Value, QrEFD_E100DT_FIN.Value, TabName, FldOrde);
    //
    FmEFD_E116.EdImporExpor.ValueVariant := QrEFD_E110ImporExpor.Value;
    FmEFD_E116.EdAnoMes.ValueVariant := QrEFD_E110AnoMes.Value;
    FmEFD_E116.EdEmpresa.ValueVariant := QrEFD_E110Empresa.Value;
    FmEFD_E116.EdE100.ValueVariant := QrEFD_E110E100.Value;
    FmEFD_E116.EdE110.ValueVariant := QrEFD_E110LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEFD_E116.EdLinArq.ValueVariant := QrEFD_E116LinArq.Value;
      //
      FmEFD_E116.EdCOD_OR.Text := QrEFD_E116COD_OR.Value;
      FmEFD_E116.EdVL_OR.ValueVariant := QrEFD_E116VL_OR.Value;
      FmEFD_E116.TPDT_VCTO.Date := QrEFD_E116DT_VCTO.Value;
      FmEFD_E116.EdCOD_REC.Text := QrEFD_E116COD_REC.Value;
      FmEFD_E116.EdNUM_PROC.Text := QrEFD_E116NUM_PROC.Value;
      FmEFD_E116.RGIND_PROC.ItemIndex := DefineItem_IND_PROC(QrEFD_E116IND_PROC.Value);
      FmEFD_E116.EdPROC.Text := QrEFD_E116PROC.Value;
      FmEFD_E116.EdTXT_COMPL.Text := QrEFD_E116TXT_COMPL.Value;
      FmEFD_E116.TPMES_REF.Date := QrEFD_E116MES_REF.Value;
    end else
    begin
      FmEFD_E116.TPDT_VCTO.Date := QrEFD_E100DT_FIN.Value + 1;
      FmEFD_E116.TPMES_REF.Date := QrEFD_E100DT_INI.Value;
    end;
    //
    FmEFD_E116.ShowModal;
    FmEFD_E116.Destroy;
  end;
end;

procedure TFmEFD_E001.InsUpdE500(SQLType: TSQLType);
var
  Ini, Fim: TDateTime;
  Ano, Mes, IND_APUR, Dias: Integer;
begin
  if UmyMod.FormInsUpd_Cria(TFmEFD_E500, FmEFD_E500, afmoNegarComAviso,
  QrEFD_E500, SQLType) then
  begin
    if SQLType = stIns then
    begin
      if not SPED_EFD_PF.DefineDatasPeriodoSPED(FEmpresa,
      QrEFD_E001AnoMes.Value, 'E500',
      QrEFD_E500DT_INI.Value, QrEFD_E500DT_FIN.Value, Ini, Fim) then ;
(*
      Ano := QrEFD_E001AnoMes.Value div 100;
      Mes := QrEFD_E001AnoMes.Value mod 100;
      Ini := EncodeDate(Ano, Mes, 1);
      Fim := IncMonth(Ini) - 1;
*)
      Dias := Trunc(Fim) - Trunc(Ini);
      case dias of
        10:      IND_APUR := 0;
        28..31 : IND_APUR := 1;
        else     IND_APUR := -1;
      end;
    end else
    begin
      Ini := QrEFD_E500DT_INI.Value;
      Fim := QrEFD_E500DT_FIN.Value;
      IND_APUR := Geral.IMV(QrEFD_E500IND_APUR.Value);
    end;
    FmEFD_E500.ImgTipo.SQLType := SQLType;
    FmEFD_E500.EdImporExpor.ValueVariant := QrEFD_E001ImporExpor.Value;
    FmEFD_E500.EdAnoMes.ValueVariant := QrEFD_E001AnoMes.Value;
    FmEFD_E500.EdEmpresa.ValueVariant := QrEFD_E001Empresa.Value;
    //
    FmEFD_E500.TPDT_INI.Date := Ini;
    FmEFD_E500.TPDT_FIN.Date := Fim;
    FmEFD_E500.RGIND_APUR.ItemIndex := IND_APUR;
    //
    FmEFD_E500.ShowModal;
    FmEFD_E500.Destroy;
  end;
end;

procedure TFmEFD_E001.InsUpdE530(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEFD_E530, FmEFD_E530, afmoNegarComAviso) then
  begin
    FmEFD_E530.ImgTipo.SQLType := SQLType;
    //
    FmEFD_E530.TPDT_INI.Date := QrEFD_E100DT_INI.Value;
    FmEFD_E530.TPDT_FIN.Date := QrEFD_E100DT_FIN.Value;
    //
    FmEFD_E530.FAnoMes       := QrEFD_E100AnoMes.Value;
    FmEFD_E530.ReopenTabelas();
    //
    FmEFD_E530.EdImporExpor.ValueVariant := QrEFD_E520ImporExpor.Value;
    FmEFD_E530.EdAnoMes.ValueVariant     := QrEFD_E520AnoMes.Value;
    FmEFD_E530.EdEmpresa.ValueVariant    := QrEFD_E520Empresa.Value;
    FmEFD_E530.EdE520.ValueVariant       := QrEFD_E520LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEFD_E530.EdLinArq.ValueVariant := QrEFD_E530LinArq.Value;
      //
      FmEFD_E530.RGIND_AJ.ItemIndex      := Geral.IMV(QrEFD_E530IND_AJ.Value);
      FmEFD_E530.EdVL_AJ.ValueVariant    := QrEFD_E530VL_AJ.Value;
      FmEFD_E530.EdCOD_AJ.ValueVariant   := QrEFD_E530COD_AJ.Value;
      case Geral.IMV(QrEFD_E530IND_DOC.Value) of
        0: FmEFD_E530.RGIND_DOC.ItemIndex := 0;
        1: FmEFD_E530.RGIND_DOC.ItemIndex := 1;
        2: FmEFD_E530.RGIND_DOC.ItemIndex := 2;
        9: FmEFD_E530.RGIND_DOC.ItemIndex := 3;
        else Geral.MB_Aviso('Valor de "IND_DOC" n�o implementado!');
      end;
      FmEFD_E530.EdNUM_DOC.Text          := QrEFD_E530NUM_DOC.Value;
      FmEFD_E530.MeDESCR_AJ.Text         := QrEFD_E530DESCR_AJ.Value;
    end;
    //
    FmEFD_E530.ShowModal;
    FmEFD_E530.Destroy;
  end;
end;

procedure TFmEFD_E001.InsUpdH005(SQLType: TSQLType);
var
  DT_INV: TDateTime;
  Ano, Mes: Word;
  MOT_INV: String;
begin
  if UmyMod.FormInsUpd_Cria(TFmEFD_H005, FmEFD_H005, afmoNegarComAviso,
  QrEFD_H005, SQLType) then
  begin
    if SQLType = stIns then
    begin
      Ano     := QrEFD_E001AnoMes.Value div 100;
      Mes     := QrEFD_E001AnoMes.Value mod 100;
      DT_INV  := IncMonth(EncodeDate(Ano, Mes, 1), 1) - 1;
      MOT_INV := '01';
    end else
    begin
      DT_INV  := QrEFD_H005DT_INV.Value;
      MOT_INV := QrEFD_H005MOT_INV.Value;
    end;
    FmEFD_H005.ImgTipo.SQLType := SQLType;
    FmEFD_H005.EdImporExpor.ValueVariant := QrEFD_E001ImporExpor.Value;
    FmEFD_H005.EdAnoMes.ValueVariant := QrEFD_E001AnoMes.Value;
    FmEFD_H005.EdEmpresa.ValueVariant := QrEFD_E001Empresa.Value;
    //
    FmEFD_H005.TPDT_INV.Date := DT_INV;
    FmEFD_H005.EdMOT_INV.Text := MOT_INV;
    //
    FmEFD_H005.ShowModal;
    FmEFD_H005.Destroy;
  end;
end;

procedure TFmEFD_E001.InsUpdH010(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEFD_H010, FmEFD_H010, afmoNegarComAviso) then
  begin
    FmEFD_H010.ImgTipo.SQLType := SQLType;
    //
    FmEFD_H010.TPDT_INV.Date := QrEFD_H005DT_INV.Value;
    //
    FmEFD_H010.EdImporExpor.ValueVariant   := QrEFD_H005ImporExpor.Value;
    FmEFD_H010.EdAnoMes.ValueVariant       := QrEFD_H005AnoMes.Value;
    FmEFD_H010.EdEmpresa.ValueVariant      := QrEFD_H005Empresa.Value;
    FmEFD_H010.EdH005.ValueVariant         := QrEFD_H005LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEFD_H010.EdLinArq.ValueVariant     := QrEFD_H010LinArq.Value;
      FmEFD_H010.FBalID                    := QrEFD_H010BalID.Value;
      FmEFD_H010.FBalNum                   := QrEFD_H010BalNum.Value;
      FmEFD_H010.FBalItm                   := QrEFD_H010BalItm.Value;
      FmEFD_H010.FBalEnt                   := QrEFD_H010BalEnt.Value;
      //
      FmEFD_H010.EdGraGruX.Text            := QrEFD_H010COD_ITEM.Value;
      FmEFD_H010.CBGraGruX.KeyValue        := QrEFD_H010COD_ITEM.Value;
      FmEFD_H010.EdUNID.Text               := QrEFD_H010UNID.Value;
      FmEFD_H010.EdQTD.ValueVariant        := QrEFD_H010QTD.Value;
      FmEFD_H010.EdVL_UNIT.ValueVariant    := QrEFD_H010VL_UNIT.Value;
      FmEFD_H010.EdVL_ITEM.ValueVariant    := QrEFD_H010VL_ITEM.Value;
      FmEFD_H010.RGIND_PROP.ItemIndex      := Geral.IMV(QrEFD_H010IND_PROP.Value);
      FmEFD_H010.EdCOD_PART.ValueVariant   := Geral.IMV(QrEFD_H010COD_PART.Value);
      FmEFD_H010.CBCOD_PART.KeyValue       := Geral.IMV(QrEFD_H010COD_PART.Value);
      FmEFD_H010.EdTXT_COMPL.ValueVariant  := QrEFD_H010TXT_COMPL.Value;
      FmEFD_H010.EdCOD_CTA.Text            := QrEFD_H010COD_CTA.Value;
      FmEFD_H010.EdVL_ITEM_IR.ValueVariant := QrEFD_H010VL_ITEM_IR.Value;
    end;
    //
    FmEFD_H010.ShowModal;
    FmEFD_H010.Destroy;
  end;
end;

procedure TFmEFD_E001.InsUpdH020(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEFD_H020, FmEFD_H020, afmoNegarComAviso) then
  begin
    FmEFD_H020.ImgTipo.SQLType := SQLType;
    //
    FmEFD_H020.FAnoMes                     := QrEFD_H010AnoMes.Value;
    FmEFD_H020.TPDT_INV.Date               := QrEFD_H005DT_INV.Value;
    //
    FmEFD_H020.EdImporExpor.ValueVariant   := QrEFD_H010ImporExpor.Value;
    FmEFD_H020.EdAnoMes.ValueVariant       := QrEFD_H010AnoMes.Value;
    FmEFD_H020.EdEmpresa.ValueVariant      := QrEFD_H010Empresa.Value;
    FmEFD_H020.EdH010.ValueVariant         := QrEFD_H010LinArq.Value;
    //
    FmEFD_H020.ReopenTabelas();
    //
    if SQLType = stUpd then
    begin
      FmEFD_H020.EdLinArq.ValueVariant     := QrEFD_H020LinArq.Value;
      //
      FmEFD_H020.EdCST_ICMS.ValueVariant   := QrEFD_H020CST_ICMS.Value;
      FmEFD_H020.CBCST_ICMS.KeyValue       := QrEFD_H020CST_ICMS.Value;
      FmEFD_H020.EdBC_ICMS.ValueVariant    := QrEFD_H020BC_ICMS.Value;
      FmEFD_H020.EdVL_ICMS.ValueVariant    := QrEFD_H020VL_ICMS.Value;
    end;
    //
    FmEFD_H020.ShowModal;
    FmEFD_H020.Destroy;
  end;
end;

procedure TFmEFD_E001.InsUpdK100(SQLType: TSQLType);
var
  Ini, Fim: TDateTime;
  Ano, Mes: Integer;
begin
  if UmyMod.FormInsUpd_Cria(TFmEFD_K100, FmEFD_K100, afmoNegarComAviso,
  QrEFD_K100, SQLType) then
  begin
    if SQLType = stIns then
    begin
      if not SPED_EFD_PF.DefineDatasPeriodoSPED(FEmpresa,
      QrEFD_E001AnoMes.Value, 'K100',
      QrEFD_K100DT_INI.Value, QrEFD_K100DT_FIN.Value, Ini, Fim) then ;
(*
      Ano := QrEFD_E001AnoMes.Value div 100;
      Mes := QrEFD_E001AnoMes.Value mod 100;
      Ini := EncodeDate(Ano, Mes, 1);
      Fim := IncMonth(Ini) - 1;
*)
    end else
    begin
      Ini := QrEFD_K100DT_INI.Value;
      Fim := QrEFD_K100DT_FIN.Value;
    end;
      FmEFD_K100.ImgTipo.SQLType := SQLType;
    FmEFD_K100.EdImporExpor.ValueVariant := QrEFD_E001ImporExpor.Value;
    FmEFD_K100.EdAnoMes.ValueVariant     := QrEFD_E001AnoMes.Value;
    FmEFD_K100.EdEmpresa.ValueVariant    := QrEFD_E001Empresa.Value;
    //
    FmEFD_K100.TPDT_INI.Date := Ini;
    FmEFD_K100.TPDT_FIN.Date := Fim;
    //
    FmEFD_K100.ShowModal;
    FmEFD_K100.Destroy;
  end;
end;

procedure TFmEFD_E001.InsUpdK200(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEFD_K200, FmEFD_K200, afmoNegarComAviso) then
  begin
    FmEFD_K200.ImgTipo.SQLType := SQLType;
    //
    FmEFD_K200.TPDT_EST.Date := QrEFD_K100DT_FIN.Value;
    //
    FmEFD_K200.EdImporExpor.ValueVariant   := QrEFD_K100ImporExpor.Value;
    FmEFD_K200.EdAnoMes.ValueVariant       := QrEFD_K100AnoMes.Value;
    FmEFD_K200.EdEmpresa.ValueVariant      := QrEFD_K100Empresa.Value;
    FmEFD_K200.EdK100.ValueVariant         := QrEFD_K100LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEFD_K200.EdLinArq.ValueVariant     := QrEFD_K200LinArq.Value;
      FmEFD_K200.FBalID                    := QrEFD_K200BalID.Value;
      FmEFD_K200.FBalNum                   := QrEFD_K200BalNum.Value;
      FmEFD_K200.FBalItm                   := QrEFD_K200BalItm.Value;
      FmEFD_K200.FBalEnt                   := QrEFD_K200BalEnt.Value;
      //
      FmEFD_K200.EdGraGruX.Text            := QrEFD_K200COD_ITEM.Value;
      FmEFD_K200.CBGraGruX.KeyValue        := QrEFD_K200COD_ITEM.Value;
      FmEFD_K200.EdQTD.ValueVariant        := QrEFD_K200QTD.Value;
      FmEFD_K200.RGIND_EST.ItemIndex       := Geral.IMV(QrEFD_K200IND_EST.Value);
      FmEFD_K200.EdCOD_PART.ValueVariant   := Geral.IMV(QrEFD_K200COD_PART.Value);
      FmEFD_K200.CBCOD_PART.KeyValue       := Geral.IMV(QrEFD_K200COD_PART.Value);
    end;
    //
    FmEFD_K200.ShowModal;
    FmEFD_K200.Destroy;
  end;
end;

procedure TFmEFD_E001.InsUpdK220(SQLType: TSQLType);
begin
   // Parei aqui!

{
  if DBCheck.CriaFm(TFmEFD_K220, FmEFD_K220, afmoNegarComAviso) then
  begin
    FmEFD_K220.ImgTipo.SQLType := SQLType;
    //
    FmEFD_K220.EdImporExpor.ValueVariant   := QrEFD_K100ImporExpor.Value;
    FmEFD_K220.EdAnoMes.ValueVariant       := QrEFD_K100AnoMes.Value;
    FmEFD_K220.EdEmpresa.ValueVariant      := QrEFD_K100Empresa.Value;
    FmEFD_K220.EdK100.ValueVariant         := QrEFD_K100LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEFD_K220.EdLinArq.ValueVariant     := QrEFD_K220LinArq.Value;
      FmEFD_K220.FID_SEK                   := QrEFD_K220ID_SEK.Value;
      FmEFD_K220.FBalID                    := QrEFD_K220BalID.Value;
      FmEFD_K220.FBalNum                   := QrEFD_K220BalNum.Value;
      FmEFD_K220.FBalItm                   := QrEFD_K220BalItm.Value;
      FmEFD_K220.FBalEnt                   := QrEFD_K220BalEnt.Value;
      //
      FmEFD_K220.TPDT_MOV.Date := QrEFD_K220DT_MOV.Value;
      FmEFD_K220.EdGraGruX.Text            := QrEFD_K220COD_ITEM.Value;
      FmEFD_K220.CBGraGruX.KeyValue        := QrEFD_K220COD_ITEM.Value;
      FmEFD_K220.EdQTD.ValueVariant        := QrEFD_K220QTD.Value;
      FmEFD_K220.RGIND_EST.ItemIndex       := Geral.IMV(QrEFD_K220IND_EST.Value);
      FmEFD_K220.EdCOD_PART.ValueVariant   := Geral.IMV(QrEFD_K220COD_PART.Value);
      FmEFD_K220.CBCOD_PART.KeyValue       := Geral.IMV(QrEFD_K220COD_PART.Value);
    end;
    //
    FmEFD_K220.ShowModal;
    FmEFD_K220.Destroy;
  end;
}
end;

procedure TFmEFD_E001.InsUpdK230(SQLType: TSQLType);
begin
   // Parei aqui!
end;

procedure TFmEFD_E001.JanelaDoMovimento();
begin
  case PCK235.ActivePageIndex of
    0: AppPF.MostraFormOP(QrEFD_K230COD_DOC_OP.Value, Geral.FF0(QrEFD_K235ID_Item.Value));
    1: AppPF.MostraFormOP(QrEFD_K250COD_DOC_OP.Value, Geral.FF0(QrEFD_K255ID_Item.Value));
    else Geral.MB_ERRO('"PCK235.ActivePageIndex" n�o implementado em "Vaiparajanelademovimento"');
  end;
end;

procedure TFmEFD_E001.K2301Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEFD_K230, TDBGrid(DBGK230),
    'efd_k230', ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'], istPergunta, '');
end;

procedure TFmEFD_E001.K2351Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEFD_K235, TDBGrid(DBGK235),
    'efd_k235', ['ImporExpor', 'AnoMes', 'Empresa', 'K230', 'LinArq'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'K230', 'LinArq'], istPergunta, '');
end;

procedure TFmEFD_E001.K2501Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEFD_K250, TDBGrid(DBGK250),
    'efd_k250', ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'], istPergunta, '');
end;

procedure TFmEFD_E001.K2551Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEFD_K255, TDBGrid(DBGK255),
    'efd_k255', ['ImporExpor', 'AnoMes', 'Empresa', 'K250', 'LinArq'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'K250', 'LinArq'], istPergunta, '');
end;

procedure TFmEFD_E001.K2601Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEFD_K260, TDBGrid(DBGK260),
    'efd_k260', ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'K100', 'LinArq'], istPergunta, '');
end;

procedure TFmEFD_E001.K2651Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEFD_K265, TDBGrid(DBGK265),
    'efd_k265', ['ImporExpor', 'AnoMes', 'Empresa', 'K260', 'LinArq'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'K260', 'LinArq'], istPergunta, '');
end;

procedure TFmEFD_E001.MostraFormEFD_1010();
const
  SIM = 'S';
var
  SQLType: TSQLType;
begin
  if QrEFD_1010.RecordCount > 0 then
    SQLType := stUpd
  else
    SQLType := stIns;
  if UmyMod.FormInsUpd_Cria(TFmEFD_1010, FmEFD_1010, afmoNegarComAviso,
  QrEFD_1010, stUpd) then
  begin
    FmEFD_1010.ImgTipo.SQLType := SQLType;
    FmEFD_1010.EdImporExpor.ValueVariant := QrEFD_E001ImporExpor.Value;
    FmEFD_1010.EdAnoMes.ValueVariant := QrEFD_E001AnoMes.Value;
    FmEFD_1010.EdEmpresa.ValueVariant := QrEFD_E001Empresa.Value;
    //
    FmEFD_1010.EdLinArq.ValueVariant := QrEFD_E500LinArq.Value;
    if SQLType = stUpd then
    begin
      FmEFD_1010.EdLinArq.ValueVariant        := QrEFD_1010LinArq.Value;
      //
      FmEFD_1010.CkIND_EXP.Checked    := QrEFD_1010IND_EXP.Value   = SIM;
      FmEFD_1010.CkIND_CCRF.Checked   := QrEFD_1010IND_CCRF.Value  = SIM;
      FmEFD_1010.CkIND_COMB.Checked   := QrEFD_1010IND_COMB.Value  = SIM;
      FmEFD_1010.CkIND_USINA.Checked  := QrEFD_1010IND_USINA.Value = SIM;
      FmEFD_1010.CkIND_VA.Checked     := QrEFD_1010IND_VA.Value    = SIM;
      FmEFD_1010.CkIND_EE.Checked     := QrEFD_1010IND_EE.Value    = SIM;
      FmEFD_1010.CkIND_CART.Checked   := QrEFD_1010IND_CART.Value  = SIM;
      FmEFD_1010.CkIND_FORM.Checked   := QrEFD_1010IND_FORM.Value  = SIM;
      FmEFD_1010.CkIND_AER.Checked    := QrEFD_1010IND_AER.Value   = SIM;
    end;
    //
    FmEFD_1010.ShowModal;
    FmEFD_1010.Destroy;
    //
    ReopenEFD_1010();
  end;
end;

procedure TFmEFD_E001.MostraFormEFD_E110();
var
  SQLType: TSQLType;
begin
//  MyObjects.MostraPopUpDeBotao(PMApuracao, BtApuracao);
  PCE100.ActivePageIndex := 0;
  if (QrEFD_E100.State = dsInactive) or (QrEFD_E100.RecordCount = 0) then
  begin
    Geral.MensagemBox('Informe primeiro um intervalo de apura��o!', 'Aviso',
    MB_OK+MB_ICONWARNING);
  end else begin
    if QrEFD_E110.RecordCount > 0 then
      SQLType := stUpd
    else
      SQLType := stIns;
    if UmyMod.FormInsUpd_Cria(TFmEFD_E110, FmEFD_E110, afmoNegarComAviso,
    QrEFD_E110, stUpd) then
    begin
      FmEFD_E110.ImgTipo.SQLType := SQLType;
      FmEFD_E110.EdImporExpor.ValueVariant := QrEFD_E001ImporExpor.Value;
      FmEFD_E110.EdAnoMes.ValueVariant := QrEFD_E001AnoMes.Value;
      FmEFD_E110.EdEmpresa.ValueVariant := QrEFD_E001Empresa.Value;
      FmEFD_E110.EdE100.ValueVariant := QrEFD_E100LinArq.Value;
      //
      FmEFD_E110.TPDT_INI.Date := QrEFD_E100DT_INI.Value;
      FmEFD_E110.TPDT_FIN.Date := QrEFD_E100DT_FIN.Value;
      //
      FmEFD_E110.EdLinArq.ValueVariant := QrEFD_E100LinArq.Value;
      if SQLType = stUpd then
      begin
        FmEFD_E110.EdVL_TOT_DEBITOS.ValueVariant :=      QrEFD_E110VL_TOT_DEBITOS.Value;
        FmEFD_E110.EdVL_AJ_DEBITOS.ValueVariant :=       QrEFD_E110VL_AJ_DEBITOS.Value;
        FmEFD_E110.EdVL_TOT_AJ_DEBITOS.ValueVariant :=   QrEFD_E110VL_TOT_AJ_DEBITOS.Value;
        FmEFD_E110.EdVL_ESTORNOS_CRED.ValueVariant :=    QrEFD_E110VL_ESTORNOS_CRED.Value;
        FmEFD_E110.EdVL_TOT_CREDITOS.ValueVariant :=     QrEFD_E110VL_TOT_CREDITOS.Value;
        FmEFD_E110.EdVL_AJ_CREDITOS.ValueVariant :=      QrEFD_E110VL_AJ_CREDITOS.Value;
        FmEFD_E110.EdVL_TOT_AJ_CREDITOS.ValueVariant :=  QrEFD_E110VL_TOT_AJ_CREDITOS.Value;
        FmEFD_E110.EdVL_ESTORNOS_DEB.ValueVariant :=     QrEFD_E110VL_ESTORNOS_DEB.Value;
        FmEFD_E110.EdVL_SLD_CREDOR_ANT.ValueVariant :=   QrEFD_E110VL_SLD_CREDOR_ANT.Value;
        FmEFD_E110.EdVL_SLD_APURADO.ValueVariant :=      QrEFD_E110VL_SLD_APURADO.Value;
        FmEFD_E110.EdVL_TOT_DED.ValueVariant :=          QrEFD_E110VL_TOT_DED.Value;
        FmEFD_E110.EdVL_ICMS_RECOLHER.ValueVariant :=    QrEFD_E110VL_ICMS_RECOLHER.Value;
        FmEFD_E110.EdVL_SLD_CREDOR_TRANSPORTAR.ValueVariant :=  QrEFD_E110VL_SLD_CREDOR_TRANSPORTAR.Value;
        FmEFD_E110.EdDEB_ESP.ValueVariant :=             QrEFD_E110DEB_ESP.Value;
      end;
      //
      FmEFD_E110.ShowModal;
      FmEFD_E110.Destroy;
    end;
  end;
end;

procedure TFmEFD_E001.MostraFormEFD_E520();
var
  SQLType: TSQLType;
begin
  PCE100.ActivePageIndex := 0;
  if (QrEFD_E500.State = dsInactive) or (QrEFD_E500.RecordCount = 0) then
  begin
    Geral.MensagemBox('Informe primeiro um intervalo de apura��o!', 'Aviso',
    MB_OK+MB_ICONWARNING);
  end else begin
    if QrEFD_E520.RecordCount > 0 then
      SQLType := stUpd
    else
      SQLType := stIns;
    if UmyMod.FormInsUpd_Cria(TFmEFD_E520, FmEFD_E520, afmoNegarComAviso,
    QrEFD_E520, stUpd) then
    begin
      FmEFD_E520.ImgTipo.SQLType := SQLType;
      FmEFD_E520.EdImporExpor.ValueVariant := QrEFD_E001ImporExpor.Value;
      FmEFD_E520.EdAnoMes.ValueVariant := QrEFD_E001AnoMes.Value;
      FmEFD_E520.EdEmpresa.ValueVariant := QrEFD_E001Empresa.Value;
      FmEFD_E520.EdE500.ValueVariant := QrEFD_E500LinArq.Value;
      //
      FmEFD_E520.TPDT_INI.Date := QrEFD_E500DT_INI.Value;
      FmEFD_E520.TPDT_FIN.Date := QrEFD_E500DT_FIN.Value;
      //
      FmEFD_E520.EdLinArq.ValueVariant := QrEFD_E500LinArq.Value;
      if SQLType = stUpd then
      begin
        FmEFD_E520.EdVL_SD_ANT_IPI.ValueVariant := QrEFD_E520VL_SD_ANT_IPI.Value;
        FmEFD_E520.EdVL_DEB_IPI.ValueVariant    := QrEFD_E520VL_DEB_IPI.Value;
        FmEFD_E520.EdVL_CRED_IPI.ValueVariant   := QrEFD_E520VL_CRED_IPI.Value;
        FmEFD_E520.EdVL_OD_IPI.ValueVariant     := QrEFD_E520VL_OD_IPI.Value;
        FmEFD_E520.EdVL_OC_IPI.ValueVariant     := QrEFD_E520VL_OC_IPI.Value;
        FmEFD_E520.EdVL_SC_IPI.ValueVariant     := QrEFD_E520VL_SC_IPI.Value;
        FmEFD_E520.EdVL_SD_IPI.ValueVariant     := QrEFD_E520VL_SD_IPI.Value;
      end;
      //
      FmEFD_E520.ShowModal;
      FmEFD_E520.Destroy;
    end;
  end;
end;

procedure TFmEFD_E001.odos1Click(Sender: TObject);
begin
  ExcluiItensNormaisDeProducao();
end;

procedure TFmEFD_E001.PCK200Change(Sender: TObject);
begin
  if PCK200.ActivePageIndex = 3 then
  begin
    if (QrEFD_K230.RecordCount = 0) and (QrEFD_K250.RecordCount > 0) then
      PCK235.ActivePageIndex := 1
    else
      PCK235.ActivePageIndex := 0;
  end;
end;

function TFmEFD_E001.PeriodoJaExiste(ImporExpor, AnoMes, Empresa: Integer;
Avisa: Boolean): Boolean;
var
  Qry1: TmySQLQuery;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry1.Close;
    Qry1.Database := Dmod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT COUNT(*) Registros ',
    'FROM efd_e001 ',
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
    Result := Qry1.FieldByName('Registros').AsInteger > 0;
    if Result and Avisa then
    begin
      Geral.MensagemBox('O periodo selecionado j� existe!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
  finally
    Qry1.Free;
  end;
end;

procedure TFmEFD_E001.PME111Popup(Sender: TObject);
var
  Habilita, Habil2, Habil3(*, Exclui*): Boolean;
begin
  // E111
  Habilita := (QrEFD_E110.State <> dsInactive) and (QrEFD_E110.RecordCount = 1);
  Incluinovoajuste1.Enabled := Habilita;
  Habilita := Habilita and
    (QrEFD_E111.State <> dsInactive) and (QrEFD_E111.RecordCount > 0);
  Alteraajusteselecionado1.Enabled := Habilita;
  Habil2 := Habilita;
  // E112
  Incluinovainformaoadicional1.Enabled := Habil2;
  // E113
  Incluinovaidentificaodedocumentofiscal1.Enabled := Habil2;
  // E112
  Habil2 := Habilita and
    (QrEFD_E112.State <> dsInactive) and (QrEFD_E112.RecordCount > 0);
  Alterainformaoadicionalselecionada1.Enabled := Habil2;
  Excluiinformaoadicionalselecionada1.Enabled := Habil2;
  // E113
  Habil3 := Habilita and
    (QrEFD_E113.State <> dsInactive) and (QrEFD_E113.RecordCount > 0);
  Alteraidentificaoselecionada1.Enabled := Habil3;
  Excluiidentificaoselecionada1.Enabled := Habil3;
  // E111
  Excluiajusteselecionado1.Enabled := Habilita and (not Habil2) and (not Habil3);
end;

procedure TFmEFD_E001.PME500Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluinovointervaloIPI1, QrEFD_E001);
  MyObjects.HabilitaMenuItemItsUpd(AlteraintervaloIPIselecionado1, QrEFD_E500);
  MyObjects.HabilitaMenuItemItsDel(ExcluiintervaloIPIselecionado1, QrEFD_E500);
end;

procedure TFmEFD_E001.PME530Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiajustedaapuracaodoIPI1, QrEFD_E520);
  MyObjects.HabilitaMenuItemItsUpd(AlteraajustedaapuracaodoIPIselecionado1, QrEFD_E530);
  MyObjects.HabilitaMenuItemItsDel(ExcluiajustedaapuracaodoIPIselecionado1, QrEFD_E530);
end;

procedure TFmEFD_E001.PMH005Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluidatadeinventrio1, QrEFD_E001);
  MyObjects.HabilitaMenuItemItsUpd(AlteraInventrio1, QrEFD_H005);
  MyObjects.HabilitaMenuItemCabDel(Excluiinventrio1, QrEFD_H005, QrEFD_H010);
end;

procedure TFmEFD_E001.PMH010Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Importadebalano1, QrEFD_H005);
  MyObjects.HabilitaMenuItemItsIns(Incluiitem1, QrEFD_H005);
  MyObjects.HabilitaMenuItemItsUpd(Alteraitem1, QrEFD_H010);
  MyObjects.HabilitaMenuItemCabDel(Excluiitemns1, QrEFD_H010, QrEFD_H020);
end;

procedure TFmEFD_E001.PMH020Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluiinformaocomplementar1, QrEFD_H010);
  MyObjects.HabilitaMenuItemItsUpd(Alterainformaocomplementar1, QrEFD_H020);
  MyObjects.HabilitaMenuItemItsDel(Excluiinformaocomplementar1, QrEFD_H020);
end;

procedure TFmEFD_E001.PMK100Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluinovointervalodeProduoeEstoque1, QrEFD_E001);
  MyObjects.HabilitaMenuItemItsUpd(AlteraintervalodeProduoeEstoque1, QrEFD_K100);
  MyObjects.HabilitaMenuItemItsDel(ExcluiintervalodeProduoeEstoque1, QrEFD_K100);
end;

procedure TFmEFD_E001.PMK200Popup(Sender: TObject);
var
  b200, b280: Boolean;
begin
  b200 := PCK200.ActivePageIndex = 0;
  b280 := PCK200.ActivePageIndex = 4;
  //
  MyObjects.HabilitaMenuItemItsIns(ImportaK200_1, QrEFD_K100);
  MyObjects.HabilitaMenuItemItsIns(IncluiK200_1, QrEFD_K200);
  MyObjects.HabilitaMenuItemItsUpd(AlteraK200_1, QrEFD_K200, b200);
  MyObjects.HabilitaMenuItemItsDel(ExcluiK200_1, QrEFD_K200, b200);
  MyObjects.HabilitaMenuItemItsDel(ExcluiK200_280_1, QrEFD_K200, b200 or b280);
end;

procedure TFmEFD_E001.PMK220Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ImportaK220_1, QrEFD_K100);
  MyObjects.HabilitaMenuItemItsIns(IncluiK220_1, QrEFD_K220);
  MyObjects.HabilitaMenuItemItsUpd(AlteraK220_1, QrEFD_K220);
  MyObjects.HabilitaMenuItemItsDel(ExcluiK220_1, QrEFD_K220);
end;

procedure TFmEFD_E001.PMK230Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabDel(K2301, QrEFD_K230, QrEFD_K235);
  MyObjects.HabilitaMenuItemCabDel(K2501, QrEFD_K250, QrEFD_K255);
  MyObjects.HabilitaMenuItemCabDel(K2601, QrEFD_K260, QrEFD_K265);
end;

procedure TFmEFD_E001.PMK280Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsDel(ExcluiK280_1, QrEFD_K280);
end;

procedure TFmEFD_E001.PMPeriodoPopup(Sender: TObject);
begin
  Excluiperiodoselecionado1.Enabled :=
    (QrEFD_E001.State <> dsInactive) and
    (QrEFD_E001.RecordCount > 0)
  and
    (QrEFD_E100.State <> dsInactive) and
    (QrEFD_E100.RecordCount = 0);
end;

procedure TFmEFD_E001.PMValDecltorioPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEFD_E110.State <> dsInactive) and (QrEFD_E110.RecordCount = 1);
  Incluinovovalordeclaratrio1.Enabled := Habilita;
  Habilita := Habilita and
    (QrEFD_E115.State <> dsInactive) and (QrEFD_E115.RecordCount > 0);
  Alteravalordeclaratrioselecionado1.Enabled := Habilita;
  Excluivalordeclaratrioselecionado1.Enabled := Habilita;
end;

procedure TFmEFD_E001.ProduoK230aK2551Click(Sender: TObject);
begin
  //ReopenEFD_K230(0, rkPrint);
  //
  MyObjects.frxDefineDataSets(frxEFD_SPEDE_K230_001, [
  DModG.frxDsDono,
  frxDsEFD_K210,
  frxDsEFD_K215,
  frxDsEFD_K230,
  frxDsEFD_K235,
  frxDsEFD_K250,
  frxDsEFD_K255,
  frxDsEFD_K270_230,
  frxDsEFD_K275_235
  //frxDsEFD_K280;
  ]);
  //
  MyObjects.frxMostra(frxEFD_SPEDE_K230_001, 'Itens produzidos');
end;

procedure TFmEFD_E001.PME100Popup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEFD_E001.State <> dsInactive) and (QrEFD_E001.RecordCount > 0);
  Incluinovointervalo1.Enabled := Habilita;
  //
  Habilita := Habilita and
    (QrEFD_E100.State <> dsInactive) and (QrEFD_E100.RecordCount > 0);
  Alteraintervaloselecionado1.Enabled := Habilita;
  Excluiintervaloselecionado1.Enabled := Habilita and
    (QrEFD_E100.State <> dsInactive) and (QrEFD_E100.RecordCount > 0)
  and
    (QrEFD_E111.State <> dsInactive) and (QrEFD_E111.RecordCount = 0)
  and
    (QrEFD_E116.State <> dsInactive) and (QrEFD_E116.RecordCount = 0);
end;

procedure TFmEFD_E001.PMObrigacoesPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEFD_E110.State <> dsInactive) and (QrEFD_E110.RecordCount = 1);
  IncluinovaobrigaodoICMSarecolher1.Enabled := Habilita;
  Habilita := Habilita and
    (QrEFD_E116.State <> dsInactive) and (QrEFD_E116.RecordCount > 0);
  Alteraobrigaoselecionada1.Enabled := Habilita;
  Excluiobrigaoselecionada1.Enabled := Habilita;
end;

procedure TFmEFD_E001.RGEmitenteClick(Sender: TObject);
begin
  ReopenEFD_E100(0);
end;

procedure TFmEFD_E001.ReopenEFD_1010();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_1010, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_1010 l010 ',
  'WHERE l010.ImporExpor=' + Geral.FF0(QrEFD_E001ImporExpor.Value),
  'AND l010.Empresa=' + Geral.FF0(QrEFD_E001Empresa.Value),
  'AND l010.AnoMes=' + Geral.FF0(QrEFD_E001AnoMes.Value),
  '']);
end;

procedure TFmEFD_E001.ReopenEFD_E001(AnoMes: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E001, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT, ',
  'CONCAT(RIGHT(e001.AnoMes, 2), "/", ',
  'LEFT(LPAD(e001.AnoMes, 6, "0"), 4)) MES_ANO, e001.* ',
  'FROM efd_e001 e001 ',
  'LEFT JOIN entidades ent ON ent.Codigo=e001.Empresa ',
  'WHERE e001.ImporExpor=' + Geral.FF0(FImporExpor),
  'AND e001.Empresa=' + Geral.FF0(FEmpresa),
  'ORDER BY e001.AnoMes DESC ',
  '']);
  //
  QrEFD_E001.Locate('AnoMes', AnoMes, []);
end;

procedure TFmEFD_E001.ReopenEFD_E100(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E100, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_e100 e100 ',
  'WHERE e100.ImporExpor=' + Geral.FF0(QrEFD_E001ImporExpor.Value),
  'AND e100.Empresa=' + Geral.FF0(QrEFD_E001Empresa.Value),
  'AND e100.AnoMes=' + Geral.FF0(QrEFD_E001AnoMes.Value),
  'ORDER BY e100.DT_INI',
  '']);
  //
  QrEFD_E100.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_E110();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E110, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_e110 E110 ',
  'WHERE E110.ImporExpor=' + Geral.FF0(QrEFD_E001ImporExpor.Value),
  'AND E110.Empresa=' + Geral.FF0(QrEFD_E001Empresa.Value),
  'AND E110.AnoMes=' + Geral.FF0(QrEFD_E001AnoMes.Value),
  'AND E110.LinArq=' + Geral.FF0(QrEFD_E100LinArq.Value),
  '']);
end;

procedure TFmEFD_E001.ReopenEFD_E111(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E111, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_e111 E111 ',
  'WHERE E111.ImporExpor=' + Geral.FF0(QrEFD_E110ImporExpor.Value),
  'AND E111.Empresa=' + Geral.FF0(QrEFD_E110Empresa.Value),
  'AND E111.AnoMes=' + Geral.FF0(QrEFD_E110AnoMes.Value),
  'AND E111.E110=' + Geral.FF0(QrEFD_E110LinArq.Value),
  '']);
end;

procedure TFmEFD_E001.ReopenEFD_E112(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E112, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_e112 E112 ',
  'WHERE E112.ImporExpor=' + Geral.FF0(QrEFD_E111ImporExpor.Value),
  'AND E112.Empresa=' + Geral.FF0(QrEFD_E111Empresa.Value),
  'AND E112.AnoMes=' + Geral.FF0(QrEFD_E111AnoMes.Value),
  'AND E112.E111=' + Geral.FF0(QrEFD_E111LinArq.Value),
  '']);
end;

procedure TFmEFD_E001.ReopenEFD_E113(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E113, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_e113 E113 ',
  'WHERE E113.ImporExpor=' + Geral.FF0(QrEFD_E111ImporExpor.Value),
  'AND E113.Empresa=' + Geral.FF0(QrEFD_E111Empresa.Value),
  'AND E113.AnoMes=' + Geral.FF0(QrEFD_E111AnoMes.Value),
  'AND E113.E111=' + Geral.FF0(QrEFD_E111LinArq.Value),
  '']);
end;

procedure TFmEFD_E001.ReopenEFD_E115(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E115, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_e115 E115 ',
  'WHERE E115.ImporExpor=' + Geral.FF0(QrEFD_E110ImporExpor.Value),
  'AND E115.Empresa=' + Geral.FF0(QrEFD_E110Empresa.Value),
  'AND E115.AnoMes=' + Geral.FF0(QrEFD_E110AnoMes.Value),
  'AND E115.E110=' + Geral.FF0(QrEFD_E110LinArq.Value),
  '']);
end;

procedure TFmEFD_E001.ReopenEFD_E116(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E116, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_e116 E116 ',
  'WHERE E116.ImporExpor=' + Geral.FF0(QrEFD_E110ImporExpor.Value),
  'AND E116.Empresa=' + Geral.FF0(QrEFD_E110Empresa.Value),
  'AND E116.AnoMes=' + Geral.FF0(QrEFD_E110AnoMes.Value),
  'AND E116.E110=' + Geral.FF0(QrEFD_E110LinArq.Value),
  '']);
end;

procedure TFmEFD_E001.ReopenEFD_E500(LinArq: Integer);
var
  ATT_IND_APUR: String;
begin
  ATT_IND_APUR := dmkPF.ArrayToTexto('e500.IND_APUR', 'NO_IND_APUR', pvPos, True,
    sEFD_IND_APUR);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E500, Dmod.MyDB, [
  //'SELECT ELT(e500.IND_APUR + 1, "Mensal", ',
  //'"Decendial", "???") NO_IND_APUR, e500.* ',
  'SELECT ',
  ATT_IND_APUR,
  'e500.* ',
  'FROM efd_e500 e500 ',
  'WHERE e500.ImporExpor=' + Geral.FF0(QrEFD_E001ImporExpor.Value),
  'AND e500.Empresa=' + Geral.FF0(QrEFD_E001Empresa.Value),
  'AND e500.AnoMes=' + Geral.FF0(QrEFD_E001AnoMes.Value),
  'ORDER BY e500.DT_INI, e500.DT_FIN',
  '']);
  //
  QrEFD_E500.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_E510(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E510, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_e510 E510 ',
  'WHERE E510.ImporExpor=' + Geral.FF0(QrEFD_E001ImporExpor.Value),
  'AND E510.Empresa=' + Geral.FF0(QrEFD_E001Empresa.Value),
  'AND E510.AnoMes=' + Geral.FF0(QrEFD_E001AnoMes.Value),
  //'AND E510.LinArq=' + Geral.FF0(QrEFD_E500LinArq.Value),
  'AND E510.E500=' + Geral.FF0(QrEFD_E500LinArq.Value),
  '']);
  //
  QrEFD_E510.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_E520;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E520, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_e520 E520 ',
  'WHERE E520.ImporExpor=' + Geral.FF0(QrEFD_E001ImporExpor.Value),
  'AND E520.Empresa=' + Geral.FF0(QrEFD_E001Empresa.Value),
  'AND E520.AnoMes=' + Geral.FF0(QrEFD_E001AnoMes.Value),
  //'AND E520.LinArq=' + Geral.FF0(QrEFD_E500LinArq.Value),
  'AND E520.E500=' + Geral.FF0(QrEFD_E500LinArq.Value),
  '']);
end;

procedure TFmEFD_E001.ReopenEFD_E530(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E530, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_e530 E530 ',
  'WHERE E530.ImporExpor=' + Geral.FF0(QrEFD_E520ImporExpor.Value),
  'AND E530.Empresa=' + Geral.FF0(QrEFD_E520Empresa.Value),
  'AND E530.AnoMes=' + Geral.FF0(QrEFD_E520AnoMes.Value),
  'AND E530.E520=' + Geral.FF0(QrEFD_E520LinArq.Value),
  '']);
  //
  QrEFD_E530.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_H005(LinArq: Integer);
var
  ATT_MOT_INV: String;
begin
  ATT_MOT_INV := dmkPF.ArrayToTexto('h005.MOT_INV', 'NO_MOT_INV', pvPos, True,
    sEFD_MOT_INV);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_H005, Dmod.MyDB, [
  'SELECT ',
  ATT_MOT_INV,
  'h005.* ',
  'FROM efd_h005 h005 ',
  'WHERE h005.ImporExpor=' + Geral.FF0(QrEFD_E001ImporExpor.Value),
  'AND h005.Empresa=' + Geral.FF0(QrEFD_E001Empresa.Value),
  'AND h005.AnoMes=' + Geral.FF0(QrEFD_E001AnoMes.Value),
  'ORDER BY h005.DT_INV',
  '']);
  //
  QrEFD_h005.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_H010(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_H010, Dmod.MyDB, [
  'SELECT IF(h010.COD_PART="", "", IF(ter.Tipo=0, ',
  'ter.RazaoSocial, ter.Nome)) NO_PART, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, h010.* ',
  'FROM efd_h010 h010 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=h010.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'LEFT JOIN entidades  ter ON ter.Codigo=h010.COD_PART ',
  'WHERE h010.ImporExpor=' + Geral.FF0(QrEFD_E001ImporExpor.Value),
  'AND h010.Empresa=' + Geral.FF0(QrEFD_E001Empresa.Value),
  'AND h010.AnoMes=' + Geral.FF0(QrEFD_E001AnoMes.Value),
  'AND h010.H005=' + Geral.FF0(QrEFD_H005LinArq.Value),
  '']);
  //
  //Geral.MB_SQL(Self, QrEFD_H010);
  QrEFD_H010.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_H020(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_H020, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_h020 H020 ',
  'WHERE H020.ImporExpor=' + Geral.FF0(QrEFD_E001ImporExpor.Value),
  'AND H020.Empresa=' + Geral.FF0(QrEFD_E001Empresa.Value),
  'AND H020.AnoMes=' + Geral.FF0(QrEFD_E001AnoMes.Value),
  'AND H020.H010=' + Geral.FF0(QrEFD_H010LinArq.Value),
  '']);
  //
  QrEFD_H020.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K100(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K100, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_k100 k100 ',
  'WHERE k100.ImporExpor=' + Geral.FF0(QrEFD_E001ImporExpor.Value),
  'AND k100.Empresa=' + Geral.FF0(QrEFD_E001Empresa.Value),
  'AND k100.AnoMes=' + Geral.FF0(QrEFD_E001AnoMes.Value),
  'ORDER BY k100.DT_INI, k100.DT_FIN ',
  '']);
  //
  QrEFD_K100.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K200(LinArq: Integer; ReopenKind: TReopenKind);
var
  ATT_IND_EST, Ordem: String;
begin
  ATT_IND_EST := dmkPF.ArrayToTexto('k200.IND_EST', 'NO_IND_EST', pvPos, True,
    sEFD_IND_EST);
  //
  case ReopenKind of
    rkGrade: Ordem := 'ORDER BY DT_EST ';
    rkPrint: Ordem := 'ORDER BY IND_EST, NO_PART, NO_PRD_TAM_COR, DT_EST';
    else Ordem := '???';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K200, Dmod.MyDB, [
  'SELECT IF(K200.COD_PART="", "", IF(ter.Tipo=0, ',
  'ter.RazaoSocial, ter.Nome)) NO_PART, ',
  'unm.Sigla, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  ATT_IND_EST,
  'K200.* ',
  'FROM efd_K200 K200 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K200.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN entidades  ter ON ter.Codigo=K200.COD_PART ',
  'WHERE k200.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k200.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k200.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k200.K100=' + Geral.FF0(QrEFD_K100LinArq.Value),
  Ordem,
  '']);
  //
  //Geral.MB_SQL(Self, QrEFD_K200);
  QrEFD_K200.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K210(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K210, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'IF(k210.DT_FIN_OS = 0, "", ',
  'DATE_FORMAT(k210.DT_FIN_OS, "%d/%m/%Y")) DT_FIN_OS_Txt, ',
  'K210.* ',
  'FROM efd_K210 k210 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K210.COD_ITEM_ORI ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE k210.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k210.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k210.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k210.K100=' + Geral.FF0(QrEFD_K100LinArq.Value),
  'ORDER BY DT_INI_OS, DT_FIN_OS, COD_DOC_OS, COD_ITEM ',
  '']);
  //
  //Geral.MB_SQL(Self, QrEFD_K210);
  QrEFD_K210.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K215(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K215, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'k215.* ',
  'FROM efd_k215 k215 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k215.COD_ITEM_DES ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE k215.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k215.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k215.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k215.K210=' + Geral.FF0(QrEFD_K210.FieldByName('LinArq').AsInteger), //.Value),
  '']);
  //Geral.MB_SQL(Self, QrEFD_K215);
  //
  QrEFD_K215.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K220(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K220, Dmod.MyDB, [
  'SELECT  ',
  'CONCAT(gg1o.Nome, ',
  'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), ',
  'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) ',
  'NO_PRD_TAM_COR_ORI, ',
  'CONCAT(gg1d.Nome, ',
  'IF(gtid.PrintTam=0, "", CONCAT(" ", gtid.Nome)), ',
  'IF(gccd.PrintCor=0,"", CONCAT(" ", gccd.Nome))) ',
  'NO_PRD_TAM_COR_DEST, ',
  'IF(gg1o.UnidMed=gg1d.UnidMed, unmo.Sigla, "???") Sigla, ',
  'K220.* ',
  'FROM efd_K220 K220 ',
  'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K220.COD_ITEM_ORI ',
  'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC ',
  'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad ',
  'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI ',
  'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 ',
  'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed ',
  ' ',
  'LEFT JOIN gragrux    ggxd ON ggxd.Controle=K220.COD_ITEM_DEST ',
  'LEFT JOIN gragruc    ggcd ON ggcd.Controle=ggxd.GraGruC ',
  'LEFT JOIN gracorcad  gccd ON gccd.Codigo=ggcd.GraCorCad ',
  'LEFT JOIN gratamits  gtid ON gtid.Controle=ggxd.GraTamI ',
  'LEFT JOIN gragru1    gg1d ON gg1d.Nivel1=ggxd.GraGru1 ',
  'LEFT JOIN unidmed    unmd ON unmd.Codigo=gg1d.UnidMed ',
  'WHERE k220.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k220.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k220.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k220.K100=' + Geral.FF0(QrEFD_K100LinArq.Value),
  'ORDER BY k220.MovimCod, k220.DT_MOV, COD_ITEM_ORI, COD_ITEM_DEST ',
  '']);
  //Geral.MB_SQL(Self, QrEFD_K220);
  //
  QrEFD_K220.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K230(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K230, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
  'IF(k230.DT_FIN_OP = 0, "", ',
  'DATE_FORMAT(k230.DT_FIN_OP, "%d/%m/%Y")) DT_FIN_OP_Txt,',
  'K230.* ',
  'FROM efd_K230 k230 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K230.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k230.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k230.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k230.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k230.K100=' + Geral.FF0(QrEFD_K100LinArq.Value),
  'ORDER BY DT_INI_OP, DT_FIN_OP, COD_DOC_OP, COD_ITEM ',
  '']);
  //Geral.MB_SQL(Self, QrEFD_K230);
  QrEFD_K230.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K235(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K235, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,  ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, ',
  'k235.*  ',
  'FROM efd_k235 k235 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k235.COD_ITEM  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   ',
  'WHERE k235.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k235.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k235.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k235.K230=' + Geral.FF0(QrEFD_K230LinArq.Value),
  '']);
  //Geral.MB_SQL(Self, QrEFD_K235);
  //
  QrEFD_K235.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K250(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K250, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
  'IF(k250.DT_PROD = 0, "", ',
  'DATE_FORMAT(k250.DT_PROD, "%d/%m/%Y")) DT_PROD_Txt,',
  'K250.* ',
  'FROM efd_K250 k250 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K250.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k250.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k250.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k250.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k250.K100=' + Geral.FF0(QrEFD_K100LinArq.Value),
  'ORDER BY DT_PROD, COD_DOC_OP, COD_ITEM ',
  '']);
  //
  QrEFD_K250.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K255(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K255, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,  ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, ',
  'k255.*  ',
  'FROM efd_k255 k255 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k255.COD_ITEM  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   ',
  'WHERE k255.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k255.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k255.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k255.K250=' + Geral.FF0(QrEFD_K250LinArq.Value),
  '']);
  //
  QrEFD_K255.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K260(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K260, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,',
  'CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR,',
  'IF(k260.DT_RET = 0, "",',
  'DATE_FORMAT(k260.DT_RET, "%d/%m/%Y")) DT_RET_Txt,',
  'K260.*',
  'FROM efd_K260 k260',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K260.COD_ITEM',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed',
  'WHERE k260.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k260.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k260.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k260.K100=' + Geral.FF0(QrEFD_K100LinArq.Value),
  'ORDER BY DT_SAIDA, DT_RET, COD_OP_OS, COD_ITEM ',
  '']);
  //
  QrEFD_K260.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K265(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K265, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'k265.* ',
  'FROM efd_k265 k265 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k265.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE k265.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k265.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k265.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k265.K260=' + Geral.FF0(QrEFD_K260LinArq.Value),
  '']);
  //
  QrEFD_K265.Locate('LinArq', LinArq, []);
  //Geral.MB_SQL(Self, QrEFD_K265);
end;

procedure TFmEFD_E001.ReopenEFD_K270_210(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K270_210, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, K270.* ',
  'FROM efd_K270 k270 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k270.K100=' + Geral.FF0(QrEFD_K100LinArq.Value),
  'AND k270.ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_K270_08_Origem.sek2708oriK210K215)),
  'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ',  '']);
  //
  //Geral.MB_SQL(Self, QrEFD_K270_210);
  QrEFD_K270_210.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K270_220(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K270_220, Dmod.MyDB, [
  'SELECT  ',
  'CONCAT(gg1o.Nome, ',
  'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), ',
  'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) ',
  'NO_PRD_TAM_COR, unmo.Sigla, ',
  'K270.* ',
  'FROM efd_K270 K270 ',
  'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K270.COD_ITEM',
  'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC ',
  'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad ',
  'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI ',
  'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 ',
  'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed ',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k270.K100=' + Geral.FF0(QrEFD_K100LinArq.Value),
  'AND k270.ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_K270_08_Origem.sek2708oriK220)),
  'ORDER BY k270.MovimCod, k270.DT_INI_AP, DT_FIN_AP, COD_ITEM ',
  '']);
  //Geral.MB_SQL(Self, QrEFD_K270);
  //
  QrEFD_K270_220.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K270_230(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K270_230, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR,  ',
  'K270.* ',
  'FROM efd_K270 k270 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k270.K100=' + Geral.FF0(QrEFD_K100LinArq.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_K270_08_Origem.sek2708oriK230K235)),
  'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ',
  '']);
  //
  QrEFD_K270_230.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K270_250(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K270_250, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,',
  'CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, K270.*',
  'FROM efd_K270 k270',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k270.K100=' + Geral.FF0(QrEFD_K100LinArq.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_K270_08_Origem.sek2708oriK250K255)),
  'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM',
  '']);
  //
  QrEFD_K270_250.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K270_260(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K270_260, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,',
  'CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, K270.*',
  'FROM efd_K270 k270',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed',
  'WHERE k270.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k270.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k270.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k270.K100=' + Geral.FF0(QrEFD_K100LinArq.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_K270_08_Origem.sek2708oriK260K265)),
  'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM',
  '']);
  //
  QrEFD_K270_260.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K275_215(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K275_215, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'k275.* ',
  'FROM efd_k275 k275 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'WHERE k275.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k275.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k275.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k275.K270=' + Geral.FF0(QrEFD_K270_210LinArq.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_K270_08_Origem.sek2708oriK210K215)),
  '']);
  //Geral.MB_SQL(Self, QrEFD_K275);
  //
  QrEFD_K275_215.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K275_220(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K275_220, Dmod.MyDB, [
  'SELECT ',
  'CONCAT(gg1o.Nome, ',
  'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), ',
  'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) ',
  'NO_PRD_TAM_COR, unmo.Sigla, ',
  'K275.* ',
  'FROM efd_K275 K275 ',
  'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K275.COD_ITEM ',
  'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC ',
  'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad ',
  'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI ',
  'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 ',
  'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed ',
  'WHERE k275.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k275.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k275.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k275.K270=' + Geral.FF0(QrEFD_K270_220LinArq.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_K270_08_Origem.sek2708oriK220)),
  '']);
  //Geral.MB_SQL(Self, QrEFD_K275);
  //
  QrEFD_K275_220.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K275_235(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K275_235, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,  ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, ',
  'k275.*  ',
  'FROM efd_k275 k275 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   ',
  'WHERE k275.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k275.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k275.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k275.K270=' + Geral.FF0(QrEFD_K270_230LinArq.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_K270_08_Origem.sek2708oriK230K235)),
  '']);
  //Geral.MB_SQL(Self, QrEFD_K275);
  //
  QrEFD_K275_235.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K275_255(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K275_255, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,  ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, ',
  'k275.*  ',
  'FROM efd_k275 k275 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   ',
  'WHERE k275.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k275.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k275.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k275.K270=' + Geral.FF0(QrEFD_K270_250LinArq.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_K270_08_Origem.sek2708oriK250K255)),
  '']);
  //Geral.MB_SQL(Self, QrEFD_K275_255);
  //
  QrEFD_K275_255.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K275_265(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K275_265, Dmod.MyDB, [
  'SELECT unm.Grandeza, unm.Sigla,  ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, ',
  'k275.*  ',
  'FROM efd_k275 k275 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   ',
  'WHERE k275.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k275.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k275.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k275.K270=' + Geral.FF0(QrEFD_K270_260LinArq.Value),
  'AND ORIGEM=' + Geral.FF0(Integer(TSPED_EFD_K270_08_Origem.sek2708oriK260K265)),
  '']);
  //
  QrEFD_K275_265.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K280(LinArq: Integer; ReopenKind: TReopenKind);
var
  ATT_IND_EST, ATT_OrigemSPEDEFDKnd, Ordem: String;
begin
  ATT_IND_EST := dmkPF.ArrayToTexto('k280.IND_EST', 'NO_IND_EST', pvPos, True,
    sEFD_IND_EST);
  //
  ATT_OrigemSPEDEFDKnd := dmkPF.ArrayToTexto('k280.OriSPEDEFDKnd',
  'NO_OriSPEDEFDKnd', pvPos, True, sOrigemSPEDEFDKnd);
  //
  case ReopenKind of
    rkGrade: Ordem := 'ORDER BY DT_EST ';
    rkPrint: Ordem := 'ORDER BY IND_EST, NO_PART, NO_PRD_TAM_COR, DT_EST';
    else Ordem := '???';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K280, Dmod.MyDB, [
  'SELECT IF(K280.COD_PART="", "", IF(ter.Tipo=0, ',
  'ter.RazaoSocial, ter.Nome)) NO_PART, ',
  'unm.Sigla, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  ATT_IND_EST,
  ATT_OrigemSPEDEFDKnd,
  'K280.* ',
  'FROM efd_K280 K280 ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=K280.COD_ITEM ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN entidades  ter ON ter.Codigo=K280.COD_PART ',
  'WHERE k280.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND k280.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND k280.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'AND k280.K100=' + Geral.FF0(QrEFD_K100LinArq.Value),
  Ordem,
  '']);
  //
  //Geral.MB_SQL(Self, QrEFD_K280);
  QrEFD_K280.Locate('LinArq', LinArq, []);
end;

procedure TFmEFD_E001.ReopenEFD_K_ConfG(GraGruX: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K_ConfG, Dmod.MyDB, [
  'SELECT ekc.GraGruX,  ',
  '  SUM(ekc.Movim01) Sdo_Ini, ',
  '  SUM(ekc.Movim02) Compra,  ',
  '  SUM(ekc.Movim03) Producao, ',
  '  SUM(ekc.Movim12) ProduReforma, ',
  '  SUM(ekc.Movim11) EntrouDesmonte, ',
  '  SUM(ekc.Movim04) EntrouClasse,  ',
  '  SUM(ekc.Movim05) Consumo, ',
  '  SUM(ekc.Movim13) InsumReforma, ',
  '  SUM(ekc.Movim15) SaiuDesmonte, ',
  '  SUM(ekc.Movim06) SaiuClasse,  ',
  '  SUM(ekc.Movim07) Venda, ',
  '  SUM(ekc.Movim08) ETE, ',
  //
  '  SUM(ekc.Movim16) BalancoIndev,  ',
  '  SUM(ekc.Movim17) ErrEmpresa,  ',
  '  SUM(ekc.Movim18) Indevido,  ',
  '  SUM(ekc.Movim19) SubProduto, ',
  '  SUM(ekc.Movim20) Final,  ',
  ' ',
  '  SUM(ekc.Movim01) + ',
  '  SUM(ekc.Movim02) + ',
  '  SUM(ekc.Movim03) + ',
  '  SUM(ekc.Movim11) + ',
  '  SUM(ekc.Movim12) + ',
  '  SUM(ekc.Movim04) + ',
  '  SUM(ekc.Movim05) - ',
  '  SUM(ekc.Movim13) + ',
  '  SUM(ekc.Movim15) + ',
  '  SUM(ekc.Movim06) + ',
  '  SUM(ekc.Movim07) + ',
  '  SUM(ekc.Movim08) + ',
  //
  '  SUM(ekc.Movim16) + ',
  '  SUM(ekc.Movim17) + ',
  '  SUM(ekc.Movim18) + ',
  '  SUM(ekc.Movim19) - ',
  '  SUM(ekc.Movim20) Diferenca,  ',
  'med.Sigla, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, ',
  'med.Grandeza, ekc.ESTSTabSorc  ',
  'FROM efd_k_confg ekc  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=ekc.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed  ',
  'WHERE ekc.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
  'AND ekc.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
  'AND ekc.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
  'GROUP BY GraGruX ',
  'ORDER BY GraGruX  ',
  '']);
  //Geral.MB_SQL(Self, QrEFD_K_ConfG);
  //
  QrEFD_K_ConfG.Locate('GraGruX', GraGruX, []);
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEFD_E001.BtSaida0Click(Sender: TObject);
begin
  //VAR_CADASTRO := Qr?.Value;
  Close;
end;

procedure TFmEFD_E001.BtVerificaClick(Sender: TObject);
begin
  VerificaBlocoK();
  PCEx00.ActivePageIndex := 4;
end;

procedure TFmEFD_E001.DBGK230DblClick(Sender: TObject);
begin
  JanelaDoMovimento();
end;

procedure TFmEFD_E001.DBGK250DblClick(Sender: TObject);
begin
  JanelaDoMovimento();
end;

procedure TFmEFD_E001.DBGK270_230DblClick(Sender: TObject);
begin
  JanelaDoMovimento();
end;

procedure TFmEFD_E001.dmkDBGridZTO7CellClick(Column: TColumn);
var
  GraGruX, Insumo, AnoMesPsq: Integer;
  DiaIni, DiaFim: TDateTime;
  SQL_Periodo: String;
  ATT_MovimID: String;
begin
  FColumnSel := '';
  QrPsq01.Close;
  DBGPsq01.Visible := True;
  DBGPsq02.Visible := False;
  //
  GraGruX := QrEFD_K_ConfGGraGruX.Value;
  AnoMesPsq := QrEFD_K100AnoMes.Value;
  if (Column.FieldName = 'Sdo_Ini') or (Column.FieldName = 'Final') then
  begin
    if (Column.FieldName = 'Sdo_Ini') then
      AnoMesPsq := dmkPF.IncrementaAnoMes(AnoMesPsq, -1);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq01, Dmod.MyDB, [
    'SELECT * ',
    'FROM efd_k200 ',
    'WHERE ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND AnoMes=' + Geral.FF0(AnoMesPsq),
    'AND Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
  end
  (*else if Column.FieldName = 'Compra' then*)
  else if Column.FieldName = 'Producao' then
  begin
    FColumnSel := Column.FieldName;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq01, Dmod.MyDB, [
    'SELECT Codigo, MovimID, MovimCod, ',
    'DT_FIN_OP DATA_FIM, QTD_ENC QTDE,',
    'GraGruX',
    'FROM efd_k230',
    'WHERE ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '',
    'UNION',
    '',
    'SELECT Codigo, MovimID, MovimCod, ',
    'DT_PROD DATA_FIM, QTD QTDE,',
    'GraGruX  ',
    'FROM efd_k250',
    'WHERE ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
  end else
  if (Column.FieldName = 'EntrouClasse')
  or (Column.FieldName = 'SaiuClasse') then
  begin
    FColumnSel := Column.FieldName;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq01, Dmod.MyDB, [
    'SELECT * FROM efd_k220 ',
    'WHERE ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND ( ',
    '  GGXDst=' + Geral.FF0(GraGruX),
    '  OR GGXOri=' + Geral.FF0(GraGruX),
    ')',
    '']);
  end else
  if Column.FieldName = 'Consumo' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq01, Dmod.MyDB, [
    'SELECT ekc.ImporExpor, ekc.AnoMes, ekc.Empresa,  ',
    'ekc.LinArq, ekc.REG, ekc.K230 K2X0, ekc.DT_SAIDA DATA,  ',
    'ekc.COD_ITEM, ekc.QTD, ekc.COD_INS_SUBST,  ',
    'ekc.ID_SEK, ekc.ID_Item, ekc.MovimID,  ',
    'ekc.Codigo, ekc.MovimCod, ekc.Controle,  ',
    'ekc.GraGruX, ekc.ESTSTabSorc  ',
    'FROM efd_k235 ekc ',
    'WHERE ekc.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND ekc.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND ekc.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND ekc.GraGruX=' + Geral.FF0(GraGruX),
    ' ',
    'UNION ',
    ' ',
    'SELECT ekc.ImporExpor, ekc.AnoMes, ekc.Empresa,  ',
    'ekc.LinArq, ekc.REG, ekc.K250 K2X0, ekc.DT_CONS DATA,  ',
    'ekc.COD_ITEM, ekc.QTD, ekc.COD_INS_SUBST,  ',
    'ekc.ID_SEK, ekc.ID_Item, ekc.MovimID,  ',
    'ekc.Codigo, ekc.MovimCod, ekc.Controle,  ',
    'ekc.GraGruX, ekc.ESTSTabSorc  ',
    'FROM efd_k255 ekc ',
    'WHERE ekc.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND ekc.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND ekc.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND ekc.GraGruX=' + Geral.FF0(GraGruX),
    '']);
  end else
  //else if Column.FieldName = 'SaiuClasse' then  Acima!!!!!!!!!!!!!!!!!!
  //else
  if Column.FieldName = 'Venda' then
  begin
    FColumnSel := Column.FieldName;
    //
    DiaIni      := Geral.AnoMesToData(QrEFD_E001AnoMes.Value, 1);
    DiaFim      := IncMonth(DiaIni, 1) -1;
    SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataHora ', DiaIni, DiaFim, True, True);
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq01, Dmod.MyDB, [
    'SELECT Controle, MovimID, Codigo, MovimCod,',
    'DataHora, Pecas, AreaM2, PesoKg, SrcMovID, SrcNivel1,',
    'SrcNivel2, SrcGGX   ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    SQL_Periodo,
    'AND MovimID=2',
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
  end
  else if Column.FieldName = 'Indevido' then
  begin
(*
    ATT_MovimID := dmkPF.ArrayToTexto('MovimID', 'NO_MovimID', pvPos, True,
      sEstqMovimID);
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq01, Dmod.MyDB, [
    'SELECT ekc.MovimID, ' + ATT_MovimID + ' ekc.Movim18 ',
    'FROM efd_k_confg ekc ',
    'WHERE ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND MovimXX=18',
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
*)
    DiaIni      := Geral.AnoMesToData(QrEFD_E001AnoMes.Value, 1);
    DiaFim      := IncMonth(DiaIni, 1) -1;
    SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataHora ', DiaIni, DiaFim, True, True);
    (*
    case TEstqSPEDTabSorc(QrEFD_K_ConfGESTSTabSorc.Value) of
      estsND: ;
      estsVMI:
      begin
    *)
        UnDmkDAC_PF.AbreMySQLQuery0(QrPsq01, Dmod.MyDB, [
        'SELECT vmi.Codigo, vmi.GraGruX, vmi.MovimID, vmi.MovimCod, ',
        'vmi.Controle, SUM(vmi.Pecas) Pecas, SUM(vmi.AreaM2) AreaM2, ',
        'SUM(PesoKg) PesoKg, med.Grandeza ',
        'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
        'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
        'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
        SQL_Periodo,
        'AND vmi.MovimID IN (0,3,4,5,9,10,12,13,17,18)',
        'AND vmi.GraGruX=' + Geral.FF0(GraGruX),
        'AND Empresa=' + Geral.FF0(FEmpresa),
        'GROUP BY GraGruX',
        'ORDER BY GraGruX ',
        '']);
        DBGPsq01.Visible := QrPsq01.RecordCount > 0;
      (*
      end;
      estsPQx:
      begin
      *)
        DiaIni      := Geral.AnoMesToData(QrEFD_E001AnoMes.Value, 1);
        DiaFim      := IncMonth(DiaIni, 1) -1;
        SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataX ', DiaIni, DiaFim, True, True);
        //
        Insumo      := VS_PF.ObtemInsumoDeGraGruX(GraGruX);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrPsq02, Dmod.MyDB, [
        'DROP TABLE IF EXISTS _SPED_EFD_K2XX_SEM_MC;',
        'CREATE TABLE _SPED_EFD_K2XX_SEM_MC',
        ' ',
        'SELECT * ',
        'FROM ' + TMeuDB + '.pqx ',
        SQL_Periodo,
        'AND Tipo = 110',
        'AND Insumo=' + Geral.FF0(Insumo),
        'AND OrigemCodi IN (',
        '  SELECT Codigo',
        '  FROM ' + TMeuDB + '.emit',
        '  WHERE VSMovCod=0)',
        ' ',
        'UNION',
        '',
        'SELECT * ',
        'FROM ' + TMeuDB + '.pqx ',
        SQL_Periodo,
        'AND Tipo = 190',
        'AND Insumo=' + Geral.FF0(Insumo),
        'AND OrigemCodi IN (',
        '  SELECT Codigo',
        '  FROM ' + TMeuDB + '.pqo',
        '  WHERE VSMovCod=0)',
        ';',
        '',
        'SELECT * ',
        'FROM _SPED_EFD_K2XX_SEM_MC',
        'ORDER BY Insumo;',
        '']);
        DBGPsq02.Visible := QrPsq02.RecordCount > 0;
    (*
      end;
    end;
    *)
  end
  (*else if Column.FieldName = 'Final' then
  else if Column.FieldName = 'Diferenca' then
  *)
end;

procedure TFmEFD_E001.dmkDBGridZTO8DblClick(Sender: TObject);
begin
  JanelaDoMovimento();
end;

procedure TFmEFD_E001.DBGPsq02DblClick(Sender: TObject);
begin
  if (FColumnSel = 'Producao')
  or (FColumnSel = 'EntrouClasse')
  or (FColumnSel = 'SaiuClasse')
  or (FColumnSel = 'Venda') then
  begin
    VS_PF.MostroFormVSMovimCod(QrPsq01.FieldByName('MovimCod').AsInteger);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEFD_E001.AlteraajustedaapuracaodoIPIselecionado1Click(
  Sender: TObject);
begin
  InsUpdE530(stUpd);
end;

procedure TFmEFD_E001.Alteraajusteselecionado1Click(Sender: TObject);
begin
  InsUpdE111(stUpd);
end;

procedure TFmEFD_E001.Alteraidentificaoselecionada1Click(Sender: TObject);
begin
  InsUpdE113(stUpd);
end;

procedure TFmEFD_E001.Alterainformaoadicionalselecionada1Click(Sender: TObject);
begin
  InsUpdE112(stUpd);
end;

procedure TFmEFD_E001.Alterainformaocomplementar1Click(Sender: TObject);
begin
  InsUpdH020(stUpd);
end;

procedure TFmEFD_E001.AlteraintervalodeProduoeEstoque1Click(Sender: TObject);
begin
  InsUpdK100(stUpd);
end;

procedure TFmEFD_E001.AlteraintervaloIPIselecionado1Click(Sender: TObject);
begin
  InsUpdE500(stUpd);
end;

procedure TFmEFD_E001.Alteraintervaloselecionado1Click(Sender: TObject);
begin
  InsUpdE100(stUpd);
end;

procedure TFmEFD_E001.AlteraInventrio1Click(Sender: TObject);
begin
  InsUpdH005(stUpd);
end;

procedure TFmEFD_E001.Alteraitem1Click(Sender: TObject);
begin
  InsUpdH010(stUpd);
end;

procedure TFmEFD_E001.AlteraK200_1Click(Sender: TObject);
begin
  InsUpdK200(stUpd);
end;

procedure TFmEFD_E001.AlteraK220_1Click(Sender: TObject);
begin
  InsUpdK220(stUpd);
end;

procedure TFmEFD_E001.AlteraK230_1Click(Sender: TObject);
begin
  InsUpdK230(stUpd);
end;

procedure TFmEFD_E001.Alteraobrigaoselecionada1Click(Sender: TObject);
begin
  InsUpdE116(stUpd);
end;

procedure TFmEFD_E001.Alteravalordeclaratrioselecionado1Click(Sender: TObject);
begin
  InsUpdE115(stUpd);
end;

procedure TFmEFD_E001.AtualizaValoresE520deE530();
var
  Qry: TmySQLQuery;
  VL_SD_ANT_IPI, VL_DEB_IPI, VL_CRED_IPI, VL_OD_IPI, VL_OC_IPI, VL_SC_IPI,
  VL_SD_IPI, Valor, Saldo: Double;
  IND_AJ: String;
  ImporExpor, AnoMes, Empresa, LinArq, E500: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    VL_OD_IPI := 0;
    VL_OC_IPI := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT IND_AJ, SUM(VL_AJ) VL_AJ  ',
    'FROM efd_e530 ',
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND AnoMes=' + Geral.FF0(QrEFD_E520AnoMes.Value),
    'AND Empresa=' + Geral.FF0(QrEFD_E520Empresa.Value),
    'AND E520=' + Geral.FF0(QrEFD_E520LinArq.Value),
    '']);
    while not Qry.Eof do
    begin
      Valor  := Qry.FieldByName('VL_AJ').AsFloat;
      IND_AJ := Qry.FieldByName('IND_AJ').AsString;
      if IND_AJ = '0' then
        VL_OD_IPI := VL_OD_IPI + Valor
      else
      if IND_AJ = '1' then
        VL_OC_IPI := VL_OC_IPI + Valor
      else
        Geral.MB_Erro('Indicador inv�lido em "AtualizaValoresE520deE530":' +
        slineBreak + '"' + IND_AJ + '" ');
      //
      Qry.Next;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT VL_SD_ANT_IPI, VL_DEB_IPI, VL_CRED_IPI  ',
    'FROM efd_e520 ',
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND AnoMes=' + Geral.FF0(QrEFD_E520AnoMes.Value),
    'AND Empresa=' + Geral.FF0(QrEFD_E520Empresa.Value),
    'AND LinArq=' + Geral.FF0(QrEFD_E520LinArq.Value),
    'AND E500=' + Geral.FF0(QrEFD_E520E500.Value),
    '']);
    VL_SD_ANT_IPI := Qry.FieldByName('VL_SD_ANT_IPI').AsFloat;
    VL_DEB_IPI    := Qry.FieldByName('VL_DEB_IPI').AsFloat;
    VL_CRED_IPI   := Qry.FieldByName('VL_CRED_IPI').AsFloat;
    //
//Campo 07 (VL_SC_IPI) - Valida��o: se a soma dos campos VL_DEB_IPI e VL_OD_IPI menos a soma dos campos
//VL_SD_ANT_IPI, VL_CRED_IPI e VL_OC_IPI for menor que �0� (zero), ent�o o campo VL_SC_IPI deve ser igual ao
//valor absoluto da express�o, e o valor do campo VL_SD_IPI deve ser igual a �0� (zero).
    VL_SC_IPI     := 0;
    VL_SD_IPI     := 0;
    Saldo :=  (VL_DEB_IPI + VL_OD_IPI) - (VL_SD_ANT_IPI + VL_CRED_IPI + VL_OC_IPI);
    if Saldo < 0 then
  (*07*)VL_SC_IPI      := - Saldo
//Campo 08 (VL_SD_IPI) - Valida��o: se a soma dos campos VL_DEB_IPI e VL_OD_IPI menos a soma dos campos
//VL_SD_ANT_IPI, VL_CRED_IPI e VL_OC_IPI for maior ou igual a �0� (zero), ent�o o campo 08 (VL_SD_IPI) deve ser
//igual ao resultado da express�o, e o valor do campo VL_SC_IPI deve ser igual a �0� (zero).
   else
  (*08*)VL_SD_IPI      := Saldo;

    //
    ImporExpor         := FImporExpor;
    AnoMes             := QrEFD_E520AnoMes.Value;
    Empresa            := QrEFD_E520Empresa.Value;
    LinArq             := QrEFD_E520LinArq.Value;
    E500               := QrEFD_E520E500.Value;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'efd_e520', False, [
    'VL_OD_IPI', 'VL_OC_IPI',
    'VL_SC_IPI', 'VL_SD_IPI'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'E500'], [
    VL_OD_IPI, VL_OC_IPI,
    VL_SC_IPI, VL_SD_IPI], [
    ImporExpor, AnoMes, Empresa, LinArq, E500], True);
  finally
    Qry.Free;
  end;
end;

procedure TFmEFD_E001.AtualizaValoresH005deH010();
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE efd_h005 ',
  'SET VL_INV=( ',
  '  SELECT SUM(VL_ITEM) ',
  '  FROM efd_h010 ',
  '  WHERE ImporExpor=' + Geral.FF0(QrEFD_E001ImporExpor.Value),
  '  AND Empresa=' + Geral.FF0(QrEFD_E001Empresa.Value),
  '  AND AnoMes=201401 ',
  '  AND H005=' + Geral.FF0(QrEFD_H005LinArq.Value),
  '  ) ',
  '  WHERE ImporExpor=' + Geral.FF0(QrEFD_E001ImporExpor.Value),
  '  AND Empresa=' + Geral.FF0(QrEFD_E001Empresa.Value),
  '  AND AnoMes=' + Geral.FF0(QrEFD_E001AnoMes.Value),
  '  AND LinArq=' + Geral.FF0(QrEFD_H005LinArq.Value),
  '']);
  //
  ReopenEFD_H005(QrEFD_H005LinArq.Value);
end;

procedure TFmEFD_E001.BitBtn1Click(Sender: TObject);
begin
  AppPF.MostraFormCfgMovEFD(QrEFD_E001AnoMes.Value);
end;

procedure TFmEFD_E001.BlocoKRegistrosK2001Click(Sender: TObject);
begin
  ReopenEFD_K200(0, rkPrint);
  //
  MyObjects.frxDefineDataSets(frxEFD_SPEDE_K200_001, [
  DModG.frxDsDono,
  frxDsEFD_K200
  ]);
  //
  MyObjects.frxMostra(frxEFD_SPEDE_K200_001, 'K200');
end;

procedure TFmEFD_E001.BlocoKRegistrosK2201Click(Sender: TObject);
begin
  //ReopenEFD_K220(0, rkPrint);
  //
  MyObjects.frxDefineDataSets(frxEFD_SPEDE_K220_001, [
  DModG.frxDsDono,
  frxDsEFD_K220,
  frxDsEFD_K270_220,
  frxDsEFD_K275_220
  ]);
  //
  MyObjects.frxMostra(frxEFD_SPEDE_K220_001, 'K220');
end;

procedure TFmEFD_E001.Bt1010Click(Sender: TObject);
begin
  MostraFormEFD_1010();
end;

procedure TFmEFD_E001.BtK280Click(Sender: TObject);
begin
  //if not PCK200.ActivePageIndex = 4 then
    PCK200.ActivePageIndex := 4;
  MyObjects.MostraPopUpDeBotao(PMK280, BtK280);
end;

procedure TFmEFD_E001.BtDifGeraClick(Sender: TObject);
begin
  VerificaBlocoK();
end;

procedure TFmEFD_E001.BtDifImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxEFD_SPEDE_K_VERIF_001, [
  DModG.frxDsDono,
  frxDsEFD_K_ConfG
  ]);
  //
  MyObjects.frxMostra(frxEFD_SPEDE_K_VERIF_001, 'Verifica bloco K');
end;

procedure TFmEFD_E001.BtE100Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PME100, BtE100);
end;

procedure TFmEFD_E001.BtE110Click(Sender: TObject);
begin
  MostraFormEFD_E110();
end;

procedure TFmEFD_E001.BtE111Click(Sender: TObject);
begin
  PCE100.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PME111, BtE111);
end;

procedure TFmEFD_E001.BtE115Click(Sender: TObject);
begin
  PCE100.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMValDecltorio, BtE115);
end;

procedure TFmEFD_E001.BtE116Click(Sender: TObject);
begin
  PCE100.ActivePageIndex := 3;
  MyObjects.MostraPopUpDeBotao(PMObrigacoes, BtE116);
end;

procedure TFmEFD_E001.BtE500Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PME500, BtE500);
end;

procedure TFmEFD_E001.BtE520Click(Sender: TObject);
begin
  MostraFormEFD_E520();
end;

procedure TFmEFD_E001.BtE530Click(Sender: TObject);
begin
  PCE500.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PME530, BtE530);
end;

procedure TFmEFD_E001.BtH005Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMH005, BtH005);
end;

procedure TFmEFD_E001.BtH010Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMH010, BtH010);
end;

procedure TFmEFD_E001.BtH020Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMH020, BtH020);
end;

procedure TFmEFD_E001.BtImprime1Click(Sender: TObject);
begin
{
  if (PageControl3.ActivePageIndex = 1) then
    MyObjects.frxMostra(frxSPEDEFD_PRINT_001_02, 'Falhas na Exporta��o')
  else
    Geral.MensagemBox('A guia selecionada n�o possui relat�rio!',
    'Informa��o', MB_OK+MB_ICONINFORMATION);
}
end;

procedure TFmEFD_E001.BtK100Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMK100, BtK100);
end;

procedure TFmEFD_E001.BtK200Click(Sender: TObject);
begin
  if not (PCK200.ActivePageIndex in ([0,4])) then
    PCK200.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMK200, BtK200);
end;

procedure TFmEFD_E001.BtK220Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMK220, BtK220);
end;

procedure TFmEFD_E001.BtK230Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 3;
  MyObjects.MostraPopUpDeBotao(PMK230, BtK230);
end;

procedure TFmEFD_E001.BtPeriodoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPeriodo, BtPeriodo);
end;

procedure TFmEFD_E001.FormCreate(Sender: TObject);
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FEmprTXT := '';
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  PCE100.ActivePageIndex := 0;
  PCEx00.ActivePageIndex := 0;
  PCK200.ActivePageIndex := 0;
  ImgTipo.SQLType := stLok;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmEFD_E001.SbNumeroClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Codigo(QrCadComItensCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEFD_E001.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmEFD_E001.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEFD_E001.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrCadComItensCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmEFD_E001.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEFD_E001.QrEFD_E001AfterOpen(DataSet: TDataSet);
begin
  BtE100.Enabled := True;
  BtE500.Enabled := True;
  BtH005.Enabled := True;
  BtK100.Enabled := True;
  Bt1010.Enabled := True;
end;

procedure TFmEFD_E001.QrEFD_E001AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_E100(0);
  ReopenEFD_E500(0);
  ReopenEFD_H005(0);
  ReopenEFD_K100(0);
  ReopenEFD_1010();
end;

procedure TFmEFD_E001.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //Geral.MB_Aviso('Raspa de divisao nao pode aparecer na reclasse de Ope/pwe!');
end;

procedure TFmEFD_E001.SbQueryClick(Sender: TObject);
begin
{
  LocCod(QrCadComItensCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cadcomitens', Dmod.MyDB, CO_VAZIO));
}
end;

procedure TFmEFD_E001.Vaiparajanelademovimento1Click(Sender: TObject);
begin
  JanelaDoMovimento();
end;

procedure TFmEFD_E001.VerificaBlocoK();
var
  Qry: TmySQLQuery;
  SQL_PeriodoVS, SQL_PeriodoPQ: String;
  Campo, ImporExpor, AnoMes, Empresa, GraGruX, REGISTRO, MovimID: Integer;
  DiaIni, DiaFim: TDateTime;
  ESTSTabSorc: TEstqSPEDTabSorc;
  //
  procedure InsereAtualVerif(GGX: Integer; QTD: Double);
  begin
    //GraGruX := Qry.FieldByName('GraGruX').AsInteger;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efd_k_confg', False, [
    'Movim' + Geral.FFN(Campo, 2)], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'GraGruX', 'REGISTRO', 'MovimID',
    'MovimXX', 'ESTSTabSorc'], [
    QTD], [
    ImporExpor, AnoMes, Empresa,
    GGX, REGISTRO, MovimID,
    Campo, Integer(ESTSTabSorc)], True);
  end;
  //
var
  QTD: Double;
  //
  procedure InsereEstoqueDoInicioDoPeriodo();
  var
    MesAnt: Integer;
  begin
    MesAnt := Geral.IncrementaMes_AnoMes(QrEFD_K100AnoMes.Value, -1);
    //
    Campo := 1;
    Registro := 200;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k200.GraGruX, SUM(k200.QTD) QTD ',
    'FROM efd_k200 k200 ',
    'WHERE k200.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k200.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k200.AnoMes=' + Geral.FF0(MesAnt),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSEntradaPorCompra();
  const
    sProcName = 'InsereVSEntradaPorCompra()';
  var
    Grandeza: Integer;
  begin
    Campo := 2;
    Registro := 0;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.GraGruX, SUM(vmi.Pecas) Pecas,',
    'SUM(vmi.AreaM2) AreaM2, SUM(PesoKg) PesoKg, ',
    'med.Grandeza  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
    SQL_PeriodoVS,
    'AND vmi.MovimID IN (1,16,21,22)',
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      Grandeza := Qry.FieldByName('Grandeza').AsInteger;
      case Grandeza of
        0: QTD := Qry.FieldByName('Pecas').AsFloat;
        1: QTD := Qry.FieldByName('AreaM2').AsFloat;
        2: QTD := Qry.FieldByName('PesoKg').AsFloat;
        else
          Geral.MB_Erro('"Grandeza" = ' + Geral.FF0(Grandeza) + ' indefinido em '
          + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      end;
      if QTD <= 0 then
        Geral.MB_Erro('"QTD" = ' + Geral.FFT(QTD, 2, siNegativo) + ' inv�lido em '
        + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InserePQEntradaPorCompra();
  const
    sProcName = 'InserePQEntradaPorCompra()';
  var
    GGX: Integer;
  begin
    Campo := 2;
    Registro := 0;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsPQx;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Insumo, SUM(Peso) Peso ',
    'FROM pqx ',
    SQL_PeriodoPQ,
    'AND Tipo=' + Geral.FF0(VAR_FATID_0010),
    'GROUP BY Insumo ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD := Qry.FieldByName('Peso').AsFloat;
      if QTD <= 0 then
        Geral.MB_Erro('"QTD" = ' + Geral.FFT(QTD, 2, siNegativo) + ' inv�lido em '
        + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak);
      GGX := UnPQx.ObtemGGXdeInsumo(Qry.FieldByName('Insumo').AsInteger);
      InsereAtualVerif(GGX, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSEntradaPorProducao();
  begin
    Campo := 3;
    Registro := 230;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k230.GraGruX, SUM(k230.QTD_ENC) QTD',
    'FROM efd_k230 k230',
    'WHERE k230.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k230.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k230.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 3;
    Registro := 250;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k250.GraGruX, SUM(k250.QTD) QTD',
    'FROM efd_k250 k250',
    'WHERE k250.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k250.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k250.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 3;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_POS) QTD ',
    'FROM efd_k270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND QTD_COR_POS>0 ',
    'AND RegisPai IN ("K230", "K235", "K250", "K255", "K260", "K265") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 3;
    Registro := 275;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k275.GraGruX, SUM(k275.QTD_COR_POS) QTD ',
    'FROM efd_k275 k275 ',
    'WHERE k275.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k275.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k275.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND QTD_COR_POS>0 ',
    'AND RegisPai IN ("K230", "K235", "K250", "K255", "K260", "K265") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //
  procedure InsereVSProdutoDeReforma();
  begin
    Campo := 12;
    Registro := 260;
    MovimID := 33;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k260.GraGruX, ',
    'SUM(k260.QTD_RET-k260.QTD_SAIDA) QTD',
    'FROM efd_k260 k260',
    'WHERE k260.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k260.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k260.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSInsumoNaReforma();
  begin
    Campo := 13;
    Registro := 265;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsPQx;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k265.GraGruX,',
    'SUM(k265.QTD_CONS-k265.QTD_RET) QTD',
    'FROM efd_k265 k265',
    'WHERE k265.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k265.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k265.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSEntradaPorDesmonte();
  begin
    Campo := 11;
    Registro := 215;
    MovimID := 11;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k215.GraGruX, SUM(k215.QTD_DES) QTD',
    'FROM efd_k215 k215',
    'WHERE k215.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k215.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k215.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 11;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_POS) QTD ',
    'FROM efd_k270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND QTD_COR_POS>0 ',
    'AND RegisPai IN ("K210", "K215") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 11;
    Registro := 275;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k275.GraGruX, SUM(k275.QTD_COR_POS) QTD ',
    'FROM efd_k275 k275 ',
    'WHERE k275.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k275.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k275.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND QTD_COR_POS>0 ',
    'AND RegisPai IN ("K210", "K215") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //
  procedure InsereVSEntradaPorClasse();
  begin
    Campo := 4;
    Registro := 220;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k220.GGXDst GraGruX, SUM(k220.QTD) QTD',
    'FROM efd_k220 k220',
    'WHERE k220.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k220.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k220.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 4;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_POS) QTD ',
    'FROM efd_k270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND QTD_COR_POS>0 ',
    'AND RegisPai IN ("K220") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 4;
    Registro := 275;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k275.GraGruX, SUM(k275.QTD_COR_POS) QTD ',
    'FROM efd_k275 k275 ',
    'WHERE k275.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k275.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k275.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND QTD_COR_POS>0 ',
    'AND RegisPai IN ("K220") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //
  procedure InsereVSSaidaParaProduzir();
  begin
    Campo := 5;
    Registro := 235;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k235.GraGruX, SUM(k235.QTD) QTD ',
    'FROM efd_k235 k235 ',
    'WHERE k235.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k235.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k235.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //


    Campo := 5;
    Registro := 255;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k255.GraGruX, SUM(k255.QTD) QTD ',
    'FROM efd_k255 k255 ',
    'WHERE k255.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k255.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k255.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 5;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_NEG) QTD ',
    'FROM efd_k270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND QTD_COR_NEG>0 ',
    'AND RegisPai IN ("K230", "K235", "K250", "K255", "K260", "K265") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 5;
    Registro := 275;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k275.GraGruX, SUM(k275.QTD_COR_NEG) QTD ',
    'FROM efd_k275 k275 ',
    'WHERE k275.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k275.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k275.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND QTD_COR_NEG>0 ',
    'AND RegisPai IN ("K230", "K235", "K250", "K255", "K260", "K265") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //
  procedure InsereVSSaidaParaDesmontar();
  begin
    Campo := 15;
    Registro := 210;
    MovimID := 11;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k210.GraGruX, SUM(k210.QTD_ORI) QTD ',
    'FROM efd_k210 k210 ',
    'WHERE k210.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k210.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k210.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 15;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_NEG) QTD ',
    'FROM efd_k270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND QTD_COR_NEG>0 ',
    'AND RegisPai IN ("K210", "K215") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 15;
    Registro := 275;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k275.GraGruX, SUM(k275.QTD_COR_NEG) QTD ',
    'FROM efd_k275 k275 ',
    'WHERE k275.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k275.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k275.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND QTD_COR_NEG>0 ',
    'AND RegisPai IN ("K210", "K215") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //
  procedure InsereVSSaidaParaClasse();
  begin
    Campo := 6;
    Registro := 220;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k220.GGXOri GraGruX, SUM(k220.QTD) QTD',
    'FROM efd_k220 k220',
    'WHERE k220.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k220.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k220.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := -Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 6;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_NEG) QTD ',
    'FROM efd_k270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND QTD_COR_NEG>0 ',
    'AND RegisPai IN ("K220") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 6;
    Registro := 275;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k275.GraGruX, SUM(k275.QTD_COR_NEG) QTD ',
    'FROM efd_k275 k275 ',
    'WHERE k275.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k275.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k275.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND QTD_COR_NEG>0 ',
    'AND RegisPai IN ("K220") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //
  procedure InsereVSSaidaPorVenda();
  const
    sProcName = 'InsereVSEntradaPorVenda()';
  var
    Grandeza: Integer;
  begin
    //fazer venda de outro GraGruX!
    Campo := 7;
    Registro := 0;
    MovimID := 2;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    //'SELECT vmi.GraGruX, SUM(vmi.Pecas) Pecas,',
    'SELECT IF(vmi.GGXRcl <> 0 AND ',
    'vmi.GGXRcl <> vmi.GraGruX, vmi.GGXRcl, vmi.GraGruX) GGX, ',
    'SUM(vmi.Pecas) Pecas,',
    'SUM(vmi.AreaM2) AreaM2, SUM(PesoKg) PesoKg, ',
    'med.Grandeza  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
    SQL_PeriodoVS,
    'AND vmi.MovimID IN (2)',
    'AND Empresa=' + Geral.FF0(FEmpresa),
    //'GROUP BY GraGruX',
    //'ORDER BY GraGruX ',
    'GROUP BY GGX',
    'ORDER BY GGX ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      Grandeza := Qry.FieldByName('Grandeza').AsInteger;
      GraGruX  := Qry.FieldByName('GGX').AsInteger;
      //
      case Grandeza of
        0: QTD := Qry.FieldByName('Pecas').AsFloat;
        1: QTD := Qry.FieldByName('AreaM2').AsFloat;
        2: QTD := Qry.FieldByName('PesoKg').AsFloat;
        else
          Geral.MB_Erro('"Grandeza" = ' + Geral.FF0(Grandeza) + ' indefinido em '
          + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      end;
      if QTD >= 0 then
        Geral.MB_Erro('"QTD" = ' + Geral.FFT(QTD, 2, siNegativo) + ' inv�lido em '
        + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      InsereAtualVerif(GraGruX, QTD);
      Qry.Next;
    end;
  end;
  //

  procedure InserePQSaidaParaETE();
  const
    sProcName = 'InserePQSaidaParaETE()';
  var
    Grandeza, GGX: Integer;
  begin
    Campo := 8;
    Registro := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsPQx;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Insumo, SUM(Peso) Peso ',
    'FROM pqx ',
    SQL_PeriodoPQ,
    'AND Tipo=150',// + Geral.FF0(VAR_FATID_0010),
    'GROUP BY Insumo ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD := Qry.FieldByName('Peso').AsFloat;
      GGX := UnPQx.ObtemGGXdeInsumo(Qry.FieldByName('Insumo').AsInteger);
      InsereAtualVerif(GGX, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InserePQBalanco();
  const
    sProcName = 'InserePQBalanco';
  var
    GGX: Integer;
  begin
    Campo := 16;
    Registro := 0;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsPQx;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _sped_efd_k2xx_o_p; ',
    'CREATE TABLE _sped_efd_k2xx_o_p ',
    ' ',
    'SELECT ggx.Controle GGX, SUM(pqx.Peso) Peso ',
    'FROM ' + TMeuDB + '.pqx pqx ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.GraGru1=pqx.Insumo ',
    ' ',
    SQL_PeriodoPQ,
    'AND Tipo<>0 ',
    'GROUP BY ggx.Controle ',
    ' ',
    'UNION ',
    ' ',
    'SELECT k200.GraGruX GGX, SUM(-k200.QTD) Peso ',
    'FROM ' + TMeuDB + '.efd_k200 k200  ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=k200.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE k200.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k200.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'AND k200.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND gg1.PrdGrupTip=-2 ',
    'GROUP BY GraGruX  ',
    ' ',
    'UNION ',
    ' ',
    'SELECT k200.GraGruX GGX, SUM(k200.QTD) Peso ',
    'FROM ' + TMeuDB + '.efd_k200 k200  ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=k200.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE k200.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k200.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value - 1),
    'AND k200.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND gg1.PrdGrupTip=-2 ',
    'GROUP BY GraGruX  ',
    ' ',
    '; ',
    ' ',
    'SELECT GGX, SUM(Peso) Peso ',
    'FROM  _sped_efd_k2xx_o_p ',
    'GROUP BY GGX ',
    'ORDER BY GGX; ',
    ' ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD := -Qry.FieldByName('Peso').AsFloat;
      GGX := Qry.FieldByName('GGX').AsInteger;
      InsereAtualVerif(GGX, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSErrosEmpresa();
  const
    sProcName = 'InsereVSErrosEmpresa()';
  var
    Grandeza: Integer;
  begin
    Campo := 17;
    Registro := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.GraGruX, vmi.MovimID, SUM(vmi.Pecas) Pecas, ',
    'SUM(vmi.AreaM2) AreaM2, SUM(PesoKg) PesoKg,  ',
    'med.Grandeza, COUNT(vmi.GraGruX) ITENS   ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX  ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed  ',
    SQL_PeriodoVS,
    'AND Empresa>-11 ', // N�o � uma empresa!
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX  ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      MovimID  := Qry.FieldByName('MovimID').AsInteger;
      Grandeza := Qry.FieldByName('Grandeza').AsInteger;
      case Grandeza of
        0: QTD := Qry.FieldByName('Pecas').AsFloat;
        1: QTD := Qry.FieldByName('AreaM2').AsFloat;
        2: QTD := Qry.FieldByName('PesoKg').AsFloat;
        else
          Geral.MB_Erro('"Grandeza" = ' + Geral.FF0(Grandeza) + ' indefinido em '
          + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      end;
(*
      if QTD <= 0 then
        Geral.MB_Erro('"QTD" = ' + Geral.FFT(QTD, 2, siNegativo) + ' inv�lido em '
        + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
*)
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSIndevidos();
  const
    sProcName = 'InsereVSIndevidos()';
  var
    Grandeza: Integer;
  begin
    Campo := 18;
    Registro := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.GraGruX, vmi.MovimID, SUM(vmi.Pecas) Pecas,',
    'SUM(vmi.AreaM2) AreaM2, SUM(PesoKg) PesoKg, ',
    'med.Grandeza  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
    SQL_PeriodoVS,
    'AND vmi.MovimID IN (0,3,4,5,9,10,12,13,17,18)',
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      MovimID  := Qry.FieldByName('MovimID').AsInteger;
      Grandeza := Qry.FieldByName('Grandeza').AsInteger;
      case Grandeza of
        0: QTD := Qry.FieldByName('Pecas').AsFloat;
        1: QTD := Qry.FieldByName('AreaM2').AsFloat;
        2: QTD := Qry.FieldByName('PesoKg').AsFloat;
        else
          Geral.MB_Erro('"Grandeza" = ' + Geral.FF0(Grandeza) + ' indefinido em '
          + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      end;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InserePQIndevidos();
  const
    sProcName = 'InserePQIndevidos)';
  var
    GGX: Integer;
  begin
    Campo := 18;
    Registro := 0;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsPQx;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _SPED_EFD_K2XX_SEM_MC;',
    'CREATE TABLE _SPED_EFD_K2XX_SEM_MC',
    ' ',
    'SELECT Insumo, SUM(Peso) Peso ',
    'FROM ' + TMeuDB + '.pqx ',
    SQL_PeriodoPQ,
    'AND Tipo = 110',
    'AND OrigemCodi IN (',
    '  SELECT Codigo',
    '  FROM ' + TMeuDB + '.emit',
    '  WHERE VSMovCod=0)',
    'GROUP BY Insumo',
    'UNION',
    '',
    'SELECT Insumo, SUM(Peso) Peso ',
    'FROM ' + TMeuDB + '.pqx ',
    SQL_PeriodoPQ,
    'AND Tipo = 190',
    'AND OrigemCodi IN (',
    '  SELECT Codigo',
    '  FROM ' + TMeuDB + '.pqo',
    '  WHERE VSMovCod=0)',
    'GROUP BY Insumo',
    ';',
    '',
    'SELECT Insumo, SUM(Peso) Peso ',
    'FROM _SPED_EFD_K2XX_SEM_MC',
    'GROUP BY Insumo;',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD := Qry.FieldByName('Peso').AsFloat;
      GGX := UnPQx.ObtemGGXdeInsumo(Qry.FieldByName('Insumo').AsInteger);
      //
      InsereAtualVerif(GGX, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSSubProduto1();
  const
    sProcName = 'InsereVSSubProduto1()';
  var
    Grandeza: Integer;
  begin
    EXIT; // !!! Duplicando!!!
    Campo := 19;
    Registro := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.GraGruX, vmi.MovimID, SUM(vmi.Pecas) Pecas,',
    'SUM(vmi.AreaM2) AreaM2, SUM(PesoKg) PesoKg, ',
    'med.Grandeza  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
    SQL_PeriodoVS,
    'AND (vmi.MovimID IN (23) ',
(*
    '  OR ',
    '    (vmi.MovimID = 11 AND MovimNiv=9 AND ',
    '     cou.CouNiv2 IN (' + CO_XXXNIV2_SUBPRD + ')) ', // Raspa ou subproduto!
*)
    ') ',
    'AND Empresa=' + Geral.FF0(FEmpresa),
    'GROUP BY GraGruX, MovimID',
    'ORDER BY GraGruX ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      MovimID  := Qry.FieldByName('MovimID').AsInteger;
      Grandeza := Qry.FieldByName('Grandeza').AsInteger;
      case Grandeza of
        0: QTD := Qry.FieldByName('Pecas').AsFloat;
        1: QTD := Qry.FieldByName('AreaM2').AsFloat;
        2: QTD := Qry.FieldByName('PesoKg').AsFloat;
        else
          Geral.MB_Erro('"Grandeza" = ' + Geral.FF0(Grandeza) + ' indefinido em '
          + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      end;
      if QTD <= 0 then
        Geral.MB_Erro('"QTD" = ' + Geral.FFT(QTD, 2, siNegativo) + ' inv�lido em '
        + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSSubProduto2();
  var
    Qtde: Double;
  begin
    Campo := 19;
    //
    Registro := 235;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k23s.GraGruX, SUM(k23s.QTD_ENC) QTD',
    'FROM efd_k23subprd k23s',
    'WHERE k23s.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k23s.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k23s.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
    //
    Registro := 255;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k25s.GraGruX, SUM(k25s.QTD) QTD',
    'FROM efd_k25subprd k25s',
    'WHERE k25s.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k25s.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k25s.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereEstoqueDoFimDoPeriodo();
  begin
    Campo := 20;
    Registro := 200;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k200.GraGruX, SUM(k200.QTD) QTD ',
    'FROM efd_k200 k200 ',
    'WHERE k200.ImporExpor=' + Geral.FF0(QrEFD_K100ImporExpor.Value),
    'AND k200.Empresa=' + Geral.FF0(QrEFD_K100Empresa.Value),
    'AND k200.AnoMes=' + Geral.FF0(QrEFD_K100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
begin
  if not UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efd_k_confg', [
  'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
  [QrEFD_E001ImporExpor.Value, QrEFD_E001AnoMes.Value,
  QrEFD_E001Empresa.Value], '') then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    DiaIni        := Geral.AnoMesToData(QrEFD_E001AnoMes.Value, 1);
    DiaFim        := IncMonth(DiaIni, 1) -1;
    SQL_PeriodoVS := dmkPF.SQL_Periodo('WHERE DataHora ', DiaIni, DiaFim, True, True);
    SQL_PeriodoPQ := dmkPF.SQL_Periodo('WHERE DataX ', DiaIni, DiaFim, True, True);
    ImporExpor    := QrEFD_E001ImporExpor.Value;
    AnoMes        := QrEFD_E001AnoMes.Value;
    Empresa       := QrEFD_E001Empresa.Value;
    Qry := TmySQLQuery.Create(Dmod);
    try
      (*01*)InsereEstoqueDoInicioDoPeriodo();
      //
      (*02*)InsereVSEntradaPorCompra();
      (*02*)InserePQEntradaPorCompra();
      (*03*)InsereVSEntradaPorProducao();
      (*04*)InsereVSEntradaPorClasse();
      (*11*)InsereVSEntradaPorDesmonte();
      //
      (*12*)InsereVSProdutoDeReforma();
      //
      (*05*)InsereVSSaidaParaProduzir();
      (*06*)InsereVSSaidaParaClasse();
      (*07*)InsereVSSaidaPorVenda();
      (*15*)InsereVSSaidaParaDesmontar();
      //
      (*13*)InsereVSInsumoNaReforma();
      //
      (*08*)InserePQSaidaParaETE();
      //
      (*16*)InserePQBalanco();
      (*17*)InsereVSErrosEmpresa();
      (*18*)InsereVSIndevidos();
      (*18*)InserePQIndevidos();
      (*19*)InsereVSSubProduto1();
      (*19*)InsereVSSubProduto2();
      //
      (*20*)InsereEstoqueDoFimDoPeriodo();
      //
      ReopenEFD_K_ConfG(0);
    finally
      Qry.Free;
    end;
  finally
    Screen.Cursor := crDefault
  end;
end;

procedure TFmEFD_E001.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEFD_E001.FormShow(Sender: TObject);
var
  Qry: TmySQLQuery;
  N_D, Nao, Sim: Integer;
begin
  // Temporario
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando Tipo_Item dos produtos grade');
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT  ',
    'SUM(IF(TIPO_ITEM < 0, 1, 0)) N_D, ',
    'SUM(IF(TIPO_ITEM < 1, 1, 0)) Nao, ',
    'SUM(IF(TIPO_ITEM > 0, 1, 0)) Sim ',
    'FROM prdgruptip ',
    'WHERE Codigo <>0 ',
    '']);
    N_D := Qry.FieldByName('N_D').AsInteger;
    Nao := Qry.FieldByName('Nao').AsInteger;
    Sim := Qry.FieldByName('Sim').AsInteger;
    if Sim = 0 then
      Dmod.MyDB.Execute(
      'UPDATE prdgruptip SET TIPO_ITEM=-1 WHERE TIPO_ITEM=0 ' +
      'AND Codigo <> 0');
    if (N_D > 0) or ((Nao > 0) and (Sim = 0)) then
    begin
      Geral.MB_Aviso('Existem ' + Geral.FF0(Nao) +
      ' tipos de produtos de grade sem defini��o de:' + sLineBreak +
      '"Tipo de item - Atividades Industriais, Comerciais e Servi�os"');
      Close;
      Exit;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Qry.Free;
  end;
  // Temporario
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando Tipo_Item dos produtos grade');
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT  ',
    'SUM(IF(TIPO_ITEM =  -1, 1, 0)) Nao, ',
    'SUM(IF(TIPO_ITEM <> -1, 1, 0)) Sim ',
    'FROM gragru2 ',
    'WHERE Nivel2<>0 ',
    '']);
    Nao := Qry.FieldByName('Nao').AsInteger;
    Sim := Qry.FieldByName('Sim').AsInteger;
    if (Nao > 0) then
    begin
      Geral.MB_Aviso('Existem ' + Geral.FF0(Nao) +
      ' "Nivel 2" de grade sem defini��o de:' + sLineBreak +
      '"Tipo de item - Atividades Industriais, Comerciais e Servi�os"' +
      sLineBreak +
      'Para alterar v� na janela de produtos de grade e clique com o bot�o contrario na grade do "N�vel 2"');
      Close;
      Exit;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Qry.Free;
  end;
end;

procedure TFmEFD_E001.frxEFD_SPEDE_K200_001GetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_SPED' then
    Value := QrEFD_E001Mes_Ano.Value
  else
  if VarName ='VARF_DATA_IMP' then
    Value := Now()
  else
  if VarName ='VARF_NoMovimCod_220' then
    Value := AppPF.ObtemNomeDeIDMovIts(QrEFD_K220MovimID.Value)
  else
  if VarName ='VARF_NoMovimCod_270' then
    Value := AppPF.ObtemNomeDeIDMovIts(QrEFD_K270_220MovimID.Value);
end;

procedure TFmEFD_E001.QrEFD_E001BeforeClose(DataSet: TDataSet);
begin
  BtE100.Enabled := False;
  BtE500.Enabled := False;
  BtH005.Enabled := False;
  BtK100.Enabled := False;
  Bt1010.Enabled := False;
  //
  QrEFD_E100.Close;
  QrEFD_E500.Close;
  QrEFD_H005.Close;
  QrEFD_K100.Close;
  QrEFD_1010.Close;
end;

procedure TFmEFD_E001.QrEFD_E100AfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrEFD_E100.RecordCount > 0;
  BtE110.Enabled := Habilita;
  BtE111.Enabled := Habilita;
  BtE115.Enabled := Habilita;
  BtE116.Enabled := Habilita;
end;

procedure TFmEFD_E001.QrEFD_E100AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_E110();
end;

procedure TFmEFD_E001.QrEFD_E100BeforeClose(DataSet: TDataSet);
begin
  BtE110.Enabled := False;
  QrEFD_E110.Close;
end;

procedure TFmEFD_E001.QrEFD_E110AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_E111(0);
  ReopenEFD_E115(0);
  ReopenEFD_E116(0);
end;

procedure TFmEFD_E001.QrEFD_E110BeforeClose(DataSet: TDataSet);
begin
  BtE110.Enabled := False;
  BtE110.Enabled := False;
  BtE116.Enabled := False;
  QrEFD_E111.Close;
  QrEFD_E115.Close;
  QrEFD_E116.Close;
end;

procedure TFmEFD_E001.QrEFD_E111AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_E112(0);
  ReopenEFD_E113(0);
end;

procedure TFmEFD_E001.QrEFD_E111BeforeClose(DataSet: TDataSet);
begin
  //BtE112.Enabled := False;
  //BtE113.Enabled := False;
  QrEFD_E112.Close;
  QrEFD_E113.Close;
end;

procedure TFmEFD_E001.QrEFD_E500AfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrEFD_E500.RecordCount > 0;
  BtE520.Enabled := Habilita;
  BtE530.Enabled := Habilita;
end;

procedure TFmEFD_E001.QrEFD_E500AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_E510(0);
  ReopenEFD_E520();
end;

procedure TFmEFD_E001.QrEFD_E500BeforeClose(DataSet: TDataSet);
begin
  //BtE510.Enabled := False;
  BtE520.Enabled := False;
  QrEFD_E510.Close;
  QrEFD_E520.Close;
end;

procedure TFmEFD_E001.QrEFD_E520AfterOpen(DataSet: TDataSet);
begin
  BtE530.Enabled := QrEFD_E520.RecordCount > 0;
end;

procedure TFmEFD_E001.QrEFD_E520AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_E530(0);
end;

procedure TFmEFD_E001.QrEFD_E520BeforeClose(DataSet: TDataSet);
begin
  BtE530.Enabled := False;
  QrEFD_E530.Close;
end;

procedure TFmEFD_E001.QrEFD_H005AfterOpen(DataSet: TDataSet);
begin
  BtH010.Enabled := QrEFD_H005.RecordCount > 0;
end;

procedure TFmEFD_E001.QrEFD_H005AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_H010(0);
end;

procedure TFmEFD_E001.QrEFD_H005BeforeClose(DataSet: TDataSet);
begin
  QrEFD_H010.Close;
  BtH010.Enabled := False;
end;

procedure TFmEFD_E001.QrEFD_H010AfterOpen(DataSet: TDataSet);
begin
  BtH020.Enabled := QrEFD_H010.RecordCount > 0;
end;

procedure TFmEFD_E001.QrEFD_H010AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_H020(0);
end;

procedure TFmEFD_E001.QrEFD_H010BeforeClose(DataSet: TDataSet);
begin
  QrEFD_H020.Close;
  BtH020.Enabled := False;
end;

procedure TFmEFD_E001.QrEFD_K100AfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrEFD_K100.RecordCount > 0;
  BtK200.Enabled := Habilita;
  BtK220.Enabled := Habilita;
  BtK230.Enabled := Habilita;
  BtK280.Enabled := Habilita;
  BtVerifica.Enabled := Habilita;
  BtDifGera.Enabled := Habilita;
  BtDifImprime.Enabled := Habilita;
end;

procedure TFmEFD_E001.QrEFD_K100AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_K200(0, rkGrade);
  ReopenEFD_K210(0);
  ReopenEFD_K220(0);
  ReopenEFD_K230(0);
  ReopenEFD_K250(0);
  ReopenEFD_K260(0);
  ReopenEFD_K270_210(0);
  ReopenEFD_K270_220(0);
  ReopenEFD_K270_230(0);
  ReopenEFD_K270_250(0);
  ReopenEFD_K270_260(0);
  ReopenEFD_K_ConfG(0);
  ReopenEFD_K280(0, rkGrade);
end;

procedure TFmEFD_E001.QrEFD_K100BeforeClose(DataSet: TDataSet);
begin
  BtK200.Enabled := False;
  BtK220.Enabled := False;
  BtK230.Enabled := False;
  BtVerifica.Enabled := False;
  BtDifGera.Enabled := False;
  BtDifImprime.Enabled := False;
  //
  QrEFD_K200.Close;
  QrEFD_K220.Close;
  QrEFD_K230.Close;
  QrEFD_K250.Close;
  QrEFD_K_ConfG.Close;
end;

procedure TFmEFD_E001.QrEFD_K210AfterScroll(DataSet: TDataSet);
begin
 ReopenEFD_K215(0);
end;

procedure TFmEFD_E001.QrEFD_K210BeforeClose(DataSet: TDataSet);
begin
  QrEFD_K215.Close;
end;

procedure TFmEFD_E001.QrEFD_K230AfterScroll(DataSet: TDataSet);
begin
 ReopenEFD_K235(0);
end;

procedure TFmEFD_E001.QrEFD_K230BeforeClose(DataSet: TDataSet);
begin
  QrEFD_K235.Close;
end;

procedure TFmEFD_E001.QrEFD_K250AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_K255(0);
end;

procedure TFmEFD_E001.QrEFD_K250BeforeClose(DataSet: TDataSet);
begin
  QrEFD_K255.Close;
end;

procedure TFmEFD_E001.QrEFD_K260AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_K265(0);
end;

procedure TFmEFD_E001.QrEFD_K260BeforeClose(DataSet: TDataSet);
begin
  QrEFD_K265.Close;
end;

procedure TFmEFD_E001.QrEFD_K270_210AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_K275_215(0);
end;

procedure TFmEFD_E001.QrEFD_K270_210BeforeClose(DataSet: TDataSet);
begin
  QrEFD_K275_215.Close;
end;

procedure TFmEFD_E001.QrEFD_K270_220AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_K275_220(0);
end;

procedure TFmEFD_E001.QrEFD_K270_220BeforeClose(DataSet: TDataSet);
begin
  QrEFD_K275_220.Close;
end;

procedure TFmEFD_E001.QrEFD_K270_230AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_K275_235(0);
end;

procedure TFmEFD_E001.QrEFD_K270_230BeforeClose(DataSet: TDataSet);
begin
  QrEFD_K275_235.Close;
end;

procedure TFmEFD_E001.QrEFD_K270_250AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_K275_255(0);
end;

procedure TFmEFD_E001.QrEFD_K270_250BeforeClose(DataSet: TDataSet);
begin
  QrEFD_K275_255.Close;
end;

procedure TFmEFD_E001.QrEFD_K270_260AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_K275_265(0);
end;

procedure TFmEFD_E001.QrEFD_K270_260BeforeClose(DataSet: TDataSet);
begin
  QrEFD_K275_255.Close;
end;

end.

