object FmSPEDEstqLoadXLS: TFmSPEDEstqLoadXLS
  Left = 339
  Top = 185
  Caption = 
    'SPE-D_STQ-004 :: Carrega Planilha Excel em Confer'#234'ncia de Estoqu' +
    'e SPED'
  ClientHeight = 535
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 487
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtAbrir: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Abrir'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtAbrirClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtSalva: TBitBtn
      Tag = 24
      Left = 121
      Top = 4
      Width = 90
      Height = 40
      Caption = 'Sal&va'
      Enabled = False
      TabOrder = 2
      OnClick = BtSalvaClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Carrega Planilha Excel em Confer'#234'ncia de Estoque SPED'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 153
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Panel8: TPanel
      Left = 0
      Top = 92
      Width = 1008
      Height = 61
      Align = alBottom
      Caption = 'Panel8'
      TabOrder = 0
      object LaAviso2: TLabel
        Left = 12
        Top = 44
        Width = 13
        Height = 13
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LaAviso1: TLabel
        Left = 12
        Top = 4
        Width = 13
        Height = 13
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PB1: TProgressBar
        Left = 12
        Top = 20
        Width = 985
        Height = 17
        TabOrder = 0
      end
    end
    object Panel9: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 92
      Align = alClient
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 39
        Height = 13
        Caption = 'Arquivo:'
      end
      object Label2: TLabel
        Left = 8
        Top = 44
        Width = 136
        Height = 13
        Caption = 'Configura'#231#227'o de importa'#231#227'o:'
      end
      object SpeedButton1: TSpeedButton
        Left = 972
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object SpeedButton2: TSpeedButton
        Left = 972
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton2Click
      end
      object EdArquivo: TEdit
        Left = 8
        Top = 20
        Width = 961
        Height = 21
        TabOrder = 0
        Text = 
          'C:\_MLArend\Clientes\Ideal - MJ Novaes\Backup\Diario Real\PQ_200' +
          '6_2008.xls'
        OnChange = EdArquivoChange
      end
      object EdXcelCfgCab: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdXcelCfgCabChange
        DBLookupComboBox = CBXcelCfgCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBXcelCfgCab: TdmkDBLookupComboBox
        Left = 64
        Top = 60
        Width = 905
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsXcelCfgCab
        TabOrder = 2
        dmkEditCB = EdXcelCfgCab
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object RGModeloPla_: TRadioGroup
        Left = 452
        Top = 16
        Width = 353
        Height = 61
        Caption = ' Modelo de planilha: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'NENHUM'
          'IDEAL 2006-2008'
          'IDEAL JUN 2011')
        TabOrder = 3
        Visible = False
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 201
    Width = 1008
    Height = 286
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 1008
      Height = 286
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet2: TTabSheet
        Caption = 'Arquivo Original'
        ImageIndex = 1
        object Grade1: TStringGrid
          Left = 0
          Top = 0
          Width = 716
          Height = 258
          Align = alClient
          ColCount = 2
          DefaultColWidth = 18
          DefaultRowHeight = 18
          RowCount = 2
          TabOrder = 0
          ExplicitHeight = 190
        end
        object Grade0: TStringGrid
          Left = 716
          Top = 0
          Width = 284
          Height = 258
          Align = alRight
          DefaultColWidth = 32
          DefaultRowHeight = 18
          RowCount = 2
          TabOrder = 1
          ExplicitHeight = 190
          ColWidths = (
            32
            32
            32
            32
            126)
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Erros e Avisos na abertura do arquivo'
        ImageIndex = 2
        object MeErros: TMemo
          Left = 0
          Top = 0
          Width = 1000
          Height = 258
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          WordWrap = False
          OnChange = MeErrosChange
        end
      end
      object TabSheet1: TTabSheet
        Caption = ' Erros e advert'#234'ncias antes de salvar'
        ImageIndex = 2
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 45
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object BtVincula: TBitBtn
            Left = 798
            Top = 2
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Vincula'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtVinculaClick
          end
          object BtConfere: TBitBtn
            Left = 890
            Top = 2
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Confere'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtConfereClick
          end
          object BtImprime2: TBitBtn
            Tag = 14
            Left = 264
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Imprimir'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 2
            OnClick = BtImprime2Click
          end
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 253
            Height = 45
            Align = alLeft
            Caption = ' Avisos de importa'#231#227'o: '
            TabOrder = 3
            object CkErros: TCheckBox
              Left = 16
              Top = 16
              Width = 97
              Height = 17
              Caption = 'Mostrar erros'
              TabOrder = 0
              OnClick = CkErrosClick
            end
            object CkAdver: TCheckBox
              Left = 128
              Top = 16
              Width = 121
              Height = 17
              Caption = 'Mostrar advert'#234'ncias'
              TabOrder = 1
              OnClick = CkErrosClick
            end
          end
        end
        object DBGrid3: TDBGrid
          Left = 0
          Top = 45
          Width = 1000
          Height = 213
          Align = alClient
          DataSource = DsErros
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Planilha'
              Width = 115
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Celula'
              Title.Caption = 'C'#233'lula'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Nome do Produto'
              Width = 209
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ERRO'
              Title.Caption = 'Erro ou advert'#234'ncia'
              Width = 596
              Visible = True
            end>
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Dados abertos a serem salvos na base de dados'
        ImageIndex = 3
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 258
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Panel4: TPanel
            Left = 1
            Top = 1
            Width = 365
            Height = 256
            Align = alLeft
            Caption = 'Panel4'
            TabOrder = 0
            object DBGrid1: TdmkDBGridDAC
              Left = 1
              Top = 29
              Width = 363
              Height = 226
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Title.Caption = 'ok'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MES_ANO'
                  Title.Caption = 'Per'#237'odo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqIniQtd'
                  Title.Caption = 'Qtd. Inicial'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqIniVal'
                  Title.Caption = 'Val. Inicial'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqFimQtd'
                  Title.Caption = 'Qtd. Final'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqFimVal'
                  Title.Caption = 'Val. Final'
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsMeses
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              EditForceNextYear = False
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Title.Caption = 'ok'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MES_ANO'
                  Title.Caption = 'Per'#237'odo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqIniQtd'
                  Title.Caption = 'Qtd. Inicial'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqIniVal'
                  Title.Caption = 'Val. Inicial'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqFimQtd'
                  Title.Caption = 'Qtd. Final'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqFimVal'
                  Title.Caption = 'Val. Final'
                  Visible = True
                end>
            end
            object Panel5: TPanel
              Left = 1
              Top = 1
              Width = 363
              Height = 28
              Align = alTop
              ParentBackground = False
              TabOrder = 1
            end
          end
          object DBGrid2: TDBGrid
            Left = 366
            Top = 1
            Width = 633
            Height = 256
            Align = alClient
            DataSource = DsMeses
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.xls'
    Title = 'Arquivo Excel (XLS)'
    Left = 516
    Top = 108
  end
  object QrXcelCfgCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrXcelCfgCabBeforeClose
    AfterScroll = QrXcelCfgCabAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM xcelcfgcab'
      'ORDER BY Nome')
    Left = 4
    Top = 4
    object QrXcelCfgCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrXcelCfgCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrXcelCfgCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrXcelCfgCabLinTit: TIntegerField
      FieldName = 'LinTit'
    end
    object QrXcelCfgCabLinIni: TIntegerField
      FieldName = 'LinIni'
    end
  end
  object DsXcelCfgCab: TDataSource
    DataSet = QrXcelCfgCab
    Left = 32
    Top = 4
  end
  object QrXcelCfgIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM XcelCfgIts'
      'WHERE Codigo=:P0'
      'ORDER BY Coluna, Controle')
    Left = 60
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrXcelCfgItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'xcelcfgits.Codigo'
    end
    object QrXcelCfgItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'xcelcfgits.Controle'
    end
    object QrXcelCfgItsColuna: TWideStringField
      FieldName = 'Coluna'
      Origin = 'xcelcfgits.Coluna'
      Size = 3
    end
    object QrXcelCfgItsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'xcelcfgits.Nome'
      Size = 50
    end
    object QrXcelCfgItsDataType: TSmallintField
      FieldName = 'DataType'
      Origin = 'xcelcfgits.DataType'
    end
    object QrXcelCfgItsCasas: TSmallintField
      FieldName = 'Casas'
      Origin = 'xcelcfgits.Casas'
    end
    object QrXcelCfgItsTabSource: TWideStringField
      FieldName = 'TabSource'
      Origin = 'xcelcfgits.TabSource'
    end
    object QrXcelCfgItsFldSource: TWideStringField
      FieldName = 'FldSource'
      Origin = 'xcelcfgits.FldSource'
      Size = 30
    end
    object QrXcelCfgItsTabDest: TWideStringField
      FieldName = 'TabDest'
      Origin = 'xcelcfgits.TabDest'
    end
    object QrXcelCfgItsFldDest: TWideStringField
      FieldName = 'FldDest'
      Origin = 'xcelcfgits.FldDest'
      Size = 30
    end
    object QrXcelCfgItsID_TabFld: TIntegerField
      FieldName = 'ID_TabFld'
      Origin = 'xcelcfgits.ID_TabFld'
    end
    object QrXcelCfgItsNO_TabFld: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TabFld'
      Size = 255
      Calculated = True
    end
    object QrXcelCfgItsNO_DataType: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DataType'
      Size = 50
      Calculated = True
    end
  end
  object QrMeses: TMySQLQuery
    SQL.Strings = (
      'SELECT '
      
        'CONCAT(RIGHT(ses.AnoMes, 2), '#39'/'#39', LEFT(LPAD(ses.AnoMes, 6, "0"),' +
        ' 4)) MES_ANO,'
      'ses.* '
      'FROM _spedestq_sum_ ses'
      'ORDER BY AnoMes')
    Left = 948
    Top = 4
    object QrMesesAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrMesesEstqIniQtd: TFloatField
      FieldName = 'EstqIniQtd'
    end
    object QrMesesEstqIniPrc: TFloatField
      FieldName = 'EstqIniPrc'
    end
    object QrMesesEstqIniVal: TFloatField
      FieldName = 'EstqIniVal'
    end
    object QrMesesComprasQtd: TFloatField
      FieldName = 'ComprasQtd'
    end
    object QrMesesComprasPrc: TFloatField
      FieldName = 'ComprasPrc'
    end
    object QrMesesComprasVal: TFloatField
      FieldName = 'ComprasVal'
    end
    object QrMesesConsumoQtd: TFloatField
      FieldName = 'ConsumoQtd'
    end
    object QrMesesConsumoPrc: TFloatField
      FieldName = 'ConsumoPrc'
    end
    object QrMesesConsumoVal: TFloatField
      FieldName = 'ConsumoVal'
    end
    object QrMesesEstqFimQtd: TFloatField
      FieldName = 'EstqFimQtd'
    end
    object QrMesesEstqFimPrc: TFloatField
      FieldName = 'EstqFimPrc'
    end
    object QrMesesEstqFimVal: TFloatField
      FieldName = 'EstqFimVal'
    end
    object QrMesesAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrMesesMES_ANO: TWideStringField
      FieldName = 'MES_ANO'
      Required = True
      Size = 7
    end
  end
  object DsMeses: TDataSource
    DataSet = QrMeses
    Left = 976
    Top = 4
  end
  object QrItens: TMySQLQuery
    SQL.Strings = (
      'SELECT COUNT(GraGruX) Itens, '
      'GraGruX, Sit_Prod, Terceiro,'
      'SUM(EstqIniQtd)  EstqIniQtd,'
      'SUM(EstqIniPrc*EstqIniQtd) / SUM(EstqIniQtd) EstqIniPrc,'
      'SUM(EstqIniVal)  EstqIniVal,'
      'SUM(ComprasQtd)  ComprasQtd,'
      'SUM(ComprasPrc*ComprasQtd) / SUM(ComprasQtd) ComprasPrc,'
      'SUM(ComprasVal)  ComprasVal,'
      'SUM(ConsumoQtd)  ConsumoQtd,'
      'SUM(ConsumoPrc*ConsumoQtd) / SUM(ConsumoQtd) ConsumoPrc,'
      'SUM(ConsumoVal)  ConsumoVal,'
      'SUM(EstqFimQtd)  EstqFimQtd,'
      'SUM(EstqFimPrc*EstqFimQtd) / SUM(EstqFimQtd) EstqFimPrc,'
      'SUM(EstqFimVal)  EstqFimVal'
      'FROM _spedestq_its_'
      'GROUP BY GraGruX, Sit_Prod, Terceiro'
      '')
    Left = 948
    Top = 32
    object QrItensItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
    object QrItensGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrItensSit_Prod: TSmallintField
      FieldName = 'Sit_Prod'
    end
    object QrItensTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrItensEstqIniQtd: TFloatField
      FieldName = 'EstqIniQtd'
    end
    object QrItensEstqIniPrc: TFloatField
      FieldName = 'EstqIniPrc'
    end
    object QrItensEstqIniVal: TFloatField
      FieldName = 'EstqIniVal'
    end
    object QrItensComprasQtd: TFloatField
      FieldName = 'ComprasQtd'
    end
    object QrItensComprasPrc: TFloatField
      FieldName = 'ComprasPrc'
    end
    object QrItensComprasVal: TFloatField
      FieldName = 'ComprasVal'
    end
    object QrItensConsumoQtd: TFloatField
      FieldName = 'ConsumoQtd'
    end
    object QrItensConsumoPrc: TFloatField
      FieldName = 'ConsumoPrc'
    end
    object QrItensConsumoVal: TFloatField
      FieldName = 'ConsumoVal'
    end
    object QrItensEstqFimQtd: TFloatField
      FieldName = 'EstqFimQtd'
    end
    object QrItensEstqFimPrc: TFloatField
      FieldName = 'EstqFimPrc'
    end
    object QrItensEstqFimVal: TFloatField
      FieldName = 'EstqFimVal'
    end
  end
  object DsItens: TDataSource
    DataSet = QrItens
    Left = 976
    Top = 32
  end
  object QrIts: TMySQLQuery
    SQL.Strings = (
      'SELECT * FROM _spedestq_its_')
    Left = 240
    Top = 84
    object QrItsLinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrItsLinPla: TIntegerField
      FieldName = 'LinPla'
    end
    object QrItsAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrItsSeqLin: TIntegerField
      FieldName = 'SeqLin'
    end
    object QrItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrItsSit_Prod: TSmallintField
      FieldName = 'Sit_Prod'
    end
    object QrItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrItsEstqIniQtd: TFloatField
      FieldName = 'EstqIniQtd'
    end
    object QrItsEstqIniPrc: TFloatField
      FieldName = 'EstqIniPrc'
    end
    object QrItsEstqIniVal: TFloatField
      FieldName = 'EstqIniVal'
    end
    object QrItsComprasQtd: TFloatField
      FieldName = 'ComprasQtd'
    end
    object QrItsComprasPrc: TFloatField
      FieldName = 'ComprasPrc'
    end
    object QrItsComprasVal: TFloatField
      FieldName = 'ComprasVal'
    end
    object QrItsConsumoQtd: TFloatField
      FieldName = 'ConsumoQtd'
    end
    object QrItsConsumoPrc: TFloatField
      FieldName = 'ConsumoPrc'
    end
    object QrItsConsumoVal: TFloatField
      FieldName = 'ConsumoVal'
    end
    object QrItsEstqFimQtd: TFloatField
      FieldName = 'EstqFimQtd'
    end
    object QrItsEstqFimPrc: TFloatField
      FieldName = 'EstqFimPrc'
    end
    object QrItsEstqFimVal: TFloatField
      FieldName = 'EstqFimVal'
    end
    object QrItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrItsPlanilha: TWideStringField
      FieldName = 'Planilha'
      Size = 50
    end
    object QrItsCelula: TWideStringField
      FieldName = 'Celula'
      Size = 30
    end
  end
  object QrErros: TMySQLQuery
    AfterOpen = QrErrosAfterOpen
    BeforeClose = QrErrosBeforeClose
    AfterScroll = QrErrosAfterScroll
    OnCalcFields = QrErrosCalcFields
    SQL.Strings = (
      'SELECT * FROM _spedestq_err_')
    Left = 268
    Top = 84
    object QrErrosLinArq: TIntegerField
      FieldName = 'LinArq'
      Origin = '_spedestq_err_.LinArq'
    end
    object QrErrosErro: TIntegerField
      FieldName = 'Erro'
      Origin = '_spedestq_err_.Erro'
    end
    object QrErrosNO_ERRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_ERRO'
      Size = 255
      Calculated = True
    end
    object QrErrosLinPla: TIntegerField
      FieldName = 'LinPla'
      Origin = '_spedestq_err_.LinPla'
    end
    object QrErrosPlanilha: TWideStringField
      FieldName = 'Planilha'
      Origin = '_spedestq_err_.Planilha'
      Size = 50
    end
    object QrErrosCelula: TWideStringField
      FieldName = 'Celula'
      Origin = '_spedestq_err_.Celula'
      Size = 30
    end
    object QrErrosNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsErros: TDataSource
    DataSet = QrErros
    Left = 296
    Top = 84
  end
  object QrAntNew: TMySQLQuery
    SQL.Strings = (
      'SELECT EstqFimQtd, EstqFimVal'
      'FROM _spedestq_its_'
      'WHERE GraGruX=:P0'
      'AND AnoMes=:P1')
    Left = 324
    Top = 84
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAntNewEstqFimQtd: TFloatField
      FieldName = 'EstqFimQtd'
    end
    object QrAntNewEstqFimVal: TFloatField
      FieldName = 'EstqFimVal'
    end
  end
  object QrAntOld: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT EstqFimQtd, EstqFimVal'
      'FROM spedestqits'
      'WHERE GraGruX=:P0'
      'AND AnoMes=:P1')
    Left = 352
    Top = 84
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAntOldEstqFimQtd: TFloatField
      FieldName = 'EstqFimQtd'
    end
    object QrAntOldEstqFimVal: TFloatField
      FieldName = 'EstqFimVal'
    end
  end
  object QrLocGGX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraGruX '
      'FROM gragruvinc'
      'WHERE Nome=:P0'
      '')
    Left = 88
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocGGXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //        '
      'end.')
    OnGetValue = frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_01GetValue
    Left = 296
    Top = 112
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsErros
        DataSetName = 'frxDsErros'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 83.149652680000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1009.134510000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'AVISOS DE IMPORTA'#199#195'O DE ESTOQUE DE PLANILHA EXCEL')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315033860000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LISTA DE [VAR_TIPO_REL]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 60.472480000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#233'lula')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Top = 60.472480000000000000
          Width = 483.779796060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 158.740167240000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Planilha')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007876460000000000
        Top = 313.700990000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 11.338582680000000000
        ParentFont = False
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDsErros
        DataSetName = 'frxDsErros'
        RowCount = 0
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataField = 'Celula'
          DataSet = frxDsErros
          DataSetName = 'frxDsErros'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErros."Celula"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Width = 483.779796060000000000
          Height = 11.338582680000000000
          DataField = 'Nome'
          DataSet = frxDsErros
          DataSetName = 'frxDsErros'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErros."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Width = 158.740167240000000000
          Height = 11.338582680000000000
          DataField = 'Planilha'
          DataSet = frxDsErros
          DataSetName = 'frxDsErros'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErros."Planilha"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PGT: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = []
        Height = 18.897637800000000000
        ParentFont = False
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsErros."Erro"'
        object frxDsSMIC2NO_PGT: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 593.385697400000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsErros."NO_ERRO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 86.929190000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de aviso: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PRD: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 604.724609610000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total: [frxDsErros."NO_ERRO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590409610000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[COUNT(MasterData1,0)] itens')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsErros: TfrxDBDataset
    UserName = 'frxDsErros'
    CloseDataSource = False
    DataSet = QrErros
    BCDToCurrency = False
    DataSetOptions = []
    Left = 324
    Top = 112
  end
  object QrErr2: TMySQLQuery
    BeforeClose = QrErr2BeforeClose
    AfterScroll = QrErr2AfterScroll
    OnCalcFields = QrErr2CalcFields
    SQL.Strings = (
      'SELECT DISTINCT Erro, Reduzido, Nome'
      'FROM _spedestq_err_'
      'WHERE Erro >0'
      'ORDER BY Erro DESC, Reduzido, Nome')
    Left = 296
    Top = 140
    object QrErr2Erro: TIntegerField
      FieldName = 'Erro'
    end
    object QrErr2NO_ERRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_ERRO'
      Size = 255
      Calculated = True
    end
    object QrErr2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrErr2GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrErr2Its: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM _spedestq_err_'
      'WHERE Erro=:P0'
      'AND Nome=:P1'
      'ORDER BY LinArq, Celula')
    Left = 296
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrErr2ItsLinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrErr2ItsErro: TIntegerField
      FieldName = 'Erro'
    end
    object QrErr2ItsLinPla: TIntegerField
      FieldName = 'LinPla'
    end
    object QrErr2ItsPlanilha: TWideStringField
      FieldName = 'Planilha'
      Size = 50
    end
    object QrErr2ItsCelula: TWideStringField
      FieldName = 'Celula'
      Size = 30
    end
    object QrErr2ItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object PMImprime2: TPopupMenu
    Left = 292
    Top = 252
    object Listadeavisos1: TMenuItem
      Caption = 'Lista geral de avisos'
      OnClick = Listadeavisos1Click
    end
    object Agruparportiponomedoproduto1: TMenuItem
      Caption = 'Agrupar por tipo / nome do produto'
      OnClick = Agruparportiponomedoproduto1Click
    end
    object Apenaslistarprodutos1: TMenuItem
      Caption = 'Apenas listar produtos'
      OnClick = Apenaslistarprodutos1Click
    end
  end
  object frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_02: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //        '
      'end.')
    OnGetValue = frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_01GetValue
    Left = 352
    Top = 140
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsErr2
        DataSetName = 'frxDsErr2'
      end
      item
        DataSet = frxDsErr2Its
        DataSetName = 'frxDsErr2Its'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 71.811062680000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1009.134510000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'AVISOS DE IMPORTA'#199#195'O DE ESTOQUE DE PLANILHA EXCEL')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315033860000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_TIPO_REL] POR PRODUTO')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 60.472480000000000000
          Width = 604.724756060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Produto')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 75.590556060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007876460000000000
        Top = 347.716760000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 22.677172680000000000
        ParentFont = False
        Top = 192.756030000000000000
        Width = 680.315400000000000000
        DataSet = frxDsErr2
        DataSetName = 'frxDsErr2'
        RowCount = 0
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Top = 11.338590000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#233'lula')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 11.338590000000000000
          Width = 158.740167240000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Planilha')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 604.724756060000000000
          Height = 11.338582680000000000
          DataField = 'Nome'
          DataSet = frxDsErr2
          DataSetName = 'frxDsErr2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErr2."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 75.590556060000000000
          Height = 11.338582680000000000
          DataField = 'GraGruX'
          DataSet = frxDsErr2
          DataSetName = 'frxDsErr2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErr2."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PGT: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = []
        Height = 18.897637800000000000
        ParentFont = False
        Top = 151.181200000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsErr2."Erro"'
        object frxDsSMIC2NO_PGT: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 593.385697400000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsErr2."NO_ERRO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Width = 86.929190000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Tipo de aviso: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PRD: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 272.126160000000000000
        Width = 680.315400000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315209610000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'Total: [COUNT(DetailData1,0)] itens em [COUNT(MasterData1,0)] no' +
              'mes de produtos diferentes.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338582680000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        DataSet = frxDsErr2Its
        DataSetName = 'frxDsErr2Its'
        RowCount = 0
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataField = 'Celula'
          DataSet = frxDsErr2Its
          DataSetName = 'frxDsErr2Its'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErr2Its."Celula"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 158.740167240000000000
          Height = 11.338582680000000000
          DataField = 'Planilha'
          DataSet = frxDsErr2Its
          DataSetName = 'frxDsErr2Its'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErr2Its."Planilha"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsErr2: TfrxDBDataset
    UserName = 'frxDsErr2'
    CloseDataSource = False
    DataSet = QrErr2
    BCDToCurrency = False
    DataSetOptions = []
    Left = 324
    Top = 140
  end
  object frxDsErr2Its: TfrxDBDataset
    UserName = 'frxDsErr2Its'
    CloseDataSource = False
    DataSet = QrErr2Its
    BCDToCurrency = False
    DataSetOptions = []
    Left = 324
    Top = 168
  end
  object frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_03: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //        '
      'end.')
    OnGetValue = frxSPEDEFD_LOAD_ESTQ_XLS_PRINT_002_01GetValue
    Left = 552
    Top = 156
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProds
        DataSetName = 'frxDsProds'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 71.811062680000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1009.134510000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'IMPORTA'#199#195'O DE ESTOQUE DE PLANILHA EXCEL')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315033860000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LISTA DE PRODUTOS')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 60.472480000000000000
          Width = 566.929456060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Produto')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 60.472480000000000000
          Width = 75.590556060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 37.795256060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007876460000000000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 11.338582680000000000
        ParentFont = False
        Top = 151.181200000000000000
        Width = 680.315400000000000000
        DataSet = frxDsProds
        DataSetName = 'frxDsProds'
        RowCount = 0
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 566.929456060000000000
          Height = 11.338582680000000000
          DataField = 'Nome'
          DataSet = frxDsProds
          DataSetName = 'frxDsProds'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProds."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 75.590556060000000000
          Height = 11.338582680000000000
          DataField = 'GraGruX'
          DataSet = frxDsProds
          DataSetName = 'frxDsProds'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProds."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795256060000000000
          Height = 11.338582680000000000
          DataField = 'SEQ'
          DataSet = frxDsProds
          DataSetName = 'frxDsProds'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProds."SEQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrProds: TMySQLQuery
    OnCalcFields = QrProdsCalcFields
    SQL.Strings = (
      'SELECT DISTINCT GraGruX, Nome'
      'FROM _spedestq_its_ '
      'ORDER BY NOME, GraGruX')
    Left = 496
    Top = 156
    object QrProdsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrProdsNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrProdsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object frxDsProds: TfrxDBDataset
    UserName = 'frxDsProds'
    CloseDataSource = False
    DataSet = QrProds
    BCDToCurrency = False
    DataSetOptions = []
    Left = 524
    Top = 156
  end
end
