unit UnSPED_Create;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnMLAGeral,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTable = (
    ntrttSPEDEFD_0190, ntrttSPEDEFD_0200, ntrttSPEDEFD_0400, ntrttSPEDEFD_0500,
    ntrttSpedEfdIcmsIpi_0150, ntrttSpedEfdIcmsIpi_0190, ntrttSpedEfdIcmsIpi_0200,
    ntrttSpedEfdIcmsIpi_K200, ntrttSpedEfdIcmsIpi_K280,
    ntrttSPEDEFD_IMECs);
  TAcaoCreate = (acDrop, acCreate, acFind);
  TSPED_Create = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrttSPEDEFD_0190(Qry: TmySQLQuery);
    procedure Cria_ntrttSPEDEFD_0200(Qry: TmySQLQuery);
    procedure Cria_ntrttSPEDEFD_0400(Qry: TmySQLQuery);
    procedure Cria_ntrttSPEDEFD_0500(Qry: TmySQLQuery);

    //
    procedure Cria_ntrttSPEDEFD_IMECs(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttSpedEfdIcmsIpi_0150(Qry: TmySQLQuery);
    procedure Cria_ntrttSpedEfdIcmsIpi_0190(Qry: TmySQLQuery);
    procedure Cria_ntrttSpedEfdIcmsIpi_0200(Qry: TmySQLQuery);
    procedure Cria_ntrttSpedEfdIcmsIpi_K200(Qry: TmySQLQuery);
    procedure Cria_ntrttSpedEfdIcmsIpi_K280(Qry: TmySQLQuery);
  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  SPED_Create: TSPED_Create;

implementation

uses UnMyObjects, Module;


{ TCreateSPED }

procedure TSPED_Create.Cria_ntrttSpedEfdIcmsIpi_0150(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  REG                            varchar(4)                                 ,');
  Qry.SQL.Add('  COD_PART                       varchar(60)                                ,');
  Qry.SQL.Add('  NOME                           varchar(100)                               ,');
  Qry.SQL.Add('  COD_PAIS                       int(5)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CNPJ                           varchar(14)                                ,');
  Qry.SQL.Add('  CPF                            varchar(11)                                ,');
  Qry.SQL.Add('  IE                             varchar(14)                                ,');
  Qry.SQL.Add('  COD_MUN                        int(7)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SUFRAMA                        varchar(9)                                 ,');
  Qry.SQL.Add('  END                            varchar(60)                                ,');
  Qry.SQL.Add('  NUM                            varchar(10)                                ,');
  Qry.SQL.Add('  COMPL                          varchar(60)                                ,');
  Qry.SQL.Add('  BAIRRO                         varchar(60)                                ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TSPED_Create.Cria_ntrttSpedEfdIcmsIpi_0190(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  REG                            varchar(4)                                 ,');
  Qry.SQL.Add('  UNID                           varchar(6)                                 ,');
  Qry.SQL.Add('  DESCR                          varchar(255)                               ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TSPED_Create.Cria_ntrttSpedEfdIcmsIpi_0200(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  REG                            varchar(4)                                 ,');
  Qry.SQL.Add('  COD_ITEM                       varchar(60)                                ,');
  Qry.SQL.Add('  DESCR_ITEM                     varchar(255)                               ,');
  Qry.SQL.Add('  COD_BARRA                      varchar(255)                               ,');
  Qry.SQL.Add('  COD_ANT_ITEM                   varchar(60)                                ,');
  Qry.SQL.Add('  UNID_INV                       varchar(6)                                 ,');
  Qry.SQL.Add('  TIPO_ITEM                      tinyint(2)                                 ,');
  Qry.SQL.Add('  COD_NCM                        varchar(8)                                 ,');
  Qry.SQL.Add('  EX_IPI                         char(3)                                    ,');
  Qry.SQL.Add('  COD_GEN                        tinyint(2)                                 ,');
  Qry.SQL.Add('  COD_LST                        varchar(5)                                 ,');
  Qry.SQL.Add('  ALIQ_ICMS                      double(5,2)                                ,');
  Qry.SQL.Add('  CEST                           int(7)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TSPED_Create.Cria_ntrttSpedEfdIcmsIpi_K200(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  REG                            varchar(4)                                 ,');
  Qry.SQL.Add('  DT_EST                         date                                       ,');
  Qry.SQL.Add('  COD_ITEM                       varchar(60)                                ,');
  Qry.SQL.Add('  QTD                            double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  IND_EST                        char(1)                                    ,');
  Qry.SQL.Add('  COD_PART                       varchar(60)                                ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TSPED_Create.Cria_ntrttSpedEfdIcmsIpi_K280(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  REG                            varchar(4)                                 ,');
  Qry.SQL.Add('  DT_EST                         date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  COD_ITEM                       varchar(60)                                ,');
  Qry.SQL.Add('  QTD_COR_POS                    double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QTD_COR_NEG                    double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  IND_EST                        char(1)                                    ,');
  Qry.SQL.Add('  COD_PART                       varchar(60)                                ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TSPED_Create.Cria_ntrttSPEDEFD_0190(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  NoUnidMed            varchar(6)                                 ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                       ');
  //Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
  //Qry.SQL.Add('  PRIMARY KEY (GraGruX)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TSPED_Create.Cria_ntrttSPEDEFD_0200(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  GraGruX              int(11)      NOT NULL                      ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                       ');
  //Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
  //Qry.SQL.Add('  PRIMARY KEY (GraGruX)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TSPED_Create.Cria_ntrttSPEDEFD_0400(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  FisRegCad            int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  ide_natOp            varchar(511)                               ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                       ');
  //Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
  //Qry.SQL.Add('  PRIMARY KEY (GraGruX)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TSPED_Create.Cria_ntrttSPEDEFD_0500(Qry: TmySQLQuery);
begin
    Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  FinContab                      tinyint(3)   NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  DtaSPED                        date                                       ,');
    Qry.SQL.Add('  Nome                           varchar(60)  NOT NULL  DEFAULT "Nulo"      ,');
    Qry.SQL.Add('  SPED_COD_CTA_REF               varchar(60)                                ,');
    Qry.SQL.Add('  DataCad_pl                     date                                       ,');
    Qry.SQL.Add('  DataAlt_pl                     date                                       ,');
    Qry.SQL.Add('  DataCad_cj                     date                                       ,');
    Qry.SQL.Add('  DataAlt_cj                     date                                       ,');
    Qry.SQL.Add('  DataCad_gr                     date                                       ,');
    Qry.SQL.Add('  DataAlt_gr                     date                                       ,');
    Qry.SQL.Add('  DataCad_sg                     date                                       ,');
    Qry.SQL.Add('  DataAlt_sg                     date                                       ,');
    Qry.SQL.Add('  DataCad_co                     date                                       ,');
    Qry.SQL.Add('  DataAlt_co                     date                                       ,');
    Qry.SQL.Add('  Ordens                         varchar(59)                                ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                       ');
  //Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
  //Qry.SQL.Add('  PRIMARY KEY (GraGruX)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TSPED_Create.Cria_ntrttSPEDEFD_IMECs(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  MovCodPai            int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  MovimCod             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  MovimID              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  JmpMovID             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  JmpNivel1            int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  JmpNivel2            int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SrcMovID             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SrcNivel1            int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  SrcNivel2            int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  AnoCA                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  MesCA                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  TipoTab              varchar(15)  NOT NULL                      ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                       ');
  //Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
  //Qry.SQL.Add('  PRIMARY KEY (GraGruX)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

function TSPED_Create.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
  Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes: Integer;
  NomeTab: String): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      //
      ntrttSPEDEFD_0190:     Nome := Lowercase('_SPEDEFD_0190_');
      ntrttSPEDEFD_0200:     Nome := Lowercase('_SPEDEFD_0200_');
      ntrttSPEDEFD_0400:     Nome := Lowercase('_SPEDEFD_0400_');
      ntrttSPEDEFD_0500:     Nome := Lowercase('_SPEDEFD_0500_');
      //
      ntrttSPEDEFD_IMECs:    Nome := Lowercase('_SEII_IMECs_');
      //
      ntrttSpedEfdIcmsIpi_0150:     Nome := Lowercase('_SpedEfdIcmsIpi_0150_');
      ntrttSpedEfdIcmsIpi_0190:     Nome := Lowercase('_SpedEfdIcmsIpi_0190_');
      ntrttSpedEfdIcmsIpi_0200:     Nome := Lowercase('_SpedEfdIcmsIpi_0200_');
      ntrttSpedEfdIcmsIpi_K200:     Nome := Lowercase('_SpedEfdIcmsIpi_K200_');
      ntrttSpedEfdIcmsIpi_K280:     Nome := Lowercase('_SpedEfdIcmsIpi_K280_');
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MensagemBox(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)',
    'Erro', MB_OK+MB_ICONERROR);
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrttSPEDEFD_0190:        Cria_ntrttSPEDEFD_0190(Qry);
    ntrttSPEDEFD_0200:        Cria_ntrttSPEDEFD_0200(Qry);
    ntrttSPEDEFD_0400:        Cria_ntrttSPEDEFD_0400(Qry);
    ntrttSPEDEFD_0500:        Cria_ntrttSPEDEFD_0500(Qry);
    //
    ntrttSPEDEFD_IMECs:       Cria_ntrttSPEDEFD_IMECs(Qry);
    //
    ntrttSpedEfdIcmsIpi_0150: Cria_ntrttSpedEfdIcmsIpi_0150(Qry);
    ntrttSpedEfdIcmsIpi_0190: Cria_ntrttSpedEfdIcmsIpi_0190(Qry);
    ntrttSpedEfdIcmsIpi_0200: Cria_ntrttSpedEfdIcmsIpi_0200(Qry);
    ntrttSpedEfdIcmsIpi_K200: Cria_ntrttSpedEfdIcmsIpi_K200(Qry);
    ntrttSpedEfdIcmsIpi_K280: Cria_ntrttSpedEfdIcmsIpi_K280(Qry);
    //
    //
    else Geral.MensagemBox('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!', 'Erro', MB_OK+MB_ICONERROR);
  end;
  Result := TabNo;
end;

end.
