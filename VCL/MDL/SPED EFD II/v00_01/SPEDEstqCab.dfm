object FmSPEDEstqCab: TFmSPEDEstqCab
  Left = 339
  Top = 185
  Caption = 'SPE-D_STQ-002 :: Cadastra Per'#237'odo Confer'#234'ncia Estoque SPED'
  ClientHeight = 154
  ClientWidth = 704
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 106
    Width = 704
    Height = 48
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 110
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 592
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 704
    Height = 48
    Align = alTop
    Caption = 'Cadastra Per'#237'odo Confer'#234'ncia Estoque SPED'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 702
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 704
    Height = 58
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 4
      Width = 23
      Height = 13
      Caption = 'Filial:'
    end
    object LaMes: TLabel
      Left = 640
      Top = 4
      Width = 23
      Height = 13
      Caption = 'M'#234's:'
    end
    object EdEmpresa: TdmkEditCB
      Left = 8
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdEmpresaChange
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 64
      Top = 20
      Width = 573
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 1
      dmkEditCB = EdEmpresa
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdMes: TdmkEdit
      Left = 640
      Top = 20
      Width = 54
      Height = 21
      Alignment = taCenter
      TabOrder = 2
      FormatType = dmktfMesAno
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfLong
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = True
      PermiteNulo = False
      ValueVariant = Null
      OnExit = EdMesExit
    end
  end
end
