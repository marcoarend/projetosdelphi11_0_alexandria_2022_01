unit EFD_RegObrig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkStringGrid, dmkEdit;

type
  TFmEFD_RegObrig = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Panel6: TPanel;
    GroupBox1: TGroupBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Grade1: TdmkStringGrid;
    TabSheet2: TTabSheet;
    Memo1: TMemo;
    Label27: TLabel;
    EdCaminho: TdmkEdit;
    SpeedButton8: TSpeedButton;
    PB1: TProgressBar;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    MeAvisos: TMemo;
    SGCampos: TStringGrid;
    SGBlocos: TStringGrid;
    SbCarrega: TSpeedButton;
    BtCarrega: TBitBtn;
    BtInsBlcs: TBitBtn;
    BtInsFlds: TBitBtn;
    RGTabela: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtInsBlcsClick(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure BtInsFldsClick(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure RGTabelaClick(Sender: TObject);
  private
    { Private declarations }
    FNiveis: array[0..32] of String;
    //
    function  ArrumaTexto(texto: String): String;
    procedure LiberaInclusoes();
  public
    { Public declarations }
    procedure CarregaXLS();
  end;

  var
  FmEFD_RegObrig: TFmEFD_RegObrig;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral, UnDmkProcFunc;

{$R *.DFM}

function TFmEFD_RegObrig.ArrumaTexto(texto: String): String;
var
  i: Integer;
  Res: String;
begin
  Result := '';
  for i := 1 to Length(Texto) do
  begin
    if (CharInSet(Texto[i], (['a'..'z']))) then
     Result := Result + Texto[i]
    else
    if (CharInSet(Texto[i], (['A'..'Z']))) then
      Result := Result + Texto[i]
    else
    if (CharInSet(Texto[i], (['0'..'9']))) then
      Result := Result + Texto[i]
    else if Texto[i] = '_' then Result := Result + '_' // copia
    else if Texto[i] = '�' then Result := Result + 'c'
    else if Texto[i] = '�' then Result := Result + 'a'
    else if Texto[i] = '�' then Result := Result + 'a'
    else if Texto[i] = '�' then Result := Result + 'a'
    else if Texto[i] = '�' then Result := Result + 'a'
    else if Texto[i] = '�' then Result := Result + 'e'
    else if Texto[i] = '�' then Result := Result + 'e'
    else if Texto[i] = '�' then Result := Result + 'i'
    else if Texto[i] = '�' then Result := Result + 'o'
    else if Texto[i] = '�' then Result := Result + 'o'
    else if Texto[i] = '�' then Result := Result + 'o'
    else if Texto[i] = '�' then Result := Result + 'u'
    else if Texto[i] = '�' then Result := Result + 'u'
    else if Texto[i] = '�' then Result := Result + 'C'
    else if Texto[i] = '�' then Result := Result + 'A'
    else if Texto[i] = '�' then Result := Result + 'A'
    else if Texto[i] = '�' then Result := Result + 'A'
    else if Texto[i] = '�' then Result := Result + 'A'
    else if Texto[i] = '�' then Result := Result + 'E'
    else if Texto[i] = '�' then Result := Result + 'E'
    else if Texto[i] = '�' then Result := Result + 'I'
    else if Texto[i] = '�' then Result := Result + 'O'
    else if Texto[i] = '�' then Result := Result + 'O'
    else if Texto[i] = '�' then Result := Result + 'O'
    else if Texto[i] = '�' then Result := Result + 'U'
    else if Texto[i] = '�' then Result := Result + 'U'
    else ; // nada
  end;
end;

procedure TFmEFD_RegObrig.BtCarregaClick(Sender: TObject);
var
  I: Integer;
  Arquivo, Linha, UF, Versoes, URL: String;
  Versao: Double;
  lstArq: TStringList;
begin
  Memo1.Lines.Clear;
  MeAvisos.Lines.Clear;
  if pos('http', LowerCase(EdCaminho.ValueVariant)) > 0 then
  begin
    Geral.MB_Info('Voc� precisa selecionar um arquivo um arquivo para abr�-lo!');
    Exit;
  end;
  //
  if MyObjects.Xls_To_StringGrid(Grade1, EdCaminho.Text, PB1, LaAviso1, LaAviso2, 1) then
  begin
    //BtCarrega.Enabled := True;
    CarregaXLS();
    MyObjects.LarguraAutomaticaGrade(SGCampos);
    PageControl1.ActivePageIndex := 1;
    //
    //BtSalvar.Visible := True;
  end;
  LiberaInclusoes();
end;

procedure TFmEFD_RegObrig.BtInsBlcsClick(Sender: TObject);
var
  Bloco, Registro, RegisPai, Nivel: String;
  I, OcorAciNiv, OcorEstNiv, Implementd, Ordem: Integer;
  SQLType: TSQLType;
  //
  XUPN: String;
begin
  Screen.Cursor := crHourGlass;
  try
    for I := 1 to SGBlocos.RowCount -1 do
    begin
      Bloco          := SGBlocos.Cells[01, I];
      Registro       := SGBlocos.Cells[02, I];
      Nivel          := SGBlocos.Cells[03, I];
      xUPN           := SGBlocos.Cells[04, I];
      RegisPai       := SGBlocos.Cells[05, I];
      OcorAciNiv     := 0;
      OcorEstNiv     := 0;
      Implementd     := 0;
      Ordem          := 0;
      //
      xUPN := Uppercase(xUPN);
      if (xUPN = '1:1')
      or (xUPN = '1') then
        OcorEstNiv := 1
      else
      if (xUPN = '1:N')
      or (xUPN = '0') then
        OcorEstNiv := 0
      else
        Geral.MB_Aviso('N�vel inv�lido:' + xUPN);
      //
      //if
      UMyMod.SQLReplace(DModG.QrAllUpd, 'spedefdpiscofinsblcs', [
      'Nivel', 'RegisPai', 'OcorAciNiv',
      'OcorEstNiv', 'Implementd', 'Ordem'], [
      'Bloco', 'Registro'], [
      Nivel, RegisPai, OcorAciNiv,
      OcorEstNiv, Implementd, Ordem], [
      Bloco, Registro], False);
      //
      //
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Registro inserindo: "' +
      Bloco + '.' + Registro);
      //
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Registros inseridos!');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEFD_RegObrig.BtInsFldsClick(Sender: TObject);
var
  Bloco, Registro, Campo, Tipo, CObrig, EObrig, SObrig, DescrLin1, DescrLin2,
  DescrLin3, DescrLin4, DescrLin5, DescrLin6, DescrLin7, DescrLin8: String;
  Numero, VersaoIni, VersaoFim, Tam, TObrig, Decimais: String;
  //
  I: Integer;
  xTam: String;
begin
  Screen.Cursor := crHourGlass;
  try
    for I := 1 to SGCampos.RowCount -1 do
    begin
      Bloco          := SGCampos.Cells[01, I];
      Registro       := SGCampos.Cells[02, I];
      Numero         := SGCampos.Cells[03, I];
      VersaoIni      := '0';
      VersaoFim      := '0';
      Campo          := SGCampos.Cells[04, I];
      Tipo           := SGCampos.Cells[06, I];
      xTam           := SGCampos.Cells[07, I];
      TObrig         := '0';
      Decimais       := SGCampos.Cells[08, I];
      CObrig         := SGCampos.Cells[09, I];
      EObrig         := SGCampos.Cells[10, I];
      SObrig         := SGCampos.Cells[11, I];
      DescrLin1      := SGCampos.Cells[05, I];
      DescrLin2      := SGCampos.Cells[12, I];
      DescrLin3      := SGCampos.Cells[13, I];
      DescrLin4      := SGCampos.Cells[14, I];
      DescrLin5      := SGCampos.Cells[15, I];
      DescrLin6      := SGCampos.Cells[16, I];
      DescrLin7      := SGCampos.Cells[17, I];
      DescrLin8      := SGCampos.Cells[18, I];
      //
      if pos('*', xTam) > 0 then
        TObrig := '1';
      Tam := '0' + Geral.SoNumero_TT(xTam);
      //
      Decimais := Geral.SoNumero_TT(Decimais);
      if Decimais = '' then
        Decimais := '0';
      //
       UMyMod.SQLReplace(DModG.QrAllUpd, 'spedefdpiscofinsflds', [
      'Campo', 'Tipo', 'Tam',
      'TObrig', 'Decimais', 'CObrig',
      'EObrig', 'SObrig', 'DescrLin1',
      'DescrLin2', 'DescrLin3', 'DescrLin4',
      'DescrLin5', 'DescrLin6', 'DescrLin7',
      'DescrLin8'], [
      'Bloco', 'Registro', 'Numero', 'VersaoIni', 'VersaoFim'], [
      Campo, Tipo, Tam,
      TObrig, Decimais, CObrig,
      EObrig, SObrig, DescrLin1,
      DescrLin2, DescrLin3, DescrLin4,
      DescrLin5, DescrLin6, DescrLin7,
      DescrLin8], [
      Bloco, Registro, Numero, VersaoIni, VersaoFim], False);
      //
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Campo inserido: "' +
      Bloco + '.' + Registro + ' - ' + Campo);
      //
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Registros inseridos!');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEFD_RegObrig.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_RegObrig.CarregaXLS();
var
  I, N, B: Integer;
  Bloco, Registro, Numero, VersaoIni, VersaoFim, Descri, Campo, Tipo, Tam,
  Decimais, CObrig, Resto, xN, xC, xB, xR, xNiv, xUpn, Nivel, UmPraN,
  OcorrT,
  PerfA_ObrigE, ObrigT, PerfA_ObrigS, PerfB_ObrigE, PerfB_ObrigS, PerfC_ObrigE,
  PerfC_ObrigS: String;
  SeqDescr, ColS, iNiv: Integer;
begin
  SGBlocos.RowCount := 2;
  SGBlocos.ColCount := 20;
  //
  SGCampos.RowCount := 2;
  SGCampos.ColCount := Grade1.ColCount + 8;
  for I := 0 to Grade1.Colcount - 1 do
    SGCampos.ColWidths[I] := Grade1.ColWidths[I];
  B := 0;
  N := 0;
  //
(*
  Descri := '';
  Nivel  := '';
  OcorrT := '';
*)
  for I := 1 to Grade1.RowCount -1 do
  begin
    xB           := Trim(Grade1.Cells[01, I]);
    xR           := Trim(Grade1.Cells[02, I]);
    xNiv         := Trim(Grade1.Cells[03, I]);
    xUPN         := Trim(Grade1.Cells[04, I]);
    xN           := Trim(Grade1.Cells[05, I]);
    xC           := Trim(Grade1.Cells[06, I]);
    //
    //
    if (xB <> EmptyStr) and (xR <> EmptyStr) then
    begin
      Bloco    := xB;
      Registro := xR;
      Nivel    := xNiv;
      UmPraN   := xUPN;
      //
      iNiv := Geral.IMV(xNiv);
      //
      B := B + 1;
      //
      SGBlocos.RowCount := B + 1;
      SGBlocos.Cells[00, B] := Grade1.Cells[00, B];
      //
      SGBlocos.Cells[01, B] := Bloco;
      SGBlocos.Cells[02, B] := Registro;
      SGBlocos.Cells[03, B] := xNiv;
      SGBlocos.Cells[04, B] := xUPN;
      if iNiv > 0 then
        SGBlocos.Cells[05, B] := FNiveis[iNiv-1]
      else
        SGBlocos.Cells[05, B] := '';
      //
      FNiveis[iNiv] := Registro;
    end;
    if (xN = 'N�') or (xC = 'Campo') then
    begin
      MeAvisos.Lines.Add('Linha ' + Geral.FF0(I + 1) + ' Numero = "N�" ou Campo = "Campo"');
    end
    else
    begin
      //Bloco        := Trim(Grade1.Cells[01, I]);
      //Registro     := Trim(Grade1.Cells[02, I]);
      //Nivel        := Trim(Grade1.Cells[03, I]);
      //UmPraN       := Trim(Grade1.Cells[04, I]);
      Numero       := Trim(Grade1.Cells[05, I]);
      Campo        := Trim(Grade1.Cells[06, I]);
      Descri       := Trim(Grade1.Cells[07, I]);
      Tipo         := Trim(Grade1.Cells[08, I]);
      Tam          := Trim(Grade1.Cells[09, I]);
      Decimais     := Trim(Grade1.Cells[10, I]);
      CObrig       := Trim(Grade1.Cells[11, I]);
      //
      //Campo := StringReplace(Campo, ' ', '', [rfReplaceAll, rfIgnoreCase]);
      Campo := ArrumaTexto(Campo);
      //
      Descri := StringReplace(Descri, '�', '-', [rfReplaceAll, rfIgnoreCase]);
      while pos('  ', Descri) > 0 do
        Descri := StringReplace(Descri, ' ', '', [rfReplaceAll, rfIgnoreCase]);
        //
      //� -
      Resto := Numero + Campo + Tipo + Tam + Decimais + CObrig;
      if (Bloco <> '') and (Registro <> '') then
      begin
        if (Resto = EmptyStr) and (Descri = EmptyStr) then
        begin
          MeAvisos.Lines.Add('Linha ' + Geral.FF0(I + 1) + ' > Linha Vazia');
        end else
        if (Numero = EmptyStr) or (Campo = EmptyStr) or (Tipo = EmptyStr) then
        begin
          if Descri <> EmptyStr then
          begin
            if SeqDescr < 8 then
            begin
              SeqDescr := SeqDescr + 1;
              ColS := SeqDescr + 10;
              SGCampos.Cells[ColS, N] := Descri;
            end;
            //
            //SGCampos.Cells[01, N] := Bloco;
            //SGCampos.Cells[02, N] := Registro;
            SGCampos.Cells[03, N] :=  SGCampos.Cells[03, N] + Numero;
            SGCampos.Cells[04, N] :=  SGCampos.Cells[04, N] + Campo;
            //SGCampos.Cells[05, N] :=  SGCampos.Cells[05, N] + Descri;
            SGCampos.Cells[06, N] :=  SGCampos.Cells[06, N] + Tipo;
            SGCampos.Cells[07, N] :=  SGCampos.Cells[07, N] + Tam;
            SGCampos.Cells[08, N] :=  SGCampos.Cells[08, N] + Decimais;
            SGCampos.Cells[09, N] :=  SGCampos.Cells[09, N] + CObrig;
          end;
        end else
        begin
          N := N + 1;
          SGCampos.RowCount := N + 1;
          SGCampos.Cells[00, N] := Grade1.Cells[00, N];
          //
          SGCampos.Cells[01, N] := Bloco;
          SGCampos.Cells[02, N] := Registro;
          SGCampos.Cells[03, N] := Numero;
          SGCampos.Cells[04, N] := Campo;
          SGCampos.Cells[05, N] := Descri;
          SGCampos.Cells[06, N] := Tipo;
          SGCampos.Cells[07, N] := Tam;
          SGCampos.Cells[08, N] := Decimais;
          SGCampos.Cells[09, N] := CObrig;
          //
          SeqDescr := 1;
        end;

      end else
      if (Bloco + Registro <> '') then
        Geral.MB_Aviso('Bloco ou Registro orf�o!' + sLineBreak +
        'Linha: ' + Geral.FF0(I) + sLineBreak +
        'Bloco: ' + Bloco + sLineBreak +
        'Registro: ' + Registro)

    end;
  end;
end;

procedure TFmEFD_RegObrig.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_RegObrig.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MeAvisos.Lines.Clear;
end;

procedure TFmEFD_RegObrig.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEFD_RegObrig.LiberaInclusoes();
var
  Habilita: Boolean;
begin
  Habilita := (RGTabela.ItemIndex > 0) and (SGCampos.RowCount > 2);
  //
  BtInsBlcs.Enabled := Habilita;
  BtInsFlds.Enabled := Habilita;
end;

procedure TFmEFD_RegObrig.RGTabelaClick(Sender: TObject);
begin
  LiberaInclusoes();
end;

procedure TFmEFD_RegObrig.SpeedButton8Click(Sender: TObject);
var
  Arq: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Arquivo', '', [], Arq) then
    EdCaminho.Text := Arq;
end;

end.
