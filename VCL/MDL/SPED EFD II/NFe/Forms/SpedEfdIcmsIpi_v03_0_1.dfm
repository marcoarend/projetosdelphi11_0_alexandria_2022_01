object DfSEII_v03_0_1: TDfSEII_v03_0_1
  Left = 0
  Top = 0
  Caption = 'DfSEII_v03_0_1'
  ClientHeight = 807
  ClientWidth = 967
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object QrE001: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrE001AfterOpen
    BeforeClose = QrE001BeforeClose
    AfterScroll = QrE001AfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT,'
      'CONCAT(RIGHT(e001.AnoMes, 2), "/",'
      'LEFT(LPAD(e001.AnoMes, 6, "0"), 4)) MES_ANO, e001.*'
      'FROM EFD_E001 e001'
      'LEFT JOIN entidades ent ON ent.Codigo=e001.Empresa'
      'WHERE e001.ImporExpor=1'
      'AND e001.Empresa=-11'
      'ORDER BY e001.AnoMes DESC')
    Left = 16
    Top = 4
    object QrE001NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrE001MES_ANO: TWideStringField
      FieldName = 'MES_ANO'
      Required = True
      Size = 7
    end
    object QrE001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE001AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE001LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE001REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE001IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
  end
  object DsE001: TDataSource
    DataSet = QrE001
    Left = 16
    Top = 52
  end
  object QrE100: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrE100AfterOpen
    BeforeClose = QrE100BeforeClose
    AfterScroll = QrE100AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efdicmsipie100 e100 '
      'WHERE e100.ImporExpor=3 '
      'AND e100.Empresa=-11 '
      'AND e100.AnoMes=201001'
      'ORDER BY e100.DT_INI'
      '')
    Left = 16
    Top = 100
    object QrE100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE100AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE100Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE100LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE100REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE100DT_INI: TDateField
      FieldName = 'DT_INI'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrE100DT_FIN: TDateField
      FieldName = 'DT_FIN'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsE100: TDataSource
    DataSet = QrE100
    Left = 16
    Top = 148
  end
  object QrE110: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrE110BeforeClose
    AfterScroll = QrE110AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efdicmsipie110 e110 '
      'WHERE e110.ImporExpor=3 '
      'AND e110.Empresa=-11 '
      'AND e110.AnoMes=201001'
      'AND e110.LinArq>0')
    Left = 16
    Top = 196
    object QrE110ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE110AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE110Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE110LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE110E100: TIntegerField
      FieldName = 'E100'
    end
    object QrE110REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE110VL_TOT_DEBITOS: TFloatField
      FieldName = 'VL_TOT_DEBITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE110VL_AJ_DEBITOS: TFloatField
      FieldName = 'VL_AJ_DEBITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE110VL_TOT_AJ_DEBITOS: TFloatField
      FieldName = 'VL_TOT_AJ_DEBITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE110VL_TOT_CREDITOS: TFloatField
      FieldName = 'VL_TOT_CREDITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE110VL_ESTORNOS_CRED: TFloatField
      FieldName = 'VL_ESTORNOS_CRED'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE110VL_AJ_CREDITOS: TFloatField
      FieldName = 'VL_AJ_CREDITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE110VL_TOT_AJ_CREDITOS: TFloatField
      FieldName = 'VL_TOT_AJ_CREDITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE110VL_ESTORNOS_DEB: TFloatField
      FieldName = 'VL_ESTORNOS_DEB'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE110VL_SLD_CREDOR_ANT: TFloatField
      FieldName = 'VL_SLD_CREDOR_ANT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE110VL_SLD_APURADO: TFloatField
      FieldName = 'VL_SLD_APURADO'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE110VL_TOT_DED: TFloatField
      FieldName = 'VL_TOT_DED'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE110VL_ICMS_RECOLHER: TFloatField
      FieldName = 'VL_ICMS_RECOLHER'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE110VL_SLD_CREDOR_TRANSPORTAR: TFloatField
      FieldName = 'VL_SLD_CREDOR_TRANSPORTAR'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE110DEB_ESP: TFloatField
      FieldName = 'DEB_ESP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsE110: TDataSource
    DataSet = QrE110
    Left = 16
    Top = 244
  end
  object QrE111: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrE111BeforeClose
    AfterScroll = QrE111AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efdicmsipie111 e111'
      'WHERE e111.ImporExpor=3'
      'AND e111.Empresa=-11'
      'AND e111.AnoMes=201001'
      'AND e111.LinArq>0'
      '')
    Left = 16
    Top = 288
    object QrE111ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE111AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE111Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE111LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE111E110: TIntegerField
      FieldName = 'E110'
    end
    object QrE111REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE111COD_AJ_APUR: TWideStringField
      FieldName = 'COD_AJ_APUR'
      Size = 8
    end
    object QrE111DESCR_COMPL_AJ: TWideStringField
      FieldName = 'DESCR_COMPL_AJ'
      Size = 255
    end
    object QrE111VL_AJ_APUR: TFloatField
      FieldName = 'VL_AJ_APUR'
    end
  end
  object DsE111: TDataSource
    DataSet = QrE111
    Left = 16
    Top = 336
  end
  object QrE115: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efdicmsipie115 e115'
      'WHERE e115.ImporExpor=3'
      'AND e115.Empresa=-11'
      'AND e115.AnoMes=201001'
      'AND e115.LinArq>0'
      '')
    Left = 60
    Top = 28
    object QrE115ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE115AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE115Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE115LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE115E110: TIntegerField
      FieldName = 'E110'
    end
    object QrE115REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE115COD_INF_ADIC: TWideStringField
      FieldName = 'COD_INF_ADIC'
      Size = 8
    end
    object QrE115VL_INF_ADIC: TFloatField
      FieldName = 'VL_INF_ADIC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrE115DESCR_COMPL_AJ: TWideStringField
      FieldName = 'DESCR_COMPL_AJ'
      Size = 255
    end
  end
  object DsE115: TDataSource
    DataSet = QrE115
    Left = 60
    Top = 76
  end
  object QrE116: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efdicmsipie116 e116'
      'WHERE e116.ImporExpor=3'
      'AND e116.Empresa=-11'
      'AND e116.AnoMes=201001'
      'AND e116.LinArq>0'
      '')
    Left = 60
    Top = 124
    object QrE116ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE116AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE116Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE116LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE116E110: TIntegerField
      FieldName = 'E110'
    end
    object QrE116REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE116COD_OR: TWideStringField
      FieldName = 'COD_OR'
      Size = 3
    end
    object QrE116VL_OR: TFloatField
      FieldName = 'VL_OR'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrE116DT_VCTO: TDateField
      FieldName = 'DT_VCTO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrE116COD_REC: TWideStringField
      FieldName = 'COD_REC'
      Size = 255
    end
    object QrE116NUM_PROC: TWideStringField
      FieldName = 'NUM_PROC'
      Size = 15
    end
    object QrE116IND_PROC: TWideStringField
      FieldName = 'IND_PROC'
      Size = 1
    end
    object QrE116PROC: TWideStringField
      FieldName = 'PROC'
      Size = 255
    end
    object QrE116TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
    object QrE116MES_REF: TDateField
      FieldName = 'MES_REF'
      DisplayFormat = 'MMYYYY'
    end
  end
  object DsE116: TDataSource
    DataSet = QrE116
    Left = 60
    Top = 172
  end
  object QrE112: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efdicmsipie112 e112'
      'WHERE e112.ImporExpor=3'
      'AND e112.Empresa=-11'
      'AND e112.AnoMes=201001'
      'AND e112.LinArq>0'
      '')
    Left = 60
    Top = 220
    object QrE112ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE112AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE112Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE112LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE112E111: TIntegerField
      FieldName = 'E111'
    end
    object QrE112REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE112NUM_DA: TWideStringField
      FieldName = 'NUM_DA'
      Size = 255
    end
    object QrE112NUM_PROC: TWideStringField
      FieldName = 'NUM_PROC'
      Size = 15
    end
    object QrE112IND_PROC: TWideStringField
      FieldName = 'IND_PROC'
      Size = 1
    end
    object QrE112PROC: TWideStringField
      FieldName = 'PROC'
      Size = 255
    end
    object QrE112TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
  end
  object DsE112: TDataSource
    DataSet = QrE112
    Left = 60
    Top = 268
  end
  object QrE113: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efdicmsipie113 e113'
      'WHERE e113.ImporExpor=3'
      'AND e113.Empresa=-11'
      'AND e113.AnoMes=201001'
      'AND e113.LinArq>0'
      '')
    Left = 60
    Top = 316
    object QrE113ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE113AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE113Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE113LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE113E111: TIntegerField
      FieldName = 'E111'
    end
    object QrE113REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE113COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrE113COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrE113SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrE113SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrE113NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrE113DT_DOC: TDateField
      FieldName = 'DT_DOC'
    end
    object QrE113COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrE113VL_AJ_ITEM: TFloatField
      FieldName = 'VL_AJ_ITEM'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsE113: TDataSource
    DataSet = QrE113
    Left = 60
    Top = 368
  end
  object QrE500: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrE500AfterOpen
    BeforeClose = QrE500BeforeClose
    AfterScroll = QrE500AfterScroll
    SQL.Strings = (
      'SELECT ELT(e500.IND_APUR + 1, "Mensal",'
      '"Decendial", "???") NO_IND_APUR, e500.*'
      'FROM efdicmsipie500 e500'
      'WHERE e500.ImporExpor=3'
      'AND e500.Empresa=-11'
      'AND e500.AnoMes=205001'
      'ORDER BY e500.DT_INI')
    Left = 104
    Top = 52
    object QrE500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE500AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE500Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE500LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE500REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE500IND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrE500DT_INI: TDateField
      FieldName = 'DT_INI'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrE500DT_FIN: TDateField
      FieldName = 'DT_FIN'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrE500NO_IND_APUR: TWideStringField
      FieldName = 'NO_IND_APUR'
      Size = 15
    end
  end
  object DsE500: TDataSource
    DataSet = QrE500
    Left = 104
    Top = 100
  end
  object QrE510: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efdicmsipie510 e510'
      'WHERE e510.ImporExpor=3'
      'AND e510.Empresa=-11'
      'AND e510.AnoMes=201001'
      'AND e510.LinArq>0')
    Left = 104
    Top = 148
    object QrE510ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE510AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE510Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE510LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE510E500: TIntegerField
      FieldName = 'E500'
    end
    object QrE510REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE510CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrE510CST_IPI: TWideStringField
      FieldName = 'CST_IPI'
      Size = 2
    end
    object QrE510VL_CONT_IPI: TFloatField
      FieldName = 'VL_CONT_IPI'
    end
    object QrE510VL_BC_IPI: TFloatField
      FieldName = 'VL_BC_IPI'
    end
    object QrE510VL_IPI: TFloatField
      FieldName = 'VL_IPI'
    end
    object QrE510Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrE510DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrE510DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrE510UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrE510UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrE510AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrE510Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsE510: TDataSource
    DataSet = QrE510
    Left = 104
    Top = 196
  end
  object QrE520: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrE520AfterOpen
    BeforeClose = QrE520BeforeClose
    AfterScroll = QrE520AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efdicmsipie520 e520'
      'WHERE e520.ImporExpor=3'
      'AND e520.Empresa=-11'
      'AND e520.AnoMes=201001'
      'AND e520.LinArq>0')
    Left = 104
    Top = 244
    object QrE520ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE520AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE520Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE520LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE520E500: TIntegerField
      FieldName = 'E500'
    end
    object QrE520REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE520VL_SD_ANT_IPI: TFloatField
      FieldName = 'VL_SD_ANT_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrE520VL_DEB_IPI: TFloatField
      FieldName = 'VL_DEB_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrE520VL_CRED_IPI: TFloatField
      FieldName = 'VL_CRED_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrE520VL_OD_IPI: TFloatField
      FieldName = 'VL_OD_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrE520VL_OC_IPI: TFloatField
      FieldName = 'VL_OC_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrE520VL_SC_IPI: TFloatField
      FieldName = 'VL_SC_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrE520VL_SD_IPI: TFloatField
      FieldName = 'VL_SD_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrE520Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrE520DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrE520DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrE520UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrE520UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrE520AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrE520Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsE520: TDataSource
    DataSet = QrE520
    Left = 104
    Top = 292
  end
  object QrE530: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efdicmsipie530 e530'
      'WHERE e530.ImporExpor=3'
      'AND e530.Empresa=-11'
      'AND e530.AnoMes=201001'
      'AND e530.LinArq>0'
      '')
    Left = 100
    Top = 344
    object QrE530ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrE530AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrE530Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrE530LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrE530E520: TIntegerField
      FieldName = 'E520'
    end
    object QrE530REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrE530IND_AJ: TWideStringField
      FieldName = 'IND_AJ'
      Size = 1
    end
    object QrE530VL_AJ: TFloatField
      FieldName = 'VL_AJ'
    end
    object QrE530COD_AJ: TWideStringField
      FieldName = 'COD_AJ'
      Size = 3
    end
    object QrE530IND_DOC: TWideStringField
      FieldName = 'IND_DOC'
      Size = 1
    end
    object QrE530NUM_DOC: TWideStringField
      FieldName = 'NUM_DOC'
      Size = 255
    end
    object QrE530DESCR_AJ: TWideStringField
      FieldName = 'DESCR_AJ'
      Size = 255
    end
  end
  object DsE530: TDataSource
    DataSet = QrE530
    Left = 100
    Top = 392
  end
  object QrH005: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrH005AfterOpen
    BeforeClose = QrH005BeforeClose
    AfterScroll = QrH005AfterScroll
    SQL.Strings = (
      'SELECT * FROM efdicmsipih005')
    Left = 144
    Top = 28
    object QrH005ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrH005AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrH005Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrH005LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrH005REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrH005DT_INV: TDateField
      FieldName = 'DT_INV'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrH005VL_INV: TFloatField
      FieldName = 'VL_INV'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrH005MOT_INV: TWideStringField
      FieldName = 'MOT_INV'
      Size = 2
    end
    object QrH005NO_MOT_INV: TWideStringField
      FieldName = 'NO_MOT_INV'
      Size = 255
    end
    object QrH005Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrH005DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrH005DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrH005UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrH005UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrH005AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrH005Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsH005: TDataSource
    DataSet = QrH005
    Left = 144
    Top = 76
  end
  object QrH010: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrH010AfterOpen
    BeforeClose = QrH010BeforeClose
    AfterScroll = QrH010AfterScroll
    SQL.Strings = (
      'SELECT * FROM efdicmsipih010')
    Left = 144
    Top = 128
    object QrH010ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrH010AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrH010Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrH010H005: TIntegerField
      FieldName = 'H005'
    end
    object QrH010LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrH010REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrH010COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrH010UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrH010QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrH010VL_UNIT: TFloatField
      FieldName = 'VL_UNIT'
      DisplayFormat = '#,###,###,##0.000000'
    end
    object QrH010VL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrH010IND_PROP: TWideStringField
      FieldName = 'IND_PROP'
      Size = 1
    end
    object QrH010COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrH010TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
    object QrH010COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrH010Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrH010DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrH010DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrH010UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrH010UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrH010AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrH010Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrH010VL_ITEM_IR: TFloatField
      FieldName = 'VL_ITEM_IR'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrH010NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrH010NO_PART: TWideStringField
      FieldName = 'NO_PART'
      Size = 100
    end
    object QrH010BalID: TIntegerField
      FieldName = 'BalID'
    end
    object QrH010BalNum: TIntegerField
      FieldName = 'BalNum'
    end
    object QrH010BalItm: TIntegerField
      FieldName = 'BalItm'
    end
    object QrH010BalEnt: TIntegerField
      FieldName = 'BalEnt'
    end
  end
  object DsH010: TDataSource
    DataSet = QrH010
    Left = 144
    Top = 176
  end
  object QrH020: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM efdicmsipih020')
    Left = 144
    Top = 224
    object QrH020ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrH020AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrH020Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrH020H010: TIntegerField
      FieldName = 'H010'
    end
    object QrH020LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrH020REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrH020CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrH020BC_ICMS: TFloatField
      FieldName = 'BC_ICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrH020VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrH020Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrH020DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrH020DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrH020UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrH020UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrH020AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrH020Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsH020: TDataSource
    DataSet = QrH020
    Left = 144
    Top = 272
  end
  object frxErrEmpresa: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxE_K200_001GetValue
    Left = 220
    Top = 632
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsErrEmpresa
        DataSetName = 'frxDsErrEmpresa'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Itens de Erros de Empresa')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA_SPED]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 279.685220000000000000
          Top = 51.023622050000000000
          Width = 219.212642360000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Left = 238.110390000000000000
          Top = 51.023622050000000000
          Width = 41.574805590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 619.842920000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 559.370440000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 498.897960000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Left = 498.897960000000000000
          Top = 51.023622050000000000
          Width = 181.417400940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ind'#250'stria')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 98.267780000000000000
          Top = 64.251944090000000000
          Width = 15.118095590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Left = 113.385900000000000000
          Top = 64.251944090000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Codigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 154.960730000000000000
          Top = 64.251944090000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-C')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo14: TfrxMemoView
          Left = 196.535560000000000000
          Top = 51.023622050000000000
          Width = 41.574805590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo16: TfrxMemoView
          Top = 64.252010000000000000
          Width = 15.118095590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo17: TfrxMemoView
          Left = 15.118120000000000000
          Top = 64.252010000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Codigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo18: TfrxMemoView
          Left = 56.692950000000000000
          Top = 64.252010000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo19: TfrxMemoView
          Left = 98.267780000000000000
          Top = 51.023622050000000000
          Width = 98.267740940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Registro')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo23: TfrxMemoView
          Top = 51.023622050000000000
          Width = 98.267740940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Origem')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        DataSet = frxDsErrEmpresa
        DataSetName = 'frxDsErrEmpresa'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 279.685220000000000000
          Width = 219.212642360000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrEmpresa."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 238.110390000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'PesoKg'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErrEmpresa."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Left = 559.370440000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'AreaM2'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErrEmpresa."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Left = 498.897960000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErrEmpresa."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo2: TfrxMemoView
          Left = 98.267780000000000000
          Width = 15.118110240000000000
          Height = 13.228346460000000000
          DataField = 'MovimID'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."MovimID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 113.385900000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'Codigo'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 154.960730000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'MovimCod'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."MovimCod"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 196.535560000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'Empresa'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."Empresa"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Width = 15.118110240000000000
          Height = 13.228346460000000000
          DataField = 'SrcMovID'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."SrcMovID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 15.118120000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'SrcNivel1'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."SrcNivel1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 56.692950000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'SrcNivel2'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."SrcNivel2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000000000
          Width = 498.897764720000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 498.897960000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsErrEmpresa."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 559.370440000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsErrEmpresa."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 619.842920000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsErrEmpresa."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsErrEmpresa: TfrxDBDataset
    UserName = 'frxDsErrEmpresa'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'Codigo=Codigo'
      'MovimID=MovimID'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'PesoKg=PesoKg'
      'Grandeza=Grandeza'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR')
    DataSet = QrErrEmpresa
    BCDToCurrency = False
    Left = 220
    Top = 584
  end
  object QrErrEmpresa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.Codigo, vmi.MovimID, vmi.MovimCod, vmi.Empresa,  '
      'vmi.GraGruX, Pecas, AreaM2, PesoKg, med.Grandeza, '
      'CONCAT(gg1.Nome, IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) NO_PRD_TAM_COR '
      'FROM vsmovits vmi   '
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX   '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed   '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2017-12-31 23:59:59"  '
      'AND Empresa>-11  ')
    Left = 220
    Top = 536
    object QrErrEmpresaSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrErrEmpresaSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrErrEmpresaSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrErrEmpresaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrErrEmpresaMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrErrEmpresaMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrErrEmpresaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrErrEmpresaGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrErrEmpresaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrErrEmpresaAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrErrEmpresaPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrErrEmpresaGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrErrEmpresaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object DsK220: TDataSource
    DataSet = QrK220
    Left = 188
    Top = 288
  end
  object QrK220: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'CONCAT(gg1o.Nome, '
      'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), '
      'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) '
      'NO_PRD_TAM_COR_ORI, '
      'CONCAT(gg1d.Nome, '
      'IF(gtid.PrintTam=0, "", CONCAT(" ", gtid.Nome)), '
      'IF(gccd.PrintCor=0,"", CONCAT(" ", gccd.Nome))) '
      'NO_PRD_TAM_COR_DEST, '
      'IF(gg1o.UnidMed=gg1d.UnidMed, unmo.Sigla, "???") Sigla, '
      'K220.* '
      'FROM efdicmsipiK220 K220 '
      'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K220.COD_ITEM_ORI '
      'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC '
      'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad '
      'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI '
      'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 '
      'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed '
      ' '
      'LEFT JOIN gragrux    ggxd ON ggxd.Controle=K220.COD_ITEM_DEST '
      'LEFT JOIN gragruc    ggcd ON ggcd.Controle=ggxd.GraGruC '
      'LEFT JOIN gracorcad  gccd ON gccd.Codigo=ggcd.GraCorCad '
      'LEFT JOIN gratamits  gtid ON gtid.Controle=ggxd.GraTamI '
      'LEFT JOIN gragru1    gg1d ON gg1d.Nivel1=ggxd.GraGru1 '
      'LEFT JOIN unidmed    unmd ON unmd.Codigo=gg1d.UnidMed '
      'WHERE k220.ImporExpor=3'
      'AND k220.Empresa=-11'
      'AND k220.AnoMes=201702'
      'AND k220.PeriApu=1'
      
        'ORDER BY k220.KndNSU, k220.KndCod, k220.DT_MOV, COD_ITEM_ORI, CO' +
        'D_ITEM_DEST ')
    Left = 188
    Top = 244
    object QrK220NO_PRD_TAM_COR_ORI: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_ORI'
      Size = 157
    end
    object QrK220NO_PRD_TAM_COR_DEST: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_DEST'
      Size = 157
    end
    object QrK220Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK220ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK220AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK220Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK220PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK220KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK220KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK220KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK220KndItmOri: TIntegerField
      FieldName = 'KndItmOri'
    end
    object QrK220KndItmDst: TIntegerField
      FieldName = 'KndItmDst'
    end
    object QrK220KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK220KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK220ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK220DT_MOV: TDateField
      FieldName = 'DT_MOV'
    end
    object QrK220COD_ITEM_ORI: TWideStringField
      FieldName = 'COD_ITEM_ORI'
      Size = 60
    end
    object QrK220COD_ITEM_DEST: TWideStringField
      FieldName = 'COD_ITEM_DEST'
      Size = 60
    end
    object QrK220QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK220GGXDst: TIntegerField
      FieldName = 'GGXDst'
    end
    object QrK220GGXOri: TIntegerField
      FieldName = 'GGXOri'
    end
    object QrK220ESOMIEM: TIntegerField
      FieldName = 'ESOMIEM'
    end
    object QrK220OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK220Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK220DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK220DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK220UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK220UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK220AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK220Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrK220IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK220QTD_ORI: TFloatField
      FieldName = 'QTD_ORI'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK220QTD_DEST: TFloatField
      FieldName = 'QTD_DEST'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object DsK200: TDataSource
    DataSet = QrK200
    Left = 188
    Top = 196
  end
  object QrK200: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(K200.COD_PART="", "", IF(ter.Tipo=0, '
      'ter.RazaoSocial, ter.Nome)) NO_PART, '
      'unm.Sigla, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      
        ' ELT(k200.IND_EST + 1,"Estoque de propriedade do informante e em' +
        ' seu poder","Estoque de propriedade do informante e em posse de ' +
        'terceiros","Estoque de propriedade de terceiros e em posse do in' +
        'formante","???") NO_IND_EST,'
      ' ELT(k200.KndTab + 1,"N/D","Mat.prima","Insumo") NO_KndTab,'
      'K200.* '
      'FROM efdicmsipiK200 K200 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K200.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed '
      'LEFT JOIN entidades  ter ON ter.Codigo=K200.COD_PART '
      'WHERE k200.ImporExpor=3'
      'AND k200.Empresa=-11'
      'AND k200.AnoMes=201708'
      'AND k200.PeriApu=1'
      'ORDER BY DT_EST ')
    Left = 188
    Top = 148
    object QrK200NO_PART: TWideStringField
      FieldName = 'NO_PART'
      Size = 100
    end
    object QrK200Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK200NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK200NO_IND_EST: TWideStringField
      FieldName = 'NO_IND_EST'
      Size = 60
    end
    object QrK200NO_KndTab: TWideStringField
      FieldName = 'NO_KndTab'
      Size = 9
    end
    object QrK200ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK200AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK200Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK200PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK200KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK200KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK200KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK200KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK200KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK200KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK200DT_EST: TDateField
      FieldName = 'DT_EST'
    end
    object QrK200COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK200QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK200IND_EST: TWideStringField
      FieldName = 'IND_EST'
      Size = 1
    end
    object QrK200COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrK200GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK200Grandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrK200Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrK200AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrK200PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrK200Entidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrK200Tipo_Item: TIntegerField
      FieldName = 'Tipo_Item'
    end
    object QrK200Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK200DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK200DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK200UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK200UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK200AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK200Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrK200IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK200ID_SEK: TIntegerField
      FieldName = 'ID_SEK'
    end
  end
  object DsK100: TDataSource
    DataSet = QrK100
    Left = 188
    Top = 100
  end
  object QrK100: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrK100AfterOpen
    BeforeClose = QrK100BeforeClose
    AfterScroll = QrK100AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efdicmsipik100 e100 '
      'WHERE e100.ImporExpor=3 '
      'AND e100.Empresa=-11 '
      'AND e100.AnoMes=201001'
      'ORDER BY e100.DT_INI'
      '')
    Left = 188
    Top = 52
    object QrK100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK100AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK100Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK100PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK100DT_INI: TDateField
      FieldName = 'DT_INI'
    end
    object QrK100DT_FIN: TDateField
      FieldName = 'DT_FIN'
    end
    object QrK100Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK100DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK100DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK100UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK100UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK100AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK100Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrK210: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrK210BeforeClose
    AfterScroll = QrK210AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'IF(k210.DT_FIN_OS = 0, "", '
      'DATE_FORMAT(k210.DT_FIN_OS, "%d/%m/%Y")) DT_FIN_OS_Txt, '
      'K210.* '
      'FROM efdicmsipiK210 k210 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K210.COD_ITEM_ORI '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed '
      'WHERE k210.ImporExpor=3'
      'AND k210.Empresa=-11'
      'AND k210.AnoMes=201702'
      'AND k210.PeriApu=1'
      'ORDER BY DT_INI_OS, DT_FIN_OS, COD_DOC_OS, COD_ITEM ')
    Left = 236
    Top = 80
    object QrK210Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK210Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK210NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK210DT_FIN_OS_Txt: TWideStringField
      FieldName = 'DT_FIN_OS_Txt'
      Size = 10
    end
    object QrK210ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK210AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK210Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK210PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK210KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK210KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK210KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK210KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK210KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK210KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK210IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK210ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK210DT_INI_OS: TDateField
      FieldName = 'DT_INI_OS'
    end
    object QrK210DT_FIN_OS: TDateField
      FieldName = 'DT_FIN_OS'
    end
    object QrK210COD_DOC_OS: TWideStringField
      FieldName = 'COD_DOC_OS'
      Size = 30
    end
    object QrK210COD_ITEM_ORI: TWideStringField
      FieldName = 'COD_ITEM_ORI'
      Size = 60
    end
    object QrK210QTD_ORI: TFloatField
      FieldName = 'QTD_ORI'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK210GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK210OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK210OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK210Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK210DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK210DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK210UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK210UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK210AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK210Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK210: TDataSource
    DataSet = QrK210
    Left = 232
    Top = 124
  end
  object QrK215: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'k215.* '
      'FROM efdicmsipik215 k215 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k215.COD_ITEM_DES '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ')
    Left = 232
    Top = 172
    object QrK215Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK215Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK215NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK215ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK215AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK215Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK215PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK215KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK215KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK215KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK215KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK215KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK215KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK215IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK215IDSeq2: TIntegerField
      FieldName = 'IDSeq2'
    end
    object QrK215COD_ITEM_DES: TWideStringField
      FieldName = 'COD_ITEM_DES'
      Size = 60
    end
    object QrK215QTD_DES: TFloatField
      FieldName = 'QTD_DES'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK215GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK215ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrK215OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK215OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK215Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK215DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK215DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK215UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK215UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK215AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK215Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrK215ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
  end
  object DsK215: TDataSource
    DataSet = QrK215
    Left = 232
    Top = 220
  end
  object QrK230: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrK230BeforeClose
    AfterScroll = QrK230AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'IF(k230.DT_FIN_OP = 0, "", '
      'DATE_FORMAT(k230.DT_FIN_OP, "%d/%m/%Y")) DT_FIN_OP_Txt,'
      'K230.* '
      'FROM efdicmsipiK230 k230 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K230.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k230.ImporExpor=3'
      'AND k230.Empresa=-11'
      'AND k230.AnoMes=201702'
      'AND k230.PeriApu=1'
      'ORDER BY DT_INI_OP, DT_FIN_OP, COD_DOC_OP, COD_ITEM')
    Left = 276
    Top = 104
    object QrK230Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK230Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK230NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK230DT_FIN_OP_Txt: TWideStringField
      FieldName = 'DT_FIN_OP_Txt'
      Size = 10
    end
    object QrK230ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK230AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK230Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK230PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK230KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK230KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK230KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK230KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK230KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK230KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK230IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK230ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK230DT_INI_OP: TDateField
      FieldName = 'DT_INI_OP'
    end
    object QrK230DT_FIN_OP: TDateField
      FieldName = 'DT_FIN_OP'
    end
    object QrK230COD_DOC_OP: TWideStringField
      FieldName = 'COD_DOC_OP'
      Size = 30
    end
    object QrK230COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK230QTD_ENC: TFloatField
      FieldName = 'QTD_ENC'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK230GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK230OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK230Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK230DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK230DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK230UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK230UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK230AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK230Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK230: TDataSource
    DataSet = QrK230
    Left = 276
    Top = 148
  end
  object QrK235: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,  '
      'CONCAT(gg1.Nome,   '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR, '
      'k235.*  '
      'FROM efdicmsipik235 k235 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k235.COD_ITEM  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   ')
    Left = 276
    Top = 196
    object QrK235Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK235Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK235NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK235ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK235AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK235Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK235PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK235KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK235KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK235KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK235KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK235KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK235KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK235IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK235IDSeq2: TIntegerField
      FieldName = 'IDSeq2'
    end
    object QrK235ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK235DT_SAIDA: TDateField
      FieldName = 'DT_SAIDA'
    end
    object QrK235COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK235QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK235COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrK235GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK235ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrK235OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK235Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK235DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK235DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK235UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK235UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK235AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK235Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK235: TDataSource
    DataSet = QrK235
    Left = 276
    Top = 244
  end
  object frxE_K200_001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxE_K200_001GetValue
    Left = 304
    Top = 512
    Datasets = <
      item
        DataSet = frxDsK200
        DataSetName = 'frxDsK200'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'K200 - ESTOQUE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA_SPED]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 41.574830000000000000
          Top = 51.023622047244090000
          Width = 362.834782360000000000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Top = 51.023622047244090000
          Width = 41.574805590000000000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 585.827150000000000000
          Top = 64.252010000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 525.354670000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 464.882190000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 404.409710000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Left = 404.409710000000000000
          Top = 51.023622050000000000
          Width = 181.417400940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ind'#250'stria')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Left = 585.827150000000000000
          Top = 51.023622050000000000
          Width = 94.488210940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fisco')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        DataSet = frxDsK200
        DataSetName = 'frxDsK200'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 41.574830000000000000
          Width = 362.834782360000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsK200
          DataSetName = 'frxDsK200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK200."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 41.574803149606300000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsK200
          DataSetName = 'frxDsK200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK200."COD_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsK200
          DataSetName = 'frxDsK200'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK200."QTD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataSet = frxDsK200
          DataSetName = 'frxDsK200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK200."Sigla"] ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 525.354670000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'PesoKg'
          DataSet = frxDsK200
          DataSetName = 'frxDsK200'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK200."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Left = 464.882190000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'AreaM2'
          DataSet = frxDsK200
          DataSetName = 'frxDsK200'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK200."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Left = 404.409710000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'Pecas'
          DataSet = frxDsK200
          DataSetName = 'frxDsK200'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK200."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 536.693260000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 483.779840000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000000000
          Width = 404.409514720000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Top = 15.118120000000000000
          Width = 60.472433620000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 404.409710000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 464.882190000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 525.354670000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_00: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsK200."IND_EST"'
        object Me_GH0: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsK200."NO_IND_EST"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_00: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Me_FT0: TfrxMemoView
          Width = 404.409309690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK200."NO_IND_EST"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu0PesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 404.409710000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 464.882190000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 525.354670000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_02: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 249.448980000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsK200."COD_ITEM"'
        object Me_GH2: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsK200."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsK200."COD_PART"'
        object Me_GH1: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsK200."NO_PART"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 370.393940000000000000
        Width = 680.315400000000000000
        object Me_FT1: TfrxMemoView
          Width = 404.409309690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK200."NO_PART"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu1PesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 404.409710000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 464.882190000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 525.354670000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FT_02: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 332.598640000000000000
        Visible = False
        Width = 680.315400000000000000
        object Me_FT2: TfrxMemoView
          Width = 404.409309690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK200."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu2PesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 404.409710000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 464.882190000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 525.354670000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK200."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsK200: TfrxDBDataset
    UserName = 'frxDsK200'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K100=K100'
      'DT_EST=DT_EST'
      'COD_ITEM=COD_ITEM'
      'QTD=QTD'
      'IND_EST=IND_EST'
      'COD_PART=COD_PART'
      'BalID=BalID'
      'BalNum=BalNum'
      'BalItm=BalItm'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'BalEnt=BalEnt'
      'NO_PART=NO_PART'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_IND_EST=NO_IND_EST'
      'Sigla=Sigla'
      'GraGruX=GraGruX'
      'Grandeza=Grandeza'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'PesoKg=PesoKg')
    DataSet = QrK200
    BCDToCurrency = False
    Left = 304
    Top = 560
  end
  object frxE_K220_001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxE_K200_001GetValue
    Left = 304
    Top = 608
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsK220
        DataSetName = 'frxDsK220'
      end
      item
        DataSet = frxDsK270_220
        DataSetName = 'frxDsK270_220'
      end
      item
        DataSet = frxDsK275_220
        DataSetName = 'frxDsK275_220'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'K220 - OUTRAS MOVIMENTA'#199#213'ES INTERNAS ENTRE MERCADORIAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA_SPED]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 79.370130000000000000
          Top = 64.252010000000000000
          Width = 232.440932680000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Left = 37.795300000000000000
          Top = 64.252010000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 585.827150000000000000
          Top = 64.252010000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Left = 585.827150000000000000
          Top = 51.023622050000000000
          Width = 94.488210940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fisco')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 37.795300000000000000
          Top = 51.023622050000000000
          Width = 274.015735830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Origem')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 311.811035830000000000
          Top = 51.023622050000000000
          Width = 274.015735830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Destino')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 353.385838980000000000
          Top = 64.252010000000000000
          Width = 232.440942440000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 311.811035830000000000
          Top = 64.252010000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Top = 51.023622050000000000
          Width = 37.795275590551180000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDsK220
        DataSetName = 'frxDsK220'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 79.370130000000000000
          Width = 232.440932680000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR_ORI'
          DataSet = frxDsK220
          DataSetName = 'frxDsK220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK220."NO_PRD_TAM_COR_ORI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 37.795300000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'GGXOri'
          DataSet = frxDsK220
          DataSetName = 'frxDsK220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK220."GGXOri"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsK220
          DataSetName = 'frxDsK220'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK220."QTD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsK220
          DataSetName = 'frxDsK220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK220."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 353.385836540000000000
          Width = 232.440942440000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR_DEST'
          DataSet = frxDsK220
          DataSetName = 'frxDsK220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK220."NO_PRD_TAM_COR_DEST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 311.811035830000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'GGXDst'
          DataSet = frxDsK220
          DataSetName = 'frxDsK220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK220."GGXDst"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_MOV'
          DataSet = frxDsK220
          DataSetName = 'frxDsK220'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK220."DT_MOV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 555.590910000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 502.677490000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000000000
          Width = 619.842724720000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Top = 15.118120000000000000
          Width = 60.472433620000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '['
            'SUM(<frxDsK220."QTD">,MD002,1)'
            '+SUM(<frxDsK270_220."QTD_COR_POS">,MD001)'
            '-SUM(<frxDsK270_220."QTD_COR_NEG">,MD001)'
            ']')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_00: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsK220."MovimCod"'
        object Me_GH0: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'IME-C [frxDsK220."MovimCod"] - [VARF_NoMovimCod_220]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object FT_00: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        object Me_FT0: TfrxMemoView
          Width = 619.842519690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'IME-C [frxDsK220."MovimCod"] - [VARF_NoMovimCod_220]: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu0PesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK220."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 366.614410000000000000
        Width = 680.315400000000000000
        DataSet = frxDsK270_220
        DataSetName = 'frxDsK270_220'
        RowCount = 0
        object Memo12: TfrxMemoView
          Left = 79.370130000000000000
          Width = 232.440932680000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsK270_220
          DataSetName = 'frxDsK270_220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK270_220."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 37.795300000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsK270_220
          DataSetName = 'frxDsK270_220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK270_220."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsK270_220
          DataSetName = 'frxDsK270_220'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsK270_220."QTD_COR_POS">-<frxDsK270_220."QTD_COR_NEG">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsK270_220
          DataSetName = 'frxDsK270_220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK270_220."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 353.385836540000000000
          Width = 232.440942440000000000
          Height = 13.228346460000000000
          DataSet = frxDsK220
          DataSetName = 'frxDsK220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK275_220."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 311.811035830000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataSet = frxDsK220
          DataSetName = 'frxDsK220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK275_220."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_INI_AP'
          DataSet = frxDsK270_220
          DataSetName = 'frxDsK270_220'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK270_220."DT_INI_AP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsK220."MovimCod"'
        object Memo19: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'IME-C [frxDsK220."MovimCod"] - [VARF_NoMovimCod_220]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GF_01: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 404.409710000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          Width = 619.842519690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'IME-C [frxDsK270_220."MovimCod"] - [VARF_NoMovimCod_270]: ')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo21: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '['
            'SUM(<frxDsK270_220."QTD_COR_POS">,MD001)'
            '-SUM(<frxDsK270_220."QTD_COR_NEG">,MD001)'
            ']')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        object Memo22: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'K270 - OUTRAS MOVIMENTA'#199#213'ES INTERNAS ENTRE MERCADORIAS - CORRE'#199#195 +
              'O DE APONTAMENTO')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Top = 442.205010000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsK220: TfrxDBDataset
    UserName = 'frxDsK220'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PRD_TAM_COR_ORI=NO_PRD_TAM_COR_ORI'
      'NO_PRD_TAM_COR_DEST=NO_PRD_TAM_COR_DEST'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K100=K100'
      'DT_MOV=DT_MOV'
      'COD_ITEM_ORI=COD_ITEM_ORI'
      'COD_ITEM_DEST=COD_ITEM_DEST'
      'QTD=QTD'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'MovimID=MovimID'
      'ID_SEK=ID_SEK'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'CtrlDst=CtrlDst'
      'GGXDst=GGXDst'
      'GGXOri=GGXOri'
      'Sigla=Sigla')
    DataSet = QrK220
    BCDToCurrency = False
    Left = 304
    Top = 656
  end
  object frxDsK270_220: TfrxDBDataset
    UserName = 'frxDsK270_220'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Sigla=Sigla'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K100=K100'
      'ID_SEK=ID_SEK'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'DT_INI_AP=DT_INI_AP'
      'DT_FIN_AP=DT_FIN_AP'
      'COD_OP_OS=COD_OP_OS'
      'COD_ITEM=COD_ITEM'
      'QTD_COR_POS=QTD_COR_POS'
      'QTD_COR_NEG=QTD_COR_NEG'
      'ORIGEM=ORIGEM'
      'GraGruX=GraGruX'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrK270_220
    BCDToCurrency = False
    Left = 304
    Top = 700
  end
  object frxDsK275_220: TfrxDBDataset
    UserName = 'frxDsK275_220'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Sigla=Sigla'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K270=K270'
      'ID_SEK=ID_SEK'
      'COD_ITEM=COD_ITEM'
      'QTD_COR_POS=QTD_COR_POS'
      'QTD_COR_NEG=QTD_COR_NEG'
      'COD_INS_SUBST=COD_INS_SUBST'
      'ORIGEM=ORIGEM'
      'ID_Item=ID_Item'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Controle=Controle'
      'GraGruX=GraGruX'
      'ESTSTabSorc=ESTSTabSorc'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrK275_220
    BCDToCurrency = False
    Left = 304
    Top = 744
  end
  object frxDsK255: TfrxDBDataset
    UserName = 'frxDsK255'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'DT_CONS=DT_CONS'
      'COD_ITEM=COD_ITEM'
      'QTD=QTD'
      'COD_INS_SUBST=COD_INS_SUBST'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'K250=K250'
      'ID_SEK=ID_SEK'
      'ID_Item=ID_Item'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Controle=Controle')
    DataSet = QrK255
    BCDToCurrency = False
    Left = 388
    Top = 680
  end
  object frxDsK250: TfrxDBDataset
    UserName = 'frxDsK250'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'DT_PROD_Txt=DT_PROD_Txt'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K100=K100'
      'DT_PROD=DT_PROD'
      'COD_ITEM=COD_ITEM'
      'QTD=QTD'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'ID_SEK=ID_SEK'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'COD_DOC_OP=COD_DOC_OP')
    DataSet = QrK250
    BCDToCurrency = False
    Left = 388
    Top = 632
  end
  object frxDsK235: TfrxDBDataset
    UserName = 'frxDsK235'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'DT_SAIDA=DT_SAIDA'
      'COD_ITEM=COD_ITEM'
      'QTD=QTD'
      'COD_INS_SUBST=COD_INS_SUBST'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'K230=K230'
      'ID_SEK=ID_SEK'
      'ID_Item=ID_Item'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Controle=Controle')
    DataSet = QrK235
    BCDToCurrency = False
    Left = 388
    Top = 584
  end
  object frxDsK230: TfrxDBDataset
    UserName = 'frxDsK230'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'DT_INI_OP=DT_INI_OP'
      'K100=K100'
      'DT_FIN_OP=DT_FIN_OP'
      'COD_DOC_OP=COD_DOC_OP'
      'COD_ITEM=COD_ITEM'
      'QTD_ENC=QTD_ENC'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'DT_FIN_OP_Txt=DT_FIN_OP_Txt'
      'Grandeza=Grandeza'
      'Sigla=Sigla')
    DataSet = QrK230
    BCDToCurrency = False
    Left = 388
    Top = 536
  end
  object frxE_K230_001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxE_K200_001GetValue
    Left = 388
    Top = 492
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsK210
        DataSetName = 'frxDsK210'
      end
      item
        DataSet = frxDsK215
        DataSetName = 'frxDsK215'
      end
      item
        DataSet = frxDsK230
        DataSetName = 'frxDsK230'
      end
      item
        DataSet = frxDsK235
        DataSetName = 'frxDsK235'
      end
      item
        DataSet = frxDsK250
        DataSetName = 'frxDsK250'
      end
      item
        DataSet = frxDsK255
        DataSetName = 'frxDsK255'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'K230 a K255 - ITENS PRODUZIDOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA_SPED]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 158.740260000000000000
          Top = 51.023622047244090000
          Width = 427.086614173228300000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Left = 117.165430000000000000
          Top = 51.023622047244090000
          Width = 41.574805590000000000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 585.827150000000000000
          Top = 64.252010000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Left = 585.827150000000000000
          Top = 51.023622050000000000
          Width = 94.488210940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fisco')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Top = 51.023622047244090000
          Width = 37.795275590551180000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'In'#237'cio')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo14: TfrxMemoView
          Left = 37.795300000000000000
          Top = 51.023622047244090000
          Width = 37.795275590000000000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'T'#233'rmino')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo15: TfrxMemoView
          Left = 75.590600000000000000
          Top = 51.023622050000000000
          Width = 41.574805590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'OP/IME-C')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 26.456692920000000000
        Top = 381.732530000000000000
        Width = 680.315400000000000000
        DataSet = frxDsK230
        DataSetName = 'frxDsK230'
        RowCount = 0
        object MeValCodi: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_FIN_OP'
          DataSet = frxDsK230
          DataSetName = 'frxDsK230'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK230."DT_FIN_OP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsK230
          DataSetName = 'frxDsK230'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK230."QTD_ENC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsK230
          DataSetName = 'frxDsK230'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK230."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 158.740167240000000000
          Width = 427.086604410000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsK230
          DataSetName = 'frxDsK230'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK230."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 117.165364090000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsK230
          DataSetName = 'frxDsK230'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK230."COD_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_INI_OP'
          DataSet = frxDsK230
          DataSetName = 'frxDsK230'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK230."DT_INI_OP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 75.590600000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          DataField = 'COD_DOC_OP'
          DataSet = frxDsK230
          DataSetName = 'frxDsK230'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK230."COD_DOC_OP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 619.842920000000000000
          Top = 13.228346460000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo24: TfrxMemoView
          Left = 585.827150000000000000
          Top = 13.228346460000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo25: TfrxMemoView
          Left = 75.590600000000000000
          Top = 13.228346460000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Susbtituto')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo26: TfrxMemoView
          Left = 37.795300000000000000
          Top = 13.228346460000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sa'#237'da')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo30: TfrxMemoView
          Left = 120.944960000000000000
          Top = 13.228346460000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Left = 166.299320000000000000
          Top = 13.228346460000000000
          Width = 419.527790940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material usado para produzir')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo36: TfrxMemoView
          Top = 13.228346460000000000
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 782.362710000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 729.449290000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000000000
          Width = 619.842724720000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL PRODUZIDO: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Top = 15.118120000000000000
          Width = 60.472433620000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxDsK230."QTD_ENC">,MD002,1) + SUM(<frxDsK250."QTD">,MD00' +
              '3,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 336.378170000000000000
        Width = 680.315400000000000000
        object Me_GH0: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'K230 - ITENS PRODUZIDOS (NAS INSTALA'#199#213'ES DA EMPRESA)')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 22.677170240000000000
        Top = 468.661720000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK235."QTD">,DD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Width = 616.062975040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              'K235 - ITENS CONSUMIDOS (NAS INSTALA'#199#213'ES DA EMPRESA) na OP/IME-C' +
              ' [frxDsK230."COD_DOC_OP"]: ')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD003: TfrxMasterData
        FillType = ftBrush
        Height = 26.456692920000000000
        Top = 559.370440000000000000
        Width = 680.315400000000000000
        DataSet = frxDsK250
        DataSetName = 'frxDsK250'
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'QTD'
          DataSet = frxDsK250
          DataSetName = 'frxDsK250'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK250."QTD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsK250
          DataSetName = 'frxDsK250'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK250."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 158.740167240000000000
          Width = 427.086604410000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsK250
          DataSetName = 'frxDsK250'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK250."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 117.165364090000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsK250
          DataSetName = 'frxDsK250'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK250."COD_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_PROD'
          DataSet = frxDsK250
          DataSetName = 'frxDsK250'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK250."DT_PROD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 75.590600000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          DataField = 'COD_DOC_OP'
          DataSet = frxDsK250
          DataSetName = 'frxDsK250'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK250."COD_DOC_OP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsK250
          DataSetName = 'frxDsK250'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 619.842920000000000000
          Top = 13.228346456692910000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo39: TfrxMemoView
          Left = 585.827150000000000000
          Top = 13.228346456692910000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo40: TfrxMemoView
          Left = 75.590600000000000000
          Top = 13.228346460000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Susbtituto')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          Left = 37.795300000000000000
          Top = 13.228346460000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo42: TfrxMemoView
          Left = 120.944960000000000000
          Top = 13.228346460000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo43: TfrxMemoView
          Left = 166.299320000000000000
          Top = 13.228346460000000000
          Width = 419.527790940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material usado para produzir')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo44: TfrxMemoView
          Top = 13.228346456692910000
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 514.016080000000000000
        Width = 680.315400000000000000
        object Memo21: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'K250 - ITENS PRODUZIDOS POR TERCEIROS')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        Height = 22.677170240000000000
        Top = 646.299630000000000000
        Width = 680.315400000000000000
        object Memo22: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK255."QTD">,DD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Width = 616.062975040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              'K255 - ITENS CONSUMIDOS POR TERCEIROS na OP/IME-C [frxDsK250."CO' +
              'D_DOC_OP"]: ')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DD002: TfrxDetailData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        DataSet = frxDsK235
        DataSetName = 'frxDsK235'
        RowCount = 0
        object Memo27: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_SAIDA'
          DataSet = frxDsK235
          DataSetName = 'frxDsK235'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK235."DT_SAIDA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 75.590600000000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          DataField = 'COD_INS_SUBST'
          DataSet = frxDsK235
          DataSetName = 'frxDsK235'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK235."COD_INS_SUBST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo32: TfrxMemoView
          Left = 120.944960000000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsK235
          DataSetName = 'frxDsK235'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK235."COD_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo33: TfrxMemoView
          Left = 166.299320000000000000
          Width = 419.527790940000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsK235
          DataSetName = 'frxDsK235'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK235."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo34: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsK235
          DataSetName = 'frxDsK235'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK235."QTD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsK235
          DataSetName = 'frxDsK235'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK235."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsK235
          DataSetName = 'frxDsK235'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK235."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object DD003: TfrxDetailData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 608.504330000000000000
        Width = 680.315400000000000000
        DataSet = frxDsK255
        DataSetName = 'frxDsK255'
        RowCount = 0
        object Memo45: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_CONS'
          DataSet = frxDsK255
          DataSetName = 'frxDsK255'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK255."DT_CONS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 75.590600000000000000
          Width = 41.574790940000000000
          Height = 13.228346460000000000
          DataField = 'COD_INS_SUBST'
          DataSet = frxDsK255
          DataSetName = 'frxDsK255'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK255."COD_INS_SUBST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Left = 120.944960000000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsK255
          DataSetName = 'frxDsK255'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK255."COD_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo51: TfrxMemoView
          Left = 166.299320000000000000
          Width = 419.527790940000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsK255
          DataSetName = 'frxDsK255'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK255."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo52: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'QTD'
          DataSet = frxDsK255
          DataSetName = 'frxDsK255'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK255."QTD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsK255
          DataSetName = 'frxDsK255'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK255."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsK255
          DataSetName = 'frxDsK255'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK255."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        Height = 26.456692920000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDsK210
        DataSetName = 'frxDsK210'
        RowCount = 0
        object Memo55: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_FIN_OS'
          DataSet = frxDsK210
          DataSetName = 'frxDsK210'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK210."DT_FIN_OS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'QTD_ORI'
          DataSet = frxDsK210
          DataSetName = 'frxDsK210'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK210."QTD_ORI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsK210
          DataSetName = 'frxDsK210'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK210."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 158.740167240000000000
          Width = 427.086604410000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsK210
          DataSetName = 'frxDsK210'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK210."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 117.165364090000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM_ORI'
          DataSet = frxDsK210
          DataSetName = 'frxDsK210'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK210."COD_ITEM_ORI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_INI_OS'
          DataSet = frxDsK210
          DataSetName = 'frxDsK210'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK210."DT_INI_OS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 75.590600000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          DataField = 'COD_DOC_OS'
          DataSet = frxDsK210
          DataSetName = 'frxDsK210'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK210."COD_DOC_OS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo62: TfrxMemoView
          Left = 619.842920000000000000
          Top = 13.228346460000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo63: TfrxMemoView
          Left = 585.827150000000000000
          Top = 13.228346460000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo64: TfrxMemoView
          Left = 75.590600000000000000
          Top = 13.228346460000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Susbtituto')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo65: TfrxMemoView
          Left = 37.795300000000000000
          Top = 13.228346460000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sa'#237'da')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo66: TfrxMemoView
          Left = 120.944960000000000000
          Top = 13.228346460000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo67: TfrxMemoView
          Left = 166.299320000000000000
          Top = 13.228346460000000000
          Width = 419.527790940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material usado para produzir')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo68: TfrxMemoView
          Top = 13.228346460000000000
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object Header3: TfrxHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        object Memo69: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'K210 - DESMONTAGEM DE MERCADORIAS')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer3: TfrxFooter
        FillType = ftBrush
        Height = 22.677170240000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        object Memo70: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK235."QTD">,DD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Width = 616.062975040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              'K235 - ITENS CONSUMIDOS (NAS INSTALA'#199#213'ES DA EMPRESA) na OP/IME-C' +
              ' [frxDsK230."COD_DOC_OP"]: ')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DD001: TfrxDetailData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        DataSet = frxDsK215
        DataSetName = 'frxDsK215'
        RowCount = 0
        object Memo72: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsK235
          DataSetName = 'frxDsK235'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 75.590600000000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          DataSet = frxDsK235
          DataSetName = 'frxDsK235'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo74: TfrxMemoView
          Left = 120.944960000000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM_DES'
          DataSet = frxDsK215
          DataSetName = 'frxDsK215'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK215."COD_ITEM_DES"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo75: TfrxMemoView
          Left = 166.299320000000000000
          Width = 419.527790940000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsK215
          DataSetName = 'frxDsK215'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK215."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo76: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'QTD_DES'
          DataSet = frxDsK215
          DataSetName = 'frxDsK215'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK215."QTD_DES"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsK215
          DataSetName = 'frxDsK215'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK215."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsK215
          DataSetName = 'frxDsK215'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK215."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
    end
  end
  object DsK255: TDataSource
    DataSet = QrK255
    Left = 320
    Top = 224
  end
  object QrK255: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,  '
      'CONCAT(gg1.Nome,   '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR, '
      'k255.*  '
      'FROM efdicmsipik255 k255 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k255.COD_ITEM  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   ')
    Left = 320
    Top = 176
    object QrK255Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK255Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK255NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK255ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK255AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK255Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK255PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK255KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK255KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK255KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK255KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK255KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK255KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK255IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK255IDSeq2: TIntegerField
      FieldName = 'IDSeq2'
    end
    object QrK255ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK255DT_CONS: TDateField
      FieldName = 'DT_CONS'
    end
    object QrK255COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK255QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK255COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrK255GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK255ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrK255OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK255Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK255DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK255DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK255UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK255UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK255AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK255Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK250: TDataSource
    DataSet = QrK250
    Left = 320
    Top = 128
  end
  object QrK250: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrK250BeforeClose
    AfterScroll = QrK250AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'IF(k250.DT_PROD = 0, "", '
      'DATE_FORMAT(k250.DT_PROD, "%d/%m/%Y")) DT_PROD_Txt,'
      'K250.* '
      'FROM efdicmsipiK250 k250 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K250.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  ')
    Left = 320
    Top = 80
    object QrK250Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK250Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK250NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK250DT_PROD_Txt: TWideStringField
      FieldName = 'DT_PROD_Txt'
      Size = 10
    end
    object QrK250ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK250AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK250Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK250PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK250KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK250KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK250KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK250KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK250KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK250KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK250IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK250ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK250DT_PROD: TDateField
      FieldName = 'DT_PROD'
    end
    object QrK250COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK250QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK250COD_DOC_OP: TWideStringField
      FieldName = 'COD_DOC_OP'
      Size = 30
    end
    object QrK250GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK250OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK250Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK250DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK250DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK250UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK250UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK250AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK250Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object Qr1010: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efdicmsipi1010')
    Left = 836
    Top = 544
    object Qr1010ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object Qr1010AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object Qr1010Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr1010LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object Qr1010REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object Qr1010IND_EXP: TWideStringField
      FieldName = 'IND_EXP'
      Size = 1
    end
    object Qr1010IND_CCRF: TWideStringField
      FieldName = 'IND_CCRF'
      Size = 1
    end
    object Qr1010IND_COMB: TWideStringField
      FieldName = 'IND_COMB'
      Size = 1
    end
    object Qr1010IND_USINA: TWideStringField
      FieldName = 'IND_USINA'
      Size = 1
    end
    object Qr1010IND_VA: TWideStringField
      FieldName = 'IND_VA'
      Size = 1
    end
    object Qr1010IND_EE: TWideStringField
      FieldName = 'IND_EE'
      Size = 1
    end
    object Qr1010IND_CART: TWideStringField
      FieldName = 'IND_CART'
      Size = 1
    end
    object Qr1010IND_FORM: TWideStringField
      FieldName = 'IND_FORM'
      Size = 1
    end
    object Qr1010IND_AER: TWideStringField
      FieldName = 'IND_AER'
      Size = 1
    end
    object Qr1010Lk: TIntegerField
      FieldName = 'Lk'
    end
    object Qr1010DataCad: TDateField
      FieldName = 'DataCad'
    end
    object Qr1010DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object Qr1010UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object Qr1010UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object Qr1010AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object Qr1010Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object Ds1010: TDataSource
    DataSet = Qr1010
    Left = 836
    Top = 588
  end
  object QrK270_220: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrK270_220BeforeClose
    AfterScroll = QrK270_220AfterScroll
    SQL.Strings = (
      'SELECT  '
      'CONCAT(gg1o.Nome, '
      'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), '
      'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) '
      'NO_PRD_TAM_COR, unmo.Sigla, '
      'K270.* '
      'FROM efdicmsipiK270 K270 '
      'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K270.COD_ITEM'
      'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC '
      'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad '
      'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI '
      'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 '
      'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed '
      'WHERE k270.ImporExpor=3'
      'AND k270.Empresa=-11'
      'AND k270.AnoMes>0'
      'AND k270.K100>0'
      'AND k270.ORIGEM>0'
      'ORDER BY k270.MovimCod, k270.DT_INI_AP, DT_FIN_AP, COD_ITEM')
    Left = 476
    Top = 108
    object QrK270_220Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK270_220NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK270_220ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK270_220AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK270_220Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK270_220PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK270_220IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK270_220KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK270_220KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK270_220KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK270_220KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK270_220KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK270_220KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK270_220DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrK270_220DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrK270_220COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrK270_220COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK270_220QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_220QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_220ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrK270_220GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK270_220OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK270_220OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK270_220OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK270_220Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK270_220DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK270_220DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK270_220UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK270_220UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK270_220AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK270_220Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK270_220: TDataSource
    DataSet = QrK270_220
    Left = 476
    Top = 152
  end
  object QrK275_220: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CONCAT(gg1o.Nome, '
      'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), '
      'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) '
      'NO_PRD_TAM_COR, unmo.Sigla, '
      'K275.* '
      'FROM efdicmsipiK275 K275 '
      'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K275.COD_ITEM '
      'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC '
      'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad '
      'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI '
      'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 '
      'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed '
      'WHERE k275.ImporExpor=3'
      'AND k275.Empresa=-11'
      'AND k275.AnoMes=201702'
      'AND k275.IDSeq1=1'
      'AND ORIGEM=5')
    Left = 476
    Top = 200
    object QrK275_220NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK275_220Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK275_220ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK275_220AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK275_220Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK275_220PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK275_220IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK275_220IDSeq2: TIntegerField
      FieldName = 'IDSeq2'
    end
    object QrK275_220KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK275_220KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK275_220KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK275_220KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK275_220KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK275_220KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK275_220ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK275_220COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK275_220QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK275_220QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK275_220COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrK275_220GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK275_220ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrK275_220ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrK275_220OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK275_220OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK275_220OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK275_220Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK275_220DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK275_220DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK275_220UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK275_220UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK275_220AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK275_220Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK275_220: TDataSource
    DataSet = QrK275_220
    Left = 476
    Top = 244
  end
  object frxE_K_VERIF_001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxE_K200_001GetValue
    Left = 484
    Top = 580
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsK_ConfG
        DataSetName = 'frxDsK_ConfG'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 48.000024410000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Shape3: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 971.339210000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 744.567410000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'K - MOVIMENTA'#199#195'O POR PRODUTO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA_SPED]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 857.953310000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 30.236240000000000000
          Top = 37.795300000000000000
          Width = 119.055118110236200000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Top = 37.795300000000000000
          Width = 30.236215590000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 899.528140000000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Final')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 149.291338582677200000
          Top = 37.795300000000000000
          Width = 32.125984250000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 648.188998350000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vendido')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 576.377952760000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 468.661417322834700000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Consumido')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 396.850393700787400000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Left = 289.133858267716500000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Produzido')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Left = 217.322834645669300000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Entrada')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 181.417322834645700000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inicial')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo17: TfrxMemoView
          Left = 935.433070866142000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Diferen'#231'a')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo28: TfrxMemoView
          Left = 755.906000000000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-produto')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo30: TfrxMemoView
          Left = 827.717070000000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Indevido')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo35: TfrxMemoView
          Left = 791.811077320000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Err. Empresa')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo38: TfrxMemoView
          Left = 720.000075670000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ETE')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo39: TfrxMemoView
          Left = 863.622078980000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Balan. indev.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo44: TfrxMemoView
          Left = 684.094930000000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Bal.corrigido')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Left = 432.755905511811000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saiu em desmonte')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo53: TfrxMemoView
          Left = 540.472440944881900000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Insu.cons.corr')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo54: TfrxMemoView
          Left = 325.039370078740200000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Prod.Conjunta')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo59: TfrxMemoView
          Left = 253.228346456692900000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Entrou de desmonte')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo62: TfrxMemoView
          Left = 612.283860000000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Corr.Produ.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo65: TfrxMemoView
          Left = 360.944881889763700000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Corr.Produ.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo70: TfrxMemoView
          Left = 504.566929133858200000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cons.Conjunto')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 10.204724410000000000
        Top = 128.504020000000000000
        Width = 971.339210000000000000
        DataSet = frxDsK_ConfG
        DataSetName = 'frxDsK_ConfG'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 30.236240000000000000
          Width = 119.055118110236200000
          Height = 10.204724410000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsK_ConfG."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 30.236213150000000000
          Height = 10.204724410000000000
          DataField = 'GraGruX'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK_ConfG."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 935.433070866142000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataField = 'Diferenca'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."Diferenca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 149.291338580000000000
          Width = 32.125984250000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsK_ConfG."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 899.528140000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataField = 'Final'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."Final"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 648.188998350000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataField = 'Venda'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."Venda"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 576.377952755905500000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataField = 'SaiuClasse'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."SaiuClasse"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 468.661417322834700000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataField = 'Consumo'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."Consumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 396.850393700787400000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."EntrouClasse"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 289.133858267716500000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."Producao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 217.322834645669300000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."Compra"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 181.417322830000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataField = 'Sdo_Ini'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."Sdo_Ini"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 755.906000000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataField = 'SubProduto'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."SubProduto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 827.717070000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataField = 'Indevido'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."Indevido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 791.811077320000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataField = 'ErrEmpresa'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."ErrEmpresa"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 720.000075670000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataField = 'ETE'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."ETE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 863.622078980000000000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."BalancoIndev"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 684.094930000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 432.755905511811000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."SaiuDesmonte"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 540.472440944881900000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataField = 'InsumReforma'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."InsumReforma"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 325.039370078740200000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."ProducaoCompartilhada"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 253.228346456692900000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."EntrouDesmonte"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 612.283860000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."InsumReforma"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 360.944881889763700000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataField = 'ProduReforma'
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."ProduReforma"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 504.566929130000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsK_ConfG."ConsumoCompartilhado"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 249.448980000000000000
        Width = 971.339210000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 706.772110000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 457.323066540000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 25.322844410000000000
        Top = 200.315090000000000000
        Width = 971.339210000000000000
        object Memo19: TfrxMemoView
          Left = 935.433070866142000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."Diferenca">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 791.811077320000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."ErrEmpresa">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 648.188998350000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."Venda">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 576.377952760000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."SaiuClasse">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 468.661417322834700000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."Consumo">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 396.850393700787400000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."EntrouClasse">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 289.133858267716500000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."Producao">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 217.322834645669300000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."Compra">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 181.417322834645700000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."Sdo_Ini">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 755.906000000000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."SubProduto">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 720.000075670000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."Indevido">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 899.528140000000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."Final">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 863.622078980000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."BalancoIndev">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 827.717070000000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."Indevido">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 684.094930000000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448800000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 432.755905511811000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."SaiuDesmonte">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 540.472440944881900000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."Insumreforma">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 504.566929130000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."ConsumoCompartilhado">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000010000
          Width = 149.291338582677200000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 253.228346456692900000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."EntrouDesmonte">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 612.283860000000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."InsumReforma">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 360.944881889763700000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."ProduReforma">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 325.039370078740200000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsK_ConfG
          DataSetName = 'frxDsK_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsK_ConfG."Producao">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsK_ConfG: TfrxDBDataset
    UserName = 'frxDsK_ConfG'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GraGruX=GraGruX'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Grandeza=Grandeza'
      'Sdo_Ini=Sdo_Ini'
      'Compra=Compra'
      'Producao=Producao'
      'EntrouClasse=EntrouClasse'
      'Consumo=Consumo'
      'SaiuClasse=SaiuClasse'
      'Venda=Venda'
      'Final=Final'
      'Diferenca=Diferenca'
      'Indevido=Indevido'
      'SubProduto=SubProduto'
      'ErrEmpresa=ErrEmpresa'
      'ETE=ETE'
      'BalancoIndev=BalancoIndev'
      'ESTSTabSorc=ESTSTabSorc'
      'SaiuDesmonte=SaiuDesmonte'
      'EntrouDesmonte=EntrouDesmonte'
      'ProduReforma=ProduReforma'
      'InsumReforma=InsumReforma'
      'ProducaoCompartilhada=ProducaoCompartilhada'
      'ConsumoCompartilhado=ConsumoCompartilhado')
    DataSet = QrK_ConfG
    BCDToCurrency = False
    Left = 484
    Top = 628
  end
  object QrK270_230: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrK270_230BeforeClose
    AfterScroll = QrK270_230AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'K270.* '
      'FROM efdicmsipiK270 k270 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k270.ImporExpor=3'
      'AND k270.Empresa=-11'
      '/*AND k270.AnoMes='
      'AND k270.K100=*/'
      'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ')
    Left = 536
    Top = 84
    object QrK270_230Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK270_230Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK270_230NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK270_230ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK270_230AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK270_230Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK270_230PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK270_230IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK270_230KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK270_230KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK270_230KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK270_230KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK270_230KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK270_230KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK270_230DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrK270_230DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrK270_230COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrK270_230COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK270_230QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_230QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_230ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrK270_230GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK270_230OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK270_230OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK270_230OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK270_230Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK270_230DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK270_230DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK270_230UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK270_230UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK270_230AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK270_230Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK270_230: TDataSource
    DataSet = QrK270_230
    Left = 536
    Top = 128
  end
  object QrK275_235: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,  '
      'CONCAT(gg1.Nome,   '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR, '
      'k275.*  '
      'FROM efdicmsipik275 k275 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   '
      'WHERE k275.ImporExpor=3'
      'AND k275.Empresa=-11'
      '/*AND k275.AnoMes='
      'AND k275.K230=*/')
    Left = 536
    Top = 176
    object QrK275_235Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK275_235Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK275_235NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK275_235ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK275_235AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK275_235Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK275_235PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK275_235IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK275_235IDSeq2: TIntegerField
      FieldName = 'IDSeq2'
    end
    object QrK275_235KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK275_235KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK275_235KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK275_235KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK275_235KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK275_235KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK275_235ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK275_235COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK275_235QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK275_235QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK275_235COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrK275_235GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK275_235ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrK275_235ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrK275_235OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK275_235OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK275_235OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK275_235Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK275_235DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK275_235DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK275_235UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK275_235UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK275_235AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK275_235Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrK275_235RegisPai: TWideStringField
      FieldName = 'RegisPai'
      Size = 4
    end
  end
  object DsK275_235: TDataSource
    DataSet = QrK275_235
    Left = 536
    Top = 224
  end
  object QrK270_250: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrK270_250BeforeClose
    AfterScroll = QrK270_250AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, K270.*'
      'FROM efdicmsipiK270 k270'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE k270.ImporExpor=3'
      'AND k270.Empresa=-11'
      'AND k270.AnoMes=201701'
      'AND k270.K100=1'
      'AND ORIGEM=2'
      'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM')
    Left = 596
    Top = 60
    object QrK270_250Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK270_250Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK270_250NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK270_250ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK270_250AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK270_250Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK270_250PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK270_250IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK270_250KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK270_250KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK270_250KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK270_250KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK270_250KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK270_250KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK270_250DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrK270_250DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrK270_250COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrK270_250COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK270_250QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_250QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_250ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrK270_250GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK270_250OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK270_250OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK270_250OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK270_250Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK270_250DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK270_250DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK270_250UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK270_250UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK270_250AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK270_250Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK270_250: TDataSource
    DataSet = QrK270_250
    Left = 596
    Top = 108
  end
  object QrK275_255: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,  '
      'CONCAT(gg1.Nome,   '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR, '
      'k275.*  '
      'FROM efdicmsipik275 k275 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   '
      'WHERE k275.ImporExpor=3'
      'AND k275.Empresa=-11'
      'AND k275.AnoMes=201701'
      'AND k275.K270=2')
    Left = 596
    Top = 156
    object QrK275_255NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK275_255Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK275_255ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK275_255AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK275_255Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK275_255PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK275_255IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK275_255IDSeq2: TIntegerField
      FieldName = 'IDSeq2'
    end
    object QrK275_255KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK275_255KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK275_255KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK275_255KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK275_255KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK275_255KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK275_255ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK275_255COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK275_255QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK275_255QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK275_255COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrK275_255GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK275_255ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrK275_255ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrK275_255OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK275_255OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK275_255OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK275_255Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK275_255DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK275_255DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK275_255UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK275_255UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK275_255AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK275_255Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrK275_255Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
  end
  object DsK275_255: TDataSource
    DataSet = QrK275_255
    Left = 596
    Top = 204
  end
  object QrK260: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrK260BeforeClose
    AfterScroll = QrK260AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR,'
      'IF(k260.DT_RET = 0, "",'
      'DATE_FORMAT(k260.DT_RET, "%d/%m/%Y")) DT_RET_Txt,'
      'K260.*'
      'FROM efdicmsipiK260 k260'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K260.COD_ITEM'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed')
    Left = 364
    Top = 104
    object QrK260Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK260Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK260NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK260DT_RET_Txt: TWideStringField
      FieldName = 'DT_RET_Txt'
      Size = 10
    end
    object QrK260ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK260AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK260Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK260PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK260KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK260KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK260KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK260KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK260KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK260KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK260IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK260ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK260COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrK260COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK260DT_SAIDA: TDateField
      FieldName = 'DT_SAIDA'
    end
    object QrK260QTD_SAIDA: TFloatField
      FieldName = 'QTD_SAIDA'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK260DT_RET: TDateField
      FieldName = 'DT_RET'
    end
    object QrK260QTD_RET: TFloatField
      FieldName = 'QTD_RET'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK260GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK260OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK260Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK260DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK260DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK260UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK260UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK260AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK260Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK260: TDataSource
    DataSet = QrK260
    Left = 364
    Top = 152
  end
  object QrK265: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'k265.* '
      'FROM efdicmsipik265 k265 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k265.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ')
    Left = 360
    Top = 196
    object QrK265Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK265Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK265NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK265ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK265AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK265Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK265PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK265KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK265KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK265KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK265KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK265KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK265KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK265IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK265IDSeq2: TIntegerField
      FieldName = 'IDSeq2'
    end
    object QrK265ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK265COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK265QTD_CONS: TFloatField
      FieldName = 'QTD_CONS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK265QTD_RET: TFloatField
      FieldName = 'QTD_RET'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK265GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK265ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrK265OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK265Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK265DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK265DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK265UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK265UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK265AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK265Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK265: TDataSource
    DataSet = QrK265
    Left = 360
    Top = 244
  end
  object QrK270_260: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrK270_260BeforeClose
    AfterScroll = QrK270_260AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, K270.*'
      'FROM efdicmsipiK270 k270'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE k270.ImporExpor=3'
      'AND k270.Empresa=-11'
      'AND k270.AnoMes=201701'
      'AND k270.K100=1'
      'AND ORIGEM=2'
      'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM')
    Left = 660
    Top = 84
    object QrK270_260Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK270_260Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK270_260NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK270_260ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK270_260AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK270_260Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK270_260PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK270_260IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK270_260KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK270_260KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK270_260KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK270_260KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK270_260KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK270_260KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK270_260DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrK270_260DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrK270_260COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrK270_260COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK270_260QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_260QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_260ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrK270_260GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK270_260OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK270_260OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK270_260OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK270_260Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK270_260DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK270_260DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK270_260UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK270_260UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK270_260AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK270_260Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK270_260: TDataSource
    DataSet = QrK270_260
    Left = 660
    Top = 132
  end
  object QrK275_265: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,  '
      'CONCAT(gg1.Nome,   '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR, '
      'k275.*  '
      'FROM efdicmsipik275 k275 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   '
      'WHERE k275.ImporExpor=3'
      'AND k275.Empresa=-11'
      'AND k275.AnoMes=201701'
      'AND k275.K270=2')
    Left = 660
    Top = 180
    object QrK275_265NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK275_265Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK275_265ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK275_265AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK275_265Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK275_265PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK275_265IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK275_265IDSeq2: TIntegerField
      FieldName = 'IDSeq2'
    end
    object QrK275_265KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK275_265KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK275_265KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK275_265KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK275_265KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK275_265KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK275_265ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK275_265COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK275_265QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK275_265QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK275_265COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrK275_265GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK275_265ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrK275_265ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrK275_265OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK275_265OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK275_265OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK275_265Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK275_265DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK275_265DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK275_265UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK275_265UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK275_265AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK275_265Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrK275_265Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
  end
  object DsK275_265: TDataSource
    DataSet = QrK275_265
    Left = 660
    Top = 228
  end
  object frxDsK270_230: TfrxDBDataset
    UserName = 'frxDsK270_230'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K100=K100'
      'ID_SEK=ID_SEK'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'DT_INI_AP=DT_INI_AP'
      'DT_FIN_AP=DT_FIN_AP'
      'COD_OP_OS=COD_OP_OS'
      'COD_ITEM=COD_ITEM'
      'QTD_COR_POS=QTD_COR_POS'
      'QTD_COR_NEG=QTD_COR_NEG'
      'ORIGEM=ORIGEM'
      'GraGruX=GraGruX'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrK270_230
    BCDToCurrency = False
    Left = 592
    Top = 512
  end
  object frxDsK275_235: TfrxDBDataset
    UserName = 'frxDsK275_235'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K270=K270'
      'ID_SEK=ID_SEK'
      'COD_ITEM=COD_ITEM'
      'QTD_COR_POS=QTD_COR_POS'
      'QTD_COR_NEG=QTD_COR_NEG'
      'COD_INS_SUBST=COD_INS_SUBST'
      'ID_Item=ID_Item'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Controle=Controle'
      'GraGruX=GraGruX'
      'ESTSTabSorc=ESTSTabSorc'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrK275_235
    BCDToCurrency = False
    Left = 592
    Top = 560
  end
  object frxDsK210: TfrxDBDataset
    UserName = 'frxDsK210'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'DT_FIN_OS_Txt=DT_FIN_OS_Txt'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K100=K100'
      'ID_SEK=ID_SEK'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'DT_INI_OS=DT_INI_OS'
      'DT_FIN_OS=DT_FIN_OS'
      'COD_DOC_OS=COD_DOC_OS'
      'COD_ITEM_ORI=COD_ITEM_ORI'
      'QTD_ORI=QTD_ORI'
      'GraGruX=GraGruX'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrK210
    BCDToCurrency = False
    Left = 632
    Top = 604
  end
  object frxDsK215: TfrxDBDataset
    UserName = 'frxDsK215'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K210=K210'
      'COD_ITEM_DES=COD_ITEM_DES'
      'QTD_DES=QTD_DES'
      'ID_Item=ID_Item'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Controle=Controle'
      'GraGruX=GraGruX'
      'ESTSTabSorc=ESTSTabSorc'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrK215
    BCDToCurrency = False
    Left = 632
    Top = 652
  end
  object QrK280: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(K280.COD_PART="", "", IF(ter.Tipo=0, '
      'ter.RazaoSocial, ter.Nome)) NO_PART, '
      'unm.Sigla, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      
        ' ELT(k280.IND_EST + 1,"Estoque de propriedade do informante e em' +
        ' seu poder","Estoque de propriedade do informante e em posse de ' +
        'terceiros","Estoque de propriedade de terceiros e em posse do in' +
        'formante","???") NO_IND_EST,'
      
        ' ELT(k280.KndTab + 1,"N/D","Mat.prima","Insumo","*Corre'#231#227'o*") NO' +
        '_KndTab,'
      
        ' ELT(k280.OriSPEDEFDKnd + 1,"Indefinido","Estoque","Classe","Pro' +
        'du'#231#227'o") NO_OriSPEDEFDKnd,'
      'K280.* '
      'FROM efdicmsipiK280 K280 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K280.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed '
      'LEFT JOIN entidades  ter ON ter.Codigo=K280.COD_PART '
      'WHERE k280.ImporExpor=3'
      'AND k280.Empresa=-11'
      'AND k280.AnoMes=201712'
      'AND k280.PeriApu=1'
      'ORDER BY DT_EST ')
    Left = 708
    Top = 108
    object QrK280NO_PART: TWideStringField
      FieldName = 'NO_PART'
      Size = 100
    end
    object QrK280Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK280NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK280NO_IND_EST: TWideStringField
      FieldName = 'NO_IND_EST'
      Size = 60
    end
    object QrK280NO_KndTab: TWideStringField
      FieldName = 'NO_KndTab'
    end
    object QrK280NO_OriSPEDEFDKnd: TWideStringField
      FieldName = 'NO_OriSPEDEFDKnd'
      Size = 10
    end
    object QrK280ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK280AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK280Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK280PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK280KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK280KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK280KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK280KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK280KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK280KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK280DT_EST: TDateField
      FieldName = 'DT_EST'
    end
    object QrK280COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK280QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK280QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK280IND_EST: TWideStringField
      FieldName = 'IND_EST'
      Size = 1
    end
    object QrK280COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrK280DebCred: TSmallintField
      FieldName = 'DebCred'
    end
    object QrK280GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK280Grandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrK280Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrK280AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrK280PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrK280Entidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrK280Tipo_Item: TIntegerField
      FieldName = 'Tipo_Item'
    end
    object QrK280RegisPai: TWideStringField
      FieldName = 'RegisPai'
      Size = 4
    end
    object QrK280RegisAvo: TWideStringField
      FieldName = 'RegisAvo'
      Size = 4
    end
    object QrK280ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrK280OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK280OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK280OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK280Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK280DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK280DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK280UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK280UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK280AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK280Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrK280IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK280OriBalID: TIntegerField
      FieldName = 'OriBalID'
    end
    object QrK280OriKndReg: TIntegerField
      FieldName = 'OriKndReg'
    end
    object QrK280ID_SEK: TIntegerField
      FieldName = 'ID_SEK'
    end
  end
  object DsK280: TDataSource
    DataSet = QrK280
    Left = 708
    Top = 156
  end
  object QrK_ConfG: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.GraGruX,  '
      '  SUM(vmi.Movim01) Sdo_Ini, '
      '  SUM(vmi.Movim02) Compra,  '
      '  SUM(vmi.Movim03) Producao, '
      '  SUM(vmi.Movim04) EntrouClasse,  '
      '  SUM(vmi.Movim05) Consumo, '
      '  SUM(vmi.Movim06) SaiuClasse,  '
      '  SUM(vmi.Movim07) Venda, '
      '  SUM(vmi.Movim08) Final,  '
      ' '
      '  SUM(vmi.Movim01) + '
      '  SUM(vmi.Movim02) + '
      '  SUM(vmi.Movim03) + '
      '  SUM(vmi.Movim04) + '
      '  SUM(vmi.Movim05) + '
      '  SUM(vmi.Movim06) + '
      '  SUM(vmi.Movim07) - '
      '  SUM(vmi.Movim08) Diferenca,  '
      'med.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, '
      'med.Grandeza   '
      'FROM efdicmsipik_confg vmi  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed  '
      'WHERE ImporExpor=3 '
      'AND AnoMes=201611 '
      'AND Empresa=-11 '
      'GROUP BY GraGruX '
      'ORDER BY GraGruX  ')
    Left = 600
    Top = 744
    object QrK_ConfGGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK_ConfGSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK_ConfGNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK_ConfGGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK_ConfGSdo_Ini: TFloatField
      FieldName = 'Sdo_Ini'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGCompra: TFloatField
      FieldName = 'Compra'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGProducao: TFloatField
      FieldName = 'Producao'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGEntrouClasse: TFloatField
      FieldName = 'EntrouClasse'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGConsumo: TFloatField
      FieldName = 'Consumo'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGSaiuClasse: TFloatField
      FieldName = 'SaiuClasse'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGVenda: TFloatField
      FieldName = 'Venda'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGFinal: TFloatField
      FieldName = 'Final'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGDiferenca: TFloatField
      FieldName = 'Diferenca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGIndevido: TFloatField
      FieldName = 'Indevido'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGSubProduto: TFloatField
      FieldName = 'SubProduto'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGErrEmpresa: TFloatField
      FieldName = 'ErrEmpresa'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGETE: TFloatField
      FieldName = 'ETE'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGBalancoIndev: TFloatField
      FieldName = 'BalancoIndev'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrK_ConfGSaiuDesmonte: TFloatField
      FieldName = 'SaiuDesmonte'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGEntrouDesmonte: TFloatField
      FieldName = 'EntrouDesmonte'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGProduReforma: TFloatField
      FieldName = 'ProduReforma'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGInsumReforma: TFloatField
      FieldName = 'InsumReforma'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGProducaoCompartilhada: TFloatField
      FieldName = 'ProducaoCompartilhada'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK_ConfGConsumoCompartilhado: TFloatField
      FieldName = 'ConsumoCompartilhado'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object DsK_ConfG: TDataSource
    DataSet = QrK_ConfG
    Left = 596
    Top = 792
  end
  object QrPsq01: TmySQLQuery
    Database = Dmod.MyDB
    Left = 668
    Top = 744
  end
  object DsPsq01: TDataSource
    DataSet = QrPsq01
    Left = 668
    Top = 796
  end
  object QrPsq02: TmySQLQuery
    Database = Dmod.MyDB
    Left = 724
    Top = 744
  end
  object DsPsq02: TDataSource
    DataSet = QrPsq02
    Left = 724
    Top = 796
  end
  object QrK270_210: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrK270_210BeforeClose
    AfterScroll = QrK270_210AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, K270.* '
      'FROM efdicmsipiK270 k270 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed '
      'WHERE k270.ImporExpor=3'
      'AND k270.Empresa=-11'
      'AND k270.AnoMes=201702'
      'AND k270.PeriApu=1'
      'AND k270.ORIGEM=3'
      'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ')
    Left = 416
    Top = 80
    object QrK270_210Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK270_210Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK270_210NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK270_210ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK270_210AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK270_210Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK270_210PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK270_210IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK270_210KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK270_210KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK270_210KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK270_210KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK270_210KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK270_210KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK270_210DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrK270_210DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrK270_210COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrK270_210COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK270_210QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_210QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_210ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrK270_210GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK270_210OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK270_210OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK270_210OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK270_210Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK270_210DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK270_210DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK270_210UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK270_210UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK270_210AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK270_210Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK270_210: TDataSource
    DataSet = QrK270_210
    Left = 416
    Top = 128
  end
  object QrK275_215: TmySQLQuery
    Database = Dmod.MyDB
    Left = 416
    Top = 176
    object QrK275_215NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK275_215Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK275_215ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK275_215AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK275_215Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK275_215PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK275_215IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK275_215IDSeq2: TIntegerField
      FieldName = 'IDSeq2'
    end
    object QrK275_215KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK275_215KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK275_215KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK275_215KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK275_215KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK275_215KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK275_215ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK275_215COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK275_215QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object DsK275_215: TDataSource
    DataSet = QrK275_215
    Left = 416
    Top = 224
  end
  object QrVMI: TmySQLQuery
    Database = Dmod.MyDB
    Left = 104
    Top = 680
  end
  object DsVMI: TDataSource
    DataSet = QrVMI
    Left = 104
    Top = 728
  end
  object QrK290: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrK290BeforeClose
    AfterScroll = QrK290AfterScroll
    SQL.Strings = (
      'SELECT IF(k290.DT_FIN_OP = 0, "", '
      'DATE_FORMAT(k290.DT_FIN_OP, "%d/%m/%Y")) DT_FIN_OP_Txt,'
      'K290.* '
      'FROM efdicmsipiK290 k290 '
      'WHERE k290.ImporExpor=3'
      'AND k290.Empresa=-11'
      'AND k290.AnoMes=201709'
      'AND k290.PeriApu=1'
      'ORDER BY DT_INI_OP, DT_FIN_OP, COD_DOC_OP')
    Left = 756
    Top = 32
    object QrK290DT_FIN_OP_Txt: TWideStringField
      FieldName = 'DT_FIN_OP_Txt'
      Size = 10
    end
    object QrK290ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK290AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK290Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK290PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK290KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK290KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK290KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK290KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK290KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK290KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK290IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK290ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK290DT_INI_OP: TDateField
      FieldName = 'DT_INI_OP'
    end
    object QrK290DT_FIN_OP: TDateField
      FieldName = 'DT_FIN_OP'
    end
    object QrK290COD_DOC_OP: TWideStringField
      FieldName = 'COD_DOC_OP'
      Size = 30
    end
    object QrK290OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK290Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK290DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK290DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK290UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK290UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK290AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK290Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK290: TDataSource
    DataSet = QrK290
    Left = 756
    Top = 80
  end
  object QrK300: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrK300BeforeClose
    AfterScroll = QrK300AfterScroll
    SQL.Strings = (
      'SELECT IF(k290.DT_FIN_OP = 0, "", '
      'DATE_FORMAT(k290.DT_FIN_OP, "%d/%m/%Y")) DT_FIN_OP_Txt,'
      'K290.* '
      'FROM efdicmsipiK290 k290 '
      'WHERE k290.ImporExpor=3'
      'AND k290.Empresa=-11'
      'AND k290.AnoMes=201709'
      'AND k290.PeriApu=1'
      'ORDER BY DT_INI_OP, DT_FIN_OP, COD_DOC_OP')
    Left = 804
    Top = 12
    object QrK300ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK300AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK300Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK300PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK300KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK300KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK300KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK300KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK300KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK300KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK300IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK300ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK300DT_PROD: TDateField
      FieldName = 'DT_PROD'
    end
    object QrK300OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK300Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK300DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK300DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK300UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK300UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK300AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK300Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrK300DT_PROD_Txt: TWideStringField
      FieldName = 'DT_PROD_Txt'
      Size = 10
    end
  end
  object DsK300: TDataSource
    DataSet = QrK300
    Left = 804
    Top = 60
  end
  object QrK291: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'IF(k230.DT_FIN_OP = 0, "", '
      'DATE_FORMAT(k230.DT_FIN_OP, "%d/%m/%Y")) DT_FIN_OP_Txt,'
      'K230.* '
      'FROM efdicmsipiK230 k230 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K230.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k230.ImporExpor=3'
      'AND k230.Empresa=-11'
      'AND k230.AnoMes=201702'
      'AND k230.PeriApu=1'
      'ORDER BY DT_INI_OP, DT_FIN_OP, COD_DOC_OP, COD_ITEM')
    Left = 756
    Top = 128
    object QrK291Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK291Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK291NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK291ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK291AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK291Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK291PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK291KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK291KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK291KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK291KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK291KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK291KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK291IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK291IDSeq2: TIntegerField
      FieldName = 'IDSeq2'
    end
    object QrK291ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK291COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK291QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK291GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK291OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK291Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK291DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK291DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK291UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK291UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK291AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK291Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK291: TDataSource
    DataSet = QrK291
    Left = 756
    Top = 172
  end
  object QrK292: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'IF(k230.DT_FIN_OP = 0, "", '
      'DATE_FORMAT(k230.DT_FIN_OP, "%d/%m/%Y")) DT_FIN_OP_Txt,'
      'K230.* '
      'FROM efdicmsipiK230 k230 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K230.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k230.ImporExpor=3'
      'AND k230.Empresa=-11'
      'AND k230.AnoMes=201702'
      'AND k230.PeriApu=1'
      'ORDER BY DT_INI_OP, DT_FIN_OP, COD_DOC_OP, COD_ITEM')
    Left = 756
    Top = 220
    object QrK292Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK292Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK292NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK292ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK292AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK292Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK292PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK292KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK292KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK292KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK292KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK292KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK292KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK292IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK292IDSeq2: TIntegerField
      FieldName = 'IDSeq2'
    end
    object QrK292ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK292COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK292QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK292GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK292OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK292Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK292DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK292DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK292UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK292UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK292AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK292Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK292: TDataSource
    DataSet = QrK292
    Left = 756
    Top = 268
  end
  object QrK301: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'IF(k230.DT_FIN_OP = 0, "", '
      'DATE_FORMAT(k230.DT_FIN_OP, "%d/%m/%Y")) DT_FIN_OP_Txt,'
      'K230.* '
      'FROM efdicmsipiK230 k230 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K230.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k230.ImporExpor=3'
      'AND k230.Empresa=-11'
      'AND k230.AnoMes=201702'
      'AND k230.PeriApu=1'
      'ORDER BY DT_INI_OP, DT_FIN_OP, COD_DOC_OP, COD_ITEM')
    Left = 804
    Top = 104
    object QrK301Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK301Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK301NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK301ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK301AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK301Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK301PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK301KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK301KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK301KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK301KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK301KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK301KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK301IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK301IDSeq2: TIntegerField
      FieldName = 'IDSeq2'
    end
    object QrK301ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK301COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK301QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK301GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK301OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK301Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK301DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK301DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK301UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK301UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK301AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK301Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK301: TDataSource
    DataSet = QrK301
    Left = 804
    Top = 148
  end
  object QrK302: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'IF(k230.DT_FIN_OP = 0, "", '
      'DATE_FORMAT(k230.DT_FIN_OP, "%d/%m/%Y")) DT_FIN_OP_Txt,'
      'K230.* '
      'FROM efdicmsipiK230 k230 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K230.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k230.ImporExpor=3'
      'AND k230.Empresa=-11'
      'AND k230.AnoMes=201702'
      'AND k230.PeriApu=1'
      'ORDER BY DT_INI_OP, DT_FIN_OP, COD_DOC_OP, COD_ITEM')
    Left = 804
    Top = 196
    object QrK302Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK302Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK302NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK302ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK302AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK302Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK302PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK302KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK302KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK302KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK302KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK302KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK302KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK302IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK302IDSeq2: TIntegerField
      FieldName = 'IDSeq2'
    end
    object QrK302ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrK302COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK302QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK302GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK302OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK302Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK302DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK302DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK302UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK302UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK302AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK302Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK302: TDataSource
    DataSet = QrK302
    Left = 804
    Top = 248
  end
  object QrK270_291: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'K270.* '
      'FROM efdicmsipiK270 k270 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k270.ImporExpor=3'
      'AND k270.Empresa=-11'
      '/*AND k270.AnoMes='
      'AND k270.K100=*/'
      'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ')
    Left = 864
    Top = 65528
    object QrK270_291Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK270_291Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK270_291NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK270_291ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK270_291AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK270_291Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK270_291PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK270_291IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK270_291KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK270_291KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK270_291KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK270_291KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK270_291KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK270_291KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK270_291DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrK270_291DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrK270_291COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrK270_291COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK270_291QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_291QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_291ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrK270_291GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK270_291OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK270_291OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK270_291OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK270_291Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK270_291DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK270_291DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK270_291UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK270_291UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK270_291AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK270_291Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK270_291: TDataSource
    DataSet = QrK270_291
    Left = 864
    Top = 36
  end
  object QrK270_292: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'K270.* '
      'FROM efdicmsipiK270 k270 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k270.ImporExpor=3'
      'AND k270.Empresa=-11'
      '/*AND k270.AnoMes='
      'AND k270.K100=*/'
      'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ')
    Left = 864
    Top = 84
    object QrK270_292Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK270_292Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK270_292NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK270_292ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK270_292AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK270_292Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK270_292PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK270_292IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK270_292KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK270_292KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK270_292KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK270_292KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK270_292KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK270_292KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK270_292DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrK270_292DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrK270_292COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrK270_292COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK270_292QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_292QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_292ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrK270_292GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK270_292OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK270_292OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK270_292OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK270_292Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK270_292DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK270_292DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK270_292UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK270_292UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK270_292AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK270_292Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK270_292: TDataSource
    DataSet = QrK270_292
    Left = 864
    Top = 128
  end
  object QrK270_301: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'K270.* '
      'FROM efdicmsipiK270 k270 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k270.ImporExpor=3'
      'AND k270.Empresa=-11'
      '/*AND k270.AnoMes='
      'AND k270.K100=*/'
      'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ')
    Left = 864
    Top = 176
    object QrK270_301Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK270_301Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK270_301NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK270_301ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK270_301AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK270_301Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK270_301PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK270_301IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK270_301KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK270_301KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK270_301KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK270_301KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK270_301KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK270_301KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK270_301DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrK270_301DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrK270_301COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrK270_301COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK270_301QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_301QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_301ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrK270_301GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK270_301OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK270_301OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK270_301OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK270_301Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK270_301DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK270_301DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK270_301UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK270_301UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK270_301AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK270_301Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK270_301: TDataSource
    DataSet = QrK270_301
    Left = 864
    Top = 224
  end
  object QrK270_302: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'K270.* '
      'FROM efdicmsipiK270 k270 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k270.ImporExpor=3'
      'AND k270.Empresa=-11'
      '/*AND k270.AnoMes='
      'AND k270.K100=*/'
      'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ')
    Left = 864
    Top = 272
    object QrK270_302Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrK270_302Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrK270_302NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrK270_302ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrK270_302AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrK270_302Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrK270_302PeriApu: TIntegerField
      FieldName = 'PeriApu'
    end
    object QrK270_302IDSeq1: TIntegerField
      FieldName = 'IDSeq1'
    end
    object QrK270_302KndTab: TIntegerField
      FieldName = 'KndTab'
    end
    object QrK270_302KndCod: TIntegerField
      FieldName = 'KndCod'
    end
    object QrK270_302KndNSU: TIntegerField
      FieldName = 'KndNSU'
    end
    object QrK270_302KndItm: TIntegerField
      FieldName = 'KndItm'
    end
    object QrK270_302KndAID: TIntegerField
      FieldName = 'KndAID'
    end
    object QrK270_302KndNiv: TIntegerField
      FieldName = 'KndNiv'
    end
    object QrK270_302DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrK270_302DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrK270_302COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrK270_302COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrK270_302QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_302QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrK270_302ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrK270_302GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrK270_302OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrK270_302OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrK270_302OriSPEDEFDKnd: TIntegerField
      FieldName = 'OriSPEDEFDKnd'
    end
    object QrK270_302Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrK270_302DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrK270_302DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrK270_302UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrK270_302UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrK270_302AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrK270_302Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsK270_302: TDataSource
    DataSet = QrK270_302
    Left = 864
    Top = 320
  end
end
