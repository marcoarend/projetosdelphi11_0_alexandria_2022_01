unit EfdIcmsIpiExporta_v03_0_2_a;
{
Se��o 9 � Importa��o de Blocos da EFD ICMS/IPI
O Programa de Valida��o e Assinatura da EFD-ICMS/IPI (PVA-EFD-ICMS/IPI), na vers�o 2.0.6 e seguintes,
permite a importa��o de qualquer bloco que esteja completo estruturalmente com sobreposi��o de todas as informa��es
existentes no bloco da EFD-ICMS/IPI anteriormente importada.
A op��o somente ser� disponibilizada quando a EFD-ICMS/IPI a ser alterada estiver aberta no PVA-EFDICMS/
IPI.O bloco a ser importado dever� estar estruturado, contendo:
1. o registro de abertura do arquivo digital e identifica��o da entidade (id�ntico ao da EFD-ICMS/IPI a ser
alterada);
2. o registro de abertura do bloco;
3. registros a serem inclu�dos; e
4. o registro de encerramento do bloco.
Ser�o validadas as informa��es constantes nos registros 0000 de ambos os arquivos.
A partir da vers�o 2.3.0 (publicada em novembro/2016), o PVA passou a permitir a sele��o m�ltipla de arquivos
para importa��o.
}

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnSPED_Geral, Variants,
  UnDmkProcFunc, DmkDAC_PF, UnDmkEnums, UnGrade_Jan, dmkImage, UnAppPF,
  UnEfdIcmsIpi_PF_v03_0_2_a, SPED_Listas, Vcl.Menus, frxClass, frxDBSet,
  UnEfdIcmsIpi_PF, UnSPED_PF;

type
  //
  TFmEfdIcmsIpiExporta_v03_0_2_a = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrParamsEmp: TMySQLQuery;
    QrParamsEmpSPED_EFD_IND_PERFIL: TWideStringField;
    QrParamsEmpSPED_EFD_IND_ATIV: TSmallintField;
    EdMes: TdmkEdit;
    LaMes: TLabel;
    QrEmpresa: TmySQLQuery;
    QrEmpresaTipo: TSmallintField;
    QrEmpresaENumero: TIntegerField;
    QrEmpresaPNumero: TIntegerField;
    QrEmpresaELograd: TSmallintField;
    QrEmpresaPLograd: TSmallintField;
    QrEmpresaECEP: TIntegerField;
    QrEmpresaPCEP: TIntegerField;
    QrEmpresaNOME_ENT: TWideStringField;
    QrEmpresaCodigo: TIntegerField;
    QrEmpresaCNPJ_CPF: TWideStringField;
    QrVersao: TMySQLQuery;
    QrCampos: TmySQLQuery;
    QrCamposBloco: TWideStringField;
    QrCamposRegistro: TWideStringField;
    QrCamposNumero: TIntegerField;
    QrCamposVersaoIni: TIntegerField;
    QrCamposVersaoFim: TIntegerField;
    QrCamposCampo: TWideStringField;
    QrCamposTipo: TWideStringField;
    QrCamposTam: TSmallintField;
    QrCamposTObrig: TSmallintField;
    QrCamposDecimais: TSmallintField;
    QrCamposEObrig: TWideStringField;
    QrCamposDescrLin1: TWideStringField;
    QrCamposDescrLin2: TWideStringField;
    QrCamposDescrLin3: TWideStringField;
    QrCamposDescrLin4: TWideStringField;
    QrCamposDescrLin5: TWideStringField;
    QrCamposDescrLin6: TWideStringField;
    QrCamposDescrLin7: TWideStringField;
    QrCamposDescrLin8: TWideStringField;
    RGCOD_FIN: TRadioGroup;
    QrEmpresaNOMEUF: TWideStringField;
    QrEmpresaIE: TWideStringField;
    QrEmpresaECodMunici: TIntegerField;
    QrEmpresaPCodMunici: TIntegerField;
    QrEmpresaNIRE: TWideStringField;
    QrEmpresaSUFRAMA: TWideStringField;
    QrEmpresaNO_2_ENT: TWideStringField;
    QrEmpresaNOMELOGRAD: TWideStringField;
    QrEmpresaRUA: TWideStringField;
    QrEmpresaCOMPL: TWideStringField;
    QrEmpresaBAIRRO: TWideStringField;
    QrEmpresaTE1: TWideStringField;
    QrEmpresaFAX: TWideStringField;
    QrEmpresaEMAIL: TWideStringField;
    QrParamsEmpSPED_EFD_CadContador: TIntegerField;
    QrParamsEmpSPED_EFD_CRCContador: TWideStringField;
    QrParamsEmpSPED_EFD_EscriContab: TIntegerField;
    QrParamsEmpSPED_EFD_EnderContab: TSmallintField;
    QrEnder: TmySQLQuery;
    QrEnderTipo: TSmallintField;
    QrEnderENumero: TIntegerField;
    QrEnderPNumero: TIntegerField;
    QrEnderELograd: TSmallintField;
    QrEnderPLograd: TSmallintField;
    QrEnderECEP: TIntegerField;
    QrEnderPCEP: TIntegerField;
    QrEnderIE: TWideStringField;
    QrEnderECodMunici: TIntegerField;
    QrEnderPCodMunici: TIntegerField;
    QrEnderNIRE: TWideStringField;
    QrEnderSUFRAMA: TWideStringField;
    QrEnderNOME_ENT: TWideStringField;
    QrEnderCNPJ_CPF: TWideStringField;
    QrEnderNOMEUF: TWideStringField;
    QrEnderNO_2_ENT: TWideStringField;
    QrEnderNOMELOGRAD: TWideStringField;
    QrEnderRUA: TWideStringField;
    QrEnderCOMPL: TWideStringField;
    QrEnderBAIRRO: TWideStringField;
    QrEnderTE1: TWideStringField;
    QrEnderFAX: TWideStringField;
    QrEnderEMAIL: TWideStringField;
    QrEnderCodigo: TIntegerField;
    QrParamsEmpNO_CTD: TWideStringField;
    QrParamsEmpNO_CTB: TWideStringField;
    QrParamsEmpCPF_CTD: TWideStringField;
    QrParamsEmpCNPJ_CTB: TWideStringField;
    QrSelEnt: TmySQLQuery;
    QrEnderECodiPais: TIntegerField;
    QrEnderPCodiPais: TIntegerField;
    QrUniMedi: TmySQLQuery;
    QrForca: TmySQLQuery;
    CkCorrigeCriadosAForca: TCheckBox;
    QrForcaDataFiscal: TDateField;
    QrForcaCriAForca: TSmallintField;
    QrForcaFatID: TIntegerField;
    QrForcaFatNum: TIntegerField;
    QrForcaEmpresa: TIntegerField;
    QrForcanItem: TIntegerField;
    QrForcaGGX_NfeI: TIntegerField;
    QrForcaprod_uTrib: TWideStringField;
    QrForcaGGX_SMIA: TIntegerField;
    QrGGX: TmySQLQuery;
    QrGGXUnidMed: TIntegerField;
    QrGGXSigla: TWideStringField;
    QrParamsEmpSPED_EFD_Path: TWideStringField;
    QrCorrige001: TmySQLQuery;
    QrCorrige001FatID: TIntegerField;
    QrCorrige001FatNum: TIntegerField;
    QrCorrige001Empresa: TIntegerField;
    QrCorrige001nItem: TIntegerField;
    QrCorrige001prod_cProd: TWideStringField;
    QrCorrige1x3: TmySQLQuery;
    QrReduzidos: TmySQLQuery;
    QrCorrige1x3FatID: TIntegerField;
    QrCorrige1x3FatNum: TIntegerField;
    QrCorrige1x3Empresa: TIntegerField;
    QrCorrige1x3nItem: TIntegerField;
    QrCorrige1x3Nivel1: TIntegerField;
    QrSPEDEFDIcmsIpiErrs: TmySQLQuery;
    QrProduto2: TmySQLQuery;
    QrProduto2Controle: TIntegerField;
    QrProduto2NO_PRD_TAM_COR: TWideStringField;
    QrProduto2cGTIN_EAN: TWideStringField;
    QrProduto2NCM: TWideStringField;
    QrProduto2EX_TIPI: TWideStringField;
    QrProduto2SIGLA: TWideStringField;
    QrProduto2Tipo_Item: TSmallintField;
    QrReduzidosGraGruX: TIntegerField;
    QrProduto2COD_LST: TWideStringField;
    QrProduto2SPEDEFD_ALIQ_ICMS: TFloatField;
    QrCabA_C: TmySQLQuery;
    QrCabA_CFatID: TIntegerField;
    QrCabA_CFatNum: TIntegerField;
    QrCabA_CEmpresa: TIntegerField;
    QrCabA_CIDCtrl: TIntegerField;
    QrCabA_CId: TWideStringField;
    QrCabA_Cide_indPag: TSmallintField;
    QrCabA_Cide_mod: TSmallintField;
    QrCabA_Cide_serie: TIntegerField;
    QrCabA_Cide_nNF: TIntegerField;
    QrCabA_Cide_dEmi: TDateField;
    QrCabA_Cide_dSaiEnt: TDateField;
    QrCabA_Cide_tpNF: TSmallintField;
    QrCabA_Cide_tpAmb: TSmallintField;
    QrCabA_Cide_finNFe: TSmallintField;
    QrCabA_Cemit_CNPJ: TWideStringField;
    QrCabA_Cemit_CPF: TWideStringField;
    QrCabA_Cdest_CNPJ: TWideStringField;
    QrCabA_Cdest_CPF: TWideStringField;
    QrCabA_CICMSTot_vBC: TFloatField;
    QrCabA_CICMSTot_vICMS: TFloatField;
    QrCabA_CICMSTot_vBCST: TFloatField;
    QrCabA_CICMSTot_vST: TFloatField;
    QrCabA_CICMSTot_vProd: TFloatField;
    QrCabA_CICMSTot_vFrete: TFloatField;
    QrCabA_CICMSTot_vSeg: TFloatField;
    QrCabA_CICMSTot_vDesc: TFloatField;
    QrCabA_CICMSTot_vII: TFloatField;
    QrCabA_CICMSTot_vIPI: TFloatField;
    QrCabA_CICMSTot_vPIS: TFloatField;
    QrCabA_CICMSTot_vCOFINS: TFloatField;
    QrCabA_CICMSTot_vOutro: TFloatField;
    QrCabA_CICMSTot_vNF: TFloatField;
    QrCabA_CISSQNtot_vServ: TFloatField;
    QrCabA_CISSQNtot_vBC: TFloatField;
    QrCabA_CISSQNtot_vISS: TFloatField;
    QrCabA_CISSQNtot_vPIS: TFloatField;
    QrCabA_CISSQNtot_vCOFINS: TFloatField;
    QrCabA_CRetTrib_vRetPIS: TFloatField;
    QrCabA_CRetTrib_vRetCOFINS: TFloatField;
    QrCabA_CRetTrib_vRetCSLL: TFloatField;
    QrCabA_CRetTrib_vBCIRRF: TFloatField;
    QrCabA_CRetTrib_vIRRF: TFloatField;
    QrCabA_CRetTrib_vBCRetPrev: TFloatField;
    QrCabA_CRetTrib_vRetPrev: TFloatField;
    QrCabA_CModFrete: TSmallintField;
    QrCabA_CCobr_Fat_nFat: TWideStringField;
    QrCabA_CCobr_Fat_vOrig: TFloatField;
    QrCabA_CCobr_Fat_vDesc: TFloatField;
    QrCabA_CCobr_Fat_vLiq: TFloatField;
    QrCabA_CInfAdic_InfAdFisco: TWideMemoField;
    QrCabA_CInfAdic_InfCpl: TWideMemoField;
    QrCabA_CExporta_UFEmbarq: TWideStringField;
    QrCabA_CExporta_XLocEmbarq: TWideStringField;
    QrCabA_CCompra_XNEmp: TWideStringField;
    QrCabA_CCompra_XPed: TWideStringField;
    QrCabA_CCompra_XCont: TWideStringField;
    QrCabA_CStatus: TIntegerField;
    QrCabA_CinfProt_Id: TWideStringField;
    QrCabA_CinfProt_cStat: TIntegerField;
    QrCabA_CinfCanc_cStat: TIntegerField;
    QrCabA_CDataFiscal: TDateField;
    QrCabA_CSINTEGRA_ExpDeclNum: TWideStringField;
    QrCabA_CSINTEGRA_ExpDeclDta: TDateField;
    QrCabA_CSINTEGRA_ExpNat: TWideStringField;
    QrCabA_CSINTEGRA_ExpRegNum: TWideStringField;
    QrCabA_CSINTEGRA_ExpRegDta: TDateField;
    QrCabA_CSINTEGRA_ExpConhNum: TWideStringField;
    QrCabA_CSINTEGRA_ExpConhDta: TDateField;
    QrCabA_CSINTEGRA_ExpConhTip: TWideStringField;
    QrCabA_CSINTEGRA_ExpPais: TWideStringField;
    QrCabA_CSINTEGRA_ExpAverDta: TDateField;
    QrCabA_CCodInfoEmit: TIntegerField;
    QrCabA_CCodInfoDest: TIntegerField;
    QrCabA_CCriAForca: TSmallintField;
    QrCabA_Cide_hSaiEnt: TTimeField;
    QrCabA_Cide_dhCont: TDateTimeField;
    QrCabA_Cemit_CRT: TSmallintField;
    QrCabA_Cdest_email: TWideStringField;
    QrCabA_CVagao: TWideStringField;
    QrCabA_CBalsa: TWideStringField;
    QrCabA_CCodInfoTrsp: TIntegerField;
    QrCabA_COrdemServ: TIntegerField;
    QrCabA_CSituacao: TSmallintField;
    QrCabA_CAntigo: TWideStringField;
    QrCabA_CNFG_Serie: TWideStringField;
    QrCabA_CNF_ICMSAlq: TFloatField;
    QrCabA_CNF_CFOP: TWideStringField;
    QrCabA_CImportado: TSmallintField;
    QrCabA_CNFG_SubSerie: TWideStringField;
    QrCabA_CNFG_ValIsen: TFloatField;
    QrCabA_CNFG_NaoTrib: TFloatField;
    QrCabA_CNFG_Outros: TFloatField;
    QrCabA_CCOD_MOD: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrCorrige002: TmySQLQuery;
    QrCorrige002FatID: TIntegerField;
    QrCorrige002FatNum: TIntegerField;
    QrCorrige002Empresa: TIntegerField;
    QrCorrige002IDCtrl: TIntegerField;
    QrCorrige002ide_mod: TSmallintField;
    QrCorrige002ide_serie: TIntegerField;
    QrCorrige002ide_nNF: TIntegerField;
    QrCorrige002ide_dEmi: TDateField;
    QrCorrige002ide_dSaiEnt: TDateField;
    QrCorrige002ide_finNFe: TSmallintField;
    QrCorrige002emit_CNPJ: TWideStringField;
    QrCorrige002emit_CPF: TWideStringField;
    QrCorrige002Status: TIntegerField;
    QrCorrige002NFG_Serie: TWideStringField;
    QrCorrige002NFG_SubSerie: TWideStringField;
    QrCorrige002COD_MOD: TWideStringField;
    QrCorrige002CodInfoEmit: TIntegerField;
    QrCorrige002ide_tpNF: TSmallintField;
    QrCorrige002DataFiscal: TDateField;
    QrCabA_CCOD_SIT: TSmallintField;
    QrCabA_CVL_ABAT_NT: TFloatField;
    QrCamposSObrig: TWideStringField;
    QrCamposCObrig: TWideStringField;
    QrCorrige002CodInfoDest: TIntegerField;
    QrItsI_: TMySQLQuery;
    QrItsI_nItem: TIntegerField;
    QrItsI_prod_CFOP: TIntegerField;
    QrItsI_GraGruX: TIntegerField;
    QrItsI_prod_xProd: TWideStringField;
    QrItsN: TmySQLQuery;
    QrItsNnItem: TIntegerField;
    QrItsNICMS_Orig: TSmallintField;
    QrItsNICMS_CST: TSmallintField;
    QrItsNICMS_vBC: TFloatField;
    QrItsNICMS_pICMS: TFloatField;
    QrItsNICMS_vICMS: TFloatField;
    QrItsNICMS_vBCST: TFloatField;
    QrItsNICMS_vICMSST: TFloatField;
    QrItsI_FatID: TIntegerField;
    QrItsI_FatNum: TIntegerField;
    QrItsI_Empresa: TIntegerField;
    QrItsI_prod_vProd: TFloatField;
    QrItsI_prod_vDesc: TFloatField;
    QrItsI_prod_NCM: TWideStringField;
    QrStqMovIts: TmySQLQuery;
    QrItsI_MeuID: TIntegerField;
    QrStqMovItsBaixa: TSmallintField;
    QrItsO: TmySQLQuery;
    QrItsOIND_APUR: TWideStringField;
    QrItsNCOD_NAT: TWideStringField;
    QrItsNICMS_pICMSST: TFloatField;
    QrItsOIPI_CST: TSmallintField;
    QrItsOIPI_vBC: TFloatField;
    QrItsOIPI_qUnid: TFloatField;
    QrItsOIPI_vUnid: TFloatField;
    QrItsOIPI_pIPI: TFloatField;
    QrItsOIPI_vIPI: TFloatField;
    QrItsOIPI_cEnq: TWideStringField;
    QrItsQ: TmySQLQuery;
    QrItsQPIS_CST: TSmallintField;
    QrItsQPIS_vBC: TFloatField;
    QrItsQPIS_pPIS: TFloatField;
    QrItsQPIS_vPIS: TFloatField;
    QrItsQPIS_qBCProd: TFloatField;
    QrItsQPIS_vAliqProd: TFloatField;
    QrItsS: TmySQLQuery;
    QrItsSCOFINS_CST: TSmallintField;
    QrItsSCOFINS_vBC: TFloatField;
    QrItsSCOFINS_pCOFINS: TFloatField;
    QrItsSCOFINS_qBCProd: TFloatField;
    QrItsSCOFINS_vAliqProd: TFloatField;
    QrItsSCOFINS_vCOFINS: TFloatField;
    QrGruI: TmySQLQuery;
    QrItsI_prod_uTrib: TWideStringField;
    QrItsI_prod_qTrib: TFloatField;
    QrCorrI: TmySQLQuery;
    QrCorrIFatID: TIntegerField;
    QrCorrIFatNum: TIntegerField;
    QrCorrIEmpresa: TIntegerField;
    QrCorrInItem: TIntegerField;
    QrCorrIGraGruX: TIntegerField;
    QrErrCFOP: TmySQLQuery;
    QrErrCFOPFatID: TIntegerField;
    QrErrCFOPFatNum: TIntegerField;
    QrErrCFOPEmpresa: TIntegerField;
    QrErrCFOPnItem: TIntegerField;
    QrErrCFOPprod_CFOP: TIntegerField;
    QrErrCFOPCodTxt: TWideStringField;
    QrErrCFOPCodigo: TIntegerField;
    QrErrCFOPNome: TWideStringField;
    QrErrCFOPDataIni: TDateField;
    QrErrCFOPDataFim: TDateField;
    QrCFOP: TmySQLQuery;
    QrAllI: TmySQLQuery;
    QrGruInCST: TSmallintField;
    QrGruInCFOP: TIntegerField;
    QrGruIValor: TFloatField;
    QrGruIprod_vProd: TFloatField;
    QrGruIIPI_vIPI: TFloatField;
    QrAllIValor: TFloatField;
    QrAllIprod_vProd: TFloatField;
    QrAllIIPI_vIPI: TFloatField;
    TabSheet3: TTabSheet;
    MeErros: TMemo;
    MeGerado: TMemo;
    Panel4: TPanel;
    QrBlocos: TmySQLQuery;
    QrBlocosBloco: TWideStringField;
    QrBlocosOrdem: TIntegerField;
    QrBlocosAtivo: TSmallintField;
    TVBlocos: TTreeView;
    QrErrMod: TmySQLQuery;
    QrErrModDataFiscal: TDateField;
    QrErrModFatID: TIntegerField;
    QrErrModFatNum: TIntegerField;
    QrErrModEmpresa: TIntegerField;
    QrErrModide_mod: TSmallintField;
    QrErrModide_serie: TIntegerField;
    QrErrModide_nNF: TIntegerField;
    QrErrModide_dEmi: TDateField;
    QrErrModICMSTot_vNF: TFloatField;
    QrErrModTerceiro: TLargeintField;
    QrErrModNO_TERCEIRO: TWideStringField;
    TabSheet4: TTabSheet;
    DsErrMod: TDataSource;
    DBGrid1: TDBGrid;
    Label4: TLabel;
    QrErrModIDCtrl: TIntegerField;
    QrEFD_E100: TmySQLQuery;
    QrEFD_E100DT_INI: TDateField;
    QrEFD_E100DT_FIN: TDateField;
    QrEFD_E110: TmySQLQuery;
    QrEFD_E110ImporExpor: TSmallintField;
    QrEFD_E110AnoMes: TIntegerField;
    QrEFD_E110Empresa: TIntegerField;
    QrEFD_E110LinArq: TIntegerField;
    QrEFD_E110E100: TIntegerField;
    QrEFD_E110REG: TWideStringField;
    QrEFD_E110VL_TOT_DEBITOS: TFloatField;
    QrEFD_E110VL_AJ_DEBITOS: TFloatField;
    QrEFD_E110VL_TOT_AJ_DEBITOS: TFloatField;
    QrEFD_E110VL_TOT_CREDITOS: TFloatField;
    QrEFD_E110VL_ESTORNOS_CRED: TFloatField;
    QrEFD_E110VL_AJ_CREDITOS: TFloatField;
    QrEFD_E110VL_TOT_AJ_CREDITOS: TFloatField;
    QrEFD_E110VL_ESTORNOS_DEB: TFloatField;
    QrEFD_E110VL_SLD_CREDOR_ANT: TFloatField;
    QrEFD_E110VL_SLD_APURADO: TFloatField;
    QrEFD_E110VL_TOT_DED: TFloatField;
    QrEFD_E110VL_ICMS_RECOLHER: TFloatField;
    QrEFD_E110VL_SLD_CREDOR_TRANSPORTAR: TFloatField;
    QrEFD_E110DEB_ESP: TFloatField;
    QrEFD_E100ImporExpor: TSmallintField;
    QrEFD_E100AnoMes: TIntegerField;
    QrEFD_E100Empresa: TIntegerField;
    QrEFD_E100LinArq: TIntegerField;
    QrEFD_E111: TmySQLQuery;
    QrEFD_E111ImporExpor: TSmallintField;
    QrEFD_E111AnoMes: TIntegerField;
    QrEFD_E111Empresa: TIntegerField;
    QrEFD_E111LinArq: TIntegerField;
    QrEFD_E111E110: TIntegerField;
    QrEFD_E111REG: TWideStringField;
    QrEFD_E111COD_AJ_APUR: TWideStringField;
    QrEFD_E111DESCR_COMPL_AJ: TWideStringField;
    QrEFD_E111VL_AJ_APUR: TFloatField;
    QrEFD_E115: TmySQLQuery;
    QrEFD_E115ImporExpor: TSmallintField;
    QrEFD_E115AnoMes: TIntegerField;
    QrEFD_E115Empresa: TIntegerField;
    QrEFD_E115LinArq: TIntegerField;
    QrEFD_E115E110: TIntegerField;
    QrEFD_E115REG: TWideStringField;
    QrEFD_E115COD_INF_ADIC: TWideStringField;
    QrEFD_E115VL_INF_ADIC: TFloatField;
    QrEFD_E115DESCR_COMPL_AJ: TWideStringField;
    QrEFD_E116: TmySQLQuery;
    QrEFD_E116ImporExpor: TSmallintField;
    QrEFD_E116AnoMes: TIntegerField;
    QrEFD_E116Empresa: TIntegerField;
    QrEFD_E116LinArq: TIntegerField;
    QrEFD_E116E110: TIntegerField;
    QrEFD_E116REG: TWideStringField;
    QrEFD_E116COD_OR: TWideStringField;
    QrEFD_E116VL_OR: TFloatField;
    QrEFD_E116DT_VCTO: TDateField;
    QrEFD_E116COD_REC: TWideStringField;
    QrEFD_E116NUM_PROC: TWideStringField;
    QrEFD_E116IND_PROC: TWideStringField;
    QrEFD_E116PROC: TWideStringField;
    QrEFD_E116TXT_COMPL: TWideStringField;
    QrEFD_E116MES_REF: TDateField;
    QrEFD_E112: TmySQLQuery;
    QrEFD_E112ImporExpor: TSmallintField;
    QrEFD_E112AnoMes: TIntegerField;
    QrEFD_E112Empresa: TIntegerField;
    QrEFD_E112LinArq: TIntegerField;
    QrEFD_E112E111: TIntegerField;
    QrEFD_E112REG: TWideStringField;
    QrEFD_E112NUM_DA: TWideStringField;
    QrEFD_E112NUM_PROC: TWideStringField;
    QrEFD_E112IND_PROC: TWideStringField;
    QrEFD_E112PROC: TWideStringField;
    QrEFD_E112TXT_COMPL: TWideStringField;
    QrEFD_E113: TmySQLQuery;
    QrEFD_E113ImporExpor: TSmallintField;
    QrEFD_E113AnoMes: TIntegerField;
    QrEFD_E113Empresa: TIntegerField;
    QrEFD_E113LinArq: TIntegerField;
    QrEFD_E113E111: TIntegerField;
    QrEFD_E113REG: TWideStringField;
    QrEFD_E113COD_PART: TWideStringField;
    QrEFD_E113COD_MOD: TWideStringField;
    QrEFD_E113SER: TWideStringField;
    QrEFD_E113SUB: TWideStringField;
    QrEFD_E113NUM_DOC: TIntegerField;
    QrEFD_E113DT_DOC: TDateField;
    QrEFD_E113COD_ITEM: TWideStringField;
    QrEFD_E113VL_AJ_ITEM: TFloatField;
    QrEFD_D100: TmySQLQuery;
    QrSelEntCodigo: TIntegerField;
    QrBlcs: TmySQLQuery;
    QrBlcsBloco: TWideStringField;
    QrBlcsRegistro: TWideStringField;
    QrBlcsNivel: TSmallintField;
    QrBlcsRegisPai: TWideStringField;
    QrBlcsOcorAciNiv: TIntegerField;
    QrBlcsOcorEstNiv: TIntegerField;
    QrBlcsImplementd: TSmallintField;
    QrBlcsOrdem: TIntegerField;
    QrBlcsAtivo: TSmallintField;
    QrNiv2: TmySQLQuery;
    QrNiv2Registro: TWideStringField;
    QrEFD_C500: TmySQLQuery;
    QrEFD_C500NO_TERC: TWideStringField;
    QrEFD_C500ImporExpor: TSmallintField;
    QrEFD_C500AnoMes: TIntegerField;
    QrEFD_C500Empresa: TIntegerField;
    QrEFD_C500LinArq: TIntegerField;
    QrEFD_C500REG: TWideStringField;
    QrEFD_C500IND_OPER: TWideStringField;
    QrEFD_C500IND_EMIT: TWideStringField;
    QrEFD_C500COD_PART: TWideStringField;
    QrEFD_C500COD_MOD: TWideStringField;
    QrEFD_C500COD_SIT: TWideStringField;
    QrEFD_C500SER: TWideStringField;
    QrEFD_C500SUB: TWideStringField;
    QrEFD_C500COD_CONS: TWideStringField;
    QrEFD_C500NUM_DOC: TIntegerField;
    QrEFD_C500DT_DOC: TDateField;
    QrEFD_C500DT_E_S: TDateField;
    QrEFD_C500VL_DOC: TFloatField;
    QrEFD_C500VL_DESC: TFloatField;
    QrEFD_C500VL_FORN: TFloatField;
    QrEFD_C500VL_SERV_NT: TFloatField;
    QrEFD_C500VL_TERC: TFloatField;
    QrEFD_C500VL_DA: TFloatField;
    QrEFD_C500VL_BC_ICMS: TFloatField;
    QrEFD_C500VL_ICMS: TFloatField;
    QrEFD_C500VL_BC_ICMS_ST: TFloatField;
    QrEFD_C500VL_ICMS_ST: TFloatField;
    QrEFD_C500COD_INF: TWideStringField;
    QrEFD_C500VL_PIS: TFloatField;
    QrEFD_C500VL_COFINS: TFloatField;
    QrEFD_C500TP_LIGACAO: TWideStringField;
    QrEFD_C500COD_GRUPO_TENSAO: TWideStringField;
    QrEFD_C500Terceiro: TIntegerField;
    QrEFD_C500Importado: TSmallintField;
    QrEFD_C500CST_ICMS: TWideStringField;
    QrEFD_C500CFOP: TWideStringField;
    QrEFD_C500ALIQ_ICMS: TFloatField;
    QrEFD_C500VL_RED_BC: TFloatField;
    QrEFD_D500: TmySQLQuery;
    QrEFD_D500ImporExpor: TSmallintField;
    QrEFD_D500AnoMes: TIntegerField;
    QrEFD_D500Empresa: TIntegerField;
    QrEFD_D500LinArq: TIntegerField;
    QrEFD_D500REG: TWideStringField;
    QrEFD_D500IND_OPER: TWideStringField;
    QrEFD_D500IND_EMIT: TWideStringField;
    QrEFD_D500COD_PART: TWideStringField;
    QrEFD_D500COD_MOD: TWideStringField;
    QrEFD_D500COD_SIT: TWideStringField;
    QrEFD_D500SER: TWideStringField;
    QrEFD_D500SUB: TWideStringField;
    QrEFD_D500NUM_DOC: TIntegerField;
    QrEFD_D500DT_DOC: TDateField;
    QrEFD_D500DT_A_P: TDateField;
    QrEFD_D500VL_DOC: TFloatField;
    QrEFD_D500VL_DESC: TFloatField;
    QrEFD_D500VL_SERV: TFloatField;
    QrEFD_D500VL_SERV_NT: TFloatField;
    QrEFD_D500VL_TERC: TFloatField;
    QrEFD_D500VL_DA: TFloatField;
    QrEFD_D500VL_BC_ICMS: TFloatField;
    QrEFD_D500VL_ICMS: TFloatField;
    QrEFD_D500COD_INF: TWideStringField;
    QrEFD_D500VL_PIS: TFloatField;
    QrEFD_D500VL_COFINS: TFloatField;
    QrEFD_D500COD_CTA: TWideStringField;
    QrEFD_D500TP_ASSINANTE: TWideStringField;
    QrEFD_D500Terceiro: TIntegerField;
    QrEFD_D500Importado: TSmallintField;
    QrEFD_D500CST_ICMS: TWideStringField;
    QrEFD_D500CFOP: TWideStringField;
    QrEFD_D500ALIQ_ICMS: TFloatField;
    QrEFD_D500VL_RED_BC: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel47: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    GBConfirma: TGroupBox;
    Panel16: TPanel;
    StatusBar: TStatusBar;
    PainelConfirma: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    BtErros: TBitBtn;
    RGPreenche: TRadioGroup;
    QrGruIICMS_vBC: TFloatField;
    QrGruIICMS_vICMS: TFloatField;
    QrGruIICMS_vBCST: TFloatField;
    QrGruIICMS_vICMSST: TFloatField;
    QrAllIICMS_vBC: TFloatField;
    QrAllIICMS_vICMS: TFloatField;
    QrAllIICMS_vBCST: TFloatField;
    QrAllIICMS_vICMSST: TFloatField;
    CkRecriaRegAotmaticos: TCheckBox;
    QrParamsEmpSPED_EFD_DtFiscal: TSmallintField;
    QrItsNICMS_vICMSOp: TFloatField;
    QrItsNICMS_pDif: TFloatField;
    QrItsNICMS_vICMSDif: TFloatField;
    QrItsNICMS_vICMSDeson: TFloatField;
    QrItsNICMS_modBC: TSmallintField;
    QrItsNICMS_pRedBC: TFloatField;
    QrItsNICMS_modBCST: TSmallintField;
    QrItsNICMS_pMVAST: TFloatField;
    QrItsNICMS_pRedBCST: TFloatField;
    QrItsNICMS_CSOSN: TIntegerField;
    QrItsNICMS_UFST: TWideStringField;
    QrItsNICMS_pBCOp: TFloatField;
    QrItsNICMS_vBCSTRet: TFloatField;
    QrItsNICMS_vICMSSTRet: TFloatField;
    QrItsNICMS_motDesICMS: TSmallintField;
    QrItsNICMS_pCredSN: TFloatField;
    QrItsNICMS_vCredICMSSN: TFloatField;
    QrGruIpICMS: TFloatField;
    QrVersaoCodTxt: TWideStringField;
    QrEFD_D100ImporExpor: TSmallintField;
    QrEFD_D100AnoMes: TIntegerField;
    QrEFD_D100Empresa: TIntegerField;
    QrEFD_D100LinArq: TIntegerField;
    QrEFD_D100REG: TWideStringField;
    QrEFD_D100IND_OPER: TWideStringField;
    QrEFD_D100IND_EMIT: TWideStringField;
    QrEFD_D100COD_PART: TWideStringField;
    QrEFD_D100COD_MOD: TWideStringField;
    QrEFD_D100COD_SIT: TWideStringField;
    QrEFD_D100SER: TWideStringField;
    QrEFD_D100SUB: TWideStringField;
    QrEFD_D100NUM_DOC: TIntegerField;
    QrEFD_D100CHV_CTE: TWideStringField;
    QrEFD_D100DT_DOC: TDateField;
    QrEFD_D100DT_A_P: TDateField;
    QrEFD_D100TP_CTE: TSmallintField;
    QrEFD_D100CHV_CTE_REF: TWideStringField;
    QrEFD_D100VL_DOC: TFloatField;
    QrEFD_D100VL_DESC: TFloatField;
    QrEFD_D100IND_FRT: TWideStringField;
    QrEFD_D100VL_SERV: TFloatField;
    QrEFD_D100VL_BC_ICMS: TFloatField;
    QrEFD_D100VL_ICMS: TFloatField;
    QrEFD_D100VL_NT: TFloatField;
    QrEFD_D100COD_INF: TWideStringField;
    QrEFD_D100COD_CTA: TWideStringField;
    QrEFD_D100Terceiro: TIntegerField;
    QrEFD_D100Importado: TSmallintField;
    QrEFD_D100Lk: TIntegerField;
    QrEFD_D100DataCad: TDateField;
    QrEFD_D100DataAlt: TDateField;
    QrEFD_D100UserCad: TIntegerField;
    QrEFD_D100UserAlt: TIntegerField;
    QrEFD_D100AlterWeb: TSmallintField;
    QrEFD_D100Ativo: TSmallintField;
    QrEFD_D100CST_ICMS: TIntegerField;
    QrEFD_D100CFOP: TIntegerField;
    QrEFD_D100ALIQ_ICMS: TFloatField;
    QrEFD_D100VL_RED_BC: TFloatField;
    QrEFD_E500: TmySQLQuery;
    QrEFD_E500ImporExpor: TSmallintField;
    QrEFD_E500AnoMes: TIntegerField;
    QrEFD_E500Empresa: TIntegerField;
    QrEFD_E500LinArq: TIntegerField;
    QrEFD_E500IND_APUR: TWideStringField;
    QrEFD_E500DT_INI: TDateField;
    QrEFD_E500DT_FIN: TDateField;
    QrEFD_E520: TmySQLQuery;
    QrEFD_E520ImporExpor: TSmallintField;
    QrEFD_E520AnoMes: TIntegerField;
    QrEFD_E520Empresa: TIntegerField;
    QrEFD_E520LinArq: TIntegerField;
    QrEFD_E520E500: TIntegerField;
    QrEFD_E520REG: TWideStringField;
    QrEFD_E520VL_SD_ANT_IPI: TFloatField;
    QrEFD_E520VL_DEB_IPI: TFloatField;
    QrEFD_E520VL_CRED_IPI: TFloatField;
    QrEFD_E520VL_OD_IPI: TFloatField;
    QrEFD_E520VL_OC_IPI: TFloatField;
    QrEFD_E520VL_SC_IPI: TFloatField;
    QrEFD_E520VL_SD_IPI: TFloatField;
    QrEFD_E530: TmySQLQuery;
    QrEFD_E530ImporExpor: TSmallintField;
    QrEFD_E530AnoMes: TIntegerField;
    QrEFD_E530Empresa: TIntegerField;
    QrEFD_E530LinArq: TIntegerField;
    QrEFD_E530E520: TIntegerField;
    QrEFD_E530REG: TWideStringField;
    QrEFD_E530IND_AJ: TWideStringField;
    QrEFD_E530VL_AJ: TFloatField;
    QrEFD_E530COD_AJ: TWideStringField;
    QrEFD_E530IND_DOC: TWideStringField;
    QrEFD_E530NUM_DOC: TWideStringField;
    QrEFD_E530DESCR_AJ: TWideStringField;
    QrEFD_H005: TmySQLQuery;
    QrEFD_H005ImporExpor: TSmallintField;
    QrEFD_H005AnoMes: TIntegerField;
    QrEFD_H005Empresa: TIntegerField;
    QrEFD_H005LinArq: TIntegerField;
    QrEFD_H005REG: TWideStringField;
    QrEFD_H005DT_INV: TDateField;
    QrEFD_H005VL_INV: TFloatField;
    QrEFD_H005MOT_INV: TWideStringField;
    QrEFD_H005Lk: TIntegerField;
    QrEFD_H005DataCad: TDateField;
    QrEFD_H005DataAlt: TDateField;
    QrEFD_H005UserCad: TIntegerField;
    QrEFD_H005UserAlt: TIntegerField;
    QrEFD_H005AlterWeb: TSmallintField;
    QrEFD_H005Ativo: TSmallintField;
    QrEFD_H010: TmySQLQuery;
    QrEFD_H010ImporExpor: TSmallintField;
    QrEFD_H010AnoMes: TIntegerField;
    QrEFD_H010Empresa: TIntegerField;
    QrEFD_H010H005: TIntegerField;
    QrEFD_H010LinArq: TIntegerField;
    QrEFD_H010REG: TWideStringField;
    QrEFD_H010COD_ITEM: TWideStringField;
    QrEFD_H010UNID: TWideStringField;
    QrEFD_H010QTD: TFloatField;
    QrEFD_H010VL_UNIT: TFloatField;
    QrEFD_H010VL_ITEM: TFloatField;
    QrEFD_H010IND_PROP: TWideStringField;
    QrEFD_H010COD_PART: TWideStringField;
    QrEFD_H010TXT_COMPL: TWideStringField;
    QrEFD_H010COD_CTA: TWideStringField;
    QrEFD_H010Lk: TIntegerField;
    QrEFD_H010DataCad: TDateField;
    QrEFD_H010DataAlt: TDateField;
    QrEFD_H010UserCad: TIntegerField;
    QrEFD_H010UserAlt: TIntegerField;
    QrEFD_H010AlterWeb: TSmallintField;
    QrEFD_H010Ativo: TSmallintField;
    QrEFD_H010VL_ITEM_IR: TFloatField;
    QrEFD_H020: TmySQLQuery;
    QrEFD_H020ImporExpor: TSmallintField;
    QrEFD_H020AnoMes: TIntegerField;
    QrEFD_H020Empresa: TIntegerField;
    QrEFD_H020H010: TIntegerField;
    QrEFD_H020LinArq: TIntegerField;
    QrEFD_H020REG: TWideStringField;
    QrEFD_H020CST_ICMS: TIntegerField;
    QrEFD_H020BC_ICMS: TFloatField;
    QrEFD_H020VL_ICMS: TFloatField;
    QrEFD_H020Lk: TIntegerField;
    QrEFD_H020DataCad: TDateField;
    QrEFD_H020DataAlt: TDateField;
    QrEFD_H020UserCad: TIntegerField;
    QrEFD_H020UserAlt: TIntegerField;
    QrEFD_H020AlterWeb: TSmallintField;
    QrEFD_H020Ativo: TSmallintField;
    QrProduto1: TmySQLQuery;
    QrProduto1Controle: TIntegerField;
    QrProduto1NO_PRD_TAM_COR: TWideStringField;
    QrProduto1cGTIN_EAN: TWideStringField;
    QrProduto1NCM: TWideStringField;
    QrProduto1EX_TIPI: TWideStringField;
    QrProduto1COD_LST: TWideStringField;
    QrProduto1SIGLA: TWideStringField;
    QrProduto1Tipo_Item: TSmallintField;
    QrProduto1SPEDEFD_ALIQ_ICMS: TFloatField;
    QrEFD_K100: TmySQLQuery;
    QrEFD_K100DT_INI: TDateField;
    QrEFD_K100DT_FIN: TDateField;
    QrEFD_K200: TmySQLQuery;
    QrEFD_K220: TmySQLQuery;
    QrEFD_K220DT_MOV: TDateField;
    QrEFD_K220COD_ITEM_ORI: TWideStringField;
    QrEFD_K220COD_ITEM_DEST: TWideStringField;
    QrEFD_K220QTD: TFloatField;
    QrEFD_K230: TmySQLQuery;
    QrEFD_K235: TmySQLQuery;
    QrEFD_K235DT_SAIDA: TDateField;
    QrEFD_K235COD_ITEM: TWideStringField;
    QrEFD_K235QTD: TFloatField;
    QrEFD_K235COD_INS_SUBST: TWideStringField;
    QrEFD_K250: TmySQLQuery;
    QrEFD_K250DT_PROD: TDateField;
    QrEFD_K250COD_ITEM: TWideStringField;
    QrEFD_K250QTD: TFloatField;
    QrEFD_K255: TmySQLQuery;
    QrEFD_K255DT_CONS: TDateField;
    QrEFD_K255COD_ITEM: TWideStringField;
    QrEFD_K255QTD: TFloatField;
    QrEFD_K255COD_INS_SUBST: TWideStringField;
    QrEFD_K100PeriApu: TIntegerField;
    QrProduto2prod_CEST: TIntegerField;
    QrEFD_K220ID_SEK: TIntegerField;
    QrGraGru1Cons: TmySQLQuery;
    QrProduto2GraGru1: TIntegerField;
    QrGraGru1ConsQtd_Comp: TFloatField;
    QrGraGru1ConsPerda: TFloatField;
    QrPsq: TmySQLQuery;
    QrEFD_K235ID_SEK: TIntegerField;
    QrEFD_K255ID_SEK: TIntegerField;
    QrGraGru1ConsGGX_CONSU: TIntegerField;
    QrEFD_1010: TmySQLQuery;
    QrEFD_1010ImporExpor: TSmallintField;
    QrEFD_1010AnoMes: TIntegerField;
    QrEFD_1010Empresa: TIntegerField;
    QrEFD_1010LinArq: TIntegerField;
    QrEFD_1010REG: TWideStringField;
    QrEFD_1010IND_EXP: TWideStringField;
    QrEFD_1010IND_CCRF: TWideStringField;
    QrEFD_1010IND_COMB: TWideStringField;
    QrEFD_1010IND_USINA: TWideStringField;
    QrEFD_1010IND_VA: TWideStringField;
    QrEFD_1010IND_EE: TWideStringField;
    QrEFD_1010IND_CART: TWideStringField;
    QrEFD_1010IND_FORM: TWideStringField;
    QrEFD_1010IND_AER: TWideStringField;
    QrEFD_1010Lk: TIntegerField;
    QrEFD_1010DataCad: TDateField;
    QrEFD_1010DataAlt: TDateField;
    QrEFD_1010UserCad: TIntegerField;
    QrEFD_1010UserAlt: TIntegerField;
    QrEFD_1010AlterWeb: TSmallintField;
    QrEFD_1010Ativo: TSmallintField;
    QrParamsEmpSPED_EFD_ID_0200: TSmallintField;
    QrParamsEmpSPED_EFD_ID_0150: TSmallintField;
    QrEnt: TmySQLQuery;
    QrNFeEFD_C170: TmySQLQuery;
    QrNFeEFD_C170FatID: TIntegerField;
    QrNFeEFD_C170FatNum: TIntegerField;
    QrNFeEFD_C170Empresa: TIntegerField;
    QrNFeEFD_C170nItem: TIntegerField;
    QrNFeEFD_C170GraGruX: TIntegerField;
    QrNFeEFD_C170COD_ITEM: TWideStringField;
    QrNFeEFD_C170DESCR_COMPL: TWideStringField;
    QrNFeEFD_C170QTD: TFloatField;
    QrNFeEFD_C170UNID: TWideStringField;
    QrNFeEFD_C170VL_ITEM: TFloatField;
    QrNFeEFD_C170VL_DESC: TFloatField;
    QrNFeEFD_C170IND_MOV: TWideStringField;
    QrNFeEFD_C170CST_ICMS: TIntegerField;
    QrNFeEFD_C170CFOP: TIntegerField;
    QrNFeEFD_C170COD_NAT: TWideStringField;
    QrNFeEFD_C170VL_BC_ICMS: TFloatField;
    QrNFeEFD_C170ALIQ_ICMS: TFloatField;
    QrNFeEFD_C170VL_ICMS: TFloatField;
    QrNFeEFD_C170VL_BC_ICMS_ST: TFloatField;
    QrNFeEFD_C170ALIQ_ST: TFloatField;
    QrNFeEFD_C170VL_ICMS_ST: TFloatField;
    QrNFeEFD_C170IND_APUR: TWideStringField;
    QrNFeEFD_C170CST_IPI: TWideStringField;
    QrNFeEFD_C170COD_ENQ: TWideStringField;
    QrNFeEFD_C170VL_BC_IPI: TFloatField;
    QrNFeEFD_C170ALIQ_IPI: TFloatField;
    QrNFeEFD_C170VL_IPI: TFloatField;
    QrNFeEFD_C170CST_PIS: TSmallintField;
    QrNFeEFD_C170VL_BC_PIS: TFloatField;
    QrNFeEFD_C170ALIQ_PIS_p: TFloatField;
    QrNFeEFD_C170QUANT_BC_PIS: TFloatField;
    QrNFeEFD_C170ALIQ_PIS_r: TFloatField;
    QrNFeEFD_C170VL_PIS: TFloatField;
    QrNFeEFD_C170CST_COFINS: TSmallintField;
    QrNFeEFD_C170VL_BC_COFINS: TFloatField;
    QrNFeEFD_C170ALIQ_COFINS_p: TFloatField;
    QrNFeEFD_C170QUANT_BC_COFINS: TFloatField;
    QrNFeEFD_C170ALIQ_COFINS_r: TFloatField;
    QrNFeEFD_C170VL_COFINS: TFloatField;
    QrNFeEFD_C170COD_CTA: TWideStringField;
    QrNFeEFD_C170Lk: TIntegerField;
    QrNFeEFD_C170DataCad: TDateField;
    QrNFeEFD_C170DataAlt: TDateField;
    QrNFeEFD_C170UserCad: TIntegerField;
    QrNFeEFD_C170UserAlt: TIntegerField;
    QrNFeEFD_C170AlterWeb: TSmallintField;
    QrNFeEFD_C170Ativo: TSmallintField;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedNome: TWideStringField;
    QrUnidMedSigla: TWideStringField;
    QrUniMediNoUnidMed: TWideStringField;
    Qr0220: TmySQLQuery;
    Qr0220Sigla: TWideStringField;
    Qr0220UNID: TWideStringField;
    Qr0220prod_qTrib: TFloatField;
    Qr0220QTD: TFloatField;
    Qr0220FATOR: TFloatField;
    Panel5: TPanel;
    SbBloco_K_estoque: TSpeedButton;
    QrEFD_K230ImporExpor: TSmallintField;
    QrEFD_K230AnoMes: TIntegerField;
    QrEFD_K230Empresa: TIntegerField;
    QrEFD_K230PeriApu: TIntegerField;
    QrEFD_K230KndTab: TIntegerField;
    QrEFD_K230KndCod: TIntegerField;
    QrEFD_K230KndNSU: TIntegerField;
    QrEFD_K230KndItm: TIntegerField;
    QrEFD_K230KndAID: TIntegerField;
    QrEFD_K230KndNiv: TIntegerField;
    QrEFD_K230IDSeq1: TIntegerField;
    QrEFD_K230ID_SEK: TSmallintField;
    QrEFD_K230DT_INI_OP: TDateField;
    QrEFD_K230DT_FIN_OP: TDateField;
    QrEFD_K230COD_DOC_OP: TWideStringField;
    QrEFD_K230COD_ITEM: TWideStringField;
    QrEFD_K230QTD_ENC: TFloatField;
    QrEFD_K230GraGruX: TIntegerField;
    QrEFD_K230OriOpeProc: TSmallintField;
    QrEFD_K230Lk: TIntegerField;
    QrEFD_K230DataCad: TDateField;
    QrEFD_K230DataAlt: TDateField;
    QrEFD_K230UserCad: TIntegerField;
    QrEFD_K230UserAlt: TIntegerField;
    QrEFD_K230AlterWeb: TSmallintField;
    QrEFD_K230Ativo: TSmallintField;
    QrEFD_K230AWServerID: TIntegerField;
    QrEFD_K230AWStatSinc: TSmallintField;
    SbTeste_minimo_EFD_ICMS_IPI: TSpeedButton;
    PMBloco_K_estoque: TPopupMenu;
    Comcadastros1: TMenuItem;
    N1: TMenuItem;
    QrEFD_K280: TmySQLQuery;
    QrEFD_K200DT_EST: TDateField;
    QrEFD_K200COD_ITEM: TWideStringField;
    QrEFD_K200IND_EST: TWideStringField;
    QrEFD_K200COD_PART: TWideStringField;
    QrEFD_K200QTD: TFloatField;
    QrEFD_K200Entidade: TIntegerField;
    QrEFD_K280DT_EST: TDateField;
    QrEFD_K280COD_ITEM: TWideStringField;
    QrEFD_K280IND_EST: TWideStringField;
    QrEFD_K280COD_PART: TWideStringField;
    QrEFD_K280QTD_COR_POS: TFloatField;
    QrEFD_K280QTD_COR_NEG: TFloatField;
    QrEFD_K280Entidade: TIntegerField;
    QrEFD_K290: TmySQLQuery;
    QrEFD_K300: TmySQLQuery;
    QrEFD_K290DT_INI_OP: TDateField;
    QrEFD_K290DT_FIN_OP: TDateField;
    QrEFD_K290COD_DOC_OP: TWideStringField;
    QrEFD_K300DT_PROD: TDateField;
    QrEFD_K301: TmySQLQuery;
    QrEFD_K302: TmySQLQuery;
    QrEFD_K301COD_ITEM: TWideStringField;
    QrEFD_K301QTD: TFloatField;
    QrEFD_K302COD_ITEM: TWideStringField;
    QrEFD_K302QTD: TFloatField;
    QrEFD_K291: TmySQLQuery;
    QrEFD_K291COD_ITEM: TWideStringField;
    QrEFD_K291QTD: TFloatField;
    QrEFD_K292: TmySQLQuery;
    QrEFD_K292COD_ITEM: TWideStringField;
    QrEFD_K292QTD: TFloatField;
    BtMes: TSpeedButton;
    SbBloco_K_completo: TSpeedButton;
    PMBloco_K_completo: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    BtEstoque: TBitBtn;
    frxSEII_Estq: TfrxReport;
    QrSEII_0190: TmySQLQuery;
    QrSEII_0190REG: TWideStringField;
    QrSEII_0190UNID: TWideStringField;
    QrSEII_0190DESCR: TWideStringField;
    QrSEII_0190Ativo: TSmallintField;
    frxDsSEII_0190: TfrxDBDataset;
    QrSEII_0200: TmySQLQuery;
    QrSEII_0200REG: TWideStringField;
    QrSEII_0200COD_ITEM: TWideStringField;
    QrSEII_0200DESCR_ITEM: TWideStringField;
    QrSEII_0200COD_BARRA: TWideStringField;
    QrSEII_0200COD_ANT_ITEM: TWideStringField;
    QrSEII_0200UNID_INV: TWideStringField;
    QrSEII_0200TIPO_ITEM: TSmallintField;
    QrSEII_0200COD_NCM: TWideStringField;
    QrSEII_0200EX_IPI: TWideStringField;
    QrSEII_0200COD_GEN: TSmallintField;
    QrSEII_0200COD_LST: TWideStringField;
    QrSEII_0200ALIQ_ICMS: TFloatField;
    QrSEII_0200CEST: TIntegerField;
    QrSEII_0200Ativo: TSmallintField;
    QrSEII_0200GraGruX: TIntegerField;
    frxDsSEII_0200: TfrxDBDataset;
    frxDsSEII_K280: TfrxDBDataset;
    QrSEII_K280: TmySQLQuery;
    QrSEII_K280REG: TWideStringField;
    QrSEII_K280DT_EST: TDateField;
    QrSEII_K280COD_ITEM: TWideStringField;
    QrSEII_K280QTD_COR_POS: TFloatField;
    QrSEII_K280QTD_COR_NEG: TFloatField;
    QrSEII_K280IND_EST: TWideStringField;
    QrSEII_K280COD_PART: TWideStringField;
    QrSEII_K280Ativo: TSmallintField;
    QrSEII_K280NO_IND_EST: TWideStringField;
    QrSEII_K280NO_TERCEIRO: TWideStringField;
    frxDsSEII_k200: TfrxDBDataset;
    QrSEII_K200: TmySQLQuery;
    QrSEII_K200REG: TWideStringField;
    QrSEII_K200DT_EST: TDateField;
    QrSEII_K200COD_ITEM: TWideStringField;
    QrSEII_K200QTD: TFloatField;
    QrSEII_K200IND_EST: TWideStringField;
    QrSEII_K200COD_PART: TWideStringField;
    QrSEII_K200Ativo: TSmallintField;
    QrSEII_K200NO_TERCEIRO: TWideStringField;
    QrSEII_K200NO_IND_EST: TWideStringField;
    QrSEII_K280DESCR_ITEM: TWideStringField;
    QrSEII_K200DESCR_ITEM: TWideStringField;
    QrSEII_K200GRUPOS: TWideStringField;
    QrSEII_K280GRUPOS: TWideStringField;
    QrSEII_K200UNID_INV: TWideStringField;
    QrSEII_K280UNID_INV: TWideStringField;
    QrEFD_K210: TmySQLQuery;
    QrEFD_K215: TmySQLQuery;
    QrEFD_K215COD_ITEM_DES: TWideStringField;
    QrEFD_K215QTD_DES: TFloatField;
    QrEFD_K215ID_SEK: TSmallintField;
    QrEFD_K210ImporExpor: TSmallintField;
    QrEFD_K210AnoMes: TIntegerField;
    QrEFD_K210Empresa: TIntegerField;
    QrEFD_K210PeriApu: TIntegerField;
    QrEFD_K210KndTab: TIntegerField;
    QrEFD_K210KndCod: TIntegerField;
    QrEFD_K210KndNSU: TIntegerField;
    QrEFD_K210KndItm: TIntegerField;
    QrEFD_K210KndAID: TIntegerField;
    QrEFD_K210KndNiv: TIntegerField;
    QrEFD_K210IDSeq1: TIntegerField;
    QrEFD_K210ID_SEK: TSmallintField;
    QrEFD_K210DT_INI_OS: TDateField;
    QrEFD_K210DT_FIN_OS: TDateField;
    QrEFD_K210COD_DOC_OS: TWideStringField;
    QrEFD_K210COD_ITEM_ORI: TWideStringField;
    QrEFD_K210QTD_ORI: TFloatField;
    QrEFD_K210GraGruX: TIntegerField;
    QrEFD_K210OriOpeProc: TSmallintField;
    QrEFD_K210OrigemIDKnd: TIntegerField;
    QrEFD_K210Lk: TIntegerField;
    QrEFD_K210DataCad: TDateField;
    QrEFD_K210DataAlt: TDateField;
    QrEFD_K210UserCad: TIntegerField;
    QrEFD_K210UserAlt: TIntegerField;
    QrEFD_K210AlterWeb: TSmallintField;
    QrEFD_K210Ativo: TSmallintField;
    QrEFD_K210AWServerID: TIntegerField;
    QrEFD_K210AWStatSinc: TSmallintField;
    QrEfdInnNFsCab: TMySQLQuery;
    QrEfdInnNFsCabMovFatID: TIntegerField;
    QrEfdInnNFsCabMovFatNum: TIntegerField;
    QrEfdInnNFsCabMovimCod: TIntegerField;
    QrEfdInnNFsCabEmpresa: TIntegerField;
    QrEfdInnNFsCabControle: TIntegerField;
    QrEfdInnNFsCabTerceiro: TIntegerField;
    QrEfdInnNFsCabPecas: TFloatField;
    QrEfdInnNFsCabPesoKg: TFloatField;
    QrEfdInnNFsCabAreaM2: TFloatField;
    QrEfdInnNFsCabAreaP2: TFloatField;
    QrEfdInnNFsCabValorT: TFloatField;
    QrEfdInnNFsCabMotorista: TIntegerField;
    QrEfdInnNFsCabPlaca: TWideStringField;
    QrEfdInnNFsCabCOD_MOD: TSmallintField;
    QrEfdInnNFsCabCOD_SIT: TSmallintField;
    QrEfdInnNFsCabSER: TIntegerField;
    QrEfdInnNFsCabNUM_DOC: TIntegerField;
    QrEfdInnNFsCabCHV_NFE: TWideStringField;
    QrEfdInnNFsCabNFeStatus: TIntegerField;
    QrEfdInnNFsCabDT_DOC: TDateField;
    QrEfdInnNFsCabDT_E_S: TDateField;
    QrEfdInnNFsCabVL_DOC: TFloatField;
    QrEfdInnNFsCabIND_PGTO: TWideStringField;
    QrEfdInnNFsCabVL_DESC: TFloatField;
    QrEfdInnNFsCabVL_ABAT_NT: TFloatField;
    QrEfdInnNFsCabVL_MERC: TFloatField;
    QrEfdInnNFsCabIND_FRT: TWideStringField;
    QrEfdInnNFsCabVL_FRT: TFloatField;
    QrEfdInnNFsCabVL_SEG: TFloatField;
    QrEfdInnNFsCabVL_OUT_DA: TFloatField;
    QrEfdInnNFsCabVL_BC_ICMS: TFloatField;
    QrEfdInnNFsCabVL_ICMS: TFloatField;
    QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField;
    QrEfdInnNFsCabVL_ICMS_ST: TFloatField;
    QrEfdInnNFsCabVL_IPI: TFloatField;
    QrEfdInnNFsCabVL_PIS: TFloatField;
    QrEfdInnNFsCabVL_COFINS: TFloatField;
    QrEfdInnNFsCabVL_PIS_ST: TFloatField;
    QrEfdInnNFsCabVL_COFINS_ST: TFloatField;
    QrEfdInnNFsCabNFe_FatID: TIntegerField;
    QrEfdInnNFsCabNFe_FatNum: TIntegerField;
    QrEfdInnNFsCabNFe_StaLnk: TSmallintField;
    QrEfdInnNFsCabVSVmcWrn: TSmallintField;
    QrEfdInnNFsCabVSVmcObs: TWideStringField;
    QrEfdInnNFsCabVSVmcSeq: TWideStringField;
    QrEfdInnNFsCabVSVmcSta: TSmallintField;
    QrEfdInnNFsCabIsLinked: TSmallintField;
    QrEfdInnNFsCabSqLinked: TIntegerField;
    QrEfdInnNFsCabCliInt: TIntegerField;
    QrCabA_CAtrelaFatID: TIntegerField;
    QrCabA_CAtrelaStaLnk: TSmallintField;
    QrEfdInnNFsIts: TMySQLQuery;
    QrEfdInnNFsItsMovFatID: TIntegerField;
    QrEfdInnNFsItsMovFatNum: TIntegerField;
    QrEfdInnNFsItsMovimCod: TIntegerField;
    QrEfdInnNFsItsEmpresa: TIntegerField;
    QrEfdInnNFsItsMovimNiv: TIntegerField;
    QrEfdInnNFsItsMovimTwn: TIntegerField;
    QrEfdInnNFsItsControle: TIntegerField;
    QrEfdInnNFsItsConta: TIntegerField;
    QrEfdInnNFsItsGraGru1: TIntegerField;
    QrEfdInnNFsItsGraGruX: TIntegerField;
    QrEfdInnNFsItsprod_vProd: TFloatField;
    QrEfdInnNFsItsprod_vFrete: TFloatField;
    QrEfdInnNFsItsprod_vSeg: TFloatField;
    QrEfdInnNFsItsprod_vOutro: TFloatField;
    QrEfdInnNFsItsQTD: TFloatField;
    QrEfdInnNFsItsUNID: TWideStringField;
    QrEfdInnNFsItsVL_ITEM: TFloatField;
    QrEfdInnNFsItsVL_DESC: TFloatField;
    QrEfdInnNFsItsIND_MOV: TWideStringField;
    QrEfdInnNFsItsCFOP: TIntegerField;
    QrEfdInnNFsItsCOD_NAT: TWideStringField;
    QrEfdInnNFsItsVL_BC_ICMS: TFloatField;
    QrEfdInnNFsItsALIQ_ICMS: TFloatField;
    QrEfdInnNFsItsVL_ICMS: TFloatField;
    QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField;
    QrEfdInnNFsItsALIQ_ST: TFloatField;
    QrEfdInnNFsItsVL_ICMS_ST: TFloatField;
    QrEfdInnNFsItsIND_APUR: TWideStringField;
    QrEfdInnNFsItsCOD_ENQ: TWideStringField;
    QrEfdInnNFsItsVL_BC_IPI: TFloatField;
    QrEfdInnNFsItsALIQ_IPI: TFloatField;
    QrEfdInnNFsItsVL_IPI: TFloatField;
    QrEfdInnNFsItsVL_BC_PIS: TFloatField;
    QrEfdInnNFsItsALIQ_PIS_p: TFloatField;
    QrEfdInnNFsItsQUANT_BC_PIS: TFloatField;
    QrEfdInnNFsItsALIQ_PIS_r: TFloatField;
    QrEfdInnNFsItsVL_PIS: TFloatField;
    QrEfdInnNFsItsVL_BC_COFINS: TFloatField;
    QrEfdInnNFsItsALIQ_COFINS_p: TFloatField;
    QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField;
    QrEfdInnNFsItsALIQ_COFINS_r: TFloatField;
    QrEfdInnNFsItsVL_COFINS: TFloatField;
    QrEfdInnNFsItsCOD_CTA: TWideStringField;
    QrEfdInnNFsItsVL_ABAT_NT: TFloatField;
    QrEfdInnNFsItsDtCorrApo: TDateTimeField;
    QrEfdInnNFsItsxLote: TWideStringField;
    QrEfdInnNFsItsOri_IPIpIPI: TFloatField;
    QrEfdInnNFsItsOri_IPIvIPI: TFloatField;
    QrEfdInnNFsItsIsLinked: TSmallintField;
    QrEfdInnNFsItsSqLinked: TIntegerField;
    QrEfdInnNFsItsSqLnkID: TIntegerField;
    QrItsI_prod_cProd: TWideStringField;
    QrItsI_prod_cEAN: TWideStringField;
    QrItsI_prod_cBarra: TWideStringField;
    QrItsI_prod_CEST: TIntegerField;
    QrItsI_prod_indEscala: TWideStringField;
    QrItsI_prod_CNPJFab: TWideStringField;
    QrItsI_prod_cBenef: TWideStringField;
    QrItsI_prod_EXTIPI: TWideStringField;
    QrItsI_prod_genero: TSmallintField;
    QrItsI_prod_uCom: TWideStringField;
    QrItsI_prod_qCom: TFloatField;
    QrItsI_prod_vUnCom: TFloatField;
    QrItsI_prod_cEANTrib: TWideStringField;
    QrItsI_prod_cBarraTrib: TWideStringField;
    QrItsI_prod_vUnTrib: TFloatField;
    QrItsI_prod_vFrete: TFloatField;
    QrItsI_prod_vSeg: TFloatField;
    QrItsI_prod_vOutro: TFloatField;
    QrItsI_prod_indTot: TSmallintField;
    QrItsI_prod_xPed: TWideStringField;
    QrItsI_prod_nItemPed: TIntegerField;
    QrItsI_Tem_IPI: TSmallintField;
    QrItsI__Ativo_: TSmallintField;
    QrItsI_InfAdCuztm: TIntegerField;
    QrItsI_EhServico: TIntegerField;
    QrItsI_UsaSubsTrib: TSmallintField;
    QrItsI_ICMSRec_pRedBC: TFloatField;
    QrItsI_ICMSRec_vBC: TFloatField;
    QrItsI_ICMSRec_pAliq: TFloatField;
    QrItsI_ICMSRec_vICMS: TFloatField;
    QrItsI_IPIRec_pRedBC: TFloatField;
    QrItsI_IPIRec_vBC: TFloatField;
    QrItsI_IPIRec_pAliq: TFloatField;
    QrItsI_IPIRec_vIPI: TFloatField;
    QrItsI_PISRec_pRedBC: TFloatField;
    QrItsI_PISRec_vBC: TFloatField;
    QrItsI_PISRec_pAliq: TFloatField;
    QrItsI_PISRec_vPIS: TFloatField;
    QrItsI_COFINSRec_pRedBC: TFloatField;
    QrItsI_COFINSRec_vBC: TFloatField;
    QrItsI_COFINSRec_pAliq: TFloatField;
    QrItsI_COFINSRec_vCOFINS: TFloatField;
    QrItsI_Nivel1: TIntegerField;
    QrItsI_UnidMedCom: TIntegerField;
    QrItsI_UnidMedTrib: TIntegerField;
    QrItsI_ICMSRec_vBCST: TFloatField;
    QrItsI_ICMSRec_vICMSST: TFloatField;
    QrItsI_ICMSRec_pAliqST: TFloatField;
    QrItsI_Tem_II: TSmallintField;
    QrItsI_prod_nFCI: TWideStringField;
    QrItsI_StqMovValA: TIntegerField;
    QrItsI_AtrelaID: TIntegerField;
    QrCabA_CFisRegCad: TIntegerField;
    QrFisRegMvt: TMySQLQuery;
    QrFisRegMvtTipoCalc: TSmallintField;
    QrIndMov: TMySQLQuery;
    QrIndMovIND_MOV: TFloatField;
    QrI: TMySQLQuery;
    QrIFatID: TIntegerField;
    QrIFatNum: TIntegerField;
    QrIEmpresa: TIntegerField;
    QrInItem: TIntegerField;
    QrIprod_cProd: TWideStringField;
    QrIprod_cEAN: TWideStringField;
    QrIprod_cBarra: TWideStringField;
    QrIprod_xProd: TWideStringField;
    QrIprod_NCM: TWideStringField;
    QrIprod_CEST: TIntegerField;
    QrIprod_indEscala: TWideStringField;
    QrIprod_CNPJFab: TWideStringField;
    QrIprod_cBenef: TWideStringField;
    QrIprod_EXTIPI: TWideStringField;
    QrIprod_genero: TSmallintField;
    QrIprod_CFOP: TIntegerField;
    QrIprod_uCom: TWideStringField;
    QrIprod_qCom: TFloatField;
    QrIprod_vUnCom: TFloatField;
    QrIprod_vProd: TFloatField;
    QrIprod_cEANTrib: TWideStringField;
    QrIprod_cBarraTrib: TWideStringField;
    QrIprod_uTrib: TWideStringField;
    QrIprod_qTrib: TFloatField;
    QrIprod_vUnTrib: TFloatField;
    QrIprod_vFrete: TFloatField;
    QrIprod_vSeg: TFloatField;
    QrIprod_vDesc: TFloatField;
    QrIprod_vOutro: TFloatField;
    QrIprod_indTot: TSmallintField;
    QrIprod_xPed: TWideStringField;
    QrIprod_nItemPed: TIntegerField;
    QrITem_IPI: TSmallintField;
    QrI_Ativo_: TSmallintField;
    QrIInfAdCuztm: TIntegerField;
    QrIEhServico: TIntegerField;
    QrIUsaSubsTrib: TSmallintField;
    QrIICMSRec_pRedBC: TFloatField;
    QrIICMSRec_vBC: TFloatField;
    QrIICMSRec_pAliq: TFloatField;
    QrIICMSRec_vICMS: TFloatField;
    QrIIPIRec_pRedBC: TFloatField;
    QrIIPIRec_vBC: TFloatField;
    QrIIPIRec_pAliq: TFloatField;
    QrIIPIRec_vIPI: TFloatField;
    QrIPISRec_pRedBC: TFloatField;
    QrIPISRec_vBC: TFloatField;
    QrIPISRec_pAliq: TFloatField;
    QrIPISRec_vPIS: TFloatField;
    QrICOFINSRec_pRedBC: TFloatField;
    QrICOFINSRec_vBC: TFloatField;
    QrICOFINSRec_pAliq: TFloatField;
    QrICOFINSRec_vCOFINS: TFloatField;
    QrIMeuID: TIntegerField;
    QrINivel1: TIntegerField;
    QrIGraGruX: TIntegerField;
    QrIUnidMedCom: TIntegerField;
    QrIUnidMedTrib: TIntegerField;
    QrIICMSRec_vBCST: TFloatField;
    QrIICMSRec_vICMSST: TFloatField;
    QrIICMSRec_pAliqST: TFloatField;
    QrITem_II: TSmallintField;
    QrIprod_nFCI: TWideStringField;
    QrIStqMovValA: TIntegerField;
    QrIAtrelaID: TIntegerField;
    QrIICMS_Orig: TSmallintField;
    QrIICMS_CST: TSmallintField;
    QrIICMS_vBC: TFloatField;
    QrIICMS_pICMS: TFloatField;
    QrIICMS_vICMS: TFloatField;
    QrIICMS_vBCST: TFloatField;
    QrIICMS_pICMSST: TFloatField;
    QrIICMS_vICMSST: TFloatField;
    QrIIPI_cEnq: TWideStringField;
    QrIIND_APUR: TWideStringField;
    QrIIPI_CST: TSmallintField;
    QrIIPI_vBC: TFloatField;
    QrIIPI_pIPI: TFloatField;
    QrIIPI_vIPI: TFloatField;
    QrIPIS_CST: TSmallintField;
    QrIPIS_vBC: TFloatField;
    QrIPIS_pPIS: TFloatField;
    QrIPIS_vPIS: TFloatField;
    QrIPISST_vBC: TFloatField;
    QrIPISST_pPIS: TFloatField;
    QrIPISST_vPIS: TFloatField;
    QrICOFINS_CST: TSmallintField;
    QrICOFINS_vBC: TFloatField;
    QrICOFINS_pCOFINS: TFloatField;
    QrICOFINS_vCOFINS: TFloatField;
    QrICOFINSST_vBC: TFloatField;
    QrICOFINSST_pCOFINS: TFloatField;
    QrICOFINSST_vCOFINS: TFloatField;
    QrAtrela0053: TMySQLQuery;
    QrAtrela0053NFe_FatNum: TIntegerField;
    QrAtrela0053FatID: TIntegerField;
    QrAtrela0053FatNum: TIntegerField;
    QrAtrela0053Empresa: TIntegerField;
    QrAtrela0053ide_dEmi: TDateField;
    QrAtrela0053NFe_FatID: TIntegerField;
    QrAtrela0053DataFiscalNul: TDateField;
    QrAtrela0053DataFiscalInn: TDateField;
    QrCabA_CAtrelaFatNum: TIntegerField;
    QrEFD_1010IND_GIAF1: TWideStringField;
    QrEFD_1010IND_GIAF3: TWideStringField;
    QrEFD_1010IND_GIAF4: TWideStringField;
    QrEFD_1010IND_REST_RESSARC_COMPL_ICMS: TWideStringField;
    QrAnal0053: TMySQLQuery;
    QrEfdInnCTsCab: TMySQLQuery;
    QrEfdInnCTsCabMovFatID: TIntegerField;
    QrEfdInnCTsCabMovFatNum: TIntegerField;
    QrEfdInnCTsCabMovimCod: TIntegerField;
    QrEfdInnCTsCabEmpresa: TIntegerField;
    QrEfdInnCTsCabControle: TIntegerField;
    QrEfdInnCTsCabIsLinked: TSmallintField;
    QrEfdInnCTsCabSqLinked: TIntegerField;
    QrEfdInnCTsCabCliInt: TIntegerField;
    QrEfdInnCTsCabTerceiro: TIntegerField;
    QrEfdInnCTsCabPecas: TFloatField;
    QrEfdInnCTsCabPesoKg: TFloatField;
    QrEfdInnCTsCabAreaM2: TFloatField;
    QrEfdInnCTsCabAreaP2: TFloatField;
    QrEfdInnCTsCabValorT: TFloatField;
    QrEfdInnCTsCabMotorista: TIntegerField;
    QrEfdInnCTsCabPlaca: TWideStringField;
    QrEfdInnCTsCabIND_OPER: TWideStringField;
    QrEfdInnCTsCabIND_EMIT: TWideStringField;
    QrEfdInnCTsCabCOD_MOD: TWideStringField;
    QrEfdInnCTsCabCOD_SIT: TSmallintField;
    QrEfdInnCTsCabSER: TWideStringField;
    QrEfdInnCTsCabSUB: TWideStringField;
    QrEfdInnCTsCabNUM_DOC: TIntegerField;
    QrEfdInnCTsCabCHV_CTE: TWideStringField;
    QrEfdInnCTsCabCTeStatus: TIntegerField;
    QrEfdInnCTsCabDT_DOC: TDateField;
    QrEfdInnCTsCabDT_A_P: TDateField;
    QrEfdInnCTsCabTP_CT_e: TSmallintField;
    QrEfdInnCTsCabCHV_CTE_REF: TWideStringField;
    QrEfdInnCTsCabVL_DOC: TFloatField;
    QrEfdInnCTsCabVL_DESC: TFloatField;
    QrEfdInnCTsCabIND_FRT: TWideStringField;
    QrEfdInnCTsCabVL_SERV: TFloatField;
    QrEfdInnCTsCabVL_BC_ICMS: TFloatField;
    QrEfdInnCTsCabVL_ICMS: TFloatField;
    QrEfdInnCTsCabVL_NT: TFloatField;
    QrEfdInnCTsCabCOD_INF: TWideStringField;
    QrEfdInnCTsCabCOD_CTA: TWideStringField;
    QrEfdInnCTsCabCOD_MUN_ORIG: TIntegerField;
    QrEfdInnCTsCabCOD_MUN_DEST: TIntegerField;
    QrEfdInnCTsCabNFe_FatID: TIntegerField;
    QrEfdInnCTsCabNFe_FatNum: TIntegerField;
    QrEfdInnCTsCabNFe_StaLnk: TSmallintField;
    QrEfdInnCTsCabVSVmcWrn: TSmallintField;
    QrEfdInnCTsCabVSVmcObs: TWideStringField;
    QrEfdInnCTsCabVSVmcSeq: TWideStringField;
    QrEfdInnCTsCabVSVmcSta: TSmallintField;
    QrEfdInnCTsCabCST_ICMS: TIntegerField;
    QrEfdInnCTsCabCFOP: TIntegerField;
    QrEfdInnCTsCabALIQ_ICMS: TFloatField;
    QrEfdInnCTsCabVL_OPR: TFloatField;
    QrEfdInnCTsCabVL_RED_BC: TFloatField;
    QrEfdInnCTsCabCOD_OBS: TWideStringField;
    QrEFD_C500CHV_DOCe: TWideStringField;
    QrEFD_C500FIN_DOCe: TSmallintField;
    QrEFD_C500CHV_DOCe_REF: TWideStringField;
    QrEFD_C500IND_DEST: TSmallintField;
    QrEFD_C500COD_MUN_DEST: TIntegerField;
    QrEFD_C500COD_CTA: TWideStringField;
    QrEFD_C500COD_MOD_DOC_REF: TSmallintField;
    QrEFD_C500HASH_DOC_REF: TWideStringField;
    QrEFD_C500SER_DOC_REF: TWideStringField;
    QrEFD_C500NUM_DOC_REF: TIntegerField;
    QrEFD_C500MES_DOC_REF: TIntegerField;
    QrEFD_C500ENER_INJET: TFloatField;
    QrEFD_C500OUTRAS_DED: TFloatField;
    SbImplementado: TSpeedButton;
    QrEfdInnNFsItsCST_IPI: TWideStringField;
    QrEfdInnNFsItsCST_PIS: TWideStringField;
    QrEfdInnNFsItsCST_COFINS: TWideStringField;
    QrEfdInnNFsItsCST_ICMS: TWideStringField;
    QrAnal0053CST_ICMS: TWideStringField;
    QrAnal0053CFOP: TIntegerField;
    QrAnal0053ALIQ_ICMS: TFloatField;
    QrAnal0053VL_BC_ICMS: TFloatField;
    QrAnal0053VL_ICMS: TFloatField;
    QrAnal0053VL_BC_ICMS_ST: TFloatField;
    QrAnal0053VL_ICMS_ST: TFloatField;
    QrAnal0053VL_RED_BC: TFloatField;
    QrAnal0053VL_IPI: TFloatField;
    QrAnal0053VL_OPR: TFloatField;
    QrEfdInnNFsCabRegrFiscal: TIntegerField;
    QrNatOp: TMySQLQuery;
    QrNatOpFisRegCad: TIntegerField;
    QrNatOpide_natOp: TWideStringField;
    QrCAT66SP2018: TMySQLQuery;
    QrCAT66SP2018CFOP: TIntegerField;
    QrCAT66SP2018AjusteVL_OUTROS: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdMesChange(Sender: TObject);
    procedure MeGeradoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure MeGeradoMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MeGeradoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RGCOD_FINClick(Sender: TObject);
    procedure BtErrosClick(Sender: TObject);
    procedure TVBlocosClick(Sender: TObject);
    procedure TVBlocosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure QrEFD_E100BeforeClose(DataSet: TDataSet);
    procedure QrEFD_E110AfterScroll(DataSet: TDataSet);
    procedure QrEFD_E110BeforeClose(DataSet: TDataSet);
    procedure QrEFD_E111BeforeClose(DataSet: TDataSet);
    procedure QrEFD_E111AfterScroll(DataSet: TDataSet);
    procedure QrEFD_E100AfterScroll(DataSet: TDataSet);
    procedure QrEFD_E520BeforeClose(DataSet: TDataSet);
    procedure QrEFD_E520AfterScroll(DataSet: TDataSet);
    procedure QrEFD_H005AfterScroll(DataSet: TDataSet);
    procedure QrEFD_H010AfterScroll(DataSet: TDataSet);
    procedure QrEFD_K100AfterScroll(DataSet: TDataSet);
    procedure QrEFD_K100BeforeClose(DataSet: TDataSet);
    procedure QrEFD_H010BeforeClose(DataSet: TDataSet);
    procedure QrEFD_K230BeforeClose(DataSet: TDataSet);
    procedure QrEFD_K230AfterScroll(DataSet: TDataSet);
    procedure QrEFD_K250AfterScroll(DataSet: TDataSet);
    procedure QrEFD_K250BeforeClose(DataSet: TDataSet);
    procedure SbTeste_minimo_EFD_ICMS_IPIClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure Comcadastros1Click(Sender: TObject);
    procedure SbBloco_K_estoqueClick(Sender: TObject);
    procedure QrEFD_K300AfterScroll(DataSet: TDataSet);
    procedure QrEFD_K290AfterScroll(DataSet: TDataSet);
    procedure BtMesClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SbBloco_K_completoClick(Sender: TObject);
    procedure BtEstoqueClick(Sender: TObject);
    procedure frxSEII_EstqGetValue(const VarName: string; var Value: Variant);
    procedure QrEFD_K210BeforeClose(DataSet: TDataSet);
    procedure QrEFD_K210AfterScroll(DataSet: TDataSet);
    procedure SbImplementadoClick(Sender: TObject);
    procedure EdMesRedefinido(Sender: TObject);
  private
    { Private declarations }
    FDtFiscal0053_Entr, FCordaDe0053, FDtFiscalFrete_Entr: String;
    FContaIND_MOV_1: Integer;
    FGeraBloco: Boolean;
    FSelEnt, FSPEDEFD_Blcs, FBloco, FRegistro, FEmpresa_Txt, FAnoMes_Txt,
    FVersao, FSPEDEFD_0200, FSPEDEFD_0190,
    FSpedEfdIcmsIpi_0150, FSpedEfdIcmsIpi_0190, FSpedEfdIcmsIpi_0200,
    FSpedEfdIcmsIpi_K200, FSpedEfdIcmsIpi_K280: String;
    FEmpresa_Int, FAnoMes_Int, FLinArq,
    FLin0200, FLin0400, FLin0460,
    FLinC100, FLinC195, FLinC500, FLinD100, FLinD195, FLinD500, FLinK100, FLinK210, FLinK230, FLinK250,
    FLinK290, FLinK300,

    FQTD_LIN_0210, FQTD_LIN_0220, FQTD_LIN_0400, FQTD_LIN_0460,
    FQTD_LIN_C170, FQTD_LIN_C190, FQTD_LIN_C195, FQTD_LIN_C197, FQTD_LIN_C590,
    FQTD_LIN_D190, FQTD_LIN_D195, FQTD_LIN_D197, FQTD_LIN_D590,
    FQTD_LIN_E111, FQTD_LIN_E112, FQTD_LIN_E113, FQTD_LIN_E115, FQTD_LIN_E116,
    FQTD_LIN_E510, FQTD_LIN_E520, FQTD_LIN_E530,
    FQTD_LIN_H010, FQTD_LIN_H020,
    FQTD_LIN_K100, FQTD_LIN_K200, FQTD_LIN_K210, FQTD_LIN_K215, FQTD_LIN_K220,
    FQTD_LIN_K230, FQTD_LIN_K235, FQTD_LIN_K250, FQTD_LIN_K255, FQTD_LIN_K260,
    FQTD_LIN_K265, FQTD_LIN_K270, FQTD_LIN_K275, FQTD_LIN_K280, FQTD_LIN_K290,
    FQTD_LIN_K291, FQTD_LIN_K292, FQTD_LIN_K300, FQTD_LIN_K301, FQTD_LIN_K302,
    FQTD_LIN_0, FQTD_LIN_B, FQTD_LIN_C, FQTD_LIN_D, FQTD_LIN_E, FQTD_LIN_G, FQTD_LIN_H,
    FQTD_LIN_K, FQTD_LIN_1, FQTD_LIN_9, FREG_BLC_Count: Integer;
    FREG_BLC_Fields: array of String;
    FREG_BLC_Linhas: array of Integer;
    FLinStr: WideString;
    FArqStr: TStringList;
    FDataIni_Dta, FDataFim_Dta: TDateTime;
    FDataIni_Txt, FDataFim_Txt, FIntervaloFiscal: String;
    FSQLCampos, FSQLIndex: array of String;
    FValCampos, FValIndex: array of Variant;
    FSelReg0400, FSelReg0460,
    FSelRegC100, FSelRegC195, FSelRegC197, FSelRegC500,
    FSelRegD100, FSelRegD195, FSelRegD197, FSelRegD500,
    FSelRegK210, FSelRegK220, FSelRegK230, FSelRegK250, FSelRegK290,
    FSelRegK300: Boolean;
    //
    FLstCAT66SP2018: Array of Integer;


    function  AddCampoSQL(Campo: String; Valor: Variant): Boolean;
    procedure AtualizaTabelasSPEDEFD();
    function  AtualizaCampoNFeCabA(Campo: String; Valor: Variant;
              FatID, FatNum, Empresa: Integer): Boolean;
    //
    procedure AdicionaLinha(var Contador: Integer);
    procedure AdicionaBLC(Linhas: Integer; Registro: String = '');
    function  AC_6(IndOper: TIndicadorDoTipoDeOperacao; Campo: String;
              Inteiro64: Int64; PermiteZero, InformaZero: Boolean): Boolean;
    function  AC_d(IndOper: TIndicadorDoTipoDeOperacao; Campo: String;
              Data: TDateTime; PermiteZero, InformaZero: Boolean;
              Formato: Integer): Boolean;
    function  AC_f(IndOper: TIndicadorDoTipoDeOperacao; Campo: String;
              Flutuante: Double; PermiteZero, InformaZero: Boolean): Boolean;
    function  AC_i(IndOper: TIndicadorDoTipoDeOperacao; Campo: String;
              Inteiro: Integer; PermiteZero, InformaZero: Boolean;
              LeftZeros: Boolean = False): Boolean;
    function  AC_n(IndOper: TIndicadorDoTipoDeOperacao; Campo, Valor: String):
              Boolean;
    function  AC_x(IndOper: TIndicadorDoTipoDeOperacao; Campo, Valor: String):
              Boolean;
    function  BuscaValorMesAnterior(Tabela, Campo: String): Double;
    procedure InfoPosMeGerado();
    function  CalculaCampoEFD_E110_02(): Double;
    function  CalculaCampoEFD_E110_06(): Double;
    function  CalculaCampoEFD_E520_03(): Double;
    function  CalculaCampoEFD_E520_04(): Double;
    function  CampoNaoConfere(): Boolean;
    function  CampoNaoEhNumero(Valor: String): Boolean;
    function  CampoObrigatorio_6(Inteiro64: Int64; PermiteZero: Boolean): Boolean;
    function  CampoObrigatorio_d(Data: TDatetime; PermiteZero: Boolean): Boolean;
    function  CampoObrigatorio_f(Flutuante: Double; PermiteZero: Boolean): Boolean;
    function  CampoObrigatorio_i(Inteiro: Integer; PermiteZero: Boolean): Boolean;
    function  CampoObrigatorio_x(Valor: String): Boolean;
{   N�o Usa
    function  CFOP_Nao_Cadastrado(Empresa: Integer; DataIni, DataFim: TDateTime): Boolean;
}
    function  CorrigeCOD_SIT(): Boolean;
    function  CorrigeDataFiscal(): Boolean;
    function  CorrigeItensCraidosAForca(): Boolean;
    function  CorrigeNotasFiscaiDeTerceiros(): Boolean;
    function  CorrigeNotasFiscaiProprias(): Boolean;
    function  CorrigeItensDeMateriaPrima(): Boolean;
    procedure DefineEmpresaEAnoMes();
    function  Define_REG(): Boolean;
    function  ExcluiPeriodoSelecionado(): Boolean;
    procedure CorrigeUnidMedNfeItsI();
    procedure HabilitaBotaoOK();
    function  MensagemCampoDesconhecido(): Boolean;
    function  MensagemCampoNaoEhNumero(): Boolean;
    procedure MensagemCampoObrigatorio();
    procedure MensagemCampoIncorreto(tpEsper: String);
    procedure MensagemDecimaisIncorreto(tpEsper: String);
    procedure MensagemDocumentoInvalido(Doc: String);
    procedure MensagemTamanhoDifereDoObrigatorio(Valor: String);
    procedure MensagemTamanhoMuitoGrande(Valor: String);
    function  NaoTemErros(Avisa: Boolean): Boolean;
    function  ObrigatoriedadeConfere(const IndOper: TIndicadorDoTipoDeOperacao;
              var IndicadorPreenchimento: TIndicadorPreenchimento): Boolean;

    function  ReopenCampos(var Res: Boolean): Boolean;
    function  SalvaArquivo(): Boolean;
    procedure PredefineLinha();
    function  TamanhoMuitoGrande(Valor: String): Boolean;
    function  TamanhoDifereDoObrigatorio(Valor: String): Boolean;
    //
    function  GeraRegistro_0000(): Boolean;
    function  GeraRegistro_0001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_0002(): Boolean;
    function  GeraRegistro_0005(): Boolean;
    function  GeraRegistro_0100(): Boolean;
    function  GeraRegistro_0150(): Boolean;
    function  GeraRegistro_0190_Antigo(): Boolean;
    function  GeraRegistro_0190_Novo(): Boolean;
    function  GeraRegistro_0200_Antigo(): Boolean;
    function  GeraRegistro_0200_Novo(): Boolean;
    function  GeraRegistro_0205(): Boolean;
    function  GeraRegistro_0210(): Boolean;
    function  GeraRegistro_0220(): Boolean;
    function  GeraRegistro_0400(): Boolean;
    function  GeraRegistro_0460_A(): Boolean;
    function  GeraRegistro_0460_B(): Boolean;
    function  GeraRegistro_0990(): Boolean;

    function  GeraRegistro_B001(IND_DAD: Integer): Boolean;
    function  GeraRegistro_B990(): Boolean;

    function  GeraRegistro_C001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_C100(): Boolean;
    function  GeraRegistro_C100_Emit(): Boolean;
    function  GeraRegistro_C100_Entr(): Boolean;
    //function  GeraRegistro_C170_Old(): Boolean;
    function  GeraRegistro_C170(): Boolean;
    function  GeraRegistro_C170_Emit(): Boolean;
    function  GeraRegistro_C170_Entr(): Boolean;
    function  GeraRegistro_C190_Emit(): Boolean;
    function  GeraRegistro_C190_Entr(Controle: Integer): Boolean;
    {function  GeraRegistro_C190_Sai(): Boolean;
    function  GeraRegistro_C190_Ent(): Boolean;
    function  GeraRegistro_C190_A(): Boolean;
    function  GeraRegistro_C190_B(): Boolean;}
    function  GeraRegistro_C195(EfdInnNFsCabControle: Integer): Boolean;
    function  GeraRegistro_C197(): Boolean;
    function  GeraRegistro_C500(): Boolean;
    function  GeraRegistro_C590(): Boolean;
    function  GeraRegistro_C990(): Boolean;

    function  GeraRegistro_D001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_D100(): Boolean;
    function  GeraRegistro_D100_Novo(): Boolean;
    function  GeraRegistro_D190(): Boolean;
    function  GeraRegistro_D195(EfdInnCTsCabControle: Integer): Boolean;
    function  GeraRegistro_D197(): Boolean;
    function  GeraRegistro_D500(): Boolean;
    function  GeraRegistro_D590(): Boolean;
    function  GeraRegistro_D990(): Boolean;

    function  GeraRegistro_E001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_E100(): Boolean;
    function  GeraRegistro_E110(): Boolean;
    function  GeraRegistro_E111(): Boolean;
    function  GeraRegistro_E112(): Boolean;
    function  GeraRegistro_E113(): Boolean;
    function  GeraRegistro_E115(): Boolean;
    function  GeraRegistro_E116(): Boolean;
    function  GeraRegistro_E500(): Boolean;
    function  GeraRegistro_E510(): Boolean;
    function  GeraRegistro_E520(): Boolean;
    function  GeraRegistro_E530(): Boolean;
    function  GeraRegistro_E990(): Boolean;

    function  GeraRegistro_G001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_G990(): Boolean;

    function  GeraRegistro_H001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_H005(): Boolean;
    function  GeraRegistro_H010(): Boolean;
    function  GeraRegistro_H020(): Boolean;
    function  GeraRegistro_H990(): Boolean;

    function  GeraRegistro_K001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_K100(): Boolean;
    function  GeraRegistro_K200(): Boolean;
    function  GeraRegistro_K210(): Boolean;
    function  GeraRegistro_K215(): Boolean;
    function  GeraRegistro_K220(): Boolean;
    function  GeraRegistro_K230(): Boolean;
    function  GeraRegistro_K235(): Boolean;
    function  GeraRegistro_K250(): Boolean;
    function  GeraRegistro_K255(): Boolean;
    function  GeraRegistro_K280(): Boolean;
    function  GeraRegistro_K290(): Boolean;
    function  GeraRegistro_K291(): Boolean;
    function  GeraRegistro_K292(): Boolean;
    function  GeraRegistro_K300(): Boolean;
    function  GeraRegistro_K301(): Boolean;
    function  GeraRegistro_K302(): Boolean;
    function  GeraRegistro_K990(): Boolean;

    function  GeraRegistro_1001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_1010(): Boolean;
    function  GeraRegistro_1100(): Boolean;
    function  GeraRegistro_1990(): Boolean;
    function  GeraRegistro_9001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_9900(): Boolean;
    function  GeraRegistro_9990(): Boolean;
    function  GeraRegistro_9999(): Boolean;

    procedure ImprimeEstoque();
    procedure ImprimeDaddosTxtSPEDGerado(NO_Empresa: String);


    function  ReCriaNoh(const Noh: TTreeNode; const nN: String; const cN:
              Integer; const Ativo: Integer): TTreeNode;
    procedure RecriaRegistroEFD_E110();
    procedure RecriaRegistroEFD_E520();

    procedure ReopenCabA(Filtra: Boolean; Filtro: Variant);
    procedure ReopenEFD_C500();
    procedure ReopenEFD_E110();
    procedure ReopenEFD_E520();
    procedure ReopenEFD_E530();

    procedure ReopenEmpresa();
    procedure ReopenEnder(Entidade: Integer);
    procedure ReopenVersoes();
    {
    function  Blocos_Selecionados1(): String;
    }
    function  Blocos_Selecionados2(Excecao: array of String): String;
    procedure Grava_SpedEfdIcmsIpiErrs(Erro: TEFDExpErr; Campo: String);
    function  PodeGerarBloco(Bloco: String): Boolean;
    {
    procedure ConfiguraTreeView1();
    }
    procedure ConfiguraTreeView2(Ativos, Cinzas: array of String);
    //
    procedure VerificaImplemantacoesNaoFeitas();
    function  ObtemCOD_ITEM(GraGruX: Integer): String;
    function  ObtemCOD_PART(Entidade: Integer): String;
    function  ObtemITO_CabA(var EhE: Boolean; var IND_OPER, tpNF: String;
              var ITO: TIndicadorDoTipoDeOperacao): Boolean;
    function  ObtemITO_InNF(var EhE: Boolean; var IND_OPER, tpNF: String;
              var ITO: TIndicadorDoTipoDeOperacao): Boolean;
    procedure VerificaDESCR_ITEM(DESCR_ITEM: String; Query: TmySQLQuery);
    function  SelReg(Reg: String): Boolean;
    //function AddCampoSQL(Campo: String; Valor: Variant): Boolean;
    function  DefineIND_MOV(FisRegCad, ide_finNFe: Integer; prod_cProd: String):
              Integer;
    procedure ReabreAtrela0053(JaAtrelados: TSimNao);
    procedure RedefinePeriodos();
    function  GeraCOD_OBS_CAT66SP2018(Seq: Integer): String;

  public
    { Public declarations }
  end;

  var
  FmEfdIcmsIpiExporta_v03_0_2_a: TFmEfdIcmsIpiExporta_v03_0_2_a;

implementation

uses UnMyObjects, ModuleGeral, Module, SPED_EFD_II_Tabs, UMySQLModule, MyDBCheck,
  EfdIcmsIpiErros_v03_0_2_a, Principal, ModuleNFe_0000, ModProd, SPED_EFD_Tabelas,
  UnGrade_Create, UnInternalConsts, MyGlyfs, UCreate, UnSPED_Create, NFe_PF;

{$R *.DFM}

const
  FImporExpor: Integer = 2; // Exporta. N�o Mexer.

function TFmEfdIcmsIpiExporta_v03_0_2_a.AC_6(IndOper: TIndicadorDoTipoDeOperacao;
Campo: String; Inteiro64: Int64;
PermiteZero, InformaZero: Boolean): Boolean;
var
  Valor: String;
  N: Integer;
  IndicadorPreenchimento: TIndicadorPreenchimento;
begin
(*
  if QrCamposCampo.Value = Campo then
  begin
    Result := True;
    if (Inteiro64 = 0) and (not InformaZero) then
      Valor := ''
    else
      Valor := FormatFloat('0', Inteiro64);
    if (QrCamposTObrig.Value = 1) and (Valor <> '') then
    begin
      while Length(Valor) < QrCamposTam.Value do
        Valor := '0' + Valor;
    end;
    //
    FLinStr := FLinStr + Trim(Geral.TxtSemPipe(Valor)) + '|';
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := Campo;
    SetLength(FValCampos, N + 1);
    FValCampos[N] := Valor;
    //
    if ObrigatoriedadeConfere(IndOper) then
  //TIndicadorPreenchimento = (ipcNaoPreencher=0, ipcCondicional=1, ipcObrigatorio=2);

    if CampoObrigatorio_x(Valor) then
      MensagemCampoObrigatorio()
    else
    if CampoObrigatorio_6(Inteiro64, PermiteZero) then
      MensagemCampoObrigatorio()
    else
    if QrCamposTipo.Value <> 'N'  then
      MensagemCampoIncorreto('N')
    else
    if QrCamposDecimais.Value > 0  then
      MensagemDecimaisIncorreto('6')
    else
    if TamanhoMuitoGrande(Valor)  then
      MensagemTamanhoMuitoGrande(Valor)
    else
    if TamanhoDifereDoObrigatorio(Valor)  then
      MensagemTamanhoDifereDoObrigatorio(Valor);
    //
    Result := SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
  end else
    Result := False;
*)
  if QrCamposCampo.Value = Campo then
  begin
    Result := True;
    if (Inteiro64 = 0) and (not InformaZero) then
      Valor := ''
    else
      Valor := FormatFloat('0', Inteiro64);
    if (QrCamposTObrig.Value = 1) and (Valor <> '') then
    begin
      while Length(Valor) < QrCamposTam.Value do
        Valor := '0' + Valor;
    end;
    //
    if ObrigatoriedadeConfere(IndOper, IndicadorPreenchimento) then
      if CampoObrigatorio_x(Valor) then
        MensagemCampoObrigatorio()
    else
    if CampoObrigatorio_6(Inteiro64, PermiteZero) then
      MensagemCampoObrigatorio()
    else
    if QrCamposTipo.Value <> 'N'  then
      MensagemCampoIncorreto('N')
    else
    if QrCamposDecimais.Value > 0  then
      MensagemDecimaisIncorreto('6')
    else
    if TamanhoMuitoGrande(Valor)  then
      MensagemTamanhoMuitoGrande(Valor)
    else
    if TamanhoDifereDoObrigatorio(Valor)  then
      MensagemTamanhoDifereDoObrigatorio(Valor);
    //
   if (Inteiro64 = 0) and (not InformaZero) then
      Valor := ''
    else
      Valor := FormatFloat('0', Inteiro64);
    if (QrCamposTObrig.Value = 1) and (Valor <> '') then
    begin
      while Length(Valor) < QrCamposTam.Value do
        Valor := '0' + Valor;
    end;
    //
    if IndicadorPreenchimento = TIndicadorPreenchimento.ipcNaoPreencher then
      FLinStr := FLinStr + '|'
    else
      FLinStr := FLinStr + Trim(Geral.TxtSemPipe(Valor)) + '|';
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := Campo;
    SetLength(FValCampos, N + 1);
    FValCampos[N] := Valor;
    //
     Result := SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
  end else
    Result := False;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.AC_d(IndOper: TIndicadorDoTipoDeOperacao;
  Campo: String; Data: TDateTime; PermiteZero, InformaZero: Boolean;
  Formato: Integer): Boolean;
var
  ValTxt, ValSql: String;
  N: Integer;
  IndicadorPreenchimento: TIndicadorPreenchimento;
begin
  if QrCamposCampo.Value = Campo then
  begin
    Result := True;
    //
    if (Data < 2) and (not InformaZero) then
    begin
      ValSql := '0000-00-00';
      ValTxt := '';
    end else
    begin
      ValSql := Geral.FDT(Data, 1);
      ValTxt := Geral.FDT(Data, Formato);
    end;
(*
    if (QrCamposTObrig.Value = 1) and (Valor <> '') then
    begin
      while Length(Valor) < QrCamposTam.Value do
        Valor := '0' + Valor;
    end;
*)
    if ObrigatoriedadeConfere(IndOper, IndicadorPreenchimento) then
    if CampoObrigatorio_x(ValTxt) then
      MensagemCampoObrigatorio()
    else
    if CampoObrigatorio_d(Data, PermiteZero) then
      MensagemCampoObrigatorio()
    else
    if QrCamposTipo.Value <> 'N'  then
      MensagemCampoIncorreto('N')
    else
    if QrCamposDecimais.Value > 0  then
      MensagemDecimaisIncorreto('I')
    else
    if TamanhoMuitoGrande(ValTxt)  then
      MensagemTamanhoMuitoGrande(ValTxt)
    else
    if TamanhoDifereDoObrigatorio(ValTxt)  then
      MensagemTamanhoDifereDoObrigatorio(ValTxt);
    //
    if IndicadorPreenchimento = TIndicadorPreenchimento.ipcNaoPreencher then
      FLinStr := FLinStr + '|'
    else
      FLinStr := FLinStr + Geral.TxtSemPipe(ValTxt) + '|';
    //
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := Campo;
    SetLength(FValCampos, N + 1);
    FValCampos[N] := ValSql;
    //
    //
    Result := SPED_Geral.VDom(FBloco, FRegistro, Campo, ValTxt, MeErros);
  end else
    Result := False;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.AC_f(IndOper: TIndicadorDoTipoDeOperacao;
Campo: String; Flutuante: Double; PermiteZero,
  InformaZero: Boolean): Boolean;
var
  Valor: String;
  N: Integer;
  IndicadorPreenchimento: TIndicadorPreenchimento;
begin
  if QrCamposCampo.Value = Campo then
  begin
    Result := True;
    //
    if (Flutuante = 0) and (not InformaZero) then
      Valor := ''
    else
      Valor := Geral.DFT_SPED(Flutuante, QrCamposDecimais.Value, siNegativo);
    if (QrCamposTObrig.Value = 1) and (Valor <> '') then
    begin
      while Length(Valor) < QrCamposTam.Value do
        Valor := '0' + Valor;
    end;
    if ObrigatoriedadeConfere(IndOper, IndicadorPreenchimento) then

    if CampoObrigatorio_x(Valor) then
      MensagemCampoObrigatorio()
    else
    if CampoObrigatorio_f(Flutuante, PermiteZero) then
      MensagemCampoObrigatorio()
    else
    if QrCamposTipo.Value <> 'N'  then
      MensagemCampoIncorreto('N')
    else
    if QrCamposDecimais.Value = 0  then
      MensagemDecimaisIncorreto('F')
    else
    if TamanhoMuitoGrande(Valor)  then
      MensagemTamanhoMuitoGrande(Valor)
    else
    if TamanhoDifereDoObrigatorio(Valor)  then
      MensagemTamanhoDifereDoObrigatorio(Valor);
    //
    if IndicadorPreenchimento = TIndicadorPreenchimento.ipcNaoPreencher then
      FLinStr := FLinStr + '|'
    else
      FLinStr := FLinStr + Trim(Geral.TxtSemPipe(Valor)) + '|';
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := Campo;
    SetLength(FValCampos, N + 1);
    FValCampos[N] := Geral.DMV(Valor);
    //
    Result := SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
  end else
    Result := False;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.AC_i(IndOper: TIndicadorDoTipoDeOperacao;
Campo: String; Inteiro: Integer;
PermiteZero, InformaZero: Boolean; LeftZeros: Boolean): Boolean;
var
  Valor: String;
  N: Integer;
  IndicadorPreenchimento: TIndicadorPreenchimento;
begin
  if QrCamposCampo.Value = Campo then
  begin
    Result := True;
    //
    if (Inteiro = 0) and (not InformaZero) then
      Valor := ''
    else
      Valor := Geral.FF0(Inteiro);
    if ((QrCamposTObrig.Value = 1) and (Valor <> '')) or LeftZeros then
    begin
      while Length(Valor) < QrCamposTam.Value do
        Valor := '0' + Valor;
    end;
    if ObrigatoriedadeConfere(IndOper, IndicadorPreenchimento) then


    if CampoObrigatorio_x(Valor) then
      MensagemCampoObrigatorio()
    else
    if CampoObrigatorio_i(Inteiro, PermiteZero) then
      MensagemCampoObrigatorio()
    else
    if QrCamposTipo.Value <> 'N'  then
      MensagemCampoIncorreto('N')
    else
    if QrCamposDecimais.Value > 0  then
      MensagemDecimaisIncorreto('I')
    else
    if TamanhoMuitoGrande(Valor)  then
      MensagemTamanhoMuitoGrande(Valor)
    else
    if TamanhoDifereDoObrigatorio(Valor)  then
      MensagemTamanhoDifereDoObrigatorio(Valor);
    //
    if IndicadorPreenchimento = TIndicadorPreenchimento.ipcNaoPreencher then
      FLinStr := FLinStr + '|'
    else
      FLinStr := FLinStr + Trim(Geral.TxtSemPipe(Valor)) + '|';
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := Campo;
    SetLength(FValCampos, N + 1);
    FValCampos[N] := Inteiro;
    //
    Result := SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
  end else
    Result := False;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.AC_n(IndOper: TIndicadorDoTipoDeOperacao; Campo,
  Valor: String): Boolean;
var
  N: Integer;
  IndicadorPreenchimento: TIndicadorPreenchimento;
begin
  if QrCamposCampo.Value = Campo then
  begin
    Result := True;
    if ObrigatoriedadeConfere(IndOper, IndicadorPreenchimento) then
    if CampoObrigatorio_x(Valor) then
      MensagemCampoObrigatorio()
    else
    if CampoNaoEhNumero(Valor) then
      MensagemCampoNaoEhNumero()
    else
    if QrCamposTipo.Value <> 'N'  then
      MensagemCampoIncorreto('N')
    else
    if QrCamposDecimais.Value > 0  then
      MensagemDecimaisIncorreto('N')
    else
    if TamanhoMuitoGrande(Valor) then
      MensagemTamanhoMuitoGrande(Valor)
    else
    if TamanhoDifereDoObrigatorio(Valor) then
      MensagemTamanhoDifereDoObrigatorio(Valor);
    //
    if IndicadorPreenchimento = TIndicadorPreenchimento.ipcNaoPreencher then
      FLinStr := FLinStr + '|'
    else
      FLinStr := FLinStr + Trim(Geral.TxtSemPipe(Valor)) + '|';
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := Campo;
    SetLength(FValCampos, N + 1);
    FValCampos[N] := Valor;
    //
    Result := SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
  end else
    Result := False;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.AC_x(IndOper: TIndicadorDoTipoDeOperacao;
Campo, Valor: String): Boolean;
var
  N: Integer;
  Texto: String;
  IndicadorPreenchimento: TIndicadorPreenchimento;
begin
  if QrCamposCampo.Value = Campo then
  begin
    Result  := True;
    Texto   := Geral.Substitui(Geral.Substitui(Valor, #13, ''), #10, ' ');
    if ObrigatoriedadeConfere(IndOper, IndicadorPreenchimento) then
    if CampoObrigatorio_x(Texto) then
      MensagemCampoObrigatorio()
    else
    if QrCamposTipo.Value <> 'C'  then
      MensagemCampoIncorreto('C')
    else
    if QrCamposDecimais.Value > 0  then
      MensagemDecimaisIncorreto('C')
    else
    if TamanhoMuitoGrande(Texto)  then
      MensagemTamanhoMuitoGrande(Texto)
    else
    if TamanhoDifereDoObrigatorio(Texto)  then
      MensagemTamanhoDifereDoObrigatorio(Texto);
    //
    if IndicadorPreenchimento = TIndicadorPreenchimento.ipcNaoPreencher then
      FLinStr := FLinStr + '|'
    else
      FLinStr := FLinStr + Trim(Geral.TxtSemPipe(Texto)) + '|';
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := Campo;
    SetLength(FValCampos, N + 1);
    FValCampos[N] := Texto;
    //
    Result := SPED_Geral.VDom(FBloco, FRegistro, Campo, Texto, MeErros);
  end else
    Result := False;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.AddCampoSQL(Campo: String; Valor: Variant): Boolean;
var
  N: Integer;
begin
  N := Length(FSQLCampos);
  SetLength(FSQLCampos, N + 1);
  FSQLCampos[N] := Campo;
  SetLength(FValCampos, N + 1);
  FValCampos[N] := Valor;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.AdicionaBLC(Linhas: Integer; Registro: String = '');
var
  Reg: String;
begin
  if Linhas > 0 then
  begin
    if Registro = '' then
      Reg := FRegistro
    else
      Reg := Registro;
    FREG_BLC_Count := FREG_BLC_Count + 1;
    SetLength(FREG_BLC_Fields, FREG_BLC_Count);
    SetLength(FREG_BLC_Linhas, FREG_BLC_Count);
    FREG_BLC_Fields[FREG_BLC_Count - 1] := Reg;
    FREG_BLC_Linhas[FREG_BLC_Count - 1] := Linhas;
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.AdicionaLinha(var Contador: Integer);
var
  Tabela: String;
begin
  FArqStr.Add(FLinStr);
  MeGerado.Lines.Add(FLinStr);
  //
  Tabela := 'spedefdicmsipi' + FValCampos[1];
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False,
    FSQLCampos, FSQLIndex, FValCampos, FValIndex, True);
  //
  Contador := Contador + 1;
  if FLinStr <> FArqStr[FArqStr.Count-1] then
    Geral.MB_ERRO(
    'Linha gerada n�o foi adicionada corretamente ao texto do arquivo a ser gerado (1)!'
    + sLineBreak + 'Informe a Dermatek!');
  if FLinStr <> MeGerado.Lines[MeGerado.Lines.Count-1] then
    Geral.MB_ERRO('Linha gerada n�o foi adicionada corretamente ao texto do arquivo a ser gerado (2)!'
    + sLineBreak + 'Informe a Dermatek!');
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.AtualizaCampoNFeCabA(Campo: String;
  Valor: Variant; FatID, FatNum, Empresa: Integer): Boolean;
begin
  Result :=
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
  Campo], ['FatID', 'FatNum', 'Empresa'], [
  Valor], [FatID, FatNum, Empresa], True);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.AtualizaTabelasSPEDEFD();
begin
  if Geral.MB_Pergunta(
  'Deseja verificar se h� atualiza��o das tabelas de consulta do SPED EFD?') = ID_YES then
  begin
    if DBCheck.CriaFm(TFmSPED_EFD_Tabelas, FmSPED_EFD_Tabelas, afmoNegarComAviso) then
    begin
      FmSPED_EFD_Tabelas.ShowModal;
      FmSPED_EFD_Tabelas.Destroy;
    end;
  end;
end;

{
function TFmEfdIcmsIpiExporta.Blocos_Selecionados1(): String;
begin
  QrBlocos.Close;
  QrBlocos.Database := DmodG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(QrBlocos, DmodG.MyPID_DB, [
'SELECT * ',
'FROM ' + FSPEDEFD_Blcs,
'WHERE Bloco="C"',
'OR Bloco="D"',
'AND Ativo=1',
'']);
  if QrBlocos.RecordCount = 1 then
    Result := 'AND EFD_EXP_REG="' + QrBlocosBloco.Value + '100"'
  else
    Result := '';
end;
}

function TFmEfdIcmsIpiExporta_v03_0_2_a.Blocos_Selecionados2(Excecao: array of String): String;
var
  _OR: String;
begin
  _OR := '';
  Result := '';
  //
  QrNiv2.Close;
  QrNiv2.Database := DmodG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(QrNiv2, DmodG.MyPID_DB, [
  'SELECT Registro  ',
  'FROM ' + FSPEDEFD_Blcs,
  'WHERE Nivel=2 ',
  'AND Implementd <> 0 ',
  'AND Ativo=1',
  'AND ( ',
  '  Bloco = "C" ',
  '  OR ',
  '  Bloco = "D" ',
  ') ',
  '']);
  if QrNiv2.RecordCount > 0 then
  begin
    Result := 'AND (';
    QrNiv2.First;
    while not QrNiv2.Eof do
    begin
      if dmkPF.IndexOfArrStr(QrNiv2Registro.Value, Excecao) = -1 then
      begin
        if QrNiv2.RecNo = 1 then
        begin
          Result := Result + sLineBreak +
            '  (EFD_EXP_REG="' + QrNiv2Registro.Value + '")';
          _OR := 'OR';
        end else
          Result := Result + sLineBreak +
            '  ' + _OR + ' (EFD_EXP_REG="' + QrNiv2Registro.Value + '")';
      end;
      //
      QrNiv2.Next;
    end;
    Result := Result + ')';
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.BtErrosClick(Sender: TObject);
begin
  DefineEmpresaEAnoMes();
  NaoTemErros(True);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.BtEstoqueClick(Sender: TObject);
begin
  DefineEmpresaEAnoMes();
  ImprimeEstoque();
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.BtMesClick(Sender: TObject);
var
  Qry: TmySQLQuery;
  AnoMes: Integer;
  Mes: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(AnoMes) AnoMes ',
    'FROM efdicmsipie001 ',
    'WHERE Empresa=' + FEmpresa_Txt,
    '']);
    if Qry.RecordCount > 0 then
    begin
      AnoMes := Qry.FieldByName('AnoMes').AsInteger;
      if AnoMes > 200001 then
        EdMes.ValueVariant := Geral.AnoMesToData(AnoMes, 1);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.BtOKClick(Sender: TObject);
var
  TextoErr: String;
  //DataIni, DataFim: TDateTime;
  I, P, IND_MOV, IND_DAD, Registros, Emitente: Integer;
  //Gerar,
  TemMovim, TemErro, InfReg, GerouC100: Boolean;
  Noh: TTreeNode;
begin
  FContaIND_MOV_1 := 0;
  //
  DefineEmpresaEAnoMes();
  //
  //
  // ini 2022-04-14
(*
  case QrParamsEmpSPED_EFD_DtFiscal.Value of
    1: FDtFiscal0053 := 'DT_DOC';
    2: FDtFiscal0053 := 'DT_E_S';
    else FDtFiscal0053 := 'DT_???';
  end;
*)
  FDtFiscal0053_Entr := 'DT_E_S';
  //
(*
  case QrParamsEmpSPED_EFD_DtFiscal.Value of
    1: FDtFiscalFrete := 'DT_DOC';
    2: FDtFiscalFrete := 'DT_A_P';
    else FDtFiscalFrete := 'DT_???';
  end;
*)
  FDtFiscalFrete_Entr := 'DT_A_P';
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT FatNum  ',
  'FROM nfecaba ',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_0053),
  'AND DataFiscal BETWEEN "' + FdataIni_TXT + '" AND "' + FdataFim_TXT + '" ',
  '']);
  FCordaDe0053 := MyObjects.CordaDeQuery(Dmod.QrAux, 'FatNum');
  Dmod.QrAux.Close;
  // fim 2022-04-14
  FSelReg0400 := Selreg('0400');
  FSelReg0460 := Selreg('0460');
  FSelRegC100 := SelReg('C100');
  FSelRegC195 := SelReg('C195');
  FSelRegC197 := SelReg('C197');
  FSelRegC500 := SelReg('C500');
  //
  FSelRegD100 := SelReg('D100');
  FSelRegD195 := SelReg('D195');
  FSelRegD197 := SelReg('D197');
  FSelRegD500 := SelReg('D500');
  //
  FSelRegK210 := SelReg('K210');
  FSelRegK220 := SelReg('K220');
  FSelRegK230 := SelReg('K230');
  FSelRegK250 := SelReg('K250');
  FSelRegK290 := SelReg('K290');
  FSelRegK300 := SelReg('K300');
  //
  if not EfdIcmsIpi_PF.LiberaAcaoVS_SPED(FImporExpor, FAnoMes_Int, FEmpresa_Int,
  TEstagioVS_SPED.evsspedExportaSPED) then
    Exit;
  // Temporario - implementar opcoes SPED da Filial
  VAR_INF_SPED_EFD_VAL_ZERO := True;
  VAR_INF_SPED_EFD_NOME_DESCR_COMPL := True;
  // FimTemporario
  MeGerado.Lines.Clear;
  MeErros.Lines.Clear;
  MeErros.Lines.Add(
  'Criar aviso de erro na exporta��o SPED do valor a recolher no E116 <> E110');
  MeErros.Lines.Add(
  'Criar registros 0220 par NFs de compra!');
  (* Parei Aqui! ver *)
  TextoErr := '';
  if (QrParamsEmpSPED_EFD_IND_PERFIL.Value = '?') then
    TextoErr := TextoErr + '- Perfil de apresenta��o do arquivo fiscal' + sLineBreak;
  if (QrParamsEmpSPED_EFD_IND_ATIV.Value = 9) then
    TextoErr := TextoErr + '- Indicador de tipo de atividade' + sLineBreak;
  if (QrParamsEmpSPED_EFD_DtFiscal.Value < 1) then
    TextoErr := TextoErr + '- Data de Escritura��o de Emiss�es' + sLineBreak;
  if (QrParamsEmpSPED_EFD_ID_0150.Value < 1) then
    TextoErr := TextoErr + '- ID no arquivo exporta��o - Entidade' + sLineBreak;
  if (QrParamsEmpSPED_EFD_ID_0200.Value < 1) then
    TextoErr := TextoErr + '- ID no arquivo exporta��o - Produto' + sLineBreak;
  if TextoErr <> '' then
  begin
    TextoErr := 'Exporta��o abortada!' + sLineBreak +
    'Certifique-se que os itens abaixo est�o corretos: ' + sLineBreak +
    TextoErr;
    MyObjects.FIC(True, nil, TextoErr);
    Exit;
  end;
  //
  if not CorrigeDataFiscal() then
    Exit;
{
  if not CorrigeCOD_SIT() then
    Exit;
}
  //
  if CkCorrigeCriadosAForca.Checked then
    if not CorrigeItensCraidosAForca() then
      Exit;
  CorrigeNotasFiscaiProprias();
  CorrigeNotasFiscaiDeTerceiros();
  CorrigeItensDeMateriaPrima();
  CorrigeUnidMedNfeItsI();
  AtualizaTabelasSPEDEFD();
  //
  VerificaImplemantacoesNaoFeitas();
  //
  if not CorrigeCOD_SIT() then
    if (RGPreenche.ItemIndex = 2) then
    begin
      PageControl1.ActivePageIndex := 2;
      //Exit;
    end;
  //
  FLinArq := 0;
  FQTD_LIN_0210 := 0;
  FQTD_LIN_0220 := 0;
  FQTD_LIN_0400 := 0;
  FQTD_LIN_0460 := 0;
  FQTD_LIN_C170 := 0;
  FQTD_LIN_C190 := 0;
  FQTD_LIN_C195 := 0;
  FQTD_LIN_C197 := 0;
  //
  FQTD_LIN_C590 := 0;
  //
  FQTD_LIN_D190 := 0;
  FQTD_LIN_D195 := 0;
  FQTD_LIN_D197 := 0;
  FQTD_LIN_D590 := 0;
  //
  FQTD_LIN_E111 := 0;
  FQTD_LIN_E112 := 0;
  FQTD_LIN_E113 := 0;
  FQTD_LIN_E115 := 0;
  FQTD_LIN_E116 := 0;
  FQTD_LIN_E510 := 0;
  FQTD_LIN_E520 := 0;
  FQTD_LIN_E530 := 0;
  //
  FQTD_LIN_H010 := 0;
  FQTD_LIN_H020 := 0;
  //
  FQTD_LIN_K100 := 0;
  FQTD_LIN_K200 := 0;
  FQTD_LIN_K210 := 0;
  FQTD_LIN_K215 := 0;
  FQTD_LIN_K220 := 0;
  FQTD_LIN_K230 := 0;
  FQTD_LIN_K235 := 0;
  FQTD_LIN_K250 := 0;
  FQTD_LIN_K255 := 0;
  FQTD_LIN_K280 := 0;
  FQTD_LIN_K290 := 0;
  FQTD_LIN_K291 := 0;
  FQTD_LIN_K292 := 0;
  FQTD_LIN_K300 := 0;
  FQTD_LIN_K301 := 0;
  FQTD_LIN_K302 := 0;
  //
  FQTD_LIN_0 := 0;
  FQTD_LIN_B := 0;
  FQTD_LIN_C := 0;
  FQTD_LIN_D := 0;
  FQTD_LIN_E := 0;
  FQTD_LIN_G := 0;
  FQTD_LIN_H := 0;
  FQTD_LIN_K := 0;
  FQTD_LIN_1 := 0;
  FQTD_LIN_9 := 0;
  FREG_BLC_Count := 0;
  SetLength(FREG_BLC_Fields, 0);
  SetLength(FREG_BLC_Linhas, 0);
      //
  Screen.Cursor := crHourGlass;
  FArqStr := TStringList.Create;
  FArqStr.Clear;
  try
    ReopenVersoes();
    FVersao := QrVersaoCodTxt.Value;
    if MyObjects.FIC(FVersao = '', nil, 'N�o foi poss�vel definir a vers�o do arquivo' +
    sLineBreak + 'AVISE A DERMATEK!') then
      Exit;
    //
    ReopenEmpresa();
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo dados duplic�veis');
    if not ExcluiPeriodoSelecionado() then
      Exit;
    //
    if not Define_REG() then
      Exit;

    //
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   0                                                          //
    ////////////////////////////////////////////////////////////////////////////
    if GeraRegistro_0000() then
    begin
      FGeraBloco := PodeGerarBloco('0');
      IND_MOV := dmkPF.EscolhaDe2Int(FGeraBloco, 0, 1);
      if GeraRegistro_0001(IND_MOV) then
      begin
        if FGeraBloco then
        begin
          GeraRegistro_0002();
          GeraRegistro_0005();
          GeraRegistro_0100();
          if (RGPreenche.ItemIndex = 2) then
          begin
            if GeraRegistro_0150() then
            begin
              // Falta fazer!
              //if ? then GeraRegistro_0175() then
            end;
            InfReg := UpperCase(QrParamsEmpSPED_EFD_IND_PERFIL.Value) <> 'C';
            if InfReg then
            begin
              GeraRegistro_0190_Novo();
              GeraRegistro_0200_Novo();
              if FSelReg0400 then
                GeraRegistro_0400();
              if FSelReg0460 then
                GeraRegistro_0460_B();
              //
              AdicionaBLC(FQTD_LIN_0460, '0460');
              AdicionaBLC(FQTD_LIN_0400, '0400');
            end;
          end;
        end;
      end;
      GeraRegistro_0990();
    end;
    //
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   B                                                          //
    ////////////////////////////////////////////////////////////////////////////
    IND_DAD := 1; // N�o gera!
    if GeraRegistro_B001(IND_DAD) then
    begin
      // ...
      GeraRegistro_B990();
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   C                                                          //
    ////////////////////////////////////////////////////////////////////////////
    if FSelRegC100 or FSelRegC500 then
    begin
      FGeraBloco := (RGPreenche.ItemIndex = 2) and PodeGerarBloco('C');
      if FGeraBloco then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando para gerar o bloco "C"');
        ReopenCabA(True, 'C100');
        ReopenEFD_C500();
        Registros := QrCabA_C.RecordCount + QrEFD_C500.RecordCount;
        IND_MOV := dmkPF.EscolhaDe2Int(Registros > 0, 0, 1);
      end else
        IND_MOV := 1;
    end else
      IND_MOV := 1;
    if GeraRegistro_C001(IND_MOV) then
    begin
      if FGeraBloco then
      begin
        if (RGPreenche.ItemIndex = 2) then
        begin
          PB1.Position := 0;
          PB1.Max := QrCabA_C.RecordCount;
          if FSelRegC100 then
          begin
            QrCabA_C.First;
            while not QrCabA_C.Eof do
            begin
              MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
              //
              // ini 2022-04-12
              //if GeraRegistro_C100() then
              case QrCabA_CFatID.Value of
                VAR_FATID_0001:
                begin
                  Emitente  := QrCabA_CCodInfoEmit.Value;
                  GerouC100 := GeraRegistro_C100_Emit();
                end;
                VAR_FATID_0053:
                begin
                  Emitente  := QrEfdInnNFsCabTerceiro.Value;
                  GerouC100 := GeraRegistro_C100_Entr();
                  //GerouC100 := False;
                end;
                else
                begin
                  GerouC100 := False;
                  Geral.MB_Aviso('FatID ' + Geral.FF0(QrCabA_CFatID.Value) +
                  ' n�o implementado para gera��o do registro C100!');
                end;
              end;
              if GerouC100 then
              begin
                //
      (*          // 2016-02-07 Nao entendi porque!!!\*)
                if (Emitente = FEmpresa_int) then
                  InfReg := False
                else
                  InfReg := True;
                if UpperCase(QrParamsEmpSPED_EFD_IND_PERFIL.Value) = 'C' then
                   InfReg := False;
                if InfReg then
                begin
                  // Itens
                  case QrCabA_CFatID.Value of
                    VAR_FATID_0001:
                    begin
(*
                      UnDmkDAC_PF.AbreMySQLQuery0(QrI, Dmod.MyDB, [
                      'SELECT itsi.*,  ',
                      'itsn.ICMS_Orig, itsn.ICMS_CST, itsn.ICMS_vBC, ',
                      'itsn.ICMS_pICMS, itsn.ICMS_vICMS, itsn.ICMS_vBCST, ',
                      'itsn.ICMS_pICMSST, itsn.ICMS_vICMSST,  ',
                      ' ',
                      'itso.IPI_cEnq, itso.IND_APUR, itso.IPI_CST,  ',
                      'itso.IPI_vBC, itso.IPI_pIPI, itso.IPI_vIPI, ',
                      ' ',
                      'itsq.PIS_CST, itsq.PIS_vBC,  ',
                      'itsq.PIS_pPIS, itsq.PIS_vPIS, ',
                      ' ',
                      'itsr.PISST_vBC,  ',
                      'itsr.PISST_pPIS, itsr.PISST_vPIS, ',
                      ' ',
                      'itss.COFINS_CST, itss.COFINS_vBC,  ',
                      'itss.COFINS_pCOFINS, itss.COFINS_vCOFINS, ',
                      ' ',
                      'itst.COFINSST_vBC,  ',
                      'itst.COFINSST_pCOFINS, itst.COFINSST_vCOFINS ',
                      ' ',
                      ' ',
                      'FROM nfeitsi itsi ',
                      ' ',
                      'LEFT JOIN nfeitsn itsn ON  ',
                      '  itsn.FatID=itsi.FatID ',
                      '  AND itsn.FatNum=itsi.FatNum ',
                      '  AND itsn.Empresa=itsi.Empresa ',
                      '  AND itsn.nItem=itsi.nItem ',
                      ' ',
                      'LEFT JOIN nfeitso itso ON ',
                      '  itso.FatID=itsi.FatID ',
                      '  AND itso.FatNum=itsi.FatNum ',
                      '  AND itso.Empresa=itsi.Empresa ',
                      '  AND itso.nItem=itsi.nItem ',
                      ' ',
                      'LEFT JOIN nfeitsq itsq ON ',
                      '  itsq.FatID=itsi.FatID ',
                      '  AND itsq.FatNum=itsi.FatNum ',
                      '  AND itsq.Empresa=itsi.Empresa ',
                      '  AND itsq.nItem=itsi.nItem ',
                      ' ',
                      'LEFT JOIN nfeitsr itsr ON ',
                      '  itsr.FatID=itsi.FatID ',
                      '  AND itsr.FatNum=itsi.FatNum ',
                      '  AND itsr.Empresa=itsi.Empresa ',
                      '  AND itsr.nItem=itsi.nItem ',
                      ' ',
                      'LEFT JOIN nfeitss itss ON ',
                      '  itss.FatID=itsi.FatID ',
                      '  AND itss.FatNum=itsi.FatNum ',
                      '  AND itss.Empresa=itsi.Empresa ',
                      '  AND itss.nItem=itsi.nItem ',
                      ' ',
                      'LEFT JOIN nfeitst itst ON ',
                      '  itst.FatID=itsi.FatID ',
                      '  AND itst.FatNum=itsi.FatNum ',
                      '  AND itst.Empresa=itsi.Empresa ',
                      '  AND itst.nItem=itsi.nItem ',
                      ' ',
                      ' ',
                      'WHERE itsi.FatID=' + Geral.FF0(QrCabA_CFatID.Value),
                      'AND itsi.FatNum='  + Geral.FF0(QrCabA_CFatNum.Value),
                      'AND itsi.Empresa=' + Geral.FF0(QrCabA_CEmpresa.Value),
                      'ORDER BY itsi.nItem ',
                      '']);
*)
                      SPED_PF.ReopenNFeItens_EFD(QrI, QrCabA_CFatID.Value,
                        QrCabA_CFatNum.Value, QrCabA_CEmpresa.Value);
                      //
                      QrI.First;
                      while not QrI.Eof do
                      begin
                        GeraRegistro_C170_Emit();
                        //
                        QrI.Next;
                      end;
                    end;
                    VAR_FATID_0053:
                    begin
                      UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnNFsIts, Dmod.MyDB, [
                      'SELECT * ',
                      'FROM efdinnnfsits',
                      'WHERE Controle=' + Geral.FF0(QrEfdInnNFsCabControle.Value),
                      'ORDER BY Conta ',
                      '']);
                      QrEfdInnNFsIts.First;
                      while not QrEfdInnNFsIts.Eof do
                      begin
                        GeraRegistro_C170_Entr();
                        //
                        QrEfdInnNFsIts.Next;
                      end;
                    end;
                    else
                    begin
                      //GerouC170 := False;
                      Geral.MB_Aviso('FatID ' + Geral.FF0(QrCabA_CFatID.Value) +
                      ' n�o implementado para gera��o do registro C170!');
                    end;
                  end;
                end;
                // Fim itens
                //
                // Somat�rios por CST, CFOP, AliqICMS dos Itens
                case QrCabA_CFatID.Value of
                  VAR_FATID_0001:
                    GeraRegistro_C190_Emit();
                  VAR_FATID_0053:
                    GeraRegistro_C190_Entr(QrEfdInnNFsCabControle.Value);
                end;
                GeraRegistro_C195(QrEfdInnNFsCabControle.Value);
                //
              end;
              QrCabA_C.Next;
            end;
          end;
          //
          // Energia El�trica, �gua, G�s
          if FSelRegC500 then
          begin
            QrEFD_C500.First;
            while not QrEFD_C500.Eof do
            begin
              GeraRegistro_C500();
      { TODO : Fazendo bloco C }
              GeraRegistro_C590();
              //
              QrEFD_C500.Next;
            end;
          end;
          //
          if FSelRegC100 then
          begin
            AdicionaBLC(QrCabA_C.RecordCount, 'C100');
            AdicionaBLC(FQTD_LIN_C170, 'C170');
            AdicionaBLC(FQTD_LIN_C190, 'C190');
            AdicionaBLC(FQTD_LIN_C195, 'C195');
            AdicionaBLC(FQTD_LIN_C197, 'C197');
          end;
          if FSelRegC500 then
          begin
            AdicionaBLC(QrEFD_C500.RecordCount, 'C500');
            AdicionaBLC(FQTD_LIN_C590, 'C590');
          end;
          //
          PB1.Position := 0;
        end;
      end;
      GeraRegistro_C990();
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   D                                                          //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('D');
    //
    if FGeraBloco then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando para gerar o bloco "D"');
      //
      (*
      UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_D100, Dmod.MyDB, [
      'SELECT * FROM efdicmsipid100 ',
      'WHERE ImporExpor<>2',
      'AND AnoMes=' + FAnoMes_Txt,
      'AND Empresa=' + FEmpresa_TXT,
      '']);
      *)
      UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnCTsCab, Dmod.MyDB, [
      'SELECT * ',
      'FROM efdinnctscab ',
      'WHERE ' + FDtFiscalFrete_Entr + ' BETWEEN "' + FDataIni_TXT + '" AND "' + FDataFim_TXT + '"',
      'ORDER BY ' + FDtFiscalFrete_Entr + ', Controle ',
      '']);
      //Geral.MB_Teste(QrEfdInnCTsCab.SQL.Text);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_D500, Dmod.MyDB, [
      'SELECT * FROM efdicmsipid500 ',
      'WHERE ImporExpor<>2',
      'AND AnoMes=' + FAnoMes_Txt,
      'AND Empresa=' + FEmpresa_TXT,
      '']);
      //
      //Registros := QrEFD_D100.RecordCount + QrEFD_D500.RecordCount;
      Registros := QrEfdInnCTsCab.RecordCount + QrEFD_D500.RecordCount;
      //
      if FSelRegD100 or FSelRegD500 then
        IND_MOV := dmkPF.EscolhaDe2Int(Registros > 0, 0, 1)
      else
        IND_MOV := 1;
    end else
      IND_MOV := 1;
    //
    if GeraRegistro_D001(IND_MOV) then
    begin
      if FGeraBloco then
      begin
        if (RGPreenche.ItemIndex = 2) then
        begin
          if FSelRegD100 then
          begin
            GeraRegistro_D100_Novo();
            //
            AdicionaBLC(QrEfdInnCTsCab.RecordCount, 'D100');
            AdicionaBLC(FQTD_LIN_D190, 'D190');
            AdicionaBLC(FQTD_LIN_D195, 'D195');
            AdicionaBLC(FQTD_LIN_D197, 'D197');
          end;
          //
          if FSelRegD500 then
          begin
            QrEFD_D500.First;
            while not QrEFD_D500.Eof do
            begin
              GeraRegistro_D500();
              GeraRegistro_D590();
              //
              QrEFD_D500.Next;
            end;
            AdicionaBLC(QrEFD_D500.RecordCount, 'D500');
            AdicionaBLC(FQTD_LIN_D590, 'D590');
          end;
        end;
  { TODO : Fazendo bloco D }
      end;
      GeraRegistro_D990();
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   E                                                          //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('E');
    if FGeraBloco then
    begin
      (*************************************************************************
      //ICMS
      *************************************************************************)
      UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E100, Dmod.MyDB, [
      'SELECT * FROM efdicmsipie100 ',
      'WHERE ImporExpor=3',
      'AND AnoMes=' + FAnoMes_Txt,
      'AND Empresa=' + FEmpresa_TXT,
      '']);
      (*************************************************************************
      //IPI
      *************************************************************************)
        UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E500, Dmod.MyDB, [
        'SELECT * FROM efdicmsipie500 ',
        'WHERE ImporExpor=3',
        'AND AnoMes=' + FAnoMes_Txt,
        'AND Empresa=' + FEmpresa_TXT,
        '']);
      (*************************************************************************
      //
      *************************************************************************)
      //TemMovim   := QrEFD_E100.RecordCount > 0;
      TemMovim   := (QrEFD_E100.RecordCount > 0)
                 or
                 (QrEFD_E500.RecordCount > 0)
                 or
                 (QrParamsEmpSPED_EFD_IND_PERFIL.Value <> 'A');
      IND_MOV    := dmkPF.EscolhaDe2Int(TemMovim, 0, 1);
//      if (FQTD_LIN_C190 <> 0) and (TemMovim = False) then
      if TemMovim and (QrEFD_E100.RecordCount = 0) then
      begin
        //Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG D110 do D100');
        Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG E100 e E110');
        Geral.MB_Aviso('Nenhum Per�odo de apura��o do ICMS foi cadastrado!' +
        sLineBreak + 'Cadastre na janela de apura��o de ICMS / IPI!');
      end;
      if GeraRegistro_E001(IND_MOV) then
      begin
        if TemMovim then
        begin
          QrEFD_E100.First;
          while not QrEFD_E100.Eof do
          begin
            GeraRegistro_E100();
            GeraRegistro_E110();
            //
            if (QrEFD_E111.State <> dsInactive) then
            begin
              QrEFD_E111.First;
              while not QrEFD_E111.Eof do
              begin
                GeraRegistro_E111();
                //
                QrEFD_E112.First;
                while not QrEFD_E112.Eof do
                begin
                  GeraRegistro_E112();
                  QrEFD_E112.Next;
                end;
                //
                QrEFD_E113.First;
                while not QrEFD_E113.Eof do
                begin
                  GeraRegistro_E113();
                  QrEFD_E113.Next;
                end;
                //
                QrEFD_E111.Next;
              end;
            end;
            //
            if (QrEFD_E115.State <> dsInactive) then
            begin
              QrEFD_E115.First;
              while not QrEFD_E115.Eof do
              begin
                GeraRegistro_E115();
                QrEFD_E115.Next;
              end;
            end;
            //
            if (QrEFD_E116.State <> dsInactive) then
            begin
              QrEFD_E116.First;
              while not QrEFD_E116.Eof do
              begin
                GeraRegistro_E116();
                QrEFD_E116.Next;
              end;
            end;
            //
            QrEFD_E100.Next;
          end;
          AdicionaBLC(QrEFD_E100.RecordCount, 'E100');
          AdicionaBLC(QrEFD_E100.RecordCount, 'E110');
          AdicionaBLC(FQTD_LIN_E111, 'E111');
          AdicionaBLC(FQTD_LIN_E112, 'E112');
          AdicionaBLC(FQTD_LIN_E113, 'E113');
          AdicionaBLC(FQTD_LIN_E115, 'E115');
          AdicionaBLC(FQTD_LIN_E116, 'E116');
        end;
        (*************************************************************************
        //IPI
        *************************************************************************)
        //InfReg := UpperCase(QrParamsEmpSPED_EFD_IND_PERFIL.Value) <> 'C';
        //if InfReg then
        //begin
        TemMovim   := QrEFD_E500.RecordCount > 0;
        IND_MOV    := dmkPF.EscolhaDe2Int(TemMovim, 0, 1);
        if (FQTD_LIN_C590 <> 0) and (TemMovim = False) then
        begin
          //Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG D550 do D500');
          Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG E500 e E550');
          Geral.MB_Aviso('Nenhum Per�odo de apura��o do IPI foi cadastrado!' +
          sLineBreak + 'Cadastre na janela de apura��o de IPI / IPI!');
        end;
        //if GeraRegistro_E001(IND_MOV) then
        if TemMovim then
        begin
          QrEFD_E500.First;
          while not QrEFD_E500.Eof do
          begin
            GeraRegistro_E500();
            GeraRegistro_E510();
            GeraRegistro_E520();
            if (QrEFD_E530.State <> dsInactive) then
            begin
              QrEFD_E530.First;
              while not QrEFD_E530.Eof do
              begin
                GeraRegistro_E530();
                //
                QrEFD_E530.Next;
              end;
            end;
            //
            QrEFD_E500.Next;
          end;
          AdicionaBLC(QrEFD_E500.RecordCount, 'E500');
          //AdicionaBLC(QrEFD_E500.RecordCount, 'E520');
          AdicionaBLC(FQTD_LIN_E520, 'E520');
          AdicionaBLC(FQTD_LIN_E530, 'E530');
        end else
        begin
          if QrParamsEmpSPED_EFD_IND_PERFIL.Value <> 'C' then
          begin
            Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG E500 e filhos');
            Geral.MB_Aviso('Nenhum Per�odo de apura��o do IPI foi cadastrado!' +
            sLineBreak + 'Cadastre na janela de apura��o de IPI / IPI!');
          end;
        end;
      end;
      GeraRegistro_E990();
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   G                                                          //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('G');
    if FGeraBloco then
    begin
{ TODO : Fazer bloco G }
      TemMovim   := False;
      IND_MOV := dmkPF.EscolhaDe2Int(TemMovim, 0, 1);
      GeraRegistro_G001(IND_MOV);
      GeraRegistro_G990();
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   H                                                          //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('H');
    if FGeraBloco then
    begin
      (*************************************************************************
      //Inventario
      *************************************************************************)
      UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_H005, Dmod.MyDB, [
      'SELECT * FROM efdicmsipih005 ',
      'WHERE ImporExpor=3',
      'AND AnoMes=' + FAnoMes_Txt,
      'AND Empresa=' + FEmpresa_TXT,
      '']);
      TemMovim   := QrEFD_H005.RecordCount > 0;
      IND_MOV    := dmkPF.EscolhaDe2Int(TemMovim, 0, 1);
      if GeraRegistro_H001(IND_MOV) then
      begin
  (*    Precisa ?????
        if (FQTD_LIN_???? <> 0) and (TemMovim = False) then
        begin
          Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG H005 e H???');
          Geral.MB_Aviso('Nenhum Per�odo de Invent�rio foi cadastrado!' +
          sLineBreak + 'Cadastre na janela de apura��o de ICMS / IPI!');
        end;
  *)
        //if GeraRegistro_E001(IND_MOV) then
        if TemMovim then
        begin
          QrEFD_H005.First;
          while not QrEFD_H005.Eof do
          begin
            GeraRegistro_H005();
            if (QrEFD_H010.State <> dsInactive) then
            begin
              QrEFD_H010.First;
              while not QrEFD_H010.Eof do
              begin
                GeraRegistro_H010();
                //
                if (QrEFD_H020.State <> dsInactive) then
                begin
                  QrEFD_H020.First;
                  while not QrEFD_H020.Eof do
                  begin
                    GeraRegistro_H020();
                    //
                    QrEFD_H020.Next;
                  end;
                end;
                QrEFD_H010.Next;
              end;
            end;
            //
            QrEFD_H005.Next;
          end;
          AdicionaBLC(QrEFD_H005.RecordCount, 'H005');
          //AdicionaBLC(QrEFD_H005.RecordCount, 'E520');
          AdicionaBLC(FQTD_LIN_H010, 'H010');
          AdicionaBLC(FQTD_LIN_H020, 'H020');
        end;
        GeraRegistro_H990();
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   K                                                          //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('K');
    if FGeraBloco then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K100, Dmod.MyDB, [
      'SELECT * FROM efdicmsipik100 ',
      'WHERE ImporExpor=3',
      'AND AnoMes=' + FAnoMes_Txt,
      'AND Empresa=' + FEmpresa_TXT,
      '']);
      TemMovim   := QrEFD_K100.RecordCount > 0;
      IND_MOV    := dmkPF.EscolhaDe2Int(TemMovim, 0, 1);
      if GeraRegistro_K001(IND_MOV) then
      begin
        //
        if TemMovim then
        begin
          QrEFD_K100.First;
          while not QrEFD_K100.Eof do
          begin
            GeraRegistro_K100();
            //
            PB1.Position := 0;
            PB1.Max := QrEFD_K200.RecordCount;
            QrEFD_K200.First;
            while not QrEFD_K200.Eof do
            begin
              MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
              GeraRegistro_K200();
              //
              QrEFD_K200.Next;
            end;
            //
            if FSelRegK210 then
            begin
              PB1.Position := 0;
              PB1.Max := QrEFD_K210.RecordCount;
              QrEFD_K210.First;
              while not QrEFD_K210.Eof do
              begin
                MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
                GeraRegistro_K210();
                //
                QrEFD_K215.First;
                while not QrEFD_K215.Eof do
                begin
                  GeraRegistro_K215();
                  //
                  QrEFD_K215.Next;
                end;
                QrEFD_K210.Next;
              end;
            end;
            //
            if FSelRegK220 then
            begin
              PB1.Position := 0;
              PB1.Max := QrEFD_K220.RecordCount;
              QrEFD_K220.First;
              while not QrEFD_K220.Eof do
              begin
                MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
                GeraRegistro_K220();
                //
                QrEFD_K220.Next;
              end;
            end;
            //
            if FSelRegK230 then
            begin
              PB1.Position := 0;
              PB1.Max := QrEFD_K230.RecordCount;
              QrEFD_K230.First;
              while not QrEFD_K230.Eof do
              begin
                MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
                GeraRegistro_K230();
                //
                QrEFD_K235.First;
                while not QrEFD_K235.Eof do
                begin
                  GeraRegistro_K235();
                  //
                  QrEFD_K235.Next;
                end;
                QrEFD_K230.Next;
              end;
            end;
            //
            if FSelRegK250 then
            begin
              PB1.Position := 0;
              PB1.Max := QrEFD_K250.RecordCount;
              QrEFD_K250.First;
              while not QrEFD_K250.Eof do
              begin
                MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
                GeraRegistro_K250();
                //
                QrEFD_K255.First;
                while not QrEFD_K255.Eof do
                begin
                  GeraRegistro_K255();
                  //
                  QrEFD_K255.Next;
                end;
                QrEFD_K250.Next;
              end;
            end;
            //
            PB1.Position := 0;
            PB1.Max := QrEFD_K280.RecordCount;
            QrEFD_K280.First;
            while not QrEFD_K280.Eof do
            begin
              MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
              GeraRegistro_K280();
              //
              QrEFD_K280.Next;
            end;
            //
            if FSelRegK290 then
            begin
              PB1.Position := 0;
              PB1.Max := QrEFD_K290.RecordCount;
              QrEFD_K290.First;
              while not QrEFD_K290.Eof do
              begin
                MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
                GeraRegistro_K290();
                //
                QrEFD_K291.First;
                while not QrEFD_K291.Eof do
                begin
                  MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
                  GeraRegistro_K291();
                  //
                  QrEFD_K291.Next;
                end;
                //
                QrEFD_K292.First;
                while not QrEFD_K292.Eof do
                begin
                  MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
                  GeraRegistro_K292();
                  //
                  QrEFD_K292.Next;
                end;
                //
                QrEFD_K290.Next;
              end;
            end;
            //
            if FSelRegK300 then
            begin
              PB1.Position := 0;
              PB1.Max := QrEFD_K300.RecordCount;
              QrEFD_K300.First;
              while not QrEFD_K300.Eof do
              begin
                MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
                GeraRegistro_K300();
                //
                QrEFD_K301.First;
                while not QrEFD_K301.Eof do
                begin
                  MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
                  GeraRegistro_K301();
                  //
                  QrEFD_K301.Next;
                end;
                //
                QrEFD_K302.First;
                while not QrEFD_K302.Eof do
                begin
                  MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
                  GeraRegistro_K302();
                  //
                  QrEFD_K302.Next;
                end;
                //
                QrEFD_K300.Next;
              end;
            end;
            //
            QrEFD_K100.Next;
          end;
          AdicionaBLC(FQTD_LIN_K100, 'K100');
          AdicionaBLC(FQTD_LIN_K200, 'K200');
          AdicionaBLC(FQTD_LIN_K210, 'K210');
          AdicionaBLC(FQTD_LIN_K215, 'K215');
          AdicionaBLC(FQTD_LIN_K220, 'K220');
          AdicionaBLC(FQTD_LIN_K230, 'K230');
          AdicionaBLC(FQTD_LIN_K235, 'K235');
          AdicionaBLC(FQTD_LIN_K250, 'K250');
          AdicionaBLC(FQTD_LIN_K255, 'K255');
          AdicionaBLC(FQTD_LIN_K280, 'K280');
          AdicionaBLC(FQTD_LIN_K290, 'K290');
          AdicionaBLC(FQTD_LIN_K291, 'K291');
          AdicionaBLC(FQTD_LIN_K292, 'K292');
          AdicionaBLC(FQTD_LIN_K300, 'K300');
          AdicionaBLC(FQTD_LIN_K301, 'K301');
          AdicionaBLC(FQTD_LIN_K302, 'K302');
        end;
        GeraRegistro_K990();
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   1                                                          //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('1');
    if FGeraBloco then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_1010, Dmod.MyDB, [
      'SELECT * FROM efdicmsipi1010 ',
      'WHERE ImporExpor=3',
      'AND AnoMes=' + FAnoMes_Txt,
      'AND Empresa=' + FEmpresa_TXT,
      '']);
      TemMovim   := (QrEFD_1010.RecordCount > 0);
      //TemMovim   := True;
      if TemMovim and (QrEFD_1010.RecordCount = 0) then
      begin
        Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG 1010');
        Geral.MB_Aviso('O registro de obrigatoriedade de refgistros do bloco 1 n�o foi cadastrado!' +
        sLineBreak + 'Cadastre na janela de apura��o de ICMS / IPI - Aba "1-Outros"!');
      end;
      IND_MOV    := dmkPF.EscolhaDe2Int(TemMovim, 0, 1);
      GeraRegistro_1001(IND_MOV);
      begin
        if TemMovim then
        begin
  { TODO : Fazer bloco 1 }
          GeraRegistro_1010();
        end;
      end;
    end;
    GeraRegistro_1990();
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   9                                                          //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('9');
    IND_MOV := dmkPF.EscolhaDe2Int(FGeraBloco, 0, 1);
    if GeraRegistro_9001(IND_MOV) then
    begin
      if FGeraBloco then
      begin
        GeraRegistro_9900();
        GeraRegistro_9990();
      end;
      GeraRegistro_9999();
    end;
    PB1.Position := 0;
    //
    if NaoTemErros(False) then ; // deixar gravar pois alguns erros podem ser aceitos
    begin
      if SalvaArquivo() then
      begin
        EfdIcmsIpi_PF.EncerraMesSPED(FImporExpor, FAnoMes_Int, FEmpresa_Int, 'EnvioEFD');
        MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
      end;
    end;
    //
    if FContaIND_MOV_1 > 0 then
      Geral.MB_Info(Geral.FF0(FContaIND_MOV_1) +
        ' itens de NF de sa�da sem movimenta��o de estoque!');
  finally
    if LaAviso1.Caption <> '' then
    begin
      P := pos('...', LaAviso1.Caption) + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'EXPORTA��O ABORTADA EM: ' +
        Copy(LaAviso1.Caption, P));
    end
    else
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Exporta��o finalizada!');
    //
    Screen.Cursor := crDefault;
    if FArqStr <> nil then
      FArqStr.Free;
    //
    if MeErros.Lines.Count > 0 then
      PageControl1.ActivePageIndex := 2;
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.BuscaValorMesAnterior(Tabela,
  Campo: String): Double;
var
  Qry: TmySQLQuery;
  Periodo: Integer;
  //
  procedure AbreQuery(Campo: String);
  begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ' + campo + ' MyValor',
      'FROM efdicmsipi' + Lowercase(Tabela),
      'WHERE ImporExpor=3',//; + Geral.FF0(FImporExpor),
      'AND Empresa=' + Geral.FF0(QrEFD_E100Empresa.Value),
      'AND AnoMes=' + Geral.FF0(Periodo),
      '']);
  end;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
  if Tabela = 'e110' then
  begin
    if Campo = 'VL_SLD_CREDOR_ANT' then
    begin
      Periodo := dmkPF.IncrementaAnoMes(QrEFD_E100AnoMes.Value, -1);
      AbreQuery('VL_SLD_CREDOR_TRANSPORTAR');
      if Qry.recordCount > 0 then
        Result := Qry.FieldByName('MyValor').AsFloat
      else
      begin
        Periodo := QrEFD_E100AnoMes.Value;
        AbreQuery('VL_SLD_CREDOR_ANT');
        Result := Qry.FieldByName('MyValor').AsFloat;
      end;
    end;
  end;
  finally
    Qry.Free;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CalculaCampoEFD_E110_02(): Double;
(*  Guia Pr�tico EFD-ICMS/IPI � Vers�o 2.0.18
    Atualiza��o: 21/12/2015
    Pagina 127 de 224
Campo 02 (VL_TOT_DEBITOS) - Valida��o: o valor informado deve corresponder ao somat�rio de todos os documentos
fiscais de sa�da que geram d�bito de ICMS. Deste somat�rio, est�o exclu�dos os documentos extempor�neos (COD_SIT
com valor igual �01�), os documentos complementares extempor�neos (COD_SIT com valor igual �07�) e os documentos
fiscais com CFOP 5605 � Transfer�ncia de saldo devedor de ICMS de outro estabelecimento da mesma empresa. Devem
ser inclu�dos os documentos fiscais com CFOP igual a 1605 - Recebimento, por transfer�ncia, de saldo devedor do ICMS
de outro estabelecimento da mesma empresa.
O valor neste campo deve ser igual � soma dos VL_ICMS de todos os registros C190, C320, C390, C490, C590, C690,
C790, C850, C890, D190, D300, D390, D410, D590, D690, D696, com as datas dos campos DT_DOC (C300, C405,
C600, D300, D355, D400, D600) ou DT_E_S (C100, C500) ou DT_DOC_FIN (C700, D695) ou DT_A_P (D100, D500)
dentro do per�odo informado no registro E100.
Quando o campo DT_E_S ou DT_A_P n�o for informado, utilizar o campo DT_DOC.
Para os estados que utilizam como data da escritura��o a data de emiss�o, todos os documentos devem ser declarados na
compet�ncia da emiss�o. Neste caso, se a data de sa�da (DT_E_S ou DT_A_P) for posterior � data final informada no
campo 03 do registro E100, o campo referente � data de sa�da n�o deve ser preenchido.
Para os estados que utilizam como data da escritura��o a data de efetiva sa�da, todos os documentos devem ser declarados
na compet�ncia espec�fica da data de sa�da como documento regular (COD_SIT igual a �00�), obedecendo � legisla��o
estadual pertinente.
*)
const
  Tabelas: array [0..15] of String = ('spedefdicmsipiC190', 'spedefdicmsipiC320', 'spedefdicmsipiC390', 'spedefdicmsipiC490', 'spedefdicmsipiC590', 'spedefdicmsipiC690', 'spedefdicmsipiC790', 'spedefdicmsipiC850', 'spedefdicmsipiC890', 'spedefdicmsipiD190', 'spedefdicmsipiD300', 'spedefdicmsipiD390', 'spedefdicmsipiD410', 'spedefdicmsipiD590', 'spedefdicmsipiD690', 'spedefdicmsipiD696');
  TabsPai: array [0..15] of String = ('spedefdicmsipiC100', 'spedefdicmsipiC300', 'spedefdicmsipiC300', 'spedefdicmsipiC405', 'spedefdicmsipiC500', 'spedefdicmsipiC600', 'spedefdicmsipiC700', 'spedefdicmsipiC800', 'spedefdicmsipiC860', 'spedefdicmsipiD100', 'spedefdicmsipiD300', 'spedefdicmsipiD355', 'spedefdicmsipiD400', 'spedefdicmsipiD500', 'spedefdicmsipiD600', 'spedefdicmsipiD695');
  FldsDta: array [0..15] of String = ('DT_E_S',      'DT_DOC',      'DT_DOC',      'DT_DOC',      'DT_E_S',      'DT_DOC',       'DT_DOC_FIN', 'DT_DOC',      'DT_DOC',      'DT_A_P',      'DT_DOC',      'DT_DOC',      'DT_DOC',      'DT_A_P',      'DT_DOC',      'DT_DOC_FIN' );
var
  Qry: TmySQLQuery;
  I: Integer;
  //VL_ICMS: Double;
  Tabela, TabPai, Campo, FldData, DtApurIni, DtApurFim: String;
begin
  Result    := 0;
  DtApurIni := Geral.FDT(QrEFD_E100DT_INI.Value, 1);
  DtApurFim := Geral.FDT(QrEFD_E100DT_FIN.Value, 1);
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    for I := Low(Tabelas) to High(Tabelas) do
    begin
      Tabela  := LowerCase(Tabelas[I]);
      TabPai  := LowerCase(TabsPai[I]);
      //Campo   := Copy(TabPai, 8);
      Campo   := Copy(TabPai, 15);
      FldData := LowerCase(FldsDta[I]);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SHOW TABLES ',
      'FROM ' + TMeuDB,
      'LIKE "' + Tabela + '" ',
      '']);
      if Qry.RecordCount > 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT SUM(VL_ICMS) VL_ICMS ',
        'FROM ' + Tabela,
        'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
        'AND Empresa=' + Geral.FF0(QrEFD_E100Empresa.Value),
        'AND AnoMes=' + Geral.FF0(QrEFD_E100AnoMes.Value),
        'AND ' + Campo + ' IN (',
        '  SELECT LinArq',
        '  FROM ' + TabPai,
        '  WHERE ImporExpor=' + Geral.FF0(FImporExpor),
        '  AND Empresa=' + Geral.FF0(QrEFD_E100Empresa.Value),
        //'  AND COD_PART=' + Geral.FF0(QrEFD_E100Empresa.Value),
        '  AND AnoMes=' + Geral.FF0(QrEFD_E100AnoMes.Value),
        '  AND ' + FldData + ' BETWEEN "' + DtApurIni + '" AND "' + DtApurFim + '"',
        //'  AND NOT (COD_SIT IN (01, 07)) ',
        '  AND COD_SIT IN (00, 06, 08) ',
        ')',
        'AND (',
        '  (',
        '    (CFOP DIV 1000 IN (5,6,7)) AND (CFOP<>5605) ',
        '  OR',
        '    (CFOP = 1605)',
        '  )',
        ')',
        '']);
        //Geral.MB_teste(Qry.SQL.Text);
        Result := Result + Qry.FieldByName('VL_ICMS').AsFloat;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CalculaCampoEFD_E110_06(): Double;
(*  Guia Pr�tico EFD-ICMS/IPI � Vers�o 2.0.18
    Atualiza��o: 21/12/2015
    Pagina 128 de 224
Campo 06 (VL_TOT_CREDITOS) - Valida��o: o valor informado deve corresponder ao somat�rio de todos os
documentos fiscais de entrada que geram cr�dito de ICMS. O valor neste campo deve ser igual � soma dos VL_ICMS de
todos os registros C190, C590, D190 e D590. Deste somat�rio, est�o exclu�dos os documentos fiscais com CFOP 1605 -
Recebimento, por transfer�ncia, de saldo devedor do ICMS de outro estabelecimento da mesma empresa e inclu�dos os
documentos fiscais com CFOP 5605 � Transfer�ncia de saldo devedor de ICMS de outro estabelecimento da mesma
empresa. Os documentos fiscais devem ser somados conforme o per�odo informado no registro E100 e a data informada no
campo DT_E_S (C100, C500) ou campo DT_A_P (D100, D500), exceto se COD_SIT do documento for igual a �01�
(extempor�neo) ou igual a 07 (NF Complementar extempor�nea), cujo valor ser� somado no primeiro per�odo de apura��o
informado no registro E100.
Quando o campo DT_E_S ou DT_A_P n�o for informado, � utilizada a data constante no campo DT_DOC.
*)
const
  Tabelas: array [0..3] of String = ('spedefdicmsipiC190', 'spedefdicmsipiC590', 'spedefdicmsipiD190', 'spedefdicmsipiD590');
  TabsPai: array [0..3] of String = ('spedefdicmsipiC100', 'spedefdicmsipiC500', 'spedefdicmsipiD100', 'spedefdicmsipiD500');
  FldsDta: array [0..3] of String = ('DT_E_S',      'DT_E_S',      'DT_A_P',      'DT_A_P'     );
var
  Qry: TmySQLQuery;
  I: Integer;
  //VL_ICMS: Double;
  Tabela, TabPai, Campo, FldData, DtApurIni, DtApurFim: String;
begin
  Result    := 0;
  DtApurIni := Geral.FDT(QrEFD_E100DT_INI.Value, 1);
  DtApurFim := Geral.FDT(QrEFD_E100DT_FIN.Value, 1);
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    for I := Low(Tabelas) to High(Tabelas) do
    begin
      Tabela  := LowerCase(Tabelas[I]);
      TabPai  := LowerCase(TabsPai[I]);
      //Campo   := Copy(TabPai, 8);
      Campo   := Copy(TabPai, 15);
      FldData := LowerCase(FldsDta[I]);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SHOW TABLES ',
      'FROM ' + TMeuDB,
      'LIKE "' + Tabela + '" ',
      '']);
      if Qry.RecordCount > 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT SUM(VL_ICMS) VL_ICMS ',
        'FROM ' + Tabela,
        'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
        'AND Empresa=' + Geral.FF0(QrEFD_E100Empresa.Value),
        'AND AnoMes=' + Geral.FF0(QrEFD_E100AnoMes.Value),
        'AND ' + Campo + ' IN (',
        '  SELECT LinArq',
        '  FROM ' + TabPai,
        '  WHERE ImporExpor=' + Geral.FF0(FImporExpor),
        '  AND Empresa=' + Geral.FF0(QrEFD_E100Empresa.Value),
        //'  AND COD_PART=' + Geral.FF0(QrEFD_E100Empresa.Value),
        '  AND AnoMes=' + Geral.FF0(QrEFD_E100AnoMes.Value),
        '  AND ' + FldData + ' BETWEEN "' + DtApurIni + '" AND "' + DtApurFim + '"',
        //'  AND NOT (COD_SIT IN (01, 07)) ',
        '  AND COD_SIT IN (00, 06, 08) ',
        ')',
        'AND (',
        '  (',
        '    (CFOP DIV 1000 IN (1,2,3)) AND (CFOP<>1605) ',
        '  OR',
        '    (CFOP = 5605)',
        '  )',
        ')',
        '']);
        //Geral.MB_SQL(Self, Qry);
        Result := Result + Qry.FieldByName('VL_ICMS').AsFloat;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CalculaCampoEFD_E520_03(): Double;
var
  Qry: TmySQLQuery;
  Tabela, TabPai, Campo, FldData, DtApurIni, DtApurFim: String;
begin
  Result    := 0;
  DtApurIni := Geral.FDT(QrEFD_E100DT_INI.Value, 1);
  DtApurFim := Geral.FDT(QrEFD_E100DT_FIN.Value, 1);
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    Tabela  := 'spedefdicmsipic190';
    TabPai  := 'spedefdicmsipic100';
    Campo   := 'C100';
    FldData := 'DT_E_S';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(VL_IPI) VL_IPI ',
    'FROM ' + Tabela,
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND Empresa=' + Geral.FF0(QrEFD_E100Empresa.Value),
    'AND AnoMes=' + Geral.FF0(QrEFD_E100AnoMes.Value),
    'AND ' + Campo + ' IN (',
    '  SELECT LinArq',
    '  FROM ' + TabPai,
    '  WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    '  AND Empresa=' + Geral.FF0(QrEFD_E100Empresa.Value),
    '  AND AnoMes=' + Geral.FF0(QrEFD_E100AnoMes.Value),
    '  AND ' + FldData + ' BETWEEN "' + DtApurIni + '" AND "' + DtApurFim + '"',
    '  AND COD_SIT IN (00, 06, 08) ',
    ')',
    'AND (',
    '  CFOP DIV 1000 IN (5,6) ',
    ')',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Result := Result + Qry.FieldByName('VL_IPI').AsFloat;
  finally
    Qry.Free;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CalculaCampoEFD_E520_04(): Double;
var
  Qry: TmySQLQuery;
  Tabela, TabPai, Campo, FldData, DtApurIni, DtApurFim: String;
begin
  Result    := 0;
  DtApurIni := Geral.FDT(QrEFD_E100DT_INI.Value, 1);
  DtApurFim := Geral.FDT(QrEFD_E100DT_FIN.Value, 1);
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    Tabela  := 'spedefdicmsipic190';
    TabPai  := 'spedefdicmsipic100';
    Campo   := 'C100';
    FldData := 'DT_E_S';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(VL_IPI) VL_IPI ',
    'FROM ' + Tabela,
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND Empresa=' + Geral.FF0(QrEFD_E100Empresa.Value),
    'AND AnoMes=' + Geral.FF0(QrEFD_E100AnoMes.Value),
    'AND ' + Campo + ' IN (',
    '  SELECT LinArq',
    '  FROM ' + TabPai,
    '  WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    '  AND Empresa=' + Geral.FF0(QrEFD_E100Empresa.Value),
    '  AND AnoMes=' + Geral.FF0(QrEFD_E100AnoMes.Value),
    '  AND ' + FldData + ' BETWEEN "' + DtApurIni + '" AND "' + DtApurFim + '"',
    '  AND COD_SIT IN (00, 06, 08) ',
    ')',
    'AND (',
    '  CFOP DIV 1000 IN (1,2,3) ',
    ')',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Result := Result + Qry.FieldByName('VL_IPI').AsFloat;
  finally
    Qry.Free;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CampoNaoConfere(): Boolean;
begin
  Result := False;
  if QrCamposNumero.Value <> QrCampos.RecNo then
  begin
{
    Result := True;
    Geral.MB_ERRO('Erro de sequencia na gera��o de linha:' + sLineBreak +
    'Bloco: "' + QrCamposBloco.Value + '"' + sLineBreak +
    'Registro: "' + QrCamposRegistro.Value + '"' + sLineBreak +
    'Campo: "' + QrCamposCampo.Value + '"' + sLineBreak + sLineBreak +
    'Sequencia esperada: ' + Geral.FF0(QrCampos.RecNo) + sLineBreak +
    'Sequencia informada: ' + Geral.FF0(QrCamposNumero.Value),
    'ERRO', MB_OK+MB_ICONERROR);
    Exit;
}
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
    'Erro'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'LinArq', 'REG', 'Campo'], [
    Integer(efdexerErrSeqGerLin)], [
    FImporExpor, FAnoMes_Int, FEmpresa_Int,
    FLinArq, FRegistro, QrCamposCampo.Value], True);
    //
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CampoNaoEhNumero(Valor: String): Boolean;
begin
  Result := Length(Valor) <> Length(Geral.SoNumero_TT(Valor));
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CampoObrigatorio_6(Inteiro64: Int64;
PermiteZero: Boolean): Boolean;
begin
  Result := (Uppercase(QrCamposCObrig.Value) = 'O') and (Inteiro64 = 0) and (PermiteZero = False);
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CampoObrigatorio_d(Data: TDatetime;
  PermiteZero: Boolean): Boolean;
begin
  Result := (Uppercase(QrCamposCObrig.Value) = 'O') and (Data < 2) and (PermiteZero = False);
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CampoObrigatorio_f(Flutuante: Double;
  PermiteZero: Boolean): Boolean;
begin
  Result := (Uppercase(QrCamposCObrig.Value) = 'O') and (Flutuante = 0) and (PermiteZero = False);
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CampoObrigatorio_i(Inteiro: Integer; PermiteZero: Boolean): Boolean;
begin
  Result := (Uppercase(QrCamposCObrig.Value) = 'O') and (Inteiro = 0) and (PermiteZero = False);
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CampoObrigatorio_x(Valor: String): Boolean;
begin
  Result := (Uppercase(QrCamposCObrig.Value) = 'O') and (Length(Valor) = 0);
end;

{
function TFmEfdIcmsIpiExporta.CFOP_Nao_Cadastrado(Empresa: Integer; DataIni,
DataFim: TDateTime): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Count(*) Itens ',
  'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
  '']);
  //
  if Dmod.QrAux.FieldByName('Itens').AsInteger = 0 then
  begin
    Geral.MB_Avsio('N�o h� cadastro de nenhum CFOP na tabela "' + CO_NOME_TbSPEDEFD_CFOP + '"!' +
    sLineBreak + '� necess�rio atualizar as tabelas de consulta!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrCFOP, Dmod.MyDB, [
  'SELECT nfi.FatID, nfi.FatNum, nfi.Empresa,  ',
  'nfi.nItem, nfi.prod_CFOP, t002.* ',
  'FROM nfeItsi nfi ',
  'LEFT JOIN nfecaba nfa ON nfa.FatID=nfi.FatID ',
  '     AND nfa.FatNum=nfi.FatNum ',
  '     AND nfa.Empresa=nfi.Empresa ',
  'LEFT JOIN ' + CO_NOME_TbSPEDEFD_CFOP + ' t002 ON t002.Codigo=nfi.prod_CFOP ',
  'WHERE nfa.Empresa=' + Geral.FF0(Empresa),
  'AND nfa.DataFiscal BETWEEN "' + Geral.FDT(DataIni, 1) + '" AND "' +
    Geral.FDT(DataFim, 1) + '" ',
  'AND t002.Codigo IS NULL ',
  '']);
  if QrErrCFOP.RecordCount > 0 then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
    'Erro'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'Campo'], [FNumErro[13]], [
    FImporExpor, FAnoMes_Int, FEmpresa_Int, FLinArq, FRegistro, QrCamposCampo.Value], True);
  end;
  //
end;
}

{
procedure TFmEfdIcmsIpiExporta.ConfiguraTreeView1();
  function CriaNoh(const Noh: TTreeNode; const nN: String; const cN: Integer): TTreeNode;
  begin
    if Noh = nil then
      Result := TvBlocos.Items.Add(Noh, nN)
    else
      Result := TvBlocos.Items.AddChild(Noh, nN);
    Result.ImageIndex := cN;
    Result.StateIndex := QrBlocosAtivo.Value + Integer(cCBNot);
  end;
begin
  FSPEDEFD_Blcs := GradeCriar.RecriaTempTableNovo(ntrttSPEDEFD_BLCS, DModG.QrUpdPID1, False);
  UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
  'INSERT INTO ' + FSPEDEFD_Blcs,
  'SELECT DISTINCT Bloco, Ordem, 1 Ativo ',
  'FROM ' + TMeuDB + '.spedefdicmsipiblcs ',
  'WHERE Implementd=1',
    '']);
  TVBlocos.StateImages := FmMyGlyfs.ImgChkTV;
  TVBlocos.Items.Clear;
  QrBlocos.Close;
  QrBlocos.Database := DmodG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(QrBlocos, DmodG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FSPEDEFD_Blcs,
  'ORDER BY Ordem, Bloco',
  '']);
  while not QrBlocos.Eof do
  begin
    CriaNoh(nil, QrBlocosBloco.Value, 0);
    QrBlocos.Next;
  end;
  //
end;
}

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.Comcadastros1Click(Sender: TObject);
begin
  ConfiguraTreeView2(
  [(*'0005', '0100',*) '0150', '0190', '0200',
   'K200','K280'],
  ['0', '0001', '0990', 'K', 'K001', 'K100', 'K990']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.ConfiguraTreeView2(Ativos, Cinzas: array of String);
  function CriaNoh(const Noh: TTreeNode; const nN: String; const cN: Integer;
  Ativar: Integer): TTreeNode;
  begin
    if Noh = nil then
      Result := TvBlocos.Items.Add(Noh, nN)
    else
      Result := TvBlocos.Items.AddChild(Noh, nN);
    Result.ImageIndex := cN;
    //Result.StateIndex := QrBlcsAtivo.Value + Integer(cCBNot);
    Result.StateIndex := Ativar;
  end;
const
  MaxNiv = 255;
var
  Idxs: array[0..MaxNiv] of Integer;
  Nohs: array[0..MaxNiv] of TTreeNode;
  I, J, K: Integer;
  sRegistros: String;
begin
  //try
  for I := 0 to MaxNiv do
  begin
    Idxs[I] := 0;
    Nohs[I] := nil;
  end;
  //
  QrBlcs.Close;
  QrBlcs.Database := DModG.MyPID_DB;
  //
  FSPEDEFD_Blcs := GradeCriar.RecriaTempTableNovo(ntrttSPEDEFD_BLCS, DModG.QrUpdPID1, False);
  // Blocos
  UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
    'INSERT INTO ' + FSPEDEFD_Blcs,
    'SELECT DISTINCT Bloco, Bloco Registro, 0 Nivel,',
    'Bloco RegisPai,  0 OcorAciNiv, 1 OcorEstNiv,',
    'Implementd, Ordem, 0 Ativo, 2 StateIndex ',
    'FROM ' + VAR_AllID_DB_NOME + '.spedefdicmsipiblcs ',
    'WHERE Implementd <> 0 ',
    '']);
  // Registros
  UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
    'INSERT INTO ' + FSPEDEFD_Blcs,
    'SELECT Bloco, Registro, Nivel,',
    'RegisPai,  OcorAciNiv, OcorEstNiv,',
    'Implementd, Ordem, 0 Ativo, 2 StateIndex ',
    'FROM ' + VAR_AllID_DB_NOME + '.spedefdicmsipiblcs ',
    'WHERE Nivel > 0 ',
    'AND Implementd <> 0 ',
    '']);
  //
  sRegistros := MyObjects.CordaDeSQL_IN(Ativos);
  if sRegistros <> '' then
  begin
    UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
    'UPDATE ' + FSPEDEFD_Blcs,
    'SET Ativo=1 ',
    'WHERE Registro IN (' + sRegistros + ') ',
    '']);
  end;
  TVBlocos.StateImages := FmMyGlyfs.ImgChkTV;
  TVBlocos.Items.Clear;
  // Desmarcar!
  TVBlocos.FullExpand();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrBlcs, DModG.MyPID_DB, [
  'SELECT * FROM ' + FSPEDEFD_Blcs,
  'WHERE Implementd <> 0',
  'ORDER BY Ordem, Registro',
  '']);
  while not QrBlcs.Eof do
  begin
    //Ativa := False;
    K := 2;
    I := MaxNiv - QrBlcsNivel.Value;
    // Cinzas
    for J := Low(Cinzas) to High(Cinzas) do
    begin
      if Cinzas[J] = QrBlcsRegistro.Value then
      begin
        //Ativa := True;
        K := 1;
        Break;
      end;
    end;
    // Ativos
    for J := Low(Ativos) to High(Ativos) do
    begin
      if Ativos[J] = QrBlcsRegistro.Value then
      begin
        //Ativa := True;
        K := 3;
        Break;
      end;
    end;
    Nohs[I] := CriaNoh(Nohs[I+1], QrBlcsRegistro.Value, 0, K);
    TVBlocos.FullExpand();
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'UPDATE ' + FSPEDEFD_Blcs,
    'SET StateIndex=' + Geral.FF0(K),
    'WHERE Bloco="' + QrBlcsBloco.Value + '"',
    'AND Registro="' + QrBlcsRegistro.Value + '"',
    '']);
    QrBlcs.Next;
  end;
  //
{
for i:= 0 to frmTreeView.tv.Selected.Count -1 do

node:=frmTreeView.tv.Selected[i].Item;

frmTreeView.tv.items[6].selectd:=true;
}
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CorrigeCOD_SIT(): Boolean;
const
  Texto = 'Corrigindo o c�digo da situa��o dos documentos fiscais';
var
  Finalidade: TFinalidadeEmissaoDocFiscal;
  COD_SIT, NUM_DOC: Integer;
  tpNF, IND_OPER, IND_EMIT, COD_MOD, SER: String;
  EhE: Boolean;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, Texto);
  Result := True;
(*
  QrCorrige002.Close;
  QrCorrige002.Params[00].AsInteger := FEmpresa_Int;
  QrCorrige002.Params[01].AsString  := FDataIni_Txt;
  QrCorrige002.Params[02].AsString  := FDataFim_Txt;
  QrCorrige002. O p e n ;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrCorrige002, Dmod.MyDB, [
  'SELECT CodInfoEmit, CodInfoDest, ',
  'ide_tpNF, DataFiscal, ',
  'FatID, FatNum, Empresa, IDCtrl, ide_mod, ',
  'ide_serie, ide_nNF, ide_dEmi, ide_dSaiEnt, ',
  'ide_finNFe, emit_CNPJ, emit_CPF, Status, ',
  'NFG_Serie, NFG_SubSerie, COD_MOD ',
  'FROM nfecaba nfea ',
  'WHERE ide_tpAmb<>2 ',
  'AND COD_SIT in (98,99) ',
  'AND Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND DataFiscal BETWEEN "' + FDataIni_Txt + '" AND "' + FDataFim_Txt + '" ',
  '']);
  //Geral.MB_SQL(self, QrCorrige002);
  PB1.Position := 0;
  PB1.Max := QrCorrige002.RecordCount;
  //
  QrCorrige002.First;
  while not QrCorrige002.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, Texto + '. Item ' + FormatFloat('0',
    QrCorrige002.RecNo) + ' de ' + Geral.FF0(QrCorrige002.RecordCount));
    //
    case QrCorrige002ide_finNFe.Value of
      01: Finalidade := fedfNormal;
      02: Finalidade := fedfComplementar;
      03: Finalidade := fedfAjuste;
    end;
    //
    EhE := QrCorrige002CodInfoEmit.Value = FEmpresa_Int;
    tpNF := Geral.FF0(QrCorrige002ide_tpNF.Value);
    IND_OPER := dmkPF.EscolhaDe2Str(EhE, tpNF, '0');
    IND_EMIT := dmkPF.EscolhaDe2Str(EhE, '0', '1');
    COD_MOD := FormatFloat('00', QrCorrige002ide_mod.Value);
    if (COD_MOD <> '01') and (COD_MOD <> '55') then
      COD_MOD := QrCorrige002COD_MOD.Value;

    if QrCorrige002NFG_Serie.Value <> '' then
      SER := QrCorrige002NFG_Serie.Value
    else SER := Geral.FF0(QrCorrige002ide_serie.Value );
    NUM_DOC := QrCorrige002ide_nNF.Value;
    if SPED_Geral.Obtem_SPED_COD_SIT_de_NFe_Status(IND_OPER, IND_EMIT,
    QrCorrige002Status.Value, QrCorrige002ide_dEmi.Value,
    QrCorrige002ide_dSaiEnt.Value, QrCorrige002DataFiscal.Value, COD_MOD, SER,
    NUM_DOC, QrCorrige002FatID.Value, QrCorrige002FatNum.Value,
    QrCorrige002CodInfoEmit.Value, QrCorrige002CodInfoDest.Value, Finalidade,
    FAnoMes_Int, COD_SIT, MeErros)
    then
    begin
      if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
      'COD_SIT'], ['IDCtrl'
      ], [COD_SIT], [QrCorrige002IDCtrl.Value], True) then
        Result := False;
    end else
      Result := False;
    //
    QrCorrige002.Next;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CorrigeDataFiscal(): Boolean;
var
  QtdeIni, QtdeFim: Integer;
  Qry: TmySQLQuery;
  Texto, Campo, Corda: String;
  //
  procedure ReopenErros();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ide_dSaiEnt,  ide_dEmi, DataFiscal ',
    'FROM nfecaba ',
    'WHERE ide_dSaiEnt > ide_dEmi ',
    'AND DataFiscal <> ' + Campo,
    'AND ide_dEmi BETWEEN "' + FDataIni_Txt + '" AND "' + FDataFim_Txt + '" ',
    '']);
  end;
begin
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo Data Fiscal!');
  //Texto := '';
  //
  // Notas da empresa
  Qry := TmySQLQuery.Create(Dmod);
  try
    case QrParamsEmpSPED_EFD_DtFiscal.Value of
      1: Campo := 'ide_dEmi';
      2: Campo := 'ide_dSaiEnt';
      else Campo := 'ide_?????';
    end;
    ReopenErros();
    QtdeIni := Qry.RecordCount;
    if QtdeIni > 0 then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
      'UPDATE nfecaba ',
      'SET DataFiscal=' + Campo,
      'WHERE ide_dSaiEnt > ide_dEmi ',
      'AND DataFiscal <> ' + Campo,
      'AND ide_dEmi BETWEEN "' + FDataIni_Txt + '" AND "' + FDataFim_Txt + '" ',
      '']);
      ReopenErros();
      QtdeFim := Qry.RecordCount;
      Result := QtdeFim = 0;
      if QtdeFim < QtdeIni then
      begin
        Texto := Geral.FF0(QtdeIni - QtdeFim) +
        ' registros tiveram sua data fiscal corrigida!' + sLinebreak;
        if QtdeFim > 0 then
        begin
          Texto := Texto + 'Mas ' + Geral.FF0(QtdeFim) +
          ' registros n�o forma corrigidos!';
        end;
        Geral.MB_ERRO(Texto);
      end;
    end else
      Result := True;
  finally
    Qry.Free;
  end;
  // Notas de terceiros
  ReabreAtrela0053(TSimNao.snNao); // Parei aqui! Mudar para sim depois?????
  QtdeIni := QrAtrela0053.RecordCount;
  if QtdeIni > 0 then
  begin
    QrAtrela0053.First;
    //
    while not QrAtrela0053.Eof do
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE nfecaba ',
      'SET DataFiscal="' + Geral.FDT(QrAtrela0053DataFiscalInn.Value, 1) + '"',
      'WHERE FatID=' + Geral.FF0(QrAtrela0053FatID.Value),
      'AND FatNum=' + Geral.FF0(QrAtrela0053FatNum.Value),
      'AND Empresa=' + Geral.FF0(QrAtrela0053Empresa.Value),
      '']);
      //
      QrAtrela0053.Next;
    end;
    //
    ReabreAtrela0053(TSimNao.snNao); // Parei aqui! Mudar para sim depois?????
    QtdeFim := QrAtrela0053.RecordCount;
    Result := QtdeFim = 0;
    if QtdeFim < QtdeIni then
    begin
      Texto := Geral.FF0(QtdeIni - QtdeFim) +
      ' registros de notas de terceiros tiveram sua data fiscal corrigida!' +
      sLinebreak;
      if QtdeFim > 0 then
      begin
        Texto := Texto + 'Mas ' + Geral.FF0(QtdeFim) +
        ' registros n�o forma corrigidos!';
      end;
      Geral.MB_ERRO(Texto);
    end;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CorrigeItensCraidosAForca(): Boolean;
var
  prod_uCom, prod_uTrib: String;
  GraGruX, UnidMedCom, UnidMedTrib, FatID, FatNum, Empresa, nItem: Integer;
begin
  //Result := False;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo itens criados a for�a');
    QrForca.Close;
    UnDmkDAC_PF.AbreQuery(QrForca, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    PB1.Position := 0;
    PB1.Max := QrForca.RecordCount;
    //
    while not QrForca.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Corrigindo item criados a for�a: ' +
      FormatFloat('', QrForca.RecNo));
      //
      QrGGX.Close;
      QrGGX.Params[0].AsInteger := QrForcaGGX_SMIA.Value;
      UnDmkDAC_PF.AbreQuery(QrGGX, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
      if QrGGX.RecordCount > 0 then
      begin
        prod_uCom   := QrGGXSigla.Value;
        prod_uTrib  := QrGGXSigla.Value;
        GraGruX     := QrForcaGGX_SMIA.Value;
        UnidMedCom  := QrGGXUnidMed.Value;
        UnidMedTrib := QrGGXUnidMed.Value;
        FatID       := QrForcaFatID.Value;
        FatNum      := QrForcaFatNum.Value;
        Empresa     := QrForcaEmpresa.Value;
        nItem       := QrForcanItem.Value;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeitsi', False, [
        (*'prod_cProd', 'prod_cEAN', 'prod_xProd',
        'prod_NCM', 'prod_EXTIPI', 'prod_genero',
        'prod_CFOP',*) 'prod_uCom', (*'prod_qCom',
        'prod_vUnCom', 'prod_vProd', 'prod_cEANTrib',*)
        'prod_uTrib', (*'prod_qTrib', 'prod_vUnTrib',
        'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
        'Tem_IPI', '_Ativo_', 'InfAdCuztm',
        'EhServico', 'ICMSRec_pRedBC', 'ICMSRec_vBC',
        'ICMSRec_pAliq', 'ICMSRec_vICMS', 'IPIRec_pRedBC',
        'IPIRec_vBC', 'IPIRec_pAliq', 'IPIRec_vIPI',
        'PISRec_pRedBC', 'PISRec_vBC', 'PISRec_pAliq',
        'PISRec_vPIS', 'COFINSRec_pRedBC', 'COFINSRec_vBC',
        'COFINSRec_pAliq', 'COFINSRec_vCOFINS', 'MeuID',
        'Nivel1', 'prod_vOutro', 'prod_indTot',
        'prod_xPed', 'prod_nItemPed',*) 'GraGruX',
        'UnidMedCom', 'UnidMedTrib'], [
        'FatID', 'FatNum', 'Empresa', 'nItem'], [
        (*prod_cProd, prod_cEAN, prod_xProd,
        prod_NCM, prod_EXTIPI, prod_genero,
        prod_CFOP,*) prod_uCom, (*prod_qCom,
        prod_vUnCom, prod_vProd, prod_cEANTrib,*)
        prod_uTrib, (*prod_qTrib, prod_vUnTrib,
        prod_vFrete, prod_vSeg, prod_vDesc,
        Tem_IPI, _Ativo_, InfAdCuztm,
        EhServico, ICMSRec_pRedBC, ICMSRec_vBC,
        ICMSRec_pAliq, ICMSRec_vICMS, IPIRec_pRedBC,
        IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI,
        PISRec_pRedBC, PISRec_vBC, PISRec_pAliq,
        PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
        COFINSRec_pAliq, COFINSRec_vCOFINS, MeuID,
        Nivel1, prod_vOutro, prod_indTot,
        prod_xPed, prod_nItemPed,*) GraGruX,
        UnidMedCom, UnidMedTrib], [
        FatID, FatNum, Empresa, nItem], True);
      end;
      QrForca.Next;
    end;
    Result := True;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Item criados a for�a verificados.');
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CorrigeNotasFiscaiDeTerceiros(): Boolean;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo Reduzido de notas fiscais de terceiros!');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE nfecaba SET Status=infCanc_cStat');
  Dmod.QrUpd.SQL.Add('WHERE Status=0');
  Dmod.QrUpd.SQL.Add('AND infCanc_cStat<>0');
  Dmod.QrUpd.SQL.Add('AND Empresa<>CodInfoEmit');
  Dmod.QrUpd.SQL.Add('AND FatID<>1;');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('UPDATE nfecaba SET Status=infProt_cStat');
  Dmod.QrUpd.SQL.Add('WHERE Status=0');
  Dmod.QrUpd.SQL.Add('AND infCanc_cStat=0');
  Dmod.QrUpd.SQL.Add('AND infProt_cStat<>0');
  Dmod.QrUpd.SQL.Add('AND Empresa<>CodInfoEmit');
  Dmod.QrUpd.SQL.Add('AND FatID<>1;');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('UPDATE nfecaba SET Status=100');
  Dmod.QrUpd.SQL.Add('WHERE Status=0');
  Dmod.QrUpd.SQL.Add('AND infProt_cStat=0');
  Dmod.QrUpd.SQL.Add('AND infCanc_cStat=0');
  Dmod.QrUpd.SQL.Add('AND Empresa<>CodInfoEmit');
  Dmod.QrUpd.SQL.Add('AND FatID<>1;');
  Dmod.QrUpd.ExecSQL;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CorrigeNotasFiscaiProprias(): Boolean;
var
  //UnidMedCom, UnidMedTrib,
  GraGruX, FatID, FatNum, Empresa, nItem: Integer;
begin
  //Result := False;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo Reduzido de notas fiscais pr�prias!');
(*
    QrCorrige001.Close;
    UnDmkDAC_PF.AbreQuery(QrCorrige001, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorrige001, Dmod.MyDB, [
    'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa,  ',
    'nfei.nItem, nfei.prod_cProd  ',
    'FROM nfeitsi nfei ',
    'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID ',
    '     AND nfea.FatNum=nfei.FatNum ',
    '     AND nfea.Empresa=nfei.Empresa ',
    'WHERE nfea.Empresa=nfea.CodInfoEmit ',
    'AND GraGruX=0 ',
    'AND prod_cProd <> "" ',
    '']);
    PB1.Position := 0;
    PB1.Max := QrCorrige001.RecordCount;
    //
    while not QrCorrige001.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      'Corrigindo item pr�prio: ' + FormatFloat('', QrCorrige001.RecNo));
      //
      GraGruX     := StrToInt(QrCorrige001prod_cProd.Value);
      FatID       := QrCorrige001FatID.Value;
      FatNum      := QrCorrige001FatNum.Value;
      Empresa     := QrCorrige001Empresa.Value;
      nItem       := QrCorrige001nItem.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeitsi', False, [
      'GraGruX'], [
      'FatID', 'FatNum', 'Empresa', 'nItem'], [
      GraGruX], [
      FatID, FatNum, Empresa, nItem], True);
      //
      QrCorrige001.Next;
    end;
    Result := True;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Item pr�prios verificados.');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.CorrigeUnidMedNfeItsI();
var
  FatID, FatNum, Empresa, nItem: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCorrI, Dmod.MyDB, [
  'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa, nfei.nItem, nfei.GraGruX ',
  'FROM nfeitsi nfei ',
  'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID ',
  '     AND nfea.FatNum=nfei.FatNum ',
  '     AND nfea.Empresa=nfei.EMpresa ',
  'WHERE prod_uTrib="" ',
  'AND  nfea.Empresa=' + FEmpresa_Txt,
  'AND nfea.DataFiscal BETWEEN ' + FIntervaloFiscal,
  '']);
  if QrCorrI.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('Existem ' + Geral.FF0(QrCorrI.RecordCount) +
    ' itens de notas sem a unidade tribut�vel.' + sLineBreak +
    'Deseja atribuir a unidade do cadastro do produto para estes itens?') = ID_YES then
    begin
      QrCorrI.First;
      PB1.Max := QrCorrI.RecordCount;
      //
      while not QrCorrI.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Corrigindo unidade tribut�vel: ' + FormatFloat('', QrCorrI.RecNo));
        //
        (*
        QrProd.Close;
        QrProd.Params[0].AsInteger := QrCorrIGraGruX.Value;
        UnDmkDAC_PF.AbreQuery(QrProd, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
        *)
        DmProd.ReopenProd(QrCorrIGraGruX.Value);
        //
        FatID := QrCorrIFatID.Value;
        FatNum := QrCorrIFatNum.Value;
        Empresa := QrCorrIEmpresa.Value;
        nItem := QrCorrInItem.Value;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeitsi', False, [
        'prod_uTrib'], [
        'FatID', 'FatNum', 'Empresa', 'nItem'], [
        DmProd.QrProdSigla.Value], [
        FatID, FatNum, Empresa, nItem], True);
        //
        QrCorrI.Next;
      end;
      QrCorrI.Close;
      UnDmkDAC_PF.AbreQuery(QrCorrI, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
      if QrCorrI.RecordCount > 0 then
      begin
        Geral.MB_Aviso(Geral.FF0(QrCorrI.RecordCount) +
        ' itens de notas sem a unidade tribut�vel n�o foram corrigidos.' + sLineBreak +
        'Verifica a unidade no cadastro do produto!');
      end;
    end;
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.DBGrid1DblClick(Sender: TObject);
begin
  if (QrErrMod.State <> dsInactive) and (QrErrMod.RecordCount > 0) then
    Grade_Jan.MostraFormEntrada(QrErrModFatID.Value, QrErrModIDCtrl.Value);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.DefineEmpresaEAnoMes();
  function ValidaPeriodoInteiroDeUmMes(const DtI, DtF:
  TDateTime): Boolean;
  var
    //AnoIa, MesIa, DiaIa,
    AnoIi, MesIi, DiaIi,
    AnoFf, AnoFd, MesFf, MesFd, DiaFf, DiaFd: Word;
  begin
    Result := False;
    FAnoMes_Int := 0;
    DecodeDate(DtI, AnoIi, MesIi, DiaIi);
    //DecodeDate(DtI-1, AnoIi, MesIi, DiaIi);
    DecodeDate(DtF, AnoFf, MesFf, DiaFf);
    DecodeDate(DtF + 1, AnoFd, MesFd, DiaFd);
    FAnoMes_Int := AnoIi * 100 + MesIi;
    FAnoMes_Txt := Geral.FF0(FAnoMes_Int);

    FDataIni_Txt := Geral.FDT(DtI, 1);
    FDataFim_Txt := Geral.FDT(DtF, 1);
    FIntervaloFiscal := ' "' + FDataIni_Txt + '" AND "' + FDataFim_Txt + '"';
    //
    if DiaIi <> 1 then
      Geral.MB_Aviso(
      'Exporta��o cancelada! Dia inicial deve ser o primeiro dia (dia 1) do m�s,'
      + sLineBreak +
      'mesmo que as atividades tenham se iniciado ap�s o dia primeiro!')
    else if DiaFd <> 1 then
      Geral.MB_Aviso(
      'Exporta��o cancelada! Dia final deve ser o �ltimo dia do m�s,'
      + sLineBreak +
      'mesmo que as atividades tenham sido finalizados antes do �ltimo dia do m�s!')
    else if MesIi <> MesFf then
      Geral.MB_Aviso(
      'Exporta��o cancelada! O per�odo deve ser de um �nico m�s!' + sLineBreak +
      'Data inicial selecionada:  ' + Geral.FDT(DtI, 3) + sLineBreak +
      'Data Final selecionada:   ' + Geral.FDT(DtF, 3))
    else if FAnoMes_Int < 200001 then
      Geral.MB_Aviso(
      'Exporta��o cancelada! O per�odo deve ser de um ano ap�s 2000!' + sLineBreak +
      'Data inicial selecionada:  ' + Geral.FDT(DtI, 3) + sLineBreak +
      'Data Final selecionada:   ' + Geral.FDT(DtF, 3))
    else
      Result := True;
  end;
begin
  FEmpresa_Int := 0;
  FEmpresa_Txt := '0';
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Empresa n�o definida!') then
    Exit;
  FEmpresa_Int := DModG.QrEmpresasCodigo.Value;
  FEmpresa_Txt := Geral.FF0(FEmpresa_Int);
(*
  QrParamsEmp.Close;
  QrParamsEmp.Params[0].AsInteger := FEmpresa_Int;
  UnDmkDAC_PF.AbreQuery(QrParamsEmp, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrParamsEmp, Dmod.MyDB, [
  'SELECT pem.SPED_EFD_IND_PERFIL, pem.SPED_EFD_IND_ATIV, ',
  'pem.SPED_EFD_CadContador, pem.SPED_EFD_CRCContador, ',
  'pem.SPED_EFD_EscriContab, pem.SPED_EFD_EnderContab, ',
  'SPED_EFD_Path, SPED_EFD_DtFiscal, ',
  'SPED_EFD_ID_0150, SPED_EFD_ID_0200, ',
  'IF(ctd.Tipo=0, ctd.RazaoSocial, ctd.Nome) NO_CTD, ',
  'ctd.CPF CPF_CTD, ctb.CNPJ CNPJ_CTB, ',
  'IF(ctb.Tipo=0, ctb.RazaoSocial, ctb.Nome) NO_CTB ',
  'FROM paramsemp pem ',
  'LEFT JOIN entidades ctd ON ctd.Codigo=pem.SPED_EFD_CadContador ',
  'LEFT JOIN entidades ctb ON ctb.Codigo=pem.SPED_EFD_EscriContab ',
  'WHERE pem.Codigo=' + FEmpresa_Txt,
  '']);
  //
  if not ValidaPeriodoInteiroDeUmMes(FDataIni_Dta, FDataFim_Dta) then
    Exit;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.DefineIND_MOV(FisRegCad, ide_finNFe:
  Integer; prod_cProd: String): Integer;
begin
  Result := 0; // Movimenta
  if ide_finNFe in ([
    2, // NF-e complementar
    3// NF-e de Ajuste
  ]) then
    Result := 1 // n�o movimenta
  else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrFisRegMvt, Dmod.MyDB, [
    'SELECT frm.TipoCalc ',
    'FROM fisregmvt frm ',
    'WHERE frm.Codigo=' + Geral.FF0(FisRegCad),
    '']);
    //
    if QrFisRegMvt.RecordCount > 0 then
    begin
      if QrFisRegMvtTipoCalc.Value = 0 then // Movimento Nulo
        Result := 1; // sem movimento
    end;
    //
    if Result = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrIndMov, Dmod.MyDB, [
      'SELECT  ',
      '(IF((gg1.Nivel2 <> 0) and (gg2.Tipo_Item in (0,1,2,3,4,5,6,7,8,10)), 0,  ',
      '  IF(pgt.TipPrd=3, 1,  ',
      '    IF(pgt.Tipo_Item in (0,1,2,3,4,5,6,7,8,10), 0,  ',
      '      1) ',
      '  ) ',
      ') + 0.000) IND_MOV ',
      'FROM gragrux ggx ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN gragru2 gg2 ON gg1.Nivel2=gg2.Nivel2 ',
      'LEFT JOIN prdgruptip pgt ON gg1.PrdGrupTip=pgt.Codigo ',
      'WHERE ggx.Controle=' + prod_cProd,
      '']);
      Result := Trunc(QrIndMovIND_MOV.Value);
    end;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.Define_REG(): Boolean;
var
  Modelo: Variant;
begin
  ReopenCabA(False, Null);
  QrCabA_C.First;
  PB1.Position := 0;
  PB1.Max := QrCabA_C.RecordCount;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Definindo blocos dos modelos');
  while not QrCabA_C.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    Modelo :=  SPED_Geral.DefineREG_de_Modelo_de_NF(QrCabA_Cide_Mod.Value,
      QrCabA_CCOD_MOD.Value);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
    'EFD_EXP_REG'], [
    'FatID', 'FatNum', 'Empresa'], [
    Modelo], [
    QrCabA_CFatID.Value, QrCabA_CFatNum.Value, QrCabA_CEmpresa.Value], True);
    //
    QrCabA_C.Next;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrMod, Dmod.MyDB, [
  'SELECT nfa.DataFiscal, nfa.FatID, nfa.FatNum, nfa.Empresa,',
  'nfa.ide_mod, nfa.ide_serie, nfa.ide_nNF, nfa.ide_dEmi, nfa.ICMSTot_vNF,',
  'IF(nfa.CodInfoEmit=' + FEmpresa_Txt + ', nfa.CodInfoDest, nfa.CodInfoEmit) Terceiro,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, nfa.IDCtrl',
  'FROM nfecaba nfa',
  'LEFT JOIN entidades ent ON ent.Codigo=',
  '     IF(CodInfoEmit=' + FEmpresa_Txt + ', CodInfoDest, CodInfoEmit)',
  'WHERE ide_tpAmb<>2',
  'AND Status IN (100,101,102,110,301)',
  'AND DataFiscal BETWEEN ' + FIntervaloFiscal,
  'AND EFD_EXP_REG=""',
  'ORDER BY DataFiscal, ide_dEmi, NO_TERCEIRO',
  '']);
  Result := QrErrMod.RecordCount = 0;
  if not Result then
  begin
    PageControl1.ActivePageIndex := 3;
    Geral.MB_Aviso('Exporta��o abortada!' + sLineBreak + sLineBreak +
    'Existem ' + Geral.FF0(QrErrMod.RecordCount) +
    ' notas fiscais sem o modelo fiscal definido!' + sLineBreak + sLineBreak +
    'O modelo fiscal � fundamental para criar os blocos de exporta��o!');
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.CorrigeItensDeMateriaPrima(): Boolean;
var
  //UnidMedCom, UnidMedTrib,
  GraGruX, FatID, FatNum, Empresa, nItem: Integer;
begin
  //Result := False;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo Reduzido de notas fiscais de mat�ria-prima!');
(*
    QrCorrige1x3.Close;
    UnDmkDAC_PF.AbreQuery(QrCorrige1x3, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorrige1x3, Dmod.MyDB, [
    'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa,  ',
    'nfei.nItem, nfei.Nivel1 ',
    'FROM nfeitsi nfei ',
    'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID ',
    '     AND nfea.FatNum=nfei.FatNum ',
    '     AND nfea.Empresa=nfei.Empresa ',
    'WHERE nfei.FatID IN (103,113) ',
    'AND GraGruX=0 ',
    'AND Nivel1 < 0 ',
    '']);
    PB1.Position := 0;
    PB1.Max := QrCorrige1x3.RecordCount;
    //
    while not QrCorrige1x3.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      'Corrigindo item de mat�ria-prima: ' + FormatFloat('', QrCorrige1x3.RecNo));
      //
      GraGruX     := QrCorrige1x3Nivel1.Value;
      FatID       := QrCorrige1x3FatID.Value;
      FatNum      := QrCorrige1x3FatNum.Value;
      Empresa     := QrCorrige1x3Empresa.Value;
      nItem       := QrCorrige1x3nItem.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeitsi', False, [
      'GraGruX'], [
      'FatID', 'FatNum', 'Empresa', 'nItem'], [
      GraGruX], [
      FatID, FatNum, Empresa, nItem], True);
      //
      QrCorrige1x3.Next;
    end;
    Result := True;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Item de mat�ria-prima verificados.');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.EdEmpresaChange(Sender: TObject);
begin
  HabilitaBotaoOK();
  DefineEmpresaEAnoMes();
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.EdMesChange(Sender: TObject);
begin
  RedefinePeriodos();
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.EdMesRedefinido(Sender: TObject);
begin
  RedefinePeriodos();
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.ExcluiPeriodoSelecionado(): Boolean;
var
  IE_TXT: String;
  I: Integer;
begin
  Result := False;
  IE_TXT := Geral.FF0(FImporExpor);
  //
(*
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT AnoMes');
  Dmod.QrAux.SQL.Add('FROM spedefdicmsipierrs');
  Dmod.QrAux.SQL.Add('WHERE ImporExpor=' + IE_TXT);
  Dmod.QrAux.SQL.Add('AND Empresa=' + FEmpresa_Txt);
  Dmod.QrAux.SQL.Add('AND AnoMes=' + FAnoMes_Txt);
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('UNION');
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('SELECT AnoMes');
  Dmod.QrAux.SQL.Add('FROM spedefd0000');
  Dmod.QrAux.SQL.Add('WHERE ImporExpor=' + IE_TXT);
  Dmod.QrAux.SQL.Add('AND Empresa=' + FEmpresa_Txt);
  Dmod.QrAux.SQL.Add('AND AnoMes=' + FAnoMes_Txt);
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('UNION');
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('SELECT AnoMes');
  Dmod.QrAux.SQL.Add('FROM spedefd0001');
  Dmod.QrAux.SQL.Add('WHERE ImporExpor=' + IE_TXT);
  Dmod.QrAux.SQL.Add('AND Empresa=' + FEmpresa_Txt);
  Dmod.QrAux.SQL.Add('AND AnoMes=' + FAnoMes_Txt);
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('UNION');
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.SQL.Add('SELECT AnoMes');
  Dmod.QrAux.SQL.Add('FROM spedefdc100');
  Dmod.QrAux.SQL.Add('WHERE ImporExpor=' + IE_TXT);
  Dmod.QrAux.SQL.Add('AND Empresa=' + FEmpresa_Txt);
  Dmod.QrAux.SQL.Add('AND AnoMes=' + FAnoMes_Txt);
  Dmod.QrAux.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //Geral.MB_SQL(Self, Dmod.QrAux);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT AnoMes ',
  'FROM spedefdicmsipierrs ',
  'WHERE ImporExpor=' + IE_TXT,
  'AND Empresa=' + FEmpresa_Txt,
  'AND AnoMes=' + FAnoMes_Txt,
  ' ',
  'UNION ',
  ' ',
  'SELECT AnoMes ',
  'FROM spedefdicmsipi0000 ',
  'WHERE ImporExpor=' + IE_TXT,
  'AND Empresa=' + FEmpresa_Txt,
  'AND AnoMes=' + FAnoMes_Txt,
  ' ',
  'UNION ',
  ' ',
  'SELECT AnoMes ',
  'FROM spedefdicmsipi0001 ',
  'WHERE ImporExpor=' + IE_TXT,
  'AND Empresa=' + FEmpresa_Txt,
  'AND AnoMes=' + FAnoMes_Txt,
  ' ',
  'UNION ',
  ' ',
  'SELECT AnoMes ',
  'FROM spedefdicmsipic100 ',
  'WHERE ImporExpor=' + IE_TXT,
  'AND Empresa=' + FEmpresa_Txt,
  'AND AnoMes=' + FAnoMes_Txt,
  '']);
  if Dmod.QrAux.RecordCount > 0 then
  begin
(*
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SHOW TABLES');
    Dmod.QrAux.SQL.Add('FROM ' + TMeuDB);
    Dmod.QrAux.SQL.Add('LIKE "spedefd%"');
    Dmod.QrAux.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    //Geral.MB_SQL(Self, Dmod.QrAux);
    //
*)
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SHOW TABLES ',
    'FROM ' + TMeuDB,
    'LIKE "spedefdicmsipi%" ',
    '']);
    if Dmod.QrAux.RecordCount - FNot_SPED_EFD_II <> FQtd_SPED_EFD_II then
    begin
      Geral.MB_Aviso(
      'Importa��o cancelada! Quantidade de tabelas desatualizadas!' + sLineBreak +
      'Tot - Nao - Sim' + sLineBreak +
      '===============' + sLineBreak +
      Geral.FF0(Dmod.QrAux.RecordCount) + ' - ' +
      Geral.FF0(FNot_SPED_EFD_II) + ' <> ' +
      Geral.FF0(FQtd_SPED_EFD_II) + ' !!! ' +
      'AVISE A DERMATEK!');
    end else begin
      if Geral.MB_Pergunta(
      'J� existem dados para este per�odo!' + sLineBreak +
      'Os dados j� existentes ser�o exclu�dos! Deseja continuar assim mesmo?') = ID_YES then
      begin
        Screen.Cursor := crHourGlass;
        try
          for I := 0 to High(FTbs_SPED_EFD_II) (*- 1*) do
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('DELETE FROM ' + FTbs_SPED_EFD_II[I]);
            Dmod.QrUpd.SQL.Add('WHERE ImporExpor=' + IE_TXT);
            Dmod.QrUpd.SQL.Add('AND Empresa=' + FEmpresa_Txt);
            Dmod.QrUpd.SQL.Add('AND AnoMes=' + FAnoMes_Txt);
            Dmod.QrUpd.ExecSQL;
          end;
          Result := True;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
    (*
    // Evitar exclus�o errada!
    if (GRADE_TABS_PARTIPO_0004 <> 0) and (FAnoMes_Int <> 0) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo dados duplic�veis no estoque');
      QrRecupera.Close;
      QrRecupera.Params[00].AsInteger := VAR_FATID_????;
      QrRecupera.Params[01].AsInteger := GRADE_TABS_PARTIPO_0004;
      QrRecupera.Params[02].AsInteger := FAnoMes_Int;
      QrRecupera. O p e n ;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0');
      Dmod.QrUpd.SQL.Add('AND Empresa=:P1');
      Dmod.QrUpd.SQL.Add('AND partipo=:P2');
      Dmod.QrUpd.SQL.Add('AND parcodi=:P3');
      Dmod.QrUpd.Params[00].AsInteger := VAR_FATID_????;
      Dmod.QrUpd.Params[01].AsInteger := Empresa;
      Dmod.QrUpd.Params[02].AsInteger := GRADE_TABS_PARTIPO_0004;
      Dmod.QrUpd.Params[03].AsInteger := FAnoMes_Int;
      Dmod.QrUpd.ExecSQL;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Recuperando c�digos "StqMovItsA.IDCtrl"');
      PB1.Position := 0;
      PB1.Max := QrRecupera.RecordCount;
      QrRecupera.First;
      while not QrRecupera.Eof do
      begin
        MyObjects.Informa2EUpdPB(LaAviso1, LaAviso2, True,
          'Recuperando c�digos "StqMovItsA.IDCtrl" ' + FormatFloat('0',
          QrRecuperaIDCtrl.Value));
        PB1.Position := PB1.Position + 1;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'livres', False, [
        'Codigo', 'Tabela'], [], [
        QrRecuperaIDCtrl.Value, 'stqmovitsa'], [
        ], False);
        //
        QrRecupera.Next;
      end;
    end;
    *)
  end else
    Result := True;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType       := stLok;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  FVersao := '';
  FEmpresa_Int := 0;
  FEmpresa_Txt := '0';
  FAnoMes_Int := 0;
  FAnoMes_Txt := '';
  FDataIni_Dta := 0;
  FDataFim_Dta := 0;
  FDataIni_Txt := '';
  FDataFim_Txt := '';
  FIntervaloFiscal := '';
  EdMes.ValueVariant := Date - 21;
  //
  ConfiguraTreeView2([], []);
  //
  PageControl1.ActivePageIndex := 0;
  QrSelEnt.Close;
  QrSelEnt.Database := DModG.MyPID_DB;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.frxSEII_EstqGetValue(
  const VarName: string; var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_SPED' then
    Value := EdMes.Text
  else
  if VarName ='VARF_DATA_IMP' then
    Value := Now()
  else
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraCOD_OBS_CAT66SP2018(
  Seq: Integer): String;
begin
  Result := 'QT' + Geral.FF0(Seq);
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0000(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_INI, DT_FIN: TDateTime;
  CodMunici: Integer;
  CNPJ_CPF: String;
  CNPJ, CPF: Int64;
begin
  Result := False;
  FBloco := '0';
  FRegistro := '0000';
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
(*
  DT_INI := Geral.IMV(Geral.FDT(TPDataIni.Date, 23));
  DT_FIN := Geral.IMV(Geral.FDT(TPDataFim.Date, 23));
*)
  DT_INI := Trunc(TPDataIni.Date);
  DT_FIN := Trunc(TPDataFim.Date);
  //
  CNPJ_CPF := Geral.SoNumero_TT(QrEmpresaCNPJ_CPF.Value);
  case Length(CNPJ_CPF) of
    11:
    begin
      CPF := Geral.I64(CNPJ_CPF);
      CNPJ := 0;
      CodMunici := QrEmpresaPCodMunici.Value;
    end;
    14:
    begin
      CNPJ := Geral.I64(CNPJ_CPF);
      CPF := 0;
      CodMunici := QrEmpresaECodMunici.Value;
    end;
    else
    begin
      MensagemDocumentoInvalido(QrEmpresaCNPJ_CPF.Value);
      Exit;
    end;
  end;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_n(ITO, 'COD_VER', FVersao) then
      (*03*) if not AC_i(ITO, 'COD_FIN', RGCOD_FIN.ItemIndex-1, True, True) then
      (*04*) if not AC_d(ITO, 'DT_INI', DT_INI, False, False, 23) then
      (*05*) if not AC_d(ITO, 'DT_FIN', DT_FIN, False, False, 23) then
      (*06*) if not AC_x(ITO, 'NOME', Copy(QrEmpresaNOME_ENT.Value, 1, 100)) then
      (*07*) if not AC_6(ITO, 'CNPJ', CNPJ, False, False) then
      (*08*) if not AC_6(ITO, 'CPF', CPF, False, False) then
      (*09*) if not AC_x(ITO, 'UF', QrEmpresaNOMEUF.Value) then
      (*10*) if not AC_x(ITO, 'IE', QrEmpresaIE.Value) then
      (*11*) if not AC_i(ITO, 'COD_MUN', CodMunici, False, False) then
      (*12*) if not AC_x(ITO, 'IM', QrEmpresaNIRE.Value) then
      (*13*) if not AC_x(ITO, 'SUFRAMA', QrEmpresaSUFRAMA.Value) then
      (*14*) if not AC_x(ITO, 'IND_PERFIL', QrParamsEmpSPED_EFD_IND_PERFIL.Value) then
      (*15*) if not AC_i(ITO, 'IND_ATIV', QrParamsEmpSPED_EFD_IND_ATIV.Value, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    if CodMunici = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCodMunIndef)(*FNumErro[17]*)], [
      FImporExpor, FAnoMes_Int,
      FEmpresa_Int, FLinArq, FRegistro, 'COD_MUN'], True);
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_0);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := '0';
  FRegistro := '0001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_0);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0002(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  CLAS_ESTAB_IND: Integer;
begin
  DModG.ReopenPrmsEmpSPED(FEmpresa_Int);
  CLAS_ESTAB_IND := DModG.QrPrmsEmpEFD_IICLAS_ESTAB_IND.Value;
  //
  FBloco := '0';
  FRegistro := '0002';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'CLAS_ESTAB_IND', CLAS_ESTAB_IND, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_0);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0005(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  CEP, Numero_i: Integer;
  LOGR_RUA, NUMERO_x, Fone, Fax: String;
begin
  FBloco := '0';
  FRegistro := '0005';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  if QrEmpresaTipo.Value = 0 then
  begin
    CEP := QrEmpresaECEP.Value;
    Numero_i := QrEmpresaENumero.Value;
  end else begin
    CEP := QrEmpresaPCEP.Value;
    Numero_i := QrEmpresaPNumero.Value;
  end;
  LOGR_RUA := QrEmpresaNOMELOGRAD.Value;
  if Trim(LOGR_RUA) <> '' then
    LOGR_RUA := LOGR_RUA + ' ';
  LOGR_RUA := LOGR_RUA + QrEmpresaRua.Value;
  if Numero_i = 0 then
    NUMERO_x := 'S/N'
  else
    NUMERO_x := Geral.FF0(Numero_i);
  Fone := Geral.SoNumero_TT(QrEmpresaTE1.Value);
  while Length(Fone) > 10 do
    Fone := Copy(Fone, 2);
  Fax := Geral.SoNumero_TT(QrEmpresaFax.Value);
  while Length(Fax) > 10 do
    Fax := Copy(Fax, 2);
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'FANTASIA', QrEmpresaNO_2_ENT.Value) then
      (*03*) if not AC_i(ITO, 'CEP', CEP, False, False) then
      (*04*) if not AC_x(ITO, 'END', LOGR_RUA) then
      (*05*) if not AC_x(ITO, 'NUM', NUMERO_x) then
      (*06*) if not AC_x(ITO, 'COMPL', QrEmpresaCOMPL.Value) then
      (*07*) if not AC_x(ITO, 'BAIRRO', QrEmpresaBAIRRO.Value) then
      (*08*) if not AC_x(ITO, 'FONE', Fone) then
      (*09*) if not AC_x(ITO, 'FAX', Fax) then
      (*10*) if not AC_x(ITO, 'EMAIL', QrEmpresaEMAIL.Value) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_0);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0100(): Boolean;
const
  ITO = itoNaoAplicavel;
var
 CPF, CNPJ: Int64;
 CEP, COD_MUN, Numero_i: Integer;
 LOGR_RUA, NUM, COMPL, BAIRRO, FONE, FAX, EMAIL, Numero_x: String;
begin
  FBloco := '0';
  FRegistro := '0100';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  CPF := Geral.I64(Geral.SoNumero_TT(QrParamsEmpCPF_CTD.Value));
  CNPJ := Geral.I64(Geral.SoNumero_TT(QrParamsEmpCNPJ_CTB.Value));
  if QrParamsEmpSPED_EFD_EnderContab.Value = 0 then
  begin
    // Ningu�m
    Geral.MB_Aviso(
    'Aten��o! ' + sLineBreak +
    'N�o est� definido se o endere�o a ser informado no registro 0100 ser� do contador ou do escrit�rio de contabilidade!'
    + sLinebreak + 'Nenhum ser� considerado!');
    //
    CEP := 0;
    LOGR_RUA := '';
    NUM := '';
    COMPL := '';
    BAIRRO := '';
    FONE := '';
    FAX := '';
    EMAIL := '';
    COD_MUN := 0;
  end else
  begin
    // 1: do contador.
    // 2: do escrit�rio
    if QrParamsEmpSPED_EFD_EnderContab.Value = 1 then
    begin
(*
      QrEnder.Close;
      QrEnder.Params[0].AsInteger := QrParamsEmpSPED_EFD_CadContador.Value;
      UnDmkDAC_PF.AbreQuery(QrEnder, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
      ReopenEnder(QrParamsEmpSPED_EFD_CadContador.Value);
      //
      Numero_i := QrEnderPNumero.Value;
      CEP := QrEnderPCEP.Value;
      COD_MUN := QrEnderPCodMunici.Value;
    end else
    begin
(*
      QrEnder.Close;
      QrEnder.Params[0].AsInteger := QrParamsEmpSPED_EFD_CadContador.Value;
      UnDmkDAC_PF.AbreQuery(QrEnder, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
      ReopenEnder(QrParamsEmpSPED_EFD_EscriContab.Value);
      //
      Numero_i := QrEnderENumero.Value;
      CEP := QrEnderECEP.Value;
      COD_MUN := QrEnderPCodMunici.Value;
    end;
    LOGR_RUA := QrEnderNOMELOGRAD.Value;
    if Trim(LOGR_RUA) <> '' then
      LOGR_RUA := LOGR_RUA + ' ';
    LOGR_RUA := LOGR_RUA + QrEnderRua.Value;
    if Numero_i = 0 then
      NUMERO_x := 'S/N'
    else
      NUMERO_x := Geral.FF0(Numero_i);
    Fone := Geral.SoNumero_TT(QrEnderTE1.Value);
    COMPL := QrEnderCOMPL.Value;
    BAIRRO := QrEnderBAIRRO.Value;
    Fone := Geral.SoNumero_TT(QrEmpresaTE1.Value);
    while Length(Fone) > 10 do
      Fone := Copy(Fone, 2);
    Fax := Geral.SoNumero_TT(QrEmpresaFax.Value);
    while Length(Fax) > 10 do
      Fax := Copy(Fax, 2);
    //
    EMAIL := QrEnderEMAIL.Value;
  end;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'NOME', QrParamsEmpNO_CTD.Value) then
      (*03*) if not AC_6(ITO, 'CPF', CPF, False, False) then
      (*04*) if not AC_x(ITO, 'CRC', QrParamsEmpSPED_EFD_CRCContador.Value) then
      (*05*) if not AC_6(ITO, 'CNPJ', CNPJ, False, False) then
      (*06*) if not AC_i(ITO, 'CEP', CEP, False, False) then
      (*07*) if not AC_x(ITO, 'END', LOGR_RUA) then
      (*08*) if not AC_x(ITO, 'NUM', Numero_x) then
      (*09*) if not AC_x(ITO, 'COMPL', COMPL) then
      (*10*) if not AC_x(ITO, 'BAIRRO', BAIRRO) then
      (*11*) if not AC_x(ITO, 'FONE', FONE) then
      (*12*) if not AC_x(ITO, 'FAX', FAX) then
      (*13*) if not AC_x(ITO, 'EMAIL', EMAIL) then
      (*14*) if not AC_i(ITO, 'COD_MUN', COD_MUN, False, False) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    if COD_MUN = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCodMunIndef)(*FNumErro[17]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'COD_MUN'], True);
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_0);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0150(): Boolean;
  procedure InsereSelEntFatID0053();
    function MontaSQL(Tabela, Campo, CampoData: String; FiltraPorNFs: Boolean): String;
    var
      SQLFiltro: String;
    begin
      if FiltraPorNFs then
        SQLFiltro := 'AND NFe_FatNum IN (' + FCordaDe0053 + ') '
      else
        SQLFiltro := '';
      //
      Result := Geral.ATS([
      'INSERT INTO ' + FSelEnt,
      'SELECT DISTINCT einc.' + Campo + ' Codigo, "" Nome, 1 Ativo ',
      'FROM ' + TMeuDB + '.' + Tabela + ' einc',
      'WHERE einc.' + CampoData + ' BETWEEN "' + FDataIni_TXT + '" AND "' + FDataFim_TXT + '"',
      SQLFiltro,
      'ON DUPLICATE KEY UPDATE Ativo=1 ',
      '']);
    end;
  var
    SQL, Campo: String;
  begin
    if FCordaDe0053 <> EmptyStr then
    begin
      SQL := MontaSQL('efdinnnfscab', 'Terceiro', FDtFiscal0053_Entr, True);
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [SQL]);
      //SQL := MontaSQL('efdinnnfscab', 'Empresa', FDtFiscal0053_Entr, True);
      //UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [SQL]);
      SQL := MontaSQL('efdinnctscab', 'Terceiro', FDtFiscalFrete_Entr, False);
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [SQL]);
      //SQL := MontaSQL('efdinnctscab', 'Empresa', FDtFiscalFrete_Entr, False);
      //UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [SQL]);
    end;
  end;
  procedure Insere_Cod_Part_B(Tabela, Campo: String);
  begin
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'INSERT INTO ' + FSelEnt,
    ' ',
    'SELECT DISTINCT ' + Campo + ' Codigo, ',
    '"" Nome, 1 Ativo ',
    'FROM ' + TMeuDB + '.' + Tabela,
    'WHERE ImporExpor IN (1,3) ',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_Txt,
    'AND ' + Campo + ' <> "" ',
    'ON DUPLICATE KEY UPDATE Ativo=1 ',
    '']);
  end;
const
  ITO = itoNaoAplicavel;
var
  COD_PART, NOME, LOGR_RUA, NUMERO_x, CNPJ_CPF, IE: String;
  COD_PAIS, COD_MUN, NUMERO_i: Integer;
  CNPJ, CPF: Int64;
  Blc, SQLExec: String;
begin
  FBloco := '0';
  FRegistro := '0150';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  FSelEnt := UCriar.RecriaTempTableNovo(ntrtt_CodNom, DmodG.QrUpdPID1, False, 0, '_sel_ent_');
  Blc := Blocos_Selecionados2([]);
  SQLExec := Geral.ATS([
  'INSERT INTO ' + FSelEnt,
  '',
  'SELECT DISTINCT CodInfoEmit Codigo,',
  '"" Nome, 1 Ativo ',
  'FROM ' + TMeuDB + '.nfecaba ',
  'WHERE ide_tpAmb<>2  ',
  'AND Empresa=' + FEmpresa_Txt,
  //'AND FatID<>' + Geral.FF0(VAR_FATID_0053), // Qual est� certo????
  'AND FatID=' + Geral.FF0(VAR_FATID_0001),    // Tem FatID=2 ou 51 ????
  'AND CodInfoEmit <> ' + FEmpresa_Txt,
  'AND DataFiscal BETWEEN ' +  FIntervaloFiscal,
  'AND Status IN (100,101,102,110,301) ',
  Blc,
  ' ',
  'UNION ',
  ' ',
  'SELECT DISTINCT CodInfoEmit Codigo,',
  '"" Nome, 1 Ativo ',
  'FROM ' + TMeuDB + '.nfecaba ',
  'WHERE ide_tpAmb<>2  ',
  'AND Empresa=' + FEmpresa_Txt,
  //'AND FatID<>' + Geral.FF0(VAR_FATID_0053), // Qual est� certo????
  'AND FatID=' + Geral.FF0(VAR_FATID_0001),    // Tem FatID=2 ou 51 ????
  'AND CodInfoDest <> ' + FEmpresa_Txt,
  'AND DataFiscal BETWEEN ' +  FIntervaloFiscal,
  'AND Status IN (100,101,102,110,301) ',
  Blc,
  '']);
  //Geral.MB_Teste(SQLExec);
  //UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, ['', SQLExec, '']);
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, SQLExec);
  //UnDmkDAC_PF.ExecutaQuery(DModG.QrUpdPID1, DModG.MyPID_DB, SQLExec);
  //UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [SQLExec]);
  //
(*
  if QrNiv2.Locate('Registro', 'C500', []) then
  begin
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'INSERT INTO ' + FSelEnt,
    '',
    'SELECT DISTINCT Terceiro Codigo,',
    '"" Nome, 1 Ativo',
    'FROM ' + TMeuDB + '.efdicmsipic500',
    'WHERE ImporExpor IN (1,3)',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_Txt,
    'ON DUPLICATE KEY UPDATE Ativo=1',
    '']);
  end;
  //
  if QrNiv2.Locate('Registro', 'D100', []) then
  begin
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'INSERT INTO ' + FSelEnt,
    '',
    'SELECT DISTINCT Terceiro Codigo,',
    '"" Nome, 1 Ativo',
    'FROM ' + TMeuDB + '.efdicmsipid100',
    'WHERE ImporExpor IN (1,3)',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_Txt,
    'ON DUPLICATE KEY UPDATE Ativo=1',
    '']);
  end;
  //
  if QrNiv2.Locate('Registro', 'D500', []) then
  begin
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'INSERT INTO ' + FSelEnt,
    '',
    'SELECT DISTINCT Terceiro Codigo,',
    '"" Nome, 1 Ativo',
    'FROM ' + TMeuDB + '.efdicmsipid500',
    'WHERE ImporExpor IN (1,3)',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_Txt,
    'ON DUPLICATE KEY UPDATE Ativo=1',
    '']);
  end;
*)
  //Geral.MB_Info(DModG.QrUpdPID1.SQL.Text);
  //
  InsereSelEntFatID0053();

  if FSelRegC100 then
    Insere_Cod_Part_B('efdicmsipic100', 'Terceiro');
  if FSelRegC500 then
    Insere_Cod_Part_B('efdicmsipic500', 'Terceiro');
  if FSelRegD500 then
    Insere_Cod_Part_B('efdicmsipid500', 'Terceiro');
  Insere_Cod_Part_B('efdicmsipik200', 'COD_PART');
  Insere_Cod_Part_B('efdicmsipik280', 'COD_PART');

  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSelEnt, DModG.MyPID_DB, [
  'SELECT DISTINCT Codigo ',
  'FROM ' + FSelEnt,
  '']);
  //
  PB1.Position := 0;
  PB1.Max := QrSelEnt.RecordCount;
  QrSelEnt.First;
  while not QrSelEnt.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    Numero_i := 0;
(*
    QrEnder.Close;
    QrEnder.Params[0].AsInteger := QrSelEntCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrEnder, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
    ReopenEnder(QrSelEntCodigo.Value);
    //
    //COD_PART := Geral.FF0(QrSelEntCodigo.Value); /
    COD_PART := ObtemCOD_PART(QrSelEntCodigo.Value);
    NOME := Copy(QrEnderNOME_ENT.Value, 1, 100);
    CNPJ_CPF := Geral.SoNumero_TT(QrEnderCNPJ_CPF.Value);
    if QrEnderTipo.Value = 0 then
    begin
      COD_PAIS := QrEnderECodiPais.Value;
      CNPJ := Geral.I64MV(CNPJ_CPF);
      CPF := 0;
      COD_MUN := QrEnderECodMunici.Value;
    end else
    begin
      COD_PAIS := QrEnderPCodiPais.Value;
      CNPJ := 0;
      CPF := Geral.I64MV(CNPJ_CPF);
      COD_MUN := QrEnderPCodMunici.Value;
    end;
    IE := Geral.SoNumero_TT(QrEnderIE.Value);
    LOGR_RUA := QrEnderNOMELOGRAD.Value;
    if Trim(LOGR_RUA) <> '' then
      LOGR_RUA := LOGR_RUA + ' ';
    LOGR_RUA := LOGR_RUA + QrEnderRua.Value;
    if Numero_i = 0 then
      NUMERO_x := 'S/N'
    else
      NUMERO_x := Geral.FF0(Numero_i);
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_PART', COD_PART) then
        (*03*) if not AC_x(ITO, 'NOME', NOME) then
        (*04*) if not AC_i(ITO, 'COD_PAIS', COD_PAIS, False, True) then
        (*05*) if not AC_6(ITO, 'CNPJ', CNPJ, True, False) then
        (*06*) if not AC_6(ITO, 'CPF', CPF, True, False) then
        (*07*) if not AC_x(ITO, 'IE', IE) then
        (*08*) if not AC_i(ITO, 'COD_MUN', COD_MUN, COD_PAIS <> 1058, True) then
        (*09*) if not AC_x(ITO, 'SUFRAMA', QrEnderSUFRAMA.Value) then
        (*10*) if not AC_x(ITO, 'END', LOGR_RUA) then
        (*11*) if not AC_x(ITO, 'NUM', NUMERO_x) then
        (*12*) if not AC_x(ITO, 'COMPL', QrEnderCOMPL.Value) then
        (*13*) if not AC_x(ITO, 'BAIRRO', QrEnderBAIRRO.Value) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
    if COD_MUN = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCodMunIndef)(*FNumErro[17]*)], [
      FImporExpor, FAnoMes_Int,
      FEmpresa_Int, FLinArq, FRegistro, 'COD_MUN'], True);
    end;
    finally
      AdicionaLinha(FQTD_LIN_0);
    end;
    //
    QrSelEnt.Next;
  end;
  AdicionaBLC(QrSelEnt.RecordCount);
  Result := True;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0190_Antigo(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  UNID, DESCR, SQL_Proprio: String;
begin
  FBloco := '0';
  FRegistro := '0190';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  SQL_Proprio := 'AND CodInfoEmit<>' + FEmpresa_Txt;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrUniMedi, Dmod.MyDB, [
  'SELECT DISTINCT nfei.prod_uTrib ',
  'FROM nfeitsi nfei ',
  'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID ',
  '     AND nfea.FatNum=nfei.FatNum ',
  '     AND nfea.Empresa=nfei.EMpresa ',
  'WHERE  nfea.ide_tpAmb<>2  ',
  'AND nfea.Status IN (100,101,102,110,301) ',
  'AND nfea.Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND nfea.DataFiscal BETWEEN "' + FDataIni_Txt + '" AND  "' + FDataFim_Txt + '" ',
  SQL_Proprio,
  'ORDER BY prod_uTrib ',
  '']);
(*
  QrUniMedi.Close;
  QrUniMedi.Params[00].AsInteger := FEmpresa_Int;
  QrUniMedi.Params[01].AsString  := FDataIni_Txt;
  QrUniMedi.Params[02].AsString  := FDataFim_Txt;
  QrUniMedi. O p e n ;
*)
  PB1.Position := 0;
  PB1.Max := QrUniMedi.RecordCount;
  //
  QrUniMedi.First;
  while not QrUniMedi.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    (*
    QrUnidMed.Close;
    QrUnidMed.Params[0].AsString := QrUniMediNoUnidMed.Value;
    UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    *)
    UnNFe_PF.ReopenUnidMed(UNID, QrUnidMed, null);
    //
    UNID := QrUniMediNoUnidMed.Value;
    DESCR := QrUnidMedNome.Value;
    if DESCR = '' then
      DESCR := UNID;
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'UNID', UNID) then
        (*03*) if not AC_x(ITO, 'DESCR', DESCR) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
    finally
      AdicionaLinha(FQTD_LIN_0);
    end;
    //
    QrUniMedi.Next;
  end;
  AdicionaBLC(QrUniMedi.RecordCount);
  Result := True;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0190_Novo(): Boolean;
  procedure InsereUnidMedFatID0053();
  begin
    if FCordaDe0053 <> EmptyStr then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
      'INSERT INTO ' + FSPEDEFD_0190,
      'SELECT DISTINCT eini.UNID, 1 Ativo ',
      'FROM ' + TMeuDB + '.efdinnnfsits eini',
      'LEFT JOIN ' + TMeuDB + '.efdinnnfscab einc ON einc.Controle=eini.Controle',
      'WHERE einc.' + FDtFiscal0053_Entr + ' BETWEEN "' + FDataIni_TXT + '" AND "' + FDataFim_TXT + '"',
      'AND einc.NFe_FatNum IN (' + FCordaDe0053 + ') ',
      '']);
      //Geral.MB_Teste(DModG.QrUpdPID1.SQL.Text);
    end;
  end;
  procedure InsereUnidMed0190A(Tabela, Campo: String);
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + FSPEDEFD_0190,
    'SELECT ' + Campo + ' NoUnidMed, 1 Ativo ',
    'FROM ' + TMeuDB + '.' + Tabela + ' ',
    'WHERE ImporExpor=3 ',// + Geral.FF0(FImporExpor),
    'AND Empresa=' + Geral.FF0(FEmpresa_Int),
    'AND AnoMes=' + Geral.FF0(FAnoMes_Int),
    '']);
  end;
  procedure InsereUnidMed0190B(Tabela, Campo: String);
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + FSPEDEFD_0190,
    'SELECT DISTINCT med.Sigla NoUnidMed, 1 Ativo ',
    'FROM ' + TMeuDB + '.' + Tabela + ' cab',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=cab.' + Campo,
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN ' + TMeuDB + '.unidmed med ON med.Codigo=gg1.UnidMed',
    'WHERE cab.ImporExpor=3 ',
    'AND cab.Empresa=' + Geral.FF0(FEmpresa_Int),
    'AND cab.AnoMes=' + Geral.FF0(FAnoMes_Int),
    'AND cab.' + Campo + ' <> 0',
    '']);
  end;
const
  ITO = itoNaoAplicavel;
var
  UNID, DESCR, SQL_Proprio: String;
  JaGravou: Boolean;
begin
  FBloco := '0';
  FRegistro := '0190';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  JaGravou := False;
  FSPEDEFD_0190 :=
    SPED_Create.RecriaTempTableNovo(ntrttSPEDEFD_0190, DmodG.QrUpdPID1, False);
  // 2016-02-07 Nao entendi porque!!!
  SQL_Proprio := 'AND CodInfoEmit<>' + FEmpresa_Txt;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FSPEDEFD_0190,
  'SELECT DISTINCT nfei.prod_uTrib NoUnidMed, 1 Ativo ',
  'FROM ' + TMeuDB + '.nfeitsi nfei ',
  'LEFT JOIN ' + TMeuDB + '.nfecaba nfea ON nfea.FatID=nfei.FatID ',
  '     AND nfea.FatNum=nfei.FatNum ',
  '     AND nfea.Empresa=nfei.EMpresa ',
  'WHERE  nfea.ide_tpAmb<>2  ',
  'AND nfea.Status IN (100,101,102,110,301) ',
  'AND nfea.Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND nfea.DataFiscal BETWEEN "' + FDataIni_Txt + '" AND  "' + FDataFim_Txt + '" ',
  // ini 2022-04-13
  'AND nfea.FatID=' + Geral.FF0(VAR_FATID_0001),
  // fim 2022-04-13
  SQL_Proprio,
  //'ORDER BY prod_uTrib ',
  '']);
  InsereUnidMedFatID0053();
  InsereUnidMed0190A('efdicmsipih010', 'UNID');
  InsereUnidMed0190B('efdicmsipik200', 'GraGruX');
  InsereUnidMed0190B('efdicmsipik220', 'GGXDst');
  InsereUnidMed0190B('efdicmsipik220', 'GGXOri');
  InsereUnidMed0190B('efdicmsipik230', 'GraGruX');
  InsereUnidMed0190B('efdicmsipik235', 'GraGruX');
  InsereUnidMed0190B('efdicmsipik250', 'GraGruX');
  InsereUnidMed0190B('efdicmsipik255', 'GraGruX');
  InsereUnidMed0190B('efdicmsipik280', 'GraGruX');
  //InsereUnidMed0190B('efdicmsipik290', 'GraGruX');
  InsereUnidMed0190B('efdicmsipik291', 'GraGruX');
  InsereUnidMed0190B('efdicmsipik292', 'GraGruX');
  //InsereUnidMed0190B('efdicmsipik300', 'GraGruX');
  InsereUnidMed0190B('efdicmsipik301', 'GraGruX');
  InsereUnidMed0190B('efdicmsipik302', 'GraGruX');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrUniMedi, DModG.MyPID_DB, [
  'SELECT DISTINCT NoUnidMed ',
  'FROM ' + FSPEDEFD_0190,
  '']);
  PB1.Position := 0;
  PB1.Max := QrUniMedi.RecordCount;
  //
  QrUniMedi.First;
  while not QrUniMedi.Eof do
  begin
    MeErros.Lines.Add('Implementar registro 0220!');
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    (*
    QrUnidMed.Close;
    QrUnidMed.Params[0].AsString := QrUniMediNoUnidMed.Value;
    UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    *)
    UNID  := QrUniMediNoUnidMed.Value;
    UnNFe_PF.ReopenUnidMed(UNID, QrUnidMed, null);
    //
    DESCR := QrUnidMedNome.Value;
    if DESCR = '' then
      //DESCR := UNID;
    try
      //Geral.MB_SQL(Self, QrUnidMed);
      if not JaGravou then
      begin
        Grava_SpedEfdIcmsIpiErrs(efdexerObrigSemCont, 'DESCR');
        JaGravou := True;
      end;
    except
      ;
    end;
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'UNID', UNID) then
        (*03*) if not AC_x(ITO, 'DESCR', DESCR) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
    finally
      AdicionaLinha(FQTD_LIN_0);
    end;
    //
    QrUniMedi.Next;
  end;
  AdicionaBLC(QrUniMedi.RecordCount);
  Result := True;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0200_Antigo(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_ITEM, DESCR_ITEM, COD_BARRA, COD_ANT_ITEM, UNID_INV, COD_NCM, EX_IPI,
  C_L, NCM, Campo, TIPO_ITEM, SQL_Proprio, COD_LST: String;
  COD_GEN: Integer;
  ALIQ_ICMS: Double;
begin
  FBloco := '0';
  FRegistro := '0200';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
{  QrReduzidos.Close;
  QrReduzidos.Params[00].AsInteger := FEmpresa_Int;
  QrReduzidos.Params[01].AsString  := FDataIni_Txt;
  QrReduzidos.Params[02].AsString  := FDataFim_Txt;
  QrReduzidos. O p e n ;
}
  // 2016-02-07 Nao entendi porque!!!
  SQL_Proprio := 'AND CodInfoEmit<>' + FEmpresa_Txt;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrReduzidos, Dmod.MyDB, [
  'SELECT DISTINCT nfei.GraGruX ',
  'FROM nfeitsi nfei ',
  'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID ',
  '     AND nfea.FatNum=nfei.FatNum ',
  '     AND nfea.Empresa=nfei.EMpresa ',
  'WHERE nfea.ide_tpAmb<>2 ',
  'AND Status IN (100,101,102,110,301) ',
  'AND nfea.Empresa=' + FEmpresa_Txt,
  'AND nfea.DataFiscal BETWEEN ' + FIntervaloFiscal,
  'AND GraGruX <> 0 ',
  Blocos_Selecionados2(['C500','D500']),
  SQL_Proprio,
  'ORDER BY GraGruX ',
  '']);

  PB1.Position := 0;
  PB1.Max := QrReduzidos.RecordCount;
    //
  QrReduzidos.First;
  while not QrReduzidos.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
(*
    QrProduto1.Close;
    QrProduto1.Params[0].AsInteger := QrReduzidosGraGruX.Value;
    UnDmkDAC_PF.AbreQuery(QrProduto1, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrProduto1, Dmod.MyDB, [
    'SELECT ggx.Controle, CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
    'NO_PRD_TAM_COR, gg1.cGTIN_EAN , gg1.NCM, ',
    'gg1.EX_TIPI, gg1.COD_LST, gg1.SPEDEFD_ALIQ_ICMS, ',
    'unm.SIGLA, pgt.Tipo_Item ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
    'WHERE ggx.Controle=' + Geral.FF0(QrReduzidosGraGruX.Value),
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
    //Geral.MB_SQL(Self, QrProduto1);
    //
    C_L := Geral.SoNumero_TT(QrProduto1COD_LST.Value);
    if Geral.IMV(C_L) = 0 then
      C_L := ''
    else
    begin
      while Length(C_L) < 4 do
      begin
        C_L := '0' + C_L;
      end;
      C_L := C_L[1] + C_L[2] + '.' + C_L[3] + C_L[4];
    end;
    //
    COD_ITEM     := ObtemCOD_ITEM(QrReduzidosGraGruX.Value);
    DESCR_ITEM   := Copy(QrProduto1NO_PRD_TAM_COR.Value, 1, 255);
    VerificaDESCR_ITEM(DESCR_ITEM, QrProduto1);
    COD_BARRA    := QrProduto1cGTIN_EAN.Value;
    COD_ANT_ITEM := ''; // Nao preeencher aqui, mas no 0205
    UNID_INV     := QrProduto1SIGLA.Value;
    TIPO_ITEM    := FormatFloat('00', QrProduto1Tipo_Item.Value);
    COD_NCM      := Geral.SoNumero_TT(QrProduto1NCM.Value);
    EX_IPI       := QrProduto1EX_TIPI.Value;
    COD_GEN      := Geral.IMV(Copy(COD_NCM, 1, 2));
    //COD_LST      := Geral.IMV(C_L);
    COD_LST      := C_L;
    ALIQ_ICMS    := QrProduto1SPEDEFD_ALIQ_ICMS.Value;
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
        (*03*) if not AC_x(ITO, 'DESCR_ITEM', DESCR_ITEM) then
        (*04*) if not AC_x(ITO, 'COD_BARRA', COD_BARRA) then
        (*05*) if not AC_n(ITO, 'COD_ANT_ITEM', COD_ANT_ITEM) then
        (*06*) if not AC_x(ITO, 'UNID_INV', UNID_INV) then
        (*07*) if not AC_n(ITO, 'TIPO_ITEM', TIPO_ITEM) then
        (*08*) if not AC_x(ITO, 'COD_NCM', COD_NCM) then
        (*09*) if not AC_x(ITO, 'EX_IPI', EX_IPI) then
        (*10*) if not AC_i(ITO, 'COD_GEN', COD_GEN, COD_LST <> '', True) then
        (*11*) if not AC_x(ITO, 'COD_LST', COD_LST) then
        (*12*) if not AC_f(ITO, 'ALIQ_ICMS', ALIQ_ICMS, True, False) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
      if C_L <> QrProduto1COD_LST.Value then
      begin
        Campo := 'COD_LST';
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes', 'Empresa',
        'LinArq', 'REG', 'Campo'], [
        Integer(efdexerServicInval)(*FNumErro[10]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, Campo], True);
      end;
      NCM := Geral.SoNumero_TT(QrProduto1NCM.Value);
      if Length(NCM) < 2 then
      begin
        Campo := 'COD_NCM';
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes', 'Empresa',
        'LinArq', 'REG', 'Campo'], [
        Integer(efdexerFaltaNCM)(*FNumErro[11]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, Campo], True);
      end;
    finally
      AdicionaLinha(FQTD_LIN_0);
    end;
    //
    QrReduzidos.Next;
  end;
  AdicionaBLC(QrReduzidos.RecordCount);
  Result := True;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0200_Novo(): Boolean;
  procedure InsereGraGruXFatID0053();
  begin
    if FCordaDe0053 <> EmptyStr then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
      'INSERT INTO ' + FSPEDEFD_0200,
      'SELECT DISTINCT eini.GraGruX, 1 Ativo ',
      'FROM ' + TMeuDB + '.efdinnnfsits eini',
      'LEFT JOIN ' + TMeuDB + '.efdinnnfscab einc ON einc.Controle=eini.Controle',
      'WHERE einc.' + FDtFiscal0053_Entr + ' BETWEEN "' + FDataIni_TXT + '" AND "' + FDataFim_TXT + '"',
      'AND einc.NFe_FatNum IN (' + FCordaDe0053 + ') ',
      '']);
    end;
  end;
  procedure InsereGraGruX0200(Tabela, Campo: String);
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + FSPEDEFD_0200,
    'SELECT ' + Campo + ' GraGruX, 1 Ativo ',
    'FROM ' + TMeuDB + '.' + Tabela + ' ',
    'WHERE ImporExpor=3 ',// + Geral.FF0(FImporExpor),
    'AND Empresa=' + Geral.FF0(FEmpresa_Int),
    'AND AnoMes=' + Geral.FF0(FAnoMes_Int),
    '']);
  end;
const
  ITO = itoNaoAplicavel;
var
  COD_ITEM, DESCR_ITEM, COD_BARRA, COD_ANT_ITEM, UNID_INV, COD_NCM, EX_IPI,
  C_L, NCM, Campo, TIPO_ITEM, COD_LST, SQL_Proprio: String;
  COD_GEN, CEST, GraGruX: Integer;
  ALIQ_ICMS: Double;
  //
  procedure InsereAtual();
  begin
    FBloco := '0';
    FRegistro := '0200';
    Result := False;
    //PB1.Max := 0;
    //PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    try
      PredefineLinha();
      FLin0200 := FLinArq;
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
        (*03*) if not AC_x(ITO, 'DESCR_ITEM', DESCR_ITEM) then
        (*04*) if not AC_x(ITO, 'COD_BARRA', COD_BARRA) then
        (*05*) if not AC_n(ITO, 'COD_ANT_ITEM', COD_ANT_ITEM) then
        (*06*) if not AC_x(ITO, 'UNID_INV', UNID_INV) then
        (*07*) if not AC_n(ITO, 'TIPO_ITEM', TIPO_ITEM) then
        (*08*) if not AC_x(ITO, 'COD_NCM', COD_NCM) then
        (*09*) if not AC_x(ITO, 'EX_IPI', EX_IPI) then
        (*10*) if not AC_i(ITO, 'COD_GEN', COD_GEN, COD_LST <> '', True) then
        (*11*) if not AC_x(ITO, 'COD_LST', COD_LST) then
        (*12*) if not AC_f(ITO, 'ALIQ_ICMS', ALIQ_ICMS, True, False) then
        (*13*) if not AC_i(ITO, 'CEST', CEST, True, (*True*)False) then
        //
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      AddCampoSQL('GraGruX', GraGruX);
      //
      if C_L <> QrProduto2COD_LST.Value then
      begin
        Campo := 'COD_LST';
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes', 'Empresa',
        'LinArq', 'REG', 'Campo'], [
        Integer(efdexerServicInval)(*FNumErro[10]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, Campo], True);
      end;
      NCM := Geral.SoNumero_TT(QrProduto2NCM.Value);
      if Length(NCM) < 2 then
      begin
        Campo := 'COD_NCM';
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes', 'Empresa',
        'LinArq', 'REG', 'Campo'], [
        Integer(efdexerFaltaNCM)(*FNumErro[11]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, Campo], True);
      end;
    finally
      AdicionaLinha(FQTD_LIN_0);
      //
    end;
  end;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _efdicmsipi0210_x;',
  'CREATE TABLE _efdicmsipi0210_x',
  '',
  'SELECT DISTINCT gragrux',
  'FROM ' + TMeuDB + '.efdicmsipiK230 ',
  'WHERE ImporExpor=3 ',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_Txt,
  '',
  'UNION',
  '',
  'SELECT DISTINCT gragrux',
  'FROM ' + TMeuDB + '.efdicmsipiK250 ',
  'WHERE ImporExpor=3 ',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_Txt,
  ';',
  //'',
  //'SELECT * FROM _efdicmsipi0210_x',
  '',
  '']);
(*
  FBloco := '0';
  FRegistro := '0200';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
*)
  //
  FSPEDEFD_0200 :=
    SPED_Create.RecriaTempTableNovo(ntrttSPEDEFD_0200, DmodG.QrUpdPID1, False);
  // 2016-02-07 Nao entendi porque!!!
  SQL_Proprio := 'AND CodInfoEmit<>' + FEmpresa_Txt;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FSPEDEFD_0200,
  'SELECT DISTINCT nfei.GraGruX, 1 Ativo ',
  'FROM ' + TMeuDB + '.nfeitsi nfei ',
  'LEFT JOIN ' + TMeuDB + '.nfecaba nfea ON nfea.FatID=nfei.FatID ',
  '     AND nfea.FatNum=nfei.FatNum ',
  '     AND nfea.Empresa=nfei.EMpresa ',
  'WHERE nfea.ide_tpAmb<>2 ',
  'AND Status IN (100,101,102,110,301) ',
  'AND nfea.Empresa=' + FEmpresa_Txt,
  'AND nfea.DataFiscal BETWEEN ' + FIntervaloFiscal,
  'AND GraGruX <> 0 ',
  Blocos_Selecionados2(['C500','D500']),
  SQL_Proprio,
  'ORDER BY GraGruX ',
  '']);
  //
{
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FSPEDEFD_0200,
  'SELECT COD_ITEM GraGruX, 1 Ativo ',
  'FROM ' + TMeuDB + '.efdicmsipih010 ',
  'WHERE ImporExpor=3 ',// + Geral.FF0(FImporExpor),
  'AND Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND AnoMes=' + Geral.FF0(FAnoMes_Int),
  '']);
}
  InsereGraGruXFatID0053();
  InsereGraGruX0200('efdicmsipih010', 'COD_ITEM');
  InsereGraGruX0200('efdicmsipik200', 'GraGruX');
  InsereGraGruX0200('efdicmsipik220', 'GGXDst');
  InsereGraGruX0200('efdicmsipik220', 'GGXOri');
  InsereGraGruX0200('efdicmsipik230', 'GraGruX');
  InsereGraGruX0200('efdicmsipik235', 'GraGruX');
  InsereGraGruX0200('efdicmsipik250', 'GraGruX');
  InsereGraGruX0200('efdicmsipik255', 'GraGruX');
  InsereGraGruX0200('efdicmsipik280', 'GraGruX');
  if Geral.IMV(FVersao) >= 13 then // 2019-01-01
  begin
    //InsereGraGruX0200('efdicmsipik290', 'GraGruX');
    InsereGraGruX0200('efdicmsipik291', 'GraGruX');
    InsereGraGruX0200('efdicmsipik292', 'GraGruX');
    //InsereGraGruX0200('efdicmsipik300', 'GraGruX');
    InsereGraGruX0200('efdicmsipik301', 'GraGruX');
    InsereGraGruX0200('efdicmsipik302', 'GraGruX');
  end;
  //
  if not AppPF.ReabreSPEDEFD0200Exportacao(QrProduto2, FSPEDEFD_0200) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrProduto2, DmodG.MyPID_DB, [
    'SELECT ggx.Controle, ggx.GraGru1, CONCAT(gg1.Nome,  ',
    'IF(gtc.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome)))  ',
    'NO_PRD_TAM_COR, gg1.cGTIN_EAN , gg1.NCM, ',
    'gg1.EX_TIPI, gg1.COD_LST, gg1.SPEDEFD_ALIQ_ICMS, ',
    //'unm.SIGLA, pgt.Tipo_Item ',
    'unm.SIGLA, gg1.prod_CEST, ',
    //'IF(gg1.Nivel2 <> 0, gg2.Tipo_Item, pgt.Tipo_Item) Tipo_Item ',
    'IF(gg1.Nivel2 <> 0 AND gg2.Tipo_Item >= 0, gg2.Tipo_Item, pgt.Tipo_Item) Tipo_Item',
    //
    'FROM ' + TMeuDB + '.gragrux ggx ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gratamcad  gtc ON gtc.Codigo=gti.Codigo ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
    'WHERE ggx.Controle IN ( ',
    '  SELECT DISTINCT GraGruX  ',
    '  FROM ' + FSPEDEFD_0200,
    ') ',
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
  end;
  //Geral.MB_SQL(Self, QrProduto2);
  //
  PB1.Position := 0;
  PB1.Max := QrProduto2.RecordCount;
    //
  QrProduto2.First;
  while not QrProduto2.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    C_L := Geral.SoNumero_TT(QrProduto2COD_LST.Value);
    if Geral.IMV(C_L) = 0 then
      C_L := ''
    else
    begin
      while Length(C_L) < 4 do
      begin
        C_L := '0' + C_L;
      end;
      C_L := C_L[1] + C_L[2] + '.' + C_L[3] + C_L[4];
    end;
    //
    COD_ITEM     := ObtemCOD_ITEM(QrProduto2Controle.Value);
    DESCR_ITEM   := Copy(QrProduto2NO_PRD_TAM_COR.Value, 1, 255);
    VerificaDESCR_ITEM(DESCR_ITEM, QrProduto2);
    COD_BARRA    := QrProduto2cGTIN_EAN.Value;
    COD_ANT_ITEM := ''; // Nao preeencher aqui, mas no 0205
    UNID_INV     := QrProduto2SIGLA.Value;
    //
    TIPO_ITEM    := FormatFloat('00', QrProduto2Tipo_Item.Value);
    COD_NCM      := Geral.SoNumero_TT(QrProduto2NCM.Value);
    EX_IPI       := QrProduto2EX_TIPI.Value;
    COD_GEN      := Geral.IMV(Copy(COD_NCM, 1, 2));
    //COD_LST      := Geral.IMV(C_L);
    COD_LST      := C_L;
    ALIQ_ICMS    := QrProduto2SPEDEFD_ALIQ_ICMS.Value;
    CEST         := QrProduto2prod_CEST.Value;
    //
    GraGruX      := QrProduto2Controle.Value;
    //
    InsereAtual();
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, DModG.MyPID_DB, [
    'SELECT GraGruX',
    'FROM _efdicmsipi0210_x',
    'WHERE GraGruX=' + Geral.FF0(QrProduto2Controle.Value),
    '']);
    GeraRegistro_0205();
    //
    if QrPsq.RecordCount > 0 then
      GeraRegistro_0210();
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qr0220, Dmod.MyDB, [
    'SELECT med.Sigla, c170.UNID, SUM(itsi.prod_qTrib) prod_qTrib, ',
    'SUM(c170.QTD) QTD,   ',
    // Assim ou invertido?
    'SUM(itsi.prod_qTrib) / SUM(c170.QTD) FATOR ',
    'FROM nfeefd_c170 c170 ',
    'LEFT JOIN nfecaba caba ON ',
    '  caba.FatID = c170.FatID ',
    '  AND caba.FatNum=c170.FatNum ',
    '  AND caba.Empresa=c170.Empresa ',
    'LEFT JOIN nfeitsi itsi ON ',
    '  itsi.FatID = c170.FatID ',
    '  AND itsi.FatNum=c170.FatNum ',
    '  AND itsi.Empresa=c170.Empresa ',
    '  AND itsi.nItem=c170.nItem ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=c170.GraGrux ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
    'WHERE caba.DataFiscal BETWEEN ' + FIntervaloFiscal,
    'AND UPPER(med.Sigla) <> UPPER(c170.UNID) ',
    'AND c170.GraGruX=' + Geral.FF0(QrProduto2Controle.Value),
    'GROUP BY med.Sigla, c170.UNID ',
    '']);
    if Qr0220.RecordCount > 0 then
      GeraRegistro_0220();
    //
    QrProduto2.Next;
  end;
  AdicionaBLC(QrProduto2.RecordCount, '0200');
  AdicionaBLC(FQTD_LIN_0210, '0210');
  AdicionaBLC(FQTD_LIN_0220, '0220');
  //
  Result := True;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0205(): Boolean;
begin
  // Parei aqui! Altera��es de Nome!
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0210(): Boolean;
const
  ITO = itoNaoAplicavel;
  //
  procedure GeraRegistroAtual();
  var
    COD_ITEM_COMP: String;
    QTD_COMP, PERDA: Double;
  begin
    FBloco := '0';
    FRegistro := '0210';
    Result := False;
    //PB1.Max := 0;
    //PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    COD_ITEM_COMP := ObtemCOD_ITEM(QrGraGru1ConsGGX_CONSU.Value);
    QTD_COMP      := QrGraGru1ConsQtd_Comp.Value;
    PERDA         := QrGraGru1ConsPerda.Value;
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_ITEM_COMP', COD_ITEM_COMP) then
        (*03*) if not AC_f(ITO, 'QTD_COMP', QTD_COMP, False, True) then
        (*04*) if not AC_f(ITO, 'PERDA', PERDA, True, True) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
(*
      if ?? <> ?? then
      begin
        Campo := ??;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'Campo'], [FNumErro[10]], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int, FLinArq, FRegistro, Campo], True);
      end;
*)
      AddCampoSQL('Reg0200', FLin0200);
    finally
      FQTD_LIN_0210 :=  FQTD_LIN_0210 + 1;
      AdicionaLinha(FQTD_LIN_0);
    end;
  end;
begin
////////////////  ini 2019-02-02 ///////////////////////////////////////////////
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1Cons, Dmod.MyDB, [
  'DROP TABLE IF EXISTS _efd_0210_a;',
  'CREATE TABLE _efd_0210_a',
  '  SELECT LinArq',
  '  FROM ' + TMeuDB + '.efdicmsipiK230',
  '  WHERE GraGruX=' + Geral.FF0(QrProduto2Controle.Value),
  '  AND ImporExpor=3',
  '  AND AnoMes=' + FAnoMes_Txt,
  '  AND Empresa=' + FEmpresa_TXT,
  ';',
  'DROP TABLE IF EXISTS _efd_0210_b;',
  'CREATE TABLE _efd_0210_b',
  'SELECT DISTINCT _235.GraGruX',
  'FROM ' + TMeuDB + '.efdicmsipiK235 _235',
  'LEFT JOIN _efd_0210_a _a ON _a.LinArq=_235.K230',
  'WHERE _235.ImporExpor=3',
  'AND _235.AnoMes=' + FAnoMes_Txt,
  'AND _235.Empresa=' + FEmpresa_TXT,
  'AND _235.K230=_a.LinArq',
  ';',
  '',
  'DROP TABLE IF EXISTS _efd_0210_a;',
  'CREATE TABLE _efd_0210_a',
  '  SELECT LinArq',
  '  FROM ' + TMeuDB + '.efdicmsipiK250',
  '  WHERE GraGruX=' + Geral.FF0(QrProduto2Controle.Value),
  '  AND ImporExpor=3',
  '  AND AnoMes=' + FAnoMes_Txt,
  '  AND Empresa=' + FEmpresa_TXT,
  ';',
  '/*',
  'DROP TABLE IF EXISTS _efd_0210_b;',
  'CREATE TABLE _efd_0210_b',
  '*/',
  'INSERT INTO _efd_0210_b',
  'SELECT DISTINCT _255.GraGruX',
  'FROM ' + TMeuDB + '.efdicmsipiK255 _255',
  'LEFT JOIN _efd_0210_a _a ON _a.LinArq=_255.K250',
  'WHERE _255.ImporExpor=3',
  'AND _255.AnoMes=' + FAnoMes_Txt,
  'AND _255.Empresa=' + FEmpresa_TXT,
  'AND _255.K250=_a.LinArq',
  ';',
  '',
  'SELECT DISTINCT _b.GraGruX  GGX_CONSU, g1c.Qtd_Comp, g1c.Perda ',
  'FROM _efd_0210_b _b ',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=_b.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragru1cons g1c ON g1c.Niv1_COMP=ggx.GraGru1',
  'WHERE g1c.Nivel1=' + Geral.FF0(QrProduto2GraGru1.Value),
  ';',
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGru1Cons, Dmod.MyDB, [
  'DROP TABLE IF EXISTS _efd_0210_a;',
  'CREATE TABLE _efd_0210_a',
  '  SELECT IDSeq1',
  '  FROM ' + TMeuDB + '.efdicmsipiK230',
  '  WHERE GraGruX=' + Geral.FF0(QrProduto2Controle.Value),
  '  AND ImporExpor=3',
  '  AND AnoMes=' + FAnoMes_Txt,
  '  AND Empresa=' + FEmpresa_TXT,
  ';',
  'DROP TABLE IF EXISTS _efd_0210_b;',
  'CREATE TABLE _efd_0210_b',
  'SELECT DISTINCT _235.GraGruX',
  'FROM ' + TMeuDB + '.efdicmsipiK235 _235',
  'LEFT JOIN _efd_0210_a _a ON _a.IDSeq1=_235.IDSeq1',
  'WHERE _235.ImporExpor=3',
  'AND _235.AnoMes=' + FAnoMes_Txt,
  'AND _235.Empresa=' + FEmpresa_TXT,
  'AND _235.IDSeq1=_a.IDSeq1',
  ';',
  '',
  'DROP TABLE IF EXISTS _efd_0210_a;',
  'CREATE TABLE _efd_0210_a',
  '  SELECT IDSeq1',
  '  FROM ' + TMeuDB + '.efdicmsipiK250',
  '  WHERE GraGruX=' + Geral.FF0(QrProduto2Controle.Value),
  '  AND ImporExpor=3',
  '  AND AnoMes=' + FAnoMes_Txt,
  '  AND Empresa=' + FEmpresa_TXT,
  ';',
  '/*',
  'DROP TABLE IF EXISTS _efd_0210_b;',
  'CREATE TABLE _efd_0210_b',
  '*/',
  'INSERT INTO _efd_0210_b',
  'SELECT DISTINCT _255.GraGruX',
  'FROM ' + TMeuDB + '.efdicmsipiK255 _255',
  'LEFT JOIN _efd_0210_a _a ON _a.IDSeq1=_255.IDSeq1',
  'WHERE _255.ImporExpor=3',
  'AND _255.AnoMes=' + FAnoMes_Txt,
  'AND _255.Empresa=' + FEmpresa_TXT,
  'AND _255.IDSeq1=_a.IDSeq1',
  ';',
  '',
  'SELECT DISTINCT _b.GraGruX  GGX_CONSU, g1c.Qtd_Comp, g1c.Perda ',
  'FROM _efd_0210_b _b ',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=_b.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragru1cons g1c ON g1c.Niv1_COMP=ggx.GraGru1',
  'WHERE g1c.Nivel1=' + Geral.FF0(QrProduto2GraGru1.Value),
  ';',

////////////////  fim 2019-02-02 ///////////////////////////////////////////////

  //buscar ggx dos gg1!
  '']);
  //Geral.MB_SQL(Self, QrGraGru1Cons);
  if QrGraGru1Cons.RecordCount = 0 then
  begin
    Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG 0210');
  end else
  begin
    QrGraGru1Cons.First;
    while not QrGraGru1Cons.Eof do
    begin
      GeraRegistroAtual();
      //
      QrGraGru1Cons.Next;
    end;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0220: Boolean;
const
  ITO = itoNaoAplicavel;
  //
  procedure GeraRegistroAtual();
  var
    UNID_CONV: String;
    FAT_CONV: Double;
  begin
    FBloco := '0';
    FRegistro := '0220';
    Result := False;
    //PB1.Max := 0;
    //PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    UNID_CONV     := Qr0220UNID.Value;
    FAT_CONV      := Qr0220FATOR.Value;
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'UNID_CONV', UNID_CONV) then
        (*03*) if not AC_f(ITO, 'FAT_CONV', FAT_CONV, False, True) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
      AddCampoSQL('Reg0200', FLin0200);
    finally
      FQTD_LIN_0220 :=  FQTD_LIN_0220 + 1;
      AdicionaLinha(FQTD_LIN_0);
    end;
  end;
begin
  Qr0220.First;
  while not Qr0220.Eof do
  begin
    GeraRegistroAtual();
    //
    Qr0220.Next;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0400(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_NAT, DESCR_NAT: String;
  //
  procedure InsereAtual();
  begin
    FBloco := '0';
    FRegistro := '0400';
    Result := False;
    //PB1.Max := 0;
    //PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    try
      PredefineLinha();
      FLin0400 := FLinArq;
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_NAT', COD_NAT) then
        (*03*) if not AC_x(ITO, 'DESCR_NAT', DESCR_NAT) then
        //
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      {
      AddCampoSQL('GraGruX', GraGruX);
      //
      if C_L <> QrProduto2COD_LST.Value then
      begin
        Campo := 'COD_LST';
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes', 'Empresa',
        'LinArq', 'REG', 'Campo'], [
        Integer(efdexerServicInval)(*FNumErro[10]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, Campo], True);
      end;
      NCM := Geral.SoNumero_TT(QrProduto2NCM.Value);
      if Length(NCM) < 2 then
      begin
        Campo := 'COD_NCM';
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes', 'Empresa',
        'LinArq', 'REG', 'Campo'], [
        Integer(efdexerFaltaNCM)(*FNumErro[11]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, Campo], True);
      end;
      }
    finally
      AdicionaLinha(FQTD_LIN_0);
      //
    end;
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNatOp, Dmod.MyDB, [
  'SELECT nfa.FisRegCad, frc.ide_natOp',
  'FROM nfecaba nfa',
  'LEFT JOIN fisregcad frc ON frc.Codigo=nfa.FisRegCad',
  //'WHERE nfa.DataFiscal BETWEEN "2022-01-01" AND "2022-02-28"',
  'WHERE nfa.DataFiscal  BETWEEN "' + FDataIni_TXT + '" AND "' + FDataFim_TXT + '" ',
  'GROUP BY nfa.FisRegCad',
  '']);
  QrNatOp.First;
  while not QrNatOp.Eof do
  begin
    COD_NAT   := Geral.FF0(QrNatOpFisRegCad.Value);
    DESCR_NAT := QrNatOpide_natOp.Value;
    InsereAtual();
    //
    QrNatOp.Next;
  end;
  //AdicionaBLC(QrNatOp.RecordCount, '0400');
  FQTD_LIN_0400 := FQTD_LIN_0400 + QrNatOp.RecordCount;
  //
  Result := True;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0460_A(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_OBS, TXT: String;
  //
  procedure InsereAtual();
  begin
    FBloco := '0';
    FRegistro := '0460';
    Result := False;
    //PB1.Max := 0;
    //PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    try
      PredefineLinha();
      FLin0460 := FLinArq;
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_OBS', COD_OBS) then
        (*03*) if not AC_x(ITO, 'TXT', TXT) then
        //
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      {
      AddCampoSQL('GraGruX', GraGruX);
      //
      if C_L <> QrProduto2COD_LST.Value then
      begin
        Campo := 'COD_LST';
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes', 'Empresa',
        'LinArq', 'REG', 'Campo'], [
        Integer(efdexerServicInval)(*FNumErro[10]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, Campo], True);
      end;
      NCM := Geral.SoNumero_TT(QrProduto2NCM.Value);
      if Length(NCM) < 2 then
      begin
        Campo := 'COD_NCM';
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes', 'Empresa',
        'LinArq', 'REG', 'Campo'], [
        Integer(efdexerFaltaNCM)(*FNumErro[11]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, Campo], True);
      end;
      }
    finally
      AdicionaLinha(FQTD_LIN_0);
      //
    end;
  end;
begin
  if (QrEmpresaNOMEUF.Value = 'SP') and (FSelRegC195) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCAT66SP2018, Dmod.MyDB, [
    'SELECT its.CFOP, SUM(its.AjusteVL_OUTROS) AjusteVL_OUTROS',
    'FROM efdinnnfsits its',
    'LEFT JOIN efdinnnfscab cab ON cab.Controle=its.Controle',
    'WHERE cab.NFe_FatID=' + Geral.FF0(VAR_FATID_0053),
    'AND cab.Empresa=' + FEmpresa_txt,
    'AND cab.NFe_FatNum IN (' + FCordaDe0053 + ')',
    //'AND its.EFD_II_C195=' + Geral.FF0(1),
    'AND its.AjusteVL_OUTROS > 0.00 ',
    'GROUP BY its.CFOP',
    'ORDER BY its.CFOP',
    '']);
    SetLength(FLstCAT66SP2018, QrCAT66SP2018.RecordCount + 1);
    FLstCAT66SP2018[0] := 0;
    QrCAT66SP2018.First;
    while not QrCAT66SP2018.Eof do
    begin
      FLstCAT66SP2018[QrCAT66SP2018.RecNo] := QrCAT66SP2018CFOP.Value;
      //
      COD_OBS := GeraCOD_OBS_CAT66SP2018(QrCAT66SP2018.RecNo);
      TXT := 'CFOP ' + Geral.FF0(QrCAT66SP2018CFOP.Value);
      InsereAtual();
      //
      QrCAT66SP2018.Next;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCAT66SP2018, Dmod.MyDB, [
    'SELECT its.CFOP, SUM(its.AjusteVL_OUTROS) AjusteVL_OUTROS',
    'FROM efdinnnfsits its',
    'LEFT JOIN efdinnnfscab cab ON cab.Controle=its.Controle',
    'WHERE cab.NFe_FatID=' + Geral.FF0(VAR_FATID_0053),
    'AND cab.Empresa=' + FEmpresa_txt,
    'AND cab.NFe_FatNum IN (' + FCordaDe0053 + ')',
    //'AND its.EFD_II_C195=' + Geral.FF0(1),
    'AND its.AjusteVL_OUTROS > 0.00 ',
    'GROUP BY its.CFOP',
    'ORDER BY its.CFOP',
    '']);
    //Geral.MB_Teste(QrCAT66SP2018.SQL.Text);
    SetLength(FLstCAT66SP2018, QrCAT66SP2018.RecordCount + 1);
    FLstCAT66SP2018[0] := 0;
    QrCAT66SP2018.First;
    while not QrCAT66SP2018.Eof do
    begin
      FLstCAT66SP2018[QrCAT66SP2018.RecNo] := QrCAT66SP2018CFOP.Value;
      //
      COD_OBS := GeraCOD_OBS_CAT66SP2018(QrCAT66SP2018.RecNo);
      TXT := 'CFOP ' + Geral.FF0(QrCAT66SP2018CFOP.Value);
      InsereAtual();
      //
      QrCAT66SP2018.Next;
    end;
    FQTD_LIN_0460 := FQTD_LIN_0460 + QrCAT66SP2018.RecordCount;
  end;
  //
  Result := True;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0460_B(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_OBS, TXT: String;
  //
  procedure InsereAtual();
  begin
    FBloco := '0';
    FRegistro := '0460';
    Result := False;
    //PB1.Max := 0;
    //PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    try
      PredefineLinha();
      FLin0460 := FLinArq;
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_OBS', COD_OBS) then
        (*03*) if not AC_x(ITO, 'TXT', TXT) then
        //
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
    finally
      AdicionaLinha(FQTD_LIN_0);
      //
    end;
  end;
begin
  if (QrEmpresaNOMEUF.Value = 'SP') and (FSelRegC195) then
  begin
(*
    UnDmkDAC_PF.AbreMySQLQuery0(QrCAT66SP2018, Dmod.MyDB, [
    'SELECT its.CFOP, SUM(its.AjusteVL_OUTROS) AjusteVL_OUTROS',
    'FROM efdinnnfsits its',
    'LEFT JOIN efdinnnfscab cab ON cab.Controle=its.Controle',
    'WHERE cab.NFe_FatID=' + Geral.FF0(VAR_FATID_0053),
    'AND cab.Empresa=' + FEmpresa_txt,
    'AND cab.NFe_FatNum IN (' + FCordaDe0053 + ')',
    //'AND its.EFD_II_C195=' + Geral.FF0(1),
    'AND its.AjusteVL_OUTROS > 0.00 ',
    'GROUP BY its.CFOP',
    'ORDER BY its.CFOP',
    '']);
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrCAT66SP2018, DmodG.MyPID_DB, [
    'SELECT its.CFOP, SUM(its.AjusteVL_OUTROS) AjusteVL_OUTROS',
    'FROM ' + TMeuDB + '.efdinnnfsits its',
    'LEFT JOIN ' + TMeuDB + '.efdinnnfscab cab ON cab.Controle=its.Controle',
    'WHERE cab.NFe_FatID=' + Geral.FF0(VAR_FATID_0053),
    'AND cab.Empresa=' + FEmpresa_txt,
    'AND cab.NFe_FatNum IN (' + FCordaDe0053 + ')',
    //'AND its.EFD_II_C195=' + Geral.FF0(1),
    'AND its.AjusteVL_OUTROS > 0.00 ',
    'GROUP BY its.CFOP',
    ' ',
    'UNION ',
    ' ',
    'SELECT CFOP, VL_NT AjusteVL_OUTROS ',
    'FROM ' + TMeuDB + '.efdinnctscab  ',
    //'WHERE DT_A_P BETWEEN "2022-02-01" AND "2022-02-28" ',
    'WHERE ' + FDtFiscalFrete_Entr + ' BETWEEN "' + FDataIni_TXT + '" AND "' + FDataFim_TXT + '"',
    'AND VL_NT > 0.00 ',
    'GROUP BY CFOP; ',
    ' ',
    'SELECT * FROM _CAT66SP2018_  ',
    '']);
    SetLength(FLstCAT66SP2018, QrCAT66SP2018.RecordCount + 1);
    FLstCAT66SP2018[0] := 0;
    QrCAT66SP2018.First;
    while not QrCAT66SP2018.Eof do
    begin
      FLstCAT66SP2018[QrCAT66SP2018.RecNo] := QrCAT66SP2018CFOP.Value;
      //
      COD_OBS := GeraCOD_OBS_CAT66SP2018(QrCAT66SP2018.RecNo);
      TXT := 'CFOP ' + Geral.FF0(QrCAT66SP2018CFOP.Value);
      InsereAtual();
      //
      QrCAT66SP2018.Next;
    end;
    //
    FQTD_LIN_0460 := FQTD_LIN_0460 + QrCAT66SP2018.RecordCount;
  end;
  //
  Result := True;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_0990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := '0';
  FRegistro := '0990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_0', FQTD_LIN_0 + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_0);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_1001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := '1';
  FRegistro := '1001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_1);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_1010(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  IND_EXP, IND_CCRF, IND_COMB, IND_USINA, IND_VA, IND_EE, IND_CART, IND_FORM,
  IND_AER, IND_GIAF1, IND_GIAF3, IND_GIAF4, IND_REST_RESSARC_COMPL_ICMS: String;
begin
  FBloco := '1';
  FRegistro := '1010';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  IND_EXP                     := QrEFD_1010IND_EXP.Value;
  IND_CCRF                    := QrEFD_1010IND_CCRF.Value;
  IND_COMB                    := QrEFD_1010IND_COMB.Value;
  IND_USINA                   := QrEFD_1010IND_USINA.Value;
  IND_VA                      := QrEFD_1010IND_VA.Value;
  IND_EE                      := QrEFD_1010IND_EE.Value;
  IND_CART                    := QrEFD_1010IND_CART.Value;
  IND_FORM                    := QrEFD_1010IND_FORM.Value;
  IND_AER                     := QrEFD_1010IND_AER.Value;
  IND_GIAF1                   := QrEFD_1010IND_GIAF1.Value;
  IND_GIAF3                   := QrEFD_1010IND_GIAF3.Value;
  IND_GIAF4                   := QrEFD_1010IND_GIAF4.Value;
  IND_REST_RESSARC_COMPL_ICMS := QrEFD_1010IND_REST_RESSARC_COMPL_ICMS.Value;

  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_EXP',   IND_EXP) then
      (*03*) if not AC_x(ITO, 'IND_CCRF',  IND_CCRF) then
      (*04*) if not AC_x(ITO, 'IND_COMB',  IND_COMB) then
      (*05*) if not AC_x(ITO, 'IND_USINA', IND_USINA) then
      (*06*) if not AC_x(ITO, 'IND_VA',    IND_VA) then
      (*07*) if not AC_x(ITO, 'IND_EE',    IND_EE) then
      (*08*) if not AC_x(ITO, 'IND_CART',  IND_CART) then
      (*09*) if not AC_x(ITO, 'IND_FORM',  IND_FORM) then
      (*10*) if not AC_x(ITO, 'IND_AER',   IND_AER) then
      (*11*) if not AC_x(ITO, 'IND_GIAF1', IND_GIAF1) then
      (*12*) if not AC_x(ITO, 'IND_GIAF3', IND_GIAF3) then
      (*13*) if not AC_x(ITO, 'IND_GIAF4', IND_GIAF4) then
      (*14*) if not AC_x(ITO, 'IND_REST_RESSARC_COMPL_ICMS', IND_REST_RESSARC_COMPL_ICMS) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    MeErros.Lines.Add('ATEN��O!! Registro 1010 informado incompleto!!!');
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_1);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_1100(): Boolean;
begin
//
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_1990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := '1';
  FRegistro := '1990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_1', FQTD_LIN_1 + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_1);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_9001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := '9';
  FRegistro := '9001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_9);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_9900(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  I: Integer;
begin
  FBloco := '9';
  FRegistro := '9900';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  AdicionaBLC(FREG_BLC_Count + 3); // 9900, 9990 e 9999
  AdicionaBLC(1, '9990');
  AdicionaBLC(1, '9999');
  for I := 0 to FREG_BLC_Count -1 do
  begin
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'REG_BLC', FREG_BLC_Fields[I]) then
        (*03*) if not AC_i(ITO, 'QTD_REG_BLC', FREG_BLC_Linhas[I], False, True) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_9);
    end;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_9990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := '9';
  FRegistro := '9990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_9', FQTD_LIN_9 + 2, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_9);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_9999(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  Qtd_Lin: Integer;
begin
  FBloco := '9';
  FRegistro := '9999';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  Qtd_Lin := FQTD_LIN_0 + FQTD_LIN_B + FQTD_LIN_C + FQTD_LIN_D +  + FQTD_LIN_E + FQTD_LIN_G +
  FQTD_LIN_H + FQTD_LIN_K + FQTD_LIN_1 + FQTD_LIN_9;
  //
  if (Qtd_Lin <> FArqStr.Count) or (Qtd_lin <> MeGerado.Lines.Count)  then
  begin
    Geral.MB_ERRO('Contagem da quantidade de linhas no arquivo n�o confere!'
    + sLineBreak + 'AVISE A DERMATEK!');
    Exit;
  end;
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN', Qtd_Lin + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    Result := True;
  finally
    AdicionaLinha(Qtd_lin);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_B001(
  IND_DAD: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'B';
  FRegistro := 'B001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_DAD', Geral.FF0(IND_DAD)) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_B);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_B990: Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'B';
  FRegistro := 'B990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_B', FQTD_LIN_B + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_B);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'C';
  FRegistro := 'C001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C100(): Boolean;
const
  ModelosNF: array[00..03] of String = ('01', '55', '1B', '04');
var
  ITO: TIndicadorDoTipoDeOperacao;
  //
  CodEmit, CodDest, (*OutInn,*) tpNF,
  IND_OPER, IND_EMIT, COD_PART, COD_MOD, SER, CHV_NFE, IND_PGTO, IND_FRT: String;
  N, COD_SIT, NUM_DOC: Integer;
  DT_DOC, DT_E_S: TDateTime;
  EhE: Boolean;
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST: Double;
begin
  FBloco := 'C';
  FRegistro := 'C100';
  Result := False;
(* Configurado antes da chamada da procedure
  PB1.Max := 0;
  PB1.Position := 0;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
(*
  CodEmit := Geral.FF0(QrCabA_CCodInfoEmit.Value);
  CodDest := Geral.FF0(QrCabA_CCodInfoEmit.Value);
  COD_PART := MLAGeral.EscolhaDe2Str(EhE, CodDest, CodEmit);
*)
  COD_PART := ObtemCOD_PART(QrCabA_CCodInfoEmit.Value);
  //
(*
  EhE := QrCabA_CCodInfoEmit.Value = FEmpresa_Int;
  tpNF := Geral.FF0(QrCabA_Cide_tpNF.Value);
  IND_OPER := MLAGeral.EscolhaDe2Str(EhE, tpNF, '0');
  if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
*)
  ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
    //
  IND_EMIT := dmkPF.EscolhaDe2Str(EhE, '0', '1');
  COD_MOD := FormatFloat('00', QrCabA_Cide_mod.Value);
  if (COD_MOD <> '01') and (COD_MOD <> '55') then
    COD_MOD := QrCabA_CCOD_MOD.Value;
  if QrCabA_CNFG_Serie.Value <> '' then
    SER := QrCabA_CNFG_Serie.Value
  else SER := Geral.FF0(QrCabA_Cide_serie.Value );
  NUM_DOC := QrCabA_Cide_nNF.Value;
  COD_SIT := QrCabA_CCOD_SIT.Value;
  CHV_NFE := QrCabA_CID.Value;
  DT_DOC := QrCabA_Cide_dEmi.Value;
  if QrCabA_Cide_dSaiEnt.Value < QrCabA_Cide_dEmi.Value then
  begin
    AtualizaCampoNFeCabA('ide_dSaiEnt', QrCabA_Cide_dEmi.Value,
    QrCabA_CFatID.Value, QrCabA_CFatNum.Value, QrCabA_CEmpresa.Value);
    //DT_E_S := QrCabA_Cide_dEmi.Value
  end else
  begin
    //DT_E_S := QrCabA_Cide_dEmi.Value;
  end;
  DT_E_S := QrCabA_CDataFiscal.Value;
  //
  VL_DOC := QrCabA_CICMSTot_vNF.Value;
  IND_PGTO := SPED_Geral.Obtem_SPED_IND_PGTO_de_NFe_indPag(QrCabA_Cide_indPag.Value);
  VL_DESC := QrCabA_CICMSTot_vDesc.Value;
  VL_ABAT_NT := QrCabA_CVL_ABAT_NT.Value;
  VL_MERC := QrCabA_CICMSTot_vProd.Value;
  IND_FRT := SPED_Geral.Obtem_SPED_IND_FRT_de_NFe_modFrete(QrCabA_CmodFrete.Value);
  VL_FRT := QrCabA_CICMSTot_vFrete.Value;
  VL_SEG := QrCabA_CICMSTot_vSeg.Value;
  VL_OUT_DA := QrCabA_CICMSTot_vOutro.Value;
  VL_BC_ICMS := QrCabA_CICMSTot_vBC.Value;
  VL_ICMS := QrCabA_CICMSTot_vICMS.Value;
  VL_BC_ICMS_ST := QrCabA_CICMSTot_vBCST.Value;
  VL_ICMS_ST := QrCabA_CICMSTot_vST.Value;
  VL_IPI := QrCabA_CICMSTot_vIPI.Value;
  VL_PIS := QrCabA_CICMSTot_vPIS.Value;
  VL_COFINS := QrCabA_CICMSTot_vCOFINS.Value;
  VL_PIS_ST := QrCabA_CRetTrib_vRetPIS.Value;
  VL_COFINS_ST := QrCabA_CRetTrib_vRetCOFINS.Value;
  //
  try
    PredefineLinha();
    FLinC100 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_OPER', IND_OPER) then
      (*03*) if not AC_x(ITO, 'IND_EMIT', IND_EMIT) then
      (*04*) if not AC_x(ITO, 'COD_PART', COD_PART) then
      (*05*) if not AC_x(ITO, 'COD_MOD', COD_MOD) then
      (*06*) if not AC_i(ITO, 'COD_SIT', COD_SIT, True, True) then
      (*07*) if not AC_x(ITO, 'SER', SER) then
      (*08*) if not AC_i(ITO, 'NUM_DOC', NUM_DOC, False, True) then
      (*09*) if not AC_n(ITO, 'CHV_NFE', CHV_NFE) then
      (*10*) if not AC_d(ITO, 'DT_DOC', DT_DOC, False, False, 23) then
      (*11*) if not AC_d(ITO, 'DT_E_S', DT_E_S, True, False, 23) then
      (*12*) if not AC_f(ITO, 'VL_DOC', VL_DOC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*13*) if not AC_x(ITO, 'IND_PGTO', IND_PGTO) then
      (*14*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*15*) if not AC_f(ITO, 'VL_ABAT_NT', VL_ABAT_NT, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*16*) if not AC_f(ITO, 'VL_MERC', VL_MERC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*17*) if not AC_x(ITO, 'IND_FRT', IND_FRT) then
      (*18*) if not AC_f(ITO, 'VL_FRT', VL_FRT, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*19*) if not AC_f(ITO, 'VL_SEG', VL_SEG, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*20*) if not AC_f(ITO, 'VL_OUT_DA', VL_OUT_DA, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*21*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*22*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*23*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*24*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*25*) if not AC_f(ITO, 'VL_IPI', VL_IPI, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*26*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*27*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*28*) if not AC_f(ITO, 'VL_PIS_ST', VL_PIS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*29*) if not AC_f(ITO, 'VL_COFINS_ST', VL_COFINS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'FatID';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrCabA_CFatID.Value;
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'FatNum';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrCabA_CFatNum.Value;;
    //
    if dmkPF.IndexOfArrStr(COD_MOD, ModelosNF) = -1 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerModFiscInval)(*FNumErro[14]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'COD_MOD'], True);
    end;
    //
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
  // Parei Aqui!!
{ TODO : notas denegadas e inutilizadas! }
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C100_Emit(): Boolean;
const
  ModelosNF: array[00..03] of String = ('01', '55', '1B', '04');
var
  ITO: TIndicadorDoTipoDeOperacao;
  //
  CodEmit, CodDest, (*OutInn,*) tpNF,
  IND_OPER, IND_EMIT, COD_PART, COD_MOD, SER, CHV_NFE, IND_PGTO, IND_FRT: String;
  N, COD_SIT, NUM_DOC: Integer;
  DT_DOC, DT_E_S: TDateTime;
  EhE: Boolean;
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST: Double;
begin
  FBloco := 'C';
  FRegistro := 'C100';
  Result := False;
(* Configurado antes da chamada da procedure
  PB1.Max := 0;
  PB1.Position := 0;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
(*
  CodEmit := Geral.FF0(QrCabA_CCodInfoEmit.Value);
  CodDest := Geral.FF0(QrCabA_CCodInfoEmit.Value);
  COD_PART := MLAGeral.EscolhaDe2Str(EhE, CodDest, CodEmit);
*)
  COD_PART := ObtemCOD_PART(QrCabA_CCodInfoEmit.Value);
  //
(*
  EhE := QrCabA_CCodInfoEmit.Value = FEmpresa_Int;
  tpNF := Geral.FF0(QrCabA_Cide_tpNF.Value);
  IND_OPER := MLAGeral.EscolhaDe2Str(EhE, tpNF, '0');
  if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
*)
  ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
    //
  IND_EMIT := dmkPF.EscolhaDe2Str(EhE, '0', '1');
  COD_MOD := FormatFloat('00', QrCabA_Cide_mod.Value);
  if (COD_MOD <> '01') and (COD_MOD <> '55') then
    COD_MOD := QrCabA_CCOD_MOD.Value;
  if QrCabA_CNFG_Serie.Value <> '' then
    SER := QrCabA_CNFG_Serie.Value
  else SER := Geral.FF0(QrCabA_Cide_serie.Value );
  NUM_DOC := QrCabA_Cide_nNF.Value;
  COD_SIT := QrCabA_CCOD_SIT.Value;
  CHV_NFE := QrCabA_CID.Value;
  DT_DOC := QrCabA_Cide_dEmi.Value;
  if QrCabA_Cide_dSaiEnt.Value < QrCabA_Cide_dEmi.Value then
  begin
    AtualizaCampoNFeCabA('ide_dSaiEnt', QrCabA_Cide_dEmi.Value,
    QrCabA_CFatID.Value, QrCabA_CFatNum.Value, QrCabA_CEmpresa.Value);
    //DT_E_S := QrCabA_Cide_dEmi.Value
  end else
  begin
    //DT_E_S := QrCabA_Cide_dEmi.Value;
  end;
  DT_E_S := QrCabA_CDataFiscal.Value;
  //
  VL_DOC := QrCabA_CICMSTot_vNF.Value;
  IND_PGTO := SPED_Geral.Obtem_SPED_IND_PGTO_de_NFe_indPag(QrCabA_Cide_indPag.Value);
  VL_DESC := QrCabA_CICMSTot_vDesc.Value;
  VL_ABAT_NT := QrCabA_CVL_ABAT_NT.Value;
  VL_MERC := QrCabA_CICMSTot_vProd.Value;
  IND_FRT := SPED_Geral.Obtem_SPED_IND_FRT_de_NFe_modFrete(QrCabA_CmodFrete.Value);
  VL_FRT := QrCabA_CICMSTot_vFrete.Value;
  VL_SEG := QrCabA_CICMSTot_vSeg.Value;
  VL_OUT_DA := QrCabA_CICMSTot_vOutro.Value;
  VL_BC_ICMS := QrCabA_CICMSTot_vBC.Value;
  VL_ICMS := QrCabA_CICMSTot_vICMS.Value;
  VL_BC_ICMS_ST := QrCabA_CICMSTot_vBCST.Value;
  VL_ICMS_ST := QrCabA_CICMSTot_vST.Value;
  VL_IPI := QrCabA_CICMSTot_vIPI.Value;
  VL_PIS := QrCabA_CICMSTot_vPIS.Value;
  VL_COFINS := QrCabA_CICMSTot_vCOFINS.Value;
  VL_PIS_ST := QrCabA_CRetTrib_vRetPIS.Value;
  VL_COFINS_ST := QrCabA_CRetTrib_vRetCOFINS.Value;
  //
  try
    PredefineLinha();
    FLinC100 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_OPER', IND_OPER) then
      (*03*) if not AC_x(ITO, 'IND_EMIT', IND_EMIT) then
      (*04*) if not AC_x(ITO, 'COD_PART', COD_PART) then
      (*05*) if not AC_x(ITO, 'COD_MOD', COD_MOD) then
      (*06*) if not AC_i(ITO, 'COD_SIT', COD_SIT, True, True) then
      (*07*) if not AC_x(ITO, 'SER', SER) then
      (*08*) if not AC_i(ITO, 'NUM_DOC', NUM_DOC, False, True) then
      (*09*) if not AC_n(ITO, 'CHV_NFE', CHV_NFE) then
      (*10*) if not AC_d(ITO, 'DT_DOC', DT_DOC, False, False, 23) then
      (*11*) if not AC_d(ITO, 'DT_E_S', DT_E_S, True, False, 23) then
      (*12*) if not AC_f(ITO, 'VL_DOC', VL_DOC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*13*) if not AC_x(ITO, 'IND_PGTO', IND_PGTO) then
      (*14*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*15*) if not AC_f(ITO, 'VL_ABAT_NT', VL_ABAT_NT, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*16*) if not AC_f(ITO, 'VL_MERC', VL_MERC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*17*) if not AC_x(ITO, 'IND_FRT', IND_FRT) then
      (*18*) if not AC_f(ITO, 'VL_FRT', VL_FRT, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*19*) if not AC_f(ITO, 'VL_SEG', VL_SEG, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*20*) if not AC_f(ITO, 'VL_OUT_DA', VL_OUT_DA, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*21*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*22*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*23*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*24*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*25*) if not AC_f(ITO, 'VL_IPI', VL_IPI, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*26*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*27*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*28*) if not AC_f(ITO, 'VL_PIS_ST', VL_PIS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*29*) if not AC_f(ITO, 'VL_COFINS_ST', VL_COFINS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'FatID';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrCabA_CFatID.Value;
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'FatNum';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrCabA_CFatNum.Value;;
    //
    if dmkPF.IndexOfArrStr(COD_MOD, ModelosNF) = -1 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerModFiscInval)(*FNumErro[14]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'COD_MOD'], True);
    end;
    //
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
  // Parei Aqui!!
{ TODO : notas denegadas e inutilizadas! }
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C100_Entr(): Boolean;
const
  ModelosNF: array[00..03] of String = ('01', '55', '1B', '04');
var
  ITO: TIndicadorDoTipoDeOperacao;
  //
  CodEmit, CodDest, (*OutInn,*) tpNF,
  IND_OPER, IND_EMIT, COD_PART, COD_MOD, SER, CHV_NFE, IND_PGTO, IND_FRT: String;
  N, COD_SIT, NUM_DOC: Integer;
  DT_DOC, DT_E_S: TDateTime;
  EhE: Boolean;
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST: Double;
begin
  FBloco := 'C';
  FRegistro := 'C100';
  Result := False;
(* Configurado antes da chamada da procedure
  PB1.Max := 0;
  PB1.Position := 0;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnNFsCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM efdinnnfscab',
  'WHERE MovFatID=' + Geral.FF0(QrCabA_CAtrelaFatID.Value),
  'AND MovFatNum=' + Geral.FF0(QrCabA_CAtrelaFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrCabA_CEmpresa.Value),
  'AND CHV_NFE="' + QrCabA_CId.Value + '"',
  'ORDER BY Controle DESC ',
  '']);
  //Geral.MB_Teste(QrEfdInnNFsCab.SQL.Text); Desmarcar se necess�rio
  if QrEfdInnNFsCab.RecordCount = 0 then
  begin
    MeErros.Lines.Add(
    'NFe entrada uso e consumo: Lan�amento n�o encontrado: MovFatNum=' +
    Geral.FF0(QrCabA_CAtrelaFatNum.Value) + ' NFe ' +
    Geral.FF0(QrCabA_Cide_nNF.Value) + ' de ' +
    Geral.FDT(QrCabA_Cide_dEmi.Value, 2));
    Exit;
  end else
  if QrEfdInnNFsCab.RecordCount > 1 then
  begin
    MeErros.Lines.Add(
    'NFe entrada uso e consumo: Mais de uma entrada para o MovFatNum=' +
    Geral.FF0(QrCabA_CAtrelaFatNum.Value) + '. Considerado o �ltimo, Controle:' +
    Geral.FF0(QrEfdInnNFsCabControle.Value));
  end;


  //COD_PART := ObtemCOD_PART(QrCabA_CCodInfoEmit.Value);
  COD_PART := ObtemCOD_PART(QrEfdInnNFsCabTerceiro.Value);
  //
  //ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  ObtemITO_InNF(EhE, IND_OPER, tpNF, ITO);
  //
  IND_EMIT := '1'; //dmkPF.EscolhaDe2Str(EhE, '0', '1');
  //COD_MOD := FormatFloat('00', QrCabA_Cide_mod.Value);
  COD_MOD := Geral.FFN(QrEfdInnNFsCabCOD_MOD.Value, 2);
  (*if (COD_MOD <> '01') and (COD_MOD <> '55') then
    COD_MOD := QrCabA_CCOD_MOD.Value;
  if QrCabA_CNFG_Serie.Value <> '' then
    SER := QrCabA_CNFG_Serie.Value
  else SER := Geral.FF0(QrCabA_Cide_serie.Value );*)
  SER := Geral.FF0(QrEfdInnNFsCabSER.Value);
  //NUM_DOC := QrCabA_Cide_nNF.Value;
  NUM_DOC := QrEfdInnNFsCabNUM_DOC.Value;
  //COD_SIT := QrCabA_CCOD_SIT.Value;
  COD_SIT := QrEfdInnNFsCabCOD_SIT.Value;
  //CHV_NFE := QrCabA_CID.Value;
  CHV_NFE := QrEfdInnNFsCabCHV_NFE.Value;
  //DT_DOC := QrCabA_Cide_dEmi.Value;
  DT_DOC := QrEfdInnNFsCabDT_DOC.Value;
  (*if QrCabA_Cide_dSaiEnt.Value < QrCabA_Cide_dEmi.Value then
  begin
    AtualizaCampoNFeCabA('ide_dSaiEnt', QrCabA_Cide_dEmi.Value,
    QrCabA_CFatID.Value, QrCabA_CFatNum.Value, QrCabA_CEmpresa.Value);
  end;*)
  DT_E_S := QrEfdInnNFsCabDT_E_S.Value;
  //
  //VL_DOC := QrCabA_CICMSTot_vNF.Value;
  VL_DOC := QrEfdInnNFsCabVL_DOC.Value;
  //IND_PGTO := SPED_Geral.Obtem_SPED_IND_PGTO_de_NFe_indPag(QrCabA_Cide_indPag.Value);
  IND_PGTO := QrEfdInnNFsCabIND_PGTO.Value;
  //VL_DESC := QrCabA_CICMSTot_vDesc.Value;
  VL_DESC := QrEfdInnNFsCabVL_DESC.Value;
  //VL_ABAT_NT := QrCabA_CVL_ABAT_NT.Value;
  VL_ABAT_NT    := QrEfdInnNFsCabVL_ABAT_NT.Value;
  VL_MERC       := QrEfdInnNFsCabVL_MERC.Value;
  IND_FRT       := SPED_Geral.Obtem_SPED_IND_FRT_de_NFe_modFrete(QrCabA_CmodFrete.Value);
  VL_FRT        := QrEfdInnNFsCabVL_FRT.Value;
  VL_SEG        := QrEfdInnNFsCabVL_SEG.Value;
  VL_OUT_DA     := QrEfdInnNFsCabVL_OUT_DA.Value;
  VL_BC_ICMS    := QrEfdInnNFsCabVL_BC_ICMS.Value;
  VL_ICMS       := QrEfdInnNFsCabVL_ICMS.Value;
  VL_BC_ICMS_ST := QrEfdInnNFsCabVL_BC_ICMS_ST.Value;
  VL_ICMS_ST    := QrEfdInnNFsCabVL_ICMS_ST.Value;
  VL_IPI        := QrEfdInnNFsCabVL_IPI.Value;
  VL_PIS        := QrEfdInnNFsCabVL_PIS.Value;
  VL_COFINS     := QrEfdInnNFsCabVL_COFINS.Value;
  VL_PIS_ST     := QrEfdInnNFsCabVL_PIS_ST.Value;
  VL_COFINS_ST  := QrEfdInnNFsCabVL_COFINS_ST.Value;
  //
(*
Campo 16 (VL_MERC) - Valida��o: se o campo COD_MOD for diferente de �55�, campo IND_EMIT for diferente de �0�
e o campo COD_SIT for igual a �00� ou �01�, o valor informado no campo deve ser igual � soma do campo VL_ITEM dos
registros C170 (�filhos� deste registro C100).
*)
  VL_MERC := VL_MERC + VL_FRT + VL_SEG + VL_OUT_DA - VL_DESC;
  try
    PredefineLinha();
    FLinC100 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_OPER', IND_OPER) then
      (*03*) if not AC_x(ITO, 'IND_EMIT', IND_EMIT) then
      (*04*) if not AC_x(ITO, 'COD_PART', COD_PART) then
      (*05*) if not AC_x(ITO, 'COD_MOD', COD_MOD) then
      (*06*) if not AC_i(ITO, 'COD_SIT', COD_SIT, True, True) then
      (*07*) if not AC_x(ITO, 'SER', SER) then
      (*08*) if not AC_i(ITO, 'NUM_DOC', NUM_DOC, False, True) then
      (*09*) if not AC_n(ITO, 'CHV_NFE', CHV_NFE) then
      (*10*) if not AC_d(ITO, 'DT_DOC', DT_DOC, False, False, 23) then
      (*11*) if not AC_d(ITO, 'DT_E_S', DT_E_S, True, False, 23) then
      (*12*) if not AC_f(ITO, 'VL_DOC', VL_DOC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*13*) if not AC_x(ITO, 'IND_PGTO', IND_PGTO) then
      (*14*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*15*) if not AC_f(ITO, 'VL_ABAT_NT', VL_ABAT_NT, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*16*) if not AC_f(ITO, 'VL_MERC', VL_MERC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*17*) if not AC_x(ITO, 'IND_FRT', IND_FRT) then
      (*18*) if not AC_f(ITO, 'VL_FRT', VL_FRT, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*19*) if not AC_f(ITO, 'VL_SEG', VL_SEG, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*20*) if not AC_f(ITO, 'VL_OUT_DA', VL_OUT_DA, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*21*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*22*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*23*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*24*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*25*) if not AC_f(ITO, 'VL_IPI', VL_IPI, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*26*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*27*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*28*) if not AC_f(ITO, 'VL_PIS_ST', VL_PIS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*29*) if not AC_f(ITO, 'VL_COFINS_ST', VL_COFINS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'FatID';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrCabA_CFatID.Value;
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'FatNum';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrCabA_CFatNum.Value;;
    //
    if dmkPF.IndexOfArrStr(COD_MOD, ModelosNF) = -1 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerModFiscInval)(*FNumErro[14]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'COD_MOD'], True);
    end;
    //
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
  // Parei Aqui!!
end;

{
function TFmEfdIcmsIpiExporta.GeraRegistro_C170_Old(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, IND_OPER: String;
  //
  N, NUM_ITEM, CST_ICMS, CFOP: Integer;
  COD_ITEM, DESCR_COMPL, UNID, IND_MOV, COD_NAT, IND_APUR, CST_IPI, COD_ENQ,
  CST_PIS, CST_COFINS, COD_CTA: String;
  QTD, VL_ITEM, VL_DESC, VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST, ALIQ_ST,
  VL_ICMS_ST, VL_BC_IPI, ALIQ_IPI, VL_IPI, VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
  VL_PIS, ALIQ_PIS_r, VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS, ALIQ_COFINS_r,
  VL_COFINS: Double;
  GrupoCFOP: Byte;
  TemErro: Boolean;
begin
  //Result := True; EXIT;
  //
  FBloco := 'C';
  FRegistro := 'C170';
  Result := False;
(* Configurado para a NF e n�o para o itens
  PB1.Max := 0;
  PB1.Position := 0;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
(*
  EhE := QrCabA_CCodInfoEmit.Value = FEmpresa_Int;
  tpNF := Geral.FF0(QrCabA_Cide_tpNF.Value);
  IND_OPER := MLAGeral.EscolhaDe2Str(EhE, tpNF, '0');
  if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
*)
  ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);

  // Parei aqui! � isso mesmo?
  (*
  QrProd.Close;
  QrProd.Params[0].AsInteger := QrItsIGraGruX.Value;
  UnDmkDAC_PF.AbreQuery(QrProd, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  COD_CTA := DmProd.ObtemCTB(
        QrProdNivel2.Value, QrProdNivel3.Value, QrProdGenero.Value);
  *)
  COD_CTA := DmProd.ObtemCTB_peloGGX(QrItsIGraGruX.Value);
  //
  NUM_ITEM := QrItsInItem.Value;
  COD_ITEM := ObtemCOD_ITEM(QrItsIGraGruX.Value);
  DESCR_COMPL := QrItsIprod_xProd.Value;
  QTD := QrItsIprod_qTrib.Value;
  //
  UNID := QrItsIprod_uTrib.Value;
  if Trim(UNID) = ''  then
  begin
    UNID := DmProd.QrProdSigla.Value;
  end;
  //
  VL_ITEM := QrItsIprod_vProd.Value;
  VL_DESC := QrItsIprod_vDesc.Value;
  // Baixa estoque
  QrStqMovIts.Close;
  QrStqMovIts.Params[00].AsInteger := QrItsIFatID.Value;
  QrStqMovIts.Params[01].AsInteger := QrItsIFatNum.Value;
  QrStqMovIts.Params[02].AsInteger := QrItsIEmpresa.Value;
  QrStqMovIts.Params[03].AsInteger := QrItsIGraGruX.Value;
  QrStqMovIts.Params[04].AsInteger := QrItsIFatID.Value;
  QrStqMovIts.Params[05].AsInteger := QrItsIFatNum.Value;
  QrStqMovIts.Params[06].AsInteger := QrItsIEmpresa.Value;
  QrStqMovIts.Params[07].AsInteger := QrItsIGraGruX.Value;
  UnDmkDAC_PF.AbreQuery(QrStqMovIts, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  if QrStqMovIts.RecordCount > 0 then
  begin
    if QrStqMovItsBaixa.Value = 0 then
      IND_MOV := '1'
    else
      IND_MOV := '0';
  end else
    IND_MOV := '0';

  // ICMS
  QrItsN.Close;
  QrItsN.Params[00].AsInteger := QrItsIFatID.Value;
  QrItsN.Params[01].AsInteger := QrItsIFatNum.Value;
  QrItsN.Params[02].AsInteger := QrItsIEmpresa.Value;
  QrItsN.Params[03].AsInteger := QrItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrItsN, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  CST_ICMS := (QrItsNICMS_Orig.Value * 100) + QrItsNICMS_CST.Value;
  CFOP := QrItsIprod_CFOP.Value;
  // Teste
  if (CFOP div 1000) in ([5,6]) then
  begin
    CFOP := CFOP - 4000;
    Geral.MB_Aviso('Teste: CFOP = ' + Geral.FF0(CFOP));
  end;

  // Fim Teste!
  COD_NAT := QrItsNCOD_NAT.Value;
  (*
  VL_BC_ICMS := QrItsNICMS_vBC.Value;
  ALIQ_ICMS := QrItsNICMS_pICMS.Value;
  VL_ICMS  := QrItsNICMS_vICMS.Value;
  VL_BC_ICMS_ST := QrItsNICMS_vBCST.Value;
  ALIQ_ST := QrItsNICMS_pICMSST.Value;
  VL_ICMS_ST := QrItsNICMS_vICMSST.Value;
  *)
  VL_BC_ICMS := QrItsNICMS_vBC.Value;
  ALIQ_ICMS := QrItsNICMS_pICMS.Value;
  VL_ICMS  := QrItsNICMS_vICMS.Value;
  VL_BC_ICMS_ST := QrItsNICMS_vBCST.Value;
  ALIQ_ST := QrItsNICMS_pICMSST.Value;
  VL_ICMS_ST := QrItsNICMS_vICMSST.Value;
  // IPI
  //if not Contribuinte do IPI then
  // IPI
  QrItsO.Close;
  QrItsO.Params[00].AsInteger := QrItsIFatID.Value;
  QrItsO.Params[01].AsInteger := QrItsIFatNum.Value;
  QrItsO.Params[02].AsInteger := QrItsIEmpresa.Value;
  QrItsO.Params[03].AsInteger := QrItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrItsO, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  IND_APUR  := '';
  CST_IPI   := '';
  COD_ENQ   := '';
  VL_BC_IPI := 0;
  ALIQ_IPI  := 0;
  VL_IPI    := 0;
  if QrItsO.RecordCount > 0 then
  begin
    IND_APUR  := QrItsOIND_APUR.Value;
    CST_IPI   := Geral.FFF(QrItsOIPI_CST.Value, 2);
    COD_ENQ   := QrItsOIPI_cEnq.Value;
    VL_BC_IPI := QrItsOIPI_vBC.Value;
    ALIQ_IPI  := QrItsOIPI_pIPI.Value;
    VL_IPI    := QrItsOIPI_vIPI.Value;
(*
  end else begin
  IND_APUR := QrItsIIPIRec_IND_APUR.Value;
  CST_IPI := FormatFloat('00', QrItsIIPIRec_CST.Value);
  COD_ENQ := QrItsIIPIRec_cEnq.Value;
(*
  VL_BC_IPI:= QrItsOIPI_vBC.Value;
  ALIQ_IPI:= QrItsOIPI_pIPI.Value;
  VL_IPI:= QrItsOIPI_vIPI.Value;
*)
  VL_BC_IPI:= QrItsIIPIRec_vBC.Value;
  ALIQ_IPI:= QrItsIIPIRec_pAliq.Value;
  VL_IPI:= QrItsIIPIRec_vIPI.Value;
*)
  end;
  // PIS
  (*
  if QrItsQPIS_CST.Value > 0 then
  begin
    CST_PIS := FormatFloat('00', QrItsQPIS_CST.Value);
    VL_BC_PIS := QrItsPIS_vBC.Value;
    ALIQ_PIS_p := QrItsPIS_pAliq.Value;
    VL_PIS := QrItsPIS_vPIS.Value;
  end else
  *)begin
    CST_PIS    := Geral.FFF(DmProd.QrProdPIS_CST.Value, 2);
    VL_BC_PIS  := QrItsIprod_vProd.Value;
    ALIQ_PIS_p := DmProd.QrProdPIS_AlqP.Value;
    ALIQ_PIS_r := DmProd.QrProdPIS_AlqV.Value;
    ALIQ_PIS_p := 0;// ver o que fazer!!!
    VL_PIS     := Geral.RoundC(VL_BC_PIS * ALIQ_PIS_p / 100, 2);
  end;

  // COFINS
  (*
  if QrItsQCOFINS_CST.Value > 0 then
  begin
    CST_COFINS := FormatFloat('00', QrItsQCOFINS_CST.Value);
    VL_BC_COFINS := QrItsCOFINS_vBC.Value;
    ALIQ_COFINS_p := QrItsCOFINS_pAliq.Value;
    VL_COFINS := QrItsCOFINS_vCOFINS.Value;
  end else
  *)begin
    CST_COFINS    := Geral.FFF(DmProd.QrProdCOFINS_CST.Value, 2);
    VL_BC_COFINS  := QrItsIprod_vProd.Value;
    ALIQ_COFINS_p := DmProd.QrProdCOFINS_AlqP.Value;
    ALIQ_COFINS_r := DmProd.QrProdCOFINS_AlqV.Value;
    ALIQ_COFINS_p := 0;// ver o que fazer!!!
    VL_COFINS     := Geral.RoundC(VL_BC_COFINS * ALIQ_COFINS_p / 100, 2);
  end;

  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'NUM_ITEM', NUM_ITEM, False, True) then
      (*03*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*04*) if not AC_x(ITO, 'DESCR_COMPL', DESCR_COMPL) then
      (*05*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      (*06*) if not AC_x(ITO, 'UNID', UNID) then
      (*07*) if not AC_f(ITO, 'VL_ITEM', VL_ITEM, True, True) then
      (*08*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, True) then
      (*09*) if not AC_x(ITO, 'IND_MOV', IND_MOV) then
      (*10*) if not AC_i(ITO, 'CST_ICMS', CST_ICMS, True, True) then
      (*11*) if not AC_i(ITO, 'CFOP', CFOP, False, True) then
      (*12*) if not AC_x(ITO, 'COD_NAT', COD_NAT) then
      (*13*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
      (*14*) if not AC_f(ITO, 'ALIQ_ICMS', ALIQ_ICMS, True, True) then
      (*15*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*16*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, True) then
      (*17*) if not AC_f(ITO, 'ALIQ_ST', ALIQ_ST, True, True) then
      (*18*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, True) then
      (*19*) if not AC_x(ITO, 'IND_APUR', IND_APUR) then
      (*20*) if not AC_x(ITO, 'CST_IPI', CST_IPI) then
      (*21*) if not AC_x(ITO, 'COD_ENQ', COD_ENQ) then
      (*22*) if not AC_f(ITO, 'VL_BC_IPI', VL_BC_IPI, True, True) then
      (*23*) if not AC_f(ITO, 'ALIQ_IPI', ALIQ_IPI, True, True) then
      (*24*) if not AC_f(ITO, 'VL_IPI', VL_IPI, True, True) then
      (*25*) if not AC_n(ITO, 'CST_PIS', CST_PIS) then
      (*26*) if not AC_f(ITO, 'VL_BC_PIS', VL_BC_PIS, True, True) then
      (*27*) if not AC_f(ITO, 'ALIQ_PIS_p', ALIQ_PIS_p, True, True) then
      (*28*) if not AC_f(ITO, 'QUANT_BC_PIS', QUANT_BC_PIS, True, True) then
      (*29*) if not AC_f(ITO, 'ALIQ_PIS_r', ALIQ_PIS_r, True, True) then
      (*30*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, True) then
      (*31*) if not AC_n(ITO, 'CST_COFINS', CST_COFINS) then
      (*32*) if not AC_f(ITO, 'VL_BC_COFINS', VL_BC_COFINS, True, True) then
      (*33*) if not AC_f(ITO, 'ALIQ_COFINS_p', ALIQ_COFINS_p, True, True) then
      (*34*) if not AC_f(ITO, 'QUANT_BC_COFINS', QUANT_BC_COFINS, True, True) then
      (*35*) if not AC_f(ITO, 'ALIQ_COFINS_r', ALIQ_COFINS_r, True, True) then
      (*36*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, True) then
      (*37*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCFOP, DModG.AllID_DB, [
    'SELECT Codigo ',
    'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
    'WHERE Codigo="' + Geral.FF0(CFOP) + '"',
    '']);
    if QrCFOP.RecordCount = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCFOPUnkn)(*FNumErro[13]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'CFOP'], True);
    end else
    begin
      GrupoCFOP := CFOP div 1000;
      case ITO of
        itoEntrada: TemErro := not (GrupoCFOP in ([1,2,3]));
        itoSaida: TemErro := not (GrupoCFOP in ([5,6,7]));
        else TemErro := True;
      end;
      if TemErro then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes',
        'Empresa', 'LinArq', 'REG', 'Campo'], [
        Integer(efdexerCFOPInval)(*FNumErro[15]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, 'CFOP'], True);
      end;
    end;
    //AdicionaBLC(1);
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'C100';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinC100;
    //
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'GraGruX';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrItsIGraGruX.Value;
    //
    FQTD_LIN_C170 := FQTD_LIN_C170 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;
}

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C170(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, IND_OPER: String;
  //
  N, NUM_ITEM, CST_ICMS, CFOP: Integer;
  COD_ITEM, DESCR_COMPL, UNID, IND_MOV, COD_NAT, IND_APUR, CST_IPI, COD_ENQ,
  CST_PIS, CST_COFINS, COD_CTA: String;
  QTD, VL_ITEM, VL_DESC, VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST, ALIQ_ST,
  VL_ICMS_ST, VL_BC_IPI, ALIQ_IPI, VL_IPI, VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
  VL_PIS, ALIQ_PIS_r, VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS, ALIQ_COFINS_r,
  VL_COFINS: Double;
  GrupoCFOP: Byte;
  TemErro: Boolean;
begin
  //Result := True; EXIT;
  //
  FBloco := 'C';
  FRegistro := 'C170';
  Result := False;
(* Configurado para a NF e n�o para o itens
  PB1.Max := 0;
  PB1.Position := 0;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEFD_C170, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeefd_c170 ',
  'WHERE FatID=' + Geral.FF0(QrItsI_FatID.Value),
  'AND FatNum=' + Geral.FF0(QrItsI_FatNum.Value),
  'AND Empresa=' + Geral.FF0(QrItsI_Empresa.Value),
  'AND nItem=' + Geral.FF0(QrItsI_nItem.Value),
  '']);
  //
  NUM_ITEM       := QrItsI_nItem.Value;
  COD_ITEM       := QrNFeEFD_C170COD_ITEM.Value;
  DESCR_COMPL    := QrNFeEFD_C170DESCR_COMPL.Value;
  QTD            := QrNFeEFD_C170QTD.Value;
  UNID           := QrNFeEFD_C170UNID.Value;
  VL_ITEM        := QrNFeEFD_C170VL_ITEM.Value;
  VL_DESC        := QrNFeEFD_C170VL_DESC.Value;
  IND_MOV        := QrNFeEFD_C170IND_MOV.Value;
  CST_ICMS       := QrNFeEFD_C170CST_ICMS.Value;
  CFOP           := QrNFeEFD_C170CFOP.Value;
  COD_NAT        := QrNFeEFD_C170COD_NAT.Value;
  VL_BC_ICMS     := QrNFeEFD_C170VL_BC_ICMS.Value;
  ALIQ_ICMS      := QrNFeEFD_C170ALIQ_ICMS.Value;
  VL_ICMS        := QrNFeEFD_C170VL_ICMS.Value;
  VL_BC_ICMS_ST  := QrNFeEFD_C170VL_BC_ICMS_ST.Value;
  ALIQ_ST        := QrNFeEFD_C170ALIQ_ST.Value;
  VL_ICMS_ST     := QrNFeEFD_C170VL_ICMS_ST.Value;
  IND_APUR       := QrNFeEFD_C170IND_APUR.Value;
  CST_IPI        := QrNFeEFD_C170CST_IPI.Value;
  COD_ENQ        := QrNFeEFD_C170COD_ENQ.Value;
  VL_BC_IPI      := QrNFeEFD_C170VL_BC_IPI.Value;
  ALIQ_IPI       := QrNFeEFD_C170ALIQ_IPI.Value;
  VL_IPI         := QrNFeEFD_C170VL_IPI.Value;
  CST_PIS        := Geral.FF0_EmptyZero(QrNFeEFD_C170CST_PIS.Value);
  VL_BC_PIS      := QrNFeEFD_C170VL_BC_PIS.Value;
  ALIQ_PIS_p     := QrNFeEFD_C170ALIQ_PIS_p.Value;
  QUANT_BC_PIS   := QrNFeEFD_C170QUANT_BC_PIS .Value;
  ALIQ_PIS_r     := QrNFeEFD_C170ALIQ_PIS_r.Value;
  VL_PIS         := QrNFeEFD_C170VL_PIS.Value;
  CST_COFINS     := Geral.FF0_EmptyZero(QrNFeEFD_C170CST_COFINS.Value);
  VL_BC_COFINS   := QrNFeEFD_C170VL_BC_COFINS.Value;
  ALIQ_COFINS_p  := QrNFeEFD_C170ALIQ_COFINS_p.Value;
  QUANT_BC_COFINS:= QrNFeEFD_C170QUANT_BC_COFINS.Value;
  ALIQ_COFINS_r  := QrNFeEFD_C170ALIQ_COFINS_r.Value;
  VL_COFINS      := QrNFeEFD_C170VL_COFINS.Value;
  COD_CTA        := QrNFeEFD_C170COD_CTA.Value;
  //
  if (Trim(DESCR_COMPL) = '') and VAR_INF_SPED_EFD_NOME_DESCR_COMPL then
    DESCR_COMPL := QrItsI_prod_xProd.Value;
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'NUM_ITEM', NUM_ITEM, False, True, True) then
      (*03*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*04*) if not AC_x(ITO, 'DESCR_COMPL', DESCR_COMPL) then
      (*05*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      (*06*) if not AC_x(ITO, 'UNID', UNID) then
      (*07*) if not AC_f(ITO, 'VL_ITEM', VL_ITEM, True, True) then
      (*08*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, True) then
      (*09*) if not AC_x(ITO, 'IND_MOV', IND_MOV) then
      (*10*) if not AC_i(ITO, 'CST_ICMS', CST_ICMS, True, True) then
      (*11*) if not AC_i(ITO, 'CFOP', CFOP, False, True) then
      (*12*) if not AC_x(ITO, 'COD_NAT', COD_NAT) then
      (*13*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
      (*14*) if not AC_f(ITO, 'ALIQ_ICMS', ALIQ_ICMS, True, True) then
      (*15*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*16*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, True) then
      (*17*) if not AC_f(ITO, 'ALIQ_ST', ALIQ_ST, True, True) then
      (*18*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, True) then
      (*19*) if not AC_x(ITO, 'IND_APUR', IND_APUR) then
      (*20*) if not AC_x(ITO, 'CST_IPI', CST_IPI) then
      (*21*) if not AC_x(ITO, 'COD_ENQ', COD_ENQ) then
      (*22*) if not AC_f(ITO, 'VL_BC_IPI', VL_BC_IPI, True, True) then
      (*23*) if not AC_f(ITO, 'ALIQ_IPI', ALIQ_IPI, True, True) then
      (*24*) if not AC_f(ITO, 'VL_IPI', VL_IPI, True, True) then
      (*25*) if not AC_n(ITO, 'CST_PIS', CST_PIS) then
      (*26*) if not AC_f(ITO, 'VL_BC_PIS', VL_BC_PIS, True, True) then
      (*27*) if not AC_f(ITO, 'ALIQ_PIS_p', ALIQ_PIS_p, True, True) then
      (*28*) if not AC_f(ITO, 'QUANT_BC_PIS', QUANT_BC_PIS, True, True) then
      (*29*) if not AC_f(ITO, 'ALIQ_PIS_r', ALIQ_PIS_r, True, True) then
      (*30*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, True) then
      (*31*) if not AC_n(ITO, 'CST_COFINS', CST_COFINS) then
      (*32*) if not AC_f(ITO, 'VL_BC_COFINS', VL_BC_COFINS, True, True) then
      (*33*) if not AC_f(ITO, 'ALIQ_COFINS_p', ALIQ_COFINS_p, True, True) then
      (*34*) if not AC_f(ITO, 'QUANT_BC_COFINS', QUANT_BC_COFINS, True, True) then
      (*35*) if not AC_f(ITO, 'ALIQ_COFINS_r', ALIQ_COFINS_r, True, True) then
      (*36*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, True) then
      (*37*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCFOP, DModG.AllID_DB, [
    'SELECT Codigo ',
    'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
    'WHERE Codigo="' + Geral.FF0(CFOP) + '"',
    '']);
    if QrCFOP.RecordCount = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCFOPUnkn)(*FNumErro[13]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'CFOP'], True);
    end else
    begin
      GrupoCFOP := CFOP div 1000;
      case ITO of
        itoEntrada: TemErro := not (GrupoCFOP in ([1,2,3]));
        itoSaida: TemErro := not (GrupoCFOP in ([5,6,7]));
        else TemErro := True;
      end;
      if TemErro then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes',
        'Empresa', 'LinArq', 'REG', 'Campo'], [
        Integer(efdexerCFOPInval)(*FNumErro[15]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, 'CFOP'], True);
      end;
    end;
    //AdicionaBLC(1);
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'C100';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinC100;
    //
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'GraGruX';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrItsI_GraGruX.Value;
    //
    FQTD_LIN_C170 := FQTD_LIN_C170 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C170_Emit(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, IND_OPER: String;
  //
  N, NUM_ITEM, CST_ICMS, CFOP: Integer;
  COD_ITEM, DESCR_COMPL, UNID, IND_MOV, COD_NAT, IND_APUR, CST_IPI, COD_ENQ,
  CST_PIS, CST_COFINS, COD_CTA: String;
  QTD, VL_ITEM, VL_DESC, VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST, ALIQ_ST,
  VL_ICMS_ST, VL_BC_IPI, ALIQ_IPI, VL_IPI, VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
  VL_PIS, ALIQ_PIS_r, VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS, ALIQ_COFINS_r,
  VL_COFINS, VL_ABAT_NT: Double;
  GrupoCFOP: Byte;
  TemErro: Boolean;
  IndMov: Integer;
begin
  //Result := True; EXIT;
  //
  FBloco := 'C';
  FRegistro := 'C170';
  Result := False;
(* Configurado para a NF e n�o para o itens
  PB1.Max := 0;
  PB1.Position := 0;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEFD_C170, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeefd_c170 ',
  'WHERE FatID=' + Geral.FF0(QrIFatID.Value),
  'AND FatNum=' + Geral.FF0(QrIFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrIEmpresa.Value),
  'AND nItem=' + Geral.FF0(QrInItem.Value),
  '']);
*)
  //
  IndMov := DefineIND_MOV(QrCabA_CFisRegCad.Value, QrCabA_Cide_finNFe.Value, QrIprod_cProd.Value);
  if IndMov = 1 then
    FContaIND_MOV_1 := FContaIND_MOV_1 + 1;
  //
  NUM_ITEM       := QrInItem.Value;
  COD_ITEM       := QrIprod_cProd.Value;
  DESCR_COMPL    := '';//QrI.Value;
  QTD            := QrIprod_qCom.Value;
  UNID           := QrIprod_uCom.Value;
  VL_ITEM        := QrIprod_vProd.Value;
  VL_DESC        := QrIprod_vDesc.Value;
  IND_MOV        := Geral.FF0(IndMov);

  CST_ICMS       := (QrIICMS_Orig.Value * 100) + QrIICMS_CST.Value;
  CFOP           := QrIprod_CFOP.Value;
  if not FSelReg0400 then
    COD_NAT := ''
  else
    COD_NAT        := Geral.FF0(QrCabA_CFisRegCad.Value);
  VL_BC_ICMS     := QrIICMS_vBC.Value;
  ALIQ_ICMS      := QrIICMS_pICMS.Value;
  VL_ICMS        := QrIICMS_vICMS.Value;
  VL_BC_ICMS_ST  := QrIICMS_vBCST.Value;
  ALIQ_ST        := QrIICMS_pICMSST.Value;
  VL_ICMS_ST     := QrIICMS_vICMSST.Value;
  IND_APUR       := QrIIND_APUR.Value;
  CST_IPI        := Geral.FFN(QrIIPI_CST.Value, 2);
  COD_ENQ        := QrIIPI_cEnq.Value;
  VL_BC_IPI      := QrIIPI_vBC.Value;
  ALIQ_IPI       := QrIIPI_pIPI.Value;
  VL_IPI         := QrIIPI_vIPI.Value;
  CST_PIS        := Geral.FFN(QrIPIS_CST.Value, 2);
  VL_BC_PIS      := QrIPIS_vBC.Value;
  ALIQ_PIS_p     := QrIPIS_pPIS.Value;
  QUANT_BC_PIS   := 0.000;
  ALIQ_PIS_r     := 0.00;
  VL_PIS         := QrIPIS_vPIS.Value;
  CST_COFINS     := Geral.FFN(QrICOFINS_CST.Value, 2);
  VL_BC_COFINS   := QrICOFINS_vBC.Value;
  ALIQ_COFINS_p  := QrICOFINS_pCOFINS.Value;
  QUANT_BC_COFINS:= 0.000;
  ALIQ_COFINS_r  := 0.00;
  VL_COFINS      := QrICOFINS_vCOFINS.Value;
  COD_CTA        := '';
  VL_ABAT_NT     := 0.00;
  //
  if (Trim(DESCR_COMPL) = '') and VAR_INF_SPED_EFD_NOME_DESCR_COMPL then
    DESCR_COMPL := QrIprod_xProd.Value;
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'NUM_ITEM', NUM_ITEM, False, True, True) then
      (*03*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*04*) if not AC_x(ITO, 'DESCR_COMPL', DESCR_COMPL) then
      (*05*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      (*06*) if not AC_x(ITO, 'UNID', UNID) then
      (*07*) if not AC_f(ITO, 'VL_ITEM', VL_ITEM, True, True) then
      (*08*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, True) then
      (*09*) if not AC_x(ITO, 'IND_MOV', IND_MOV) then
      (*10*) if not AC_i(ITO, 'CST_ICMS', CST_ICMS, True, True) then
      (*11*) if not AC_i(ITO, 'CFOP', CFOP, False, True) then
      (*12*) if not AC_x(ITO, 'COD_NAT', COD_NAT) then
      (*13*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
      (*14*) if not AC_f(ITO, 'ALIQ_ICMS', ALIQ_ICMS, True, True) then
      (*15*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*16*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, True) then
      (*17*) if not AC_f(ITO, 'ALIQ_ST', ALIQ_ST, True, True) then
      (*18*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, True) then
      (*19*) if not AC_x(ITO, 'IND_APUR', IND_APUR) then
      (*20*) if not AC_x(ITO, 'CST_IPI', CST_IPI) then
      (*21*) if not AC_x(ITO, 'COD_ENQ', COD_ENQ) then
      (*22*) if not AC_f(ITO, 'VL_BC_IPI', VL_BC_IPI, True, True) then
      (*23*) if not AC_f(ITO, 'ALIQ_IPI', ALIQ_IPI, True, True) then
      (*24*) if not AC_f(ITO, 'VL_IPI', VL_IPI, True, True) then
      (*25*) if not AC_n(ITO, 'CST_PIS', CST_PIS) then
      (*26*) if not AC_f(ITO, 'VL_BC_PIS', VL_BC_PIS, True, True) then
      (*27*) if not AC_f(ITO, 'ALIQ_PIS_p', ALIQ_PIS_p, True, True) then
      (*28*) if not AC_f(ITO, 'QUANT_BC_PIS', QUANT_BC_PIS, True, True) then
      (*29*) if not AC_f(ITO, 'ALIQ_PIS_r', ALIQ_PIS_r, True, True) then
      (*30*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, True) then
      (*31*) if not AC_n(ITO, 'CST_COFINS', CST_COFINS) then
      (*32*) if not AC_f(ITO, 'VL_BC_COFINS', VL_BC_COFINS, True, True) then
      (*33*) if not AC_f(ITO, 'ALIQ_COFINS_p', ALIQ_COFINS_p, True, True) then
      (*34*) if not AC_f(ITO, 'QUANT_BC_COFINS', QUANT_BC_COFINS, True, True) then
      (*35*) if not AC_f(ITO, 'ALIQ_COFINS_r', ALIQ_COFINS_r, True, True) then
      (*36*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, True) then
      (*37*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
      (*38*) if not AC_f(ITO, 'VL_ABAT_NT', VL_ABAT_NT, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCFOP, DModG.AllID_DB, [
    'SELECT Codigo ',
    'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
    'WHERE Codigo="' + Geral.FF0(CFOP) + '"',
    '']);
    if QrCFOP.RecordCount = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCFOPUnkn)(*FNumErro[13]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'CFOP'], True);
    end else
    begin
      GrupoCFOP := CFOP div 1000;
      case ITO of
        itoEntrada: TemErro := not (GrupoCFOP in ([1,2,3]));
        itoSaida: TemErro := not (GrupoCFOP in ([5,6,7]));
        else TemErro := True;
      end;
      if TemErro then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes',
        'Empresa', 'LinArq', 'REG', 'Campo'], [
        Integer(efdexerCFOPInval)(*FNumErro[15]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, 'CFOP'], True);
      end;
    end;
    //AdicionaBLC(1);
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'C100';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinC100;
    //
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'GraGruX';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrIGraGruX.Value;
    //
    FQTD_LIN_C170 := FQTD_LIN_C170 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C170_Entr: Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, IND_OPER: String;
  //
  N, NUM_ITEM, CFOP: Integer;
  COD_ITEM, DESCR_COMPL, UNID, IND_MOV, COD_NAT, IND_APUR, CST_IPI, COD_ENQ,
  CST_PIS, CST_COFINS, COD_CTA, CST_ICMS: String;
  QTD, VL_ITEM, VL_DESC, VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST, ALIQ_ST,
  VL_ICMS_ST, VL_BC_IPI, ALIQ_IPI, VL_IPI, VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
  VL_PIS, ALIQ_PIS_r, VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS, ALIQ_COFINS_r,
  VL_COFINS, VL_ABAT_NT: Double;
  GrupoCFOP: Byte;
  TemErro: Boolean;
begin
  //Result := True; EXIT;
  //
  FBloco := 'C';
  FRegistro := 'C170';
  Result := False;
(* Configurado para a NF e n�o para o itens
  PB1.Max := 0;
  PB1.Position := 0;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  //ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  ObtemITO_InNF(EhE, IND_OPER, tpNF, ITO);
(*
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEFD_C170, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeefd_c170 ',
  'WHERE FatID=' + Geral.FF0(QrItsIFatID.Value),
  'AND FatNum=' + Geral.FF0(QrItsIFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrItsIEmpresa.Value),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  '']);
  //
*)
  NUM_ITEM       := QrEfdInnNFsIts.RecNo;
  COD_ITEM       := Geral.FF0(QrEfdInnNFsItsGraGruX.Value); // QrNFeEFD_C170COD_ITEM.Value;
  DESCR_COMPL    := ''; //ver o que fazer! QrNFeEFD_C170DESCR_COMPL.Value;
  QTD            := QrEfdInnNFsItsQTD            .Value; // QrNFeEFD_C170QTD.Value;
  UNID           := QrEfdInnNFsItsUNID           .Value; // QrNFeEFD_C170UNID.Value;
  VL_ITEM        := QrEfdInnNFsItsVL_ITEM        .Value; // QrNFeEFD_C170VL_ITEM.Value;
  VL_DESC        := QrEfdInnNFsItsVL_DESC        .Value; // QrNFeEFD_C170VL_DESC.Value;
  IND_MOV        := QrEfdInnNFsItsIND_MOV        .Value; // QrNFeEFD_C170IND_MOV.Value;
  CST_ICMS       := QrEfdInnNFsItsCST_ICMS       .Value; // QrNFeEFD_C170CST_ICMS.Value;
  CFOP           := QrEfdInnNFsItsCFOP           .Value; // QrNFeEFD_C170CFOP.Value;
  if not FSelReg0400 then
    COD_NAT := ''
  else
    COD_NAT        := QrEfdInnNFsItsCOD_NAT        .Value; // QrNFeEFD_C170COD_NAT.Value;
  VL_BC_ICMS     := QrEfdInnNFsItsVL_BC_ICMS     .Value; // QrNFeEFD_C170VL_BC_ICMS.Value;
  ALIQ_ICMS      := QrEfdInnNFsItsALIQ_ICMS      .Value; // QrNFeEFD_C170ALIQ_ICMS.Value;
  VL_ICMS        := QrEfdInnNFsItsVL_ICMS        .Value; // QrNFeEFD_C170VL_ICMS.Value;
  VL_BC_ICMS_ST  := QrEfdInnNFsItsVL_BC_ICMS_ST  .Value; // QrNFeEFD_C170VL_BC_ICMS_ST.Value;
  ALIQ_ST        := QrEfdInnNFsItsALIQ_ST        .Value; // QrNFeEFD_C170ALIQ_ST.Value;
  VL_ICMS_ST     := QrEfdInnNFsItsVL_ICMS_ST     .Value; // QrNFeEFD_C170VL_ICMS_ST.Value;
  IND_APUR       := QrEfdInnNFsItsIND_APUR       .Value; // QrNFeEFD_C170IND_APUR.Value;
  CST_IPI        := QrEfdInnNFsItsCST_IPI        .Value; // QrNFeEFD_C170CST_IPI.Value;
  COD_ENQ        := QrEfdInnNFsItsCOD_ENQ        .Value; // QrNFeEFD_C170COD_ENQ.Value;
  VL_BC_IPI      := QrEfdInnNFsItsVL_BC_IPI      .Value; // QrNFeEFD_C170VL_BC_IPI.Value;
  ALIQ_IPI       := QrEfdInnNFsItsALIQ_IPI       .Value; // QrNFeEFD_C170ALIQ_IPI.Value;
  VL_IPI         := QrEfdInnNFsItsVL_IPI         .Value; // QrNFeEFD_C170VL_IPI.Value;
  CST_PIS        := QrEfdInnNFsItsCST_PIS        .Value; // Geral.FF0_EmptyZero(QrNFeEFD_C170CST_PIS.Value);
  VL_BC_PIS      := QrEfdInnNFsItsVL_BC_PIS      .Value; // QrNFeEFD_C170VL_BC_PIS.Value;
  ALIQ_PIS_p     := QrEfdInnNFsItsALIQ_PIS_p     .Value; // QrNFeEFD_C170ALIQ_PIS_p.Value;
  QUANT_BC_PIS   := QrEfdInnNFsItsQUANT_BC_PIS   .Value; // QrNFeEFD_C170QUANT_BC_PIS .Value;
  ALIQ_PIS_r     := QrEfdInnNFsItsALIQ_PIS_r     .Value; // QrNFeEFD_C170ALIQ_PIS_r.Value;
  VL_PIS         := QrEfdInnNFsItsVL_PIS         .Value; // QrNFeEFD_C170VL_PIS.Value;
  CST_COFINS     := QrEfdInnNFsItsCST_COFINS     .Value; // Geral.FF0_EmptyZero(QrNFeEFD_C170CST_COFINS.Value);
  VL_BC_COFINS   := QrEfdInnNFsItsVL_BC_COFINS   .Value; // QrNFeEFD_C170VL_BC_COFINS.Value;
  ALIQ_COFINS_p  := QrEfdInnNFsItsALIQ_COFINS_p  .Value; // QrNFeEFD_C170ALIQ_COFINS_p.Value;
  QUANT_BC_COFINS:= QrEfdInnNFsItsQUANT_BC_COFINS.Value; // QrNFeEFD_C170QUANT_BC_COFINS.Value;
  ALIQ_COFINS_r  := QrEfdInnNFsItsALIQ_COFINS_r  .Value; // QrNFeEFD_C170ALIQ_COFINS_r.Value;
  VL_COFINS      := QrEfdInnNFsItsVL_COFINS      .Value; // QrNFeEFD_C170VL_COFINS.Value;
  COD_CTA        := QrEfdInnNFsItsCOD_CTA        .Value; // QrNFeEFD_C170COD_CTA.Value;
  // ini 2022-04-12
  VL_ABAT_NT     := QrEfdInnNFsItsVL_ABAT_NT     .Value;
  //if (Trim(DESCR_COMPL) = '') and VAR_INF_SPED_EFD_NOME_DESCR_COMPL then
  //  DESCR_COMPL := QrItsIprod_xProd.Value;
  // fim 2022-04-12
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'NUM_ITEM', NUM_ITEM, False, True, True) then
      (*03*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*04*) if not AC_x(ITO, 'DESCR_COMPL', DESCR_COMPL) then
      (*05*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      (*06*) if not AC_x(ITO, 'UNID', UNID) then
      (*07*) if not AC_f(ITO, 'VL_ITEM', VL_ITEM, True, True) then
      (*08*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, True) then
      (*09*) if not AC_x(ITO, 'IND_MOV', IND_MOV) then
      //(*10*) if not AC_i(ITO, 'CST_ICMS', CST_ICMS, True, True) then
      (*10*) if not AC_n(ITO, 'CST_ICMS', CST_ICMS) then
      (*11*) if not AC_i(ITO, 'CFOP', CFOP, False, True) then
      (*12*) if not AC_x(ITO, 'COD_NAT', COD_NAT) then
      (*13*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
      (*14*) if not AC_f(ITO, 'ALIQ_ICMS', ALIQ_ICMS, True, True) then
      (*15*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*16*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, True) then
      (*17*) if not AC_f(ITO, 'ALIQ_ST', ALIQ_ST, True, True) then
      (*18*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, True) then
      (*19*) if not AC_x(ITO, 'IND_APUR', IND_APUR) then
      (*20*) if not AC_x(ITO, 'CST_IPI', CST_IPI) then
      (*21*) if not AC_x(ITO, 'COD_ENQ', COD_ENQ) then
      (*22*) if not AC_f(ITO, 'VL_BC_IPI', VL_BC_IPI, True, True) then
      (*23*) if not AC_f(ITO, 'ALIQ_IPI', ALIQ_IPI, True, True) then
      (*24*) if not AC_f(ITO, 'VL_IPI', VL_IPI, True, True) then
      (*25*) if not AC_n(ITO, 'CST_PIS', CST_PIS) then
      (*26*) if not AC_f(ITO, 'VL_BC_PIS', VL_BC_PIS, True, True) then
      (*27*) if not AC_f(ITO, 'ALIQ_PIS_p', ALIQ_PIS_p, True, True) then
      (*28*) if not AC_f(ITO, 'QUANT_BC_PIS', QUANT_BC_PIS, True, True) then
      (*29*) if not AC_f(ITO, 'ALIQ_PIS_r', ALIQ_PIS_r, True, True) then
      (*30*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, True) then
      (*31*) if not AC_n(ITO, 'CST_COFINS', CST_COFINS) then
      (*32*) if not AC_f(ITO, 'VL_BC_COFINS', VL_BC_COFINS, True, True) then
      (*33*) if not AC_f(ITO, 'ALIQ_COFINS_p', ALIQ_COFINS_p, True, True) then
      (*34*) if not AC_f(ITO, 'QUANT_BC_COFINS', QUANT_BC_COFINS, True, True) then
      (*35*) if not AC_f(ITO, 'ALIQ_COFINS_r', ALIQ_COFINS_r, True, True) then
      (*36*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, True) then
      (*37*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
      (*38*) if not AC_f(ITO, 'VL_ABAT_NT', VL_ABAT_NT, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCFOP, DModG.AllID_DB, [
    'SELECT Codigo ',
    'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
    'WHERE Codigo="' + Geral.FF0(CFOP) + '"',
    '']);
    if QrCFOP.RecordCount = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCFOPUnkn)(*FNumErro[13]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'CFOP'], True);
    end else
    begin
      GrupoCFOP := CFOP div 1000;
      case ITO of
        itoEntrada: TemErro := not (GrupoCFOP in ([1,2,3]));
        itoSaida: TemErro := not (GrupoCFOP in ([5,6,7]));
        else TemErro := True;
      end;
      if TemErro then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes',
        'Empresa', 'LinArq', 'REG', 'Campo'], [
        Integer(efdexerCFOPInval)(*FNumErro[15]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, 'CFOP'], True);
      end;
    end;
    //AdicionaBLC(1);
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'C100';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinC100;
    //
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'GraGruX';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrEfdInnNFsItsGraGruX.Value;
    //
    FQTD_LIN_C170 := FQTD_LIN_C170 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C190_Emit(): Boolean;
  function GeraRegistro(): Boolean;
  var
    ITO: TIndicadorDoTipoDeOperacao;
    EhE: Boolean;
    tpNF, IND_OPER, CST_ICMS: String;
    //
    N, CFOP: Integer;
    ALIQ_ICMS, VL_OPR, VL_BC_ICMS, VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_RED_BC,
    VL_IPI: Double;
    COD_OBS: String;
  begin
    FBloco := 'C';
    FRegistro := 'C190';
    Result := False;
  (* Configurado para a NF e n�o para o itens
    PB1.Max := 0;
    PB1.Position := 0;
  *)
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
    //
    CST_ICMS := Geral.FFN(QrGruInCST.Value, 3);
    CFOP := QrGruInCFOP.Value;
    ALIQ_ICMS := QrGruIpICMS.Value;
    VL_OPR := QrGruIValor.Value;
    VL_BC_ICMS    := QrGruIICMS_vBC.Value;
    VL_ICMS       := QrGruIICMS_vICMS.Value;
    VL_BC_ICMS_ST := QrGruIICMS_vBCST.Value;
    VL_ICMS_ST    := QrGruIICMS_vICMSST.Value;
    //VL_RED_BC := QrGruIprod_vProd.Value - VL_BC_ICMS;
    VL_RED_BC := 0.00;
    VL_IPI := QrGruIIPI_vIPI.Value;
    COD_OBS := ''; // Parei Aqui! Ver o que fazer!
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_n(ITO, 'CST_ICMS',  CST_ICMS) then
        (*03*) if not AC_i(ITO, 'CFOP',  CFOP, True, True) then
        (*04*) if not AC_f(ITO, 'ALIQ_ICMS',  ALIQ_ICMS, True, True) then
        (*05*) if not AC_f(ITO, 'VL_OPR',  VL_OPR, True, True) then
        (*06*) if not AC_f(ITO, 'VL_BC_ICMS',  VL_BC_ICMS, True, True) then
        (*07*) if not AC_f(ITO, 'VL_ICMS',  VL_ICMS, True, True) then
        (*08*) if not AC_f(ITO, 'VL_BC_ICMS_ST',  VL_BC_ICMS_ST, True, True) then
        (*09*) if not AC_f(ITO, 'VL_ICMS_ST',  VL_ICMS_ST, True, True) then
        (*10*) if not AC_f(ITO, 'VL_RED_BC',  VL_RED_BC, True, True) then
        (*11*) if not AC_f(ITO, 'VL_IPI',  VL_IPI, True, True) then
        (*12*) if not AC_x(ITO, 'COD_OBS',  COD_OBS) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //

      if VL_OPR < 0.01 then
        Grava_SpedEfdIcmsIpiErrs(efdexerFldObrigValOp, 'VL_OPR');
      //
      //AdicionaBLC(1);
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'C100';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinC100;
      //
      FQTD_LIN_C190 := FQTD_LIN_C190 + 1;
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_C);
    end;
  end;
var
  TemErro, EhE: Boolean;
  IND_OPER, tpNF: String;
  ITO: TIndicadorDoTipoDeOperacao;
begin
  ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  //
  if ITO = itoSaida then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGruI, Dmod.MyDB, [
    'SELECT ((n.ICMS_Orig * 100)+ n.ICMS_CST) nCST, i.ICMSRec_pAliq, ',
    'SUM(i.ICMSRec_vBC) ICMSRec_vBC, SUM(i.ICMSRec_vICMS) ICMSRec_vICMS,   ',
    'i.prod_CFOP nCFOP, SUM(i.prod_vProd + i.prod_vFrete + i.prod_vSeg + ',
    'i.prod_vOutro + n.ICMS_vICMSST + ' +
    ' IF (o.IPI_vIPI IS NULL, 0, o.IPI_vIPI)) Valor, ',
    'SUM(i.prod_vProd) prod_vProd, SUM(o.IPI_vIPI) IPI_vIPI, ',
    'SUM(n.ICMS_vBC) ICMS_vBC, SUM(n.ICMS_vICMS) ICMS_vICMS, ',
    'SUM(n.ICMS_vBCST) ICMS_vBCST, SUM(n.ICMS_vICMSST) ICMS_vICMSST, ',
    'n.ICMS_pICMS pICMS',
    'FROM nfeitsi i  ',
    'LEFT JOIN nfeitsn n ON n.FatID=i.FatID   ',
    '  AND n.FatNum=i.FatNum AND n.Empresa=i.Empresa  ',
    '  AND n.nItem=i.nItem  ',
    'LEFT JOIN nfeitso o ON o.FatID=i.FatID   ',
    '  AND o.FatNum=i.FatNum AND o.Empresa=i.Empresa  ',
    '  AND o.nItem=i.nItem  ',
    //
    'WHERE i.FatID=' + Geral.FF0(QrCabA_CFatID.Value),
    'AND i.FatNum='  + Geral.FF0(QrCabA_CFatNum.Value),
    'AND i.Empresa=' + Geral.FF0(QrCabA_CEmpresa.Value),
    'GROUP BY nCST, pICMS, nCFOP ',
    '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGruI, Dmod.MyDB, [
    'SELECT c.CST_ICMS nCST, i.ICMSRec_pAliq, ',
    'SUM(i.ICMSRec_vBC) ICMSRec_vBC, SUM(i.ICMSRec_vICMS) ICMSRec_vICMS,    ',
    'c.CFOP nCFOP, SUM(i.prod_vProd + i.prod_vFrete + i.prod_vSeg +  ',
    'i.prod_vOutro + n.ICMS_vICMSST +  IF (o.IPI_vIPI IS NULL, 0, o.IPI_vIPI)) Valor,  ',
    'SUM(i.prod_vProd) prod_vProd, SUM(o.IPI_vIPI) IPI_vIPI,  ',
    'SUM(n.ICMS_vBC) ICMS_vBC, SUM(n.ICMS_vICMS) ICMS_vICMS,  ',
    'SUM(n.ICMS_vBCST) ICMS_vBCST, SUM(n.ICMS_vICMSST) ICMS_vICMSST,  ',
    'c.ALIQ_ICMS pICMS  ',
    'FROM nfeitsi i   ',
    'LEFT JOIN nfeitsn n ON n.FatID=i.FatID    ',
    '  AND n.FatNum=i.FatNum AND n.Empresa=i.Empresa   ',
    '  AND n.nItem=i.nItem   ',
    'LEFT JOIN nfeitso o ON o.FatID=i.FatID    ',
    '  AND o.FatNum=i.FatNum AND o.Empresa=i.Empresa   ',
    '  AND o.nItem=i.nItem   ',
    'LEFT JOIN nfeefd_c170 c ON c.FatID=i.FatID    ',
    '  AND c.FatNum=i.FatNum AND c.Empresa=i.Empresa   ',
    '  AND c.nItem=i.nItem   ',
      //
    'WHERE i.FatID=' + Geral.FF0(QrCabA_CFatID.Value),
    'AND i.FatNum='  + Geral.FF0(QrCabA_CFatNum.Value),
    'AND i.Empresa=' + Geral.FF0(QrCabA_CEmpresa.Value),
    'GROUP BY nCST, pICMS, nCFOP ',
    '']);
  end;
  //
  if QrGruI.RecordCount > 1 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrAllI, Dmod.MyDB, [
    'SELECT  ',
    'SUM(i.ICMSRec_vBC) ICMSRec_vBC, SUM(i.ICMSRec_vICMS) ICMSRec_vICMS,  ',
    'SUM(i.prod_vProd +  ',
    'i.prod_vFrete + i.prod_vSeg + i.prod_vOutro) Valor, ',
    'SUM(i.prod_vProd) prod_vProd, SUM(o.IPI_vIPI) IPI_vIPI, ',
    'SUM(n.ICMS_vBC) ICMS_vBC, SUM(n.ICMS_vICMS) ICMS_vICMS, ',
    'SUM(n.ICMS_vBCST) ICMS_vBCST, SUM(n.ICMS_vICMSST) ICMS_vICMSST ',
    'FROM nfeitsi i ',
    'LEFT JOIN nfeitsn n ON n.FatID=i.FatID  ',
    '  AND n.FatNum=i.FatNum AND n.Empresa=i.Empresa ',
    '  AND n.nItem=i.nItem ',
    'LEFT JOIN nfeitso o ON o.FatID=i.FatID  ',
    '  AND o.FatNum=i.FatNum AND o.Empresa=i.Empresa ',
    '  AND o.nItem=i.nItem ',
    'WHERE i.FatID=' + Geral.FF0(QrCabA_CFatID.Value),
    'AND i.FatNum='  + Geral.FF0(QrCabA_CFatNum.Value),
    'AND i.Empresa=' + Geral.FF0(QrCabA_CEmpresa.Value),
    '']);
    //if ITO = itoEntrada then
      //Geral.MB_SQL(Self, QrGruI);
    TemErro :=
      Int(QrAllIICMS_vBC.Value * 100) <> Int(QrCabA_CICMSTot_vBC.Value * 100);
  end else
    TemErro :=
      Int(QrGruIICMS_vBC.Value * 100) <> Int(QrCabA_CICMSTot_vBC.Value * 100);
  if TemErro then
    Grava_SpedEfdIcmsIpiErrs(efdexerValInval, 'VL_BC_ICMS');
  QrGruI.First;
  while not QrGruI.Eof do
  begin
    if not GeraRegistro() then Exit;
    //
    QrGruI.Next;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C190_Entr(Controle: Integer): Boolean;
  function GeraRegistro(): Boolean;
  var
    ITO: TIndicadorDoTipoDeOperacao;
    //EhE: Boolean;
    //tpNF, IND_OPER
    CST_ICMS: String;
    //
    N, CFOP: Integer;
    ALIQ_ICMS, VL_OPR, VL_BC_ICMS, VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_RED_BC,
    VL_IPI: Double;
    COD_OBS: String;
  begin
    FBloco := 'C';
    FRegistro := 'C190';
    Result := False;
  (* Configurado para a NF e n�o para o itens
    PB1.Max := 0;
    PB1.Position := 0;
  *)
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    //ObtemITO_InNF(EhE, IND_OPER, tpNF, ITO);
    ITO := itoEntrada;
    //
    CST_ICMS      := QrAnal0053CST_ICMS     .Value;
    CFOP          := QrAnal0053CFOP         .Value;
    ALIQ_ICMS     := QrAnal0053ALIQ_ICMS    .Value;
    VL_OPR        := QrAnal0053VL_OPR       .Value;
    VL_BC_ICMS    := QrAnal0053VL_BC_ICMS   .Value;
    VL_ICMS       := QrAnal0053VL_ICMS      .Value;
    VL_BC_ICMS_ST := QrAnal0053VL_BC_ICMS_ST.Value;
    VL_ICMS_ST    := QrAnal0053VL_ICMS_ST   .Value;
    //VL_RED_BC := QrAnal0053prod_vProd.Value - VL_BC_ICMS;
    VL_RED_BC     := 0.00; // Parei Aqui! Ver o que fazer!
    VL_IPI        := QrAnal0053VL_IPI.Value;
    COD_OBS := ''; // Parei Aqui! Ver o que fazer!
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_n(ITO, 'CST_ICMS',  CST_ICMS) then
        (*03*) if not AC_i(ITO, 'CFOP',  CFOP, True, True) then
        (*04*) if not AC_f(ITO, 'ALIQ_ICMS',  ALIQ_ICMS, True, True) then
        (*05*) if not AC_f(ITO, 'VL_OPR',  VL_OPR, True, True) then
        (*06*) if not AC_f(ITO, 'VL_BC_ICMS',  VL_BC_ICMS, True, True) then
        (*07*) if not AC_f(ITO, 'VL_ICMS',  VL_ICMS, True, True) then
        (*08*) if not AC_f(ITO, 'VL_BC_ICMS_ST',  VL_BC_ICMS_ST, True, True) then
        (*09*) if not AC_f(ITO, 'VL_ICMS_ST',  VL_ICMS_ST, True, True) then
        (*10*) if not AC_f(ITO, 'VL_RED_BC',  VL_RED_BC, True, True) then
        (*11*) if not AC_f(ITO, 'VL_IPI',  VL_IPI, True, True) then
        (*12*) if not AC_x(ITO, 'COD_OBS',  COD_OBS) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //

      if VL_OPR < 0.01 then
        Grava_SpedEfdIcmsIpiErrs(efdexerFldObrigValOp, 'VL_OPR');
      //
      //AdicionaBLC(1);
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'C100';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinC100;
      //
      FQTD_LIN_C190 := FQTD_LIN_C190 + 1;
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_C);
    end;
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAnal0053, Dmod.MyDB, [
  'SELECT CST_ICMS, CFOP, ALIQ_ICMS, ',
  'SUM(VL_BC_ICMS) VL_BC_ICMS,',
  'SUM(VL_ICMS) VL_ICMS,',
  'SUM(VL_BC_ICMS_ST) VL_BC_ICMS_ST,',
  'SUM(VL_ICMS_ST) VL_ICMS_ST,',
  'SUM(VL_RED_BC) VL_RED_BC,',
  'SUM(VL_IPI) VL_IPI,',
  'SUM(prod_vProd + prod_vFrete + prod_vOutro - VL_DESC + VL_IPI) VL_OPR ',
  'FROM efdinnnfsits',
  'WHERE Controle=' + Geral.FF0(Controle),
  'GROUP BY CST_ICMS, CFOP, ALIQ_ICMS',
  '']);
  //
  QrAnal0053.First;
  while not QrAnal0053.Eof do
  begin
    GeraRegistro();
    QrAnal0053.Next;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C195(EfdInnNFsCabControle: Integer): Boolean;
  function GeraRegistroC195_CAT66SP2018(): Boolean;
  var
    ITO: TIndicadorDoTipoDeOperacao;
    //
    COD_OBS, TXT_COMPL: String;
    //
    N, I, Seq: Integer;
  begin
    FBloco := 'C';
    FRegistro := 'C195';
    Result := False;
  (* Configurado para a NF e n�o para o itens
    PB1.Max := 0;
    PB1.Position := 0;
  *)
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    //ObtemITO_InNF(EhE, IND_OPER, tpNF, ITO);
    ITO := itoEntrada;
    //
    Seq := 0;
    for I := 1 to Length(FLstCAT66SP2018) do
    begin
      if FLstCAT66SP2018[I] = QrCAT66SP2018CFOP.Value then
      begin
        Seq := I;
        Break;
      end;
    end;
    COD_OBS     := GeraCOD_OBS_CAT66SP2018(Seq);
    TXT_COMPL   := 'CFOP ' + Geral.FF0(QrCAT66SP2018CFOP.Value);
    try
      PredefineLinha();
      FLinC195 := FLinArq;
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_OBS',  COD_OBS) then
        (*03*) if not AC_x(ITO, 'TXT_COMPL',  TXT_COMPL) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //

      //AdicionaBLC(1);
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'C100';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinC100;
      //
      FQTD_LIN_C195 := FQTD_LIN_C195 + 1;
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_C);
    end;
  end;
  function GeraRegistroC197_CAT66SP2018(): Boolean;
  var
    ITO: TIndicadorDoTipoDeOperacao;
    //
    COD_AJ, DESCR_COMPL_AJ: String;
    //
    N: Integer;
  begin
    FBloco := 'C';
    FRegistro := 'C197';
    Result := False;
  (* Configurado para a NF e n�o para o itens
    PB1.Max := 0;
    PB1.Position := 0;
  *)
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    //ObtemITO_InNF(EhE, IND_OPER, tpNF, ITO);
    ITO := itoEntrada;
    //
    COD_AJ           := 'SP90090104';
    DESCR_COMPL_AJ   := Geral.FF0(QrCAT66SP2018CFOP.Value);
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_AJ',  COD_AJ) then
        (*03*) if not AC_x(ITO, 'DESCR_COMPL_AJ',  DESCR_COMPL_AJ) then
        (*04*) if not AC_x(ITO, 'COD_ITEM',  EmptyStr) then
        (*05*) if not AC_f(ITO, 'VL_BC_ICMS',  0.00, True, True) then
        (*06*) if not AC_f(ITO, 'ALIQ_ICMS',  0.00, True, True) then
        (*07*) if not AC_f(ITO, 'VL_ICMS',  0.00, True, True) then
        (*08*) if not AC_f(ITO, 'VL_OUTROS',  QrCAT66SP2018AjusteVL_OUTROS.Value, False, True) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //

      //AdicionaBLC(1);
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'C100';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinC100;
      //
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'C195';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinC195;
      //
      FQTD_LIN_C197 := FQTD_LIN_C197 + 1;
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_C);
    end;
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCAT66SP2018, Dmod.MyDB, [
  'SELECT its.CFOP, SUM(its.AjusteVL_OUTROS) AjusteVL_OUTROS',
  'FROM efdinnnfsits its',
  'WHERE its.Controle=' + Geral.FF0(EfdInnNFsCabControle),
//  'AND its.EFD_II_C195=' + Geral.FF0(1),
  'AND its.AjusteVL_OUTROS > 0.00 ',
  'GROUP BY its.CFOP',
  'ORDER BY its.CFOP',
  '']);
  //
  QrCAT66SP2018.First;
  while not QrCAT66SP2018.Eof do
  begin
    GeraRegistroC195_CAT66SP2018();
    GeraRegistroC197_CAT66SP2018();
    //
    QrCAT66SP2018.Next;
  end;
  //
  Result := True;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C197(): Boolean;
begin
//
end;

{
function TFmEfdIcmsIpiExporta.GeraRegistro_C190_A(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, IND_OPER: String;
  //
  N, CST_ICMS, CFOP: Integer;
  ALIQ_ICMS, VL_OPR, VL_BC_ICMS, VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_RED_BC,
  VL_IPI: Double;
  COD_OBS: String;
begin
  FBloco := 'C';
  FRegistro := 'C190';
  Result := False;
(* Configurado para a NF e n�o para o itens
  PB1.Max := 0;
  PB1.Position := 0;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
(*
  EhE := QrCabA_CCodInfoEmit.Value = FEmpresa_Int;
  tpNF := Geral.FF0(QrCabA_Cide_tpNF.Value);
  IND_OPER := MLAGeral.EscolhaDe2Str(EhE, tpNF, '0');
  if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
*)
  ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  //
  //CST_ICMS := (QrGruIICMS_Orig.Value * 100) + QrGruIICMS_CST.Value;
  CST_ICMS := QrGruInCST.Value;
  //CFOP := QrGruIprod_CFOP.Value;
  CFOP := QrGruInCFOP.Value;
  //ALIQ_ICMS := QrGruIICMS_pICMS.Value;
  ALIQ_ICMS := QrGruIpICMS.Value;
  VL_OPR := QrGruIValor.Value;
(*
  VL_BC_ICMS := QrGruIICMSRec_vBC.Value;
  VL_ICMS := QrGruIICMSRec_vICMS.Value;
  VL_BC_ICMS_ST := 0;// Parei aqui! QrGruIICMSRec_vBCST.Value;
  VL_ICMS_ST := 0;//QrGruIICMSRec_vICMSST.Value;
*)
  VL_BC_ICMS    := QrGruIICMS_vBC.Value;
  VL_ICMS       := QrGruIICMS_vICMS.Value;
  VL_BC_ICMS_ST := QrGruIICMS_vBCST.Value;
  VL_ICMS_ST    := QrGruIICMS_vICMSST.Value;
  VL_RED_BC := QrGruIprod_vProd.Value - VL_BC_ICMS;
  (*
  if VL_RED_BC < 0 then
  begin
    VL_RED_BC := 0;
  end;
  *)
  VL_IPI := QrGruIIPI_vIPI.Value;
  COD_OBS := ''; // Parei Aqui! Ver o que fazer!
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'CST_ICMS',  CST_ICMS, True, True) then
      (*03*) if not AC_i(ITO, 'CFOP',  CFOP, True, True) then
      (*04*) if not AC_f(ITO, 'ALIQ_ICMS',  ALIQ_ICMS, True, True) then
      (*05*) if not AC_f(ITO, 'VL_OPR',  VL_OPR, True, True) then
      (*06*) if not AC_f(ITO, 'VL_BC_ICMS',  VL_BC_ICMS, True, True) then
      (*07*) if not AC_f(ITO, 'VL_ICMS',  VL_ICMS, True, True) then
      (*08*) if not AC_f(ITO, 'VL_BC_ICMS_ST',  VL_BC_ICMS_ST, True, True) then
      (*09*) if not AC_f(ITO, 'VL_ICMS_ST',  VL_ICMS_ST, True, True) then
      (*10*) if not AC_f(ITO, 'VL_RED_BC',  VL_RED_BC, True, True) then
      (*11*) if not AC_f(ITO, 'VL_IPI',  VL_IPI, True, True) then
      (*12*) if not AC_x(ITO, 'COD_OBS',  COD_OBS) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //

    if VL_OPR < 0.01 then
      Grava_SpedEfdIcmsIpiErrs(efdexerFldObrigValOp, 'VL_OPR');
    //
    //AdicionaBLC(1);
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'C100';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinC100;
    //
    FQTD_LIN_C190 := FQTD_LIN_C190 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdIcmsIpiExporta.GeraRegistro_C190_B: Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, IND_OPER: String;
  //
  N, CST_ICMS, CFOP: Integer;
  ALIQ_ICMS, VL_OPR, VL_BC_ICMS, VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_RED_BC,
  VL_IPI: Double;
  COD_OBS: String;
begin
  FBloco := 'C';
  FRegistro := 'C190';
  Result := False;
(* Configurado para a NF e n�o para o itens
  PB1.Max := 0;
  PB1.Position := 0;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
(*
  EhE := QrCabA_CCodInfoEmit.Value = FEmpresa_Int;
  tpNF := Geral.FF0(QrCabA_Cide_tpNF.Value);
  IND_OPER := MLAGeral.EscolhaDe2Str(EhE, tpNF, '0');
  if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
*)
  ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  //

  //CST_ICMS := (QrGruIICMS_Orig.Value * 100) + QrGruIICMS_CST.Value;
  CST_ICMS := QrGruInCST.Value;
  //CFOP := QrGruIprod_CFOP.Value;
  CFOP := QrGruInCFOP.Value;
  //ALIQ_ICMS := QrGruIICMS_pICMS.Value;
  ALIQ_ICMS := QrGruIpICMS.Value;
  VL_OPR := QrGruIValor.Value;
(*
  VL_BC_ICMS := QrGruIICMSRec_vBC.Value;
  VL_ICMS := QrGruIICMSRec_vICMS.Value;
  VL_BC_ICMS_ST := 0;// Parei aqui! QrGruIICMSRec_vBCST.Value;
  VL_ICMS_ST := 0;//QrGruIICMSRec_vICMSST.Value;
*)
  VL_BC_ICMS    := QrGruIICMS_vBC.Value;
  VL_ICMS       := QrGruIICMS_vICMS.Value;
  VL_BC_ICMS_ST := QrGruIICMS_vBCST.Value;
  VL_ICMS_ST    := QrGruIICMS_vICMSST.Value;
  VL_RED_BC := QrGruIprod_vProd.Value - VL_BC_ICMS;
  (*
  if VL_RED_BC < 0 then
  begin
    VL_RED_BC := 0;
  end;
  *)
  VL_IPI := QrGruIIPI_vIPI.Value;
  COD_OBS := ''; // Parei Aqui! Ver o que fazer!
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'CST_ICMS',  CST_ICMS, True, True) then
      (*03*) if not AC_i(ITO, 'CFOP',  CFOP, True, True) then
      (*04*) if not AC_f(ITO, 'ALIQ_ICMS',  ALIQ_ICMS, True, True) then
      (*05*) if not AC_f(ITO, 'VL_OPR',  VL_OPR, True, True) then
      (*06*) if not AC_f(ITO, 'VL_BC_ICMS',  VL_BC_ICMS, True, True) then
      (*07*) if not AC_f(ITO, 'VL_ICMS',  VL_ICMS, True, True) then
      (*08*) if not AC_f(ITO, 'VL_BC_ICMS_ST',  VL_BC_ICMS_ST, True, True) then
      (*09*) if not AC_f(ITO, 'VL_ICMS_ST',  VL_ICMS_ST, True, True) then
      (*10*) if not AC_f(ITO, 'VL_RED_BC',  VL_RED_BC, True, True) then
      (*11*) if not AC_f(ITO, 'VL_IPI',  VL_IPI, True, True) then
      (*12*) if not AC_x(ITO, 'COD_OBS',  COD_OBS) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //

    if VL_OPR < 0.01 then
      Grava_SpedEfdIcmsIpiErrs(efdexerFldObrigValOp, 'VL_OPR');
    //
    //AdicionaBLC(1);
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'C100';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinC100;
    //
    FQTD_LIN_C190 := FQTD_LIN_C190 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdIcmsIpiExporta.GeraRegistro_C190_Ent(): Boolean;
var
  TemErro:Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGruI, Dmod.MyDB, [
  //'SELECT n.ICMS_Orig, n.ICMS_CST, i.ICMSRec_pAliq,    ',
  'SELECT c.CST_ICMS nCST, i.ICMSRec_pAliq, ',
  'SUM(i.ICMSRec_vBC) ICMSRec_vBC, SUM(i.ICMSRec_vICMS) ICMSRec_vICMS,    ',
  'c.CFOP nCFOP, SUM(i.prod_vProd + i.prod_vFrete + i.prod_vSeg +  ',
  'i.prod_vOutro + n.ICMS_vICMSST +  IF (o.IPI_vIPI IS NULL, 0, o.IPI_vIPI)) Valor,  ',
  'SUM(i.prod_vProd) prod_vProd, SUM(o.IPI_vIPI) IPI_vIPI,  ',
  'SUM(n.ICMS_vBC) ICMS_vBC, SUM(n.ICMS_vICMS) ICMS_vICMS,  ',
  'SUM(n.ICMS_vBCST) ICMS_vBCST, SUM(n.ICMS_vICMSST) ICMS_vICMSST,  ',
  'c.ALIQ_ICMS pICMS  ',
  'FROM nfeitsi i   ',
  'LEFT JOIN nfeitsn n ON n.FatID=i.FatID    ',
  '  AND n.FatNum=i.FatNum AND n.Empresa=i.Empresa   ',
  '  AND n.nItem=i.nItem   ',
  'LEFT JOIN nfeitso o ON o.FatID=i.FatID    ',
  '  AND o.FatNum=i.FatNum AND o.Empresa=i.Empresa   ',
  '  AND o.nItem=i.nItem   ',
  'LEFT JOIN nfeefd_c170 c ON c.FatID=i.FatID    ',
  '  AND c.FatNum=i.FatNum AND c.Empresa=i.Empresa   ',
  '  AND c.nItem=c.nItem   ',
    //
  'WHERE i.FatID=' + Geral.FF0(QrCabA_CFatID.Value),
  'AND i.FatNum='  + Geral.FF0(QrCabA_CFatNum.Value),
  'AND i.Empresa=' + Geral.FF0(QrCabA_CEmpresa.Value),
  'GROUP BY nCST, pICMS, nCFOP ',
  '']);
  //Geral.MB_SQL(Self, QrGruI);
  if QrGruI.RecordCount > 1 then
  begin
(*
    QrAllI.Close;
    QrAllI.Params[00].AsInteger := QrCabA_CFatID.Value;
    QrAllI.Params[01].AsInteger := QrCabA_CFatNum.Value;
    QrAllI.Params[02].AsInteger := QrCabA_CEmpresa.Value;
    UnDmkDAC_PF.AbreQuery(QrAllI, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    //
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrAllI, Dmod.MyDB, [
    'SELECT  ',
    'SUM(i.ICMSRec_vBC) ICMSRec_vBC, SUM(i.ICMSRec_vICMS) ICMSRec_vICMS,  ',
    'SUM(i.prod_vProd +  ',
    'i.prod_vFrete + i.prod_vSeg + i.prod_vOutro) Valor, ',
    'SUM(i.prod_vProd) prod_vProd, SUM(o.IPI_vIPI) IPI_vIPI, ',
    'SUM(n.ICMS_vBC) ICMS_vBC, SUM(n.ICMS_vICMS) ICMS_vICMS, ',
    'SUM(n.ICMS_vBCST) ICMS_vBCST, SUM(n.ICMS_vICMSST) ICMS_vICMSST ',
    'FROM nfeitsi i ',
    'LEFT JOIN nfeitsn n ON n.FatID=i.FatID  ',
    '  AND n.FatNum=i.FatNum AND n.Empresa=i.Empresa ',
    '  AND n.nItem=i.nItem ',
    'LEFT JOIN nfeitso o ON o.FatID=i.FatID  ',
    '  AND o.FatNum=i.FatNum AND o.Empresa=i.Empresa ',
    '  AND o.nItem=i.nItem ',
    'WHERE i.FatID=' + Geral.FF0(QrCabA_CFatID.Value),
    'AND i.FatNum='  + Geral.FF0(QrCabA_CFatNum.Value),
    'AND i.Empresa=' + Geral.FF0(QrCabA_CEmpresa.Value),
    '']);
    TemErro :=
      Int(QrAllIICMS_vBC.Value * 100) <> Int(QrCabA_CICMSTot_vBC.Value * 100);
  end else
    TemErro :=
      Int(QrGruIICMS_vBC.Value * 100) <> Int(QrCabA_CICMSTot_vBC.Value * 100);
  if TemErro then
(*
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
    'Erro'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'Campo'], [FNumErro[16]], [
    FImporExpor, FAnoMes_Int, FEmpresa_Int, FLinArq, FRegistro, 'VL_BC_ICMS'], True);
  end;
*)
  Grava_SpedEfdIcmsIpiErrs(efdexerValInval, 'VL_BC_ICMS');
  QrGruI.First;
  while not QrGruI.Eof do
  begin
    if not GeraRegistro_C190_B() then Exit;
    //
    QrGruI.Next;
  end;
end;

function TFmEfdIcmsIpiExporta.GeraRegistro_C190_Sai(): Boolean;
var
  TemErro:Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGruI, Dmod.MyDB, [
(*
  'SELECT n.ICMS_Orig, n.ICMS_CST, i.ICMSRec_pAliq,  ',
  'SUM(i.ICMSRec_vBC) ICMSRec_vBC, SUM(i.ICMSRec_vICMS) ICMSRec_vICMS,  ',
  'i.prod_CFOP, SUM(i.prod_vProd +  ',
  'i.prod_vFrete + i.prod_vSeg + i.prod_vOutro) Valor, ',
  'SUM(i.prod_vProd) prod_vProd, SUM(o.IPI_vIPI) IPI_vIPI     ',
  'FROM nfeitsi i ',
  'LEFT JOIN nfeitsn n ON n.FatID=i.FatID  ',
  '  AND n.FatNum=i.FatNum AND n.Empresa=i.Empresa ',
  '  AND n.nItem=i.nItem ',
  'LEFT JOIN nfeitso o ON o.FatID=i.FatID  ',
  '  AND o.FatNum=i.FatNum AND o.Empresa=i.Empresa ',
  '  AND o.nItem=i.nItem ',
*)
  //'SELECT n.ICMS_Orig, n.ICMS_CST, i.ICMSRec_pAliq,   ',
  'SELECT ((n.ICMS_Orig * 100)+ n.ICMS_CST) nCST, i.ICMSRec_pAliq, ',
  'SUM(i.ICMSRec_vBC) ICMSRec_vBC, SUM(i.ICMSRec_vICMS) ICMSRec_vICMS,   ',
  'i.prod_CFOP nCFOP, SUM(i.prod_vProd + i.prod_vFrete + i.prod_vSeg + ',
  'i.prod_vOutro + n.ICMS_vICMSST + ' +
  ' IF (o.IPI_vIPI IS NULL, 0, o.IPI_vIPI)) Valor, ',
  'SUM(i.prod_vProd) prod_vProd, SUM(o.IPI_vIPI) IPI_vIPI, ',
  'SUM(n.ICMS_vBC) ICMS_vBC, SUM(n.ICMS_vICMS) ICMS_vICMS, ',
  'SUM(n.ICMS_vBCST) ICMS_vBCST, SUM(n.ICMS_vICMSST) ICMS_vICMSST, ',
  'n.ICMS_pICMS pICMS',
  'FROM nfeitsi i  ',
  'LEFT JOIN nfeitsn n ON n.FatID=i.FatID   ',
  '  AND n.FatNum=i.FatNum AND n.Empresa=i.Empresa  ',
  '  AND n.nItem=i.nItem  ',
  'LEFT JOIN nfeitso o ON o.FatID=i.FatID   ',
  '  AND o.FatNum=i.FatNum AND o.Empresa=i.Empresa  ',
  '  AND o.nItem=i.nItem  ',
  //
  'WHERE i.FatID=' + Geral.FF0(QrCabA_CFatID.Value),
  'AND i.FatNum='  + Geral.FF0(QrCabA_CFatNum.Value),
  'AND i.Empresa=' + Geral.FF0(QrCabA_CEmpresa.Value),
  'GROUP BY nCST, pICMS, nCFOP ',
  '']);
  //Geral.MB_SQL(Self, QrGruI);
  if QrGruI.RecordCount > 1 then
  begin
(*
    QrAllI.Close;
    QrAllI.Params[00].AsInteger := QrCabA_CFatID.Value;
    QrAllI.Params[01].AsInteger := QrCabA_CFatNum.Value;
    QrAllI.Params[02].AsInteger := QrCabA_CEmpresa.Value;
    UnDmkDAC_PF.AbreQuery(QrAllI, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    //
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrAllI, Dmod.MyDB, [
    'SELECT  ',
    'SUM(i.ICMSRec_vBC) ICMSRec_vBC, SUM(i.ICMSRec_vICMS) ICMSRec_vICMS,  ',
    'SUM(i.prod_vProd +  ',
    'i.prod_vFrete + i.prod_vSeg + i.prod_vOutro) Valor, ',
    'SUM(i.prod_vProd) prod_vProd, SUM(o.IPI_vIPI) IPI_vIPI, ',
    'SUM(n.ICMS_vBC) ICMS_vBC, SUM(n.ICMS_vICMS) ICMS_vICMS, ',
    'SUM(n.ICMS_vBCST) ICMS_vBCST, SUM(n.ICMS_vICMSST) ICMS_vICMSST ',
    'FROM nfeitsi i ',
    'LEFT JOIN nfeitsn n ON n.FatID=i.FatID  ',
    '  AND n.FatNum=i.FatNum AND n.Empresa=i.Empresa ',
    '  AND n.nItem=i.nItem ',
    'LEFT JOIN nfeitso o ON o.FatID=i.FatID  ',
    '  AND o.FatNum=i.FatNum AND o.Empresa=i.Empresa ',
    '  AND o.nItem=i.nItem ',
    'WHERE i.FatID=' + Geral.FF0(QrCabA_CFatID.Value),
    'AND i.FatNum='  + Geral.FF0(QrCabA_CFatNum.Value),
    'AND i.Empresa=' + Geral.FF0(QrCabA_CEmpresa.Value),
    '']);
    TemErro :=
      Int(QrAllIICMS_vBC.Value * 100) <> Int(QrCabA_CICMSTot_vBC.Value * 100);
  end else
    TemErro :=
      Int(QrGruIICMS_vBC.Value * 100) <> Int(QrCabA_CICMSTot_vBC.Value * 100);
  if TemErro then
(*
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
    'Erro'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'REG', 'Campo'], [FNumErro[16]], [
    FImporExpor, FAnoMes_Int, FEmpresa_Int, FLinArq, FRegistro, 'VL_BC_ICMS'], True);
  end;
*)
  Grava_SpedEfdIcmsIpiErrs(efdexerValInval, 'VL_BC_ICMS');
  QrGruI.First;
  while not QrGruI.Eof do
  begin
    if not GeraRegistro_C190_A() then Exit;
    //
    QrGruI.Next;
  end;
end;
}

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C500(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  //
  IND_OPER, IND_EMIT, COD_PART, COD_MOD, COD_SIT, SER, SUB, COD_CONS: String;
  NUM_DOC: Integer;
  DT_DOC, DT_E_S: TDateTime;
  VL_DOC, VL_DESC, VL_FORN, VL_SERV_NT, VL_TERC, VL_DA, VL_BC_ICMS, VL_ICMS,
  VL_BC_ICMS_ST, VL_ICMS_ST: Double;
  COD_INF: String;
  VL_PIS, VL_COFINS: Double;
  TP_LIGACAO, COD_GRUPO_TENSAO: String;
var
  CHV_DOCe, CHV_DOCe_REF, COD_CTA, HASH_DOC_REF, SER_DOC_REF: String;
  FIN_DOCe, IND_DEST, COD_MUN_DEST, COD_MOD_DOC_REF, NUM_DOC_REF, MES_DOC_REF: Integer;
  ENER_INJET, OUTRAS_DED: Double;
begin
  FBloco := 'C';
  FRegistro := 'C500';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
    //
    IND_OPER         := QrEFD_C500IND_OPER.Value;
    IND_EMIT         := QrEFD_C500IND_EMIT.Value;
    //COD_PART         := Geral.FF0(QrEFD_C500Terceiro.Value);
    COD_PART         := ObtemCOD_PART(QrEFD_C500Terceiro.Value);
    COD_MOD          := QrEFD_C500COD_MOD.Value;
    COD_SIT          := QrEFD_C500COD_SIT.Value;
    SER              := QrEFD_C500SER.Value;
    SUB              := QrEFD_C500SUB.Value;
    COD_CONS         := QrEFD_C500COD_CONS.Value;
    NUM_DOC          := QrEFD_C500NUM_DOC.Value;
    DT_DOC           := QrEFD_C500DT_DOC.Value;
    DT_E_S           := QrEFD_C500DT_E_S.Value;
    VL_DOC           := QrEFD_C500VL_DOC.Value;
    VL_DESC          := QrEFD_C500VL_DESC.Value;
    VL_FORN          := QrEFD_C500VL_FORN.Value;
    VL_SERV_NT       := QrEFD_C500VL_SERV_NT.Value;
    VL_TERC          := QrEFD_C500VL_TERC.Value;
    VL_DA            := QrEFD_C500VL_DA.Value;
    VL_BC_ICMS       := QrEFD_C500VL_BC_ICMS.Value;
    VL_ICMS          := QrEFD_C500VL_ICMS.Value;
    VL_BC_ICMS_ST    := QrEFD_C500VL_BC_ICMS_ST.Value;
    VL_ICMS_ST       := QrEFD_C500VL_ICMS_ST.Value;
    COD_INF          := QrEFD_C500COD_INF.Value;
    VL_PIS           := QrEFD_C500VL_PIS.Value;
    VL_COFINS        := QrEFD_C500VL_COFINS.Value;
    TP_LIGACAO       := QrEFD_C500TP_LIGACAO.Value;
    COD_GRUPO_TENSAO := QrEFD_C500COD_GRUPO_TENSAO.Value;
    CHV_DOCe         := QrEFD_C500CHV_DOCe.Value;
    FIN_DOCe         := QrEFD_C500FIN_DOCe.Value;
    CHV_DOCe_REF     := QrEFD_C500CHV_DOCe_REF.Value;
    IND_DEST         := QrEFD_C500IND_DEST.Value;
    COD_MUN_DEST     := QrEFD_C500COD_MUN_DEST.Value;
    COD_CTA          := QrEFD_C500COD_CTA.Value;
    COD_MOD_DOC_REF  := QrEFD_C500COD_MOD_DOC_REF.Value;
    HASH_DOC_REF     := QrEFD_C500HASH_DOC_REF.Value;
    SER_DOC_REF      := QrEFD_C500SER_DOC_REF.Value;
    NUM_DOC_REF      := QrEFD_C500NUM_DOC_REF.Value;
    MES_DOC_REF      := QrEFD_C500MES_DOC_REF.Value;
    ENER_INJET       := QrEFD_C500ENER_INJET.Value;
    OUTRAS_DED       := QrEFD_C500OUTRAS_DED.Value;

      //

  if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
 try
    PredefineLinha();
    FLinC500 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
       //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_OPER', IND_OPER) then
      (*03*) if not AC_x(ITO, 'IND_EMIT', IND_EMIT) then
      (*04*) if not AC_x(ITO, 'COD_PART', COD_PART) then
      (*05*) if not AC_x(ITO, 'COD_MOD', COD_MOD) then
      (*06*) if not AC_n(ITO, 'COD_SIT', COD_SIT) then
      (*07*) if not AC_x(ITO, 'SER', SER) then
      (*08*) if not AC_n(ITO, 'SUB', SUB) then
      (*09*) if not AC_x(ITO, 'COD_CONS', COD_CONS) then
      (*10*) if not AC_i(ITO, 'NUM_DOC', NUM_DOC, False, True) then
      (*11*) if not AC_d(ITO, 'DT_DOC', DT_DOC, False, True, 23) then
      (*12*) if not AC_d(ITO, 'DT_E_S', DT_E_S, False, True, 23) then
      (*13*) if not AC_f(ITO, 'VL_DOC', VL_DOC, True, True) then
      (*14*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, True) then
      (*15*) if not AC_f(ITO, 'VL_FORN', VL_FORN, True, True) then
      (*16*) if not AC_f(ITO, 'VL_SERV_NT', VL_SERV_NT, True, True) then
      (*17*) if not AC_f(ITO, 'VL_TERC', VL_TERC, True, True) then
      (*18*) if not AC_f(ITO, 'VL_DA', VL_DA, True, True) then
      (*19*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
      (*20*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*21*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, True) then
      (*22*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, True) then
      (*23*) if not AC_x(ITO, 'COD_INF', COD_INF) then
      (*24*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, True) then
      (*25*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, True) then
      (*26*) if not AC_n(ITO, 'TP_LIGACAO', TP_LIGACAO) then
      (*27*) if not AC_x(ITO, 'COD_GRUPO_TENSAO', COD_GRUPO_TENSAO) then
      (*28*) if not AC_n(ITO, 'CHV_DOCe', CHV_DOCe) then
      (*29*) if not AC_i(ITO, 'FIN_DOCe', FIN_DOCe, False, False) then
      (*30*) if not AC_n(ITO, 'CHV_DOCe_REF', CHV_DOCe_REF) then
      (*31*) if not AC_i(ITO, 'IND_DEST', IND_DEST, False, true) then
      (*32*) if not AC_i(ITO, 'COD_MUN_DEST', COD_MUN_DEST, True, False) then
      (*33*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
      (*34*) if not AC_i(ITO, 'COD_MOD_DOC_REF', COD_MOD_DOC_REF, True, False) then
      (*35*) if not AC_x(ITO, 'HASH_DOC_REF', HASH_DOC_REF) then
      (*36*) if not AC_x(ITO, 'SER_DOC_REF', SER_DOC_REF) then
      (*37*) if not AC_i(ITO, 'NUM_DOC_REF', NUM_DOC_REF, True, False) then
      (*38*) if not AC_i(ITO, 'MES_DOC_REF', MES_DOC_REF, True, False) then
      (*39*) if not AC_f(ITO, 'ENER_INJET', ENER_INJET, True, False) then
      (*40*) if not AC_f(ITO, 'OUTRAS_DED', OUTRAS_DED, True, False) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
{
    //
    if QrEFD_D110.RecordCount <> 1 then
      Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG D110 do D100');
}
    //
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C590(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, IND_OPER, CST_ICMS, CFOP: String;
  //
  N: Integer;
  ALIQ_ICMS, VL_OPR, VL_BC_ICMS, VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST,
  VL_RED_BC: Double;
  COD_OBS: String;
begin
  FBloco := 'C';
  FRegistro := 'C590';
  Result := False;
(* Configurado para a NF e n�o para o itens
  PB1.Max := 0;
  PB1.Position := 0;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
(*
  EhE := QrCabA_CCodInfoEmit.Value = FEmpresa_Int;
  tpNF := Geral.FF0(QrCabA_Cide_tpNF.Value);
  IND_OPER := MLAGeral.EscolhaDe2Str(EhE, tpNF, '0');
  if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
*)
  ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  //

  CST_ICMS := QrEFD_C500CST_ICMS.Value;
  CFOP := QrEFD_C500CFOP.Value;
  ALIQ_ICMS := QrEFD_C500ALIQ_ICMS.Value;
  VL_OPR := QrEFD_C500VL_DOC.Value;
  VL_BC_ICMS := QrEFD_C500VL_BC_ICMS.Value;
  VL_ICMS := QrEFD_C500VL_ICMS.Value;
  VL_BC_ICMS_ST := QrEFD_C500VL_BC_ICMS_ST.Value;
  VL_ICMS_ST := QrEFD_C500VL_ICMS_ST.Value;
  VL_RED_BC := QrEFD_C500VL_RED_BC.Value;
  COD_OBS := ''; // Parei Aqui! Ver o que fazer!
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_n(ITO, 'CST_ICMS',  CST_ICMS(*, True, True*)) then
      (*03*) if not AC_n(ITO, 'CFOP',  CFOP(*, True, True*)) then
      (*04*) if not AC_f(ITO, 'ALIQ_ICMS',  ALIQ_ICMS, True, True) then
      (*05*) if not AC_f(ITO, 'VL_OPR',  VL_OPR, True, True) then
      (*06*) if not AC_f(ITO, 'VL_BC_ICMS',  VL_BC_ICMS, True, True) then
      (*07*) if not AC_f(ITO, 'VL_ICMS',  VL_ICMS, True, True) then
      (*08*) if not AC_f(ITO, 'VL_BC_ICMS_ST',  VL_BC_ICMS_ST, True, True) then
      (*09*) if not AC_f(ITO, 'VL_ICMS_ST',  VL_ICMS_ST, True, True) then
      (*10*) if not AC_f(ITO, 'VL_RED_BC',  VL_RED_BC, True, True) then
      (*11*) if not AC_x(ITO, 'COD_OBS',  COD_OBS) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //

    if VL_OPR < 0.01 then
      Grava_SpedEfdIcmsIpiErrs(efdexerFldObrigValOp, 'VL_OPR');
    //
    //AdicionaBLC(1);
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'C500';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinC500;
    //
    FQTD_LIN_C590 := FQTD_LIN_C590 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_C990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'C';
  FRegistro := 'C990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_C', FQTD_LIN_C + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_D001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'D';
  FRegistro := 'D001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_D);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_D100(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  //
  COD_SIT: String;
  DT_DOC, DT_A_P: TDateTime;
  TP_CTE, NUM_DOC: Integer;
  IND_OPER, IND_EMIT, CHV_CTE, CHV_CTE_REF, COD_INF, COD_CTA,
  COD_PART, COD_MOD, SER, SUB, IND_FRT: String;
  VL_DESC, VL_DOC, VL_SERV, VL_BC_ICMS, VL_ICMS, VL_NT: Double;
begin
  FBloco := 'D';
  FRegistro := 'D100';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  IND_OPER    := QrEFD_D100IND_OPER.Value;
  IND_EMIT    := QrEFD_D100IND_EMIT.Value;
  //COD_PART    := Geral.FF0(QrEFD_D100Terceiro.Value);
  COD_PART    := ObtemCOD_PART(QrEFD_D100Terceiro.Value);
  COD_MOD     := QrEFD_D100COD_MOD.Value;
  COD_SIT     := QrEFD_D100COD_SIT.Value;
  SER         := QrEFD_D100SER.Value;
  SUB         := QrEFD_D100SUB.Value;
  NUM_DOC     := QrEFD_D100NUM_DOC.Value;
  CHV_CTE     := QrEFD_D100CHV_CTE.Value;
//  DT_DOC      := Geral.IMV(Geral.FDT(QrEFD_D100DT_DOC.Value, 23));
//  DT_A_P      := Geral.IMV(Geral.FDT(QrEFD_D100DT_A_P.Value, 23));
  DT_DOC      := QrEFD_D100DT_DOC.Value;
  DT_A_P      := QrEFD_D100DT_A_P.Value;
  //
  TP_CTE      := QrEFD_D100TP_CTE.Value;
  CHV_CTE_REF := QrEFD_D100CHV_CTE_REF.Value;
  VL_DOC      := QrEFD_D100VL_DOC.Value;
  VL_DESC     := QrEFD_D100VL_DESC.Value;
  IND_FRT     := QrEFD_D100IND_FRT.Value;
  VL_SERV     := QrEFD_D100VL_SERV.Value;
  VL_BC_ICMS  := QrEFD_D100VL_BC_ICMS.Value;
  VL_ICMS     := QrEFD_D100VL_ICMS.Value;
  VL_NT       := QrEFD_D100VL_NT.Value;
  COD_INF     := QrEFD_D100COD_INF.Value;
  COD_CTA     := QrEFD_D100COD_CTA.Value;

   //
  if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
 try
    PredefineLinha();
    FLinD100 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
       //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_OPER', IND_OPER) then
      (*03*) if not AC_x(ITO, 'IND_EMIT', IND_EMIT) then
      (*04*) if not AC_x(ITO, 'COD_PART', COD_PART) then
      (*05*) if not AC_x(ITO, 'COD_MOD', COD_MOD) then
      (*06*) if not AC_n(ITO, 'COD_SIT', COD_SIT) then
      (*07*) if not AC_x(ITO, 'SER', SER) then
      (*08*) if not AC_x(ITO, 'SUB', SUB) then
      (*09*) if not AC_i(ITO, 'NUM_DOC', NUM_DOC, False, True) then
      (*10*) if not AC_n(ITO, 'CHV_CTE', CHV_CTE) then
      (*11*) if not AC_d(ITO, 'DT_DOC', DT_DOC, False, True, 23) then
      (*12*) if not AC_d(ITO, 'DT_A_P', DT_A_P, False, True, 23) then
      (*13*) if not AC_i(ITO, 'TP_CTE', TP_CTE, True, True) then
      (*14*) if not AC_n(ITO, 'CHV_CTE_REF', CHV_CTE_REF) then
      (*15*) if not AC_f(ITO, 'VL_DOC', VL_DOC, True, True) then
      (*16*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, True) then
      (*17*) if not AC_x(ITO, 'IND_FRT', IND_FRT) then
      (*18*) if not AC_f(ITO, 'VL_SERV', VL_SERV, True, True) then
      (*19*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
      (*20*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*21*) if not AC_f(ITO, 'VL_NT', VL_NT, True, True) then
      (*22*) if not AC_x(ITO, 'COD_INF', COD_INF) then
      (*23*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
{
    //
    if QrEFD_D110.RecordCount <> 1 then
      Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG D110 do D100');
}
    //
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_D);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_D100_Novo(): Boolean;
  function GeraRegistro_D100_Atual(): Boolean;
  var
    ITO: TIndicadorDoTipoDeOperacao;
    //
    DT_DOC, DT_A_P: TDateTime;
    COD_SIT, TP_CTE, NUM_DOC: Integer;
    IND_OPER, IND_EMIT, CHV_CTE, CHV_CTE_REF, COD_INF, COD_CTA,
    COD_PART, COD_MOD, SER, SUB, IND_FRT: String;
    VL_DESC, VL_DOC, VL_SERV, VL_BC_ICMS, VL_ICMS, VL_NT: Double;
    COD_MUN_ORIG, COD_MUN_DEST: Integer;
  begin
    FBloco := 'D';
    FRegistro := 'D100';
    Result := False;
    PB1.Max := 0;
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    IND_OPER    := QrEfdInnCTsCabIND_OPER.Value;
    IND_EMIT    := QrEfdInnCTsCabIND_EMIT.Value;
    //COD_PART    := Geral.FF0(QrEfdInnCTsCabTerceiro.Value);
    COD_PART    := ObtemCOD_PART(QrEfdInnCTsCabTerceiro.Value);
    COD_MOD     := QrEfdInnCTsCabCOD_MOD.Value;
    while Length(COD_MOD) < 2 do
      COD_MOD := '0' + COD_MOD;
    COD_SIT     := QrEfdInnCTsCabCOD_SIT.Value;
    SER         := QrEfdInnCTsCabSER.Value;
    SUB         := QrEfdInnCTsCabSUB.Value;
    NUM_DOC     := QrEfdInnCTsCabNUM_DOC.Value;
    CHV_CTE     := QrEfdInnCTsCabCHV_CTE.Value;
  //  DT_DOC      := Geral.IMV(Geral.FDT(QrEfdInnCTsCabDT_DOC.Value, 23));
  //  DT_A_P      := Geral.IMV(Geral.FDT(QrEfdInnCTsCabDT_A_P.Value, 23));
    DT_DOC      := QrEfdInnCTsCabDT_DOC.Value;
    DT_A_P      := QrEfdInnCTsCabDT_A_P.Value;
    //
    TP_CTE      := QrEfdInnCTsCabTP_CT_e.Value;
    CHV_CTE_REF := QrEfdInnCTsCabCHV_CTE_REF.Value;
    VL_DOC      := QrEfdInnCTsCabVL_DOC.Value;
    VL_DESC     := QrEfdInnCTsCabVL_DESC.Value;
    IND_FRT     := QrEfdInnCTsCabIND_FRT.Value;
    VL_SERV     := QrEfdInnCTsCabVL_SERV.Value;
    VL_BC_ICMS  := QrEfdInnCTsCabVL_BC_ICMS.Value;
    VL_ICMS     := QrEfdInnCTsCabVL_ICMS.Value;
    VL_NT       := QrEfdInnCTsCabVL_NT.Value;
    COD_INF     := QrEfdInnCTsCabCOD_INF.Value;
    COD_CTA     := QrEfdInnCTsCabCOD_CTA.Value;
    COD_MUN_ORIG := QrEfdInnCTsCabCOD_MUN_ORIG.Value;
    COD_MUN_DEST := QrEfdInnCTsCabCOD_MUN_DEST.Value;
     //
    if IND_OPER = '0' then
      ITO := itoEntrada
    else
      ITO := itoSaida;
   try
      PredefineLinha();
      FLinD100 := FLinArq;
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
         //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'IND_OPER', IND_OPER) then
        (*03*) if not AC_x(ITO, 'IND_EMIT', IND_EMIT) then
        (*04*) if not AC_x(ITO, 'COD_PART', COD_PART) then
        (*05*) if not AC_x(ITO, 'COD_MOD', COD_MOD) then
        (*06*) if not AC_n(ITO, 'COD_SIT', Geral.FFN(COD_SIT, 2)) then
        (*07*) if not AC_x(ITO, 'SER', SER) then
        (*08*) if not AC_x(ITO, 'SUB', SUB) then
        (*09*) if not AC_i(ITO, 'NUM_DOC', NUM_DOC, False, True) then
        (*10*) if not AC_n(ITO, 'CHV_CTE', CHV_CTE) then
        (*11*) if not AC_d(ITO, 'DT_DOC', DT_DOC, False, True, 23) then
        (*12*) if not AC_d(ITO, 'DT_A_P', DT_A_P, False, True, 23) then
        (*13*) if not AC_i(ITO, 'TP_CTE', TP_CTE, True, True) then
        (*14*) if not AC_n(ITO, 'CHV_CTE_REF', CHV_CTE_REF) then
        (*15*) if not AC_f(ITO, 'VL_DOC', VL_DOC, True, True) then
        (*16*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, True) then
        (*17*) if not AC_x(ITO, 'IND_FRT', IND_FRT) then
        (*18*) if not AC_f(ITO, 'VL_SERV', VL_SERV, True, True) then
        (*19*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
        (*20*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
        (*21*) if not AC_f(ITO, 'VL_NT', VL_NT, True, True) then
        (*22*) if not AC_x(ITO, 'COD_INF', COD_INF) then
        (*23*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
        (*24*) if not AC_i(ITO, 'COD_MUN_ORIG', COD_MUN_ORIG, True, True) then
        (*25*) if not AC_i(ITO, 'COD_MUN_DEST', COD_MUN_DEST, True, True) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
  {
      //
      if QrEFD_D110.RecordCount <> 1 then
        Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG D110 do D100');
  }
      //
      //AdicionaBLC(1);
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_D);
    end;
  end;
  //
  function GeraRegistro_D190_Atual(): Boolean;
  var
    ITO: TIndicadorDoTipoDeOperacao;
    //
    N, CST_ICMS, CFOP: Integer;
    ALIQ_ICMS, VL_OPR, VL_BC_ICMS, VL_ICMS, VL_RED_BC: Double;
    COD_OBS: String;
  begin
    FBloco := 'D';
    FRegistro := 'D190';
    Result := False;
    PB1.Max := 0;
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    CST_ICMS   := QrEfdInnCTsCabCST_ICMS.Value;
    CFOP       := QrEfdInnCTsCabCFOP.Value;
    ALIQ_ICMS  := QrEfdInnCTsCabALIQ_ICMS.Value;
    VL_OPR     := QrEfdInnCTsCabVL_DOC.Value;
    VL_BC_ICMS := QrEfdInnCTsCabVL_BC_ICMS.Value;
    VL_ICMS    := QrEfdInnCTsCabVL_ICMS.Value;
    VL_RED_BC  := QrEfdInnCTsCabVL_RED_BC.Value;
    COD_OBS    := '';//QrEfdInnCTsCabCOD_OBS.Value;
     //
    if QrEfdInnCTsCabIND_OPER.Value = '0' then
      ITO := itoEntrada
    else
      ITO := itoSaida;
   try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
         //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_i(ITO, 'CST_ICMS', CST_ICMS, True, True) then
        (*03*) if not AC_i(ITO, 'CFOP', CFOP, False, True) then
        (*04*) if not AC_f(ITO, 'ALIQ_ICMS', ALIQ_ICMS, True, True) then
        (*05*) if not AC_f(ITO, 'VL_OPR', VL_OPR, True, True) then
        (*06*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
        (*07*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
        (*08*) if not AC_f(ITO, 'VL_RED_BC', VL_RED_BC, True, True) then
        (*09*) if not AC_x(ITO, 'COD_OBS', COD_OBS) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
  (*
      //
      if ??? then
        Grava_SpedEfdIcmsIpiErrs(??, '????????');
  *)
      //
      //AdicionaBLC(1);
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'D100';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinD100;
      //
      FQTD_LIN_D190 := FQTD_LIN_D190 + 1;
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_D);
    end;
  end;
begin
  QrEfdInnCTsCab.First;
  while not QrEfdInnCTsCab.Eof do
  begin
    GeraRegistro_D100_Atual();
    GeraRegistro_D190_Atual();
    GeraRegistro_D195(QrEfdInnCTsCabControle.Value);
    //
    QrEfdInnCTsCab.Next;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_D190(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  //
  N, CST_ICMS, CFOP: Integer;
  ALIQ_ICMS, VL_OPR, VL_BC_ICMS, VL_ICMS, VL_RED_BC: Double;
  COD_OBS: String;
begin
  FBloco := 'D';
  FRegistro := 'D190';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  CST_ICMS   := QrEFD_D100CST_ICMS.Value;
  CFOP       := QrEFD_D100CFOP.Value;
  ALIQ_ICMS  := QrEFD_D100ALIQ_ICMS.Value;
  VL_OPR     := QrEFD_D100VL_DOC.Value;
  VL_BC_ICMS := QrEFD_D100VL_BC_ICMS.Value;
  VL_ICMS    := QrEFD_D100VL_ICMS.Value;
  VL_RED_BC  := QrEFD_D100VL_RED_BC.Value;
  COD_OBS    := '';//QrEFD_D100COD_OBS.Value;
   //
  if QrEFD_D100IND_OPER.Value = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
 try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
       //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'CST_ICMS', CST_ICMS, True, True) then
      (*03*) if not AC_i(ITO, 'CFOP', CFOP, False, True) then
      (*04*) if not AC_f(ITO, 'ALIQ_ICMS', ALIQ_ICMS, True, True) then
      (*05*) if not AC_f(ITO, 'VL_OPR', VL_OPR, True, True) then
      (*06*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
      (*07*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*08*) if not AC_f(ITO, 'VL_RED_BC', VL_RED_BC, True, True) then
      (*09*) if not AC_x(ITO, 'COD_OBS', COD_OBS) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
(*
    //
    if ??? then
      Grava_SpedEfdIcmsIpiErrs(??, '????????');
*)
    //
    //AdicionaBLC(1);
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'D100';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinD100;
    //
    FQTD_LIN_D190 := FQTD_LIN_D190 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_D);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_D195(EfdInnCTsCabControle: Integer): Boolean;
  function GeraRegistroD195_CAT66SP2018(): Boolean;
  var
    ITO: TIndicadorDoTipoDeOperacao;
    //
    COD_OBS, TXT_COMPL: String;
    //
    N, I, Seq: Integer;
  begin
    FBloco := 'D';
    FRegistro := 'D195';
    Result := False;
  (* Configurado para a NF e n�o para o itens
    PB1.Max := 0;
    PB1.Position := 0;
  *)
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    //ObtemITO_InNF(EhE, IND_OPER, tpNF, ITO);
    ITO := itoEntrada;
    //
    Seq := 0;
    for I := 1 to Length(FLstCAT66SP2018) do
    begin
      if FLstCAT66SP2018[I] = QrCAT66SP2018CFOP.Value then
      begin
        Seq := I;
        Break;
      end;
    end;
    COD_OBS     := GeraCOD_OBS_CAT66SP2018(Seq);
    TXT_COMPL   := 'CFOP ' + Geral.FF0(QrCAT66SP2018CFOP.Value);
    try
      PredefineLinha();
      FLinD195 := FLinArq;
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_OBS',  COD_OBS) then
        (*03*) if not AC_x(ITO, 'TXT_COMPL',  TXT_COMPL) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //

      //AdicionaBLC(1);
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'D100';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinC100;
      //
      FQTD_LIN_D195 := FQTD_LIN_D195 + 1;
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_D);
    end;
  end;
  function GeraRegistroD197_CAT66SP2018(): Boolean;
  var
    ITO: TIndicadorDoTipoDeOperacao;
    //
    COD_AJ, DESCR_COMPL_AJ: String;
    //
    N: Integer;
  begin
    FBloco := 'D';
    FRegistro := 'D197';
    Result := False;
  (* Configurado para a NF e n�o para o itens
    PB1.Max := 0;
    PB1.Position := 0;
  *)
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    //ObtemITO_InNF(EhE, IND_OPER, tpNF, ITO);
    ITO := itoEntrada;
    //
    COD_AJ           := 'SP90090104';
    DESCR_COMPL_AJ   := Geral.FF0(QrCAT66SP2018CFOP.Value);
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_AJ',  COD_AJ) then
        (*03*) if not AC_x(ITO, 'DESCR_COMPL_AJ',  DESCR_COMPL_AJ) then
        (*04*) if not AC_x(ITO, 'COD_ITEM',  EmptyStr) then
        (*05*) if not AC_f(ITO, 'VL_BC_ICMS',  0.00, True, True) then
        (*06*) if not AC_f(ITO, 'ALIQ_ICMS',  0.00, True, True) then
        (*07*) if not AC_f(ITO, 'VL_ICMS',  0.00, True, True) then
        (*08*) if not AC_f(ITO, 'VL_OUTROS',  QrCAT66SP2018AjusteVL_OUTROS.Value, False, True) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //

      //AdicionaBLC(1);
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'D100';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinC100;
      //
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'D195';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinD195;
      //
      FQTD_LIN_D197 := FQTD_LIN_D197 + 1;
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_D);
    end;
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCAT66SP2018, Dmod.MyDB, [
  'SELECT CFOP, VL_NT AjusteVL_OUTROS ',
  'FROM efdinnctscab  ',
  'WHERE Controle=' + Geral.FF0(EfdInnCTsCabControle),
  'AND VL_NT > 0.00',
  'GROUP BY CFOP ',
  '']);
  //
  QrCAT66SP2018.First;
  while not QrCAT66SP2018.Eof do
  begin
    GeraRegistroD195_CAT66SP2018();
    GeraRegistroD197_CAT66SP2018();
    //
    QrCAT66SP2018.Next;
  end;
  //
  Result := True;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_D197(): Boolean;
begin
 //
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_D500(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  //
  COD_SIT: String;
  NUM_DOC: Integer;
  DT_DOC, DT_A_P: TDateTime;
  IND_OPER, IND_EMIT, COD_INF, COD_CTA,
  COD_PART, COD_MOD, SER, SUB: String;
  VL_DESC, VL_DOC, VL_SERV, VL_SERV_NT, VL_TERC, VL_DA, VL_BC_ICMS, VL_ICMS,
  VL_PIS, VL_COFINS: Double;
  TP_ASSINANTE: String;
begin
  FBloco := 'D';
  FRegistro := 'D500';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  IND_OPER     := QrEFD_D500IND_OPER.Value;
  IND_EMIT     := QrEFD_D500IND_EMIT.Value;
  //COD_PART     := Geral.FF0(QrEFD_D500Terceiro.Value);
  COD_PART     := ObtemCOD_PART(QrEFD_D500Terceiro.Value);
  COD_MOD      := QrEFD_D500COD_MOD.Value;
  COD_SIT      := QrEFD_D500COD_SIT.Value;
  SER          := QrEFD_D500SER.Value;
  SUB          := QrEFD_D500SUB.Value;
  NUM_DOC      := QrEFD_D500NUM_DOC.Value;
  DT_DOC       := QrEFD_D500DT_DOC.Value;
  DT_A_P       := QrEFD_D500DT_A_P.Value;
  VL_DOC       := QrEFD_D500VL_DOC.Value;
  VL_DESC      := QrEFD_D500VL_DESC.Value;
  VL_SERV      := QrEFD_D500VL_SERV.Value;
  VL_SERV_NT   := QrEFD_D500VL_SERV_NT.Value;
  VL_TERC      := QrEFD_D500VL_TERC.Value;
  VL_DA        := QrEFD_D500VL_DA.Value;
  VL_BC_ICMS   := QrEFD_D500VL_BC_ICMS.Value;
  VL_ICMS      := QrEFD_D500VL_ICMS.Value;
  COD_INF      := QrEFD_D500COD_INF.Value;
  VL_PIS       := QrEFD_D500VL_PIS.Value;
  VL_COFINS    := QrEFD_D500VL_COFINS.Value;
  COD_CTA      := QrEFD_D500COD_CTA.Value;
  TP_ASSINANTE := QrEFD_D500TP_ASSINANTE.Value;
   //
  if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
 try
    PredefineLinha();
    FLinD500 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
       //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_OPER', IND_OPER) then
      (*03*) if not AC_x(ITO, 'IND_EMIT', IND_EMIT) then
      (*04*) if not AC_x(ITO, 'COD_PART', COD_PART) then
      (*05*) if not AC_x(ITO, 'COD_MOD', COD_MOD) then
      (*06*) if not AC_n(ITO, 'COD_SIT', COD_SIT) then
      (*07*) if not AC_x(ITO, 'SER', SER) then
      (*08*) if not AC_n(ITO, 'SUB', SUB) then
      (*09*) if not AC_i(ITO, 'NUM_DOC', NUM_DOC, False, True) then
      (*10*) if not AC_d(ITO, 'DT_DOC', DT_DOC, False, True, 23) then
      (*11*) if not AC_d(ITO, 'DT_A_P', DT_A_P, False, True, 23) then
      (*12*) if not AC_f(ITO, 'VL_DOC', VL_DOC, True, True) then
      (*13*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, True) then
      (*14*) if not AC_f(ITO, 'VL_SERV', VL_SERV, True, True) then
      (*15*) if not AC_f(ITO, 'VL_SERV_NT', VL_SERV_NT, True, True) then
      (*16*) if not AC_f(ITO, 'VL_TERC', VL_TERC, True, True) then
      (*17*) if not AC_f(ITO, 'VL_DA', VL_DA, True, True) then
      (*18*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
      (*19*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*20*) if not AC_x(ITO, 'COD_INF', COD_INF) then
      (*21*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, True) then
      (*22*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, True) then
      (*23*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
      (*24*) if not AC_n(ITO, 'TP_ASSINANTE', TP_ASSINANTE) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
{
    //
    if QrEFD_D510.RecordCount <> 1 then
      Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG D510 do D500');
}
    //
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_D);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_D590(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  //
  N, CST_ICMS, CFOP: Integer;
  ALIQ_ICMS, VL_OPR, VL_BC_ICMS, VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_RED_BC: Double;
  COD_OBS: String;
begin
  FBloco := 'D';
  FRegistro := 'D590';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  CST_ICMS      := Geral.IMV(QrEFD_D500CST_ICMS.Value);
  CFOP          := Geral.IMV(QrEFD_D500CFOP.Value);
  ALIQ_ICMS     := QrEFD_D500ALIQ_ICMS.Value;
  VL_OPR        := QrEFD_D500VL_DOC.Value;
  VL_BC_ICMS    := QrEFD_D500VL_BC_ICMS.Value;
  VL_ICMS       := QrEFD_D500VL_ICMS.Value;
  VL_BC_ICMS_ST := 0.00; // QrEFD_D500VL_BC_ICMS_ST.Value;
  VL_ICMS_ST    := 0.00; // QrEFD_D500VL_ICMS_ST.Value;
  VL_RED_BC     := QrEFD_D500VL_RED_BC.Value;
  COD_OBS       := '';//QrEFD_D500COD_OBS.Value;
   //
  if QrEFD_D500IND_OPER.Value = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
 try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
       //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'CST_ICMS', CST_ICMS, True, True) then
      (*03*) if not AC_i(ITO, 'CFOP', CFOP, False, True) then
      (*04*) if not AC_f(ITO, 'ALIQ_ICMS', ALIQ_ICMS, True, True) then
      (*05*) if not AC_f(ITO, 'VL_OPR', VL_OPR, True, True) then
      (*06*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
      (*07*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*08*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, True) then
      (*09*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, True) then
      (*10*) if not AC_f(ITO, 'VL_RED_BC', VL_RED_BC, True, True) then
      (*11*) if not AC_x(ITO, 'COD_OBS', COD_OBS) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //

(*
    //
    if ??? then
      Grava_SpedEfdIcmsIpiErrs(??, '????????');
*)
    //
    //AdicionaBLC(1);
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'D500';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinD500;
    //
    FQTD_LIN_D590 := FQTD_LIN_D590 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_D);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_D990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'D';
  FRegistro := 'D990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_D', FQTD_LIN_D + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_D);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_E001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'E';
  FRegistro := 'E001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_E);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_E100(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_INI, DT_FIN: TDateTime;
begin
  FBloco := 'E';
  FRegistro := 'E100';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  //DT_INI := Geral.IMV(Geral.FDT(QrEFD_E100DT_INI.Value, 23));
  //DT_FIN := Geral.IMV(Geral.FDT(QrEFD_E100DT_FIN.Value, 23));
  DT_INI := QrEFD_E100DT_INI.Value;
  DT_FIN := QrEFD_E100DT_FIN.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_INI', DT_INI, False, False, 23) then
      (*03*) if not AC_d(ITO, 'DT_FIN', DT_FIN, False, False, 23) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //
    if QrEFD_E110.RecordCount <> 1 then
      Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG E110 do E100');
    //
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_E);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_E110(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  VL_TOT_DEBITOS,
  VL_AJ_DEBITOS, VL_TOT_AJ_DEBITOS, VL_ESTORNOS_CRED,
  VL_TOT_CREDITOS, VL_AJ_CREDITOS, VL_TOT_AJ_CREDITOS,
  VL_ESTORNOS_DEB, VL_SLD_CREDOR_ANT, VL_SLD_APURADO,
  VL_TOT_DED, VL_ICMS_RECOLHER, VL_SLD_CREDOR_TRANSPORTAR,
  DEB_ESP: Double;
begin
  if CkRecriaRegAotmaticos.Checked then
    RecriaRegistroEFD_E110();
  //
  FBloco := 'E';
  FRegistro := 'E110';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  VL_TOT_DEBITOS :=      QrEFD_E110VL_TOT_DEBITOS.Value;
  VL_AJ_DEBITOS :=       QrEFD_E110VL_AJ_DEBITOS.Value;
  VL_TOT_AJ_DEBITOS :=   QrEFD_E110VL_TOT_AJ_DEBITOS.Value;
  VL_ESTORNOS_CRED :=    QrEFD_E110VL_ESTORNOS_CRED.Value;
  VL_TOT_CREDITOS :=     QrEFD_E110VL_TOT_CREDITOS.Value;
  VL_AJ_CREDITOS :=      QrEFD_E110VL_AJ_CREDITOS.Value;
  VL_TOT_AJ_CREDITOS :=  QrEFD_E110VL_TOT_AJ_CREDITOS.Value;
  VL_ESTORNOS_DEB :=     QrEFD_E110VL_ESTORNOS_DEB.Value;
  VL_SLD_CREDOR_ANT :=   QrEFD_E110VL_SLD_CREDOR_ANT.Value;
  VL_SLD_APURADO :=      QrEFD_E110VL_SLD_APURADO.Value;
  VL_TOT_DED :=          QrEFD_E110VL_TOT_DED.Value;
  VL_ICMS_RECOLHER :=    QrEFD_E110VL_ICMS_RECOLHER.Value;
  VL_SLD_CREDOR_TRANSPORTAR :=  QrEFD_E110VL_SLD_CREDOR_TRANSPORTAR.Value;
  DEB_ESP :=             QrEFD_E110DEB_ESP.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_f(ITO, 'VL_TOT_DEBITOS', VL_TOT_DEBITOS, True, True) then
      (*03*) if not AC_f(ITO, 'VL_AJ_DEBITOS', VL_AJ_DEBITOS, True, True) then
      (*04*) if not AC_f(ITO, 'VL_TOT_AJ_DEBITOS', VL_TOT_AJ_DEBITOS, True, True) then
      (*05*) if not AC_f(ITO, 'VL_ESTORNOS_CRED', VL_ESTORNOS_CRED, True, True) then
      (*06*) if not AC_f(ITO, 'VL_TOT_CREDITOS', VL_TOT_CREDITOS, True, True) then
      (*07*) if not AC_f(ITO, 'VL_AJ_CREDITOS', VL_AJ_CREDITOS, True, True) then
      (*08*) if not AC_f(ITO, 'VL_TOT_AJ_CREDITOS', VL_TOT_AJ_CREDITOS, True, True) then
      (*09*) if not AC_f(ITO, 'VL_ESTORNOS_DEB', VL_ESTORNOS_DEB, True, True) then
      (*10*) if not AC_f(ITO, 'VL_SLD_CREDOR_ANT', VL_SLD_CREDOR_ANT, True, True) then
      (*11*) if not AC_f(ITO, 'VL_SLD_APURADO', VL_SLD_APURADO, True, True) then
      (*12*) if not AC_f(ITO, 'VL_TOT_DED', VL_TOT_DED, True, True) then
      (*13*) if not AC_f(ITO, 'VL_ICMS_RECOLHER', VL_ICMS_RECOLHER, True, True) then
      (*14*) if not AC_f(ITO, 'VL_SLD_CREDOR_TRANSPORTAR', VL_SLD_CREDOR_TRANSPORTAR, True, True) then
      (*15*) if not AC_f(ITO, 'DEB_ESP', DEB_ESP, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_E);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_E111(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_AJ_APUR, DESCR_COMPL_AJ: String;
  VL_AJ_APUR: Double;
begin
  FBloco := 'E';
  FRegistro := 'E111';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  COD_AJ_APUR :=         QrEFD_E111COD_AJ_APUR.Value;
  DESCR_COMPL_AJ :=      QrEFD_E111DESCR_COMPL_AJ.Value;
  VL_AJ_APUR :=          QrEFD_E111VL_AJ_APUR.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'COD_AJ_APUR', COD_AJ_APUR) then
      (*03*) if not AC_x(ITO, 'DESCR_COMPL_AJ', DESCR_COMPL_AJ) then
      (*04*) if not AC_f(ITO, 'VL_AJ_APUR', VL_AJ_APUR, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    FQTD_LIN_E111 := FQTD_LIN_E111 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_E);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_E112(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  NUM_DA, NUM_PROC, IND_PROC, PROC, TXT_COMPL: String;
begin
  FBloco := 'E';
  FRegistro := 'E112';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  NUM_DA    := QrEFD_E112NUM_DA.Value;
  NUM_PROC  := QrEFD_E112NUM_PROC.Value;
  IND_PROC  := QrEFD_E112IND_PROC.Value;
  PROC      := QrEFD_E112PROC.Value;
  TXT_COMPL := QrEFD_E112TXT_COMPL.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'NUM_DA', NUM_DA) then
      (*03*) if not AC_x(ITO, 'NUM_PROC', NUM_PROC) then
      (*04*) if not AC_x(ITO, 'IND_PROC', IND_PROC) then
      (*05*) if not AC_x(ITO, 'PROC', PROC) then
      (*06*) if not AC_x(ITO, 'TXT_COMPL', TXT_COMPL) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    FQTD_LIN_E112 := FQTD_LIN_E112 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_E);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_E113(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_PART, COD_MOD, SER, SUB: String;
  NUM_DOC: Integer;
  DT_DOC: TDateTime;
  COD_ITEM: String;
  VL_AJ_ITEM: Double;
begin
  FBloco := 'E';
  FRegistro := 'E113';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  //COD_PART    := QrEFD_E113COD_PART.Value;
  COD_PART    := ObtemCOD_PART(Geral.IMV(QrEFD_E113COD_PART.Value));
  COD_MOD     := QrEFD_E113COD_MOD.Value;
  SER         := QrEFD_E113SER.Value;
  SUB         := QrEFD_E113SUB.Value;
  NUM_DOC     := QrEFD_E113NUM_DOC.Value;
  DT_DOC      := QrEFD_E113DT_DOC.Value;
  COD_ITEM    := ObtemCOD_ITEM(Geral.IMV(QrEFD_E113COD_ITEM.Value));
  VL_AJ_ITEM  := QrEFD_E113VL_AJ_ITEM.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'COD_PART', COD_PART) then
      (*03*) if not AC_x(ITO, 'COD_MOD', COD_MOD) then
      (*04*) if not AC_x(ITO, 'SER', SER) then
      (*05*) if not AC_x(ITO, 'SUB', SUB) then
      (*06*) if not AC_i(ITO, 'NUM_DOC', NUM_DOC, False, True) then
      (*07*) if not AC_d(ITO, 'DT_DOC', DT_DOC, False, True, 23) then
      (*08*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*09*) if not AC_f(ITO, 'VL_AJ_ITEM', VL_AJ_ITEM, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    FQTD_LIN_E113 := FQTD_LIN_E113 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_E);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_E115(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_INF_ADIC: String;
  VL_INF_ADIC: Double;
  DESCR_COMPL_AJ: String;
begin
  FBloco := 'E';
  FRegistro := 'E115';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  COD_INF_ADIC   := QrEFD_E115COD_INF_ADIC.Value;
  VL_INF_ADIC    := QrEFD_E115VL_INF_ADIC.Value;
  DESCR_COMPL_AJ := QrEFD_E115DESCR_COMPL_AJ.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'COD_INF_ADIC', COD_INF_ADIC) then
      (*03*) if not AC_f(ITO, 'VL_INF_ADIC', VL_INF_ADIC, True, True) then
      (*04*) if not AC_x(ITO, 'DESCR_COMPL_AJ', DESCR_COMPL_AJ) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    FQTD_LIN_E115 := FQTD_LIN_E115 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_E);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_E116(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_OR, COD_REC, NUM_PROC, IND_PROC, PROC, TXT_COMPL: String;
  VL_OR: Double;
  DT_VCTO: TDateTime;
  MES_REF: TDateTime;
begin
  FBloco := 'E';
  FRegistro := 'E116';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  COD_OR    := QrEFD_E116COD_OR.Value;
  VL_OR     := QrEFD_E116VL_OR.Value;
  DT_VCTO   := QrEFD_E116DT_VCTO.Value;
  COD_REC   := QrEFD_E116COD_REC.Value;
  NUM_PROC  := QrEFD_E116NUM_PROC.Value;
  IND_PROC  := QrEFD_E116IND_PROC.Value;
  PROC      := QrEFD_E116PROC.Value;
  TXT_COMPL := QrEFD_E116TXT_COMPL.Value;
  MES_REF   := QrEFD_E116MES_REF.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'COD_OR', COD_OR) then
      (*03*) if not AC_f(ITO, 'VL_OR', VL_OR, True, True) then
      (*04*) if not AC_d(ITO, 'DT_VCTO', DT_VCTO, False, False, 23) then
      (*05*) if not AC_x(ITO, 'COD_REC', COD_REC) then
      (*06*) if not AC_x(ITO, 'NUM_PROC', NUM_PROC) then
      (*07*) if not AC_x(ITO, 'IND_PROC', IND_PROC) then
      (*08*) if not AC_x(ITO, 'PROC', PROC) then
      (*09*) if not AC_x(ITO, 'TXT_COMPL', TXT_COMPL) then
      (*10*) if not AC_d(ITO, 'MES_REF', MES_REF, True, True, 24) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    FQTD_LIN_E116 := FQTD_LIN_E116 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_E);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_E500: Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_INI, DT_FIN: TDateTime;
  IND_APUR: String;
begin
  FBloco := 'E';
  FRegistro := 'E500';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  DT_INI   := QrEFD_E500DT_INI.Value;
  DT_FIN   := QrEFD_E500DT_FIN.Value;
  IND_APUR := QrEFD_E500IND_APUR.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_APUR', IND_APUR) then
      (*03*) if not AC_d(ITO, 'DT_INI', DT_INI, False, False, 23) then
      (*04*) if not AC_d(ITO, 'DT_FIN', DT_FIN, False, False, 23) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //
(*
    if QrEFD_E510.RecordCount < 1 then
      Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG E110 do E500');
*)
    //
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_E);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_E510(): Boolean;
var
  Qry: TmySQLQuery;
  DtApurIni, DtApurFim: String;
  //
  REG, CST_IPI: String;
  E500, CFOP: Integer;
  VL_CONT_IPI, VL_BC_IPI, VL_IPI: Double;
  //
  procedure AdicionaAtual();
  const
    ITO = itoNaoAplicavel;
  var
    DT_INI, DT_FIN: TDateTime;
    IND_APUR: String;
  begin
    FBloco := 'E';
    FRegistro := 'E510';
    Result := False;
    PB1.Max := 0;
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_i(ITO, 'CFOP', CFOP, False, True) then
        (*03*) if not AC_x(ITO, 'CST_IPI', CST_IPI) then
        (*04*) if not AC_f(ITO, 'VL_CONT_IPI', VL_CONT_IPI, True, True) then
        (*05*) if not AC_f(ITO, 'VL_BC_IPI', VL_BC_IPI, True, True) then
        (*06*) if not AC_f(ITO, 'VL_IPI', VL_IPI, True, True) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
      //
  (*
      if QrEFD_E510.RecordCount < 1 then
        Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG E110 do E500');
  *)
      //
      //AdicionaBLC(1);
      FQTD_LIN_E510 := FQTD_LIN_E510 + 1;
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_E);
    end;
  end;
  //
begin
  Result    := False;
  DtApurIni := Geral.FDT(QrEFD_E500DT_INI.Value, 1);
  DtApurFim := Geral.FDT(QrEFD_E500DT_FIN.Value, 1);
  E500      := QrEFD_E500LinArq.Value;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    MeErros.Lines.Add('E510 - Fazer das vendas tambem !! Registro C100!');
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT IND_APUR, CFOP, CST_IPI,  ',
    'SUM(VL_BC_IPI) VL_BC_IPI, SUM(VL_IPI) VL_IPI   ',
    'FROM spedefdicmsipic170  ',
    'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    'AND Empresa=' + Geral.FF0(QrEFD_E500Empresa.Value),
    'AND AnoMes=' + Geral.FF0(QrEFD_E500AnoMes.Value),
    'AND IND_APUR="' + QrEFD_E500IND_APUR.Value + '" ',
    'AND C100 IN (  ',
    '  SELECT LinArq  ',
    '  FROM spedefdicmsipic100  ',
    '  WHERE ImporExpor=' + Geral.FF0(FImporExpor),
    '  AND Empresa=' + Geral.FF0(QrEFD_E500Empresa.Value),
    '  AND AnoMes=' + Geral.FF0(QrEFD_E500AnoMes.Value),
    '  AND DT_E_S BETWEEN "' + DtApurIni + '" AND "' + DtApurFim + '"',
    '  AND COD_SIT IN (00, 06, 08)   ',
    '  AND IND_OPER="0" ', // Entrada
    ')  ',
    'GROUP BY IND_APUR, CFOP, CST_IPI  ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      CST_IPI     := Qry.FieldByName('CST_IPI').AsString;
      CFOP        := Qry.FieldByName('CFOP').AsInteger;
      VL_CONT_IPI := 0; // Ver o que fazer!! Qry.FieldByName('VL_CONT_IPI').AsFloat;
      VL_BC_IPI   := Qry.FieldByName('VL_BC_IPI').AsFloat;
      VL_IPI      := Qry.FieldByName('VL_IPI').AsFloat;
      //
      AdicionaAtual();
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  AdicionaBLC(FQTD_LIN_E510, 'E510');
  Result := True;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_E520(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  VL_SD_ANT_IPI, VL_DEB_IPI, VL_CRED_IPI, VL_OD_IPI, VL_OC_IPI, VL_SC_IPI,
  VL_SD_IPI: Double;
  SQLType: TSQLType;
begin
  if CkRecriaRegAotmaticos.Checked then
    RecriaRegistroEFD_E520();
  //
  FBloco := 'E';
  FRegistro := 'E520';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  VL_SD_ANT_IPI  := QrEFD_E520VL_SD_ANT_IPI.Value;
  VL_DEB_IPI     := QrEFD_E520VL_DEB_IPI.Value;
  VL_CRED_IPI    := QrEFD_E520VL_CRED_IPI.Value;
  VL_OD_IPI      := QrEFD_E520VL_OD_IPI.Value;
  VL_OC_IPI      := QrEFD_E520VL_OC_IPI.Value;
  VL_SC_IPI      := QrEFD_E520VL_SC_IPI.Value;
  VL_SD_IPI      := QrEFD_E520VL_SD_IPI.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_f(ITO, 'VL_SD_ANT_IPI', VL_SD_ANT_IPI, True, True) then
      (*03*) if not AC_f(ITO, 'VL_DEB_IPI'   , VL_DEB_IPI   , True, True) then
      (*04*) if not AC_f(ITO, 'VL_CRED_IPI'  , VL_CRED_IPI  , True, True) then
      (*05*) if not AC_f(ITO, 'VL_OD_IPI'    , VL_OD_IPI    , True, True) then
      (*06*) if not AC_f(ITO, 'VL_OC_IPI'    , VL_OC_IPI    , True, True) then
      (*07*) if not AC_f(ITO, 'VL_SC_IPI'    , VL_SC_IPI    , True, True) then
      (*08*) if not AC_f(ITO, 'VL_SD_IPI'    , VL_SD_IPI    , True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    FQTD_LIN_E520 := FQTD_LIN_E520 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_E);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_E530(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  IND_AJ, COD_AJ, IND_DOC, NUM_DOC, DESCR_AJ: String;
  VL_AJ: Double;
  SQLType: TSQLType;
begin
  //
  FBloco := 'E';
  FRegistro := 'E530';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  IND_AJ         := QrEFD_E530IND_AJ.Value;
  VL_AJ          := QrEFD_E530VL_AJ.Value;
  COD_AJ         := QrEFD_E530COD_AJ.Value;
  IND_DOC        := QrEFD_E530IND_DOC.Value;
  NUM_DOC        := QrEFD_E530NUM_DOC.Value;
  DESCR_AJ       := QrEFD_E530DESCR_AJ.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_AJ'   , IND_AJ) then
      (*03*) if not AC_f(ITO, 'VL_AJ'    , VL_AJ, True, True) then
      (*04*) if not AC_x(ITO, 'COD_AJ'   , COD_AJ) then
      (*05*) if not AC_x(ITO, 'IND_DOC'  , IND_DOC) then
      (*06*) if not AC_x(ITO, 'NUM_DOC'  , NUM_DOC) then
      (*07*) if not AC_x(ITO, 'DESCR_AJ' , DESCR_AJ) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    FQTD_LIN_E530 := FQTD_LIN_E530 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_E);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_E990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'E';
  FRegistro := 'E990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_E', FQTD_LIN_E + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_E);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_G001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'G';
  FRegistro := 'G001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_G);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_G990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'G';
  FRegistro := 'G990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_G', FQTD_LIN_G + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_G);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_H001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'H';
  FRegistro := 'H001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_H);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_H005: Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_INV: TDateTime;
  VL_INV: Double;
  MOT_INV: String;
begin
  FBloco := 'H';
  FRegistro := 'H005';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  DT_INV   := QrEFD_H005DT_INV.Value;
  VL_INV   := QrEFD_H005VL_INV.Value;
  MOT_INV  := QrEFD_H005MOT_INV.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_INV', DT_INV, False, False, 23) then
      (*03*) if not AC_f(ITO, 'VL_INV', VL_INV, True, True) then
      (*04*) if not AC_x(ITO, 'MOT_INV', MOT_INV) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //
(*
    if QrEFD_H010.RecordCount < 1 then
      Grava_SpedEfdIcmsIpiErrs(efdexerRegFilhObrig, 'REG H010 do H005');
*)
    //
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_H);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_H010(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_ITEM, UNID, IND_PROP, COD_PART, TXT_COMPL, COD_CTA: String;
  H005: Integer;
  QTD, VL_UNIT, VL_ITEM, VL_ITEM_IR: Double;
  SQLType: TSQLType;
begin
  FBloco := 'H';
  FRegistro := 'H010';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  COD_ITEM       := ObtemCOD_ITEM(Geral.IMV(QrEFD_H010COD_ITEM.Value));
  UNID           := QrEFD_H010UNID.Value;
  QTD            := QrEFD_H010QTD.Value;
  VL_UNIT        := QrEFD_H010VL_UNIT.Value;
  VL_ITEM        := QrEFD_H010VL_ITEM.Value;
  IND_PROP       := QrEFD_H010IND_PROP.Value;
  //COD_PART       := QrEFD_H010COD_PART.Value;
  COD_PART       := ObtemCOD_PART(Geral.IMV(QrEFD_H010COD_PART.Value));
  TXT_COMPL      := QrEFD_H010TXT_COMPL.Value;
  COD_CTA        := QrEFD_H010COD_CTA.Value;
  H005           := QrEFD_H010H005.Value;
  VL_ITEM_IR     := QrEFD_H010VL_ITEM_IR.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'COD_ITEM'  , COD_ITEM) then
      (*03*) if not AC_x(ITO, 'UNID'      , UNID) then
      (*04*) if not AC_f(ITO, 'QTD'       , QTD, True, True) then
      (*05*) if not AC_f(ITO, 'VL_UNIT'   , VL_UNIT, True, True) then
      (*06*) if not AC_f(ITO, 'VL_ITEM'   , VL_ITEM, True, True) then
      (*07*) if not AC_x(ITO, 'IND_PROP'  , IND_PROP) then
      (*08*) if not AC_x(ITO, 'COD_PART'  , COD_PART) then
      (*09*) if not AC_x(ITO, 'TXT_COMPL' , TXT_COMPL) then
      (*10*) if not AC_x(ITO, 'COD_CTA'   , COD_CTA) then
      (*11*) if not AC_f(ITO, 'VL_ITEM_IR', VL_ITEM_IR, True, True) then
      (****) if not AC_i(ITO, 'H005'      , H005, False, False) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    FQTD_LIN_H010 := FQTD_LIN_H010 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_H);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_H020(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  H010, CST_ICMS: Integer;
  BC_ICMS, VL_ICMS: Double;
  SQLType: TSQLType;
begin
  FBloco := 'H';
  FRegistro := 'H020';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  CST_ICMS       := QrEFD_H020CST_ICMS.Value;
  BC_ICMS        := QrEFD_H020BC_ICMS.Value;
  VL_ICMS        := QrEFD_H020VL_ICMS.Value;
  H010           := QrEFD_H020H010.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'CST_ICMS'  , CST_ICMS, False, False) then
      (*03*) if not AC_f(ITO, 'BC_ICMS'   , BC_ICMS, True, True) then
      (*04*) if not AC_f(ITO, 'VL_ICMS'   , VL_ICMS, True, True) then
      (****) if not AC_i(ITO, 'H010'      , H010, False, False) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    FQTD_LIN_H020 := FQTD_LIN_H020 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_H);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_H990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'H';
  FRegistro := 'H990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_H', FQTD_LIN_H + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_H);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'K';
  FRegistro := 'K001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K100(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_INI, DT_FIN: TDateTime;
begin
  FBloco := 'K';
  FRegistro := 'K100';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  //DT_INI         := Geral.FDT(QrEFD_K100DT_INI.Value, 1);
  //DT_FIN         := Geral.FDT(QrEFD_K100DT_FIN.Value, 1);
  DT_INI         := QrEFD_K100DT_INI.Value;
  DT_FIN         := QrEFD_K100DT_FIN.Value;
  //
  try
    PredefineLinha();
    FLinK100 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_INI', DT_INI, False, False, 23) then
      (*03*) if not AC_d(ITO, 'DT_FIN', DT_FIN, False, False, 23) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    FQTD_LIN_K100 := FQTD_LIN_K100 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K200: Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_ITEM, IND_EST, COD_PART: String;
  DT_EST: TDateTime;
  K100, BalID, BalNum, BalItm, BalEnt, N: Integer;
  QTD: Double;
begin
  FBloco := 'K';
  FRegistro := 'K200';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  DT_EST         := QrEFD_K200DT_EST.Value;
  COD_ITEM       := ObtemCOD_ITEM(Geral.IMV(QrEFD_K200COD_ITEM.Value));
  QTD            := QrEFD_K200QTD.Value;
  IND_EST        := QrEFD_K200IND_EST.Value;
  //COD_PART       := QrEFD_K200COD_PART.Value;
  COD_PART       := ObtemCOD_PART(Geral.IMV(QrEFD_K200COD_PART.Value));
  if COD_PART = '0' then
    COD_PART := '';
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_EST', DT_EST, False, False, 23) then
      (*03*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*04*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      (*05*) if not AC_x(ITO, 'IND_EST', IND_EST) then
      (*06*) if not AC_x(ITO, 'COD_PART', COD_PART) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K100', FLinK100);
(*
    AddCampoSQL('BalID', 0);///QrEFD_K200BalID.Value);
    AddCampoSQL('BalNum', 0); //QrEFD_K200BalNum.Value);
    AddCampoSQL('BalItm', 0); //QrEFD_K200BalItm.Value);
    AddCampoSQL('BalEnt', QrEFD_K200Entidade.Value);
*)
    //
    FQTD_LIN_K200 := FQTD_LIN_K200 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K210: Boolean;
const
  ITO = itoNaoAplicavel;
var
  REG, DT_INI_OS, DT_FIN_OS: TDateTime;
  COD_DOC_OS, COD_ITEM_ORI: String;
  ID_SEK, MovimID, Codigo, MovimCod: Integer;
  QTD_ORI: Double;
begin
  FBloco := 'K';
  FRegistro := 'K210';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  DT_INI_OS      := QrEFD_K210DT_INI_OS.Value;
  DT_FIN_OS      := QrEFD_K210DT_FIN_OS.Value;
  COD_DOC_OS     := QrEFD_K210COD_DOC_OS.Value;
  COD_ITEM_ORI   := ObtemCOD_ITEM(Geral.IMV(QrEFD_K210COD_ITEM_ORI.Value));
  QTD_ORI        := QrEFD_K210QTD_ORI.Value;
  //
  try
    PredefineLinha();
    FLinK210 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_INI_OS', DT_INI_OS, False, False, 21) then
      (*03*) if not AC_d(ITO, 'DT_FIN_OS', DT_FIN_OS, False, False, 21) then
      (*04*) if not AC_x(ITO, 'COD_DOC_OS', COD_DOC_OS) then
      (*05*) if not AC_x(ITO, 'COD_ITEM_ORI', COD_ITEM_ORI) then
      (*06*) if not AC_f(ITO, 'QTD_ORI', QTD_ORI, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K100', FLinK100);
    //AddCampoSQL('ID_SEK', QrEFD_K210ID_SEK.Value);
    //AddCampoSQL('MovimID', QrEFD_K210MovimID.Value);
    //AddCampoSQL('Codigo', QrEFD_K210Codigo.Value);
    //AddCampoSQL('MovimCod', QrEFD_K210MovimCod.Value);
    //
    FQTD_LIN_K210 := FQTD_LIN_K210 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K215: Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_ITEM_DES: String;
  ID_SEK: Integer;
  QTD_DES: Double;
begin
  FBloco := 'K';
  FRegistro := 'K215';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  COD_ITEM_DES   := ObtemCOD_ITEM(Geral.IMV(QrEFD_K215COD_ITEM_DES.Value));
  QTD_DES        := QrEFD_K215QTD_DES.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'COD_ITEM_DES', COD_ITEM_DES) then
      (*03*) if not AC_f(ITO, 'QTD_DES', QTD_DES, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K210', FLinK210);
    //AddCampoSQL('ID_SEK', QrEFD_K215ID_SEK.Value);
    //
    FQTD_LIN_K215 := FQTD_LIN_K215 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K220(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_MOV: TDateTime;
  COD_ITEM_ORI, COD_ITEM_DEST: String;
  ID_SEK: Integer;
  QTD: Double;
begin
  FBloco := 'K';
  FRegistro := 'K220';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  DT_MOV         := QrEFD_K220DT_Mov.Value;
  COD_ITEM_ORI   := ObtemCOD_ITEM(Geral.IMV(QrEFD_K220COD_ITEM_ORI.Value));
  COD_ITEM_DEST  := ObtemCOD_ITEM(Geral.IMV(QrEFD_K220COD_ITEM_DEST.Value));
  QTD            := QrEFD_K220QTD.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_MOV', DT_MOV, False, False, 23) then
      (*03*) if not AC_x(ITO, 'COD_ITEM_ORI', COD_ITEM_ORI) then
      (*04*) if not AC_x(ITO, 'COD_ITEM_DEST', COD_ITEM_DEST) then
      (*05*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K100', FLinK100);
    AddCampoSQL('ID_SEK', QrEFD_K220ID_SEK.Value);
    //
    FQTD_LIN_K220 := FQTD_LIN_K220 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K230: Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_INI_OP, DT_FIN_OP: TDateTime;
  COD_DOC_OP, COD_ITEM: String;
  ID_SEK, MovimID, Codigo, MovimCod: Integer;
  QTD_ENC: Double;
begin
  FBloco := 'K';
  FRegistro := 'K230';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  DT_INI_OP      := QrEFD_K230DT_INI_OP.Value;
  DT_FIN_OP      := QrEFD_K230DT_FIN_OP.Value;
  COD_DOC_OP     := QrEFD_K230COD_DOC_OP.Value;
  COD_ITEM       := ObtemCOD_ITEM(Geral.IMV(QrEFD_K230COD_ITEM.Value));
  QTD_ENC        := QrEFD_K230QTD_ENC.Value;
  //
  try
    PredefineLinha();
    FLinK230 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_INI_OP', DT_INI_OP, False, False, 23) then
      (*03*) if not AC_d(ITO, 'DT_FIN_OP', DT_FIN_OP, False, False, 23) then
      (*04*) if not AC_x(ITO, 'COD_DOC_OP', COD_DOC_OP) then
      (*05*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*06*) if not AC_f(ITO, 'QTD_ENC', QTD_ENC, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K100', FLinK100);
    //AddCampoSQL('ID_SEK', QrEFD_K230ID_SEK.Value);
    //AddCampoSQL('MovimID', QrEFD_K230MovimID.Value);
    //AddCampoSQL('Codigo', QrEFD_K230Codigo.Value);
    //AddCampoSQL('MovimCod', QrEFD_K230MovimCod.Value);
    //
    FQTD_LIN_K230 := FQTD_LIN_K230 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K235(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_SAIDA: TDateTime;
  COD_ITEM, COD_INS_SUBST: String;
  ID_SEK: Integer;
  QTD: Double;
begin
  FBloco := 'K';
  FRegistro := 'K235';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  DT_SAIDA       := QrEFD_K235DT_SAIDA.Value;
  COD_ITEM       := ObtemCOD_ITEM(Geral.IMV(QrEFD_K235COD_ITEM.Value));
  QTD            := QrEFD_K235QTD.Value;
  COD_INS_SUBST  := QrEFD_K235COD_INS_SUBST.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_SAIDA', DT_SAIDA, False, False, 23) then
      (*03*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*04*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      (*05*) if not AC_x(ITO, 'COD_INS_SUBST', COD_INS_SUBST) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K230', FLinK230);
    AddCampoSQL('ID_SEK', QrEFD_K235ID_SEK.Value);
    //
    FQTD_LIN_K235 := FQTD_LIN_K235 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K250: Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_PROD: TDateTime;
  COD_ITEM: String;
  ID_SEK, MovimID, Codigo, MovimCod: Integer;
  QTD: Double;
begin
  FBloco := 'K';
  FRegistro := 'K250';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  DT_PROD        := QrEFD_K250DT_PROD.Value;
  COD_ITEM       := ObtemCOD_ITEM(Geral.IMV(QrEFD_K250COD_ITEM.Value));
  QTD            := QrEFD_K250QTD.Value;
  //
  try
    PredefineLinha();
    FLinK250 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_PROD', DT_PROD, False, False, 23) then
      (*03*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*04*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K100', FLinK100);
    //AddCampoSQL('ID_SEK', QrEFD_K250ID_SEK.Value);
    //AddCampoSQL('MovimID', QrEFD_K250MovimID.Value);
    //AddCampoSQL('Codigo', QrEFD_K250Codigo.Value);
    //AddCampoSQL('MovimCod', QrEFD_K250MovimCod.Value);
    //
    FQTD_LIN_K250 := FQTD_LIN_K250 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K255: Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_CONS: TDateTime;
  COD_ITEM, COD_INS_SUBST: String;
  ID_SEK: Integer;
  QTD: Double;
  SQLType: TSQLType;
begin
  FBloco := 'K';
  FRegistro := 'K255';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  DT_CONS        := QrEFD_K255DT_CONS.Value;
  COD_ITEM       := ObtemCOD_ITEM(Geral.IMV(QrEFD_K255COD_ITEM.Value));
  QTD            := QrEFD_K255QTD.Value;
  COD_INS_SUBST  := QrEFD_K255COD_INS_SUBST.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_CONS', DT_CONS, False, False, 23) then
      (*03*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*04*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      (*05*) if not AC_x(ITO, 'COD_INS_SUBST', COD_INS_SUBST) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K250', FLinK250);
    AddCampoSQL('ID_SEK', QrEFD_K255ID_SEK.Value);
    //
    FQTD_LIN_K255 := FQTD_LIN_K255 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K280: Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_ITEM, IND_EST, COD_PART: String;
  DT_EST: TDateTime;
  K100, BalID, BalNum, BalItm, BalEnt, N: Integer;
  QTD_COR_POS, QTD_COR_NEG: Double;
begin
  FBloco := 'K';
  FRegistro := 'K280';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  DT_EST         := QrEFD_K280DT_EST.Value;
  COD_ITEM       := ObtemCOD_ITEM(Geral.IMV(QrEFD_K280COD_ITEM.Value));
  QTD_COR_POS    := QrEFD_K280QTD_COR_POS.Value;
  QTD_COR_NEG    := QrEFD_K280QTD_COR_NEG.Value;
  IND_EST        := QrEFD_K280IND_EST.Value;
  //COD_PART       := QrEFD_K280COD_PART.Value;
  COD_PART       := ObtemCOD_PART(Geral.IMV(QrEFD_K280COD_PART.Value));
  if COD_PART = '0' then
    COD_PART := '';
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_EST', DT_EST, False, False, 23) then
      (*03*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*04*) if not AC_f(ITO, 'QTD_COR_POS', QTD_COR_POS, True, False) then
      (*05*) if not AC_f(ITO, 'QTD_COR_NEG', QTD_COR_NEG, True, False) then
      (*06*) if not AC_x(ITO, 'IND_EST', IND_EST) then
      (*07*) if not AC_x(ITO, 'COD_PART', COD_PART) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K100', FLinK100);
(*
    AddCampoSQL('BalID', 0);///QrEFD_K280BalID.Value);
    AddCampoSQL('BalNum', 0); //QrEFD_K280BalNum.Value);
    AddCampoSQL('BalItm', 0); //QrEFD_K280BalItm.Value);
    AddCampoSQL('BalEnt', QrEFD_K280Entidade.Value);
*)
    //
    FQTD_LIN_K280 := FQTD_LIN_K280 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K290(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_INI_OP, DT_FIN_OP: TDateTime;
  COD_DOC_OP: String;
  //ID_SEK, MovimID, Codigo, MovimCod: Integer;
begin
  FBloco := 'K';
  FRegistro := 'K290';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  DT_INI_OP      := QrEFD_K290DT_INI_OP.Value;
  DT_FIN_OP      := QrEFD_K290DT_FIN_OP.Value;
  COD_DOC_OP     := QrEFD_K290COD_DOC_OP.Value;
  //
  try
    PredefineLinha();
    FLinK290 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_INI_OP', DT_INI_OP, False, False, 23) then
      (*03*) if not AC_d(ITO, 'DT_FIN_OP', DT_FIN_OP, False, False, 23) then
      (*04*) if not AC_x(ITO, 'COD_DOC_OP', COD_DOC_OP) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K100', FLinK100);
    //AddCampoSQL('ID_SEK', QrEFD_K290ID_SEK.Value);
    //AddCampoSQL('MovimID', QrEFD_K290MovimID.Value);
    //AddCampoSQL('Codigo', QrEFD_K290Codigo.Value);
    //AddCampoSQL('MovimCod', QrEFD_K290MovimCod.Value);
    //
    FQTD_LIN_K290 := FQTD_LIN_K290 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K291(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_SAIDA: TDateTime;
  COD_ITEM: String;
  //ID_SEK: Integer;
  QTD: Double;
begin
  FBloco := 'K';
  FRegistro := 'K291';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  COD_ITEM       := ObtemCOD_ITEM(Geral.IMV(QrEFD_K291COD_ITEM.Value));
  QTD            := QrEFD_K291QTD.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*03*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K290', FLinK290);
    //AddCampoSQL('ID_SEK', QrEFD_K291ID_SEK.Value);
    //
    FQTD_LIN_K291 := FQTD_LIN_K291 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K292(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_SAIDA: TDateTime;
  COD_ITEM: String;
  //ID_SEK: Integer;
  QTD: Double;
begin
  FBloco := 'K';
  FRegistro := 'K292';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  COD_ITEM       := ObtemCOD_ITEM(Geral.IMV(QrEFD_K292COD_ITEM.Value));
  QTD            := QrEFD_K292QTD.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*03*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K290', FLinK290);
    //AddCampoSQL('ID_SEK', QrEFD_K292ID_SEK.Value);
    //
    FQTD_LIN_K292 := FQTD_LIN_K292 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K300(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_PROD: TDateTime;
  //ID_SEK, MovimID, Codigo, MovimCod: Integer;
begin
  FBloco := 'K';
  FRegistro := 'K300';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  DT_PROD        := QrEFD_K300DT_PROD.Value;
  //
  try
    PredefineLinha();
    FLinK300 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_PROD', DT_PROD, False, False, 23) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K100', FLinK100);
    //AddCampoSQL('ID_SEK', QrEFD_K300ID_SEK.Value);
    //AddCampoSQL('MovimID', QrEFD_K300MovimID.Value);
    //AddCampoSQL('Codigo', QrEFD_K300Codigo.Value);
    //AddCampoSQL('MovimCod', QrEFD_K300MovimCod.Value);
    //
    FQTD_LIN_K300 := FQTD_LIN_K300 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K301(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_SAIDA: TDateTime;
  COD_ITEM: String;
  //ID_SEK: Integer;
  QTD: Double;
begin
  FBloco := 'K';
  FRegistro := 'K301';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  COD_ITEM       := ObtemCOD_ITEM(Geral.IMV(QrEFD_K301COD_ITEM.Value));
  QTD            := QrEFD_K301QTD.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*03*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K300', FLinK300);
    //AddCampoSQL('ID_SEK', QrEFD_K301ID_SEK.Value);
    //
    FQTD_LIN_K301 := FQTD_LIN_K301 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K302(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_SAIDA: TDateTime;
  COD_ITEM: String;
  ID_SEK: Integer;
  QTD: Double;
begin
  FBloco := 'K';
  FRegistro := 'K302';
  Result := False;
  //PB1.Max := 0;
  //PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  COD_ITEM       := ObtemCOD_ITEM(Geral.IMV(QrEFD_K302COD_ITEM.Value));
  QTD            := QrEFD_K302QTD.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*03*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AddCampoSQL('K300', FLinK300);
    //AddCampoSQL('ID_SEK', QrEFD_K302ID_SEK.Value);
    //
    FQTD_LIN_K302 := FQTD_LIN_K302 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.GeraRegistro_K990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'K';
  FRegistro := 'K990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_K', FQTD_LIN_K + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_K);
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.Grava_SpedEfdIcmsIpiErrs(Erro: TEFDExpErr; Campo: String);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(Erro)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, Campo], True);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.HabilitaBotaoOK;
begin
  BtOK.Enabled := (EdEmpresa.ValueVariant <> 0) and (RGCOD_FIN.ItemIndex > 0);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.ImprimeDaddosTxtSPEDGerado(
  NO_Empresa: String);
//
var
  ATT_IND_EST: String;
  //
  procedure ReopenEstoque(NomeTab: String; Query: TmySQLQuery);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
    'SELECT IF(k2X0.COD_PART="", "' + NO_Empresa + '", _150.Nome) NO_TERCEIRO,  ',
    '_200.DESCR_ITEM, _200.UNID_INV, ',
    ATT_IND_EST,
    'k2X0.*, IF(gg1.PrdGrupTip=-2, "Insumos", "Produtos") GRUPOS  ',
    'FROM ' + NomeTab + ' k2X0 ',
    'LEFT JOIN _spedefdicmsipi_0150_ _150 ON _150.COD_PART=k2X0.COD_PART ',
    'LEFT JOIN _spedefdicmsipi_0200_ _200 ON _200.COD_ITEM=k2X0.COD_ITEM ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=_200.GraGruX',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
    'ORDER BY k2X0.COD_PART, k2X0.DT_EST, GRUPOS, DESCR_ITEM ',
    '']);
    //Geral.MB_SQL(Self, Query);
  end;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabelas para impress�o!');
  //
  ATT_IND_EST := dmkPF.ArrayToTexto('k2X0.IND_EST', 'NO_IND_EST', pvPos, True,
  sEFD_IND_EST);
  //
  ReopenEstoque(FSpedEfdIcmsIpi_K200, QrSEII_K200);
  ReopenEstoque(FSpedEfdIcmsIpi_K280, QrSEII_K280);
  //
  MyObjects.frxDefineDataSets(frxSEII_Estq, [
  DModG.frxDsDono,
  frxDsSEII_0190,
  frxDsSEII_0200,
  frxDsSEII_K200,
  frxDsSEII_K280
  ]);
  //
  MyObjects.frxMostra(frxSEII_Estq, 'Dados de Texto de SPED EFD ICMS IPI Gerado');
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.ImprimeEstoque();
var
  Lista: TStringList;
  //
  procedure InsereSpedEfdIcmsIpi_0150();
  var
    REG, COD_PART, NOME, CNPJ, CPF, IE, SUFRAMA, _END, NUM, COMPL, BAIRRO: String;
    COD_PAIS, COD_MUN: Integer;
  begin
    REG            := Lista[00];
    COD_PART       := Lista[01];
    NOME           := Lista[02];
    COD_PAIS       := Geral.IMV(Lista[03]);
    CNPJ           := Lista[04];
    CPF            := Lista[05];
    IE             := Lista[06];
    COD_MUN        := Geral.IMV(Lista[07]);
    SUFRAMA        := Lista[08];
    _END           := Lista[09];
    NUM            := Lista[10];
    COMPL          := Lista[11];
    BAIRRO         := Lista[12];
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FSpedEfdIcmsIpi_0150, False, [
    'REG', 'COD_PART', 'NOME',
    'COD_PAIS', 'CNPJ', 'CPF',
    'IE', 'COD_MUN', 'SUFRAMA',
    'END', 'NUM', 'COMPL',
    'BAIRRO'], [
    ], [
    REG, COD_PART, NOME,
    COD_PAIS, CNPJ, CPF,
    IE, COD_MUN, SUFRAMA,
    _END, NUM, COMPL,
    BAIRRO], [
    ], False);
  end;
  //
  procedure InsereSpedEfdIcmsIpi_0190();
  var
    REG, UNID, DESCR: String;
  begin
    REG            := Lista[00];
    UNID           := Lista[01];
    DESCR          := Lista[02];
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FSpedEfdIcmsIpi_0190, False, [
    'REG', 'UNID', 'DESCR'], [
    ], [
    REG, UNID, DESCR], [
    ], False);
  end;
  //
  procedure InsereSpedEfdIcmsIpi_0200();
  var
    REG, COD_ITEM, DESCR_ITEM, COD_BARRA, COD_ANT_ITEM, UNID_INV, COD_NCM,
    EX_IPI, COD_LST: String;
    TIPO_ITEM, COD_GEN, CEST, GraGruX: Integer;
    ALIQ_ICMS: Double;
  begin
    REG            := Lista[00];
    COD_ITEM       := Lista[01];
    DESCR_ITEM     := Lista[02];
    COD_BARRA      := Lista[03];
    COD_ANT_ITEM   := Lista[04];
    UNID_INV       := Lista[05];
    TIPO_ITEM      := Geral.IMV(Lista[06]);
    COD_NCM        := Lista[07];
    EX_IPI         := Lista[08];
    COD_GEN        := Geral.IMV(Lista[09]);
    COD_LST        := Lista[10];
    ALIQ_ICMS      := Geral.DMV(Lista[11]);
    CEST           := Geral.IMV(Lista[12]);
    unNFe_PF.ObtemSPEDEFD_GraGruX(
      COD_ITEM, QrParamsEmpSPED_EFD_ID_0200.Value, GraGruX);
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FSpedEfdIcmsIpi_0200, False, [
    'REG', 'COD_ITEM', 'DESCR_ITEM',
    'COD_BARRA', 'COD_ANT_ITEM', 'UNID_INV',
    'TIPO_ITEM', 'COD_NCM', 'EX_IPI',
    'COD_GEN', 'COD_LST', 'ALIQ_ICMS',
    'CEST', 'GraGruX'], [
    ], [
    REG, COD_ITEM, DESCR_ITEM,
    COD_BARRA, COD_ANT_ITEM, UNID_INV,
    TIPO_ITEM, COD_NCM, EX_IPI,
    COD_GEN, COD_LST, ALIQ_ICMS,
    CEST, GraGruX], [
    ], False);
  end;
  procedure InsereSpedEfdIcmsIpi_K200();
  var
    REG, DT_EST, COD_ITEM, IND_EST, COD_PART: String;
    QTD: Double;
  begin
    REG            := Lista[00];
    DT_EST         := Geral.FDT(Geral.STD_SPED(Lista[01]), 1);
    COD_ITEM       := Lista[02];
    QTD            := Geral.DMV(Lista[03]);
    IND_EST        := Lista[04];
    COD_PART       := Lista[05];
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FSpedEfdIcmsIpi_K200, False, [
    'REG', 'DT_EST', 'COD_ITEM',
    'QTD', 'IND_EST', 'COD_PART'], [
    ], [
    REG, DT_EST, COD_ITEM,
    QTD, IND_EST, COD_PART], [
    ], False);
  end;
  procedure InsereSpedEfdIcmsIpi_K280();
  var
    REG, DT_EST, COD_ITEM, IND_EST, COD_PART: String;
    QTD_COR_POS, QTD_COR_NEG: Double;
  begin
    REG            := Lista[00];
    DT_EST         := Geral.FDT(Geral.STD_SPED(Lista[01]), 1);
    COD_ITEM       := Lista[02];
    QTD_COR_POS    := Geral.DMV(Lista[03]);
    QTD_COR_NEG    := Geral.DMV(Lista[04]);
    IND_EST        := Lista[05];
    COD_PART       := Lista[06];
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FSpedEfdIcmsIpi_K280, False, [
    'REG', 'DT_EST', 'COD_ITEM',
    'QTD_COR_POS', 'QTD_COR_NEG', 'IND_EST',
    'COD_PART'], [
    ], [
    REG, DT_EST, COD_ITEM,
    QTD_COR_POS, QTD_COR_NEG, IND_EST,
    COD_PART], [
    ], False);
  end;
const
  Separador = '|';
var
  I: Integer;
  Linha, REG: String;
begin
  Lista := TStringList.Create;
  try
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas tempor�rias');
  //
  FSpedEfdIcmsIpi_0150 :=
    SPED_Create.RecriaTempTableNovo(ntrttSpedEfdIcmsIpi_0150, DmodG.QrUpdPID1, False);
  FSpedEfdIcmsIpi_0190 :=
    SPED_Create.RecriaTempTableNovo(ntrttSpedEfdIcmsIpi_0190, DmodG.QrUpdPID1, False);
  FSpedEfdIcmsIpi_0200 :=
    SPED_Create.RecriaTempTableNovo(ntrttSpedEfdIcmsIpi_0200, DmodG.QrUpdPID1, False);
  FSpedEfdIcmsIpi_K200 :=
    SPED_Create.RecriaTempTableNovo(ntrttSpedEfdIcmsIpi_K200, DmodG.QrUpdPID1, False);
  FSpedEfdIcmsIpi_K280 :=
    SPED_Create.RecriaTempTableNovo(ntrttSpedEfdIcmsIpi_K280, DmodG.QrUpdPID1, False);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Lendo texto gerado');
  //
  for I := 0 to MeGerado.Lines.Count - 1 do
  begin
    Linha := MeGerado.Lines[I];
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Lendo texto gerado. Linha ' + Geral.FF0(I) + '.: ' + Linha);
    //
    Lista := Geral.Explode3(Linha, Separador, 1);
    if Lista.Count > 1 then
    begin
      REG := Lista[0];
      //Geral.MB_Info(REG);
      if Uppercase(REG) = '0150' then InsereSpedEfdIcmsIpi_0150() else
      if Uppercase(REG) = '0190' then InsereSpedEfdIcmsIpi_0190() else
      if Uppercase(REG) = '0200' then InsereSpedEfdIcmsIpi_0200() else
      if Uppercase(REG) = 'K200' then InsereSpedEfdIcmsIpi_K200() else
      if Uppercase(REG) = 'K280' then InsereSpedEfdIcmsIpi_K280() else
      ;
    end;
  end;
  //
  ImprimeDaddosTxtSPEDGerado(CBEmpresa.Text);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Lista.Free;
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.InfoPosMeGerado();
var
  L, C: Integer;
  //MudouLinha: Boolean;
  //Linha: String;
  //
  //Registro, Campo: Integer;
  //SubTipo: Char;
  //DescriCampo, ConteudoCampo, ValorCampo: String;
begin
  //MudouLinha := False;
  L := Geral.RichRow(MeGerado) + 1;
  C := Geral.RichCol(MeGerado) + 1;
  StatusBar.Panels[1].Text := Format('L: %3d   C: %3d', [L, C]);
  //
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.MeGeradoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  InfoPosMeGerado();
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.MeGeradoMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  InfoPosMeGerado();
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.MeGeradoMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  InfoPosMeGerado();
end;

function  TFmEfdIcmsIpiExporta_v03_0_2_a.MensagemCampoDesconhecido(): Boolean;
begin
{
  Geral.MB_ERRO('Campo desconhecido!' + sLineBreak +
  'Bloco: "' + QrCamposBloco.Value + '"' + sLineBreak +
  'Registro: "' + QrCamposRegistro.Value + '"' + sLineBreak +
  'Campo: "' + QrCamposCampo.Value + '"',
  'ERRO', MB_OK+MB_ICONERROR);
  //
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerUnknown)(*FNumErro[1]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
  Result := False;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.MensagemCampoIncorreto(tpEsper: String);
begin
{
  Geral.MB_ERRO('Tipo de campo incorreto:' + sLineBreak +
  'Bloco: "' + QrCamposBloco.Value + '"' + sLineBreak +
  'Registro: "' + QrCamposRegistro.Value + '"' + sLineBreak +
  'Campo: "' + QrCamposCampo.Value + '"' + sLineBreak + sLineBreak +
  'Tipo esperado: "' + tpEsper + '"' + sLineBreak +
  'Tipo informado: "' + QrCamposTipo.Value  + '"',
  'ERRO', MB_OK+MB_ICONERROR);
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerTipoFldIncorr)(*FNumErro[4]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.MensagemCampoNaoEhNumero: Boolean;
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerErrFldAlfa)(*FNumErro[12]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.MensagemCampoObrigatorio();
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerObrigSemCont)(*FNumErro[2]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.MensagemDecimaisIncorreto(tpEsper: String);
begin
{
  Geral.MB_ERRO('Decimais difere do esperado:' + sLineBreak +
  'Bloco: "' + QrCamposBloco.Value + '"' + sLineBreak +
  'Registro: "' + QrCamposRegistro.Value + '"' + sLineBreak +
  'Campo informado: "' + QrCamposCampo.Value + '"' + sLineBreak + sLineBreak +
  'Campo esperado: "' + tpEsper + '"' + sLineBreak +
  'Decimais esperado: 0' + sLineBreak +
  'Decimais solicitado: "' + Geral.FF0(QrCamposDecimais.Value) + '"',
  'ERRO', MB_OK+MB_ICONERROR);
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'],  [
  Integer(efdexerDifDecimais)(*[FNumErro[5]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.MensagemDocumentoInvalido(Doc: String);
begin
{
  Geral.MB_ERRO('Documento inv�lido!' + sLineBreak +
  'Bloco: "' + QrCamposBloco.Value + '"' + sLineBreak +
  'Registro: "' + QrCamposRegistro.Value + '"' + sLineBreak +
  'Campo: "' + QrCamposCampo.Value + '"' + sLineBreak + sLineBreak +
  'Documento: "' + DOc + '"',
  'ERRO', MB_OK+MB_ICONERROR);
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerDocInvalido)(*FNumErro[6]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.MensagemTamanhoDifereDoObrigatorio(Valor: String);
begin
{
  Geral.MB_ERRO('Tamanho difere do obrigat�rio:' + sLineBreak +
  'Bloco: "' + QrCamposBloco.Value + '"' + sLineBreak +
  'Registro: "' + QrCamposRegistro.Value + '"' + sLineBreak +
  'Campo: "' + QrCamposCampo.Value + '"' + sLineBreak + sLineBreak +
  'Texto: "' + Valor + '"' + sLineBreak +
  'Tamanho obrigatorio: "' + Geral.FF0(QrCamposTam.Value) + '"' + sLineBreak +
  'Tamanho do texto: "' + Geral.FF0(Length(Valor)) + '"',
  'ERRO', MB_OK+MB_ICONERROR);
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerTamTxtDif)(*FNumErro[7]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.MensagemTamanhoMuitoGrande(Valor: String);
begin
{
  Geral.MB_ERRO('Tamanho do texto do campo excede o m�ximo:' + sLineBreak +
  'Bloco: "' + QrCamposBloco.Value + '"' + sLineBreak +
  'Registro: "' + QrCamposRegistro.Value + '"' + sLineBreak +
  'Campo: "' + QrCamposCampo.Value + '"' + sLineBreak + sLineBreak +
  'Texto: "' + Valor + '"' + sLineBreak +
  'Tamanho m�ximo: "' + Geral.FF0(QrCamposTam.Value) + '"' + sLineBreak +
  'Tamanho do texto: "' + Geral.FF0(Length(Valor))  + '"',
  'ERRO', MB_OK+MB_ICONERROR);
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerTamTxtExce)(*FNumErro[8]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.MenuItem1Click(Sender: TObject);
begin
  ConfiguraTreeView2(
  [(*'0005', '0100',*) '0150', '0190', '0200',
  'K', 'K001', 'K100', 'K200', 'K210', 'K215', 'K220', 'K230', 'K235', 'K250',
  'K255', 'K260', 'K265', 'K270', 'K275', 'K280', 'K290', 'K291', 'K292',
  'K300', 'K301', 'K302', 'K990'],
  ['0', '0001', '0990']);

end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.MenuItem2Click(Sender: TObject);
begin
  ConfiguraTreeView2(
  ['K', 'K001', 'K100', 'K200', 'K210', 'K215', 'K220', 'K230', 'K235', 'K250',
  'K255', 'K260', 'K265', 'K270', 'K275', 'K280', 'K290', 'K291', 'K292',
  'K300', 'K301', 'K302', 'K990'],
  ['0', '0001', '0990']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.N1Click(Sender: TObject);
begin
  ConfiguraTreeView2(
  ['K200','K280'],
  ['0', '0001', '0990', 'K', 'K001', 'K100', 'K990']);
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.NaoTemErros(Avisa: Boolean): Boolean;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando exist�ncia de erros em registros!');
(*
  QrSpedEfdIcmsIpiErrs.Close;
  QrSpedEfdIcmsIpiErrs.Params[00].AsInteger := FImporExpor;
  QrSpedEfdIcmsIpiErrs.Params[01].AsInteger := FAnoMes_Int;
  QrSpedEfdIcmsIpiErrs.Params[02].AsInteger := FEmpresa_Int;
  UnDmkDAC_PF.AbreQuery(QrSpedEfdIcmsIpiErrs, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrSpedEfdIcmsIpiErrs, Dmod.MyDB, [
  'SELECT *  ',
  'FROM SpedEfdIcmsIpiErrs ',
  'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
  'AND AnoMes=' + Geral.FF0(FAnoMes_Int),
  'AND Empresa=' + Geral.FF0(FEmpresa_Int),
  '']);
  Result := QrSpedEfdIcmsIpiErrs.RecordCount = 0;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  if not Result then
  begin
    if DBCheck.CriaFm(TFmEfdIcmsIpiErros_v03_0_2_a, FmEfdIcmsIpiErros_v03_0_2_a, afmoLiberado) then
    begin
      FmEfdIcmsIpiErros_v03_0_2_a.FImporExpor := FImporExpor;
      FmEfdIcmsIpiErros_v03_0_2_a.FAnoMes     := FAnoMes_Int;
      FmEfdIcmsIpiErros_v03_0_2_a.FEmpresa    := FEmpresa_Int;
      FmEfdIcmsIpiErros_v03_0_2_a.FDataIni    := FDataIni_Txt;
      FmEfdIcmsIpiErros_v03_0_2_a.FDataFim    := FDataFim_Txt;
      FmEfdIcmsIpiErros_v03_0_2_a.FEmprTXT    := FEmpresa_Txt;
      FmEfdIcmsIpiErros_v03_0_2_a.ReopenErros();
      //
      FmEfdIcmsIpiErros_v03_0_2_a.ShowModal;
      FmEfdIcmsIpiErros_v03_0_2_a.Destroy;
    end;
  end else
  if Avisa then
    Geral.MB_Aviso('N�o h� reportagem de erros para o per�odo selecionado!');
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.ObrigatoriedadeConfere(const IndOper:
  TIndicadorDoTipoDeOperacao; var IndicadorPreenchimento: TIndicadorPreenchimento): Boolean;
var
  Obrigatoriedade: String;
begin
  Obrigatoriedade := '?';
  case IndOper of
    itoNaoAplicavel: Obrigatoriedade := QrCamposCObrig.Value;
    itoEntrada: Obrigatoriedade := QrCamposEObrig.Value;
    itoSaida: Obrigatoriedade := QrCamposSObrig.Value;
  end;
  //
  if Obrigatoriedade = 'N' then
  begin
    Result := True;
    IndicadorPreenchimento := TIndicadorPreenchimento.ipcNaoPreencher;
  end
  else
  if Obrigatoriedade = 'O' then
  begin
    Result := True;
    IndicadorPreenchimento := TIndicadorPreenchimento.ipcObrigatorio;
  end else
  if Obrigatoriedade = 'OC' then
  begin
    Result := True;
    IndicadorPreenchimento := TIndicadorPreenchimento.ipcCondicional;
  end else
  begin
    Result := False;
    IndicadorPreenchimento := TIndicadorPreenchimento.ipcIndefinido;
  end;
  //
  if not Result then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdicmsipierrs', False, [
    'Erro'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'LinArq', 'REG', 'Campo'], [
    Integer(efdexerTipObrigUnkn)(*FNumErro[9]*)], [
    FImporExpor, FAnoMes_Int, FEmpresa_Int,
    FLinArq, FRegistro, QrCamposCampo.Value], True);
    //
    Result := True;
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.ObtemCOD_ITEM(GraGruX: Integer): String;
(*
const
  Campo = 'COD_ITEM...';
var
  Valor: String;
  //
  function ObtemNaoCodigo(Campo: String): String;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEnt, Dmod.MyDB, [
    'SELECT ggx.COD_ITEM, gg1.NCM ',
    'FROM gragru1 gg1 ',
    'LEFT JOIN gragrux ggx ON ggx.GraGru1=gg1.Nivel1 ',
    'WHERE ggx.Controle=' + Geral.FF0(GraGrux),
    '']);
    //
    Result := QrEnt.FieldByName(Campo).AsString;
  end;
begin
  case QrParamsEmpSPED_EFD_ID_0200.Value of
    1: Result := Geral.FF0(GraGruX);
    2: Result := ObtemNaoCodigo('COD_ITEM');
    3: Result := Geral.Sonumero_TT(ObtemNaoCodigo('NCM'));
    4: Result := 'Dmk_' + Geral.FF0(GraGruX);
    else
    begin
      Result := '';
      Valor  := 'Entidade c�digo ' + Geral.FF0(GraGruX);
      SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
    end;
  end;
*)
const
  Campo = 'COD_ITEM...';
var
  Valor: String;
  //
begin
  if GraGruX = 0 then
    Geral.MB_Erro('GraGuuX = 0');
  if UnNFe_PF.ObtemSPEDEFD_COD_ITEM(GraGruX,
  QrParamsEmpSPED_EFD_ID_0200.Value, Result) then
  begin
    Valor := 'Reduzido c�digo ' + Geral.FF0(GraGruX);
    SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
  end;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.ObtemCOD_PART(Entidade: Integer): String;
(*
  0150.02 - C100.04 - C113.04 - C160.02 - C165.02 - C176.06 - C176.21 -
  C500.04 - C510.18 - D100.04 - D130.02 - D130.03 - D240.02 - D710.02 -
  D170.03 - D400.02 - D500.04 - D510.17 - E113.02 - E240.02 - E313.02 -
  G130.03 - H010.08 - K200.06 - K280.07 - 1110.02 - 1500.04 - 1510.18 -
  1600.02 - 1923.02 -
*)
(*
const
  Campo = 'COD_PART...';
var
  Valor: String;
  //
  function ObtemNaoCodigo(Campo: String): String;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEnt, Dmod.MyDB, [
    'SELECT COD_PART, ',
    'IF(Tipo=0, CNPJ, CPF) CNPJ_CPF ',
    'FROM entidades ',
    'WHERE Codigo=' + Geral.FF0(Entidade),
    '']);
    //
    Result := QrEnt.FieldByName(Campo).AsString;
  end;
begin
  case QrParamsEmpSPED_EFD_ID_0150.Value of
    1: Result := Geral.FF0(Entidade);
    2: Result := ObtemNaoCodigo('COD_PART');
    3: Result := Geral.SoNumero_TT(ObtemNaoCodigo('CNPJ_CPF'));
    4: Result := 'Dmk_' + Geral.FF0(Entidade);
    else
    begin
      Result := '';
      Valor  := 'Entidade c�digo ' + Geral.FF0(Entidade);
      SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
    end;
  end;
*)
const
  Campo = 'COD_PART...';
var
  COD_PART, Valor: String;
begin
  Result := '0';
  if not UnNFe_PF.ObtemSPEDEFD_COD_PART(Entidade,
  QrParamsEmpSPED_EFD_ID_0150.Value, COD_PART) then
  begin
    Valor  := 'Entidade c�digo ' + Geral.FF0(Entidade);
    SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
  end else
    Result := COD_PART;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.ObtemITO_CabA(var EhE: Boolean; var IND_OPER, tpNF:
  String; var ITO: TIndicadorDoTipoDeOperacao): Boolean;
begin
  EhE := QrCabA_CCodInfoEmit.Value = FEmpresa_Int;
  tpNF := Geral.FF0(QrCabA_Cide_tpNF.Value);
  IND_OPER := dmkPF.EscolhaDe2Str(EhE, tpNF, '0');
  if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.ObtemITO_InNF(var EhE: Boolean;
  var IND_OPER, tpNF: String; var ITO: TIndicadorDoTipoDeOperacao): Boolean;
begin
  EhE := QrEfdInnNFsCabEmpresa.Value = FEmpresa_Int;
  tpNF := '0'; // Tipo de Opera��o da NF-e: 0=Entrada; 1=Sa�da
  IND_OPER := dmkPF.EscolhaDe2Str(EhE, tpNF, '0');
  (*if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;*)
  ITO := itoEntrada;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.PodeGerarBloco(Bloco: String): Boolean;
begin
  QrBlocos.Close;
  QrBlocos.Database := DmodG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(QrBlocos, DmodG.MyPID_DB, [
'SELECT * ',
'FROM ' + FSPEDEFD_Blcs,
'WHERE Registro="' + Bloco + '"',
//'AND Ativo=1',
'AND StateIndex IN (1,3)',
'']);
  Result := QrBlocos.RecordCount > 0;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.PredefineLinha();
begin
  FLinStr := '|';
  FLinArq := FLinArq + 1;
  //
  SetLength(FSQLCampos, 1);
  FSQLCampos[0] := 'LinArq';
  //
  SetLength(FValCampos, 1);
  FValCampos[0] := FLinArq;
  //
  SetLength(FSQLIndex, 3);
  SetLength(FValIndex, 3);
  //
  FSQLIndex[0] := 'ImporExpor';
  FValIndex[0] := FImporExpor;
  //
  FSQLIndex[1] := 'AnoMes';
  FValIndex[1] := FAnoMes_Int;
  ///
  FSQLIndex[2] := 'Empresa';
  FValIndex[2] := FEmpresa_Int;
{
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd, LaTipo.SQLType, 'spedefdicmsipi0000', auto_increment?[
'LinArq', 'REG', 'COD_VER',
'COD_FIN', 'DT_INI', 'DT_FIN',
'NOME', 'CNPJ', 'CPF',
'UF', 'IE', 'COD_MUN',
'IM', 'SUFRAMA', 'IND_PERFIL',
'IND_ATIV'], [
], [
LinArq, REG, COD_VER,
COD_FIN, DT_INI, DT_FIN,
NOME, CNPJ, CPF,
UF, IE, COD_MUN,
IM, SUFRAMA, IND_PERFIL,
IND_ATIV], [
ImporExpor, AnoMes, Empresa], UserDataAlterweb?, IGNORE?
}
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_E100AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_E110();
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_E100BeforeClose(DataSet: TDataSet);
begin
  QrEFD_E110.Close;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_E110AfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E111, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie111 E111 ',
  'WHERE E111.ImporExpor=' + Geral.FF0(QrEFD_E110ImporExpor.Value),
  'AND E111.Empresa=' + Geral.FF0(QrEFD_E110Empresa.Value),
  'AND E111.AnoMes=' + Geral.FF0(QrEFD_E110AnoMes.Value),
  'AND E111.E110=' + Geral.FF0(QrEFD_E110LinArq.Value),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E115, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie115 E115 ',
  'WHERE E115.ImporExpor=' + Geral.FF0(QrEFD_E110ImporExpor.Value),
  'AND E115.Empresa=' + Geral.FF0(QrEFD_E110Empresa.Value),
  'AND E115.AnoMes=' + Geral.FF0(QrEFD_E110AnoMes.Value),
  'AND E115.E110=' + Geral.FF0(QrEFD_E110LinArq.Value),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E116, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie116 E116 ',
  'WHERE E116.ImporExpor=' + Geral.FF0(QrEFD_E110ImporExpor.Value),
  'AND E116.Empresa=' + Geral.FF0(QrEFD_E110Empresa.Value),
  'AND E116.AnoMes=' + Geral.FF0(QrEFD_E110AnoMes.Value),
  'AND E116.E110=' + Geral.FF0(QrEFD_E110LinArq.Value),
  '']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_E110BeforeClose(DataSet: TDataSet);
begin
  QrEFD_E111.Close;
  QrEFD_E115.Close;
  QrEFD_E116.Close;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_E111AfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E112, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie112 E112 ',
  'WHERE E112.ImporExpor=' + Geral.FF0(QrEFD_E111ImporExpor.Value),
  'AND E112.Empresa=' + Geral.FF0(QrEFD_E111Empresa.Value),
  'AND E112.AnoMes=' + Geral.FF0(QrEFD_E111AnoMes.Value),
  'AND E112.E111=' + Geral.FF0(QrEFD_E111LinArq.Value),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E113, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie113 E113 ',
  'WHERE E113.ImporExpor=' + Geral.FF0(QrEFD_E111ImporExpor.Value),
  'AND E113.Empresa=' + Geral.FF0(QrEFD_E111Empresa.Value),
  'AND E113.AnoMes=' + Geral.FF0(QrEFD_E111AnoMes.Value),
  'AND E113.E111=' + Geral.FF0(QrEFD_E111LinArq.Value),
  '']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_E111BeforeClose(DataSet: TDataSet);
begin
  QrEFD_E112.Close;
  QrEFD_E113.Close;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_E520AfterScroll(DataSet: TDataSet);
begin
  ReopenEFD_E530();
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_E520BeforeClose(DataSet: TDataSet);
begin
  QrEFD_E530.Close;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_H005AfterScroll(DataSet: TDataSet);
begin
  UndmkDAC_PF.AbreMySQLQuery0(QrEFD_H010, Dmod.MyDB, [
  'SELECT h010.*  ',
  'FROM efdicmsipih010 h010 ',
  'WHERE h010.ImporExpor=' + Geral.FF0(QrEFD_H005ImporExpor.Value),
  'AND h010.Empresa=' + Geral.FF0(QrEFD_H005Empresa.Value),
  'AND h010.AnoMes=' + Geral.FF0(QrEFD_H005AnoMes.Value),
  'AND h010.H005=' + Geral.FF0(QrEFD_H005LinArq.Value),
  '']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_H010AfterScroll(DataSet: TDataSet);
begin
  UndmkDAC_PF.AbreMySQLQuery0(QrEFD_H020, Dmod.MyDB, [
  'SELECT h020.*  ',
  'FROM efdicmsipih020 h020 ',
  'WHERE h020.ImporExpor=' + Geral.FF0(QrEFD_H010ImporExpor.Value),
  'AND h020.Empresa=' + Geral.FF0(QrEFD_H010Empresa.Value),
  'AND h020.AnoMes=' + Geral.FF0(QrEFD_H010AnoMes.Value),
  'AND h020.H010=' + Geral.FF0(QrEFD_H010LinArq.Value),
  '']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_H010BeforeClose(DataSet: TDataSet);
begin
  QrEFD_H020.Close;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_K100AfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K200, Dmod.MyDB, [
(*
  'SELECT * FROM efdicmsipik200 ',
  'WHERE ImporExpor=3',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_TXT,
  //'AND K100=' + Geral.FF0(QrEFD_K100LinArq.Value),
  'AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
*)
  'SELECT DT_EST, COD_ITEM, IND_EST, COD_PART, Entidade, ',
  'SUM(QTD) QTD ',
  'FROM efdicmsipik200  ',
  'WHERE ImporExpor=3 ',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_TXT,
  'AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
  'GROUP BY DT_EST, COD_ITEM, IND_EST, COD_PART, Entidade ',
  'ORDER BY DT_EST, COD_ITEM, IND_EST, COD_PART, Entidade ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K280, Dmod.MyDB, [
  'SELECT DT_EST, COD_ITEM, IND_EST, COD_PART, Entidade,',
  'SUM(QTD_COR_POS) QTD_COR_POS, ',
  'SUM(QTD_COR_NEG) QTD_COR_NEG ',
  'FROM efdicmsipik280  ',
  'WHERE ImporExpor=3 ',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_TXT,
  'AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
  'GROUP BY DT_EST, COD_ITEM, IND_EST, COD_PART, Entidade',
  'ORDER BY DT_EST, COD_ITEM, IND_EST, COD_PART, Entidade',
  '']);
  //
////////////////////////////////////////////////////////////////////////////////
  //
  if FSelRegK210 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K210, Dmod.MyDB, [
    'SELECT * ',
    'FROM efdicmsipik210 ',
    'WHERE ImporExpor=3',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_TXT,
    'AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
    '']);
  end;
  //
  if FSelRegK220 then
  begin
  (*
    UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K220, Dmod.MyDB, [
    'SELECT * FROM efdicmsipik220 ',
    'WHERE ImporExpor=3',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_TXT,
    'AND K100=' + Geral.FF0(QrEFD_K100PeriApu.Value),
    '']);
  *)
    UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K220, Dmod.MyDB, [
    'SELECT DT_MOV, COD_ITEM_ORI, COD_ITEM_DEST,',
    'SUM(QTD) QTD, ID_SEK ',
    'FROM efdicmsipik220 ',
    'WHERE ImporExpor=3',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_TXT,
    //'AND K100=' + Geral.FF0(QrEFD_K100PeriApu.Value),
    'AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
    'GROUP BY DT_MOV, COD_ITEM_ORI, COD_ITEM_DEST',
    '']);
  end;
  //
  if FSelRegK230 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K230, Dmod.MyDB, [
    'SELECT * FROM efdicmsipik230 ',
    'WHERE ImporExpor=3',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_TXT,
    //'AND K100=' + Geral.FF0(QrEFD_K100PeriApu.Value),
    'AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
    '']);
  end;
  if FSelRegK250 then
  begin
  (*
    UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K250, Dmod.MyDB, [
    'SELECT * FROM efdicmsipik250 ',
    'WHERE ImporExpor=3',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_TXT,
    'AND K100=' + Geral.FF0(QrEFD_K100PeriApu.Value),
    '']);
  *)
    UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K250, Dmod.MyDB, [
    'SELECT DT_PROD, COD_ITEM, SUM(QTD) QTD ',
    'FROM efdicmsipik250 ',
    'WHERE ImporExpor=3',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_TXT,
    //'AND K100=' + Geral.FF0(QrEFD_K100PeriApu.Value),
    'AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
    'GROUP BY DT_PROD, COD_ITEM',
    'ORDER BY DT_PROD, COD_ITEM',
    '']);
    //Geral.MB_SQL(Self, QrEFD_K250);
  end;
  if FSelRegK290 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K290, Dmod.MyDB, [
    'SELECT DT_INI_OP, DT_FIN_OP, COD_DOC_OP',
    'FROM efdicmsipik290',
    'WHERE ImporExpor=3',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_TXT,
    'AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
    'ORDER BY COD_DOC_OP',
    '']);
    //Geral.MB_SQL(Self, QrEFD_K290);
  end;
  if FSelRegK300 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K300, Dmod.MyDB, [
    'SELECT DT_PROD',
    'FROM efdicmsipik300',
    'WHERE ImporExpor=3',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_TXT,
    'AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
    'ORDER BY DT_PROD',
    '']);
    //Geral.MB_SQL(Self, QrEFD_K300);
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_K100BeforeClose(DataSet: TDataSet);
begin
  QrEFD_K200.Close;
  QrEFD_K220.Close;
  QrEFD_K230.Close;
  QrEFD_K250.Close;
  QrEFD_K280.Close;
  QrEFD_K290.Close;
  QrEFD_K300.Close;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_K210AfterScroll(
  DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K215, Dmod.MyDB, [
  'SELECT COD_ITEM_DES, ',
  'SUM(QTD_DES) QTD_DES, ID_SEK',
  'FROM efdicmsipik215 ',
  'WHERE ImporExpor=3',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_TXT,
  'AND PeriApu=' + Geral.FF0(QrEFD_K210PeriApu.Value),
  'GROUP BY COD_ITEM_DES',
  '']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_K210BeforeClose(
  DataSet: TDataSet);
begin
  QrEFD_K215.Close;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_K230AfterScroll(DataSet: TDataSet);
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K235, Dmod.MyDB, [
  'SELECT * FROM efdicmsipik235 ',
  'WHERE ImporExpor=3',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_TXT,
  'AND K230=' + Geral.FF0(QrEFD_K230LinArq.Value),
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K235, Dmod.MyDB, [
  'SELECT DT_SAIDA, COD_ITEM, COD_INS_SUBST, ',
  'SUM(QTD) QTD, ID_SEK',
  'FROM efdicmsipik235 ',
  'WHERE ImporExpor=3',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_TXT,
  //'AND K230=' + Geral.FF0(QrEFD_K230PeriApu.Value),
  'AND PeriApu=' + Geral.FF0(QrEFD_K230PeriApu.Value),
  'GROUP BY DT_SAIDA, COD_ITEM, COD_INS_SUBST',
  '']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_K230BeforeClose(DataSet: TDataSet);
begin
  QrEFD_K235.Close;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_K250AfterScroll(DataSet: TDataSet);
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K255, Dmod.MyDB, [
  'SELECT DT_CONS, COD_ITEM, COD_INS_SUBST, ',
  'SUM(QTD) QTD, ID_SEK',
  'FROM efdicmsipik255 ',
  'WHERE ImporExpor=3',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_TXT,
  'AND K250=' + Geral.FF0(QrEFD_K250LinArq.Value),
  'GROUP BY DT_CONS, COD_ITEM, COD_INS_SUBST',
  '']);
*)
  if QrEFD_K250.RecordCount > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K255, Dmod.MyDB, [
    'SELECT DT_CONS, COD_ITEM, COD_INS_SUBST, ',
    'SUM(QTD) QTD, ID_SEK',
    'FROM efdicmsipik255 ',
    'WHERE ImporExpor=3',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_TXT,
    //'AND K250 IN (',
    'AND IDSeq1 IN (',
    //'  SELECT LinArq',
    '  SELECT IDSeq1',
    '  FROM efdicmsipik250 ',
    '  WHERE ImporExpor=3',
    '  AND AnoMes=' + FAnoMes_Txt,
    '  AND Empresa=' + FEmpresa_TXT,
    //'  AND K100=' + Geral.FF0(QrEFD_K100PeriApu.Value),
    '  AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
    '  AND DT_PROD="' + Geral.FDT(QrEFD_K250DT_PROD.Value, 1) + '"',
    '  AND COD_ITEM="' + QrEFD_K250COD_ITEM.Value + '"',
    '',
    ')',
    'GROUP BY DT_CONS, COD_ITEM, COD_INS_SUBST',
    '']);
    //Geral.MB_SQL(Self, QrEFD_K255);
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_K250BeforeClose(DataSet: TDataSet);
begin
  QrEFD_K255.Close;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_K290AfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K291, Dmod.MyDB, [
  'SELECT COD_ITEM, SUM(QTD) QTD',
  'FROM efdicmsipik291',
  'WHERE ImporExpor=3 ',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_TXT,
  'AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
  'AND IDSeq1 IN',
  '(',
  '  SELECT IDSeq1 ',
  '  FROM efdicmsipik290',
  '  WHERE ImporExpor=3 ',
  '  AND AnoMes=' + FAnoMes_Txt,
  '  AND Empresa=' + FEmpresa_TXT,
  '  AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
  ')',
  'GROUP BY COD_ITEM',
  'ORDER BY GraGruX',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K292, Dmod.MyDB, [
  'SELECT COD_ITEM, SUM(QTD) QTD',
  'FROM efdicmsipik292',
  'WHERE ImporExpor=3 ',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_TXT,
  'AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
  'AND IDSeq1 IN',
  '(',
  '  SELECT IDSeq1 ',
  '  FROM efdicmsipik290',
  '  WHERE ImporExpor=3 ',
  '  AND AnoMes=' + FAnoMes_Txt,
  '  AND Empresa=' + FEmpresa_TXT,
  '  AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
  ')',
  'GROUP BY COD_ITEM',
  'ORDER BY GraGruX',
  '']);
  //
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.QrEFD_K300AfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K301, Dmod.MyDB, [
  'SELECT COD_ITEM, SUM(QTD) QTD',
  'FROM efdicmsipik301',
  'WHERE ImporExpor=3 ',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_TXT,
  'AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
  'AND IDSeq1 IN',
  '(',
  '  SELECT IDSeq1 ',
  '  FROM efdicmsipik300',
  '  WHERE ImporExpor=3 ',
  '  AND AnoMes=' + FAnoMes_Txt,
  '  AND Empresa=' + FEmpresa_TXT,
  '  AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
  ')',
  'GROUP BY COD_ITEM',
  'ORDER BY GraGruX',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_K302, Dmod.MyDB, [
  'SELECT COD_ITEM, SUM(QTD) QTD',
  'FROM efdicmsipik302',
  'WHERE ImporExpor=3 ',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_TXT,
  'AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
  'AND IDSeq1 IN',
  '(',
  '  SELECT IDSeq1 ',
  '  FROM efdicmsipik300',
  '  WHERE ImporExpor=3 ',
  '  AND AnoMes=' + FAnoMes_Txt,
  '  AND Empresa=' + FEmpresa_TXT,
  '  AND PeriApu=' + Geral.FF0(QrEFD_K100PeriApu.Value),
  ')',
  'GROUP BY COD_ITEM',
  'ORDER BY GraGruX',
  '']);
  //
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.ReabreAtrela0053(JaAtrelados: TSimNao);
var
  SQL_Atralados: String;
begin
  case JaAtrelados of
    TSimNao.snNao: SQL_Atralados := ' NOT ';
    TSimNao.snSim: SQL_Atralados := '';
    else SQL_Atralados := ' ??? ';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtrela0053, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_A_;',
  'CREATE TABLE _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_A_',
  'SELECT NFe_FatID, NFe_FatNum, ' + FDtFiscal0053_Entr + ' DataFiscalInn ',
  'FROM ' + TMeuDB + '.efdinnnfscab',
  'WHERE NFe_FatID=' + Geral.FF0(VAR_FATID_0053),
  'AND  NFe_FatNum<>0',
  'AND Empresa=' + FEmpresa_txt,
  'AND ' + FDtFiscal0053_Entr + ' BETWEEN "' + FDataIni_TXT + '" AND "' + FDataFim_TXT + '";',
  '',
  'DROP TABLE IF EXISTS _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_B_;',
  'CREATE TABLE _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_B_',
  'SELECT FatID, FatNum, Empresa, ide_dEmi, DataFiscal DataFiscalNul',
  'FROM ' + TMeuDB + '.nfecaba',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_0053),
  'AND  FatNum<>0',
  'AND Empresa=' + FEmpresa_txt,
  'AND DataFiscal < "1900-01-01";',
  '',
  'SELECT b.*, a.*',
  'FROM _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_B_ b',
  'LEFT JOIN _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_A_ a ',
  '  ON b.FatNum=a.NFe_FatNum',
  'WHERE ' + SQL_Atralados + ' (a.NFe_FatNum) IS NULL',
  ';']);
  //
  //Geral.MB_Teste(QrAtrela0053.SQL.Text)
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.ReCriaNoh(const Noh: TTreeNode; const nN: String;
  const cN: Integer; const Ativo: Integer): TTreeNode;
begin
  if Noh = nil then
    Result := TvBlocos.Items.Add(Noh, nN)
  else
    Result := TvBlocos.Items.AddChild(Noh, nN);
  Result.ImageIndex := cN;
  //Result.StateIndex := QrBlcsAtivo.Value + Integer(cCBNot);
  Result.StateIndex := Ativo + Integer(cCBNot);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.RecriaRegistroEFD_E110();
var
  REG: String;
  ImporExpor, AnoMes, Empresa, LinArq, E100: Integer;
  VL_TOT_DEBITOS, VL_AJ_DEBITOS, VL_TOT_AJ_DEBITOS, VL_ESTORNOS_CRED,
  VL_TOT_CREDITOS, VL_AJ_CREDITOS, VL_TOT_AJ_CREDITOS, VL_ESTORNOS_DEB,
  VL_SLD_CREDOR_ANT, VL_SLD_APURADO, VL_TOT_DED, VL_ICMS_RECOLHER,
  VL_SLD_CREDOR_TRANSPORTAR, DEB_ESP: Double;
  SQLType: TSQLType;
begin
  //SQLType        := stUpd;
  ImporExpor     := QrEFD_E100ImporExpor.Value;
  Empresa        := QrEFD_E100Empresa.Value;
  AnoMes         := QrEFD_E100AnoMes.Value;
  LinArq         := QrEFD_E100LinArq.Value;
(*  E100           := ;
  REG            := ;*)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  VL_TOT_DEBITOS            := CalculaCampoEFD_E110_02();
  VL_AJ_DEBITOS             := 0;  // Ver o que fazer
  VL_TOT_AJ_DEBITOS         := 0;  // Ver o que fazer
  VL_ESTORNOS_CRED          := 0;  // Ver o que fazer*)
  VL_TOT_CREDITOS           := CalculaCampoEFD_E110_06();
  VL_AJ_CREDITOS            := 0;  // Ver o que fazer
  VL_TOT_AJ_CREDITOS        := 0;  // Ver o que fazer
  VL_ESTORNOS_DEB           := 0;  // Ver o que fazer
  VL_SLD_CREDOR_ANT         := BuscaValorMesAnterior('e110', 'VL_SLD_CREDOR_ANT');  // Ver o que fazer
(*
Campo 11 (VL_SLD_APURADO) - Valida��o: o valor informado deve ser preenchido com base na express�o: soma do
total de d�bitos (VL_TOT_DEBITOS) com total de ajustes (VL_AJ_DEBITOS +VL_TOT_AJ_DEBITOS) com total de
estorno de cr�dito (VL_ESTORNOS_CRED) menos a soma do total de cr�ditos (VL_TOT_CREDITOS) com total de
ajuste de cr�ditos (VL_,AJ_CREDITOS + VL_TOT_AJ_CREDITOS) com total de estorno de d�bito
(VL_ESTORNOS_DEB) com saldo credor do per�odo anterior (VL_SLD_CREDOR_ANT). Se o valor da express�o for
maior ou igual a �0� (zero), ent�o este valor deve ser informado neste campo e o campo 14
(VL_SLD_CREDOR_TRANSPORTAR) deve ser igual a �0� (zero). Se o valor da express�o for menor que �0� (zero),
ent�o este campo deve ser preenchido com �0� (zero) e o valor absoluto da express�o deve ser informado no campo
VL_SLD_CREDOR_TRANSPORTAR.
*)
  VL_SLD_APURADO :=
    ((VL_TOT_DEBITOS) + (VL_AJ_DEBITOS +VL_TOT_AJ_DEBITOS) + (VL_ESTORNOS_CRED))
    -
    ((VL_TOT_CREDITOS) + (VL_AJ_CREDITOS + VL_TOT_AJ_CREDITOS) + (VL_ESTORNOS_DEB) + (VL_SLD_CREDOR_ANT));

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
(*
Campo 12 (VL_TOT_DED) - Valida��o: o valor informado deve corresponder ao somat�rio do campo VL_ICMS dos
registros C197, C597 e D197, se o terceiro caractere do c�digo de ajuste do registro C197, C597 ou D197, for �6� e o quarto
caractere for �0�, somado ao valor total informado nos registros E111, quando o terceiro caractere for igual a �0� e o quarto
caractere for igual a �4�, do campo COD_AJ_APUR do registro E111.
Para o somat�rio do campo VL_ICMS dos registros C197, C597 e D197 devem ser considerados os documentos fiscais
compreendidos no per�odo informado no registro E100, comparando com a data informada no campo DT_E_S do registro
C100 ou C500 e DT_DOC ou DT_A_P do registro D100, exceto se COD_SIT do registro C100 ou C500 for igual a �01�
(extempor�neo) ou igual a �07� (Complementar extempor�nea), cujo valor deve ser somado no primeiro per�odo de apura��o
informado no registro E100, quando houver mais de um per�odo de apura��o. Quando o campo DT_E_S n�o for informado,
utilizar o campo DT_DOC.
Neste campo s�o informados os valores que, segundo a legisla��o da UF, devam ser tratados como �Dedu��o do imposto�,
ainda que no campo VL_SLD_APURADO tenha como resultado o valor zero.*)

  VL_TOT_DED                := 0.00;  // Ver o que fazer

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

(*
Campo 13 (VL_ICMS_RECOLHER) � Valida��o: o valor informado deve corresponder � diferen�a entre o campo
VL_SLD_APURADO e o campo VL_TOT_DED. Se o resultado dessa opera��o for negativo, informe o valor zero neste
campo, e o valor absoluto correspondente no campo VL_SLD_CREDOR_TRANSPORTAR. Verificar se a legisla��o da
UF permite que dedu��o seja maior que o saldo devedor.
O valor da soma deste campo com o campo DEB_ESP deve ser igual � soma dos valores do campo VL_OR do registro
E116.
*)
  VL_ICMS_RECOLHER          := (VL_SLD_APURADO - VL_TOT_DED);
  if VL_ICMS_RECOLHER < 0 then
  begin
    VL_SLD_CREDOR_TRANSPORTAR := - VL_ICMS_RECOLHER;
    VL_ICMS_RECOLHER          := 0;
  end
  else
    VL_SLD_CREDOR_TRANSPORTAR := 0;
  //
  DEB_ESP                   := 0;  // Ver o que fazer
  //





////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

(*
Campo 14 (VL_SLD_CREDOR_TRANSPORTAR) � Valida��o: se o valor da express�o: soma do total de d�bitos
(VL_TOT_DEBITOS) com total de ajustes (VL_AJ_DEBITOS + VL_TOT_AJ_DEBITOS) com total de estorno de cr�dito
(VL_ESTORNOS_CRED) menos a soma do total de cr�ditos (VL_TOT_CREDITOS) com total de ajuste de cr�ditos
(VL_AJ_CREDITOS + VL_TOT_AJ_CREDITOS) com total de estorno de d�bito (VL_ESTORNOS_DEB) com saldo
credor do per�odo anterior (VL_SLD_CREDOR_ANT) com total de dedu��es (VL_TOT_DED) for maior que �0� (zero),
este campo deve ser preenchido com �0� (zero). Se for menor que �0� (zero), o valor absoluto do resultado deve ser
informado neste campo.
*)
  if VL_SLD_APURADO < 0 then
  begin
    VL_SLD_CREDOR_TRANSPORTAR := (-VL_SLD_APURADO) + VL_TOT_DED;
    VL_SLD_APURADO := 0;
  end;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT AnoMes ',
  'FROM efdicmsipie110 ',
  'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
  'AND AnoMes=' + Geral.FF0(AnoMes),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND LinArq=' + Geral.FF0(LinArq),
  '']);
  // ver se existe antes de fazer o upd
  if Dmod.QrAux.RecordCount > 0 then
    SQLType := stUpd
  else
    SQLType := stIns;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdicmsipie110', False, [
  (*'E100', 'REG',*) 'VL_TOT_DEBITOS',
  'VL_AJ_DEBITOS', 'VL_TOT_AJ_DEBITOS', 'VL_ESTORNOS_CRED',
  'VL_TOT_CREDITOS', 'VL_AJ_CREDITOS', 'VL_TOT_AJ_CREDITOS',
  'VL_ESTORNOS_DEB', 'VL_SLD_CREDOR_ANT', 'VL_SLD_APURADO',
  'VL_TOT_DED', 'VL_ICMS_RECOLHER', 'VL_SLD_CREDOR_TRANSPORTAR',
  'DEB_ESP'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  (*E100, REG,*) VL_TOT_DEBITOS,
  VL_AJ_DEBITOS, VL_TOT_AJ_DEBITOS, VL_ESTORNOS_CRED,
  VL_TOT_CREDITOS, VL_AJ_CREDITOS, VL_TOT_AJ_CREDITOS,
  VL_ESTORNOS_DEB, VL_SLD_CREDOR_ANT, VL_SLD_APURADO,
  VL_TOT_DED, VL_ICMS_RECOLHER, VL_SLD_CREDOR_TRANSPORTAR,
  DEB_ESP], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
    ReopenEFD_E110();
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.RecriaRegistroEFD_E520();
var
  REG: String;
  ImporExpor, AnoMes, Empresa, LinArq, E500: Integer;
  VL_SD_ANT_IPI,
  VL_DEB_IPI, VL_CRED_IPI, VL_OD_IPI, VL_OC_IPI, VL_SC_IPI,
  VL_SD_IPI, Saldo: Double;
  SQLType: TSQLType;
  Qry: TmySQLQuery;
begin
  SQLType        := stUpd;
  ImporExpor     := QrEFD_E500ImporExpor.Value;
  Empresa        := QrEFD_E500Empresa.Value;
  AnoMes         := QrEFD_E500AnoMes.Value;
  LinArq         := QrEFD_E500LinArq.Value;
  E500           := QrEFD_E500LinArq.Value;;
  REG            := 'E520';
  VL_SC_IPI      := 0;
  VL_SD_IPI      := 0;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM efdicmsipie520 ',
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND LinArq=' + Geral.FF0(LinArq),
    'AND E500=' + Geral.FF0(E500),
    '']);
    (*02*)VL_SD_ANT_IPI  := Qry.FieldByName('VL_SD_ANT_IPI').AsFloat;
  //Campo 03 (VL_DEB_IPI) - Valida��o: o valor informado deve corresponder ao somat�rio do campo VL_IPI do registro
  //E510, quando o CFOP iniciar por �5� ou �6� dos registros C190.
    (*03*)VL_DEB_IPI     := CalculaCampoEFD_E520_03();
  //Campo 04 (VL_CRED_IPI) - Valida��o: o valor informado deve corresponder ao somat�rio do campo VL_IPI do registro
  //E510, quando o CFOP iniciar por �1�, �2� ou �3� dos registros C190.
    (*04*)VL_CRED_IPI    := CalculaCampoEFD_E520_04();
  //Campo 05 (VL_OD_IPI) - Valida��o: o valor informado deve corresponder ao somat�rio do campo VL_AJ do registro
  //E530, quando o campo IND_AJ do registro E530 for igual a �0�.
    (*05*)VL_OD_IPI      := Qry.FieldByName('VL_OD_IPI').AsFloat;
  //Campo 06 (VL_OC_IPI) - Valida��o: o valor informado deve corresponder ao somat�rio do campo VL_AJ do registro
  //E530, quando o campo IND_AJ do registro E530 for igual a �1�.
    (*06*)VL_OC_IPI      := Qry.FieldByName('VL_OC_IPI').AsFloat;
  //Campo 07 (VL_SC_IPI) - Valida��o: se a soma dos campos VL_DEB_IPI e VL_OD_IPI menos a soma dos campos
  //VL_SD_ANT_IPI, VL_CRED_IPI e VL_OC_IPI for menor que �0� (zero), ent�o o campo VL_SC_IPI deve ser igual ao
  //valor absoluto da express�o, e o valor do campo VL_SD_IPI deve ser igual a �0� (zero).
    Saldo := (VL_DEB_IPI + VL_OD_IPI) - (VL_SD_ANT_IPI + VL_CRED_IPI + VL_OC_IPI);
    if Saldo < 0 then
    (*07*)VL_SC_IPI      := - Saldo
  //Campo 08 (VL_SD_IPI) - Valida��o: se a soma dos campos VL_DEB_IPI e VL_OD_IPI menos a soma dos campos
  //VL_SD_ANT_IPI, VL_CRED_IPI e VL_OC_IPI for maior ou igual a �0� (zero), ent�o o campo 08 (VL_SD_IPI) deve ser
  //igual ao resultado da express�o, e o valor do campo VL_SC_IPI deve ser igual a �0� (zero).
     else
    (*08*)VL_SD_IPI      := Saldo;
    //
    if UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'efdicmsipie520', False, [
    'REG', 'VL_SD_ANT_IPI', 'VL_DEB_IPI',
    'VL_CRED_IPI', 'VL_OD_IPI', 'VL_OC_IPI',
    'VL_SC_IPI', 'VL_SD_IPI'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'E500'], [
    'VL_SD_ANT_IPI', 'VL_DEB_IPI',
    'VL_CRED_IPI', 'VL_OD_IPI', 'VL_OC_IPI',
    'VL_SC_IPI', 'VL_SD_IPI'], [
    REG, VL_SD_ANT_IPI, VL_DEB_IPI,
    VL_CRED_IPI, VL_OD_IPI, VL_OC_IPI,
    VL_SC_IPI, VL_SD_IPI], [
    ImporExpor, AnoMes, Empresa, LinArq, E500], [
    VL_SD_ANT_IPI, VL_DEB_IPI,
    VL_CRED_IPI, VL_OD_IPI, VL_OC_IPI,
    VL_SC_IPI, VL_SD_IPI], True) then
      ReopenEFD_E520();
  finally
    Qry.Free;
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.RedefinePeriodos;
begin
  if Length(EdMes.Text) = 7 then
  begin
    TPDataIni.Date := Int(Geral.ValidaDataBR(EdMes.ValueVariant, False, False));
    TPDataFim.Date := IncMonth(TPDataIni.Date, 1) - 1;
    //
    FDataIni_Dta := Int(TPDataIni.Date);
    FDataFim_Dta := Int(TPDataFim.Date);
    //
  end;
end;

(*
procedure TFmEfdIcmsIpiExporta_v03_0_2_a.ReopenCabA(Filtra: Boolean; Filtro: Variant);
var
  LinFiltro: String;
begin
  if Filtra then
    LinFiltro := 'AND EFD_EXP_REG = "' + Filtro + '"'
  else
    LinFiltro := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrCabA_C, Dmod.MyDB, [
  'SELECT  ',
  'FatID, FatNum, Empresa, IDCtrl, Id,  ide_indPag, ide_mod,  ',
  'ide_serie, ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF, ',
  'ide_tpAmb, ide_finNFe, emit_CNPJ, emit_CPF,  ',
  'dest_CNPJ, dest_CPF,  ',
  'ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,  ',
  'ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete,  ',
  'ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII,  ',
  'ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,  ',
  'ICMSTot_vOutro, ICMSTot_vNF, ISSQNtot_vServ,  ',
  'ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,  ',
  'ISSQNtot_vCOFINS, RetTrib_vRetPIS, RetTrib_vRetCOFINS,  ',
  'RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,  ',
  'RetTrib_vBCRetPrev, RetTrib_vRetPrev, ModFrete,  ',
  'Cobr_Fat_nFat, Cobr_Fat_vOrig,  ',
  'Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,  ',
  'InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq,  ',
  'Compra_XNEmp, Compra_XPed, Compra_XCont,  ',
  ' ',
  'Status, infProt_Id,  ',
  'infProt_cStat, infCanc_cStat,  ',
  ' ',
  'ICMSRec_pRedBC,  ',
  'ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,  ',
  'IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,  ',
  'IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,  ',
  'PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,  ',
  'COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,  ',
  'DataFiscal, SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_ExpNat,  ',
  'SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum,  ',
  'SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais,  ',
  'SINTEGRA_ExpAverDta, CodInfoEmit, CodInfoDest,  ',
  'CriAForca, ide_hSaiEnt, ide_dhCont,  ',
  'emit_CRT, dest_email,  ',
  'Vagao, Balsa, CodInfoTrsp,  ',
  'OrdemServ, Situacao, Antigo,  ',
  'NFG_Serie, NF_ICMSAlq, NF_CFOP,  ',
  'Importado, NFG_SubSerie, NFG_ValIsen,  ',
  'NFG_NaoTrib, NFG_Outros, COD_MOD, COD_SIT, VL_ABAT_NT, ',
  'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk, FisRegCad ',
  'FROM nfecaba nfea  ',
  'WHERE ide_tpAmb<>2 ',
  'AND Status IN (100,101,102,110,301) ',
  LinFiltro,
  {
  'AND  ( ',
  '  COD_MOD IN ('01','1B','04','55') ',
  //'  OR ide_mod IN (1,55) ',
  ') ',
  }
  'AND Empresa=' + FEmpresa_Txt,
  'AND DataFiscal BETWEEN ' +  FIntervaloFiscal,
  'ORDER BY DataFiscal, ide_dEmi, IDCtrl ',
  '']);
  //Geral.MB_Teste(QrCabA_C.SQL.Text);
end;
*)
procedure TFmEfdIcmsIpiExporta_v03_0_2_a.ReopenCabA(Filtra: Boolean; Filtro: Variant);
var
  LinFiltro: String;
begin
  if Filtra then
    LinFiltro := 'AND EFD_EXP_REG = "' + Filtro + '"'
  else
    LinFiltro := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrCabA_C, Dmod.MyDB, [

  'SELECT nfea.*  ',
  '/*FatID, FatNum, Empresa, IDCtrl, Id,  ide_indPag, ide_mod,   ',
  'ide_serie, ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF,  ',
  'ide_tpAmb, ide_finNFe, emit_CNPJ, emit_CPF,   ',
  'dest_CNPJ, dest_CPF,   ',
  'ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,   ',
  'ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete,   ',
  'ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII,   ',
  'ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,   ',
  'ICMSTot_vOutro, ICMSTot_vNF, ISSQNtot_vServ,   ',
  'ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,   ',
  'ISSQNtot_vCOFINS, RetTrib_vRetPIS, RetTrib_vRetCOFINS,   ',
  'RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,   ',
  'RetTrib_vBCRetPrev, RetTrib_vRetPrev, ModFrete,   ',
  'Cobr_Fat_nFat, Cobr_Fat_vOrig,   ',
  'Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,   ',
  'InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq,   ',
  'Compra_XNEmp, Compra_XPed, Compra_XCont,   ',
  '  ',
  'Status, infProt_Id,   ',
  'infProt_cStat, infCanc_cStat,   ',
  '  ',
  'ICMSRec_pRedBC,   ',
  'ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,   ',
  'IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,   ',
  'IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,   ',
  'PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,   ',
  'COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,   ',
  'DataFiscal, SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_ExpNat,   ',
  'SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum,   ',
  'SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais,   ',
  'SINTEGRA_ExpAverDta, CodInfoEmit, CodInfoDest,   ',
  'CriAForca, ide_hSaiEnt, ide_dhCont,   ',
  'emit_CRT, dest_email,   ',
  'Vagao, Balsa, CodInfoTrsp,   ',
  'OrdemServ, Situacao, Antigo,   ',
  'NFG_Serie, NF_ICMSAlq, NF_CFOP,   ',
  'Importado, NFG_SubSerie, NFG_ValIsen,   ',
  'NFG_NaoTrib, NFG_Outros, COD_MOD, COD_SIT, VL_ABAT_NT,  ',
  'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk, FisRegCad */ ',
  'FROM nfecaba nfea   ',
  ' ',
  'LEFT JOIN efdinnnfscab einc  ',
  '  ON  ',
  '    einc.MovFatID=nfea.AtrelaFatID ',
  '    AND einc.MovFatNum=nfea.AtrelaFatNum ',
  '    AND einc.Empresa=nfea.Empresa ',
  '    AND einc.CHV_NFE=nfea.Id ',
  'WHERE nfea.ide_tpAmb<>2  ',
  'AND nfea.Status IN (100,101,102,110,301)  ',
  'AND nfea.Empresa=' + FEmpresa_Txt,
  'AND nfea.DataFiscal BETWEEN ' +  FIntervaloFiscal,
  //'ORDER BY DataFiscal, ide_dEmi, IDCtrl ',
  'ORDER BY nfea.DataFiscal, einc.Controle  ',
  '']);
  //Geral.MB_Teste(QrCabA_C.SQL.Text);
end;


procedure TFmEfdIcmsIpiExporta_v03_0_2_a.ReopenEFD_C500();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_C500, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ',
  'NO_TERC, c500.*',
  'FROM efdicmsipic500 c500 ',
  'LEFT JOIN entidades ent ON ent.Codigo=c500.Terceiro',
  'WHERE c500.ImporExpor=3 ',
  'AND c500.AnoMes=' + FAnoMes_Txt,
  'AND c500.Empresa=' + FEmpresa_Txt,
  '']);
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.ReopenCampos(var Res: Boolean): Boolean;
const
  IniDir = 'C:\Dermatek\Downloads\';
  Arquivo = 'Tabelas_SPED_EFD.SQL';
  Titulo = 'Carregamento de tabelas SPED EFD';
  Filtro = '';
var
  //Arq: String;
  I, StateIndex: Integer;
  Noh: TTreeNode;
begin
  // Verificar se os campos foram cadastrados
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrCampos, DmodG.AllID_DB, [
    'SELECT * FROM spedefdicmsipiflds ',
    'WHERE Bloco="' + FBloco + '" ',
    'AND Registro="' + FRegistro + '"',
    'ORDER BY Numero ',
    '']);
*)
  if ((FBloco = '0') and (FRegistro = '0000'))
  or ((FBloco = '9') and (FRegistro = '9999')) then
  //or ((FBloco = 'B') and (FRegistro = 'B001')) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCampos, DmodG.AllID_DB, [
    'SELECT * FROM spedefdicmsipiflds ',
    'WHERE Bloco="' + FBloco + '" ',
    'AND Registro="' + FRegistro + '"',
    'ORDER BY Numero ',
    '']);
    if QrCampos.RecordCount = 0 then
    begin
      Geral.MB_ERRO('Nenhum campo do Registro ' + FRegistro + ' do Bloco ' +
      FBloco + ' foi localizado na base de dados!');
      if Geral.MB_Pergunta('Deseja carregar os campos na base e dados?') = ID_YES then
      begin
        FmPrincipal.AGBLaySPEDEFDClick(Self);
        Close;
        Exit;
      end;
    end;
  end
  else
  begin
    // Ver se foi selecionado
    for I := 0 to TVBlocos.Items.Count -1 do
    begin
      Noh := TVBlocos.Items[I];
      if (Noh.Text = FRegistro) then
      begin
        StateIndex := Noh.StateIndex;
        Break;
      end;
  (*
      if
      (
        (Noh.Parent = nil) and (Noh.Text = FBloco)
      ) or
      (
        (Noh.Parent <> nil) and (Noh.Text = FRegistro)
      ) then
      begin
        StateIndex := Noh.StateIndex;
        Break;
      end;
  *)
    end;
    if not (StateIndex in ([1(*Check Cinza*), 3(*Check Preto*)])) then Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCampos, DmodG.AllID_DB, [
    'SELECT fld.*  ',
    'FROM spedefdicmsipiflds fld ',
    'LEFT JOIN ' + VAR_MyPID_DB_NOME + '.' + FSPEDEFD_Blcs + ' blc ON blc.Bloco=fld.Bloco ',
    '  AND blc.Registro=fld.Registro ',
    'WHERE fld.Bloco="' + FBloco + '"  ',
    'AND fld.Registro="' + FRegistro + '" ',
    //'AND blc.StateIndex IN (1,3) ',
    'ORDER BY fld.Numero  ',
    '']);
    //Geral.MB_SQL(Self, QrCampos);
  end;
  //
  Result := QrCampos.RecordCount > 0;
  Res := Result;
  if not Result then
  begin
    Geral.MB_ERRO('Nenhum campo do Registro ' + FRegistro + ' do Bloco ' +
    FBloco + ' foi localizado na base de dados!');
    if Geral.MB_Pergunta('Deseja carregar os campos na base e dados?') = ID_YES then
    begin
      FmPrincipal.AGBLaySPEDEFDClick(Self);
      Close;
      Exit;
    end;
    //
  end else
  begin
    // Abrir s� os campos v�lidos
    UnDmkDAC_PF.AbreMySQLQuery0(QrCampos, DmodG.AllID_DB, [
      'SELECT * FROM spedefdicmsipiflds ',
      'WHERE Bloco="' + FBloco + '" ',
      'AND Registro="' + FRegistro + '"',
      'AND VersaoIni <=' + FVersao,
      'AND (VersaoFim=0 OR VersaoFim>=' + FVersao + ') ',
      'ORDER BY Numero ',
      '']);
    //
    Result := QrCampos.RecordCount > 0;
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.ReopenEFD_E110();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E110, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie110 E110 ',
  'WHERE E110.ImporExpor=' + Geral.FF0(QrEFD_E100ImporExpor.Value),
  'AND E110.Empresa=' + Geral.FF0(QrEFD_E100Empresa.Value),
  'AND E110.AnoMes=' + Geral.FF0(QrEFD_E100AnoMes.Value),
  'AND E110.LinArq=' + Geral.FF0(QrEFD_E100LinArq.Value),
  '']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.ReopenEFD_E520();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E520, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie520 E520 ',
  'WHERE E520.ImporExpor=' + Geral.FF0(QrEFD_E100ImporExpor.Value),
  'AND E520.Empresa=' + Geral.FF0(QrEFD_E100Empresa.Value),
  'AND E520.AnoMes=' + Geral.FF0(QrEFD_E100AnoMes.Value),
  'AND E520.LinArq=' + Geral.FF0(QrEFD_E100LinArq.Value),
  '']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.ReopenEFD_E530();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_E530, Dmod.MyDB, [
  'SELECT *',
  'FROM efdicmsipie530 E530 ',
  'WHERE E530.ImporExpor=' + Geral.FF0(QrEFD_E520ImporExpor.Value),
  'AND E530.Empresa=' + Geral.FF0(QrEFD_E520Empresa.Value),
  'AND E530.AnoMes=' + Geral.FF0(QrEFD_E520AnoMes.Value),
  'AND E530.LinArq=' + Geral.FF0(QrEFD_E520LinArq.Value),
  'AND E530.E520=' + Geral.FF0(QrEFD_E520LinArq.Value),
  '']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.ReopenEmpresa();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresa, Dmod.MyDB, [
  'SELECT Tipo, ENumero, PNumero, ELograd, PLograd, ECEP, PCEP,  ',
  'IE, ECodMunici, PCodMunici, NIRE, SUFRAMA, ',
  'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOME_ENT,  ',
  'IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJ_CPF,  ',
  'IF(en.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,  ',
  'IF(en.Tipo=0, en.Fantasia, en.Apelido) NO_2_ENT,  ',
  'IF(en.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,  ',
  'IF(en.Tipo=0, en.ERua, en.PRua) RUA,  ',
  'IF(en.Tipo=0, en.ECompl, en.PCompl) COMPL, ',
  'IF(en.Tipo=0, en.EBairro, en.PBairro) BAIRRO, ',
  'IF(en.Tipo=0, en.ETe1, en.PTe1) TE1, ',
  'IF(en.Tipo=0, en.EFax, en.PFax) FAX, ',
  'IF(en.Tipo=0, en.EEmail, en.PEmail) EMAIL, ',
  'en.Codigo ',
  'FROM entidades en  ',
  'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
  'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
  'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
  'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
  'WHERE en.Codigo=' + Geral.FF0(FEmpresa_Int),
  '']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.ReopenEnder(Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEnder, Dmod.MyDB, [
  'SELECT Tipo, ENumero, PNumero, ELograd, PLograd,  ',
  'ECEP, PCEP, IE, ECodMunici, PCodMunici, ',
  ' NIRE, SUFRAMA, ECodiPais, PCodiPais, ',
  'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOME_ENT,  ',
  'IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJ_CPF,  ',
  'IF(en.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,  ',
  'IF(en.Tipo=0, en.Fantasia, en.Apelido) NO_2_ENT,  ',
  'IF(en.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,  ',
  'IF(en.Tipo=0, en.ERua, en.PRua) RUA,  ',
  'IF(en.Tipo=0, en.ECompl, en.PCompl) COMPL, ',
  'IF(en.Tipo=0, en.EBairro, en.PBairro) BAIRRO, ',
  'IF(en.Tipo=0, en.ETe1, en.PTe1) TE1, ',
  'IF(en.Tipo=0, en.EFax, en.PFax) FAX, ',
  'IF(en.Tipo=0, en.EEmail, en.PEmail) EMAIL, ',
  'en.Codigo ',
  'FROM entidades en  ',
  'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
  'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
  'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
  'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
  'WHERE en.Codigo=' + Geral.FF0(Entidade),
  '']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.ReopenVersoes();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVersao, DModG.AllID_DB, [
  'SELECT CodTxt ',
  'FROM tbspedefdicmsipi011 ',
  'WHERE (DataFim >="' + FDataFim_Txt + '") ',
  'OR (DataIni <"' + FDataFim_Txt + '" AND DataFim<2) ',
  '']);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.RGCOD_FINClick(Sender: TObject);
begin
  HabilitaBotaoOK();
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.SalvaArquivo(): Boolean;
var
  dIni, dFim, Arq, Dir, Arquivo: String;
  Continua: Boolean;
begin
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando dados em arquivo!');
  //
  dIni := FormatDateTime('YYYYMMDD', TPDataIni.Date);
  dFim := FormatDateTime('YYYYMMDD', TPDataFim.Date);
  Arq := QrEmpresaCNPJ_CPF.Value + '_' + dIni + '_' + dFim;
  Dir := '';
  Dir := QrParamsEmpSPED_EFD_Path.Value;
  if Trim(Dir) = '' then
    Dir := 'C:\Dermatek\SPED\EFD';
  Arquivo := dmkPF.CaminhoArquivo(Dir, Arq, 'txt');
  if FileExists(Arquivo) then
    Continua := Geral.MB_Pergunta('O arquivo "' + Arquivo +
    ' j� existe. Deseja sobrescrev�-lo?') = ID_YES
  else
    Continua := True;
  if Continua then
  begin
    //if MLAGeral.ExportaMemoToFileExt(MeGerado, Arquivo, True, False, True, 1310, Null) then
    if MyObjects.ExportaMemoToFileExt(MeGerado, Arquivo, True, False, etxtsaNaoMuda, 1310, Null) then
    begin
      Result := True;
      StatusBar.Panels[3].Text := Arquivo;
      //
      Geral.MB_Aviso('O arquivo digital foi salvo como: ' + sLineBreak
      + sLineBreak + Arquivo + sLineBreak + sLineBreak);
    end;
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.SbBloco_K_completoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBloco_K_completo, SbBloco_K_completo);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.SbBloco_K_estoqueClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBloco_K_estoque, SbBloco_K_estoque);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.SbImplementadoClick(Sender: TObject);
begin
  ConfiguraTreeView2(
  [
    '0200', '0400', '0460', 'C170', 'C190', 'C590', 'D190', 'D590', 'K200', 'K280'
  ],
  [
    '0', '0001', '0002', '0005', '0100', '0150', '0190', '0990',
    'B', 'B001', 'B990',
    'C', 'C001', 'C100', 'C500', 'C990',
    'D', 'D001', 'D100', 'D500', 'D990',
    'E', 'E001', 'E100', 'E110', 'E500', 'E520', 'E990',
    'G', 'G001', 'G990',
    '1', '1001', '1010', '1990',
    'H', 'H001', 'H990',
    '9', '9001', '9900', '9990', '9999',
    'K', 'K001', 'K100', 'K990']);

end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.SbTeste_minimo_EFD_ICMS_IPIClick(Sender: TObject);
begin
  ConfiguraTreeView2(
  [
    '0200', 'K200', 'K280'
  ],
  [
    '0', '0001', '0002', '0005', '0100', '0150', '0190', '0990',
    'B', 'B001', 'B990',
    'C', 'C001', 'C990',
    'D', 'D001', 'D990',
    'E', 'E001', 'E100', 'E110', 'E500', 'E520', 'E990',
    'G', 'G001', 'G990',
    '1', '1001', '1010', '1990',
    'H', 'H001', 'H990',
    '9', '9001', '9900', '9990', '9999',
    'K', 'K001', 'K100', 'K990']);

end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.SelReg(Reg: String): Boolean;
var
  I: Integer;
  Noh: TTreeNode;
begin
  Result := False;
  for I := 0 to TVBlocos.Items.Count -1 do
  begin
    Noh := TVBlocos.Items[I];
    if Uppercase(Noh.Text) = Uppercase(Reg) then
    begin
      //Geral.MB_Info(Noh.Text + sLineBreak + 'StateIndex = ' +
      //Geral.FF0(Noh.StateIndex) + sLineBreak + 'ImageIndex = ' + Geral.FF0(Noh.ImageIndex));
      Result := Noh.StateIndex in ([1,3]);
      Exit;
    end;
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.SpeedButton1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBloco_K_completo, SbBloco_K_completo);
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.TamanhoDifereDoObrigatorio(Valor: String): Boolean;
begin
  if QrCamposTObrig.Value = 1 then
  begin
    if Uppercase(QrCamposCObrig.Value) = 'O' then
      Result := Length(Valor) <> QrCamposTam.Value
    else
      Result := (Length(Valor) <> 0) and ((Length(Valor) <> QrCamposTam.Value));
  end else
    Result := False;
end;

function TFmEfdIcmsIpiExporta_v03_0_2_a.TamanhoMuitoGrande(Valor: String): Boolean;
begin
  if QrCamposTam.Value = 0 then
    Result := Length(Valor) > 255
  else
    Result := Length(Valor) > QrCamposTam.Value;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.TVBlocosClick(Sender: TObject);
var
  P: TPoint;
  Nivel, (*Codigo,*) Ativa: Integer;
  cNx: String;
begin
  GetCursorPos(P);
  P := TVBlocos.ScreenToClient(P);
  if (htOnStateIcon in TVBlocos.GetHitTestInfoAt(P.X,P.Y)) then
  begin
    if TVBlocos.Selected.StateIndex = 2 then
      Ativa := 1
    else
      Ativa := 0;
    //
    Nivel := 5 - MyObjects.ToggleTreeViewCheckBoxes_Child(TVBlocos.Selected);
    {
    Codigo := TVBlocos.Selected.ImageIndex;
    dmkEdit1.ValueVariant := Nivel;
    dmkEdit2.ValueVariant := Codigo;
    //
    }
    cNx := Geral.FF0(Nivel);
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE ' + FSPEDEFD_Blcs + ' SET ');
    DModG.QrUpdPID1.SQL.Add('Ativo=' + Geral.FF0(Ativa));
    DModG.QrUpdPID1.SQL.Add('WHERE Registro="' + TVBlocos.Selected.Text + '"');
    DModG.QrUpdPID1.ExecSQL;
    //
  end;
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.TVBlocosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_SPACE) and Assigned(TVBlocos.Selected) then
    MyObjects.ToggleTreeViewCheckBoxes_Child(TVBlocos.Selected);
end;
procedure TFmEfdIcmsIpiExporta_v03_0_2_a.VerificaDESCR_ITEM(DESCR_ITEM: String;
  Query: TmySQLQuery);
begin
  if Trim(DESCR_ITEM) = '' then
    (*//*)Geral.MB_SQL(Self, Query);
end;

procedure TFmEfdIcmsIpiExporta_v03_0_2_a.VerificaImplemantacoesNaoFeitas();
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
(*  Resolvido em 2016-02-08 !!! ???
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT i.prod_CFOP  ',
    'FROM nfeitsi i ',
    'LEFT JOIN nfecaba a ON  ',
    '  a.FatID=i.FatID AND ',
    '  a.FatNum=i.FatNum AND ',
    '  a.Empresa=i.Empresa ',
    'WHERE i.prod_CFOP = 1605 ',
    'OR i.prod_CFOP = 5605 ',
    'AND a.DataFiscal BETWEEN "' + FDataIni_Txt + '" AND "' + FDataFim_Txt + '" ',
    '']);
    if Qry.RecordCount > 0 then
      Geral.MB_ERRO('ERRO! CFOPs 1605 e 5605 n�o implementados no Registro E110!!');
*)
  finally
    Qry.Free;
  end;
end;

{ TODO : Atualiza tabelas EFD na web }


{
DROP TABLE IF EXISTS _Copy_Bloco_Flds_;
CREATE TABLE _Copy_Bloco_Flds_
SElECT * FROM spedefdicmsipiflds
WHERE
(
  RegIstro='C195'
OR
  RegIstro='C197'
)
;
UPDATE _Copy_Bloco_Flds_
SET Bloco="D"
;
UPDATE _Copy_Bloco_Flds_
SET REGISTRO="D195"
WHERE REGISTRO="D195"
;
UPDATE _Copy_Bloco_Flds_
SET REGISTRO="D197"
WHERE REGISTRO="D197"
;
SELECT * FROM _Copy_Bloco_Flds_;
INSERT INTO spedefdicmsipiflds
SELECT * FROM _Copy_Bloco_Flds_;

}
end.
