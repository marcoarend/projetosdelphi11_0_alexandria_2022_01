unit SPED_EFD_Compara;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, Mask, Menus, dmkDBGrid, UnDmkProcFunc, DmkDAC_PF,
  UnDmkEnums;

type
  TFmSPED_EFD_Compara = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    SpeedButton2: TSpeedButton;
    EdEntidade: TdmkEdit;
    EdAnoMes: TdmkEdit;
    QrC100: TmySQLQuery;
    DsC100: TDataSource;
    QrNFeA: TmySQLQuery;
    QrPQE: TmySQLQuery;
    DsPQE: TDataSource;
    QrPQENO_ENT: TWideStringField;
    QrPQECodigo: TIntegerField;
    QrPQEData: TDateField;
    QrPQEIQ: TIntegerField;
    QrPQEmodNF: TSmallintField;
    QrPQESerie: TIntegerField;
    QrPQENF: TIntegerField;
    QrPQEValorNF: TFloatField;
    QrPQEIts: TmySQLQuery;
    QrPQEItsCodigo: TIntegerField;
    QrPQEItsControle: TIntegerField;
    QrPQEItsConta: TIntegerField;
    QrPQEItsInsumo: TIntegerField;
    QrPQEItsVolumes: TIntegerField;
    QrPQEItsPesoVB: TFloatField;
    QrPQEItsPesoVL: TFloatField;
    QrPQEItsValorItem: TFloatField;
    QrPQEItsIPI: TFloatField;
    QrPQEItsRIPI: TFloatField;
    QrPQEItsCFin: TFloatField;
    QrPQEItsICMS: TFloatField;
    QrPQEItsRICMS: TFloatField;
    QrPQEItsTotalCusto: TFloatField;
    QrPQEItsTotalPeso: TFloatField;
    QrPQEItsprod_CFOP: TWideStringField;
    QrPQEItsIPI_pIPI: TFloatField;
    QrPQEItsIPI_vIPI: TFloatField;
    QrNFsMPs: TmySQLQuery;
    QrNFsMPsNF: TIntegerField;
    QrNFsMPsPecasNF: TFloatField;
    QrNFsMPsPNF: TFloatField;
    QrNFsMPsPLE: TFloatField;
    QrNFsMPsCMPValor: TFloatField;
    QrNFsMPsNF_Modelo: TWideStringField;
    QrNFsMPsNF_Serie: TWideStringField;
    QrNFsMPsCFOP: TWideStringField;
    QrNFsMPsMyForn: TLargeintField;
    DsNFsMPs: TDataSource;
    QrNFsMPsEmissao: TDateField;
    QrNFsMPsNO_FRN: TWideStringField;
    QrMPInIts: TmySQLQuery;
    QrSumMPI: TmySQLQuery;
    QrSumMPIPecasNF: TFloatField;
    QrSumMPIPNF: TFloatField;
    QrSumMPIPLE: TFloatField;
    QrSumMPICMPValor: TFloatField;
    DsSumMPI: TDataSource;
    QrMPInItsNO_FRN: TWideStringField;
    QrMPInItsEmissao: TDateField;
    QrMPInItsNF: TIntegerField;
    QrMPInItsPecasNF: TFloatField;
    QrMPInItsPNF: TFloatField;
    QrMPInItsPLE: TFloatField;
    QrMPInItsCMPValor: TFloatField;
    QrMPInItsNF_Modelo: TWideStringField;
    QrMPInItsNF_Serie: TWideStringField;
    QrMPInItsCFOP: TWideStringField;
    QrMPInItsMyForn: TLargeintField;
    QrMPInItsConta: TIntegerField;
    QrMPInItsTipificacao: TIntegerField;
    QrMPInItsAnimal: TSmallintField;
    QrMPInItsControle: TIntegerField;
    QrSumMPIPRECO_KG: TFloatField;
    QrSumMPIPRECO_PC: TFloatField;
    DsMPInIts: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet5: TTabSheet;
    MeErros: TMemo;
    Panel7: TPanel;
    Label3: TLabel;
    ProgressBar1: TProgressBar;
    BitBtn1: TBitBtn;
    Panel13: TPanel;
    BitBtn2: TBitBtn;
    C500: TTabSheet;
    PageControl4: TPageControl;
    TabSheet6: TTabSheet;
    QrC500: TmySQLQuery;
    DsC500: TDataSource;
    QrC500NO_ENT: TWideStringField;
    QrC500Entidade: TIntegerField;
    QrC500ImporExpor: TSmallintField;
    QrC500AnoMes: TIntegerField;
    QrC500Empresa: TIntegerField;
    QrC500LinArq: TIntegerField;
    QrC500REG: TWideStringField;
    QrC500IND_OPER: TWideStringField;
    QrC500IND_EMIT: TWideStringField;
    QrC500COD_PART: TWideStringField;
    QrC500COD_MOD: TWideStringField;
    QrC500SER: TWideStringField;
    QrC500COD_CONS: TWideStringField;
    QrC500NUM_DOC: TIntegerField;
    QrC500DT_DOC: TDateField;
    QrC500DT_E_S: TDateField;
    QrC500VL_DOC: TFloatField;
    QrC500VL_DESC: TFloatField;
    QrC500VL_FORN: TFloatField;
    QrC500VL_SERV_NT: TFloatField;
    QrC500VL_TERC: TFloatField;
    QrC500VL_DA: TFloatField;
    QrC500VL_BC_ICMS: TFloatField;
    QrC500VL_ICMS: TFloatField;
    QrC500VL_BC_ICMS_ST: TFloatField;
    QrC500VL_ICMS_ST: TFloatField;
    QrC500COD_INF: TWideStringField;
    QrC500VL_PIS: TFloatField;
    QrC500VL_COFINS: TFloatField;
    QrC500COD_GRUPO_TENSAO: TWideStringField;
    Panel14: TPanel;
    DBGrid5: TDBGrid;
    Panel15: TPanel;
    Splitter2: TSplitter;
    BtSeqAcoesC500: TBitBtn;
    PMSeqAcoes_C500: TPopupMenu;
    C500_SeqAcoes_1: TMenuItem;
    C500_SeqAcoes_2: TMenuItem;
    QrC001: TmySQLQuery;
    DsC001: TDataSource;
    QrC001ImporExpor: TSmallintField;
    QrC001AnoMes: TIntegerField;
    QrC001Empresa: TIntegerField;
    QrC001LinArq: TIntegerField;
    QrC001REG: TWideStringField;
    QrC001IND_MOV: TWideStringField;
    Panel16: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    QrC500COD_SIT: TWideStringField;
    QrC500SUB: TWideStringField;
    QrC500TP_LIGACAO: TWideStringField;
    QrC590: TmySQLQuery;
    DsC590: TDataSource;
    QrC590ImporExpor: TSmallintField;
    QrC590AnoMes: TIntegerField;
    QrC590Empresa: TIntegerField;
    QrC590LinArq: TIntegerField;
    QrC590C500: TIntegerField;
    QrC590REG: TWideStringField;
    QrC590CST_ICMS: TIntegerField;
    QrC590CFOP: TIntegerField;
    QrC590ALIQ_ICMS: TFloatField;
    QrC590VL_OPR: TFloatField;
    QrC590VL_BC_ICMS: TFloatField;
    QrC590VL_ICMS: TFloatField;
    QrC590VL_BC_ICMS_ST: TFloatField;
    QrC590VL_ICMS_ST: TFloatField;
    QrC590VL_RED_BC: TFloatField;
    QrC590COD_OBS: TWideStringField;
    TabSheet7: TTabSheet;
    QrD500: TmySQLQuery;
    DsD500: TDataSource;
    DsD590: TDataSource;
    QrD590: TmySQLQuery;
    QrD500NO_ENT: TWideStringField;
    QrD500Entidade: TIntegerField;
    QrD500ImporExpor: TSmallintField;
    QrD500AnoMes: TIntegerField;
    QrD500Empresa: TIntegerField;
    QrD500LinArq: TIntegerField;
    QrD500REG: TWideStringField;
    QrD500IND_OPER: TWideStringField;
    QrD500IND_EMIT: TWideStringField;
    QrD500COD_PART: TWideStringField;
    QrD500COD_MOD: TWideStringField;
    QrD500COD_SIT: TWideStringField;
    QrD500SER: TWideStringField;
    QrD500SUB: TWideStringField;
    QrD500NUM_DOC: TIntegerField;
    QrD500DT_DOC: TDateField;
    QrD500DT_A_P: TDateField;
    QrD500VL_DOC: TFloatField;
    QrD500VL_DESC: TFloatField;
    QrD500VL_SERV: TFloatField;
    QrD500VL_SERV_NT: TFloatField;
    QrD500VL_TERC: TFloatField;
    QrD500VL_DA: TFloatField;
    QrD500VL_BC_ICMS: TFloatField;
    QrD500VL_ICMS: TFloatField;
    QrD500COD_INF: TWideStringField;
    QrD500VL_PIS: TFloatField;
    QrD500VL_COFINS: TFloatField;
    QrD500COD_CTA: TWideStringField;
    QrD500TP_ASSINANTE: TWideStringField;
    PMSeqAcoes_D500: TPopupMenu;
    D500_SeqAcoes_1: TMenuItem;
    D500_SeqAcoes_2: TMenuItem;
    QrD590ImporExpor: TSmallintField;
    QrD590AnoMes: TIntegerField;
    QrD590Empresa: TIntegerField;
    QrD590LinArq: TIntegerField;
    QrD590D500: TIntegerField;
    QrD590REG: TWideStringField;
    QrD590CST_ICMS: TIntegerField;
    QrD590CFOP: TIntegerField;
    QrD590ALIQ_ICMS: TFloatField;
    QrD590VL_OPR: TFloatField;
    QrD590VL_BC_ICMS: TFloatField;
    QrD590VL_ICMS: TFloatField;
    QrD590VL_BC_ICMS_ST: TFloatField;
    QrD590VL_ICMS_ST: TFloatField;
    QrD590VL_RED_BC: TFloatField;
    QrD590COD_OBS: TWideStringField;
    QrD001: TmySQLQuery;
    DsD001: TDataSource;
    QrD001ImporExpor: TSmallintField;
    QrD001AnoMes: TIntegerField;
    QrD001Empresa: TIntegerField;
    QrD001LinArq: TIntegerField;
    QrD001REG: TWideStringField;
    QrD001IND_MOV: TWideStringField;
    QrD001Stat_D100i: TSmallintField;
    QrD001Stat_D500i: TSmallintField;
    TabSheet9: TTabSheet;
    PageControl5: TPageControl;
    TabSheet8: TTabSheet;
    Splitter3: TSplitter;
    Panel17: TPanel;
    DBGrid6: TDBGrid;
    Panel18: TPanel;
    BtSeqAcoesD500: TBitBtn;
    PageControl6: TPageControl;
    TabSheet10: TTabSheet;
    Splitter4: TSplitter;
    Panel19: TPanel;
    DBGrid7: TDBGrid;
    Panel20: TPanel;
    BtSeqAcoesD100: TBitBtn;
    QrD100: TmySQLQuery;
    DsD100: TDataSource;
    DsD190: TDataSource;
    QrD190: TmySQLQuery;
    QrD190ImporExpor: TSmallintField;
    QrD190AnoMes: TIntegerField;
    QrD190Empresa: TIntegerField;
    QrD190LinArq: TIntegerField;
    QrD190D100: TIntegerField;
    QrD190REG: TWideStringField;
    QrD190CST_ICMS: TIntegerField;
    QrD190CFOP: TIntegerField;
    QrD190ALIQ_ICMS: TFloatField;
    QrD190VL_OPR: TFloatField;
    QrD190VL_BC_ICMS: TFloatField;
    QrD190VL_ICMS: TFloatField;
    QrD190VL_RED_BC: TFloatField;
    QrD190COD_OBS: TWideStringField;
    PMSeqAcoes_D100: TPopupMenu;
    D100_SeqAcoes_1: TMenuItem;
    D100_SeqAcoes_2: TMenuItem;
    QrC100NO_ENT: TWideStringField;
    QrC100Entidade: TIntegerField;
    QrC100ImporExpor: TSmallintField;
    QrC100AnoMes: TIntegerField;
    QrC100Empresa: TIntegerField;
    QrC100LinArq: TIntegerField;
    QrC100REG: TWideStringField;
    QrC100IND_OPER: TWideStringField;
    QrC100IND_EMIT: TWideStringField;
    QrC100COD_PART: TWideStringField;
    QrC100COD_MOD: TWideStringField;
    QrC100COD_SIT: TWideStringField;
    QrC100SER: TWideStringField;
    QrC100NUM_DOC: TIntegerField;
    QrC100CHV_NFE: TWideStringField;
    QrC100DT_DOC: TDateField;
    QrC100DT_E_S: TDateField;
    QrC100VL_DOC: TFloatField;
    QrC100IND_PGTO: TWideStringField;
    QrC100VL_DESC: TFloatField;
    QrC100VL_ABAT_NT: TFloatField;
    QrC100VL_MERC: TFloatField;
    QrC100IND_FRT: TWideStringField;
    QrC100VL_FRT: TFloatField;
    QrC100VL_SEG: TFloatField;
    QrC100VL_OUT_DA: TFloatField;
    QrC100VL_BC_ICMS: TFloatField;
    QrC100VL_ICMS: TFloatField;
    QrC100VL_BC_ICMS_ST: TFloatField;
    QrC100VL_ICMS_ST: TFloatField;
    QrC100VL_IPI: TFloatField;
    QrC100VL_PIS: TFloatField;
    QrC100VL_COFINS: TFloatField;
    QrC100VL_PIS_ST: TFloatField;
    QrC100VL_COFINS_ST: TFloatField;
    QrC100ParTipo: TIntegerField;
    QrC100ParCodi: TIntegerField;
    QrC100FatID: TIntegerField;
    QrC100FatNum: TIntegerField;
    QrD100NO_ENT: TWideStringField;
    QrD100Entidade: TIntegerField;
    QrD100ImporExpor: TSmallintField;
    QrD100AnoMes: TIntegerField;
    QrD100Empresa: TIntegerField;
    QrD100LinArq: TIntegerField;
    QrD100REG: TWideStringField;
    QrD100IND_OPER: TWideStringField;
    QrD100IND_EMIT: TWideStringField;
    QrD100COD_PART: TWideStringField;
    QrD100COD_MOD: TWideStringField;
    QrD100COD_SIT: TWideStringField;
    QrD100SER: TWideStringField;
    QrD100SUB: TWideStringField;
    QrD100NUM_DOC: TIntegerField;
    QrD100CHV_CTE: TWideStringField;
    QrD100DT_DOC: TDateField;
    QrD100DT_A_P: TDateField;
    QrD100CHV_CTE_REF: TWideStringField;
    QrD100VL_DOC: TFloatField;
    QrD100VL_DESC: TFloatField;
    QrD100IND_FRT: TWideStringField;
    QrD100VL_SERV: TFloatField;
    QrD100VL_BC_ICMS: TFloatField;
    QrD100VL_ICMS: TFloatField;
    QrD100VL_NT: TFloatField;
    QrD100COD_INF: TWideStringField;
    QrD100COD_CTA: TWideStringField;
    Panel21: TPanel;
    DBG_C100: TdmkDBGrid;
    Panel22: TPanel;
    Label13: TLabel;
    DBEdit3: TDBEdit;
    QrC100E_S: TWideStringField;
    QrC100P_T: TWideStringField;
    PageControl3: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    PageControl7: TPageControl;
    TabSheet11: TTabSheet;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    CkFornecedor_0_0: TCheckBox;
    CkNF_0_0: TCheckBox;
    CkValorDoc_0_0: TCheckBox;
    CkValorMerc_0_0: TCheckBox;
    CkFiltra_0_0: TCheckBox;
    BtExclui_0_0: TBitBtn;
    BtEstatistica_0_0: TBitBtn;
    Pn_0_0: TPanel;
    LaAviso_0_0: TLabel;
    PB_0_0: TProgressBar;
    BtRastreia_0_0: TBitBtn;
    Panel5: TPanel;
    BtSaida_0_0: TBitBtn;
    Panel12: TPanel;
    Label8: TLabel;
    DBGrid2: TDBGrid;
    TabSheet12: TTabSheet;
    Splitter1: TSplitter;
    Panel6: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    GroupBox2: TGroupBox;
    CkFornecedor_0_1: TCheckBox;
    CkNF_0_1: TCheckBox;
    CkValorDoc_0_1: TCheckBox;
    CkDiasAD_0_1: TCheckBox;
    CkFiltra_0_1: TCheckBox;
    BtExclui_0_1: TBitBtn;
    BtEstatistica_0_1: TBitBtn;
    EdDiasA_0_1: TdmkEdit;
    EdDiasD_0_1: TdmkEdit;
    DBGrid3: TDBGrid;
    Panel8: TPanel;
    Label4: TLabel;
    ProgressBar2: TProgressBar;
    BtRastreia_0_1: TBitBtn;
    Panel9: TPanel;
    BitBtn4: TBitBtn;
    Panel10: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Panel11: TPanel;
    Label7: TLabel;
    DBGrid4: TDBGrid;
    Label14: TLabel;
    Label15: TLabel;
    DBG_C170: TDBGrid;
    Splitter5: TSplitter;
    QrC170: TmySQLQuery;
    DsC170: TDataSource;
    BtSeqAcoesC100: TBitBtn;
    PMSeqAcoes_C100: TPopupMenu;
    C100_SeqAcoes_1: TMenuItem;
    C100_SeqAcoes_2: TMenuItem;
    C100_SeqAcoes_3: TMenuItem;
    C100_SeqAcoes_4: TMenuItem;
    QrLocPQE: TmySQLQuery;
    QrLocPQECodigo: TIntegerField;
    QrLocPQEValorNF: TFloatField;
    QrLocMPc: TmySQLQuery;
    QrLocMPcConta: TIntegerField;
    QrLocEMP: TmySQLQuery;
    QrLocEMPCodigo: TIntegerField;
    QrC001Stat_C100: TSmallintField;
    QrC001Stat_C500: TSmallintField;
    QrCabA: TmySQLQuery;
    QrCabAIDCtrl: TIntegerField;
    QrCabAFatID: TIntegerField;
    QrCabAFatNum: TIntegerField;
    QrCabAEmpresa: TIntegerField;
    QrC100ACHOU_TXT: TWideStringField;
    QrExclMul: TmySQLQuery;
    QrExclMulFatID: TIntegerField;
    QrExclMulFatNum: TIntegerField;
    QrExclMulEmpresa: TIntegerField;
    QrExclMulinfProt_cStat: TIntegerField;
    QrC190: TmySQLQuery;
    DsC190: TDataSource;
    QrC170ImporExpor: TSmallintField;
    QrC170AnoMes: TIntegerField;
    QrC170Empresa: TIntegerField;
    QrC170LinArq: TIntegerField;
    QrC170C100: TIntegerField;
    QrC170GraGruX: TIntegerField;
    QrC170REG: TWideStringField;
    QrC170NUM_ITEM: TIntegerField;
    QrC170COD_ITEM: TWideStringField;
    QrC170DESCR_COMPL: TWideStringField;
    QrC170QTD: TFloatField;
    QrC170VL_ITEM: TFloatField;
    QrC170VL_DESC: TFloatField;
    QrC170IND_MOV: TWideStringField;
    QrC170CST_ICMS: TIntegerField;
    QrC170CFOP: TIntegerField;
    QrC170COD_NAT: TWideStringField;
    QrC170VL_BC_ICMS: TFloatField;
    QrC170ALIQ_ICMS: TFloatField;
    QrC170VL_ICMS: TFloatField;
    QrC170VL_BC_ICMS_ST: TFloatField;
    QrC170ALIQ_ST: TFloatField;
    QrC170VL_ICMS_ST: TFloatField;
    QrC170IND_APUR: TWideStringField;
    QrC170CST_IPI: TWideStringField;
    QrC170COD_ENQ: TWideStringField;
    QrC170VL_BC_IPI: TFloatField;
    QrC170ALIQ_IPI: TFloatField;
    QrC170VL_IPI: TFloatField;
    QrC170CST_PIS: TSmallintField;
    QrC170VL_BC_PIS: TFloatField;
    QrC170ALIQ_PIS_p: TFloatField;
    QrC170QUANT_BC_PIS: TFloatField;
    QrC170ALIQ_PIS_r: TFloatField;
    QrC170VL_PIS: TFloatField;
    QrC170CST_COFINS: TSmallintField;
    QrC170VL_BC_COFINS: TFloatField;
    QrC170ALIQ_COFINS_p: TFloatField;
    QrC170QUANT_BC_COFINS: TFloatField;
    QrC170ALIQ_COFINS_r: TFloatField;
    QrC170VL_COFINS: TFloatField;
    QrC170COD_CTA: TWideStringField;
    QrC170UNID: TWideStringField;
    QrPQE_2_: TmySQLQuery;
    QrPQE_2_FatNum: TIntegerField;
    QrPQE_2_dtEmi: TDateField;
    QrPQE_2_dtSaiEnt: TDateField;
    QrPQE_2_DataFiscal: TDateField;
    QrPQE_2_ValTot: TFloatField;
    QrPQE_2_Empresa: TIntegerField;
    QrPQE_2_CodInfoDest: TIntegerField;
    QrPQE_2_CodInfoEmit: TIntegerField;
    QrPQE_2_Id: TWideStringField;
    QrPQE_2_ide_mod: TSmallintField;
    QrPQE_2_Serie: TIntegerField;
    QrPQE_2_nNF: TIntegerField;
    QrPQE_2_Frete: TFloatField;
    QrPQE_2_Seguro: TFloatField;
    QrPQE_2_Desconto: TFloatField;
    QrPQE_2_IPI: TFloatField;
    QrPQE_2_PIS: TFloatField;
    QrPQE_2_COFINS: TFloatField;
    QrPQE_2_Outros: TFloatField;
    QrPQEIts_2_: TmySQLQuery;
    QrPQEIts_2_CUSTOITEM: TFloatField;
    QrPQEIts_2_VALORKG: TFloatField;
    QrPQEIts_2_TOTALKGBRUTO: TFloatField;
    QrPQEIts_2_CUSTOKG: TFloatField;
    QrPQEIts_2_PRECOKG: TFloatField;
    QrPQEIts_2_Codigo: TIntegerField;
    QrPQEIts_2_Controle: TIntegerField;
    QrPQEIts_2_Conta: TIntegerField;
    QrPQEIts_2_Volumes: TIntegerField;
    QrPQEIts_2_PesoVB: TFloatField;
    QrPQEIts_2_PesoVL: TFloatField;
    QrPQEIts_2_ValorItem: TFloatField;
    QrPQEIts_2_IPI: TFloatField;
    QrPQEIts_2_RIPI: TFloatField;
    QrPQEIts_2_CFin: TFloatField;
    QrPQEIts_2_ICMS: TFloatField;
    QrPQEIts_2_RICMS: TFloatField;
    QrPQEIts_2_TotalCusto: TFloatField;
    QrPQEIts_2_Insumo: TIntegerField;
    QrPQEIts_2_TotalPeso: TFloatField;
    QrPQEIts_2_NOMEPQ: TWideStringField;
    QrPQEIts_2_prod_cProd: TWideStringField;
    QrPQEIts_2_prod_cEAN: TWideStringField;
    QrPQEIts_2_prod_xProd: TWideStringField;
    QrPQEIts_2_prod_NCM: TWideStringField;
    QrPQEIts_2_prod_EX_TIPI: TWideStringField;
    QrPQEIts_2_prod_genero: TSmallintField;
    QrPQEIts_2_prod_CFOP: TWideStringField;
    QrPQEIts_2_prod_uCom: TWideStringField;
    QrPQEIts_2_prod_qCom: TFloatField;
    QrPQEIts_2_prod_vUnCom: TFloatField;
    QrPQEIts_2_prod_vProd: TFloatField;
    QrPQEIts_2_prod_cEANTrib: TWideStringField;
    QrPQEIts_2_prod_uTrib: TWideStringField;
    QrPQEIts_2_prod_qTrib: TFloatField;
    QrPQEIts_2_prod_vUnTrib: TFloatField;
    QrPQEIts_2_prod_vFrete: TFloatField;
    QrPQEIts_2_prod_vSeg: TFloatField;
    QrPQEIts_2_prod_vDesc: TFloatField;
    QrPQEIts_2_ICMS_Orig: TSmallintField;
    QrPQEIts_2_ICMS_CST: TSmallintField;
    QrPQEIts_2_ICMS_modBC: TSmallintField;
    QrPQEIts_2_ICMS_pRedBC: TFloatField;
    QrPQEIts_2_ICMS_vBC: TFloatField;
    QrPQEIts_2_ICMS_pICMS: TFloatField;
    QrPQEIts_2_ICMS_vICMS: TFloatField;
    QrPQEIts_2_ICMS_modBCST: TSmallintField;
    QrPQEIts_2_ICMS_pMVAST: TFloatField;
    QrPQEIts_2_ICMS_pRedBCST: TFloatField;
    QrPQEIts_2_ICMS_vBCST: TFloatField;
    QrPQEIts_2_ICMS_pICMSST: TFloatField;
    QrPQEIts_2_ICMS_vICMSST: TFloatField;
    QrPQEIts_2_IPI_cEnq: TWideStringField;
    QrPQEIts_2_IPI_CST: TSmallintField;
    QrPQEIts_2_IPI_vBC: TFloatField;
    QrPQEIts_2_IPI_qUnid: TFloatField;
    QrPQEIts_2_IPI_vUnid: TFloatField;
    QrPQEIts_2_IPI_pIPI: TFloatField;
    QrPQEIts_2_IPI_vIPI: TFloatField;
    QrPQEIts_2_PIS_CST: TSmallintField;
    QrPQEIts_2_PIS_vBC: TFloatField;
    QrPQEIts_2_PIS_pPIS: TFloatField;
    QrPQEIts_2_PIS_vPIS: TFloatField;
    QrPQEIts_2_PIS_qBCProd: TFloatField;
    QrPQEIts_2_PIS_vAliqProd: TFloatField;
    QrPQEIts_2_PISST_vBC: TFloatField;
    QrPQEIts_2_PISST_pPIS: TFloatField;
    QrPQEIts_2_PISST_qBCProd: TFloatField;
    QrPQEIts_2_PISST_vAliqProd: TFloatField;
    QrPQEIts_2_PISST_vPIS: TFloatField;
    QrPQEIts_2_COFINS_CST: TSmallintField;
    QrPQEIts_2_COFINS_vBC: TFloatField;
    QrPQEIts_2_COFINS_pCOFINS: TFloatField;
    QrPQEIts_2_COFINS_qBCProd: TFloatField;
    QrPQEIts_2_COFINS_vAliqProd: TFloatField;
    QrPQEIts_2_COFINS_vCOFINS: TFloatField;
    QrPQEIts_2_COFINSST_vBC: TFloatField;
    QrPQEIts_2_COFINSST_pCOFINS: TFloatField;
    QrPQEIts_2_COFINSST_qBCProd: TFloatField;
    QrPQEIts_2_COFINSST_vAliqProd: TFloatField;
    QrPQEIts_2_COFINSST_vCOFINS: TFloatField;
    QrLocEFD: TmySQLQuery;
    QrLocEFDLinArq: TIntegerField;
    QrD100TP_CTE: TSmallintField;
    procedure BtSaida_0_0Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure EdAnoMesChange(Sender: TObject);
    procedure CkFiltra_0_0Click(Sender: TObject);
    procedure QrC100BeforeClose(DataSet: TDataSet);
    procedure QrC100AfterScroll(DataSet: TDataSet);
    procedure BtRastreia_0_0Click(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure QrPQEBeforeClose(DataSet: TDataSet);
    procedure QrPQEAfterOpen(DataSet: TDataSet);
    procedure BtExclui_0_0Click(Sender: TObject);
    procedure DBGrid3DblClick(Sender: TObject);
    procedure QrNFsMPsAfterOpen(DataSet: TDataSet);
    procedure BtEstatistica_0_1Click(Sender: TObject);
    procedure BtEstatistica_0_0Click(Sender: TObject);
    procedure QrC100AfterOpen(DataSet: TDataSet);
    procedure BtRastreia_0_1Click(Sender: TObject);
    procedure QrSumMPICalcFields(DataSet: TDataSet);
    procedure CkFiltra_0_1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure PMSeqAcoes_C500Popup(Sender: TObject);
    procedure C500_SeqAcoes_1Click(Sender: TObject);
    procedure C500_SeqAcoes_2Click(Sender: TObject);
    procedure QrC001AfterScroll(DataSet: TDataSet);
    procedure BtSeqAcoesC500Click(Sender: TObject);
    procedure QrC001BeforeClose(DataSet: TDataSet);
    procedure QrC500BeforeClose(DataSet: TDataSet);
    procedure D500_SeqAcoes_1Click(Sender: TObject);
    procedure D500_SeqAcoes_2Click(Sender: TObject);
    procedure BtSeqAcoesD500Click(Sender: TObject);
    procedure PMSeqAcoes_D500Popup(Sender: TObject);
    procedure QrD001AfterScroll(DataSet: TDataSet);
    procedure QrD001BeforeClose(DataSet: TDataSet);
    procedure D100_SeqAcoes_1Click(Sender: TObject);
    procedure D100_SeqAcoes_2Click(Sender: TObject);
    procedure BtSeqAcoesD100Click(Sender: TObject);
    procedure PMSeqAcoes_D100Popup(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtSeqAcoesC100Click(Sender: TObject);
    procedure C100_SeqAcoes_1Click(Sender: TObject);
    procedure C100_SeqAcoes_2Click(Sender: TObject);
    procedure C100_SeqAcoes_3Click(Sender: TObject);
    procedure C100_SeqAcoes_4Click(Sender: TObject);
    procedure DBG_C100DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure PMSeqAcoes_C100Popup(Sender: TObject);
  private
    { Private declarations }
    //FSPEDEFD_C100: String;
    FImporExpor_Int, FAnoMes_Int, FEmpresa_Int: Integer;
    FImporExpor_Txt, FAnoMes_Txt, FEmpresa_Txt: String;
    //
    procedure CriaNF_C100_de_EFD();
    procedure CriaNF_C100_de_PQE(Codigo: Integer);
    procedure CriaNF_C170_de_EFD(ImporExpor, C100: Integer);
    procedure CriaNF_C170_de_PQE(ImporExpor, C100, Codigo: Integer);
    procedure DefineVariaveis();
    procedure ReopenTabelas();
    procedure ReopenPQE();
    procedure ReopenMPInIts();
    procedure IncrementaStatus(Tabela, Campo: String; Status: Integer);
    procedure ReopenC170(LinArq: Integer);
    procedure DefineDatasMinEMax(const MesesAMenos, MesesAMais: Integer; var
              DataMin, DataMax: String);
{
    procedure IncluiStqMovItsA_C170(Empresa, GraGruX: Integer);
}
    function Localiza_EFD(): Boolean;
  public
    { Public declarations }
  end;

  var
  FmSPED_EFD_Compara: TFmSPED_EFD_Compara;

implementation

uses UnMyObjects, Module, UnGrade_Create, ModuleGeral, UnGrade_Tabs, UnSPED_Geral,
UMySQLModule, PQE, UnInternalConsts, MyDBCheck, ModuleNFe_0000, ModProd;

{$R *.DFM}

const
(*   ini 2022-03-29
 CO_Tabela_0150 = 'spedefd0150';
 CO_Tabela_C001 = 'spedefdc001';
 CO_Tabela_C100 = 'spedefdc100';
 CO_Tabela_C170 = 'spedefdc170';
 CO_Tabela_C500 = 'spedefdc500';
 CO_Tabela_C590 = 'spedefdc590';
 CO_Tabela_D001 = 'spedefdd001';
 CO_Tabela_D100 = 'spedefdd100';
 CO_Tabela_D190 = 'spedefdd190';
 CO_Tabela_D500 = 'spedefdd500';
 CO_Tabela_D590 = 'spedefdd590';
*)

 CO_Tabela_0150 = 'spedefdicmsipi0150';
 CO_Tabela_C001 = 'spedefdicmsipic001';
 CO_Tabela_C100 = 'spedefdicmsipic100';
 CO_Tabela_C170 = 'spedefdicmsipic170';
 CO_Tabela_C500 = 'spedefdicmsipic500';
 CO_Tabela_C590 = 'spedefdicmsipic590';
 CO_Tabela_D001 = 'spedefdicmsipid001';
 CO_Tabela_D100 = 'spedefdicmsipid100';
 CO_Tabela_D190 = 'spedefdicmsipid190';
 CO_Tabela_D500 = 'spedefdicmsipid500';
 CO_Tabela_D590 = 'spedefdicmsipid590';

//    fim 2022-03-29


procedure TFmSPED_EFD_Compara.BtEstatistica_0_0Click(Sender: TObject);
var
  Q0, Q1, Q2, Qn: Integer;
begin
  Q0 := 0;
  Q1 := 0;
  Q2 := 0;
  Qn := 0;
  QrC100.First;
  while not QrC100.Eof do
  begin
    Application.ProcessMessages;
    case QrPQE.RecordCount of
      0: Q0 := Q0 + 1;
      1: Q1 := Q1 + 1;
      2: Q2 := Q2 + 1;
      else Qn := Qn + 1;
    end;
    QrC100.Next;
  end;
  Geral.MensagemArray(
    MB_ICONINFORMATION, 'Mensagem', ['0: ', '1: ', '2: ', 'n: '], [
    FormatFloat('0', Q0), FormatFloat('0', Q1), FormatFloat('0', Q2),
    FormatFloat('0', Qn)], nil);
end;

procedure TFmSPED_EFD_Compara.BtEstatistica_0_1Click(Sender: TObject);
var
  Q0, Q1, Q2, Qn: Integer;
begin
  Q0 := 0;
  Q1 := 0;
  Q2 := 0;
  Qn := 0;
  QrC100.First;
  while not QrC100.Eof do
  begin
    Application.ProcessMessages;
    case QrNFsMPs.RecordCount of
      0: Q0 := Q0 + 1;
      1: Q1 := Q1 + 1;
      2: Q2 := Q2 + 1;
      else Qn := Qn + 1;
    end;
    QrC100.Next;
  end;
  Geral.MensagemArray(
    MB_ICONINFORMATION, 'Mensagem', ['0: ', '1: ', '2: ', 'n: '], [
    FormatFloat('0', Q0), FormatFloat('0', Q1), FormatFloat('0', Q2),
    FormatFloat('0', Qn)], nil);
end;

procedure TFmSPED_EFD_Compara.BtExclui_0_0Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o da entrada selecionada?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('DELETE FROM pqx WHERE Tipo=' + Geral.FF0(VAR_FATID_0010) + ' AND OrigemCodi=:P0;');
    DMod.QrUpd.SQL.Add('DELETE FROM pqeits WHERE Codigo=:P0;');
    DMod.QrUpd.SQL.Add('DELETE FROM pqe WHERE Codigo=:P0;');
    DMod.QrUpd.Params[0].AsInteger := QrPQECodigo.Value;
    DMod.QrUpd.ExecSQL;
    //
    ReopenPQE();
  end;
end;

procedure TFmSPED_EFD_Compara.BtRastreia_0_0Click(Sender: TObject);
const
  FatID = VAR_FATID_0251;
  StqCenCad = 1;
  Baixa = 1;
var
  Q0, Q1, Q2, Qn, QSim, QNao: Integer;
  // NFeCabA
  CodInfoDest, CodInfoEmit, ide_tpNF, ide_mod, Status, ide_serie, ide_nNF,
  ide_indPag, modFrete, EFD_INN_AnoMes, EFD_INN_Empresa, EFD_INN_LinArq, IDCtrl,
  FatNum, Empresa, Codigo: Integer;
  COD_SIT, NFG_Serie, Id, ide_dEmi, ide_dSaiEnt, DataFiscal, COD_MOD(*, DataE*): String;
  ICMSTot_vNF, ICMSTot_vDesc, VL_ABAT_NT, ICMSTot_vProd, ICMSTot_vFrete,
  ICMSTot_vSeg, ICMSTot_vOutro, ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,
  ICMSTot_vST, ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
  RetTrib_vRetPIS, RetTrib_vRetCOFINS: Double;
  // NFeItsI
  nItem, MeuID, Nivel1, GraGruX, prod_CFOP: Integer;
  prod_qCom, prod_vUnCom, prod_vProd, prod_qTrib, prod_vUnTrib: Double;
  // StqMovItsa
  DataHora: String;
  Tipo, OriCodi, OriCtrl, ParCodi: Integer;
  Qtde, CustoAll: Double;
  // PQX
  ParTipo, StqMovIts, OrigemCodi, OrigemCtrl: Integer;
begin
  Empresa := FEmpresa_Int;
  Q0 := 0;
  Q1 := 0;
  Q2 := 0;
  Qn := 0;
  QSim := 0;
  QNao := 0;
  QrC100.First;
  while not QrC100.Eof do
  begin
    Application.ProcessMessages;
    case QrPQE.RecordCount of
      0: Q0 := Q0 + 1;
      1: Q1 := Q1 + 1;
      2: Q2 := Q2 + 1;
      else Qn := Qn + 1;
    end;
    //
    //
    if QrPQE.RecordCount = 1 then
    begin
      //QrC100REG.Value;
      ide_tpNF := Geral.IMV(QrC100IND_OPER.Value);
      if QrC100IND_EMIT.Value = '1' then
      begin
        //QrC100COD_PART.Value;
        CodInfoDest := FEmpresa_Int;
        CodInfoEmit := QrC100Entidade.Value;
        COD_MOD := QrC100COD_MOD.Value;
        ide_mod := Geral.IMV(COD_MOD);
        COD_SIT := QrC100COD_SIT.Value;
        Status := SPED_Geral.Obtem_NFe_Status_de_SPED_COD_SIT(Geral.IMV(COD_SIT));
        NFG_Serie := QrC100SER.Value;
        ide_serie := Geral.IMV(NFG_Serie);
        ide_nNF := QrC100NUM_DOC.Value;
        Id := QrC100CHV_NFE.Value;
        DataFiscal := Geral.FDT(QrC100DT_DOC.Value, 1);
        ide_dEmi := Geral.FDT(QrC100DT_DOC.Value - 2, 1);
        ide_dSaiEnt := ide_dEmi;
        ICMSTot_vNF := QrC100VL_DOC.Value;
        ide_indPag := SPED_Geral.Obtem_NFe_indPag_de_SPED_IND_PGTO(QrC100IND_PGTO.Value);
        ICMSTot_vDesc := QrC100VL_DESC.Value;
        VL_ABAT_NT := QrC100VL_ABAT_NT.Value;
        ICMSTot_vProd := QrC100VL_MERC.Value;
        modFrete := SPED_Geral.Obtem_NFe_modFrete_de_SPED_IND_FRT(QrC100IND_FRT.Value);
        ICMSTot_vFrete := QrC100VL_FRT.Value;
        ICMSTot_vSeg := QrC100VL_SEG.Value;
        ICMSTot_vOutro := QrC100VL_OUT_DA.Value;
        ICMSTot_vBC := QrC100VL_BC_ICMS.Value;
        ICMSTot_vICMS := QrC100VL_ICMS.Value;
        ICMSTot_vBCST := QrC100VL_BC_ICMS_ST.Value;
        ICMSTot_vST := QrC100VL_ICMS_ST.Value;
        ICMSTot_vIPI := QrC100VL_IPI.Value;
        ICMSTot_vPIS := QrC100VL_PIS.Value;
        ICMSTot_vCOFINS := QrC100VL_COFINS.Value;
        RetTrib_vRetPIS := QrC100VL_PIS_ST.Value;
        RetTrib_vRetCOFINS := QrC100VL_COFINS_ST.Value;
        // := QrC100ImporExpor.Value;
        EFD_INN_AnoMes := QrC100AnoMes.Value;
        EFD_INN_Empresa := QrC100Empresa.Value;
        EFD_INN_LinArq := QrC100LinArq.Value;
        //
        IDCtrl := DModG.BuscaProximoInteiro('nfecaba', 'IDCtrl', '', 0);
        FatNum := UMyMod.BuscaEmLivreY_Def('pqe', 'Codigo', stIns, 0, nil, True);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False, [
        'IDCtrl', (*'LoteEnv', 'versao',*)
        'Id', (*'ide_cUF', 'ide_cNF',
        'ide_natOp',*) 'ide_indPag', 'ide_mod',
        'ide_serie', 'ide_nNF', 'ide_dEmi',
        'ide_dSaiEnt', 'ide_tpNF', (*'ide_cMunFG',
        'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
        'ide_tpAmb', 'ide_finNFe', 'ide_procEmi',
        'ide_verProc', 'emit_CNPJ', 'emit_CPF',
        'emit_xNome', 'emit_xFant', 'emit_xLgr',
        'emit_nro', 'emit_xCpl', 'emit_xBairro',
        'emit_cMun', 'emit_xMun', 'emit_UF',
        'emit_CEP', 'emit_cPais', 'emit_xPais',
        'emit_fone', 'emit_IE', 'emit_IEST',
        'emit_IM', 'emit_CNAE', 'dest_CNPJ',
        'dest_CPF', 'dest_xNome', 'dest_xLgr',
        'dest_nro', 'dest_xCpl', 'dest_xBairro',
        'dest_cMun', 'dest_xMun', 'dest_UF',
        'dest_CEP', 'dest_cPais', 'dest_xPais',
        'dest_fone', 'dest_IE', 'dest_ISUF',*)
        'ICMSTot_vBC', 'ICMSTot_vICMS', 'ICMSTot_vBCST',
        'ICMSTot_vST', 'ICMSTot_vProd', 'ICMSTot_vFrete',
        'ICMSTot_vSeg', 'ICMSTot_vDesc', (*'ICMSTot_vII',*)
        'ICMSTot_vIPI', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
        'ICMSTot_vOutro', 'ICMSTot_vNF', (*'ISSQNtot_vServ',
        'ISSQNtot_vBC', 'ISSQNtot_vISS', 'ISSQNtot_vPIS',
        'ISSQNtot_vCOFINS',*) 'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS',
        (*'RetTrib_vRetCSLL', 'RetTrib_vBCIRRF', 'RetTrib_vIRRF',
        'RetTrib_vBCRetPrev', 'RetTrib_vRetPrev',*) 'ModFrete',
        (*'Transporta_CNPJ', 'Transporta_CPF', 'Transporta_XNome',
        'Transporta_IE', 'Transporta_XEnder', 'Transporta_XMun',
        'Transporta_UF', 'RetTransp_vServ', 'RetTransp_vBCRet',
        'RetTransp_PICMSRet', 'RetTransp_vICMSRet', 'RetTransp_CFOP',
        'RetTransp_CMunFG', 'VeicTransp_Placa', 'VeicTransp_UF',
        'VeicTransp_RNTC', 'Cobr_Fat_nFat', 'Cobr_Fat_vOrig',
        'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq', 'InfAdic_InfAdFisco',
        'InfAdic_InfCpl', 'Exporta_UFEmbarq', 'Exporta_XLocEmbarq',
        'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',*)
        'Status', (*'infProt_Id', 'infProt_tpAmb',
        'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
        'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
        'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
        'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
        'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
        'infCanc_xJust', '_Ativo_', 'FisRegCad',
        'CartEmiss', 'TabelaPrc', 'CondicaoPg',
        'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',
        'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
        'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
        'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
        'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
        'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
        'DataFiscal', (*'protNFe_versao', 'retCancNFe_versao',
        'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta', 'SINTEGRA_ExpNat',
        'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta', 'SINTEGRA_ExpConhNum',
        'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip', 'SINTEGRA_ExpPais',
        'SINTEGRA_ExpAverDta',*) 'CodInfoEmit', 'CodInfoDest',
        (*'CriAForca', 'ide_hSaiEnt', 'ide_dhCont',
        'ide_xJust', 'emit_CRT', 'dest_email',
        'Vagao', 'Balsa', 'CodInfoTrsp',
        'OrdemServ', 'Situacao', 'Antigo',*)
        'NFG_Serie', (*'NF_ICMSAlq', 'NF_CFOP',
        'Importado', 'NFG_SubSerie', 'NFG_ValIsen',
        'NFG_NaoTrib', 'NFG_Outros',*) 'COD_MOD',
        'COD_SIT', 'VL_ABAT_NT', 'EFD_INN_AnoMes',
        'EFD_INN_Empresa', 'EFD_INN_LinArq'], [
        'FatID', 'FatNum', 'Empresa'], [
        IDCtrl, (*LoteEnv, versao,*)
        Id, (*ide_cUF, ide_cNF,
        ide_natOp,*) ide_indPag, ide_mod,
        ide_serie, ide_nNF, ide_dEmi,
        ide_dSaiEnt, ide_tpNF, (*ide_cMunFG,
        ide_tpImp, ide_tpEmis, ide_cDV,
        ide_tpAmb, ide_finNFe, ide_procEmi,
        ide_verProc, emit_CNPJ, emit_CPF,
        emit_xNome, emit_xFant, emit_xLgr,
        emit_nro, emit_xCpl, emit_xBairro,
        emit_cMun, emit_xMun, emit_UF,
        emit_CEP, emit_cPais, emit_xPais,
        emit_fone, emit_IE, emit_IEST,
        emit_IM, emit_CNAE, dest_CNPJ,
        dest_CPF, dest_xNome, dest_xLgr,
        dest_nro, dest_xCpl, dest_xBairro,
        dest_cMun, dest_xMun, dest_UF,
        dest_CEP, dest_cPais, dest_xPais,
        dest_fone, dest_IE, dest_ISUF,*)
        ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,
        ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete,
        ICMSTot_vSeg, ICMSTot_vDesc, (*ICMSTot_vII,*)
        ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
        ICMSTot_vOutro, ICMSTot_vNF, (*ISSQNtot_vServ,
        ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,
        ISSQNtot_vCOFINS,*) RetTrib_vRetPIS, RetTrib_vRetCOFINS,
        (*RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,
        RetTrib_vBCRetPrev, RetTrib_vRetPrev,*) ModFrete,
        (*Transporta_CNPJ, Transporta_CPF, Transporta_XNome,
        Transporta_IE, Transporta_XEnder, Transporta_XMun,
        Transporta_UF, RetTransp_vServ, RetTransp_vBCRet,
        RetTransp_PICMSRet, RetTransp_vICMSRet, RetTransp_CFOP,
        RetTransp_CMunFG, VeicTransp_Placa, VeicTransp_UF,
        VeicTransp_RNTC, Cobr_Fat_nFat, Cobr_Fat_vOrig,
        Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,
        InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq,
        Compra_XNEmp, Compra_XPed, Compra_XCont,*)
        Status, (*infProt_Id, infProt_tpAmb,
        infProt_verAplic, infProt_dhRecbto, infProt_nProt,
        infProt_digVal, infProt_cStat, infProt_xMotivo,
        infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
        infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,
        infCanc_cStat, infCanc_xMotivo, infCanc_cJust,
        infCanc_xJust, _Ativo_, FisRegCad,
        CartEmiss, TabelaPrc, CondicaoPg,
        FreteExtra, SegurExtra, ICMSRec_pRedBC,
        ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
        IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
        IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
        PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
        COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
        DataFiscal, (*protNFe_versao, retCancNFe_versao,
        SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_ExpNat,
        SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum,
        SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais,
        SINTEGRA_ExpAverDta,*) CodInfoEmit, CodInfoDest,
        (*CriAForca, ide_hSaiEnt, ide_dhCont,
        ide_xJust, emit_CRT, dest_email,
        Vagao, Balsa, CodInfoTrsp,
        OrdemServ, Situacao, Antigo,*)
        NFG_Serie, (*NF_ICMSAlq, NF_CFOP,
        Importado, NFG_SubSerie, NFG_ValIsen,
        NFG_NaoTrib, NFG_Outros,*) COD_MOD,
        COD_SIT, VL_ABAT_NT, EFD_INN_AnoMes,
        EFD_INN_Empresa, EFD_INN_LinArq], [
        FatID, FatNum, Empresa], True) then
        begin
          //DataE := DataFiscal;
          Codigo := QrPQECodigo.Value;
          ///
          QrPQEIts.Close;
          QrPQEIts.Params[0].AsInteger := Codigo;
          QrPQEIts.Open;
          ///
          QrPQEIts.First;
          while not QrPQEIts.Eof do
          begin
(*
    QrPQEItsVolumes: TIntegerField;
    QrPQEItsPesoVB: TFloatField;
    : TFloatField;
    : TFloatField;
    QrPQEItsIPI: TFloatField;
    QrPQEItsRIPI: TFloatField;
    QrPQEItsCFin: TFloatField;
    QrPQEItsICMS: TFloatField;
    QrPQEItsRICMS: TFloatField;
    QrPQEItsTotalCusto: TFloatField;
    QrPQEItsTotalPeso: TFloatField;
*)

            prod_CFOP := Geral.IMV(Geral.SoNumero_TT(QrPQEItsprod_CFOP.Value));
            prod_qCom := QrPQEItsPesoVL.Value;
            prod_vUnCom := QrPQEItsValorItem.Value;
            prod_vProd := QrPQEItsTotalCusto.Value;
            prod_qTrib := prod_qCom;
            prod_vUnTrib := prod_vUnCom;
            MeuID := QrPQEItsControle.Value;
            Nivel1 := QrPQEItsInsumo.Value;
            GraGruX := Dmod.ObtemGraGruXDeGraGru1(Nivel1);
            nItem := QrPQEIts.RecNo;

if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
(*'prod_cProd', 'prod_cEAN', 'prod_xProd',
'prod_NCM', 'prod_EXTIPI', 'prod_genero',*)
'prod_CFOP', (*'prod_uCom',*) 'prod_qCom',
'prod_vUnCom', 'prod_vProd', (*'prod_cEANTrib',
'prod_uTrib',*) 'prod_qTrib', 'prod_vUnTrib',
(*'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
'Tem_IPI', '_Ativo_', 'InfAdCuztm',
'EhServico', 'ICMSRec_pRedBC', 'ICMSRec_vBC',
'ICMSRec_pAliq', 'ICMSRec_vICMS', 'IPIRec_pRedBC',
'IPIRec_vBC', 'IPIRec_pAliq', 'IPIRec_vIPI',
'PISRec_pRedBC', 'PISRec_vBC', 'PISRec_pAliq',
'PISRec_vPIS', 'COFINSRec_pRedBC', 'COFINSRec_vBC',
'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*) 'MeuID',
'Nivel1', (*'prod_vOutro', 'prod_indTot',
'prod_xPed', 'prod_nItemPed',*) 'GraGruX'(*,
'UnidMedCom', 'UnidMedTrib'*)], [
'FatID', 'FatNum', 'Empresa', 'nItem'], [
(*prod_cProd, prod_cEAN, prod_xProd,
prod_NCM, prod_EXTIPI, prod_genero,*)
prod_CFOP, (*prod_uCom,*) prod_qCom,
prod_vUnCom, prod_vProd, (*prod_cEANTrib,
prod_uTrib,*) prod_qTrib, prod_vUnTrib,
(*prod_vFrete, prod_vSeg, prod_vDesc,
Tem_IPI, _Ativo_, InfAdCuztm,
EhServico, ICMSRec_pRedBC, ICMSRec_vBC,
ICMSRec_pAliq, ICMSRec_vICMS, IPIRec_pRedBC,
IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI,
PISRec_pRedBC, PISRec_vBC, PISRec_pAliq,
PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
COFINSRec_pAliq, COFINSRec_vCOFINS,*) MeuID,
Nivel1, (*prod_vOutro, prod_indTot,
prod_xPed, prod_nItemPed,*) GraGruX(*,
UnidMedCom, UnidMedTrib*)], [
FatID, FatNum, Empresa, nItem], True) then

(*
N�o fiz ICMS nem IPI pois tem muito poucos
*)

// Estoque >
            DataHora := DataFiscal;
            Tipo := FatID;
            OriCodi := FatNum;
            OriCtrl := QrPQEItsControle.Value;
            Qtde := QrPQEItsPesoVL.Value;
            CustoAll := QrPQEItsTotalCusto.Value;
            IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
            // N�o colocar como constante para no futuro n�o confundir
            ParTipo := GRADE_TABS_PARTIPO_0001; // Identifica o par no PQX e no StqMovItsA(B)
            ParCodi := IDCtrl;

            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
            'DataHora', 'Tipo', 'OriCodi',
            'OriCtrl', (*'OriCnta', 'OriPart',*)
            'Empresa', 'StqCenCad', 'GraGruX',
            'Qtde', (*'Pecas', 'Peso',
            'AreaM2', 'AreaP2', 'FatorClas',
            'QuemUsou', 'Retorno',*) 'ParTipo',
            'ParCodi', (*'DebCtrl', 'SMIMultIns',*)
            'CustoAll', (*'ValorAll', 'GrupoBal',*)
            'Baixa'(*, 'AntQtde'*)], [
            'IDCtrl'], [
            DataHora, Tipo, OriCodi,
            OriCtrl, (*OriCnta, OriPart,*)
            Empresa, StqCenCad, GraGruX,
            Qtde, (*Pecas, Peso,
            AreaM2, AreaP2, FatorClas,
            QuemUsou, Retorno,*) ParTipo,
            ParCodi, (*DebCtrl, SMIMultIns,*)
            CustoAll, (*ValorAll, GrupoBal,*)
            Baixa(*, AntQtde*)], [
            IDCtrl], False) then
            begin
              StqMovIts := IDCtrl;
              OrigemCodi := OriCodi;
              OrigemCtrl := OriCtrl;
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pqx', False, [
              'StqMovIts'], [
              'OrigemCodi', 'OrigemCtrl', 'Tipo'], [
              StqMovIts], [
              OrigemCodi, OrigemCtrl, Tipo], False);
            end;
            //
            QrPQEIts.Next;
          end;
          ///
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pqe', False, [
          (*'Data', 'IQ', 'CI',
          'Transportadora', 'NF', 'Frete',
          'PesoB', 'PesoL', 'ValorNF',
          'RICMS', 'RICMSF', 'Conhecimento',
          'Pedido', 'DataE', (*'Juros',
          'ICMS', 'Cancelado', 'TipoNF',
          'refNFe', 'modNF', 'Serie',
          'ValProd', 'Volumes', 'IPI',
          'PIS', 'COFINS', 'Seguro',
          'Desconto', 'Outros', 'DataS',*)
          'EFD_INN_AnoMes', 'EFD_INN_Empresa', 'EFD_INN_LinArq'], [
          'Codigo'], [
          (*Data, IQ, CI,
          Transportadora, NF, Frete,
          PesoB, PesoL, ValorNF,
          RICMS, RICMSF, Conhecimento,
          Pedido, DataE, Juros,
          ICMS, Cancelado, TipoNF,
          refNFe, modNF, Serie,
          ValProd, Volumes, IPI,
          PIS, COFINS, Seguro,
          Desconto, Outros, DataS,*)
          EFD_INN_AnoMes, EFD_INN_Empresa, EFD_INN_LinArq], [
          Codigo], True) then
          begin
          end;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_Tabela_C100, False, [
          'ParTipo', 'ParCodi'], [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
          GRADE_TABS_PARTIPO_0005, QrPQECodigo.Value], [
          FImporExpor_Int, FAnoMes_Int, FEmpresa_Int, QrC100LinArq.Value], True);
        end;
      end;
      QSim := QSim + 1;
    end else
      QNao := QNao + 1;
    //
    QrC100.Next;
  end;
  Geral.MensagemArray(
    MB_ICONINFORMATION, 'Mensagem', ['0: ', '1: ', '2: ', 'n: ', '',
    'Sincronizados', 'N�o sincron.:'], [
    FormatFloat('0', Q0), FormatFloat('0', Q1), FormatFloat('0', Q2),
    FormatFloat('0', Qn), '', FormatFloat('0', QSim), FormatFloat('0', QNao)],
    nil);
  ReopenTabelas();
end;

procedure TFmSPED_EFD_Compara.BtRastreia_0_1Click(Sender: TObject);
const
  FatID = VAR_FATID_0213;
  StqCenCad = 1;
  Baixa = 1;
var
  Q0, Q1, Q2, Qn, QSim, QNao, Empresa: Integer;
  // NFeCabA
  CodInfoDest, CodInfoEmit, ide_tpNF, ide_mod, Status, ide_serie, ide_nNF,
  ide_indPag, modFrete, EFD_INN_AnoMes, EFD_INN_Empresa, EFD_INN_LinArq, IDCtrl,
  FatNum: Integer;
  COD_SIT, NFG_Serie, Id, ide_dEmi, ide_dSaiEnt, DataFiscal, COD_MOD(*, DataE*): String;
  ICMSTot_vNF, ICMSTot_vDesc, VL_ABAT_NT, ICMSTot_vProd, ICMSTot_vFrete,
  ICMSTot_vSeg, ICMSTot_vOutro, ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,
  ICMSTot_vST, ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
  RetTrib_vRetPIS, RetTrib_vRetCOFINS: Double;
  // NFeItsI
  nItem, MeuID, Nivel1, GraGruX, prod_CFOP: Integer;
  prod_qCom, prod_vUnCom, prod_vProd, prod_qTrib, prod_vUnTrib: Double;
  prod_uCom, prod_uTrib: String;
  // StqMovItsa
  DataHora: String;
  Tipo, OriCodi, OriCtrl, ParCodi: Integer;
  Qtde, CustoAll: Double;
  // MPIIts
  NF_Modelo, NF_Serie: String;
  EmitNFAvul, ParTipo, StqMovIts: Integer;
begin
  Empresa := FEmpresa_Int;
  Q0 := 0;
  Q1 := 0;
  Q2 := 0;
  Qn := 0;
  QSim := 0;
  QNao := 0;
  QrC100.First;
  while not QrC100.Eof do
  begin
    Application.ProcessMessages;
    case QrNFsMPs.RecordCount of
      0: Q0 := Q0 + 1;
      1: Q1 := Q1 + 1;
      2: Q2 := Q2 + 1;
      else Qn := Qn + 1;
    end;
    //
    //
    if (QrC100NUM_DOC.Value = QrNFsMPsNF.Value) and
    (Trunc(QrC100VL_DOC.Value * 100) = Trunc(QrSumMPICMPValor.Value * 100)) then
    begin
      //QrC100REG.Value;
      ide_tpNF := Geral.IMV(QrC100IND_OPER.Value);
      if QrC100IND_EMIT.Value = '1' then
      begin
        //QrC100COD_PART.Value;
        CodInfoDest := FEmpresa_Int;
        CodInfoEmit := QrC100Entidade.Value;
        COD_MOD := QrC100COD_MOD.Value;
        ide_mod := Geral.IMV(COD_MOD);
        COD_SIT := QrC100COD_SIT.Value;
        Status := SPED_Geral.Obtem_NFe_Status_de_SPED_COD_SIT(Geral.IMV(COD_SIT));
        NFG_Serie := QrC100SER.Value;
        ide_serie := Geral.IMV(NFG_Serie);
        ide_nNF := QrC100NUM_DOC.Value;
        Id := QrC100CHV_NFE.Value;
        DataFiscal := Geral.FDT(QrC100DT_DOC.Value, 1);
        ide_dEmi := Geral.FDT(QrC100DT_DOC.Value - 2, 1);
        ide_dSaiEnt := ide_dEmi;
        ICMSTot_vNF := QrC100VL_DOC.Value;
        ide_indPag := SPED_Geral.Obtem_NFe_indPag_de_SPED_IND_PGTO(QrC100IND_PGTO.Value);
        ICMSTot_vDesc := QrC100VL_DESC.Value;
        VL_ABAT_NT := QrC100VL_ABAT_NT.Value;
        ICMSTot_vProd := QrC100VL_MERC.Value;
        modFrete := SPED_Geral.Obtem_NFe_modFrete_de_SPED_IND_FRT(QrC100IND_FRT.Value);
        ICMSTot_vFrete := QrC100VL_FRT.Value;
        ICMSTot_vSeg := QrC100VL_SEG.Value;
        ICMSTot_vOutro := QrC100VL_OUT_DA.Value;
        ICMSTot_vBC := QrC100VL_BC_ICMS.Value;
        ICMSTot_vICMS := QrC100VL_ICMS.Value;
        ICMSTot_vBCST := QrC100VL_BC_ICMS_ST.Value;
        ICMSTot_vST := QrC100VL_ICMS_ST.Value;
        ICMSTot_vIPI := QrC100VL_IPI.Value;
        ICMSTot_vPIS := QrC100VL_PIS.Value;
        ICMSTot_vCOFINS := QrC100VL_COFINS.Value;
        RetTrib_vRetPIS := QrC100VL_PIS_ST.Value;
        RetTrib_vRetCOFINS := QrC100VL_COFINS_ST.Value;
        // := QrC100ImporExpor.Value;
        EFD_INN_AnoMes := QrC100AnoMes.Value;
        EFD_INN_Empresa := QrC100Empresa.Value;
        EFD_INN_LinArq := QrC100LinArq.Value;
        //
        IDCtrl := DModG.BuscaProximoInteiro('nfecaba', 'IDCtrl', '', 0);

        //FatNum := UMyMod.BuscaEmLivreY_Def('pqe', 'Codigo', stIns, 0, nil, True);
        FatNum := DModG.BuscaProximoInteiro('nfecaba', 'FatNum', 'FatID', FatID);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False, [
        'IDCtrl', (*'LoteEnv', 'versao',*)
        'Id', (*'ide_cUF', 'ide_cNF',
        'ide_natOp',*) 'ide_indPag', 'ide_mod',
        'ide_serie', 'ide_nNF', 'ide_dEmi',
        'ide_dSaiEnt', 'ide_tpNF', (*'ide_cMunFG',
        'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
        'ide_tpAmb', 'ide_finNFe', 'ide_procEmi',
        'ide_verProc', 'emit_CNPJ', 'emit_CPF',
        'emit_xNome', 'emit_xFant', 'emit_xLgr',
        'emit_nro', 'emit_xCpl', 'emit_xBairro',
        'emit_cMun', 'emit_xMun', 'emit_UF',
        'emit_CEP', 'emit_cPais', 'emit_xPais',
        'emit_fone', 'emit_IE', 'emit_IEST',
        'emit_IM', 'emit_CNAE', 'dest_CNPJ',
        'dest_CPF', 'dest_xNome', 'dest_xLgr',
        'dest_nro', 'dest_xCpl', 'dest_xBairro',
        'dest_cMun', 'dest_xMun', 'dest_UF',
        'dest_CEP', 'dest_cPais', 'dest_xPais',
        'dest_fone', 'dest_IE', 'dest_ISUF',*)
        'ICMSTot_vBC', 'ICMSTot_vICMS', 'ICMSTot_vBCST',
        'ICMSTot_vST', 'ICMSTot_vProd', 'ICMSTot_vFrete',
        'ICMSTot_vSeg', 'ICMSTot_vDesc', (*'ICMSTot_vII',*)
        'ICMSTot_vIPI', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
        'ICMSTot_vOutro', 'ICMSTot_vNF', (*'ISSQNtot_vServ',
        'ISSQNtot_vBC', 'ISSQNtot_vISS', 'ISSQNtot_vPIS',
        'ISSQNtot_vCOFINS',*) 'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS',
        (*'RetTrib_vRetCSLL', 'RetTrib_vBCIRRF', 'RetTrib_vIRRF',
        'RetTrib_vBCRetPrev', 'RetTrib_vRetPrev',*) 'ModFrete',
        (*'Transporta_CNPJ', 'Transporta_CPF', 'Transporta_XNome',
        'Transporta_IE', 'Transporta_XEnder', 'Transporta_XMun',
        'Transporta_UF', 'RetTransp_vServ', 'RetTransp_vBCRet',
        'RetTransp_PICMSRet', 'RetTransp_vICMSRet', 'RetTransp_CFOP',
        'RetTransp_CMunFG', 'VeicTransp_Placa', 'VeicTransp_UF',
        'VeicTransp_RNTC', 'Cobr_Fat_nFat', 'Cobr_Fat_vOrig',
        'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq', 'InfAdic_InfAdFisco',
        'InfAdic_InfCpl', 'Exporta_UFEmbarq', 'Exporta_XLocEmbarq',
        'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',*)
        'Status', (*'infProt_Id', 'infProt_tpAmb',
        'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
        'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
        'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
        'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
        'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
        'infCanc_xJust', '_Ativo_', 'FisRegCad',
        'CartEmiss', 'TabelaPrc', 'CondicaoPg',
        'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',
        'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
        'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
        'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
        'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
        'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
        'DataFiscal', (*'protNFe_versao', 'retCancNFe_versao',
        'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta', 'SINTEGRA_ExpNat',
        'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta', 'SINTEGRA_ExpConhNum',
        'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip', 'SINTEGRA_ExpPais',
        'SINTEGRA_ExpAverDta',*) 'CodInfoEmit', 'CodInfoDest',
        (*'CriAForca', 'ide_hSaiEnt', 'ide_dhCont',
        'ide_xJust', 'emit_CRT', 'dest_email',
        'Vagao', 'Balsa', 'CodInfoTrsp',
        'OrdemServ', 'Situacao', 'Antigo',*)
        'NFG_Serie', (*'NF_ICMSAlq', 'NF_CFOP',
        'Importado', 'NFG_SubSerie', 'NFG_ValIsen',
        'NFG_NaoTrib', 'NFG_Outros',*) 'COD_MOD',
        'COD_SIT', 'VL_ABAT_NT', 'EFD_INN_AnoMes',
        'EFD_INN_Empresa', 'EFD_INN_LinArq'], [
        'FatID', 'FatNum', 'Empresa'], [
        IDCtrl, (*LoteEnv, versao,*)
        Id, (*ide_cUF, ide_cNF,
        ide_natOp,*) ide_indPag, ide_mod,
        ide_serie, ide_nNF, ide_dEmi,
        ide_dSaiEnt, ide_tpNF, (*ide_cMunFG,
        ide_tpImp, ide_tpEmis, ide_cDV,
        ide_tpAmb, ide_finNFe, ide_procEmi,
        ide_verProc, emit_CNPJ, emit_CPF,
        emit_xNome, emit_xFant, emit_xLgr,
        emit_nro, emit_xCpl, emit_xBairro,
        emit_cMun, emit_xMun, emit_UF,
        emit_CEP, emit_cPais, emit_xPais,
        emit_fone, emit_IE, emit_IEST,
        emit_IM, emit_CNAE, dest_CNPJ,
        dest_CPF, dest_xNome, dest_xLgr,
        dest_nro, dest_xCpl, dest_xBairro,
        dest_cMun, dest_xMun, dest_UF,
        dest_CEP, dest_cPais, dest_xPais,
        dest_fone, dest_IE, dest_ISUF,*)
        ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,
        ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete,
        ICMSTot_vSeg, ICMSTot_vDesc, (*ICMSTot_vII,*)
        ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
        ICMSTot_vOutro, ICMSTot_vNF, (*ISSQNtot_vServ,
        ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,
        ISSQNtot_vCOFINS,*) RetTrib_vRetPIS, RetTrib_vRetCOFINS,
        (*RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,
        RetTrib_vBCRetPrev, RetTrib_vRetPrev,*) ModFrete,
        (*Transporta_CNPJ, Transporta_CPF, Transporta_XNome,
        Transporta_IE, Transporta_XEnder, Transporta_XMun,
        Transporta_UF, RetTransp_vServ, RetTransp_vBCRet,
        RetTransp_PICMSRet, RetTransp_vICMSRet, RetTransp_CFOP,
        RetTransp_CMunFG, VeicTransp_Placa, VeicTransp_UF,
        VeicTransp_RNTC, Cobr_Fat_nFat, Cobr_Fat_vOrig,
        Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,
        InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq,
        Compra_XNEmp, Compra_XPed, Compra_XCont,*)
        Status, (*infProt_Id, infProt_tpAmb,
        infProt_verAplic, infProt_dhRecbto, infProt_nProt,
        infProt_digVal, infProt_cStat, infProt_xMotivo,
        infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
        infCanc_dhRecbto, infCanc_nProt, infCanc_digVal,
        infCanc_cStat, infCanc_xMotivo, infCanc_cJust,
        infCanc_xJust, _Ativo_, FisRegCad,
        CartEmiss, TabelaPrc, CondicaoPg,
        FreteExtra, SegurExtra, ICMSRec_pRedBC,
        ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
        IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
        IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
        PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
        COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
        DataFiscal, (*protNFe_versao, retCancNFe_versao,
        SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_ExpNat,
        SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum,
        SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais,
        SINTEGRA_ExpAverDta,*) CodInfoEmit, CodInfoDest,
        (*CriAForca, ide_hSaiEnt, ide_dhCont,
        ide_xJust, emit_CRT, dest_email,
        Vagao, Balsa, CodInfoTrsp,
        OrdemServ, Situacao, Antigo,*)
        NFG_Serie, (*NF_ICMSAlq, NF_CFOP,
        Importado, NFG_SubSerie, NFG_ValIsen,
        NFG_NaoTrib, NFG_Outros,*) COD_MOD,
        COD_SIT, VL_ABAT_NT, EFD_INN_AnoMes,
        EFD_INN_Empresa, EFD_INN_LinArq], [
        FatID, FatNum, Empresa], True) then
        begin
          QrMPInIts.First;
          //
          //  Fazer apenas um registro com o total dos couros!
          //
          nItem := QrMPInIts.RecNo;
          MeuID := QrMPInItsConta.Value;
          GraGruX      := Dmod.ObtemGraGruXDeCouro(QrMPInItsTipificacao.Value, QrMPInItsAnimal.Value);
          Nivel1       := GraGruX;(*GraGruX > � o mesmo que o Nivel1*)
          prod_CFOP    := Geral.IMV(Geral.SoNumero_TT(QrMPInItsCFOP.Value));
          prod_uCom    := 'PC';
          prod_uTrib   := 'KG';
          prod_qCom    := QrSumMPIPecasNF.Value;
          prod_qTrib   := QrSumMPIPNF.Value;
          prod_vUnCom  := QrSumMPIPRECO_PC.Value;
          prod_vUnTrib := QrSumMPIPRECO_KG.Value;
          prod_vProd   := QrSumMPICMPValor.Value;

if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
(*'prod_cProd', 'prod_cEAN', 'prod_xProd',
'prod_NCM', 'prod_EXTIPI', 'prod_genero',*)
'prod_CFOP', 'prod_uCom', 'prod_qCom',
'prod_vUnCom', 'prod_vProd', (*'prod_cEANTrib',*)
'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
(*'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
'Tem_IPI', '_Ativo_', 'InfAdCuztm',
'EhServico', 'ICMSRec_pRedBC', 'ICMSRec_vBC',
'ICMSRec_pAliq', 'ICMSRec_vICMS', 'IPIRec_pRedBC',
'IPIRec_vBC', 'IPIRec_pAliq', 'IPIRec_vIPI',
'PISRec_pRedBC', 'PISRec_vBC', 'PISRec_pAliq',
'PISRec_vPIS', 'COFINSRec_pRedBC', 'COFINSRec_vBC',
'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*) 'MeuID',
'Nivel1', (*'prod_vOutro', 'prod_indTot',
'prod_xPed', 'prod_nItemPed',*) 'GraGruX'(*,
'UnidMedCom', 'UnidMedTrib'*)], [
'FatID', 'FatNum', 'Empresa', 'nItem'], [
(*prod_cProd, prod_cEAN, prod_xProd,
prod_NCM, prod_EXTIPI, prod_genero,*)
prod_CFOP, prod_uCom, prod_qCom,
prod_vUnCom, prod_vProd, (*prod_cEANTrib,*)
prod_uTrib, prod_qTrib, prod_vUnTrib,
(*prod_vFrete, prod_vSeg, prod_vDesc,
Tem_IPI, _Ativo_, InfAdCuztm,
EhServico, ICMSRec_pRedBC, ICMSRec_vBC,
ICMSRec_pAliq, ICMSRec_vICMS, IPIRec_pRedBC,
IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI,
PISRec_pRedBC, PISRec_vBC, PISRec_pAliq,
PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
COFINSRec_pAliq, COFINSRec_vCOFINS,*) MeuID,
Nivel1, (*prod_vOutro, prod_indTot,
prod_xPed, prod_nItemPed,*) GraGruX(*,
UnidMedCom, UnidMedTrib*)], [
FatID, FatNum, Empresa, nItem], True) then

(*
N�o fiz ICMS nem IPI pois tem muito poucos
*)

          while not QrMPInIts.Eof do
          begin
// Estoque >
            DataHora := DataFiscal;
            Tipo := FatID;
            OriCodi := FatNum;
            OriCtrl := QrMPInItsConta.Value;
            Qtde := QrMPInItsPNF.Value;
            CustoAll := QrMPInItsCMPValor.Value;
            IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
            // N�o colocar como constante para no futuro n�o confundir
            ParTipo := GRADE_TABS_PARTIPO_0002; // Identifica o par no MPIn e no StqMovItsA(B)
            ParCodi := IDCtrl;

            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
            'DataHora', 'Tipo', 'OriCodi',
            'OriCtrl', (*'OriCnta', 'OriPart',*)
            'Empresa', 'StqCenCad', 'GraGruX',
            'Qtde', (*'Pecas', 'Peso',
            'AreaM2', 'AreaP2', 'FatorClas',
            'QuemUsou', 'Retorno',*) 'ParTipo',
            'ParCodi', (*'DebCtrl', 'SMIMultIns',*)
            'CustoAll', (*'ValorAll', 'GrupoBal',*)
            'Baixa'(*, 'AntQtde'*)], [
            'IDCtrl'], [
            DataHora, Tipo, OriCodi,
            OriCtrl, (*OriCnta, OriPart,*)
            Empresa, StqCenCad, GraGruX,
            Qtde, (*Pecas, Peso,
            AreaM2, AreaP2, FatorClas,
            QuemUsou, Retorno,*) ParTipo,
            ParCodi, (*DebCtrl, SMIMultIns,*)
            CustoAll, (*ValorAll, GrupoBal,*)
            Baixa(*, AntQtde*)], [
            IDCtrl], False) then
            begin
              EmitNFAvul := QrC100Entidade.Value;
              NF_Modelo := QrC100COD_MOD.Value;
              NF_Serie := QrC100SER.Value;
              StqMovIts := IDCtrl;
              //
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mpinits', False, [
              'EFD_INN_AnoMes', 'EFD_INN_Empresa', 'EFD_INN_LinArq',
              'NF_modelo', 'NF_Serie', 'EmitNFAvul',
              'StqMovIts'], [
              'Conta'], [
              EFD_INN_AnoMes, EFD_INN_Empresa, EFD_INN_LinArq,
              NF_Modelo, NF_Serie, EmitNFAvul,
              StqMovIts], [
              QrMPInItsConta.Value], False);
            end;
            //
            QrMPInIts.Next;
          end;
          //
          // Pagar o mesmo que criou o NFeItsI!
          QrMPInIts.First;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_Tabela_C100, False, [
          'ParTipo', 'ParCodi'], [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
          GRADE_TABS_PARTIPO_0006, QrMPInItsConta.Value], [
          FImporExpor_Int, FAnoMes_Int, FEmpresa_Int, QrC100LinArq.Value], True);
        end;
      end;
      QSim := QSim + 1;
    end else
      QNao := QNao + 1;
    //
    QrC100.Next;
  end;
  Geral.MensagemArray(
    MB_ICONINFORMATION, 'Mensagem', ['0: ', '1: ', '2: ', 'n: ', '',
    'Sincronizados', 'N�o sincron.:'], [
    FormatFloat('0', Q0), FormatFloat('0', Q1), FormatFloat('0', Q2),
    FormatFloat('0', Qn), '', FormatFloat('0', QSim), FormatFloat('0', QNao)],
    nil);
  ReopenTabelas();
end;

procedure TFmSPED_EFD_Compara.BtSaida_0_0Click(Sender: TObject);
begin
  Close;
end;

procedure TFmSPED_EFD_Compara.BtSeqAcoesC100Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSeqAcoes_C100, BtSeqAcoesC100);
end;

procedure TFmSPED_EFD_Compara.BtSeqAcoesC500Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSeqAcoes_C500, BtSeqAcoesC500);
end;

procedure TFmSPED_EFD_Compara.BtSeqAcoesD100Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSeqAcoes_D100, BtSeqAcoesD100);
end;

procedure TFmSPED_EFD_Compara.BtSeqAcoesD500Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSeqAcoes_D500, BtSeqAcoesD500);
end;

procedure TFmSPED_EFD_Compara.C100_SeqAcoes_1Click(Sender: TObject);
const
  Status = 1;
var
  DataMin, DataMax, TotRec, Terceiro, NotaFiscal: String;
  ParTipo, FatID, FatNum, ImporExpor, AnoMes, Empresa, LinArq(*, ConfVal*): Integer;
  Achou: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    DefineDatasMinEMax(1, 1, DataMin, DataMax);
    //
    PB1.Max := QrC100.RecordCount;
    TotRec := FormatFloat('0', QrC100.RecordCount);
    //
    QrC100.First;
    while not QrC100.Eof do
    begin
      Terceiro := FormatFloat('0', QrC100Entidade.Value);
      NotaFiscal := FormatFloat('0', QrC100NUM_DOC.Value);
      //
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Atrelando item ' +
      FormatFloat('0', QrC100.RecNo) + ' / ' + TotRec);
      Achou := False;
      //
      ImporExpor := QrC100ImporExpor.Value;
      AnoMes := QrC100AnoMes.Value;
      Empresa := QrC100Empresa.Value;
      LinArq := QrC100LinArq.Value;
      //ConfVal := 0;
      //
      // Caso for emitida por terceiros
      if QrC100IND_EMIT.Value = '1' then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLocPQE, Dmod.MyDB, [
        'SELECT Codigo, ValorNF FROM PQE ',
        'WHERE IQ=' + Terceiro,
        'AND NF=' + NotaFiscal,
        'AND Data BETWEEN "' + DataMin + '" AND "' + DataMax + '"'
        ]);
        if QrLocPQE.RecordCount = 1 then
        begin
  {
          if QrLocPQEValorNF.Value = QrC100ValorNF.Value then
            ConfVal := 1;
  }
          //
          ParTipo := GRADE_TABS_PARTIPO_0005;
          FatID := VAR_FATID_0151;
          FatNum := QrLocPQECodigo.Value;
          //
          Achou := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_Tabela_C100, False, [
          'ParTipo', 'FatID', 'FatNum'], [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
          ParTipo,
          FatID, FatNum], [
          ImporExpor, AnoMes, Empresa, LinArq], True);
        end;
        if not Achou then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrLocMPc, Dmod.MyDB, [
          'SELECT its.Conta ',
          'FROM mpinits its ',
          'LEFT JOIN MPIn mpi ON mpi.Controle=its.Controle',
          'WHERE its.EmitNFAvul=0 ',
          'AND mpi.Procedencia=' + Terceiro,
          'AND its.NF=' + NotaFiscal,
          'AND its.Emissao BETWEEN "' + DataMin + '" AND "' + DataMax + '"'
          ]);
          if QrLocMPc.RecordCount = 1 then
          begin
            ParTipo := GRADE_TABS_PARTIPO_0006;
            FatID := VAR_FATID_0113;
            FatNum := QrLocMPcConta.Value;
            //
            Achou := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_Tabela_C100, False, [
            'ParTipo', 'FatID', 'FatNum'], [
            'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
            ParTipo,
            FatID, FatNum], [
            ImporExpor, AnoMes, Empresa, LinArq], True);
          end;
        end;
        if not Achou then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrLocEMP, Dmod.MyDB, [
          'SELECT Codigo ',
          'FROM entimp ',
          'WHERE Codigo=' + Terceiro
          ]);
          if QrLocEMP.RecordCount > 0 then
          begin
            ParTipo := GRADE_TABS_PARTIPO_0006;
            FatID := VAR_FATID_0113;
            FatNum := 0; // N�o tem nota associada
            //
            Achou := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_Tabela_C100, False, [
            'ParTipo', 'FatID', 'FatNum'], [
            'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
            ParTipo,
            FatID, FatNum], [
            ImporExpor, AnoMes, Empresa, LinArq], True);
          end;
        end;
        if not Achou then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrLocEMP, Dmod.MyDB, [
          'SELECT Procedencia Codigo ',
          'FROM mpin ',
          'WHERE Procedencia=' + Terceiro
          ]);
          if QrLocEMP.RecordCount > 0 then
          begin
            ParTipo := GRADE_TABS_PARTIPO_0006;
            FatID := VAR_FATID_0113;
            FatNum := 0; // N�o tem nota associada
            //
            Achou := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_Tabela_C100, False, [
            'ParTipo', 'FatID', 'FatNum'], [
            'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
            ParTipo,
            FatID, FatNum], [
            ImporExpor, AnoMes, Empresa, LinArq], True);
          end;
        end;
        if not Achou then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrLocEMP, Dmod.MyDB, [
          'SELECT Fornece Codigo ',
          'FROM mpinits ',
          'WHERE Fornece=' + Terceiro
          ]);
          if QrLocEMP.RecordCount > 0 then
          begin
            ParTipo := GRADE_TABS_PARTIPO_0006;
            FatID := VAR_FATID_0113;
            FatNum := 0; // N�o tem nota associada
            //
            Achou := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_Tabela_C100, False, [
            'ParTipo', 'FatID', 'FatNum'], [
            'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
            ParTipo,
            FatID, FatNum], [
            ImporExpor, AnoMes, Empresa, LinArq], True);
          end;
        end;
        if not Achou then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrLocEMP, Dmod.MyDB, [
          'SELECT EmitNFAvul Codigo ',
          'FROM mpinits ',
          'WHERE EmitNFAvul=' + Terceiro
          ]);
          if QrLocEMP.RecordCount > 0 then
          begin
            ParTipo := GRADE_TABS_PARTIPO_0006;
            FatID := VAR_FATID_0113;
            FatNum := 0; // N�o tem nota associada
            //
            Achou := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_Tabela_C100, False, [
            'ParTipo', 'FatID', 'FatNum'], [
            'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
            ParTipo,
            FatID, FatNum], [
            ImporExpor, AnoMes, Empresa, LinArq], True);
          end;
        end;
        if not Achou then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrLocEMP, Dmod.MyDB, [
          'SELECT IQ Codigo ',
          'FROM PQE ',
          'WHERE IQ=' + Terceiro
          ]);
          if QrLocEMP.RecordCount > 0 then
          begin
            ParTipo := GRADE_TABS_PARTIPO_0005;
            FatID := VAR_FATID_0151;
            FatNum := 0; // N�o tem nota associada
            //
            //Achou :=
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_Tabela_C100, False, [
            'ParTipo', 'FatID', 'FatNum'], [
            'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
            ParTipo,
            FatID, FatNum], [
            ImporExpor, AnoMes, Empresa, LinArq], True);
          end;
        end;
      end else
      // Caso for emiss�o pr�pria
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrCabA, Dmod.MyDB, [
        'SELECT IDCtrl, FatID, FatNum, Empresa ',
        'FROM nfecaba ',
        'WHERE ide_tpAmb <> 2 ',
        'AND CodInfoEmit=' + FEmpresa_Txt,
        'AND CodInfoDest=' + Terceiro,
        'AND ide_nNF=' + NotaFiscal,
        'AND ide_dEmi BETWEEN "' + DataMin + '" AND "' + DataMax + '"',
        '']);
        if QrCabA.RecordCount = 1 then
        begin
          ParTipo := GRADE_TABS_PARTIPO_0007;
          FatID := QrCabAFatID.Value;
          FatNum := QrCabAFatNum.Value;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_Tabela_C100, False, [
          'ParTipo', 'FatID', 'FatNum'], [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
          ParTipo,
          FatID, FatNum], [
          ImporExpor, AnoMes, Empresa, LinArq], True);
        end;
      end;
      //
      QrC100.Next;
    end;
    //
    IncrementaStatus(CO_Tabela_C001, 'Stat_C100', Status);
    //
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;

  { TODO : Fazer count de tipos! }
  (*
  SELECT IF(FatNum=0, 0, 1) TemFatNum,
  FatID, COUNT(LinArq) Itens
  FROM sintegr_50
  WHERE ImporExpor=1
  AND AnoMes=201001
  AND EMpresa=-11
  GROUP BY FatID, TemFatNum
  ORDER BY FatID, TemFatNum
  *)

end;

procedure TFmSPED_EFD_Compara.C100_SeqAcoes_2Click(Sender: TObject);
const
  Status = 2;
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o de todas notas C100 '
  + #13#10 + 'importadas para a empresa no per�odo selecionado?', Dmod.QrUpd,
  'efd_c100', ['ImporExpor', 'Empresa', 'AnoMes', 'Importado'],
  ['<>','=','=','>'], ['2', FEmpresa_Txt, FAnoMes_Txt, 0], '') then
    IncrementaStatus(CO_Tabela_C001, 'Stat_C100', Status);
end;

procedure TFmSPED_EFD_Compara.C100_SeqAcoes_3Click(Sender: TObject);
const
  Status = 3;
var
  DataMin, DataMax: String;
  Empresa: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    DefineDatasMinEMax(0, 0, DataMin, DataMax);
    //ImporExpor := QrC001ImporExpor.Value;
    //AnoMes := QrC001AnoMes.Value;
    Empresa := QrC001Empresa.Value;
    //
    //if CkRecriarPQ.Checked then Excluir criadas a for�a de PQE
    begin
      PB1.Position := 0;
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Pesquisando notas de uso e consumo a serem excluidas');
      QrExclMul.Close;
      QrExclMul.Params[00].AsInteger := VAR_FATID_0151;
      QrExclMul.Params[01].AsInteger := Empresa;
      QrExclMul.Params[02].AsString  := DataMin;
      QrExclMul.Params[03].AsString  := DataMax;
      QrExclMul.Open;
      //
      if QrExclMul.RecordCount > 0 then
      begin
        PB1.Max := QrExclMul.RecordCount;
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Excluindo notas de uso e consumo geradas a for�a');
        //
        while not QrExclMul.Eof do
        begin
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
          DmNFe_0000.ExcluiNfe(QrExclMulinfProt_cStat.Value, QrExclMulFatID.Value,
            QrExclMulFatNum.Value, QrExclMulEmpresa.Value, True, True);
          //
          QrExclMul.Next;
        end;
      end;
    end;

    //if CkRecriarMP.Checked then Excluir criadas a forca de MPIn
    begin
      PB1.Position := 0;
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Pesquisando notas de mat�ria-prima a serem excluidas');
      QrExclMul.Close;
      QrExclMul.Params[00].AsInteger := VAR_FATID_0113;
      QrExclMul.Params[01].AsInteger := Empresa;
      QrExclMul.Params[02].AsString  := DataMin;
      QrExclMul.Params[03].AsString  := DataMax;
      QrExclMul.Open;
      //
      if QrExclMul.RecordCount > 0 then
      begin
        PB1.Max := QrExclMul.RecordCount;
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Excluindo notas de mat�ria-prima geradas a for�a');
        while not QrExclMul.Eof do
        begin
          PB1.Position := PB1.Position + 1;
          DmNFe_0000.ExcluiNfe(QrExclMulinfProt_cStat.Value, QrExclMulFatID.Value,
            QrExclMulFatNum.Value, QrExclMulEmpresa.Value, True, True);
          //
          QrExclMul.Next;
        end;
      end;
    end;
    //
    IncrementaStatus(CO_Tabela_C001, 'Stat_C100', Status);
    //
    PB1.POsition := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSPED_EFD_Compara.C100_SeqAcoes_4Click(Sender: TObject);
const
  Status = 4;
var
  Acao: Integer;
  DataMin, DataMax: String;
begin
  Acao := MyObjects.SelRadioGroup('Cria��o de NFs', 'Amplitude da a��o',
  [(*0*)'Criar todas NFs poss�veis dando prefer�ncia aos importados',
   (*1*)'Criar todas NFs poss�veis dando prefer�ncia ao v�nculo',
   (*2*)'Somente criar NFs importadas (desde que tenham itens)',
   (*3*)'Somente criar NFs vinculadas'], 1);
  if Acao > -1 then
  begin
    Screen.Cursor := crHourGlass;
    try
      DefineDatasMinEMax(1, 1, DataMin, DataMax);
      //
      QrC100.First;
      while not QrC100.Eof do
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando Documento ' +
          FormatFloat('000000', QrC100.RecNo) + '/' +
          FormatFloat('000000', QrC100.RecordCount));
        //
        if (Acao in([0,2])) and (QrC170.RecordCount > 0) then
        begin
          if not Localiza_EFD() then
          begin
            CriaNF_C100_de_EFD();
            Localiza_EFD();
          end;
        end
        else if Acao <> 2 then
        begin
          if QrC100ParTipo.Value in ([1,5]) then //PQE
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(QrLocPQE, Dmod.MyDB, [
            'SELECT Codigo, ValorNF FROM PQE ',
            'WHERE IQ=' + FormatFloat('0', QrC100Entidade.Value),
            'AND NF=' + FormatFloat('0', QrC100NUM_DOC.Value),
            'AND Data BETWEEN "' + DataMin + '" AND "' + DataMax + '"'
            ]);
            if QrLocPQE.RecordCount = 1 then
              CriaNF_C100_de_PQE(QrLocPQECodigo.Value);
          end;
        end;
        //
        QrC100.Next;
      end;
      //
      IncrementaStatus(CO_Tabela_C001, 'Stat_C100', Status);
      //
      PB1.Position := 0;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    finally
      Screen.Cursor := crDefault;
    end;
  end else Geral.MensagemBox('Cria��o de NFs cancelada pelo usu�rio!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmSPED_EFD_Compara.C500_SeqAcoes_1Click(Sender: TObject);
const
  Status = 1;
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o de todas notas C500 '
  + #13#10 + 'importadas para a empresa no per�odo selecionado?', Dmod.QrUpd,
  'efd_c500', ['ImporExpor', 'Empresa', 'AnoMes', 'Importado'],
  ['=','=','=','>'], [FImporExpor_Txt, FEmpresa_Txt, FAnoMes_Txt, 0], '') then
    IncrementaStatus(CO_Tabela_C001, 'Stat_C500', Status);
end;

procedure TFmSPED_EFD_Compara.C500_SeqAcoes_2Click(Sender: TObject);
const
  ImporExpor = '3'; // Criar! n�o mexer
  //
  function CriaNF_C500(): Boolean;
  const
    REG = 'C500';
    Importado = 2;
  var
    AnoMes, Empresa, LinArq: Integer;
    IND_OPER, IND_EMIT, COD_PART, COD_MOD, COD_SIT, SER, SUB, COD_CONS, NUM_DOC,
    DT_DOC, DT_E_S: String;
    VL_DOC, VL_DESC, VL_FORN, VL_SERV_NT, VL_TERC, VL_DA, VL_BC_ICMS, VL_ICMS,
    VL_BC_ICMS_ST, VL_ICMS_ST: Double;
    COD_INF: String;
    VL_PIS, VL_COFINS: Double;
    TP_LIGACAO, COD_GRUPO_TENSAO: String;
    Terceiro: Integer;
    CST_ICMS, CFOP: Integer;
    ALIQ_ICMS, VL_RED_BC: Double;
  begin
    AnoMes := FAnoMes_Int;
    Empresa := FEmpresa_Int;

    //
    IND_OPER         := QrC500IND_OPER.Value;
    IND_EMIT         := QrC500IND_EMIT.Value;
    COD_PART         := QrC500COD_PART.Value;
    COD_MOD          := QrC500COD_MOD.Value;
    COD_SIT          := QrC500COD_SIT.Value;
    SER              := QrC500SER.Value;
    SUB              := QrC500SUB.Value;
    COD_CONS         := QrC500COD_CONS.Value;
    NUM_DOC          := FormatFloat('0', QrC500NUM_DOC.Value);
    DT_DOC           := Geral.FDT(QrC500DT_DOC.Value, 1);
    DT_E_S           := Geral.FDT(QrC500DT_E_S.Value, 1);
    VL_DOC           := QrC500VL_DOC.Value;
    VL_DESC          := QrC500VL_DESC.Value;
    VL_FORN          := QrC500VL_FORN.Value;
    VL_SERV_NT       := QrC500VL_SERV_NT.Value;
    VL_TERC          := QrC500VL_TERC.Value;
    VL_DA            := QrC500VL_DA.Value;
    VL_BC_ICMS       := QrC500VL_BC_ICMS.Value;
    VL_ICMS          := QrC500VL_ICMS.Value;
    VL_BC_ICMS_ST    := QrC500VL_BC_ICMS_ST.Value;
    VL_ICMS_ST       := QrC500VL_ICMS_ST.Value;
    COD_INF          := QrC500COD_INF.Value;
    VL_PIS           := QrC500VL_PIS.Value;
    VL_COFINS        := QrC500VL_COFINS.Value;
    TP_LIGACAO       := QrC500TP_LIGACAO.Value;
    COD_GRUPO_TENSAO := QrC500COD_GRUPO_TENSAO.Value;
    Terceiro         := QrC500Entidade.Value;
    CST_ICMS         := QrC590CST_ICMS.Value;
    CFOP             := QrC590CFOP.Value;
    ALIQ_ICMS        := QrC590ALIQ_ICMS.Value;
    VL_RED_BC        := QrC590VL_RED_BC.Value;
    //
    LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_c500', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
      stIns, 0, siPositivo, nil);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efd_c500', False, [
    'REG', 'IND_OPER', 'IND_EMIT',
    'COD_PART', 'COD_MOD', 'COD_SIT',
    'SER', 'SUB', 'COD_CONS',
    'NUM_DOC', 'DT_DOC', 'DT_E_S',
    'VL_DOC', 'VL_DESC', 'VL_FORN',
    'VL_SERV_NT', 'VL_TERC', 'VL_DA',
    'VL_BC_ICMS', 'VL_ICMS', 'VL_BC_ICMS_ST',
    'VL_ICMS_ST', 'COD_INF', 'VL_PIS',
    'VL_COFINS', 'TP_LIGACAO', 'COD_GRUPO_TENSAO',
    'Terceiro', 'Importado', 'CST_ICMS',
    'CFOP', 'ALIQ_ICMS', 'VL_RED_BC'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
    REG, IND_OPER, IND_EMIT,
    COD_PART, COD_MOD, COD_SIT,
    SER, SUB, COD_CONS,
    NUM_DOC, DT_DOC, DT_E_S,
    VL_DOC, VL_DESC, VL_FORN,
    VL_SERV_NT, VL_TERC, VL_DA,
    VL_BC_ICMS, VL_ICMS, VL_BC_ICMS_ST,
    VL_ICMS_ST, COD_INF, VL_PIS,
    VL_COFINS, TP_LIGACAO, COD_GRUPO_TENSAO,
    Terceiro, Importado, CST_ICMS,
    CFOP, ALIQ_ICMS, VL_RED_BC], [
    ImporExpor, AnoMes, Empresa, LinArq], True);
  end;
const
  Status = 2;
  LinArq = 1;
  REG = 'C001';
  IND_MOV = '0';
begin
  PB1.Position := 0;
  PB1.Max := QrC500.RecordCount;
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Verificando se o periodo existe');
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_c001',
  'WHERE ImporExpor=' + ImporExpor,
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_Txt,
  '']);
  if Dmod.QrAux.RecordCount = 0 then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, StIns, 'efd_c001', False, [
    'LinArq', 'REG', 'IND_MOV'], [
    'ImporExpor', 'AnoMes', 'Empresa'], [
    LinArq, REG, IND_MOV], [
    ImporExpor, FAnoMes_Int, FEmpresa_Int], True);
  end;
  //
  QrC500.First;
  while not QrC500.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Incluindo registro ' +
      FormatFloat('0', QrC500.RecNo));
    CriaNF_C500();
    //
    QrC500.Next;
  end;
  IncrementaStatus(CO_Tabela_C001, 'Stat_C500', Status);
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmSPED_EFD_Compara.CkFiltra_0_0Click(Sender: TObject);
begin
  ReopenPQE();
end;

procedure TFmSPED_EFD_Compara.CkFiltra_0_1Click(Sender: TObject);
begin
  ReopenMPInIts();
end;

procedure TFmSPED_EFD_Compara.CriaNF_C100_de_EFD();
const
  Importado = 2;
  ImporExpor = 3;
var
  AnoMes, Empresa, LinArq: Integer;
  REG, IND_OPER, IND_EMIT, COD_PART, COD_MOD, COD_SIT, SER: String;
  NUM_DOC: Integer;
  IND_PGTO, IND_FRT,
  CHV_NFE, DT_DOC, DT_E_S: String;
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST: Double;
  ParTipo, ParCodi, FatID, FatNum, ConfVal, Terceiro: Integer;
  CST_ICMS, CFOP: String;
  ALIQ_ICMS, VL_RED_BC: Double;
begin
  AnoMes := QrC100AnoMes.Value;
  Empresa := QrC001Empresa.Value;
  //
  REG           := QrC100REG.Value;
  IND_OPER      := QrC100IND_OPER.Value;
  IND_EMIT      := QrC100IND_EMIT.Value;
  COD_PART      := QrC100COD_PART.Value;
  COD_MOD       := QrC100COD_MOD.Value;
  COD_SIT       := QrC100COD_SIT.Value;
  SER           := QrC100SER.Value;
  NUM_DOC       := QrC100NUM_DOC.Value;
  CHV_NFE       := QrC100CHV_NFE.Value;
  DT_DOC        := Geral.FDT(QrC100DT_DOC.Value, 1);
  DT_E_S        := Geral.FDT(QrC100DT_E_S.Value, 1);
  VL_DOC        := QrC100VL_DOC.Value;
  IND_PGTO      := QrC100IND_PGTO.Value;
  VL_DESC       := QrC100VL_DESC.Value;
  VL_ABAT_NT    := QrC100VL_ABAT_NT.Value;
  VL_MERC       := QrC100VL_MERC.Value;
  IND_FRT       := QrC100IND_FRT.Value;
  VL_FRT        := QrC100VL_FRT.Value;
  VL_SEG        := QrC100VL_SEG.Value;
  VL_OUT_DA     := QrC100VL_OUT_DA.Value;
  VL_BC_ICMS    := QrC100VL_BC_ICMS.Value;
  VL_ICMS       := QrC100VL_ICMS.Value;
  VL_BC_ICMS_ST := QrC100VL_BC_ICMS_ST.Value;
  VL_ICMS_ST    := QrC100VL_ICMS_ST.Value;
  VL_IPI        := QrC100VL_IPI.Value;
  VL_PIS        := QrC100VL_PIS.Value;
  VL_COFINS     := QrC100VL_COFINS.Value;
  VL_PIS_ST     := QrC100VL_PIS_ST.Value;
  VL_COFINS_ST  := QrC100VL_COFINS_ST.Value;
  ParTipo       := QrC100ParTipo.Value;
  ParCodi       := QrC100ParCodi.Value;
  FatID         := QrC100FatID.Value;
  FatNum        := QrC100FatNum.Value;
  ConfVal       := 0;
  Terceiro      := QrC100Entidade.Value;
  CST_ICMS      := '';
  CFOP          := '';
  ALIQ_ICMS     := 0;
  VL_RED_BC     := 0;
  //
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_c100', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
    stIns, 0, siPositivo, nil);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efd_c100', False, [
  'REG', 'IND_OPER', 'IND_EMIT',
  'COD_PART', 'COD_MOD', 'COD_SIT',
  'SER', 'NUM_DOC', 'CHV_NFE',
  'DT_DOC', 'DT_E_S', 'VL_DOC',
  'IND_PGTO', 'VL_DESC', 'VL_ABAT_NT',
  'VL_MERC', 'IND_FRT', 'VL_FRT',
  'VL_SEG', 'VL_OUT_DA', 'VL_BC_ICMS',
  'VL_ICMS', 'VL_BC_ICMS_ST', 'VL_ICMS_ST',
  'VL_IPI', 'VL_PIS', 'VL_COFINS',
  'VL_PIS_ST', 'VL_COFINS_ST', 'ParTipo',
  'ParCodi', 'FatID', 'FatNum',
  'ConfVal', 'Terceiro', 'Importado',
  'CST_ICMS', 'CFOP', 'ALIQ_ICMS',
  'VL_RED_BC'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  REG, IND_OPER, IND_EMIT,
  COD_PART, COD_MOD, COD_SIT,
  SER, NUM_DOC, CHV_NFE,
  DT_DOC, DT_E_S, VL_DOC,
  IND_PGTO, VL_DESC, VL_ABAT_NT,
  VL_MERC, IND_FRT, VL_FRT,
  VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST,
  VL_IPI, VL_PIS, VL_COFINS,
  VL_PIS_ST, VL_COFINS_ST, ParTipo,
  ParCodi, FatID, FatNum,
  ConfVal, Terceiro, Importado,
  CST_ICMS, CFOP, ALIQ_ICMS,
  VL_RED_BC], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
    QrC170.First;
    while not QrC170.Eof do
    begin
      CriaNF_C170_de_EFD(ImporExpor, LinArq);
      //
      QrC170.Next;
    end;
  end;
end;

procedure TFmSPED_EFD_Compara.CriaNF_C100_de_PQE(Codigo: Integer);
const
  Importado = 2;
  ImporExpor = 3;
  REG = 'C100';
var
  AnoMes, Empresa, LinArq: Integer;
  IND_OPER, IND_EMIT, COD_PART, COD_MOD, COD_SIT, SER: String;
  NUM_DOC: Integer;
  IND_PGTO, IND_FRT,
  CHV_NFE, DT_DOC, DT_E_S: String;
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST: Double;
  ParTipo, ParCodi, FatID, FatNum, ConfVal, Terceiro: Integer;
  CST_ICMS, CFOP: String;
  ALIQ_ICMS, VL_RED_BC: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQE_2_, Dmod.MyDB, [
  'SELECT Codigo FatNum, DataE dtEmi, DataS dtSaiEnt,  ',
  'Data DataFiscal, ValorNF ValTot, CI Empresa,  ',
  'CI CodInfoDest, IQ CodInfoEmit, refNFe Id,  ',
  'modNF ide_mod, Serie,  NF nNF, Frete, Seguro, ',
  'Desconto, IPI, PIS, COFINS, Outros',
  'FROM PQE ',
  'WHERE Codigo=' + FormatFloat('0', Codigo),
  '']);
  AnoMes := QrC001AnoMes.Value;
  Empresa := QrC001Empresa.Value;
  Terceiro := QrPQE_2_CodInfoEmit.Value;
  //
  IND_OPER      := '0'; // Entrada
  IND_EMIT      := '1'; // Terceiros
  COD_PART      := FormatFloat('0', Terceiro);
  COD_MOD       := FormatFloat('00', QrPQE_2_ide_mod.Value);
  COD_SIT       := '00'; // Normal
  SER           := FormatFloat('0', QrPQE_2_Serie.Value);
  NUM_DOC       := QrPQE_2_nNF.Value;
  CHV_NFE       := QrPQE_2_Id.Value;
  DT_DOC        := Geral.FDT(QrPQE_2_dtEmi.Value, 1);
  DT_E_S        := Geral.FDT(QrPQE_2_dtSaiEnt.Value, 1);
  VL_DOC        := QrPQE_2_ValTot.Value;
  IND_PGTO      := '1'; // A prazo
  VL_DESC       := QrPQE_2_Desconto.Value;
  VL_ABAT_NT    := 0;
  VL_MERC       := QrPQE_2_ValTot.Value;
  IND_FRT       := '2';
  VL_FRT        := QrPQE_2_Frete.Value;
  VL_SEG        := QrPQE_2_Seguro.Value;
  VL_OUT_DA     := QrPQE_2_Outros.Value;
  VL_BC_ICMS    := 0; 
  VL_ICMS       := 0; 
  VL_BC_ICMS_ST := 0; 
  VL_ICMS_ST    := 0; 
  VL_IPI        := 0; 
  VL_PIS        := 0; 
  VL_COFINS     := 0; 
  VL_PIS_ST     := 0; 
  VL_COFINS_ST  := 0; 
  ParTipo       := 0; 
  ParCodi       := 0; 
  FatID         := 0; 
  FatNum        := 0; 
  ConfVal       := 0;
  CST_ICMS      := '';
  CFOP          := '';
  ALIQ_ICMS     := 0;
  VL_RED_BC     := 0;
  //
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_c100', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
    stIns, 0, siPositivo, nil);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efd_c100', False, [
  'REG', 'IND_OPER', 'IND_EMIT',
  'COD_PART', 'COD_MOD', 'COD_SIT',
  'SER', 'NUM_DOC', 'CHV_NFE',
  'DT_DOC', 'DT_E_S', 'VL_DOC',
  'IND_PGTO', 'VL_DESC', 'VL_ABAT_NT',
  'VL_MERC', 'IND_FRT', 'VL_FRT',
  'VL_SEG', 'VL_OUT_DA', 'VL_BC_ICMS',
  'VL_ICMS', 'VL_BC_ICMS_ST', 'VL_ICMS_ST',
  'VL_IPI', 'VL_PIS', 'VL_COFINS',
  'VL_PIS_ST', 'VL_COFINS_ST', 'ParTipo',
  'ParCodi', 'FatID', 'FatNum',
  'ConfVal', 'Terceiro', 'Importado',
  'CST_ICMS', 'CFOP', 'ALIQ_ICMS',
  'VL_RED_BC'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  REG, IND_OPER, IND_EMIT,
  COD_PART, COD_MOD, COD_SIT,
  SER, NUM_DOC, CHV_NFE,
  DT_DOC, DT_E_S, VL_DOC,
  IND_PGTO, VL_DESC, VL_ABAT_NT,
  VL_MERC, IND_FRT, VL_FRT,
  VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST,
  VL_IPI, VL_PIS, VL_COFINS,
  VL_PIS_ST, VL_COFINS_ST, ParTipo,
  ParCodi, FatID, FatNum,
  ConfVal, Terceiro, Importado,
  CST_ICMS, CFOP, ALIQ_ICMS,
  VL_RED_BC], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
    { N�o excluir, pois teria que recriar!
    UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'stqmovitsa',
    ['Tipo', 'OriCodi'(*, 'OriCtrl'*)], ['=','='(*,'='*)],
    [VAR_FATID_0151, QrPQE_2_FatNum.Value(*, ?*)], '');
    }
    //
    CriaNF_C170_de_PQE(ImporExpor, LinArq, Codigo);
  end;
end;

procedure TFmSPED_EFD_Compara.CriaNF_C170_de_EFD(ImporExpor, C100: Integer);
var
  GraGruX, AnoMes, Empresa, LinArq: Integer;
  REG: String;
  NUM_ITEM: Integer;
  COD_ITEM, DESCR_COMPL: String;
  QTD: Double;
  UNID: String;
  VL_ITEM, VL_DESC: Double;
  IND_MOV: String;
  CST_ICMS, CFOP: Integer;
  COD_NAT: String;
  VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST,
  ALIQ_ST, VL_ICMS_ST: Double;
  IND_APUR, CST_IPI, COD_ENQ: String;
  VL_BC_IPI, ALIQ_IPI, VL_IPI: Double;
  CST_PIS: Integer;
  VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
  ALIQ_PIS_r, VL_PIS: Double;
  CST_COFINS: Integer;
  VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS,
  ALIQ_COFINS_r, VL_COFINS: Double;
  COD_CTA: String;
begin
  REG             := QrC170REG.Value;
  NUM_ITEM        := QrC170NUM_ITEM.Value;
  COD_ITEM        := QrC170COD_ITEM.Value;
  DESCR_COMPL     := QrC170DESCR_COMPL.Value;
  QTD             := QrC170QTD.Value;
  UNID            := QrC170UNID.Value;
  VL_ITEM         := QrC170VL_ITEM.Value;
  VL_DESC         := QrC170VL_DESC.Value;
  IND_MOV         := QrC170IND_MOV.Value;
  CST_ICMS        := QrC170CST_ICMS.Value;
  CFOP            := QrC170CFOP.Value;
  COD_NAT         := QrC170COD_NAT.Value;
  VL_BC_ICMS      := QrC170VL_BC_ICMS.Value;
  ALIQ_ICMS       := QrC170ALIQ_ICMS.Value;
  VL_ICMS         := QrC170VL_ICMS.Value;
  VL_BC_ICMS_ST   := QrC170VL_BC_ICMS_ST.Value;
  ALIQ_ST         := QrC170ALIQ_ST.Value;
  VL_ICMS_ST      := QrC170VL_ICMS_ST.Value;
  IND_APUR        := QrC170IND_APUR.Value;
  CST_IPI         := QrC170CST_IPI.Value;
  COD_ENQ         := QrC170COD_ENQ.Value;
  VL_BC_IPI       := QrC170VL_BC_IPI.Value;
  ALIQ_IPI        := QrC170ALIQ_IPI.Value;
  VL_IPI          := QrC170VL_IPI.Value;
  CST_PIS         := QrC170CST_PIS.Value;
  VL_BC_PIS       := QrC170VL_BC_PIS.Value;
  ALIQ_PIS_p      := QrC170ALIQ_PIS_p.Value;
  QUANT_BC_PIS    := QrC170QUANT_BC_PIS.Value;
  ALIQ_PIS_r      := QrC170ALIQ_PIS_r.Value;
  VL_PIS          := QrC170VL_PIS.Value;
  CST_COFINS      := QrC170CST_COFINS.Value;
  VL_BC_COFINS    := QrC170VL_BC_COFINS.Value;
  ALIQ_COFINS_p   := QrC170ALIQ_COFINS_p.Value;
  QUANT_BC_COFINS := QrC170QUANT_BC_COFINS.Value;
  ALIQ_COFINS_r   := QrC170ALIQ_COFINS_r.Value;
  VL_COFINS       := QrC170VL_COFINS.Value;
  COD_CTA         := QrC170COD_CTA.Value;

  //
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_c170', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
    stIns, 0, siPositivo, nil);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efd_c170', False, [
  'C100', 'GraGruX', 'REG',
  'NUM_ITEM', 'COD_ITEM', 'DESCR_COMPL',
  'QTD', 'UNID', 'VL_ITEM',
  'VL_DESC', 'IND_MOV', 'CST_ICMS',
  'CFOP', 'COD_NAT', 'VL_BC_ICMS',
  'ALIQ_ICMS', 'VL_ICMS', 'VL_BC_ICMS_ST',
  'ALIQ_ST', 'VL_ICMS_ST', 'IND_APUR',
  'CST_IPI', 'COD_ENQ', 'VL_BC_IPI',
  'ALIQ_IPI', 'VL_IPI', 'CST_PIS',
  'VL_BC_PIS', 'ALIQ_PIS_p', 'QUANT_BC_PIS',
  'ALIQ_PIS_r', 'VL_PIS', 'CST_COFINS',
  'VL_BC_COFINS', 'ALIQ_COFINS_p', 'QUANT_BC_COFINS',
  'ALIQ_COFINS_r', 'VL_COFINS', 'COD_CTA'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  C100, GraGruX, REG,
  NUM_ITEM, COD_ITEM, DESCR_COMPL,
  QTD, UNID, VL_ITEM,
  VL_DESC, IND_MOV, CST_ICMS,
  CFOP, COD_NAT, VL_BC_ICMS,
  ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST,
  ALIQ_ST, VL_ICMS_ST, IND_APUR,
  CST_IPI, COD_ENQ, VL_BC_IPI,
  ALIQ_IPI, VL_IPI, CST_PIS,
  VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
  ALIQ_PIS_r, VL_PIS, CST_COFINS,
  VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS,
  ALIQ_COFINS_r, VL_COFINS, COD_CTA], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
{ TODO : Ver se precisa! }
{
    IncluiStqMovItsA_C170(Empresa, GraGruX);
}
  end;
end;

procedure TFmSPED_EFD_Compara.CriaNF_C170_de_PQE(ImporExpor, C100,
  Codigo: Integer);
const
  REG = 'C170';
var
  GraGruX, AnoMes, Empresa, LinArq: Integer;
  NUM_ITEM: Integer;
  COD_ITEM, DESCR_COMPL: String;
  QTD: Double;
  UNID: String;
  VL_ITEM, VL_DESC: Double;
  IND_MOV: String;
  CST_ICMS, CFOP: Integer;
  COD_NAT: String;
  VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST,
  ALIQ_ST, VL_ICMS_ST: Double;
  IND_APUR, CST_IPI, COD_ENQ: String;
  VL_BC_IPI, ALIQ_IPI, VL_IPI: Double;
  CST_PIS: Integer;
  VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
  ALIQ_PIS_r, VL_PIS: Double;
  CST_COFINS: Integer;
  VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS,
  ALIQ_COFINS_r, VL_COFINS: Double;
  COD_CTA: String;
  //
  UnidMedCom: Integer;
begin
  QrPQEIts_2_.Close;
  QrPQEIts_2_.Params[0].AsInteger := QrPQE_2_FatNum.Value;
  QrPQEIts_2_.Open;
  while not QrPQEIts_2_.Eof do
  begin
    NUM_ITEM        := QrPQEIts_2_.RecNo;
    COD_ITEM        := FormatFloat('0', QrPQEIts_2_Insumo.Value);
    DESCR_COMPL     := QrPQEIts_2_NOMEPQ.Value;
    QTD             := QrPQEIts_2_TotalPeso.Value;
    DmProd.Obtem_prod_uTribdeGraGru1(QrPQEIts_2_Insumo.Value, UNID, UnidMedCom);
    VL_ITEM         := QrPQEIts_2_TotalCusto.Value;
    VL_DESC         := 0;
    IND_MOV         := '0';
    CST_ICMS        := QrPQEIts_2_ICMS_CST.Value;
    CFOP            := Geral.IMV(QrPQEIts_2_prod_CFOP.Value);
    COD_NAT         := '';
    VL_BC_ICMS      := 0;
    ALIQ_ICMS       := 0;
    VL_ICMS         := 0;
    VL_BC_ICMS_ST   := 0;
    ALIQ_ST         := 0;
    VL_ICMS_ST      := 0;
    IND_APUR        := '0';
    CST_IPI         := '00';
    COD_ENQ         := '';
    VL_BC_IPI       := 0;
    ALIQ_IPI        := 0;
    VL_IPI          := 0;
    CST_PIS         := 1;
    VL_BC_PIS       := 0;
    ALIQ_PIS_p      := 0;
    QUANT_BC_PIS    := 0;
    ALIQ_PIS_r      := 0;
    VL_PIS          := 0;
    CST_COFINS      := 1;
    VL_BC_COFINS    := 0;
    ALIQ_COFINS_p   := 0;
    QUANT_BC_COFINS := 0;
    ALIQ_COFINS_r   := 0;
    VL_COFINS       := 0;
    COD_CTA         := '';

    //
    LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_c170', 'LinArq', [
    (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
      stIns, 0, siPositivo, nil);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efd_c170', False, [
    'C100', 'GraGruX', 'REG',
    'NUM_ITEM', 'COD_ITEM', 'DESCR_COMPL',
    'QTD', 'UNID', 'VL_ITEM',
    'VL_DESC', 'IND_MOV', 'CST_ICMS',
    'CFOP', 'COD_NAT', 'VL_BC_ICMS',
    'ALIQ_ICMS', 'VL_ICMS', 'VL_BC_ICMS_ST',
    'ALIQ_ST', 'VL_ICMS_ST', 'IND_APUR',
    'CST_IPI', 'COD_ENQ', 'VL_BC_IPI',
    'ALIQ_IPI', 'VL_IPI', 'CST_PIS',
    'VL_BC_PIS', 'ALIQ_PIS_p', 'QUANT_BC_PIS',
    'ALIQ_PIS_r', 'VL_PIS', 'CST_COFINS',
    'VL_BC_COFINS', 'ALIQ_COFINS_p', 'QUANT_BC_COFINS',
    'ALIQ_COFINS_r', 'VL_COFINS', 'COD_CTA'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
    C100, GraGruX, REG,
    NUM_ITEM, COD_ITEM, DESCR_COMPL,
    QTD, UNID, VL_ITEM,
    VL_DESC, IND_MOV, CST_ICMS,
    CFOP, COD_NAT, VL_BC_ICMS,
    ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST,
    ALIQ_ST, VL_ICMS_ST, IND_APUR,
    CST_IPI, COD_ENQ, VL_BC_IPI,
    ALIQ_IPI, VL_IPI, CST_PIS,
    VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
    ALIQ_PIS_r, VL_PIS, CST_COFINS,
    VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS,
    ALIQ_COFINS_r, VL_COFINS, COD_CTA], [
    ImporExpor, AnoMes, Empresa, LinArq], True) then
    begin
      // N�o precisa! J� foi incluido?
      //IncluiStqMovItsA_PQEIts(Empresa, GraGruX);
    end;
    QrPQEIts_2_.Next;
  end;
end;

procedure TFmSPED_EFD_Compara.D100_SeqAcoes_1Click(Sender: TObject);
const
  Status = 1;
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o de todas notas D100 '
  + #13#10 + 'importadas para a empresa no per�odo selecionado?', Dmod.QrUpd,
  'efd_d100', ['ImporExpor', 'Empresa', 'AnoMes', 'Importado'],
  ['=','=','=','>'], [FImporExpor_Txt, FEmpresa_Txt, FAnoMes_Txt, 0], '') then
    IncrementaStatus(CO_Tabela_D001, 'Stat_d100i', Status);
end;

procedure TFmSPED_EFD_Compara.D100_SeqAcoes_2Click(Sender: TObject);
const
  ImporExpor = '3'; // Criar! n�o mexer
  //
  function CriaNF_D100(): Boolean;
  const
    REG = 'D100';
    Importado = 2;
  var
    AnoMes, Empresa, LinArq: Integer;
    IND_OPER, IND_EMIT, COD_PART, COD_MOD, COD_SIT, SER, SUB, NUM_DOC, CHV_CTE,
    DT_DOC, DT_A_P, TP_CTE, CHV_CTE_REF, IND_FRT: String;
    VL_DOC, VL_DESC, VL_SERV, VL_BC_ICMS, VL_ICMS, VL_NT: Double;
    COD_INF: String;
    COD_CTA: String;
    Terceiro: Integer;
    CST_ICMS, CFOP: Integer;
    ALIQ_ICMS, VL_RED_BC: Double;
  begin
    AnoMes := FAnoMes_Int;
    Empresa := FEmpresa_Int;

    //
    IND_OPER         := QrD100IND_OPER.Value;
    IND_EMIT         := QrD100IND_EMIT.Value;
    COD_PART         := QrD100COD_PART.Value;
    COD_MOD          := QrD100COD_MOD.Value;
    COD_SIT          := QrD100COD_SIT.Value;
    SER              := QrD100SER.Value;
    SUB              := QrD100SUB.Value;
    NUM_DOC          := FormatFloat('0', QrD100NUM_DOC.Value);
    CHV_CTE          := QrD100CHV_CTE.Value;
    DT_DOC           := Geral.FDT(QrD100DT_DOC.Value, 1);
    DT_A_P           := Geral.FDT(QrD100DT_A_P.Value, 1);
    TP_CTE           := FormatFloat('0', QrD100TP_CTE.Value); // 2022-03-29
    CHV_CTE_REF      := QrD100CHV_CTE_REF.Value;
    VL_DOC           := QrD100VL_DOC.Value;
    VL_DESC          := QrD100VL_DESC.Value;
    IND_FRT          := QrD100IND_FRT.Value;
    VL_SERV          := QrD100VL_SERV.Value;
    VL_BC_ICMS       := QrD100VL_BC_ICMS.Value;
    VL_ICMS          := QrD100VL_ICMS.Value;
    VL_NT            := QrD100VL_NT.Value;
    COD_INF          := QrD100COD_INF.Value;
    Terceiro         := QrD100Entidade.Value;
    CST_ICMS         := QrD190CST_ICMS.Value;
    CFOP             := QrD190CFOP.Value;
    ALIQ_ICMS        := QrD190ALIQ_ICMS.Value;
    VL_RED_BC        := QrD190VL_RED_BC.Value;
    COD_CTA          := '';
    //
    LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_d100', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
      stIns, 0, siPositivo, nil);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efd_d100', False, [
    'REG', 'IND_OPER', 'IND_EMIT',
    'COD_PART', 'COD_MOD', 'COD_SIT',
    'SER', 'SUB', 'NUM_DOC',
    'CHV_CTE', 'DT_DOC', 'DT_A_P',
    'TP_CTE', 'CHV_CTE_REF', 'VL_DOC',
    'VL_DESC', 'IND_FRT', 'VL_SERV',
    'VL_BC_ICMS', 'VL_ICMS', 'VL_NT',
    'COD_INF', 'COD_CTA', 'Terceiro',
    'Importado', 'CST_ICMS', 'CFOP',
    'ALIQ_ICMS', 'VL_RED_BC'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
    REG, IND_OPER, IND_EMIT,
    COD_PART, COD_MOD, COD_SIT,
    SER, SUB, NUM_DOC,
    CHV_CTE, DT_DOC, DT_A_P,
    TP_CTE, CHV_CTE_REF, VL_DOC,
    VL_DESC, IND_FRT, VL_SERV,
    VL_BC_ICMS, VL_ICMS, VL_NT,
    COD_INF, COD_CTA, Terceiro,
    Importado, CST_ICMS, CFOP,
    ALIQ_ICMS, VL_RED_BC], [
    ImporExpor, AnoMes, Empresa, LinArq], True);
  end;
const
  Status = 2;
  LinArq = 1;
  REG = 'D001';
  IND_MOV = '0';
begin
  PB1.Position := 0;
  PB1.Max := QrD100.RecordCount;
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Verificando se o periodo existe');
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_d001',
  'WHERE ImporExpor=' + ImporExpor,
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_Txt,
  '']);
  if Dmod.QrAux.RecordCount = 0 then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, StIns, 'efd_d001', False, [
    'LinArq', 'REG', 'IND_MOV'], [
    'ImporExpor', 'AnoMes', 'Empresa'], [
    LinArq, REG, IND_MOV], [
    ImporExpor, FAnoMes_Int, FEmpresa_Int], True);
  end;
  //
  QrD100.First;
  while not QrD100.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Incluindo registro ' +
      FormatFloat('0', QrD100.RecNo));
    CriaNF_D100();
    //
    QrD100.Next;
  end;
  IncrementaStatus(CO_Tabela_D001, 'Stat_D100i', Status);
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmSPED_EFD_Compara.D500_SeqAcoes_1Click(Sender: TObject);
const
  Status = 1;
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o de todas notas D500 '
  + #13#10 + 'importadas para a empresa no per�odo selecionado?', Dmod.QrUpd,
  'efd_d500', ['ImporExpor', 'Empresa', 'AnoMes', 'Importado'],
  ['=','=','=','>'], [FImporExpor_Txt, FEmpresa_Txt, FAnoMes_Txt, 0], '') then
    IncrementaStatus(CO_Tabela_D001, 'Stat_D500i', Status);
end;

procedure TFmSPED_EFD_Compara.D500_SeqAcoes_2Click(Sender: TObject);
const
  ImporExpor = '3'; // Criar! n�o mexer
  //
  function CriaNF_D500(): Boolean;
  const
    REG = 'D500';
    Importado = 2;
  var
    AnoMes, Empresa, LinArq: Integer;
    IND_OPER, IND_EMIT, COD_PART, COD_MOD, COD_SIT, SER, SUB, NUM_DOC,
    DT_DOC, DT_A_P: String;
    VL_DOC, VL_DESC, VL_SERV, VL_SERV_NT, VL_TERC, VL_DA, VL_BC_ICMS, VL_ICMS
    : Double;
    COD_INF: String;
    VL_PIS, VL_COFINS: Double;
    TP_ASSINANTE: String;
    Terceiro: Integer;
    CST_ICMS, CFOP: Integer;
    ALIQ_ICMS, VL_RED_BC: Double;
  begin
    AnoMes := FAnoMes_Int;
    Empresa := FEmpresa_Int;

    //
    IND_OPER         := QrD500IND_OPER.Value;
    IND_EMIT         := QrD500IND_EMIT.Value;
    COD_PART         := QrD500COD_PART.Value;
    COD_MOD          := QrD500COD_MOD.Value;
    COD_SIT          := QrD500COD_SIT.Value;
    SER              := QrD500SER.Value;
    SUB              := QrD500SUB.Value;
    NUM_DOC          := FormatFloat('0', QrD500NUM_DOC.Value);
    DT_DOC           := Geral.FDT(QrD500DT_DOC.Value, 1);
    DT_A_P           := Geral.FDT(QrD500DT_A_P.Value, 1);
    VL_DOC           := QrD500VL_DOC.Value;
    VL_DESC          := QrD500VL_DESC.Value;
    VL_SERV          := QrD500VL_SERV.Value;
    VL_SERV_NT       := QrD500VL_SERV_NT.Value;
    VL_TERC          := QrD500VL_TERC.Value;
    VL_DA            := QrD500VL_DA.Value;
    VL_BC_ICMS       := QrD500VL_BC_ICMS.Value;
    VL_ICMS          := QrD500VL_ICMS.Value;
    COD_INF          := QrD500COD_INF.Value;
    VL_PIS           := QrD500VL_PIS.Value;
    VL_COFINS        := QrD500VL_COFINS.Value;
    Terceiro         := QrD500Entidade.Value;
    TP_ASSINANTE     := QrD500TP_ASSINANTE.Value;
    CST_ICMS         := QrD590CST_ICMS.Value;
    CFOP             := QrD590CFOP.Value;
    ALIQ_ICMS        := QrD590ALIQ_ICMS.Value;
    VL_RED_BC        := QrD590VL_RED_BC.Value;
    //
    LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_d500', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
      stIns, 0, siPositivo, nil);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efd_d500', False, [
    'REG', 'IND_OPER', 'IND_EMIT',
    'COD_PART', 'COD_MOD', 'COD_SIT',
    'SER', 'SUB',
    'NUM_DOC', 'DT_DOC', 'DT_A_P',
    'VL_DOC', 'VL_DESC', 'VL_SERV',
    'VL_SERV_NT', 'VL_TERC', 'VL_DA',
    'VL_BC_ICMS', 'VL_ICMS', 'COD_INF',
    'VL_PIS', 'VL_COFINS', 'TP_ASSINANTE',
    'Terceiro', 'Importado', 'CST_ICMS',
    'CFOP', 'ALIQ_ICMS', 'VL_RED_BC'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
    REG, IND_OPER, IND_EMIT,
    COD_PART, COD_MOD, COD_SIT,
    SER, SUB,
    NUM_DOC, DT_DOC, DT_A_P,
    VL_DOC, VL_DESC, VL_SERV,
    VL_SERV_NT, VL_TERC, VL_DA,
    VL_BC_ICMS, VL_ICMS, COD_INF,
    VL_PIS, VL_COFINS, TP_ASSINANTE,
    Terceiro, Importado, CST_ICMS,
    CFOP, ALIQ_ICMS, VL_RED_BC], [
    ImporExpor, AnoMes, Empresa, LinArq], True);
  end;
const
  Status = 2;
  LinArq = 1;
  REG = 'D001';
  IND_MOV = '0';
begin
  PB1.Position := 0;
  PB1.Max := QrD500.RecordCount;
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Verificando se o periodo existe');
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT *',
  'FROM efd_d001',
  'WHERE ImporExpor=' + ImporExpor,
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_Txt,
  '']);
  if Dmod.QrAux.RecordCount = 0 then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, StIns, 'efd_d001', False, [
    'LinArq', 'REG', 'IND_MOV'], [
    'ImporExpor', 'AnoMes', 'Empresa'], [
    LinArq, REG, IND_MOV], [
    ImporExpor, FAnoMes_Int, FEmpresa_Int], True);
  end;
  //
  QrD500.First;
  while not QrD500.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Incluindo registro ' +
      FormatFloat('0', QrD500.RecNo));
    CriaNF_D500();
    //
    QrD500.Next;
  end;
  IncrementaStatus(CO_Tabela_D001, 'Stat_D500i', Status);
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmSPED_EFD_Compara.DBGrid2DblClick(Sender: TObject);
begin
  if (QrPQE.State <> dsInactive) and (QrPQE.RecordCount > 0) then
  begin
    DModG.ReopenEmpresas(VAR_USUARIO, 0);
    if DBCheck.CriaFm(TFmPQE, FmPQE, afmoNegarComAviso) then
    begin
      FmPQE.LocCod(QrPQECodigo.Value, QrPQECodigo.Value);
      FmPQE.ShowModal;
      FmPQE.Destroy;
    end;
  end;
end;

procedure TFmSPED_EFD_Compara.DBGrid3DblClick(Sender: TObject);
begin
{ Parei aqui! Fazer MP?
  if (QrPQE.State <> dsInactive) and (QrPQE.RecordCount > 0) then
  begin
    DModG.ReopenEmpresas(VAR_USUARIO, 0);
    if DBCheck.CriaFm(TFmPQE, FmPQE, afmoNegarComAviso) then
    begin
      FmPQE.LocCod(QrPQECodigo.Value, QrPQECodigo.Value);
      FmPQE.ShowModal;
      FmPQE.Destroy;
    end;
  end;
}
end;

procedure TFmSPED_EFD_Compara.DBG_C100DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if Column.FieldName = 'ACHOU_TXT' then
  begin
    if QrC100FatNum.Value > 0 then
      Cor := clBlue
    else
      Cor := clRed;
    with DBG_C100.Canvas do
    begin
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmSPED_EFD_Compara.DefineDatasMinEMax(const MesesAMenos,
  MesesAMais: Integer; var DataMin, DataMax: String);
var
  Ano, Mes: Word;
  DataI, DataF: TDateTime;
begin
  Ano := Geral.IMV(Copy(FAnoMes_Txt, 1, 4));
  Mes := Geral.IMV(Copy(FAnoMes_Txt, 5, 2));
  DataI := EncodeDate(Ano, Mes, 1);
  DataF := IncMonth(DataI, 1) - 1;
  DataMin := Geral.FDT(IncMonth(DataI, - MesesAMenos), 1);
  DataMax := Geral.FDT(IncMonth(DataF, + MesesAMais), 1);
end;

procedure TFmSPED_EFD_Compara.DefineVariaveis();
begin
  FImporExpor_Int := 1;
  FImporExpor_Txt := FormatFloat('0', FImporExpor_Int);
  //
  FAnoMes_Int := EdAnoMes.ValueVariant;
  FAnoMes_Txt := FormatFloat('000000', FAnoMes_Int);
  //
  FEmpresa_Int := EdEntidade.ValueVariant;
  FEmpresa_Txt := FormatFloat('0', FEmpresa_Int);
  //
  QrC100.Close;
end;

procedure TFmSPED_EFD_Compara.EdAnoMesChange(Sender: TObject);
begin
  DefineVariaveis();
  ReopenTabelas();
end;

procedure TFmSPED_EFD_Compara.EdEntidadeChange(Sender: TObject);
begin
  DefineVariaveis();
end;

procedure TFmSPED_EFD_Compara.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DefineVariaveis();
end;

procedure TFmSPED_EFD_Compara.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl4.ActivePageIndex := 0;
  PageControl5.ActivePageIndex := 0;
  PageControl6.ActivePageIndex := 0;
end;

procedure TFmSPED_EFD_Compara.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

{
procedure TFmSPED_EFD_Compara.IncluiStqMovItsA_C170(Empresa, GraGruX: Integer);
const
  Tipo = VAR_FATID_0251;
  StqCenCad = 1;
var
  IDCtrl, OriCodi, OriCtrl: Integer;
  DataHora: String;
  Qtde, CustoAll: Double;
  Baixa, UnidMed: Integer;
begin
  OriCodi := QrC170AnoMes.Value;
  OriCtrl := QrC170LinArq.Value;
  IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
  //
  DataHora := Geral.FDT(QrC100DT_E_S.Value, 1);
  Qtde := QrC170QTD.Value;
  CustoAll := QrC170VL_ITEM.Value;
  UnidMed := DmProd.ObtemUnidMedDeSigla(QrC170UNID.Value);
  if QrC170IND_MOV.Value = '0' then // Baixa
    Baixa := 1
  else
    Baixa := 0;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
  'DataHora', 'Tipo', 'OriCodi',
  'OriCtrl', (*'OriCnta', 'OriPart',*)
  'Empresa', 'StqCenCad', 'GraGruX',
  'Qtde', (*'Pecas', 'Peso',
  'AreaM2', 'AreaP2', 'FatorClas',
  'QuemUsou', 'Retorno', 'ParTipo',
  'ParCodi', 'DebCtrl', 'SMIMultIns',*)
  'CustoAll', (*'ValorAll', 'GrupoBal',*)
  'Baixa', (*'AntQtde',*) 'UnidMed'], [
  'IDCtrl'], [
  DataHora, Tipo, OriCodi,
  OriCtrl, (*OriCnta, OriPart,*)
  Empresa, StqCenCad, GraGruX,
  Qtde, (*Pecas, Peso,
  AreaM2, AreaP2, FatorClas,
  QuemUsou, Retorno, ParTipo,
  ParCodi, DebCtrl, SMIMultIns,*)
  CustoAll, (*ValorAll, GrupoBal,*)
  Baixa, (*AntQtde,*) UnidMed], [
  IDCtrl], False);
end;
}

procedure TFmSPED_EFD_Compara.IncrementaStatus(Tabela, Campo: String;
  Status: Integer);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
  Campo], [
  'ImporExpor', 'AnoMes', 'Empresa'], [
  Status], [
  FImporExpor_Int, FAnoMes_Int, FEmpresa_Int], True);
  //
  ReopenTabelas();
end;

function TFmSPED_EFD_Compara.Localiza_EFD(): Boolean;
var
  ParTipo, FatID, FatNum: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocEFD, Dmod.MyDB, [
  'SELECT *  ',
  'FROM efd_c100 ',
  'WHERE ImporExpor <> 2 ',
  'AND AnoMes=' + FAnoMes_Txt,
  'AND Empresa=' + FEmpresa_Txt,
  'AND Terceiro=' + FormatFloat('0', QrC100Entidade.Value),
  'AND COD_MOD="' + QrC100COD_MOD.Value + '"',
  'AND SER="' + QrC100SER.Value + '"',
  'AND NUM_DOC=' + FormatFloat('0', QrC100NUM_DOC.Value),
  '']);
  //
  Result := QrLocEFD.RecordCount > 0;
  if Result then
  begin
    ParTipo := GRADE_TABS_PARTIPO_0007;
    FatID := VAR_FATID_0251;
    FatNum := QrLocEFDLinArq.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_Tabela_C100, False, [
    'ParTipo', 'FatID', 'FatNum'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
    ParTipo,
    FatID, FatNum], [
    QrC100ImporExpor.Value, QrC100AnoMes.Value,
    QrC100Empresa.Value, QrC100LinArq.Value], True);
  end;
end;

procedure TFmSPED_EFD_Compara.PMSeqAcoes_C100Popup(Sender: TObject);
begin
  C100_SeqAcoes_1.Enabled := QrC001Stat_C100.Value = 0;
  C100_SeqAcoes_2.Enabled := QrC001Stat_C100.Value = 1;
  C100_SeqAcoes_3.Enabled := QrC001Stat_C100.Value = 2;
  C100_SeqAcoes_4.Enabled := QrC001Stat_C100.Value = 3;
end;

procedure TFmSPED_EFD_Compara.PMSeqAcoes_C500Popup(Sender: TObject);
begin
  C500_SeqAcoes_1.Enabled := QrC001Stat_C500.Value = 0;
  C500_SeqAcoes_2.Enabled := QrC001Stat_C500.Value = 1;
end;

procedure TFmSPED_EFD_Compara.PMSeqAcoes_D100Popup(Sender: TObject);
begin
  D100_SeqAcoes_1.Enabled := QrD001Stat_D100i.Value = 0;
  D100_SeqAcoes_2.Enabled := QrD001Stat_D100i.Value = 1;
end;

procedure TFmSPED_EFD_Compara.PMSeqAcoes_D500Popup(Sender: TObject);
begin
  D500_SeqAcoes_1.Enabled := QrD001Stat_D500i.Value = 0;
  D500_SeqAcoes_2.Enabled := QrD001Stat_D500i.Value = 1;
end;

procedure TFmSPED_EFD_Compara.QrC001AfterScroll(DataSet: TDataSet);
begin
  BtSeqAcoesC100.Enabled := True;
  BtSeqAcoesC500.Enabled := True;
end;

procedure TFmSPED_EFD_Compara.QrC001BeforeClose(DataSet: TDataSet);
begin
  BtSeqAcoesC100.Enabled := False;
  BtSeqAcoesC500.Enabled := False;
end;

procedure TFmSPED_EFD_Compara.QrC100AfterOpen(DataSet: TDataSet);
begin
  BtEstatistica_0_0.Enabled := QrC100.RecordCount > 0;
  BtEstatistica_0_1.Enabled := QrC100.RecordCount > 0;
end;

procedure TFmSPED_EFD_Compara.QrC100AfterScroll(DataSet: TDataSet);
begin
  if CkFiltra_0_0.Checked then
    ReopenPQE();
  if CkFiltra_0_1.Checked then
    ReopenMPInIts();
  ReopenC170(0);
end;

procedure TFmSPED_EFD_Compara.QrC100BeforeClose(DataSet: TDataSet);
begin
  QrPQE.Close;
  QrNFsMPs.Close;
  QrC170.Close;
  BtEstatistica_0_0.Enabled := False;
  BtEstatistica_0_1.Enabled := False;
end;

procedure TFmSPED_EFD_Compara.QrC500BeforeClose(DataSet: TDataSet);
begin
  QrC590.Close;
end;

procedure TFmSPED_EFD_Compara.QrD001AfterScroll(DataSet: TDataSet);
begin
  BtSeqAcoesD100.Enabled := True;
  BtSeqAcoesD500.Enabled := True;
end;

procedure TFmSPED_EFD_Compara.QrD001BeforeClose(DataSet: TDataSet);
begin
  BtSeqAcoesD100.Enabled := False;
  BtSeqAcoesD500.Enabled := False;
end;

procedure TFmSPED_EFD_Compara.QrNFsMPsAfterOpen(DataSet: TDataSet);
begin
  BtExclui_0_1.Enabled := QrNFsMPs.RecordCount >= 2;
end;

procedure TFmSPED_EFD_Compara.QrPQEAfterOpen(DataSet: TDataSet);
begin
  BtExclui_0_0.Enabled := QrPQE.RecordCount >= 2;
end;

procedure TFmSPED_EFD_Compara.QrPQEBeforeClose(DataSet: TDataSet);
begin
  BtExclui_0_0.Enabled := False;
end;

procedure TFmSPED_EFD_Compara.QrSumMPICalcFields(DataSet: TDataSet);
begin
  if QrSumMPIPNF.Value = 0 then
    QrSumMPIPRECO_KG.Value := 0
  else
    QrSumMPIPRECO_KG.Value :=
    QrSumMPICMPValor.Value /
    QrSumMPIPNF.Value;
  //
  if QrSumMPIPecasNF.Value = 0 then
    QrSumMPIPRECO_PC.Value := 0
  else
    QrSumMPIPRECO_PC.Value :=
    QrSumMPICMPValor.Value /
    QrSumMPIPecasNF.Value;
end;

procedure TFmSPED_EFD_Compara.ReopenTabelas();
begin
  Screen.Cursor := crHourGlass;
  try
    QrC001.Close;
    QrC100.Close;
    QrC500.Close;
    QrC590.Close;
    //
    QrD001.Close;
    QrD100.Close;
    QrD190.Close;
    QrD500.Close;
    QrD590.Close;
    //
    if Length(EdAnoMes.Text) = 6 then
    begin
      // Cabe�alho Produtos
      UnDmkDAC_PF.AbreMySQLQuery0(QrC001, Dmod.MyDB, [
      'SELECT c001.* ',
      'FROM ' + CO_Tabela_C001 + ' c001 ',
      'WHERE c001.ImporExpor=' + FImporExpor_Txt,
      'AND c001.AnoMes=' + FAnoMes_Txt,
      'AND c001.Empresa=' + FEmpresa_Txt,
      '']);

      // Mercadorias
      UnDmkDAC_PF.AbreMySQLQuery0(QrC100, Dmod.MyDB, [
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
      '_0150.Entidade, IF(c100.IND_OPER=0, "E", "S") E_S,  ',
      'IF(c100.IND_EMIT=0, "P", "T") P_T, ',
      'IF(FatNum <> 0, "SIM", "N�O") ACHOU_TXT, c100.* ',
      'FROM ' + CO_Tabela_C100 + ' c100 ',
      'LEFT JOIN ' + CO_Tabela_0150 + ' _0150 ON _0150.COD_PART=c100.COD_PART ',
      '  AND _0150.ImporExpor=c100.ImporExpor ',
      '  AND _0150.AnoMes=c100.AnoMes ',
      '  AND _0150.Empresa=c100.Empresa ',
      'LEFT JOIN entidades ent ON ent.Codigo=_0150.Entidade ',
      'WHERE '(*c100.IND_EMIT=1 ',
      'AND ParTipo=0 AND ParCodi=0 ',
      'AND*) + ' c100.ImporExpor=' + FImporExpor_Txt,
      'AND c100.AnoMes=' + FAnoMes_Txt,
      'AND c100.Empresa=' + FEmpresa_Txt,
      '']);
      //
      if not CkFiltra_0_0.Checked then
        ReopenPQE();
      if not CkFiltra_0_1.Checked then
        ReopenMPInIts();

      //  Energia el�trica, �gua, luz!
      UnDmkDAC_PF.AbreMySQLQuery0(QrC500, Dmod.MyDB, [
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
      ' _0150.Entidade, c500.* ',
      'FROM ' + CO_Tabela_C500 + ' c500 ',
      'LEFT JOIN ' + CO_Tabela_0150 + ' _0150 ON _0150.COD_PART=c500.COD_PART ',
      '  AND _0150.ImporExpor=c500.ImporExpor ',
      '  AND _0150.AnoMes=c500.AnoMes ',
      '  AND _0150.Empresa=c500.Empresa ',
      'LEFT JOIN entidades ent ON ent.Codigo=_0150.Entidade ',
      'WHERE c500.IND_EMIT=1 ',
      'AND c500.ImporExpor=' + FImporExpor_Txt,
      'AND c500.AnoMes=' + FAnoMes_Txt,
      'AND c500.Empresa=' + FEmpresa_Txt,
      '']);
      UnDmkDAC_PF.AbreMySQLQuery0(QrC590, Dmod.MyDB, [
      'SELECT c590.* ',
      'FROM ' + CO_Tabela_C590 + ' c590 ',
      'WHERE c590.ImporExpor=' + FImporExpor_Txt,
      'AND c590.AnoMes=' + FAnoMes_Txt,
      'AND c590.Empresa=' + FEmpresa_Txt,
      'AND c590.C500=' + FormatFloat('0', QrC500LinArq.Value),
      '']);

      // Cabe�alho Servi�os
      UnDmkDAC_PF.AbreMySQLQuery0(QrD001, Dmod.MyDB, [
      'SELECT d001.* ',
      'FROM ' + CO_Tabela_D001 + ' d001 ',
      'WHERE d001.ImporExpor=' + FImporExpor_Txt,
      'AND d001.AnoMes=' + FAnoMes_Txt,
      'AND d001.Empresa=' + FEmpresa_Txt,
      '']);

      //  Conhecimentos de frete!
      UnDmkDAC_PF.AbreMySQLQuery0(QrD100, Dmod.MyDB, [
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
      ' _0150.Entidade, d100.* ',
      'FROM ' + CO_Tabela_D100 + ' d100 ',
      'LEFT JOIN ' + CO_Tabela_0150 + ' _0150 ON _0150.COD_PART=d100.COD_PART ',
      '  AND _0150.ImporExpor=d100.ImporExpor ',
      '  AND _0150.AnoMes=d100.AnoMes ',
      '  AND _0150.Empresa=d100.Empresa ',
      'LEFT JOIN entidades ent ON ent.Codigo=_0150.Entidade ',
      'WHERE d100.IND_EMIT=1 ',
      'AND d100.ImporExpor=' + FImporExpor_Txt,
      'AND d100.AnoMes=' + FAnoMes_Txt,
      'AND d100.Empresa=' + FEmpresa_Txt,
      '']);
      UnDmkDAC_PF.AbreMySQLQuery0(QrD190, Dmod.MyDB, [
      'SELECT d190.* ',
      'FROM ' + CO_Tabela_D190 + ' d190 ',
      'WHERE d190.ImporExpor=' + FImporExpor_Txt,
      'AND d190.AnoMes=' + FAnoMes_Txt,
      'AND d190.Empresa=' + FEmpresa_Txt,
      'AND d190.D100=' + FormatFloat('0', QrD100LinArq.Value),
      '']);
      //

      //  Comunica��o e Telecomunica��o!
      UnDmkDAC_PF.AbreMySQLQuery0(QrD500, Dmod.MyDB, [
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
      ' _0150.Entidade, d500.* ',
      'FROM ' + CO_Tabela_D500 + ' d500 ',
      'LEFT JOIN ' + CO_Tabela_0150 + ' _0150 ON _0150.COD_PART=d500.COD_PART ',
      '  AND _0150.ImporExpor=d500.ImporExpor ',
      '  AND _0150.AnoMes=d500.AnoMes ',
      '  AND _0150.Empresa=d500.Empresa ',
      'LEFT JOIN entidades ent ON ent.Codigo=_0150.Entidade ',
      'WHERE d500.IND_EMIT=1 ',
      'AND d500.ImporExpor=' + FImporExpor_Txt,
      'AND d500.AnoMes=' + FAnoMes_Txt,
      'AND d500.Empresa=' + FEmpresa_Txt,
      '']);
      UnDmkDAC_PF.AbreMySQLQuery0(QrD590, Dmod.MyDB, [
      'SELECT d590.* ',
      'FROM ' + CO_Tabela_D590 + ' d590 ',
      'WHERE d590.ImporExpor=' + FImporExpor_Txt,
      'AND d590.AnoMes=' + FAnoMes_Txt,
      'AND d590.Empresa=' + FEmpresa_Txt,
      'AND d590.D500=' + FormatFloat('0', QrD500LinArq.Value),
      '']);
      //

    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSPED_EFD_Compara.ReopenC170(LinArq: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrC170, Dmod.MyDB, [
  'SELECT c170.*  ',
  'FROM ' + CO_Tabela_C170 + ' c170 ',
  'WHERE c170.ImporExpor=' + FImporExpor_Txt,
  'AND c170.AnoMes=' + FAnoMes_Txt,
  'AND c170.Empresa=' + FEmpresa_Txt,
  'AND C170.C100=' + FormatFloat('0', QrC100LinArq.Value),
  '']);
  //
  QrC170.Locate('LinArq', LinArq, []);
end;

procedure TFmSPED_EFD_Compara.ReopenMPInIts();
var
  Texto, Liga: String;
  //Mostra: Boolean;
begin
  QrNFsMPs.Close;
  QrNFsMPs.SQL.Clear;
  QrNFsMPs.SQL.Add('SELECT ');
  QrNFsMPs.SQL.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN, ');
  QrNFsMPs.SQL.Add('mpi.Emissao,');
  QrNFsMPs.SQL.Add('mpi.NF, SUM(mpi.PecasNF) PecasNF, SUM(mpi.PNF) PNF,');
  QrNFsMPs.SQL.Add('SUM(mpi.PLE) PLE, SUM(mpi.CMPValor) CMPValor,');
  QrNFsMPs.SQL.Add('NF_Modelo, NF_Serie, CFOP,');
  QrNFsMPs.SQL.Add('IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece) MyForn');
  QrNFsMPs.SQL.Add('FROM mpinits mpi');
  QrNFsMPs.SQL.Add('LEFT JOIN mpin mpe ON mpe.Controle=mpi.Controle');
  QrNFsMPs.SQL.Add('LEFT JOIN entidades frn ON frn.Codigo=');
  QrNFsMPs.SQL.Add('  IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece)');
  QrNFsMPs.SQL.Add('WHERE mpi.EFD_INN_AnoMes = 0');
  QrNFsMPs.SQL.Add('AND mpi.EFD_INN_Empresa = 0');
  QrNFsMPs.SQL.Add('AND mpi.EFD_INN_LinArq = 0');
  QrNFsMPs.SQL.Add('AND mpe.ClienteI=' + FEmpresa_Txt);
  if CkFiltra_0_1.Checked then
  begin
    Texto := '';
    Liga := '';
    if CkFornecedor_0_1.Checked then
    begin
      Texto := Texto + Liga +
        'IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece)=' +
        FormatFloat('0', QrC100Entidade.Value) + #13#10;
      Liga := ' AND ';
    end;
    if CkNF_0_1.Checked then
    begin
      Texto := TeXto + Liga + 'mpi.NF=' + FormatFloat('0', QrC100NUM_DOC.Value) + #13#10;
      Liga := ' AND ';
    end;
    if CkValorDoc_0_1.Checked then
    begin
      Texto := TeXto + Liga + 'mpi.CMPValor=' + Geral.FFT_Dot(QrC100VL_DOC.Value, 2, siNegativo) + #13#10;
    end;
    if CkDiasAD_0_1.Checked then
    begin
      Texto := TeXto + Liga + dmkPF.SQL_Periodo('mpi.Emissao ',
      QrC100DT_DOC.Value - EdDiasA_0_1.ValueVariant,
      QrC100DT_DOC.Value + EdDiasD_0_1.ValueVariant, True, True);
      //Mostra := True;
    end (*else Mostra := False*);
    if Texto <> '' then
      Texto := 'AND (' + #13#10 + Texto + #13#10 + ')';
  end;
  QrNFsMPs.SQL.Add(Texto);
  QrNFsMPs.SQL.Add('GROUP BY mpi.NF, MyForn');
  QrNFsMPs.Open;
  BtRastreia_0_1.Enabled :=
    (QrC100.State <> dsInactive) and (QrC100.RecordCount > 0) and
    CkFiltra_0_1.Checked and
    (*CkFornecedor_0_1.Checked and*)
    CkNF_0_1.Checked (*and
    CkValorDoc_0_1.Checked*);
  //
  QrSumMPI.Close;
  QrSumMPI.SQL.Clear;
  QrSumMPI.SQL.Add('SELECT SUM(mpi.PecasNF) PecasNF, SUM(mpi.PNF) PNF,');
  QrSumMPI.SQL.Add('SUM(mpi.PLE) PLE, SUM(mpi.CMPValor) CMPValor');
  QrSumMPI.SQL.Add('FROM mpinits mpi');
  QrSumMPI.SQL.Add('LEFT JOIN mpin mpe ON mpe.Controle=mpi.Controle');
  QrSumMPI.SQL.Add('WHERE mpi.EFD_INN_AnoMes = 0');
  QrSumMPI.SQL.Add('AND mpi.EFD_INN_Empresa = 0');
  QrSumMPI.SQL.Add('AND mpi.EFD_INN_LinArq = 0');
  QrSumMPI.SQL.Add('AND mpe.ClienteI=' + FEmpresa_Txt);
  QrSumMPI.SQL.Add(Texto);
  QrSumMPI.Open;
  //
  QrMPInIts.Close;
  QrMPInIts.SQL.Clear;
  QrMPInIts.SQL.Add('SELECT IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,');
  QrMPInIts.SQL.Add('mpi.Emissao, mpi.Conta, mpi.NF, mpi.PecasNF,  mpi.PNF,');
  QrMPInIts.SQL.Add('mpi.PLE, mpi.CMPValor, mpi.NF_Modelo, mpi.NF_Serie, mpi.CFOP,');
  QrMPInIts.SQL.Add('IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece) MyForn, ');
  QrMPInIts.SQL.Add('mpi.Controle, mpe.Tipificacao, mpe.Animal ');
  QrMPInIts.SQL.Add('FROM mpinits mpi');
  QrMPInIts.SQL.Add('LEFT JOIN mpin mpe ON mpe.Controle=mpi.Controle');
  QrMPInIts.SQL.Add('LEFT JOIN entidades frn ON frn.Codigo=');
  QrMPInIts.SQL.Add('  IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece)');
  QrMPInIts.SQL.Add('WHERE mpi.EFD_INN_AnoMes = 0');
  QrMPInIts.SQL.Add('AND mpi.EFD_INN_Empresa = 0');
  QrMPInIts.SQL.Add('AND mpi.EFD_INN_LinArq = 0');
  QrMPInIts.SQL.Add('AND mpe.ClienteI=' + FEmpresa_Txt);
  QrMPInIts.SQL.Add(Texto);
  QrMPInIts.SQL.Add('ORDER BY mpe.Animal, mpe.Tipificacao');
  QrMPInIts.Open;
(*
  if Mostra then
    MLAGeral.LeMeuSQLy(QrMPInIts, '', nil, False, True);
*)
end;

procedure TFmSPED_EFD_Compara.ReopenPQE();
var
  Texto, Liga: String;
begin
  QrPQE.Close;
  QrPQE.SQL.Clear;
  QrPQE.SQL.Add('SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,');
  QrPQE.SQL.Add('pqe.Codigo, pqe.Data, pqe.IQ, pqe.modNF,');
  QrPQE.SQL.Add('pqe.Serie, pqe.NF, pqe.ValorNF');
  QrPQE.SQL.Add('FROM PQE pqe');
  QrPQE.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=pqe.IQ');
  QrPQE.SQL.Add('WHERE EFD_INN_AnoMes = 0');
  QrPQE.SQL.Add('AND EFD_INN_Empresa = 0');
  QrPQE.SQL.Add('AND EFD_INN_LinArq = 0');
  QrPQE.SQL.Add('AND pqe.CI=' + FEmpresa_Txt);
  if CkFiltra_0_0.Checked then
  begin
    Texto := '';
    Liga := '';
    if CkFornecedor_0_0.Checked then
    begin
      Texto := TeXto + Liga + 'IQ=' + FormatFloat('0', QrC100Entidade.Value) + #13#10;
      Liga := ' AND ';
    end;
    if CkNF_0_0.Checked then
    begin
      Texto := TeXto + Liga + 'NF=' + FormatFloat('0', QrC100NUM_DOC.Value) + #13#10;
      Liga := ' AND ';
    end;
    if CkValorDoc_0_0.Checked and CkValorMerc_0_0.Checked then
    begin
      Texto := TeXto + Liga + '(ValorNF=' + Geral.FFT_Dot(QrC100VL_DOC.Value, 2, siNegativo) +
      ' OR ValorNF=' + Geral.FFT_Dot(QrC100VL_MERC.Value, 2, siNegativo) + ')' + #13#10;
    end else
    if CkValorDoc_0_0.Checked then
    begin
      Texto := TeXto + Liga + 'ValorNF=' + Geral.FFT_Dot(QrC100VL_DOC.Value, 2, siNegativo) + #13#10;
    end else
    if CkValorMerc_0_0.Checked then
    begin
      Texto := TeXto + Liga + 'ValorNF=' + Geral.FFT_Dot(QrC100VL_MERC.Value, 2, siNegativo) + #13#10;
    end;
    if Texto <> '' then
    begin
      QrPQE.SQL.Add('AND (');
      QrPQE.SQL.Add(Texto);
      QrPQE.SQL.Add(')');
    end;
  end;
  QrPQE.SQL.Add('ORDER BY pqe.Data, pqe.NF');
  //MLAGeral.LeMeuSQLy(QrPQE, '', nil, False, True);
  QrPQE.Open;
  BtRastreia_0_0.Enabled :=
    (QrC100.State <> dsInactive) and (QrC100.RecordCount > 0) and
    //(QrPQE.State <> dsInactive) and (QrPQE.RecordCount > 0) and
    CkFiltra_0_0.Checked and
    CkFornecedor_0_0.Checked and
    CkNF_0_0.Checked and
    CkValorDoc_0_0.Checked and
    CkValorMerc_0_0.Checked;
end;

procedure TFmSPED_EFD_Compara.SpeedButton2Click(Sender: TObject);
begin
  ReopenTabelas();
end;

end.
