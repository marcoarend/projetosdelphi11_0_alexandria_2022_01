unit UnSPED_EFD_ICMS_IPI_PF_v03_0_2_a;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) dmkEdit, dmkRadioGroup,
  dmkMemo, AdvToolBar, dmkCheckGroup, UnDmkProcFunc, TypInfo, UnMyObjects,
  dmkEditDateTimePicker;

type
{
  TEstagioVS_SPED = (evsspedNone=0, evsspedEncerraVS=1, evsspedGeraSPED=2,
                     evsspedExportaSPED=3);
  TCorrApoFormaLctoPosNegSPED = (caflpnIndef=0, caflpnMovNormEsquecido=1,
                               caflpnDesfazimentoMov=2);
}
  TUnSPED_EFD_ICMS_IPI_PF_v03_0_2_a = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormEfdIcmsIpiE001();
    procedure MostraFormSPEDEFDEnce();
    procedure MostraFormSpedefdIcmsIpiBalancos(ImporExpor, AnoMes, Empresa,
              LinArq: Integer; Registro: String; Data: TDateTime);
    procedure MostraFormSpedEfdIcmsIpiClaProd(ImporExpor, AnoMes, Empresa,
              LinArq: Integer; Registro: String; Data: TDateTime);
    procedure MostraFormEfdIcmsIpiExporta();
  end;
var
  SPED_EFD_ICMS_IPI_PF_v03_0_2_a: TUnSPED_EFD_ICMS_IPI_PF_v03_0_2_a;

implementation

uses UnMLAGeral, MyDBCheck, DmkDAC_PF, Module, ModuleGeral, UnDmkWeb, Restaura2,
  UMySQLModule, UnGrade_PF,
///////////////////////////     SPED ICMS IPI    ///////////////////////////////
  EfdIcmsIpiExporta_v03_0_2_a, EfdIcmsIpiE001_v03_0_2_a, EfdIcmsIpiEnce_v03_0_2_a,
  SpedEfdIcmsipiBalancos_v03_0_2_a, SpedEfdIcmsIpiClaProd_v03_0_2_a,
///////////////////////////     SPED ICMS IPI    ///////////////////////////////
  ModAppGraG1, ModVS;


{ TUnSPED_EFD_ICMS_IPI_PF_v03_0_2_a }

procedure TUnSPED_EFD_ICMS_IPI_PF_v03_0_2_a.MostraFormEfdIcmsIpiE001();
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiE001_v03_0_2_a, FmEfdIcmsIpiE001_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiE001_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiE001_v03_0_2_a.Destroy;
  end;
end;

procedure TUnSPED_EFD_ICMS_IPI_PF_v03_0_2_a.MostraFormSPEDEFDEnce();
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiEnce_v03_0_2_a, FmEfdIcmsIpiEnce_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiEnce_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiEnce_v03_0_2_a.Destroy;
  end;
end;

procedure TUnSPED_EFD_ICMS_IPI_PF_v03_0_2_a.MostraFormSpedefdIcmsIpiBalancos(
  ImporExpor, AnoMes, Empresa, LinArq: Integer; Registro: String;
  Data: TDateTime);
var
  Ano, Mes: Word;
begin
  if DBCheck.CriaFm(TFmSpedefdIcmsIpiBalancos_v03_0_2_a, FmSpedefdIcmsIpiBalancos_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmSpedefdIcmsIpiBalancos_v03_0_2_a.FImporExpor := ImporExpor;
    FmSpedefdIcmsIpiBalancos_v03_0_2_a.FAnoMes     := AnoMes;
    FmSpedefdIcmsIpiBalancos_v03_0_2_a.FEmpresa    := Empresa;
    FmSpedefdIcmsIpiBalancos_v03_0_2_a.FRegPai     := LinArq;
    FmSpedefdIcmsIpiBalancos_v03_0_2_a.FData       := Data;
    //
    dmkPF.AnoMesDecode(AnoMes, Ano, Mes);
    FmSpedefdIcmsIpiBalancos_v03_0_2_a.CBMes.ItemIndex := Mes - 1;
    FmSpedefdIcmsIpiBalancos_v03_0_2_a.CBAno.Text      := Geral.FF0(Ano);
    FmSpedefdIcmsIpiBalancos_v03_0_2_a.TPData.Date     := Data;
    //
    if Registro = 'H010' then
    begin
      FmSpedefdIcmsIpiBalancos_v03_0_2_a.PCRegistro.ActivePageIndex := 1;
      FmSpedefdIcmsIpiBalancos_v03_0_2_a.LaData.Visible             := False;
      FmSpedefdIcmsIpiBalancos_v03_0_2_a.TPData.Visible             := False;
      FmSpedefdIcmsIpiBalancos_v03_0_2_a.PnPeriodo.Enabled          := True;
    end else
    if Registro = 'K200' then
    begin
      FmSpedefdIcmsIpiBalancos_v03_0_2_a.PCRegistro.ActivePageIndex := 2;
      FmSpedefdIcmsIpiBalancos_v03_0_2_a.LaData.Visible             := True;
      FmSpedefdIcmsIpiBalancos_v03_0_2_a.TPData.Visible             := True;
      FmSpedefdIcmsIpiBalancos_v03_0_2_a.PnPeriodo.Enabled          := False;
    end else
      FmSpedefdIcmsIpiBalancos_v03_0_2_a.PCRegistro.ActivePageIndex := 0;
    //
    if FmSpedefdIcmsIpiBalancos_v03_0_2_a.PCRegistro.ActivePageIndex > 0 then
      FmSpedefdIcmsIpiBalancos_v03_0_2_a.ShowModal
    else
      Geral.MB_Erro('Registro n�o implementdo em balan�o de SPED EFD: ' +
      Registro);
    FmSpedefdIcmsIpiBalancos_v03_0_2_a.Destroy;
  end;
end;

procedure TUnSPED_EFD_ICMS_IPI_PF_v03_0_2_a.MostraFormSpedEfdIcmsIpiClaProd(
  ImporExpor, AnoMes, Empresa, LinArq: Integer; Registro: String;
  Data: TDateTime);
var
  Ano, Mes: Word;
begin
  if DBCheck.CriaFm(TFmSpedEfdIcmsIpiClaProd_v03_0_2_a, FmSpedEfdIcmsIpiClaProd_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.FImporExpor := ImporExpor;
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.FAnoMes     := AnoMes;
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.FEmpresa    := Empresa;
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.FPeriApu    := LinArq;
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.FData       := Data;
    //
    dmkPF.AnoMesDecode(AnoMes, Ano, Mes);
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.CBMes.ItemIndex := Mes - 1;
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.CBAno.Text      := Geral.FF0(Ano);
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.TPData.Date     := Data;
    //
    //
    if Registro = 'K220' then
    begin
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PCRegistro.ActivePageIndex := 1;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.LaData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.TPData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PnPeriodo.Enabled          := True;
    end else
    if Registro = 'K230' then
    begin
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PCRegistro.ActivePageIndex := 2;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.LaData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.TPData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PnPeriodo.Enabled          := True;
    end else
(*
    if Registro = 'K200' then
    begin
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PCRegistro.ActivePageIndex := 2;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.LaData.Visible             := True;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.TPData.Visible             := True;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PnPeriodo.Enabled          := False;
    end else
*)
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PCRegistro.ActivePageIndex := 0;
    //
    if FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PCRegistro.ActivePageIndex > 0 then
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.ShowModal
    else
      Geral.MB_Erro('Registro n�o implementdo em SPED EFD K2XX: ' +
      Registro);
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.Destroy;
  end;
end;

procedure TUnSPED_EFD_ICMS_IPI_PF_v03_0_2_a.MostraFormEfdIcmsIpiExporta();
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiExporta_v03_0_2_a, FmEfdIcmsIpiExporta_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiExporta_v03_0_2_a.EdEmpresa.ValueVariant := 1;
    FmEfdIcmsIpiExporta_v03_0_2_a.CBEmpresa.KeyValue := 1;
    FmEfdIcmsIpiExporta_v03_0_2_a.RGCOD_FIN.ItemIndex := 1;
    FmEfdIcmsIpiExporta_v03_0_2_a.RGPreenche.ItemIndex := 2;
    FmEfdIcmsIpiExporta_v03_0_2_a.EdMes.ValueVariant := Date - 21;
    FmEfdIcmsIpiExporta_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiExporta_v03_0_2_a.Destroy;
  end;
end;

end.
