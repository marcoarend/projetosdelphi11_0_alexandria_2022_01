unit EfdIcmsIpiK250_v03_0_2_a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup, dmkEditCalc, UnDmkProcFunc,
  UnEfdIcmsIpi_PF, UnAppEnums;

type
  TFmEfdIcmsIpiK250_v03_0_2_a = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdPeriApu: TdmkEdit;
    Label6: TLabel;
    EdKndTab: TdmkEdit;
    Label9: TLabel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXEx_TIPI: TWideStringField;
    DsGraGruX: TDataSource;
    Panel5: TPanel;
    EdGraGruX: TdmkEditCB;
    Label232: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    StaticText2: TStaticText;
    EdQTD: TdmkEdit;
    EdKndCod: TdmkEdit;
    Label7: TLabel;
    EdKndNSU: TdmkEdit;
    Label8: TLabel;
    EdKndItm: TdmkEdit;
    Label10: TLabel;
    EdKndAID: TdmkEdit;
    Label1: TLabel;
    EdKndNiv: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    EdIDSeq1: TdmkEdit;
    QrGraGruXGrandeza: TIntegerField;
    QrGraGruXTipo_Item: TIntegerField;
    TPDT_PROD: TdmkEditDateTimePicker;
    DBText1: TDBText;
    StaticText1: TStaticText;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdKndTabRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGruX();
  public
    { Public declarations }
    FID_SEK, FOriOpeProc, FOrigemIDKnd, FMovimCod: Integer;
  end;

  var
  FmEfdIcmsIpiK250_v03_0_2_a: TFmEfdIcmsIpiK250_v03_0_2_a;

implementation

uses UnMyObjects, Module, (*EfdIcmsIpiE001,*) UMySQLModule, DmkDAC_PF, MyListas,
  ModuleFin, UnFinanceiro, SpedEfdIcmsIpi_v03_0_2_a;

{$R *.DFM}

procedure TFmEfdIcmsIpiK250_v03_0_2_a.BtOKClick(Sender: TObject);
var
  DT_PROD, COD_ITEM, COD_DOC_OP: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq1, ID_SEK, GraGruX, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  DataProd: TDateTime;
  ComOP: Boolean;
begin
//N�O permitir informar OP quando n�o existir IME-C
  ComOP          := EfdIcmsIpi_PF.HabilitaDocOP(KndTab);
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  PeriApu        := EdPeriApu.ValueVariant;
  if SQLType = stIns then
  begin
    KndTab         := Integer(TEstqSPEDTabSorc.estsNoSrc);
    KndCod         := PeriApu;
    KndNSU         := PeriApu;
    KndItm         := 0; // Abaixo
    KndAID         := Integer(TEstqMovimID.emidAjuste);
    KndNiv         := Integer(TEstqMovimNiv.eminSemNiv);
    IDSeq1         := 0; // Abaixo
    ID_SEK         := Integer(TSPED_EFD_ComoLancou.seclLancadoManual);
    OriOpeProc     := Integer(TOrigemOpeProc.oopND);
    //OrigemIDKnd    := Integer(TOrigemIDKnd.oidk000ND);
  end else
  begin
    KndTab         := EdKndTab.ValueVariant;
    KndCod         := EdKndCod.ValueVariant;
    KndNSU         := EdKndNSU.ValueVariant;
    KndItm         := EdKndItm.ValueVariant;
    KndAID         := EdKndAID.ValueVariant;
    KndNiv         := EdKndNiv.ValueVariant;
    IDSeq1         := EdIDSeq1.ValueVariant;
    ID_SEK         := FID_SEK;
    OriOpeProc     := FOriOpeProc;
    //OrigemIDKnd    := FOrigemIDKnd;
  end;
  DataProd       := Trunc(TPDT_PROD.Date);
  GraGruX        := EdGraGruX.ValueVariant;
  DT_PROD        := Geral.FDT(DataProd, 1);
  COD_ITEM       := dmkPF.ITS_Null(GraGruX);
  QTD            := EdQTD.ValueVariant;
  COD_DOC_OP     := dmkPF.ITS_Null(FMovimCod);
  //
  if MyObjects.FIC(ComOP and (TPDT_PROD.Date < 2), TPDT_PROD,
    'Informe a data inicial da Ordem de Servi�o! (IME-C)') then Exit;
(*
  if MyObjects.FIC(ComOP and (MovimCod = 0), EdMovimCod,
    'Informe o c�digo da Ordem de Servi�o! (IME-C)') then Exit;
*)
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o c�digo do produto!') then
    Exit;
  //ID_SEK         := ;
  //
  (*Campo 01 (REG) - Valor V�lido: [K250]*)
  (*Campo 02 (DT_PROD) - Valida��o: a data deve estar compreendida no per�odo
  informado nos campos DT_INI e DT_FIN*)

  //if not EfdIcmsIpi_PF.ValidaDataInicialDentroDoAnoMesSPED(AnoMes, DataProd,
  if not EfdIcmsIpi_PF.ValidaDataUnicaDentroDoAnoMesSPED(AnoMes, DataProd,
  COD_DOC_OP) then Exit;


  (*Campo 03 (COD_ITEM) � Valida��es:
    a) o c�digo do item produzido dever� existir no campo COD_ITEM do Registro
       0200;
    b) o TIPO_ITEM do Registro 0200 deve ser igual a 03 � Produto em Processo ou
       04 � Produto Acabado.*)

  if not EfdIcmsIpi_PF.ValidaGGXEstahDentroDoAnoMesSPED(AnoMes, GraGruX,
  COD_DOC_OP) then Exit;

  (*Campo 04 (QTD) - Preenchimento: a quantidade produzida deve considerar a
  quantidade que foi recebida do terceiro e a varia��o de estoque ocorrida em
  terceiro. Cada legisla��o estadual prev� situa��es espec�ficas. Consulte a
  Secretaria de Fazenda/Tributa��o de seu estado. N�o � admitida quantidade
  negativa.*)

  if not EfdIcmsIpi_PF.ValidaExigenciaValorMaiorQueZeroOuZero(AnoMes, DataProd,
  DataProd, QTD, COD_DOC_OP) then Exit;
  //

  KndItm := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik250', 'KndItm', [
  'ImporExpor', 'AnoMes', 'KndTab'], [ImporExpor, AnoMes, KndTab],
  SQLType, KndItm, siPositivo, EdKndItm);
  //
  IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik250', 'IDSeq1', [
  'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
  ImporExpor, AnoMes, Empresa, PeriApu],
  SQLType, IDSeq1, siPositivo, EdIDSeq1);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdicmsipik250', False, [
  'KndAID', 'KndNiv', 'IDSeq1',
  'ID_SEK', 'DT_PROD', 'COD_ITEM',
  'QTD', 'COD_DOC_OP', 'OriOpeProc',
  'GraGruX'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'], [
  KndAID, KndNiv, IDSeq1,
  ID_SEK, DT_PROD, COD_ITEM,
  QTD, COD_DOC_OP, OriOpeProc,
  GraGruX], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True) then
  begin
    DfSEII_v03_0_2_a.ReopenK250(IDSeq1);
    Close;
  end;
end;

procedure TFmEfdIcmsIpiK250_v03_0_2_a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpiK250_v03_0_2_a.EdKndTabRedefinido(Sender: TObject);
begin
  //EfdIcmsIpi_PF.HabilitaMovimCodPorKndTab(Self, EdKndTab.ValueVariant,
  //[LaDT_INI_OP, TPDT_INI_OP, LaMovimCod, EdMovimCod]);
end;

procedure TFmEfdIcmsIpiK250_v03_0_2_a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiK250_v03_0_2_a.FormCreate(Sender: TObject);
begin
  TPDT_PROD.Date := 0;
  FMovimCod      := 0;
  ReopenGraGruX();
end;

procedure TFmEfdIcmsIpiK250_v03_0_2_a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdIcmsIpiK250_v03_0_2_a.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
begin
  SQL_AND := '';
  SQL_LFT := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  SQL_LFT,
  'WHERE ggx.Controle > 0 ',
  SQL_AND,
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

end.
