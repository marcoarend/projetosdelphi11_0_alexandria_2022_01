unit EfdIcmsIpiK270_v03_0_2_a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup, dmkEditCalc, UnDmkProcFunc,
  UnEfdIcmsIpi_PF, UnAppPF, UnAppEnums;

type
  TFmEfdIcmsIpiK270_v03_0_2_a = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdPeriApu: TdmkEdit;
    Label6: TLabel;
    EdKndTab: TdmkEdit;
    Label9: TLabel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXEx_TIPI: TWideStringField;
    DsGraGruX: TDataSource;
    Panel5: TPanel;
    EdGraGruX: TdmkEditCB;
    Label232: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    StaticText2: TStaticText;
    EdQTD_COR_POS: TdmkEdit;
    EdKndCod: TdmkEdit;
    Label7: TLabel;
    EdKndNSU: TdmkEdit;
    Label8: TLabel;
    EdKndItm: TdmkEdit;
    Label10: TLabel;
    EdKndAID: TdmkEdit;
    Label1: TLabel;
    EdKndNiv: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    EdIDSeq1: TdmkEdit;
    QrGraGruXGrandeza: TIntegerField;
    QrGraGruXTipo_Item: TIntegerField;
    TPDT_INI_AP: TdmkEditDateTimePicker;
    LaDT_INI_AP: TLabel;
    Label13: TLabel;
    TPDT_FIN_AP: TdmkEditDateTimePicker;
    LaMovimCod: TLabel;
    EdMovimCod: TdmkEdit;
    EdTxt: TdmkEdit;
    DBText1: TDBText;
    EdOrigemIDKnd: TdmkEdit;
    LaOrigemIDKnd: TLabel;
    Label14: TLabel;
    EdOriSPEDEFDKnd: TdmkEdit;
    EdRegisPai: TdmkEdit;
    LaRegisPai: TLabel;
    StaticText1: TStaticText;
    EdQTD_COR_NEG: TdmkEdit;
    DBText2: TDBText;
    RGORIGEM: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdMovimCodRedefinido(Sender: TObject);
    procedure EdMovimCodChange(Sender: TObject);
    procedure EdKndTabRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGruX();
  public
    { Public declarations }
    FID_SEK, FOriOpeProc, FOrigemIDKnd: Integer;
    FNumRegPai: Integer;
  end;

  var
  FmEfdIcmsIpiK270_v03_0_2_a: TFmEfdIcmsIpiK270_v03_0_2_a;

implementation

uses UnMyObjects, Module, (*EfdIcmsIpiE001,*) UMySQLModule, DmkDAC_PF, MyListas,
  ModuleFin, UnFinanceiro, SpedEfdIcmsIpi_v03_0_2_a;

{$R *.DFM}

procedure TFmEfdIcmsIpiK270_v03_0_2_a.BtOKClick(Sender: TObject);
var
  DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM, ORIGEM, RegisPai: String;
  ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1, KndTab, KndCod, KndNSU, KndItm,
  KndAID, KndNiv, GraGruX, OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd, ID_SEK,
  MovimCod: Integer;
  QTD_COR_POS, QTD_COR_NEG: Double;
  SQLType: TSQLType;
  DataIni, DataFim: TDateTime;
  ComOP: Boolean;
  NewReg: String;
  NewNumReg: Integer;
  //
  procedure ReopenTab(NumReg: Integer);
  begin
    case NumReg of
      210: DfSEII_v03_0_2_a.ReopenK270_210(IDSeq1);
      220: DfSEII_v03_0_2_a.ReopenK270_220(IDSeq1);
      230: DfSEII_v03_0_2_a.ReopenK270_230(IDSeq1);
      250: DfSEII_v03_0_2_a.ReopenK270_250(IDSeq1);
      260: DfSEII_v03_0_2_a.ReopenK270_260(IDSeq1);
      291: DfSEII_v03_0_2_a.ReopenK270_291(IDSeq1);
      292: DfSEII_v03_0_2_a.ReopenK270_292(IDSeq1);
      301: DfSEII_v03_0_2_a.ReopenK270_301(IDSeq1);
      302: DfSEII_v03_0_2_a.ReopenK270_302(IDSeq1);
    end;
  end;
begin
//N�O permitir informar OP quando n�o existir IME-C
  ComOP          := EfdIcmsIpi_PF.HabilitaDocOP(KndTab);
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  PeriApu        := EdPeriApu.ValueVariant;
  KndTab         := EdKndTab.ValueVariant;
  if SQLType = stIns then
  begin
    //KndTab         := Integer(TEstqSPEDTabSorc.estsNoSrc);
    KndCod         := PeriApu;
    KndNSU         := PeriApu;
    KndItm         := 0; // Abaixo
    KndAID         := Integer(TEstqMovimID.emidAjuste);
    KndNiv         := Integer(TEstqMovimNiv.eminSemNiv);
    IDSeq1         := 0; // Abaixo
    ID_SEK         := Integer(TSPED_EFD_ComoLancou.seclLancadoManual);
    OriOpeProc     := Integer(TOrigemOpeProc.oopND);
    //OrigemIDKnd    := Integer(TOrigemIDKnd.oidk000ND);
    OrigemIDKnd    := 0;
    OriSPEDEFDKnd  := 0;
    RegisPai       := '';
  end else
  begin
    //KndTab         := EdKndTab.ValueVariant;
    KndCod         := EdKndCod.ValueVariant;
    KndNSU         := EdKndNSU.ValueVariant;
    KndItm         := EdKndItm.ValueVariant;
    KndAID         := EdKndAID.ValueVariant;
    KndNiv         := EdKndNiv.ValueVariant;
    IDSeq1         := EdIDSeq1.ValueVariant;
    ID_SEK         := FID_SEK;
    OriOpeProc     := FOriOpeProc;
    //OrigemIDKnd    := FOrigemIDKnd;
    OrigemIDKnd    := EdOrigemIDKnd.ValueVariant;
    OriSPEDEFDKnd  := EdOriSPEDEFDKnd.ValueVariant;
    RegisPai       := EdRegisPai.ValueVariant;
  end;
  GraGruX        := EdGraGruX.ValueVariant;
  DataIni        := Trunc(TPDT_INI_AP.Date);
  DataFim        := Trunc(TPDT_FIN_AP.Date);
  MovimCod       := EdMovimCod.ValueVariant;
  //
  DT_INI_AP      := Geral.FDT(DataIni, 1);
  DT_FIN_AP      := Geral.FDT(DataFim, 1);
  COD_OP_OS      := dmkPF.ITS_Null(MovimCod);
  COD_ITEM       := dmkPF.ITS_Null(GraGruX);
  QTD_COR_POS    := EdQTD_COR_POS.ValueVariant;
  QTD_COR_NEG    := EdQTD_COR_NEG.ValueVariant;
  ORIGEM         := Geral.FF0(RGORIGEM.ItemIndex + 1);
  //
  if MyObjects.FIC(ComOP and (TPDT_INI_AP.Date < 2), TPDT_INI_AP,
    'Informe a data inicial da Ordem de Servi�o! (IME-C)') then Exit;
  if MyObjects.FIC(ComOP and (MovimCod = 0), EdMovimCod,
    'Informe o c�digo da Ordem de Servi�o! (IME-C)') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o c�digo do produto!') then
    Exit;
  if MyObjects.FIC(RGORIGEM.ItemIndex < 0, RGORIGEM, 'Informe o registro de ORIGEM!') then
    Exit;
  //ID_SEK         := ;
  //
  (*Campo 01 (REG) - Valor V�lido: [K270]*)
  (*Campos 02 e 03 (DT_INI_AP e DT_FIN_AP) � Preenchimento: estes campos poder�o
   n�o ser preenchidos somente na hip�tese em que o campo 4 (COD_OP_OS), na
   corre��o de apontamento, se referir:
   a) a uma ordem de produ��o que esteja em aberto (DT_FIN_OP do Registro K230
      em branco) com o campo 08 (ORIGEM) do registro K270 igual a 1, no presente
      per�odo de apura��o do K100 ou em per�odo de apura��o imediatamente
      anterior ao presente per�odo de apura��o do K100;
   b) a uma ordem de servi�o que esteja em aberto (DT_FIN_OS do Registro K210 em
      branco) com o campo 08 (ORIGEM) do registro K270 igual a 3, no presente
      per�odo de apura��o do K100 ou em per�odo de apura��o imediatamente
      anterior ao presente per�odo de apura��o do K100;
   c) a uma ordem de produ��o ou ordem de servi�o que esteja em aberto
      (COD_OP_OS do Registro K260 em branco) com o  campo 08 (ORIGEM) do
      registro K270 igual a 4, no presente per�odo de apura��o do K100 ou em
      per�odo de apura��o imediatamente anterior ao presente per�odo de apura��o
      do K100.
   Valida��o: a data inicial e a data final t�m de ser anteriores � data inicial
   do per�odo informado no Registro 0000.*)

  if not EfdIcmsIpi_PF.ValidaDataInicialDentroDoAnoMesSPED(AnoMes, DataIni,
    DataFim, COD_OP_OS) then Exit;

  if not EfdIcmsIpi_PF.ValidaDataFinalDentroDoAnoMesSPED(AnoMes, DataIni,
  DataFim, COD_OP_OS) then Exit;

  (*Campo 04 *)

  if not EfdIcmsIpi_PF.ValidaOSDentroDoAnoMesSPED(AnoMes, DataIni,
  DataFim, COD_OP_OS) then Exit;

  (*Campo 05 (COD_ITEM) � Valida��o: o c�digo do item produzido que est� sendo
  corrigido dever� existir no campo COD_ITEM do Registro 0200.*)

  if not EfdIcmsIpi_PF.ValidaGGXEstahDentroDoAnoMesSPED(AnoMes, GraGruX,
  COD_OP_OS) then Exit;

  (*Campos 06 e 07 (QTD_COR_POS e QTD_COR_NEG) � Valida��o: n�o � admitida
  quantidade negativa.
  Valida��o: somente um dos campos pode ser preenchido.*)

  if not EfdIcmsIpi_PF.ValidaExigenciaValorMaiorQueZeroOuZero(AnoMes, DataIni,
  DataFim, QTD_COR_POS, COD_OP_OS) then Exit;
  //
  if not EfdIcmsIpi_PF.ValidaExigenciaValorMaiorQueZeroOuZero(AnoMes, DataIni,
  DataFim, QTD_COR_NEG, COD_OP_OS) then Exit;
  //
  if MyObjects.FIC((QTD_COR_POS <> 0) and (QTD_COR_NEG <> 0), EdQTD_COR_POS,
  'Somente um dos campos de quantidade pode ser preenchido!') then Exit;

  KndItm := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik270', 'KndItm', [
  'ImporExpor', 'AnoMes', 'KndTab'], [ImporExpor, AnoMes, KndTab],
  SQLType, KndItm, siPositivo, EdKndItm);
  //
  IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik270', 'IDSeq1', [
  'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
  ImporExpor, AnoMes, Empresa, PeriApu],
  SQLType, IDSeq1, siPositivo, EdIDSeq1);
  //
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdicmsipik270', False, [
  'IDSeq1', 'KndAID', 'KndNiv',
  'DT_INI_AP', 'DT_FIN_AP', 'COD_OP_OS',
  'COD_ITEM', 'QTD_COR_POS', 'QTD_COR_NEG',
  'OriOpeProc', 'OrigemIDKnd', 'OriSPEDEFDKnd',
  'RegisPai', 'ID_SEK',
  'ORIGEM', 'GraGruX'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'], [
  IDSeq1, KndAID, KndNiv,
  DT_INI_AP, DT_FIN_AP, COD_OP_OS,
  COD_ITEM, QTD_COR_POS, QTD_COR_NEG,
  OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd,
  RegisPai, ID_SEK,
  ORIGEM, GraGruX], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True) then
  begin
    if SQLType = stUpd then
      ReopenTab(FNumRegPai);
    if EfdIcmsIpi_PF.ObtemSPED_EFD_REGdeKndRegOrigem(TSPED_EFD_KndRegOrigem(
    Geral.IMV(Origem)), NewREG)
    then
    begin
      NewNumReg := Geral.IMV(Geral.SoNumero_TT(NewREG));
      if (SQLType = stIns) or (NewNumReg <> FNumRegPai) then
        ReopenTab(NewNumReg);
    end;
    Close;
  end;
end;

procedure TFmEfdIcmsIpiK270_v03_0_2_a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpiK270_v03_0_2_a.EdKndTabRedefinido(Sender: TObject);
begin
  EfdIcmsIpi_PF.HabilitaMovimCodPorKndTab(Self, EdKndTab.ValueVariant,
  [LaDT_INI_AP, TPDT_INI_AP, LaMovimCod, EdMovimCod]);
end;

procedure TFmEfdIcmsIpiK270_v03_0_2_a.EdMovimCodChange(Sender: TObject);
begin
  EdTxt.Text := '';
end;

procedure TFmEfdIcmsIpiK270_v03_0_2_a.EdMovimCodRedefinido(Sender: TObject);
begin
  AppPF.VerificaSeExisteIMEC(EdTxt, EdMovimCod.ValueVariant);
end;

procedure TFmEfdIcmsIpiK270_v03_0_2_a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiK270_v03_0_2_a.FormCreate(Sender: TObject);
begin
  TPDT_INI_AP.Date := 0;
  TPDT_FIN_AP.Date := 0;
  ReopenGraGruX();
end;

procedure TFmEfdIcmsIpiK270_v03_0_2_a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdIcmsIpiK270_v03_0_2_a.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
begin
  SQL_AND := '';
  SQL_LFT := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  SQL_LFT,
  'WHERE ggx.Controle > 0 ',
  SQL_AND,
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

end.
