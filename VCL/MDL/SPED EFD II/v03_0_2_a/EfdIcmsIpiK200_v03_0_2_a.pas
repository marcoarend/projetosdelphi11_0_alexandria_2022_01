unit EfdIcmsIpiK200_v03_0_2_a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup, dmkEditCalc, UnDmkProcFunc,
  UnEfdIcmsIpi_PF, UnAppEnums;

type
  TFmEfdIcmsIpiK200_v03_0_2_a = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdPeriApu: TdmkEdit;
    Label6: TLabel;
    EdKndTab: TdmkEdit;
    Label9: TLabel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXEx_TIPI: TWideStringField;
    DsGraGruX: TDataSource;
    Panel5: TPanel;
    EdGraGruX: TdmkEditCB;
    Label232: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    StaticText2: TStaticText;
    EdQTD: TdmkEdit;
    RGIND_EST: TdmkRadioGroup;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    DsEntidades: TDataSource;
    LaCOD_PART: TLabel;
    EdCOD_PART: TdmkEditCB;
    CBCOD_PART: TdmkDBLookupComboBox;
    EdKndCod: TdmkEdit;
    Label7: TLabel;
    EdKndNSU: TdmkEdit;
    Label8: TLabel;
    EdKndItm: TdmkEdit;
    Label10: TLabel;
    EdKndAID: TdmkEdit;
    Label1: TLabel;
    EdKndNiv: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    EdIDSeq1: TdmkEdit;
    TPDT_EST: TdmkEditDateTimePicker;
    Label2: TLabel;
    QrGraGruXGrandeza: TIntegerField;
    QrGraGruXTipo_Item: TIntegerField;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    EdPecas: TdmkEdit;
    LaPecas: TLabel;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    EdAreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    DBText1: TDBText;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGIND_ESTClick(Sender: TObject);
    procedure EdQTDChange(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdQTDRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGruX();
    procedure InfoExtraQTD();
  public
    { Public declarations }
    FID_SEK: Integer;
  end;

  var
  FmEfdIcmsIpiK200_v03_0_2_a: TFmEfdIcmsIpiK200_v03_0_2_a;

implementation

uses UnMyObjects, Module, (*EfdIcmsIpiE001,*) UMySQLModule, DmkDAC_PF, MyListas,
  ModuleFin, UnFinanceiro, SpedEfdIcmsIpi_v03_0_2_a;

{$R *.DFM}

procedure TFmEfdIcmsIpiK200_v03_0_2_a.BtOKClick(Sender: TObject);
const
  COD_DOC_OS = '';
var
  DT_EST, COD_ITEM, IND_EST, COD_PART: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq1, ID_SEK, GraGruX, Grandeza, Entidade, Tipo_Item: Integer;
  QTD, Pecas, AreaM2, PesoKg: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  PeriApu        := EdPeriApu.ValueVariant;
  if SQLType = stIns then
  begin
    KndTab         := Integer(TEstqSPEDTabSorc.estsNoSrc);
    KndCod         := PeriApu;
    KndNSU         := PeriApu;
    KndItm         := 0; // Abaixo
    KndAID         := Integer(TEstqMovimID.emidAjuste);
    KndNiv         := Integer(TEstqMovimNiv.eminSemNiv);
    IDSeq1         := 0; // Abaixo
    ID_SEK         := Integer(TSPED_EFD_ComoLancou.seclLancadoManual);
  end else
  begin
    KndTab         := EdKndTab.ValueVariant;
    KndCod         := EdKndCod.ValueVariant;
    KndNSU         := EdKndNSU.ValueVariant;
    KndItm         := EdKndItm.ValueVariant;
    KndAID         := EdKndAID.ValueVariant;
    KndNiv         := EdKndNiv.ValueVariant;
    IDSeq1         := EdIDSeq1.ValueVariant;
    ID_SEK         := FID_SEK;
  end;
  DT_EST         := Geral.FDT(TPDT_EST.Date, 1);
  GraGruX        := EdGraGruX.ValueVariant;
  COD_ITEM       := dmkPF.ITS_Null(GraGruX);
  QTD            := EdQTD.ValueVariant;
  IND_EST        := dmkPF.ITS_Zero(RGIND_EST.ItemIndex);
  COD_PART       := EdCOD_PART.ValueVariant;
  Grandeza       := QrGraGruXGrandeza.Value;
  Pecas          := EdPecas.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  Entidade       := EdCOD_PART.ValueVariant;
  Tipo_Item      := QrGraGruXTipo_Item.Value;
  //
  if COD_PART = '0' then
    COD_PART := '';
  //
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o c�digo do produto!') then
    Exit;
  if MyObjects.FIC(Trim(QrGraGruXSIGLAUNIDMED.Value) = '', EdGraGruX,
    'C�digo do produto sem sigla de unidade definida!') then Exit;
  //
  (*Campo 01 (REG) - Valor V�lido: [K200]*)
  (*Campo 02 (DT_EST) � Valida��o: a data do estoque deve ser igual � data final
  do per�odo de apura��o � campo DT_FIN do Registro K100.*)

  if MyObjects.FIC(Trunc(TPDT_EST.Date) <> Trunc(DfSEII_v03_0_2_a.QrK100DT_FIN.Value),
  nil, 'A data do estoque deve ser igual � data final do per�odo de apura��o!')
  then Exit;

  (*Campo 03 (COD_ITEM) � Valida��o: o c�digo do item dever� existir no campo
  COD_ITEM do Registro 0200. Somente podem ser informados nesse campo valores de
  COD_ITEM cujos tipos sejam iguais a 00, 01, 02, 03, 04, 05, 06 e 10 � campo
  TIPO_ITEM do Registro 0200.*)

  if not EfdIcmsIpi_PF.ValidaGGXEstahDentroDoAnoMesSPED(AnoMes, GraGruX,
  COD_DOC_OS) then Exit;

  (*Campo 05 (IND_EST) - Valores V�lidos: [0, 1, 2]
  Valida��o: se preenchido com valor �1� (posse de terceiros) ou �2� (propriedade
  de terceiros), o campo COD_PART ser� obrigat�rio.
  A quantidade em estoque existente no estabelecimento industrializador do
  produto industrializado para terceiro por encomenda dever� ser:
    a) quando o informante for o estabelecimento industrializador , do tipo 2 -
    estoque de propriedade de terceiros e em posse do informante;
    b) quando o informante for o estabelecimento encomendante, do tipo 1 -
    estoque de propriedade do informante e em posse de terceiros;
  A quantidade em estoque existente no estabelecimento industrializador de
  insumo recebido do encomendante para industrializa��o por encomenda dever� ser:
    a) quando o informante for o estabelecimento industrializador, do tipo 2 -
    estoque de propriedade de terceiros e em posse do informante;
    b) quando o informante for o estabelecimento encomendante, do tipo 1 -
    estoque de propriedade do informante e em posse de terceiros.*)

  if MyObjects.FIC((RGIND_EST.ItemIndex < 0) or (RGIND_EST.ItemIndex > 2), RGIND_EST,
    'Informe o indicador de propriedade/posse!') then Exit;
  if MyObjects.FIC((EdCOD_PART.ValueVariant <> 0) and (RGIND_EST.ItemIndex = 0), EdCOD_PART,
    'N�o informe o propriet�rio/possuidor quando for o informante!') then Exit;
  if MyObjects.FIC((EdCOD_PART.ValueVariant = 0) and (RGIND_EST.ItemIndex <> 0), EdCOD_PART,
    'Informe o propriet�rio/possuidor quando n�o for o informante!') then Exit;


  (*Campo 06 (COD_PART) � Preenchimento: obrigat�rio quando o IND_EST for �1� ou
  �2�.
  Valida��o: o valor fornecido deve constar no campo COD_PART do registro 0150.*)

    // Ver acima Campo 05!

  KndItm := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik200', 'KndItm', [
  'ImporExpor', 'AnoMes', 'KndTab'], [ImporExpor, AnoMes, KndTab],
  SQLType, KndItm, siPositivo, EdKndItm);
  //
  IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik200', 'IDSeq1', [
  'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
  ImporExpor, AnoMes, Empresa, PeriApu],
  SQLType, IDSeq1, siPositivo, EdIDSeq1);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdicmsipik200', False, [
  'KndAID', 'KndNiv', 'IDSeq1',
  'ID_SEK',
  'DT_EST', 'COD_ITEM', 'QTD',
  'IND_EST', 'COD_PART', 'Grandeza',
  'Pecas', 'AreaM2', 'PesoKg',
  'Entidade', 'Tipo_Item', 'GraGruX'
  ], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'
  ], [
  KndAID, KndNiv, IDSeq1,
  ID_SEK,
  DT_EST, COD_ITEM, QTD,
  IND_EST, COD_PART, Grandeza,
  Pecas, AreaM2, PesoKg,
  Entidade, Tipo_Item, GraGruX
  ], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True) then
  begin
    DfSEII_v03_0_2_a.ReopenK200(KndTab, KndItm, 0, rkGrade);
    Close;
  end;
end;

procedure TFmEfdIcmsIpiK200_v03_0_2_a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpiK200_v03_0_2_a.EdGraGruXRedefinido(Sender: TObject);
begin
  InfoExtraQTD();
end;

procedure TFmEfdIcmsIpiK200_v03_0_2_a.EdQTDChange(Sender: TObject);
begin
  InfoExtraQTD();
end;

procedure TFmEfdIcmsIpiK200_v03_0_2_a.EdQTDRedefinido(Sender: TObject);
begin
  InfoExtraQTD();
end;

procedure TFmEfdIcmsIpiK200_v03_0_2_a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiK200_v03_0_2_a.FormCreate(Sender: TObject);
begin
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  ReopenGraGruX();
end;

procedure TFmEfdIcmsIpiK200_v03_0_2_a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdIcmsIpiK200_v03_0_2_a.InfoExtraQTD();
begin
  if (EdGraGruX.ValueVariant <> 0) and
  (EdQTD.ValueVariant <> 0) then
  begin
    EdPecas.ValueVariant  := 0;
    EdAreaM2.ValueVariant := 0;
    EdPesoKg.ValueVariant := 0;
    //
    case TGrandezaUnidMed(QrGraGruXGrandeza.Value) of
      gumPeca:   EdPecas.ValueVariant  := EdQTD.ValueVariant;
      gumAreaM2: EdAreaM2.ValueVariant := EdQTD.ValueVariant;
      gumPesoKg: EdPesoKg.ValueVariant := EdQTD.ValueVariant;
    end;
  end;
end;

procedure TFmEfdIcmsIpiK200_v03_0_2_a.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
begin
  SQL_AND := '';
  SQL_LFT := '';
(*
  if TdmkAppID(CO_DMKID_APP) = dmkappB_L_U_E_D_E_R_M then
  begin
    SQL_AND := 'AND NOT (pqc.PQ IS NULL)';
    SQL_LFT := 'LEFT JOIN pqcli pqc ON pqc.PQ=gg1.Nivel1';
  end;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gratamcad    gtc ON gtc.Codigo=gti.Codigo ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  SQL_LFT,
  'WHERE ggx.Controle > 0 ', //-900000 ',
  'AND NOT gti.Codigo IS NULL ',
  SQL_AND,
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

procedure TFmEfdIcmsIpiK200_v03_0_2_a.RGIND_ESTClick(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (RGIND_EST.ItemIndex > 0) and (RGIND_EST.ItemIndex < 3);
  LaCOD_PART.Enabled := Habilita;
  EdCOD_PART.Enabled := Habilita;
  CBCOD_PART.Enabled := Habilita;
  if not Habilita then
  begin
    EdCOD_PART.ValueVariant := 0;
    CBCOD_PART.KeyValue     := 0;
  end;
end;

end.
