unit UnEfdIcmsIpi_Jan_v03_0_2_a;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) dmkEdit, dmkRadioGroup,
  dmkMemo, dmkCheckGroup, UnDmkProcFunc, TypInfo, UnMyObjects,
  dmkEditDateTimePicker;

type
{
  TEstagioVS_SPED = (evsspedNone=0, evsspedEncerraVS=1, evsspedGeraSPED=2,
                     evsspedExportaSPED=3);
  TCorrApoFormaLctoPosNegSPED = (caflpnIndef=0, caflpnMovNormEsquecido=1,
                               caflpnDesfazimentoMov=2);
}
  TUnEfdIcmsIpi_Jan_v03_0_2_a = class(TObject)
  private
    { Private declarations }
    procedure MsgVersao_EFD_ICMS_IPI_NaoImplementada();

  public
    { Public declarations }
    procedure MostraFormEFD_C001();
    //
    procedure MostraFormEfdIcmsIpiE001();
    procedure MostraFormSPEDEFDEnce();
    procedure MostraFormSpedefdIcmsIpiBalancos(ImporExpor, AnoMes, Empresa,
              LinArq: Integer; Registro: String; Data: TDateTime);
    procedure MostraFormSpedEfdIcmsIpiClaProd(ImporExpor, AnoMes, Empresa,
              LinArq: Integer; Registro: String; Data: TDateTime);
    procedure MostraFormEfdIcmsIpiExporta();
    procedure MostraFormSPED_EFD_Compara(Versao, SubVersao: Integer);
  end;
var
  EfdIcmsIpi_Jan_v03_0_2_a: TUnEfdIcmsIpi_Jan_v03_0_2_a;

implementation

uses UnMLAGeral, MyDBCheck, DmkDAC_PF, Module, ModuleGeral, UnDmkWeb, Restaura2,
  UMySQLModule, UnGrade_PF,
  //EFD_C001,
  EFD_C001_v03_0_9,
///////////////////////////     SPED ICMS IPI    ///////////////////////////////
  EfdIcmsIpiExporta_v03_0_9, EfdIcmsIpiE001_v03_0_9, EfdIcmsIpiEnce_v03_0_2_a,
{$IFDef usaVSePQx}
  SpedEfdIcmsipiBalancos_v03_0_9, SpedEfdIcmsIpiClaProd_v03_0_9,
  SPED_EFD_Compara_22,
{$EndIf}
///////////////////////////     SPED ICMS IPI    ///////////////////////////////
  ModAppGraG1;


{ TUnEfdIcmsIpi_Jan_v03_0_2_a }

procedure TUnEfdIcmsIpi_Jan_v03_0_2_a.MostraFormEfdIcmsIpiE001();
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiE001_v03_0_9, FmEfdIcmsIpiE001_v03_0_9, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiE001_v03_0_9.ShowModal;
    FmEfdIcmsIpiE001_v03_0_9.Destroy;
  end;
end;

procedure TUnEfdIcmsIpi_Jan_v03_0_2_a.MostraFormSPEDEFDEnce();
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiEnce_v03_0_2_a, FmEfdIcmsIpiEnce_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiEnce_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiEnce_v03_0_2_a.Destroy;
  end;
end;

procedure TUnEfdIcmsIpi_Jan_v03_0_2_a.MostraFormSpedefdIcmsIpiBalancos(
  ImporExpor, AnoMes, Empresa, LinArq: Integer; Registro: String;
  Data: TDateTime);
var
  Ano, Mes: Word;
begin
{$IFDef usaVSePQx}
  if DBCheck.CriaFm(TFmSpedEfdIcmsipiBalancos_v03_0_9, FmSpedEfdIcmsipiBalancos_v03_0_9, afmoNegarComAviso) then
  begin
    FmSpedEfdIcmsipiBalancos_v03_0_9.FImporExpor := ImporExpor;
    FmSpedEfdIcmsipiBalancos_v03_0_9.FAnoMes     := AnoMes;
    FmSpedEfdIcmsipiBalancos_v03_0_9.FEmpresa    := Empresa;
    FmSpedEfdIcmsipiBalancos_v03_0_9.FRegPai     := LinArq;
    FmSpedEfdIcmsipiBalancos_v03_0_9.FData       := Data;
    //
    dmkPF.AnoMesDecode(AnoMes, Ano, Mes);
    FmSpedEfdIcmsipiBalancos_v03_0_9.CBMes.ItemIndex := Mes - 1;
    FmSpedEfdIcmsipiBalancos_v03_0_9.CBAno.Text      := Geral.FF0(Ano);
    FmSpedEfdIcmsipiBalancos_v03_0_9.TPData.Date     := Data;
    //
    if Registro = 'H010' then
    begin
      FmSpedEfdIcmsipiBalancos_v03_0_9.PCRegistro.ActivePageIndex := 1;
      FmSpedEfdIcmsipiBalancos_v03_0_9.LaData.Visible             := False;
      FmSpedEfdIcmsipiBalancos_v03_0_9.TPData.Visible             := False;
      FmSpedEfdIcmsipiBalancos_v03_0_9.PnPeriodo.Enabled          := True;
    end else
    if Registro = 'K200' then
    begin
      FmSpedEfdIcmsipiBalancos_v03_0_9.PCRegistro.ActivePageIndex := 2;
      FmSpedEfdIcmsipiBalancos_v03_0_9.LaData.Visible             := True;
      FmSpedEfdIcmsipiBalancos_v03_0_9.TPData.Visible             := True;
      FmSpedEfdIcmsipiBalancos_v03_0_9.PnPeriodo.Enabled          := False;
    end else
      FmSpedEfdIcmsipiBalancos_v03_0_9.PCRegistro.ActivePageIndex := 0;
    //
    if FmSpedEfdIcmsipiBalancos_v03_0_9.PCRegistro.ActivePageIndex > 0 then
      FmSpedEfdIcmsipiBalancos_v03_0_9.ShowModal
    else
      Geral.MB_Erro('Registro n�o implementdo em balan�o de SPED EFD: ' +
      Registro);
    FmSpedEfdIcmsipiBalancos_v03_0_9.Destroy;
  end;
{$EndIf}
end;

procedure TUnEfdIcmsIpi_Jan_v03_0_2_a.MostraFormSpedEfdIcmsIpiClaProd(
  ImporExpor, AnoMes, Empresa, LinArq: Integer; Registro: String;
  Data: TDateTime);
var
  Ano, Mes: Word;
begin
{$IFDef usaVSePQx}
  if DBCheck.CriaFm(TFmSpedEfdIcmsIpiClaProd_v03_0_2_a, FmSpedEfdIcmsIpiClaProd_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.FImporExpor := ImporExpor;
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.FAnoMes     := AnoMes;
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.FEmpresa    := Empresa;
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.FPeriApu    := LinArq;
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.FData       := Data;
    //
    dmkPF.AnoMesDecode(AnoMes, Ano, Mes);
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.CBMes.ItemIndex := Mes - 1;
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.CBAno.Text      := Geral.FF0(Ano);
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.TPData.Date     := Data;
    //
    //
    if Registro = 'K220' then
    begin
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PCRegistro.ActivePageIndex := 1;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.LaData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.TPData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PnPeriodo.Enabled          := True;
    end else
    if Registro = 'K230' then
    begin
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PCRegistro.ActivePageIndex := 2;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.LaData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.TPData.Visible             := False;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PnPeriodo.Enabled          := True;
    end else
(*
    if Registro = 'K200' then
    begin
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PCRegistro.ActivePageIndex := 2;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.LaData.Visible             := True;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.TPData.Visible             := True;
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PnPeriodo.Enabled          := False;
    end else
*)
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PCRegistro.ActivePageIndex := 0;
    //
    if FmSpedEfdIcmsIpiClaProd_v03_0_2_a.PCRegistro.ActivePageIndex > 0 then
      FmSpedEfdIcmsIpiClaProd_v03_0_2_a.ShowModal
    else
      Geral.MB_Erro('Registro n�o implementdo em SPED EFD K2XX: ' +
      Registro);
    FmSpedEfdIcmsIpiClaProd_v03_0_2_a.Destroy;
  end;
{$EndIf}
end;

procedure TUnEfdIcmsIpi_Jan_v03_0_2_a.MostraFormSPED_EFD_Compara(Versao, SubVersao: Integer);
begin
  case Versao of
    22:
    begin
      if DBCheck.CriaFm(TFmSPED_EFD_Compara_22, FmSPED_EFD_Compara_22, afmoNegarComAviso) then
      begin
        FmSPED_EFD_Compara_22.ShowModal;
        FmSPED_EFD_Compara_22.Destroy;
      end;
    end;
    else MsgVersao_EFD_ICMS_IPI_NaoImplementada();
  end;
end;

procedure TUnEfdIcmsIpi_Jan_v03_0_2_a.MsgVersao_EFD_ICMS_IPI_NaoImplementada;
begin
  Geral.MB_Aviso('Vers�o de SPED EFD ICMS-IPI n�o implementada!');
end;

procedure TUnEfdIcmsIpi_Jan_v03_0_2_a.MostraFormEfdIcmsIpiExporta();
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiExporta_v03_0_9, FmEfdIcmsIpiExporta_v03_0_9, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiExporta_v03_0_9.EdEmpresa.ValueVariant := 1;
    FmEfdIcmsIpiExporta_v03_0_9.CBEmpresa.KeyValue := 1;
    FmEfdIcmsIpiExporta_v03_0_9.RGCOD_FIN.ItemIndex := 1;
    FmEfdIcmsIpiExporta_v03_0_9.RGPreenche.ItemIndex := 2;
    FmEfdIcmsIpiExporta_v03_0_9.EdMes.ValueVariant := Date - 21;
    FmEfdIcmsIpiExporta_v03_0_9.ShowModal;
    FmEfdIcmsIpiExporta_v03_0_9.Destroy;
  end;
end;

procedure TUnEfdIcmsIpi_Jan_v03_0_2_a.MostraFormEFD_C001();
begin
  if DBCheck.CriaFm(TFmEFD_C001_v03_0_9, FmEFD_C001_v03_0_9, afmoNegarComAviso) then
  begin
    FmEFD_C001_v03_0_9.ShowModal;
    FmEFD_C001_v03_0_9.Destroy;
  end;
end;

end.
