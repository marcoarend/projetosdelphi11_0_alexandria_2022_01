unit XXDataEFDData;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditDateTimePicker;

type
  TFmXXDataEFDData = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    LaHora: TLabel;
    TPDataFisico: TdmkEditDateTimePicker;
    EdHoraFisico: TdmkEdit;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    TPDataSPEDEFD: TdmkEditDateTimePicker;
    EdHoraSPEDEFD: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure Resultado(Confirma: Boolean);
  public
    { Public declarations }
    FResult: Boolean;
    FDataFisico, FDataSPedEFD: String;
  end;

  var
  FmXXDataEFDData: TFmXXDataEFDData;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmXXDataEFDData.BtOKClick(Sender: TObject);
begin
  Resultado(True);
end;

procedure TFmXXDataEFDData.BtSaidaClick(Sender: TObject);
begin
  Resultado(False);
end;

procedure TFmXXDataEFDData.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmXXDataEFDData.FormCreate(Sender: TObject);
begin
  FResult := False;
  ImgTipo.SQLType := stLok;
end;

procedure TFmXXDataEFDData.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmXXDataEFDData.Resultado(Confirma: Boolean);
begin
  FResult := Confirma;
  FDataFisico  := Geral.FDT_TP_Ed(TPDataFisico.Date, EdHoraFisico.Text);
  FDataSPedEFD := Geral.FDT_TP_Ed(TPDataSPEDEFD.Date, EdHoraSPEDEFD.Text);
  //
  Close;
end;

end.
