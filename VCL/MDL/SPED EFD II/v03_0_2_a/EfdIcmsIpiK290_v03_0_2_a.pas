unit EfdIcmsIpiK290_v03_0_2_a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup, dmkEditCalc, UnDmkProcFunc,
  UnEfdIcmsIpi_PF, UnAppPF, UnAppEnums;

type
  TFmEfdIcmsIpiK290_v03_0_2_a = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdPeriApu: TdmkEdit;
    Label6: TLabel;
    EdKndTab: TdmkEdit;
    Label9: TLabel;
    Panel5: TPanel;
    EdKndCod: TdmkEdit;
    Label7: TLabel;
    EdKndNSU: TdmkEdit;
    Label8: TLabel;
    EdKndItm: TdmkEdit;
    Label10: TLabel;
    EdKndAID: TdmkEdit;
    Label1: TLabel;
    EdKndNiv: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    EdIDSeq1: TdmkEdit;
    TPDT_INI_OP: TdmkEditDateTimePicker;
    LaDT_INI_OP: TLabel;
    Label13: TLabel;
    TPDT_FIN_OP: TdmkEditDateTimePicker;
    LaMovimCod: TLabel;
    EdMovimCod: TdmkEdit;
    EdTxt: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdMovimCodRedefinido(Sender: TObject);
    procedure EdMovimCodChange(Sender: TObject);
    procedure EdKndTabRedefinido(Sender: TObject);
  private
    { Private declarations }
    //procedure ReopenGraGruX();
  public
    { Public declarations }
    FID_SEK, FOriOpeProc, FOrigemIDKnd: Integer;
  end;

  var
  FmEfdIcmsIpiK290_v03_0_2_a: TFmEfdIcmsIpiK290_v03_0_2_a;

implementation

uses UnMyObjects, Module, (*EfdIcmsIpiE001,*) UMySQLModule, DmkDAC_PF, MyListas,
  ModuleFin, UnFinanceiro, SpedEfdIcmsIpi_v03_0_2_a;

{$R *.DFM}

procedure TFmEfdIcmsIpiK290_v03_0_2_a.BtOKClick(Sender: TObject);
var
  DT_INI_OP, DT_FIN_OP, COD_DOC_OP: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq1, ID_SEK, OriOpeProc: Integer;
  SQLType: TSQLType;
  MovimCod: Integer;
  DataIni, DataFim: TDateTime;
  ComOP: Boolean;
begin
//N�O permitir informar OP quando n�o existir IME-C
  ComOP          := EfdIcmsIpi_PF.HabilitaDocOP(KndTab);
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  PeriApu        := EdPeriApu.ValueVariant;
  KndTab         := EdKndTab.ValueVariant;
  if SQLType = stIns then
  begin
    //KndTab         := Integer(TEstqSPEDTabSorc.estsNoSrc);
    KndCod         := PeriApu;
    KndNSU         := PeriApu;
    KndItm         := 0; // Abaixo
    KndAID         := Integer(TEstqMovimID.emidAjuste);
    KndNiv         := Integer(TEstqMovimNiv.eminSemNiv);
    IDSeq1         := 0; // Abaixo
    ID_SEK         := Integer(TSPED_EFD_ComoLancou.seclLancadoManual);
    OriOpeProc     := Integer(TOrigemOpeProc.oopND);
    //OrigemIDKnd    := Integer(TOrigemIDKnd.oidk000ND);
  end else
  begin
    //KndTab         := EdKndTab.ValueVariant;
    KndCod         := EdKndCod.ValueVariant;
    KndNSU         := EdKndNSU.ValueVariant;
    KndItm         := EdKndItm.ValueVariant;
    KndAID         := EdKndAID.ValueVariant;
    KndNiv         := EdKndNiv.ValueVariant;
    IDSeq1         := EdIDSeq1.ValueVariant;
    ID_SEK         := FID_SEK;
    OriOpeProc     := FOriOpeProc;
    //OrigemIDKnd    := FOrigemIDKnd;
  end;
  DataIni        := Trunc(TPDT_INI_OP.Date);
  DataFim        := Trunc(TPDT_FIN_OP.Date);
  MovimCod       := EdMovimCod.ValueVariant;
  //
  DT_INI_OP      := Geral.FDT(DataIni, 1);
  DT_FIN_OP      := Geral.FDT(DataFim, 1);
  COD_DOC_OP     := dmkPF.ITS_Null(MovimCod);

  if MyObjects.FIC(ComOP and (TPDT_INI_OP.Date < 2), TPDT_INI_OP,
    'Informe a data inicial da Ordem de Servi�o! (IME-C)') then Exit;
  if MyObjects.FIC(ComOP and (MovimCod = 0), EdMovimCod,
    'Informe o c�digo da Ordem de Servi�o! (IME-C)') then Exit;
  //ID_SEK         := ;
  //
  (*Campo 01 (REG) - Valor V�lido: [K290]*)
  (*Campo 02 (DT_INI_OP) - Preenchimento: a data de in�cio dever� ser informada
  se existir ordem de produ��o, ainda que iniciada em per�odo de apura��o cujo
  registro K100 correspondente esteja em um arquivo relativo a um m�s anterior.
  Valida��o: obrigat�rio se informado o campo COD_DOC_OP ou o campo DT_FIN_OP.
  O valor informado deve ser menor ou igual a DT_FIN do registro K100.*)

  if not EfdIcmsIpi_PF.ValidaDataInicialDentroDoAnoMesSPED(AnoMes, DataIni,
  DataFim, COD_DOC_OP) then Exit;

  (*
  Campo 03 (DT_FIN_OP) - Preenchimento: informar a data de conclus�o da ordem de
  produ��o. Ficar� em branco, caso a ordem de produ��o n�o seja conclu�da at� a
  data de encerramento do per�odo de apura��o. Nesta situa��o a produ��o ficou
  em elabora��o.
  Valida��o: se preenchido:
  a) DT_FIN_OP deve ser menor ou igual a DT_FIN do registro K100 e ser maior ou
     igual a DT_INI_OP;
  b) quando DT_FIN_OP for menor que o campo DT_INI do registro 0000, a mesma
     deve ser informada no primeiro per�odo de apura��o do K100;
  c) quando DT_FIN_OP for menor que o campo DT_INI do registro 0000, �
     obrigat�rio existir pelo menos um Registro K291 e n�o devem existir
     Registros K292.*)

  if not EfdIcmsIpi_PF.ValidaDataFinalDentroDoAnoMesSPED(AnoMes, DataIni,
  DataFim, COD_DOC_OP) then Exit;

  (*Campo 04 (COD_DOC_OP) � Preenchimento: informar o c�digo da ordem de produ��o.
    Valida��o: obrigat�rio se informado o campo DT_INI_OP.*)

  if not EfdIcmsIpi_PF.ValidaOSDentroDoAnoMesSPED(AnoMes, DataIni,
  DataFim, COD_DOC_OP) then Exit;

  KndItm := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik290', 'KndItm', [
  'ImporExpor', 'AnoMes', 'KndTab'], [ImporExpor, AnoMes, KndTab],
  SQLType, KndItm, siPositivo, EdKndItm);
  //
  IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik290', 'IDSeq1', [
  'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
  ImporExpor, AnoMes, Empresa, PeriApu],
  SQLType, IDSeq1, siPositivo, EdIDSeq1);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdicmsipik290', False, [
  'KndAID', 'KndNiv', 'IDSeq1',
  'ID_SEK', 'DT_INI_OP', 'DT_FIN_OP',
  'OriOpeProc'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm', 'COD_DOC_OP'], [
  KndAID, KndNiv, IDSeq1,
  ID_SEK, DT_INI_OP, DT_FIN_OP,
  OriOpeProc], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm, COD_DOC_OP], True) then
  begin
    DfSEII_v03_0_2_a.ReopenK290(IDSeq1);
    Close;
  end;
end;

procedure TFmEfdIcmsIpiK290_v03_0_2_a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpiK290_v03_0_2_a.EdKndTabRedefinido(Sender: TObject);
begin
  EfdIcmsIpi_PF.HabilitaMovimCodPorKndTab(Self, EdKndTab.ValueVariant,
  [LaDT_INI_OP, TPDT_INI_OP, LaMovimCod, EdMovimCod]);
end;

procedure TFmEfdIcmsIpiK290_v03_0_2_a.EdMovimCodChange(Sender: TObject);
begin
  EdTxt.Text := '';
end;

procedure TFmEfdIcmsIpiK290_v03_0_2_a.EdMovimCodRedefinido(Sender: TObject);
begin
  AppPF.VerificaSeExisteIMEC(EdTxt, EdMovimCod.ValueVariant);
end;

procedure TFmEfdIcmsIpiK290_v03_0_2_a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiK290_v03_0_2_a.FormCreate(Sender: TObject);
begin
  TPDT_INI_OP.Date := 0;
  TPDT_FIN_OP.Date := 0;
  //ReopenGraGruX();
end;

procedure TFmEfdIcmsIpiK290_v03_0_2_a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
