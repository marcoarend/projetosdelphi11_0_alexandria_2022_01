unit EfdIcmsIpiE001_v03_0_2_a;
(* Origem: C:\_Compilers\Delphi_XE2\VCL\MDL\SPED\v00_01\EFD_E001.pas*)
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Menus, ComCtrls, Grids,
  DBGrids, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, Variants,
  dmkDBGrid, frxClass, frxDBSet, dmkCheckGroup, DmkDAC_PF, dmkImage, UnDmkEnums,
  dmkDBGridZTO, UnAppPF, UnProjGroup_Vars, AppListas, UnProjGroup_Consts,
  UnEfdIcmsIpi_Jan_v03_0_2_a, UnEfdIcmsIpi_PF, UnApp_Jan;

type
  //FHackDBGrid = class(THackDBGrid);
  //
  TFmEfdIcmsIpiE001_v03_0_2_a = class(TForm)
    dmkPermissoes1: TdmkPermissoes;
    PMPeriodo: TPopupMenu;
    PME111: TPopupMenu;
    Itemns1: TMenuItem;
    PME100: TPopupMenu;
    Incluinovoperiodo1: TMenuItem;
    Excluiperiodoselecionado1: TMenuItem;
    Incluinovointervalo1: TMenuItem;
    Alteraintervaloselecionado1: TMenuItem;
    Excluiintervaloselecionado1: TMenuItem;
    Incluinovoajuste1: TMenuItem;
    Alteraajusteselecionado1: TMenuItem;
    Excluiajusteselecionado1: TMenuItem;
    PMObrigacoes: TPopupMenu;
    IncluinovaobrigaodoICMSarecolher1: TMenuItem;
    Alteraobrigaoselecionada1: TMenuItem;
    Excluiobrigaoselecionada1: TMenuItem;
    N1: TMenuItem;
    E113Informaesadicionais1: TMenuItem;
    E1121: TMenuItem;
    Incluinovainformaoadicional1: TMenuItem;
    Alterainformaoadicionalselecionada1: TMenuItem;
    Excluiinformaoadicionalselecionada1: TMenuItem;
    Incluinovaidentificaodedocumentofiscal1: TMenuItem;
    Alteraidentificaoselecionada1: TMenuItem;
    Excluiidentificaoselecionada1: TMenuItem;
    PMValDecltorio: TPopupMenu;
    Incluinovovalordeclaratrio1: TMenuItem;
    Alteravalordeclaratrioselecionado1: TMenuItem;
    Excluivalordeclaratrioselecionado1: TMenuItem;
    PME500: TPopupMenu;
    IncluinovointervaloIPI1: TMenuItem;
    AlteraintervaloIPIselecionado1: TMenuItem;
    ExcluiintervaloIPIselecionado1: TMenuItem;
    PME530: TPopupMenu;
    IncluiajustedaapuracaodoIPI1: TMenuItem;
    AlteraajustedaapuracaodoIPIselecionado1: TMenuItem;
    ExcluiajustedaapuracaodoIPIselecionado1: TMenuItem;
    PMH005: TPopupMenu;
    Incluidatadeinventrio1: TMenuItem;
    AlteraInventrio1: TMenuItem;
    Excluiinventrio1: TMenuItem;
    PMH010: TPopupMenu;
    Importadebalano1: TMenuItem;
    Incluiitem1: TMenuItem;
    Alteraitem1: TMenuItem;
    Excluiitemns1: TMenuItem;
    PMH020: TPopupMenu;
    Incluiinformaocomplementar1: TMenuItem;
    Alterainformaocomplementar1: TMenuItem;
    Excluiinformaocomplementar1: TMenuItem;
    PMK100: TPopupMenu;
    IncluinovointervalodeProduoeEstoque1: TMenuItem;
    AlteraintervalodeProduoeEstoque1: TMenuItem;
    ExcluiintervalodeProduoeEstoque1: TMenuItem;
    PMK200: TPopupMenu;
    ImportaK200_1: TMenuItem;
    IncluiK200_1: TMenuItem;
    AlteraK200_1: TMenuItem;
    ExcluiK200_1: TMenuItem;
    PMK220: TPopupMenu;
    ImportaK220_1: TMenuItem;
    IncluiK220_1: TMenuItem;
    AlteraK220_1: TMenuItem;
    ExcluiK220_1: TMenuItem;
    PMK230: TPopupMenu;
    ImportaK230_1: TMenuItem;
    IncluiK230_1: TMenuItem;
    AlteraK230_1: TMenuItem;
    ExcluiK230_1: TMenuItem;
    K2301: TMenuItem;
    K2351: TMenuItem;
    K2501: TMenuItem;
    K2551: TMenuItem;
    N2: TMenuItem;
    odos1: TMenuItem;
    Vaiparajanelademovimento1: TMenuItem;
    PMImprime: TPopupMenu;
    BlocoKRegistrosK2201: TMenuItem;
    BlocoKRegistrosK2001: TMenuItem;
    ProduoK230aK2551: TMenuItem;
    N3: TMenuItem;
    Erroempresaitens1: TMenuItem;
    Excluisegmentointeiroimportado1: TMenuItem;
    K2601: TMenuItem;
    K2651: TMenuItem;
    ExcluiK200_280_1: TMenuItem;
    N4: TMenuItem;
    PMK280: TPopupMenu;
    ExcluiK280_1: TMenuItem;
    ExcluiK220_280_1: TMenuItem;
    ExcluiK230_280_1: TMenuItem;
    Excluiregistrosd1: TMenuItem;
    N5: TMenuItem;
    ExcluirTODASTABELASK1: TMenuItem;
    PnForm: TPanel;
    GBAvisos1: TGroupBox;
    Panel12: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    PainelDados: TPanel;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    Panel9: TPanel;
    PCEx00: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    DBGE100: TdmkDBGrid;
    Panel32: TPanel;
    BtE100: TBitBtn;
    PCE100: TPageControl;
    TabSheet3: TTabSheet;
    PnE110: TPanel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label7: TStaticText;
    Label8: TStaticText;
    Label9: TStaticText;
    Label10: TStaticText;
    Label11: TStaticText;
    Label12: TStaticText;
    Label13: TStaticText;
    Label14: TStaticText;
    Label15: TStaticText;
    Label16: TStaticText;
    Label17: TStaticText;
    Label18: TStaticText;
    Label19: TStaticText;
    Label20: TStaticText;
    TabSheet4: TTabSheet;
    Panel5: TPanel;
    Splitter2: TSplitter;
    Label25: TLabel;
    DBGE111: TDBGrid;
    Panel14: TPanel;
    Label21: TLabel;
    Splitter1: TSplitter;
    Label22: TLabel;
    DBGE113: TDBGrid;
    DBGE112: TDBGrid;
    TabSheet5: TTabSheet;
    Panel15: TPanel;
    Label23: TLabel;
    DBGE115: TDBGrid;
    TabSheet6: TTabSheet;
    Panel8: TPanel;
    Label24: TLabel;
    DBGE116: TDBGrid;
    Panel13: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Panel17: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    Panel22: TPanel;
    BtE110: TBitBtn;
    BtE111: TBitBtn;
    BtE116: TBitBtn;
    BtE115: TBitBtn;
    TabSheet7: TTabSheet;
    Panel18: TPanel;
    GroupBox3: TGroupBox;
    DBGE500: TdmkDBGridZTO;
    Panel33: TPanel;
    BtE500: TBitBtn;
    PCE500: TPageControl;
    TabSheet8: TTabSheet;
    DBGE510: TdmkDBGridZTO;
    TabSheet9: TTabSheet;
    Panel19: TPanel;
    DBGE520: TdmkDBGridZTO;
    Panel20: TPanel;
    Label26: TLabel;
    DBGE530: TdmkDBGridZTO;
    Panel23: TPanel;
    BtE520: TBitBtn;
    BtE530: TBitBtn;
    TabSheet10: TTabSheet;
    Panel21: TPanel;
    GroupBox4: TGroupBox;
    DBGH005: TdmkDBGridZTO;
    Panel24: TPanel;
    BtH010: TBitBtn;
    BtH005: TBitBtn;
    BtH020: TBitBtn;
    Panel25: TPanel;
    Label28: TLabel;
    DBGH010: TdmkDBGridZTO;
    Panel26: TPanel;
    Label27: TLabel;
    DBGH020: TdmkDBGridZTO;
    TabSheet11: TTabSheet;
    Panel27: TPanel;
    BtK200: TBitBtn;
    BtK220: TBitBtn;
    BtK230: TBitBtn;
    BtVerifica: TBitBtn;
    BtK280: TBitBtn;
    PCK200: TPageControl;
    TabSheet12: TTabSheet;
    DBGK200: TdmkDBGridZTO;
    TabSheet19: TTabSheet;
    PCK21X: TPageControl;
    TabSheet22: TTabSheet;
    Splitter6: TSplitter;
    DBGK210: TdmkDBGridZTO;
    DBGK215: TdmkDBGridZTO;
    TabSheet23: TTabSheet;
    Splitter10: TSplitter;
    DBGK270_210: TdmkDBGridZTO;
    DBGK275_215: TdmkDBGridZTO;
    TabSheet13: TTabSheet;
    Splitter8: TSplitter;
    Label31: TLabel;
    DBGK220: TdmkDBGridZTO;
    DBGK270_220: TdmkDBGridZTO;
    DBGK275_220: TdmkDBGridZTO;
    TabSheet16: TTabSheet;
    PCKPrd: TPageControl;
    TabSheet17: TTabSheet;
    Splitter3: TSplitter;
    DBGK230: TdmkDBGridZTO;
    DBGK235: TdmkDBGridZTO;
    TabSheet18: TTabSheet;
    Splitter4: TSplitter;
    DBGK250: TdmkDBGridZTO;
    DBGK255: TdmkDBGridZTO;
    TabSheet24: TTabSheet;
    Splitter11: TSplitter;
    DBGK260: TdmkDBGridZTO;
    DBGK265: TdmkDBGridZTO;
    TabSheet20: TTabSheet;
    TabSheet25: TTabSheet;
    DBGK280: TdmkDBGridZTO;
    Panel30: TPanel;
    Panel31: TPanel;
    BtK100: TBitBtn;
    DBGK100: TdmkDBGridZTO;
    TabSheet14: TTabSheet;
    Splitter5: TSplitter;
    DBGK_ConfG: TdmkDBGridZTO;
    Panel28: TPanel;
    BtDifGera: TBitBtn;
    BtDifImprime: TBitBtn;
    BtConferencias: TBitBtn;
    Panel34: TPanel;
    DBGPsq01: TdmkDBGridZTO;
    DBGPsq02: TdmkDBGridZTO;
    TabSheet15: TTabSheet;
    Panel35: TPanel;
    Panel36: TPanel;
    Bt1010: TBitBtn;
    Panel37: TPanel;
    CkIND_EXP: TDBCheckBox;
    CkIND_CCRF: TDBCheckBox;
    CkIND_COMB: TDBCheckBox;
    CkIND_USINA: TDBCheckBox;
    CkIND_VA: TDBCheckBox;
    CkIND_EE: TDBCheckBox;
    CkIND_CART: TDBCheckBox;
    CkIND_FORM: TDBCheckBox;
    CkIND_AER: TDBCheckBox;
    Panel10: TPanel;
    DBGCab: TdmkDBGrid;
    Panel29: TPanel;
    BtPeriodo: TBitBtn;
    Panel3: TPanel;
    Label29: TLabel;
    Label30: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Panel2: TPanel;
    BtSaida0: TBitBtn;
    Panel16: TPanel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    DBEdit1: TDBEdit;
    Panel11: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    BtLocVMI: TBitBtn;
    TabSheet27: TTabSheet;
    DBGK290: TdmkDBGridZTO;
    Splitter13: TSplitter;
    Panel38: TPanel;
    DBGK291: TdmkDBGridZTO;
    Splitter14: TSplitter;
    DBGK292: TdmkDBGridZTO;
    TabSheet28: TTabSheet;
    Panel39: TPanel;
    Splitter15: TSplitter;
    DBGK301: TdmkDBGridZTO;
    DBGK302: TdmkDBGridZTO;
    DBGK300: TdmkDBGridZTO;
    Splitter16: TSplitter;
    Panel40: TPanel;
    RGVeriKItsShow: TRadioGroup;
    Importatodosregistrosdeprooduo1: TMenuItem;
    IncluiK280_1: TMenuItem;
    AlteraK280_1: TMenuItem;
    Desmontagemdemercadoria1: TMenuItem;
    IncluiK210_1: TMenuItem;
    IncluiK215_1: TMenuItem;
    AlteraK2X0_1: TMenuItem;
    AlteraK2X5_1: TMenuItem;
    ProduoIsolada1: TMenuItem;
    IncluiK230_2: TMenuItem;
    IncluiK235_2: TMenuItem;
    LaTempo: TLabel;
    PMConferencias: TPopupMenu;
    Movimentaies1: TMenuItem;
    BaixaeestoquedeInNatura1: TMenuItem;
    Outrasbaixaseestoque1: TMenuItem;
    Geraoxestoque1: TMenuItem;
    VSxPesagemPQ1: TMenuItem;
    N25X250Industrializaoefetuadaporterceiros1: TMenuItem;
    N250Itensproduzidos1: TMenuItem;
    N255InsumosConsumidos1: TMenuItem;
    N29XProduoconjuntanoestabelecimento1: TMenuItem;
    K291ProduoconjuntaItensproduzidos1: TMenuItem;
    K290Ordemdeproduo1: TMenuItem;
    K292ProduoconjuntaItensconsumidos1: TMenuItem;
    AlteraK2X5_2: TMenuItem;
    K2901: TMenuItem;
    K2911: TMenuItem;
    K2921: TMenuItem;
    K3001: TMenuItem;
    K3011: TMenuItem;
    K3021: TMenuItem;
    K30XProduoconjuntaIndustrializaoefetuadaporterceiro1: TMenuItem;
    K300Ordemdeproduo1: TMenuItem;
    K301Itemproduzidos1: TMenuItem;
    K302Itemconsumido1: TMenuItem;
    PC_K270: TPageControl;
    TabSheet30: TTabSheet;
    TabSheet31: TTabSheet;
    TabSheet21: TTabSheet;
    TabSheet26: TTabSheet;
    TabSheet29: TTabSheet;
    DBGK270_260: TdmkDBGridZTO;
    Splitter12: TSplitter;
    DBGK275_265: TdmkDBGridZTO;
    DBGK270_250: TdmkDBGridZTO;
    Splitter9: TSplitter;
    DBGK275_255: TdmkDBGridZTO;
    DBGK270_230: TdmkDBGridZTO;
    Splitter7: TSplitter;
    DBGK275_235: TdmkDBGridZTO;
    TabSheet32: TTabSheet;
    DBGK270_291: TdmkDBGridZTO;
    Splitter17: TSplitter;
    DBGK270_292: TdmkDBGridZTO;
    Splitter18: TSplitter;
    DBGK270_301: TdmkDBGridZTO;
    Splitter19: TSplitter;
    DBGK270_302: TdmkDBGridZTO;
    AlteraK2X5_3: TMenuItem;
    K27XCorreodeapontamentodeproduo1: TMenuItem;
    K270K210K220K230K250K260K291K292K301eK3021: TMenuItem;
    K275K215K220K235K255EK2651: TMenuItem;
    procedure BtSaida0Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPeriodoClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure PME111Popup(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure PMPeriodoPopup(Sender: TObject);
    procedure BtImprime1Click(Sender: TObject);
    procedure RGEmitenteClick(Sender: TObject);
    procedure PME100Popup(Sender: TObject);
    procedure Incluinovoperiodo1Click(Sender: TObject);
    procedure Excluiperiodoselecionado1Click(Sender: TObject);
    procedure Incluinovointervalo1Click(Sender: TObject);
    procedure Alteraintervaloselecionado1Click(Sender: TObject);
    procedure Excluiintervaloselecionado1Click(Sender: TObject);
    procedure Incluinovoajuste1Click(Sender: TObject);
    procedure Alteraajusteselecionado1Click(Sender: TObject);
    procedure IncluinovaobrigaodoICMSarecolher1Click(Sender: TObject);
    procedure Alteraobrigaoselecionada1Click(Sender: TObject);
    procedure PMObrigacoesPopup(Sender: TObject);
    procedure Excluiajusteselecionado1Click(Sender: TObject);
    procedure Excluiobrigaoselecionada1Click(Sender: TObject);
    procedure Incluinovainformaoadicional1Click(Sender: TObject);
    procedure Alterainformaoadicionalselecionada1Click(Sender: TObject);
    procedure Excluiinformaoadicionalselecionada1Click(Sender: TObject);
    procedure Excluiidentificaoselecionada1Click(Sender: TObject);
    procedure Incluinovaidentificaodedocumentofiscal1Click(Sender: TObject);
    procedure Alteraidentificaoselecionada1Click(Sender: TObject);
    procedure PMValDecltorioPopup(Sender: TObject);
    procedure Incluinovovalordeclaratrio1Click(Sender: TObject);
    procedure Alteravalordeclaratrioselecionado1Click(Sender: TObject);
    procedure Excluivalordeclaratrioselecionado1Click(Sender: TObject);
    procedure IncluinovointervaloIPI1Click(Sender: TObject);
    procedure AlteraintervaloIPIselecionado1Click(Sender: TObject);
    procedure ExcluiintervaloIPIselecionado1Click(Sender: TObject);
    procedure IncluiajustedaapuracaodoIPI1Click(Sender: TObject);
    procedure AlteraajustedaapuracaodoIPIselecionado1Click(Sender: TObject);
    procedure ExcluiajustedaapuracaodoIPIselecionado1Click(Sender: TObject);
    procedure PME530Popup(Sender: TObject);
    procedure PME500Popup(Sender: TObject);
    procedure BtE100Click(Sender: TObject);
    procedure BtE500Click(Sender: TObject);
    procedure BtE520Click(Sender: TObject);
    procedure BtE110Click(Sender: TObject);
    procedure BtE111Click(Sender: TObject);
    procedure BtE530Click(Sender: TObject);
    procedure BtE115Click(Sender: TObject);
    procedure BtE116Click(Sender: TObject);
    procedure BtH005Click(Sender: TObject);
    procedure Incluidatadeinventrio1Click(Sender: TObject);
    procedure AlteraInventrio1Click(Sender: TObject);
    procedure Excluiinventrio1Click(Sender: TObject);
    procedure PMH005Popup(Sender: TObject);
    procedure Incluiitem1Click(Sender: TObject);
    procedure Alteraitem1Click(Sender: TObject);
    procedure Excluiitemns1Click(Sender: TObject);
    procedure BtH010Click(Sender: TObject);
    procedure Importadebalano1Click(Sender: TObject);
    procedure Incluiinformaocomplementar1Click(Sender: TObject);
    procedure Alterainformaocomplementar1Click(Sender: TObject);
    procedure Excluiinformaocomplementar1Click(Sender: TObject);
    procedure BtH020Click(Sender: TObject);
    procedure PMH020Popup(Sender: TObject);
    procedure PMH010Popup(Sender: TObject);
    procedure BtK100Click(Sender: TObject);
    procedure IncluinovointervalodeProduoeEstoque1Click(Sender: TObject);
    procedure AlteraintervalodeProduoeEstoque1Click(Sender: TObject);
    procedure ExcluiintervalodeProduoeEstoque1Click(Sender: TObject);
    procedure PMK100Popup(Sender: TObject);
    procedure ImportaK200_1Click(Sender: TObject);
    procedure IncluiK200_1Click(Sender: TObject);
    procedure AlteraK200_1Click(Sender: TObject);
    procedure ExcluiK200_1Click(Sender: TObject);
    procedure BtK200Click(Sender: TObject);
    procedure PMK220Popup(Sender: TObject);
    procedure PMK200Popup(Sender: TObject);
    procedure ImportaK220_1Click(Sender: TObject);
    procedure IncluiK220_1Click(Sender: TObject);
    procedure AlteraK220_1Click(Sender: TObject);
    procedure ExcluiK220_1Click(Sender: TObject);
    procedure BtK220Click(Sender: TObject);
    procedure BtK230Click(Sender: TObject);
    procedure ImportaK230_1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PCK200Change(Sender: TObject);
    procedure K2301Click(Sender: TObject);
    procedure K2351Click(Sender: TObject);
    procedure K2501Click(Sender: TObject);
    procedure K2551Click(Sender: TObject);
    procedure odos1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BlocoKRegistrosK2001Click(Sender: TObject);
    procedure BlocoKRegistrosK2201Click(Sender: TObject);
    procedure ProduoK230aK2551Click(Sender: TObject);
    procedure BtVerificaClick(Sender: TObject);
    procedure BtDifGeraClick(Sender: TObject);
    procedure BtDifImprimeClick(Sender: TObject);
    procedure DBGK_ConfGCellClick(Column: TColumn);
    procedure DBGPsq02DblClick(Sender: TObject);
    procedure Erroempresaitens1Click(Sender: TObject);
    procedure Bt1010Click(Sender: TObject);
    procedure Excluisegmentointeiroimportado1Click(Sender: TObject);
    procedure K2601Click(Sender: TObject);
    procedure K2651Click(Sender: TObject);
    procedure PMK230Popup(Sender: TObject);
    procedure ExcluiK200_280_1Click(Sender: TObject);
    procedure BtK280Click(Sender: TObject);
    procedure ExcluiK280_1Click(Sender: TObject);
    procedure PMK280Popup(Sender: TObject);
    procedure ExcluiK220_280_1Click(Sender: TObject);
    procedure ExcluiK230_280_1Click(Sender: TObject);
    procedure Excluiregistrosd1Click(Sender: TObject);
    procedure ExcluirTODASTABELASK1Click(Sender: TObject);
    procedure BtLocVMIClick(Sender: TObject);
    procedure RGVeriKItsShowClick(Sender: TObject);
    procedure DBGK200DblClick(Sender: TObject);
    // meu
    procedure JanelaPorDblClick(Sender: TObject);
    procedure Importatodosregistrosdeprooduo1Click(Sender: TObject);
    procedure IncluiK280_1Click(Sender: TObject);
    procedure AlteraK280_1Click(Sender: TObject);
    procedure IncluiK210_1Click(Sender: TObject);
    procedure IncluiK215_1Click(Sender: TObject);
    procedure AlteraK2X0_1Click(Sender: TObject);
    procedure AlteraItemGenerico(Sender: TObject);
    procedure Desmontagemdemercadoria1Click(Sender: TObject);
    procedure IncluiK230_2Click(Sender: TObject);
    procedure IncluiK235_2Click(Sender: TObject);
    procedure ProduoIsolada1Click(Sender: TObject);
    procedure Movimentaies1Click(Sender: TObject);
    procedure BaixaeestoquedeInNatura1Click(Sender: TObject);
    procedure Outrasbaixaseestoque1Click(Sender: TObject);
    procedure BtConferenciasClick(Sender: TObject);
    procedure Geraoxestoque1Click(Sender: TObject);
    procedure VSxPesagemPQ1Click(Sender: TObject);
    procedure N250Itensproduzidos1Click(Sender: TObject);
    procedure N255InsumosConsumidos1Click(Sender: TObject);
    procedure K290Ordemdeproduo1Click(Sender: TObject);
    procedure K291ProduoconjuntaItensproduzidos1Click(Sender: TObject);
    procedure K292ProduoconjuntaItensconsumidos1Click(Sender: TObject);
    procedure K2901Click(Sender: TObject);
    procedure K2911Click(Sender: TObject);
    procedure K2921Click(Sender: TObject);
    procedure K3001Click(Sender: TObject);
    procedure K3011Click(Sender: TObject);
    procedure K3021Click(Sender: TObject);
    procedure K300Ordemdeproduo1Click(Sender: TObject);
    procedure K301Itemproduzidos1Click(Sender: TObject);
    procedure K302Itemconsumido1Click(Sender: TObject);
    procedure K270K210K220K230K250K260K291K292K301eK3021Click(Sender: TObject);
    procedure K275K215K220K235K255EK2651Click(Sender: TObject);
  private
    { Private declarations }
    FColumnSel: String;
    procedure CriaDfSEII();



    //


    //FExportarItem: Boolean;
    function  PeriodoJaExiste(ImporExpor, AnoMes, Empresa: Integer;
              Avisa: Boolean): Boolean;
    procedure InsUpdE100(SQLType: TSQLType);
    procedure InsUpdE111(SQLType: TSQLType);
    procedure InsUpdE112(SQLType: TSQLType);
    procedure InsUpdE113(SQLType: TSQLType);
    procedure InsUpdE115(SQLType: TSQLType);
    procedure InsUpdE116(SQLType: TSQLType);
    procedure InsUpdE500(SQLType: TSQLType);
    procedure InsUpdE530(SQLType: TSQLType);
    procedure InsUpdH005(SQLType: TSQLType);
    procedure InsUpdH010(SQLType: TSQLType);
    procedure InsUpdH020(SQLType: TSQLType);
    procedure InsUpdK100(SQLType: TSQLType);
    procedure InsUpdK200(SQLType: TSQLType);
    procedure InsUpdK210(SQLType: TSQLType);
    procedure InsUpdK215(SQLType: TSQLType);
    procedure InsUpdK220(SQLType: TSQLType);
    procedure InsUpdK230(SQLType: TSQLType);
    procedure InsUpdK235(SQLType: TSQLType);
    procedure InsUpdK250(SQLType: TSQLType);
    procedure InsUpdK255(SQLType: TSQLType);
    procedure InsUpdK260(SQLType: TSQLType);
    procedure InsUpdK265(SQLType: TSQLType);
    procedure InsUpdK270(RegPai: Integer; SQLType: TSQLType);
    procedure InsUpdK275(RegPai, RegFilho: Integer; SQLType: TSQLType);
    procedure InsUpdK290(SQLType: TSQLType);
    procedure InsUpdK291(SQLType: TSQLType);
    procedure InsUpdK292(SQLType: TSQLType);
    procedure InsUpdK300(SQLType: TSQLType);
    procedure InsUpdK301(SQLType: TSQLType);
    procedure InsUpdK302(SQLType: TSQLType);
    //
    procedure InsUpdK280(SQLType: TSQLType);
    //procedure JanelaDoMovimento();
    //
    procedure LocalizarVMI();
    //
    procedure MostraFormEFD_E110();
    procedure MostraFormEFD_E520();
    //
    procedure MostraFormEFD_1010();
    //
    procedure ExcluiItensdeTabela(Tabela: String);
    procedure ExcluiItensK7X(ImporExpor, AnoMes, Empresa: Integer;
              SohEspecifico: TSPED_EFD_KndRegOrigem);
    procedure ExcluiItensNormaisDeProducao();
    procedure ExcluiItensK27XParcialTipoEFD(OriSPEDEFDKnd: TOrigemSPEDEFDKnd);
    procedure ExcluiItensK280ParcialTipoEFD(OriSPEDEFDKnd: TOrigemSPEDEFDKnd);
    procedure ExcluiRegsitrosImportados_Estoque();
    procedure ExcluiRegsitrosImportados_Classe();
    procedure ExcluiRegsitrosImportados_Producao();

    //
  public
    { Public declarations }
    //FEmpresa: Integer;
    FExpImpTXT, FEmprTXT: String;
    //
    procedure AtualizaValoresE520deE530();
    procedure AtualizaValoresH005deH010();
    //
    //procedure VerificaBlocoK();
  end;

var
  FmEfdIcmsIpiE001_v03_0_2_a: TFmEfdIcmsIpiE001_v03_0_2_a;

implementation

uses UnMyObjects, Module, MyDBCheck, ModuleGeral, ModProd,
  UCreate, UnDmkProcFunc, CfgExpFile, UnGrade_Tabs, ModuleNFe_0000,
  UnEfdIcmsIpi_PF_v03_0_2_a, SPED_Listas, Principal, Periodo, UnALL_Jan,
  (*E FD_E111, E FD_E116, E FD_E112, E FD_E113, E FD_E115,
  E FD_E520, E FD_E530, E FD_H005, E FD_H010, E FD_H020, E FD_K100,
  E FD_K200, E FD_K220, *)
  //UnVS_PF, PQx,
  EfdIcmsIpiExporta_v03_0_9, SpedEfdIcmsIpi_v03_0_2_a,
  EfdIcmsIpiE100_v03_0_2_a, EfdIcmsIpiE110_v03_0_2_a, EfdIcmsIpiE500_v03_0_2_a, EfdIcmsIpiE520_v03_0_2_a,
  EfdIcmsIpiH005_v03_0_2_a,
  EfdIcmsIpiK100_v03_0_2_a, EfdIcmsIpiK200_v03_0_2_a, EfdIcmsIpiK210_v03_0_2_a, EfdIcmsIpiK215_v03_0_2_a,
  EfdIcmsIpiK220_v03_0_2_a,
  EfdIcmsIpiK230_v03_0_2_a, EfdIcmsIpiK235_v03_0_2_a,
  EfdIcmsIpiK250_v03_0_2_a, EfdIcmsIpiK255_v03_0_2_a,
  EfdIcmsIpiK280_v03_0_2_a,
  EfdIcmsIpiK290_v03_0_2_a, EfdIcmsIpiK291_v03_0_2_a, EfdIcmsIpiK292_v03_0_2_a,
  EfdIcmsIpiK300_v03_0_2_a, EfdIcmsIpiK301_v03_0_2_a, EfdIcmsIpiK302_v03_0_2_a,
  EfdIcmsIpi1010_v03_0_2_a, EfdIcmsIpiK270_v03_0_2_a;

{$R *.DFM}

const
  FFormatFloat = '00000';
  //DfSEII_v03_0_2_a.FImporExpor = 3; // 3=Criar! N�o Mexer

procedure TFmEfdIcmsIpiE001_v03_0_2_a.EdEmpresaChange(Sender: TObject);
var
  Empresa: Integer;
begin
  if EdEmpresa.ValueVariant <> 0 then
  begin
    Empresa := DModG.QrEmpresasCodigo.Value;
    DfSEII_v03_0_2_a.FEmpresa := Empresa;
    FEmprTXT := Geral.FF0(DfSEII_v03_0_2_a.FEmpresa);
    DfSEII_v03_0_2_a.FNO_Empresa := CBEmpresa.Text;
    DModG.ReopenParamsEmp(Empresa);
  end else
  begin
    DfSEII_v03_0_2_a.FEmpresa := 0;
    FEmprTXT := '0';
    DfSEII_v03_0_2_a.FNO_Empresa := '';
  end;
  BtPeriodo.Enabled := DfSEII_v03_0_2_a.FEmpresa <> 0;
  DfSEII_v03_0_2_a.ReopenE001(0);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Erroempresaitens1Click(Sender: TObject);
begin
  DfSEII_v03_0_2_a.ImprimeErrEmpresa();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiajustedaapuracaodoIPIselecionado1Click(
  Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o do ajuste selecionado?',
  Dmod.QrUpd, 'efdicmsipie530', ['ImporExpor', 'AnoMes', 'Empresa', 'E520', 'LinArq'
  ], ['=','=','=','=','='], [DfSEII_v03_0_2_a.QrE530ImporExpor.Value, DfSEII_v03_0_2_a.QrE530AnoMes.Value,
  DfSEII_v03_0_2_a.QrE530Empresa.Value, DfSEII_v03_0_2_a.QrE530E520.Value, DfSEII_v03_0_2_a.QrE530LinArq.Value], '') then
  begin
    AtualizaValoresE520deE530();
    DfSEII_v03_0_2_a.ReopenE500(DfSEII_v03_0_2_a.QrE500LinArq.Value);
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Excluiajusteselecionado1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o do ajuste selecionado?',
  Dmod.QrUpd, 'efdicmsipie111', ['ImporExpor', 'AnoMes', 'Empresa', 'E110', 'LinArq'
  ], ['=','=','=','=','='], [DfSEII_v03_0_2_a.QrE111ImporExpor.Value, DfSEII_v03_0_2_a.QrE111AnoMes.Value,
  DfSEII_v03_0_2_a.QrE111Empresa.Value, DfSEII_v03_0_2_a.QrE111E110.Value, DfSEII_v03_0_2_a.QrE111LinArq.Value], '') then
    DfSEII_v03_0_2_a.ReopenE111(DfSEII_v03_0_2_a.QrE111LinArq.Value);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Excluiidentificaoselecionada1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o da identifica��o de documento selecionada?',
  Dmod.QrUpd, 'efdicmsipie113', ['ImporExpor', 'AnoMes', 'Empresa', 'E111', 'LinArq'
  ], ['=','=','=','=','='], [DfSEII_v03_0_2_a.QrE113ImporExpor.Value, DfSEII_v03_0_2_a.QrE113AnoMes.Value,
  DfSEII_v03_0_2_a.QrE113Empresa.Value, DfSEII_v03_0_2_a.QrE113E111.Value, DfSEII_v03_0_2_a.QrE113LinArq.Value], '') then
    DfSEII_v03_0_2_a.ReopenE113(DfSEII_v03_0_2_a.QrE113LinArq.Value);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Excluiinformaoadicionalselecionada1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o da informa��o adicional selecionada?',
  Dmod.QrUpd, 'efdicmsipie112', ['ImporExpor', 'AnoMes', 'Empresa', 'E111', 'LinArq'
  ], ['=','=','=','=','='], [DfSEII_v03_0_2_a.QrE112ImporExpor.Value, DfSEII_v03_0_2_a.QrE112AnoMes.Value,
  DfSEII_v03_0_2_a.QrE112Empresa.Value, DfSEII_v03_0_2_a.QrE112E111.Value, DfSEII_v03_0_2_a.QrE112LinArq.Value], '') then
    DfSEII_v03_0_2_a.ReopenE112(DfSEII_v03_0_2_a.QrE112LinArq.Value);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Excluiinformaocomplementar1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o da informa��o adicional selecionada?',
  Dmod.QrUpd, 'efdicmsipih020', ['ImporExpor', 'AnoMes', 'Empresa', 'H010', 'LinArq'
  ], ['=','=','=','=','='], [DfSEII_v03_0_2_a.QrH020ImporExpor.Value, DfSEII_v03_0_2_a.QrH020AnoMes.Value,
  DfSEII_v03_0_2_a.QrH020Empresa.Value, DfSEII_v03_0_2_a.QrH020H010.Value, DfSEII_v03_0_2_a.QrH020LinArq.Value], '') then
    DfSEII_v03_0_2_a.ReopenH020(DfSEII_v03_0_2_a.QrH020LinArq.Value);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiintervalodeProduoeEstoque1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do intervalo e seus valores?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
(*
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipik110', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [DfSEII_v03_0_2_a.QrK110ImporExpor.Value, DfSEII_v03_0_2_a.QrK110AnoMes.Value,
    DfSEII_v03_0_2_a.QrK110Empresa.Value, DfSEII_v03_0_2_a.QrK110LinArq.Value], '') then
    //
*)
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipik100', [
    'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], ['=','=','=','='],
    [DfSEII_v03_0_2_a.QrK100ImporExpor.Value, DfSEII_v03_0_2_a.QrK100AnoMes.Value,
    DfSEII_v03_0_2_a.QrK100Empresa.Value, DfSEII_v03_0_2_a.QrK100PeriApu.Value], '') then
(*
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipik100', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [DfSEII_v03_0_2_a.QrK100ImporExpor.Value, DfSEII_v03_0_2_a.QrK100AnoMes.Value,
    DfSEII_v03_0_2_a.QrK100Empresa.Value, DfSEII_v03_0_2_a.QrK100PeriApu.Value], '') then
*)
    //
    DfSEII_v03_0_2_a.ReopenK100(0);
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiintervaloIPIselecionado1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do intervalo e seus valores?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipie110', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [DfSEII_v03_0_2_a.QrE110ImporExpor.Value, DfSEII_v03_0_2_a.QrE110AnoMes.Value,
    DfSEII_v03_0_2_a.QrE110Empresa.Value, DfSEII_v03_0_2_a.QrE110LinArq.Value], '') then
    //
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipie500', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [DfSEII_v03_0_2_a.QrE500ImporExpor.Value, DfSEII_v03_0_2_a.QrE500AnoMes.Value,
    DfSEII_v03_0_2_a.QrE500Empresa.Value, DfSEII_v03_0_2_a.QrE500LinArq.Value], '') then
    //
    DfSEII_v03_0_2_a.ReopenE500(0);
  end;

end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Excluiintervaloselecionado1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do intervalo e seus valores?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipie110', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [DfSEII_v03_0_2_a.QrE110ImporExpor.Value, DfSEII_v03_0_2_a.QrE110AnoMes.Value,
    DfSEII_v03_0_2_a.QrE110Empresa.Value, DfSEII_v03_0_2_a.QrE110LinArq.Value], '') then
    //
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipie100', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=','='],
    [DfSEII_v03_0_2_a.QrE100ImporExpor.Value, DfSEII_v03_0_2_a.QrE100AnoMes.Value,
    DfSEII_v03_0_2_a.QrE100Empresa.Value, DfSEII_v03_0_2_a.QrE100LinArq.Value], '') then
    //
    DfSEII_v03_0_2_a.ReopenE100(0);
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Excluiinventrio1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do invent�rio selecionado?') = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipih005', [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], ['=','=','=', '='],
    [DfSEII_v03_0_2_a.QrH005ImporExpor.Value, DfSEII_v03_0_2_a.QrH005AnoMes.Value,
    DfSEII_v03_0_2_a.QrH005Empresa.Value, DfSEII_v03_0_2_a.QrH005LinArq.Value], '') then
    DfSEII_v03_0_2_a.ReopenH005(0);
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Excluiitemns1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrH010, TDBGrid(DBGH010),
    'efdicmsipih010', ['ImporExpor', 'AnoMes', 'Empresa', 'H005', 'LinArq'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'H005', 'LinArq'], istPergunta, '');
  AtualizaValoresH005deH010();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiItensDeTabela(Tabela: String);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'DELETE FROM ' + Tabela +
  ' WHERE ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value) +
  ' AND AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value) +
  ' AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value) +
  ';']);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiItensK27XParcialTipoEFD(
  OriSPEDEFDKnd: TOrigemSPEDEFDKnd);
begin
  if DfSEII_v03_0_2_a.QrK100.RecordCount > 0 then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipik275', [
    'ImporExpor', 'AnoMes', 'Empresa', 'OriSPEDEFDKnd'],
    ['=','=','=','='],
    [DfSEII_v03_0_2_a.QrK100ImporExpor.Value, DfSEII_v03_0_2_a.QrK100AnoMes.Value,
    DfSEII_v03_0_2_a.QrK100Empresa.Value, Integer(OriSPEDEFDKnd)], '') then
    begin
      if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipik270', [
      'ImporExpor', 'AnoMes', 'Empresa', 'OriSPEDEFDKnd'],
      ['=','=','=','='],
      [DfSEII_v03_0_2_a.QrK100ImporExpor.Value, DfSEII_v03_0_2_a.QrK100AnoMes.Value,
      DfSEII_v03_0_2_a.QrK100Empresa.Value, Integer(OriSPEDEFDKnd)], '') then
      begin
        DfSEII_v03_0_2_a.ReopenK270_210(0);
        DfSEII_v03_0_2_a.ReopenK270_220(0);
        DfSEII_v03_0_2_a.ReopenK270_230(0);
        DfSEII_v03_0_2_a.ReopenK270_250(0);
        DfSEII_v03_0_2_a.ReopenK270_260(0);
      end;
    end;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiItensK280ParcialTipoEFD(
  OriSPEDEFDKnd: TOrigemSPEDEFDKnd);
begin
  if DfSEII_v03_0_2_a.QrK100.RecordCount > 0 then
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipik280', [
    'ImporExpor', 'AnoMes', 'Empresa', 'OriSPEDEFDKnd'],
    ['=','=','=','='],
    [DfSEII_v03_0_2_a.QrK100ImporExpor.Value, DfSEII_v03_0_2_a.QrK100AnoMes.Value,
    DfSEII_v03_0_2_a.QrK100Empresa.Value, Integer(OriSPEDEFDKnd)], '') then
      DfSEII_v03_0_2_a.ReopenK280(0, 0, 0, rkGrade);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiItensK7X(ImporExpor, AnoMes, Empresa: Integer;
  SohEspecifico: TSPED_EFD_KndRegOrigem);
const
  sProcName = 'FmEfdIcmsIpiE001.ExcluiItensK7X()';
var
  StrOri: String;
begin
  StrOri := Geral.FF0(Integer(SohEspecifico));
  if Integer(SohEspecifico) <> 0 then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipik270', [
    'ImporExpor', 'AnoMes', 'Empresa', 'ORIGEM'], [
    '=','=','=','='],
    [ImporExpor, AnoMes, Empresa, StrOri], '') then
    //
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipik275', [
    'ImporExpor', 'AnoMes', 'Empresa', 'ORIGEM'], [
    '=','=','=','='],
    [ImporExpor, AnoMes, Empresa, StrOri], '') then
    //
    case SohEspecifico of
      (*1*)sek2708oriK230K235: DfSEII_v03_0_2_a.ReopenK270_230(0);
      (*2*)sek2708oriK250K255: DfSEII_v03_0_2_a.ReopenK270_250(0);
      (*3*)sek2708oriK210K215: DfSEII_v03_0_2_a.ReopenK270_210(0);
      (*4*)sek2708oriK260K265: DfSEII_v03_0_2_a.ReopenK270_260(0);
      (*5*)sek2708oriK220:     DfSEII_v03_0_2_a.ReopenK270_220(0);
      (*6*)sek2708oriK291:     DfSEII_v03_0_2_a.ReopenK270_291(0);
      (*7*)sek2708oriK292:     DfSEII_v03_0_2_a.ReopenK270_292(0);
      (*8*)sek2708oriK301:     DfSEII_v03_0_2_a.ReopenK270_301(0);
      (*9*)sek2708oriK302:     DfSEII_v03_0_2_a.ReopenK270_302(0);
      else Geral.MB_Aviso('Reopen n�o implementado em ' + sProcName);
    end;
  end else
  begin
    //if Geral.MB_Pergunta(
    //'Confirma a exclus�o de todos itens corre��o de apontamento do periodo selecionado?'
    //) = ID_YES then
    begin
      if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipik270', [
      'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
      [ImporExpor, AnoMes, Empresa], '') then
      //
      if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipik275', [
      'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
      [ImporExpor, AnoMes, Empresa], '') then
      //
      DfSEII_v03_0_2_a.ReopenK270_210(0);
      DfSEII_v03_0_2_a.ReopenK270_220(0);
      DfSEII_v03_0_2_a.ReopenK270_230(0);
      DfSEII_v03_0_2_a.ReopenK270_250(0);
      DfSEII_v03_0_2_a.ReopenK270_260(0);
    end;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiItensNormaisDeProducao();
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o de todos os registros de produ��o?') = ID_YES then
  begin
    ExcluiItensDeTabela('efdicmsipik210');
    ExcluiItensDeTabela('efdicmsipik215');
    ExcluiItensDeTabela('efdicmsipik230');
    ExcluiItensDeTabela('efdicmsipik235');
    ExcluiItensDeTabela('efdicmsipik23subprd');
    ExcluiItensDeTabela('efdicmsipik250');
    ExcluiItensDeTabela('efdicmsipik255');
    ExcluiItensDeTabela('efdicmsipik25subprd');
    ExcluiItensDeTabela('efdicmsipik260');
    ExcluiItensDeTabela('efdicmsipik265');
    ExcluiItensDeTabela('efdicmsipik290');
    ExcluiItensDeTabela('efdicmsipik291');
    ExcluiItensDeTabela('efdicmsipik292');
    ExcluiItensDeTabela('efdicmsipik29subprd');
    ExcluiItensDeTabela('efdicmsipik300');
    ExcluiItensDeTabela('efdicmsipik301');
    ExcluiItensDeTabela('efdicmsipik302');
    ExcluiItensDeTabela('efdicmsipik30subprd');
    //
    ExcluiItensK7X(DfSEII_v03_0_2_a.QrE001ImporExpor.Value, DfSEII_v03_0_2_a.QrE001AnoMes.Value,
      DfSEII_v03_0_2_a.QrE001Empresa.Value, sek2708oriK210K215);
    ExcluiItensK7X(DfSEII_v03_0_2_a.QrE001ImporExpor.Value, DfSEII_v03_0_2_a.QrE001AnoMes.Value,
      DfSEII_v03_0_2_a.QrE001Empresa.Value, sek2708oriK230K235);
    ExcluiItensK7X(DfSEII_v03_0_2_a.QrE001ImporExpor.Value, DfSEII_v03_0_2_a.QrE001AnoMes.Value,
      DfSEII_v03_0_2_a.QrE001Empresa.Value, sek2708oriK250K255);
    //
    DfSEII_v03_0_2_a.ReopenK210(0);
    DfSEII_v03_0_2_a.ReopenK230(0);
    DfSEII_v03_0_2_a.ReopenK250(0);
    DfSEII_v03_0_2_a.ReopenK260(0);
    DfSEII_v03_0_2_a.ReopenK290(0);
    DfSEII_v03_0_2_a.ReopenK300(0);
    DfSEII_v03_0_2_a.ReopenK270_210(0);
    DfSEII_v03_0_2_a.ReopenK270_230(0);
    DfSEII_v03_0_2_a.ReopenK270_250(0);
    DfSEII_v03_0_2_a.ReopenK270_291(0);
    DfSEII_v03_0_2_a.ReopenK270_292(0);
    DfSEII_v03_0_2_a.ReopenK270_301(0);
    DfSEII_v03_0_2_a.ReopenK270_302(0);
    DfSEII_v03_0_2_a.ReopenK280(0, 0, 0, rkGrade);
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiK200_1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK200, TDBGrid(DBGK200),
    'efdicmsipik200', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiK200_280_1Click(Sender: TObject);
begin
  ExcluiRegsitrosImportados_Estoque();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiK220_1Click(Sender: TObject);
  procedure ExcluiSelecionados();
    procedure ExcluiItemAtual(Reabre: Boolean);
    begin
      DBCheck.ExcluiRegistro(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK220, 'efdicmsipik220',
      ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'],
      ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'], Reabre, '');
    end;
  var
    q: TSelType;
    n, c: Integer;
  begin
    DBCheck.Quais_Selecionou(DfSEII_v03_0_2_a.QrK220, TDBGrid(DBGK220), q);
    case q of
      istAtual: ExcluiItemAtual(True);
      istSelecionados:
      begin
        with DBGK220.DataSource.DataSet do
        for n := 0 to DBGK220.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(DBGK220.SelectedRows.Items[n]));
          GotoBookmark(DBGK220.SelectedRows.Items[n]);
          ExcluiItemAtual(False);
        end;
        DfSEII_v03_0_2_a.ReopenK220(0, 0);
        DfSEII_v03_0_2_a.ReopenK270_220(0);
      end;
      istTodos:
      begin
        DfSEII_v03_0_2_a.QrK220.First;
        while not DfSEII_v03_0_2_a.QrK220.Eof do
        begin
          ExcluiItemAtual(False);
          //
          DfSEII_v03_0_2_a.QrK220.Next;
        end;
        ExcluiItensDeTabela('efdicmsipik23subprd');
        ExcluiItensDeTabela('efdicmsipik25subprd');
        ExcluiItensK7X(DfSEII_v03_0_2_a.QrE001ImporExpor.Value, DfSEII_v03_0_2_a.QrE001AnoMes.Value,
          DfSEII_v03_0_2_a.QrE001Empresa.Value, sek2708oriK220);
        DfSEII_v03_0_2_a.ReopenK220(0, 0);
        DfSEII_v03_0_2_a.ReopenK270_220(0);
      end;
    end;
  end;
begin
{
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK220, TDBGrid(DBGK220),
    'efdicmsipik220', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq?],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq?], istPergunta, '');
  //  Problema aqui! excluindo tudo sempre! mesmo que escolhe soh um.
  //  Resolver no futuro com procedure ExcluiSelecionados(); acima!
  ExcluiItensDeTabela('efdicmsipik23subprd');
  ExcluiItensDeTabela('efdicmsipik25subprd');
}
  ExcluiSelecionados();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiK220_280_1Click(Sender: TObject);
begin
  ExcluiRegsitrosImportados_Classe();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiK230_280_1Click(Sender: TObject);
begin
  ExcluiRegsitrosImportados_Producao();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiK280_1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK280, TDBGrid(DBGK280),
    'efdicmsipik280', [
    'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'KndTab', 'KndCod', 'KndNSU',
    'KndItm', 'DT_EST', 'DebCred', 'GraGruX'],
    [
    'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'KndTab', 'KndCod', 'KndNSU',
    'KndItm', 'DT_EST', 'DebCred', 'GraGruX'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Excluiobrigaoselecionada1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a exclus�o da obriga�ao selecionada?',
  Dmod.QrUpd, 'efdicmsipie116', ['ImporExpor', 'AnoMes', 'Empresa', 'E110', 'LinArq'
  ], ['=','=','=','=','='], [DfSEII_v03_0_2_a.QrE116ImporExpor.Value, DfSEII_v03_0_2_a.QrE116AnoMes.Value,
  DfSEII_v03_0_2_a.QrE116Empresa.Value, DfSEII_v03_0_2_a.QrE116E110.Value, DfSEII_v03_0_2_a.QrE116LinArq.Value], '') then
    DfSEII_v03_0_2_a.ReopenE116(DfSEII_v03_0_2_a.QrE116LinArq.Value);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Excluiperiodoselecionado1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do per�odo selecionado?',
  'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipie001', [
    'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
    [DfSEII_v03_0_2_a.QrE001ImporExpor.Value, DfSEII_v03_0_2_a.QrE001AnoMes.Value,
    DfSEII_v03_0_2_a.QrE001Empresa.Value], '') then
    DfSEII_v03_0_2_a.ReopenE001(0);
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Excluiregistrosd1Click(Sender: TObject);
begin
  ExcluiRegsitrosImportados_Estoque();
  ExcluiRegsitrosImportados_Classe();
  ExcluiRegsitrosImportados_Producao();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiRegsitrosImportados_Classe();
begin
  if Geral.MB_Pergunta('Confirma a exclus�o de todos itens de classe?') =
  ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipik220', [
    'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
    [DfSEII_v03_0_2_a.QrK220ImporExpor.Value, DfSEII_v03_0_2_a.QrK220AnoMes.Value,
    DfSEII_v03_0_2_a.QrK220Empresa.Value], '') then
    begin
      DfSEII_v03_0_2_a.ReopenK220(0, 0); //, rkGrade);
      //
      ExcluiItensK27XParcialTipoEFD(TOrigemSPEDEFDKnd.osekClasse);
      ExcluiItensK280ParcialTipoEFD(TOrigemSPEDEFDKnd.osekClasse);
    end;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiRegsitrosImportados_Estoque();
begin
  if Geral.MB_Pergunta('Confirma a exclus�o de todos itens de estoque?') =
  ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipik200', [
    'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
    [DfSEII_v03_0_2_a.QrK200ImporExpor.Value, DfSEII_v03_0_2_a.QrK200AnoMes.Value,
    DfSEII_v03_0_2_a.QrK200Empresa.Value], '') then
    begin
      DfSEII_v03_0_2_a.ReopenK200(0, 0, 0, rkGrade);
      //
      ExcluiItensK27XParcialTipoEFD(TOrigemSPEDEFDKnd.osekEstoque);
      ExcluiItensK280ParcialTipoEFD(TOrigemSPEDEFDKnd.osekEstoque);
    end;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluiRegsitrosImportados_Producao();
begin
  ExcluiItensNormaisDeProducao();
  ExcluiItensK27XParcialTipoEFD(osekProducao);
  ExcluiItensK280ParcialTipoEFD(osekProducao);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ExcluirTODASTABELASK1Click(Sender: TObject);
begin
(*
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  if Geral.MB_Pergunta('Deseja EXCLUIR TODAS TABELAS E FD_K... e SPEDEFDK...?')=
  ID_YES then
  begin
    if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DROP TABLE IF EXISTS SPEDEFDK001; ',
    'DROP TABLE IF EXISTS SPEDEFDK100; ',
    'DROP TABLE IF EXISTS SPEDEFDK200; ',
    'DROP TABLE IF EXISTS SPEDEFDK210; ',
    'DROP TABLE IF EXISTS SPEDEFDK215; ',
    'DROP TABLE IF EXISTS SPEDEFDK220; ',
    'DROP TABLE IF EXISTS SPEDEFDK230; ',
    'DROP TABLE IF EXISTS SPEDEFDK235; ',
    'DROP TABLE IF EXISTS SPEDEFDK250; ',
    'DROP TABLE IF EXISTS SPEDEFDK255; ',
    'DROP TABLE IF EXISTS SPEDEFDK260; ',
    'DROP TABLE IF EXISTS SPEDEFDK265; ',
    'DROP TABLE IF EXISTS SPEDEFDK270; ',
    'DROP TABLE IF EXISTS SPEDEFDK275; ',
    'DROP TABLE IF EXISTS SPEDEFDK280; ',
    'DROP TABLE IF EXISTS SPEDEFDK990; ',
    ' ',
    'DROP TABLE IF EXISTS E FD_K100; ',
    'DROP TABLE IF EXISTS E FD_K200; ',
    'DROP TABLE IF EXISTS E FD_K210; ',
    'DROP TABLE IF EXISTS E FD_K215; ',
    'DROP TABLE IF EXISTS E FD_K220; ',
    'DROP TABLE IF EXISTS E FD_K230; ',
    'DROP TABLE IF EXISTS E FD_K235; ',
    'DROP TABLE IF EXISTS E FD_K23SubPrd; ',
    'DROP TABLE IF EXISTS E FD_K250; ',
    'DROP TABLE IF EXISTS E FD_K255; ',
    'DROP TABLE IF EXISTS E FD_K260; ',
    'DROP TABLE IF EXISTS E FD_K265; ',
    'DROP TABLE IF EXISTS E FD_K270; ',
    'DROP TABLE IF EXISTS E FD_K275; ',
    'DROP TABLE IF EXISTS E FD_K280; ',
    'DROP TABLE IF EXISTS E FD_K25SubPrd; ',
    'DROP TABLE IF EXISTS E FD_K_ConfG; ',
    '']) then
      Geral.MB_Info('SQL de DROP TABLE executada sem erros!');
  end;
  ALL_Jan.MostraFormVerifiDB();
  //
  Close;
*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Excluisegmentointeiroimportado1Click(Sender: TObject);
  procedure ExcluiSegmento(Segmento: Integer);
  var
    AnoMes: Integer;
    //
    procedure ExcluiSegmentoDeTabela(Tabela: String);
    begin
      Dmod.MyDB.Execute('DELETE FROM ' + Tabela +
      ' WHERE AnoMes=' + Geral.FF0(AnoMes) +
      ' AND OriOpeProc=' + Geral.FF0(Segmento));
    end;
  begin
    AnoMes := DfSEII_v03_0_2_a.QrE001AnoMes.Value;
    ExcluiSegmentoDeTabela('efdicmsipik210');
    ExcluiSegmentoDeTabela('efdicmsipik215');
    ExcluiSegmentoDeTabela('efdicmsipik230');
    ExcluiSegmentoDeTabela('efdicmsipik235');
    ExcluiSegmentoDeTabela('efdicmsipik250');
    ExcluiSegmentoDeTabela('efdicmsipik255');
  end;
var
  Segmento, Item: Integer;
begin
(*&�%
  Item := MyObjects.SelRadioGroup('Exclus�o', 'Segmento', sSPED_KPROD_SEGM, -1);
  if Item >= 0 then
  begin
    Screen.Cursor := crHourGlass;
    try
      Segmento := Item + 1;
      ExcluiSegmento(Segmento);
    finally
      Screen.Cursor := crDefault;
    end;
    Geral.MB_Info('Exclus�o executada!');
  end;
%�&*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Excluivalordeclaratrioselecionado1Click(Sender: TObject);
begin
  if UMyMod.ExcluiRegistros('Confirma a declaral��o de valor selacionada?',
  Dmod.QrUpd, 'efdicmsipie115', ['ImporExpor', 'AnoMes', 'Empresa', 'E110', 'LinArq'
  ], ['=','=','=','=','='], [DfSEII_v03_0_2_a.QrE115ImporExpor.Value, DfSEII_v03_0_2_a.QrE115AnoMes.Value,
  DfSEII_v03_0_2_a.QrE115Empresa.Value, DfSEII_v03_0_2_a.QrE115E110.Value, DfSEII_v03_0_2_a.QrE115LinArq.Value], '') then
    DfSEII_v03_0_2_a.ReopenE115(DfSEII_v03_0_2_a.QrE115LinArq.Value);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Importadebalano1Click(Sender: TObject);
begin
  EfdIcmsIpi_Jan_v03_0_2_a.MostraFormSpedEfdIcmsIpiBalancos(DfSEII_v03_0_2_a.QrH005ImporExpor.Value,
    DfSEII_v03_0_2_a.QrH005AnoMes.Value, DfSEII_v03_0_2_a.QrH005Empresa.Value, DfSEII_v03_0_2_a.QrH005LinArq.Value,
    'H010', DfSEII_v03_0_2_a.QrH005DT_INV.Value);
  AtualizaValoresH005deH010();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ImportaK200_1Click(Sender: TObject);
begin
  EfdIcmsIpi_Jan_v03_0_2_a.MostraFormSpedEfdIcmsIpiBalancos(DfSEII_v03_0_2_a.QrK100ImporExpor.Value,
    DfSEII_v03_0_2_a.QrK100AnoMes.Value, DfSEII_v03_0_2_a.QrK100Empresa.Value, DfSEII_v03_0_2_a.QrK100PeriApu.Value,
    'K200', DfSEII_v03_0_2_a.QrK100DT_FIN.Value);
  DfSEII_v03_0_2_a.ReopenK200(0, 0, 0, rkGrade);
  DfSEII_v03_0_2_a.ReopenK280(0, 0, 0, rkGrade);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ImportaK220_1Click(Sender: TObject);
begin
  EfdIcmsIpi_Jan_v03_0_2_a.MostraFormSpedEfdIcmsIpiClaProd(DfSEII_v03_0_2_a.QrK100ImporExpor.Value,
    DfSEII_v03_0_2_a.QrK100AnoMes.Value, DfSEII_v03_0_2_a.QrK100Empresa.Value, DfSEII_v03_0_2_a.QrK100PeriApu.Value,
    'K220', DfSEII_v03_0_2_a.QrK100DT_FIN.Value);
  DfSEII_v03_0_2_a.ReopenK220(0, 0);
  DfSEII_v03_0_2_a.ReopenK270_220(0);
  DfSEII_v03_0_2_a.ReopenK280(0, 0, 0, rkGrade);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ImportaK230_1Click(Sender: TObject);
begin
  EfdIcmsIpi_Jan_v03_0_2_a.MostraFormSpedEfdIcmsIpiClaProd(DfSEII_v03_0_2_a.QrK100ImporExpor.Value,
    DfSEII_v03_0_2_a.QrK100AnoMes.Value, DfSEII_v03_0_2_a.QrK100Empresa.Value, DfSEII_v03_0_2_a.QrK100PeriApu.Value,
    'K230', DfSEII_v03_0_2_a.QrK100DT_FIN.Value);
  DfSEII_v03_0_2_a.ReopenK210(0);
  DfSEII_v03_0_2_a.ReopenK230(0);
  DfSEII_v03_0_2_a.ReopenK250(0);
  DfSEII_v03_0_2_a.ReopenK260(0);
  DfSEII_v03_0_2_a.ReopenK290(0);
  DfSEII_v03_0_2_a.ReopenK300(0);
  DfSEII_v03_0_2_a.ReopenK270_210(0);
  DfSEII_v03_0_2_a.ReopenK270_230(0);
  DfSEII_v03_0_2_a.ReopenK270_250(0);
  DfSEII_v03_0_2_a.ReopenK270_291(0);
  DfSEII_v03_0_2_a.ReopenK270_292(0);
  DfSEII_v03_0_2_a.ReopenK270_301(0);
  DfSEII_v03_0_2_a.ReopenK270_302(0);
  DfSEII_v03_0_2_a.ReopenK280(0, 0, 0, rkGrade);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Importatodosregistrosdeprooduo1Click(
  Sender: TObject);
begin
  ImportaK200_1Click(ImportaK200_1);
  ImportaK220_1Click(ImportaK220_1);
  ImportaK230_1Click(ImportaK230_1);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.IncluiajustedaapuracaodoIPI1Click(Sender: TObject);
begin
  InsUpdE530(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Incluidatadeinventrio1Click(Sender: TObject);
begin
  InsUpdH005(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Incluiinformaocomplementar1Click(Sender: TObject);
begin
  InsUpdH020(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Incluiitem1Click(Sender: TObject);
begin
  InsUpdH010(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.IncluiK200_1Click(Sender: TObject);
begin
  InsUpdK200(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.IncluiK210_1Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 1;
  InsUpdK210(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.IncluiK215_1Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 1;
  InsUpdK215(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.IncluiK220_1Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 2;
  InsUpdK220(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.IncluiK280_1Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 4;
  InsUpdK280(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Incluinovaidentificaodedocumentofiscal1Click(
  Sender: TObject);
begin
  InsUpdE113(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Incluinovainformaoadicional1Click(Sender: TObject);
begin
  InsUpdE112(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.IncluinovaobrigaodoICMSarecolher1Click(Sender: TObject);
begin
  InsUpdE116(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Incluinovoajuste1Click(Sender: TObject);
begin
  InsUpdE111(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Incluinovointervalo1Click(Sender: TObject);
begin
  InsUpdE100(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.IncluinovointervalodeProduoeEstoque1Click(
  Sender: TObject);
begin
  InsUpdK100(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.IncluinovointervaloIPI1Click(Sender: TObject);
begin
  InsUpdE500(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Incluinovoperiodo1Click(Sender: TObject);
const
  LinArq = 0;
  REG = 'E001';
  IND_MOV = 1;
var
  Ano, Mes, Dia: Word;
  Cancelou, Continua: Boolean;
  AnoMes, MovimXX: Integer;
begin
  Continua := False;
  DecodeDate(Date, Ano, Mes, Dia);
  MLAGeral.EscolhePeriodo_MesEAno(TFmPeriodo, FmPeriodo, Mes, Ano, Cancelou, True, True);
  if not Cancelou then
  begin
    AnoMes := (Ano * 100) + Mes;
    //ShowMessage(Geral.FF0((Ano * 100) + Mes));
    if not PeriodoJaExiste(DfSEII_v03_0_2_a.FImporExpor, AnoMes, DfSEII_v03_0_2_a.FEmpresa, True) then
    begin
(* /////////////////////////////////////////////////////////////////////////////
      Permitir abrir pois n�o atrapalha e na verdade impede de lan�ar
      mais info n�o retroativo corretamente!

      if not EfdIcmsIpi_PF.LiberaAcaoVS_SPED(AnoMes, DfSEII_v03_0_2_a.FEmpresa,
      TEstagioVS_SPED.evsspedEncerraVS) then
        Exit;
//////////////////////////////////////////////////////////////////////////////*)
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efdicmsipie001', False, [
      'LinArq', 'REG', 'IND_MOV'], [
      'ImporExpor', 'AnoMes', 'Empresa'], [
      LinArq, REG, IND_MOV], [
      DfSEII_v03_0_2_a.FImporExpor, AnoMes, DfSEII_v03_0_2_a.FEmpresa], True) then
        DfSEII_v03_0_2_a.ReopenE001(AnoMes);
    end;
    DfSEII_v03_0_2_a.ReopenE001(AnoMes);
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Incluinovovalordeclaratrio1Click(Sender: TObject);
begin
  InsUpdE115(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdE100(SQLType: TSQLType);
const
  Registro = 'E100';
var
  Ini, Fim: TDateTime;
  Ano, Mes: Integer;
begin
  if UmyMod.FormInsUpd_Cria(TFmEfdIcmsIpiE100_v03_0_2_a, FmEfdIcmsIpiE100_v03_0_2_a, afmoNegarComAviso,
  DfSEII_v03_0_2_a.QrE100, SQLType) then
  begin
    if SQLType = stIns then
    begin
      if not EfdIcmsIpi_PF.DefineDatasPeriodoSPED(DfSEII_v03_0_2_a.FEmpresa,
      DfSEII_v03_0_2_a.QrE001AnoMes.Value, 'E100',
      DfSEII_v03_0_2_a.QrE100DT_INI.Value, DfSEII_v03_0_2_a.QrE100DT_FIN.Value, Ini, Fim) then ;
    end else
    begin
      Ini := DfSEII_v03_0_2_a.QrE100DT_INI.Value;
      Fim := DfSEII_v03_0_2_a.QrE100DT_FIN.Value;
    end;
    FmEfdIcmsIpiE100_v03_0_2_a.ImgTipo.SQLType := SQLType;
    FmEfdIcmsIpiE100_v03_0_2_a.EdImporExpor.ValueVariant := DfSEII_v03_0_2_a.QrE001ImporExpor.Value;
    FmEfdIcmsIpiE100_v03_0_2_a.EdAnoMes.ValueVariant := DfSEII_v03_0_2_a.QrE001AnoMes.Value;
    FmEfdIcmsIpiE100_v03_0_2_a.EdEmpresa.ValueVariant := DfSEII_v03_0_2_a.QrE001Empresa.Value;
    //
    FmEfdIcmsIpiE100_v03_0_2_a.TPDT_INI.Date := Ini;
    FmEfdIcmsIpiE100_v03_0_2_a.TPDT_FIN.Date := Fim;
    //
    FmEfdIcmsIpiE100_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiE100_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdE111(SQLType: TSQLType);
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "E111"');
(*
  if DBCheck.CriaFm(TFmEfdIcmsIpiE111, FmEfdIcmsIpiE111, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiE111.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiE111.TPDT_INI.Date := DfSEII_v03_0_2_a.QrE100DT_INI.Value;
    FmEfdIcmsIpiE111.TPDT_FIN.Date := DfSEII_v03_0_2_a.QrE100DT_FIN.Value;
    //
    FmEfdIcmsIpiE111.EdImporExpor.ValueVariant := DfSEII_v03_0_2_a.QrE110ImporExpor.Value;
    FmEfdIcmsIpiE111.EdAnoMes.ValueVariant := DfSEII_v03_0_2_a.QrE110AnoMes.Value;
    FmEfdIcmsIpiE111.EdEmpresa.ValueVariant := DfSEII_v03_0_2_a.QrE110Empresa.Value;
    FmEfdIcmsIpiE111.EdE110.ValueVariant := DfSEII_v03_0_2_a.QrE110LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiE111.EdLinArq.ValueVariant := DfSEII_v03_0_2_a.QrE111LinArq.Value;
      //
      FmEfdIcmsIpiE111.EdCOD_AJ_APUR.ValueVariant := DfSEII_v03_0_2_a.QrE111COD_AJ_APUR.Value;
      FmEfdIcmsIpiE111.EdDESCR_COMPL_AJ.ValueVariant := DfSEII_v03_0_2_a.QrE111DESCR_COMPL_AJ.Value;
      FmEfdIcmsIpiE111.EdVL_AJ_APUR.ValueVariant := DfSEII_v03_0_2_a.QrE111VL_AJ_APUR.Value;
    end;
    //
    FmEfdIcmsIpiE111.ShowModal;
    FmEfdIcmsIpiE111.Destroy;
  end;
*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdE112(SQLType: TSQLType);
  function DefineItem_IND_PROC(x: String): Integer;
  begin
    if x = '0' then Result := 1 else
    if x = '1' then Result := 2 else
    if x = '2' then Result := 3 else
    if x = '9' then Result := 4 else
                    Result := 0;
  end;
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "E112"');
(*
  if DBCheck.CriaFm(TFmEfdIcmsIpiE112, FmEfdIcmsIpiE112, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiE112.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiE112.TPDT_INI.Date := DfSEII_v03_0_2_a.QrE100DT_INI.Value;
    FmEfdIcmsIpiE112.TPDT_FIN.Date := DfSEII_v03_0_2_a.QrE100DT_FIN.Value;
    //
    FmEfdIcmsIpiE112.EdImporExpor.ValueVariant := DfSEII_v03_0_2_a.QrE111ImporExpor.Value;
    FmEfdIcmsIpiE112.EdAnoMes.ValueVariant := DfSEII_v03_0_2_a.QrE111AnoMes.Value;
    FmEfdIcmsIpiE112.EdEmpresa.ValueVariant := DfSEII_v03_0_2_a.QrE111Empresa.Value;
    FmEfdIcmsIpiE112.EdE111.ValueVariant := DfSEII_v03_0_2_a.QrE111LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiE112.EdLinArq.ValueVariant := DfSEII_v03_0_2_a.QrE112LinArq.Value;
      //
      FmEfdIcmsIpiE112.EdNUM_DA.Text := DfSEII_v03_0_2_a.QrE112NUM_DA.Value;
      FmEfdIcmsIpiE112.EdNUM_PROC.Text := DfSEII_v03_0_2_a.QrE112NUM_PROC.Value;
      FmEfdIcmsIpiE112.RGIND_PROC.ItemIndex := DefineItem_IND_PROC(DfSEII_v03_0_2_a.QrE112IND_PROC.Value);
      FmEfdIcmsIpiE112.EdPROC.Text := DfSEII_v03_0_2_a.QrE112PROC.Value;
      FmEfdIcmsIpiE112.EdTXT_COMPL.Text := DfSEII_v03_0_2_a.QrE112TXT_COMPL.Value;
    end;
    //
    FmEfdIcmsIpiE112.ShowModal;
    FmEfdIcmsIpiE112.Destroy;
  end;
*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdE113(SQLType: TSQLType);
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "E113"');
(*
  if DBCheck.CriaFm(TFmEfdIcmsIpiE113, FmEfdIcmsIpiE113, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiE113.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiE113.TPDT_INI.Date := DfSEII_v03_0_2_a.QrE100DT_INI.Value;
    FmEfdIcmsIpiE113.TPDT_FIN.Date := DfSEII_v03_0_2_a.QrE100DT_FIN.Value;
    //
    FmEfdIcmsIpiE113.EdImporExpor.ValueVariant := DfSEII_v03_0_2_a.QrE111ImporExpor.Value;
    FmEfdIcmsIpiE113.EdAnoMes.ValueVariant := DfSEII_v03_0_2_a.QrE111AnoMes.Value;
    FmEfdIcmsIpiE113.EdEmpresa.ValueVariant := DfSEII_v03_0_2_a.QrE111Empresa.Value;
    FmEfdIcmsIpiE113.EdE111.ValueVariant := DfSEII_v03_0_2_a.QrE111LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiE113.EdLinArq.ValueVariant := DfSEII_v03_0_2_a.QrE113LinArq.Value;
      //
      FmEfdIcmsIpiE113.EdCOD_PART.Text := DfSEII_v03_0_2_a.QrE113COD_PART.Value;
      FmEfdIcmsIpiE113.EdCOD_MOD.Text := DfSEII_v03_0_2_a.QrE113COD_MOD.Value;
      FmEfdIcmsIpiE113.EdSER.Text := DfSEII_v03_0_2_a.QrE113SER.Value;
      FmEfdIcmsIpiE113.EdSUB.Text := DfSEII_v03_0_2_a.QrE113SUB.Value;
      FmEfdIcmsIpiE113.EdNUM_DOC.ValueVariant := DfSEII_v03_0_2_a.QrE113NUM_DOC.Value;
      FmEfdIcmsIpiE113.TPDT_DOC.Date := DfSEII_v03_0_2_a.QrE113DT_DOC.Value;
      FmEfdIcmsIpiE113.EdCOD_ITEM.Text := DfSEII_v03_0_2_a.QrE113COD_ITEM.Value;
      FmEfdIcmsIpiE113.EdVL_AJ_ITEM.ValueVariant := DfSEII_v03_0_2_a.QrE113VL_AJ_ITEM.Value;
    end;
    //
    FmEfdIcmsIpiE113.ShowModal;
    FmEfdIcmsIpiE113.Destroy;
  end;
*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdE115(SQLType: TSQLType);
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "E115"');
(*
  if DBCheck.CriaFm(TFmEfdIcmsIpiE115, FmEfdIcmsIpiE115, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiE115.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiE115.TPDT_INI.Date := DfSEII_v03_0_2_a.QrE100DT_INI.Value;
    FmEfdIcmsIpiE115.TPDT_FIN.Date := DfSEII_v03_0_2_a.QrE100DT_FIN.Value;
    //
    FmEfdIcmsIpiE115.EdImporExpor.ValueVariant := DfSEII_v03_0_2_a.QrE110ImporExpor.Value;
    FmEfdIcmsIpiE115.EdAnoMes.ValueVariant := DfSEII_v03_0_2_a.QrE110AnoMes.Value;
    FmEfdIcmsIpiE115.EdEmpresa.ValueVariant := DfSEII_v03_0_2_a.QrE110Empresa.Value;
    FmEfdIcmsIpiE115.EdE110.ValueVariant := DfSEII_v03_0_2_a.QrE110LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiE115.EdLinArq.ValueVariant := DfSEII_v03_0_2_a.QrE115LinArq.Value;
      //
      FmEfdIcmsIpiE115.EdCOD_INF_ADIC.Text := DfSEII_v03_0_2_a.QrE115COD_INF_ADIC.Value;
      FmEfdIcmsIpiE115.EdVL_INF_ADIC.ValueVariant := DfSEII_v03_0_2_a.QrE115VL_INF_ADIC.Value;
      FmEfdIcmsIpiE115.EdDESCR_COMPL_AJ.Text := DfSEII_v03_0_2_a.QrE115DESCR_COMPL_AJ.Value;
    end;
    //
    FmEfdIcmsIpiE115.ShowModal;
    FmEfdIcmsIpiE115.Destroy;
  end;
*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdE116(SQLType: TSQLType);
  function DefineItem_IND_PROC(x: String): Integer;
  begin
    if x = '0' then Result := 1 else
    if x = '1' then Result := 2 else
    if x = '2' then Result := 3 else
    if x = '9' then Result := 4 else
                    Result := 0;
  end;
const
  TabName = 'TbSpedEfd005';
  FldOrde = 'Nome';
var
  Ano, Mes, Dia: Word;
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "E116"');
(*
  if DBCheck.CriaFm(TFmEfdIcmsIpiE116, FmEfdIcmsIpiE116, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiE116.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiE116.TPDT_INI.Date := DfSEII_v03_0_2_a.QrE100DT_INI.Value;
    FmEfdIcmsIpiE116.TPDT_FIN.Date := DfSEII_v03_0_2_a.QrE100DT_FIN.Value;
    //
    EfdIcmsIpi_PF.ReopenTbSpedEfdXXX(FmEfdIcmsIpiE116.QrTbSpedEfd005,
    DfSEII_v03_0_2_a.QrE100DT_INI.Value, DfSEII_v03_0_2_a.QrE100DT_FIN.Value, TabName, FldOrde);
    //
    FmEfdIcmsIpiE116.EdImporExpor.ValueVariant := DfSEII_v03_0_2_a.QrE110ImporExpor.Value;
    FmEfdIcmsIpiE116.EdAnoMes.ValueVariant := DfSEII_v03_0_2_a.QrE110AnoMes.Value;
    FmEfdIcmsIpiE116.EdEmpresa.ValueVariant := DfSEII_v03_0_2_a.QrE110Empresa.Value;
    FmEfdIcmsIpiE116.EdE100.ValueVariant := DfSEII_v03_0_2_a.QrE110E100.Value;
    FmEfdIcmsIpiE116.EdE110.ValueVariant := DfSEII_v03_0_2_a.QrE110LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiE116.EdLinArq.ValueVariant := DfSEII_v03_0_2_a.QrE116LinArq.Value;
      //
      FmEfdIcmsIpiE116.EdCOD_OR.Text := DfSEII_v03_0_2_a.QrE116COD_OR.Value;
      FmEfdIcmsIpiE116.EdVL_OR.ValueVariant := DfSEII_v03_0_2_a.QrE116VL_OR.Value;
      FmEfdIcmsIpiE116.TPDT_VCTO.Date := DfSEII_v03_0_2_a.QrE116DT_VCTO.Value;
      FmEfdIcmsIpiE116.EdCOD_REC.Text := DfSEII_v03_0_2_a.QrE116COD_REC.Value;
      FmEfdIcmsIpiE116.EdNUM_PROC.Text := DfSEII_v03_0_2_a.QrE116NUM_PROC.Value;
      FmEfdIcmsIpiE116.RGIND_PROC.ItemIndex := DefineItem_IND_PROC(DfSEII_v03_0_2_a.QrE116IND_PROC.Value);
      FmEfdIcmsIpiE116.EdPROC.Text := DfSEII_v03_0_2_a.QrE116PROC.Value;
      FmEfdIcmsIpiE116.EdTXT_COMPL.Text := DfSEII_v03_0_2_a.QrE116TXT_COMPL.Value;
      FmEfdIcmsIpiE116.TPMES_REF.Date := DfSEII_v03_0_2_a.QrE116MES_REF.Value;
    end else
    begin
      FmEfdIcmsIpiE116.TPDT_VCTO.Date := DfSEII_v03_0_2_a.QrE100DT_FIN.Value + 1;
      FmEfdIcmsIpiE116.TPMES_REF.Date := DfSEII_v03_0_2_a.QrE100DT_INI.Value;
    end;
    //
    FmEfdIcmsIpiE116.ShowModal;
    FmEfdIcmsIpiE116.Destroy;
  end;
*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdE500(SQLType: TSQLType);
var
  Ini, Fim: TDateTime;
  Ano, Mes, IND_APUR, Dias: Integer;
begin
  //Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "E500"');
  if UmyMod.FormInsUpd_Cria(TFmEfdIcmsIpiE500_v03_0_2_a, FmEfdIcmsIpiE500_v03_0_2_a, afmoNegarComAviso,
  DfSEII_v03_0_2_a.QrE500, SQLType) then
  begin
    if SQLType = stIns then
    begin
      if not EfdIcmsIpi_PF.DefineDatasPeriodoSPED(DfSEII_v03_0_2_a.FEmpresa,
      DfSEII_v03_0_2_a.QrE001AnoMes.Value, 'E500',
      DfSEII_v03_0_2_a.QrE500DT_INI.Value, DfSEII_v03_0_2_a.QrE500DT_FIN.Value, Ini, Fim) then ;
      Dias := Trunc(Fim) - Trunc(Ini);
      case dias of
        10:      IND_APUR := 0;
        28..31 : IND_APUR := 1;
        else     IND_APUR := -1;
      end;
    end else
    begin
      Ini := DfSEII_v03_0_2_a.QrE500DT_INI.Value;
      Fim := DfSEII_v03_0_2_a.QrE500DT_FIN.Value;
      IND_APUR := Geral.IMV(DfSEII_v03_0_2_a.QrE500IND_APUR.Value);
    end;
    FmEfdIcmsIpiE500_v03_0_2_a.ImgTipo.SQLType := SQLType;
    FmEfdIcmsIpiE500_v03_0_2_a.EdImporExpor.ValueVariant := DfSEII_v03_0_2_a.QrE001ImporExpor.Value;
    FmEfdIcmsIpiE500_v03_0_2_a.EdAnoMes.ValueVariant := DfSEII_v03_0_2_a.QrE001AnoMes.Value;
    FmEfdIcmsIpiE500_v03_0_2_a.EdEmpresa.ValueVariant := DfSEII_v03_0_2_a.QrE001Empresa.Value;
    //
    FmEfdIcmsIpiE500_v03_0_2_a.TPDT_INI.Date := Ini;
    FmEfdIcmsIpiE500_v03_0_2_a.TPDT_FIN.Date := Fim;
    FmEfdIcmsIpiE500_v03_0_2_a.RGIND_APUR.ItemIndex := IND_APUR;
    //
    FmEfdIcmsIpiE500_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiE500_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdE530(SQLType: TSQLType);
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "E530"');
(*
  if DBCheck.CriaFm(TFmEfdIcmsIpiE530, FmEfdIcmsIpiE530, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiE530.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiE530.TPDT_INI.Date := DfSEII_v03_0_2_a.QrE100DT_INI.Value;
    FmEfdIcmsIpiE530.TPDT_FIN.Date := DfSEII_v03_0_2_a.QrE100DT_FIN.Value;
    //
    FmEfdIcmsIpiE530.FAnoMes       := DfSEII_v03_0_2_a.QrE100AnoMes.Value;
    FmEfdIcmsIpiE530.ReopenTabelas();
    //
    FmEfdIcmsIpiE530.EdImporExpor.ValueVariant := DfSEII_v03_0_2_a.QrE520ImporExpor.Value;
    FmEfdIcmsIpiE530.EdAnoMes.ValueVariant     := DfSEII_v03_0_2_a.QrE520AnoMes.Value;
    FmEfdIcmsIpiE530.EdEmpresa.ValueVariant    := DfSEII_v03_0_2_a.QrE520Empresa.Value;
    FmEfdIcmsIpiE530.EdE520.ValueVariant       := DfSEII_v03_0_2_a.QrE520LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiE530.EdLinArq.ValueVariant := DfSEII_v03_0_2_a.QrE530LinArq.Value;
      //
      FmEfdIcmsIpiE530.RGIND_AJ.ItemIndex      := Geral.IMV(DfSEII_v03_0_2_a.QrE530IND_AJ.Value);
      FmEfdIcmsIpiE530.EdVL_AJ.ValueVariant    := DfSEII_v03_0_2_a.QrE530VL_AJ.Value;
      FmEfdIcmsIpiE530.EdCOD_AJ.ValueVariant   := DfSEII_v03_0_2_a.QrE530COD_AJ.Value;
      case Geral.IMV(DfSEII_v03_0_2_a.QrE530IND_DOC.Value) of
        0: FmEfdIcmsIpiE530.RGIND_DOC.ItemIndex := 0;
        1: FmEfdIcmsIpiE530.RGIND_DOC.ItemIndex := 1;
        2: FmEfdIcmsIpiE530.RGIND_DOC.ItemIndex := 2;
        9: FmEfdIcmsIpiE530.RGIND_DOC.ItemIndex := 3;
        else Geral.MB_Aviso('Valor de "IND_DOC" n�o implementado!');
      end;
      FmEfdIcmsIpiE530.EdNUM_DOC.Text          := DfSEII_v03_0_2_a.QrE530NUM_DOC.Value;
      FmEfdIcmsIpiE530.MeDESCR_AJ.Text         := DfSEII_v03_0_2_a.QrE530DESCR_AJ.Value;
    end;
    //
    FmEfdIcmsIpiE530.ShowModal;
    FmEfdIcmsIpiE530.Destroy;
  end;
*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdH005(SQLType: TSQLType);
var
  DT_INV: TDateTime;
  Ano, Mes: Word;
  MOT_INV: String;
begin
  if UmyMod.FormInsUpd_Cria(TFmEfdIcmsIpiH005_v03_0_2_a, FmEfdIcmsIpiH005_v03_0_2_a, afmoNegarComAviso,
  DfSEII_v03_0_2_a.QrH005, SQLType) then
  begin
    if SQLType = stIns then
    begin
      Ano     := DfSEII_v03_0_2_a.QrE001AnoMes.Value div 100;
      Mes     := DfSEII_v03_0_2_a.QrE001AnoMes.Value mod 100;
      DT_INV  := IncMonth(EncodeDate(Ano, Mes, 1), 1) - 1;
      MOT_INV := '01';
    end else
    begin
      DT_INV  := DfSEII_v03_0_2_a.QrH005DT_INV.Value;
      MOT_INV := DfSEII_v03_0_2_a.QrH005MOT_INV.Value;
    end;
    FmEfdIcmsIpiH005_v03_0_2_a.ImgTipo.SQLType := SQLType;
    FmEfdIcmsIpiH005_v03_0_2_a.EdImporExpor.ValueVariant := DfSEII_v03_0_2_a.QrE001ImporExpor.Value;
    FmEfdIcmsIpiH005_v03_0_2_a.EdAnoMes.ValueVariant := DfSEII_v03_0_2_a.QrE001AnoMes.Value;
    FmEfdIcmsIpiH005_v03_0_2_a.EdEmpresa.ValueVariant := DfSEII_v03_0_2_a.QrE001Empresa.Value;
    //
    FmEfdIcmsIpiH005_v03_0_2_a.TPDT_INV.Date := DT_INV;
    FmEfdIcmsIpiH005_v03_0_2_a.EdMOT_INV.Text := MOT_INV;
    //
    FmEfdIcmsIpiH005_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiH005_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdH010(SQLType: TSQLType);
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "H010"');
(*
  if DBCheck.CriaFm(TFmEfdIcmsIpiH010, FmEfdIcmsIpiH010, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiH010.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiH010.TPDT_INV.Date := DfSEII_v03_0_2_a.QrH005DT_INV.Value;
    //
    FmEfdIcmsIpiH010.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrH005ImporExpor.Value;
    FmEfdIcmsIpiH010.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrH005AnoMes.Value;
    FmEfdIcmsIpiH010.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrH005Empresa.Value;
    FmEfdIcmsIpiH010.EdH005.ValueVariant         := DfSEII_v03_0_2_a.QrH005LinArq.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiH010.EdLinArq.ValueVariant     := DfSEII_v03_0_2_a.QrH010LinArq.Value;
      FmEfdIcmsIpiH010.FBalID                    := DfSEII_v03_0_2_a.QrH010BalID.Value;
      FmEfdIcmsIpiH010.FBalNum                   := DfSEII_v03_0_2_a.QrH010BalNum.Value;
      FmEfdIcmsIpiH010.FBalItm                   := DfSEII_v03_0_2_a.QrH010BalItm.Value;
      FmEfdIcmsIpiH010.FBalEnt                   := DfSEII_v03_0_2_a.QrH010BalEnt.Value;
      //
      FmEfdIcmsIpiH010.EdGraGruX.Text            := DfSEII_v03_0_2_a.QrH010COD_ITEM.Value;
      FmEfdIcmsIpiH010.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.QrH010COD_ITEM.Value;
      FmEfdIcmsIpiH010.EdUNID.Text               := DfSEII_v03_0_2_a.QrH010UNID.Value;
      FmEfdIcmsIpiH010.EdQTD.ValueVariant        := DfSEII_v03_0_2_a.QrH010QTD.Value;
      FmEfdIcmsIpiH010.EdVL_UNIT.ValueVariant    := DfSEII_v03_0_2_a.QrH010VL_UNIT.Value;
      FmEfdIcmsIpiH010.EdVL_ITEM.ValueVariant    := DfSEII_v03_0_2_a.QrH010VL_ITEM.Value;
      FmEfdIcmsIpiH010.RGIND_PROP.ItemIndex      := Geral.IMV(DfSEII_v03_0_2_a.QrH010IND_PROP.Value);
      FmEfdIcmsIpiH010.EdCOD_PART.ValueVariant   := Geral.IMV(DfSEII_v03_0_2_a.QrH010COD_PART.Value);
      FmEfdIcmsIpiH010.CBCOD_PART.KeyValue       := Geral.IMV(DfSEII_v03_0_2_a.QrH010COD_PART.Value);
      FmEfdIcmsIpiH010.EdTXT_COMPL.ValueVariant  := DfSEII_v03_0_2_a.QrH010TXT_COMPL.Value;
      FmEfdIcmsIpiH010.EdCOD_CTA.Text            := DfSEII_v03_0_2_a.QrH010COD_CTA.Value;
      FmEfdIcmsIpiH010.EdVL_ITEM_IR.ValueVariant := DfSEII_v03_0_2_a.QrH010VL_ITEM_IR.Value;
    end;
    //
    FmEfdIcmsIpiH010.ShowModal;
    FmEfdIcmsIpiH010.Destroy;
  end;
*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdH020(SQLType: TSQLType);
begin
  Geral.MB_Info('Falta Implementar SPED EFD ICMS IPI "H020"');
(*
  if DBCheck.CriaFm(TFmEfdIcmsIpiH020, FmEfdIcmsIpiH020, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiH020.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiH020.FAnoMes                     := DfSEII_v03_0_2_a.QrH010AnoMes.Value;
    FmEfdIcmsIpiH020.TPDT_INV.Date               := DfSEII_v03_0_2_a.QrH005DT_INV.Value;
    //
    FmEfdIcmsIpiH020.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrH010ImporExpor.Value;
    FmEfdIcmsIpiH020.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrH010AnoMes.Value;
    FmEfdIcmsIpiH020.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrH010Empresa.Value;
    FmEfdIcmsIpiH020.EdH010.ValueVariant         := DfSEII_v03_0_2_a.QrH010LinArq.Value;
    //
    FmEfdIcmsIpiH020.ReopenTabelas();
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiH020.EdLinArq.ValueVariant     := DfSEII_v03_0_2_a.QrH020LinArq.Value;
      //
      FmEfdIcmsIpiH020.EdCST_ICMS.ValueVariant   := DfSEII_v03_0_2_a.QrH020CST_ICMS.Value;
      FmEfdIcmsIpiH020.CBCST_ICMS.KeyValue       := DfSEII_v03_0_2_a.QrH020CST_ICMS.Value;
      FmEfdIcmsIpiH020.EdBC_ICMS.ValueVariant    := DfSEII_v03_0_2_a.QrH020BC_ICMS.Value;
      FmEfdIcmsIpiH020.EdVL_ICMS.ValueVariant    := DfSEII_v03_0_2_a.QrH020VL_ICMS.Value;
    end;
    //
    FmEfdIcmsIpiH020.ShowModal;
    FmEfdIcmsIpiH020.Destroy;
  end;
*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK100(SQLType: TSQLType);
var
  Ini, Fim: TDateTime;
  Ano, Mes: Integer;
begin
  if UmyMod.FormInsUpd_Cria(TFmEfdIcmsIpiK100_v03_0_2_a, FmEfdIcmsIpiK100_v03_0_2_a, afmoNegarComAviso,
  DfSEII_v03_0_2_a.QrK100, SQLType) then
  begin
    if SQLType = stIns then
    begin
      if not EfdIcmsIpi_PF.DefineDatasPeriodoSPED(DfSEII_v03_0_2_a.FEmpresa,
      DfSEII_v03_0_2_a.QrE001AnoMes.Value, 'K100',
      DfSEII_v03_0_2_a.QrK100DT_INI.Value, DfSEII_v03_0_2_a.QrK100DT_FIN.Value, Ini, Fim) then ;
    end else
    begin
      Ini := DfSEII_v03_0_2_a.QrK100DT_INI.Value;
      Fim := DfSEII_v03_0_2_a.QrK100DT_FIN.Value;
    end;
      FmEfdIcmsIpiK100_v03_0_2_a.ImgTipo.SQLType := SQLType;
    FmEfdIcmsIpiK100_v03_0_2_a.EdImporExpor.ValueVariant := DfSEII_v03_0_2_a.QrE001ImporExpor.Value;
    FmEfdIcmsIpiK100_v03_0_2_a.EdAnoMes.ValueVariant     := DfSEII_v03_0_2_a.QrE001AnoMes.Value;
    FmEfdIcmsIpiK100_v03_0_2_a.EdEmpresa.ValueVariant    := DfSEII_v03_0_2_a.QrE001Empresa.Value;
    //
    FmEfdIcmsIpiK100_v03_0_2_a.TPDT_INI.Date := Ini;
    FmEfdIcmsIpiK100_v03_0_2_a.TPDT_FIN.Date := Fim;
    //
    FmEfdIcmsIpiK100_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK100_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK200(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK200_v03_0_2_a, FmEfdIcmsIpiK200_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK200_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK200_v03_0_2_a.TPDT_EST.Date := DfSEII_v03_0_2_a.QrK100DT_FIN.Value;
    //
    FmEfdIcmsIpiK200_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK200_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK200_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK200_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiK200_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK200ID_SEK.Value;
      //
      FmEfdIcmsIpiK200_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK200KndTab.Value;
      FmEfdIcmsIpiK200_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK200KndCod.Value;
      FmEfdIcmsIpiK200_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK200KndNSU.Value;
      FmEfdIcmsIpiK200_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.QrK200KndItm.Value;
      FmEfdIcmsIpiK200_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK200KndAID.Value;
      FmEfdIcmsIpiK200_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK200KndNiv.Value;
      FmEfdIcmsIpiK200_v03_0_2_a.EdIDSeq1.ValueVariant     := DfSEII_v03_0_2_a.QrK200IDSeq1.Value;

      FmEfdIcmsIpiK200_v03_0_2_a.EdGraGruX.Text            := DfSEII_v03_0_2_a.QrK200COD_ITEM.Value;
      FmEfdIcmsIpiK200_v03_0_2_a.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.QrK200COD_ITEM.Value;
      FmEfdIcmsIpiK200_v03_0_2_a.EdQTD.ValueVariant        := DfSEII_v03_0_2_a.QrK200QTD.Value;
      FmEfdIcmsIpiK200_v03_0_2_a.RGIND_EST.ItemIndex       := Geral.IMV(DfSEII_v03_0_2_a.QrK200IND_EST.Value);
      FmEfdIcmsIpiK200_v03_0_2_a.EdCOD_PART.ValueVariant   := Geral.IMV(DfSEII_v03_0_2_a.QrK200COD_PART.Value);
      FmEfdIcmsIpiK200_v03_0_2_a.CBCOD_PART.KeyValue       := Geral.IMV(DfSEII_v03_0_2_a.QrK200COD_PART.Value);

      FmEfdIcmsIpiK200_v03_0_2_a.EdPecas.ValueVariant      := DfSEII_v03_0_2_a.QrK200Pecas.Value;
      FmEfdIcmsIpiK200_v03_0_2_a.EdAreaM2.ValueVariant     := DfSEII_v03_0_2_a.QrK200AreaM2.Value;
      FmEfdIcmsIpiK200_v03_0_2_a.EdPesoKg.ValueVariant     := DfSEII_v03_0_2_a.QrK200PesoKg.Value;

    end;
    //
    FmEfdIcmsIpiK200_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK200_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK210(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK210_v03_0_2_a, FmEfdIcmsIpiK210_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK210_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK210_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK210_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK210_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK210_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiK210_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK210ID_SEK.Value;
      //FmEfdIcmsIpiK210_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.QrK210ESOMIEM.Value;
      FmEfdIcmsIpiK210_v03_0_2_a.FOriOpeProc               := DfSEII_v03_0_2_a.QrK210OriOpeProc.Value;
      FmEfdIcmsIpiK210_v03_0_2_a.FOrigemIDKnd              := DfSEII_v03_0_2_a.QrK210OrigemIDKnd.Value;
      //
      FmEfdIcmsIpiK210_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK210KndTab.Value;
      FmEfdIcmsIpiK210_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK210KndCod.Value;
      FmEfdIcmsIpiK210_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK210KndNSU.Value;
      FmEfdIcmsIpiK210_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.QrK210KndItm.Value;
      FmEfdIcmsIpiK210_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK210KndAID.Value;
      FmEfdIcmsIpiK210_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK210KndNiv.Value;
      FmEfdIcmsIpiK210_v03_0_2_a.EdIDSeq1.ValueVariant     := DfSEII_v03_0_2_a.QrK210IDSeq1.Value;
      //
      FmEfdIcmsIpiK210_v03_0_2_a.TPDT_INI_OS.Date          := DfSEII_v03_0_2_a.QrK210DT_INI_OS.Value;
      FmEfdIcmsIpiK210_v03_0_2_a.TPDT_FIN_OS.Date          := DfSEII_v03_0_2_a.QrK210DT_FIN_OS.Value;
      FmEfdIcmsIpiK210_v03_0_2_a.EdMovimCod.ValueVariant   := Geral.IMV(DfSEII_v03_0_2_a.QrK210COD_DOC_OS.Value);
      FmEfdIcmsIpiK210_v03_0_2_a.EdGraGruX.Text            := DfSEII_v03_0_2_a.QrK210COD_ITEM_ORI.Value;
      FmEfdIcmsIpiK210_v03_0_2_a.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.QrK210COD_ITEM_ORI.Value;
      FmEfdIcmsIpiK210_v03_0_2_a.EdQTD_ORI.ValueVariant    := DfSEII_v03_0_2_a.QrK210QTD_ORI.Value;
    end;
    //
    FmEfdIcmsIpiK210_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK210_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK215(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK215_v03_0_2_a, FmEfdIcmsIpiK215_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK215_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK215_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK215_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK215_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK215_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    FmEfdIcmsIpiK215_v03_0_2_a.EdIDSeq1.ValueVariant       := DfSEII_v03_0_2_a.QrK210IDSeq1.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiK215_v03_0_2_a.FESTSTabSorc              := DfSEII_v03_0_2_a.QrK215ESTSTabSorc.Value;
      FmEfdIcmsIpiK215_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK215ID_SEK.Value;
      //FmEfdIcmsIpiK215_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.QrK215ESOMIEM.Value;
      FmEfdIcmsIpiK215_v03_0_2_a.FOriOpeProc               := DfSEII_v03_0_2_a.QrK215OriOpeProc.Value;
      FmEfdIcmsIpiK215_v03_0_2_a.FOrigemIDKnd              := DfSEII_v03_0_2_a.QrK215OrigemIDKnd.Value;
      //
      FmEfdIcmsIpiK215_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK215KndTab.Value;
      FmEfdIcmsIpiK215_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK215KndCod.Value;
      FmEfdIcmsIpiK215_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK215KndNSU.Value;
      FmEfdIcmsIpiK215_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.QrK215KndItm.Value;
      FmEfdIcmsIpiK215_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK215KndAID.Value;
      FmEfdIcmsIpiK215_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK215KndNiv.Value;
      FmEfdIcmsIpiK215_v03_0_2_a.EdIDSeq2.ValueVariant     := DfSEII_v03_0_2_a.QrK215IDSeq2.Value;
      //
      FmEfdIcmsIpiK215_v03_0_2_a.EdGraGruX.Text            := DfSEII_v03_0_2_a.QrK215COD_ITEM_DES.Value;
      FmEfdIcmsIpiK215_v03_0_2_a.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.QrK215COD_ITEM_DES.Value;
      FmEfdIcmsIpiK215_v03_0_2_a.EdQTD_DES.ValueVariant    := DfSEII_v03_0_2_a.QrK215QTD_DES.Value;
    end;
    //
    FmEfdIcmsIpiK215_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK215_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK220(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK220_v03_0_2_a, FmEfdIcmsIpiK220_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK220_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK220_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK220_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK220_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK220_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiK220_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK220ID_SEK.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.QrK220ESOMIEM.Value;
      //
      FmEfdIcmsIpiK220_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK220KndTab.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK220KndCod.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK220KndNSU.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.EdKndItmOri.ValueVariant  := DfSEII_v03_0_2_a.QrK220KndItmOri.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.EdKndItmDst.ValueVariant  := DfSEII_v03_0_2_a.QrK220KndItmDst.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK220KndAID.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK220KndNiv.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.EdIDSeq1.ValueVariant     := DfSEII_v03_0_2_a.QrK220IDSeq1.Value;
      //
      FmEfdIcmsIpiK220_v03_0_2_a.TPDT_MOV.Date             := DfSEII_v03_0_2_a.QrK220DT_MOV.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.EdGGXOri.Text             := DfSEII_v03_0_2_a.QrK220COD_ITEM_ORI.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.CBGGXOri.KeyValue         := DfSEII_v03_0_2_a.QrK220COD_ITEM_ORI.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.EdGGXDst.Text             := DfSEII_v03_0_2_a.QrK220COD_ITEM_DEST.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.CBGGXDst.KeyValue         := DfSEII_v03_0_2_a.QrK220COD_ITEM_DEST.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.EdQTD_ORI.ValueVariant    := DfSEII_v03_0_2_a.QrK220QTD_ORI.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.EdQTD_DEST.ValueVariant   := DfSEII_v03_0_2_a.QrK220QTD_DEST.Value;
      FmEfdIcmsIpiK220_v03_0_2_a.EdQTD_.ValueVariant       := DfSEII_v03_0_2_a.QrK220QTD.Value;
    end;
    //
    FmEfdIcmsIpiK220_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK220_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK230(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK230_v03_0_2_a, FmEfdIcmsIpiK230_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK230_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK230_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK230_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK230_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK230_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiK230_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK230ID_SEK.Value;
      //FmEfdIcmsIpiK230_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.QrK230ESOMIEM.Value;
      FmEfdIcmsIpiK230_v03_0_2_a.FOriOpeProc               := DfSEII_v03_0_2_a.QrK230OriOpeProc.Value;
      //FmEfdIcmsIpiK230_v03_0_2_a.FOrigemIDKnd              := DfSEII_v03_0_2_a.QrK230OrigemIDKnd.Value;
      //
      FmEfdIcmsIpiK230_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK230KndTab.Value;
      FmEfdIcmsIpiK230_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK230KndCod.Value;
      FmEfdIcmsIpiK230_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK230KndNSU.Value;
      FmEfdIcmsIpiK230_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.QrK230KndItm.Value;
      FmEfdIcmsIpiK230_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK230KndAID.Value;
      FmEfdIcmsIpiK230_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK230KndNiv.Value;
      FmEfdIcmsIpiK230_v03_0_2_a.EdIDSeq1.ValueVariant     := DfSEII_v03_0_2_a.QrK230IDSeq1.Value;
      //
      FmEfdIcmsIpiK230_v03_0_2_a.TPDT_INI_OP.Date          := DfSEII_v03_0_2_a.QrK230DT_INI_OP.Value;
      FmEfdIcmsIpiK230_v03_0_2_a.TPDT_FIN_OP.Date          := DfSEII_v03_0_2_a.QrK230DT_FIN_OP.Value;
      FmEfdIcmsIpiK230_v03_0_2_a.EdMovimCod.ValueVariant   := Geral.IMV(DfSEII_v03_0_2_a.QrK230COD_DOC_OP.Value);
      FmEfdIcmsIpiK230_v03_0_2_a.EdGraGruX.Text            := DfSEII_v03_0_2_a.QrK230COD_ITEM.Value;
      FmEfdIcmsIpiK230_v03_0_2_a.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.QrK230COD_ITEM.Value;
      FmEfdIcmsIpiK230_v03_0_2_a.EdQTD_ENC.ValueVariant    := DfSEII_v03_0_2_a.QrK230QTD_ENC.Value;
    end else
    begin
      FmEfdIcmsIpiK230_v03_0_2_a.EdKndTab.ValueVariant     := Integer(TEstqSPEDTabSorc.estsNoSrc);
    end;
    //
    FmEfdIcmsIpiK230_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK230_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK235(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK235_v03_0_2_a, FmEfdIcmsIpiK235_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK235_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK235_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK235_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK235_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK235_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    FmEfdIcmsIpiK235_v03_0_2_a.EdIDSeq1.ValueVariant       := DfSEII_v03_0_2_a.QrK230IDSeq1.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiK235_v03_0_2_a.FESTSTabSorc              := DfSEII_v03_0_2_a.QrK235ESTSTabSorc.Value;
      FmEfdIcmsIpiK235_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK235ID_SEK.Value;
      //FmEfdIcmsIpiK235_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.QrK235ESOMIEM.Value;
      FmEfdIcmsIpiK235_v03_0_2_a.FOriOpeProc               := DfSEII_v03_0_2_a.QrK235OriOpeProc.Value;
      //FmEfdIcmsIpiK235_v03_0_2_a.FOrigemIDKnd              := DfSEII_v03_0_2_a.QrK235OrigemIDKnd.Value;
      //
      FmEfdIcmsIpiK235_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK235KndTab.Value;
      FmEfdIcmsIpiK235_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK235KndCod.Value;
      FmEfdIcmsIpiK235_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK235KndNSU.Value;
      FmEfdIcmsIpiK235_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.QrK235KndItm.Value;
      FmEfdIcmsIpiK235_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK235KndAID.Value;
      FmEfdIcmsIpiK235_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK235KndNiv.Value;
      FmEfdIcmsIpiK235_v03_0_2_a.EdIDSeq2.ValueVariant     := DfSEII_v03_0_2_a.QrK235IDSeq2.Value;
      //
      FmEfdIcmsIpiK235_v03_0_2_a.TPDT_SAIDA.Date           := DfSEII_v03_0_2_a.QrK235DT_SAIDA.Value;
      FmEfdIcmsIpiK235_v03_0_2_a.EdGraGruX.Text            := DfSEII_v03_0_2_a.QrK235COD_ITEM.Value;
      FmEfdIcmsIpiK235_v03_0_2_a.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.QrK235COD_ITEM.Value;
      FmEfdIcmsIpiK235_v03_0_2_a.EdQTD.ValueVariant        := DfSEII_v03_0_2_a.QrK235QTD.Value;
      FmEfdIcmsIpiK235_v03_0_2_a.EdCOD_INS_SUBST.Text      := DfSEII_v03_0_2_a.QrK235COD_INS_SUBST.Value;
    end;
    //
    FmEfdIcmsIpiK235_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK235_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK250(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK250_v03_0_2_a, FmEfdIcmsIpiK250_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK250_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK250_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK250_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK250_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK250_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiK250_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK250ID_SEK.Value;
      //FmEfdIcmsIpiK250_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.QrK250ESOMIEM.Value;
      FmEfdIcmsIpiK250_v03_0_2_a.FOriOpeProc               := DfSEII_v03_0_2_a.QrK250OriOpeProc.Value;
      //FmEfdIcmsIpiK250_v03_0_2_a.FOrigemIDKnd              := DfSEII_v03_0_2_a.QrK250OrigemIDKnd.Value;
      //
      FmEfdIcmsIpiK250_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK250KndTab.Value;
      FmEfdIcmsIpiK250_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK250KndCod.Value;
      FmEfdIcmsIpiK250_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK250KndNSU.Value;
      FmEfdIcmsIpiK250_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.QrK250KndItm.Value;
      FmEfdIcmsIpiK250_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK250KndAID.Value;
      FmEfdIcmsIpiK250_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK250KndNiv.Value;
      FmEfdIcmsIpiK250_v03_0_2_a.EdIDSeq1.ValueVariant     := DfSEII_v03_0_2_a.QrK250IDSeq1.Value;
      //
      FmEfdIcmsIpiK250_v03_0_2_a.TPDT_PROD.Date            := DfSEII_v03_0_2_a.QrK250DT_PROD.Value;
      //FmEfdIcmsIpiK250_v03_0_2_a.EdMovimCod.ValueVariant   := Geral.IMV(DfSEII_v03_0_2_a.QrK250COD_DOC_OP.Value);
      FmEfdIcmsIpiK250_v03_0_2_a.FMovimCod                 := Geral.IMV(DfSEII_v03_0_2_a.QrK250COD_DOC_OP.Value);
      FmEfdIcmsIpiK250_v03_0_2_a.EdGraGruX.Text            := DfSEII_v03_0_2_a.QrK250COD_ITEM.Value;
      FmEfdIcmsIpiK250_v03_0_2_a.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.QrK250COD_ITEM.Value;
      FmEfdIcmsIpiK250_v03_0_2_a.EdQTD.ValueVariant        := DfSEII_v03_0_2_a.QrK250QTD.Value;
    end;
    //
    FmEfdIcmsIpiK250_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK250_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK255(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpik255_v03_0_2_a, FmEfdIcmsIpik255_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpik255_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpik255_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpik255_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpik255_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpik255_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    FmEfdIcmsIpik255_v03_0_2_a.EdIDSeq1.ValueVariant       := DfSEII_v03_0_2_a.Qrk250IDSeq1.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpik255_v03_0_2_a.FESTSTabSorc              := DfSEII_v03_0_2_a.Qrk255ESTSTabSorc.Value;
      FmEfdIcmsIpik255_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.Qrk255ID_SEK.Value;
      //FmEfdIcmsIpik255_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.Qrk255ESOMIEM.Value;
      FmEfdIcmsIpik255_v03_0_2_a.FOriOpeProc               := DfSEII_v03_0_2_a.Qrk255OriOpeProc.Value;
      //FmEfdIcmsIpik255_v03_0_2_a.FOrigemIDKnd              := DfSEII_v03_0_2_a.Qrk255OrigemIDKnd.Value;
      //
      FmEfdIcmsIpik255_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.Qrk255KndTab.Value;
      FmEfdIcmsIpik255_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.Qrk255KndCod.Value;
      FmEfdIcmsIpik255_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.Qrk255KndNSU.Value;
      FmEfdIcmsIpik255_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.Qrk255KndItm.Value;
      FmEfdIcmsIpik255_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.Qrk255KndAID.Value;
      FmEfdIcmsIpik255_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.Qrk255KndNiv.Value;
      FmEfdIcmsIpik255_v03_0_2_a.EdIDSeq2.ValueVariant     := DfSEII_v03_0_2_a.Qrk255IDSeq2.Value;
      //
      FmEfdIcmsIpik255_v03_0_2_a.TPDT_CONS.Date            := DfSEII_v03_0_2_a.QrK255DT_CONS.Value;
      FmEfdIcmsIpik255_v03_0_2_a.EdGraGruX.Text            := DfSEII_v03_0_2_a.Qrk255COD_ITEM.Value;
      FmEfdIcmsIpik255_v03_0_2_a.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.Qrk255COD_ITEM.Value;
      FmEfdIcmsIpik255_v03_0_2_a.EdQTD.ValueVariant        := DfSEII_v03_0_2_a.Qrk255QTD.Value;
      FmEfdIcmsIpik255_v03_0_2_a.EdCOD_INS_SUBST.Text      := DfSEII_v03_0_2_a.Qrk255COD_INS_SUBST.Value;
    end;
    //
    FmEfdIcmsIpik255_v03_0_2_a.ShowModal;
    FmEfdIcmsIpik255_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK260(SQLType: TSQLType);
begin
   //
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK265(SQLType: TSQLType);
begin
    //
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK270(RegPai: Integer; SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK270_v03_0_2_a, FmEfdIcmsIpiK270_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK270_v03_0_2_a.ImgTipo.SQLType := SQLType;
    FmEfdIcmsIpiK270_v03_0_2_a.FNumRegPai := RegPai;
    //
    FmEfdIcmsIpiK270_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK270_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK270_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK270_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    //
    if SQLType = stUpd then
    begin
      DfSEII_v03_0_2_a.ReopenK270_XXX(RegPai);
      //
      FmEfdIcmsIpiK270_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK270_XXXID_SEK.Value;
      //FmEfdIcmsIpiK270_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.QrK270_XXXESOMIEM.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.FOriOpeProc               := DfSEII_v03_0_2_a.QrK270_XXXOriOpeProc.Value;
      //FmEfdIcmsIpiK270_v03_0_2_a.FOrigemIDKnd              := DfSEII_v03_0_2_a.QrK270_XXXOrigemIDKnd.Value;
      //
      FmEfdIcmsIpiK270_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK270_XXXKndTab.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK270_XXXKndCod.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK270_XXXKndNSU.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.QrK270_XXXKndItm.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK270_XXXKndAID.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK270_XXXKndNiv.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.EdIDSeq1.ValueVariant     := DfSEII_v03_0_2_a.QrK270_XXXIDSeq1.Value;
      //
      FmEfdIcmsIpiK270_v03_0_2_a.EdOrigemIDKnd.ValueVariant    := DfSEII_v03_0_2_a.QrK270_XXXOrigemIDKnd.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.EdOriSPEDEFDKnd.ValueVariant  := DfSEII_v03_0_2_a.QrK270_XXXOriSPEDEFDKnd.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.EdRegisPai.ValueVariant       := DfSEII_v03_0_2_a.QrK270_XXXRegisPai.Value;
      //
      FmEfdIcmsIpiK270_v03_0_2_a.TPDT_INI_AP.Date          := DfSEII_v03_0_2_a.QrK270_XXXDT_INI_AP.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.TPDT_FIN_AP.Date          := DfSEII_v03_0_2_a.QrK270_XXXDT_FIN_AP.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.EdMovimCod.ValueVariant   := Geral.IMV(DfSEII_v03_0_2_a.QrK270_XXXCOD_OP_OS.Value);
      //FmEfdIcmsIpiK270_v03_0_2_a.EdGraGruX.Text            := DfSEII_v03_0_2_a.QrK270_XXXCOD_ITEM.Value;
      //FmEfdIcmsIpiK270_v03_0_2_a.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.QrK270_XXXCOD_ITEM.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.EdGraGruX.ValueVariant    := DfSEII_v03_0_2_a.QrK270_XXXGraGruX.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.QrK270_XXXGraGruX.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.EdQTD_COR_POS.ValueVariant    := DfSEII_v03_0_2_a.QrK270_XXXQTD_COR_POS.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.EdQTD_COR_NEG.ValueVariant    := DfSEII_v03_0_2_a.QrK270_XXXQTD_COR_NEG.Value;
      FmEfdIcmsIpiK270_v03_0_2_a.RGORIGEM.ItemIndex        := Geral.IMV(DfSEII_v03_0_2_a.QrK270_XXXORIGEM.Value) - 1;
    end else
    begin
      FmEfdIcmsIpiK270_v03_0_2_a.EdKndTab.ValueVariant     := Integer(TEstqSPEDTabSorc.estsNoSrc);
    end;
    //
    FmEfdIcmsIpiK270_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK270_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK275(RegPai, RegFilho: Integer; SQLType: TSQLType);
begin
      //
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK280(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK280_v03_0_2_a, FmEfdIcmsIpiK280_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK280_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK280_v03_0_2_a.EdImporExpor.ValueVariant    := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK280_v03_0_2_a.EdAnoMes.ValueVariant        := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK280_v03_0_2_a.EdEmpresa.ValueVariant       := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK280_v03_0_2_a.EdPeriApu.ValueVariant       := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiK280_v03_0_2_a.FID_SEK                    := DfSEII_v03_0_2_a.QrK280ID_SEK.Value;
      //
      FmEfdIcmsIpiK280_v03_0_2_a.EdKndTab.ValueVariant      := DfSEII_v03_0_2_a.QrK280KndTab.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.EdKndCod.ValueVariant      := DfSEII_v03_0_2_a.QrK280KndCod.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.EdKndNSU.ValueVariant      := DfSEII_v03_0_2_a.QrK280KndNSU.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.EdKndItm.ValueVariant      := DfSEII_v03_0_2_a.QrK280KndItm.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.EdKndAID.ValueVariant      := DfSEII_v03_0_2_a.QrK280KndAID.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.EdKndNiv.ValueVariant      := DfSEII_v03_0_2_a.QrK280KndNiv.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.EdIDSeq1.ValueVariant      := DfSEII_v03_0_2_a.QrK280IDSeq1.Value;

      FmEfdIcmsIpiK280_v03_0_2_a.FRegisPai                  := DfSEII_v03_0_2_a.QrK280RegisPai.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.FRegisAvo                  := DfSEII_v03_0_2_a.QrK280RegisAvo.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.FESTSTabSorc               := DfSEII_v03_0_2_a.QrK280ESTSTabSorc.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.FOriOpeProc                := DfSEII_v03_0_2_a.QrK280OriOpeProc.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.FOrigemIDKnd               := DfSEII_v03_0_2_a.QrK280OrigemIDKnd.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.FOriSPEDEFDKnd             := DfSEII_v03_0_2_a.QrK280OriSPEDEFDKnd.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.FOriBalID                  := DfSEII_v03_0_2_a.QrK280OriBalID.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.FOriKndReg                 := DfSEII_v03_0_2_a.QrK280OriKndReg.Value;

      FmEfdIcmsIpiK280_v03_0_2_a.TPDT_EST.Date              := DfSEII_v03_0_2_a.QrK100DT_FIN.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.EdGraGruX.Text             := DfSEII_v03_0_2_a.QrK280COD_ITEM.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.CBGraGruX.KeyValue         := DfSEII_v03_0_2_a.QrK280COD_ITEM.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.EdQTD_COR_POS.ValueVariant := DfSEII_v03_0_2_a.QrK280QTD_COR_POS.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.EdQTD_COR_NEG.ValueVariant := DfSEII_v03_0_2_a.QrK280QTD_COR_NEG.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.RGIND_EST.ItemIndex        := Geral.IMV(DfSEII_v03_0_2_a.QrK280IND_EST.Value);
      FmEfdIcmsIpiK280_v03_0_2_a.EdCOD_PART.ValueVariant    := Geral.IMV(DfSEII_v03_0_2_a.QrK280COD_PART.Value);
      FmEfdIcmsIpiK280_v03_0_2_a.CBCOD_PART.KeyValue        := Geral.IMV(DfSEII_v03_0_2_a.QrK280COD_PART.Value);

      FmEfdIcmsIpiK280_v03_0_2_a.EdPecas.ValueVariant       := DfSEII_v03_0_2_a.QrK280Pecas.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.EdAreaM2.ValueVariant      := DfSEII_v03_0_2_a.QrK280AreaM2.Value;
      FmEfdIcmsIpiK280_v03_0_2_a.EdPesoKg.ValueVariant      := DfSEII_v03_0_2_a.QrK280PesoKg.Value;

    end;
    //
    FmEfdIcmsIpiK280_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK280_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK290(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK290_v03_0_2_a, FmEfdIcmsIpiK290_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK290_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK290_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK290_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK290_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK290_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiK290_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK290ID_SEK.Value;
      //FmEfdIcmsIpiK290_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.QrK290ESOMIEM.Value;
      FmEfdIcmsIpiK290_v03_0_2_a.FOriOpeProc               := DfSEII_v03_0_2_a.QrK290OriOpeProc.Value;
      //FmEfdIcmsIpiK290_v03_0_2_a.FOrigemIDKnd              := DfSEII_v03_0_2_a.QrK290OrigemIDKnd.Value;
      //
      FmEfdIcmsIpiK290_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK290KndTab.Value;
      FmEfdIcmsIpiK290_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK290KndCod.Value;
      FmEfdIcmsIpiK290_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK290KndNSU.Value;
      FmEfdIcmsIpiK290_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.QrK290KndItm.Value;
      FmEfdIcmsIpiK290_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK290KndAID.Value;
      FmEfdIcmsIpiK290_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK290KndNiv.Value;
      FmEfdIcmsIpiK290_v03_0_2_a.EdIDSeq1.ValueVariant     := DfSEII_v03_0_2_a.QrK290IDSeq1.Value;
      //
      FmEfdIcmsIpiK290_v03_0_2_a.TPDT_INI_OP.Date          := DfSEII_v03_0_2_a.QrK290DT_INI_OP.Value;
      FmEfdIcmsIpiK290_v03_0_2_a.TPDT_FIN_OP.Date          := DfSEII_v03_0_2_a.QrK290DT_FIN_OP.Value;
      FmEfdIcmsIpiK290_v03_0_2_a.EdMovimCod.ValueVariant   := Geral.IMV(DfSEII_v03_0_2_a.QrK290COD_DOC_OP.Value);
    end else
    begin
      FmEfdIcmsIpiK290_v03_0_2_a.EdKndTab.ValueVariant     := Integer(TEstqSPEDTabSorc.estsNoSrc);
    end;
    //
    FmEfdIcmsIpiK290_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK290_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK291(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK291_v03_0_2_a, FmEfdIcmsIpiK291_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK291_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK291_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK291_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK291_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK291_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    FmEfdIcmsIpiK291_v03_0_2_a.EdIDSeq1.ValueVariant       := DfSEII_v03_0_2_a.QrK290IDSeq1.Value;
    //
    if SQLType = stUpd then
    begin
      //FmEfdIcmsIpiK291_v03_0_2_a.FESTSTabSorc              := DfSEII_v03_0_2_a.QrK291ESTSTabSorc.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK291ID_SEK.Value;
      //FmEfdIcmsIpiK291_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.QrK291ESOMIEM.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.FOriOpeProc               := DfSEII_v03_0_2_a.QrK291OriOpeProc.Value;
      //FmEfdIcmsIpiK291_v03_0_2_a.FOrigemIDKnd              := DfSEII_v03_0_2_a.QrK291OrigemIDKnd.Value;
      //
      FmEfdIcmsIpiK291_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK291KndTab.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK291KndCod.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK291KndNSU.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.QrK291KndItm.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK291KndAID.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK291KndNiv.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdIDSeq2.ValueVariant     := DfSEII_v03_0_2_a.QrK291IDSeq2.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdJmpMovID.ValueVariant   := DfSEII_v03_0_2_a.QrK291JmpMovID.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdJmpMovCod.ValueVariant  := DfSEII_v03_0_2_a.QrK291JmpMovCod.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdJmpNivel1.ValueVariant  := DfSEII_v03_0_2_a.QrK291JmpNivel1.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdJmpNivel2.ValueVariant  := DfSEII_v03_0_2_a.QrK291JmpNivel2.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdPaiMovID.ValueVariant   := DfSEII_v03_0_2_a.QrK291PaiMovID.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdPaiMovCod.ValueVariant  := DfSEII_v03_0_2_a.QrK291PaiMovCod.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdPaiNivel1.ValueVariant  := DfSEII_v03_0_2_a.QrK291PaiNivel1.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdPaiNivel2.ValueVariant  := DfSEII_v03_0_2_a.QrK291PaiNivel2.Value;
      //
      FmEfdIcmsIpiK291_v03_0_2_a.EdGraGruX.Text            := DfSEII_v03_0_2_a.QrK291COD_ITEM.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.QrK291COD_ITEM.Value;
      FmEfdIcmsIpiK291_v03_0_2_a.EdQTD.ValueVariant        := DfSEII_v03_0_2_a.QrK291QTD.Value;
      //
      FmEfdIcmsIpiK291_v03_0_2_a.CkContinuar.Checked       := False;
      FmEfdIcmsIpiK291_v03_0_2_a.CkContinuar.Enabled       := False;
    end else
    begin
      FmEfdIcmsIpiK291_v03_0_2_a.CkContinuar.Checked       := True;
      FmEfdIcmsIpiK291_v03_0_2_a.CkContinuar.Enabled       := True;
    end;
    //
    FmEfdIcmsIpiK291_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK291_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK292(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK292_v03_0_2_a, FmEfdIcmsIpiK292_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK292_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK292_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK292_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK292_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK292_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    FmEfdIcmsIpiK292_v03_0_2_a.EdIDSeq1.ValueVariant       := DfSEII_v03_0_2_a.QrK290IDSeq1.Value;
    //
    if SQLType = stUpd then
    begin
      //FmEfdIcmsIpiK292_v03_0_2_a.FESTSTabSorc              := DfSEII_v03_0_2_a.QrK292ESTSTabSorc.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK292ID_SEK.Value;
      //FmEfdIcmsIpiK292_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.QrK292ESOMIEM.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.FOriOpeProc               := DfSEII_v03_0_2_a.QrK292OriOpeProc.Value;
      //FmEfdIcmsIpiK292_v03_0_2_a.FOrigemIDKnd              := DfSEII_v03_0_2_a.QrK292OrigemIDKnd.Value;
      //
      FmEfdIcmsIpiK292_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK292KndTab.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK292KndCod.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK292KndNSU.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.QrK292KndItm.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK292KndAID.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK292KndNiv.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdIDSeq2.ValueVariant     := DfSEII_v03_0_2_a.QrK292IDSeq2.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdJmpMovID.ValueVariant   := DfSEII_v03_0_2_a.QrK292JmpMovID.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdJmpMovCod.ValueVariant  := DfSEII_v03_0_2_a.QrK292JmpMovCod.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdJmpNivel1.ValueVariant  := DfSEII_v03_0_2_a.QrK292JmpNivel1.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdJmpNivel2.ValueVariant  := DfSEII_v03_0_2_a.QrK292JmpNivel2.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdPaiMovID.ValueVariant   := DfSEII_v03_0_2_a.QrK292PaiMovID.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdPaiMovCod.ValueVariant  := DfSEII_v03_0_2_a.QrK292PaiMovCod.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdPaiNivel1.ValueVariant  := DfSEII_v03_0_2_a.QrK292PaiNivel1.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdPaiNivel2.ValueVariant  := DfSEII_v03_0_2_a.QrK292PaiNivel2.Value;
      //
      FmEfdIcmsIpiK292_v03_0_2_a.EdGraGruX.Text            := DfSEII_v03_0_2_a.QrK292COD_ITEM.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.QrK292COD_ITEM.Value;
      FmEfdIcmsIpiK292_v03_0_2_a.EdQTD.ValueVariant        := DfSEII_v03_0_2_a.QrK292QTD.Value;
      //
      FmEfdIcmsIpiK292_v03_0_2_a.CkContinuar.Checked       := False;
      FmEfdIcmsIpiK292_v03_0_2_a.CkContinuar.Enabled       := False;
    end else
    begin
      FmEfdIcmsIpiK292_v03_0_2_a.CkContinuar.Checked       := True;
      FmEfdIcmsIpiK292_v03_0_2_a.CkContinuar.Enabled       := True;
    end;
    //
    FmEfdIcmsIpiK292_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK292_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK300(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK300_v03_0_2_a, FmEfdIcmsIpiK300_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK300_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK300_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK300_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK300_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK300_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    //
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpiK300_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK300ID_SEK.Value;
      //FmEfdIcmsIpiK300_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.QrK300ESOMIEM.Value;
      FmEfdIcmsIpiK300_v03_0_2_a.FOriOpeProc               := DfSEII_v03_0_2_a.QrK300OriOpeProc.Value;
      //FmEfdIcmsIpiK300_v03_0_2_a.FOrigemIDKnd              := DfSEII_v03_0_2_a.QrK300OrigemIDKnd.Value;
      //
      FmEfdIcmsIpiK300_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK300KndTab.Value;
      FmEfdIcmsIpiK300_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK300KndCod.Value;
      FmEfdIcmsIpiK300_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK300KndNSU.Value;
      FmEfdIcmsIpiK300_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.QrK300KndItm.Value;
      FmEfdIcmsIpiK300_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK300KndAID.Value;
      FmEfdIcmsIpiK300_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK300KndNiv.Value;
      FmEfdIcmsIpiK300_v03_0_2_a.EdIDSeq1.ValueVariant     := DfSEII_v03_0_2_a.QrK300IDSeq1.Value;
      //
      FmEfdIcmsIpiK300_v03_0_2_a.TPDT_PROD.Date            := DfSEII_v03_0_2_a.QrK300DT_PROD.Value;
    end else
    begin
      FmEfdIcmsIpiK300_v03_0_2_a.EdKndTab.ValueVariant     := Integer(TEstqSPEDTabSorc.estsNoSrc);
    end;
    //
    FmEfdIcmsIpiK300_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK300_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK301(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK301_v03_0_2_a, FmEfdIcmsIpiK301_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK301_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK301_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK301_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK301_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK301_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    FmEfdIcmsIpiK301_v03_0_2_a.EdIDSeq1.ValueVariant       := DfSEII_v03_0_2_a.QrK300IDSeq1.Value;
    //
    if SQLType = stUpd then
    begin
      //FmEfdIcmsIpiK301_v03_0_2_a.FESTSTabSorc              := DfSEII_v03_0_2_a.QrK301ESTSTabSorc.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK301ID_SEK.Value;
      //FmEfdIcmsIpiK301_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.QrK301ESOMIEM.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.FOriOpeProc               := DfSEII_v03_0_2_a.QrK301OriOpeProc.Value;
      //FmEfdIcmsIpiK301_v03_0_2_a.FOrigemIDKnd              := DfSEII_v03_0_2_a.QrK301OrigemIDKnd.Value;
      //
      FmEfdIcmsIpiK301_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK301KndTab.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK301KndCod.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK301KndNSU.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.QrK301KndItm.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK301KndAID.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK301KndNiv.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdIDSeq2.ValueVariant     := DfSEII_v03_0_2_a.QrK301IDSeq2.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdJmpMovID.ValueVariant   := DfSEII_v03_0_2_a.QrK301JmpMovID.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdJmpMovCod.ValueVariant  := DfSEII_v03_0_2_a.QrK301JmpMovCod.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdJmpNivel1.ValueVariant  := DfSEII_v03_0_2_a.QrK301JmpNivel1.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdJmpNivel2.ValueVariant  := DfSEII_v03_0_2_a.QrK301JmpNivel2.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdPaiMovID.ValueVariant   := DfSEII_v03_0_2_a.QrK301PaiMovID.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdPaiMovCod.ValueVariant  := DfSEII_v03_0_2_a.QrK301PaiMovCod.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdPaiNivel1.ValueVariant  := DfSEII_v03_0_2_a.QrK301PaiNivel1.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdPaiNivel2.ValueVariant  := DfSEII_v03_0_2_a.QrK301PaiNivel2.Value;
      //
      FmEfdIcmsIpiK301_v03_0_2_a.EdGraGruX.Text            := DfSEII_v03_0_2_a.QrK301COD_ITEM.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.QrK301COD_ITEM.Value;
      FmEfdIcmsIpiK301_v03_0_2_a.EdQTD.ValueVariant        := DfSEII_v03_0_2_a.QrK301QTD.Value;
      //
      FmEfdIcmsIpiK301_v03_0_2_a.CkContinuar.Checked       := False;
      FmEfdIcmsIpiK301_v03_0_2_a.CkContinuar.Enabled       := False;
    end else
    begin
      FmEfdIcmsIpiK301_v03_0_2_a.CkContinuar.Checked       := True;
      FmEfdIcmsIpiK301_v03_0_2_a.CkContinuar.Enabled       := True;
    end;
    //
    FmEfdIcmsIpiK301_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK301_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.InsUpdK302(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmEfdIcmsIpiK302_v03_0_2_a, FmEfdIcmsIpiK302_v03_0_2_a, afmoNegarComAviso) then
  begin
    FmEfdIcmsIpiK302_v03_0_2_a.ImgTipo.SQLType := SQLType;
    //
    FmEfdIcmsIpiK302_v03_0_2_a.EdImporExpor.ValueVariant   := DfSEII_v03_0_2_a.QrK100ImporExpor.Value;
    FmEfdIcmsIpiK302_v03_0_2_a.EdAnoMes.ValueVariant       := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
    FmEfdIcmsIpiK302_v03_0_2_a.EdEmpresa.ValueVariant      := DfSEII_v03_0_2_a.QrK100Empresa.Value;
    FmEfdIcmsIpiK302_v03_0_2_a.EdPeriApu.ValueVariant      := DfSEII_v03_0_2_a.QrK100PeriApu.Value;
    FmEfdIcmsIpiK302_v03_0_2_a.EdIDSeq1.ValueVariant       := DfSEII_v03_0_2_a.QrK300IDSeq1.Value;
    //
    if SQLType = stUpd then
    begin
      //FmEfdIcmsIpiK302_v03_0_2_a.FESTSTabSorc              := DfSEII_v03_0_2_a.QrK302ESTSTabSorc.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.FID_SEK                   := DfSEII_v03_0_2_a.QrK302ID_SEK.Value;
      //FmEfdIcmsIpiK302_v03_0_2_a.FESOMIEM                  := DfSEII_v03_0_2_a.QrK302ESOMIEM.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.FOriOpeProc               := DfSEII_v03_0_2_a.QrK302OriOpeProc.Value;
      //FmEfdIcmsIpiK302_v03_0_2_a.FOrigemIDKnd              := DfSEII_v03_0_2_a.QrK302OrigemIDKnd.Value;
      //
      FmEfdIcmsIpiK302_v03_0_2_a.EdKndTab.ValueVariant     := DfSEII_v03_0_2_a.QrK302KndTab.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdKndCod.ValueVariant     := DfSEII_v03_0_2_a.QrK302KndCod.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdKndNSU.ValueVariant     := DfSEII_v03_0_2_a.QrK302KndNSU.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdKndItm.ValueVariant     := DfSEII_v03_0_2_a.QrK302KndItm.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdKndAID.ValueVariant     := DfSEII_v03_0_2_a.QrK302KndAID.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdKndNiv.ValueVariant     := DfSEII_v03_0_2_a.QrK302KndNiv.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdIDSeq2.ValueVariant     := DfSEII_v03_0_2_a.QrK302IDSeq2.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdJmpMovID.ValueVariant   := DfSEII_v03_0_2_a.QrK302JmpMovID.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdJmpMovCod.ValueVariant  := DfSEII_v03_0_2_a.QrK302JmpMovCod.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdJmpNivel1.ValueVariant  := DfSEII_v03_0_2_a.QrK302JmpNivel1.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdJmpNivel2.ValueVariant  := DfSEII_v03_0_2_a.QrK302JmpNivel2.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdPaiMovID.ValueVariant   := DfSEII_v03_0_2_a.QrK302PaiMovID.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdPaiMovCod.ValueVariant  := DfSEII_v03_0_2_a.QrK302PaiMovCod.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdPaiNivel1.ValueVariant  := DfSEII_v03_0_2_a.QrK302PaiNivel1.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdPaiNivel2.ValueVariant  := DfSEII_v03_0_2_a.QrK302PaiNivel2.Value;
      //
      FmEfdIcmsIpiK302_v03_0_2_a.EdGraGruX.Text            := DfSEII_v03_0_2_a.QrK302COD_ITEM.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.CBGraGruX.KeyValue        := DfSEII_v03_0_2_a.QrK302COD_ITEM.Value;
      FmEfdIcmsIpiK302_v03_0_2_a.EdQTD.ValueVariant        := DfSEII_v03_0_2_a.QrK302QTD.Value;
      //
      FmEfdIcmsIpiK302_v03_0_2_a.CkContinuar.Checked       := False;
      FmEfdIcmsIpiK302_v03_0_2_a.CkContinuar.Enabled       := False;
    end else
    begin
      FmEfdIcmsIpiK302_v03_0_2_a.CkContinuar.Checked       := True;
      FmEfdIcmsIpiK302_v03_0_2_a.CkContinuar.Enabled       := True;
    end;
    //
    FmEfdIcmsIpiK302_v03_0_2_a.ShowModal;
    FmEfdIcmsIpiK302_v03_0_2_a.Destroy;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.JanelaPorDblClick(Sender: TObject);
var
  IMEC, IMEI: Integer;
  DBGrid: TDBGrid;
begin
(*&�% tempor�rio por coausa do VS no Planning!
  IMEI := 0;
  IMEC := 0;
  DBGrid := TDBGrid(Sender);
  //
  if (DBGrid.SelectedField.FieldName = 'KndNSU') then
    IMEC := DBGrid.SelectedField.AsInteger;
  if (DBGrid.SelectedField.FieldName = 'KndItm')
  or (DBGrid.SelectedField.FieldName = 'KndItmOri')
  or (DBGrid.SelectedField.FieldName = 'KndItmDst') then
    IMEI := DBGrid.SelectedField.AsInteger;
  //
  if IMEI <> 0 then
    VS_PF.MostraFormVSMovIts(IMEI);
  if IMEC <> 0 then
    VS_PF.MostraFormVSMovCab(IMEC);
%�&*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K2301Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK230, TDBGrid(DBGK230),
    'efdicmsipik230', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K2351Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK235, TDBGrid(DBGK235),
    'efdicmsipik235', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K2501Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK250, TDBGrid(DBGK250),
    'efdicmsipik250', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K2551Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK255, TDBGrid(DBGK255),
    'efdicmsipik255', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K2601Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK260, TDBGrid(DBGK260),
    'efdicmsipik260', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K2651Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK265, TDBGrid(DBGK265),
    'efdicmsipik265', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K270K210K220K230K250K260K291K292K301eK3021Click(
  Sender: TObject);
begin
  PCK200.ActivePageIndex := 3;
  PCKPrd.ActivePageIndex := 4;
  InsUpdK270(0, stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K275K215K220K235K255EK2651Click(
  Sender: TObject);
const
  sprocName = 'FmEfdIcmsIpiE001_v03_0_2_a.K275K215K220K235K255EK2651Click()';
  //
  function QrOK(Query: TmySQLQuery): Boolean;
  begin
    Result := (Query.State <> dsInactive) and (Query.RecordCount > 0);
  end;
var
  Habilitado: Boolean;
  RegPai, RegFilho: Integer;
begin
  Habilitado := False;
  case PC_K270.ActivePageIndex of
    (*K215*)0: Habilitado :=  QrOK(DfSEII_v03_0_2_a.QrK270_210);
    (*K220*)1: Habilitado :=  QrOK(DfSEII_v03_0_2_a.QrK270_220);
    (*K235*)2: Habilitado :=  QrOK(DfSEII_v03_0_2_a.QrK270_230);
    (*K255*)3: Habilitado :=  QrOK(DfSEII_v03_0_2_a.QrK270_250);
    (*K265*)4: Habilitado :=  QrOK(DfSEII_v03_0_2_a.QrK270_260);
    (*K29X*)5:
    begin
      Habilitado :=  False;
      Geral.MB_Info(
      'Registros K290 a K302 n�o tem registros filhos no Registro K270!');
      Exit;
    end;
    else begin
      Geral.MB_Erro('"PC_K270.ActivePageIndex" n�o implementado em ' +
      sProcName);
      Exit;
    end;
  end;
  case PC_K270.ActivePageIndex of
    (*K215*)0: RegPai :=  210;
    (*K220*)1: RegPai :=  220;
    (*K235*)2: RegPai :=  230;
    (*K255*)3: RegPai :=  250;
    (*K265*)4: RegPai :=  260;
    //(*K29X*)5: RegPai :=  '???';
    else RegPai :=  0;
  end;
  if not Habilitado then
  begin
    Geral.MB_Erro('Antes de criar o registro filho crie o registro Pai: K' +
    Geral.FF0(RegPai));
    Exit;
  end;
  case PC_K270.ActivePageIndex of
    (*K215*)0: RegFilho :=  215;
    (*K220*)1: RegFilho :=  220;
    (*K235*)2: RegFilho :=  235;
    (*K255*)3: RegFilho :=  255;
    (*K265*)4: RegFilho :=  265;
    //(*K29X*)5: RegFilho :=  0;
    else RegFilho :=  0;
  end;
  PCK200.ActivePageIndex := 3;
  PCKPrd.ActivePageIndex := 4;
  InsUpdK275(RegPai, RegFilho, stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K2901Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK290, TDBGrid(DBGK290),
    'efdicmsipik290', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K290Ordemdeproduo1Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 3;
  PCKPrd.ActivePageIndex := 4;
  InsUpdK290(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K2911Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK291, TDBGrid(DBGK291),
    'efdicmsipik291', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K291ProduoconjuntaItensproduzidos1Click(
  Sender: TObject);
begin
  PCK200.ActivePageIndex := 3;
  PCKPrd.ActivePageIndex := 4;
  InsUpdK291(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K2921Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK292, TDBGrid(DBGK292),
    'efdicmsipik292', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K292ProduoconjuntaItensconsumidos1Click(
  Sender: TObject);
begin
  PCK200.ActivePageIndex := 3;
  PCKPrd.ActivePageIndex := 4;
  InsUpdK292(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K3001Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK300, TDBGrid(DBGK300),
    'efdicmsipik300', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K300Ordemdeproduo1Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 3;
  PCKPrd.ActivePageIndex := 5;
  InsUpdK300(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K3011Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK301, TDBGrid(DBGK301),
    'efdicmsipik301', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K301Itemproduzidos1Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 3;
  PCKPrd.ActivePageIndex := 5;
  InsUpdK301(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K3021Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DfSEII_v03_0_2_a.QrK302, TDBGrid(DBGK302),
    'efdicmsipik302', ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'],
    ['ImporExpor', 'AnoMes', 'Empresa', 'PeriApu', 'IDSeq1', 'IDSeq2'], istPergunta, '');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.K302Itemconsumido1Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 3;
  PCKPrd.ActivePageIndex := 5;
  InsUpdK302(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.LocalizarVMI();
var
  KndItm: Integer;
  //
  function ParteSQL(Tabela, Campo: String; Uniao: Boolean): String;
  var
    SQL_U: String;
  begin
    if not Uniao then
      SQL_U := ''
    else
      SQL_U := 'UNION' + sLineBreak;
    Result := Geral.ATS([
      'SELECT "' + Tabela + '.' + Campo + '" Tabela, ' + Campo + ' KndItm ',
      'FROM ' + Tabela,
      'WHERE ' + Campo + '=' + geral.FF0(KndItm),
      'AND ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
      'AND AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
      'AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
      'AND PeriApu=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100PeriApu.Value),
      '',
      SQL_U,
      '']);
  end;
const
  Aviso  = '...';
  Titulo = 'Sele��o de IME-I';
  Prompt = 'Selecione o IME-I';
var
  Txt: String;
begin
  Txt := '0';
  if InputQuery('Localiza��o de IME-I', 'Informe o IME-I', Txt) then
  begin
    KndItm := Geral.IMV(Txt);
    if (KndItm <> 0 ) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(DfSEII_v03_0_2_a.QrVMI, Dmod.MyDB, [
      ParteSQL('efdicmsipik210', 'KndItm', True),
      ParteSQL('efdicmsipik215', 'KndItm', True),
      ParteSQL('efdicmsipik220', 'KndItmOri', True),
      ParteSQL('efdicmsipik220', 'KndItmDst', True),
      ParteSQL('efdicmsipik230', 'KndItm', True),
      ParteSQL('efdicmsipik235', 'KndItm', True),
      ParteSQL('efdicmsipik250', 'KndItm', True),
      ParteSQL('efdicmsipik255', 'KndItm', True),
      ParteSQL('efdicmsipik260', 'KndItm', True),
      ParteSQL('efdicmsipik265', 'KndItm', True),
      ParteSQL('efdicmsipik270', 'KndItm', True),
      // ultimo...
      ParteSQL('efdicmsipik275', 'KndItm', False),
      '']);
      //Geral.MB_SQL(nil, DfSEII_v03_0_2_a.QrVMI);
      if DBCheck.EscolheCodigoUniGrid(Aviso, Titulo, Prompt, DfSEII_v03_0_2_a.DsVMI, False, False) then
      begin
        Geral.MB_Info(DfSEII_v03_0_2_a.QrVMI.FieldByName('Tabela').AsString);
      end;
    end;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.MostraFormEFD_1010();
const
  SIM = 'S';
var
  SQLType: TSQLType;
begin
  if DfSEII_v03_0_2_a.Qr1010.RecordCount > 0 then
    SQLType := stUpd
  else
    SQLType := stIns;
  if UmyMod.FormInsUpd_Cria(TFmEfdIcmsIpi1010_v03_0_2_a, FmEfdIcmsIpi1010_v03_0_2_a, afmoNegarComAviso,
  DfSEII_v03_0_2_a.Qr1010, stUpd) then
  begin
    FmEfdIcmsIpi1010_v03_0_2_a.ImgTipo.SQLType := SQLType;
    FmEfdIcmsIpi1010_v03_0_2_a.EdImporExpor.ValueVariant := DfSEII_v03_0_2_a.QrE001ImporExpor.Value;
    FmEfdIcmsIpi1010_v03_0_2_a.EdAnoMes.ValueVariant := DfSEII_v03_0_2_a.QrE001AnoMes.Value;
    FmEfdIcmsIpi1010_v03_0_2_a.EdEmpresa.ValueVariant := DfSEII_v03_0_2_a.QrE001Empresa.Value;
    //
    FmEfdIcmsIpi1010_v03_0_2_a.EdLinArq.ValueVariant := DfSEII_v03_0_2_a.QrE500LinArq.Value;
    if SQLType = stUpd then
    begin
      FmEfdIcmsIpi1010_v03_0_2_a.EdLinArq.ValueVariant                 := DfSEII_v03_0_2_a.Qr1010LinArq.Value;
      //
      FmEfdIcmsIpi1010_v03_0_2_a.CkIND_EXP.Checked                     := DfSEII_v03_0_2_a.Qr1010IND_EXP.Value   = SIM;
      FmEfdIcmsIpi1010_v03_0_2_a.CkIND_CCRF.Checked                    := DfSEII_v03_0_2_a.Qr1010IND_CCRF.Value  = SIM;
      FmEfdIcmsIpi1010_v03_0_2_a.CkIND_COMB.Checked                    := DfSEII_v03_0_2_a.Qr1010IND_COMB.Value  = SIM;
      FmEfdIcmsIpi1010_v03_0_2_a.CkIND_USINA.Checked                   := DfSEII_v03_0_2_a.Qr1010IND_USINA.Value = SIM;
      FmEfdIcmsIpi1010_v03_0_2_a.CkIND_VA.Checked                      := DfSEII_v03_0_2_a.Qr1010IND_VA.Value    = SIM;
      FmEfdIcmsIpi1010_v03_0_2_a.CkIND_EE.Checked                      := DfSEII_v03_0_2_a.Qr1010IND_EE.Value    = SIM;
      FmEfdIcmsIpi1010_v03_0_2_a.CkIND_CART.Checked                    := DfSEII_v03_0_2_a.Qr1010IND_CART.Value  = SIM;
      FmEfdIcmsIpi1010_v03_0_2_a.CkIND_FORM.Checked                    := DfSEII_v03_0_2_a.Qr1010IND_FORM.Value  = SIM;
      FmEfdIcmsIpi1010_v03_0_2_a.CkIND_AER.Checked                     := DfSEII_v03_0_2_a.Qr1010IND_AER.Value   = SIM;
      FmEfdIcmsIpi1010_v03_0_2_a.CkIND_GIAF1.Checked                   := DfSEII_v03_0_2_a.Qr1010IND_GIAF1.Value = SIM;
      FmEfdIcmsIpi1010_v03_0_2_a.CkIND_GIAF3.Checked                   := DfSEII_v03_0_2_a.Qr1010IND_GIAF3.Value = SIM;
      FmEfdIcmsIpi1010_v03_0_2_a.CkIND_GIAF4.Checked                   := DfSEII_v03_0_2_a.Qr1010IND_GIAF4.Value = SIM;
      FmEfdIcmsIpi1010_v03_0_2_a.CkIND_REST_RESSARC_COMPL_ICMS.Checked := DfSEII_v03_0_2_a.Qr1010IND_REST_RESSARC_COMPL_ICMS.Value = SIM;

    end;
    //
    FmEfdIcmsIpi1010_v03_0_2_a.ShowModal;
    FmEfdIcmsIpi1010_v03_0_2_a.Destroy;
    //
    DfSEII_v03_0_2_a.Reopen1010();
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.MostraFormEFD_E110();
var
  SQLType: TSQLType;
begin
//  MyObjects.MostraPopUpDeBotao(PMApuracao, BtApuracao);
  PCE100.ActivePageIndex := 0;
  if (DfSEII_v03_0_2_a.QrE100.State = dsInactive) or (DfSEII_v03_0_2_a.QrE100.RecordCount = 0) then
  begin
    Geral.MensagemBox('Informe primeiro um intervalo de apura��o!', 'Aviso',
    MB_OK+MB_ICONWARNING);
  end else begin
    if DfSEII_v03_0_2_a.QrE110.RecordCount > 0 then
      SQLType := stUpd
    else
      SQLType := stIns;
    if UmyMod.FormInsUpd_Cria(TFmEfdIcmsIpiE110_v03_0_2_a, FmEfdIcmsIpiE110_v03_0_2_a, afmoNegarComAviso,
    DfSEII_v03_0_2_a.QrE110, stUpd) then
    begin
      FmEfdIcmsIpiE110_v03_0_2_a.ImgTipo.SQLType := SQLType;
      FmEfdIcmsIpiE110_v03_0_2_a.EdImporExpor.ValueVariant := DfSEII_v03_0_2_a.QrE001ImporExpor.Value;
      FmEfdIcmsIpiE110_v03_0_2_a.EdAnoMes.ValueVariant := DfSEII_v03_0_2_a.QrE001AnoMes.Value;
      FmEfdIcmsIpiE110_v03_0_2_a.EdEmpresa.ValueVariant := DfSEII_v03_0_2_a.QrE001Empresa.Value;
      FmEfdIcmsIpiE110_v03_0_2_a.EdE100.ValueVariant := DfSEII_v03_0_2_a.QrE100LinArq.Value;
      //
      FmEfdIcmsIpiE110_v03_0_2_a.TPDT_INI.Date := DfSEII_v03_0_2_a.QrE100DT_INI.Value;
      FmEfdIcmsIpiE110_v03_0_2_a.TPDT_FIN.Date := DfSEII_v03_0_2_a.QrE100DT_FIN.Value;
      //
      FmEfdIcmsIpiE110_v03_0_2_a.EdLinArq.ValueVariant := DfSEII_v03_0_2_a.QrE100LinArq.Value;
      if SQLType = stUpd then
      begin
        FmEfdIcmsIpiE110_v03_0_2_a.EdVL_TOT_DEBITOS.ValueVariant :=      DfSEII_v03_0_2_a.QrE110VL_TOT_DEBITOS.Value;
        FmEfdIcmsIpiE110_v03_0_2_a.EdVL_AJ_DEBITOS.ValueVariant :=       DfSEII_v03_0_2_a.QrE110VL_AJ_DEBITOS.Value;
        FmEfdIcmsIpiE110_v03_0_2_a.EdVL_TOT_AJ_DEBITOS.ValueVariant :=   DfSEII_v03_0_2_a.QrE110VL_TOT_AJ_DEBITOS.Value;
        FmEfdIcmsIpiE110_v03_0_2_a.EdVL_ESTORNOS_CRED.ValueVariant :=    DfSEII_v03_0_2_a.QrE110VL_ESTORNOS_CRED.Value;
        FmEfdIcmsIpiE110_v03_0_2_a.EdVL_TOT_CREDITOS.ValueVariant :=     DfSEII_v03_0_2_a.QrE110VL_TOT_CREDITOS.Value;
        FmEfdIcmsIpiE110_v03_0_2_a.EdVL_AJ_CREDITOS.ValueVariant :=      DfSEII_v03_0_2_a.QrE110VL_AJ_CREDITOS.Value;
        FmEfdIcmsIpiE110_v03_0_2_a.EdVL_TOT_AJ_CREDITOS.ValueVariant :=  DfSEII_v03_0_2_a.QrE110VL_TOT_AJ_CREDITOS.Value;
        FmEfdIcmsIpiE110_v03_0_2_a.EdVL_ESTORNOS_DEB.ValueVariant :=     DfSEII_v03_0_2_a.QrE110VL_ESTORNOS_DEB.Value;
        FmEfdIcmsIpiE110_v03_0_2_a.EdVL_SLD_CREDOR_ANT.ValueVariant :=   DfSEII_v03_0_2_a.QrE110VL_SLD_CREDOR_ANT.Value;
        FmEfdIcmsIpiE110_v03_0_2_a.EdVL_SLD_APURADO.ValueVariant :=      DfSEII_v03_0_2_a.QrE110VL_SLD_APURADO.Value;
        FmEfdIcmsIpiE110_v03_0_2_a.EdVL_TOT_DED.ValueVariant :=          DfSEII_v03_0_2_a.QrE110VL_TOT_DED.Value;
        FmEfdIcmsIpiE110_v03_0_2_a.EdVL_ICMS_RECOLHER.ValueVariant :=    DfSEII_v03_0_2_a.QrE110VL_ICMS_RECOLHER.Value;
        FmEfdIcmsIpiE110_v03_0_2_a.EdVL_SLD_CREDOR_TRANSPORTAR.ValueVariant :=  DfSEII_v03_0_2_a.QrE110VL_SLD_CREDOR_TRANSPORTAR.Value;
        FmEfdIcmsIpiE110_v03_0_2_a.EdDEB_ESP.ValueVariant :=             DfSEII_v03_0_2_a.QrE110DEB_ESP.Value;
      end;
      //
      FmEfdIcmsIpiE110_v03_0_2_a.ShowModal;
      FmEfdIcmsIpiE110_v03_0_2_a.Destroy;
    end;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.MostraFormEFD_E520();
var
  SQLType: TSQLType;
begin
  PCE100.ActivePageIndex := 0;
  if (DfSEII_v03_0_2_a.QrE500.State = dsInactive) or (DfSEII_v03_0_2_a.QrE500.RecordCount = 0) then
  begin
    Geral.MensagemBox('Informe primeiro um intervalo de apura��o!', 'Aviso',
    MB_OK+MB_ICONWARNING);
  end else begin
    if DfSEII_v03_0_2_a.QrE520.RecordCount > 0 then
      SQLType := stUpd
    else
      SQLType := stIns;
    if UmyMod.FormInsUpd_Cria(TFmEfdIcmsIpiE520_v03_0_2_a, FmEfdIcmsIpiE520_v03_0_2_a, afmoNegarComAviso,
    DfSEII_v03_0_2_a.QrE520, stUpd) then
    begin
      FmEfdIcmsIpiE520_v03_0_2_a.ImgTipo.SQLType := SQLType;
      FmEfdIcmsIpiE520_v03_0_2_a.EdImporExpor.ValueVariant := DfSEII_v03_0_2_a.QrE001ImporExpor.Value;
      FmEfdIcmsIpiE520_v03_0_2_a.EdAnoMes.ValueVariant := DfSEII_v03_0_2_a.QrE001AnoMes.Value;
      FmEfdIcmsIpiE520_v03_0_2_a.EdEmpresa.ValueVariant := DfSEII_v03_0_2_a.QrE001Empresa.Value;
      FmEfdIcmsIpiE520_v03_0_2_a.EdE500.ValueVariant := DfSEII_v03_0_2_a.QrE500LinArq.Value;
      //
      FmEfdIcmsIpiE520_v03_0_2_a.TPDT_INI.Date := DfSEII_v03_0_2_a.QrE500DT_INI.Value;
      FmEfdIcmsIpiE520_v03_0_2_a.TPDT_FIN.Date := DfSEII_v03_0_2_a.QrE500DT_FIN.Value;
      //
      FmEfdIcmsIpiE520_v03_0_2_a.EdLinArq.ValueVariant := DfSEII_v03_0_2_a.QrE500LinArq.Value;
      if SQLType = stUpd then
      begin
        FmEfdIcmsIpiE520_v03_0_2_a.EdVL_SD_ANT_IPI.ValueVariant := DfSEII_v03_0_2_a.QrE520VL_SD_ANT_IPI.Value;
        FmEfdIcmsIpiE520_v03_0_2_a.EdVL_DEB_IPI.ValueVariant    := DfSEII_v03_0_2_a.QrE520VL_DEB_IPI.Value;
        FmEfdIcmsIpiE520_v03_0_2_a.EdVL_CRED_IPI.ValueVariant   := DfSEII_v03_0_2_a.QrE520VL_CRED_IPI.Value;
        FmEfdIcmsIpiE520_v03_0_2_a.EdVL_OD_IPI.ValueVariant     := DfSEII_v03_0_2_a.QrE520VL_OD_IPI.Value;
        FmEfdIcmsIpiE520_v03_0_2_a.EdVL_OC_IPI.ValueVariant     := DfSEII_v03_0_2_a.QrE520VL_OC_IPI.Value;
        FmEfdIcmsIpiE520_v03_0_2_a.EdVL_SC_IPI.ValueVariant     := DfSEII_v03_0_2_a.QrE520VL_SC_IPI.Value;
        FmEfdIcmsIpiE520_v03_0_2_a.EdVL_SD_IPI.ValueVariant     := DfSEII_v03_0_2_a.QrE520VL_SD_IPI.Value;
      end;
      //
      FmEfdIcmsIpiE520_v03_0_2_a.ShowModal;
      FmEfdIcmsIpiE520_v03_0_2_a.Destroy;
    end;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Movimentaies1Click(Sender: TObject);
begin
(*&�% tempor�rio por coausa do VS no Planning!
  AppPF.MostraFormCfgMovEFD(
    DfSEII_v03_0_2_a.QrE001ImporExpor.Value,
    DfSEII_v03_0_2_a.QrE001AnoMes.Value,
    DfSEII_v03_0_2_a.QrE001Empresa.Value,
    0);
%�&*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.N250Itensproduzidos1Click(
  Sender: TObject);
begin
  PCK200.ActivePageIndex := 3;
  PCKPrd.ActivePageIndex := 1;
  InsUpdK250(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.N255InsumosConsumidos1Click(
  Sender: TObject);
begin
  PCK200.ActivePageIndex := 3;
  PCKPrd.ActivePageIndex := 1;
  InsUpdK255(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.IncluiK230_2Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 3;
  PCKPrd.ActivePageIndex := 0;
  InsUpdK230(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.IncluiK235_2Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 3;
  PCKPrd.ActivePageIndex := 0;
  InsUpdK235(stIns);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.odos1Click(Sender: TObject);
begin
  ExcluiItensNormaisDeProducao();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Outrasbaixaseestoque1Click(
  Sender: TObject);
begin
(*&�% tempor�rio por coausa do VS no Planning!
  AppPF.MostraFormCfgEstqOthersEFD(
    DfSEII_v03_0_2_a.QrE001ImporExpor.Value,
    DfSEII_v03_0_2_a.QrE001AnoMes.Value,
    DfSEII_v03_0_2_a.QrE001Empresa.Value,
    0);
%�&*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PCK200Change(Sender: TObject);
begin
  if PCK200.ActivePageIndex = 3 then
  begin
    if (DfSEII_v03_0_2_a.QrK230.State <> dsInactive) and (DfSEII_v03_0_2_a.QrK230.RecordCount > 0) then
      PCKPrd.ActivePageIndex := 0
    else
//    if (DfSEII_v03_0_2_a.QrK250.State <> dsInactive) and (DfSEII_v03_0_2_a.QrK250.RecordCount > 0) then
      PCKPrd.ActivePageIndex := 1
    //else
      //PCK235.ActivePageIndex := 2;
  end;
end;

function TFmEfdIcmsIpiE001_v03_0_2_a.PeriodoJaExiste(ImporExpor, AnoMes, Empresa: Integer;
Avisa: Boolean): Boolean;
var
  Qry1: TmySQLQuery;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry1.Close;
    Qry1.Database := Dmod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT COUNT(*) Registros ',
    'FROM efdicmsipie001 ',
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
    Result := Qry1.FieldByName('Registros').AsInteger > 0;
    if Result and Avisa then
    begin
      Geral.MensagemBox('O periodo selecionado j� existe!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
  finally
    Qry1.Free;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PME111Popup(Sender: TObject);
var
  Habilita, Habil2, Habil3(*, Exclui*): Boolean;
begin
  // E111
  Habilita := (DfSEII_v03_0_2_a.QrE110.State <> dsInactive) and (DfSEII_v03_0_2_a.QrE110.RecordCount = 1);
  Incluinovoajuste1.Enabled := Habilita;
  Habilita := Habilita and
    (DfSEII_v03_0_2_a.QrE111.State <> dsInactive) and (DfSEII_v03_0_2_a.QrE111.RecordCount > 0);
  Alteraajusteselecionado1.Enabled := Habilita;
  Habil2 := Habilita;
  // E112
  Incluinovainformaoadicional1.Enabled := Habil2;
  // E113
  Incluinovaidentificaodedocumentofiscal1.Enabled := Habil2;
  // E112
  Habil2 := Habilita and
    (DfSEII_v03_0_2_a.QrE112.State <> dsInactive) and (DfSEII_v03_0_2_a.QrE112.RecordCount > 0);
  Alterainformaoadicionalselecionada1.Enabled := Habil2;
  Excluiinformaoadicionalselecionada1.Enabled := Habil2;
  // E113
  Habil3 := Habilita and
    (DfSEII_v03_0_2_a.QrE113.State <> dsInactive) and (DfSEII_v03_0_2_a.QrE113.RecordCount > 0);
  Alteraidentificaoselecionada1.Enabled := Habil3;
  Excluiidentificaoselecionada1.Enabled := Habil3;
  // E111
  Excluiajusteselecionado1.Enabled := Habilita and (not Habil2) and (not Habil3);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PME500Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluinovointervaloIPI1, DfSEII_v03_0_2_a.QrE001);
  MyObjects.HabilitaMenuItemItsUpd(AlteraintervaloIPIselecionado1, DfSEII_v03_0_2_a.QrE500);
  MyObjects.HabilitaMenuItemItsDel(ExcluiintervaloIPIselecionado1, DfSEII_v03_0_2_a.QrE500);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PME530Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiajustedaapuracaodoIPI1, DfSEII_v03_0_2_a.QrE520);
  MyObjects.HabilitaMenuItemItsUpd(AlteraajustedaapuracaodoIPIselecionado1, DfSEII_v03_0_2_a.QrE530);
  MyObjects.HabilitaMenuItemItsDel(ExcluiajustedaapuracaodoIPIselecionado1, DfSEII_v03_0_2_a.QrE530);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PMH005Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluidatadeinventrio1, DfSEII_v03_0_2_a.QrE001);
  MyObjects.HabilitaMenuItemItsUpd(AlteraInventrio1, DfSEII_v03_0_2_a.QrH005);
  MyObjects.HabilitaMenuItemCabDel(Excluiinventrio1, DfSEII_v03_0_2_a.QrH005, DfSEII_v03_0_2_a.QrH010);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PMH010Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Importadebalano1, DfSEII_v03_0_2_a.QrH005);
  MyObjects.HabilitaMenuItemItsIns(Incluiitem1, DfSEII_v03_0_2_a.QrH005);
  MyObjects.HabilitaMenuItemItsUpd(Alteraitem1, DfSEII_v03_0_2_a.QrH010);
  MyObjects.HabilitaMenuItemCabDel(Excluiitemns1, DfSEII_v03_0_2_a.QrH010, DfSEII_v03_0_2_a.QrH020);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PMH020Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluiinformaocomplementar1, DfSEII_v03_0_2_a.QrH010);
  MyObjects.HabilitaMenuItemItsUpd(Alterainformaocomplementar1, DfSEII_v03_0_2_a.QrH020);
  MyObjects.HabilitaMenuItemItsDel(Excluiinformaocomplementar1, DfSEII_v03_0_2_a.QrH020);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PMK100Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluinovointervalodeProduoeEstoque1, DfSEII_v03_0_2_a.QrE001);
  MyObjects.HabilitaMenuItemItsUpd(AlteraintervalodeProduoeEstoque1, DfSEII_v03_0_2_a.QrK100);
  MyObjects.HabilitaMenuItemItsDel(ExcluiintervalodeProduoeEstoque1, DfSEII_v03_0_2_a.QrK100);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PMK200Popup(Sender: TObject);
var
  b200, b280: Boolean;
begin
  b200 := PCK200.ActivePageIndex = 0;
  b280 := PCK200.ActivePageIndex = 4;
  //
  MyObjects.HabilitaMenuItemItsIns(ImportaK200_1, DfSEII_v03_0_2_a.QrK100);
  //MyObjects.HabilitaMenuItemItsIns(IncluiK200_1, DfSEII_v03_0_2_a.QrK200);
  MyObjects.HabilitaMenuItemItsIns(IncluiK200_1, DfSEII_v03_0_2_a.QrK100);
  MyObjects.HabilitaMenuItemItsUpd(AlteraK200_1, DfSEII_v03_0_2_a.QrK200, b200);
  MyObjects.HabilitaMenuItemItsDel(ExcluiK200_1, DfSEII_v03_0_2_a.QrK200, b200);
  MyObjects.HabilitaMenuItemItsDel(ExcluiK200_280_1, DfSEII_v03_0_2_a.QrK200, b200 or b280);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PMK220Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ImportaK220_1, DfSEII_v03_0_2_a.QrK100);
  MyObjects.HabilitaMenuItemItsIns(IncluiK220_1, DfSEII_v03_0_2_a.QrK220);
  MyObjects.HabilitaMenuItemItsUpd(AlteraK220_1, DfSEII_v03_0_2_a.QrK220);
  MyObjects.HabilitaMenuItemItsDel(ExcluiK220_1, DfSEII_v03_0_2_a.QrK220);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PMK230Popup(Sender: TObject);
begin
  //
  MyObjects.HabilitaMenuItemCabDel(K2301, DfSEII_v03_0_2_a.QrK230, DfSEII_v03_0_2_a.QrK235);
  MyObjects.HabilitaMenuItemCabDel(K2501, DfSEII_v03_0_2_a.QrK250, DfSEII_v03_0_2_a.QrK255);
  MyObjects.HabilitaMenuItemCabDel(K2601, DfSEII_v03_0_2_a.QrK260, DfSEII_v03_0_2_a.QrK265);
  //
  MyObjects.HabilitaMenuItemCabDelC1I2(K2901, DfSEII_v03_0_2_a.QrK290, DfSEII_v03_0_2_a.QrK291, DfSEII_v03_0_2_a.QrK292);
  MyObjects.HabilitaMenuItemCabDelC1I2(K3001, DfSEII_v03_0_2_a.QrK300, DfSEII_v03_0_2_a.QrK301, DfSEII_v03_0_2_a.QrK302);
  //
  AlteraK2X0_1.Enabled := False;
  AlteraK2X5_1.Enabled := False;
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiK210_1, DfSEII_v03_0_2_a.QrK100);
  MyObjects.HabilitaMenuItemItsIns(IncluiK230_2, DfSEII_v03_0_2_a.QrK100);
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiK215_1, DfSEII_v03_0_2_a.QrK210);
  MyObjects.HabilitaMenuItemItsIns(IncluiK235_2, DfSEII_v03_0_2_a.QrK230);
  //
  AlteraK2X5_2.Visible := False;
  AlteraK2X5_3.Visible := False;
  case PCK200.ActivePageIndex of
    1:
    begin
      case PCK21X.ActivePageIndex of
        0:
        begin
          AlteraK2X0_1.Caption := 'Item de Desmontagem - Origem';
          MyObjects.HabilitaMenuItemItsUpd(AlteraK2X0_1, DfSEII_v03_0_2_a.QrK210);
          AlteraK2X0_1.Tag := 210;
          //
          AlteraK2X5_1.Caption := 'Item de Desmontagem - Destino';
          MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_1, DfSEII_v03_0_2_a.QrK215);
          AlteraK2X5_1.Tag := 215;
        end;
      end;
    end;
(*
    2: // K200
    begin
    end;
*)
    3:
    begin
      case PCKPrd.ActivePageIndex of
        0: // K230
        begin
          AlteraK2X0_1.Caption := 'K230 - Item Produzido no Estabelecimento';
          MyObjects.HabilitaMenuItemItsUpd(AlteraK2X0_1, DfSEII_v03_0_2_a.QrK230);
          AlteraK2X0_1.Tag := 230;
          //
          AlteraK2X5_1.Caption := 'K235 - Insumo Consumido no Estabelecimento';
          MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_1, DfSEII_v03_0_2_a.QrK235);
          AlteraK2X5_1.Tag := 235;
        end;
        1: // K250
        begin
          AlteraK2X0_1.Caption := 'K250 - Item Produzido em terceiro';
          MyObjects.HabilitaMenuItemItsUpd(AlteraK2X0_1, DfSEII_v03_0_2_a.QrK250);
          AlteraK2X0_1.Tag := 250;
          //
          AlteraK2X5_1.Caption := 'K255 - Insumo Consumido em terceiro';
          MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_1, DfSEII_v03_0_2_a.QrK255);
          AlteraK2X5_1.Tag := 255;
        end;
        //2: // K260
        3: // K270
        begin
          case PC_K270.ActivePageIndex of
            // 210
            0:
            begin
              AlteraK2X0_1.Caption := 'K270 - K210 Corre��o de item de Desmontagem - Origem';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X0_1, DfSEII_v03_0_2_a.QrK270_210);
              AlteraK2X0_1.Tag := 270210;
              //
              AlteraK2X5_1.Caption := 'K270 - K210 Corre��o de item de Desmontagem - Destino';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_1, DfSEII_v03_0_2_a.QrK275_215);
              AlteraK2X5_1.Tag := 210215;
            end;
            // 220
            1:
            begin
              AlteraK2X0_1.Caption := 'K270 - K220 Corre��o de item reclassificado - Origem';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X0_1, DfSEII_v03_0_2_a.QrK270_220);
              AlteraK2X0_1.Tag := 2702201;
              //
              AlteraK2X5_1.Caption := 'K270 - K220 Corre��o de item reclassificado - Destino';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_1, DfSEII_v03_0_2_a.QrK275_220);
              AlteraK2X5_1.Tag := 2102202;
            end;
            // 230
            2:
            begin
              AlteraK2X0_1.Caption := 'K270 - K230 Produ��o isolada na empresa - Item produzido';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X0_1, DfSEII_v03_0_2_a.QrK270_230);
              AlteraK2X0_1.Tag := 270230;
              //
              AlteraK2X5_1.Caption := 'K270 - K235 Produ��o isolada na empresa - Item consumido';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_1, DfSEII_v03_0_2_a.QrK275_235);
              AlteraK2X5_1.Tag := 210235;
            end;
            // 250
            3:
            begin
              AlteraK2X0_1.Caption := 'K270 - K250 Produ��o isolada em terceiro - Item produzido';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X0_1, DfSEII_v03_0_2_a.QrK270_250);
              AlteraK2X0_1.Tag := 270250;
              //
              AlteraK2X5_1.Caption := 'K270 - K235 Produ��o isolada em terceiro - Item consumido';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_1, DfSEII_v03_0_2_a.QrK275_255);
              AlteraK2X5_1.Tag := 210255;
            end;
            // 260
            4:
            begin
              AlteraK2X0_1.Caption := 'K270 - K260 Reprocessamento - Item reprocessado';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X0_1, DfSEII_v03_0_2_a.QrK270_260);
              AlteraK2X0_1.Tag := 270260;
              //
              AlteraK2X5_1.Caption := 'K270 - K260 Reprocessamento - Item consumido ou retornado';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_1, DfSEII_v03_0_2_a.QrK275_265);
              AlteraK2X5_1.Tag := 210265;
            end;
            //291,292,301,302
            5:
            begin
              AlteraK2X0_1.Caption := 'K270 - K291 Produ��o conjunta na empresa - Item produzido';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X0_1, DfSEII_v03_0_2_a.QrK270_291);
              AlteraK2X0_1.Tag := 270291;
              //
              AlteraK2X5_1.Caption := 'K270 - K292 Produ��o conjunta na empresa - Item consumido';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_1, DfSEII_v03_0_2_a.QrK270_292);
              AlteraK2X5_1.Tag := 270292;
              //
              AlteraK2X5_2.Visible := True;
              AlteraK2X5_2.Caption := 'K270 - K301 - Produ��o conjunta em terceiro � Itens consumido';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_2, DfSEII_v03_0_2_a.QrK270_301);
              AlteraK2X5_2.Tag := 270301;
              //
              AlteraK2X5_3.Visible := True;
              AlteraK2X5_3.Caption := 'K270 - K302 - Produ��o conjunta em terceiro � Itens consumido';
              MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_3, DfSEII_v03_0_2_a.QrK270_302);
              AlteraK2X5_3.Tag := 270302;
            end;
          end;
        end;
        4: // K290
        begin
          AlteraK2X0_1.Caption := 'K290 - Ordem de produ��o';
          MyObjects.HabilitaMenuItemItsUpd(AlteraK2X0_1, DfSEII_v03_0_2_a.QrK290);
          AlteraK2X0_1.Tag := 290;
          //
          AlteraK2X5_1.Caption := 'K291 - Produ��o conjunta � Itens produzidos';
          MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_1, DfSEII_v03_0_2_a.QrK291);
          AlteraK2X5_1.Tag := 291;
          //
          AlteraK2X5_2.Visible := True;
          AlteraK2X5_2.Caption := 'K292 - Produ��o conjunta � Itens consumidos';
          MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_2, DfSEII_v03_0_2_a.QrK292);
          AlteraK2X5_2.Tag := 292;
        end;
        5: // K300
        begin
          AlteraK2X0_1.Caption := 'K300 - Produ��o conjunta - Industrializa��o efetuada por terceiros';
          MyObjects.HabilitaMenuItemItsUpd(AlteraK2X0_1, DfSEII_v03_0_2_a.QrK300);
          AlteraK2X0_1.Tag := 300;
          //
          AlteraK2X5_1.Caption := 'K301 - Produ��o conjunta - Industrializa��o efetuada por terceiros - itens produzidos';
          MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_1, DfSEII_v03_0_2_a.QrK301);
          AlteraK2X5_1.Tag := 301;
          //
          AlteraK2X5_2.Visible := True;
          AlteraK2X5_2.Caption := 'K302 - Produ��o conjunta - Industrializa��o efetuada por terceiros - itens consumidos';
          MyObjects.HabilitaMenuItemItsUpd(AlteraK2X5_2, DfSEII_v03_0_2_a.QrK302);
          AlteraK2X5_2.Tag := 302;
        end;
        //8: // K270_280
      end;
    end;
(*
    4:
    begin
    end;
*)
    else
    begin
      case PCK21X.ActivePageIndex of
        0:
        begin
          AlteraK2X0_1.Caption := '- - -';
          AlteraK2X0_1.Enabled := False;
          AlteraK2X0_1.Tag := 0;
          //
          AlteraK2X5_1.Caption := '- - -';
          AlteraK2X5_1.Enabled := False;
          AlteraK2X5_1.Tag := 0;
        end;
      end;
    end;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PMK280Popup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsDel(ExcluiK280_1, DfSEII_v03_0_2_a.QrK280);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PMPeriodoPopup(Sender: TObject);
begin
  Excluiperiodoselecionado1.Enabled :=
    (DfSEII_v03_0_2_a.QrE001.State <> dsInactive) and
    (DfSEII_v03_0_2_a.QrE001.RecordCount > 0)
  and
    (DfSEII_v03_0_2_a.QrE100.State <> dsInactive) and
    (DfSEII_v03_0_2_a.QrE100.RecordCount = 0);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PMValDecltorioPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (DfSEII_v03_0_2_a.QrE110.State <> dsInactive) and (DfSEII_v03_0_2_a.QrE110.RecordCount = 1);
  Incluinovovalordeclaratrio1.Enabled := Habilita;
  Habilita := Habilita and
    (DfSEII_v03_0_2_a.QrE115.State <> dsInactive) and (DfSEII_v03_0_2_a.QrE115.RecordCount > 0);
  Alteravalordeclaratrioselecionado1.Enabled := Habilita;
  Excluivalordeclaratrioselecionado1.Enabled := Habilita;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ProduoIsolada1Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 3;
  PCKPrd.ActivePageIndex := 0;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.ProduoK230aK2551Click(Sender: TObject);
begin
  DfSEII_v03_0_2_a.ImprimeItensProduzidos();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PME100Popup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (DfSEII_v03_0_2_a.QrE001.State <> dsInactive) and (DfSEII_v03_0_2_a.QrE001.RecordCount > 0);
  Incluinovointervalo1.Enabled := Habilita;
  //
  Habilita := Habilita and
    (DfSEII_v03_0_2_a.QrE100.State <> dsInactive) and (DfSEII_v03_0_2_a.QrE100.RecordCount > 0);
  Alteraintervaloselecionado1.Enabled := Habilita;
  Excluiintervaloselecionado1.Enabled := Habilita and
    (DfSEII_v03_0_2_a.QrE100.State <> dsInactive) and (DfSEII_v03_0_2_a.QrE100.RecordCount > 0)
  and
    (DfSEII_v03_0_2_a.QrE111.State <> dsInactive) and (DfSEII_v03_0_2_a.QrE111.RecordCount = 0)
  and
    (DfSEII_v03_0_2_a.QrE116.State <> dsInactive) and (DfSEII_v03_0_2_a.QrE116.RecordCount = 0);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.PMObrigacoesPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (DfSEII_v03_0_2_a.QrE110.State <> dsInactive) and (DfSEII_v03_0_2_a.QrE110.RecordCount = 1);
  IncluinovaobrigaodoICMSarecolher1.Enabled := Habilita;
  Habilita := Habilita and
    (DfSEII_v03_0_2_a.QrE116.State <> dsInactive) and (DfSEII_v03_0_2_a.QrE116.RecordCount > 0);
  Alteraobrigaoselecionada1.Enabled := Habilita;
  Excluiobrigaoselecionada1.Enabled := Habilita;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.RGEmitenteClick(Sender: TObject);
begin
  DfSEII_v03_0_2_a.ReopenE100(0);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.RGVeriKItsShowClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  DfSEII_v03_0_2_a.FVeriKItsShow := RGVeriKItsShow.ItemIndex;
  if DfSEII_v03_0_2_a.QrK_ConfG.State <> dsInactive then
    GraGruX := DfSEII_v03_0_2_a.QrK_ConfGGraGruX.Value
  else
    GraGruX := 0;
  DfSEII_v03_0_2_a.ReopenK_ConfG(GraGruX);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtSaida0Click(Sender: TObject);
begin
  //VAR_CADASTRO := Qr?.Value;
  Close;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtVerificaClick(Sender: TObject);
begin
  //VerificaBlocoK();
  PCEx00.ActivePageIndex := 4;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.CriaDfSEII();
var
  I: Integer;
begin
  Application.CreateForm(TDfSEII_v03_0_2_a, DfSEII_v03_0_2_a);
  //
  DfSEII_v03_0_2_a.FImporExpor   := 3; //3=Criar! N�o Mexer
  //DfSEII_v03_0_2_a.FEmpresa      := ?;
  //
  DfSEII_v03_0_2_a.FBtE100       := BtE100;
  DfSEII_v03_0_2_a.FBtE110       := BtE110;
  DfSEII_v03_0_2_a.FBtE111       := BtE111;
  DfSEII_v03_0_2_a.FBtE115       := BtE115;
  DfSEII_v03_0_2_a.FBtE116       := BtE116;
  DfSEII_v03_0_2_a.FBtE500       := BtE500;
  DfSEII_v03_0_2_a.FBtE520       := BtE520;
  DfSEII_v03_0_2_a.FBtE530       := BtE530;
  DfSEII_v03_0_2_a.FBtH005       := BtH005;
  DfSEII_v03_0_2_a.FBtH010       := BtH010;
  DfSEII_v03_0_2_a.FBtH020       := BtH020;
  DfSEII_v03_0_2_a.FBtK100       := BtK100;
  DfSEII_v03_0_2_a.FBtK200       := BtK200;
  DfSEII_v03_0_2_a.FBtK220       := BtK220;
  DfSEII_v03_0_2_a.FBtK230       := BtK230;
  DfSEII_v03_0_2_a.FBtK280       := BtK280;
  DfSEII_v03_0_2_a.FBt1010       := Bt1010;
  DfSEII_v03_0_2_a.FBtVerifica   := BtVerifica;
  DfSEII_v03_0_2_a.FBtDifGera    := BtDifGera;
  DfSEII_v03_0_2_a.FBtDifImprime := BtDifImprime;
  //
  DBGCab.DataSource  := DfSEII_v03_0_2_a.DsE001;
  DBGE100.DataSource := DfSEII_v03_0_2_a.DsE100;

  DBEdit2.DataSource := DfSEII_v03_0_2_a.DsE110;
  DBEdit3.DataSource := DfSEII_v03_0_2_a.DsE110;
  DBEdit4.DataSource := DfSEII_v03_0_2_a.DsE110;
  DBEdit5.DataSource := DfSEII_v03_0_2_a.DsE110;
  DBEdit6.DataSource := DfSEII_v03_0_2_a.DsE110;
  DBEdit7.DataSource := DfSEII_v03_0_2_a.DsE110;
  DBEdit8.DataSource := DfSEII_v03_0_2_a.DsE110;
  DBEdit9.DataSource := DfSEII_v03_0_2_a.DsE110;
  DBEdit10.DataSource := DfSEII_v03_0_2_a.DsE110;
  DBEdit11.DataSource := DfSEII_v03_0_2_a.DsE110;
  DBEdit12.DataSource := DfSEII_v03_0_2_a.DsE110;
  DBEdit13.DataSource := DfSEII_v03_0_2_a.DsE110;
  DBEdit14.DataSource := DfSEII_v03_0_2_a.DsE110;
  DBEdit15.DataSource := DfSEII_v03_0_2_a.DsE110;

  DBGE111.DataSource := DfSEII_v03_0_2_a.DsE111;
  DBGE112.DataSource := DfSEII_v03_0_2_a.DsE112;
  DBGE113.DataSource := DfSEII_v03_0_2_a.DsE113;
  DBGE115.DataSource := DfSEII_v03_0_2_a.DsE115;
  DBGE116.DataSource := DfSEII_v03_0_2_a.DsE116;

  DBMemo1.DataSource := DfSEII_v03_0_2_a.DsE116;
  DBMemo2.DataSource := DfSEII_v03_0_2_a.DsE116;

  DBGE500.DataSource := DfSEII_v03_0_2_a.DsE500;
  DBGE510.DataSource := DfSEII_v03_0_2_a.DsE510;
  DBGE520.DataSource := DfSEII_v03_0_2_a.DsE520;
  DBGE530.DataSource := DfSEII_v03_0_2_a.DsE530;
  DBGH005.DataSource := DfSEII_v03_0_2_a.DsH005;
  DBGH010.DataSource := DfSEII_v03_0_2_a.DsH010;
  DBGH020.DataSource := DfSEII_v03_0_2_a.DsH020;
  DBGK100.DataSource := DfSEII_v03_0_2_a.DsK100;
  DBGK200.DataSource := DfSEII_v03_0_2_a.DsK200;
  DBGK220.DataSource := DfSEII_v03_0_2_a.DsK220;
  DBGK210.DataSource := DfSEII_v03_0_2_a.DsK210;
  DBGK215.DataSource := DfSEII_v03_0_2_a.DsK215;
  DBGK230.DataSource := DfSEII_v03_0_2_a.DsK230;
  DBGK235.DataSource := DfSEII_v03_0_2_a.DsK235;
  DBGK250.DataSource := DfSEII_v03_0_2_a.DsK250;
  DBGK255.DataSource := DfSEII_v03_0_2_a.DsK255;
  DBGK260.DataSource := DfSEII_v03_0_2_a.DsK260;
  DBGK265.DataSource := DfSEII_v03_0_2_a.DsK265;
  DBGK290.DataSource := DfSEII_v03_0_2_a.DsK290;
  DBGK291.DataSource := DfSEII_v03_0_2_a.DsK291;
  DBGK292.DataSource := DfSEII_v03_0_2_a.DsK292;
  DBGK300.DataSource := DfSEII_v03_0_2_a.DsK300;
  DBGK301.DataSource := DfSEII_v03_0_2_a.DsK301;
  DBGK302.DataSource := DfSEII_v03_0_2_a.DsK302;
  DBGK270_220.DataSource := DfSEII_v03_0_2_a.DsK270_220;
  DBGK275_220.DataSource := DfSEII_v03_0_2_a.DsK275_220;
  DBGK270_210.DataSource := DfSEII_v03_0_2_a.DsK270_210;
  DBGK275_215.DataSource := DfSEII_v03_0_2_a.DsK275_215;
  DBGK270_230.DataSource := DfSEII_v03_0_2_a.DsK270_230;
  DBGK275_235.DataSource := DfSEII_v03_0_2_a.DsK275_235;
  DBGK270_250.DataSource := DfSEII_v03_0_2_a.DsK270_250;
  DBGK275_255.DataSource := DfSEII_v03_0_2_a.DsK275_255;
  DBGK270_260.DataSource := DfSEII_v03_0_2_a.DsK270_260;
  DBGK275_265.DataSource := DfSEII_v03_0_2_a.DsK275_265;
  DBGK270_291.DataSource := DfSEII_v03_0_2_a.DsK270_291;
  DBGK270_292.DataSource := DfSEII_v03_0_2_a.DsK270_292;
  DBGK270_301.DataSource := DfSEII_v03_0_2_a.DsK270_301;
  DBGK270_302.DataSource := DfSEII_v03_0_2_a.DsK270_302;
  //
  DBGK280.DataSource := DfSEII_v03_0_2_a.DsK280;
  //
  CkIND_EXP.DataSource   := DfSEII_v03_0_2_a.Ds1010;
  CkIND_CCRF.DataSource  := DfSEII_v03_0_2_a.Ds1010;
  CkIND_COMB.DataSource  := DfSEII_v03_0_2_a.Ds1010;
  CkIND_USINA.DataSource := DfSEII_v03_0_2_a.Ds1010;
  CkIND_VA.DataSource    := DfSEII_v03_0_2_a.Ds1010;
  CkIND_EE.DataSource    := DfSEII_v03_0_2_a.Ds1010;
  CkIND_CART.DataSource  := DfSEII_v03_0_2_a.Ds1010;
  CkIND_FORM.DataSource  := DfSEII_v03_0_2_a.Ds1010;
  CkIND_AER.DataSource   := DfSEII_v03_0_2_a.Ds1010;
  //
  DBGK_ConfG.DataSource := DfSEII_v03_0_2_a.DsK_ConfG;
  DBGPsq01.DataSource := DfSEII_v03_0_2_a.DsPsq01;
  DBGPsq02.DataSource := DfSEII_v03_0_2_a.DsPsq02;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.DBGK_ConfGCellClick(Column: TColumn);
var
  GraGruX, Insumo, AnoMesPsq: Integer;
  DiaIni, DiaFim: TDateTime;
  SQL_Periodo: String;
  ATT_MovimID: String;
begin
(*&�%
  FColumnSel := '';
  DfSEII_v03_0_2_a.QrPsq01.Close;
  DBGPsq01.Visible := True;
  DBGPsq02.Visible := False;
  //
  GraGruX := DfSEII_v03_0_2_a.QrK_ConfGGraGruX.Value;
  AnoMesPsq := DfSEII_v03_0_2_a.QrK100AnoMes.Value;
  if (Column.FieldName = 'Sdo_Ini') or (Column.FieldName = 'Final') then
  begin
    if (Column.FieldName = 'Sdo_Ini') then
      AnoMesPsq := dmkPF.IncrementaAnoMes(AnoMesPsq, -1);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DfSEII_v03_0_2_a.QrPsq01, Dmod.MyDB, [
    'SELECT * ',
    'FROM efdicmsipik200 ',
    'WHERE ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND AnoMes=' + Geral.FF0(AnoMesPsq),
    'AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
  end
  else if Column.FieldName = 'Compra' then
  begin
    DiaIni      := Geral.AnoMesToData(DfSEII_v03_0_2_a.QrE001AnoMes.Value, 1);
    DiaFim      := IncMonth(DiaIni, 1) -1;
    SQL_Periodo := dmkPF.SQL_Periodo('WHERE vmi.DataHora ', DiaIni, DiaFim, True, True);
    UnDmkDAC_PF.AbreMySQLQuery0(DfSEII_v03_0_2_a.QrPsq01, Dmod.MyDB, [
    'SELECT vmi.Controle, vmi.MovimID, vmi.Codigo, vmi.MovimCod,  ',
    'vmi.DataHora, vmi.Pecas, vmi.AreaM2, vmi.PesoKg, vmi.GraGruX  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX  ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
    SQL_Periodo,
    'AND vmi.MovimID IN (1,16,21,22) ',
    'AND vmi.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.FEmpresa),
    'AND vmi.GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //
    SQL_Periodo := dmkPF.SQL_Periodo('WHERE pqx.DataX ', DiaIni, DiaFim, True, True);
{
    UnDmkDAC_PF.AbreMySQLQuery0(DfSEII_v03_0_2_a.QrPsq02, Dmod.MyDB, [
    'SELECT pqx.Insumo, SUM(pqx.Peso) Peso ',
    'FROM pqx ',
    'LEFT JOIN gragrux ggx ON ggx.GraGru1=pqx.Insumo',
    SQL_Periodo,
    'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
    'AND ggx.GraGru1=' + Geral.FF0(GraGruX),
    '']);
}
    UnDmkDAC_PF.AbreMySQLQuery0(DfSEII_v03_0_2_a.QrPsq02, Dmod.MyDB, [
    'SELECT pqx.Insumo, SUM(pqx.Peso) Peso  ',
    'FROM gragrux ggx  ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN pqx ON pqx.Insumo=gg1.Nivel1 ',
    SQL_Periodo,
    'AND pqx.Tipo=' + Geral.FF0(VAR_FATID_0010),
    'AND ggx.Controle=' + Geral.FF0(GraGruX),
    '']);
    //
    DBGPsq02.Visible := DfSEII_v03_0_2_a.QrPsq02.RecordCount > 0;
    DBGPsq01.Visible := (DfSEII_v03_0_2_a.QrPsq01.RecordCount > 0) or (DBGPsq02.Visible = False);
  end
  else if Column.FieldName = 'Producao' then
  begin
    FColumnSel := Column.FieldName;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DfSEII_v03_0_2_a.QrPsq01, Dmod.MyDB, [
    'SELECT KndCod, KndAID, KndNSU, ',
    'DT_FIN_OP DATA_FIM, QTD_ENC QTDE,',
    'GraGruX',
    'FROM efdicmsipik230',
    'WHERE ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '',
    'UNION',
    '',
    'SELECT KndCod, KndAID, KndNSU, ',
    'DT_PROD DATA_FIM, QTD QTDE,',
    'GraGruX  ',
    'FROM efdicmsipik250',
    'WHERE ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
  end else
  if (Column.FieldName = 'EntrouClasse')
  or (Column.FieldName = 'SaiuClasse') then
  begin
    FColumnSel := Column.FieldName;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DfSEII_v03_0_2_a.QrPsq01, Dmod.MyDB, [
    'SELECT * FROM efdicmsipik220 ',
    'WHERE ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND PeriApu=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100PeriApu.Value),
    'AND ( ',
    '  GGXDst=' + Geral.FF0(GraGruX),
    '  OR GGXOri=' + Geral.FF0(GraGruX),
    ')',
    '']);
  end else
  if Column.FieldName = 'Consumo' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DfSEII_v03_0_2_a.QrPsq01, Dmod.MyDB, [
    'SELECT ekc.ImporExpor, ekc.AnoMes, ekc.Empresa,  ',
    'ekc.IDSeq2, "235" REG, ekc.IDSeq1 K2X0, ekc.DT_SAIDA DATA,  ',
    'ekc.COD_ITEM, ekc.QTD, ekc.COD_INS_SUBST,  ',
    'ekc.ID_SEK, ekc.KndItm, ekc.KndAID, ',
    'ekc.KndCod, ekc.KndNSU, ekc.KndItm, ',
    'ekc.GraGruX, ekc.ESTSTabSorc  ',
    'FROM efdicmsipik235 ekc ',
    'WHERE ekc.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND ekc.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND ekc.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND PeriApu=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100PeriApu.Value),
    'AND ekc.GraGruX=' + Geral.FF0(GraGruX),
    ' ',
    'UNION ',
    ' ',
    'SELECT ekc.ImporExpor, ekc.AnoMes, ekc.Empresa,  ',
    'ekc.IDSeq2, "255" REG, ekc.IDSeq1 K2X0, ekc.DT_CONS DATA,  ',
    'ekc.COD_ITEM, ekc.QTD, ekc.COD_INS_SUBST,  ',
    'ekc.ID_SEK, ekc.KndItm, ekc.KndAID, ',
    'ekc.KndCod, ekc.KndNSU, ekc.KndItm, ',
    'ekc.GraGruX, ekc.ESTSTabSorc  ',
    'FROM efdicmsipik255 ekc ',
    'WHERE ekc.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND ekc.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND ekc.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND PeriApu=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100PeriApu.Value),
    'AND ekc.GraGruX=' + Geral.FF0(GraGruX),
    '']);
  end else
  //else if Column.FieldName = 'SaiuClasse' then  Acima!!!!!!!!!!!!!!!!!!
  //else
  if Column.FieldName = 'Venda' then
  begin
    FColumnSel := Column.FieldName;
    //
    DiaIni      := Geral.AnoMesToData(DfSEII_v03_0_2_a.QrE001AnoMes.Value, 1);
    DiaFim      := IncMonth(DiaIni, 1) -1;
    SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataHora ', DiaIni, DiaFim, True, True);
    UnDmkDAC_PF.AbreMySQLQuery0(DfSEII_v03_0_2_a.QrPsq01, Dmod.MyDB, [
    'SELECT Controle, MovimID, Codigo, MovimCod,',
    'DataHora, Pecas, AreaM2, PesoKg, SrcMovID, SrcNivel1,',
    'SrcNivel2, SrcGGX   ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    SQL_Periodo,
    'AND MovimID=2',
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
  end
  else if Column.FieldName = 'Indevido' then
  begin
    DiaIni      := Geral.AnoMesToData(DfSEII_v03_0_2_a.QrE001AnoMes.Value, 1);
    DiaFim      := IncMonth(DiaIni, 1) -1;
    SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataHora ', DiaIni, DiaFim, True, True);
        UnDmkDAC_PF.AbreMySQLQuery0(DfSEII_v03_0_2_a.QrPsq01, Dmod.MyDB, [
        'SELECT vmi.Codigo, vmi.GraGruX, vmi.MovimID, vmi.MovimCod, ',
        'vmi.Controle, SUM(vmi.Pecas) Pecas, SUM(vmi.AreaM2) AreaM2, ',
        'SUM(PesoKg) PesoKg, med.Grandeza ',
        'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
        'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
        'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
        SQL_Periodo,
        'AND vmi.MovimID IN (0,3,4,5,9,10,12,13,17,18)',
        'AND vmi.GraGruX=' + Geral.FF0(GraGruX),
        'AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.FEmpresa),
        'GROUP BY GraGruX',
        'ORDER BY GraGruX ',
        '']);
        DBGPsq01.Visible := DfSEII_v03_0_2_a.QrPsq01.RecordCount > 0;
        DiaIni      := Geral.AnoMesToData(DfSEII_v03_0_2_a.QrE001AnoMes.Value, 1);
        DiaFim      := IncMonth(DiaIni, 1) -1;
        SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataX ', DiaIni, DiaFim, True, True);
        //
        Insumo      := DmMod.ObtemInsumoDeGraGruX(GraGruX);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(DfSEII_v03_0_2_a.QrPsq02, Dmod.MyDB, [
        'DROP TABLE IF EXISTS _SPED_EFD_K2XX_SEM_MC;',
        'CREATE TABLE _SPED_EFD_K2XX_SEM_MC',
        ' ',
        'SELECT * ',
        'FROM ' + TMeuDB + '.pqx ',
        SQL_Periodo,
        'AND Tipo=' + Geral.FF0(VAR_FATID_0110),
        'AND Insumo=' + Geral.FF0(Insumo),
        'AND OrigemCodi IN (',
        '  SELECT Codigo',
        '  FROM ' + TMeuDB + '.emit',
        '  WHERE VSMovCod=0)',
        ' ',
        'UNION',
        '',
        'SELECT * ',
        'FROM ' + TMeuDB + '.pqx ',
        SQL_Periodo,
        'AND Tipo=' + Geral.FF0(VAR_FATID_0190),
        'AND Insumo=' + Geral.FF0(Insumo),
        'AND OrigemCodi IN (',
        '  SELECT Codigo',
        '  FROM ' + TMeuDB + '.pqo',
        '  WHERE VSMovCod=0)',
        ';',
        '',
        'SELECT * ',
        'FROM _SPED_EFD_K2XX_SEM_MC',
        'ORDER BY Insumo;',
        '']);
        DBGPsq02.Visible := DfSEII_v03_0_2_a.QrPsq02.RecordCount > 0;
  end
%�&*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.DBGK200DblClick(Sender: TObject);
begin
  JanelaPorDblClick(TDBGrid(DBGK200));
(*
var
  IMEI: Integer;
begin
  if DBGK200.SelectedField.FieldName = 'KndItm' then
  begin
    IMEI := DBGK200.SelectedField.AsInteger;
    if IMEI <> 0 then
      VS_PF.MostraFormVSMovIts(IMEI);
  end;
*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.DBGPsq02DblClick(Sender: TObject);
begin
  if (FColumnSel = 'Producao')
  or (FColumnSel = 'EntrouClasse')
  or (FColumnSel = 'SaiuClasse')
  or (FColumnSel = 'Venda') then
  begin
    App_Jan.MostroFormMovimCod(DfSEII_v03_0_2_a.QrPsq01.FieldByName('KndNSU').AsInteger);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Desmontagemdemercadoria1Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 1;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.AlteraajustedaapuracaodoIPIselecionado1Click(
  Sender: TObject);
begin
  InsUpdE530(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Alteraajusteselecionado1Click(Sender: TObject);
begin
  InsUpdE111(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Alteraidentificaoselecionada1Click(Sender: TObject);
begin
  InsUpdE113(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Alterainformaoadicionalselecionada1Click(Sender: TObject);
begin
  InsUpdE112(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Alterainformaocomplementar1Click(Sender: TObject);
begin
  InsUpdH020(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.AlteraintervalodeProduoeEstoque1Click(Sender: TObject);
begin
  InsUpdK100(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.AlteraintervaloIPIselecionado1Click(Sender: TObject);
begin
  InsUpdE500(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Alteraintervaloselecionado1Click(Sender: TObject);
begin
  InsUpdE100(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.AlteraInventrio1Click(Sender: TObject);
begin
  InsUpdH005(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Alteraitem1Click(Sender: TObject);
begin
  InsUpdH010(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.AlteraItemGenerico(Sender: TObject);
  function Tag270(): Integer;
  begin
    Result := TMenuItem(Sender).Tag - 270000;
  end;
begin
  case TMenuItem(Sender).Tag of
    210: InsUpdK210(stUpd);
    215: InsUpdK215(stUpd);

    230: InsUpdK230(stUpd);
    235: InsUpdK235(stUpd);

    250: InsUpdK250(stUpd);
    255: InsUpdK255(stUpd);

    260: InsUpdK260(stUpd);
    265: InsUpdK265(stUpd);

    290: InsUpdK290(stUpd);
    291: InsUpdK291(stUpd);
    292: InsUpdK292(stUpd);

    300: InsUpdK300(stUpd);
    301: InsUpdK301(stUpd);
    302: InsUpdK302(stUpd);

    270210,
    270230,
    270250,
    270260,
    270291,
    270292,
    270301,
    270302,
    2702201: InsUpdK270(Tag270(), stUpd);
    //275: InsUpdK275(stUpd);
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.AlteraK200_1Click(Sender: TObject);
begin
  InsUpdK200(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.AlteraK220_1Click(Sender: TObject);
begin
  InsUpdK220(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.AlteraK280_1Click(Sender: TObject);
begin
  InsUpdK280(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.AlteraK2X0_1Click(Sender: TObject);
begin
  AlteraItemGenerico(Sender);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Alteraobrigaoselecionada1Click(Sender: TObject);
begin
  InsUpdE116(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Alteravalordeclaratrioselecionado1Click(Sender: TObject);
begin
  InsUpdE115(stUpd);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.AtualizaValoresE520deE530();
var
  Qry: TmySQLQuery;
  VL_SD_ANT_IPI, VL_DEB_IPI, VL_CRED_IPI, VL_OD_IPI, VL_OC_IPI, VL_SC_IPI,
  VL_SD_IPI, Valor, Saldo: Double;
  IND_AJ: String;
  ImporExpor, AnoMes, Empresa, LinArq, E500: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    VL_OD_IPI := 0;
    VL_OC_IPI := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT IND_AJ, SUM(VL_AJ) VL_AJ  ',
    'FROM efdicmsipie530 ',
    'WHERE ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.FImporExpor),
    'AND AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrE520AnoMes.Value),
    'AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrE520Empresa.Value),
    'AND E520=' + Geral.FF0(DfSEII_v03_0_2_a.QrE520LinArq.Value),
    '']);
    while not Qry.Eof do
    begin
      Valor  := Qry.FieldByName('VL_AJ').AsFloat;
      IND_AJ := Qry.FieldByName('IND_AJ').AsString;
      if IND_AJ = '0' then
        VL_OD_IPI := VL_OD_IPI + Valor
      else
      if IND_AJ = '1' then
        VL_OC_IPI := VL_OC_IPI + Valor
      else
        Geral.MB_Erro('Indicador inv�lido em "AtualizaValoresE520deE530":' +
        slineBreak + '"' + IND_AJ + '" ');
      //
      Qry.Next;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT VL_SD_ANT_IPI, VL_DEB_IPI, VL_CRED_IPI  ',
    'FROM efdicmsipie520 ',
    'WHERE ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.FImporExpor),
    'AND AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrE520AnoMes.Value),
    'AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrE520Empresa.Value),
    'AND LinArq=' + Geral.FF0(DfSEII_v03_0_2_a.QrE520LinArq.Value),
    'AND E500=' + Geral.FF0(DfSEII_v03_0_2_a.QrE520E500.Value),
    '']);
    VL_SD_ANT_IPI := Qry.FieldByName('VL_SD_ANT_IPI').AsFloat;
    VL_DEB_IPI    := Qry.FieldByName('VL_DEB_IPI').AsFloat;
    VL_CRED_IPI   := Qry.FieldByName('VL_CRED_IPI').AsFloat;
    //
//Campo 07 (VL_SC_IPI) - Valida��o: se a soma dos campos VL_DEB_IPI e VL_OD_IPI menos a soma dos campos
//VL_SD_ANT_IPI, VL_CRED_IPI e VL_OC_IPI for menor que �0� (zero), ent�o o campo VL_SC_IPI deve ser igual ao
//valor absoluto da express�o, e o valor do campo VL_SD_IPI deve ser igual a �0� (zero).
    VL_SC_IPI     := 0;
    VL_SD_IPI     := 0;
    Saldo :=  (VL_DEB_IPI + VL_OD_IPI) - (VL_SD_ANT_IPI + VL_CRED_IPI + VL_OC_IPI);
    if Saldo < 0 then
  (*07*)VL_SC_IPI      := - Saldo
//Campo 08 (VL_SD_IPI) - Valida��o: se a soma dos campos VL_DEB_IPI e VL_OD_IPI menos a soma dos campos
//VL_SD_ANT_IPI, VL_CRED_IPI e VL_OC_IPI for maior ou igual a �0� (zero), ent�o o campo 08 (VL_SD_IPI) deve ser
//igual ao resultado da express�o, e o valor do campo VL_SC_IPI deve ser igual a �0� (zero).
   else
  (*08*)VL_SD_IPI      := Saldo;

    //
    ImporExpor         := DfSEII_v03_0_2_a.FImporExpor;
    AnoMes             := DfSEII_v03_0_2_a.QrE520AnoMes.Value;
    Empresa            := DfSEII_v03_0_2_a.QrE520Empresa.Value;
    LinArq             := DfSEII_v03_0_2_a.QrE520LinArq.Value;
    E500               := DfSEII_v03_0_2_a.QrE520E500.Value;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'efdicmsipie520', False, [
    'VL_OD_IPI', 'VL_OC_IPI',
    'VL_SC_IPI', 'VL_SD_IPI'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'E500'], [
    VL_OD_IPI, VL_OC_IPI,
    VL_SC_IPI, VL_SD_IPI], [
    ImporExpor, AnoMes, Empresa, LinArq, E500], True);
  finally
    Qry.Free;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.AtualizaValoresH005deH010();
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE efdicmsipih005 ',
  'SET VL_INV=( ',
  '  SELECT SUM(VL_ITEM) ',
  '  FROM efdicmsipih010 ',
  '  WHERE ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrE001ImporExpor.Value),
  '  AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrE001Empresa.Value),
  '  AND AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrE001AnoMes.Value),
  '  AND H005=' + Geral.FF0(DfSEII_v03_0_2_a.QrH005LinArq.Value),
  '  ) ',
  '  WHERE ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrE001ImporExpor.Value),
  '  AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrE001Empresa.Value),
  '  AND AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrE001AnoMes.Value),
  '  AND LinArq=' + Geral.FF0(DfSEII_v03_0_2_a.QrH005LinArq.Value),
  '']);
  //
  DfSEII_v03_0_2_a.ReopenH005(DfSEII_v03_0_2_a.QrH005LinArq.Value);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BaixaeestoquedeInNatura1Click(
  Sender: TObject);
begin
(*&�%
  AppPF.MostraFormCfgEstqInNatEFD(
    DfSEII_v03_0_2_a.QrE001ImporExpor.Value,
    DfSEII_v03_0_2_a.QrE001AnoMes.Value,
    DfSEII_v03_0_2_a.QrE001Empresa.Value,
    0);
%�&*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtConferenciasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConferencias, BtConferencias);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BlocoKRegistrosK2001Click(Sender: TObject);
begin
  DfSEII_v03_0_2_a.ImprimeK200_K280();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BlocoKRegistrosK2201Click(Sender: TObject);
begin
  DfSEII_v03_0_2_a.ImprimeK220();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Bt1010Click(Sender: TObject);
begin
  MostraFormEFD_1010();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtK280Click(Sender: TObject);
begin
  //if not PCK200.ActivePageIndex = 4 then
    PCK200.ActivePageIndex := 4;
  MyObjects.MostraPopUpDeBotao(PMK280, BtK280);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtLocVMIClick(Sender: TObject);
begin
  LocalizarVMI();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtDifGeraClick(Sender: TObject);
begin
  //VerificaBlocoK();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtDifImprimeClick(Sender: TObject);
begin
  DfSEII_v03_0_2_a.ImprimeVerificacaoBlocoK();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtE100Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PME100, BtE100);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtE110Click(Sender: TObject);
begin
  MostraFormEFD_E110();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtE111Click(Sender: TObject);
begin
  PCE100.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PME111, BtE111);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtE115Click(Sender: TObject);
begin
  PCE100.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMValDecltorio, BtE115);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtE116Click(Sender: TObject);
begin
  PCE100.ActivePageIndex := 3;
  MyObjects.MostraPopUpDeBotao(PMObrigacoes, BtE116);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtE500Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PME500, BtE500);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtE520Click(Sender: TObject);
begin
  MostraFormEFD_E520();
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtE530Click(Sender: TObject);
begin
  PCE500.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PME530, BtE530);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtH005Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMH005, BtH005);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtH010Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMH010, BtH010);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtH020Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMH020, BtH020);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtImprime1Click(Sender: TObject);
begin
{
  if (PageControl3.ActivePageIndex = 1) then
    MyObjects.frxMostra(frxSPEDEFD_PRINT_001_02, 'Falhas na Exporta��o')
  else
    Geral.MensagemBox('A guia selecionada n�o possui relat�rio!',
    'Informa��o', MB_OK+MB_ICONINFORMATION);
}
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtK100Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMK100, BtK100);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtK200Click(Sender: TObject);
begin
  if not (PCK200.ActivePageIndex in ([0,4])) then
    PCK200.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMK200, BtK200);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtK220Click(Sender: TObject);
begin
  PCK200.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMK220, BtK220);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtK230Click(Sender: TObject);
begin
  //PCK200.ActivePageIndex := 3;
  MyObjects.MostraPopUpDeBotao(PMK230, BtK230);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.BtPeriodoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPeriodo, BtPeriodo);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.FormCreate(Sender: TObject);
begin
  if VAR_USUARIO = -1 then
    Geral.MB_Info('VSxPesagemPQ1Click() de Dez/2018 - W.L.');
  CriaDfSEII();
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FEmprTXT := '';
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  PCE100.ActivePageIndex := 0;
  PCEx00.ActivePageIndex := 0;
  PCK200.ActivePageIndex := 0;
  ImgTipo.SQLType := stLok;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.SbNumeroClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Codigo(QrCadComItensCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrCadComItensCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
  //
  DfSEII_v03_0_2_a.Destroy;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //Geral.MB_Aviso('Raspa de divisao nao pode aparecer na reclasse de Ope/pwe!');
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.SbQueryClick(Sender: TObject);
begin
{
  LocCod(QrCadComItensCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cadcomitens', Dmod.MyDB, CO_VAZIO));
}
end;

{
procedure TFmEfdIcmsIpiE001_v03_0_2_a.VerificaBlocoK();
var
  Qry: TmySQLQuery;
  SQL_PeriodoVS, SQL_PeriodoPQ: String;
  Campo, ImporExpor, AnoMes, Empresa, GraGruX, REGISTRO, MovimID: Integer;
  DiaIni, DiaFim: TDateTime;
  ESTSTabSorc: TEstqSPEDTabSorc;
  //
  procedure InsereAtualVerif(GGX: Integer; QTD: Double);
  begin
    //GraGruX := Qry.FieldByName('GraGruX').AsInteger;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'efdicmsipik_confg', False, [
    'Movim' + FormatFloat('00', Campo)], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'GraGruX', 'REGISTRO', 'MovimID',
    'MovimXX', 'ESTSTabSorc'], [
    QTD], [
    ImporExpor, AnoMes, Empresa,
    GGX, REGISTRO, MovimID,
    Campo, Integer(ESTSTabSorc)], True);
  end;
  //
var
  QTD: Double;
  //
  procedure InsereEstoqueDoInicioDoPeriodo();
  var
    MesAnt: Integer;
  begin
    MesAnt := Geral.IncrementaMes_AnoMes(DfSEII_v03_0_2_a.QrK100AnoMes.Value, -1);
    //
    Campo := 1;
    Registro := 200;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k200.GraGruX, SUM(k200.QTD) QTD ',
    'FROM efdicmsipik200 k200 ',
    'WHERE k200.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k200.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k200.AnoMes=' + Geral.FF0(MesAnt),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSEntradaPorCompra();
  const
    sProcName = 'InsereVSEntradaPorCompra()';
  var
    Grandeza: Integer;
  begin
    Campo := 2;
    Registro := 0;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.GraGruX, SUM(vmi.Pecas) Pecas,',
    'SUM(vmi.AreaM2) AreaM2, SUM(PesoKg) PesoKg, ',
    'med.Grandeza  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
    SQL_PeriodoVS,
    'AND vmi.MovimID IN (1,16,21,22)',
    'AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.FEmpresa),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      Grandeza := Qry.FieldByName('Grandeza').AsInteger;
      GraGruX  := Qry.FieldByName('GraGruX').AsInteger;
      //
      case Grandeza of
        0: QTD := Qry.FieldByName('Pecas').AsFloat;
        1: QTD := Qry.FieldByName('AreaM2').AsFloat;
        2: QTD := Qry.FieldByName('PesoKg').AsFloat;
        else
          Geral.MB_Erro('"Grandeza" = ' + Geral.FF0(Grandeza) + ' indefinido em '
          + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      end;
      if QTD <= 0 then
        Geral.MB_Erro('"QTD" = ' + Geral.FFT(QTD, 2, siNegativo) + ' inv�lido em '
        + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InserePQEntradaPorCompra();
  const
    sProcName = 'InserePQEntradaPorCompra()';
  var
    GGX: Integer;
  begin
    Campo := 2;
    Registro := 0;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsPQx;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Insumo, SUM(Peso) Peso ',
    'FROM pqx ',
    SQL_PeriodoPQ,
    'AND Tipo=' + Geral.FF0(VAR_FATID_0010),
    'GROUP BY Insumo ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD := Qry.FieldByName('Peso').AsFloat;
      if QTD <= 0 then
        Geral.MB_Erro('"QTD" = ' + Geral.FFT(QTD, 2, siNegativo) + ' inv�lido em '
        + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak);
      GGX := UnPQx.ObtemGGXdeInsumo(Qry.FieldByName('Insumo').AsInteger);
      InsereAtualVerif(GGX, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSEntradaPorProducao();
  begin
    Campo := 3;
    Registro := 230;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k230.GraGruX, SUM(k230.QTD_ENC) QTD',
    'FROM efdicmsipik230 k230',
    'WHERE k230.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k230.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k230.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 3;
    Registro := 250;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k250.GraGruX, SUM(k250.QTD) QTD',
    'FROM efdicmsipik250 k250',
    'WHERE k250.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k250.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k250.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 3;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_POS) QTD ',
    'FROM efdicmsipik270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND QTD_COR_POS>0 ',
    'AND RegisPai IN ("K230", "K235", "K250", "K255", "K260", "K265") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 3;
    Registro := 275;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k275.GraGruX, SUM(k275.QTD_COR_POS) QTD ',
    'FROM efdicmsipik275 k275 ',
    'WHERE k275.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k275.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k275.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND QTD_COR_POS>0 ',
    'AND RegisPai IN ("K230", "K235", "K250", "K255", "K260", "K265") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //
  procedure InsereVSEntradaPorProducaoCompartilhada();
  begin
    Campo := 5;
    Registro := 291;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k291.GraGruX, SUM(k291.QTD) QTD',
    'FROM efdicmsipik291 k291',
    'WHERE k291.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k291.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k291.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 5;
    Registro := 301;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k301.GraGruX, SUM(k301.QTD) QTD',
    'FROM efdicmsipik301 k301',
    'WHERE k301.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k301.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k301.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 5;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_POS) QTD ',  // � ????
    'FROM efdicmsipik270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    //'AND QTD_COR_POS>0 ',  ????
    'AND RegisPai IN ("K291", "K301") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //
  procedure InsereVSProdutoDeReforma();
  begin
    Campo := 7;
    Registro := 260;
    MovimID := 33;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k260.GraGruX, ',
    'SUM(k260.QTD_RET-k260.QTD_SAIDA) QTD',
    'FROM efdicmsipik260 k260',
    'WHERE k260.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k260.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k260.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSInsumoNaReforma();
  begin
    Campo := 27;
    Registro := 265;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsPQx;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k265.GraGruX,',
    'SUM(k265.QTD_CONS-k265.QTD_RET) QTD',
    'FROM efdicmsipik265 k265',
    'WHERE k265.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k265.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k265.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSEntradaPorDesmonte();
  begin
    Campo := 6;
    Registro := 215;
    MovimID := 11;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k215.GraGruX, SUM(k215.QTD_DES) QTD',
    'FROM efdicmsipik215 k215',
    'WHERE k215.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k215.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k215.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 6;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_POS) QTD ',
    'FROM efdicmsipik270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND QTD_COR_POS>0 ',
    'AND RegisPai IN ("K210", "K215") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 6;
    Registro := 275;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k275.GraGruX, SUM(k275.QTD_COR_POS) QTD ',
    'FROM efdicmsipik275 k275 ',
    'WHERE k275.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k275.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k275.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND QTD_COR_POS>0 ',
    'AND RegisPai IN ("K210", "K215") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //
  procedure InsereVSEntradaPorClasse();
  begin
    Campo := 4;
    Registro := 220;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k220.GGXDst GraGruX, SUM(k220.QTD) QTD',
    'FROM efdicmsipik220 k220',
    'WHERE k220.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k220.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k220.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 4;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_POS) QTD ',
    'FROM efdicmsipik270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND QTD_COR_POS>0 ',
    'AND RegisPai IN ("K220") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 4;
    Registro := 275;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k275.GraGruX, SUM(k275.QTD_COR_POS) QTD ',
    'FROM efdicmsipik275 k275 ',
    'WHERE k275.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k275.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k275.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND QTD_COR_POS>0 ',
    'AND RegisPai IN ("K220") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //
  procedure InsereVSSaidaParaProduzir();
  begin
    Campo := 23;
    Registro := 235;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k235.GraGruX, SUM(k235.QTD) QTD ',
    'FROM efdicmsipik235 k235 ',
    'WHERE k235.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k235.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k235.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //


    Campo := 23;
    Registro := 255;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k255.GraGruX, SUM(k255.QTD) QTD ',
    'FROM efdicmsipik255 k255 ',
    'WHERE k255.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k255.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k255.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 23;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_NEG) QTD ',
    'FROM efdicmsipik270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND QTD_COR_NEG>0 ',
    'AND RegisPai IN ("K230", "K235", "K250", "K255", "K260", "K265") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 23;
    Registro := 275;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k275.GraGruX, SUM(k275.QTD_COR_NEG) QTD ',
    'FROM efdicmsipik275 k275 ',
    'WHERE k275.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k275.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k275.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND QTD_COR_NEG>0 ',
    'AND RegisPai IN ("K230", "K235", "K250", "K255", "K260", "K265") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //
  procedure InsereVSSaidaParaProduzirCompartilhado();
  begin
    Campo := 25;
    Registro := 292;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k292.GraGruX, SUM(k292.QTD) QTD ',
    'FROM efdicmsipik292 k292 ',
    'WHERE k292.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k292.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k292.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //


    Campo := 25;
    Registro := 302;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k302.GraGruX, SUM(k302.QTD) QTD ',
    'FROM efdicmsipik302 k302 ',
    'WHERE k302.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k302.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k302.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 25;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_NEG) QTD ', // � ???
    'FROM efdicmsipik270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    //'AND QTD_COR_NEG>0 ', ???
    'AND RegisPai IN ("K292", "K302") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //

  procedure InsereVSSaidaParaDesmontar();
  begin
    Campo := 26;
    Registro := 210;
    MovimID := 11;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k210.GraGruX, SUM(k210.QTD_ORI) QTD ',
    'FROM efdicmsipik210 k210 ',
    'WHERE k210.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k210.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k210.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 26;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_NEG) QTD ',
    'FROM efdicmsipik270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND QTD_COR_NEG>0 ',
    'AND RegisPai IN ("K210", "K215") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 26;
    Registro := 275;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k275.GraGruX, SUM(k275.QTD_COR_NEG) QTD ',
    'FROM efdicmsipik275 k275 ',
    'WHERE k275.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k275.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k275.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND QTD_COR_NEG>0 ',
    'AND RegisPai IN ("K210", "K215") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //
  procedure InsereVSSaidaParaClasse();
  begin
    Campo := 24;
    Registro := 220;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k220.GGXOri GraGruX, SUM(k220.QTD) QTD',
    'FROM efdicmsipik220 k220',
    'WHERE k220.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k220.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k220.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := -Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 24;
    Registro := 270;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k270.GraGruX, SUM(k270.QTD_COR_NEG) QTD ',
    'FROM efdicmsipik270 k270 ',
    'WHERE k270.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k270.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k270.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND QTD_COR_NEG>0 ',
    'AND RegisPai IN ("K220") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

    //

    Campo := 24;
    Registro := 275;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k275.GraGruX, SUM(k275.QTD_COR_NEG) QTD ',
    'FROM efdicmsipik275 k275 ',
    'WHERE k275.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k275.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k275.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND QTD_COR_NEG>0 ',
    'AND RegisPai IN ("K220") ',
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := - Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;

  end;
  //
  procedure InsereVSSaidaPorVenda();
  const
    sProcName = 'InsereVSEntradaPorVenda()';
  var
    Grandeza: Integer;
  begin
    //fazer venda de outro GraGruX!
    Campo := 22;
    Registro := 0;
    MovimID := 2;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    //'SELECT vmi.GraGruX, SUM(vmi.Pecas) Pecas,',
    'SELECT IF(vmi.GGXRcl <> 0 AND ',
    'vmi.GGXRcl <> vmi.GraGruX, vmi.GGXRcl, vmi.GraGruX) GGX, ',
    'SUM(vmi.Pecas) Pecas,',
    'SUM(vmi.AreaM2) AreaM2, SUM(PesoKg) PesoKg, ',
    'med.Grandeza  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
    SQL_PeriodoVS,
    'AND vmi.MovimID IN (2)',
    'AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.FEmpresa),
    //'GROUP BY GraGruX',
    //'ORDER BY GraGruX ',
    'GROUP BY GGX',
    'ORDER BY GGX ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      Grandeza := Qry.FieldByName('Grandeza').AsInteger;
      GraGruX  := Qry.FieldByName('GGX').AsInteger;
      //
      case Grandeza of
        0: QTD := Qry.FieldByName('Pecas').AsFloat;
        1: QTD := Qry.FieldByName('AreaM2').AsFloat;
        2: QTD := Qry.FieldByName('PesoKg').AsFloat;
        else
          Geral.MB_Erro('"Grandeza" = ' + Geral.FF0(Grandeza) + ' indefinido em '
          + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      end;
      if QTD >= 0 then
        Geral.MB_Erro('"QTD" = ' + Geral.FFT(QTD, 2, siNegativo) + ' inv�lido em '
        + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      InsereAtualVerif(GraGruX, QTD);
      Qry.Next;
    end;
  end;
  //

  procedure InserePQSaidaParaETE();
  const
    sProcName = 'InserePQSaidaParaETE()';
  var
    Grandeza, GGX: Integer;
  begin
    Campo := 41;
    Registro := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsPQx;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Insumo, SUM(Peso) Peso ',
    'FROM pqx ',
    SQL_PeriodoPQ,
    'AND Tipo=' + Geral.FF0(VAR_FATID_0150),
    'GROUP BY Insumo ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD := Qry.FieldByName('Peso').AsFloat;
      GGX := UnPQx.ObtemGGXdeInsumo(Qry.FieldByName('Insumo').AsInteger);
      InsereAtualVerif(GGX, QTD);
      Qry.Next;
    end;
  end;

  //

  procedure InserePQSaidaDevolucao();
  const
    sProcName = 'InserePQSaidaDevolucao()';
  var
    Grandeza, GGX: Integer;
  begin
    Campo := 42;
    Registro := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsPQx;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Insumo, SUM(Peso) Peso ',
    'FROM pqx ',
    SQL_PeriodoPQ,
    'AND Tipo=' + Geral.FF0(VAR_FATID_0170),
    'GROUP BY Insumo ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD := Qry.FieldByName('Peso').AsFloat;
      GGX := UnPQx.ObtemGGXdeInsumo(Qry.FieldByName('Insumo').AsInteger);
      InsereAtualVerif(GGX, QTD);
      Qry.Next;
    end;
  end;

  //
  procedure InserePQBalanco();
  const
    sProcName = 'InserePQBalanco';
  var
    GGX: Integer;
  begin
    Campo := 43;
    Registro := 0;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsPQx;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _sped_efdicmsipik2xx_o_p; ',
    'CREATE TABLE _sped_efdicmsipik2xx_o_p ',
    ' ',
    'SELECT ggx.Controle GGX, SUM(pqx.Peso) Peso ',
    'FROM ' + TMeuDB + '.pqx pqx ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.GraGru1=pqx.Insumo ',
    SQL_PeriodoPQ,
    'AND Tipo<>0 ',
    'AND Insumo > 0 ',
    'GROUP BY ggx.Controle ',
    ' ',
    'UNION ',
    ' ',
    'SELECT k200.GraGruX GGX, SUM(-k200.QTD) Peso ',
    'FROM ' + TMeuDB + '.efdicmsipik200 k200  ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=k200.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE k200.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k200.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'AND k200.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND gg1.PrdGrupTip=-2 ',
    'GROUP BY GraGruX  ',
    ' ',
    'UNION ',
    ' ',
    'SELECT k200.GraGruX GGX, SUM(k200.QTD) Peso ',
    'FROM ' + TMeuDB + '.efdicmsipik200 k200  ',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=k200.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE k200.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k200.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value - 1),
    'AND k200.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND gg1.PrdGrupTip=-2 ',
    'GROUP BY GraGruX  ',
    ' ',
    '; ',
    ' ',
    'SELECT GGX, SUM(Peso) Peso ',
    'FROM  _sped_efdicmsipik2xx_o_p ',
    'GROUP BY GGX ',
    'ORDER BY GGX; ',
    ' ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD := -Qry.FieldByName('Peso').AsFloat;
      GGX := Qry.FieldByName('GGX').AsInteger;
      InsereAtualVerif(GGX, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSErrosEmpresa();
  const
    sProcName = 'InsereVSErrosEmpresa()';
  var
    Grandeza: Integer;
  begin
    Campo := 45;
    Registro := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.GraGruX, vmi.MovimID, SUM(vmi.Pecas) Pecas, ',
    'SUM(vmi.AreaM2) AreaM2, SUM(PesoKg) PesoKg,  ',
    'med.Grandeza, COUNT(vmi.GraGruX) ITENS   ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX  ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed  ',
    SQL_PeriodoVS,
    'AND Empresa>-11 ', // N�o � uma empresa!
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX  ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      MovimID  := Qry.FieldByName('MovimID').AsInteger;
      Grandeza := Qry.FieldByName('Grandeza').AsInteger;
      GraGruX  := Qry.FieldByName('GraGruX').AsInteger;
      //
      case Grandeza of
        0: QTD := Qry.FieldByName('Pecas').AsFloat;
        1: QTD := Qry.FieldByName('AreaM2').AsFloat;
        2: QTD := Qry.FieldByName('PesoKg').AsFloat;
        else
          Geral.MB_Erro('"Grandeza" = ' + Geral.FF0(Grandeza) + ' indefinido em '
          + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      end;
(*
      if QTD <= 0 then
        Geral.MB_Erro('"QTD" = ' + Geral.FFT(QTD, 2, siNegativo) + ' inv�lido em '
        + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
*)
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSIndevidos();
  const
    sProcName = 'InsereVSIndevidos()';
  var
    Grandeza: Integer;
  begin
    Campo := 46;
    Registro := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.GraGruX, vmi.MovimID, SUM(vmi.Pecas) Pecas,',
    'SUM(vmi.AreaM2) AreaM2, SUM(PesoKg) PesoKg, ',
    'med.Grandeza  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
    SQL_PeriodoVS,
    'AND vmi.MovimID IN (0,3,4,5,9,10,12,13,17,18)',
    'AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.FEmpresa),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      MovimID  := Qry.FieldByName('MovimID').AsInteger;
      Grandeza := Qry.FieldByName('Grandeza').AsInteger;
      GraGruX  := Qry.FieldByName('GraGruX').AsInteger;
      //
      case Grandeza of
        0: QTD := Qry.FieldByName('Pecas').AsFloat;
        1: QTD := Qry.FieldByName('AreaM2').AsFloat;
        2: QTD := Qry.FieldByName('PesoKg').AsFloat;
        else
          Geral.MB_Erro('"Grandeza" = ' + Geral.FF0(Grandeza) + ' indefinido em '
          + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      end;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InserePQIndevidos();
  const
    sProcName = 'InserePQIndevidos)';
  var
    GGX: Integer;
  begin
    Campo := 46;
    Registro := 0;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsPQx;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _SPED_EFD_K2XX_SEM_MC;',
    'CREATE TABLE _SPED_EFD_K2XX_SEM_MC',
    ' ',
    'SELECT Insumo, SUM(Peso) Peso ',
    'FROM ' + TMeuDB + '.pqx ',
    SQL_PeriodoPQ,
    'AND Peso <> 0 ',
    'AND Insumo > 0 ',
    'AND Tipo=' + Geral.FF0(VAR_FATID_0110),
    'AND OrigemCodi IN (',
    '  SELECT Codigo',
    '  FROM ' + TMeuDB + '.emit',
    '  WHERE VSMovCod=0)',
    'GROUP BY Insumo',
    'UNION',
    '',
    'SELECT Insumo, SUM(Peso) Peso ',
    'FROM ' + TMeuDB + '.pqx ',
    SQL_PeriodoPQ,
    'AND Peso <> 0 ',
    'AND Insumo > 0 ',
    'AND Tipo=' + Geral.FF0(VAR_FATID_0190),
    'AND OrigemCodi IN (',
    '  SELECT Codigo',
    '  FROM ' + TMeuDB + '.pqo',
    '  WHERE VSMovCod=0)',
    'GROUP BY Insumo',
    ';',
    '',
    'SELECT Insumo, SUM(Peso) Peso ',
    'FROM _SPED_EFD_K2XX_SEM_MC',
    'GROUP BY Insumo;',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD := Qry.FieldByName('Peso').AsFloat;
      GGX := UnPQx.ObtemGGXdeInsumo(Qry.FieldByName('Insumo').AsInteger);
      //
      InsereAtualVerif(GGX, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSSubProduto1();
  const
    sProcName = 'InsereVSSubProduto1()';
  var
    Grandeza: Integer;
  begin
    EXIT; // !!! Duplicando!!!
    Campo := 44;
    Registro := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.GraGruX, vmi.MovimID, SUM(vmi.Pecas) Pecas,',
    'SUM(vmi.AreaM2) AreaM2, SUM(PesoKg) PesoKg, ',
    'med.Grandeza  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle ',
    'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed ',
    SQL_PeriodoVS,
    'AND (vmi.MovimID IN (23) ',
(*
    '  OR ',
    '    (vmi.MovimID = 11 AND MovimNiv=9 AND ',
    '     cou.CouNiv2 IN (' + CO_XXXNIV2_SUBPRD + ')) ', // Raspa ou subproduto!
*)
    ') ',
    'AND Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.FEmpresa),
    'GROUP BY GraGruX, MovimID',
    'ORDER BY GraGruX ',
    '']);
    //Geral.MB_SQL(Self, Qry);
    Qry.First;
    while not Qry.Eof do
    begin
      MovimID  := Qry.FieldByName('MovimID').AsInteger;
      Grandeza := Qry.FieldByName('Grandeza').AsInteger;
      GraGruX  := Qry.FieldByName('GraGruX').AsInteger;
      //
      case Grandeza of
        0: QTD := Qry.FieldByName('Pecas').AsFloat;
        1: QTD := Qry.FieldByName('AreaM2').AsFloat;
        2: QTD := Qry.FieldByName('PesoKg').AsFloat;
        else
          Geral.MB_Erro('"Grandeza" = ' + Geral.FF0(Grandeza) + ' indefinido em '
          + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      end;
      if QTD <= 0 then
        Geral.MB_Erro('"QTD" = ' + Geral.FFT(QTD, 2, siNegativo) + ' inv�lido em '
        + sProcName + sLineBreak +
        'Reduzido: ' + Geral.FF0(GraGruX) + sLineBreak +
        'Grandeza: ' + sGRANDEZA_UNIDMED[Grandeza]);
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereVSSubProduto2();
  var
    Qtde: Double;
  begin
    Campo := 44;
    //
    Registro := 235;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k23s.GraGruX, SUM(k23s.QTD_ENC) QTD',
    'FROM efdicmsipik23subprd k23s',
    'WHERE k23s.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k23s.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k23s.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
    //
    Registro := 255;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k25s.GraGruX, SUM(k25s.QTD) QTD',
    'FROM efdicmsipik25subprd k25s',
    'WHERE k25s.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k25s.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k25s.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
    //
    Registro := 291;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k29s.GraGruX, SUM(k29s.QTD) QTD',
    'FROM efdicmsipik29subprd k29s',
    'WHERE k29s.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k29s.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k29s.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
    //
    Registro := 301;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsVMI;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k30s.GraGruX, SUM(k30s.QTD) QTD',
    'FROM efdicmsipik30subprd k30s',
    'WHERE k30s.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k30s.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k30s.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX',
    'ORDER BY GraGruX',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
  procedure InsereEstoqueDoFimDoPeriodo();
  begin
    Campo := 21;
    Registro := 200;
    MovimID := 0;
    ESTSTabSorc := TEstqSPEDTabSorc.estsND;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT k200.GraGruX, SUM(k200.QTD) QTD ',
    'FROM efdicmsipik200 k200 ',
    'WHERE k200.ImporExpor=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100ImporExpor.Value),
    'AND k200.Empresa=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100Empresa.Value),
    'AND k200.AnoMes=' + Geral.FF0(DfSEII_v03_0_2_a.QrK100AnoMes.Value),
    'GROUP BY GraGruX ',
    'ORDER BY GraGruX ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      QTD     := Qry.FieldByName('QTD').AsFloat;
      InsereAtualVerif(Qry.FieldByName('GraGruX').AsInteger, QTD);
      Qry.Next;
    end;
  end;
  //
begin
  if not UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'efdicmsipik_confg', [
  'ImporExpor', 'AnoMes', 'Empresa'], ['=','=','='],
  [DfSEII_v03_0_2_a.QrE001ImporExpor.Value, DfSEII_v03_0_2_a.QrE001AnoMes.Value,
  DfSEII_v03_0_2_a.QrE001Empresa.Value], '') then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    DiaIni        := Geral.AnoMesToData(DfSEII_v03_0_2_a.QrE001AnoMes.Value, 1);
    DiaFim        := IncMonth(DiaIni, 1) -1;
    SQL_PeriodoVS := dmkPF.SQL_Periodo('WHERE DataHora ', DiaIni, DiaFim, True, True);
    SQL_PeriodoPQ := dmkPF.SQL_Periodo('WHERE DataX ', DiaIni, DiaFim, True, True);
    ImporExpor    := DfSEII_v03_0_2_a.QrE001ImporExpor.Value;
    AnoMes        := DfSEII_v03_0_2_a.QrE001AnoMes.Value;
    Empresa       := DfSEII_v03_0_2_a.QrE001Empresa.Value;
    Qry := TmySQLQuery.Create(Dmod);
    try
      (*01*)InsereEstoqueDoInicioDoPeriodo();
      (*02*)InsereVSEntradaPorCompra();
      (*02*)InserePQEntradaPorCompra();
      (*03*)InsereVSEntradaPorProducao();
      (*04*)InsereVSEntradaPorClasse();
      (*05*)InsereVSEntradaPorProducaoCompartilhada();
      (*06*)InsereVSEntradaPorDesmonte();
      (*07*)InsereVSProdutoDeReforma();
      //
      (*21*)InsereEstoqueDoFimDoPeriodo();
      (*22*)InsereVSSaidaPorVenda();
      (*23*)InsereVSSaidaParaProduzir();
      (*24*)InsereVSSaidaParaClasse();
      (*25*)InsereVSSaidaParaProduzirCompartilhado();
      (*26*)InsereVSSaidaParaDesmontar();
      (*27*)InsereVSInsumoNaReforma();
      //
      (*41*)InserePQSaidaParaETE();
      (*42*)InserePQSaidaDevolucao();
      (*43*)InserePQBalanco();
      (*44*)InsereVSSubProduto1();
      (*44*)InsereVSSubProduto2();
      //
      (*45*)InsereVSErrosEmpresa();
      (*46*)InsereVSIndevidos();
      (*46*)InserePQIndevidos();
      //
      DfSEII_v03_0_2_a.ReopenK_ConfG(0);
    finally
      Qry.Free;
    end;
  finally
    Screen.Cursor := crDefault
  end;
end;
}

procedure TFmEfdIcmsIpiE001_v03_0_2_a.VSxPesagemPQ1Click(Sender: TObject);
begin
(*&�%
  AppPF.MostraFormCfgEstqVSxPQ_EFD(
    DfSEII_v03_0_2_a.QrE001ImporExpor.Value,
    DfSEII_v03_0_2_a.QrE001AnoMes.Value,
    DfSEII_v03_0_2_a.QrE001Empresa.Value,
    0);
%�&*)
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.FormShow(Sender: TObject);
var
  Qry: TmySQLQuery;
  N_D, Nao, Sim: Integer;
begin
  // Temporario
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando Tipo_Item dos produtos grade');
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT  ',
    'SUM(IF(TIPO_ITEM < 0, 1, 0)) N_D, ',
    'SUM(IF(TIPO_ITEM < 1, 1, 0)) Nao, ',
    'SUM(IF(TIPO_ITEM > 0, 1, 0)) Sim ',
    'FROM prdgruptip ',
    'WHERE Codigo <>0 ',
    '']);
    N_D := Qry.FieldByName('N_D').AsInteger;
    Nao := Qry.FieldByName('Nao').AsInteger;
    Sim := Qry.FieldByName('Sim').AsInteger;
    if Sim = 0 then
      Dmod.MyDB.Execute(
      'UPDATE prdgruptip SET TIPO_ITEM=-1 WHERE TIPO_ITEM=0 ' +
      'AND Codigo <> 0');
    if (N_D > 0) or ((Nao > 0) and (Sim = 0)) then
    begin
      Geral.MB_Aviso('Existem ' + Geral.FF0(Nao) +
      ' tipos de produtos de grade sem defini��o de:' + sLineBreak +
      '"Tipo de item - Atividades Industriais, Comerciais e Servi�os"');
      Close;
      Exit;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Qry.Free;
  end;
  // Temporario
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando Tipo_Item dos produtos grade');
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT  ',
    'SUM(IF(TIPO_ITEM =  -1, 1, 0)) Nao, ',
    'SUM(IF(TIPO_ITEM <> -1, 1, 0)) Sim ',
    'FROM gragru2 ',
    'WHERE Nivel2<>0 ',
    '']);
    Nao := Qry.FieldByName('Nao').AsInteger;
    Sim := Qry.FieldByName('Sim').AsInteger;
    if (Nao > 0) then
    begin
      Geral.MB_Aviso('Existem ' + Geral.FF0(Nao) +
      ' "Nivel 2" de grade sem defini��o de:' + sLineBreak +
      '"Tipo de item - Atividades Industriais, Comerciais e Servi�os"' +
      sLineBreak +
      'Para alterar v� na janela de produtos de grade e clique com o bot�o contrario na grade do "N�vel 2"');
      Close;
      Exit;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Qry.Free;
  end;
end;

procedure TFmEfdIcmsIpiE001_v03_0_2_a.Geraoxestoque1Click(Sender: TObject);
begin
(*&�%
  AppPF.MostraFormCfgEstqGeradoEFD(
    DfSEII_v03_0_2_a.QrE001ImporExpor.Value,
    DfSEII_v03_0_2_a.QrE001AnoMes.Value,
    DfSEII_v03_0_2_a.QrE001Empresa.Value,
    0);
%�&*)
end;

end.

