unit EfdIcmsIpiK300_v03_0_2_a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup, dmkEditCalc, UnDmkProcFunc,
  UnEfdIcmsIpi_PF, UnAppPF, UnAppEnums;

type
  TFmEfdIcmsIpiK300_v03_0_2_a = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdPeriApu: TdmkEdit;
    Label6: TLabel;
    EdKndTab: TdmkEdit;
    Label9: TLabel;
    Panel5: TPanel;
    EdKndCod: TdmkEdit;
    Label7: TLabel;
    EdKndNSU: TdmkEdit;
    Label8: TLabel;
    EdKndItm: TdmkEdit;
    Label10: TLabel;
    EdKndAID: TdmkEdit;
    Label1: TLabel;
    EdKndNiv: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    EdIDSeq1: TdmkEdit;
    TPDT_PROD: TdmkEditDateTimePicker;
    LaDT_INI_OP: TLabel;
    LaMovimCod: TLabel;
    EdMovimCod: TdmkEdit;
    EdTxt: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdMovimCodRedefinido(Sender: TObject);
    procedure EdMovimCodChange(Sender: TObject);
    procedure EdKndTabRedefinido(Sender: TObject);
  private
    { Private declarations }
    //procedure ReopenGraGruX();
  public
    { Public declarations }
    FID_SEK, FOriOpeProc, FOrigemIDKnd: Integer;
  end;

  var
  FmEfdIcmsIpiK300_v03_0_2_a: TFmEfdIcmsIpiK300_v03_0_2_a;

implementation

uses UnMyObjects, Module, (*EfdIcmsIpiE001,*) UMySQLModule, DmkDAC_PF, MyListas,
  ModuleFin, UnFinanceiro, SpedEfdIcmsIpi_v03_0_2_a;

{$R *.DFM}

procedure TFmEfdIcmsIpiK300_v03_0_2_a.BtOKClick(Sender: TObject);
const
  COD_DOC_OP = '';
var
  DT_PROD: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq1, ID_SEK, OriOpeProc, AWServerID, AWStatSinc: Integer;
  SQLType: TSQLType;
  ComOP: Boolean;
  Data: TDateTime;
begin
//N�O permitir informar OP quando n�o existir IME-C
  ComOP          := EfdIcmsIpi_PF.HabilitaDocOP(KndTab);
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  PeriApu        := EdPeriApu.ValueVariant;
  KndTab         := EdKndTab.ValueVariant;
  if SQLType = stIns then
  begin
    //KndTab         := Integer(TEstqSPEDTabSorc.estsNoSrc);
    KndCod         := PeriApu;
    KndNSU         := PeriApu;
    KndItm         := 0; // Abaixo
    KndAID         := Integer(TEstqMovimID.emidAjuste);
    KndNiv         := Integer(TEstqMovimNiv.eminSemNiv);
    IDSeq1         := 0; // Abaixo
    ID_SEK         := Integer(TSPED_EFD_ComoLancou.seclLancadoManual);
    OriOpeProc     := Integer(TOrigemOpeProc.oopND);
    //OrigemIDKnd    := Integer(TOrigemIDKnd.oidk000ND);
  end else
  begin
    //KndTab         := EdKndTab.ValueVariant;
    KndCod         := EdKndCod.ValueVariant;
    KndNSU         := EdKndNSU.ValueVariant;
    KndItm         := EdKndItm.ValueVariant;
    KndAID         := EdKndAID.ValueVariant;
    KndNiv         := EdKndNiv.ValueVariant;
    IDSeq1         := EdIDSeq1.ValueVariant;
    ID_SEK         := FID_SEK;
    OriOpeProc     := FOriOpeProc;
    //OrigemIDKnd    := FOrigemIDKnd;
  end;
  Data           := Trunc(TPDT_PROD.Date);
  //MovimCod       := EdMovimCod.ValueVariant;
  //
  DT_PROD        := Geral.FDT(Data, 1);
  //
  if MyObjects.FIC(ComOP and (TPDT_PROD.Date < 2), TPDT_PROD,
    'Informe a data inicial da Ordem de Servi�o! (IME-C)') then Exit;
(*
  if MyObjects.FIC(ComOP and (MovimCod = 0), EdMovimCod,
    'Informe o c�digo da Ordem de Servi�o! (IME-C)') then Exit;
*)
  //ID_SEK         := ;
  //
  (*Campo 01 (REG) - Valor V�lido: [K300]*)
  (*Campo 02 (DT_PROD) - Valida��o: a data deve estar compreendida no per�odo
  informado nos campos DT_INI e DT_FIN do Registro K100.*)

  if not EfdIcmsIpi_PF.ValidaDataUnicaDentroDoAnoMesSPED(AnoMes, Data,
  COD_DOC_OP) then Exit;

  KndItm := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik300', 'KndItm', [
  'ImporExpor', 'AnoMes', 'KndTab'], [ImporExpor, AnoMes, KndTab],
  SQLType, KndItm, siPositivo, EdKndItm);
  //
  IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik300', 'IDSeq1', [
  'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
  ImporExpor, AnoMes, Empresa, PeriApu],
  SQLType, IDSeq1, siPositivo, EdIDSeq1);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdicmsipik300', False, [
  'KndAID', 'KndNiv', 'IDSeq1',
  'ID_SEK', 'OriOpeProc', 'DT_PROD'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'], [
  KndAID, KndNiv, IDSeq1,
  ID_SEK, OriOpeProc, DT_PROD], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True) then
  begin
    DfSEII_v03_0_2_a.ReopenK300(IDSeq1);
    Close;
  end;
end;

procedure TFmEfdIcmsIpiK300_v03_0_2_a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpiK300_v03_0_2_a.EdKndTabRedefinido(Sender: TObject);
begin
  EfdIcmsIpi_PF.HabilitaMovimCodPorKndTab(Self, EdKndTab.ValueVariant,
  [LaMovimCod, EdMovimCod]);
end;

procedure TFmEfdIcmsIpiK300_v03_0_2_a.EdMovimCodChange(Sender: TObject);
begin
  EdTxt.Text := '';
end;

procedure TFmEfdIcmsIpiK300_v03_0_2_a.EdMovimCodRedefinido(Sender: TObject);
begin
  AppPF.VerificaSeExisteIMEC(EdTxt, EdMovimCod.ValueVariant);
end;

procedure TFmEfdIcmsIpiK300_v03_0_2_a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiK300_v03_0_2_a.FormCreate(Sender: TObject);
begin
  TPDT_PROD.Date := 0;
end;

procedure TFmEfdIcmsIpiK300_v03_0_2_a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
