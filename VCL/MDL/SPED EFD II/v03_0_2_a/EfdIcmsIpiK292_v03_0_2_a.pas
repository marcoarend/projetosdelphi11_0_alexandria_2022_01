unit EfdIcmsIpiK292_v03_0_2_a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup, dmkEditCalc, UnDmkProcFunc,
  UnEfdIcmsIpi_PF, Variants, UnAppEnums;

type
  TFmEfdIcmsIpiK292_v03_0_2_a = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdPeriApu: TdmkEdit;
    Label6: TLabel;
    EdKndTab: TdmkEdit;
    Label9: TLabel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXEx_TIPI: TWideStringField;
    DsGraGruX: TDataSource;
    Panel5: TPanel;
    EdGraGruX: TdmkEditCB;
    Label232: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    StaticText2: TStaticText;
    EdQTD: TdmkEdit;
    EdKndCod: TdmkEdit;
    Label7: TLabel;
    EdKndNSU: TdmkEdit;
    Label8: TLabel;
    EdKndItm: TdmkEdit;
    Label10: TLabel;
    EdKndAID: TdmkEdit;
    Label1: TLabel;
    EdKndNiv: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    EdIDSeq1: TdmkEdit;
    QrGraGruXGrandeza: TIntegerField;
    QrGraGruXTipo_Item: TIntegerField;
    EdIDSeq2: TdmkEdit;
    Label15: TLabel;
    DBText1: TDBText;
    EDJmpMovID: TdmkEdit;
    EdJmpMovCod: TdmkEdit;
    EDJmpNivel1: TdmkEdit;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EdJmpNivel2: TdmkEdit;
    EdPaiMovID: TdmkEdit;
    EdPaiMovCod: TdmkEdit;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdPaiNivel1: TdmkEdit;
    EdPaiNivel2: TdmkEdit;
    Label21: TLabel;
    Label22: TLabel;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdAnoMesChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGruX();
  public
    { Public declarations }
    FID_SEK, FESTSTabSorc, FOriOpeProc(*, FOrigemIDKnd*): Integer;
  end;

  var
  FmEfdIcmsIpiK292_v03_0_2_a: TFmEfdIcmsIpiK292_v03_0_2_a;

implementation

uses UnMyObjects, Module, (*EfdIcmsIpiE001,*) UMySQLModule, DmkDAC_PF, MyListas,
  ModuleFin, UnFinanceiro, SpedEfdIcmsIpi_v03_0_2_a;

{$R *.DFM}

procedure TFmEfdIcmsIpiK292_v03_0_2_a.BtOKClick(Sender: TObject);
(*
var
  COD_ITEM: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq1, IDSeq2, ID_SEK, GraGruX, OriOpeProc, JmpMovID, JmpMovCod,
  JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod, PaiNivel1, PaiNivel2: Integer;
  QTD: Double;
  SQLType: TSQLType;
  COD_DOC_OP: String;
*)
var
  COD_ITEM: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq1, IDSeq2, ID_SEK, GraGruX, ESTSTabSorc, OriOpeProc, JmpMovID,
  JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod, PaiNivel1, PaiNivel2:
  Integer;
  QTD: Double;
  SQLType: TSQLType;
  COD_DOC_OP: String;
begin
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  PeriApu        := EdPeriApu.ValueVariant;
  if SQLType = stIns then
  begin
    KndTab         := Integer(TEstqSPEDTabSorc.estsNoSrc);
    KndCod         := PeriApu;
    KndNSU         := PeriApu;
    KndItm         := 0; // Abaixo
    KndAID         := Integer(TEstqMovimID.emidAjuste);
    KndNiv         := Integer(TEstqMovimNiv.eminSemNiv);
    IDSeq1         := EdIDSeq1.ValueVariant;
    IDSeq2         := 0; // Abaixo
    ID_SEK         := Integer(TSPED_EFD_ComoLancou.seclLancadoManual);
    ESTSTabSorc    := Integer(TEstqSPEDTabSorc.estsNoSrc);
    OriOpeProc     := Integer(TOrigemOpeProc.oopND);
    //OrigemIDKnd    := Integer(TOrigemIDKnd.oidk000ND);
    JmpMovID       := 0;
    JmpMovCod      := 0;
    JmpNivel1      := 0;
    JmpNivel2      := 0;
    PaiMovID       := 0;
    PaiMovCod      := 0;
    PaiNivel1      := 0;
    PaiNivel2      := 0;
  end else
  begin
    KndTab         := EdKndTab.ValueVariant;
    KndCod         := EdKndCod.ValueVariant;
    KndNSU         := EdKndNSU.ValueVariant;
    KndItm         := EdKndItm.ValueVariant;
    KndAID         := EdKndAID.ValueVariant;
    KndNiv         := EdKndNiv.ValueVariant;
    IDSeq1         := EdIDSeq1.ValueVariant;
    IDSeq2         := EdIDSeq2.ValueVariant;;
    ID_SEK         := FID_SEK;
    ESTSTabSorc    := FESTSTabSorc;
    OriOpeProc     := FOriOpeProc;
    //OrigemIDKnd    := FOrigemIDKnd;
    JmpMovID       := EdJmpMovID.ValueVariant;
    JmpMovCod      := EdJmpMovCod.ValueVariant;
    JmpNivel1      := EdJmpNivel1.ValueVariant;
    JmpNivel2      := EdJmpNivel2.ValueVariant;
    PaiMovID       := EdPaiMovID.ValueVariant;
    PaiMovCod      := EdPaiMovCod.ValueVariant;
    PaiNivel1      := EdPaiNivel1.ValueVariant;
    PaiNivel2      := EdPaiNivel2.ValueVariant;
  end;
  GraGruX        := EdGraGruX.ValueVariant;
  //
  COD_ITEM       := dmkPF.ITS_Null(GraGruX);
  QTD            := EdQTD.ValueVariant;
  //
  COD_DOC_OP     := DfSEII_v03_0_2_a.QrK290COD_DOC_OP.Value;
  //
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o c�digo do produto!') then
    Exit;
  //
  (*Campo 01 (REG) - Valor V�lido: [K292]*)
  (*Campo 02 (COD_ITEM) � Valida��es:
  a) o c�digo do item componente/insumo dever� existir no campo COD_ITEM do
     Registro 0200;
  b) o c�digo do item componente/insumo deve ser diferente do c�digo do produto
     resultante (COD_ITEM do Registro K291);
  c) o tipo do componente/insumo (campo TIPO_ITEM do Registro 0200) deve ser
     igual a 00, 01, 02, 03, 04, 05 ou 10.*)

  if not EfdIcmsIpi_PF.ValidaGGXEstahDentroDoAnoMesSPED(AnoMes, GraGruX,
  COD_DOC_OP) then Exit;

  (*Campo 03 (QTD) � Preenchimento: n�o � admitida quantidade negativa.
   Valida��o: deve ser maior que zero.*)

  if not EfdIcmsIpi_PF.ValidaQtdeDentroDoAnoMesSPED(AnoMes, QTD, COD_DOC_OP,
  (*MaiorQueZero*)False) then Exit;

  //

  KndItm := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik292', 'KndItm', [
  'ImporExpor', 'AnoMes', 'KndTab'], [ImporExpor, AnoMes, KndTab],
  SQLType, KndItm, siPositivo, EdKndItm);
  //
  IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik292', 'IDSeq2', [
  'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
  ImporExpor, AnoMes, Empresa, PeriApu],
  SQLType, IDSeq2, siPositivo, EdIDSeq2);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdicmsipik292', False, [
  'KndAID', 'KndNiv', 'IDSeq1',
  'IDSeq2', 'ID_SEK', 'COD_ITEM',
  'QTD', 'ESTSTabSorc', 'OriOpeProc',
  'JmpMovID', 'JmpMovCod', 'JmpNivel1',
  'JmpNivel2', 'PaiMovID', 'PaiMovCod',
  'PaiNivel1', 'PaiNivel2', 'GraGruX'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'], [
  KndAID, KndNiv, IDSeq1,
  IDSeq2, ID_SEK, COD_ITEM,
  QTD, ESTSTabSorc, OriOpeProc,
  JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, PaiMovCod,
  PaiNivel1, PaiNivel2, GraGruX], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True) then
  begin
    DfSEII_v03_0_2_a.ReopenK292(IDSeq2);
    //
    if (SQLType = stIns) and (CkContinuar.Checked) then
    begin
      Geral.MB_Info('Intem inclu�do com sucesso!');
      //
      EdGraGruX.ValueVariant := 0;
      CBGraGruX.KeyValue     := Null;
      EdQTD.ValueVariant     := 0;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmEfdIcmsIpiK292_v03_0_2_a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpiK292_v03_0_2_a.EdAnoMesChange(Sender: TObject);
begin
 //MyObjects.LimitaDatasPeloAnoMesDateTimePicker(TPDT_SAIDA, EdAnoMes.ValueVariant);
end;

procedure TFmEfdIcmsIpiK292_v03_0_2_a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiK292_v03_0_2_a.FormCreate(Sender: TObject);
begin
  ReopenGraGruX();
end;

procedure TFmEfdIcmsIpiK292_v03_0_2_a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdIcmsIpiK292_v03_0_2_a.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
begin
  SQL_AND := '';
  SQL_LFT := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  SQL_LFT,
  'WHERE ggx.Controle > 0 ',
  SQL_AND,
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

end.
