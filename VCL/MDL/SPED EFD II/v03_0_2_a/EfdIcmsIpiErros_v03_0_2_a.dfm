object FmEfdIcmsIpiErros_v03_0_2_a: TFmEfdIcmsIpiErros_v03_0_2_a
  Left = 339
  Top = 185
  Caption = 'SPE-D_EII-003 :: Erros de Exporta'#231#227'o SPED EFD ICMS/IPI'
  ClientHeight = 555
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 89
    Width = 1008
    Height = 401
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 0
      Width = 440
      Height = 401
      Align = alLeft
      DataSource = DsSpedEfdIcmsIpiErrs
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'LinArq'
          Title.Caption = 'N'#186' linha'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'REG'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Campo'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Erro'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DESCRI_ERRO'
          Title.Caption = 'Descri'#231#227'o do erro'
          Width = 200
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 440
      Top = 0
      Width = 568
      Height = 401
      Align = alClient
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object LaConteudo: TLabel
        Left = 0
        Top = 0
        Width = 568
        Height = 16
        Align = alTop
        Alignment = taCenter
        Caption = ' Conte'#250'do da linha selecionada'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 202
      end
      object LaNotas: TLabel
        Left = 0
        Top = 96
        Width = 568
        Height = 16
        Align = alTop
        Alignment = taCenter
        Caption = 'Notas Fiscais dependentes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 168
      end
      object DBGLinha: TDBGrid
        Left = 0
        Top = 16
        Width = 568
        Height = 80
        Align = alTop
        DataSource = DsLinha
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            Visible = True
          end>
      end
      object DBGNotas: TDBGrid
        Left = 0
        Top = 112
        Width = 568
        Height = 289
        Align = alClient
        DataSource = DsNotas
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'DataFiscal'
            Title.Caption = 'Dta Fiscal'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodInfoDest'
            Title.Caption = 'C'#243'd. emit'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodInfoEmit'
            Title.Caption = 'C'#243'd. Dest'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'emit_CNPJ'
            Title.Caption = 'CNPJ emit'
            Width = 113
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_mod'
            Title.Caption = 'mod'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_serie'
            Title.Caption = 'S'#233'rie'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_nNF'
            Title.Caption = 'N'#186' NF'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dest_xNome'
            Title.Caption = 'Nome emit'
            Width = 213
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatID'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_FATID'
            Title.Caption = 'Descri'#231#227'o FatID'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatNum'
            Width = 56
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        Visible = False
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        Visible = False
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 509
        Height = 32
        Caption = 'Erros de Exporta'#231#227'o SPED EFD ICMS/IPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 509
        Height = 32
        Caption = 'Erros de Exporta'#231#227'o SPED EFD ICMS/IPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 509
        Height = 32
        Caption = 'Erros de Exporta'#231#227'o SPED EFD ICMS/IPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 490
    Width = 1008
    Height = 65
    Align = alBottom
    TabOrder = 2
    object PainelConfirma: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 48
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 5
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object Panel2: TPanel
        Left = 872
        Top = 0
        Width = 132
        Height = 48
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sair'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtNF: TBitBtn
        Tag = 5
        Left = 460
        Top = 4
        Width = 120
        Height = 40
        Caption = '&N.F.'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtNFClick
      end
      object BtReduzido: TBitBtn
        Tag = 20
        Left = 336
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Reduzido'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtReduzidoClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 37
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel47: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 20
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrSpedEfdIcmsIpiErrs: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrSpedEfdIcmsIpiErrsBeforeClose
    AfterScroll = QrSpedEfdIcmsIpiErrsAfterScroll
    OnCalcFields = QrSpedEfdIcmsIpiErrsCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM spedefdicmsipierrs'
      'WHERE ImporExpor=:P0'
      'AND AnoMes=:P1'
      'AND Empresa=:P2'
      ''
      ''
      '')
    Left = 4
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSpedEfdIcmsIpiErrsLinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrSpedEfdIcmsIpiErrsREG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrSpedEfdIcmsIpiErrsCampo: TWideStringField
      FieldName = 'Campo'
      Size = 50
    end
    object QrSpedEfdIcmsIpiErrsErro: TSmallintField
      FieldName = 'Erro'
    end
    object QrSpedEfdIcmsIpiErrsDESCRI_ERRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DESCRI_ERRO'
      Size = 255
      Calculated = True
    end
  end
  object DsSpedEfdIcmsIpiErrs: TDataSource
    DataSet = QrSpedEfdIcmsIpiErrs
    Left = 32
    Top = 4
  end
  object QrLinha: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ''
      ''
      ''
      '')
    Left = 528
    Top = 76
  end
  object DsLinha: TDataSource
    DataSet = QrLinha
    Left = 556
    Top = 76
  end
  object QrNotas: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNotasCalcFields
    SQL.Strings = (
      'SELECT * FROM nfecaba'
      'WHERE Empresa=-11'
      'AND DataFiscal BETWEEN "2010-01-01" AND "2010-01-31" '
      'AND CodInfoEmit = 1293'
      'OR CodInfoDest = 1293')
    Left = 528
    Top = 104
    object QrNotasFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNotasFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNotasEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNotasIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrNotasLoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrNotasversao: TFloatField
      FieldName = 'versao'
    end
    object QrNotasId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNotaside_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrNotaside_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrNotaside_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrNotaside_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrNotaside_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrNotaside_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNotaside_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNotaside_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNotaside_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrNotaside_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrNotaside_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrNotaside_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrNotaside_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrNotaside_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrNotaside_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrNotaside_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrNotaside_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrNotaside_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrNotasemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrNotasemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrNotasemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrNotasemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrNotasemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrNotasemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrNotasemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrNotasemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrNotasemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrNotasemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrNotasemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrNotasemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrNotasemit_cPais: TIntegerField
      FieldName = 'emit_cPais'
    end
    object QrNotasemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Size = 60
    end
    object QrNotasemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 14
    end
    object QrNotasemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrNotasemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Size = 14
    end
    object QrNotasemit_IM: TWideStringField
      FieldName = 'emit_IM'
      Size = 15
    end
    object QrNotasemit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Size = 7
    end
    object QrNotasdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrNotasdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrNotasdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrNotasdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrNotasdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrNotasdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrNotasdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrNotasdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
    end
    object QrNotasdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrNotasdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrNotasdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Size = 8
    end
    object QrNotasdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
    end
    object QrNotasdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Size = 60
    end
    object QrNotasdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 14
    end
    object QrNotasdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrNotasdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Size = 9
    end
    object QrNotasICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrNotasICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrNotasICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrNotasICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrNotasICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrNotasICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrNotasICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrNotasICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrNotasICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrNotasICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrNotasICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrNotasICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrNotasICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrNotasICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrNotasISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
    end
    object QrNotasISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
    end
    object QrNotasISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
    end
    object QrNotasISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
    end
    object QrNotasISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
    end
    object QrNotasRetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
    end
    object QrNotasRetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
    end
    object QrNotasRetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
    end
    object QrNotasRetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
    end
    object QrNotasRetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
    end
    object QrNotasRetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
    end
    object QrNotasRetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
    end
    object QrNotasModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrNotasTransporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Size = 14
    end
    object QrNotasTransporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Size = 11
    end
    object QrNotasTransporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Size = 60
    end
    object QrNotasTransporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
    end
    object QrNotasTransporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Size = 60
    end
    object QrNotasTransporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Size = 60
    end
    object QrNotasTransporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Size = 2
    end
    object QrNotasRetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
    end
    object QrNotasRetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
    end
    object QrNotasRetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
    end
    object QrNotasRetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
    end
    object QrNotasRetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Size = 5
    end
    object QrNotasRetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Size = 7
    end
    object QrNotasVeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Size = 8
    end
    object QrNotasVeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Size = 2
    end
    object QrNotasVeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
    end
    object QrNotasCobr_Fat_nFat: TWideStringField
      FieldName = 'Cobr_Fat_nFat'
      Size = 60
    end
    object QrNotasCobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
    end
    object QrNotasCobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
    end
    object QrNotasCobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
    end
    object QrNotasInfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNotasInfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNotasExporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Size = 2
    end
    object QrNotasExporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Size = 60
    end
    object QrNotasCompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrNotasCompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrNotasCompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
    object QrNotasStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNotasinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrNotasinfProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrNotasinfProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Size = 30
    end
    object QrNotasinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrNotasinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrNotasinfProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrNotasinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrNotasinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrNotasinfCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Size = 30
    end
    object QrNotasinfCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
    end
    object QrNotasinfCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Size = 30
    end
    object QrNotasinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrNotasinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrNotasinfCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Size = 28
    end
    object QrNotasinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrNotasinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrNotasinfCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
    end
    object QrNotasinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrNotas_Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrNotasFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
    end
    object QrNotasCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrNotasTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrNotasCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrNotasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNotasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNotasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNotasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNotasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNotasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNotasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNotasFreteExtra: TFloatField
      FieldName = 'FreteExtra'
    end
    object QrNotasSegurExtra: TFloatField
      FieldName = 'SegurExtra'
    end
    object QrNotasICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrNotasICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
    end
    object QrNotasICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrNotasICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
    end
    object QrNotasIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrNotasIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
    end
    object QrNotasIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrNotasIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
    end
    object QrNotasPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrNotasPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
    end
    object QrNotasPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrNotasPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
    end
    object QrNotasCOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object QrNotasCOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
    end
    object QrNotasCOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrNotasCOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
    end
    object QrNotasDataFiscal: TDateField
      FieldName = 'DataFiscal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNotasprotNFe_versao: TFloatField
      FieldName = 'protNFe_versao'
    end
    object QrNotasretCancNFe_versao: TFloatField
      FieldName = 'retCancNFe_versao'
    end
    object QrNotasSINTEGRA_ExpDeclNum: TWideStringField
      FieldName = 'SINTEGRA_ExpDeclNum'
      Size = 11
    end
    object QrNotasSINTEGRA_ExpDeclDta: TDateField
      FieldName = 'SINTEGRA_ExpDeclDta'
    end
    object QrNotasSINTEGRA_ExpNat: TWideStringField
      FieldName = 'SINTEGRA_ExpNat'
      Size = 1
    end
    object QrNotasSINTEGRA_ExpRegNum: TWideStringField
      FieldName = 'SINTEGRA_ExpRegNum'
      Size = 12
    end
    object QrNotasSINTEGRA_ExpRegDta: TDateField
      FieldName = 'SINTEGRA_ExpRegDta'
    end
    object QrNotasSINTEGRA_ExpConhNum: TWideStringField
      FieldName = 'SINTEGRA_ExpConhNum'
      Size = 16
    end
    object QrNotasSINTEGRA_ExpConhDta: TDateField
      FieldName = 'SINTEGRA_ExpConhDta'
    end
    object QrNotasSINTEGRA_ExpConhTip: TWideStringField
      FieldName = 'SINTEGRA_ExpConhTip'
      Size = 2
    end
    object QrNotasSINTEGRA_ExpPais: TWideStringField
      FieldName = 'SINTEGRA_ExpPais'
      Size = 4
    end
    object QrNotasSINTEGRA_ExpAverDta: TDateField
      FieldName = 'SINTEGRA_ExpAverDta'
    end
    object QrNotasCodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrNotasCodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object QrNotasCriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
    object QrNotaside_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrNotaside_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrNotaside_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrNotasemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
    object QrNotasdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrNotasVagao: TWideStringField
      FieldName = 'Vagao'
    end
    object QrNotasBalsa: TWideStringField
      FieldName = 'Balsa'
    end
    object QrNotasCodInfoTrsp: TIntegerField
      FieldName = 'CodInfoTrsp'
    end
    object QrNotasOrdemServ: TIntegerField
      FieldName = 'OrdemServ'
    end
    object QrNotasSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrNotasAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrNotasNFG_Serie: TWideStringField
      FieldName = 'NFG_Serie'
      Size = 3
    end
    object QrNotasNF_ICMSAlq: TFloatField
      FieldName = 'NF_ICMSAlq'
    end
    object QrNotasNF_CFOP: TWideStringField
      FieldName = 'NF_CFOP'
      Size = 4
    end
    object QrNotasImportado: TSmallintField
      FieldName = 'Importado'
    end
    object QrNotasNFG_SubSerie: TWideStringField
      FieldName = 'NFG_SubSerie'
      Size = 3
    end
    object QrNotasNFG_ValIsen: TFloatField
      FieldName = 'NFG_ValIsen'
    end
    object QrNotasNFG_NaoTrib: TFloatField
      FieldName = 'NFG_NaoTrib'
    end
    object QrNotasNFG_Outros: TFloatField
      FieldName = 'NFG_Outros'
    end
    object QrNotasNO_FATID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_FATID'
      Size = 255
      Calculated = True
    end
  end
  object DsNotas: TDataSource
    DataSet = QrNotas
    Left = 556
    Top = 104
  end
  object frxErros: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.407328206000000000
    ReportOptions.LastChange = 40699.948681365740000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure DetailData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Me_Tit_0001.Visible := <Me_Tit_0001>;'
      '  Me_Tit_0002.Visible := <Me_Tit_0002>;'
      '  Me_Tit_0003.Visible := <Me_Tit_0003>;'
      '  Me_Tit_0004.Visible := <Me_Tit_0004>;'
      '  Me_Tit_0005.Visible := <Me_Tit_0005>;'
      '  Me_Tit_0006.Visible := <Me_Tit_0006>;'
      '  Me_Tit_0007.Visible := <Me_Tit_0007>;'
      '  Me_Tit_0008.Visible := <Me_Tit_0008>;'
      '  Me_Tit_0009.Visible := <Me_Tit_0009>;'
      '  Me_Tit_0010.Visible := <Me_Tit_0010>;'
      '  Me_Tit_0011.Visible := <Me_Tit_0011>;'
      '  Me_Tit_0012.Visible := <Me_Tit_0012>;'
      '  Me_Tit_0013.Visible := <Me_Tit_0013>;'
      '  Me_Tit_0014.Visible := <Me_Tit_0014>;'
      '  Me_Tit_0015.Visible := <Me_Tit_0015>;'
      '  Me_Tit_0016.Visible := <Me_Tit_0016>;'
      '  Me_Tit_0017.Visible := <Me_Tit_0017>;'
      '  Me_Tit_0018.Visible := <Me_Tit_0018>;'
      '    '
      '  Me_Val_0001.Visible := <Me_Val_0001>;'
      '  Me_Val_0002.Visible := <Me_Val_0002>;'
      '  Me_Val_0003.Visible := <Me_Val_0003>;'
      '  Me_Val_0004.Visible := <Me_Val_0004>;'
      '  Me_Val_0005.Visible := <Me_Val_0005>;'
      '  Me_Val_0006.Visible := <Me_Val_0006>;'
      '  Me_Val_0007.Visible := <Me_Val_0007>;'
      '  Me_Val_0008.Visible := <Me_Val_0008>;'
      '  Me_Val_0009.Visible := <Me_Val_0009>;'
      '  Me_Val_0010.Visible := <Me_Val_0010>;'
      '  Me_Val_0011.Visible := <Me_Val_0011>;'
      '  Me_Val_0012.Visible := <Me_Val_0012>;'
      '  Me_Val_0013.Visible := <Me_Val_0013>;'
      '  Me_Val_0014.Visible := <Me_Val_0014>;'
      '  Me_Val_0015.Visible := <Me_Val_0015>;'
      '  Me_Val_0016.Visible := <Me_Val_0016>;'
      '  Me_Val_0017.Visible := <Me_Val_0017>;'
      '  Me_Val_0018.Visible := <Me_Val_0018>;'
      '    '
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxErrosGetValue
    Left = 88
    Top = 4
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDslinha
        DataSetName = 'frxDsLinha'
      end
      item
        DataSet = frxDsNotas
        DataSetName = 'frxDsNotas'
      end
      item
        DataSet = frxDsSpedEfdIcmsIpiErrs
        DataSetName = 'frxDsSpedEfdIcmsIpiErrs'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader5: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 75.590590240000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Width = 1046.929810000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 1031.811690000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 18.897650000000000000
          Width = 725.669760000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ERROS NA EXPORTA'#199#195'O SPED-EFD')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748610000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Erro')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 60.472480000000000000
          Width = 752.126438270000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o do erro')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 60.472480000000000000
          Width = 37.795153540000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'REG')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Linha')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 60.472480000000000000
          Width = 151.181102360000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do campo')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 1046.929810000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Empresa: [VARF_COD_EMPRESA] = [VARF_NO_EMPRESA]     -     Per'#237'od' +
              'o: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 385.512060000000000000
        Width = 1046.929810000000000000
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Width = 1046.929810000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object MD_NIVEL1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118141970000000000
        Top = 154.960730000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsSpedEfdIcmsIpiErrs
        DataSetName = 'frxDsSpedEfdIcmsIpiErrs'
        RowCount = 0
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Width = 37.795153540000000000
          Height = 15.118110240000000000
          DataSet = frxDsSpedEfdIcmsIpiErrs
          DataSetName = 'frxDsSpedEfdIcmsIpiErrs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSpedEfdIcmsIpiErrs."REG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsSpedEfdIcmsIpiErrs
          DataSetName = 'frxDsSpedEfdIcmsIpiErrs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'00;-00; '#39', <frxDsSpedEfdIcmsIpiErrs."Erro">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Width = 56.692950000000010000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'0000;-0000; '#39', <frxDsSpedEfdIcmsIpiErrs."LinArq">)' +
              ']')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 151.181102360000000000
          Height = 15.118110240000000000
          DataSet = frxDsSpedEfdIcmsIpiErrs
          DataSetName = 'frxDsSpedEfdIcmsIpiErrs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSpedEfdIcmsIpiErrs."Campo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 752.126438270000000000
          Height = 15.118110240000000000
          DataSet = frxDsSpedEfdIcmsIpiErrs
          DataSetName = 'frxDsSpedEfdIcmsIpiErrs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSpedEfdIcmsIpiErrs."DESCRI_ERRO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677158030000000000
        Top = 192.756030000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'DetailData1OnBeforePrint'
        DataSet = frxDslinha
        DataSetName = 'frxDsLinha'
        RowCount = 0
        object Me_Tit_0001: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0001: TfrxMemoView
          AllowVectorExport = True
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0002: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0002: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0003: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0003: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0004: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0004: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0005: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0005: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0006: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0006: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0007: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0007: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0008: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0008: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0009: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0009: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0010: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0010: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0011: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0011: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0012: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0012: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0013: TfrxMemoView
          AllowVectorExport = True
          Left = 680.315400000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0013: TfrxMemoView
          AllowVectorExport = True
          Left = 680.315400000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0014: TfrxMemoView
          AllowVectorExport = True
          Left = 737.008350000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0014: TfrxMemoView
          AllowVectorExport = True
          Left = 737.008350000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0015: TfrxMemoView
          AllowVectorExport = True
          Left = 793.701300000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0015: TfrxMemoView
          AllowVectorExport = True
          Left = 793.701300000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0016: TfrxMemoView
          AllowVectorExport = True
          Left = 850.394250000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0016: TfrxMemoView
          AllowVectorExport = True
          Left = 850.394250000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0017: TfrxMemoView
          AllowVectorExport = True
          Left = 907.087200000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0017: TfrxMemoView
          AllowVectorExport = True
          Left = 907.087200000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Tit_0018: TfrxMemoView
          AllowVectorExport = True
          Left = 963.780150000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_Val_0018: TfrxMemoView
          AllowVectorExport = True
          Left = 963.780150000000000000
          Top = 13.228339130000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData2: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 9.448818900000000000
        Top = 275.905690000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsNotas
        DataSetName = 'frxDsNotas'
        RowCount = 0
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DataField = 'DataFiscal'
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNotas."DataFiscal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DataField = 'CodInfoEmit'
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNotas."CodInfoEmit"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DataField = 'CodInfoDest'
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNotas."CodInfoDest"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 113.385890240000000000
          Height = 9.448818900000000000
          DataField = 'emit_CNPJ'
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNotas."emit_CNPJ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Width = 22.677165350000000000
          Height = 9.448818900000000000
          DataField = 'ide_mod'
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNotas."ide_mod"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Width = 26.456692913385830000
          Height = 9.448818900000000000
          DataField = 'ide_serie'
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNotas."ide_serie"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Width = 60.472440944881890000
          Height = 9.448818900000000000
          DataField = 'ide_nNF'
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNotas."ide_nNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Width = 211.653543307086600000
          Height = 9.448818900000000000
          DataField = 'emit_xNome'
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNotas."emit_xNome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 30.236220470000000000
          Height = 9.448818900000000000
          DataField = 'FatID'
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNotas."FatID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 634.961040000000000000
          Width = 355.275810240000000000
          Height = 9.448818900000000000
          DataField = 'NO_FATID'
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNotas."NO_FATID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 990.236860000000000000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DataField = 'FatNum'
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNotas."FatNum"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228348900000000000
        Top = 238.110390000000000000
        Width = 1046.929810000000000000
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data Fiscal')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd. Emit.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd. Dest.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 3.779529999999994000
          Width = 113.385890240000000000
          Height = 9.448818900000000000
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ emitente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 3.779529999999994000
          Width = 22.677165350000000000
          Height = 9.448818900000000000
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'mod')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Top = 3.779529999999994000
          Width = 26.456692910000000000
          Height = 9.448818900000000000
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Top = 3.779529999999994000
          Width = 60.472440940000000000
          Height = 9.448818900000000000
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'N'#186' NF')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 3.779529999999994000
          Width = 211.653543310000000000
          Height = 9.448818900000000000
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do emitente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 3.779529999999994000
          Width = 30.236220470000000000
          Height = 9.448818900000000000
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'FatID')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 634.961040000000000000
          Top = 3.779529999999994000
          Width = 355.275810240000000000
          Height = 9.448818900000000000
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o do FatID')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 990.236860000000000000
          Top = 3.779529999999994000
          Width = 56.692940240000000000
          Height = 9.448818900000000000
          DataSet = frxDsNotas
          DataSetName = 'frxDsNotas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'FatNum')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 309.921460000000000000
        Width = 1046.929810000000000000
      end
    end
  end
  object frxDslinha: TfrxDBDataset
    UserName = 'frxDsLinha'
    CloseDataSource = False
    DataSet = QrLinha
    BCDToCurrency = False
    DataSetOptions = []
    Left = 584
    Top = 76
  end
  object frxDsNotas: TfrxDBDataset
    UserName = 'frxDsNotas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'IDCtrl=IDCtrl'
      'LoteEnv=LoteEnv'
      'versao=versao'
      'Id=Id'
      'ide_cUF=ide_cUF'
      'ide_cNF=ide_cNF'
      'ide_natOp=ide_natOp'
      'ide_indPag=ide_indPag'
      'ide_mod=ide_mod'
      'ide_serie=ide_serie'
      'ide_nNF=ide_nNF'
      'ide_dEmi=ide_dEmi'
      'ide_dSaiEnt=ide_dSaiEnt'
      'ide_tpNF=ide_tpNF'
      'ide_cMunFG=ide_cMunFG'
      'ide_tpImp=ide_tpImp'
      'ide_tpEmis=ide_tpEmis'
      'ide_cDV=ide_cDV'
      'ide_tpAmb=ide_tpAmb'
      'ide_finNFe=ide_finNFe'
      'ide_procEmi=ide_procEmi'
      'ide_verProc=ide_verProc'
      'emit_CNPJ=emit_CNPJ'
      'emit_CPF=emit_CPF'
      'emit_xNome=emit_xNome'
      'emit_xFant=emit_xFant'
      'emit_xLgr=emit_xLgr'
      'emit_nro=emit_nro'
      'emit_xCpl=emit_xCpl'
      'emit_xBairro=emit_xBairro'
      'emit_cMun=emit_cMun'
      'emit_xMun=emit_xMun'
      'emit_UF=emit_UF'
      'emit_CEP=emit_CEP'
      'emit_cPais=emit_cPais'
      'emit_xPais=emit_xPais'
      'emit_fone=emit_fone'
      'emit_IE=emit_IE'
      'emit_IEST=emit_IEST'
      'emit_IM=emit_IM'
      'emit_CNAE=emit_CNAE'
      'dest_CNPJ=dest_CNPJ'
      'dest_CPF=dest_CPF'
      'dest_xNome=dest_xNome'
      'dest_xLgr=dest_xLgr'
      'dest_nro=dest_nro'
      'dest_xCpl=dest_xCpl'
      'dest_xBairro=dest_xBairro'
      'dest_cMun=dest_cMun'
      'dest_xMun=dest_xMun'
      'dest_UF=dest_UF'
      'dest_CEP=dest_CEP'
      'dest_cPais=dest_cPais'
      'dest_xPais=dest_xPais'
      'dest_fone=dest_fone'
      'dest_IE=dest_IE'
      'dest_ISUF=dest_ISUF'
      'ICMSTot_vBC=ICMSTot_vBC'
      'ICMSTot_vICMS=ICMSTot_vICMS'
      'ICMSTot_vBCST=ICMSTot_vBCST'
      'ICMSTot_vST=ICMSTot_vST'
      'ICMSTot_vProd=ICMSTot_vProd'
      'ICMSTot_vFrete=ICMSTot_vFrete'
      'ICMSTot_vSeg=ICMSTot_vSeg'
      'ICMSTot_vDesc=ICMSTot_vDesc'
      'ICMSTot_vII=ICMSTot_vII'
      'ICMSTot_vIPI=ICMSTot_vIPI'
      'ICMSTot_vPIS=ICMSTot_vPIS'
      'ICMSTot_vCOFINS=ICMSTot_vCOFINS'
      'ICMSTot_vOutro=ICMSTot_vOutro'
      'ICMSTot_vNF=ICMSTot_vNF'
      'ISSQNtot_vServ=ISSQNtot_vServ'
      'ISSQNtot_vBC=ISSQNtot_vBC'
      'ISSQNtot_vISS=ISSQNtot_vISS'
      'ISSQNtot_vPIS=ISSQNtot_vPIS'
      'ISSQNtot_vCOFINS=ISSQNtot_vCOFINS'
      'RetTrib_vRetPIS=RetTrib_vRetPIS'
      'RetTrib_vRetCOFINS=RetTrib_vRetCOFINS'
      'RetTrib_vRetCSLL=RetTrib_vRetCSLL'
      'RetTrib_vBCIRRF=RetTrib_vBCIRRF'
      'RetTrib_vIRRF=RetTrib_vIRRF'
      'RetTrib_vBCRetPrev=RetTrib_vBCRetPrev'
      'RetTrib_vRetPrev=RetTrib_vRetPrev'
      'ModFrete=ModFrete'
      'Transporta_CNPJ=Transporta_CNPJ'
      'Transporta_CPF=Transporta_CPF'
      'Transporta_XNome=Transporta_XNome'
      'Transporta_IE=Transporta_IE'
      'Transporta_XEnder=Transporta_XEnder'
      'Transporta_XMun=Transporta_XMun'
      'Transporta_UF=Transporta_UF'
      'RetTransp_vServ=RetTransp_vServ'
      'RetTransp_vBCRet=RetTransp_vBCRet'
      'RetTransp_PICMSRet=RetTransp_PICMSRet'
      'RetTransp_vICMSRet=RetTransp_vICMSRet'
      'RetTransp_CFOP=RetTransp_CFOP'
      'RetTransp_CMunFG=RetTransp_CMunFG'
      'VeicTransp_Placa=VeicTransp_Placa'
      'VeicTransp_UF=VeicTransp_UF'
      'VeicTransp_RNTC=VeicTransp_RNTC'
      'Cobr_Fat_nFat=Cobr_Fat_nFat'
      'Cobr_Fat_vOrig=Cobr_Fat_vOrig'
      'Cobr_Fat_vDesc=Cobr_Fat_vDesc'
      'Cobr_Fat_vLiq=Cobr_Fat_vLiq'
      'InfAdic_InfAdFisco=InfAdic_InfAdFisco'
      'InfAdic_InfCpl=InfAdic_InfCpl'
      'Exporta_UFEmbarq=Exporta_UFEmbarq'
      'Exporta_XLocEmbarq=Exporta_XLocEmbarq'
      'Compra_XNEmp=Compra_XNEmp'
      'Compra_XPed=Compra_XPed'
      'Compra_XCont=Compra_XCont'
      'Status=Status'
      'infProt_Id=infProt_Id'
      'infProt_tpAmb=infProt_tpAmb'
      'infProt_verAplic=infProt_verAplic'
      'infProt_dhRecbto=infProt_dhRecbto'
      'infProt_nProt=infProt_nProt'
      'infProt_digVal=infProt_digVal'
      'infProt_cStat=infProt_cStat'
      'infProt_xMotivo=infProt_xMotivo'
      'infCanc_Id=infCanc_Id'
      'infCanc_tpAmb=infCanc_tpAmb'
      'infCanc_verAplic=infCanc_verAplic'
      'infCanc_dhRecbto=infCanc_dhRecbto'
      'infCanc_nProt=infCanc_nProt'
      'infCanc_digVal=infCanc_digVal'
      'infCanc_cStat=infCanc_cStat'
      'infCanc_xMotivo=infCanc_xMotivo'
      'infCanc_cJust=infCanc_cJust'
      'infCanc_xJust=infCanc_xJust'
      '_Ativo_=_Ativo_'
      'FisRegCad=FisRegCad'
      'CartEmiss=CartEmiss'
      'TabelaPrc=TabelaPrc'
      'CondicaoPg=CondicaoPg'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'FreteExtra=FreteExtra'
      'SegurExtra=SegurExtra'
      'ICMSRec_pRedBC=ICMSRec_pRedBC'
      'ICMSRec_vBC=ICMSRec_vBC'
      'ICMSRec_pAliq=ICMSRec_pAliq'
      'ICMSRec_vICMS=ICMSRec_vICMS'
      'IPIRec_pRedBC=IPIRec_pRedBC'
      'IPIRec_vBC=IPIRec_vBC'
      'IPIRec_pAliq=IPIRec_pAliq'
      'IPIRec_vIPI=IPIRec_vIPI'
      'PISRec_pRedBC=PISRec_pRedBC'
      'PISRec_vBC=PISRec_vBC'
      'PISRec_pAliq=PISRec_pAliq'
      'PISRec_vPIS=PISRec_vPIS'
      'COFINSRec_pRedBC=COFINSRec_pRedBC'
      'COFINSRec_vBC=COFINSRec_vBC'
      'COFINSRec_pAliq=COFINSRec_pAliq'
      'COFINSRec_vCOFINS=COFINSRec_vCOFINS'
      'DataFiscal=DataFiscal'
      'protNFe_versao=protNFe_versao'
      'retCancNFe_versao=retCancNFe_versao'
      'SINTEGRA_ExpDeclNum=SINTEGRA_ExpDeclNum'
      'SINTEGRA_ExpDeclDta=SINTEGRA_ExpDeclDta'
      'SINTEGRA_ExpNat=SINTEGRA_ExpNat'
      'SINTEGRA_ExpRegNum=SINTEGRA_ExpRegNum'
      'SINTEGRA_ExpRegDta=SINTEGRA_ExpRegDta'
      'SINTEGRA_ExpConhNum=SINTEGRA_ExpConhNum'
      'SINTEGRA_ExpConhDta=SINTEGRA_ExpConhDta'
      'SINTEGRA_ExpConhTip=SINTEGRA_ExpConhTip'
      'SINTEGRA_ExpPais=SINTEGRA_ExpPais'
      'SINTEGRA_ExpAverDta=SINTEGRA_ExpAverDta'
      'CodInfoEmit=CodInfoEmit'
      'CodInfoDest=CodInfoDest'
      'CriAForca=CriAForca'
      'ide_hSaiEnt=ide_hSaiEnt'
      'ide_dhCont=ide_dhCont'
      'ide_xJust=ide_xJust'
      'emit_CRT=emit_CRT'
      'dest_email=dest_email'
      'Vagao=Vagao'
      'Balsa=Balsa'
      'CodInfoTrsp=CodInfoTrsp'
      'OrdemServ=OrdemServ'
      'Situacao=Situacao'
      'Antigo=Antigo'
      'NFG_Serie=NFG_Serie'
      'NF_ICMSAlq=NF_ICMSAlq'
      'NF_CFOP=NF_CFOP'
      'Importado=Importado'
      'NFG_SubSerie=NFG_SubSerie'
      'NFG_ValIsen=NFG_ValIsen'
      'NFG_NaoTrib=NFG_NaoTrib'
      'NFG_Outros=NFG_Outros'
      'NO_FATID=NO_FATID')
    DataSet = QrNotas
    BCDToCurrency = False
    DataSetOptions = []
    Left = 584
    Top = 104
  end
  object frxDsSpedEfdIcmsIpiErrs: TfrxDBDataset
    UserName = 'frxDsSpedEfdIcmsIpiErrs'
    CloseDataSource = False
    DataSet = QrSpedEfdIcmsIpiErrs
    BCDToCurrency = False
    DataSetOptions = []
    Left = 60
    Top = 4
  end
  object QrC100: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa '
      'FROM spedefdc100'
      'WHERE ImporExpor=1'
      'AND AnoMes=201001'
      'AND Empresa=-11'
      'AND LinArq=795')
    Left = 208
    Top = 172
    object QrC100FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrC100FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrC100Empresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
end
