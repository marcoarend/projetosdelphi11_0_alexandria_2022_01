object FmSPED_EFD_Compara: TFmSPED_EFD_Compara
  Left = 339
  Top = 185
  Caption = 'SPE-D_EFD-004 :: Compara Importa'#231#227'o SPED-EFD com Lan'#231'amentos'
  ClientHeight = 764
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Compara Importa'#231#227'o SPED-EFD com Lan'#231'amentos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 716
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 33
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 12
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label2: TLabel
        Left = 140
        Top = 12
        Width = 154
        Height = 13
        Caption = 'Ano / m'#234's no formato AAAAMM:'
      end
      object SpeedButton2: TSpeedButton
        Left = 346
        Top = 8
        Width = 21
        Height = 21
        Caption = '>'
        OnClick = SpeedButton2Click
      end
      object EdEntidade: TdmkEdit
        Left = 64
        Top = 8
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '-11'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = -11
        ValWarn = False
        OnChange = EdEntidadeChange
      end
      object EdAnoMes: TdmkEdit
        Left = 296
        Top = 8
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '201001'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 201001
        ValWarn = False
        OnChange = EdAnoMesChange
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 73
      Width = 1006
      Height = 642
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' C100 '
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 998
          Height = 614
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object PageControl2: TPageControl
            Left = 0
            Top = 0
            Width = 998
            Height = 614
            ActivePage = TabSheet2
            Align = alClient
            TabOrder = 0
            object TabSheet2: TTabSheet
              Caption = 'Notas Fiscais de mercadorias (compra e venda)'
              object Panel21: TPanel
                Left = 0
                Top = 0
                Width = 493
                Height = 586
                Align = alLeft
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label14: TLabel
                  Left = 0
                  Top = 0
                  Width = 493
                  Height = 13
                  Align = alTop
                  Alignment = taCenter
                  Caption = 'Notas Fiscais'
                  ExplicitWidth = 63
                end
                object Label15: TLabel
                  Left = 0
                  Top = 276
                  Width = 493
                  Height = 13
                  Align = alTop
                  Alignment = taCenter
                  Caption = 'Itens da Nota Fiscal Selecionada'
                  ExplicitWidth = 156
                end
                object Splitter5: TSplitter
                  Left = 0
                  Top = 271
                  Width = 493
                  Height = 5
                  Cursor = crVSplit
                  Align = alTop
                  ExplicitTop = 284
                end
                object DBG_C100: TdmkDBGrid
                  Left = 0
                  Top = 13
                  Width = 493
                  Height = 258
                  Align = alTop
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'DT_DOC'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'E_S'
                      Title.Caption = 'e/s'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'P_T'
                      Title.Caption = 'p/t'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Entidade'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_MOD'
                      Title.Caption = 'MOD'
                      Width = 32
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SER'
                      Width = 32
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NUM_DOC'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_DOC'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_MERC'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_SIT'
                      Title.Caption = 'Sit'
                      Width = 20
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ACHOU_TXT'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      Title.Caption = 'Achou'
                      Width = 36
                      Visible = True
                    end>
                  Color = clWindow
                  DataSource = DsC100
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDrawColumnCell = DBG_C100DrawColumnCell
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'DT_DOC'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'E_S'
                      Title.Caption = 'e/s'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'P_T'
                      Title.Caption = 'p/t'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Entidade'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_MOD'
                      Title.Caption = 'MOD'
                      Width = 32
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SER'
                      Width = 32
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NUM_DOC'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_DOC'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_MERC'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_SIT'
                      Title.Caption = 'Sit'
                      Width = 20
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ACHOU_TXT'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      Title.Caption = 'Achou'
                      Width = 36
                      Visible = True
                    end>
                end
                object Panel22: TPanel
                  Left = 0
                  Top = 538
                  Width = 493
                  Height = 48
                  Align = alBottom
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label13: TLabel
                    Left = 168
                    Top = 8
                    Width = 42
                    Height = 13
                    Caption = 'Terceiro:'
                    FocusControl = DBEdit3
                  end
                  object DBEdit3: TDBEdit
                    Left = 168
                    Top = 24
                    Width = 321
                    Height = 21
                    DataField = 'NO_ENT'
                    DataSource = DsC100
                    TabOrder = 0
                  end
                  object BtSeqAcoesC100: TBitBtn
                    Tag = 3
                    Left = 4
                    Top = 6
                    Width = 160
                    Height = 40
                    Caption = 'Sequ'#234'ncia de a'#231#245'es'
                    Enabled = False
                    NumGlyphs = 2
                    TabOrder = 1
                    OnClick = BtSeqAcoesC100Click
                  end
                end
                object DBG_C170: TDBGrid
                  Left = 0
                  Top = 289
                  Width = 493
                  Height = 249
                  Align = alClient
                  DataSource = DsC170
                  TabOrder = 2
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NUM_ITEM'
                      Title.Caption = 'Item'
                      Width = 24
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'COD_ITEM'
                      Title.Caption = 'C'#243'd. Item'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DESCR_COMPL'
                      Title.Caption = 'Descri'#231#227'o complementar'
                      Width = 124
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QTD'
                      Title.Caption = 'Quantidade'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'UNID'
                      Title.Caption = 'Unid.'
                      Width = 28
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_ITEM'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'IND_MOV'
                      Title.Caption = 'M'
                      Width = 14
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CFOP'
                      Width = 32
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ALIQ_ICMS'
                      Title.Caption = '% ICMS'
                      Width = 32
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_BC_ICMS'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VL_ICMS'
                      Width = 60
                      Visible = True
                    end>
                end
              end
              object PageControl3: TPageControl
                Left = 493
                Top = 0
                Width = 497
                Height = 586
                ActivePage = TabSheet3
                Align = alClient
                TabOrder = 1
                object TabSheet3: TTabSheet
                  Caption = 'TabSheet3'
                end
                object TabSheet4: TTabSheet
                  Caption = ' Rastreamento (Deprecado?)'
                  ImageIndex = 1
                  object PageControl7: TPageControl
                    Left = 0
                    Top = 0
                    Width = 489
                    Height = 558
                    ActivePage = TabSheet11
                    Align = alClient
                    TabOrder = 0
                    object TabSheet11: TTabSheet
                      Caption = 'Uso e Consumo'
                      object Panel4: TPanel
                        Left = 0
                        Top = 0
                        Width = 481
                        Height = 81
                        Align = alTop
                        ParentBackground = False
                        TabOrder = 0
                        object GroupBox1: TGroupBox
                          Left = 8
                          Top = 4
                          Width = 205
                          Height = 69
                          Caption = '                  '
                          TabOrder = 1
                          object CkFornecedor_0_0: TCheckBox
                            Left = 8
                            Top = 24
                            Width = 90
                            Height = 17
                            Caption = 'Fornecedor'
                            Checked = True
                            State = cbChecked
                            TabOrder = 0
                            OnClick = CkFiltra_0_0Click
                          end
                          object CkNF_0_0: TCheckBox
                            Left = 8
                            Top = 44
                            Width = 90
                            Height = 17
                            Caption = 'Nota Fiscal'
                            Checked = True
                            State = cbChecked
                            TabOrder = 1
                            OnClick = CkFiltra_0_0Click
                          end
                          object CkValorDoc_0_0: TCheckBox
                            Left = 104
                            Top = 24
                            Width = 90
                            Height = 17
                            Caption = 'Valor Total NF'
                            Checked = True
                            State = cbChecked
                            TabOrder = 2
                            OnClick = CkFiltra_0_0Click
                          end
                          object CkValorMerc_0_0: TCheckBox
                            Left = 104
                            Top = 44
                            Width = 90
                            Height = 17
                            Caption = 'Valor Produtos'
                            Checked = True
                            State = cbChecked
                            TabOrder = 3
                            OnClick = CkFiltra_0_0Click
                          end
                        end
                        object CkFiltra_0_0: TCheckBox
                          Left = 24
                          Top = 4
                          Width = 45
                          Height = 17
                          Caption = 'Filtra:'
                          Checked = True
                          State = cbChecked
                          TabOrder = 0
                          OnClick = CkFiltra_0_0Click
                        end
                        object BtExclui_0_0: TBitBtn
                          Left = 312
                          Top = 20
                          Width = 90
                          Height = 40
                          Caption = '&Exclui Entrada'
                          Enabled = False
                          NumGlyphs = 2
                          TabOrder = 2
                          OnClick = BtExclui_0_0Click
                        end
                        object BtEstatistica_0_0: TBitBtn
                          Left = 220
                          Top = 20
                          Width = 90
                          Height = 40
                          Caption = 'Es&tatistica'
                          Enabled = False
                          NumGlyphs = 2
                          TabOrder = 3
                          OnClick = BtEstatistica_0_0Click
                        end
                      end
                      object Pn_0_0: TPanel
                        Left = 0
                        Top = 482
                        Width = 481
                        Height = 48
                        Align = alBottom
                        ParentBackground = False
                        TabOrder = 1
                        object LaAviso_0_0: TLabel
                          Left = 96
                          Top = 4
                          Width = 41
                          Height = 13
                          Caption = '..........'
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clRed
                          Font.Height = -11
                          Font.Name = 'MS Sans Serif'
                          Font.Style = [fsBold]
                          ParentFont = False
                        end
                        object PB_0_0: TProgressBar
                          Left = 96
                          Top = 20
                          Width = 304
                          Height = 17
                          TabOrder = 0
                        end
                        object BtRastreia_0_0: TBitBtn
                          Left = 4
                          Top = 4
                          Width = 90
                          Height = 40
                          Caption = '&Rastreia'
                          Enabled = False
                          NumGlyphs = 2
                          TabOrder = 1
                          OnClick = BtRastreia_0_0Click
                        end
                        object Panel5: TPanel
                          Left = 384
                          Top = 1
                          Width = 96
                          Height = 46
                          Align = alRight
                          BevelOuter = bvNone
                          TabOrder = 2
                          object BtSaida_0_0: TBitBtn
                            Tag = 13
                            Left = 2
                            Top = 3
                            Width = 90
                            Height = 40
                            Cursor = crHandPoint
                            Caption = '&Desiste'
                            NumGlyphs = 2
                            ParentShowHint = False
                            ShowHint = True
                            TabOrder = 0
                            OnClick = BtSaida_0_0Click
                          end
                        end
                      end
                      object Panel12: TPanel
                        Left = 0
                        Top = 81
                        Width = 481
                        Height = 401
                        Align = alClient
                        ParentBackground = False
                        TabOrder = 2
                        object Label8: TLabel
                          Left = 1
                          Top = 1
                          Width = 377
                          Height = 16
                          Align = alTop
                          Alignment = taCenter
                          Caption = 'Duplo clique na linha localiza a entrada de Uso e Consumo'
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clRed
                          Font.Height = -13
                          Font.Name = 'Arial'
                          Font.Style = [fsBold]
                          ParentFont = False
                        end
                        object DBGrid2: TDBGrid
                          Left = 1
                          Top = 17
                          Width = 479
                          Height = 383
                          Align = alClient
                          DataSource = DsPQE
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -11
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          OnDblClick = DBGrid2DblClick
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'Data'
                              Title.Caption = 'Emiss'#227'o'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'IQ'
                              Title.Caption = 'Terceiro'
                              Width = 44
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_ENT'
                              Title.Caption = 'Nome do Cliente / Fornecedor'
                              Width = 161
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'modNF'
                              Title.Caption = 'Mod'
                              Width = 32
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Serie'
                              Title.Caption = 'S'#233'rie'
                              Width = 32
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NF'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'ValorNF'
                              Title.Caption = 'Valor NF'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Codigo'
                              Title.Caption = 'C'#243'digo'
                              Visible = True
                            end>
                        end
                      end
                    end
                    object TabSheet12: TTabSheet
                      Caption = 'Mat'#233'ria-prima'
                      ImageIndex = 1
                      object Splitter1: TSplitter
                        Left = 0
                        Top = 205
                        Width = 481
                        Height = 5
                        Cursor = crVSplit
                        Align = alTop
                        ExplicitWidth = 497
                      end
                      object Panel6: TPanel
                        Left = 0
                        Top = 0
                        Width = 481
                        Height = 81
                        Align = alTop
                        ParentBackground = False
                        TabOrder = 0
                        object Label11: TLabel
                          Left = 220
                          Top = 20
                          Width = 53
                          Height = 13
                          Caption = 'Dias antes:'
                        end
                        object Label12: TLabel
                          Left = 220
                          Top = 48
                          Width = 58
                          Height = 13
                          Caption = 'Dias depois:'
                        end
                        object GroupBox2: TGroupBox
                          Left = 8
                          Top = 4
                          Width = 205
                          Height = 69
                          Caption = '                  '
                          TabOrder = 1
                          object CkFornecedor_0_1: TCheckBox
                            Left = 8
                            Top = 24
                            Width = 90
                            Height = 17
                            Caption = 'Fornecedor'
                            Checked = True
                            State = cbChecked
                            TabOrder = 0
                            OnClick = CkFiltra_0_1Click
                          end
                          object CkNF_0_1: TCheckBox
                            Left = 8
                            Top = 44
                            Width = 90
                            Height = 17
                            Caption = 'Nota Fiscal'
                            Checked = True
                            State = cbChecked
                            TabOrder = 1
                            OnClick = CkFiltra_0_1Click
                          end
                          object CkValorDoc_0_1: TCheckBox
                            Left = 104
                            Top = 24
                            Width = 90
                            Height = 17
                            Caption = 'Valor MP'
                            Checked = True
                            State = cbChecked
                            TabOrder = 2
                            OnClick = CkFiltra_0_1Click
                          end
                          object CkDiasAD_0_1: TCheckBox
                            Left = 104
                            Top = 44
                            Width = 90
                            Height = 17
                            Caption = 'Dias (ao lado)'
                            Checked = True
                            State = cbChecked
                            TabOrder = 3
                            OnClick = CkFiltra_0_1Click
                          end
                        end
                        object CkFiltra_0_1: TCheckBox
                          Left = 24
                          Top = 4
                          Width = 45
                          Height = 17
                          Caption = 'Filtra:'
                          Checked = True
                          State = cbChecked
                          TabOrder = 0
                          OnClick = CkFiltra_0_1Click
                        end
                        object BtExclui_0_1: TBitBtn
                          Left = 400
                          Top = 40
                          Width = 90
                          Height = 40
                          Caption = '&Exclui Entrada'
                          Enabled = False
                          NumGlyphs = 2
                          TabOrder = 5
                          OnClick = BtExclui_0_0Click
                        end
                        object BtEstatistica_0_1: TBitBtn
                          Left = 400
                          Top = 1
                          Width = 90
                          Height = 40
                          Caption = 'Es&tatistica'
                          Enabled = False
                          NumGlyphs = 2
                          TabOrder = 4
                          OnClick = BtEstatistica_0_1Click
                        end
                        object EdDiasA_0_1: TdmkEdit
                          Left = 280
                          Top = 16
                          Width = 36
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 2
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '9999'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 9999
                          ValWarn = False
                          OnChange = CkFiltra_0_1Click
                        end
                        object EdDiasD_0_1: TdmkEdit
                          Left = 280
                          Top = 44
                          Width = 36
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 3
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '9999'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 9999
                          ValWarn = False
                          OnChange = CkFiltra_0_1Click
                        end
                      end
                      object DBGrid3: TDBGrid
                        Left = 0
                        Top = 81
                        Width = 481
                        Height = 124
                        Align = alTop
                        DataSource = DsNFsMPs
                        TabOrder = 1
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -11
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        OnDblClick = DBGrid3DblClick
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'Emissao'
                            Title.Caption = 'Emiss'#227'o'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'MyForn'
                            Title.Caption = 'Terceiro'
                            Width = 44
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_FRN'
                            Title.Caption = 'Nome do Cliente / Fornecedor'
                            Width = 161
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NF_Modelo'
                            Title.Caption = 'Mod'
                            Width = 32
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NF_Serie'
                            Title.Caption = 'S'#233'rie'
                            Width = 32
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NF'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'CMPValor'
                            Title.Caption = 'Valor NF'
                            Visible = True
                          end>
                      end
                      object Panel8: TPanel
                        Left = 0
                        Top = 482
                        Width = 481
                        Height = 48
                        Align = alBottom
                        ParentBackground = False
                        TabOrder = 2
                        object Label4: TLabel
                          Left = 96
                          Top = 4
                          Width = 41
                          Height = 13
                          Caption = '..........'
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clRed
                          Font.Height = -11
                          Font.Name = 'MS Sans Serif'
                          Font.Style = [fsBold]
                          ParentFont = False
                        end
                        object ProgressBar2: TProgressBar
                          Left = 96
                          Top = 20
                          Width = 304
                          Height = 17
                          TabOrder = 0
                        end
                        object BtRastreia_0_1: TBitBtn
                          Left = 4
                          Top = 4
                          Width = 90
                          Height = 40
                          Caption = '&Rastreia'
                          Enabled = False
                          NumGlyphs = 2
                          TabOrder = 1
                          OnClick = BtRastreia_0_1Click
                        end
                        object Panel9: TPanel
                          Left = 384
                          Top = 1
                          Width = 96
                          Height = 46
                          Align = alRight
                          BevelOuter = bvNone
                          TabOrder = 2
                          object BitBtn4: TBitBtn
                            Tag = 13
                            Left = 2
                            Top = 3
                            Width = 90
                            Height = 40
                            Cursor = crHandPoint
                            Caption = '&Desiste'
                            NumGlyphs = 2
                            ParentShowHint = False
                            ShowHint = True
                            TabOrder = 0
                            OnClick = BtSaida_0_0Click
                          end
                        end
                      end
                      object Panel10: TPanel
                        Left = 0
                        Top = 435
                        Width = 481
                        Height = 47
                        Align = alBottom
                        ParentBackground = False
                        TabOrder = 3
                        object Label5: TLabel
                          Left = 8
                          Top = 4
                          Width = 50
                          Height = 13
                          Caption = 'Pe'#231'as NF:'
                          FocusControl = DBEdit1
                        end
                        object Label6: TLabel
                          Left = 128
                          Top = 4
                          Width = 44
                          Height = 13
                          Caption = 'Peso NF:'
                          FocusControl = DBEdit2
                        end
                        object Label9: TLabel
                          Left = 248
                          Top = 4
                          Width = 62
                          Height = 13
                          Caption = 'Peso l'#237'quido:'
                          FocusControl = DBEdit1
                        end
                        object Label10: TLabel
                          Left = 368
                          Top = 4
                          Width = 30
                          Height = 13
                          Caption = 'Custo:'
                          FocusControl = DBEdit2
                        end
                        object DBEdit1: TDBEdit
                          Left = 8
                          Top = 20
                          Width = 116
                          Height = 21
                          DataField = 'PecasNF'
                          DataSource = DsSumMPI
                          TabOrder = 0
                        end
                        object DBEdit2: TDBEdit
                          Left = 128
                          Top = 20
                          Width = 116
                          Height = 21
                          DataField = 'PNF'
                          DataSource = DsSumMPI
                          TabOrder = 1
                        end
                        object DBEdit5: TDBEdit
                          Left = 248
                          Top = 20
                          Width = 116
                          Height = 21
                          DataField = 'PLE'
                          DataSource = DsSumMPI
                          TabOrder = 2
                        end
                        object DBEdit6: TDBEdit
                          Left = 368
                          Top = 20
                          Width = 120
                          Height = 21
                          DataField = 'CMPValor'
                          DataSource = DsSumMPI
                          TabOrder = 3
                        end
                      end
                      object Panel11: TPanel
                        Left = 0
                        Top = 210
                        Width = 481
                        Height = 225
                        Align = alClient
                        ParentBackground = False
                        TabOrder = 4
                        object Label7: TLabel
                          Left = 1
                          Top = 1
                          Width = 368
                          Height = 16
                          Align = alTop
                          Alignment = taCenter
                          Caption = 'Duplo clique na linha localiza a entrada de Mat'#233'ria-prima'
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clRed
                          Font.Height = -13
                          Font.Name = 'Arial'
                          Font.Style = [fsBold]
                          ParentFont = False
                        end
                        object DBGrid4: TDBGrid
                          Left = 1
                          Top = 17
                          Width = 479
                          Height = 207
                          Align = alClient
                          DataSource = DsMPInIts
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -11
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          OnDblClick = DBGrid3DblClick
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'Emissao'
                              Title.Caption = 'Emiss'#227'o'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'MyForn'
                              Title.Caption = 'Terceiro'
                              Width = 44
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_FRN'
                              Title.Caption = 'Nome do Cliente / Fornecedor'
                              Width = 161
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NF_Modelo'
                              Title.Caption = 'Mod'
                              Width = 32
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NF_Serie'
                              Title.Caption = 'S'#233'rie'
                              Width = 32
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NF'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'CMPValor'
                              Title.Caption = 'Valor NF'
                              Visible = True
                            end>
                        end
                      end
                    end
                  end
                end
              end
            end
            object TabSheet5: TTabSheet
              Caption = ' Erros e Avisos '
              ImageIndex = 1
              object MeErros: TMemo
                Left = 0
                Top = 0
                Width = 990
                Height = 538
                Align = alClient
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'Courier New'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                WordWrap = False
              end
              object Panel7: TPanel
                Left = 0
                Top = 538
                Width = 990
                Height = 48
                Align = alBottom
                ParentBackground = False
                TabOrder = 1
                object Label3: TLabel
                  Left = 120
                  Top = 4
                  Width = 41
                  Height = 13
                  Caption = '..........'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object ProgressBar1: TProgressBar
                  Left = 120
                  Top = 20
                  Width = 757
                  Height = 17
                  TabOrder = 0
                end
                object BitBtn1: TBitBtn
                  Left = 8
                  Top = 4
                  Width = 90
                  Height = 40
                  Caption = '&Rastreia'
                  Enabled = False
                  NumGlyphs = 2
                  TabOrder = 1
                  OnClick = BtRastreia_0_0Click
                end
                object Panel13: TPanel
                  Left = 878
                  Top = 1
                  Width = 111
                  Height = 46
                  Align = alRight
                  BevelOuter = bvNone
                  TabOrder = 2
                  object BitBtn2: TBitBtn
                    Tag = 13
                    Left = 2
                    Top = 3
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Desiste'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtSaida_0_0Click
                  end
                end
              end
            end
          end
        end
      end
      object C500: TTabSheet
        Caption = ' C500 '
        ImageIndex = 1
        object PageControl4: TPageControl
          Left = 0
          Top = 0
          Width = 998
          Height = 614
          ActivePage = TabSheet6
          Align = alClient
          TabOrder = 0
          object TabSheet6: TTabSheet
            Caption = ' Notas Fiscais de Energia El'#233'trica, '#193'gua e G'#225's'
            object Splitter2: TSplitter
              Left = 497
              Top = 0
              Height = 586
              ExplicitLeft = 916
              ExplicitTop = 296
              ExplicitHeight = 100
            end
            object Panel14: TPanel
              Left = 0
              Top = 0
              Width = 497
              Height = 586
              Align = alLeft
              ParentBackground = False
              TabOrder = 0
              object DBGrid5: TDBGrid
                Left = 1
                Top = 1
                Width = 495
                Height = 536
                Align = alClient
                DataSource = DsC500
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'DT_DOC'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Entidade'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_ENT'
                    Width = 94
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'COD_MOD'
                    Title.Caption = 'MOD'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SER'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NUM_DOC'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VL_DOC'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VL_FORN'
                    Visible = True
                  end>
              end
              object Panel15: TPanel
                Left = 1
                Top = 537
                Width = 495
                Height = 48
                Align = alBottom
                TabOrder = 1
                object BtSeqAcoesC500: TBitBtn
                  Tag = 3
                  Left = 16
                  Top = 2
                  Width = 160
                  Height = 40
                  Caption = 'Sequ'#234'ncia de a'#231#245'es'
                  Enabled = False
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtSeqAcoesC500Click
                end
              end
            end
          end
        end
      end
      object TabSheet7: TTabSheet
        Caption = ' D100 '
        ImageIndex = 2
        object PageControl6: TPageControl
          Left = 0
          Top = 0
          Width = 998
          Height = 614
          ActivePage = TabSheet10
          Align = alClient
          TabOrder = 0
          object TabSheet10: TTabSheet
            Caption = 'Conhecimentos de Frete'
            object Splitter4: TSplitter
              Left = 497
              Top = 0
              Height = 586
              ExplicitLeft = 916
              ExplicitTop = 296
              ExplicitHeight = 100
            end
            object Panel19: TPanel
              Left = 0
              Top = 0
              Width = 497
              Height = 586
              Align = alLeft
              ParentBackground = False
              TabOrder = 0
              object DBGrid7: TDBGrid
                Left = 1
                Top = 1
                Width = 495
                Height = 536
                Align = alClient
                DataSource = DsD100
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'DT_DOC'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Entidade'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_ENT'
                    Width = 94
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'COD_MOD'
                    Title.Caption = 'MOD'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SER'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NUM_DOC'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VL_DOC'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VL_SERV'
                    Visible = True
                  end>
              end
              object Panel20: TPanel
                Left = 1
                Top = 537
                Width = 495
                Height = 48
                Align = alBottom
                TabOrder = 1
                object BtSeqAcoesD100: TBitBtn
                  Tag = 3
                  Left = 16
                  Top = 2
                  Width = 160
                  Height = 40
                  Caption = 'Sequ'#234'ncia de a'#231#245'es'
                  Enabled = False
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtSeqAcoesD100Click
                end
              end
            end
          end
        end
      end
      object TabSheet9: TTabSheet
        Caption = ' D500 '
        ImageIndex = 3
        object PageControl5: TPageControl
          Left = 0
          Top = 0
          Width = 998
          Height = 614
          ActivePage = TabSheet8
          Align = alClient
          TabOrder = 0
          object TabSheet8: TTabSheet
            Caption = ' Notas Fiscais de Comunica'#231#227'o e Telecomunica'#231#227'o'
            object Splitter3: TSplitter
              Left = 497
              Top = 0
              Height = 586
              ExplicitLeft = 916
              ExplicitTop = 296
              ExplicitHeight = 100
            end
            object Panel17: TPanel
              Left = 0
              Top = 0
              Width = 497
              Height = 586
              Align = alLeft
              ParentBackground = False
              TabOrder = 0
              object DBGrid6: TDBGrid
                Left = 1
                Top = 1
                Width = 495
                Height = 536
                Align = alClient
                DataSource = DsD500
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'DT_DOC'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Entidade'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_ENT'
                    Width = 94
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'COD_MOD'
                    Title.Caption = 'MOD'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SER'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NUM_DOC'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VL_DOC'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VL_SERV'
                    Visible = True
                  end>
              end
              object Panel18: TPanel
                Left = 1
                Top = 537
                Width = 495
                Height = 48
                Align = alBottom
                TabOrder = 1
                object BtSeqAcoesD500: TBitBtn
                  Tag = 3
                  Left = 16
                  Top = 2
                  Width = 160
                  Height = 40
                  Caption = 'Sequ'#234'ncia de a'#231#245'es'
                  Enabled = False
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtSeqAcoesD500Click
                end
              end
            end
          end
        end
      end
    end
    object Panel16: TPanel
      Left = 1
      Top = 34
      Width = 1006
      Height = 39
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 1006
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object QrC100: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrC100AfterOpen
    BeforeClose = QrC100BeforeClose
    AfterScroll = QrC100AfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      '_0150.Entidade, IF(c100.IND_OPER=0, "E", "S") E_S, '
      'IF(c100.IND_EMIT=0, "P", "T") P_T,'
      'IF(FatNum <> 0, "SIM", "N'#195'O") ACHOU_TXT, c100.*'
      'FROM spedefdc100 c100'
      'LEFT JOIN spedefd0150 _0150 ON _0150.COD_PART=c100.COD_PART'
      '  AND _0150.ImporExpor=c100.ImporExpor'
      '  AND _0150.AnoMes=c100.AnoMes'
      '  AND _0150.Empresa=c100.Empresa'
      'LEFT JOIN entidades ent ON ent.Codigo=_0150.Entidade'
      'WHERE c100.IND_EMIT=1'
      'AND ParTipo=0 AND ParCodi=0'
      'AND c100.ImporExpor=:P0'
      'AND c100.AnoMes=:P1'
      'AND c100.Empresa=:P2')
    Left = 20
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrC100NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrC100Entidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'spedefd0150.Entidade'
    end
    object QrC100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
      Origin = 'spedefdc100.ImporExpor'
    end
    object QrC100AnoMes: TIntegerField
      FieldName = 'AnoMes'
      Origin = 'spedefdc100.AnoMes'
    end
    object QrC100Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'spedefdc100.Empresa'
    end
    object QrC100LinArq: TIntegerField
      FieldName = 'LinArq'
      Origin = 'spedefdc100.LinArq'
    end
    object QrC100REG: TWideStringField
      FieldName = 'REG'
      Origin = 'spedefdc100.REG'
      Size = 4
    end
    object QrC100IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Origin = 'spedefdc100.IND_OPER'
      Size = 1
    end
    object QrC100IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Origin = 'spedefdc100.IND_EMIT'
      Size = 1
    end
    object QrC100COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Origin = 'spedefdc100.COD_PART'
      Size = 60
    end
    object QrC100COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Origin = 'spedefdc100.COD_MOD'
      Size = 2
    end
    object QrC100COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Origin = 'spedefdc100.COD_SIT'
      Size = 2
    end
    object QrC100SER: TWideStringField
      FieldName = 'SER'
      Origin = 'spedefdc100.SER'
      Size = 3
    end
    object QrC100NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Origin = 'spedefdc100.NUM_DOC'
    end
    object QrC100CHV_NFE: TWideStringField
      FieldName = 'CHV_NFE'
      Origin = 'spedefdc100.CHV_NFE'
      Size = 44
    end
    object QrC100DT_DOC: TDateField
      FieldName = 'DT_DOC'
      Origin = 'spedefdc100.DT_DOC'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrC100DT_E_S: TDateField
      FieldName = 'DT_E_S'
      Origin = 'spedefdc100.DT_E_S'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrC100VL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Origin = 'spedefdc100.VL_DOC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100IND_PGTO: TWideStringField
      FieldName = 'IND_PGTO'
      Origin = 'spedefdc100.IND_PGTO'
      Size = 1
    end
    object QrC100VL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Origin = 'spedefdc100.VL_DESC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100VL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Origin = 'spedefdc100.VL_ABAT_NT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100VL_MERC: TFloatField
      FieldName = 'VL_MERC'
      Origin = 'spedefdc100.VL_MERC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100IND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Origin = 'spedefdc100.IND_FRT'
      Size = 1
    end
    object QrC100VL_FRT: TFloatField
      FieldName = 'VL_FRT'
      Origin = 'spedefdc100.VL_FRT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100VL_SEG: TFloatField
      FieldName = 'VL_SEG'
      Origin = 'spedefdc100.VL_SEG'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100VL_OUT_DA: TFloatField
      FieldName = 'VL_OUT_DA'
      Origin = 'spedefdc100.VL_OUT_DA'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Origin = 'spedefdc100.VL_BC_ICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Origin = 'spedefdc100.VL_ICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Origin = 'spedefdc100.VL_BC_ICMS_ST'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Origin = 'spedefdc100.VL_ICMS_ST'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100VL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Origin = 'spedefdc100.VL_IPI'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100VL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Origin = 'spedefdc100.VL_PIS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Origin = 'spedefdc100.VL_COFINS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100VL_PIS_ST: TFloatField
      FieldName = 'VL_PIS_ST'
      Origin = 'spedefdc100.VL_PIS_ST'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100VL_COFINS_ST: TFloatField
      FieldName = 'VL_COFINS_ST'
      Origin = 'spedefdc100.VL_COFINS_ST'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC100ParTipo: TIntegerField
      FieldName = 'ParTipo'
      Origin = 'spedefdc100.ParTipo'
    end
    object QrC100ParCodi: TIntegerField
      FieldName = 'ParCodi'
      Origin = 'spedefdc100.ParCodi'
    end
    object QrC100FatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'spedefdc100.FatID'
    end
    object QrC100FatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'spedefdc100.FatNum'
    end
    object QrC100E_S: TWideStringField
      FieldName = 'E_S'
      Required = True
      Size = 1
    end
    object QrC100P_T: TWideStringField
      FieldName = 'P_T'
      Required = True
      Size = 1
    end
    object QrC100ACHOU_TXT: TWideStringField
      FieldName = 'ACHOU_TXT'
      Required = True
      Size = 3
    end
  end
  object DsC100: TDataSource
    DataSet = QrC100
    Left = 48
    Top = 232
  end
  object QrNFeA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE CodInfoEmit=-11'
      'AND ide_mod =55'
      'AND ide_serie=1'
      'AND ide_nNF=1')
    Left = 20
    Top = 316
  end
  object QrPQE: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPQEAfterOpen
    BeforeClose = QrPQEBeforeClose
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'pqe.Codigo, pqe.Data, pqe.IQ, pqe.modNF, '
      'pqe.Serie, pqe.NF, pqe.ValorNF '
      'FROM PQE pqe'
      'LEFT JOIN entidades ent ON ent.Codigo=pqe.IQ'
      'WHERE EFD_INN_AnoMes = 0'
      'EFD_INN_Empresa = 0 '
      'EFD_INN_LinArq = 0'
      'AND pqe.CI=:P0'
      'ORDER BY pqe.Data, pqe.NF'
      '')
    Left = 20
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQENO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrPQECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQEData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQEIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrPQEmodNF: TSmallintField
      FieldName = 'modNF'
    end
    object QrPQESerie: TIntegerField
      FieldName = 'Serie'
    end
    object QrPQENF: TIntegerField
      FieldName = 'NF'
    end
    object QrPQEValorNF: TFloatField
      FieldName = 'ValorNF'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsPQE: TDataSource
    DataSet = QrPQE
    Left = 48
    Top = 260
  end
  object QrPQEIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM pqeits'
      'WHERE Codigo=:P0'
      'ORDER BY Conta')
    Left = 20
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQEItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQEItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPQEItsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPQEItsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQEItsVolumes: TIntegerField
      FieldName = 'Volumes'
    end
    object QrPQEItsPesoVB: TFloatField
      FieldName = 'PesoVB'
    end
    object QrPQEItsPesoVL: TFloatField
      FieldName = 'PesoVL'
    end
    object QrPQEItsValorItem: TFloatField
      FieldName = 'ValorItem'
    end
    object QrPQEItsIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrPQEItsRIPI: TFloatField
      FieldName = 'RIPI'
    end
    object QrPQEItsCFin: TFloatField
      FieldName = 'CFin'
    end
    object QrPQEItsICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrPQEItsRICMS: TFloatField
      FieldName = 'RICMS'
    end
    object QrPQEItsTotalCusto: TFloatField
      FieldName = 'TotalCusto'
    end
    object QrPQEItsTotalPeso: TFloatField
      FieldName = 'TotalPeso'
    end
    object QrPQEItsprod_CFOP: TWideStringField
      FieldName = 'prod_CFOP'
      Size = 4
    end
    object QrPQEItsIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrPQEItsIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
  end
  object QrNFsMPs: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrNFsMPsAfterOpen
    SQL.Strings = (
      'SELECT IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,'
      'mpi.Emissao, '
      'mpi.NF, SUM(mpi.PecasNF) PecasNF, SUM(mpi.PNF) PNF,'
      'SUM(mpi.PLE) PLE, SUM(mpi.CMPValor) CMPValor,'
      'NF_Modelo, NF_Serie, CFOP,'
      'IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece) MyForn'
      'FROM mpinits mpi'
      'LEFT JOIN mpin mpe ON mpe.Controle=mpi.Controle'
      'LEFT JOIN entidades frn ON frn.Codigo='
      '  IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece)'
      'WHERE mpe.ClienteI=-11'
      'AND IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece)<>0'
      'AND mpi.NF<>0'
      'GROUP BY mpi.NF, MyForn'
      '')
    Left = 76
    Top = 260
    object QrNFsMPsEmissao: TDateField
      FieldName = 'Emissao'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFsMPsNF: TIntegerField
      FieldName = 'NF'
    end
    object QrNFsMPsPecasNF: TFloatField
      FieldName = 'PecasNF'
    end
    object QrNFsMPsPNF: TFloatField
      FieldName = 'PNF'
    end
    object QrNFsMPsPLE: TFloatField
      FieldName = 'PLE'
    end
    object QrNFsMPsCMPValor: TFloatField
      FieldName = 'CMPValor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFsMPsNF_Modelo: TWideStringField
      FieldName = 'NF_Modelo'
      Size = 6
    end
    object QrNFsMPsNF_Serie: TWideStringField
      FieldName = 'NF_Serie'
      Size = 6
    end
    object QrNFsMPsCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrNFsMPsMyForn: TLargeintField
      FieldName = 'MyForn'
      Required = True
    end
    object QrNFsMPsNO_FRN: TWideStringField
      FieldName = 'NO_FRN'
      Size = 100
    end
  end
  object DsNFsMPs: TDataSource
    DataSet = QrNFsMPs
    Left = 104
    Top = 260
  end
  object QrMPInIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FRN,'
      'mpi.Emissao, mpi.Conta, mpi.NF, mpi.PecasNF,  mpi.PNF,'
      'mpi.PLE, mpi.CMPValor, mpi.NF_Modelo, mpi.NF_Serie, mpi.CFOP,'
      'IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece) MyForn,'
      'mpi.Controle, mpe.Tipificacao, mpe.Animal'
      'FROM mpinits mpi'
      'LEFT JOIN mpin mpe ON mpe.Controle=mpi.Controle'
      'LEFT JOIN entidades frn ON frn.Codigo='
      '  IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece)'
      'WHERE mpe.ClienteI=-11'
      'ORDER BY mpe.Animal, mpe.Tipificacao')
    Left = 76
    Top = 316
    object QrMPInItsNO_FRN: TWideStringField
      FieldName = 'NO_FRN'
      Size = 100
    end
    object QrMPInItsEmissao: TDateField
      FieldName = 'Emissao'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMPInItsNF: TIntegerField
      FieldName = 'NF'
    end
    object QrMPInItsPecasNF: TFloatField
      FieldName = 'PecasNF'
    end
    object QrMPInItsPNF: TFloatField
      FieldName = 'PNF'
    end
    object QrMPInItsPLE: TFloatField
      FieldName = 'PLE'
    end
    object QrMPInItsCMPValor: TFloatField
      FieldName = 'CMPValor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrMPInItsNF_Modelo: TWideStringField
      FieldName = 'NF_Modelo'
      Size = 6
    end
    object QrMPInItsNF_Serie: TWideStringField
      FieldName = 'NF_Serie'
      Size = 6
    end
    object QrMPInItsCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrMPInItsMyForn: TLargeintField
      FieldName = 'MyForn'
      Required = True
    end
    object QrMPInItsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMPInItsTipificacao: TIntegerField
      FieldName = 'Tipificacao'
    end
    object QrMPInItsAnimal: TSmallintField
      FieldName = 'Animal'
    end
    object QrMPInItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSumMPI: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSumMPICalcFields
    SQL.Strings = (
      'SELECT SUM(mpi.PecasNF) PecasNF, SUM(mpi.PNF) PNF,'
      'SUM(mpi.PLE) PLE, SUM(mpi.CMPValor) CMPValor'
      'FROM mpinits mpi'
      'LEFT JOIN mpin mpe ON mpe.Controle=mpi.Controle'
      'WHERE mpe.ClienteI=-11'
      'AND IF(mpi.EmitNFAvul<>0, mpi.EmitNFAvul, mpi.Fornece)<>0'
      'AND mpi.NF<>0')
    Left = 76
    Top = 288
    object QrSumMPIPecasNF: TFloatField
      FieldName = 'PecasNF'
    end
    object QrSumMPIPNF: TFloatField
      FieldName = 'PNF'
    end
    object QrSumMPIPLE: TFloatField
      FieldName = 'PLE'
    end
    object QrSumMPIPRECO_KG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO_KG'
      Calculated = True
    end
    object QrSumMPIPRECO_PC: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO_PC'
      Calculated = True
    end
    object QrSumMPICMPValor: TFloatField
      FieldName = 'CMPValor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumMPI: TDataSource
    DataSet = QrSumMPI
    Left = 104
    Top = 288
  end
  object DsMPInIts: TDataSource
    DataSet = QrMPInIts
    Left = 104
    Top = 316
  end
  object QrC500: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrC500BeforeClose
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      '_0150.Entidade, c500.*'
      'FROM spedefdc500 c500'
      'LEFT JOIN spedefd0150 _0150 ON _0150.COD_PART=c500.COD_PART'
      '  AND _0150.ImporExpor=c500.ImporExpor'
      '  AND _0150.AnoMes=c500.AnoMes'
      '  AND _0150.Empresa=c500.Empresa'
      'LEFT JOIN entidades ent ON ent.Codigo=_0150.Entidade'
      'WHERE c500.IND_EMIT=1'
      'AND c500.ImporExpor=:P0'
      'AND c500.AnoMes=:P1'
      'AND c500.Empresa=:P2'
      '')
    Left = 216
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrC500NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrC500Entidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrC500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrC500AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrC500Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrC500LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrC500REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrC500IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Size = 1
    end
    object QrC500IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Size = 1
    end
    object QrC500COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrC500COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrC500COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Size = 2
    end
    object QrC500SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrC500SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrC500COD_CONS: TWideStringField
      FieldName = 'COD_CONS'
      Size = 2
    end
    object QrC500NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrC500DT_DOC: TDateField
      FieldName = 'DT_DOC'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrC500DT_E_S: TDateField
      FieldName = 'DT_E_S'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrC500VL_DOC: TFloatField
      FieldName = 'VL_DOC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC500VL_DESC: TFloatField
      FieldName = 'VL_DESC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC500VL_FORN: TFloatField
      FieldName = 'VL_FORN'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC500VL_SERV_NT: TFloatField
      FieldName = 'VL_SERV_NT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC500VL_TERC: TFloatField
      FieldName = 'VL_TERC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC500VL_DA: TFloatField
      FieldName = 'VL_DA'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC500VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC500VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC500VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC500VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC500COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrC500VL_PIS: TFloatField
      FieldName = 'VL_PIS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC500VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrC500COD_GRUPO_TENSAO: TWideStringField
      FieldName = 'COD_GRUPO_TENSAO'
      Size = 2
    end
    object QrC500TP_LIGACAO: TWideStringField
      FieldName = 'TP_LIGACAO'
      Size = 1
    end
  end
  object DsC500: TDataSource
    DataSet = QrC500
    Left = 244
    Top = 232
  end
  object PMSeqAcoes_C500: TPopupMenu
    OnPopup = PMSeqAcoes_C500Popup
    Left = 96
    Top = 584
    object C500_SeqAcoes_1: TMenuItem
      Caption = '&Exclui  NFs C500 importadas'
      Enabled = False
      OnClick = C500_SeqAcoes_1Click
    end
    object C500_SeqAcoes_2: TMenuItem
      Caption = '&Cria NFs C500 carregadas'
      Enabled = False
      OnClick = C500_SeqAcoes_2Click
    end
  end
  object QrC001: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrC001BeforeClose
    AfterScroll = QrC001AfterScroll
    SQL.Strings = (
      'SELECT c001.*'
      'FROM spedefdc001 c001'
      'WHERE c001.ImporExpor=:P0'
      'AND c001.AnoMes=:P1'
      'AND c001.Empresa=:P2'
      '')
    Left = 4
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrC001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
      Origin = 'spedefdc001.ImporExpor'
    end
    object QrC001AnoMes: TIntegerField
      FieldName = 'AnoMes'
      Origin = 'spedefdc001.AnoMes'
    end
    object QrC001Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'spedefdc001.Empresa'
    end
    object QrC001LinArq: TIntegerField
      FieldName = 'LinArq'
      Origin = 'spedefdc001.LinArq'
    end
    object QrC001REG: TWideStringField
      FieldName = 'REG'
      Origin = 'spedefdc001.REG'
      Size = 4
    end
    object QrC001IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Origin = 'spedefdc001.IND_MOV'
      Size = 1
    end
    object QrC001Stat_C100: TSmallintField
      FieldName = 'Stat_C100'
      Origin = 'spedefdc001.Stat_C100'
    end
    object QrC001Stat_C500: TSmallintField
      FieldName = 'Stat_C500'
      Origin = 'spedefdc001.Stat_C500'
    end
  end
  object DsC001: TDataSource
    DataSet = QrC001
    Left = 32
    Top = 4
  end
  object QrC590: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT c590.*'
      'FROM spedefdc590 c590'
      'WHERE c590.ImporExpor=:P0'
      'AND c590.AnoMes=:P1'
      'AND c590.Empresa=:P2'
      'AND c590.C500=:P3'
      '')
    Left = 216
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrC590ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrC590AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrC590Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrC590LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrC590C500: TIntegerField
      FieldName = 'C500'
    end
    object QrC590REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrC590CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrC590CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrC590ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrC590VL_OPR: TFloatField
      FieldName = 'VL_OPR'
    end
    object QrC590VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrC590VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrC590VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
    end
    object QrC590VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
    end
    object QrC590VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
    object QrC590COD_OBS: TWideStringField
      FieldName = 'COD_OBS'
      Size = 6
    end
  end
  object DsC590: TDataSource
    DataSet = QrC590
    Left = 244
    Top = 260
  end
  object QrD500: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrC500BeforeClose
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      '_0150.Entidade, d500.*'
      'FROM spedefdd500 d500'
      'LEFT JOIN spedefd0150 _0150 ON _0150.COD_PART=d500.COD_PART'
      '  AND _0150.ImporExpor=d500.ImporExpor'
      '  AND _0150.AnoMes=d500.AnoMes'
      '  AND _0150.Empresa=d500.Empresa'
      'LEFT JOIN entidades ent ON ent.Codigo=_0150.Entidade'
      'WHERE d500.IND_EMIT=1'
      'AND d500.ImporExpor=:P0'
      'AND d500.AnoMes=:P1'
      'AND d500.Empresa=:P2'
      '')
    Left = 360
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrD500NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrD500Entidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrD500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrD500AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrD500Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrD500LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrD500REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrD500IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Size = 1
    end
    object QrD500IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Size = 1
    end
    object QrD500COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrD500COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrD500COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Size = 2
    end
    object QrD500SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrD500SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrD500NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrD500DT_DOC: TDateField
      FieldName = 'DT_DOC'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrD500DT_A_P: TDateField
      FieldName = 'DT_A_P'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrD500VL_DOC: TFloatField
      FieldName = 'VL_DOC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD500VL_DESC: TFloatField
      FieldName = 'VL_DESC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD500VL_SERV: TFloatField
      FieldName = 'VL_SERV'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD500VL_SERV_NT: TFloatField
      FieldName = 'VL_SERV_NT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD500VL_TERC: TFloatField
      FieldName = 'VL_TERC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD500VL_DA: TFloatField
      FieldName = 'VL_DA'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD500VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD500VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD500COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrD500VL_PIS: TFloatField
      FieldName = 'VL_PIS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD500VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD500COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrD500TP_ASSINANTE: TWideStringField
      FieldName = 'TP_ASSINANTE'
      Size = 1
    end
  end
  object DsD500: TDataSource
    DataSet = QrD500
    Left = 388
    Top = 232
  end
  object DsD590: TDataSource
    DataSet = QrD590
    Left = 388
    Top = 260
  end
  object QrD590: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT d590.*'
      'FROM spedefdd590 d590'
      'WHERE d590.ImporExpor=:P0'
      'AND d590.AnoMes=:P1'
      'AND d590.Empresa=:P2'
      'AND d590.D500=:P3'
      '')
    Left = 360
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrD590ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrD590AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrD590Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrD590LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrD590D500: TIntegerField
      FieldName = 'D500'
    end
    object QrD590REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrD590CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrD590CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrD590ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrD590VL_OPR: TFloatField
      FieldName = 'VL_OPR'
    end
    object QrD590VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrD590VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrD590VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
    end
    object QrD590VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
    end
    object QrD590VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
    object QrD590COD_OBS: TWideStringField
      FieldName = 'COD_OBS'
      Size = 6
    end
  end
  object PMSeqAcoes_D500: TPopupMenu
    OnPopup = PMSeqAcoes_D500Popup
    Left = 152
    Top = 584
    object D500_SeqAcoes_1: TMenuItem
      Caption = '&Exclui  NFs D500 importadas'
      Enabled = False
      OnClick = D500_SeqAcoes_1Click
    end
    object D500_SeqAcoes_2: TMenuItem
      Caption = '&Cria NFs D500 carregadas'
      Enabled = False
      OnClick = D500_SeqAcoes_2Click
    end
  end
  object QrD001: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrD001BeforeClose
    AfterScroll = QrD001AfterScroll
    SQL.Strings = (
      'SELECT d001.*'
      'FROM spedefdd001 d001'
      'WHERE d001.ImporExpor=:P0'
      'AND d001.AnoMes=:P1'
      'AND d001.Empresa=:P2'
      '')
    Left = 60
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrD001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrD001AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrD001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrD001LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrD001REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrD001IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
    object QrD001Stat_D100i: TSmallintField
      FieldName = 'Stat_D100i'
    end
    object QrD001Stat_D500i: TSmallintField
      FieldName = 'Stat_D500i'
    end
  end
  object DsD001: TDataSource
    DataSet = QrD001
    Left = 88
    Top = 4
  end
  object QrD100: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrC500BeforeClose
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      '_0150.Entidade, d100.*'
      'FROM spedefdd100 d100'
      'LEFT JOIN spedefd0150 _0150 ON _0150.COD_PART=d100.COD_PART'
      '  AND _0150.ImporExpor=d100.ImporExpor'
      '  AND _0150.AnoMes=d100.AnoMes'
      '  AND _0150.Empresa=d100.Empresa'
      'LEFT JOIN entidades ent ON ent.Codigo=_0150.Entidade'
      'WHERE d100.IND_EMIT=1'
      'AND d100.ImporExpor=:P0'
      'AND d100.AnoMes=:P1'
      'AND d100.Empresa=:P2'
      '')
    Left = 288
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrD100NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrD100Entidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'spedefd0150.Entidade'
    end
    object QrD100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
      Origin = 'spedefdd100.ImporExpor'
    end
    object QrD100AnoMes: TIntegerField
      FieldName = 'AnoMes'
      Origin = 'spedefdd100.AnoMes'
    end
    object QrD100Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'spedefdd100.Empresa'
    end
    object QrD100LinArq: TIntegerField
      FieldName = 'LinArq'
      Origin = 'spedefdd100.LinArq'
    end
    object QrD100REG: TWideStringField
      FieldName = 'REG'
      Origin = 'spedefdd100.REG'
      Size = 4
    end
    object QrD100IND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Origin = 'spedefdd100.IND_OPER'
      Size = 1
    end
    object QrD100IND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Origin = 'spedefdd100.IND_EMIT'
      Size = 1
    end
    object QrD100COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Origin = 'spedefdd100.COD_PART'
      Size = 60
    end
    object QrD100COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Origin = 'spedefdd100.COD_MOD'
      Size = 2
    end
    object QrD100COD_SIT: TWideStringField
      FieldName = 'COD_SIT'
      Origin = 'spedefdd100.COD_SIT'
      Size = 2
    end
    object QrD100SER: TWideStringField
      FieldName = 'SER'
      Origin = 'spedefdd100.SER'
      Size = 4
    end
    object QrD100SUB: TWideStringField
      FieldName = 'SUB'
      Origin = 'spedefdd100.SUB'
      Size = 3
    end
    object QrD100NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Origin = 'spedefdd100.NUM_DOC'
    end
    object QrD100CHV_CTE: TWideStringField
      FieldName = 'CHV_CTE'
      Origin = 'spedefdd100.CHV_CTE'
      Size = 44
    end
    object QrD100DT_DOC: TDateField
      FieldName = 'DT_DOC'
      Origin = 'spedefdd100.DT_DOC'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrD100DT_A_P: TDateField
      FieldName = 'DT_A_P'
      Origin = 'spedefdd100.DT_A_P'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrD100CHV_CTE_REF: TWideStringField
      FieldName = 'CHV_CTE_REF'
      Origin = 'spedefdd100.CHV_CTE_REF'
      Size = 44
    end
    object QrD100VL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Origin = 'spedefdd100.VL_DOC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD100VL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Origin = 'spedefdd100.VL_DESC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD100IND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Origin = 'spedefdd100.IND_FRT'
      Size = 1
    end
    object QrD100VL_SERV: TFloatField
      FieldName = 'VL_SERV'
      Origin = 'spedefdd100.VL_SERV'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD100VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Origin = 'spedefdd100.VL_BC_ICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD100VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Origin = 'spedefdd100.VL_ICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD100VL_NT: TFloatField
      FieldName = 'VL_NT'
      Origin = 'spedefdd100.VL_NT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrD100COD_INF: TWideStringField
      FieldName = 'COD_INF'
      Origin = 'spedefdd100.COD_INF'
      Size = 6
    end
    object QrD100COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Origin = 'spedefdd100.COD_CTA'
      Size = 255
    end
    object QrD100TP_CTE: TSmallintField
      FieldName = 'TP_CTE'
    end
  end
  object DsD100: TDataSource
    DataSet = QrD100
    Left = 316
    Top = 232
  end
  object DsD190: TDataSource
    DataSet = QrD190
    Left = 316
    Top = 260
  end
  object QrD190: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT d190.*'
      'FROM spedefdd190 d190'
      'WHERE d190.ImporExpor=:P0'
      'AND d190.AnoMes=:P1'
      'AND d190.Empresa=:P2'
      'AND d190.d100=:P3'
      '')
    Left = 288
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrD190ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrD190AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrD190Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrD190LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrD190D100: TIntegerField
      FieldName = 'D100'
    end
    object QrD190REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrD190CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrD190CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrD190ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrD190VL_OPR: TFloatField
      FieldName = 'VL_OPR'
    end
    object QrD190VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrD190VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrD190VL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
    end
    object QrD190COD_OBS: TWideStringField
      FieldName = 'COD_OBS'
      Size = 6
    end
  end
  object PMSeqAcoes_D100: TPopupMenu
    OnPopup = PMSeqAcoes_D100Popup
    Left = 124
    Top = 584
    object D100_SeqAcoes_1: TMenuItem
      Caption = '&Exclui  NFs D100 importadas'
      Enabled = False
      OnClick = D100_SeqAcoes_1Click
    end
    object D100_SeqAcoes_2: TMenuItem
      Caption = '&Cria NFs D100 carregadas'
      Enabled = False
      OnClick = D100_SeqAcoes_2Click
    end
  end
  object QrC170: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT c170.* '
      'FROM spedefdc170 c170'
      'WHERE c170.ImporExpor=:P0'
      'AND c170.AnoMes=:P1'
      'AND c170.Empresa=:P2'
      'AND C170.C100=:P3')
    Left = 76
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrC170ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrC170AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrC170Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrC170LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrC170C100: TIntegerField
      FieldName = 'C100'
    end
    object QrC170GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrC170REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrC170NUM_ITEM: TIntegerField
      FieldName = 'NUM_ITEM'
    end
    object QrC170COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrC170DESCR_COMPL: TWideStringField
      FieldName = 'DESCR_COMPL'
      Size = 255
    end
    object QrC170QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrC170VL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
    end
    object QrC170VL_DESC: TFloatField
      FieldName = 'VL_DESC'
    end
    object QrC170IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
    object QrC170CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrC170CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrC170COD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrC170VL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
    end
    object QrC170ALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
    end
    object QrC170VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
    end
    object QrC170VL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
    end
    object QrC170ALIQ_ST: TFloatField
      FieldName = 'ALIQ_ST'
    end
    object QrC170VL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
    end
    object QrC170IND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrC170CST_IPI: TWideStringField
      FieldName = 'CST_IPI'
      Size = 2
    end
    object QrC170COD_ENQ: TWideStringField
      FieldName = 'COD_ENQ'
      Size = 3
    end
    object QrC170VL_BC_IPI: TFloatField
      FieldName = 'VL_BC_IPI'
    end
    object QrC170ALIQ_IPI: TFloatField
      FieldName = 'ALIQ_IPI'
    end
    object QrC170VL_IPI: TFloatField
      FieldName = 'VL_IPI'
    end
    object QrC170CST_PIS: TSmallintField
      FieldName = 'CST_PIS'
    end
    object QrC170VL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
    end
    object QrC170ALIQ_PIS_p: TFloatField
      FieldName = 'ALIQ_PIS_p'
    end
    object QrC170QUANT_BC_PIS: TFloatField
      FieldName = 'QUANT_BC_PIS'
    end
    object QrC170ALIQ_PIS_r: TFloatField
      FieldName = 'ALIQ_PIS_r'
    end
    object QrC170VL_PIS: TFloatField
      FieldName = 'VL_PIS'
    end
    object QrC170CST_COFINS: TSmallintField
      FieldName = 'CST_COFINS'
    end
    object QrC170VL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
    end
    object QrC170ALIQ_COFINS_p: TFloatField
      FieldName = 'ALIQ_COFINS_p'
    end
    object QrC170QUANT_BC_COFINS: TFloatField
      FieldName = 'QUANT_BC_COFINS'
    end
    object QrC170ALIQ_COFINS_r: TFloatField
      FieldName = 'ALIQ_COFINS_r'
    end
    object QrC170VL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
    end
    object QrC170COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrC170UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
  end
  object DsC170: TDataSource
    DataSet = QrC170
    Left = 104
    Top = 232
  end
  object PMSeqAcoes_C100: TPopupMenu
    OnPopup = PMSeqAcoes_C100Popup
    Left = 68
    Top = 584
    object C100_SeqAcoes_1: TMenuItem
      Caption = '&1. Vincular com NFs digitadas (PQE e NFe)'
      Enabled = False
      OnClick = C100_SeqAcoes_1Click
    end
    object C100_SeqAcoes_2: TMenuItem
      Caption = '&2. Exclui  NFs C100 importadas'
      Enabled = False
      OnClick = C100_SeqAcoes_2Click
    end
    object C100_SeqAcoes_3: TMenuItem
      Caption = '&3. Exclui NFs criadas a for'#231'a'
      Enabled = False
      OnClick = C100_SeqAcoes_3Click
    end
    object C100_SeqAcoes_4: TMenuItem
      Caption = '&4. Cria NFs C100 carregadas e n'#227'o vinculadas'
      Enabled = False
      OnClick = C100_SeqAcoes_4Click
    end
  end
  object QrLocPQE: TMySQLQuery
    Database = Dmod.MyDB
    Left = 140
    Top = 232
    object QrLocPQECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocPQEValorNF: TFloatField
      FieldName = 'ValorNF'
    end
  end
  object QrLocMPc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT its.Conta'
      'FROM mpinits its'
      'LEFT JOIN MPIn mpi ON mpi.Controle=its.Controle'
      'WHERE its.EmitNFAvul=0'
      'AND mpi.Procedencia>0'
      'AND its.NF>0'
      ''
      '')
    Left = 140
    Top = 260
    object QrLocMPcConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object QrLocEMP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM entimp '
      'WHERE Codigo=:P0')
    Left = 140
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocEMPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IDCtrl, FatID, FatNum, Empresa '
      'FROM nfecaba '
      'WHERE ide_tpAmb <> 2 '
      'AND CodInfoEmit=-11 '
      'AND CodInfoDest=353 '
      'AND ide_nNF=8 ')
    Left = 140
    Top = 316
    object QrCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrExclMul: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum,  infProt_cStat, Empresa'
      'FROM nfecaba nca '
      'WHERE nca.FatID=:P0'
      'AND nca.Empresa=:P1'
      'AND CriAForca=1'
      'OR Importado=1'
      'AND DataFiscal BETWEEN :P2 AND :P3')
    Left = 140
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrExclMulFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrExclMulFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrExclMulEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrExclMulinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
  end
  object QrC190: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT c170.* '
      'FROM spedefdc170 c170'
      'WHERE c170.ImporExpor=:P0'
      'AND c170.AnoMes=:P1'
      'AND c170.Empresa=:P2'
      'AND C170.C100=:P3')
    Left = 76
    Top = 348
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
  end
  object DsC190: TDataSource
    DataSet = QrC190
    Left = 104
    Top = 348
  end
  object QrPQE_2_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FatNum, DataE dtEmi, DataS dtSaiEnt,  '
      'Data DataFiscal, ValorNF ValTot, CI Empresa,  '
      'CI CodInfoDest, IQ CodInfoEmit, refNFe Id,  '
      'modNF ide_mod, Serie,  NF nNF, Frete, Seguro, '
      'Desconto, IPI, PIS, COFINS, Outros'
      'FROM PQE '
      'WHERE Codigo=:P0')
    Left = 140
    Top = 372
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQE_2_FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrPQE_2_dtEmi: TDateField
      FieldName = 'dtEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQE_2_dtSaiEnt: TDateField
      FieldName = 'dtSaiEnt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQE_2_DataFiscal: TDateField
      FieldName = 'DataFiscal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQE_2_ValTot: TFloatField
      FieldName = 'ValTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPQE_2_Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPQE_2_CodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object QrPQE_2_CodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrPQE_2_Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrPQE_2_ide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrPQE_2_Serie: TIntegerField
      FieldName = 'Serie'
    end
    object QrPQE_2_nNF: TIntegerField
      FieldName = 'nNF'
    end
    object QrPQE_2_Frete: TFloatField
      FieldName = 'Frete'
    end
    object QrPQE_2_Seguro: TFloatField
      FieldName = 'Seguro'
    end
    object QrPQE_2_Desconto: TFloatField
      FieldName = 'Desconto'
    end
    object QrPQE_2_IPI: TFloatField
      FieldName = 'IPI'
    end
    object QrPQE_2_PIS: TFloatField
      FieldName = 'PIS'
    end
    object QrPQE_2_COFINS: TFloatField
      FieldName = 'COFINS'
    end
    object QrPQE_2_Outros: TFloatField
      FieldName = 'Outros'
    end
  end
  object QrPQEIts_2_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Nome NOMEPQ, ei.*'
      'FROM pqeits ei'
      'LEFT JOIN pq ON pq.Codigo=ei.Insumo'
      'WHERE ei.Codigo=:P0'
      'ORDER BY Conta'
      ''
      ''
      '')
    Left = 140
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQEIts_2_CUSTOITEM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOITEM'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
    object QrPQEIts_2_VALORKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALORKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEIts_2_TOTALKGBRUTO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALKGBRUTO'
      Calculated = True
    end
    object QrPQEIts_2_CUSTOKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEIts_2_PRECOKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECOKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEIts_2_Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPQEIts_2_Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQEIts_2_Conta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrPQEIts_2_Volumes: TIntegerField
      FieldName = 'Volumes'
      Required = True
    end
    object QrPQEIts_2_PesoVB: TFloatField
      FieldName = 'PesoVB'
      Required = True
      DisplayFormat = '#,##0.000'
    end
    object QrPQEIts_2_PesoVL: TFloatField
      FieldName = 'PesoVL'
      Required = True
      DisplayFormat = '#,##0.000'
    end
    object QrPQEIts_2_ValorItem: TFloatField
      FieldName = 'ValorItem'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQEIts_2_IPI: TFloatField
      FieldName = 'IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEIts_2_RIPI: TFloatField
      FieldName = 'RIPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEIts_2_CFin: TFloatField
      FieldName = 'CFin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEIts_2_ICMS: TFloatField
      FieldName = 'ICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEIts_2_RICMS: TFloatField
      FieldName = 'RICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEIts_2_TotalCusto: TFloatField
      FieldName = 'TotalCusto'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEIts_2_Insumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPQEIts_2_TotalPeso: TFloatField
      FieldName = 'TotalPeso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQEIts_2_NOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQEIts_2_prod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrPQEIts_2_prod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrPQEIts_2_prod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrPQEIts_2_prod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrPQEIts_2_prod_EX_TIPI: TWideStringField
      FieldName = 'prod_EX_TIPI'
      Size = 3
    end
    object QrPQEIts_2_prod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrPQEIts_2_prod_CFOP: TWideStringField
      FieldName = 'prod_CFOP'
      Size = 4
    end
    object QrPQEIts_2_prod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 4
    end
    object QrPQEIts_2_prod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrPQEIts_2_prod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrPQEIts_2_prod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrPQEIts_2_prod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrPQEIts_2_prod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 4
    end
    object QrPQEIts_2_prod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrPQEIts_2_prod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrPQEIts_2_prod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrPQEIts_2_prod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrPQEIts_2_prod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrPQEIts_2_ICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrPQEIts_2_ICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrPQEIts_2_ICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrPQEIts_2_ICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrPQEIts_2_ICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_IPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrPQEIts_2_IPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrPQEIts_2_IPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_IPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_IPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_IPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_IPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_PIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrPQEIts_2_PIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_PIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_PIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_PIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_PIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_PISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_PISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_PISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_PISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_PISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_COFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrPQEIts_2_COFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrPQEIts_2_COFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_COFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_COFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_COFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_COFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_COFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_COFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_COFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_COFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object QrLocEFD: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM efd_c100'
      'WHERE ImporExpor <> 2'
      'AND AnoMes=201001'
      'AND Empresa=-11'
      'AND Terceiro>0'
      'AND COD_MOD=55'
      'AND SER=1'
      'AND NUM_DOC=98')
    Left = 112
    Top = 400
    object QrLocEFDLinArq: TIntegerField
      FieldName = 'LinArq'
    end
  end
end
