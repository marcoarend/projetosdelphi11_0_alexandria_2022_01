unit EfdIcmsIpiK275_v03_0_2_a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup, dmkEditCalc, UnDmkProcFunc,
  UnEfdIcmsIpi_PF, UnAppEnums;

type
  TFmEfdIcmsIpiK275_v03_0_2_a = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdPeriApu: TdmkEdit;
    Label6: TLabel;
    EdKndTab: TdmkEdit;
    Label9: TLabel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXEx_TIPI: TWideStringField;
    DsGraGruX: TDataSource;
    Panel5: TPanel;
    EdGraGruX: TdmkEditCB;
    Label232: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    StaticText2: TStaticText;
    EdQTD: TdmkEdit;
    EdKndCod: TdmkEdit;
    Label7: TLabel;
    EdKndNSU: TdmkEdit;
    Label8: TLabel;
    EdKndItm: TdmkEdit;
    Label10: TLabel;
    EdKndAID: TdmkEdit;
    Label1: TLabel;
    EdKndNiv: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    EdIDSeq1: TdmkEdit;
    QrGraGruXGrandeza: TIntegerField;
    QrGraGruXTipo_Item: TIntegerField;
    EdIDSeq2: TdmkEdit;
    Label15: TLabel;
    DBText1: TDBText;
    Label2: TLabel;
    TPDT_SAIDA: TdmkEditDateTimePicker;
    Label13: TLabel;
    EdCOD_INS_SUBST: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdAnoMesChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGruX();
  public
    { Public declarations }
    FID_SEK, FESTSTabSorc, FOriOpeProc(*, FOrigemIDKnd*): Integer;
  end;

  var
  FmEfdIcmsIpiK275_v03_0_2_a: TFmEfdIcmsIpiK275_v03_0_2_a;

implementation

uses UnMyObjects, Module, (*EfdIcmsIpiE001,*) UMySQLModule, DmkDAC_PF, MyListas,
  ModuleFin, UnFinanceiro, SpedEfdIcmsIpi_v03_0_2_a;

{$R *.DFM}

procedure TFmEfdIcmsIpiK275_v03_0_2_a.BtOKClick(Sender: TObject);
var
  DT_SAIDA, COD_ITEM, COD_INS_SUBST, COD_DOC_OP: String;
  ImporExpor, AnoMes, Empresa, PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq1, IDSeq2, ID_SEK, GraGruX, ESTSTabSorc, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  Data, DtIniOP, DtFimOP: TDateTime;
begin
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  PeriApu        := EdPeriApu.ValueVariant;
  if SQLType = stIns then
  begin
    KndTab         := Integer(TEstqSPEDTabSorc.estsNoSrc);
    KndCod         := PeriApu;
    KndNSU         := PeriApu;
    KndItm         := 0; // Abaixo
    KndAID         := Integer(TEstqMovimID.emidAjuste);
    KndNiv         := Integer(TEstqMovimNiv.eminSemNiv);
    IDSeq1         := EdIDSeq1.ValueVariant;
    IDSeq2         := 0; // Abaixo
    ID_SEK         := Integer(TSPED_EFD_ComoLancou.seclLancadoManual);
    ESTSTabSorc    := Integer(TEstqSPEDTabSorc.estsNoSrc);
    OriOpeProc     := Integer(TOrigemOpeProc.oopND);
    //OrigemIDKnd    := Integer(TOrigemIDKnd.oidk000ND);
  end else
  begin
    KndTab         := EdKndTab.ValueVariant;
    KndCod         := EdKndCod.ValueVariant;
    KndNSU         := EdKndNSU.ValueVariant;
    KndItm         := EdKndItm.ValueVariant;
    KndAID         := EdKndAID.ValueVariant;
    KndNiv         := EdKndNiv.ValueVariant;
    IDSeq1         := EdIDSeq1.ValueVariant;
    IDSeq2         := EdIDSeq2.ValueVariant;;
    ID_SEK         := FID_SEK;
    ESTSTabSorc    := FESTSTabSorc;
    OriOpeProc     := FOriOpeProc;
    //OrigemIDKnd    := FOrigemIDKnd;
  end;
  GraGruX        := EdGraGruX.ValueVariant;
  Data           := Trunc(TPDT_SAIDA.Date);
  DtIniOP        := DfSEII_v03_0_2_a.QrK230DT_INI_OP.Value;
  DtFimOP        := DfSEII_v03_0_2_a.QrK230DT_FIN_OP.Value;
  DT_SAIDA       := Geral.FDT(Data, 1);
  COD_ITEM       := dmkPF.ITS_Null(GraGruX);
  QTD            := EdQTD.ValueVariant;
  COD_INS_SUBST  := Trim(EdCOD_INS_SUBST.Text);
  COD_DOC_OP     := DfSEII_v03_0_2_a.QrK230COD_DOC_OP.Value;
  //
  if MyObjects.FIC(TPDT_SAIDA.Date < 2, TPDT_SAIDA,
    'Informe a data da sa�da do estoque!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o c�digo do produto!') then
    Exit;
  //
  (*Campo 01 (REG) - Valor V�lido: [K235]*)
  (*Campo 02 (DT_SA�DA) - Valida��o: a data deve estar compreendida no per�odo
  da ordem de produ��o, se existente, campos DT_INI_OP e DT_FIN_OP do Registro
  K230. Se DT_FIN_OP do Registro K230 � Itens Produzidos estiver em branco, o
  campo DT_SA�DA dever� ser maior que o campo DT_INI_OP do Registro K230 e menor
  ou igual a DT_FIN do Registro K100. E em qualquer hip�tese a data deve estar
  compreendida no per�odo de apura��o � K100.*)

  if not EfdIcmsIpi_PF.ValidaDataItemDentroDoAnoMesSPED(AnoMes, Data, DtIniOP,
  DtFimOP, COD_DOC_OP) then Exit;

  (*Campo 03 (COD_ITEM) � Valida��es:
    a) o c�digo do item componente/insumo dever� existir no campo COD_ITEM do
    Registro 0200;
    b) caso o campo COD_INS_SUBST esteja em branco, o c�digo do item componente/
    insumo deve existir tamb�m no Registro 0210 para o mesmo produto resultante
    � K230/0200 (valida��o aplicada apenas para as UFs que adotarem o registro
    0210).
    c) o c�digo do item componente/insumo deve ser diferente do c�digo do
    produto resultante (COD_ITEM do Registro K230);
    d) o tipo do componente/insumo (campo TIPO_ITEM do Registro 0200) deve ser
    igual a 00, 01, 02, 03, 04, 05 ou 10.
  A quantidade consumida de produto intermedi�rio � tipo 06 no processo
  produtivo n�o � escriturada na EFD ICMS/IPI, tanto no Bloco K quanto no Bloco
  C (NF-e). Se o Fisco quiser saber qual foi a quantidade consumida de produto
  intermedi�rio no processo produtivo basta aplicar a f�rmula : Quantidade
  consumida = estoque inicial (K200) + entrada (C170) � sa�da (C100/NF-e -
  Devolu��o) � estoque final (K200).
  *)

  if not EfdIcmsIpi_PF.ValidaGGXEstahDentroDoAnoMesSPED(AnoMes, GraGruX,
  COD_DOC_OP) then Exit;

  (*Campo 04 (QTD) � Preenchimento: n�o � admitida quantidade negativa.*)

  if not EfdIcmsIpi_PF.ValidaQtdeDentroDoAnoMesSPED(AnoMes, QTD, COD_DOC_OP,
  (*MaiorQueZero*)False) then Exit;

  (*Campo 05 (COD_INS_SUBST) � Preenchimento: informar o c�digo do item
  componente/insumo que estava previsto para ser consumido no Registro 0210 e
  que foi substitu�do pelo COD_ITEM deste registro.
  Valida��o: o c�digo do insumo substitu�do deve existir no Registro 0210 para o
  mesmo produto resultante � K230/0200.
  O tipo do componente/insumo (campo TIPO_ITEM do Registro 0200) deve ser igual
  a 00, 01, 02, 03, 04, 05 ou 10.*)

  if not EfdIcmsIpi_PF.ValidaCOD_INS_SUBST_DentroDoAnoMesSPED(AnoMes, GraGruX,
  COD_INS_SUBST, COD_DOC_OP) then Exit;

  //

  KndItm := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik235', 'KndItm', [
  'ImporExpor', 'AnoMes', 'KndTab'], [ImporExpor, AnoMes, KndTab],
  SQLType, KndItm, siPositivo, EdKndItm);
  //
  IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efdicmsipik235', 'IDSeq2', [
  'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
  ImporExpor, AnoMes, Empresa, PeriApu],
  SQLType, IDSeq2, siPositivo, EdIDSeq2);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efdicmsipik235', False, [
  'KndAID', 'KndNiv', 'IDSeq1',
  'IDSeq2', 'ID_SEK', 'DT_SAIDA',
  'COD_ITEM', 'QTD', 'COD_INS_SUBST',
  'ESTSTabSorc', 'OriOpeProc', 'GraGruX'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'], [
  KndAID, KndNiv, IDSeq1,
  IDSeq2, ID_SEK, DT_SAIDA,
  COD_ITEM, QTD, COD_INS_SUBST,
  ESTSTabSorc, OriOpeProc, GraGruX], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True) then
  begin
    DfSEII_v03_0_2_a.ReopenK235(IDSeq2);
    Close;
  end;
end;

procedure TFmEfdIcmsIpiK275_v03_0_2_a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEfdIcmsIpiK275_v03_0_2_a.EdAnoMesChange(Sender: TObject);
begin
 MyObjects.LimitaDatasPeloAnoMesDateTimePicker(TPDT_SAIDA, EdAnoMes.ValueVariant);
end;

procedure TFmEfdIcmsIpiK275_v03_0_2_a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdIcmsIpiK275_v03_0_2_a.FormCreate(Sender: TObject);
begin
  ReopenGraGruX();
end;

procedure TFmEfdIcmsIpiK275_v03_0_2_a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdIcmsIpiK275_v03_0_2_a.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
begin
  SQL_AND := '';
  SQL_LFT := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, ',
  'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, ',
  'pgt.Tipo_Item) Tipo_Item ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  SQL_LFT,
  'WHERE ggx.Controle > 0 ',
  SQL_AND,
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

end.
