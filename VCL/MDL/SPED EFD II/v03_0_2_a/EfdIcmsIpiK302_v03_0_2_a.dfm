object FmEfdIcmsIpiK302_v03_0_2_a: TFmEfdIcmsIpiK302_v03_0_2_a
  Left = 339
  Top = 185
  Caption = 
    'EII-SPEDK-302 :: Produ'#231#227'o Conjunta '#8211' Industrializa'#231#227'o Efetuada p' +
    'or Terceiros '#8211' Insumos Consumidos'
  ClientHeight = 388
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 701
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 653
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 605
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 1026
        Height = 32
        Caption = 
          'Produ'#231#227'o Conjunta '#8211' Industrializa'#231#227'o Efetuada por Terceiros '#8211' In' +
          'sumos Consumidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 1026
        Height = 32
        Caption = 
          'Produ'#231#227'o Conjunta '#8211' Industrializa'#231#227'o Efetuada por Terceiros '#8211' In' +
          'sumos Consumidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 1026
        Height = 32
        Caption = 
          'Produ'#231#227'o Conjunta '#8211' Industrializa'#231#227'o Efetuada por Terceiros '#8211' In' +
          'sumos Consumidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 318
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitWidth = 701
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 555
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 553
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 226
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 701
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 226
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 701
      object GroupBox1: TGroupBox
        Left = 0
        Top = 105
        Width = 1008
        Height = 121
        Align = alClient
        Caption = ' Dados do item: '
        TabOrder = 0
        ExplicitWidth = 701
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 104
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitWidth = 697
          object Label232: TLabel
            Left = 52
            Top = 12
            Width = 180
            Height = 13
            Caption = '02. Meu c'#243'digo (reduzido do produto):'
          end
          object DBText1: TDBText
            Left = 640
            Top = 32
            Width = 65
            Height = 17
            DataField = 'SIGLAUNIDMED'
            DataSource = DsGraGruX
          end
          object EdGraGruX: TdmkEditCB
            Left = 52
            Top = 28
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'COD_ITEM'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGraGruX
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraGruX: TdmkDBLookupComboBox
            Left = 110
            Top = 28
            Width = 527
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_PRD_TAM_COR'
            ListSource = DsGraGruX
            TabOrder = 1
            dmkEditCB = EdGraGruX
            QryCampo = 'COD_ITEM'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object StaticText2: TStaticText
            Left = 52
            Top = 52
            Width = 469
            Height = 21
            AutoSize = False
            BorderStyle = sbsSunken
            Caption = '03. Quantidade do item'
            FocusControl = EdQTD
            TabOrder = 2
          end
          object EdQTD: TdmkEdit
            Left = 524
            Top = 52
            Width = 112
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'QTD'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object CkContinuar: TCheckBox
            Left = 52
            Top = 80
            Width = 117
            Height = 17
            Caption = 'Continuar inserindo.'
            Checked = True
            State = cbChecked
            TabOrder = 4
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 105
        Align = alTop
        Caption = ' Dados do invent'#225'rio: '
        Enabled = False
        TabOrder = 1
        ExplicitWidth = 701
        object Label3: TLabel
          Left = 8
          Top = 16
          Width = 31
          Height = 13
          Caption = 'Im/Ex:'
        end
        object Label4: TLabel
          Left = 44
          Top = 16
          Width = 42
          Height = 13
          Caption = 'AnoMes:'
        end
        object Label5: TLabel
          Left = 100
          Top = 16
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label6: TLabel
          Left = 148
          Top = 16
          Width = 28
          Height = 13
          Caption = 'K100:'
        end
        object Label9: TLabel
          Left = 184
          Top = 16
          Width = 41
          Height = 13
          Caption = 'KndTab:'
        end
        object Label7: TLabel
          Left = 232
          Top = 16
          Width = 41
          Height = 13
          Caption = 'KndCod:'
        end
        object Label8: TLabel
          Left = 296
          Top = 16
          Width = 45
          Height = 13
          Caption = 'KndNSU:'
        end
        object Label10: TLabel
          Left = 360
          Top = 16
          Width = 36
          Height = 13
          Caption = 'KndItm:'
        end
        object Label1: TLabel
          Left = 424
          Top = 16
          Width = 40
          Height = 13
          Caption = 'KndAID:'
        end
        object Label11: TLabel
          Left = 472
          Top = 16
          Width = 38
          Height = 13
          Caption = 'KndNiv:'
        end
        object Label12: TLabel
          Left = 520
          Top = 16
          Width = 39
          Height = 13
          Caption = 'IDSeq1:'
        end
        object Label15: TLabel
          Left = 584
          Top = 16
          Width = 39
          Height = 13
          Caption = 'IDSeq2:'
        end
        object Label14: TLabel
          Left = 8
          Top = 60
          Width = 54
          Height = 13
          Caption = 'JmpMovID:'
        end
        object Label16: TLabel
          Left = 72
          Top = 60
          Width = 62
          Height = 13
          Caption = 'JmpMovCod:'
        end
        object Label17: TLabel
          Left = 136
          Top = 60
          Width = 52
          Height = 13
          Caption = 'JmpNivel1:'
        end
        object Label18: TLabel
          Left = 200
          Top = 60
          Width = 52
          Height = 13
          Caption = 'JmpNivel2:'
        end
        object Label19: TLabel
          Left = 264
          Top = 60
          Width = 50
          Height = 13
          Caption = 'PaiMovID:'
        end
        object Label20: TLabel
          Left = 328
          Top = 60
          Width = 58
          Height = 13
          Caption = 'PaiMovCod:'
        end
        object Label21: TLabel
          Left = 392
          Top = 60
          Width = 48
          Height = 13
          Caption = 'PaiNivel1:'
        end
        object Label22: TLabel
          Left = 456
          Top = 60
          Width = 48
          Height = 13
          Caption = 'PaiNivel2:'
        end
        object EdImporExpor: TdmkEdit
          Left = 8
          Top = 32
          Width = 32
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAnoMes: TdmkEdit
          Left = 44
          Top = 32
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdAnoMesChange
        end
        object EdEmpresa: TdmkEdit
          Left = 100
          Top = 32
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPeriApu: TdmkEdit
          Left = 148
          Top = 32
          Width = 32
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdKndTab: TdmkEdit
          Left = 184
          Top = 32
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdKndCod: TdmkEdit
          Left = 232
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdKndNSU: TdmkEdit
          Left = 296
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdKndItm: TdmkEdit
          Left = 360
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdKndAID: TdmkEdit
          Left = 424
          Top = 32
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdKndNiv: TdmkEdit
          Left = 472
          Top = 32
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdIDSeq1: TdmkEdit
          Left = 520
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdIDSeq2: TdmkEdit
          Left = 584
          Top = 32
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EDJmpMovID: TdmkEdit
          Left = 8
          Top = 76
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdJmpMovCod: TdmkEdit
          Left = 72
          Top = 76
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 13
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EDJmpNivel1: TdmkEdit
          Left = 136
          Top = 76
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 14
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdJmpNivel2: TdmkEdit
          Left = 200
          Top = 76
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 15
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPaiMovID: TdmkEdit
          Left = 264
          Top = 76
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 16
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPaiMovCod: TdmkEdit
          Left = 328
          Top = 76
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 17
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPaiNivel1: TdmkEdit
          Left = 392
          Top = 76
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 18
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPaiNivel2: TdmkEdit
          Left = 456
          Top = 76
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 19
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 274
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitWidth = 701
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 697
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq,'
      'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle > -900000'
      'ORDER BY NO_PRD_TAM_COR')
    Left = 632
    Top = 64
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 166
    end
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGruXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGruXEx_TIPI: TWideStringField
      FieldName = 'Ex_TIPI'
      Size = 3
    end
    object QrGraGruXGrandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrGraGruXTipo_Item: TIntegerField
      FieldName = 'Tipo_Item'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 632
    Top = 112
  end
end
