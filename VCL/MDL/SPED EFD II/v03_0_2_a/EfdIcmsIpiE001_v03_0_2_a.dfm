object FmEfdIcmsIpiE001_v03_0_2_a: TFmEfdIcmsIpiE001_v03_0_2_a
  Left = 368
  Top = 194
  Caption = 'EII-SPEDE-001 :: Apura'#231#227'o do ICMS e do IPI'
  ClientHeight = 834
  ClientWidth = 1082
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnForm: TPanel
    Left = 0
    Top = 0
    Width = 1082
    Height = 834
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GBAvisos1: TGroupBox
      Left = 0
      Top = 48
      Width = 1082
      Height = 65
      Align = alTop
      Caption = ' Avisos: '
      TabOrder = 0
      object Panel12: TPanel
        Left = 2
        Top = 15
        Width = 1078
        Height = 48
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object PB1: TProgressBar
          Left = 0
          Top = 31
          Width = 1078
          Height = 17
          Align = alBottom
          TabOrder = 0
        end
      end
    end
    object PainelDados: TPanel
      Left = 0
      Top = 113
      Width = 1082
      Height = 721
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 1
      object PageControl1: TPageControl
        Left = 0
        Top = 57
        Width = 1082
        Height = 664
        ActivePage = TabSheet2
        Align = alClient
        TabOrder = 0
        object TabSheet2: TTabSheet
          Caption = ' Per'#237'odos '
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 1074
            Height = 636
            Align = alClient
            ParentBackground = False
            TabOrder = 0
            object Panel9: TPanel
              Left = 103
              Top = 1
              Width = 970
              Height = 544
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object PCEx00: TPageControl
                Left = 0
                Top = 0
                Width = 970
                Height = 544
                ActivePage = TabSheet1
                Align = alClient
                TabOrder = 0
                object TabSheet1: TTabSheet
                  Caption = ' Apura'#231#227'o do ICMS '
                  object Panel1: TPanel
                    Left = 0
                    Top = 0
                    Width = 962
                    Height = 473
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object GroupBox1: TGroupBox
                      Left = 0
                      Top = 0
                      Width = 155
                      Height = 473
                      Align = alLeft
                      Caption = ' Intervalos de apura'#231#227'o: '
                      TabOrder = 0
                      object DBGE100: TdmkDBGrid
                        Left = 2
                        Top = 59
                        Width = 151
                        Height = 412
                        Align = alClient
                        Columns = <
                          item
                            Alignment = taCenter
                            Expanded = False
                            FieldName = 'DT_INI'
                            Title.Caption = 'In'#237'cio'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'DT_FIN'
                            Title.Caption = 'Final'
                            Width = 56
                            Visible = True
                          end>
                        Color = clWindow
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        Columns = <
                          item
                            Alignment = taCenter
                            Expanded = False
                            FieldName = 'DT_INI'
                            Title.Caption = 'In'#237'cio'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'DT_FIN'
                            Title.Caption = 'Final'
                            Width = 56
                            Visible = True
                          end>
                      end
                      object Panel32: TPanel
                        Left = 2
                        Top = 15
                        Width = 151
                        Height = 44
                        Align = alTop
                        TabOrder = 1
                        object BtE100: TBitBtn
                          Tag = 10078
                          Left = 12
                          Top = 2
                          Width = 120
                          Height = 40
                          Cursor = crHandPoint
                          Caption = '&Intervalo'
                          Enabled = False
                          NumGlyphs = 2
                          ParentShowHint = False
                          ShowHint = True
                          TabOrder = 0
                          OnClick = BtE100Click
                        end
                      end
                    end
                    object PCE100: TPageControl
                      Left = 155
                      Top = 0
                      Width = 807
                      Height = 473
                      ActivePage = TabSheet6
                      Align = alClient
                      TabOrder = 1
                      object TabSheet3: TTabSheet
                        Caption = 'E110: Apura'#231#227'o do ICMS - Opera'#231#245'es Pr'#243'prias'
                        object PnE110: TPanel
                          Left = 0
                          Top = 0
                          Width = 725
                          Height = 445
                          Align = alClient
                          ParentBackground = False
                          TabOrder = 0
                          object DBEdit2: TDBEdit
                            Left = 424
                            Top = 8
                            Width = 112
                            Height = 21
                            DataField = 'VL_TOT_DEBITOS'
                            TabOrder = 0
                          end
                          object DBEdit3: TDBEdit
                            Left = 424
                            Top = 32
                            Width = 112
                            Height = 21
                            DataField = 'VL_AJ_DEBITOS'
                            TabOrder = 1
                          end
                          object DBEdit4: TDBEdit
                            Left = 424
                            Top = 56
                            Width = 112
                            Height = 21
                            DataField = 'VL_TOT_AJ_DEBITOS'
                            TabOrder = 2
                          end
                          object DBEdit6: TDBEdit
                            Left = 424
                            Top = 104
                            Width = 112
                            Height = 21
                            DataField = 'VL_TOT_CREDITOS'
                            TabOrder = 4
                          end
                          object DBEdit5: TDBEdit
                            Left = 424
                            Top = 80
                            Width = 112
                            Height = 21
                            DataField = 'VL_ESTORNOS_CRED'
                            TabOrder = 3
                          end
                          object DBEdit7: TDBEdit
                            Left = 424
                            Top = 128
                            Width = 112
                            Height = 21
                            DataField = 'VL_AJ_CREDITOS'
                            TabOrder = 5
                          end
                          object DBEdit8: TDBEdit
                            Left = 424
                            Top = 152
                            Width = 112
                            Height = 21
                            DataField = 'VL_TOT_AJ_CREDITOS'
                            TabOrder = 6
                          end
                          object DBEdit9: TDBEdit
                            Left = 424
                            Top = 176
                            Width = 112
                            Height = 21
                            DataField = 'VL_ESTORNOS_DEB'
                            TabOrder = 7
                          end
                          object DBEdit10: TDBEdit
                            Left = 424
                            Top = 200
                            Width = 112
                            Height = 21
                            DataField = 'VL_SLD_CREDOR_ANT'
                            TabOrder = 8
                          end
                          object DBEdit11: TDBEdit
                            Left = 424
                            Top = 224
                            Width = 112
                            Height = 21
                            DataField = 'VL_SLD_APURADO'
                            TabOrder = 9
                          end
                          object DBEdit12: TDBEdit
                            Left = 424
                            Top = 248
                            Width = 112
                            Height = 21
                            DataField = 'VL_TOT_DED'
                            TabOrder = 10
                          end
                          object DBEdit13: TDBEdit
                            Left = 424
                            Top = 272
                            Width = 112
                            Height = 21
                            DataField = 'VL_ICMS_RECOLHER'
                            TabOrder = 11
                          end
                          object DBEdit14: TDBEdit
                            Left = 424
                            Top = 296
                            Width = 112
                            Height = 21
                            DataField = 'VL_SLD_CREDOR_TRANSPORTAR'
                            TabOrder = 12
                          end
                          object DBEdit15: TDBEdit
                            Left = 424
                            Top = 320
                            Width = 112
                            Height = 21
                            DataField = 'DEB_ESP'
                            TabOrder = 13
                          end
                          object Label7: TStaticText
                            Left = 8
                            Top = 8
                            Width = 412
                            Height = 21
                            Alignment = taRightJustify
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = 
                              '02. Valor total dos d'#233'bitos por "Sa'#237'das e presta'#231#245'es com d'#233'bito ' +
                              'do imposto"'
                            TabOrder = 14
                          end
                          object Label8: TStaticText
                            Left = 8
                            Top = 32
                            Width = 412
                            Height = 21
                            Alignment = taRightJustify
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = 
                              '03. Valor total dos ajustes a d'#233'bito decorrentes do documento fi' +
                              'scal'
                            TabOrder = 15
                          end
                          object Label9: TStaticText
                            Left = 8
                            Top = 56
                            Width = 412
                            Height = 21
                            Alignment = taRightJustify
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = '04. Valor total de "Ajustes a d'#233'bito".'
                            TabOrder = 16
                          end
                          object Label10: TStaticText
                            Left = 8
                            Top = 80
                            Width = 412
                            Height = 21
                            Alignment = taRightJustify
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = '05. Valor total de Ajustes "Estornos de cr'#233'ditos"'
                            TabOrder = 17
                          end
                          object Label11: TStaticText
                            Left = 8
                            Top = 104
                            Width = 412
                            Height = 21
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = 
                              '06. Valor total dos cr'#233'ditos por "Entradas e aquisi'#231#245'es com cr'#233'd' +
                              'ito do imposto"'
                            TabOrder = 18
                          end
                          object Label12: TStaticText
                            Left = 8
                            Top = 128
                            Width = 412
                            Height = 21
                            Alignment = taRightJustify
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = 
                              '07. Valor total dos ajustes a cr'#233'dito decorrentes do documento f' +
                              'iscal.'
                            TabOrder = 19
                          end
                          object Label13: TStaticText
                            Left = 8
                            Top = 152
                            Width = 412
                            Height = 21
                            Alignment = taRightJustify
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = '08. Valor total de "Ajustes a cr'#233'dito"'
                            TabOrder = 20
                          end
                          object Label14: TStaticText
                            Left = 8
                            Top = 176
                            Width = 412
                            Height = 21
                            Alignment = taRightJustify
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = '09. Valor total de Ajustes "Estornos de D'#233'bitos"'
                            TabOrder = 21
                          end
                          object Label15: TStaticText
                            Left = 8
                            Top = 200
                            Width = 412
                            Height = 21
                            Alignment = taRightJustify
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = 
                              '10. Valor   total   de   "Saldo   credor   do   per'#237'odo anterior' +
                              '"'
                            TabOrder = 22
                          end
                          object Label16: TStaticText
                            Left = 8
                            Top = 224
                            Width = 412
                            Height = 21
                            Alignment = taRightJustify
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = '11. Valor do saldo devedor apurado'
                            TabOrder = 23
                          end
                          object Label17: TStaticText
                            Left = 8
                            Top = 248
                            Width = 412
                            Height = 21
                            Alignment = taRightJustify
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = '12. Valor total de "Dedu'#231#245'es"'
                            TabOrder = 24
                          end
                          object Label18: TStaticText
                            Left = 8
                            Top = 272
                            Width = 412
                            Height = 21
                            Alignment = taRightJustify
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = '13. Valor total de "ICMS a recolher" (11-12)'
                            TabOrder = 25
                          end
                          object Label19: TStaticText
                            Left = 8
                            Top = 296
                            Width = 412
                            Height = 21
                            Alignment = taRightJustify
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = 
                              '14. Valor total de "Saldo credor a transportar para o per'#237'odo se' +
                              'guinte"'
                            TabOrder = 26
                          end
                          object Label20: TStaticText
                            Left = 8
                            Top = 320
                            Width = 412
                            Height = 21
                            Alignment = taRightJustify
                            AutoSize = False
                            BorderStyle = sbsSunken
                            Caption = '15. Valores recolhidos ou a recolher, extra-apura'#231#227'o.'
                            TabOrder = 27
                          end
                        end
                      end
                      object TabSheet4: TTabSheet
                        Caption = 'E111'
                        ImageIndex = 1
                        object Panel5: TPanel
                          Left = 0
                          Top = 0
                          Width = 725
                          Height = 445
                          Align = alClient
                          ParentBackground = False
                          TabOrder = 0
                          object Splitter2: TSplitter
                            Left = 1
                            Top = 282
                            Width = 723
                            Height = 5
                            Cursor = crVSplit
                            Align = alBottom
                          end
                          object Label25: TLabel
                            Left = 1
                            Top = 1
                            Width = 265
                            Height = 13
                            Align = alTop
                            Caption = 'E111: Ajuste/benef'#237'cio/incentivo da apura'#231#227'o do ICMS'
                          end
                          object DBGE111: TDBGrid
                            Left = 1
                            Top = 14
                            Width = 723
                            Height = 268
                            Align = alClient
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'COD_AJ_APUR'
                                Title.Caption = 'C'#243'd.Ajuste'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DESCR_COMPL_AJ'
                                Title.Caption = 'Descri'#231#227'o complementar do ajuste da apura'#231#227'o'
                                Width = 550
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'VL_AJ_APUR'
                                Title.Caption = 'Valor do ajuste'
                                Visible = True
                              end>
                          end
                          object Panel14: TPanel
                            Left = 1
                            Top = 287
                            Width = 723
                            Height = 157
                            Align = alBottom
                            ParentBackground = False
                            TabOrder = 1
                            object Label21: TLabel
                              Left = 1
                              Top = 1
                              Width = 297
                              Height = 13
                              Align = alTop
                              Caption = 'E112: Informa'#231#245'es adicionais do ajustes da apura'#231#227'o do ICMS'
                            end
                            object Splitter1: TSplitter
                              Left = 1
                              Top = 75
                              Width = 721
                              Height = 5
                              Cursor = crVSplit
                              Align = alTop
                            end
                            object Label22: TLabel
                              Left = 1
                              Top = 80
                              Width = 480
                              Height = 13
                              Align = alTop
                              Caption = 
                                'E113: Informa'#231#245'es adicionais do ajustes da apura'#231#227'o do ICMS - Id' +
                                'entifica'#231#227'o dos documentos fiscais'
                            end
                            object DBGE113: TDBGrid
                              Left = 1
                              Top = 93
                              Width = 721
                              Height = 63
                              Align = alClient
                              TabOrder = 0
                              TitleFont.Charset = DEFAULT_CHARSET
                              TitleFont.Color = clWindowText
                              TitleFont.Height = -12
                              TitleFont.Name = 'MS Sans Serif'
                              TitleFont.Style = []
                              Columns = <
                                item
                                  Expanded = False
                                  FieldName = 'COD_PART'
                                  Title.Caption = 'Terceiro'
                                  Width = 46
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'COD_MOD'
                                  Title.Caption = 'Mod. NF'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'SER'
                                  Title.Caption = 'S'#233'rie'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'SUB'
                                  Title.Caption = 'Sub'
                                  Width = 25
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'NUM_DOC'
                                  Title.Caption = 'Num NF'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'DT_DOC'
                                  Title.Caption = 'Dta NF'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'COD_ITEM'
                                  Title.Caption = 'C'#243'd. Item'
                                  Width = 59
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'VL_AJ_ITEM'
                                  Title.Caption = 'Valor item'
                                  Width = 68
                                  Visible = True
                                end>
                            end
                            object DBGE112: TDBGrid
                              Left = 1
                              Top = 14
                              Width = 721
                              Height = 61
                              Align = alTop
                              TabOrder = 1
                              TitleFont.Charset = DEFAULT_CHARSET
                              TitleFont.Color = clWindowText
                              TitleFont.Height = -12
                              TitleFont.Name = 'MS Sans Serif'
                              TitleFont.Style = []
                              Columns = <
                                item
                                  Expanded = False
                                  FieldName = 'NUM_DA'
                                  Title.Caption = 'Num. DA'
                                  Width = 56
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'NUM_PROC'
                                  Title.Caption = 'Num. Proc.'
                                  Width = 72
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'IND_PROC'
                                  Title.Caption = 'IOP'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'PROC'
                                  Title.Caption = 'Descri'#231#227'o resumida'
                                  Width = 200
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'TXT_COMPL'
                                  Title.Caption = 'Descri'#231#227'o complementar'
                                  Width = 333
                                  Visible = True
                                end>
                            end
                          end
                        end
                      end
                      object TabSheet5: TTabSheet
                        Caption = 'E115'
                        ImageIndex = 2
                        object Panel15: TPanel
                          Left = 0
                          Top = 0
                          Width = 725
                          Height = 445
                          Align = alClient
                          ParentBackground = False
                          TabOrder = 0
                          object Label23: TLabel
                            Left = 1
                            Top = 1
                            Width = 278
                            Height = 13
                            Align = alTop
                            Caption = 'Informa'#231#245'es adicionais da apura'#231#227'o - Valores declarat'#243'rios'
                          end
                          object DBGE115: TDBGrid
                            Left = 1
                            Top = 14
                            Width = 723
                            Height = 430
                            Align = alClient
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'COD_INF_ADIC'
                                Title.Caption = 'C'#243'digo'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'VL_INF_ADIC'
                                Title.Caption = 'Valor'
                                Width = 80
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DESCR_COMPL_AJ'
                                Title.Caption = 'Descri'#231#227'o complementar do ajuste'
                                Width = 549
                                Visible = True
                              end>
                          end
                        end
                      end
                      object TabSheet6: TTabSheet
                        Caption = 'E116'
                        ImageIndex = 3
                        object Panel8: TPanel
                          Left = 0
                          Top = 0
                          Width = 799
                          Height = 445
                          Align = alClient
                          ParentBackground = False
                          TabOrder = 0
                          object Label24: TLabel
                            Left = 1
                            Top = 1
                            Width = 249
                            Height = 13
                            Align = alTop
                            Caption = 'Obriga'#231#245'es do ICMS a recolher - Opera'#231#245'es pr'#243'prias'
                          end
                          object DBGE116: TDBGrid
                            Left = 1
                            Top = 14
                            Width = 416
                            Height = 385
                            Align = alLeft
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'COD_OR'
                                Title.Caption = 'C'#243'd. OR'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'VL_OR'
                                Title.Caption = 'Valor OR'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DT_VCTO'
                                Title.Caption = 'Vencto'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_REC'
                                Title.Caption = 'C'#243'd. Rec.'
                                Width = 58
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NUM_PROC'
                                Title.Caption = 'Num. Proc.'
                                Width = 72
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'IOP'
                                Width = 23
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'MES_REF'
                                Title.Caption = 'M'#234's ref.'
                                Width = 44
                                Visible = True
                              end>
                          end
                          object Panel13: TPanel
                            Left = 1
                            Top = 399
                            Width = 797
                            Height = 45
                            Align = alBottom
                            ParentBackground = False
                            TabOrder = 1
                            object Label3: TLabel
                              Left = 8
                              Top = 8
                              Width = 547
                              Height = 13
                              Caption = 
                                'C'#243'd. Rec.: C'#243'digo de receita referente '#224' obriga'#231#227'o, pr'#243'prio da u' +
                                'nidade da federa'#231#227'o, conforme legisla'#231#227'o estadual.'
                            end
                            object Label4: TLabel
                              Left = 8
                              Top = 24
                              Width = 474
                              Height = 13
                              Caption = 
                                'Num. Proc.: N'#250'mero do processo ou auto de infra'#231#227'o ao qual aobri' +
                                'ga'#231#227'o est'#225' vinculada, se houver.'
                            end
                          end
                          object Panel17: TPanel
                            Left = 417
                            Top = 14
                            Width = 381
                            Height = 385
                            Align = alClient
                            ParentBackground = False
                            TabOrder = 2
                            object Label5: TLabel
                              Left = 1
                              Top = 1
                              Width = 291
                              Height = 13
                              Align = alTop
                              Caption = 'Descri'#231#227'o resumida do processo que embasou o lan'#231'amento:'
                            end
                            object Label6: TLabel
                              Left = 1
                              Top = 77
                              Width = 245
                              Height = 13
                              Align = alTop
                              Caption = 'Descri'#231#227'o complementar das obriga'#231#245'es a recolher:'
                            end
                            object DBMemo1: TDBMemo
                              Left = 1
                              Top = 90
                              Width = 379
                              Height = 294
                              Align = alClient
                              DataField = 'TXT_COMPL'
                              TabOrder = 0
                            end
                            object DBMemo2: TDBMemo
                              Left = 1
                              Top = 14
                              Width = 379
                              Height = 63
                              Align = alTop
                              DataField = 'PROC'
                              TabOrder = 1
                            end
                          end
                        end
                      end
                    end
                  end
                  object Panel22: TPanel
                    Left = 0
                    Top = 473
                    Width = 962
                    Height = 43
                    Align = alBottom
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 1
                    object BtE110: TBitBtn
                      Tag = 10079
                      Left = 4
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Apura'#231#227'o'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = BtE110Click
                    end
                    object BtE111: TBitBtn
                      Tag = 10080
                      Left = 124
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = 'A&juste'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 1
                      OnClick = BtE111Click
                    end
                    object BtE116: TBitBtn
                      Tag = 10082
                      Left = 364
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Obriga'#231#245'es'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 3
                      OnClick = BtE116Click
                    end
                    object BtE115: TBitBtn
                      Tag = 10081
                      Left = 244
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Val.Declt'#243'r'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 2
                      OnClick = BtE115Click
                    end
                  end
                end
                object TabSheet7: TTabSheet
                  Caption = 'Apura'#231#227'o do IPI '
                  ImageIndex = 1
                  object Panel18: TPanel
                    Left = 0
                    Top = 0
                    Width = 962
                    Height = 473
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object GroupBox3: TGroupBox
                      Left = 0
                      Top = 0
                      Width = 229
                      Height = 473
                      Align = alLeft
                      Caption = ' E500 :: Intervalos de apura'#231#227'o: '
                      TabOrder = 0
                      object DBGE500: TdmkDBGridZTO
                        Left = 2
                        Top = 59
                        Width = 225
                        Height = 412
                        Align = alClient
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        RowColors = <>
                        Columns = <
                          item
                            Alignment = taCenter
                            Expanded = False
                            FieldName = 'DT_INI'
                            Title.Caption = 'In'#237'cio'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'DT_FIN'
                            Title.Caption = 'Final'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_IND_APUR'
                            Title.Caption = 'Ind. Apur.'
                            Width = 67
                            Visible = True
                          end>
                      end
                      object Panel33: TPanel
                        Left = 2
                        Top = 15
                        Width = 225
                        Height = 44
                        Align = alTop
                        BevelOuter = bvNone
                        TabOrder = 1
                        object BtE500: TBitBtn
                          Tag = 10078
                          Left = 56
                          Top = 1
                          Width = 120
                          Height = 40
                          Cursor = crHandPoint
                          Caption = '&Intervalo'
                          Enabled = False
                          NumGlyphs = 2
                          ParentShowHint = False
                          ShowHint = True
                          TabOrder = 0
                          OnClick = BtE500Click
                        end
                      end
                    end
                    object PCE500: TPageControl
                      Left = 229
                      Top = 0
                      Width = 733
                      Height = 473
                      ActivePage = TabSheet9
                      Align = alClient
                      TabOrder = 1
                      object TabSheet8: TTabSheet
                        Caption = 'E510 :: Consolida'#231#227'o dos valores do IPI'
                        object DBGE510: TdmkDBGridZTO
                          Left = 0
                          Top = 0
                          Width = 651
                          Height = 445
                          Align = alClient
                          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                        end
                      end
                      object TabSheet9: TTabSheet
                        Caption = 'E520 :: Apura'#231#227'o do IPI'
                        ImageIndex = 1
                        object Panel19: TPanel
                          Left = 0
                          Top = 0
                          Width = 501
                          Height = 445
                          Align = alLeft
                          BevelOuter = bvNone
                          ParentBackground = False
                          TabOrder = 0
                          object DBGE520: TdmkDBGridZTO
                            Left = 0
                            Top = 0
                            Width = 501
                            Height = 445
                            Align = alClient
                            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'VL_SD_ANT_IPI'
                                Title.Caption = 'Sdo cred ant.'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'VL_DEB_IPI'
                                Title.Caption = 'D'#233'bitos'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'VL_CRED_IPI'
                                Title.Caption = 'Cr'#233'ditos'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'VL_OD_IPI'
                                Title.Caption = 'Outros d'#233'b.'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'VL_OC_IPI'
                                Title.Caption = 'Outros cr'#233'd.'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'VL_SC_IPI'
                                Title.Caption = 'Sdo credor'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'VL_SD_IPI'
                                Title.Caption = 'Sdo devedor'
                                Visible = True
                              end>
                          end
                        end
                        object Panel20: TPanel
                          Left = 501
                          Top = 0
                          Width = 224
                          Height = 445
                          Align = alClient
                          BevelOuter = bvNone
                          ParentBackground = False
                          TabOrder = 1
                          object Label26: TLabel
                            Left = 0
                            Top = 0
                            Width = 169
                            Height = 13
                            Align = alTop
                            Caption = ' E530 :: Ajustes da Apura'#231#227'o do IPI'
                          end
                          object DBGE530: TdmkDBGridZTO
                            Left = 0
                            Top = 13
                            Width = 224
                            Height = 432
                            Align = alClient
                            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                          end
                        end
                      end
                    end
                  end
                  object Panel23: TPanel
                    Left = 0
                    Top = 473
                    Width = 962
                    Height = 43
                    Align = alBottom
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 1
                    object BtE520: TBitBtn
                      Tag = 10079
                      Left = 2
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Apura'#231#227'o'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = BtE520Click
                    end
                    object BtE530: TBitBtn
                      Tag = 10080
                      Left = 122
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = 'A&juste'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 1
                      OnClick = BtE530Click
                    end
                  end
                end
                object TabSheet10: TTabSheet
                  Caption = 'H - Invent'#225'rio'
                  ImageIndex = 2
                  object Panel21: TPanel
                    Left = 0
                    Top = 0
                    Width = 888
                    Height = 516
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object GroupBox4: TGroupBox
                      Left = 0
                      Top = 0
                      Width = 333
                      Height = 382
                      Align = alLeft
                      Caption = ' H005 :: Totais do Invent'#225'rio'
                      TabOrder = 0
                      object DBGH005: TdmkDBGridZTO
                        Left = 2
                        Top = 15
                        Width = 329
                        Height = 365
                        Align = alClient
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        RowColors = <>
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'DT_INV'
                            Title.Caption = 'Data'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'MOT_INV'
                            Title.Caption = 'Motivo'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_MOT_INV'
                            Title.Caption = 'Descri'#231#227'o do motivo'
                            Width = 103
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VL_INV'
                            Title.Caption = 'Valor'
                            Width = 85
                            Visible = True
                          end>
                      end
                    end
                    object Panel24: TPanel
                      Left = 0
                      Top = 472
                      Width = 888
                      Height = 44
                      Align = alBottom
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 1
                      object BtH010: TBitBtn
                        Tag = 10079
                        Left = 124
                        Top = 2
                        Width = 120
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Itens'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 1
                        OnClick = BtH010Click
                      end
                      object BtH005: TBitBtn
                        Tag = 10078
                        Left = 4
                        Top = 2
                        Width = 120
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Data'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 0
                        OnClick = BtH005Click
                      end
                      object BtH020: TBitBtn
                        Tag = 10080
                        Left = 244
                        Top = 2
                        Width = 120
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Complem.'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 2
                        OnClick = BtH020Click
                      end
                    end
                    object Panel25: TPanel
                      Left = 333
                      Top = 0
                      Width = 555
                      Height = 382
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 2
                      object Label28: TLabel
                        Left = 0
                        Top = 0
                        Width = 88
                        Height = 13
                        Align = alTop
                        Caption = ' H010 :: Invent'#225'rio'
                      end
                      object DBGH010: TdmkDBGridZTO
                        Left = 0
                        Top = 13
                        Width = 555
                        Height = 369
                        Align = alClient
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        RowColors = <>
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'NO_PART'
                            Title.Caption = 'Terceiro'
                            Width = 120
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'COD_ITEM'
                            Title.Caption = 'C'#243'd. item'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_PRD_TAM_COR'
                            Title.Caption = 'Nome item'
                            Width = 180
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'IND_PROP'
                            Title.Caption = 'Ind.Prop.'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'QTD'
                            Title.Caption = 'Qtde'
                            Width = 68
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'UNID'
                            Title.Caption = 'Unidade'
                            Width = 46
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VL_UNIT'
                            Title.Caption = 'Valor unit'#225'rio'
                            Width = 72
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VL_ITEM'
                            Title.Caption = 'Valor item'
                            Width = 80
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VL_ITEM_IR'
                            Title.Caption = 'Valor IR'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'TXT_COMPL'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'COD_CTA'
                            Title.Caption = 'Conta (Plano de contas)'
                            Width = 72
                            Visible = True
                          end>
                      end
                    end
                    object Panel26: TPanel
                      Left = 0
                      Top = 382
                      Width = 888
                      Height = 90
                      Align = alBottom
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 3
                      object Label27: TLabel
                        Left = 0
                        Top = 0
                        Width = 222
                        Height = 13
                        Align = alTop
                        Caption = ' H020: Informa'#231#227'o complementar do Invent'#225'rio'
                      end
                      object DBGH020: TdmkDBGridZTO
                        Left = 0
                        Top = 13
                        Width = 888
                        Height = 77
                        Align = alClient
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        RowColors = <>
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'LinArq'
                            Title.Caption = 'ID'
                            Width = 62
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'CST_ICMS'
                            Title.Caption = 'CST ICMS'
                            Width = 54
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'BC_ICMS'
                            Title.Caption = 'BC ICMS'
                            Width = 72
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'VL_ICMS'
                            Title.Caption = 'Valor ICMS'
                            Width = 72
                            Visible = True
                          end>
                      end
                    end
                  end
                end
                object TabSheet11: TTabSheet
                  Caption = 'K - Produ'#231#227'o e Estoque'
                  ImageIndex = 3
                  object Panel27: TPanel
                    Left = 0
                    Top = 473
                    Width = 962
                    Height = 43
                    Align = alBottom
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object BtK200: TBitBtn
                      Tag = 10079
                      Left = 2
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Estoque'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = BtK200Click
                    end
                    object BtK220: TBitBtn
                      Tag = 10079
                      Left = 122
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Classe*'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 1
                      OnClick = BtK220Click
                    end
                    object BtK230: TBitBtn
                      Tag = 10079
                      Left = 242
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Produ'#231#227'o'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 2
                      OnClick = BtK230Click
                    end
                    object BtVerifica: TBitBtn
                      Tag = 10079
                      Left = 482
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Verifica'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 4
                      OnClick = BtVerificaClick
                    end
                    object BtK280: TBitBtn
                      Tag = 10079
                      Left = 362
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = 'CAEE*'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 3
                      OnClick = BtK280Click
                    end
                  end
                  object PCK200: TPageControl
                    Left = 173
                    Top = 0
                    Width = 789
                    Height = 473
                    ActivePage = TabSheet13
                    Align = alClient
                    TabOrder = 1
                    OnChange = PCK200Change
                    object TabSheet12: TTabSheet
                      Caption = 'K200 - Estoque escriturado'
                      object DBGK200: TdmkDBGridZTO
                        Left = 0
                        Top = 0
                        Width = 707
                        Height = 445
                        Align = alClient
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        RowColors = <>
                        OnDblClick = DBGK200DblClick
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'NO_KndTab'
                            Title.Caption = 'Tabela'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'KndItm'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'GraGruX'
                            Title.Caption = 'C'#243'd. item'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_PRD_TAM_COR'
                            Title.Caption = 'Nome item'
                            Width = 180
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Sigla'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'QTD'
                            Title.Caption = 'Qtde'
                            Width = 68
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_PART'
                            Title.Caption = 'Terceiro'
                            Width = 300
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_IND_EST'
                            Title.Caption = 'Ind.Prop.'
                            Width = 300
                            Visible = True
                          end>
                      end
                    end
                    object TabSheet19: TTabSheet
                      Caption = 'K210 - Desmontagem'
                      ImageIndex = 1
                      object PCK21X: TPageControl
                        Left = 0
                        Top = 0
                        Width = 781
                        Height = 445
                        ActivePage = TabSheet22
                        Align = alClient
                        TabOrder = 0
                        object TabSheet22: TTabSheet
                          Caption = 'K210 / K215'
                          object Splitter6: TSplitter
                            Left = 0
                            Top = 232
                            Width = 773
                            Height = 5
                            Cursor = crVSplit
                            Align = alBottom
                            ExplicitWidth = 699
                          end
                          object DBGK210: TdmkDBGridZTO
                            Left = 0
                            Top = 0
                            Width = 773
                            Height = 232
                            Align = alClient
                            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                            OnDblClick = JanelaPorDblClick
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'IDSeq1'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'KndNSU'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'KndItm'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DT_INI_OS'
                                Title.Caption = 'Data Ini OP'
                                Width = 62
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DT_FIN_OS_Txt'
                                Title.Caption = 'Data Fim OP'
                                Width = 62
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_DOC_OS'
                                Title.Caption = 'ID OP'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_ITEM_ORI'
                                Title.Caption = 'ID Item'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_PRD_TAM_COR'
                                Title.Caption = 'Nome Item'
                                Width = 266
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD_ORI'
                                Title.Caption = 'Qtde acabada'
                                Width = 105
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'Sigla'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'OriOpeProc'
                                Visible = True
                              end>
                          end
                          object DBGK215: TdmkDBGridZTO
                            Left = 0
                            Top = 237
                            Width = 773
                            Height = 180
                            Align = alBottom
                            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                            TabOrder = 1
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                            OnDblClick = JanelaPorDblClick
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'IDSeq2'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'KndItm'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_ITEM_DES'
                                Title.Caption = 'Reduzido'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_PRD_TAM_COR'
                                Title.Caption = 'Nome / tamanho / cor'
                                Width = 336
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'Sigla'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD_DES'
                                Title.Caption = 'Qtde'
                                Width = 72
                                Visible = True
                              end>
                          end
                        end
                        object TabSheet23: TTabSheet
                          Caption = 'K270 / K275'
                          ImageIndex = 1
                          object Splitter10: TSplitter
                            Left = 0
                            Top = 232
                            Width = 699
                            Height = 5
                            Cursor = crVSplit
                            Align = alBottom
                          end
                          object DBGK270_210: TdmkDBGridZTO
                            Left = 0
                            Top = 0
                            Width = 699
                            Height = 232
                            Align = alClient
                            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                            OnDblClick = JanelaPorDblClick
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'DT_INI_AP'
                                Title.Caption = 'Data Ini AP'
                                Width = 62
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DT_FIN_AP'
                                Title.Caption = 'Data Fim AP'
                                Width = 62
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_OP_OS'
                                Title.Caption = 'ID OP'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_ITEM'
                                Title.Caption = 'ID Item'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_PRD_TAM_COR'
                                Title.Caption = 'Nome Item'
                                Width = 266
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'Sigla'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD_COR_POS'
                                Title.Caption = 'Qtde positiva'
                                Width = 105
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD_COR_NEG'
                                Title.Caption = 'Qtd negativa'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'ORIGEM'
                                Visible = True
                              end>
                          end
                          object DBGK275_215: TdmkDBGridZTO
                            Left = 0
                            Top = 237
                            Width = 699
                            Height = 180
                            Align = alBottom
                            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                            TabOrder = 1
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                            OnDblClick = JanelaPorDblClick
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'ID_Item'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_ITEM'
                                Title.Caption = 'Reduzido'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_PRD_TAM_COR'
                                Title.Caption = 'Nome / tamanho / cor'
                                Width = 336
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'Sigla'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD_COR_NEG'
                                Title.Caption = 'Qtd negativa'
                                Width = 72
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD_COR_POS'
                                Title.Caption = 'Qtd positiva'
                                Visible = True
                              end>
                          end
                        end
                      end
                    end
                    object TabSheet13: TTabSheet
                      Caption = 'K220 - *OMIEM - Outras movimenta'#231#245'es internas entre mercadorias.'
                      ImageIndex = 2
                      object Splitter8: TSplitter
                        Left = 0
                        Top = 202
                        Width = 781
                        Height = 5
                        Cursor = crVSplit
                        Align = alBottom
                        ExplicitWidth = 707
                      end
                      object Label31: TLabel
                        Left = 0
                        Top = 207
                        Width = 408
                        Height = 13
                        Align = alBottom
                        Caption = 
                          'K270 - Corre'#231#227'o de Apontamento - Outras movimenta'#231#245'es internas e' +
                          'ntre mercadorias. '
                      end
                      object DBGK220: TdmkDBGridZTO
                        Left = 0
                        Top = 0
                        Width = 781
                        Height = 202
                        Align = alClient
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        RowColors = <>
                        OnDblClick = JanelaPorDblClick
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'DT_MOV'
                            Title.Caption = 'Data mov.'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'QTD_ORI'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'QTD_DEST'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'QTD'
                            Title.Caption = 'Qtde.'
                            Width = 80
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'COD_ITEM_ORI'
                            Title.Caption = 'GGX ori'
                            Width = 44
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_PRD_TAM_COR_ORI'
                            Title.Caption = 'Nome produto origem'
                            Width = 220
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'COD_ITEM_DEST'
                            Title.Caption = 'GGX dst'
                            Width = 44
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_PRD_TAM_COR_DEST'
                            Title.Caption = 'Nome produto destino'
                            Width = 220
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'ID_SEK'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'KndItmOri'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'KndItmDst'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'KndAID'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'ESOMIEM'
                            Visible = True
                          end>
                      end
                      object DBGK270_220: TdmkDBGridZTO
                        Left = 0
                        Top = 220
                        Width = 781
                        Height = 136
                        Align = alBottom
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                        TabOrder = 1
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        RowColors = <>
                        OnDblClick = JanelaPorDblClick
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'DT_INI_AP'
                            Title.Caption = 'Data mov.'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'DT_INI_AP'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Sigla'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'QTD_COR_POS'
                            Title.Caption = 'Qtde.'
                            Width = 80
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'QTD_COR_NEG'
                            Title.Caption = 'Qtd desfeito'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'COD_ITEM'
                            Title.Caption = 'GGX ori'
                            Width = 44
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_PRD_TAM_COR'
                            Title.Caption = 'Nome produto origem'
                            Width = 220
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'IDSeq1'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'KndAID'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'COD_OP_OS'
                            Visible = True
                          end>
                      end
                      object DBGK275_220: TdmkDBGridZTO
                        Left = 0
                        Top = 356
                        Width = 781
                        Height = 89
                        Align = alBottom
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                        TabOrder = 2
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        RowColors = <>
                        OnDblClick = JanelaPorDblClick
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'Sigla'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'QTD_COR_POS'
                            Title.Caption = 'Qtde.'
                            Width = 80
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'QTD_COR_NEG'
                            Title.Caption = 'Qtd desfeito'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'COD_ITEM'
                            Title.Caption = 'GGX ori'
                            Width = 44
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_PRD_TAM_COR'
                            Title.Caption = 'Nome produto origem'
                            Width = 220
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'ID_SEK'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'IDSeq1'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'IDSeq2'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'COD_INS_SUBST'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'ESTSTabSorc'
                            Visible = True
                          end>
                      end
                    end
                    object TabSheet16: TTabSheet
                      Caption = 'K230 a 275 e 290 a 302- Produ'#231#227'o'
                      ImageIndex = 3
                      object PCKPrd: TPageControl
                        Left = 0
                        Top = 0
                        Width = 781
                        Height = 445
                        ActivePage = TabSheet20
                        Align = alClient
                        TabOrder = 0
                        object TabSheet17: TTabSheet
                          Caption = 'K230 - Itens produzidos'
                          object Splitter3: TSplitter
                            Left = 0
                            Top = 232
                            Width = 773
                            Height = 5
                            Cursor = crVSplit
                            Align = alBottom
                            ExplicitWidth = 699
                          end
                          object DBGK230: TdmkDBGridZTO
                            Left = 0
                            Top = 0
                            Width = 773
                            Height = 232
                            Align = alClient
                            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'IDSeq1'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'KndItm'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'KndNSU'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DT_INI_OP_Txt'
                                Title.Caption = 'Data Ini OP'
                                Width = 62
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DT_FIN_OP_Txt'
                                Title.Caption = 'Data Fim OP'
                                Width = 62
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_DOC_OP'
                                Title.Caption = 'ID OP'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_ITEM'
                                Title.Caption = 'ID Item'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_PRD_TAM_COR'
                                Title.Caption = 'Nome Item'
                                Width = 266
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD_ENC'
                                Title.Caption = 'Qtde acabada'
                                Width = 105
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'Sigla'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'OriOpeProc'
                                Visible = True
                              end>
                          end
                          object DBGK235: TdmkDBGridZTO
                            Left = 0
                            Top = 237
                            Width = 773
                            Height = 180
                            Align = alBottom
                            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                            TabOrder = 1
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'IDSeq2'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'KndItm'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DT_SAIDA'
                                Title.Caption = 'Data sa'#237'da'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_ITEM'
                                Title.Caption = 'Reduzido'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_PRD_TAM_COR'
                                Title.Caption = 'Nome / tamanho / cor'
                                Width = 336
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'Sigla'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD'
                                Title.Caption = 'Qtde'
                                Width = 72
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_INS_SUBST'
                                Width = 90
                                Visible = True
                              end>
                          end
                        end
                        object TabSheet18: TTabSheet
                          Caption = 'K250 - Itens produzidos por Terceiros'
                          ImageIndex = 1
                          object Splitter4: TSplitter
                            Left = 0
                            Top = 232
                            Width = 773
                            Height = 5
                            Cursor = crVSplit
                            Align = alBottom
                            ExplicitWidth = 699
                          end
                          object DBGK250: TdmkDBGridZTO
                            Left = 0
                            Top = 0
                            Width = 773
                            Height = 232
                            Align = alClient
                            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'DT_PROD'
                                Title.Caption = 'Data prod.'
                                Width = 62
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_DOC_OP'
                                Title.Caption = 'ID OP'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_ITEM'
                                Title.Caption = 'ID Item'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_PRD_TAM_COR'
                                Title.Caption = 'Nome Item'
                                Width = 266
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD'
                                Title.Caption = 'Qtde acabada'
                                Width = 105
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'Sigla'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'OriOpeProc'
                                Visible = True
                              end>
                          end
                          object DBGK255: TdmkDBGridZTO
                            Left = 0
                            Top = 237
                            Width = 773
                            Height = 180
                            Align = alBottom
                            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                            TabOrder = 1
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'ID_Item'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DT_CONS'
                                Title.Caption = 'Data sa'#237'da'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_ITEM'
                                Title.Caption = 'Reduzido'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_PRD_TAM_COR'
                                Title.Caption = 'Nome / tamanho / cor'
                                Width = 336
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'Sigla'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD'
                                Title.Caption = 'Qtde'
                                Width = 72
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_INS_SUBST'
                                Width = 90
                                Visible = True
                              end>
                          end
                        end
                        object TabSheet24: TTabSheet
                          Caption = 'K260 - Reprocessamento/reparo de produto/insumo'
                          ImageIndex = 2
                          object Splitter11: TSplitter
                            Left = 0
                            Top = 292
                            Width = 773
                            Height = 5
                            Cursor = crVSplit
                            Align = alBottom
                            ExplicitWidth = 699
                          end
                          object DBGK260: TdmkDBGridZTO
                            Left = 0
                            Top = 0
                            Width = 773
                            Height = 292
                            Align = alClient
                            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                            Columns = <
                              item
                                Alignment = taCenter
                                Expanded = False
                                FieldName = 'DT_SAIDA'
                                Title.Caption = 'Data sa'#237'da'
                                Width = 62
                                Visible = True
                              end
                              item
                                Alignment = taRightJustify
                                Expanded = False
                                FieldName = 'COD_OP_OS'
                                Title.Caption = 'ID OP/OO'
                                Width = 56
                                Visible = True
                              end
                              item
                                Alignment = taRightJustify
                                Expanded = False
                                FieldName = 'COD_ITEM'
                                Title.Caption = 'ID Item'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_PRD_TAM_COR'
                                Title.Caption = 'Produto'
                                Width = 266
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD_SAIDA'
                                Title.Caption = 'Qtd sa'#237'da'
                                Width = 72
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DT_RET_Txt'
                                Title.Caption = 'Data ret.'
                                Width = 62
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD_RET'
                                Title.Caption = 'Qtd retorno'
                                Width = 72
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'OriOpeProc'
                                Width = 48
                                Visible = True
                              end>
                          end
                          object DBGK265: TdmkDBGridZTO
                            Left = 0
                            Top = 297
                            Width = 773
                            Height = 120
                            Align = alBottom
                            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                            TabOrder = 1
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                            Columns = <
                              item
                                Alignment = taRightJustify
                                Expanded = False
                                FieldName = 'COD_ITEM'
                                Title.Caption = 'ID Item'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_PRD_TAM_COR'
                                Title.Caption = 'Produto'
                                Width = 266
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'Sigla'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD_CONS'
                                Title.Caption = 'Qtd Consumido'
                                Width = 100
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'QTD_RET'
                                Title.Caption = 'Qtd retornado'
                                Width = 100
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'OriOpeProc'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'ESTSTabSorc'
                                Width = 56
                                Visible = True
                              end>
                          end
                        end
                        object TabSheet20: TTabSheet
                          Caption = 'K270 - Corre'#231#227'o de Apontamento K230'
                          ImageIndex = 3
                          object PC_K270: TPageControl
                            Left = 0
                            Top = 0
                            Width = 773
                            Height = 417
                            ActivePage = TabSheet30
                            Align = alClient
                            TabOrder = 0
                            object TabSheet30: TTabSheet
                              Caption = 'K210'
                            end
                            object TabSheet31: TTabSheet
                              Caption = 'K220'
                              ImageIndex = 1
                            end
                            object TabSheet21: TTabSheet
                              Caption = 'K230'
                              ImageIndex = 2
                              object Splitter7: TSplitter
                                Left = 0
                                Top = 204
                                Width = 765
                                Height = 5
                                Cursor = crVSplit
                                Align = alBottom
                                ExplicitLeft = -8
                                ExplicitTop = 180
                              end
                              object DBGK270_230: TdmkDBGridZTO
                                Left = 0
                                Top = 0
                                Width = 765
                                Height = 204
                                Align = alClient
                                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                                TabOrder = 0
                                TitleFont.Charset = DEFAULT_CHARSET
                                TitleFont.Color = clWindowText
                                TitleFont.Height = -12
                                TitleFont.Name = 'MS Sans Serif'
                                TitleFont.Style = []
                                RowColors = <>
                                Columns = <
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Ini OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Fim OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_OP_OS'
                                    Title.Caption = 'ID OP'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_ITEM'
                                    Title.Caption = 'ID Item'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_PRD_TAM_COR'
                                    Title.Caption = 'Nome Item'
                                    Width = 266
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'Sigla'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_NEG'
                                    Title.Caption = 'Qtd desfeito'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_POS'
                                    Title.Caption = 'Qtde produzido'
                                    Width = 105
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'ORIGEM'
                                    Visible = True
                                  end>
                              end
                              object DBGK275_235: TdmkDBGridZTO
                                Left = 0
                                Top = 209
                                Width = 765
                                Height = 180
                                Align = alBottom
                                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                                TabOrder = 1
                                TitleFont.Charset = DEFAULT_CHARSET
                                TitleFont.Color = clWindowText
                                TitleFont.Height = -12
                                TitleFont.Name = 'MS Sans Serif'
                                TitleFont.Style = []
                                RowColors = <>
                                Columns = <
                                  item
                                    Expanded = False
                                    FieldName = 'ID_Item'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_ITEM'
                                    Title.Caption = 'Reduzido'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_PRD_TAM_COR'
                                    Title.Caption = 'Nome / tamanho / cor'
                                    Width = 336
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'Sigla'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_POS'
                                    Title.Caption = 'Qtde retorno estq'
                                    Width = 72
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_NEG'
                                    Title.Caption = 'Qtd consumido'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_INS_SUBST'
                                    Width = 90
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'ESTSTabSorc'
                                    Visible = True
                                  end>
                              end
                            end
                            object TabSheet26: TTabSheet
                              Caption = 'K250'
                              ImageIndex = 3
                              object Splitter9: TSplitter
                                Left = 0
                                Top = 204
                                Width = 765
                                Height = 5
                                Cursor = crVSplit
                                Align = alBottom
                                ExplicitLeft = -12
                                ExplicitTop = 164
                              end
                              object DBGK270_250: TdmkDBGridZTO
                                Left = 0
                                Top = 0
                                Width = 765
                                Height = 204
                                Align = alClient
                                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                                TabOrder = 0
                                TitleFont.Charset = DEFAULT_CHARSET
                                TitleFont.Color = clWindowText
                                TitleFont.Height = -12
                                TitleFont.Name = 'MS Sans Serif'
                                TitleFont.Style = []
                                RowColors = <>
                                Columns = <
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Ini OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Fim OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_OP_OS'
                                    Title.Caption = 'ID OP'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_ITEM'
                                    Title.Caption = 'ID Item'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_PRD_TAM_COR'
                                    Title.Caption = 'Nome Item'
                                    Width = 266
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'Sigla'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_NEG'
                                    Title.Caption = 'Qtd Desfeito'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_POS'
                                    Title.Caption = 'Qtde produzido'
                                    Width = 105
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'ORIGEM'
                                    Visible = True
                                  end>
                              end
                              object DBGK275_255: TdmkDBGridZTO
                                Left = 0
                                Top = 209
                                Width = 765
                                Height = 180
                                Align = alBottom
                                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                                TabOrder = 1
                                TitleFont.Charset = DEFAULT_CHARSET
                                TitleFont.Color = clWindowText
                                TitleFont.Height = -12
                                TitleFont.Name = 'MS Sans Serif'
                                TitleFont.Style = []
                                RowColors = <>
                                Columns = <
                                  item
                                    Expanded = False
                                    FieldName = 'ID_Item'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_ITEM'
                                    Title.Caption = 'Reduzido'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_PRD_TAM_COR'
                                    Title.Caption = 'Nome / tamanho / cor'
                                    Width = 336
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'Sigla'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_POS'
                                    Title.Caption = 'Retorno estq'
                                    Width = 72
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_NEG'
                                    Title.Caption = 'Qtd consumido'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_INS_SUBST'
                                    Width = 90
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'ESTSTabSorc'
                                    Visible = True
                                  end>
                              end
                            end
                            object TabSheet29: TTabSheet
                              Caption = 'K260'
                              ImageIndex = 4
                              object Splitter12: TSplitter
                                Left = 0
                                Top = 204
                                Width = 765
                                Height = 5
                                Cursor = crVSplit
                                Align = alBottom
                                ExplicitLeft = -16
                                ExplicitTop = 168
                              end
                              object DBGK270_260: TdmkDBGridZTO
                                Left = 0
                                Top = 0
                                Width = 765
                                Height = 204
                                Align = alClient
                                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                                TabOrder = 0
                                TitleFont.Charset = DEFAULT_CHARSET
                                TitleFont.Color = clWindowText
                                TitleFont.Height = -12
                                TitleFont.Name = 'MS Sans Serif'
                                TitleFont.Style = []
                                RowColors = <>
                                Columns = <
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Ini OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Fim OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_OP_OS'
                                    Title.Caption = 'ID OP'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_ITEM'
                                    Title.Caption = 'ID Item'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_PRD_TAM_COR'
                                    Title.Caption = 'Nome Item'
                                    Width = 266
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'Sigla'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_NEG'
                                    Title.Caption = 'Qtd Desfeito'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_POS'
                                    Title.Caption = 'Qtde produzido'
                                    Width = 105
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'ORIGEM'
                                    Visible = True
                                  end>
                              end
                              object DBGK275_265: TdmkDBGridZTO
                                Left = 0
                                Top = 209
                                Width = 765
                                Height = 180
                                Align = alBottom
                                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                                TabOrder = 1
                                TitleFont.Charset = DEFAULT_CHARSET
                                TitleFont.Color = clWindowText
                                TitleFont.Height = -12
                                TitleFont.Name = 'MS Sans Serif'
                                TitleFont.Style = []
                                RowColors = <>
                                Columns = <
                                  item
                                    Expanded = False
                                    FieldName = 'ID_Item'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_ITEM'
                                    Title.Caption = 'Reduzido'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_PRD_TAM_COR'
                                    Title.Caption = 'Nome / tamanho / cor'
                                    Width = 336
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'Sigla'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_POS'
                                    Title.Caption = 'Retorno estq'
                                    Width = 72
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_NEG'
                                    Title.Caption = 'Qtd consumido'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_INS_SUBST'
                                    Width = 90
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'ESTSTabSorc'
                                    Visible = True
                                  end>
                              end
                            end
                            object TabSheet32: TTabSheet
                              Caption = 'K291 a K302'
                              ImageIndex = 5
                              object Splitter17: TSplitter
                                Left = 0
                                Top = 322
                                Width = 765
                                Height = 5
                                Cursor = crVSplit
                                Align = alTop
                                ExplicitLeft = 16
                                ExplicitTop = 340
                              end
                              object Splitter18: TSplitter
                                Left = 0
                                Top = 213
                                Width = 765
                                Height = 5
                                Cursor = crVSplit
                                Align = alTop
                                ExplicitTop = 384
                              end
                              object Splitter19: TSplitter
                                Left = 0
                                Top = 104
                                Width = 765
                                Height = 5
                                Cursor = crVSplit
                                Align = alTop
                                ExplicitTop = 384
                              end
                              object DBGK270_291: TdmkDBGridZTO
                                Left = 0
                                Top = 0
                                Width = 765
                                Height = 104
                                Align = alTop
                                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                                TabOrder = 0
                                TitleFont.Charset = DEFAULT_CHARSET
                                TitleFont.Color = clWindowText
                                TitleFont.Height = -12
                                TitleFont.Name = 'MS Sans Serif'
                                TitleFont.Style = []
                                RowColors = <>
                                OnDblClick = JanelaPorDblClick
                                Columns = <
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Ini OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Fim OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_OP_OS'
                                    Title.Caption = 'ID OP'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_ITEM'
                                    Title.Caption = 'ID Item'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_PRD_TAM_COR'
                                    Title.Caption = 'Nome Item'
                                    Width = 266
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'Sigla'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_NEG'
                                    Title.Caption = 'Qtd desfeito'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_POS'
                                    Title.Caption = 'Qtde produzido'
                                    Width = 105
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'ORIGEM'
                                    Visible = True
                                  end>
                              end
                              object DBGK270_292: TdmkDBGridZTO
                                Left = 0
                                Top = 218
                                Width = 765
                                Height = 104
                                Align = alTop
                                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                                TabOrder = 1
                                TitleFont.Charset = DEFAULT_CHARSET
                                TitleFont.Color = clWindowText
                                TitleFont.Height = -12
                                TitleFont.Name = 'MS Sans Serif'
                                TitleFont.Style = []
                                RowColors = <>
                                OnDblClick = JanelaPorDblClick
                                Columns = <
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Ini OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Fim OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_OP_OS'
                                    Title.Caption = 'ID OP'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_ITEM'
                                    Title.Caption = 'ID Item'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_PRD_TAM_COR'
                                    Title.Caption = 'Nome Item'
                                    Width = 266
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'Sigla'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_NEG'
                                    Title.Caption = 'Qtd desfeito'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_POS'
                                    Title.Caption = 'Qtde produzido'
                                    Width = 105
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'ORIGEM'
                                    Visible = True
                                  end>
                              end
                              object DBGK270_301: TdmkDBGridZTO
                                Left = 0
                                Top = 109
                                Width = 765
                                Height = 104
                                Align = alTop
                                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                                TabOrder = 2
                                TitleFont.Charset = DEFAULT_CHARSET
                                TitleFont.Color = clWindowText
                                TitleFont.Height = -12
                                TitleFont.Name = 'MS Sans Serif'
                                TitleFont.Style = []
                                RowColors = <>
                                OnDblClick = JanelaPorDblClick
                                Columns = <
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Ini OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Fim OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_OP_OS'
                                    Title.Caption = 'ID OP'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_ITEM'
                                    Title.Caption = 'ID Item'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_PRD_TAM_COR'
                                    Title.Caption = 'Nome Item'
                                    Width = 266
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'Sigla'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_NEG'
                                    Title.Caption = 'Qtd desfeito'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_POS'
                                    Title.Caption = 'Qtde produzido'
                                    Width = 105
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'ORIGEM'
                                    Visible = True
                                  end>
                              end
                              object DBGK270_302: TdmkDBGridZTO
                                Left = 0
                                Top = 327
                                Width = 765
                                Height = 62
                                Align = alClient
                                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                                TabOrder = 3
                                TitleFont.Charset = DEFAULT_CHARSET
                                TitleFont.Color = clWindowText
                                TitleFont.Height = -12
                                TitleFont.Name = 'MS Sans Serif'
                                TitleFont.Style = []
                                RowColors = <>
                                OnDblClick = JanelaPorDblClick
                                Columns = <
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Ini OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'DT_INI_AP'
                                    Title.Caption = 'Data Fim OP'
                                    Width = 62
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_OP_OS'
                                    Title.Caption = 'ID OP'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'COD_ITEM'
                                    Title.Caption = 'ID Item'
                                    Width = 56
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_PRD_TAM_COR'
                                    Title.Caption = 'Nome Item'
                                    Width = 266
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'Sigla'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_NEG'
                                    Title.Caption = 'Qtd desfeito'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'QTD_COR_POS'
                                    Title.Caption = 'Qtde produzido'
                                    Width = 105
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'ORIGEM'
                                    Visible = True
                                  end>
                              end
                            end
                          end
                        end
                        object TabSheet27: TTabSheet
                          Caption = 'K290 - Produ'#231#227'o Conjunta'
                          ImageIndex = 6
                          object Splitter13: TSplitter
                            Left = 241
                            Top = 0
                            Width = 5
                            Height = 417
                            ExplicitLeft = 324
                            ExplicitTop = 8
                          end
                          object DBGK290: TdmkDBGridZTO
                            Left = 0
                            Top = 0
                            Width = 241
                            Height = 417
                            Align = alLeft
                            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                            OnDblClick = JanelaPorDblClick
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'DT_INI_OP_Txt'
                                Title.Caption = 'Data Ini OP'
                                Width = 62
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DT_FIN_OP_Txt'
                                Title.Caption = 'Data Fim OP'
                                Width = 62
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_DOC_OP'
                                Title.Caption = 'ID OP'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'OriOpeProc'
                                Visible = True
                              end>
                          end
                          object Panel38: TPanel
                            Left = 246
                            Top = 0
                            Width = 527
                            Height = 417
                            Align = alClient
                            BevelOuter = bvNone
                            ParentBackground = False
                            TabOrder = 1
                            ExplicitWidth = 453
                            object Splitter14: TSplitter
                              Left = 0
                              Top = 229
                              Width = 527
                              Height = 5
                              Cursor = crVSplit
                              Align = alTop
                              ExplicitTop = 180
                              ExplicitWidth = 237
                            end
                            object DBGK291: TdmkDBGridZTO
                              Left = 0
                              Top = 0
                              Width = 527
                              Height = 229
                              Align = alTop
                              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                              TabOrder = 0
                              TitleFont.Charset = DEFAULT_CHARSET
                              TitleFont.Color = clWindowText
                              TitleFont.Height = -12
                              TitleFont.Name = 'MS Sans Serif'
                              TitleFont.Style = []
                              RowColors = <>
                              OnDblClick = JanelaPorDblClick
                              Columns = <
                                item
                                  Expanded = False
                                  FieldName = 'ID_Item'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'COD_ITEM'
                                  Title.Caption = 'Reduzido'
                                  Width = 56
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'Sigla'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'QTD'
                                  Title.Caption = 'Qtde'
                                  Width = 72
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'NO_PRD_TAM_COR'
                                  Title.Caption = 'Nome / tamanho / cor'
                                  Width = 336
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'COD_INS_SUBST'
                                  Width = 90
                                  Visible = True
                                end>
                            end
                            object DBGK292: TdmkDBGridZTO
                              Left = 0
                              Top = 234
                              Width = 527
                              Height = 183
                              Align = alClient
                              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                              TabOrder = 1
                              TitleFont.Charset = DEFAULT_CHARSET
                              TitleFont.Color = clWindowText
                              TitleFont.Height = -12
                              TitleFont.Name = 'MS Sans Serif'
                              TitleFont.Style = []
                              RowColors = <>
                              OnDblClick = JanelaPorDblClick
                              Columns = <
                                item
                                  Expanded = False
                                  FieldName = 'ID_Item'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'COD_ITEM'
                                  Title.Caption = 'Reduzido'
                                  Width = 56
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'Sigla'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'QTD'
                                  Title.Caption = 'Qtde'
                                  Width = 72
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'NO_PRD_TAM_COR'
                                  Title.Caption = 'Nome / tamanho / cor'
                                  Width = 336
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'COD_INS_SUBST'
                                  Width = 90
                                  Visible = True
                                end>
                            end
                          end
                        end
                        object TabSheet28: TTabSheet
                          Caption = 'K300 - Produ'#231#227'o Cojunta - Terceiros'
                          ImageIndex = 7
                          object Splitter16: TSplitter
                            Left = 189
                            Top = 0
                            Width = 5
                            Height = 417
                            ExplicitLeft = 324
                            ExplicitTop = 8
                          end
                          object Panel39: TPanel
                            Left = 194
                            Top = 0
                            Width = 579
                            Height = 417
                            Align = alClient
                            BevelOuter = bvNone
                            ParentBackground = False
                            TabOrder = 0
                            object Splitter15: TSplitter
                              Left = 0
                              Top = 229
                              Width = 579
                              Height = 5
                              Cursor = crVSplit
                              Align = alTop
                              ExplicitTop = 180
                              ExplicitWidth = 237
                            end
                            object DBGK301: TdmkDBGridZTO
                              Left = 0
                              Top = 0
                              Width = 579
                              Height = 229
                              Align = alTop
                              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                              TabOrder = 0
                              TitleFont.Charset = DEFAULT_CHARSET
                              TitleFont.Color = clWindowText
                              TitleFont.Height = -12
                              TitleFont.Name = 'MS Sans Serif'
                              TitleFont.Style = []
                              RowColors = <>
                              OnDblClick = JanelaPorDblClick
                              Columns = <
                                item
                                  Expanded = False
                                  FieldName = 'ID_Item'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'COD_ITEM'
                                  Title.Caption = 'Reduzido'
                                  Width = 56
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'Sigla'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'QTD'
                                  Title.Caption = 'Qtde'
                                  Width = 72
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'NO_PRD_TAM_COR'
                                  Title.Caption = 'Nome / tamanho / cor'
                                  Width = 336
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'COD_INS_SUBST'
                                  Width = 90
                                  Visible = True
                                end>
                            end
                            object DBGK302: TdmkDBGridZTO
                              Left = 0
                              Top = 234
                              Width = 579
                              Height = 183
                              Align = alClient
                              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                              TabOrder = 1
                              TitleFont.Charset = DEFAULT_CHARSET
                              TitleFont.Color = clWindowText
                              TitleFont.Height = -12
                              TitleFont.Name = 'MS Sans Serif'
                              TitleFont.Style = []
                              RowColors = <>
                              OnDblClick = JanelaPorDblClick
                              Columns = <
                                item
                                  Expanded = False
                                  FieldName = 'ID_Item'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'COD_ITEM'
                                  Title.Caption = 'Reduzido'
                                  Width = 56
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'Sigla'
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'QTD'
                                  Title.Caption = 'Qtde'
                                  Width = 72
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'NO_PRD_TAM_COR'
                                  Title.Caption = 'Nome / tamanho / cor'
                                  Width = 336
                                  Visible = True
                                end
                                item
                                  Expanded = False
                                  FieldName = 'COD_INS_SUBST'
                                  Width = 90
                                  Visible = True
                                end>
                            end
                          end
                          object DBGK300: TdmkDBGridZTO
                            Left = 0
                            Top = 0
                            Width = 189
                            Height = 417
                            Align = alLeft
                            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                            TabOrder = 1
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            RowColors = <>
                            OnDblClick = JanelaPorDblClick
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'DT_PROD_Txt'
                                Title.Caption = 'Data Ini OP'
                                Width = 62
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'KndNSU'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'OriOpeProc'
                                Visible = True
                              end>
                          end
                        end
                      end
                    end
                    object TabSheet25: TTabSheet
                      Caption = 'K280 - Corre'#231#227'o de estoque'
                      ImageIndex = 4
                      object DBGK280: TdmkDBGridZTO
                        Left = 0
                        Top = 0
                        Width = 707
                        Height = 445
                        Align = alClient
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        RowColors = <>
                        OnDblClick = JanelaPorDblClick
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'NO_KndTab'
                            Title.Caption = 'Tabela'
                            Width = 58
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'KndCod'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'KndNSU'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'KndItm'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'DebCred'
                            Title.Caption = 'D/C'
                            Width = 28
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'COD_ITEM'
                            Title.Caption = 'C'#243'd. item'
                            Width = 56
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_PRD_TAM_COR'
                            Title.Caption = 'Nome item'
                            Width = 180
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Sigla'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'QTD_COR_NEG'
                            Title.Caption = 'Qtde neg.'
                            Width = 68
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'QTD_COR_POS'
                            Title.Caption = 'Qtd pos.'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_IND_EST'
                            Title.Caption = 'Ind.Prop.'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'DT_EST'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_OriSPEDEFDKnd'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_PART'
                            Title.Caption = 'Terceiro'
                            Width = 120
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'RegisPai'
                            Title.Caption = 'Pai'
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'RegisAvo'
                            Title.Caption = 'Av'#244
                            Visible = True
                          end>
                      end
                    end
                  end
                  object Panel30: TPanel
                    Left = 0
                    Top = 0
                    Width = 173
                    Height = 473
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 2
                    object Panel31: TPanel
                      Left = 0
                      Top = 0
                      Width = 173
                      Height = 44
                      Align = alTop
                      BevelOuter = bvNone
                      TabOrder = 0
                      object BtK100: TBitBtn
                        Tag = 10078
                        Left = 24
                        Top = 0
                        Width = 120
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Intervalo'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 0
                        OnClick = BtK100Click
                      end
                    end
                    object DBGK100: TdmkDBGridZTO
                      Left = 0
                      Top = 44
                      Width = 173
                      Height = 429
                      Align = alClient
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                      TabOrder = 1
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'DT_INI'
                          Title.Caption = 'In'#237'cio'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DT_FIN'
                          Title.Caption = 'Final'
                          Visible = True
                        end>
                    end
                  end
                end
                object TabSheet14: TTabSheet
                  Caption = 'Verifica'#231#227'o K'
                  ImageIndex = 4
                  object Splitter5: TSplitter
                    Left = 0
                    Top = 310
                    Width = 962
                    Height = 5
                    Cursor = crVSplit
                    Align = alBottom
                    ExplicitTop = 311
                    ExplicitWidth = 888
                  end
                  object DBGK_ConfG: TdmkDBGridZTO
                    Left = 0
                    Top = 41
                    Width = 962
                    Height = 269
                    Align = alClient
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -12
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    OnCellClick = DBGK_ConfGCellClick
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'GraGruX'
                        Title.Caption = 'Reduzido'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_PRD_TAM_COR'
                        Title.Caption = 'Nome / tamanho / cor'
                        Width = 256
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Sigla'
                        Width = 32
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Sdo_Ini'
                        Title.Caption = 'Sdo Inicial'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Compra'
                        Title.Caption = 'Entrada'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'EntrouDesmonte'
                        Title.Caption = 'Entrou Desmonte'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Producao'
                        Title.Caption = 'Produ'#231#227'o'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ProducaoCompartilhada'
                        Title.Caption = 'Prod. Compart.'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'EntrouClasse'
                        Title.Caption = 'Entrou classe'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ProduReforma'
                        Title.Caption = 'Prod. reforma'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SaiuDesmonte'
                        Title.Caption = 'Saiu Desmonte'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Consumo'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ConsumoCompartilhado'
                        Title.Caption = 'Cons. Compart.'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SaiuClasse'
                        Title.Caption = 'Saiu classe'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'InsumReforma'
                        Title.Caption = 'Insum. reforma'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Venda'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Final'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ETE'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Devolucao'
                        Title.Caption = 'Devolu'#231#227'o'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SubProduto'
                        Title.Caption = 'Sub-produto'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'BalancoIndev'
                        Title.Caption = 'Balan'#231'o Indev.'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Indevido'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ErrEmpresa'
                        Title.Caption = 'Erro Empresa'
                        Width = 66
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Diferenca'
                        Title.Caption = 'Diferen'#231'a'
                        Width = 66
                        Visible = True
                      end>
                  end
                  object Panel28: TPanel
                    Left = 0
                    Top = 472
                    Width = 962
                    Height = 44
                    Align = alBottom
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 1
                    object BtDifGera: TBitBtn
                      Tag = 10078
                      Left = 4
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Gera'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = BtDifGeraClick
                    end
                    object BtDifImprime: TBitBtn
                      Tag = 5
                      Left = 124
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Imprime'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 1
                      OnClick = BtDifImprimeClick
                    end
                    object BtConferencias: TBitBtn
                      Tag = 41
                      Left = 244
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Confer'#234'ncias'
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 2
                      OnClick = BtConferenciasClick
                    end
                    object BtLocVMI: TBitBtn
                      Tag = 22
                      Left = 364
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Loc.VMI'
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 3
                      OnClick = BtLocVMIClick
                    end
                  end
                  object Panel34: TPanel
                    Left = 0
                    Top = 315
                    Width = 962
                    Height = 157
                    Align = alBottom
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 2
                    object DBGPsq01: TdmkDBGridZTO
                      Left = 0
                      Top = 0
                      Width = 530
                      Height = 157
                      Align = alClient
                      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                      OnDblClick = DBGPsq02DblClick
                    end
                    object DBGPsq02: TdmkDBGridZTO
                      Left = 530
                      Top = 0
                      Width = 432
                      Height = 157
                      Align = alRight
                      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                      TabOrder = 1
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Visible = False
                      RowColors = <>
                      OnDblClick = DBGPsq02DblClick
                    end
                  end
                  object Panel40: TPanel
                    Left = 0
                    Top = 0
                    Width = 962
                    Height = 41
                    Align = alTop
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 3
                    object RGVeriKItsShow: TRadioGroup
                      Left = 0
                      Top = 0
                      Width = 297
                      Height = 41
                      Align = alLeft
                      Caption = ' Itens a mostrar: '
                      Columns = 3
                      ItemIndex = 0
                      Items.Strings = (
                        'Todos'
                        'Mat'#233'ria-prima'
                        'Insumos')
                      TabOrder = 0
                      OnClick = RGVeriKItsShowClick
                    end
                  end
                end
                object TabSheet15: TTabSheet
                  Caption = '1 - Outros'
                  ImageIndex = 5
                  object Panel35: TPanel
                    Left = 0
                    Top = 0
                    Width = 888
                    Height = 516
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Panel36: TPanel
                      Left = 0
                      Top = 472
                      Width = 888
                      Height = 44
                      Align = alBottom
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object Bt1010: TBitBtn
                        Tag = 10079
                        Left = 4
                        Top = 2
                        Width = 120
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Obriga'#231#227'o'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 0
                        OnClick = Bt1010Click
                      end
                    end
                    object Panel37: TPanel
                      Left = 0
                      Top = 0
                      Width = 888
                      Height = 472
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 1
                      object CkIND_EXP: TDBCheckBox
                        Left = 8
                        Top = 4
                        Width = 732
                        Height = 17
                        Caption = '1100 - Ocorreu averba'#231#227'o (conclus'#227'o) de exporta'#231#227'o no per'#237'odo.'
                        DataField = 'IND_EXP'
                        TabOrder = 0
                        ValueChecked = 'S'
                        ValueUnchecked = 'N'
                      end
                      object CkIND_CCRF: TDBCheckBox
                        Left = 8
                        Top = 24
                        Width = 732
                        Height = 17
                        Caption = 
                          '1200 - Existem informa'#231#245'es acerca de cr'#233'ditos de ICMS a serem co' +
                          'ntrolados, definidos pela Sefaz.'
                        DataField = 'IND_CCRF'
                        Enabled = False
                        TabOrder = 1
                        ValueChecked = 'S'
                        ValueUnchecked = 'N'
                      end
                      object CkIND_COMB: TDBCheckBox
                        Left = 8
                        Top = 44
                        Width = 732
                        Height = 17
                        Caption = 
                          '1300 - '#201' comercio varejista de combust'#237'veis com movimenta'#231#227'o e/o' +
                          'u estoque no per'#237'odo.'
                        DataField = 'IND_COMB'
                        Enabled = False
                        TabOrder = 2
                        ValueChecked = 'S'
                        ValueUnchecked = 'N'
                      end
                      object CkIND_USINA: TDBCheckBox
                        Left = 8
                        Top = 64
                        Width = 732
                        Height = 17
                        Caption = 
                          '1390 - Usinas de a'#231#250'car e/'#225'lcool - O estabelecimento '#233' produtor ' +
                          'de a'#231#250'car e/ou '#225'lcool carburante com movimenta'#231#227'o e/ou estoque n' +
                          'o per'#237'odo.'
                        DataField = 'IND_USINA'
                        Enabled = False
                        TabOrder = 3
                        ValueChecked = 'S'
                        ValueUnchecked = 'N'
                      end
                      object CkIND_VA: TDBCheckBox
                        Left = 8
                        Top = 84
                        Width = 732
                        Height = 17
                        Caption = 
                          '1400 - Sendo o registro obrigat'#243'rio em sua Unidade de Federa'#231#227'o,' +
                          ' existem informa'#231#245'es a serem prestadas neste registro.'
                        DataField = 'IND_VA'
                        Enabled = False
                        TabOrder = 4
                        ValueChecked = 'S'
                        ValueUnchecked = 'N'
                      end
                      object CkIND_EE: TDBCheckBox
                        Left = 8
                        Top = 104
                        Width = 732
                        Height = 17
                        Caption = 
                          '1500 - A empresa '#233' distribuidora de energia e ocorreu fornecimen' +
                          'to de energia el'#233'trica para consumidores de outra UF.'
                        DataField = 'IND_EE'
                        Enabled = False
                        TabOrder = 5
                        ValueChecked = 'S'
                        ValueUnchecked = 'N'
                      end
                      object CkIND_CART: TDBCheckBox
                        Left = 8
                        Top = 124
                        Width = 732
                        Height = 17
                        Caption = '1600 - Realizou vendas com Cart'#227'o de Cr'#233'dito ou de d'#233'bito.'
                        DataField = 'IND_CART'
                        Enabled = False
                        TabOrder = 6
                        ValueChecked = 'S'
                        ValueUnchecked = 'N'
                      end
                      object CkIND_FORM: TDBCheckBox
                        Left = 8
                        Top = 144
                        Width = 732
                        Height = 17
                        Caption = 
                          '1700 - Foram emitidos documentos fiscais em papel no per'#237'odo em ' +
                          'unidade da federa'#231#227'o que exija o controle de utiliza'#231#227'o de docum' +
                          'entos fiscais.'
                        DataField = 'IND_FORM'
                        Enabled = False
                        TabOrder = 7
                        ValueChecked = 'S'
                        ValueUnchecked = 'N'
                      end
                      object CkIND_AER: TDBCheckBox
                        Left = 8
                        Top = 164
                        Width = 732
                        Height = 17
                        Caption = 
                          '1800 - A empresa prestou servi'#231'os de transporte a'#233'reo de cargas ' +
                          'e de passageiros.'
                        DataField = 'IND_AER'
                        Enabled = False
                        TabOrder = 8
                        ValueChecked = 'S'
                        ValueUnchecked = 'N'
                      end
                    end
                  end
                end
              end
            end
            object Panel10: TPanel
              Left = 1
              Top = 1
              Width = 102
              Height = 544
              Align = alLeft
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object DBGCab: TdmkDBGrid
                Left = 0
                Top = 41
                Width = 102
                Height = 503
                Align = alClient
                Columns = <
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'MES_ANO'
                    Title.Caption = 'MES / ANO'
                    Width = 63
                    Visible = True
                  end>
                Color = clWindow
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'MES_ANO'
                    Title.Caption = 'MES / ANO'
                    Width = 63
                    Visible = True
                  end>
              end
              object Panel29: TPanel
                Left = 0
                Top = 0
                Width = 102
                Height = 41
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object BtPeriodo: TBitBtn
                  Tag = 10
                  Left = 0
                  Top = 0
                  Width = 102
                  Height = 41
                  Cursor = crHandPoint
                  Align = alClient
                  Caption = '&Per'#237'odo'
                  Enabled = False
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtPeriodoClick
                end
              end
            end
            object TGroupBox
              Left = 1
              Top = 571
              Width = 1072
              Height = 64
              Align = alBottom
              TabOrder = 2
              object Panel3: TPanel
                Left = 2
                Top = 15
                Width = 1068
                Height = 47
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label29: TLabel
                  Left = 149
                  Top = 2
                  Width = 461
                  Height = 17
                  Caption = 
                    '*Classe - OMIEM - Outras movimenta'#231#245'es internas entre mercadoria' +
                    's.'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clSilver
                  Font.Height = -15
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                  Transparent = True
                end
                object Label30: TLabel
                  Left = 148
                  Top = 1
                  Width = 461
                  Height = 17
                  Caption = 
                    '*Classe - OMIEM - Outras movimenta'#231#245'es internas entre mercadoria' +
                    's.'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -15
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                  Transparent = True
                end
                object Label32: TLabel
                  Left = 149
                  Top = 22
                  Width = 377
                  Height = 17
                  Caption = '*CAEE - Corre'#231#227'o de Apontamento - Estoque escriturado.'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clSilver
                  Font.Height = -15
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                  Transparent = True
                end
                object Label33: TLabel
                  Left = 148
                  Top = 21
                  Width = 377
                  Height = 17
                  Caption = '*CAEE - Corre'#231#227'o de Apontamento - Estoque escriturado.'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -15
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                  Transparent = True
                end
                object LaTempo: TLabel
                  Left = 664
                  Top = 12
                  Width = 13
                  Height = 13
                  Caption = '...'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clHotLight
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Panel2: TPanel
                  Left = 930
                  Top = 0
                  Width = 138
                  Height = 47
                  Align = alRight
                  Alignment = taRightJustify
                  BevelOuter = bvNone
                  TabOrder = 0
                  object BtSaida0: TBitBtn
                    Tag = 13
                    Left = 4
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Sa'#237'da'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtSaida0Click
                  end
                end
              end
            end
            object Panel16: TPanel
              Left = 1
              Top = 545
              Width = 1072
              Height = 26
              Align = alBottom
              TabOrder = 3
            end
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 1082
        Height = 57
        Align = alTop
        Caption = ' Empresa e per'#237'odo selecionado: '
        TabOrder = 1
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 1078
          Height = 40
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 0
            Width = 23
            Height = 13
            Caption = 'Filial:'
          end
          object Label2: TLabel
            Left = 932
            Top = 0
            Width = 60
            Height = 13
            Caption = 'M'#202'S / ANO:'
            Enabled = False
            FocusControl = DBEdit1
          end
          object EdEmpresa: TdmkEditCB
            Left = 8
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEmpresaChange
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 64
            Top = 16
            Width = 865
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object DBEdit1: TDBEdit
            Left = 932
            Top = 16
            Width = 65
            Height = 21
            DataField = 'MES_ANO'
            Enabled = False
            TabOrder = 2
          end
        end
      end
    end
    object Panel11: TPanel
      Left = 0
      Top = 0
      Width = 1082
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object GB_R: TGroupBox
        Left = 1034
        Top = 0
        Width = 48
        Height = 48
        Align = alRight
        TabOrder = 0
        object ImgTipo: TdmkImage
          Left = 8
          Top = 11
          Width = 32
          Height = 32
          Transparent = True
          SQLType = stNil
        end
      end
      object GB_L: TGroupBox
        Left = 0
        Top = 0
        Width = 216
        Height = 48
        Align = alLeft
        TabOrder = 1
        object SbImprime: TBitBtn
          Tag = 5
          Left = 4
          Top = 3
          Width = 40
          Height = 40
          NumGlyphs = 2
          TabOrder = 0
          OnClick = SbImprimeClick
        end
        object SbNovo: TBitBtn
          Tag = 6
          Left = 46
          Top = 3
          Width = 40
          Height = 40
          Enabled = False
          NumGlyphs = 2
          TabOrder = 1
          OnClick = SbNovoClick
        end
        object SbNumero: TBitBtn
          Tag = 7
          Left = 88
          Top = 3
          Width = 40
          Height = 40
          Enabled = False
          NumGlyphs = 2
          TabOrder = 2
          OnClick = SbNumeroClick
        end
        object SbNome: TBitBtn
          Tag = 8
          Left = 130
          Top = 3
          Width = 40
          Height = 40
          Enabled = False
          NumGlyphs = 2
          TabOrder = 3
          OnClick = SbNomeClick
        end
        object SbQuery: TBitBtn
          Tag = 9
          Left = 172
          Top = 3
          Width = 40
          Height = 40
          Enabled = False
          NumGlyphs = 2
          TabOrder = 4
          OnClick = SbQueryClick
        end
      end
      object GB_M: TGroupBox
        Left = 216
        Top = 0
        Width = 818
        Height = 48
        Align = alClient
        TabOrder = 2
        object LaTitulo1A: TLabel
          Left = 7
          Top = 9
          Width = 333
          Height = 32
          Caption = 'Apura'#231#227'o do ICMS e do IPI'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clGradientActiveCaption
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Visible = False
        end
        object LaTitulo1B: TLabel
          Left = 9
          Top = 11
          Width = 333
          Height = 32
          Caption = 'Apura'#231#227'o do ICMS e do IPI'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clSilver
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object LaTitulo1C: TLabel
          Left = 8
          Top = 10
          Width = 333
          Height = 32
          Caption = 'Apura'#231#227'o do ICMS e do IPI'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clHotLight
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtSaida0
    Left = 68
    Top = 12
  end
  object PMPeriodo: TPopupMenu
    OnPopup = PMPeriodoPopup
    Left = 16
    Top = 632
    object Incluinovoperiodo1: TMenuItem
      Caption = 'Inclui novo periodo'
      OnClick = Incluinovoperiodo1Click
    end
    object Excluiperiodoselecionado1: TMenuItem
      Caption = '&Exclui periodo selecionado'
      Enabled = False
      OnClick = Excluiperiodoselecionado1Click
    end
    object Itemns1: TMenuItem
      Caption = '&Item(ns)'
      Enabled = False
      Visible = False
    end
  end
  object PME111: TPopupMenu
    OnPopup = PME111Popup
    Left = 924
    Top = 368
    object Incluinovoajuste1: TMenuItem
      Caption = 'E111: &Inclui novo ajuste'
      Enabled = False
      OnClick = Incluinovoajuste1Click
    end
    object Alteraajusteselecionado1: TMenuItem
      Caption = 'E111: &Altera ajuste selecionado'
      Enabled = False
      OnClick = Alteraajusteselecionado1Click
    end
    object Excluiajusteselecionado1: TMenuItem
      Caption = 'E111: &Exclui ajuste selecionado'
      Enabled = False
      OnClick = Excluiajusteselecionado1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object E113Informaesadicionais1: TMenuItem
      Caption = 'E11&2: Informa'#231#245'es adicionais'
      object Incluinovainformaoadicional1: TMenuItem
        Caption = '&Inclui nova informa'#231#227'o adicional'
        Enabled = False
        OnClick = Incluinovainformaoadicional1Click
      end
      object Alterainformaoadicionalselecionada1: TMenuItem
        Caption = '&Altera informa'#231#227'o adicional selecionada'
        Enabled = False
        OnClick = Alterainformaoadicionalselecionada1Click
      end
      object Excluiinformaoadicionalselecionada1: TMenuItem
        Caption = '&Exclui informa'#231#227'o adicional selecionada'
        Enabled = False
        OnClick = Excluiinformaoadicionalselecionada1Click
      end
    end
    object E1121: TMenuItem
      Caption = 'E11&3: Identifica'#231#227'o de documentos fiscais'
      object Incluinovaidentificaodedocumentofiscal1: TMenuItem
        Caption = '&Inclui nova identifica'#231#227'o de documento fiscal'
        Enabled = False
        OnClick = Incluinovaidentificaodedocumentofiscal1Click
      end
      object Alteraidentificaoselecionada1: TMenuItem
        Caption = '&Altera identifica'#231#227'o selecionada'
        Enabled = False
        OnClick = Alteraidentificaoselecionada1Click
      end
      object Excluiidentificaoselecionada1: TMenuItem
        Caption = '&Exclui identifica'#231#227'o selecionada'
        Enabled = False
        OnClick = Excluiidentificaoselecionada1Click
      end
    end
  end
  object PME100: TPopupMenu
    OnPopup = PME100Popup
    Left = 928
    Top = 320
    object Incluinovointervalo1: TMenuItem
      Caption = 'Inclui &novo intervalo ICMS'
      OnClick = Incluinovointervalo1Click
    end
    object Alteraintervaloselecionado1: TMenuItem
      Caption = '&Altera intervalo ICMS selecionado'
      OnClick = Alteraintervaloselecionado1Click
    end
    object Excluiintervaloselecionado1: TMenuItem
      Caption = '&Exclui intervalo ICMS selecionado'
      OnClick = Excluiintervaloselecionado1Click
    end
  end
  object PMObrigacoes: TPopupMenu
    OnPopup = PMObrigacoesPopup
    Left = 500
    Top = 616
    object IncluinovaobrigaodoICMSarecolher1: TMenuItem
      Caption = 'Inclui nova obriga'#231#227'o do ICMS a recolher'
      Enabled = False
      OnClick = IncluinovaobrigaodoICMSarecolher1Click
    end
    object Alteraobrigaoselecionada1: TMenuItem
      Caption = '&Altera obriga'#231#227'o selecionada'
      Enabled = False
      OnClick = Alteraobrigaoselecionada1Click
    end
    object Excluiobrigaoselecionada1: TMenuItem
      Caption = '&Exclui obriga'#231#227'o selecionada'
      Enabled = False
      OnClick = Excluiobrigaoselecionada1Click
    end
  end
  object PMValDecltorio: TPopupMenu
    OnPopup = PMValDecltorioPopup
    Left = 500
    Top = 572
    object Incluinovovalordeclaratrio1: TMenuItem
      Caption = '&Inclui novo valor declarat'#243'rio'
      Enabled = False
      OnClick = Incluinovovalordeclaratrio1Click
    end
    object Alteravalordeclaratrioselecionado1: TMenuItem
      Caption = '&Altera valor declarat'#243'rio selecionado'
      Enabled = False
      OnClick = Alteravalordeclaratrioselecionado1Click
    end
    object Excluivalordeclaratrioselecionado1: TMenuItem
      Caption = '&Exclui valor declarat'#243'rio selecionado'
      Enabled = False
      OnClick = Excluivalordeclaratrioselecionado1Click
    end
  end
  object PME500: TPopupMenu
    OnPopup = PME500Popup
    Left = 928
    Top = 416
    object IncluinovointervaloIPI1: TMenuItem
      Caption = 'Inclui &novo intervalo IPI'
      OnClick = IncluinovointervaloIPI1Click
    end
    object AlteraintervaloIPIselecionado1: TMenuItem
      Caption = '&Altera intervalo IPI selecionado'
      OnClick = AlteraintervaloIPIselecionado1Click
    end
    object ExcluiintervaloIPIselecionado1: TMenuItem
      Caption = '&Exclui intervalo IPI selecionado'
      OnClick = ExcluiintervaloIPIselecionado1Click
    end
  end
  object PME530: TPopupMenu
    OnPopup = PME530Popup
    Left = 928
    Top = 464
    object IncluiajustedaapuracaodoIPI1: TMenuItem
      Caption = '&Inclui ajuste da apuracao do IPI'
      OnClick = IncluiajustedaapuracaodoIPI1Click
    end
    object AlteraajustedaapuracaodoIPIselecionado1: TMenuItem
      Caption = '&Altera ajuste da apuracao do IPI selecionado'
      OnClick = AlteraajustedaapuracaodoIPIselecionado1Click
    end
    object ExcluiajustedaapuracaodoIPIselecionado1: TMenuItem
      Caption = '&Exclui ajuste da apuracao do IPI selecionado'
      OnClick = ExcluiajustedaapuracaodoIPIselecionado1Click
    end
  end
  object PMH005: TPopupMenu
    OnPopup = PMH005Popup
    Left = 796
    Top = 320
    object Incluidatadeinventrio1: TMenuItem
      Caption = '&Inclui invent'#225'rio'
      OnClick = Incluidatadeinventrio1Click
    end
    object AlteraInventrio1: TMenuItem
      Caption = '&Altera Invent'#225'rio'
      OnClick = AlteraInventrio1Click
    end
    object Excluiinventrio1: TMenuItem
      Caption = '&Exclui invent'#225'rio'
      OnClick = Excluiinventrio1Click
    end
  end
  object PMH010: TPopupMenu
    OnPopup = PMH010Popup
    Left = 796
    Top = 368
    object Importadebalano1: TMenuItem
      Caption = 'Importa de &Balan'#231'o'
      OnClick = Importadebalano1Click
    end
    object Incluiitem1: TMenuItem
      Caption = '&Inclui item'
      OnClick = Incluiitem1Click
    end
    object Alteraitem1: TMenuItem
      Caption = '&Altera item'
      OnClick = Alteraitem1Click
    end
    object Excluiitemns1: TMenuItem
      Caption = '&Exclui item(ns)'
      OnClick = Excluiitemns1Click
    end
  end
  object PMH020: TPopupMenu
    OnPopup = PMH020Popup
    Left = 796
    Top = 416
    object Incluiinformaocomplementar1: TMenuItem
      Caption = '&Inclui informa'#231#227'o complementar'
      OnClick = Incluiinformaocomplementar1Click
    end
    object Alterainformaocomplementar1: TMenuItem
      Caption = '&Altera informa'#231#227'o complementar'
      OnClick = Alterainformaocomplementar1Click
    end
    object Excluiinformaocomplementar1: TMenuItem
      Caption = '&Exclui informa'#231#227'o complementar'
      OnClick = Excluiinformaocomplementar1Click
    end
  end
  object PMK100: TPopupMenu
    OnPopup = PMK100Popup
    Left = 856
    Top = 324
    object IncluinovointervalodeProduoeEstoque1: TMenuItem
      Caption = 'Inclui &novo intervalo de Produ'#231#227'o e Estoque'
      OnClick = IncluinovointervalodeProduoeEstoque1Click
    end
    object AlteraintervalodeProduoeEstoque1: TMenuItem
      Caption = '&Altera intervalo de Produ'#231#227'o e Estoque'
      OnClick = AlteraintervalodeProduoeEstoque1Click
    end
    object ExcluiintervalodeProduoeEstoque1: TMenuItem
      Caption = '&Exclui intervalo de Produ'#231#227'o e Estoque'
      OnClick = ExcluiintervalodeProduoeEstoque1Click
    end
    object Excluiregistrosd1: TMenuItem
      Caption = 'Exclui todos registros de movimento importados '
      OnClick = Excluiregistrosd1Click
    end
    object Importatodosregistrosdeprooduo1: TMenuItem
      Caption = 'Importa todos registros de produ'#231#227'o'
      OnClick = Importatodosregistrosdeprooduo1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object ExcluirTODASTABELASK1: TMenuItem
      Caption = 'Excluir TODAS TABELAS K '
      OnClick = ExcluirTODASTABELASK1Click
    end
  end
  object PMK200: TPopupMenu
    OnPopup = PMK200Popup
    Left = 856
    Top = 372
    object ImportaK200_1: TMenuItem
      Caption = 'Importa de &Balan'#231'o (K200 e K280)'
      OnClick = ImportaK200_1Click
    end
    object IncluiK200_1: TMenuItem
      Caption = '&Inclui item'
      OnClick = IncluiK200_1Click
    end
    object AlteraK200_1: TMenuItem
      Caption = '&Altera item'
      OnClick = AlteraK200_1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object ExcluiK200_1: TMenuItem
      Caption = '&Exclui item(ns) (K200)'
      OnClick = ExcluiK200_1Click
    end
    object ExcluiK200_280_1: TMenuItem
      Caption = '&Exclui todos itens do estoque e os K2&80 retativos ao estoque'
      OnClick = ExcluiK200_280_1Click
    end
  end
  object PMK220: TPopupMenu
    OnPopup = PMK220Popup
    Left = 856
    Top = 420
    object ImportaK220_1: TMenuItem
      Caption = 'Importa &movimentos'
      OnClick = ImportaK220_1Click
    end
    object IncluiK220_1: TMenuItem
      Caption = '&Inclui item'
      OnClick = IncluiK220_1Click
    end
    object AlteraK220_1: TMenuItem
      Caption = '&Altera item'
      OnClick = AlteraK220_1Click
    end
    object ExcluiK220_1: TMenuItem
      Caption = '&Exclui item'
      OnClick = ExcluiK220_1Click
    end
    object ExcluiK220_280_1: TMenuItem
      Caption = 
        'Exclui todos itens de classe (incluindo os K2&80 relativos a cla' +
        'sse)'
      OnClick = ExcluiK220_280_1Click
    end
  end
  object PMK230: TPopupMenu
    OnPopup = PMK230Popup
    Left = 856
    Top = 468
    object ImportaK230_1: TMenuItem
      Caption = 'Importa &movimentos'
      OnClick = ImportaK230_1Click
    end
    object Vaiparajanelademovimento1: TMenuItem
      Caption = 'Vai para janela de movimento'
    end
    object IncluiK230_1: TMenuItem
      Caption = '&Inclui item'
      object Desmontagemdemercadoria1: TMenuItem
        Caption = 'K2&1X - Desmontagem de mercadoria'
        OnClick = Desmontagemdemercadoria1Click
        object IncluiK210_1: TMenuItem
          Caption = '210 - Item de &origem'
          OnClick = IncluiK210_1Click
        end
        object IncluiK215_1: TMenuItem
          Caption = '215 - Item de &destino'
          OnClick = IncluiK215_1Click
        end
      end
      object ProduoIsolada1: TMenuItem
        Caption = 'K2&3X - Produ'#231#227'o Isolada no Estabelecimento'
        OnClick = ProduoIsolada1Click
        object IncluiK230_2: TMenuItem
          Caption = '230 - Itens Produzidos'
          OnClick = IncluiK230_2Click
        end
        object IncluiK235_2: TMenuItem
          Caption = '235 - Insumos Consumidos'
          OnClick = IncluiK235_2Click
        end
      end
      object N25X250Industrializaoefetuadaporterceiros1: TMenuItem
        Caption = 'K2&5X - Industrializa'#231#227'o Isolada efetuada por terceiros'
        object N250Itensproduzidos1: TMenuItem
          Caption = '25&0 - Itens  produzidos'
          OnClick = N250Itensproduzidos1Click
        end
        object N255InsumosConsumidos1: TMenuItem
          Caption = '255 - Insumos Consumidos'
          OnClick = N255InsumosConsumidos1Click
        end
      end
      object N29XProduoconjuntanoestabelecimento1: TMenuItem
        Caption = 'K2&9X - Produ'#231#227'o conjunta no estabelecimento'
        object K290Ordemdeproduo1: TMenuItem
          Caption = 'K29&0 - Ordem de produ'#231#227'o'
          OnClick = K290Ordemdeproduo1Click
        end
        object K291ProduoconjuntaItensproduzidos1: TMenuItem
          Caption = 'K29&1 - Item produzido'
          OnClick = K291ProduoconjuntaItensproduzidos1Click
        end
        object K292ProduoconjuntaItensconsumidos1: TMenuItem
          Caption = 'K29&2 - Item consumido'
          OnClick = K292ProduoconjuntaItensconsumidos1Click
        end
      end
      object K30XProduoconjuntaIndustrializaoefetuadaporterceiro1: TMenuItem
        Caption = 
          'K30X - Produ'#231#227'o conjunta - Industrializa'#231#227'o efetuada por terceir' +
          'o'
        object K300Ordemdeproduo1: TMenuItem
          Caption = 'K30&0 - Ordem de produ'#231#227'o'
          OnClick = K300Ordemdeproduo1Click
        end
        object K301Itemproduzidos1: TMenuItem
          Caption = 'K30&1 - Item produzido'
          OnClick = K301Itemproduzidos1Click
        end
        object K302Itemconsumido1: TMenuItem
          Caption = 'K30&2 - Item consumido'
          OnClick = K302Itemconsumido1Click
        end
      end
      object K27XCorreodeapontamentodeproduo1: TMenuItem
        Caption = 'K27X - Corre'#231#227'o de apontamento de produ'#231#227'o'
        object K270K210K220K230K250K260K291K292K301eK3021: TMenuItem
          Caption = '27&0 - K210, K220, K230, K250, K260, K291, K292, K301 e K302'
          OnClick = K270K210K220K230K250K260K291K292K301eK3021Click
        end
        object K275K215K220K235K255EK2651: TMenuItem
          Caption = 'K275 - K215, K220, K235, K255 E K265'
          OnClick = K275K215K220K235K255EK2651Click
        end
      end
    end
    object AlteraK230_1: TMenuItem
      Caption = '&Altera item'
      object AlteraK2X0_1: TMenuItem
        Caption = 'AlteraK2X0_1'
        OnClick = AlteraItemGenerico
      end
      object AlteraK2X5_1: TMenuItem
        Caption = 'AlteraK2X5_1'
        OnClick = AlteraItemGenerico
      end
      object AlteraK2X5_2: TMenuItem
        Caption = 'AlteraK2X5_2'
        Visible = False
        OnClick = AlteraItemGenerico
      end
      object AlteraK2X5_3: TMenuItem
        Caption = 'AlteraK2X5_3'
        Visible = False
      end
    end
    object ExcluiK230_1: TMenuItem
      Caption = '&Exclui item (por registro)'
      object K2301: TMenuItem
        Caption = 'K230'
        OnClick = K2301Click
      end
      object K2351: TMenuItem
        Caption = 'K235'
        OnClick = K2351Click
      end
      object K2501: TMenuItem
        Caption = 'K250'
        OnClick = K2501Click
      end
      object K2551: TMenuItem
        Caption = 'K255'
        OnClick = K2551Click
      end
      object K2601: TMenuItem
        Caption = 'K260'
        OnClick = K2601Click
      end
      object K2651: TMenuItem
        Caption = 'K265'
        OnClick = K2651Click
      end
      object K2901: TMenuItem
        Caption = 'K290'
        OnClick = K2901Click
      end
      object K2911: TMenuItem
        Caption = 'K291'
        OnClick = K2911Click
      end
      object K2921: TMenuItem
        Caption = 'K292'
        OnClick = K2921Click
      end
      object K3001: TMenuItem
        Caption = 'K300'
        OnClick = K3001Click
      end
      object K3011: TMenuItem
        Caption = 'K301'
        OnClick = K3011Click
      end
      object K3021: TMenuItem
        Caption = 'K302'
        OnClick = K3021Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object odos1: TMenuItem
        Caption = 'Todos'
        OnClick = odos1Click
      end
    end
    object Excluisegmentointeiroimportado1: TMenuItem
      Caption = 'Exclui segmento inteiro (importado)'
      OnClick = Excluisegmentointeiroimportado1Click
    end
    object ExcluiK230_280_1: TMenuItem
      Caption = 
        'Exclui todos itens de produ'#231#227'o (incluindo os K2&80 relativos '#224' p' +
        'rodu'#231#227'o)'
      OnClick = ExcluiK230_280_1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 328
    Top = 520
    object BlocoKRegistrosK2001: TMenuItem
      Caption = 'Bloco K - Registros K200/K280'
      OnClick = BlocoKRegistrosK2001Click
    end
    object BlocoKRegistrosK2201: TMenuItem
      Caption = 'Bloco K - Registros K220'
      OnClick = BlocoKRegistrosK2201Click
    end
    object ProduoK230aK2551: TMenuItem
      Caption = 'Produ'#231#227'o (K230 a K255)'
      OnClick = ProduoK230aK2551Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Erroempresaitens1: TMenuItem
      Caption = 'Erro empresa - itens'
      OnClick = Erroempresaitens1Click
    end
  end
  object PMK280: TPopupMenu
    OnPopup = PMK280Popup
    Left = 796
    Top = 468
    object IncluiK280_1: TMenuItem
      Caption = '&Inclui item'
      OnClick = IncluiK280_1Click
    end
    object AlteraK280_1: TMenuItem
      Caption = '&Altera item'
      OnClick = AlteraK280_1Click
    end
    object ExcluiK280_1: TMenuItem
      Caption = '&Exclui item(ns)'
      OnClick = ExcluiK280_1Click
    end
  end
  object PMConferencias: TPopupMenu
    Left = 388
    Top = 628
    object Movimentaies1: TMenuItem
      Caption = '&Movimenta'#231#245'es'
      OnClick = Movimentaies1Click
    end
    object BaixaeestoquedeInNatura1: TMenuItem
      Caption = '&Baixa x estoque de In Natura'
      OnClick = BaixaeestoquedeInNatura1Click
    end
    object Outrasbaixaseestoque1: TMenuItem
      Caption = '&Outras baixas x estoque'
      OnClick = Outrasbaixaseestoque1Click
    end
    object Geraoxestoque1: TMenuItem
      Caption = '&Gera'#231#227'o x estoque'
      OnClick = Geraoxestoque1Click
    end
    object VSxPesagemPQ1: TMenuItem
      Caption = 'VS x Pesagem PQ'
      OnClick = VSxPesagemPQ1Click
    end
  end
end
