unit UnEfdIcmsIpi_PF_v03_0_2_a;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) dmkEdit, dmkRadioGroup,
  dmkMemo, dmkCheckGroup, UnDmkProcFunc, TypInfo, UnMyObjects,
  dmkEditDateTimePicker, SPED_Listas, UnEfdIcmsIpi_PF, UMySQLDB, UnMyVCLref;

type
  TUnEfdIcmsIpi_PF_v03_0_2_a = class(TObject)
  private
    { Private declarations }
    procedure DefineIDSeq1(const Tabela: String; const ImporExpor, AnoMes,
              Empresa, PeriApu: Integer; var BNCI_IDSeq, IDSeq: Integer);
    procedure DefineIDSeq2(const Tabela: String; const ImporExpor, AnoMes,
              Empresa, PeriApu: Integer; var BNCI_IDSeq, IDSeq: Integer);
    procedure ConfirmaIDSeq(const REG: String; const Resultado: Boolean; const
              RecordsAffected: Integer; const Campos: array of String; const
              Valores: array of Integer; const MeAviso: TMemo; var IDSeq1:
              Integer);
  public
    { Public declarations }
    function  InsereItemAtual_K210_Fast(const ImporExpor, AnoMes, Empresa,
              PeriApu, MovimID, Codigo, MovimCod, Controle: Integer; const
              DataIni, DataFim, DtMovim, DtCorrApo: TDateTime; const GraGruX,
              ClientMO, FornecMO, EntiSitio: Integer; const Qtde: Double; const
              OrigemOpeProc: TOrigemOpeProc; const OriIDKnd: TOrigemIDKnd;
              const DiaFim: TDateTime; const TipoPeriodoFiscal:
              TTipoPeriodoFiscal; const MeAviso: TMemo; const SrcNivel1:
              Integer; var IDSeq1, F_BNCI_IDSeq1_K210: Integer; var ListaIDSeq1:
              TMyArrOf2ArrOfInt;
              Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K210(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, Controle: Integer; const DataIni,
              DataFim, DtMovim, DtCorrApo: TDateTime; const GraGruX, ClientMO,
              FornecMO, EntiSitio: Integer; const Qtde: Double; const
              OrigemOpeProc: TOrigemOpeProc; const OriIDKnd: TOrigemIDKnd;
              const DiaFim: TDateTime; const TipoPeriodoFiscal:
              TTipoPeriodoFiscal; const MeAviso: TMemo; var IDSeq1: Integer;
              Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K215_Fast(const ImporExpor, AnoMes, Empresa, PeriApu,
              IDSeq1: Integer; Data, DtMovim, DtCorrApo: TDateTime; GraGruX:
              Integer; Qtde: Double; COD_INS_SUBST: String; ID_Item, MovimID,
              Codigo, MovimCod, Controle, ClientMO, FornecMO, EntiSitio:
              Integer; OrigemOpeProc: TOrigemOpeProc; OriIDKnd: TOrigemIDKnd;
              OriESTSTabSorc: TEstqSPEDTabSorc; TipoPeriodoFiscal:
              TTipoPeriodoFiscal; MeAviso: TMemo; F_BNCI_IDSeq2_K215: Integer): Boolean;
    function  InsereItemAtual_K215(const ImporExpor, AnoMes, Empresa, PeriApu,
              IDSeq1: Integer; Data, DtMovim, DtCorrApo: TDateTime; GraGruX:
              Integer; Qtde: Double; COD_INS_SUBST: String; ID_Item, MovimID,
              Codigo, MovimCod, Controle, ClientMO, FornecMO, EntiSitio:
              Integer; OrigemOpeProc: TOrigemOpeProc; OriIDKnd: TOrigemIDKnd;
              OriESTSTabSorc: TEstqSPEDTabSorc; TipoPeriodoFiscal:
              TTipoPeriodoFiscal; MeAviso: TMemo): Boolean;
              //...
    function  InsereItemAtual_K230_Fast(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, Controle:
              Integer; const DataIni, DataFim, DtMovim, DtCorrApo: TDateTime;
              const GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde:
              Double; const OrigemOpeProc: TOrigemOpeProc; DiaFim: TDateTime;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso: TMemo;
              var IDSeq1, F_BNCI_IDSeq1_K230: Integer; Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K230(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, Controle:
              Integer; const DataIni, DataFim, DtMovim, DtCorrApo: TDateTime;
              const GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde:
              Double; const OrigemOpeProc: TOrigemOpeProc; DiaFim: TDateTime;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso: TMemo;
              var IDSeq1: Integer; Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K235_Fast(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1:
              Integer; Data, DtCorrApo:
              TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
              ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
              EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
              TEstqSPEDTabSorc; const TipoPeriodoFiscal:
              TTipoPeriodoFiscal; const MeAviso: TMemo; var F_BNCI_IDSeq2_K235:
              Integer): Boolean;
    function  InsereItemAtual_K235(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1:
              Integer; Data, DtCorrApo:
              TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
              ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
              EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
              TEstqSPEDTabSorc; const TipoPeriodoFiscal:
              TTipoPeriodoFiscal; const MeAviso: TMemo): Boolean;
    function  InsereItemAtual_K250_Fast(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, Controle:
              Integer; const DtHrFimOpe, DtMovim, DtCorrApo: TDateTime; const
              GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde:
              Double; const OrigemOpeProc: TOrigemOpeProc; DiaFim: TDateTime;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso:
              TMemo; var IDSeq1, F_BNCI_IDSeq1_K250: Integer; Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K250(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, Controle:
              Integer; const DtHrFimOpe, DtMovim, DtCorrApo: TDateTime; const
              GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde:
              Double; const OrigemOpeProc: TOrigemOpeProc; DiaFim: TDateTime;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso:
              TMemo; var IDSeq1: Integer; Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K255_Fast(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1:
              Integer; Data, DtCorrApo:
              TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
              ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
              EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
              TEstqSPEDTabSorc; const TipoPeriodoFiscal:
              TTipoPeriodoFiscal; const MeAviso: TMemo; var F_BNCI_IDSeq2_K255:
              Integer): Boolean;
    function  InsereItemAtual_K255(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1:
              Integer; Data, DtCorrApo:
              TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
              ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
              EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
              TEstqSPEDTabSorc; const TipoPeriodoFiscal:
              TTipoPeriodoFiscal; const MeAviso: TMemo): Boolean;
    function  InsereItemAtual_K260(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, Controle:
              Integer; const DtHrAberto, DtHrFimOpe, DtMovim, DtCorrApoProd,
              DtCorrApoCons: TDateTime; const GraGruX(*, CtrlDst, CtrlBxa*),
              ClientMO, FornecMO, EntiSitio: Integer; const QtdeProd, QtdeCons:
              Double; const OrigemOpeProc: TOrigemOpeProc; const TipoPeriodoFiscal:
              TTipoPeriodoFiscal; const MeAviso: TMemo; var IDSeq1: Integer;
              const Agrega: Boolean = True): Boolean;
    function  InsereItemAtual_K265(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1:
              Integer; Data, DtCorrApo: TDateTime;
              GraGruX: Integer; QtdeCons, QtdeRet: Double; COD_INS_SUBST:
              String; ID_Item, MovimID, Codigo, MovimCod, Controle: Integer;
              OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso:
              TMemo): Boolean;
    function  InsereItemAtual_K270_Fast(const ImporExpor, AnoMes, Empresa, PeriApu:
              Integer; const RegOri: String; const Data, DtCorrApo:
              TDateTime; const MovimID, Codigo, MovimCod, Controle, GraGruX:
              Integer; const Qtde: Double; const SPED_EFD_KndRegOrigem:
              TSPED_EFD_KndRegOrigem; OrigemOpeProc: TOrigemOpeProc;
              OrigemIDKnd: TOrigemIDKnd; const OriSPEDEFDKnd: TOrigemSPEDEFDKnd;
              TabProdOuSubPrd: TTabProdOuSubPrd; MeAviso: TMemo; var IDSeq1:
              Integer): Boolean;
    function  InsereItemAtual_K270(const ImporExpor, AnoMes, Empresa, PeriApu:
              Integer; const RegOri: String; const Data, DtCorrApo:
              TDateTime; const MovimID, Codigo, MovimCod, Controle, GraGruX:
              Integer; const Qtde: Double; const SPED_EFD_KndRegOrigem:
              TSPED_EFD_KndRegOrigem; OrigemOpeProc: TOrigemOpeProc;
              OrigemIDKnd: TOrigemIDKnd; const OriSPEDEFDKnd: TOrigemSPEDEFDKnd;
              TabProdOuSubPrd: TTabProdOuSubPrd; MeAviso: TMemo; var IDSeq1:
              Integer): Boolean;
    function  InsereItemAtual_K275_Fast(const ImporExpor, AnoMes, Empresa, PeriApu:
              Integer; RegOri: String;  MovimID, Codigo, MovimCod,
              Controle, GraGruX, IDSeq1: Integer; Qtde: Double;
              COD_INS_SUBST: String; SPED_EFD_KndRegOrigem:
              TSPED_EFD_KndRegOrigem; ESTSTabSorc: TEstqSPEDTabSorc;
              OrigemOpeProc: TOrigemOpeProc; OriIDKnd: TOrigemIDKnd;
              const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd; MeAviso: TMemo): Boolean;
    function  InsereItemAtual_K275(const ImporExpor, AnoMes, Empresa, PeriApu:
              Integer; RegOri: String;  MovimID, Codigo, MovimCod,
              Controle, GraGruX, IDSeq1: Integer; Qtde: Double;
              COD_INS_SUBST: String; SPED_EFD_KndRegOrigem:
              TSPED_EFD_KndRegOrigem; ESTSTabSorc: TEstqSPEDTabSorc;
              OrigemOpeProc: TOrigemOpeProc; OriIDKnd: TOrigemIDKnd;
              const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd; MeAviso: TMemo): Boolean;
    function  InsereItensAtuais_K280_Old(BalTab: TEstqSPEDTabSorc;
              TipoPeriodoFiscal: TTipoPeriodoFiscal;
              ImporExpor, AnoMes, Empresa, K100: Integer; const RegPai,
              RegAvo: String; const DataSPED, DtCorrApo: TDateTime;
              const MovimID, Codigo, MovimCod, Controle, GraGruX, ClientMO,
              FornecMO, EntiSitio: Integer; LocalKnd: TSPEDLocalKnd;
              const Qtde: Double; const OrigemBalID: TSPED_EFD_Bal;
              const SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
              const OriESTSTabSorc: TEstqSPEDTabSorc; const OriOrigemOpeProc:
              TOrigemOpeProc; const OriOrigemIDKnd: TOrigemIDKnd;
              const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd;
              const Agrega: Boolean; var LinArq: Integer): Boolean;
    function  InsereItensAtuais_K280_New_Fast(BalTab: TEstqSPEDTabSorc;
              TipoPeriodoFiscal: TTipoPeriodoFiscal;
              ImporExpor, AnoMes, Empresa, K100_PeriApu: Integer; const RegPai,
              RegAvo: String; const DataSPED, DtCorrApo: TDateTime;
              const MovimID, Codigo, MovimCod, Controle, GraGruX, ClientMO,
              FornecMO, EntiSitio: Integer; LocalKnd: TSPEDLocalKnd;
              const Qtde: Double; const OrigemBalID: TSPED_EFD_Bal;
              const SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
              const OriESTSTabSorc: TEstqSPEDTabSorc; const OriOrigemOpeProc:
              TOrigemOpeProc; const OriOrigemIDKnd: TOrigemIDKnd;
              const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd;
              const Agrega: Boolean; const MeAviso: TMemo): Boolean;
    function  InsereItensAtuais_K280_New(BalTab: TEstqSPEDTabSorc;
              TipoPeriodoFiscal: TTipoPeriodoFiscal;
              ImporExpor, AnoMes, Empresa, K100_PeriApu: Integer; const RegPai,
              RegAvo: String; const DataSPED, DtCorrApo: TDateTime;
              const MovimID, Codigo, MovimCod, Controle, GraGruX, ClientMO,
              FornecMO, EntiSitio: Integer; LocalKnd: TSPEDLocalKnd;
              const Qtde: Double; const OrigemBalID: TSPED_EFD_Bal;
              const SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
              const OriESTSTabSorc: TEstqSPEDTabSorc; const OriOrigemOpeProc:
              TOrigemOpeProc; const OriOrigemIDKnd: TOrigemIDKnd;
              const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd;
              const Agrega: Boolean; const MeAviso: TMemo): Boolean;
    function  InsereItemAtual_K290_Fast(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, CodDocOP(*, Controle*): Integer; const
              DataIni, DataFim, (*DtMovim,*) DtCorrApo: TDateTime; const (*GraGruX,*)
              ClientMO, FornecMO, EntiSitio: Integer; (*const Qtde: Double;*)
              const OrigemOpeProc: TOrigemOpeProc; const DiaFim: TDateTime;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo; var IDSeq1,
              F_BNCI_IDSeq1_K290: Integer): Boolean;
    function  InsereItemAtual_K290(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, CodDocOP(*, Controle*): Integer; const
              DataIni, DataFim, (*DtMovim,*) DtCorrApo: TDateTime; const (*GraGruX,*)
              ClientMO, FornecMO, EntiSitio: Integer; (*const Qtde: Double;*)
              const OrigemOpeProc: TOrigemOpeProc; const DiaFim: TDateTime;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo; var IDSeq1: Integer):
              Boolean;
    function  InsereItemAtual_K291_Fast(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
              JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod,
              PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
              GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod,
              Controle, ClientMO, FornecMO, EntiSitio: Integer; OrigemOpeProc:
              TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc; const
              TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo; var F_BNCI_IDSeq2_K291:
              Integer): Boolean;
    function  InsereItemAtual_K291(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
              JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod,
              PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
              GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod,
              Controle, ClientMO, FornecMO, EntiSitio: Integer; OrigemOpeProc:
              TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc; const
              TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
    function  InsereItemAtual_K292_Fast(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
              JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod,
              PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
              GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod,
              Controle, ClientMO, FornecMO, EntiSitio: Integer; OrigemOpeProc:
              TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc; const
              TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo; var F_BNCI_IDSeq2_K292:
              Integer): Boolean;
    function  InsereItemAtual_K292(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
              JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod,
              PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
              GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod,
              Controle, ClientMO, FornecMO, EntiSitio: Integer; OrigemOpeProc:
              TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc; const
              TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
    function  InsereItemAtual_K300_Fast(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, CodDocOP(*, Controle*): Integer; const
              DataIni, DataFim, DtMovim, DtCorrApo: TDateTime; const (*GraGruX,*)
              ClientMO, FornecMO, EntiSitio: Integer; (*const Qtde: Double;*)
              const OrigemOpeProc: TOrigemOpeProc; const DiaFim: TDateTime;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo; var IDSeq1,
              F_BNCI_IDSeq1_K300: Integer): Boolean;
    function  InsereItemAtual_K300(const ImporExpor, AnoMes, Empresa, PeriApu,
              MovimID, Codigo, MovimCod, CodDocOP(*, Controle*): Integer; const
              DataIni, DataFim, DtMovim, DtCorrApo: TDateTime; const (*GraGruX,*)
              ClientMO, FornecMO, EntiSitio: Integer; (*const Qtde: Double;*)
              const OrigemOpeProc: TOrigemOpeProc; const DiaFim: TDateTime;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo; var IDSeq1: Integer):
              Boolean;
    function  InsereItemAtual_K301_Fast(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
              JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod,
              PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
              GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod,
              Controle, ClientMO, FornecMO,
              EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
              TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
              const TabSorc: TEstqSPEDTabSorc; const MeAviso: TMemo;
              var F_BNCI_IDSeq2_K301: Integer): Boolean;
    function  InsereItemAtual_K301(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
              JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod,
              PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
              GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod,
              Controle, ClientMO, FornecMO,
              EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
              TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
              const TabSorc: TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
    function  InsereItemAtual_K302_Fast(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
              JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod,
              PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
              GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod,
              Controle, ClientMO, FornecMO, EntiSitio: Integer; OrigemOpeProc:
              TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc; const
              TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo; var F_BNCI_IDSeq2_K302:
              Integer): Boolean;
    function  InsereItemAtual_K302(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
              JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod,
              PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
              GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod,
              Controle, ClientMO, FornecMO, EntiSitio: Integer; OrigemOpeProc:
              TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc; const
              TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
              TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
              // ...
    procedure VerificaDtCorrApo(REG: String; DtCorrApo: TDateTime;
              OrigemOpeProc: TOrigemOpeProc);

  end;
var
  EfdIcmsIpi_PF_v03_0_2_a: TUnEfdIcmsIpi_PF_v03_0_2_a;
  //
  VAR_INTENS_EXECUTADOS_Texto: String;
  VAR_INTENS_EXECUTADOS_Count: Integer;
  FListaExec: array of array[0..1] of Integer;
  //
  VAR_AVISA_FALTA_FAZER_InsereItemAtual_K270_Fast,
  VAR_AVISA_FALTA_FAZER_InsereItemAtual_K275_Fast,
  VAR_AVISA_FALTA_FAZER_InsereItemAtual_K300_Fast,
  VAR_AVISA_FALTA_FAZER_InsereItemAtual_K301_Fast,
  VAR_AVISA_FALTA_FAZER_InsereItemAtual_K302_Fast,
  VAR_AVISA_FALTA_FAZER_InsereItensAtuais_K280_New_Fast: Boolean;

implementation

uses UnMLAGeral, MyDBCheck, DmkDAC_PF, Module, ModuleGeral, UnDmkWeb, Restaura2,
  UMySQLModule, UnGrade_PF, ModProd, ModAppGraG1;

const
  _IGNORE_ = True;

{ TUnEfdIcmsIpi_PF_v03_0_2_a }

procedure TUnEfdIcmsIpi_PF_v03_0_2_a.ConfirmaIDSeq(const REG: String; const
  Resultado: Boolean; const RecordsAffected: Integer; const Campos:
  array of String; const Valores: array of Integer; const MeAviso: TMemo;
  var IDSeq1: Integer);
var
  x: String;
  I: Integer;
begin
  if Resultado then
  begin
    if RecordsAffected = 0 then
    begin
      x := '';
      for I := Low(Campos) to High(Campos) do
        x := x + ' (' + Campos[I] + ': ' + Geral.FF0(Valores[I]) + ')';
      MeAviso.Lines.Add(REG + ' j� importado > ' + x);
      IDSeq1 := -1;
    end;
  end else
    IDSeq1 := -1;
end;

procedure TUnEfdIcmsIpi_PF_v03_0_2_a.DefineIDSeq1(const Tabela: String; const
  ImporExpor, AnoMes, Empresa, PeriApu: Integer; var BNCI_IDSeq, IDSeq: Integer);
begin
  if BNCI_IDSeq < 1 then
    BNCI_IDSeq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, Tabela, 'IDSeq1', [
    'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
    ImporExpor, AnoMes, Empresa, PeriApu],
    stIns, BNCI_IDSeq, siPositivo, nil)
  else
    BNCI_IDSeq := BNCI_IDSeq + 1;
  //
  IDSeq := BNCI_IDSeq;
end;

procedure TUnEfdIcmsIpi_PF_v03_0_2_a.DefineIDSeq2(const Tabela: String;
  const ImporExpor, AnoMes, Empresa, PeriApu: Integer; var BNCI_IDSeq,
  IDSeq: Integer);
begin
  if BNCI_IDSeq < 1 then
    BNCI_IDSeq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, Tabela, 'IDSeq2', [
    'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
    ImporExpor, AnoMes, Empresa, PeriApu],
    stIns, BNCI_IDSeq, siPositivo, nil)
  else
    BNCI_IDSeq := BNCI_IDSeq + 1;
  //
  IDSeq := BNCI_IDSeq;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K210_Fast(const ImporExpor,
  AnoMes, Empresa, PeriApu, MovimID, Codigo, MovimCod, Controle: Integer;
  const DataIni, DataFim, DtMovim, DtCorrApo: TDateTime; const GraGruX,
  ClientMO, FornecMO, EntiSitio: Integer; const Qtde: Double; const
  OrigemOpeProc: TOrigemOpeProc; const OriIDKnd: TOrigemIDKnd; const DiaFim:
  TDateTime; const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso: TMemo;
  const SrcNivel1: Integer; var IDSeq1, F_BNCI_IDSeq1_K210: Integer;
  var ListaIDSeq1: TMyArrOf2ArrOfInt; Agrega: Boolean = True): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K210_Fast()';
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik210';
  REG      = 'K210';
  //OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_INI_OS, DT_FIN_OS, COD_DOC_OS, COD_ITEM_ORI: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, OriOpeProc, OrigemIDKnd,
  I, K: Integer;
  JaFoi: Boolean;
  QTD_ORI: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  RecordsAffected: Int64;
begin
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0;//Integer(MovimNiv);
  //
  IDSeq1         := 0;
  //ID_SEK         := ;
  DT_INI_OS      := Geral.FDT(DataIni, 1);
  if Int(DataFim) > DiaFim then
    DT_FIN_OS    := '0000-00-00'
  else
    DT_FIN_OS    := Geral.FDT(DataFim, 1);
  COD_DOC_OS     := Geral.FF0(MovimCod);
  COD_ITEM_ORI   := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD_ORI        := Qtde;
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  OrigemIDKnd    := Integer(OriIDKnd);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270_Fast(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OriIDKnd, TOrigemSPEDEFDKnd.osekProducao,
    TTabProdOuSubPrd.tpspProducao, MeAviso, IDSeq1); // Item de origem
    //
    InsereItensAtuais_K280_New_Fast(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo, MovimCod,
    Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
    OriIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
(*
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
*)
    //
    K := Length(ListaIDSeq1);
{
    JaFoi := False;
    for I := 0 to K -1 do
    begin
      if ListaIDSeq1[I][0] = SrcNivel1 then
      begin
        IDSeq1 := ListaIDSeq1[I][1];
        JaFoi := True;
        Break;
      end;
    end;
    if not JaFoi then
    begin
}
      DefineIDSeq1(TabDst, ImporExpor, AnoMes, Empresa, PeriApu, F_BNCI_IDSeq1_K210, IDSeq1);
      K := K + 1;
      SetLength(ListaIDSeq1, K);
      ListaIDSeq1[K - 1][0] := SrcNivel1;
      ListaIDSeq1[K - 1][1] := IDSeq1;
{
    end;
}
    //
    SQLType := stIns;
    //
(*
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_INI_OS', 'DT_FIN_OS',
    'COD_DOC_OS', 'COD_ITEM_ORI', 'QTD_ORI',
    'GraGruX', 'OriOpeProc', 'OrigemIDKnd'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_INI_OS, DT_FIN_OS,
    COD_DOC_OS, COD_ITEM_ORI, QTD_ORI,
    GraGruX, OriOpeProc, OrigemIDKnd], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    if not Result then
      IDSeq1 := -1;
*)
    Result := USQLDB.SQL_DB_I_U(Dmod.MyDB, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_INI_OS', 'DT_FIN_OS',
    'COD_DOC_OS', 'COD_ITEM_ORI', 'QTD_ORI',
    'GraGruX', 'OriOpeProc', 'OrigemIDKnd'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_INI_OS, DT_FIN_OS,
    COD_DOC_OS, COD_ITEM_ORI, QTD_ORI,
    GraGruX, OriOpeProc, OrigemIDKnd], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True, _IGNORE_, RecordsAffected);
    //
    ConfirmaIDSeq(REG, Result, RecordsAffected, ['KndItm', 'GraGruX'], [KndItm, GraGruX], MeAviso, IDSeq1);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K210(const ImporExpor, AnoMes, Empresa,
  PeriApu, MovimID, Codigo, MovimCod, Controle: Integer; const DataIni, DataFim,
  DtMovim, DtCorrApo: TDateTime; const GraGruX, ClientMO, FornecMO,
  EntiSitio: Integer; const Qtde: Double; const OrigemOpeProc: TOrigemOpeProc;
  const OriIDKnd: TOrigemIDKnd; const DiaFim: TDateTime; const TipoPeriodoFiscal:
  TTipoPeriodoFiscal; const MeAviso: TMemo; var IDSeq1: Integer;
  Agrega: Boolean): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K210()';
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik210';
  REG      = 'K210';
  //OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_INI_OS, DT_FIN_OS, COD_DOC_OS, COD_ITEM_ORI: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, OriOpeProc, OrigemIDKnd:
  Integer;
  QTD_ORI: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0;//Integer(MovimNiv);
  //
  IDSeq1         := 0;
  //ID_SEK         := ;
  DT_INI_OS      := Geral.FDT(DataIni, 1);
  if Int(DataFim) > DiaFim then
    DT_FIN_OS    := '0000-00-00'
  else
    DT_FIN_OS    := Geral.FDT(DataFim, 1);
  COD_DOC_OS     := Geral.FF0(MovimCod);
  COD_ITEM_ORI   := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD_ORI        := Qtde;
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  OrigemIDKnd    := Integer(OriIDKnd);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OriIDKnd, TOrigemSPEDEFDKnd.osekProducao,
    TTabProdOuSubPrd.tpspProducao, MeAviso, IDSeq1); // Item de origem
    //
    InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo, MovimCod,
    Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
    OriIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_INI_OS', 'DT_FIN_OS',
    'COD_DOC_OS', 'COD_ITEM_ORI', 'QTD_ORI',
    'GraGruX', 'OriOpeProc', 'OrigemIDKnd'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_INI_OS, DT_FIN_OS,
    COD_DOC_OS, COD_ITEM_ORI, QTD_ORI,
    GraGruX, OriOpeProc, OrigemIDKnd], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215_Fast(const ImporExpor, AnoMes, Empresa,
  PeriApu, IDSeq1: Integer; Data, DtMovim, DtCorrApo: TDateTime; GraGruX:
  Integer; Qtde: Double; COD_INS_SUBST: String; ID_Item, MovimID, Codigo,
  MovimCod, Controle, ClientMO, FornecMO, EntiSitio: Integer; OrigemOpeProc:
  TOrigemOpeProc; OriIDKnd: TOrigemIDKnd; OriESTSTabSorc: TEstqSPEDTabSorc;
  TipoPeriodoFiscal: TTipoPeriodoFiscal; MeAviso: TMemo; F_BNCI_IDSeq2_K215:
  Integer):
  Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215_Fast()';
const
  TabDst   = 'efdicmsipik215';
  REG      = 'K215';
  RegPai   = 'K210';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  COD_ITEM_DES: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, ESTSTabSorc,
  OriOpeProc, OrigemIDKnd: Integer;
  QTD_DES: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  RecordsAffected: Int64;
begin
  Result := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  COD_ITEM_DES   := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD_DES        := Qtde;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  OrigemIDKnd    := Integer(OriIDKnd);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K275_Fast(ImporExpor, AnoMes, Empresa, PeriApu, REG,
    MovimID, Codigo, MovimCod, Controle,
    GraGruX, (*LinArqPai*)IDSeq1, Qtde, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
    OriESTSTabSorc, OrigemOpeProc, OriIDKnd, TOrigemSPEDEFDKnd.osekProducao, MeAviso);
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New_Fast(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
    OriIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
{
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'COD_ITEM_DES', 'QTD_DES',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc',
    'OrigemIDKnd'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, COD_ITEM_DES, QTD_DES,
    GraGruX, ESTSTabSorc, OriOpeProc,
    OrigemIDKnd], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
}
    DefineIDSeq2(TabDst, ImporExpor, AnoMes, Empresa, PeriApu, F_BNCI_IDSeq2_K215, IDSeq2);
    //
    SQLType := stIns;
    //
    Result := USQLDB.SQL_DB_I_U(Dmod.MyDB, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'COD_ITEM_DES', 'QTD_DES',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc',
    'OrigemIDKnd'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, COD_ITEM_DES, QTD_DES,
    GraGruX, ESTSTabSorc, OriOpeProc,
    OrigemIDKnd], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True, _IGNORE_, RecordsAffected);
    //
    ConfirmaIDSeq(REG, Result, RecordsAffected, ['KndItm', 'GraGruX'], [KndItm, GraGruX], MeAviso, IDSeq2);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215(const ImporExpor, AnoMes, Empresa,
  PeriApu, IDSeq1: Integer; Data, DtMovim, DtCorrApo: TDateTime; GraGruX:
  Integer; Qtde: Double; COD_INS_SUBST: String; ID_Item, MovimID, Codigo,
  MovimCod, Controle, ClientMO, FornecMO, EntiSitio: Integer; OrigemOpeProc:
  TOrigemOpeProc; OriIDKnd: TOrigemIDKnd; OriESTSTabSorc: TEstqSPEDTabSorc;
  TipoPeriodoFiscal: TTipoPeriodoFiscal; MeAviso: TMemo):
  Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K215()';
const
  TabDst   = 'efdicmsipik215';
  REG      = 'K215';
  RegPai   = 'K210';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  COD_ITEM_DES: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, ESTSTabSorc,
  OriOpeProc, OrigemIDKnd: Integer;
  QTD_DES: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  COD_ITEM_DES   := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD_DES        := Qtde;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  OrigemIDKnd    := Integer(OriIDKnd);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K275(ImporExpor, AnoMes, Empresa, PeriApu, REG,
    MovimID, Codigo, MovimCod, Controle,
    GraGruX, (*LinArqPai*)IDSeq1, Qtde, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
    OriESTSTabSorc, OrigemOpeProc, OriIDKnd, TOrigemSPEDEFDKnd.osekProducao, MeAviso);
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
    OriIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'COD_ITEM_DES', 'QTD_DES',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc',
    'OrigemIDKnd'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, COD_ITEM_DES, QTD_DES,
    GraGruX, ESTSTabSorc, OriOpeProc,
    OrigemIDKnd], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230_Fast(const ImporExpor, AnoMes,
  Empresa, PeriApu, MovimID, Codigo, MovimCod, Controle: Integer;
  const DataIni, DataFim, DtMovim, DtCorrApo: TDateTime;
  const GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde: Double;
  const OrigemOpeProc: TOrigemOpeProc; DiaFim: TDateTime;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso: TMemo;
  var IDSeq1, F_BNCI_IDSeq1_K230: Integer; Agrega: Boolean): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230_Fast()';
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik230';
  REG      = 'K230';
  //Controle = 0;
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_INI_OP, DT_FIN_OP, COD_DOC_OP, COD_ITEM, TabIns: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, OriOpeProc: Integer;
  QTD_ENC: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  TabProdOuSubPrd: TTabProdOuSubPrd;
  RecordsAffected: Int64;
begin
  Result         := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  DT_INI_OP      := Geral.FDT(DataIni, 1);
  if Int(DataFim) > DiaFim then
    DT_FIN_OP    := '0000-00-00'
  else
    DT_FIN_OP    := Geral.FDT(DataFim, 1);
  COD_DOC_OP     := Geral.FF0(MovimCod);
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD_ENC        := Qtde;
  //
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  TabProdOuSubPrd := EfdIcmsIpi_PF.DefineTabProdOuSubPrd(GraGruX);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270_Fast(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    TabProdOuSubPrd, MeAviso, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New_Fast(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
(*
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    TabIns := EfdIcmsIpi_PF.DefineTabelaInsProdOuSubPrd(TabProdOuSubPrd, TabDst,
    'efdicmsipik23subprd', sProcName);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabIns, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_INI_OP', 'DT_FIN_OP',
    'COD_DOC_OP', 'COD_ITEM', 'QTD_ENC',
    'GraGruX', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_INI_OP, DT_FIN_OP,
    COD_DOC_OP, COD_ITEM, QTD_ENC,
    GraGruX, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
*)
    TabIns := EfdIcmsIpi_PF.DefineTabelaInsProdOuSubPrd(TabProdOuSubPrd, TabDst,
    'efdicmsipik23subprd', sProcName);
    //
    DefineIDSeq1(TabDst, ImporExpor, AnoMes, Empresa, PeriApu, F_BNCI_IDSeq1_K230, IDSeq1);
    //
    SQLType := stIns;
    //
    Result := USQLDB.SQL_DB_I_U(Dmod.MyDB, SQLType, TabIns, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_INI_OP', 'DT_FIN_OP',
    'COD_DOC_OP', 'COD_ITEM', 'QTD_ENC',
    'GraGruX', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_INI_OP, DT_FIN_OP,
    COD_DOC_OP, COD_ITEM, QTD_ENC,
    GraGruX, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True, _IGNORE_, RecordsAffected);
    //
    ConfirmaIDSeq(REG, Result, RecordsAffected, ['KndItm', 'GraGruX'], [KndItm, GraGruX], MeAviso, IDSeq1);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230(const ImporExpor, AnoMes,
  Empresa, PeriApu, MovimID, Codigo, MovimCod, Controle: Integer;
  const DataIni, DataFim, DtMovim, DtCorrApo: TDateTime;
  const GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde: Double;
  const OrigemOpeProc: TOrigemOpeProc; DiaFim: TDateTime;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso: TMemo;
  var IDSeq1: Integer; Agrega: Boolean): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K230()';
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik230';
  REG      = 'K230';
  //Controle = 0;
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_INI_OP, DT_FIN_OP, COD_DOC_OP, COD_ITEM, TabIns: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, OriOpeProc: Integer;
  QTD_ENC: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  TabProdOuSubPrd: TTabProdOuSubPrd;
begin
  Result         := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  DT_INI_OP      := Geral.FDT(DataIni, 1);
  if Int(DataFim) > DiaFim then
    DT_FIN_OP    := '0000-00-00'
  else
    DT_FIN_OP    := Geral.FDT(DataFim, 1);
  COD_DOC_OP     := Geral.FF0(MovimCod);
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD_ENC        := Qtde;
  //
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  TabProdOuSubPrd := EfdIcmsIpi_PF.DefineTabProdOuSubPrd(GraGruX);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    TabProdOuSubPrd, MeAviso, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    TabIns := EfdIcmsIpi_PF.DefineTabelaInsProdOuSubPrd(TabProdOuSubPrd, TabDst,
    'efdicmsipik23subprd', sProcName);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabIns, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_INI_OP', 'DT_FIN_OP',
    'COD_DOC_OP', 'COD_ITEM', 'QTD_ENC',
    'GraGruX', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_INI_OP, DT_FIN_OP,
    COD_DOC_OP, COD_ITEM, QTD_ENC,
    GraGruX, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235_Fast(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1: Integer; Data,
  DtCorrApo: TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
  ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc;
  OriESTSTabSorc: TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
  const MeAviso: TMemo; var F_BNCI_IDSeq2_K235: Integer): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235_Fast()';
const
  TabDst   = 'efdicmsipik235';
  REG      = 'K235';
  RegPai   = 'K230';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_SAIDA, COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq2, ESTSTabSorc, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  RecordsAffected: Int64;
begin
  Result := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  DT_SAIDA       := Geral.FDT(Data, 1);
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K275_Fast(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, MovimID, Codigo, MovimCod, Controle,
    GraGruX, (*LinArqPai*)IDSeq1, Qtde, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
    OriESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    MeAviso); // item de destino
    //
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New_Fast(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, Data, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
{
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(nil, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'DT_SAIDA',
    'COD_ITEM', 'QTD', 'COD_INS_SUBST',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, DT_SAIDA,
    COD_ITEM, QTD, COD_INS_SUBST,
    GraGruX, ESTSTabSorc, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
}
    DefineIDSeq2(TabDst, ImporExpor, AnoMes, Empresa, PeriApu, F_BNCI_IDSeq2_K235, IDSeq2);
    //
    SQLType := stIns;
    //
    Result := USQLDB.SQL_DB_I_U(Dmod.MyDB, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'DT_SAIDA',
    'COD_ITEM', 'QTD', 'COD_INS_SUBST',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, DT_SAIDA,
    COD_ITEM, QTD, COD_INS_SUBST,
    GraGruX, ESTSTabSorc, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True, _IGNORE_, RecordsAffected);
    //
    ConfirmaIDSeq(REG, Result, RecordsAffected, ['KndItm', 'GraGruX'], [KndItm, GraGruX], MeAviso, IDSeq2);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1: Integer; Data,
  DtCorrApo: TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
  ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc;
  OriESTSTabSorc: TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
  const MeAviso: TMemo): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K235()';
const
  TabDst   = 'efdicmsipik235';
  REG      = 'K235';
  RegPai   = 'K230';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
(*
var
  DT_SAIDA, COD_ITEM: String;
  ImporExpor, AnoMes, Empresa, LinArq, K100, K2X0: Integer;
  QTD: Double;
  SQLType: TSQLType;
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
*)
var
  DT_SAIDA, COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq2, ESTSTabSorc, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  DT_SAIDA       := Geral.FDT(Data, 1);
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K275(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, MovimID, Codigo, MovimCod, Controle,
    GraGruX, (*LinArqPai*)IDSeq1, Qtde, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
    OriESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    MeAviso); // item de destino
    //
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, Data, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(nil, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'DT_SAIDA',
    'COD_ITEM', 'QTD', 'COD_INS_SUBST',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, DT_SAIDA,
    COD_ITEM, QTD, COD_INS_SUBST,
    GraGruX, ESTSTabSorc, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250_Fast(const ImporExpor, AnoMes, Empresa, PeriApu, MovimID, Codigo, MovimCod,
  Controle: Integer; const DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde: Double;
  const OrigemOpeProc: TOrigemOpeProc; DiaFim: TDateTime;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso: TMemo;
  var IDSeq1, F_BNCI_IDSeq1_K250: Integer; Agrega: Boolean): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250_Fast()';
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik250';
  REG      = 'K250';
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_PROD, COD_ITEM, COD_DOC_OP, TabIns: String;
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  Data: TDateTime;
  TabProdOuSubPrd: TTabProdOuSubPrd;
  RecordsAffected: Int64;
begin
  Result         := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  Data           := EfdIcmsIpi_PF.DataFimObrigatoria(DtHrFimOpe, DiaFim);
  DT_PROD        := Geral.FDT(Data, 1);
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD            := Qtde;
  COD_DOC_OP     := Geral.FF0(MovimCod);
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  TabProdOuSubPrd := EfdIcmsIpi_PF.DefineTabProdOuSubPrd(GraGruX);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    TabProdOuSubPrd, MeAviso, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
(*
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    TabIns := EfdIcmsIpi_PF.DefineTabelaInsProdOuSubPrd(TabProdOuSubPrd, TabDst,
    'efdicmsipik25subprd', sProcName);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabIns, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_PROD', 'COD_ITEM',
    'QTD', 'COD_DOC_OP', 'GraGruX',
    'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_PROD, COD_ITEM,
    QTD, COD_DOC_OP, GraGruX,
    OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
*)
    TabIns := EfdIcmsIpi_PF.DefineTabelaInsProdOuSubPrd(TabProdOuSubPrd, TabDst,
    'efdicmsipik25subprd', sProcName);
    //
    DefineIDSeq1(TabDst, ImporExpor, AnoMes, Empresa, PeriApu, F_BNCI_IDSeq1_K250, IDSeq1);
    //
    SQLType := stIns;
    //
    Result := USQLDB.SQL_DB_I_U(Dmod.MyDB, SQLType, TabIns, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_PROD', 'COD_ITEM',
    'QTD', 'COD_DOC_OP', 'GraGruX',
    'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_PROD, COD_ITEM,
    QTD, COD_DOC_OP, GraGruX,
    OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True, _IGNORE_, RecordsAffected);
    //
    ConfirmaIDSeq(REG, Result, RecordsAffected, ['KndItm', 'GraGruX'], [KndItm, GraGruX], MeAviso, IDSeq1);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250(const ImporExpor, AnoMes, Empresa, PeriApu, MovimID, Codigo, MovimCod,
  Controle: Integer; const DtHrFimOpe, DtMovim, DtCorrApo: TDateTime;
  const GraGruX, ClientMO, FornecMO, EntiSitio: Integer; const Qtde: Double;
  const OrigemOpeProc: TOrigemOpeProc; DiaFim: TDateTime;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso: TMemo;
  var IDSeq1: Integer; Agrega: Boolean): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K250()';
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik250';
  REG      = 'K250';
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_PROD, COD_ITEM, COD_DOC_OP, TabIns: String;
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  Data: TDateTime;
  TabProdOuSubPrd: TTabProdOuSubPrd;
begin
  Result         := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  Data           := EfdIcmsIpi_PF.DataFimObrigatoria(DtHrFimOpe, DiaFim);
  DT_PROD        := Geral.FDT(Data, 1);
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD            := Qtde;
  COD_DOC_OP     := Geral.FF0(MovimCod);
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  TabProdOuSubPrd := EfdIcmsIpi_PF.DefineTabProdOuSubPrd(GraGruX);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    TabProdOuSubPrd, MeAviso, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    TabIns := EfdIcmsIpi_PF.DefineTabelaInsProdOuSubPrd(TabProdOuSubPrd, TabDst,
    'efdicmsipik25subprd', sProcName);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabIns, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_PROD', 'COD_ITEM',
    'QTD', 'COD_DOC_OP', 'GraGruX',
    'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_PROD, COD_ITEM,
    QTD, COD_DOC_OP, GraGruX,
    OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255_Fast(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1: Integer; Data,
  DtCorrApo: TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
  ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc;
  OriESTSTabSorc: TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
  const MeAviso: TMemo; var F_BNCI_IDSeq2_K255: Integer): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255_Fast()';
const
  TabDst   = 'efdicmsipik255';

  REG      = 'K255';
  RegPai   = 'K250';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_CONS, COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq2, ESTSTabSorc, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  RecordsAffected: Int64;
begin
  Result := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  DT_CONS        := Geral.FDT(Data, 1);
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K275_Fast(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, MovimID, Codigo, MovimCod, Controle,
    GraGruX, (*LinArqPai*)IDSeq1, Qtde, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
    OriESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso); // item de destino
    //
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New_Fast(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, Data, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
(*
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'DT_CONS',
    'COD_ITEM', 'QTD', 'COD_INS_SUBST',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, DT_CONS,
    COD_ITEM, QTD, COD_INS_SUBST,
    GraGruX, ESTSTabSorc, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
*)
    DefineIDSeq2(TabDst, ImporExpor, AnoMes, Empresa, PeriApu, F_BNCI_IDSeq2_K255, IDSeq2);
    //
    SQLType := stIns;
    //
    Result := USQLDB.SQL_DB_I_U(Dmod.MyDB, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'DT_CONS',
    'COD_ITEM', 'QTD', 'COD_INS_SUBST',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, DT_CONS,
    COD_ITEM, QTD, COD_INS_SUBST,
    GraGruX, ESTSTabSorc, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True, _IGNORE_, RecordsAffected);
    //
    ConfirmaIDSeq(REG, Result, RecordsAffected, ['KndItm', 'GraGruX'], [KndItm, GraGruX], MeAviso, IDSeq2);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1: Integer; Data,
  DtCorrApo: TDateTime; GraGruX: Integer; Qtde: Double; COD_INS_SUBST: String;
  ID_Item, MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc;
  OriESTSTabSorc: TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
  const MeAviso: TMemo): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K255()';
const
  TabDst   = 'efdicmsipik255';
  REG      = 'K255';
  RegPai   = 'K250';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  DT_CONS, COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, IDSeq2, ESTSTabSorc, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  DT_CONS        := Geral.FDT(Data, 1);
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K275(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, MovimID, Codigo, MovimCod, Controle,
    GraGruX, (*LinArqPai*)IDSeq1, Qtde, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
    OriESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso); // item de destino
    //
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, Data, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'DT_CONS',
    'COD_ITEM', 'QTD', 'COD_INS_SUBST',
    'GraGruX', 'ESTSTabSorc', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, DT_CONS,
    COD_ITEM, QTD, COD_INS_SUBST,
    GraGruX, ESTSTabSorc, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K260(const ImporExpor, AnoMes, Empresa,
  PeriApu, MovimID, Codigo, MovimCod, Controle: Integer; const DtHrAberto,
  DtHrFimOpe, DtMovim, DtCorrApoProd, DtCorrApoCons: TDateTime; const GraGruX,
  ClientMO, FornecMO, EntiSitio: Integer; const QtdeProd, QtdeCons: Double;
  const OrigemOpeProc: TOrigemOpeProc;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso: TMemo;
  var IDSeq1: Integer; const Agrega: Boolean): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K260()';
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik260';
  REG      = 'K260';
  OrigemIDKnd = TOrigemIDKnd.oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  COD_OP_OS, COD_ITEM, DT_SAIDA, DT_RET: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID,
  KndNiv, OriOpeProc: Integer;
  QTD_SAIDA, QTD_RET: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result         := False;
  if DtCorrApoProd = -1 then
    Geral.MB_Aviso('DtCorrApoProd = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  if DtCorrApoCons = -1 then
    Geral.MB_Aviso('DtCorrApoCons = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  COD_OP_OS      := Geral.FF0(MovimCod);
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  DT_SAIDA       := Geral.FDT(DtHrAberto, 1);
  QTD_SAIDA      := QtdeCons;
  DT_RET         := Geral.FDT(DtHrFimOpe, 1);
  QTD_RET        := QtdeProd;
  //GraGruX        := ;
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApoProd > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    //QtdPos e Neg no mesmo registro n�o pode!
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, DtMovim, DtCorrApoProd, MovimID, Codigo,
    MovimCod, Controle,  GraGruX, QtdeProd, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    TTabProdOuSubPrd.tpspProducao, MeAviso, IDSeq1); // Item de origem
    //
    if Result then
    begin
      //
    //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
      TipoPeriodoFiscal, ImporExpor, AnoMes,
      Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApoProd, MovimID, Codigo, MovimCod,
      Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
      QtdeProd, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
      OrigemIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
      //
      Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa,
    PeriApu, REG, DtMovim, DtCorrApoCons, MovimID, Codigo,
      MovimCod, Controle,  GraGruX, -QtdeCons, SPED_EFD_KndRegOrigem,
      OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
      TTabProdOuSubPrd.tpspProducao, MeAviso, IDSeq1); // Item de origem
      //
    //
      EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
      TipoPeriodoFiscal, ImporExpor, AnoMes,
      Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApoCons, MovimID, Codigo, MovimCod,
      Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
      -QtdeCons, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc,
      OrigemIDKnd, OrigemSPEDEFDKnd, (*Agrega*)True, MeAviso);
      //
    end;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'COD_OP_OS', 'COD_ITEM',
    'DT_SAIDA', 'QTD_SAIDA', 'DT_RET',
    'QTD_RET', 'GraGruX', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, COD_OP_OS, COD_ITEM,
    DT_SAIDA, QTD_SAIDA, DT_RET,
    QTD_RET, GraGruX, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K265(ImporExpor, AnoMes, Empresa,
  PeriApu, IDSeq1: Integer; Data, DtCorrApo: TDateTime; GraGruX: Integer;
  QtdeCons, QtdeRet: Double; COD_INS_SUBST: String; ID_Item, MovimID, Codigo,
  MovimCod, Controle: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
  TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal; const MeAviso:
  TMemo): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K265()';
const
  TabDst   = 'efdicmsipik265';
  REG      = 'K265';
  RegPai   = 'K260';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  TabSorc = TEstqSPEDTabSorc.estsVMI;
var
  COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, ESTSTabSorc, OriOpeProc: Integer;
  QTD_CONS, QTD_RET: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  //
  SQLType        := stNil;
  //ImporExpor     := ;
  //AnoMes         := ;
  //Empresa        := ;
  //PeriApu        := ;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD_CONS       := QtdeCons;
  QTD_RET        := QtdeRet;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    if QTD_CONS <> 0 then
      Result := InsereItemAtual_K275(ImporExpor, AnoMes, Empresa,
      PeriApu, REG, MovimID, Codigo, MovimCod, Controle,
      GraGruX, (*LinArqPai*)IDSeq1, QTD_CONS, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
      OriESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso); // item de destino
    //
    if QTD_RET <> 0 then
      Result := InsereItemAtual_K275(ImporExpor, AnoMes, Empresa,
      PeriApu, REG, MovimID, Codigo, MovimCod, Controle,
      GraGruX, (*LinArqPai*)IDSeq1, QTD_RET, COD_INS_SUBST, SPED_EFD_KndRegOrigem,
      OriESTSTabSorc, OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso); // item de destino
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'COD_ITEM',
    'QTD_CONS', 'QTD_RET', 'GraGruX',
    'ESTSTabSorc', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, COD_ITEM,
    QTD_CONS, QTD_RET, GraGruX,
    ESTSTabSorc, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K270_Fast(const ImporExpor,
  AnoMes, Empresa, PeriApu: Integer; const RegOri: String; const Data,
  DtCorrApo: TDateTime; const MovimID, Codigo, MovimCod, Controle,
  GraGruX: Integer; const Qtde: Double;
  const SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  OrigemOpeProc: TOrigemOpeProc; OrigemIDKnd: TOrigemIDKnd;
  const OriSPEDEFDKnd: TOrigemSPEDEFDKnd; TabProdOuSubPrd: TTabProdOuSubPrd;
  MeAviso: TMemo; var IDSeq1: Integer): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K270_Fast()';
begin
  if not VAR_AVISA_FALTA_FAZER_InsereItemAtual_K270_Fast then
  begin
    Geral.MB_Info('Falta fazer: ' + sProcName);
    VAR_AVISA_FALTA_FAZER_InsereItemAtual_K270_Fast := True;
  end;
  Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu, RegOri,
  Data, DtCorrApo, MovimID, Codigo, MovimCod, Controle, GraGruX, Qtde,
  SPED_EFD_KndRegOrigem, OrigemOpeProc, OrigemIDKnd, OriSPEDEFDKnd,
  TabProdOuSubPrd, MeAviso, IDSeq1);
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K270(const ImporExpor,
  AnoMes, Empresa, PeriApu: Integer; const RegOri: String; const Data,
  DtCorrApo: TDateTime; const MovimID, Codigo, MovimCod, Controle, GraGruX:
  Integer; const Qtde: Double; const SPED_EFD_KndRegOrigem:
  TSPED_EFD_KndRegOrigem; OrigemOpeProc: TOrigemOpeProc; OrigemIDKnd:
  TOrigemIDKnd; const OriSPEDEFDKnd: TOrigemSPEDEFDKnd; TabProdOuSubPrd:
  TTabProdOuSubPrd; MeAviso: TMemo; var IDSeq1: Integer): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K270()';
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik270';
  REG      = 'K270';
  ValAntes = 0;
  TabSorc  = TEstqSPEDTabSorc.estsVMI;
var
  DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM, ORIGEM, RegisPai, TabIns: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  OriOpeProc, KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv: Integer;
  QTD_COR_POS, QTD_COR_NEG, ValNovo: Double;
  SQLType: TSQLType;
  //
  ThatDtIni, ThatDtFim: TDateTime;
  SQL_POS_NEG: String;
begin
  IDSeq1         := 0;
  //SQLType        := ImgTipo.SQLType?;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  EfdIcmsIpi_PF.DefineDatasPeriCorrApoSPED(Empresa, 'K100', Data, DtCorrApo, ThatDtIni, ThatDtFim);
  //MovimID        := ;
  //Codigo         := ;
  //MovimCod       := ;
  DT_INI_AP      := Geral.FDT(ThatDtIni, 1);
  DT_FIN_AP      := Geral.FDT(ThatDtFim, 1);
  COD_OP_OS      := Geral.FF0(MovimCod);
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD_COR_POS    := 0;
  QTD_COR_NEG    := 0;
  RegisPai       := RegOri;
  ValNovo := Qtde;
  EfdIcmsIpi_PF.DefineQuantidadePositivaOuNegativa(RegOri, REG, ValAntes, ValNovo,
    caflpnMovNormEsquecido, QTD_COR_POS, QTD_COR_NEG);
  ORIGEM         := Geral.FF0(Integer(SPED_EFD_KndRegOrigem));
  //GraGruX        := ; GGXOri
  OriOpeProc     := Integer(OrigemOpeProc);
  if QTD_COR_POS > 0 then
    SQL_POS_NEG := 'AND QTD_COR_POS > 0 '
  else
    SQL_POS_NEG := 'AND QTD_COR_NEG > 0 ';
  //
  EfdIcmsIpi_PF.VerificaGGX_SPED(GraGruX, KndCod, KndNSU, KndItm, REG);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
  'SELECT * ',
  'FROM efdicmsipik270 ',
  'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
  'AND AnoMes=' + Geral.FF0(AnoMes),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND PeriApu=' + Geral.FF0(PeriApu),
  'AND KndTab=' + Geral.FF0(KndTab),
  'AND KndCod=' + Geral.FF0(KndCod),
  'AND KndNSU=' + Geral.FF0(KndNSU),
  'AND KndItm=' + Geral.FF0(KndItm),
  'AND GraGruX=' + Geral.FF0(GraGruX),
  'AND ORIGEM="' + ORIGEM + '"',
  '']);
  //Geral.MB_SQL(Self, DmProd.QrX999);
  IDSeq1 := 0;
  if DmProd.QrX999.RecordCount > 0 then
  begin
    EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
    Exit;
  end else
  begin
    IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
    'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
    ImporExpor, AnoMes, Empresa, PeriApu],
    stIns, IDSeq1, siPositivo, nil);
    //
    SQLType := stIns;
  end;
  //
  TabIns := EfdIcmsIpi_PF.DefineTabelaInsProdOuSubPrd(TabProdOuSubPrd, TabDst,
    'efdicmsipik27subprd', sProcName);
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabIns, False, [
  'IDSeq1', 'KndAID', 'KndNiv',
  'DT_INI_AP', 'DT_FIN_AP', 'COD_OP_OS',
  'COD_ITEM', 'QTD_COR_POS', 'QTD_COR_NEG',
  'ORIGEM', 'GraGruX', 'OriOpeProc',
  'OrigemIDKnd', 'OriSPEDEFDKnd', 'RegisPai'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'], [
  IDSeq1, KndAID, KndNiv,
  DT_INI_AP, DT_FIN_AP, COD_OP_OS,
  COD_ITEM, QTD_COR_POS, QTD_COR_NEG,
  ORIGEM, GraGruX, OriOpeProc,
  OrigemIDKnd, OriSPEDEFDKnd, RegisPai], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True);
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K275(const ImporExpor, AnoMes, Empresa,
  PeriApu: Integer; RegOri: String; MovimID, Codigo, MovimCod, Controle,
  GraGruX, IDSeq1: Integer; Qtde: Double; COD_INS_SUBST: String;
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem; ESTSTabSorc: TEstqSPEDTabSorc;
  OrigemOpeProc: TOrigemOpeProc; OriIDKnd: TOrigemIDKnd;
  const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd; MeAviso: TMemo): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K275()';
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik275';
  REG      = 'K275';
  ValAntes = 0;
  TabSorc  = TEstqSPEDTabSorc.estsVMI;
var
  COD_ITEM, ORIGEM, RegisPai: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  IDSeq2, KndTab, KndCod, KndNSU, KndItm,
  KndAID, KndNiv, OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd: Integer;
  QTD_COR_POS, QTD_COR_NEG: Double;
  SQLType: TSQLType;
  //
  ValNovo: Double;
begin
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //IDSeq1         := ;
  IDSeq2         := 0;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //ID_SEK         := ;
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD_COR_POS    := 0;
  QTD_COR_NEG    := 0;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  //ESTSTabSorc    := ;
  ORIGEM         := Geral.FF0(Integer(SPED_EFD_KndRegOrigem));
  OriOpeProc     := Integer(OrigemOpeProc);
  OrigemIDKnd    := Integer(OriIDKnd);
  OriSPEDEFDKnd  := Integer(OrigemSPEDEFDKnd);
  RegisPai       := RegOri;
  ValNovo := Qtde;
  EfdIcmsIpi_PF.DefineQuantidadePositivaOuNegativa(RegOri, REG, ValAntes, ValNovo,
    caflpnMovNormEsquecido, QTD_COR_POS, QTD_COR_NEG);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
  'SELECT * ',
  'FROM efdicmsipik275 ',
  'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
  'AND AnoMes=' + Geral.FF0(AnoMes),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND PeriApu=' + Geral.FF0(PeriApu),
  'AND KndTab=' + Geral.FF0(KndTab),
  'AND KndCod=' + Geral.FF0(KndCod),
  'AND KndNSU=' + Geral.FF0(KndNSU),
  'AND KndItm=' + Geral.FF0(KndItm),
  'AND GraGruX=' + Geral.FF0(GraGruX),
  '']);
  IDSeq2 := 0;
  if DmProd.QrX999.RecordCount > 0 then
  begin
    EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
    Exit;
  end else
  begin
    IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
    'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
    ImporExpor, AnoMes, Empresa, PeriApu],
    stIns, IDSeq2, siPositivo, nil);
    //
    SQLType := stIns;
  end;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
  'IDSeq1', 'IDSeq2', 'KndAID',
  'KndNiv', 'ID_SEK', 'COD_ITEM',
  'QTD_COR_POS', 'QTD_COR_NEG', 'COD_INS_SUBST',
  'GraGruX', 'ESTSTabSorc', 'ORIGEM',
  'OriOpeProc', 'OrigemIDKnd', 'OriSPEDEFDKnd',
  'RegisPai'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'PeriApu', 'KndTab', 'KndCod',
  'KndNSU', 'KndItm'], [
  IDSeq1, IDSeq2, KndAID,
  KndNiv, ID_SEK, COD_ITEM,
  QTD_COR_POS, QTD_COR_NEG, COD_INS_SUBST,
  GraGruX, ESTSTabSorc, ORIGEM,
  OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd,
  RegisPai], [
  ImporExpor, AnoMes, Empresa,
  PeriApu, KndTab, KndCod,
  KndNSU, KndItm], True);
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K275_Fast(const ImporExpor,
  AnoMes, Empresa, PeriApu: Integer; RegOri: String; MovimID, Codigo, MovimCod,
  Controle, GraGruX, IDSeq1: Integer; Qtde: Double; COD_INS_SUBST: String;
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem; ESTSTabSorc: TEstqSPEDTabSorc;
  OrigemOpeProc: TOrigemOpeProc; OriIDKnd: TOrigemIDKnd;
  const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd; MeAviso: TMemo): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K275_Fast()';
begin
  if not VAR_AVISA_FALTA_FAZER_InsereItemAtual_K275_Fast then
  begin
    Geral.MB_Info('Falta fazer: ' + sProcName);
    VAR_AVISA_FALTA_FAZER_InsereItemAtual_K275_Fast := True;
  end;
  Result := InsereItemAtual_K275(ImporExpor, AnoMes, Empresa, PeriApu, RegOri,
  MovimID, Codigo, MovimCod, Controle, GraGruX, IDSeq1, Qtde, COD_INS_SUBST,
  SPED_EFD_KndRegOrigem, ESTSTabSorc, OrigemOpeProc, OriIDKnd, OrigemSPEDEFDKnd,
  MeAviso);
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290_Fast(
const ImporExpor, AnoMes, Empresa,
  PeriApu, MovimID, Codigo, MovimCod, CodDocOP(*, Controle*): Integer; const
  DataIni, DataFim, (*DtMovim,*) DtCorrApo: TDateTime; const ClientMO, FornecMO,
  EntiSitio: Integer; const OrigemOpeProc: TOrigemOpeProc; const DiaFim:
  TDateTime; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
  const TabSorc: TEstqSPEDTabSorc; const MeAviso: TMemo;
  var IDSeq1, F_BNCI_IDSeq1_K290: Integer): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik290';
  REG      = 'K290';
var
  DT_INI_OP, DT_FIN_OP, COD_DOC_OP: String;
  //ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1, ID_SEK,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, OriOpeProc: Integer;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  RecordsAffected: Int64;
begin
  Result         := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := 0(*Controle*);
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  DT_INI_OP      := Geral.FDT(DataIni, 1);
  if Int(DataFim) > DiaFim then
    DT_FIN_OP    := '0000-00-00'
  else
    DT_FIN_OP    := Geral.FDT(DataFim, 1);
  COD_DOC_OP     := Geral.FF0(CodDocOP);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    // N�o existe registro K270 e K280 para para K290!
    (*
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, Agrega=True, MeAviso);
    //
    *)
  end else
  begin
    DefineIDSeq1(TabDst, ImporExpor, AnoMes, Empresa, PeriApu, F_BNCI_IDSeq1_K290, IDSeq1);
    //
    SQLType := stIns;
    //
    Result := USQLDB.SQL_DB_I_U(Dmod.MyDB, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_INI_OP', 'DT_FIN_OP',
    'COD_DOC_OP', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_INI_OP, DT_FIN_OP,
    COD_DOC_OP, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True, _IGNORE_, RecordsAffected);
    //
    ConfirmaIDSeq(REG, Result, RecordsAffected, ['KndNSU'], [KndNSU], MeAviso, IDSeq1);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K290(
const ImporExpor, AnoMes, Empresa,
  PeriApu, MovimID, Codigo, MovimCod, CodDocOP(*, Controle*): Integer; const
  DataIni, DataFim, (*DtMovim,*) DtCorrApo: TDateTime; const ClientMO, FornecMO,
  EntiSitio: Integer; const OrigemOpeProc: TOrigemOpeProc; const DiaFim:
  TDateTime; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
  const TabSorc: TEstqSPEDTabSorc; const MeAviso: TMemo;
  var IDSeq1: Integer): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik290';
  REG      = 'K290';
var
  DT_INI_OP, DT_FIN_OP, COD_DOC_OP: String;
  //ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1, ID_SEK,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, OriOpeProc: Integer;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result         := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := 0(*Controle*);
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  DT_INI_OP      := Geral.FDT(DataIni, 1);
  if Int(DataFim) > DiaFim then
    DT_FIN_OP    := '0000-00-00'
  else
    DT_FIN_OP    := Geral.FDT(DataFim, 1);
  COD_DOC_OP     := Geral.FF0(CodDocOP);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    // N�o existe registro K270 e K280 para para K290!
    (*
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, Agrega=True, MeAviso);
    //
    *)
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND COD_DOC_OP="' + Geral.FF0(CodDocOP) + '"',
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      MeAviso.Lines.Add('K290 j� importado > KndNSU: ' + Geral.FF0(KndItm));
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    //Result := USQLDB.SQL_DB_I_U(Dmod.MyDB, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_INI_OP', 'DT_FIN_OP',
    'COD_DOC_OP', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_INI_OP, DT_FIN_OP,
    COD_DOC_OP, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291_Fast(ImporExpor,
  AnoMes, Empresa, PeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
  PaiMovID, PaiMovCod, PaiNivel1, PaiNivel2: Integer; DtMovim,
  DtCorrApo: TDateTime; GraGruX: Integer; Qtde: Double; MovimID, Codigo,
  MovimCod, Controle, ClientMO, FornecMO, EntiSitio: Integer;
  OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc: TEstqSPEDTabSorc;
  const MeAviso: TMemo; var F_BNCI_IDSeq2_K291: Integer): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291_Fast()';
const
  TabDst   = 'efdicmsipik291';
  REG      = 'K291';
  //RegPai   = 'K290';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  //
var
  COD_ITEM, TabIns: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  ESTSTabSorc,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  //
  TabProdOuSubPrd: TTabProdOuSubPrd;
  RecordsAffected: Int64;
begin
  Result := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  if Qtde = 0 then
  begin
    EfdIcmsIpi_PF.AvisoQtdeZero(MeAviso, KndItm, GraGruX, MovimCod, Controle, REG);
    Exit;
  end;
  //
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  TabProdOuSubPrd := EfdIcmsIpi_PF.DefineTabProdOuSubPrd(GraGruX);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270_Fast(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    TabProdOuSubPrd, MeAviso, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New_Fast(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    DefineIDSeq2(TabDst, ImporExpor, AnoMes, Empresa, PeriApu, F_BNCI_IDSeq2_K291, IDSeq2);
    //
    SQLType := stIns;
    //
    TabIns := EfdIcmsIpi_PF.DefineTabelaInsProdOuSubPrd(TabProdOuSubPrd, TabDst,
    'efdicmsipik29subprd', sProcName);
    //
    Result := USQLDB.SQL_DB_I_U(Dmod.MyDB, SQLType, TabIns, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'COD_ITEM',
    'QTD', 'GraGruX', 'OriOpeProc',
    'JmpMovID', 'JmpMovCod', 'JmpNivel1',
    'JmpNivel2', 'PaiMovID', 'PaiMovCod',
    'PaiNivel1', 'PaiNivel2'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, COD_ITEM,
    QTD, GraGruX, OriOpeProc,
    JmpMovID, JmpMovCod, JmpNivel1,
    JmpNivel2, PaiMovID, PaiMovCod,
    PaiNivel1, PaiNivel2], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True, _IGNORE_, RecordsAffected);
    //
    ConfirmaIDSeq(REG, Result, RecordsAffected, ['KndItm', 'GraGruX'],
    [KndItm, GraGruX], MeAviso, IDSeq2);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291(ImporExpor, AnoMes, Empresa,
  PeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
  PaiMovCod, PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
  GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod, Controle, ClientMO,
  FornecMO, EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
  TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
  TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K291()';
const
  TabDst   = 'efdicmsipik291';
  REG      = 'K291';
  //RegPai   = 'K290';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
  //
var
  COD_ITEM, TabIns: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  ESTSTabSorc,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  //
  TabProdOuSubPrd: TTabProdOuSubPrd;
begin
  Result := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  if Qtde = 0 then
  begin
    EfdIcmsIpi_PF.AvisoQtdeZero(MeAviso, KndItm, GraGruX, MovimCod, Controle, REG);
    Exit;
  end;
  //
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  TabProdOuSubPrd := EfdIcmsIpi_PF.DefineTabProdOuSubPrd(GraGruX);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    TabProdOuSubPrd, MeAviso, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    TabIns := EfdIcmsIpi_PF.DefineTabelaInsProdOuSubPrd(TabProdOuSubPrd, TabDst,
    'efdicmsipik29subprd', sProcName);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabIns, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'COD_ITEM',
    'QTD', 'GraGruX', 'OriOpeProc',
    'JmpMovID', 'JmpMovCod', 'JmpNivel1',
    'JmpNivel2', 'PaiMovID', 'PaiMovCod',
    'PaiNivel1', 'PaiNivel2'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, COD_ITEM,
    QTD, GraGruX, OriOpeProc,
    JmpMovID, JmpMovCod, JmpNivel1,
    JmpNivel2, PaiMovID, PaiMovCod,
    PaiNivel1, PaiNivel2], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292_Fast(ImporExpor,
  AnoMes, Empresa, PeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
  PaiMovID, PaiMovCod, PaiNivel1, PaiNivel2: Integer; DtMovim,
  DtCorrApo: TDateTime; GraGruX: Integer; Qtde: Double; MovimID, Codigo,
  MovimCod, Controle, ClientMO, FornecMO, EntiSitio: Integer;
  OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc: TEstqSPEDTabSorc;
  const MeAviso: TMemo; var F_BNCI_IDSeq2_K292: Integer): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292_Fast()';
const
  TabDst   = 'efdicmsipik292';
  REG      = 'K292';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
var
  COD_ITEM: String;
  ESTSTabSorc,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  RecordsAffected: Int64;
begin
  Result := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  if Qtde = 0 then
  begin
    EfdIcmsIpi_PF.AvisoQtdeZero(MeAviso, KndItm, GraGruX, MovimCod, Controle, REG);
    Exit;
  end;
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270_Fast(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    TTabProdOuSubPrd.tpspProducao, MeAviso, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New_Fast(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
{
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'COD_ITEM',
    'QTD', 'GraGruX', 'OriOpeProc',
    'JmpMovID', 'JmpMovCod', 'JmpNivel1',
    'JmpNivel2', 'PaiMovID', 'PaiMovCod',
    'PaiNivel1', 'PaiNivel2'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, COD_ITEM,
    QTD, GraGruX, OriOpeProc,
    JmpMovID, JmpMovCod, JmpNivel1,
    JmpNivel2, PaiMovID, PaiMovCod,
    PaiNivel1, PaiNivel2], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
}
  end else
  begin
    DefineIDSeq2(TabDst, ImporExpor, AnoMes, Empresa, PeriApu, F_BNCI_IDSeq2_K292, IDSeq2);
    //
    SQLType := stIns;
    //
    Result := USQLDB.SQL_DB_I_U(Dmod.MyDB, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'COD_ITEM',
    'QTD', 'GraGruX', 'OriOpeProc',
    'JmpMovID', 'JmpMovCod', 'JmpNivel1',
    'JmpNivel2', 'PaiMovID', 'PaiMovCod',
    'PaiNivel1', 'PaiNivel2'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, COD_ITEM,
    QTD, GraGruX, OriOpeProc,
    JmpMovID, JmpMovCod, JmpNivel1,
    JmpNivel2, PaiMovID, PaiMovCod,
    PaiNivel1, PaiNivel2], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True, _IGNORE_, RecordsAffected);
    //
    ConfirmaIDSeq(REG, Result, RecordsAffected, ['KndItm', 'GraGruX'],
    [KndItm, GraGruX], MeAviso, IDSeq2);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292(ImporExpor, AnoMes, Empresa,
  PeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, PaiMovCod,
  PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime; GraGruX: Integer;
  Qtde: Double; MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc;
  OriESTSTabSorc: TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
  const TabSorc: TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K292()';
const
  TabDst   = 'efdicmsipik292';
  REG      = 'K292';
  //RegPai   = 'K290';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
var
  COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  ESTSTabSorc,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  if Qtde = 0 then
  begin
    EfdIcmsIpi_PF.AvisoQtdeZero(MeAviso, KndItm, GraGruX, MovimCod, Controle, REG);
    Exit;
  end;
  //
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    TTabProdOuSubPrd.tpspProducao, MeAviso, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'COD_ITEM',
    'QTD', 'GraGruX', 'OriOpeProc',
    'JmpMovID', 'JmpMovCod', 'JmpNivel1',
    'JmpNivel2', 'PaiMovID', 'PaiMovCod',
    'PaiNivel1', 'PaiNivel2'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, COD_ITEM,
    QTD, GraGruX, OriOpeProc,
    JmpMovID, JmpMovCod, JmpNivel1,
    JmpNivel2, PaiMovID, PaiMovCod,
    PaiNivel1, PaiNivel2], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300_Fast(const ImporExpor, AnoMes, Empresa,
  PeriApu, MovimID, Codigo, MovimCod, CodDocOP: Integer; const DataIni, DataFim,
  DtMovim,  DtCorrApo: TDateTime; const ClientMO, FornecMO, EntiSitio: Integer;
  const OrigemOpeProc: TOrigemOpeProc; const DiaFim: TDateTime;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc: TEstqSPEDTabSorc;
  const MeAviso: TMemo; var IDSeq1, F_BNCI_IDSeq1_K300: Integer): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300_Fast()';
begin
  if not VAR_AVISA_FALTA_FAZER_InsereItemAtual_K300_Fast then
  begin
    Geral.MB_Info('Falta fazer: ' + sProcName);
    VAR_AVISA_FALTA_FAZER_InsereItemAtual_K300_Fast := True;
  end;
  Result := InsereItemAtual_K300(ImporExpor, AnoMes, Empresa, PeriApu, MovimID,
  Codigo, MovimCod, CodDocOP, DataIni, DataFim, DtMovim, DtCorrApo, ClientMO,
  FornecMO, EntiSitio, OrigemOpeProc, DiaFim, TipoPeriodoFiscal, TabSorc,
  MeAviso, IDSeq1);
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K300(const ImporExpor, AnoMes, Empresa,
  PeriApu, MovimID, Codigo, MovimCod, CodDocOP: Integer; const DataIni, DataFim,
  DtMovim,  DtCorrApo: TDateTime; const ClientMO, FornecMO, EntiSitio: Integer;
  const OrigemOpeProc: TOrigemOpeProc; const DiaFim: TDateTime;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc: TEstqSPEDTabSorc;
  const MeAviso: TMemo; var IDSeq1: Integer): Boolean;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik300';
  REG      = 'K300';
var
  DT_PROD: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, OriOpeProc: Integer;
  SQLType: TSQLType;
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result         := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  //
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := 0(*Controle*);
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  IDSeq1         := -1;
  //ID_SEK         := ;
  //K250 >> DT_PROD        := EfdIcmsIpi_PF.DataFimObrigatoria(DtHrFimOpe, DiaFim);
  DT_PROD        := Geral.FDT(DtMovim, 1);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  if DtCorrApo > 1 then
  begin
    // N�o existe registro K270 e K280 para para K300!
    (*
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd, MeAviso, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K270', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    Qtde, sebalVSMovItX, SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    OrigemSPEDEFDKnd, Agrega=True, MeAviso);
    //
    *)
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND DT_PROD="' + DT_PROD + '"',
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      MeAviso.Lines.Add('K300 j� importado > KndNSU: ' + Geral.FF0(KndNSU));
      Exit;
    end else
    begin
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'ID_SEK', 'DT_PROD', 'OriOpeProc'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    ID_SEK, DT_PROD, OriOpeProc], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
    //
    if not Result then
      IDSeq1 := -1;
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301_Fast(ImporExpor,
  AnoMes, Empresa, PeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
  PaiMovID, PaiMovCod, PaiNivel1, PaiNivel2: Integer; DtMovim,
  DtCorrApo: TDateTime; GraGruX: Integer; Qtde: Double; MovimID, Codigo,
  MovimCod, Controle, ClientMO, FornecMO, EntiSitio: Integer;
  OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc: TEstqSPEDTabSorc;
  const MeAviso: TMemo; var F_BNCI_IDSeq2_K301: Integer): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301_Fast()';
begin
  if not VAR_AVISA_FALTA_FAZER_InsereItemAtual_K301_Fast then
  begin
    Geral.MB_Info('Falta fazer: ' + sProcName);
    VAR_AVISA_FALTA_FAZER_InsereItemAtual_K301_Fast := True;
  end;
  Result := InsereItemAtual_K301(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
  JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod, PaiNivel1,
  PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde, MovimID, Codigo, MovimCod,
  Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
  TipoPeriodoFiscal, TabSorc, MeAviso);
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301(ImporExpor, AnoMes, Empresa,
  PeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID,
  PaiMovCod, PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime;
  GraGruX: Integer; Qtde: Double; MovimID, Codigo, MovimCod, Controle, ClientMO,
  FornecMO, EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc:
  TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc:
  TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K301()';
const
  TabDst   = 'efdicmsipik301';
  REG      = 'K301';
  //RegPai   = 'K300';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
var
  COD_ITEM, TabIns: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  ESTSTabSorc,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  TabProdOuSubPrd: TTabProdOuSubPrd;
begin
  Result := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  if Qtde = 0 then
  begin
    EfdIcmsIpi_PF.AvisoQtdeZero(MeAviso, KndItm, GraGruX, MovimCod, Controle, REG);
    Exit;
  end;
  //
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  TabProdOuSubPrd := EfdIcmsIpi_PF.DefineTabProdOuSubPrd(GraGruX);
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    TabProdOuSubPrd, MeAviso, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    TabIns := EfdIcmsIpi_PF.DefineTabelaInsProdOuSubPrd(TabProdOuSubPrd, TabDst,
    'efdicmsipik30subprd', sProcName);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabIns, False, [
    //Result := USQLDB.SQL_DB_I_U(Dmod.MyDB, SQLType, TabIns, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'COD_ITEM',
    'QTD', 'GraGruX', 'OriOpeProc',
    'JmpMovID', 'JmpMovCod', 'JmpNivel1',
    'JmpNivel2', 'PaiMovID', 'PaiMovCod',
    'PaiNivel1', 'PaiNivel2'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, COD_ITEM,
    QTD, GraGruX, OriOpeProc,
    JmpMovID, JmpMovCod, JmpNivel1,
    JmpNivel2, PaiMovID, PaiMovCod,
    PaiNivel1, PaiNivel2], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302(ImporExpor, AnoMes, Empresa,
  PeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1,
  JmpNivel2, PaiMovID, PaiMovCod,
  PaiNivel1, PaiNivel2: Integer; DtMovim, DtCorrApo: TDateTime; GraGruX: Integer;
  Qtde: Double; MovimID, Codigo, MovimCod, Controle, ClientMO, FornecMO,
  EntiSitio: Integer; OrigemOpeProc: TOrigemOpeProc;
  OriESTSTabSorc: TEstqSPEDTabSorc; const TipoPeriodoFiscal: TTipoPeriodoFiscal;
  const TabSorc: TEstqSPEDTabSorc; const MeAviso: TMemo): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302()';
const
  TabDst   = 'efdicmsipik302';
  REG      = 'K302';
  //RegPai   = 'K300';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  OrigemIDKnd = oidk000ND;
  OrigemSPEDEFDKnd = TOrigemSPEDEFDKnd.osekProducao;
var
  COD_ITEM: String;
  //ImporExpor, AnoMes, Empresa, PeriApu,
  ESTSTabSorc,
  KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, IDSeq2, OriOpeProc: Integer;
  QTD: Double;
  SQLType: TSQLType;
  //
  SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
begin
  Result := False;
  VerificaDtCorrApo(REG, DtCorrApo, OrigemOpeProc);
  if Qtde = 0 then
  begin
    EfdIcmsIpi_PF.AvisoQtdeZero(MeAviso, KndItm, GraGruX, MovimCod, Controle, REG);
    Exit;
  end;
  //
  //
  SQLType        := stNil;
  //ImporExpor     := FImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  //PeriApu        := FPeriApu;
  //
  KndTab         := Integer(TabSorc);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := Integer(MovimID);
  KndNiv         := 0; //Integer(MovimNiv);
  //
  //IDSeq1         := ;
  IDSeq2         := 0;
  //ID_SEK         := ;
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD            := Qtde;
  //COD_INS_SUBST  := ;
  //GraGruX        := ;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OrigemOpeProc);
  //
  //
  if DtCorrApo > 1 then
  begin
    if not EfdIcmsIpi_PF.ObtemSPED_EFD_KndRegOrigem(REG,
    SPED_EFD_KndRegOrigem) then Exit;
    //
    Result := InsereItemAtual_K270(ImporExpor, AnoMes, Empresa, PeriApu,
    REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, Qtde, SPED_EFD_KndRegOrigem,
    OrigemOpeProc, OrigemIDKnd, OrigemSPEDEFDKnd,
    TTabProdOuSubPrd.tpspProducao, MeAviso, IDSeq1); // Item de origem
    //
    EfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(TEstqSPEDTabSorc.estsVMI,
    TipoPeriodoFiscal, ImporExpor, AnoMes,
    Empresa, PeriApu, 'K275', REG, DtMovim, DtCorrApo, MovimID, Codigo,
    MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio, slkPrefSitio,
    -Qtde, sebalVSMovItX,
    SPED_EFD_KndRegOrigem, estsVMI, OrigemOpeProc, OrigemIDKnd,
    TOrigemSPEDEFDKnd.osekProducao, (*Agrega*)True, MeAviso);
    //
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //Geral.MB_SQL(Self, DmProd.QrX999);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
      IDSeq2 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq2', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq2, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'IDSeq1',
    'IDSeq2', 'ID_SEK', 'COD_ITEM',
    'QTD', 'GraGruX', 'OriOpeProc',
    'JmpMovID', 'JmpMovCod', 'JmpNivel1',
    'JmpNivel2', 'PaiMovID', 'PaiMovCod',
    'PaiNivel1', 'PaiNivel2'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm'], [
    KndAID, KndNiv, IDSeq1,
    IDSeq2, ID_SEK, COD_ITEM,
    QTD, GraGruX, OriOpeProc,
    JmpMovID, JmpMovCod, JmpNivel1,
    JmpNivel2, PaiMovID, PaiMovCod,
    PaiNivel1, PaiNivel2], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm], True);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302_Fast(ImporExpor,
  AnoMes, Empresa, PeriApu, IDSeq1, JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2,
  PaiMovID, PaiMovCod, PaiNivel1, PaiNivel2: Integer; DtMovim,
  DtCorrApo: TDateTime; GraGruX: Integer; Qtde: Double; MovimID, Codigo,
  MovimCod, Controle, ClientMO, FornecMO, EntiSitio: Integer;
  OrigemOpeProc: TOrigemOpeProc; OriESTSTabSorc: TEstqSPEDTabSorc;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; const TabSorc: TEstqSPEDTabSorc;
  const MeAviso: TMemo; var F_BNCI_IDSeq2_K302: Integer): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K302_Fast()';
begin
  if not VAR_AVISA_FALTA_FAZER_InsereItemAtual_K302_Fast then
  begin
    Geral.MB_Info('Falta fazer: ' + sProcName);
    VAR_AVISA_FALTA_FAZER_InsereItemAtual_K302_Fast := True;
  end;
  //
  Result := InsereItemAtual_K302(ImporExpor, AnoMes, Empresa, PeriApu, IDSeq1,
  JmpMovID, JmpMovCod, JmpNivel1, JmpNivel2, PaiMovID, PaiMovCod, PaiNivel1,
  PaiNivel2, DtMovim, DtCorrApo, GraGruX, Qtde, MovimID, Codigo, MovimCod,
  Controle, ClientMO, FornecMO, EntiSitio, OrigemOpeProc, OriESTSTabSorc,
  TipoPeriodoFiscal, TabSorc, MeAviso);
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New(BalTab: TEstqSPEDTabSorc;
  TipoPeriodoFiscal: TTipoPeriodoFiscal; ImporExpor, AnoMes, Empresa,
  K100_PeriApu: Integer; const RegPai, RegAvo: String; const DataSPED,
  DtCorrApo: TDateTime; const MovimID, Codigo, MovimCod, Controle, GraGruX,
  ClientMO, FornecMO, EntiSitio: Integer; LocalKnd: TSPEDLocalKnd;
  const Qtde: Double; const OrigemBalID: TSPED_EFD_Bal;
  const SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  const OriESTSTabSorc: TEstqSPEDTabSorc;
  const OriOrigemOpeProc: TOrigemOpeProc; const OriOrigemIDKnd: TOrigemIDKnd;
  const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd; const Agrega: Boolean;
  const MeAviso: TMemo): Boolean;
  //
  function Escolhe(A, B: Integer): Integer;
  begin
    if A <> 0 then
      Result := A
    else
      Result := B;
  end;
const
  sProcName = '"UnEfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New()"';
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efdicmsipik280';
  REG      = 'K280';
  ValAntes = 0;
{
var
  COD_ITEM, IND_EST, COD_PART, DT_EST: String;
  BalID, BalCod, BalNum, BalItm, BalEnt, Grandeza, Entidade, Tipo_Item, ESTSTabSorc,
  OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd: Integer;
  QTD_COR_POS, QTD_COR_NEG, Pecas, AreaM2, PesoKg: Double;
  SQLType: TSQLType;
  OriGrandeza: TGrandezaUnidMed;
  DtAtu: TDateTime;
  AMIni, AMFim, AMAtu, Local: Integer;
  SQL_POS_NEG: String;
  DebCred: TDebCred;
begin
  // erro se periodo for decendial
  if TipoPeriodoFiscal <> TTipoPeriodoFiscal.spedperMensal then
  begin
    Geral.MB_Aviso('Per�odo de apura��o n�o implementado para o registro K280!'
    + sLineBreak + 'Avise a Dermatek!');
    //
    Exit;
  end;
  //
  SQLType        := stIns;
  Result         := False;
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  //
  if Qtde >= 0 then
    DebCred := TDebCred.debcredCred
  else
    DebCred := TDebCred.debcredDeb;
  case DebCred of
    TDebCred.debcredCred:
    begin
      QTD_COR_POS  := Qtde;
      SQL_POS_NEG := 'AND QTD_COR_POS > 0 ';
    end;
    TDebCred.debcredDeb:
    begin
      QTD_COR_NEG  := -Qtde;
      SQL_POS_NEG := 'AND QTD_COR_POS < 0 ';
    end;
    else
    begin
      QTD_COR_POS    := 0;
      QTD_COR_NEG    := 0;
      SQL_POS_NEG := 'AND QTD_COR_??? >?< 0 ';
    end;
  end;
  //
  //IND_EST        := ;
  //COD_PART       := ;
  Local := 0;
  case LocalKnd of
    (*0*)slkND: ; // Zero!
    (*1*)slkQualquer:    Local := Escolhe(EntiSitio, FornecMO);
    (*2*)slkForcaFornec: Local := FornecMO;
    (*3*)slkForcaSitio:  Local := EntiSitio;
    (*4*)slkPrefFornec:  Local := Escolhe(FornecMO, EntiSitio);
    (*5*)slkPrefSitio:   Local := Escolhe(EntiSitio, FornecMO);
    (*?*)else Geral.MB_Erro(
    '"LocalKnd" n�o implementado em "UnEfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280()"');
  end;
  if Local = 0 then Geral.MB_Erro(
    '"Local" indefinido em "UnEfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280()"' +
    sLineBreak + 'MovimID: ' + Geral.FF0(MovimID) +
    sLineBreak + 'Codigo: ' + Geral.FF0(Codigo) +
    sLineBreak + 'IME-C: ' + Geral.FF0(MovimCod) +
    sLineBreak + 'IME-I: ' + Geral.FF0(Controle) +
    sLineBreak + 'Reduzido: ' + Geral.FF0(GraGruX) +
    sLineBreak + 'ClientMO: ' + Geral.FF0(ClientMO) +
    sLineBreak + 'FornecMO: ' + Geral.FF0(FornecMO) +
    sLineBreak + 'EntiSitio: ' + Geral.FF0(EntiSitio)+
    sLineBreak);
  EfdIcmsIpi_PF.ObtemIND_ESTeCOD_PART(Empresa, ClientMO, Local, GraGruX,
    IND_EST, COD_PART, Entidade);
  BalID          := MovimID;
  BalCod         := Codigo;
  BalNum         := MovimCod;
  BalItm         := Controle;
  BalEnt         := Geral.IMV(COD_PART);
  Grade_PF.ObtemGrandezaDeGraGruX(GraGruX, OriGrandeza, Tipo_Item);
  Grandeza       := Integer(OriGrandeza);
  Pecas          := 0;
  AreaM2         := 0;
  PesoKg         := 0;
  case OriGrandeza of
    (*0*)gumPeca   : Pecas  := Qtde;
    (*1*)gumAreaM2 : AreaM2 := Qtde;
    (*2*)gumPesoKG : PesoKg := Qtde;
    else Geral.MB_Aviso(
    'Grandeza inesperada em "FmSPED_EFD_K2XX.InsereItensAtuais_K280()"');
  end;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OriOrigemOpeProc);
  OrigemIDKnd    := Integer(OriOrigemIDKnd);
  OriSPEDEFDKnd  := Integer(OrigemSPEDEFDKnd);
  //
  AMIni := DmkPF.DataToAnoMes(DtCorrApo);
  AMFim := DmkPF.DataToAnoMes(DataSPED);
  AMAtu := AMINi;
  while AMAtu < AMFim do
  begin
    DtAtu          := IncMonth(Geral.AnoMesToData(AMAtu, 1), 1) - 1;
    DT_EST         := Geral.FDT(DtAtu, 1);
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND DT_EST="' + DT_EST + '"',
    'AND BalID=' + Geral.FF0(BalID),
    'AND BalNum=' + Geral.FF0(BalNum),
    'AND BalItm=' + Geral.FF0(BalItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    'AND DebCred=' + Geral.FF0(Integer(DebCred)),
    '']);
    if DmProd.QrX999.RecordCount > 0 then
      Exit;
    //Geral.MB_SQL(Self, DmProd.QrX999);
    begin
      LinArq := 0;
      LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'LinArq', [
      (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
      stIns, LinArq, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'REG', 'RegisPai', 'RegisAvo',
    'K100', 'DT_EST', 'COD_ITEM',
    'QTD_COR_POS', 'QTD_COR_NEG', 'IND_EST',
    'COD_PART', 'BalTab', 'BalID',
    'BalCod', 'BalNum',
    'BalItm', 'BalEnt', 'GraGruX',
    'Grandeza', 'Pecas', 'AreaM2',
    'PesoKg', 'Entidade', 'Tipo_Item',
    'ESTSTabSorc', 'OriOpeProc', 'OrigemIDKnd',
    'OriSPEDEFDKnd', 'DebCred'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
    REG, RegPai, RegAvo,
    K100, DT_EST, COD_ITEM,
    QTD_COR_POS, QTD_COR_NEG, IND_EST,
    COD_PART, Integer(BalTab), BalID,
    BalCod, BalNum,
    BalItm, BalEnt, GraGruX,
    Grandeza, Pecas, AreaM2,
    PesoKg, Entidade, Tipo_Item,
    ESTSTabSorc, OriOpeProc, OrigemIDKnd,
    OriSPEDEFDKnd, Integer(DebCred)], [
    ImporExpor, AnoMes, Empresa, LinArq], True);
    //
    AMAtu := dmkPF.IncrementaAnoMes(AMAtu, 1);
  end;
}
var
  DT_EST, COD_ITEM, IND_EST, COD_PART, RegisPai, RegisAvo: String;
  //ImporExpor, AnoMes, Empresa, GraGruX,
  PeriApu, KndTab, KndCod, KndNSU, KndItm, KndAID, KndNiv, DebCred, Grandeza,
  Entidade, Tipo_Item, ESTSTabSorc, OriOpeProc, OrigemIDKnd,
  OriSPEDEFDKnd, OriBalID, OriKndReg: Integer;
  QTD_COR_POS, QTD_COR_NEG, Pecas, AreaM2, PesoKg: Double;
  SQLType: TSQLType;
  //
  DebiCredi: TDebCred;
  OriGrandeza: TGrandezaUnidMed;
  AMIni, AMFim, AMAtu, Local, IDSeq1: Integer;
  DtAtu: TDateTime;
  //
  procedure Msg(Texto: String);
  begin
    Geral.MB_Erro(Texto +
    sLineBreak + sProcName +
    sLineBreak + 'MovimID: ' + Geral.FF0(MovimID) +
    sLineBreak + 'Codigo: ' + Geral.FF0(Codigo) +
    sLineBreak + 'IME-C: ' + Geral.FF0(MovimCod) +
    sLineBreak + 'IME-I: ' + Geral.FF0(Controle) +
    sLineBreak + 'Reduzido: ' + Geral.FF0(GraGruX) +
    sLineBreak + 'ClientMO: ' + Geral.FF0(ClientMO) +
    sLineBreak + 'FornecMO: ' + Geral.FF0(FornecMO) +
    sLineBreak + 'EntiSitio: ' + Geral.FF0(EntiSitio)+
    sLineBreak);
  end;
begin
  Result         := False;
  // erro se periodo for decendial
  if TipoPeriodoFiscal <> TTipoPeriodoFiscal.spedperMensal then
  begin
    Geral.MB_Aviso('Per�odo de apura��o n�o implementado para o registro K280!'
    + sLineBreak + 'Avise a Dermatek!');
    //
    Exit;
  end;
  SQLType        := stNil;
  //ImporExpor     := ;    Acima
  //AnoMes         := ;    Acima
  //Empresa        := ;    Acima
  PeriApu        := K100_PeriApu;
  KndTab         := Integer(BalTab);
  KndCod         := Codigo;
  KndNSU         := MovimCod;
  KndItm         := Controle;
  KndAID         := MovimID;
  KndNiv         := 0;  // ??????  > n�o tem!
  DT_EST         := ''; // Abaixo
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  QTD_COR_POS    := 0;  // Abaixo
  QTD_COR_NEG    := 0;  // Abaixo
  IND_EST        := ''; // Abaixo EfdIcmsIpi_PF.ObtemIND_ESTeCOD_PART
  COD_PART       := ''; // Abaixo EfdIcmsIpi_PF.ObtemIND_ESTeCOD_PART
  DebCred        := 0;  // Abaixo
  //GraGruX        := ;    Acima
  Grandeza       := 0;  // Abaixo ObtemGrandezaDeGraGruX(
  Pecas          := 0;  // Abaixo
  AreaM2         := 0;  // Abaixo
  PesoKg         := 0;  // Abaixo
  Entidade       := 0;  // Abaixo EfdIcmsIpi_PF.ObtemIND_ESTeCOD_PART
  Tipo_Item      := 0;  // Abaixo ObtemGrandezaDeGraGruX(
  RegisPai       := RegPai;
  RegisAvo       := RegAvo;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OriOrigemOpeProc);
  OrigemIDKnd    := Integer(OriOrigemIDKnd);
  OriSPEDEFDKnd  := Integer(OrigemSPEDEFDKnd);
  OriBalID       := Integer(OrigemBalID);
  OriKndReg      := Integer(SPED_EFD_KndRegOrigem);
  //
  if Qtde >= 0 then
    DebiCredi    := TDebCred.debcredCred
  else
    DebiCredi    := TDebCred.debcredDeb;
  DebCred        := Integer(DebiCredi);
  QTD_COR_POS    := 0;
  QTD_COR_NEG    := 0;
  case DebiCredi of
    TDebCred.debcredCred: QTD_COR_POS  := Qtde;
    TDebCred.debcredDeb:  QTD_COR_NEG  := -Qtde;
  end;
  //
  Local := 0;
  case LocalKnd of
    (*0*)slkND: ; // Zero!
    (*1*)slkQualquer:    Local := Escolhe(EntiSitio, FornecMO);
    (*2*)slkForcaFornec: Local := FornecMO;
    (*3*)slkForcaSitio:  Local := EntiSitio;
    (*4*)slkPrefFornec:  Local := Escolhe(FornecMO, EntiSitio);
    (*5*)slkPrefSitio:   Local := Escolhe(EntiSitio, FornecMO);
    (*?*)else Msg(
    '"LocalKnd" n�o implementado!' +
    GetEnumName(TypeInfo(TSPEDLocalKnd), Integer(LocalKnd)));
  end;
  if Local = 0 then Msg(
    '"Local" indefinido!');
  EfdIcmsIpi_PF.ObtemIND_ESTeCOD_PART(Empresa, ClientMO, Local, GraGruX,
    IND_EST, COD_PART, Entidade);
  Grade_PF.ObtemGrandezaDeGraGruX(GraGruX, OriGrandeza, Tipo_Item);
  Grandeza       := Integer(OriGrandeza);
  case OriGrandeza of
    (*0*)gumPeca   : Pecas  := Qtde;
    (*1*)gumAreaM2 : AreaM2 := Qtde;
    (*2*)gumPesoKG : PesoKg := Qtde;
    else Msg(
    'Grandeza inesperada!' +
    sLineBreak + 'Grandeza: ' + Geral.FF0(Integer(OriGrandeza)) + ' >> ' +
    GetEnumName(TypeInfo(TGrandezaUnidMed), Integer(OriGrandeza)));
  end;
  //
  AMIni := DmkPF.DataToAnoMes(DtCorrApo);
  AMFim := DmkPF.DataToAnoMes(DataSPED);
  AMAtu := AMINi;
  while AMAtu < AMFim do
  begin
    DtAtu          := IncMonth(Geral.AnoMesToData(AMAtu, 1), 1) - 1;
    DT_EST         := Geral.FDT(DtAtu, 1);
(*
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND DT_EST="' + DT_EST + '"',
    'AND BalID=' + Geral.FF0(BalID),
    'AND BalNum=' + Geral.FF0(BalNum),
    'AND BalItm=' + Geral.FF0(BalItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    'AND DebCred=' + Geral.FF0(Integer(DebCred)),
    '']);
*)
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND PeriApu=' + Geral.FF0(PeriApu),
    'AND KndTab=' + Geral.FF0(KndTab),
    'AND KndCod=' + Geral.FF0(KndCod),
    'AND KndNSU=' + Geral.FF0(KndNSU),
    'AND KndItm=' + Geral.FF0(KndItm),
    'AND DT_EST="' + DT_EST + '"',
    'AND DebCred=' + Geral.FF0(Integer(DebCred)),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    if DmProd.QrX999.RecordCount > 0 then
    begin
      if MeAviso <> nil then
      EfdIcmsIpi_PF.AvisoItemJaImportado(MeAviso, KndItm, GraGruX, REG);
      Exit;
    end else
    begin
{
      LinArq := 0;
      LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'LinArq', [
      (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
      stIns, LinArq, siPositivo, nil);
      //
}
      IDSeq1 := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'IDSeq1', [
      'ImporExpor', 'AnoMes', 'Empresa', 'PeriApu'], [
      ImporExpor, AnoMes, Empresa, PeriApu],
      stIns, IDSeq1, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'KndAID', 'KndNiv', 'COD_ITEM',
    'QTD_COR_POS', 'QTD_COR_NEG', 'IND_EST',
    'COD_PART', 'Grandeza',
    'Pecas', 'AreaM2', 'PesoKg',
    'Entidade', 'Tipo_Item', 'RegisPai',
    'RegisAvo', 'ESTSTabSorc', 'OriOpeProc',
    'OrigemIDKnd', 'OriSPEDEFDKnd', 'OriBalID',
    'OriKndReg', 'IDSeq1', 'ID_SEK'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'PeriApu', 'KndTab', 'KndCod',
    'KndNSU', 'KndItm', 'DT_EST',
    'DebCred', 'GraGruX'], [
    KndAID, KndNiv, COD_ITEM,
    QTD_COR_POS, QTD_COR_NEG, IND_EST,
    COD_PART, Grandeza,
    Pecas, AreaM2, PesoKg,
    Entidade, Tipo_Item, RegisPai,
    RegisAvo, ESTSTabSorc, OriOpeProc,
    OrigemIDKnd, OriSPEDEFDKnd, OriBalID,
    OriKndReg, IDSeq1, ID_SEK], [
    ImporExpor, AnoMes, Empresa,
    PeriApu, KndTab, KndCod,
    KndNSU, KndItm, DT_EST,
    DebCred, GraGruX], True);
    //
    AMAtu := dmkPF.IncrementaAnoMes(AMAtu, 1);
  end;
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New_Fast(
  BalTab: TEstqSPEDTabSorc; TipoPeriodoFiscal: TTipoPeriodoFiscal; ImporExpor,
  AnoMes, Empresa, K100_PeriApu: Integer; const RegPai, RegAvo: String;
  const DataSPED, DtCorrApo: TDateTime; const MovimID, Codigo, MovimCod,
  Controle, GraGruX, ClientMO, FornecMO, EntiSitio: Integer;
  LocalKnd: TSPEDLocalKnd; const Qtde: Double; const OrigemBalID: TSPED_EFD_Bal;
  const SPED_EFD_KndRegOrigem: TSPED_EFD_KndRegOrigem;
  const OriESTSTabSorc: TEstqSPEDTabSorc;
  const OriOrigemOpeProc: TOrigemOpeProc; const OriOrigemIDKnd: TOrigemIDKnd;
  const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd; const Agrega: Boolean;
  const MeAviso: TMemo): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_New_Fast()';
begin
  if not VAR_AVISA_FALTA_FAZER_InsereItensAtuais_K280_New_Fast then
  begin
    Geral.MB_Info('Falta fazer: ' + sProcName);
    VAR_AVISA_FALTA_FAZER_InsereItensAtuais_K280_New_Fast := True;
  end;
  //
  Result := InsereItensAtuais_K280_New_Fast(BalTab, TipoPeriodoFiscal,
  ImporExpor, AnoMes, Empresa, K100_PeriApu, RegPai, RegAvo, DataSPED,
  DtCorrApo, MovimID, Codigo, MovimCod, Controle, GraGruX, ClientMO, FornecMO,
  EntiSitio, LocalKnd, Qtde, OrigemBalID, SPED_EFD_KndRegOrigem, OriESTSTabSorc,
  OriOrigemOpeProc, OriOrigemIDKnd, OrigemSPEDEFDKnd, Agrega, MeAviso);
end;

function TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280_Old(BalTab: TEstqSPEDTabSorc;
  TipoPeriodoFiscal: TTipoPeriodoFiscal; ImporExpor, AnoMes, Empresa, K100:
  Integer; const RegPai, RegAvo: String; const DataSPED, DtCorrApo: TDateTime;
  const MovimID, Codigo, MovimCod, Controle, GraGruX, ClientMO, FornecMO,
  EntiSitio: Integer; LocalKnd: TSPEDLocalKnd; const Qtde: Double; const
  OrigemBalID: TSPED_EFD_Bal; const SPED_EFD_KndRegOrigem:
  TSPED_EFD_KndRegOrigem; const OriESTSTabSorc: TEstqSPEDTabSorc; const
  OriOrigemOpeProc: TOrigemOpeProc; const OriOrigemIDKnd: TOrigemIDKnd; const
  OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd; const Agrega: Boolean; var LinArq:
  Integer): Boolean;
const
  sProcName = 'TUnEfdIcmsIpi_PF_v03_0_2_a.InsereItemAtual_K280_Old()';
  //
  function Escolhe(A, B: Integer): Integer;
  begin
    if A <> 0 then
      Result := A
    else
      Result := B;
  end;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efd_k280';
  REG      = 'K280';
  ValAntes = 0;
var
  COD_ITEM, IND_EST, COD_PART, DT_EST: String;
  (*ImporExpor, AnoMes, Empresa,*)
  BalID, BalCod, BalNum, BalItm, BalEnt, Grandeza, Entidade, Tipo_Item, ESTSTabSorc,
  OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd: Integer;
  QTD_COR_POS, QTD_COR_NEG, Pecas, AreaM2, PesoKg: Double;
  SQLType: TSQLType;
  OriGrandeza: TGrandezaUnidMed;
  DtAtu: TDateTime;
  AMIni, AMFim, AMAtu, Local: Integer;
  SQL_POS_NEG: String;
  DebCred: TDebCred;
begin
  // erro se periodo for decendial
  if TipoPeriodoFiscal <> TTipoPeriodoFiscal.spedperMensal then
  begin
    Geral.MB_Aviso('Per�odo de apura��o n�o implementado para o registro K280!'
    + sLineBreak + 'Avise a Dermatek!');
    //
    Exit;
  end;
  //
  SQLType        := stIns;
  Result         := False;
  //ImporExpor     := ImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  COD_ITEM       := EfdIcmsIpi_PF.TxtValidoDeGGX(GraGruX, REG, sProcName);
  //
  if Qtde >= 0 then
    DebCred := TDebCred.debcredCred
  else
    DebCred := TDebCred.debcredDeb;
  case DebCred of
    TDebCred.debcredCred:
    begin
      QTD_COR_POS  := Qtde;
      SQL_POS_NEG := 'AND QTD_COR_POS > 0 ';
    end;
    TDebCred.debcredDeb:
    begin
      QTD_COR_NEG  := -Qtde;
      SQL_POS_NEG := 'AND QTD_COR_POS < 0 ';
    end;
    else
    begin
      QTD_COR_POS    := 0;
      QTD_COR_NEG    := 0;
      SQL_POS_NEG := 'AND QTD_COR_??? >?< 0 ';
    end;
  end;
  //
  //IND_EST        := ;
  //COD_PART       := ;
  Local := 0;
  case LocalKnd of
    (*0*)slkND: ; // Zero!
    (*1*)slkQualquer:    Local := Escolhe(EntiSitio, FornecMO);
    (*2*)slkForcaFornec: Local := FornecMO;
    (*3*)slkForcaSitio:  Local := EntiSitio;
    (*4*)slkPrefFornec:  Local := Escolhe(FornecMO, EntiSitio);
    (*5*)slkPrefSitio:   Local := Escolhe(EntiSitio, FornecMO);
    (*?*)else Geral.MB_Erro(
    '"LocalKnd" n�o implementado em "UnEfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280()"');
  end;
(*
"Local" indefinido em "UnEfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280()"
MovimID: 24
Codigo: 21
IME-C: 1671
IME-I: 0
Reduzido: 138
ClientMO: -11
FornecMO: 0
EntiSitio: 0
*)
  if Local = 0 then Geral.MB_Erro(
    '"Local" indefinido em "UnEfdIcmsIpi_PF_v03_0_2_a.InsereItensAtuais_K280()"' +
    sLineBreak + 'MovimID: ' + Geral.FF0(MovimID) +
    sLineBreak + 'Codigo: ' + Geral.FF0(Codigo) +
    sLineBreak + 'IME-C: ' + Geral.FF0(MovimCod) +
    sLineBreak + 'IME-I: ' + Geral.FF0(Controle) +
    sLineBreak + 'Reduzido: ' + Geral.FF0(GraGruX) +
    sLineBreak + 'ClientMO: ' + Geral.FF0(ClientMO) +
    sLineBreak + 'FornecMO: ' + Geral.FF0(FornecMO) +
    sLineBreak + 'EntiSitio: ' + Geral.FF0(EntiSitio)+
    sLineBreak);
  //EfdIcmsIpi_PF.ObtemIND_ESTeCOD_PART(Empresa, ClientMO, FornecMO, GraGruX,
    //IND_EST, COD_PART, Entidade);
  EfdIcmsIpi_PF.ObtemIND_ESTeCOD_PART(Empresa, ClientMO, Local, GraGruX,
    IND_EST, COD_PART, Entidade);
  BalID          := MovimID;
  BalCod         := Codigo;
  BalNum         := MovimCod;
  BalItm         := Controle;
  BalEnt         := Geral.IMV(COD_PART);
  Grade_PF.ObtemGrandezaDeGraGruX(GraGruX, OriGrandeza, Tipo_Item);
  Grandeza       := Integer(OriGrandeza);
  Pecas          := 0;
  AreaM2         := 0;
  PesoKg         := 0;
  case OriGrandeza of
    (*0*)gumPeca   : Pecas  := Qtde;
    (*1*)gumAreaM2 : AreaM2 := Qtde;
    (*2*)gumPesoKG : PesoKg := Qtde;
    else Geral.MB_Aviso(
    'Grandeza inesperada em "FmSPED_EFD_K2XX.InsereItensAtuais_K280()"');
  end;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OriOrigemOpeProc);
  OrigemIDKnd    := Integer(OriOrigemIDKnd);
  OriSPEDEFDKnd  := Integer(OrigemSPEDEFDKnd);
  //
  AMIni := DmkPF.DataToAnoMes(DtCorrApo);
  AMFim := DmkPF.DataToAnoMes(DataSPED);
  AMAtu := AMINi;
  while AMAtu < AMFim do
  begin
    DtAtu          := IncMonth(Geral.AnoMesToData(AMAtu, 1), 1) - 1;
    DT_EST         := Geral.FDT(DtAtu, 1);
(*    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND K100=' + Geral.FF0(K100),
    'AND RegisPai="' + RegPai + '"',
    'AND DT_EST="' + DT_EST + '"',
    'AND ESTSTabSorc=' + Geral.FF0(ESTSTabSorc),
    'AND OriOpeProc=' + Geral.FF0(OriOpeProc),
    'AND OrigemIDKnd=' + Geral.FF0(BalItm),
    'AND OriSPEDEFDKnd=' + Geral.FF0(OriSPEDEFDKnd),
    'AND BalID=' + Geral.FF0(BalID),
    'AND BalNum=' + Geral.FF0(BalNum),
    'AND BalItm=' + Geral.FF0(BalItm),
    SQL_POS_NEG,
    '']);*)
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    //'AND K100=' + Geral.FF0(K100),
    //'AND RegisPai="' + RegPai + '"',
    'AND DT_EST="' + DT_EST + '"',
    //'AND ESTSTabSorc=' + Geral.FF0(ESTSTabSorc),
    //'AND OriOpeProc=' + Geral.FF0(OriOpeProc),
    //'AND OrigemIDKnd=' + Geral.FF0(BalItm),
    //'AND OriSPEDEFDKnd=' + Geral.FF0(OriSPEDEFDKnd),
    'AND BalID=' + Geral.FF0(BalID),
    'AND BalNum=' + Geral.FF0(BalNum),
    'AND BalItm=' + Geral.FF0(BalItm),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    'AND DebCred=' + Geral.FF0(Integer(DebCred)),
    '']);
    if DmProd.QrX999.RecordCount > 0 then
      Exit;
    //Geral.MB_SQL(Self, DmProd.QrX999);
    (*
    if DmProd.QrX999.RecordCount > 0 then
    begin
      LinArq  := DmProd.QrX999.FieldByName('LinArq').AsInteger;
      SQLType := stUpd;
      if Agrega then
      begin
        QTD_COR_POS := QTD_COR_POS + DmProd.QrX999.FieldByName('QTD_COR_POS').AsFloat;
        QTD_COR_NEG := QTD_COR_NEG + DmProd.QrX999.FieldByName('QTD_COR_NEG').AsFloat;
      end;
    end else
    *)
    begin
      LinArq := 0;
      LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'LinArq', [
      (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
      stIns, LinArq, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'REG', 'RegisPai', 'RegisAvo',
    'K100', 'DT_EST', 'COD_ITEM',
    'QTD_COR_POS', 'QTD_COR_NEG', 'IND_EST',
    'COD_PART', 'BalTab', 'BalID',
    'BalCod', 'BalNum',
    'BalItm', 'BalEnt', 'GraGruX',
    'Grandeza', 'Pecas', 'AreaM2',
    'PesoKg', 'Entidade', 'Tipo_Item',
    'ESTSTabSorc', 'OriOpeProc', 'OrigemIDKnd',
    'OriSPEDEFDKnd', 'DebCred'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
    REG, RegPai, RegAvo,
    K100, DT_EST, COD_ITEM,
    QTD_COR_POS, QTD_COR_NEG, IND_EST,
    COD_PART, Integer(BalTab), BalID,
    BalCod, BalNum,
    BalItm, BalEnt, GraGruX,
    Grandeza, Pecas, AreaM2,
    PesoKg, Entidade, Tipo_Item,
    ESTSTabSorc, OriOpeProc, OrigemIDKnd,
    OriSPEDEFDKnd, Integer(DebCred)], [
    ImporExpor, AnoMes, Empresa, LinArq], True);
    //
    AMAtu := dmkPF.IncrementaAnoMes(AMAtu, 1);
  end;
end;

procedure TUnEfdIcmsIpi_PF_v03_0_2_a.VerificaDtCorrApo(REG: String;
  DtCorrApo: TDateTime; OrigemOpeProc: TOrigemOpeProc);
begin
  if DtCorrApo = -1 then
    Geral.MB_Aviso(REG + ': DtCorrApo = -1 na OrigemOpeProc = ' +
    GetEnumName(TypeInfo(TOrigemOpeProc),Integer(OrigemOpeProc)));
end;

end.


