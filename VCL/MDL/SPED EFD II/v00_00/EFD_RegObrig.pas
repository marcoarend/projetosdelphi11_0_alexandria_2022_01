unit EFD_RegObrig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkStringGrid, dmkEdit;

type
  TFmEFD_RegObrig = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    GroupBox1: TGroupBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Grade1: TdmkStringGrid;
    TabSheet2: TTabSheet;
    Memo1: TMemo;
    Label27: TLabel;
    EdCaminho: TdmkEdit;
    SpeedButton8: TSpeedButton;
    PB1: TProgressBar;
    Grade2: TStringGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CarregaXLS();
  end;

  var
  FmEFD_RegObrig: TFmEFD_RegObrig;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmEFD_RegObrig.BtOKClick(Sender: TObject);
var
  I: Integer;
  Arquivo, Linha, UF, Versoes, URL: String;
  Versao: Double;
  lstArq: TStringList;
begin
  Memo1.Lines.Clear;
  if pos('http', LowerCase(EdCaminho.ValueVariant)) > 0 then
  begin
    Geral.MB_Info('Voc� precisa selecionar um arquivo um arquivo para abr�-lo!');
    Exit;
  end;
  //
  if MyObjects.Xls_To_StringGrid(Grade1, EdCaminho.Text, PB1, LaAviso1, LaAviso2, 1) then
  begin
    //BtCarrega.Enabled := True;
    CarregaXLS();
    PageControl1.ActivePageIndex := 1;
    //
    //BtSalvar.Visible := True;
  end;
end;

procedure TFmEFD_RegObrig.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_RegObrig.CarregaXLS();
var
  I, N: Integer;
  Bloco, Registro, Descri, Nivel, OcorrT, PerfA_ObrigE, ObrigT,
  PerfA_ObrigS, PerfB_ObrigE, PerfB_ObrigS, PerfC_ObrigE, PerfC_ObrigS: String;
begin
  Grade2.RowCount := 2;
  Grade2.ColCount := Grade1.ColCount;
  for I := 0 to Grade1.Colcount - 1 do
    Grade2.ColWidths[I] := Grade1.ColWidths[I];
  N := 0;
  //
(*
  Descri := '';
  Nivel  := '';
  OcorrT := '';
*)
  for I := 1 to Grade1.RowCount -1 do
  begin
    Bloco        := Trim(Grade1.Cells[01, I]);
    Descri       := Grade1.Cells[02, I];
    Registro     := Trim(Grade1.Cells[03, I]);
    Nivel        := Grade1.Cells[04, I];
    OcorrT       := Grade1.Cells[05, I];
    ObrigT       := Grade1.Cells[06, I];
    PerfA_ObrigE := Grade1.Cells[07, I];
    PerfA_ObrigS := Grade1.Cells[08, I];
    PerfB_ObrigE := Grade1.Cells[09, I];
    PerfB_ObrigS := Grade1.Cells[10, I];
    PerfC_ObrigE := Grade1.Cells[11, I];
    PerfC_ObrigS := Grade1.Cells[12, I];
    if (Bloco <> '') and (Registro <> '') then
    begin
      N := N + 1;
      Grade2.RowCount := N + 1;
      Grade2.Cells[00, N] := Grade1.Cells[00, N];
      //
      Grade2.Cells[01, N] := Bloco;
      Grade2.Cells[02, N] := Descri;
      Grade2.Cells[03, N] := Registro;
      Grade2.Cells[04, N] := Nivel;
      Grade2.Cells[05, N] := OcorrT;
      Grade2.Cells[06, N] := ObrigT;
      Grade2.Cells[07, N] := PerfA_ObrigE;
      Grade2.Cells[08, N] := PerfA_ObrigS;
      Grade2.Cells[09, N] := PerfB_ObrigE;
      Grade2.Cells[10, N] := PerfB_ObrigS;
      Grade2.Cells[11, N] := PerfC_ObrigE;
      Grade2.Cells[12, N] := PerfC_ObrigS;
    end else
    if (Bloco + Registro <> '') then
      Geral.MB_Aviso('Bloco ou Registro orf�o!' + sLineBreak +
      'Linha: ' + Geral.FF0(I) + sLineBreak +
      'Bloco: ' + Bloco + sLineBreak +
      'Registro: ' + Registro)
    else
    begin
      Grade2.Cells[02, N] := Grade2.Cells[02, N] + Descri;
      Grade2.Cells[04, N] := Grade2.Cells[04, N] + Nivel;
      Grade2.Cells[05, N] := Grade2.Cells[05, N] + OcorrT;
      Grade2.Cells[06, N] := Grade2.Cells[06, N] + ObrigT;
      Grade2.Cells[07, N] := Grade2.Cells[07, N] + PerfA_ObrigE;
      Grade2.Cells[08, N] := Grade2.Cells[08, N] + PerfA_ObrigS;
      Grade2.Cells[09, N] := Grade2.Cells[09, N] + PerfB_ObrigE;
      Grade2.Cells[10, N] := Grade2.Cells[10, N] + PerfB_ObrigS;
      Grade2.Cells[11, N] := Grade2.Cells[11, N] + PerfC_ObrigE;
      Grade2.Cells[12, N] := Grade2.Cells[12, N] + PerfC_ObrigS;
    end;
  end;
end;

procedure TFmEFD_RegObrig.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_RegObrig.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmEFD_RegObrig.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
