unit ImportaSintegraNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, DB, UnMLAGeral,
  UnInternalConsts, dmkImage, dmkEdit, ComCtrls, mySQLDbTables, DmkDAC_PF,
  dmkEditDateTimePicker, DBCtrls, dmkDBLookupComboBox, dmkEditCB, UnDmkEnums;

type
  TFmImportaSintegraNew = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Panel13: TPanel;
    PageControl5: TPageControl;
    TabSheet12: TTabSheet;
    GridS: TStringGrid;
    MeImportado: TMemo;
    TabSheet13: TTabSheet;
    MeIgnor: TMemo;
    TabSheet14: TTabSheet;
    MeErros: TMemo;
    TabSheet15: TTabSheet;
    MeNaoImport: TMemo;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EdEmp_CNPJ: TdmkEdit;
    EdEmp_Codigo: TdmkEdit;
    EdEmp_Nome: TdmkEdit;
    GroupBox3: TGroupBox;
    EdSINTEGRA_Path: TdmkEdit;
    SpeedButton7: TSpeedButton;
    SpeedButton1: TSpeedButton;
    QrLocPrdNom: TmySQLQuery;
    QrLocPrdNomGraGruX: TIntegerField;
    QrLocPrdNomGraGru1: TIntegerField;
    StatusBar: TStatusBar;
    BtImporta: TBitBtn;
    BtCarrega: TBitBtn;
    LaMes: TLabel;
    Label2: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    Label3: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdMes: TdmkEdit;
    PB1: TProgressBar;
    RGEmitente: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure MeImportadoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MeImportadoMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MeImportadoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure GridSDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure EdMesChange(Sender: TObject);
    procedure MeImportadoChange(Sender: TObject);
    procedure EdEmp_CNPJChange(Sender: TObject);
  private
    { Private declarations }
    F_Casas: Byte;
    F_Tam, F_PosI, F_PosF,
    FMeImportado_SelCol, FMeImportado_SelLin: Integer;
    F_Fmt: Char;
    F_Obrigatorio: Boolean;
    function  NaoPermiteIncluir(): Boolean;
    procedure InfoPosMeImportado();
  public
    { Public declarations }
    FEntidade, FAnoMes: Integer;
  end;

  var
  FmImportaSintegraNew: TFmImportaSintegraNew;

implementation

uses UnMyObjects, Module, ModuleGeral, UnGrade_Create, UMySQLModule, MyDBCheck,
  UnSintegra, Sintegra_PesqPrd;

{$R *.DFM}

  const
  FImporExpor = 1; // 1 = Importa - N�o mexer!!!

function TFmImportaSintegraNew.NaoPermiteIncluir(): Boolean;
var
  Ano, Mes, Dia: Word;
  Empresa: Integer;
begin
  Result := True;
  //
  Empresa := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a filial') then
    Exit;
  FEntidade := DModG.QrEmpresasCodigo.Value;
  DecodeDate(TPDataIni.Date, Ano, Mes, Dia);
  FAnoMes := Ano * 100 + Mes;
  if MyObjects.FIC(FAnoMes < 200000, EdMes, 'Informe o m�s') then
    Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT * FROM sintegr_10 ',
  'WHERE ImporExpor=' + FormatFloat('0', FImporExpor),
  'AND AnoMes=' + FormatFloat('0', FAnoMes),
  'AND Empresa=' + FormatFloat('0', FEntidade)]);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    if Geral.MensagemBox('J� existem dados para o per�odo selecionado.' +
    #13#10 + 'Deseja sobrescrev�-los?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      USintegra.ExcluiPeriodoSintegra(FImporExpor, FAnoMes, FEntidade);
      //
      Dmod.QrAux.Close;
      Dmod.QrAux.Open;
      Result := Dmod.QrAux.RecordCount > 0;
    end;
  end else Result := False;
end;

procedure TFmImportaSintegraNew.BtCarregaClick(Sender: TObject);
var
  I, Tipo, Terceiro, Codigo, EUF, PUF, GraGruX, Nivel1, CIF_FOB: Integer;
  Emitente, CNPJ, IE, Data, Modelo, Serie, NumNF, CFOP, CST, Emissao, UF, NomeForn,
  RazaoSocial, Fantasia: String;
  ValorNF, BCICMS, ValICMS, ValIsento, ValNCICMS, AliqICMS: Double;
  NCONTRIB, MUNICIPIO, FAX, CODCONV, CODNAT, CODFIN, LOGRADOURO, NUMERO,
  COMPLEMENT, BAIRRO, CEP, CONTATO, TELEFONE, NUMITEM, CODPROD, NomeProd: String;
  QTDADE, VLRPROD, VLRDESC, BASECALC, BASESUBTRI, VLRIPI, ALIQIPI, REDBASEICM: Double;
  DATAINI, DATAFIN, CODNCM, DESCRICAO, UNIDADE, INSCEST, SUBSERIE, SITUACAO: String;
  VALORTOT, BASEICMS, VALORICMS, VLRISENTO, OUTROS: Double;
  Transporta, LinArq: Integer;
  NomeTrsp, TotLin: String;
  Ano, Mes, Dia: Word;
  Dta: TDateTime;
  EmitenteT, EmitenteP: Boolean;
begin
  //
  if NaoPermiteIncluir() then
    Exit;

  if MyObjects.FIC(RGEmitente.ItemIndex < 1, RGEmitente,
    'Informe o "Tipo de Emitente"!') then
    Exit;
  Screen.Cursor := crHourGlass;
  try
    EmitenteT := RGEmitente.ItemIndex in ([1,3]);
    EmitenteP := RGEmitente.ItemIndex in ([2,3]);
    BtImporta.Enabled := False;
    MeIgnor.Lines.Clear;
    MeErros.Lines.Clear;
    //
    TotLin := ': Linha ' + FormatFloat('0', MeImportado.Lines.Count) + ' < ';
    PB1.Max := MeImportado.Lines.Count;
    for I := 1 to MeImportado.Lines.Count - 1 do
    begin
      LinArq := I;
      //USintegra.InfoPosMeImportado2(I);
      USintegra.InfoPosMeImportado2(MeImportado, GridS, I,
        F_Tam, F_PosI, F_PosF, F_Fmt, F_Casas, F_Obrigatorio);
      //
      Tipo := Geral.IMV(GridS.Cells[01,01]);
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Carregando Registro ' +
      FormatFloat('0', Tipo) + TotLin + FormatFloat('0', LinArq));
      case Tipo of
        10:
        begin
          EdEmp_CNPJ.ValueVariant := GridS.Cells[01,02];
          if EdEmp_Codigo.ValueVariant <> FEntidade then
          begin
            Geral.MensagemBox(
            'Filial do arquivo n�o confere com a filial selecionada!',
            'Aviso', MB_OK+MB_ICONWARNING);
            Exit;
          end else
          CNPJ      := GridS.Cells[01,02];
          INSCEST   := GridS.Cells[01,03];
          NCONTRIB  := GridS.Cells[01,04];
          MUNICIPIO := GridS.Cells[01,05];
          UF        := GridS.Cells[01,06];
          FAX       := GridS.Cells[01,07];
          DATAINI   := GridS.Cells[01,08];
          DATAFIN   := GridS.Cells[01,09];
          CODCONV   := GridS.Cells[01,10];
          CODNAT    := GridS.Cells[01,11];
          CODFIN    := GridS.Cells[01,12];
          // DATAINI
          Ano  := Geral.IMV(Copy(DATAINI, 1, 4));
          Mes  := Geral.IMV(Copy(DATAINI, 5, 2));
          Dia  := Geral.IMV(Copy(DATAINI, 7, 2));
          if (Ano=0) then
            Dta := 0
          else
            Dta := EncodeDate(Ano, Mes, dia);
          DATAINI := Geral.FDT(Dta, 1);
          if (Ano * 100) + Mes <> FAnoMes then
          begin
            Geral.MensagemBox(
            'Data inicial do arquivo n�o confere com o per�odo selecionado!',
            'Aviso', MB_OK+MB_ICONWARNING);
            Exit;
          end;
          // DATAFIN
          Ano  := Geral.IMV(Copy(DATAFIN, 1, 4));
          Mes  := Geral.IMV(Copy(DATAFIN, 5, 2));
          Dia  := Geral.IMV(Copy(DATAFIN, 7, 2));
          if (Ano=0) then
            Dta := 0
          else
            Dta := EncodeDate(Ano, Mes, dia);
          DATAFIN := Geral.FDT(Dta, 1);
          if (Ano * 100) + Mes <> FAnoMes then
          begin
            Geral.MensagemBox(
            'Data final do arquivo n�o confere com o per�odo selecionado!',
            'Aviso', MB_OK+MB_ICONWARNING);
            Exit;
          end;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'sintegr_10', False, [
          'TIPO', 'CNPJ', 'INSCEST',
          'NCONTRIB', 'MUNICIPIO', 'UF',
          'FAX', 'DATAINI', 'DATAFIN',
          'CODCONV', 'CODNAT', 'CODFIN'], [
          'ImporExpor', 'AnoMes', 'Empresa'], [
          TIPO, CNPJ, INSCEST,
          NCONTRIB, MUNICIPIO, UF,
          FAX, DATAINI, DATAFIN,
          CODCONV, CODNAT, CODFIN], [
          FImporExpor, FAnoMes, FEntidade], True);
        end;
        11:
        begin
          LOGRADOURO := GridS.Cells[01,02];
          NUMERO     := GridS.Cells[01,03];
          COMPLEMENT := GridS.Cells[01,04];
          BAIRRO     := GridS.Cells[01,05];
          CEP        := GridS.Cells[01,06];
          CONTATO    := GridS.Cells[01,07];
          TELEFONE   := GridS.Cells[01,08];
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'sintegr_11', False, [
          'TIPO', 'LOGRADOURO', 'NUMERO',
          'COMPLEMENT', 'BAIRRO', 'CEP',
          'CONTATO', 'TELEFONE'], [
          'ImporExpor', 'AnoMes', 'Empresa'], [
          TIPO, LOGRADOURO, NUMERO,
          COMPLEMENT, BAIRRO, CEP,
          CONTATO, TELEFONE], [
          FImporExpor, FAnoMes, FEntidade], True);
        end;
        50:
        begin
          Emitente := Uppercase(GridS.Cells[01,10]);
          if ((Emitente = 'T') and EmitenteT)
          or ((Emitente = 'P') and EmitenteP)
          then
          begin
            CNPJ   := GridS.Cells[01,02];
            IE     := GridS.Cells[01,03];
            Data   := GridS.Cells[02,04];
            UF     := GridS.Cells[01,05];
            Modelo := GridS.Cells[01,06];
            Serie  := GridS.Cells[01,07];
            NumNF  := GridS.Cells[01,08];
            CFOP   := GridS.Cells[01,09];
            //
            ValorNF   := Geral.DMV(GridS.Cells[02,11]);
            BCICMS    := Geral.DMV(GridS.Cells[02,12]);
            ValICMS   := Geral.DMV(GridS.Cells[02,13]);
            ValIsento := Geral.DMV(GridS.Cells[02,14]);
            ValNCICMS := Geral.DMV(GridS.Cells[02,15]);
            AliqICMS  := Geral.DMV(GridS.Cells[02,16]);
            Situacao  := GridS.Cells[01,17];
            //
            //dmkEdit1.Text := CNPJ;
            //
            Emissao := Geral.FDT(Geral.ValidaDataBR(Data, False, False), 1);
            //
            if DModG.DefineEntidade(CNPJ, False, Terceiro, NomeForn) then
            begin
              UMyMod.SQLInsUpd(DMod.QrUpd, stIns, 'sintegr_50', False, [
              'TIPO', 'CNPJ', 'IE',
              'EMISSAO', 'UF', 'MODELO',
              'SERIE', 'NUMNF', 'CFOP',
              'EMITENTE', 'ValorNF', 'BCICMS',
              'ValICMS', 'ValIsento', 'ValNCICMS',
              'AliqICMS', 'Terceiro', 'Situacao'], [
              'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'
              ], [
              TIPO, CNPJ, IE,
              EMISSAO, UF, MODELO,
              SERIE, NUMNF, CFOP,
              EMITENTE, ValorNF, BCICMS,
              ValICMS, ValIsento, ValNCICMS,
              AliqICMS, Terceiro, Situacao], [
              FImporExpor, FAnoMes, FEntidade, LinArq
              ], True);
            end else
            begin
              Pagecontrol5.ActivePageIndex := 2;
              Pagecontrol5.Update;
              Application.ProcessMessages;
              //
              if NomeForn = '' then
                MeErros.Lines.Add('Linha ' + FormatFloat('0000', I) +
                ' CNPJ/CPF cad. incompleto "' +
                CNPJ + '" NF: ' + NumNF + ' de ' + Data)
              else
                MeErros.Lines.Add('Linha ' + FormatFloat('0000', I) +
                ' CNPJ/CPF n�o cadastrado: "' +
                CNPJ + '" NF: ' + NumNF + ' de ' + Data);
              //
              Fantasia := '   CNPJ: ' + CNPJ + ' NF: ' + NumNF + ' de ' + Data;
              EUF := Geral.GetCodigoUF_da_SiglaUF(UF);
              PUF := EUF;
              // tirar, s� da problema!
              if Length(Geral.SoNumero_TT(CNPJ)) > 11 then
              begin
                Codigo := UMyMod.BuscaEmLivreY_Def('entidades', 'Codigo', stIns, 0);
                UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
                'CodUsu', 'RazaoSocial', 'CNPJ', 'IE', 'Tipo', 'EUF', 'PUF'], [
                'Codigo'], [
                Codigo, Fantasia, CNPJ, IE, 0, EUF, PUF], [
                Codigo], True);
              end else begin
                Codigo := UMyMod.BuscaEmLivreY_Def('entidades', 'Codigo', stIns, 0);
                UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
                'CodUsu', 'Nome', 'CPF', 'Tipo', 'EUF', 'PUF'], [
                'Codigo'], [
                Codigo, Fantasia, Copy(CNPJ, 4), 1, EUF, PUF], [
                Codigo], True);
              end;
            end;
          end else begin
            MeIgnor.Lines.Add('Linha ' + FormatFloat('0000', I) + ' registro tipo 50 Emitente = "' + Emitente + '" n�o importado!');
          end;
        end;
        54:
        begin
          CNPJ       := GridS.Cells[01,02];
          Modelo     := GridS.Cells[01,03];
          Serie      := GridS.Cells[01,04];
          NumNF      := GridS.Cells[01,05];
          CFOP       := GridS.Cells[01,06];
          CST        := GridS.Cells[01,07];
          NUMITEM    := GridS.Cells[01,08];
          CODPROD    := GridS.Cells[01,09];
          QTDADE     := Geral.DMV(GridS.Cells[02,10]);
          VLRPROD    := Geral.DMV(GridS.Cells[02,11]);
          VLRDESC    := Geral.DMV(GridS.Cells[02,12]);
          BASECALC   := Geral.DMV(GridS.Cells[02,13]);
          BASESUBTRI := Geral.DMV(GridS.Cells[02,14]);
          VLRIPI     := Geral.DMV(GridS.Cells[02,15]);
          ALIQICMS   := Geral.DMV(GridS.Cells[02,16]);
          //  Cuidado! apenas temp
          //GraGruX    := 0;//Geral.IMV(CodProd);
{
          if DefineReduzido(TRIM(CODPROD), False, GraGruX, NomeProd) then
          begin
}
            UMyMod.SQLInsUpd(DMod.QrUpd, stIns, 'sintegr_54', False, [
            'TIPO', 'CNPJ', 'MODELO',
            'SERIE', 'NUMNF', 'CFOP',
            'CST', 'NUMITEM', 'CODPROD',
            'QTDADE', 'VLRPROD', 'VLRDESC',
            'BASECALC', 'BASESUBTRI', 'VLRIPI',
            'ALIQICMS'], [
            'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'
            ], [
            TIPO, CNPJ, MODELO,
            SERIE, NUMNF, CFOP,
            CST, NUMITEM, CODPROD,
            QTDADE, VLRPROD, VLRDESC,
            BASECALC, BASESUBTRI, VLRIPI,
            ALIQICMS], [
            FImporExpor, FAnoMes, FEntidade, LinArq
            ], True);
{
          end else
          begin
            Pagecontrol5.ActivePageIndex := 2;
            Pagecontrol5.Update;
            Application.ProcessMessages;
            //
            MeErros.Lines.Add('Linha ' + FormatFloat('0000', I) +
            ' Produto n�o cadastrado: "' +
            CODPROD + '" NF: ' + NumNF + ' de ' + Data);
            //
          end;
}
        end;
        70:
        begin
          CNPJ      := GridS.Cells[01,02];
          INSCEST   := GridS.Cells[01,03];
          EMISSAO   := Geral.FDT(Geral.ValidaDataBR(GridS.Cells[02,04], False, False), 1);
          UF        := GridS.Cells[01,05];
          MODELO    := GridS.Cells[01,06];
          SERIE     := GridS.Cells[01,07];
          SUBSERIE  := GridS.Cells[01,08];
          NUMNF     := GridS.Cells[01,09];
          CFOP      := GridS.Cells[01,10];
          VALORTOT  := Geral.DMV(GridS.Cells[02,11]);
          BASEICMS  := Geral.DMV(GridS.Cells[02,12]);
          VALORICMS := Geral.DMV(GridS.Cells[02,13]);
          VLRISENTO := Geral.DMV(GridS.Cells[02,14]);
          OUTROS    := Geral.DMV(GridS.Cells[02,15]);
          CIF_FOB   := Geral.IMV(GridS.Cells[01,16]);
          SITUACAO  := GridS.Cells[01,17];
          //
          if DModG.DefineEntidade(CNPJ, False, Transporta, NomeTrsp) then
          begin
            UMyMod.SQLInsUpd(DMod.QrUpd, stIns, 'sintegr_70', False, [
            'TIPO', 'CNPJ', 'INSCEST',
            'EMISSAO', 'UF', 'MODELO',
            'SERIE', 'SUBSERIE', 'NUMNF',
            'CFOP', 'VALORTOT', 'BASEICMS',
            'VALORICMS', 'VLRISENTO', 'OUTROS',
            'CIF_FOB', 'SITUACAO', 'Transporta'], [
            'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'
            ], [
            TIPO, CNPJ, INSCEST,
            EMISSAO, UF, MODELO,
            SERIE, SUBSERIE, NUMNF,
            CFOP, VALORTOT, BASEICMS,
            VALORICMS, VLRISENTO, OUTROS,
            CIF_FOB, SITUACAO, Transporta], [
            FImporExpor, FAnoMes, FEntidade, LinArq
            ], True);
          end else
          begin
            Pagecontrol5.ActivePageIndex := 2;
            Pagecontrol5.Update;
            Application.ProcessMessages;
            //
            if NomeForn = '' then
              MeErros.Lines.Add('Linha ' + FormatFloat('0000', I) +
              ' CNPJ/CPF cad. incompleto "' +
              CNPJ + '" NF: ' + NumNF + ' de ' + Data)
            else
              MeErros.Lines.Add('Linha ' + FormatFloat('0000', I) +
              ' CNPJ/CPF n�o cadastrado: "' +
              CNPJ + '" NF: ' + NumNF + ' de ' + Data);
            //
            Fantasia := '   CNPJ: ' + CNPJ + ' NF: ' + NumNF + ' de ' + Data;
            EUF := Geral.GetCodigoUF_da_SiglaUF(UF);
            PUF := EUF;
            // tirar s� da problema!
            if Length(Geral.SoNumero_TT(CNPJ)) > 11 then
            begin
              Codigo := UMyMod.BuscaEmLivreY_Def('entidades', 'Codigo', stIns, 0);
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
              'CodUsu', 'RazaoSocial', 'CNPJ', 'IE', 'Tipo', 'EUF', 'PUF'], [
              'Codigo'], [
              Codigo, Fantasia, CNPJ, IE, 0, EUF, PUF], [
              Codigo], True);
            end else begin
              Codigo := UMyMod.BuscaEmLivreY_Def('entidades', 'Codigo', stIns, 0);
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
              'CodUsu', 'Nome', 'CPF', 'Tipo', 'EUF', 'PUF'], [
              'Codigo'], [
              Codigo, Fantasia, Copy(CNPJ, 4), 1, EUF, PUF], [
              Codigo], True);
            end;
          end;
        end;
        75:
        begin
          DATAINI    := Geral.FDT(Geral.ValidaDataBR(GridS.Cells[02,02], False, False), 1);
          DATAFIN    := Geral.FDT(Geral.ValidaDataBR(GridS.Cells[02,03], False, False), 1);
          CODPROD    := GridS.Cells[01,04];
          CODNCM     := GridS.Cells[01,05];
          DESCRICAO  := Trim(GridS.Cells[01,06]);
          UNIDADE    := GridS.Cells[01,07];
          AliqIPI    := Geral.DMV(GridS.Cells[02,08]);
          AliqICMS   := Geral.DMV(GridS.Cells[02,09]);
          REDBASEICM := Geral.DMV(GridS.Cells[02,10]);
          BASESUBTRI := Geral.DMV(GridS.Cells[02,11]);
          //
          QrLocPrdNom.Close;
          QrLocPrdNom.Params[0].AsString := DESCRICAO;
          QrLocPrdNom.Open;
          GraGruX := QrLocPrdNomGraGruX.Value;
          Nivel1  := QrLocPrdNomGraGru1.Value;
          //
          {
          if (GraGruX = 0) and (DESCRICAO <> '') then
          begin
            QrLocPrdTmp.Close;
            QrLocPrdTmp.Params[0].AsString := DESCRICAO;
            QrLocPrdTmp.Open;
            GraGruX := QrLocPrdTmpGraGruX.Value;
          end;
          }
          //
          if GraGruX = 0 then
          begin
            Nivel1 := 0;
            {
            if (Geral.IMV(CODPROD) < 85) then
            begin
              IncluiProduto(CODPROD, DESCRICAO, UNIDADE, CODNCM, AliqIPI);
            end;
            }
            if DBCheck.CriaFm(TFmSintegra_PesqPrd, FmSintegra_PesqPrd, afmoLiberado) then
            begin
              FmSintegra_PesqPrd.EdNaoLoc_TXT.Text := DESCRICAO;
              FmSintegra_PesqPrd.EdNaoLoc_COD.Text := CODPROD;
              //
              FmSintegra_PesqPrd.EdPesq.Text   := '%' + DESCRICAO + '%';
              FmSintegra_PesqPrd.ShowModal;
              GraGruX := FmSintegra_PesqPrd.FGraGruX;
              Nivel1  := FmSintegra_PesqPrd.FNivel1;
              FmSintegra_PesqPrd.Destroy;
            end;
            if GraGruX <> 0 then
            begin
              MeErros.Lines.Add('Linha ' + FormatFloat('0000', I) +
              ' Produto n�o localizado: "' +
              CODPROD + '" Descri��o: "' + DESCRICAO + '"');
              // ShowMessage(DESCRICAO);
              if Nivel1 <> 0 then
                UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
                'NomeTemp'], ['Nivel1'], [DESCRICAO], [
                Nivel1], True);
            end;
          end;
          //

          UMyMod.SQLInsUpd(DMod.QrUpd, stIns, 'sintegr_75', False, [
          'TIPO', 'DATAINI', 'DATAFIN',
          'CODPROD', 'GraGruX', 'Nivel1',
          'CODNCM', 'DESCRICAO', 'UNIDADE',
          'ALIQIPI', 'ALIQICMS', 'REDBASEICM',
          'BASESUBTRI'], [
          'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'
          ], [
          TIPO, DATAINI, DATAFIN,
          CODPROD, GraGruX, Nivel1,
          CODNCM, DESCRICAO, UNIDADE,
          ALIQIPI, ALIQICMS,
          REDBASEICM, BASESUBTRI], [
          FImporExpor, FAnoMes, FEntidade, LinArq
          ], True);
        end;
        else
        begin
          MeIgnor.Lines.Add('Linha ' + FormatFloat('0000', I) + ' registro tipo ' + IntToStr(Tipo) + ' n�o importado!');
          PageControl5.ActivePageIndex := 1; 
        end;
      end;
    end;
    PB1.Position := 0;
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, False, '...');
    BtImporta.Enabled :=
      (*(MeErros.Lines.Count = 0) and*) (EdEmp_Codigo.ValueVariant <> 0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImportaSintegraNew.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmImportaSintegraNew.EdEmp_CNPJChange(Sender: TObject);
var
  Empresa: Integer;
  NomeEmp: String;
begin
  if DModG.DefineEntidade(EdEmp_CNPJ.Text, True, Empresa, NomeEmp) then
  begin
    EdEmp_Codigo.ValueVariant := Empresa;
    EdEmp_Nome.Text := NomeEmp;
  end;
end;

procedure TFmImportaSintegraNew.EdMesChange(Sender: TObject);
begin
  if Length(EdMes.Text) = 7 then
  begin
    TPDataIni.Date := Int(Geral.ValidaDataBR(EdMes.ValueVariant, False, False));
    TPDataFim.Date := IncMonth(TPDataIni.Date, 1) - 1;
  end;
end;

procedure TFmImportaSintegraNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmImportaSintegraNew.FormCreate(Sender: TObject);
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  MyObjects.LimpaGrade(GridS, 1, 1, True);
  GridS.Cells[00,00] := 'N�';
  GridS.Cells[01,00] := 'Conte�do do campo';
  GridS.Cells[02,00] := 'Formatado';
  GridS.Cells[03,00] := 'Tam';
  GridS.Cells[04,00] := 'Ini';
  GridS.Cells[05,00] := 'Fim';
  GridS.Cells[06,00] := 'F';
  GridS.Cells[07,00] := 'D';
  GridS.Cells[08,00] := 'Descri��o do Campo';
  //
  GridS.ColWidths[00] := 024;
  GridS.ColWidths[01] := 140;
  GridS.ColWidths[02] := 080;
  GridS.ColWidths[03] := 028;
  GridS.ColWidths[04] := 028;
  GridS.ColWidths[05] := 028;
  GridS.ColWidths[06] := 018;
  GridS.ColWidths[07] := 018;
  GridS.ColWidths[08] := 999;
  //
  PageControl5.ActivePageIndex := 0;
  //
end;

procedure TFmImportaSintegraNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImportaSintegraNew.GridSDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeGeral(GridS, ACol, ARow, Rect, State,
  [taCenter, taLeftJustify, taRightJustify, taCenter, taCenter, taCenter,
  taCenter, taCenter, taLeftJustify], False);
end;

procedure TFmImportaSintegraNew.InfoPosMeImportado();
begin
  USintegra.InfoPosMeImportado(MeImportado, GridS, StatusBar,
    FMeImportado_SelLin, FMeImportado_SelCol, F_Tam, F_PosI, F_PosF, F_Fmt,
    F_Casas, F_Obrigatorio);
end;

procedure TFmImportaSintegraNew.MeImportadoChange(Sender: TObject);
begin
  BtCarrega.Enabled := MeImportado.Lines.Count > 1;
end;

procedure TFmImportaSintegraNew.MeImportadoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  InfoPosMeImportado();
end;

procedure TFmImportaSintegraNew.MeImportadoMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  InfoPosMeImportado();
end;

procedure TFmImportaSintegraNew.MeImportadoMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  InfoPosMeImportado();
end;

procedure TFmImportaSintegraNew.SpeedButton1Click(Sender: TObject);
begin
  MeImportado.Text := MLAGeral.LoadFileToText(EdSINTEGRA_Path.Text);
end;

procedure TFmImportaSintegraNew.SpeedButton7Click(Sender: TObject);
begin
  if MyObjects.DefineArquivo1(Self, EdSINTEGRA_Path) then
  begin
    MeImportado.Text := MLAGeral.LoadFileToText(EdSINTEGRA_Path.Text);
  end;
end;

end.
