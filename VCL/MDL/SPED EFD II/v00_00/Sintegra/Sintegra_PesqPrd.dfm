object FmSintegra_PesqPrd: TFmSintegra_PesqPrd
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ???? ???? ????'
  ClientHeight = 502
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 454
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      Enabled = False
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = '???? ???? ????'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 406
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 92
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 44
        Width = 163
        Height = 13
        Caption = 'Informe parte do nome do produto:'
      end
      object Label2: TLabel
        Left = 8
        Top = 4
        Width = 111
        Height = 13
        Caption = 'Produto n'#227'o localizado:'
      end
      object EdPesq: TdmkEdit
        Left = 8
        Top = 60
        Width = 757
        Height = 25
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '%%'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '%%'
        OnChange = EdPesqChange
      end
      object EdNaoLoc_TXT: TEdit
        Left = 192
        Top = 20
        Width = 573
        Height = 21
        TabStop = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
      object EdNaoLoc_COD: TEdit
        Left = 8
        Top = 20
        Width = 185
        Height = 21
        TabStop = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 93
      Width = 782
      Height = 312
      Align = alClient
      DataSource = DsLocPrdNom
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'GraGru1'
          Title.Caption = 'Nivel 1'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CU_CodUsu'
          Title.Caption = 'C'#243'd. Usu'#225'rio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Nome / Tamanho / Cor'
          Width = 481
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrdGrupTip'
          Visible = True
        end>
    end
  end
  object QrLocPrdNom: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLocPrdNomAfterOpen
    BeforeClose = QrLocPrdNomBeforeClose
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_CodUsu, ggx.GraGru1, ggx.Controle GraGruX, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.PrdGrupTip'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE gg1.Nome LIKE :P0')
    Left = 48
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPrdNomGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocPrdNomCU_CodUsu: TIntegerField
      FieldName = 'CU_CodUsu'
      Required = True
    end
    object QrLocPrdNomGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrLocPrdNomNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrLocPrdNomPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
  end
  object DsLocPrdNom: TDataSource
    DataSet = QrLocPrdNom
    Left = 76
    Top = 152
  end
end
