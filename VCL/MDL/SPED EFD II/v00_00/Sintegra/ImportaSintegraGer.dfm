object FmImportaSintegraGer: TFmImportaSintegraGer
  Left = 368
  Top = 194
  Caption = 'SIN-TEGRA-001 :: Importa'#231#227'o SINTEGRA'
  ClientHeight = 692
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 113
    Width = 1008
    Height = 579
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 57
      Width = 1008
      Height = 522
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet2: TTabSheet
        Caption = ' Per'#237'odos '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 494
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Panel10: TPanel
            Left = 1
            Top = 1
            Width = 102
            Height = 428
            Align = alLeft
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGCab: TdmkDBGrid
              Left = 0
              Top = 0
              Width = 102
              Height = 329
              Align = alClient
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'MES_ANO'
                  Title.Caption = 'MES / ANO'
                  Width = 63
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsSINTEGR_10
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'MES_ANO'
                  Title.Caption = 'MES / ANO'
                  Width = 63
                  Visible = True
                end>
            end
            object GroupBox1: TGroupBox
              Left = 0
              Top = 329
              Width = 102
              Height = 99
              Align = alBottom
              Caption = ' Impress'#227'o: '
              TabOrder = 1
              Visible = False
              object CGAgrup0: TdmkCheckGroup
                Left = 2
                Top = 15
                Width = 98
                Height = 82
                Align = alClient
                Caption = ' Agrupamentos: '
                Items.Strings = (
                  'Produtos'
                  'Nivel 2'
                  'Tipos de prod.')
                TabOrder = 0
                UpdType = utYes
                Value = 0
                OldValor = 0
              end
            end
          end
          object TGroupBox
            Left = 1
            Top = 429
            Width = 998
            Height = 64
            Align = alBottom
            TabOrder = 1
            object Panel3: TPanel
              Left = 2
              Top = 15
              Width = 994
              Height = 47
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object BtExclui: TBitBtn
                Tag = 12
                Left = 196
                Top = 2
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Exclui'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                Visible = False
                OnClick = BtExcluiClick
              end
              object BtNF: TBitBtn
                Left = 104
                Top = 2
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&N.F.'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtNFClick
              end
              object BtPeriodo: TBitBtn
                Tag = 191
                Left = 12
                Top = 2
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Per'#237'odo'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtPeriodoClick
              end
              object Panel2: TPanel
                Left = 885
                Top = 0
                Width = 109
                Height = 47
                Align = alRight
                Alignment = taRightJustify
                BevelOuter = bvNone
                TabOrder = 3
                object BtSaida0: TBitBtn
                  Tag = 13
                  Left = 4
                  Top = 2
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Sa'#237'da'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtSaida0Click
                end
              end
              object BtSeqAcoes: TBitBtn
                Tag = 3
                Left = 380
                Top = 2
                Width = 160
                Height = 40
                Caption = 'Sequ'#234'ncia de a'#231#245'es'
                Enabled = False
                NumGlyphs = 2
                TabOrder = 4
                OnClick = BtSeqAcoesClick
              end
            end
          end
          object PageControl2: TPageControl
            Left = 103
            Top = 1
            Width = 896
            Height = 428
            ActivePage = TabSheet3
            Align = alClient
            TabOrder = 2
            object TabSheet3: TTabSheet
              Caption = ' Notas Fiscais'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel9: TPanel
                Left = 0
                Top = 0
                Width = 888
                Height = 275
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object PageControl4: TPageControl
                  Left = 585
                  Top = 0
                  Width = 303
                  Height = 275
                  ActivePage = TabSheet4
                  Align = alRight
                  TabOrder = 0
                  object TabSheet4: TTabSheet
                    Caption = ' Atrelado '
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object DBGAtrelado: TDBGrid
                      Left = 0
                      Top = 21
                      Width = 295
                      Height = 40
                      Align = alTop
                      DataSource = DsPQE_1_
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'FatNum'
                          Title.Caption = 'C'#243'digo'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'dtEmi'
                          Title.Caption = 'Emiss'#227'o'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'dtSaiEnt'
                          Title.Caption = 'Sai/Ent'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ValTot'
                          Title.Caption = 'Valor NF'
                          Visible = True
                        end>
                    end
                    object Panel13: TPanel
                      Left = 0
                      Top = 0
                      Width = 295
                      Height = 21
                      Align = alTop
                      Caption = 'NF modelo de inclus'#227'o antigo'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clRed
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                      TabOrder = 1
                    end
                    object Panel14: TPanel
                      Left = 0
                      Top = 61
                      Width = 295
                      Height = 21
                      Align = alTop
                      Caption = 'NF modelo de inclus'#227'o novo'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clBlue
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                      TabOrder = 2
                    end
                    object DBGrid1: TDBGrid
                      Left = 0
                      Top = 82
                      Width = 295
                      Height = 40
                      Align = alTop
                      DataSource = DsNFeCabA
                      TabOrder = 3
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'FatNum'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ide_dEmi'
                          Title.Caption = 'Emiss'#227'o'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ide_dSaiEnt'
                          Title.Caption = 'Sai/Ent'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ICMSTot_vNF'
                          Title.Caption = 'Valor NF'
                          Visible = True
                        end>
                    end
                  end
                  object TabSheet5: TTabSheet
                    Caption = 'TabSheet4'
                    ImageIndex = 1
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                  end
                end
                object GroupBox3: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 585
                  Height = 275
                  Align = alClient
                  Caption = ' Dados importados do arquivo sintegra: '
                  TabOrder = 1
                  object Label3: TLabel
                    Left = 2
                    Top = 15
                    Width = 126
                    Height = 13
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'Notas Fiscais (registros 50)'
                  end
                  object Splitter1: TSplitter
                    Left = 2
                    Top = 185
                    Width = 581
                    Height = 5
                    Cursor = crVSplit
                    Align = alTop
                    ExplicitTop = 105
                    ExplicitWidth = 589
                  end
                  object Label4: TLabel
                    Left = 2
                    Top = 190
                    Width = 178
                    Height = 13
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'Itens da NF selecionada (registros 54)'
                  end
                  object DBG_s50: TdmkDBGrid
                    Left = 2
                    Top = 28
                    Width = 581
                    Height = 157
                    Align = alTop
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'EMISSAO'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'EMITENTE'
                        Title.Caption = 'E'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CNPJ'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'MODELO'
                        Width = 28
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SERIE'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NUMNF'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ValorNF'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'FatID'
                        Width = 34
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ACHOU_TXT'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = [fsBold]
                        Title.Caption = 'Achou'
                        Width = 37
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'BCICMS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ValICMS'
                        Visible = True
                      end>
                    Color = clWindow
                    DataSource = DsSINTEGR_50
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    OnDrawColumnCell = DBG_s50DrawColumnCell
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'EMISSAO'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'EMITENTE'
                        Title.Caption = 'E'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CNPJ'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'MODELO'
                        Width = 28
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SERIE'
                        Width = 24
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NUMNF'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ValorNF'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'FatID'
                        Width = 34
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ACHOU_TXT'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = [fsBold]
                        Title.Caption = 'Achou'
                        Width = 37
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'BCICMS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ValICMS'
                        Visible = True
                      end>
                  end
                  object dmkDBGrid1: TdmkDBGrid
                    Left = 2
                    Top = 203
                    Width = 581
                    Height = 70
                    Align = alClient
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'CFOP'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CST'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NUMITEM'
                        Title.Caption = 'Item'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CODPROD'
                        Title.Caption = 'C'#243'd.Prod.'
                        Width = 52
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'QTDADE'
                        Title.Caption = 'Qtde'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VLRPROD'
                        Title.Caption = 'Val. Prod.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VLRDESC'
                        Title.Caption = 'Val. Desc'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'BASECALC'
                        Title.Caption = 'BC ICMS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'BASESUBTRI'
                        Title.Caption = 'BC ICMSST'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VLRIPI'
                        Title.Caption = 'Val. IPI'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ALIQICMS'
                        Title.Caption = '% ICMS'
                        Width = 41
                        Visible = True
                      end>
                    Color = clWindow
                    DataSource = DsSINTEGR_54
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                    TabOrder = 1
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    OnDrawColumnCell = DBG_s50DrawColumnCell
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'CFOP'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CST'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NUMITEM'
                        Title.Caption = 'Item'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CODPROD'
                        Title.Caption = 'C'#243'd.Prod.'
                        Width = 52
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'QTDADE'
                        Title.Caption = 'Qtde'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VLRPROD'
                        Title.Caption = 'Val. Prod.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VLRDESC'
                        Title.Caption = 'Val. Desc'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'BASECALC'
                        Title.Caption = 'BC ICMS'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'BASESUBTRI'
                        Title.Caption = 'BC ICMSST'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VLRIPI'
                        Title.Caption = 'Val. IPI'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ALIQICMS'
                        Title.Caption = '% ICMS'
                        Width = 41
                        Visible = True
                      end>
                  end
                end
              end
              object Panel16: TPanel
                Left = 0
                Top = 275
                Width = 888
                Height = 125
                Align = alBottom
                TabOrder = 1
                object Panel15: TPanel
                  Left = 1
                  Top = 1
                  Width = 21
                  Height = 123
                  Align = alLeft
                  Caption = 'NF modelo de inclus'#227'o novo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 0
                end
                object DBGrid4: TDBGrid
                  Left = 22
                  Top = 1
                  Width = 865
                  Height = 123
                  Align = alClient
                  DataSource = DsNFEItsI
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'nItem'
                      Title.Caption = 'Item'
                      Width = 25
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Reduzido'
                      Width = 50
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_GG1'
                      Title.Caption = 'Nome do produto'
                      Width = 298
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ICMSRec_vBC'
                      Title.Caption = 'Valor BC'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ICMSRec_vICMS'
                      Title.Caption = 'Valor ICMS'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'prod_CFOP'
                      Title.Caption = 'CFOP'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'prod_NCM'
                      Title.Caption = 'NCM'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'prod_qCom'
                      Title.Caption = 'q. Com.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'prod_uCom'
                      Title.Caption = 'u. Com.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'prod_vProd'
                      Title.Caption = 'Val. Prod.'
                      Visible = True
                    end>
                end
              end
            end
            object TabSheet7: TTabSheet
              Caption = 'Conhecimentos de frete'
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel17: TPanel
                Left = 0
                Top = 0
                Width = 888
                Height = 400
                Align = alClient
                ParentBackground = False
                TabOrder = 0
                object dmkDBGrid2: TdmkDBGrid
                  Left = 1
                  Top = 1
                  Width = 886
                  Height = 398
                  Align = alClient
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'EMISSAO'
                      Title.Caption = 'Emiss'#227'o'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CNPJ'
                      Width = 112
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MODELO'
                      Title.Caption = 'Modelo'
                      Width = 28
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SERIE'
                      Title.Caption = 'S'#233'rie'
                      Width = 24
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SUBSERIE'
                      Title.Caption = 'Subs'#233'rie'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NUMNF'
                      Title.Caption = 'N'#250'mero NF'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CFOP'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VALORTOT'
                      Title.Caption = 'Valor total'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'BASEICMS'
                      Title.Caption = 'BC ICMS'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VALORICMS'
                      Title.Caption = 'Valor ICMS'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VLRISENTO'
                      Title.Caption = 'Valor isento'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'OUTROS'
                      Title.Caption = 'Valor outros'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CIF_FOB'
                      Title.Caption = 'MF'
                      Width = 20
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SITUACAO'
                      Title.Caption = 'Sit'
                      Width = 20
                      Visible = True
                    end>
                  Color = clWindow
                  DataSource = DsSINTEGR_70
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDrawColumnCell = DBG_s50DrawColumnCell
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'EMISSAO'
                      Title.Caption = 'Emiss'#227'o'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CNPJ'
                      Width = 112
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MODELO'
                      Title.Caption = 'Modelo'
                      Width = 28
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SERIE'
                      Title.Caption = 'S'#233'rie'
                      Width = 24
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SUBSERIE'
                      Title.Caption = 'Subs'#233'rie'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NUMNF'
                      Title.Caption = 'N'#250'mero NF'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CFOP'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VALORTOT'
                      Title.Caption = 'Valor total'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'BASEICMS'
                      Title.Caption = 'BC ICMS'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VALORICMS'
                      Title.Caption = 'Valor ICMS'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'VLRISENTO'
                      Title.Caption = 'Valor isento'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'OUTROS'
                      Title.Caption = 'Valor outros'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CIF_FOB'
                      Title.Caption = 'MF'
                      Width = 20
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SITUACAO'
                      Title.Caption = 'Sit'
                      Width = 20
                      Visible = True
                    end>
                end
              end
            end
          end
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 57
      Align = alTop
      Caption = ' Empresa e per'#237'odo selecionado: '
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 40
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 23
          Height = 13
          Caption = 'Filial:'
        end
        object Label2: TLabel
          Left = 932
          Top = 0
          Width = 60
          Height = 13
          Caption = 'M'#202'S / ANO:'
          Enabled = False
          FocusControl = DBEdit1
        end
        object EdEmpresa: TdmkEditCB
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 64
          Top = 16
          Width = 677
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object DBEdit1: TDBEdit
          Left = 932
          Top = 16
          Width = 65
          Height = 21
          DataField = 'MES_ANO'
          DataSource = DsSINTEGR_10
          Enabled = False
          TabOrder = 2
        end
        object RGEmitente: TRadioGroup
          Left = 744
          Top = -1
          Width = 185
          Height = 41
          Caption = ' Tipo de Emitente: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'T - Terceiros'
            'P - Pr'#243'prias')
          TabOrder = 3
          OnClick = RGEmitenteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 245
        Height = 32
        Caption = 'Importa'#231#227'o Sintegra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 245
        Height = 32
        Caption = 'Importa'#231#227'o Sintegra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 245
        Height = 32
        Caption = 'Importa'#231#227'o Sintegra'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 65
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel12: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 48
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 31
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object DsSINTEGR_10: TDataSource
    DataSet = QrSINTEGR_10
    Left = 40
    Top = 12
  end
  object QrSINTEGR_10: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSINTEGR_10AfterOpen
    BeforeClose = QrSINTEGR_10BeforeClose
    AfterScroll = QrSINTEGR_10AfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT,'
      'CONCAT(RIGHT(sec.AnoMes, 2), "/",'
      'LEFT(LPAD(sec.AnoMes, 6, "0"), 4)) MES_ANO, sec.*'
      'FROM Sintegr_10 sec'
      'LEFT JOIN entidades ent ON ent.Codigo=sec.Empresa'
      'WHERE sec.ImporExpor=1'
      'AND sec.Empresa=-11'
      'ORDER BY AnoMes DESC'
      '')
    Left = 12
    Top = 12
    object QrSINTEGR_10NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrSINTEGR_10MES_ANO: TWideStringField
      FieldName = 'MES_ANO'
      Required = True
      Size = 7
    end
    object QrSINTEGR_10ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrSINTEGR_10AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrSINTEGR_10Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSINTEGR_10TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSINTEGR_10CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSINTEGR_10INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrSINTEGR_10NCONTRIB: TWideStringField
      FieldName = 'NCONTRIB'
      Size = 35
    end
    object QrSINTEGR_10MUNICIPIO: TWideStringField
      FieldName = 'MUNICIPIO'
      Size = 30
    end
    object QrSINTEGR_10UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSINTEGR_10FAX: TWideStringField
      FieldName = 'FAX'
      Size = 10
    end
    object QrSINTEGR_10DATAINI: TDateField
      FieldName = 'DATAINI'
    end
    object QrSINTEGR_10DATAFIN: TDateField
      FieldName = 'DATAFIN'
    end
    object QrSINTEGR_10CODCONV: TWideStringField
      FieldName = 'CODCONV'
      Size = 1
    end
    object QrSINTEGR_10CODNAT: TWideStringField
      FieldName = 'CODNAT'
      Size = 1
    end
    object QrSINTEGR_10CODFIN: TWideStringField
      FieldName = 'CODFIN'
      Size = 1
    end
    object QrSINTEGR_10Stat50T: TSmallintField
      FieldName = 'Stat50T'
    end
    object QrSINTEGR_10Stat50P: TSmallintField
      FieldName = 'Stat50P'
    end
    object QrSINTEGR_10Stat70T: TSmallintField
      FieldName = 'Stat70T'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtPeriodo
    CanUpd01 = BtNF
    CanDel01 = BtExclui
    Left = 68
    Top = 12
  end
  object PMPeriodo: TPopupMenu
    OnPopup = PMPeriodoPopup
    Left = 36
    Top = 608
    object Incluinovoperiodo1: TMenuItem
      Caption = 'Inclui novo periodo'
      OnClick = Incluinovoperiodo1Click
    end
    object Excluiperiodoselecionado1: TMenuItem
      Caption = '&Exclui periodo selecionado'
      Enabled = False
      OnClick = Excluiperiodoselecionado1Click
    end
    object Itemns1: TMenuItem
      Caption = '&Item(ns)'
      Enabled = False
      Visible = False
      OnClick = Itemns1Click
    end
  end
  object QrSINTEGR_50: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSINTEGR_50AfterOpen
    BeforeClose = QrSINTEGR_50BeforeClose
    AfterScroll = QrSINTEGR_50AfterScroll
    OnCalcFields = QrSINTEGR_50CalcFields
    SQL.Strings = (
      'SELECT s50.*'
      'FROM sintegr_50 s50'
      'WHERE s50.ImporExpor=:P0'
      'AND s50.Empresa=:P1'
      'AND s50.AnoMes=:P2'
      'AND s50.EMITENTE=:P3'
      'ORDER BY LinArq')
    Left = 96
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSINTEGR_50ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrSINTEGR_50AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrSINTEGR_50Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSINTEGR_50LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrSINTEGR_50TIPO: TSmallintField
      FieldName = 'TIPO'
    end
    object QrSINTEGR_50CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSINTEGR_50IE: TWideStringField
      FieldName = 'IE'
      Size = 14
    end
    object QrSINTEGR_50EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrSINTEGR_50UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSINTEGR_50MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrSINTEGR_50SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrSINTEGR_50NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrSINTEGR_50CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrSINTEGR_50EMITENTE: TWideStringField
      FieldName = 'EMITENTE'
      Size = 1
    end
    object QrSINTEGR_50ValorNF: TFloatField
      FieldName = 'ValorNF'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_50BCICMS: TFloatField
      FieldName = 'BCICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_50ValICMS: TFloatField
      FieldName = 'ValICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_50ValIsento: TFloatField
      FieldName = 'ValIsento'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_50ValNCICMS: TFloatField
      FieldName = 'ValNCICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_50AliqICMS: TFloatField
      FieldName = 'AliqICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_50Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrSINTEGR_50Tabela: TSmallintField
      FieldName = 'Tabela'
    end
    object QrSINTEGR_50FatID: TIntegerField
      FieldName = 'FatID'
      DisplayFormat = '0;-0; '
    end
    object QrSINTEGR_50FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrSINTEGR_50ACHOU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ACHOU_TXT'
      Size = 3
      Calculated = True
    end
    object QrSINTEGR_50ConfVal: TSmallintField
      FieldName = 'ConfVal'
    end
    object QrSINTEGR_50Situacao: TWideStringField
      FieldName = 'Situacao'
      Size = 1
    end
  end
  object DsSINTEGR_50: TDataSource
    DataSet = QrSINTEGR_50
    Left = 124
    Top = 12
  end
  object PMExclui: TPopupMenu
    OnPopup = PMExcluiPopup
    Left = 232
    Top = 604
    object Periodoselecionado1: TMenuItem
      Caption = '&Periodo selecionado'
    end
    object ItemSelecionado2: TMenuItem
      Caption = '&Item(s)'
      OnClick = ItemSelecionado2Click
    end
  end
  object QrSEI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo, IE, EUF, PUF, '
      'IF(trc.Tipo=0, RazaoSocial, trc.Nome) NO_ENT,'
      'IF(trc.Tipo=0, CNPJ, trc.CPF) CNPJ_CPF,'
      'gti.PrintTam, gcc.PrintCor, gg1.NCM, '
      ''
      'gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,'
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, sei.*'
      'FROM spedestqits sei'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=sei.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'LEFT JOIN entidades  trc ON trc.Codigo=sei.Terceiro'
      'WHERE (EstqFimQtd > 0 OR EstqFimVal > 0)'
      ''
      'AND sei.ImporExpor=:P0'
      'AND sei.Empresa=:P1'
      'AND sei.AnoMes=:P2'
      'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, sei.GraGruX')
    Left = 800
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSEINivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrSEINO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 120
    end
    object QrSEIPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrSEIUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrSEINO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrSEISIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrSEINO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrSEINO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrSEIGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrSEIGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrSEIGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrSEIGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrSEIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSEIImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrSEIAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrSEIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSEIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSEISit_Prod: TSmallintField
      FieldName = 'Sit_Prod'
    end
    object QrSEITerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrSEIEstqIniQtd: TFloatField
      FieldName = 'EstqIniQtd'
    end
    object QrSEIEstqIniPrc: TFloatField
      FieldName = 'EstqIniPrc'
    end
    object QrSEIEstqIniVal: TFloatField
      FieldName = 'EstqIniVal'
    end
    object QrSEIComprasQtd: TFloatField
      FieldName = 'ComprasQtd'
    end
    object QrSEIComprasPrc: TFloatField
      FieldName = 'ComprasPrc'
    end
    object QrSEIComprasVal: TFloatField
      FieldName = 'ComprasVal'
    end
    object QrSEIConsumoQtd: TFloatField
      FieldName = 'ConsumoQtd'
    end
    object QrSEIConsumoPrc: TFloatField
      FieldName = 'ConsumoPrc'
    end
    object QrSEIConsumoVal: TFloatField
      FieldName = 'ConsumoVal'
    end
    object QrSEIEstqFimQtd: TFloatField
      FieldName = 'EstqFimQtd'
    end
    object QrSEIEstqFimPrc: TFloatField
      FieldName = 'EstqFimPrc'
    end
    object QrSEIEstqFimVal: TFloatField
      FieldName = 'EstqFimVal'
    end
    object QrSEINO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrSEICNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrSEIIE: TWideStringField
      FieldName = 'IE'
    end
    object QrSEIEUF: TSmallintField
      FieldName = 'EUF'
    end
    object QrSEIPUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrSEITipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrSEIPrintTam: TSmallintField
      FieldName = 'PrintTam'
    end
    object QrSEIPrintCor: TSmallintField
      FieldName = 'PrintCor'
    end
    object QrSEINCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
  end
  object frxDsSPEDEstqIts: TfrxDBDataset
    UserName = 'frxDsSPEDEstqIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nivel1=Nivel1'
      'NO_PRD=NO_PRD'
      'PrdGrupTip=PrdGrupTip'
      'UnidMed=UnidMed'
      'NO_PGT=NO_PGT'
      'SIGLA=SIGLA'
      'NO_TAM=NO_TAM'
      'NO_COR=NO_COR'
      'GraCorCad=GraCorCad'
      'GraGruC=GraGruC'
      'GraGru1=GraGru1'
      'GraTamI=GraTamI'
      'Controle=Controle'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'GraGruX=GraGruX'
      'Sit_Prod=Sit_Prod'
      'Terceiro=Terceiro'
      'EstqIniQtd=EstqIniQtd'
      'EstqIniPrc=EstqIniPrc'
      'EstqIniVal=EstqIniVal'
      'ComprasQtd=ComprasQtd'
      'ComprasPrc=ComprasPrc'
      'ComprasVal=ComprasVal'
      'ConsumoQtd=ConsumoQtd'
      'ConsumoPrc=ConsumoPrc'
      'ConsumoVal=ConsumoVal'
      'EstqFimQtd=EstqFimQtd'
      'EstqFimPrc=EstqFimPrc'
      'EstqFimVal=EstqFimVal'
      'Nivel2=Nivel2'
      'NO_GG2=NO_GG2'
      'Exportado=Exportado')
    DataSet = QrSINTEGR_50
    BCDToCurrency = False
    Left = 828
    Top = 40
  end
  object QrLocPQE: TmySQLQuery
    Database = Dmod.MyDB
    Left = 476
    Top = 12
    object QrLocPQECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocPQEValorNF: TFloatField
      FieldName = 'ValorNF'
    end
  end
  object QrPQE_1_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FatNum, DataE dtEmi, DataS dtSaiEnt,  '
      'Data DataFiscal, ValorNF ValTot, CI Empresa,  '
      'CI CodInfoDest, IQ CodInfoEmit, refNFe Id,  '
      'modNF ide_mod, Serie,  NF nNF, Frete, Seguro, '
      'Desconto, IPI, PIS, COFINS, Outros'
      'FROM PQE '
      'WHERE Codigo=1 ')
    Left = 508
    Top = 12
    object QrPQE_1_FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrPQE_1_dtEmi: TDateField
      FieldName = 'dtEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQE_1_dtSaiEnt: TDateField
      FieldName = 'dtSaiEnt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQE_1_DataFiscal: TDateField
      FieldName = 'DataFiscal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQE_1_ValTot: TFloatField
      FieldName = 'ValTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPQE_1_Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPQE_1_CodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object QrPQE_1_CodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrPQE_1_Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrPQE_1_ide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrPQE_1_Serie: TIntegerField
      FieldName = 'Serie'
    end
    object QrPQE_1_nNF: TIntegerField
      FieldName = 'nNF'
    end
    object QrPQE_1_Frete: TFloatField
      FieldName = 'Frete'
    end
    object QrPQE_1_Seguro: TFloatField
      FieldName = 'Seguro'
    end
    object QrPQE_1_Desconto: TFloatField
      FieldName = 'Desconto'
    end
    object QrPQE_1_IPI: TFloatField
      FieldName = 'IPI'
    end
    object QrPQE_1_PIS: TFloatField
      FieldName = 'PIS'
    end
    object QrPQE_1_COFINS: TFloatField
      FieldName = 'COFINS'
    end
    object QrPQE_1_Outros: TFloatField
      FieldName = 'Outros'
    end
  end
  object DsPQE_1_: TDataSource
    DataSet = QrPQE_1_
    Left = 532
    Top = 12
  end
  object QrLocMPc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT its.Conta'
      'FROM mpinits its'
      'LEFT JOIN MPIn mpi ON mpi.Controle=its.Controle'
      'WHERE its.EmitNFAvul=0'
      'AND mpi.Procedencia>0'
      'AND its.NF>0'
      ''
      '')
    Left = 560
    Top = 12
    object QrLocMPcConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object QrLocEMP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM entimp '
      'WHERE Codigo=:P0')
    Left = 588
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocEMPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object PMSeqAcoes0: TPopupMenu
    OnPopup = PMSeqAcoes0Popup
    Left = 552
    Top = 644
    object N50T_SeqAcoes_1: TMenuItem
      Caption = '&1. Vincular com entradas (modelo de inclus'#227'o antigo)'
      Enabled = False
      OnClick = N50T_SeqAcoes_1Click
    end
    object N50T_SeqAcoes_2: TMenuItem
      Caption = '&2. Excluir notas fiscais criadas afor'#231'a'
      Enabled = False
      OnClick = N50T_SeqAcoes_2Click
    end
    object N50T_SeqAcoes_3: TMenuItem
      Caption = '&3. Criar Notas Fiscais (modelo de inclus'#227'o novo)'
      Enabled = False
      OnClick = N50T_SeqAcoes_3Click
    end
    object N50T_SeqAcoes_4: TMenuItem
      Caption = '4'
      Visible = False
      OnClick = N50T_SeqAcoes_4Click
    end
  end
  object QrExclUni: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Status, Empresa'
      'FROM nfecaba nca '
      'WHERE nca.FatID=:P0'
      'AND nca.FatNum=:P1'
      'AND nca.Empresa=:P2')
    Left = 616
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrExclUniFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrExclUniFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrExclUniStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrExclUniEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrExclMul: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum,  infProt_cStat, Empresa'
      'FROM nfecaba nca '
      'WHERE nca.FatID=:P0'
      'AND nca.Empresa=:P1'
      'AND CriAForca=1'
      'OR Importado=1'
      'AND DataFiscal BETWEEN :P2 AND :P3')
    Left = 644
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrExclMulFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrExclMulFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrExclMulEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrExclMulinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
  end
  object QrSumPQEIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(TotalCusto) TotalCusto'
      'FROM pqeits'
      'WHERE Codigo=:P0')
    Left = 672
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPQEItsValorItem: TFloatField
      FieldName = 'ValorItem'
    end
  end
  object QrPQEIts_2_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pq.Nome NOMEPQ, ei.*'
      'FROM pqeits ei'
      'LEFT JOIN pq ON pq.Codigo=ei.Insumo'
      'WHERE ei.Codigo=:P0'
      'ORDER BY Conta'
      ''
      ''
      '')
    Left = 532
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQEIts_2_CUSTOITEM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOITEM'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
    object QrPQEIts_2_VALORKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALORKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEIts_2_TOTALKGBRUTO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALKGBRUTO'
      Calculated = True
    end
    object QrPQEIts_2_CUSTOKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEIts_2_PRECOKG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECOKG'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrPQEIts_2_Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPQEIts_2_Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPQEIts_2_Conta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrPQEIts_2_Volumes: TIntegerField
      FieldName = 'Volumes'
      Required = True
    end
    object QrPQEIts_2_PesoVB: TFloatField
      FieldName = 'PesoVB'
      Required = True
      DisplayFormat = '#,##0.000'
    end
    object QrPQEIts_2_PesoVL: TFloatField
      FieldName = 'PesoVL'
      Required = True
      DisplayFormat = '#,##0.000'
    end
    object QrPQEIts_2_ValorItem: TFloatField
      FieldName = 'ValorItem'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQEIts_2_IPI: TFloatField
      FieldName = 'IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEIts_2_RIPI: TFloatField
      FieldName = 'RIPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEIts_2_CFin: TFloatField
      FieldName = 'CFin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEIts_2_ICMS: TFloatField
      FieldName = 'ICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEIts_2_RICMS: TFloatField
      FieldName = 'RICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEIts_2_TotalCusto: TFloatField
      FieldName = 'TotalCusto'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQEIts_2_Insumo: TIntegerField
      FieldName = 'Insumo'
      Required = True
    end
    object QrPQEIts_2_TotalPeso: TFloatField
      FieldName = 'TotalPeso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQEIts_2_NOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQEIts_2_prod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrPQEIts_2_prod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrPQEIts_2_prod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrPQEIts_2_prod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrPQEIts_2_prod_EX_TIPI: TWideStringField
      FieldName = 'prod_EX_TIPI'
      Size = 3
    end
    object QrPQEIts_2_prod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrPQEIts_2_prod_CFOP: TWideStringField
      FieldName = 'prod_CFOP'
      Size = 4
    end
    object QrPQEIts_2_prod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 4
    end
    object QrPQEIts_2_prod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrPQEIts_2_prod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrPQEIts_2_prod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrPQEIts_2_prod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrPQEIts_2_prod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 4
    end
    object QrPQEIts_2_prod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrPQEIts_2_prod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrPQEIts_2_prod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrPQEIts_2_prod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrPQEIts_2_prod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrPQEIts_2_ICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrPQEIts_2_ICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrPQEIts_2_ICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrPQEIts_2_ICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrPQEIts_2_ICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_ICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_IPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrPQEIts_2_IPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrPQEIts_2_IPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_IPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_IPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_IPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_IPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_PIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrPQEIts_2_PIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_PIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_PIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_PIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_PIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_PISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_PISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_PISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_PISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_PISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_COFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrPQEIts_2_COFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrPQEIts_2_COFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_COFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_COFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_COFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_COFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_COFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPQEIts_2_COFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_COFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrPQEIts_2_COFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object QrPQE_2_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FatNum, DataE dtEmi, DataS dtSaiEnt,  '
      'Data DataFiscal, ValorNF ValTot, CI Empresa,  '
      'CI CodInfoDest, IQ CodInfoEmit, refNFe Id,  '
      'modNF ide_mod, Serie,  NF nNF, Frete, Seguro, '
      'Desconto, IPI, PIS, COFINS, Outros'
      'FROM PQE '
      'WHERE Codigo=:P0')
    Left = 504
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQE_2_FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrPQE_2_dtEmi: TDateField
      FieldName = 'dtEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQE_2_dtSaiEnt: TDateField
      FieldName = 'dtSaiEnt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQE_2_DataFiscal: TDateField
      FieldName = 'DataFiscal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQE_2_ValTot: TFloatField
      FieldName = 'ValTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPQE_2_Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPQE_2_CodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object QrPQE_2_CodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrPQE_2_Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrPQE_2_ide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrPQE_2_Serie: TIntegerField
      FieldName = 'Serie'
    end
    object QrPQE_2_nNF: TIntegerField
      FieldName = 'nNF'
    end
    object QrPQE_2_Frete: TFloatField
      FieldName = 'Frete'
    end
    object QrPQE_2_Seguro: TFloatField
      FieldName = 'Seguro'
    end
    object QrPQE_2_Desconto: TFloatField
      FieldName = 'Desconto'
    end
    object QrPQE_2_IPI: TFloatField
      FieldName = 'IPI'
    end
    object QrPQE_2_PIS: TFloatField
      FieldName = 'PIS'
    end
    object QrPQE_2_COFINS: TFloatField
      FieldName = 'COFINS'
    end
    object QrPQE_2_Outros: TFloatField
      FieldName = 'Outros'
    end
  end
  object QrNFeCabA: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFeCabABeforeClose
    AfterScroll = QrNFeCabAAfterScroll
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, IDCtrl,'
      'ide_nNF, ide_dEmi, ide_dSaiEnt, ICMSTot_vNF '
      'FROM nfecaba '
      ' '
      'WHERE EFD_INN_AnoMes <> 0'
      'AND EFD_INN_Empresa <> 0'
      'AND EFD_INN_LinArq <> 0'
      ''
      'AND EFD_INN_AnoMes=:P0'
      'AND EFD_INN_Empresa=:P1'
      'AND EFD_INN_LinArq=:P2')
    Left = 504
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrNFeCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFeCabAide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFeCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNFeCabA: TDataSource
    DataSet = QrNFeCabA
    Left = 532
    Top = 68
  end
  object QrNFEItsI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nome NO_GG1, nfei.*'
      'FROM nfeitsi nfei'
      'LEFT JOIN gragrux ggx ON ggx.Controle=nfei.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE nfei.FatID=:P0'
      'AND nfei.FatNum=:P1'
      'AND nfei.Empresa=:P2')
    Left = 504
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFEItsINO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrNFEItsIFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFEItsIFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFEItsIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFEItsInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFEItsIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrNFEItsIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrNFEItsIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrNFEItsIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrNFEItsIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrNFEItsIprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrNFEItsIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrNFEItsIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrNFEItsIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrNFEItsIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrNFEItsIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrNFEItsIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrNFEItsIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrNFEItsIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrNFEItsIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrNFEItsIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrNFEItsIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrNFEItsIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrNFEItsITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
    end
    object QrNFEItsI_Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrNFEItsIInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrNFEItsIEhServico: TIntegerField
      FieldName = 'EhServico'
    end
    object QrNFEItsILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFEItsIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFEItsIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFEItsIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFEItsIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFEItsIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFEItsIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFEItsIICMSRec_pRedBC: TFloatField
      FieldName = 'ICMSRec_pRedBC'
    end
    object QrNFEItsIICMSRec_vBC: TFloatField
      FieldName = 'ICMSRec_vBC'
    end
    object QrNFEItsIICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
    end
    object QrNFEItsIICMSRec_vICMS: TFloatField
      FieldName = 'ICMSRec_vICMS'
    end
    object QrNFEItsIIPIRec_pRedBC: TFloatField
      FieldName = 'IPIRec_pRedBC'
    end
    object QrNFEItsIIPIRec_vBC: TFloatField
      FieldName = 'IPIRec_vBC'
    end
    object QrNFEItsIIPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
    end
    object QrNFEItsIIPIRec_vIPI: TFloatField
      FieldName = 'IPIRec_vIPI'
    end
    object QrNFEItsIPISRec_pRedBC: TFloatField
      FieldName = 'PISRec_pRedBC'
    end
    object QrNFEItsIPISRec_vBC: TFloatField
      FieldName = 'PISRec_vBC'
    end
    object QrNFEItsIPISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
    end
    object QrNFEItsIPISRec_vPIS: TFloatField
      FieldName = 'PISRec_vPIS'
    end
    object QrNFEItsICOFINSRec_pRedBC: TFloatField
      FieldName = 'COFINSRec_pRedBC'
    end
    object QrNFEItsICOFINSRec_vBC: TFloatField
      FieldName = 'COFINSRec_vBC'
    end
    object QrNFEItsICOFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
    end
    object QrNFEItsICOFINSRec_vCOFINS: TFloatField
      FieldName = 'COFINSRec_vCOFINS'
    end
    object QrNFEItsIMeuID: TIntegerField
      FieldName = 'MeuID'
    end
    object QrNFEItsINivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrNFEItsIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
    object QrNFEItsIprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
    object QrNFEItsIprod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrNFEItsIprod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrNFEItsIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNFEItsIUnidMedCom: TIntegerField
      FieldName = 'UnidMedCom'
    end
    object QrNFEItsIUnidMedTrib: TIntegerField
      FieldName = 'UnidMedTrib'
    end
  end
  object DsNFEItsI: TDataSource
    DataSet = QrNFEItsI
    Left = 532
    Top = 96
  end
  object PMNF: TPopupMenu
    OnPopup = PMNFPopup
    Left = 140
    Top = 612
    object IncluiNF1: TMenuItem
      Caption = '&Inclui N.F. '
      Enabled = False
      OnClick = IncluiNF1Click
    end
    object AlteraNF1: TMenuItem
      Caption = '&Altera N.F.'
      Enabled = False
      OnClick = AlteraNF1Click
    end
  end
  object QrSINTEGR_54: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT s54.*'
      'FROM sintegr_54 s54'
      'WHERE s54.ImporExpor=:P0'
      'AND s54.AnoMes=:P1'
      'AND s54.Empresa=:P2'
      'AND s54.CNPJ=:P3'
      'AND s54.MODELO=:P4'
      'AND s54.SERIE=:P5'
      'AND s54.NUMNF=:P6'
      '')
    Left = 152
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end>
    object QrSINTEGR_54ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrSINTEGR_54AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrSINTEGR_54Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSINTEGR_54LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrSINTEGR_54TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSINTEGR_54CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSINTEGR_54MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrSINTEGR_54SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrSINTEGR_54NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrSINTEGR_54CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrSINTEGR_54CST: TWideStringField
      FieldName = 'CST'
      Size = 3
    end
    object QrSINTEGR_54NUMITEM: TWideStringField
      FieldName = 'NUMITEM'
      Size = 3
    end
    object QrSINTEGR_54CODPROD: TWideStringField
      FieldName = 'CODPROD'
      Size = 14
    end
    object QrSINTEGR_54QTDADE: TFloatField
      FieldName = 'QTDADE'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrSINTEGR_54VLRPROD: TFloatField
      FieldName = 'VLRPROD'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_54VLRDESC: TFloatField
      FieldName = 'VLRDESC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_54BASECALC: TFloatField
      FieldName = 'BASECALC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_54BASESUBTRI: TFloatField
      FieldName = 'BASESUBTRI'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_54VLRIPI: TFloatField
      FieldName = 'VLRIPI'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSINTEGR_54ALIQICMS: TFloatField
      FieldName = 'ALIQICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsSINTEGR_54: TDataSource
    DataSet = QrSINTEGR_54
    Left = 180
    Top = 12
  end
  object QrSINTEGR_70: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSINTEGR_50AfterOpen
    BeforeClose = QrSINTEGR_50BeforeClose
    AfterScroll = QrSINTEGR_50AfterScroll
    OnCalcFields = QrSINTEGR_50CalcFields
    SQL.Strings = (
      'SELECT s70.*'
      'FROM sintegr_70 s70'
      'WHERE s70.ImporExpor=:P0'
      'AND s70.Empresa=:P1'
      'AND s70.AnoMes=:P2'
      'ORDER BY LinArq'
      '')
    Left = 208
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSINTEGR_70ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrSINTEGR_70AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrSINTEGR_70Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSINTEGR_70LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrSINTEGR_70TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSINTEGR_70CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSINTEGR_70INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrSINTEGR_70EMISSAO: TDateField
      FieldName = 'EMISSAO'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSINTEGR_70UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSINTEGR_70MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrSINTEGR_70SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 1
    end
    object QrSINTEGR_70SUBSERIE: TWideStringField
      FieldName = 'SUBSERIE'
      Size = 2
    end
    object QrSINTEGR_70NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrSINTEGR_70CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrSINTEGR_70VALORTOT: TFloatField
      FieldName = 'VALORTOT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSINTEGR_70BASEICMS: TFloatField
      FieldName = 'BASEICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSINTEGR_70VALORICMS: TFloatField
      FieldName = 'VALORICMS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSINTEGR_70VLRISENTO: TFloatField
      FieldName = 'VLRISENTO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSINTEGR_70OUTROS: TFloatField
      FieldName = 'OUTROS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSINTEGR_70CIF_FOB: TSmallintField
      FieldName = 'CIF_FOB'
    end
    object QrSINTEGR_70SITUACAO: TWideStringField
      FieldName = 'SITUACAO'
      Size = 1
    end
    object QrSINTEGR_70Transporta: TIntegerField
      FieldName = 'Transporta'
    end
  end
  object DsSINTEGR_70: TDataSource
    DataSet = QrSINTEGR_70
    Left = 236
    Top = 12
  end
  object PMSeqAcoes1: TPopupMenu
    OnPopup = PMSeqAcoes1Popup
    Left = 580
    Top = 644
    object N70T_SeqAcoes_1: TMenuItem
      Caption = '&Exclui conhecimentos importados'
      OnClick = N70T_SeqAcoes_1Click
    end
    object N70T_SeqAcoes_2: TMenuItem
      Caption = '&Cria conhecimentos carregados'
      OnClick = N70T_SeqAcoes_2Click
    end
  end
end
