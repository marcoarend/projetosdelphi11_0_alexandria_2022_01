object FmSintegra_Arq: TFmSintegra_Arq
  Left = 339
  Top = 185
  Caption = 'SIN-TEGRA-001 :: Gera'#231#227'o de Arquivo Magn'#233'tico'
  ClientHeight = 692
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Gera'#231#227'o de Arquivo Magn'#233'tico'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitTop = 5
    end
  end
  object PageControl3: TPageControl
    Left = 0
    Top = 48
    Width = 1008
    Height = 644
    ActivePage = TabSheet6
    Align = alClient
    TabOrder = 1
    object TabSheet6: TTabSheet
      Caption = ' Gera'#231#227'o do arquivo '
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Splitter2: TSplitter
        Left = 0
        Top = 413
        Width = 1000
        Height = 5
        Cursor = crVSplit
        Align = alTop
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 413
        Align = alTop
        TabOrder = 0
        object PnBase: TPanel
          Left = 1
          Top = 1
          Width = 676
          Height = 411
          Align = alLeft
          ParentBackground = False
          TabOrder = 0
          object PnConfig: TPanel
            Left = 1
            Top = 1
            Width = 674
            Height = 300
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label4: TLabel
              Left = 4
              Top = 4
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label3: TLabel
              Left = 8
              Top = 196
              Width = 357
              Height = 13
              Caption = 
                #185': Neste caso, o arquivo dever'#225' conter, al'#233'm dos registros tipo ' +
                '10 e tipo 90, '
            end
            object Label5: TLabel
              Left = 16
              Top = 212
              Width = 341
              Height = 13
              Caption = 
                'apenas os registros referentes as opera'#231#245'es/presta'#231#245'es n'#227'o efeti' +
                'vadas.'
            end
            object EdEmpresa: TdmkEditCB
              Left = 4
              Top = 20
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEmpresa
              IgnoraDBLookupComboBox = False
            end
            object CBEmpresa: TdmkDBLookupComboBox
              Left = 60
              Top = 20
              Width = 609
              Height = 21
              KeyField = 'Filial'
              ListField = 'NOMEFILIAL'
              ListSource = DModG.DsEmpresas
              TabOrder = 1
              dmkEditCB = EdEmpresa
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object RG_IdNatOpInfo: TRadioGroup
              Left = 252
              Top = 44
              Width = 417
              Height = 61
              Caption = ' Identifica'#231#227'o da natureza das opera'#231#245'es informadas: '
              ItemIndex = 2
              Items.Strings = (
                
                  '1 - Interestaduais somente opera'#231#245'es sujeitas ao regime de Subst' +
                  'itui'#231#227'o Tribut'#225'ria'
                
                  '2 - Interestaduais - opera'#231#245'es com ou sem Substitui'#231#227'o Tribut'#225'ri' +
                  'a'
                '3 - Totalidade das opera'#231#245'es do informante')
              TabOrder = 2
            end
            object RG_Finalidade: TRadioGroup
              Left = 4
              Top = 108
              Width = 665
              Height = 85
              Caption = ' Finalidade da apresenta'#231#227'o do arquivo magn'#233'tico: '
              ItemIndex = 0
              Items.Strings = (
                '1 - Normal.'
                
                  '2 - Retifica'#231#227'o total de arquivo: substitui'#231#227'o total de informa'#231 +
                  #245'es prestadas pelo contribuinte referentes a este per'#237'odo.'
                
                  '3 - Retifica'#231#227'o aditiva de arquivo: acr'#233'scimo de informa'#231#227'o n'#227'o ' +
                  'inclu'#237'da em arquivos j'#225' apresentados.'
                
                  '5 - Desfazimento: arquivo de informa'#231#227'o referente a opera'#231#245'es/pr' +
                  'esta'#231#245'es n'#227'o efetivadas.'#185' ')
              TabOrder = 3
            end
            object GroupBox1: TGroupBox
              Left = 4
              Top = 44
              Width = 245
              Height = 61
              Caption = ' Per'#237'odo [F3]: '
              TabOrder = 4
              object Label1: TLabel
                Left = 8
                Top = 16
                Width = 55
                Height = 13
                Caption = 'Data inicial:'
              end
              object Label2: TLabel
                Left = 124
                Top = 16
                Width = 48
                Height = 13
                Caption = 'Data final:'
              end
              object TPDataIni: TdmkEditDateTimePicker
                Left = 8
                Top = 32
                Width = 113
                Height = 21
                Date = 40165.499141331020000000
                Time = 40165.499141331020000000
                TabOrder = 0
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                LastDayTimePicker = TPDataFim
              end
              object TPDataFim: TdmkEditDateTimePicker
                Left = 124
                Top = 32
                Width = 113
                Height = 21
                Date = 40165.499141331020000000
                Time = 40165.499141331020000000
                TabOrder = 1
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
              end
            end
            object CkRecriarForca: TCheckBox
              Left = 396
              Top = 196
              Width = 269
              Height = 17
              Caption = 'Recriar notas de entrada criadas a for'#231'a.'
              Enabled = False
              TabOrder = 5
            end
            object CkConsertaNome: TCheckBox
              Left = 396
              Top = 216
              Width = 193
              Height = 17
              Caption = 'Conserta nome de uso e consumo.'
              Checked = True
              State = cbChecked
              TabOrder = 6
            end
            object Panel3: TPanel
              Left = 0
              Top = 259
              Width = 674
              Height = 41
              Align = alBottom
              TabOrder = 7
              object LaAviso: TLabel
                Left = 8
                Top = 4
                Width = 13
                Height = 13
                Caption = '...'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object PB1: TProgressBar
                Left = 8
                Top = 20
                Width = 656
                Height = 17
                TabOrder = 0
              end
            end
            object Ck_54_900: TCheckBox
              Left = 396
              Top = 236
              Width = 237
              Height = 17
              Caption = 'Exportar itens 900 a 999 para o registro 54.'
              Checked = True
              State = cbChecked
              TabOrder = 8
            end
            object CkGera75x54: TCheckBox
              Left = 16
              Top = 236
              Width = 309
              Height = 17
              Caption = 'Gerar um registro do tipo 75 para cada registro do tipo 54.'
              TabOrder = 9
            end
          end
          object MeGerado: TMemo
            Left = 1
            Top = 301
            Width = 674
            Height = 109
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            WordWrap = False
            OnKeyUp = MeGeradoKeyUp
            OnMouseDown = MeGeradoMouseDown
            OnMouseUp = MeGeradoMouseUp
          end
        end
        object Panel6: TPanel
          Left = 677
          Top = 1
          Width = 322
          Height = 411
          Align = alClient
          TabOrder = 1
          object Grade: TStringGrid
            Left = 1
            Top = 1
            Width = 320
            Height = 193
            Align = alClient
            ColCount = 9
            DefaultColWidth = 20
            DefaultRowHeight = 18
            RowCount = 2
            TabOrder = 0
            OnDrawCell = GradeDrawCell
            ColWidths = (
              20
              105
              44
              20
              20
              20
              20
              20
              20)
          end
          object MeAvisos: TMemo
            Left = 1
            Top = 194
            Width = 320
            Height = 134
            Align = alBottom
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 402896
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            WordWrap = False
            OnKeyUp = MeGeradoKeyUp
            OnMouseDown = MeGeradoMouseDown
            OnMouseUp = MeGeradoMouseUp
          end
          object Memo2: TMemo
            Left = 1
            Top = 328
            Width = 320
            Height = 82
            Align = alBottom
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
          end
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 418
        Width = 1000
        Height = 150
        ActivePage = TabSheet4
        Align = alClient
        TabOrder = 1
        object TabSheet1: TTabSheet
          Caption = ' Itens Sem Data Fiscal '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label6: TLabel
            Left = 0
            Top = 0
            Width = 120
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Itens sem Data fiscal'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Panel5: TPanel
            Left = 964
            Top = 13
            Width = 87
            Height = 81
            Align = alRight
            TabOrder = 0
            object Button1: TButton
              Left = 4
              Top = 4
              Width = 75
              Height = 41
              Caption = 'Ajusta'#13#10'Data'
              TabOrder = 0
              OnClick = Button1Click
            end
          end
          object DBGrid1: TDBGrid
            Left = 0
            Top = 13
            Width = 964
            Height = 81
            Align = alClient
            DataSource = DsDtaFis
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Notas fiscais (50)'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 1051
          ExplicitHeight = 94
          object Label7: TLabel
            Left = 0
            Top = 0
            Width = 199
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Duplo clique abre janela de edi'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object DBGrid2: TDBGrid
            Left = 0
            Top = 13
            Width = 624
            Height = 109
            Align = alClient
            DataSource = DsCabA
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGrid2DblClick
          end
          object DBGrid6: TDBGrid
            Left = 624
            Top = 13
            Width = 368
            Height = 109
            Align = alRight
            DataSource = DsSubN
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGrid2DblClick
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Itens das Notas Fiscais ()'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PageControl2: TPageControl
            Left = 0
            Top = 0
            Width = 992
            Height = 122
            ActivePage = TabSheet5
            Align = alClient
            TabOrder = 0
            object TabSheet5: TTabSheet
              Caption = 'Dados produto'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGrid4: TDBGrid
                Left = 0
                Top = 0
                Width = 1051
                Height = 94
                Align = alClient
                DataSource = DsItsi
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
              end
            end
          end
        end
        object TabSheet8: TTabSheet
          Caption = ' Invent'#225'rio (74)'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PageControl4: TPageControl
            Left = 0
            Top = 0
            Width = 992
            Height = 122
            ActivePage = TabSheet11
            Align = alClient
            TabOrder = 0
            object TabSheet9: TTabSheet
              Caption = ' Dados do Invent'#225'rio '
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 1043
              ExplicitHeight = 66
              object DBGSMIC2: TdmkDBGrid
                Left = 0
                Top = 0
                Width = 984
                Height = 94
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'My_Idx'
                    Title.Caption = #205'ndice'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Empresa'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EntiSitio'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 54
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PGT'
                    Title.Caption = 'Tipo de Produto'
                    Width = 160
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Qtde'
                    Title.Caption = 'Quantidade'
                    Width = 73
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PrcCusUni'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorTot'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SIGLA'
                    Title.Caption = 'Sigla'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD'
                    Title.Caption = 'Produto'
                    Width = 223
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TAM'
                    Title.Caption = 'Tam.'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_COR'
                    Title.Caption = 'Cor'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PECAS'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAP2'
                    Title.Caption = #193'rea ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PESO'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DmProd.DsSMIC2
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'My_Idx'
                    Title.Caption = #205'ndice'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Empresa'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EntiSitio'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 54
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PGT'
                    Title.Caption = 'Tipo de Produto'
                    Width = 160
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Qtde'
                    Title.Caption = 'Quantidade'
                    Width = 73
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PrcCusUni'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorTot'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SIGLA'
                    Title.Caption = 'Sigla'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD'
                    Title.Caption = 'Produto'
                    Width = 223
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TAM'
                    Title.Caption = 'Tam.'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_COR'
                    Title.Caption = 'Cor'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PECAS'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAP2'
                    Title.Caption = #193'rea ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PESO'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end>
              end
            end
            object TabSheet11: TTabSheet
              Caption = ' Falhas na exporta'#231#227'o '
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGExport2: TdmkDBGrid
                Left = 0
                Top = 0
                Width = 570
                Height = 94
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'My_Idx'
                    Title.Caption = #205'ndice'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Empresa'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EntiSitio'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 54
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PGT'
                    Title.Caption = 'Tipo de Produto'
                    Width = 160
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'QTDE'
                    Title.Caption = 'Quantidade'
                    Width = 73
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PrcCusUni'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorTot'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SIGLA'
                    Title.Caption = 'Sigla'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD'
                    Title.Caption = 'Produto'
                    Width = 223
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TAM'
                    Title.Caption = 'Tam.'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_COR'
                    Title.Caption = 'Cor'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PECAS'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAP2'
                    Title.Caption = #193'rea ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PESO'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DmProd.DsExport2
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'My_Idx'
                    Title.Caption = #205'ndice'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Empresa'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EntiSitio'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'GraGruX'
                    Title.Caption = 'Reduzido'
                    Width = 54
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PGT'
                    Title.Caption = 'Tipo de Produto'
                    Width = 160
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'QTDE'
                    Title.Caption = 'Quantidade'
                    Width = 73
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PrcCusUni'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorTot'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SIGLA'
                    Title.Caption = 'Sigla'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRD'
                    Title.Caption = 'Produto'
                    Width = 223
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TAM'
                    Title.Caption = 'Tam.'
                    Width = 42
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_COR'
                    Title.Caption = 'Cor'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PECAS'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AREAP2'
                    Title.Caption = #193'rea ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PESO'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end>
              end
              object DBGListErr1: TDBGrid
                Left = 570
                Top = 0
                Width = 414
                Height = 94
                Align = alRight
                DataSource = DmProd.DsListErr1
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Texto1'
                    Width = 300
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Width = 76
                    Visible = True
                  end>
              end
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Produtos (75)'
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid3: TDBGrid
            Left = 0
            Top = 0
            Width = 992
            Height = 122
            Align = alClient
            DataSource = DsPrds
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
      object PainelConfirma: TPanel
        Left = 0
        Top = 568
        Width = 1000
        Height = 48
        Align = alBottom
        ParentBackground = False
        TabOrder = 2
        object BtGera: TBitBtn
          Tag = 14
          Left = 20
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Gera'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtGeraClick
        end
        object Panel2: TPanel
          Left = 888
          Top = 1
          Width = 111
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 2
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object Panel4: TPanel
          Left = 116
          Top = 12
          Width = 821
          Height = 24
          BevelOuter = bvNone
          TabOrder = 2
          object StatusBar: TStatusBar
            Left = 0
            Top = 5
            Width = 821
            Height = 19
            Panels = <
              item
                Text = ' Posi'#231#227'o do cursor no texto gerado:'
                Width = 192
              end
              item
                Width = 100
              end
              item
                Text = ' Arquivo salvo:'
                Width = 96
              end
              item
                Width = 50
              end>
          end
        end
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'Compara'#231#227'o (Igap'#243')'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel7: TPanel
        Left = 0
        Top = 568
        Width = 1000
        Height = 48
        Align = alBottom
        ParentBackground = False
        TabOrder = 0
        object BtCompara: TBitBtn
          Tag = 14
          Left = 20
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Compara'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtComparaClick
        end
        object Panel8: TPanel
          Left = 888
          Top = 1
          Width = 111
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn2: TBitBtn
            Tag = 13
            Left = 2
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtExclCo: TBitBtn
          Tag = 12
          Left = 112
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Exclui'
          NumGlyphs = 2
          TabOrder = 2
          OnClick = BtExclCoClick
        end
      end
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 93
        Align = alTop
        ParentBackground = False
        TabOrder = 1
        object CGTipos: TdmkCheckGroup
          Left = 1
          Top = 1
          Width = 788
          Height = 47
          Align = alLeft
          Caption = ' Tipos de registros: '
          Columns = 14
          ItemIndex = 13
          Items.Strings = (
            '10'
            '11'
            '50'
            '51'
            '53'
            '54'
            '70'
            '71'
            '74'
            '75'
            '85'
            '86'
            '88'
            '90')
          TabOrder = 0
          UpdType = utYes
          Value = 8192
          OldValor = 0
        end
        object BtTodos: TBitBtn
          Tag = 127
          Left = 796
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Todos'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtTodosClick
        end
        object BtNenhum: TBitBtn
          Tag = 127
          Left = 892
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Nenhum'
          NumGlyphs = 2
          TabOrder = 2
          OnClick = BtNenhumClick
        end
        object Memo1: TMemo
          Left = 1
          Top = 48
          Width = 998
          Height = 44
          Align = alBottom
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
      end
      object Panel11: TPanel
        Left = 0
        Top = 93
        Width = 561
        Height = 475
        Align = alClient
        ParentBackground = False
        TabOrder = 2
        object Label8: TLabel
          Left = 1
          Top = 1
          Width = 74
          Height = 13
          Align = alTop
          Alignment = taCenter
          Caption = 'Minha tabela'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label9: TLabel
          Left = 1
          Top = 165
          Width = 106
          Height = 13
          Align = alTop
          Alignment = taCenter
          Caption = 'Tabela comparada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object GradeMy: TDBGrid
          Left = 1
          Top = 14
          Width = 559
          Height = 151
          Align = alTop
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = GradeMyDblClick
        end
        object GradeCo: TDBGrid
          Left = 1
          Top = 178
          Width = 559
          Height = 296
          Align = alClient
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object Panel12: TPanel
        Left = 561
        Top = 93
        Width = 439
        Height = 475
        Align = alRight
        ParentBackground = False
        TabOrder = 3
        object Label10: TLabel
          Left = 1
          Top = 1
          Width = 136
          Height = 13
          Align = alTop
          Alignment = taCenter
          Caption = 'Diferen'#231'as encontradas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Splitter1: TSplitter
          Left = 1
          Top = 345
          Width = 437
          Height = 5
          Cursor = crVSplit
          Align = alTop
          ExplicitTop = 401
        end
        object DBGrid5: TDBGrid
          Left = 1
          Top = 350
          Width = 437
          Height = 76
          Align = alClient
          DataSource = DsDifere
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid5DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'TipoReg'
              Title.Caption = 'Reg.'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ItemReg'
              Title.Caption = 'Item'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Campo'
              Width = 88
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValMy'
              Title.Caption = 'Valor na minha tabela'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValCo'
              Title.Caption = 'Valor na tabela comparada'
              Width = 120
              Visible = True
            end>
        end
        object GradeCampos: TdmkDBGridDAC
          Left = 1
          Top = 14
          Width = 437
          Height = 331
          OnAfterReopenAfterSQLExec = GradeCamposAfterReopenAfterSQLExec
          SQLFieldsToChange.Strings = (
            'Ativo')
          SQLIndexesOnUpdate.Strings = (
            'Campo')
          Align = alTop
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'Ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Campo'
              Width = 375
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCampos
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          SQLTable = 'sintegraec'
          EditForceNextYear = False
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'Ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Campo'
              Width = 375
              Visible = True
            end>
        end
        object Panel9: TPanel
          Left = 1
          Top = 426
          Width = 437
          Height = 48
          Align = alBottom
          TabOrder = 2
          object BitBtn1: TBitBtn
            Tag = 127
            Left = 5
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Todos'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn1Click
          end
          object BitBtn3: TBitBtn
            Tag = 128
            Left = 101
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Nenhum'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BitBtn3Click
          end
          object BitBtn4: TBitBtn
            Tag = 128
            Left = 197
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Refaz dif.'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BitBtn4Click
          end
        end
      end
    end
    object TabSheet10: TTabSheet
      Caption = ' Importa Sintegra '
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel13: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 49
        Align = alTop
        ParentBackground = False
        TabOrder = 0
        object Label83: TLabel
          Left = 8
          Top = 4
          Width = 166
          Height = 13
          Caption = 'Arquivo magn'#233'tico a ser importado:'
        end
        object SpeedButton7: TSpeedButton
          Left = 506
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton7Click
        end
        object SpeedButton1: TSpeedButton
          Left = 526
          Top = 20
          Width = 21
          Height = 21
          Caption = '>'
          OnClick = SpeedButton1Click
        end
        object EdSINTEGRA_Path: TdmkEdit
          Left = 8
          Top = 20
          Width = 497
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = 'C:\_MLArend\Clientes\Ideal - MJ Novaes\2010 01\LFS00100110.TXT'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 'C:\_MLArend\Clientes\Ideal - MJ Novaes\2010 01\LFS00100110.TXT'
          ValWarn = False
        end
        object RGComoImporta: TRadioGroup
          Left = 780
          Top = 1
          Width = 219
          Height = 47
          Align = alRight
          Caption = ' Qual c'#243'digo considerar no carregamento: '
          Columns = 2
          ItemIndex = 1
          Items.Strings = (
            'Reduzido'
            'Nivel 1 / CodUsu')
          TabOrder = 1
        end
        object CkRecriarNFs: TCheckBox
          Left = 560
          Top = 24
          Width = 141
          Height = 17
          Caption = 'Recriar notas j'#225' inseridas.'
          TabOrder = 2
        end
        object CkExclEstqMP: TCheckBox
          Left = 560
          Top = 4
          Width = 213
          Height = 17
          Caption = 'Excluir itens de estoque de mat'#233'ria-prima.'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
      end
      object Panel14: TPanel
        Left = 0
        Top = 49
        Width = 1000
        Height = 567
        Align = alClient
        ParentBackground = False
        TabOrder = 1
        object Panel15: TPanel
          Left = 1
          Top = 516
          Width = 998
          Height = 50
          Align = alBottom
          TabOrder = 0
          object BtCarrega: TBitBtn
            Left = 20
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Carrega'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtCarregaClick
          end
          object BtImporta: TBitBtn
            Left = 112
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Importa'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BtImportaClick
          end
          object GroupBox2: TGroupBox
            Left = 260
            Top = 1
            Width = 737
            Height = 48
            Align = alRight
            Caption = ' Dados da Empresa: '
            TabOrder = 2
            object Label11: TLabel
              Left = 8
              Top = 24
              Width = 30
              Height = 13
              Caption = 'CNPJ:'
            end
            object Label12: TLabel
              Left = 164
              Top = 24
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
            end
            object Label13: TLabel
              Left = 264
              Top = 24
              Width = 31
              Height = 13
              Caption = 'Nome:'
            end
            object EdEmp_CNPJ: TdmkEdit
              Left = 40
              Top = 20
              Width = 112
              Height = 21
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdEmp_CNPJChange
            end
            object EdEmp_Codigo: TdmkEdit
              Left = 204
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdEmp_Nome: TdmkEdit
              Left = 300
              Top = 20
              Width = 425
              Height = 21
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
        object PageControl5: TPageControl
          Left = 1
          Top = 1
          Width = 998
          Height = 474
          ActivePage = TabSheet14
          Align = alClient
          TabOrder = 1
          object TabSheet12: TTabSheet
            Caption = ' Carregamento'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GridS: TStringGrid
              Left = 424
              Top = 0
              Width = 566
              Height = 446
              Align = alRight
              ColCount = 9
              DefaultColWidth = 20
              DefaultRowHeight = 18
              RowCount = 2
              TabOrder = 0
              OnDrawCell = GradeDrawCell
              ColWidths = (
                20
                105
                44
                20
                20
                20
                20
                20
                20)
            end
            object MeImportado: TMemo
              Left = 0
              Top = 0
              Width = 424
              Height = 446
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              WordWrap = False
              OnKeyUp = MeImportadoKeyUp
              OnMouseDown = MeImportadoMouseDown
              OnMouseUp = MeImportadoMouseUp
            end
          end
          object TabSheet13: TTabSheet
            Caption = ' Ignorados no carregamento '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object MeIgnor: TMemo
              Left = 0
              Top = 0
              Width = 990
              Height = 446
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
          object TabSheet14: TTabSheet
            Caption = ' Erros no carregamento '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object MeErros: TMemo
              Left = 0
              Top = 0
              Width = 990
              Height = 446
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
          object TabSheet15: TTabSheet
            Caption = ' N'#227'o importados '
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object MeNaoImport: TMemo
              Left = 0
              Top = 0
              Width = 990
              Height = 446
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
        end
        object Panel16: TPanel
          Left = 1
          Top = 475
          Width = 998
          Height = 41
          Align = alBottom
          TabOrder = 2
          object LaAviso2: TLabel
            Left = 8
            Top = 4
            Width = 41
            Height = 13
            Caption = '..........'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object PB2: TProgressBar
            Left = 8
            Top = 20
            Width = 981
            Height = 17
            TabOrder = 0
          end
        end
      end
    end
  end
  object QrEnti: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiCalcFields
    SQL.Strings = (
      'SELECT IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT,'
      'IF(en.Tipo=0,en.CNPJ,en.RG) CNPJ_CPF, '
      'IF(en.Tipo=0,en.IE,en.RG) IE_RG,'
      'IF(en.Tipo=0,en.ECidade,en.PCidade) xCidade, '
      'IF(en.Tipo=0,en.EFax,en.PFax) Fax,'
      'IF(en.Tipo=0,en.ERua,en.PRua) Rua,'
      'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.0 Numero,'
      'IF(en.Tipo=0,en.ECompl,en.PCompl) Compl,'
      'IF(en.Tipo=0,en.EBairro,en.PBairro) Bairro,'
      'IF(en.Tipo=0,en.ECEP,en.PCEP) CEP,'
      'en.CNPJ, en.IE,'
      'uf.Nome xUF, ll.Nome Lograd'
      'FROM entidades en'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)'
      
        'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en' +
        '.PLograd)'
      'WHERE en.Codigo=:P0')
    Left = 4
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrEntiCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntiIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEntixCidade: TWideStringField
      FieldName = 'xCidade'
      Size = 25
    end
    object QrEntiFax: TWideStringField
      FieldName = 'Fax'
    end
    object QrEntixUF: TWideStringField
      FieldName = 'xUF'
      Size = 2
    end
    object QrEntiRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrEntiNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrEntiCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrEntiBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrEntiCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrEntiLograd: TWideStringField
      FieldName = 'Lograd'
      Size = 10
    end
    object QrEntiLOGRADOURO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LOGRADOURO'
      Size = 255
      Calculated = True
    end
    object QrEntiCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntiIE: TWideStringField
      FieldName = 'IE'
    end
  end
  object QrCabA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      ''
      
        'IF(nfea.CodInfoEmit=-11,  IF(nfea.dest_CNPJ <> "", nfea.dest_CNP' +
        'J, nfea.dest_CPF),'
      
        '  IF(nfea.emit_CNPJ <> "", nfea.emit_CNPJ, nfea.emit_CPF))  CNPJ' +
        '_CPF,'
      ''
      'IF(nfea.CodInfoEmit=-11,  nfea.dest_IE,  nfea.emit_IE) IE,'
      ''
      'IF(nfea.CodInfoEmit=-11,  nfea.dest_UF,  nfea.emit_UF) UF,'
      ''
      'IF(nfea.CodInfoEmit=-11, 1, 0) EhOEmitente,'
      ' nfea.*'
      'FROM nfecaba nfea'
      'WHERE ('
      ' (Status IN (100,101,102,301,302)) OR (nfea.CodInfoEmit<>-11))'
      'AND ('
      '  (nfea.CodInfoEmit=-11) OR (nfea.CodInfoDest=-11)'
      ') AND (nfea.DataFiscal BETWEEN "2010/09/01" AND "2010/09/30")'
      'ORDER BY nfea.DataFiscal'
      '')
    Left = 32
    Top = 4
    object QrCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCabAide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrCabAide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrCabAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrCabAdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrCabAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrCabAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrCabADataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrCabACNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 14
    end
    object QrCabAIE: TWideStringField
      FieldName = 'IE'
      Size = 14
    end
    object QrCabAUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrCabAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrCabAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrCabAICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrCabAICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrCabAICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrCabAICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrCabAICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrCabAICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrCabAICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrCabAICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrCabAICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrCabAICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrCabAICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrCabAICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrCabAICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrCabAICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrCabAISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
    end
    object QrCabAEhOEmitente: TLargeintField
      FieldName = 'EhOEmitente'
      Required = True
    end
    object QrCabACodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCabACodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
  end
  object QrSubN: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT itsi.prod_CFOP, itsn.ICMS_pICMS,'
      'SUM(itsi.prod_vProd) prod_vProd,'
      
        'SUM(itsi.prod_vProd + itsi.prod_vFrete + itsi.prod_vSeg + itsi.p' +
        'rod_vDesc + itsi.prod_vOutro) prod_vNF,'
      'SUM(itsn.ICMS_vBC) ICMS_vBC,'
      'SUM(itsn.ICMS_vICMS) ICMS_vICMS'
      'FROM nfeitsi itsi'
      'LEFT JOIN nfeitsn itsn ON '
      '      itsi.FatID=itsn.FatID'
      '  AND itsi.FatNum=itsn.FatNum'
      '  AND itsi.Empresa=itsn.Empresa'
      '  AND itsi.nItem=itsn.nItem'
      'WHERE itsi.FatID=:P0'
      'AND itsi.FatNum=:P1'
      'AND itsi.Empresa=:P2'
      'GROUP BY itsi.prod_CFOP, itsn.ICMS_pICMS'
      'ORDER BY itsi.prod_CFOP, itsn.ICMS_pICMS')
    Left = 88
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSubNprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrSubNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrSubNprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrSubNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrSubNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrSubNprod_vNF: TFloatField
      FieldName = 'prod_vNF'
    end
  end
  object QrDtaFis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE DataFiscal = 0')
    Left = 60
    Top = 4
  end
  object QrSubO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT itsi.prod_CFOP, itsn.ICMS_pICMS,'
      'SUM(itsi.prod_vProd) prod_vProd,'
      
        'SUM(itsi.prod_vProd + itsi.prod_vFrete + itsi.prod_vSeg + itsi.p' +
        'rod_vDesc + itsi.prod_vOutro) prod_vNF,'
      'SUM(IF(itso.IPI_vIPI, itso.IPI_vBC, 0)) IPI_vBC,'
      'SUM(itso.IPI_vIPI) IPI_vIPI'
      'FROM nfeitsi itsi'
      'LEFT JOIN nfeitsn itsn ON '
      '      itsi.FatID=itsn.FatID'
      '  AND itsi.FatNum=itsn.FatNum'
      '  AND itsi.Empresa=itsn.Empresa'
      '  AND itsi.nItem=itsn.nItem'
      'LEFT JOIN nfeitso itso ON'
      '      itsn.FatID=itso.FatID'
      '  AND itsn.FatNum=itso.FatNum'
      '  AND itsn.Empresa=itso.Empresa'
      '  AND itsn.nItem=itso.nItem'
      'WHERE itsi.FatID=:P0'
      'AND itsi.FatNum=:P1'
      'AND itsi.Empresa=:P2'
      'GROUP BY itsi.prod_CFOP, itsn.ICMS_pICMS'
      'ORDER BY itsi.prod_CFOP, itsn.ICMS_pICMS'
      '')
    Left = 116
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSubOprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrSubOICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrSubOprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrSubOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrSubOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrSubOprod_vNF: TFloatField
      FieldName = 'prod_vNF'
    end
  end
  object QrItsI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'ORDER BY nItem')
    Left = 144
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrItsInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrItsIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrItsIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrItsIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrItsIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrItsINivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrItsIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrItsIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrItsIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
  end
  object QrItsN: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 172
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsNnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrItsNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrItsNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrItsNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrItsNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrItsNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrItsNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrItsNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
  end
  object QrItsO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 200
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsOnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrItsOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
    object QrItsOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
  end
  object QrProd_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT DISTINCT  med.Sigla, gg1.NCM, gg1.IPI_Alq, gg1.ICMSAliqSI' +
        'NTEGRA, '
      'gg1.ICMS_pRedBC, gg1.ICMS_pRedBCST, gg1.ICMSST_BaseSINTEGRA,'
      'gg1.Nivel1, gg1.Nome, itsi.prod_cProd, itsi.prod_xProd'
      'FROM nfeitsi itsi'
      
        'LEFT JOIN nfecaba caba ON caba.FatID=itsi.FatID AND caba.FatNum=' +
        'itsi.FatNum AND caba.Empresa=itsi.Empresa '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=itsi.Nivel1'
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed '
      'WHERE ('
      '  (caba.emit_CNPJ <> :P0) OR (caba.dest_CNPJ <> :P1)'
      ') AND (caba.DataFiscal BETWEEN :P2 AND :P3)'
      'ORDER BY gg1.Nivel1')
    Left = 228
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrProd_Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrProd_NCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrProd_IPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrProd_ICMSAliqSINTEGRA: TFloatField
      FieldName = 'ICMSAliqSINTEGRA'
    end
    object QrProd_ICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrProd_ICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrProd_Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrProd_Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrProd_ICMSST_BaseSINTEGRA: TFloatField
      FieldName = 'ICMSST_BaseSINTEGRA'
    end
    object QrProd_prod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrProd_prod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
  end
  object QrExpA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE ('
      '  (emit_CNPJ = :P0) OR (dest_CNPJ =:P1)'
      ') AND (SINTEGRA_ExpAverDta BETWEEN :P2 AND :P3) '
      'ORDER BY SINTEGRA_ExpDeclDta, SINTEGRA_ExpDeclNum, ide_dEmi')
    Left = 256
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrExpASINTEGRA_ExpDeclNum: TWideStringField
      FieldName = 'SINTEGRA_ExpDeclNum'
      Size = 11
    end
    object QrExpASINTEGRA_ExpDeclDta: TDateField
      FieldName = 'SINTEGRA_ExpDeclDta'
    end
    object QrExpASINTEGRA_ExpNat: TWideStringField
      FieldName = 'SINTEGRA_ExpNat'
      Size = 1
    end
    object QrExpASINTEGRA_ExpRegNum: TWideStringField
      FieldName = 'SINTEGRA_ExpRegNum'
      Size = 12
    end
    object QrExpASINTEGRA_ExpRegDta: TDateField
      FieldName = 'SINTEGRA_ExpRegDta'
    end
    object QrExpASINTEGRA_ExpConhNum: TWideStringField
      FieldName = 'SINTEGRA_ExpConhNum'
      Size = 16
    end
    object QrExpASINTEGRA_ExpConhDta: TDateField
      FieldName = 'SINTEGRA_ExpConhDta'
    end
    object QrExpASINTEGRA_ExpConhTip: TWideStringField
      FieldName = 'SINTEGRA_ExpConhTip'
      Size = 2
    end
    object QrExpASINTEGRA_ExpPais: TWideStringField
      FieldName = 'SINTEGRA_ExpPais'
      Size = 4
    end
    object QrExpASINTEGRA_ExpAverDta: TDateField
      FieldName = 'SINTEGRA_ExpAverDta'
    end
    object QrExpAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrExpAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrExpAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrExpAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
  end
  object DsCabA: TDataSource
    DataSet = QrCabA
    Left = 32
    Top = 32
  end
  object DsDtaFis: TDataSource
    DataSet = QrDtaFis
    Left = 60
    Top = 32
  end
  object QrEnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ufe.Nome UFx'
      'FROM entidades ent'
      'LEFT JOIN ufs ufe ON ufe.Codigo='
      '  IF(ent.Tipo=0, EUF, PUF)'
      'WHERE ent.Codigo=:P0')
    Left = 340
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntUFx: TWideStringField
      FieldName = 'UFx'
      Required = True
      Size = 2
    end
  end
  object QrLocod: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      'SELECT CodUsu, Nome, NCM'
      'FROM gragru1'
      'WHERE Nivel1=:P0'
      '*/'
      'SELECT gg1.CodUsu, med.Sigla, gg1.NCM,'
      'gg1.IPI_Alq, gg1.ICMSAliqSINTEGRA,'
      'gg1.ICMS_pRedBC, gg1.ICMS_pRedBCST,'
      'gg1.ICMSST_BaseSINTEGRA, gg1.Nome'
      'FROM gragru1 gg1'
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed'
      'WHERE gg1.Nivel1=:P0'
      '')
    Left = 312
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocodCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrLocodNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrLocodNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrLocodSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrLocodIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrLocodICMSAliqSINTEGRA: TFloatField
      FieldName = 'ICMSAliqSINTEGRA'
    end
    object QrLocodICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrLocodICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrLocodICMSST_BaseSINTEGRA: TFloatField
      FieldName = 'ICMSST_BaseSINTEGRA'
    end
  end
  object QrPrds: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      '/*'
      
        'SELECT s75.Codigo,med.Sigla, gg1.NCM, gg1.IPI_Alq, gg1.ICMSAliqS' +
        'INTEGRA,'
      'gg1.ICMS_pRedBC, gg1.ICMS_pRedBCST, gg1.ICMSST_BaseSINTEGRA'
      'FROM sintegra75 s75'
      'LEFT JOIN gragru1 gg1 ON gg1.CodUsu=s75.Codigo'
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed'
      'ORDER BY Codigo'
      '*/'
      'SELECT s75.*'
      'FROM sintegra75 s75'
      'ORDER BY Codigo'
      '')
    Left = 284
    Top = 4
    object QrPrdsCodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 14
    end
    object QrPrdsSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrPrdsNCM: TWideStringField
      FieldName = 'NCM'
      Size = 8
    end
    object QrPrdsIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrPrdsICMSAliqSINTEGRA: TFloatField
      FieldName = 'ICMSAliqSINTEGRA'
    end
    object QrPrdsICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrPrdsICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrPrdsICMSST_BaseSINTEGRA: TFloatField
      FieldName = 'ICMSST_BaseSINTEGRA'
    end
    object QrPrdsNome: TWideStringField
      FieldName = 'Nome'
      Size = 53
    end
    object QrPrdsItens: TIntegerField
      FieldName = 'Itens'
    end
    object QrPrdsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsPrds: TDataSource
    DataSet = QrPrds
    Left = 284
    Top = 32
  end
  object DsItsi: TDataSource
    DataSet = QrItsI
    Left = 144
    Top = 32
  end
  object DsSubN: TDataSource
    DataSet = QrSubN
    Left = 88
    Top = 32
  end
  object QrSintegra10: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra10')
    Left = 676
    Top = 184
    object QrSintegra10TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSintegra10CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSintegra10INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrSintegra10NCONTRIB: TWideStringField
      FieldName = 'NCONTRIB'
      Size = 35
    end
    object QrSintegra10MUNICIPIO: TWideStringField
      FieldName = 'MUNICIPIO'
      Size = 30
    end
    object QrSintegra10UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSintegra10FAX: TWideStringField
      FieldName = 'FAX'
      Size = 10
    end
    object QrSintegra10DATAINI: TDateField
      FieldName = 'DATAINI'
    end
    object QrSintegra10DATAFIN: TDateField
      FieldName = 'DATAFIN'
    end
    object QrSintegra10CODCONV: TWideStringField
      FieldName = 'CODCONV'
      Size = 1
    end
    object QrSintegra10CODNAT: TWideStringField
      FieldName = 'CODNAT'
      Size = 1
    end
    object QrSintegra10CODFIN: TWideStringField
      FieldName = 'CODFIN'
      Size = 1
    end
  end
  object DsSintegra10: TDataSource
    DataSet = QrSintegra10
    Left = 704
    Top = 184
  end
  object DsR10: TDataSource
    DataSet = QrR10
    Left = 704
    Top = 212
  end
  object QrR10: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _r10')
    Left = 676
    Top = 212
    object QrR10TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrR10CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrR10INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrR10NCONTRIB: TWideStringField
      FieldName = 'NCONTRIB'
      Size = 35
    end
    object QrR10MUNICIPIO: TWideStringField
      FieldName = 'MUNICIPIO'
      Size = 30
    end
    object QrR10UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrR10FAX: TWideStringField
      FieldName = 'FAX'
      Size = 10
    end
    object QrR10DATAINI: TDateField
      FieldName = 'DATAINI'
    end
    object QrR10DATAFIN: TDateField
      FieldName = 'DATAFIN'
    end
    object QrR10CODCONV: TWideStringField
      FieldName = 'CODCONV'
      Size = 1
    end
    object QrR10CODNAT: TWideStringField
      FieldName = 'CODNAT'
      Size = 1
    end
    object QrR10CODFIN: TWideStringField
      FieldName = 'CODFIN'
      Size = 1
    end
  end
  object QrR11: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _r11')
    Left = 740
    Top = 212
    object QrR11TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrR11LOGRADOURO: TWideStringField
      FieldName = 'LOGRADOURO'
      Size = 34
    end
    object QrR11NUMERO: TWideStringField
      FieldName = 'NUMERO'
      Size = 5
    end
    object QrR11COMPLEMENT: TWideStringField
      FieldName = 'COMPLEMENT'
      Size = 22
    end
    object QrR11BAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 15
    end
    object QrR11CEP: TWideStringField
      FieldName = 'CEP'
      Size = 8
    end
    object QrR11CONTATO: TWideStringField
      FieldName = 'CONTATO'
      Size = 28
    end
    object QrR11TELEFONE: TWideStringField
      FieldName = 'TELEFONE'
      Size = 12
    end
  end
  object QrSintegra11: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra11')
    Left = 740
    Top = 184
    object QrSintegra11TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSintegra11LOGRADOURO: TWideStringField
      FieldName = 'LOGRADOURO'
      Size = 34
    end
    object QrSintegra11NUMERO: TWideStringField
      FieldName = 'NUMERO'
      Size = 5
    end
    object QrSintegra11COMPLEMENT: TWideStringField
      FieldName = 'COMPLEMENT'
      Size = 22
    end
    object QrSintegra11BAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 15
    end
    object QrSintegra11CEP: TWideStringField
      FieldName = 'CEP'
      Size = 8
    end
    object QrSintegra11CONTATO: TWideStringField
      FieldName = 'CONTATO'
      Size = 28
    end
    object QrSintegra11TELEFONE: TWideStringField
      FieldName = 'TELEFONE'
      Size = 12
    end
  end
  object DsSintegra11: TDataSource
    DataSet = QrSintegra11
    Left = 768
    Top = 184
  end
  object DsR11: TDataSource
    DataSet = QrR11
    Left = 768
    Top = 212
  end
  object QrR50: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _R50'
      'ORDER BY EMISSAO, NUMNF, Serie, CNPJ')
    Left = 676
    Top = 272
    object QrR50TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrR50CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrR50INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrR50EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrR50UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrR50MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrR50SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrR50NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrR50CODNATOP: TWideStringField
      FieldName = 'CODNATOP'
      Size = 4
    end
    object QrR50EMITENTE: TWideStringField
      FieldName = 'EMITENTE'
      Size = 1
    end
    object QrR50VALORTOT: TWideStringField
      FieldName = 'VALORTOT'
      Size = 13
    end
    object QrR50BASEICMS: TWideStringField
      FieldName = 'BASEICMS'
      Size = 13
    end
    object QrR50VALORICMS: TWideStringField
      FieldName = 'VALORICMS'
      Size = 13
    end
    object QrR50VLRISENTO: TWideStringField
      FieldName = 'VLRISENTO'
      Size = 13
    end
    object QrR50OUTROS: TWideStringField
      FieldName = 'OUTROS'
      Size = 13
    end
    object QrR50ALIQUOTA: TWideStringField
      FieldName = 'ALIQUOTA'
      Size = 4
    end
    object QrR50SITUACAO: TWideStringField
      FieldName = 'SITUACAO'
      Size = 1
    end
  end
  object QrSintegra50: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra50'
      'ORDER BY EMISSAO, NUMNF, Serie, CNPJ')
    Left = 676
    Top = 244
    object QrSintegra50TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSintegra50CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSintegra50INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrSintegra50EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrSintegra50UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSintegra50MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrSintegra50SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrSintegra50NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrSintegra50CODNATOP: TWideStringField
      FieldName = 'CODNATOP'
      Size = 4
    end
    object QrSintegra50EMITENTE: TWideStringField
      FieldName = 'EMITENTE'
      Size = 1
    end
    object QrSintegra50VALORTOT: TWideStringField
      FieldName = 'VALORTOT'
      Size = 13
    end
    object QrSintegra50BASEICMS: TWideStringField
      FieldName = 'BASEICMS'
      Size = 13
    end
    object QrSintegra50VALORICMS: TWideStringField
      FieldName = 'VALORICMS'
      Size = 13
    end
    object QrSintegra50VLRISENTO: TWideStringField
      FieldName = 'VLRISENTO'
      Size = 13
    end
    object QrSintegra50OUTROS: TWideStringField
      FieldName = 'OUTROS'
      Size = 13
    end
    object QrSintegra50ALIQUOTA: TWideStringField
      FieldName = 'ALIQUOTA'
      Size = 4
    end
    object QrSintegra50SITUACAO: TWideStringField
      FieldName = 'SITUACAO'
      Size = 1
    end
  end
  object DsSintegra50: TDataSource
    DataSet = QrSintegra50
    Left = 704
    Top = 244
  end
  object DsR50: TDataSource
    DataSet = QrR50
    Left = 704
    Top = 272
  end
  object QrR51: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _R51'
      'ORDER BY EMISSAO, NUMNF, Serie, CNPJ')
    Left = 740
    Top = 272
    object QrR51TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrR51CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrR51INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrR51EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrR51UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrR51SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrR51NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrR51CODNATOP: TWideStringField
      FieldName = 'CODNATOP'
      Size = 4
    end
    object QrR51TOTALNF: TWideStringField
      FieldName = 'TOTALNF'
      Size = 13
    end
    object QrR51VLRTOTIPI: TWideStringField
      FieldName = 'VLRTOTIPI'
      Size = 13
    end
    object QrR51VLRISENTO: TWideStringField
      FieldName = 'VLRISENTO'
      Size = 13
    end
    object QrR51OUTROS: TWideStringField
      FieldName = 'OUTROS'
      Size = 13
    end
    object QrR51BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
    end
    object QrR51SITUACAO: TWideStringField
      FieldName = 'SITUACAO'
      Size = 1
    end
  end
  object QrSintegra51: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra51'
      'ORDER BY EMISSAO, NUMNF, Serie, CNPJ')
    Left = 740
    Top = 244
    object QrSintegra51TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSintegra51CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSintegra51INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrSintegra51EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrSintegra51UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSintegra51SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrSintegra51NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrSintegra51CODNATOP: TWideStringField
      FieldName = 'CODNATOP'
      Size = 4
    end
    object QrSintegra51TOTALNF: TWideStringField
      FieldName = 'TOTALNF'
      Size = 13
    end
    object QrSintegra51VLRTOTIPI: TWideStringField
      FieldName = 'VLRTOTIPI'
      Size = 13
    end
    object QrSintegra51VLRISENTO: TWideStringField
      FieldName = 'VLRISENTO'
      Size = 13
    end
    object QrSintegra51OUTROS: TWideStringField
      FieldName = 'OUTROS'
      Size = 13
    end
    object QrSintegra51BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
    end
    object QrSintegra51SITUACAO: TWideStringField
      FieldName = 'SITUACAO'
      Size = 1
    end
  end
  object DsSintegra51: TDataSource
    DataSet = QrSintegra51
    Left = 768
    Top = 244
  end
  object DsR51: TDataSource
    DataSet = QrR51
    Left = 768
    Top = 272
  end
  object QrSintegra54: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra54'
      'ORDER BY NUMNF, Serie, CNPJ, NUMITEM')
    Left = 868
    Top = 244
    object QrSintegra54TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSintegra54CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSintegra54MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrSintegra54SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrSintegra54NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrSintegra54CODNATOP: TWideStringField
      FieldName = 'CODNATOP'
      Size = 4
    end
    object QrSintegra54SITTRIB: TWideStringField
      FieldName = 'SITTRIB'
      Size = 3
    end
    object QrSintegra54NUMITEM: TWideStringField
      FieldName = 'NUMITEM'
      Size = 3
    end
    object QrSintegra54CODPROD: TWideStringField
      FieldName = 'CODPROD'
      Size = 14
    end
    object QrSintegra54QTDADE: TWideStringField
      FieldName = 'QTDADE'
      Size = 11
    end
    object QrSintegra54VLRPROD: TWideStringField
      FieldName = 'VLRPROD'
      Size = 12
    end
    object QrSintegra54VLRDESC: TWideStringField
      FieldName = 'VLRDESC'
      Size = 12
    end
    object QrSintegra54BASECALC: TWideStringField
      FieldName = 'BASECALC'
      Size = 12
    end
    object QrSintegra54BASESUBTRI: TWideStringField
      FieldName = 'BASESUBTRI'
      Size = 12
    end
    object QrSintegra54VLRIPI: TWideStringField
      FieldName = 'VLRIPI'
      Size = 12
    end
    object QrSintegra54ALIQICMS: TWideStringField
      FieldName = 'ALIQICMS'
      Size = 4
    end
  end
  object QrR54: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _r54'
      'ORDER BY NUMNF, Serie, CNPJ, NUMITEM')
    Left = 868
    Top = 272
    object QrR54TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrR54CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrR54MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrR54SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrR54NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrR54CODNATOP: TWideStringField
      FieldName = 'CODNATOP'
      Size = 4
    end
    object QrR54SITTRIB: TWideStringField
      FieldName = 'SITTRIB'
      Size = 3
    end
    object QrR54NUMITEM: TWideStringField
      FieldName = 'NUMITEM'
      Size = 3
    end
    object QrR54CODPROD: TWideStringField
      FieldName = 'CODPROD'
      Size = 14
    end
    object QrR54QTDADE: TWideStringField
      FieldName = 'QTDADE'
      Size = 11
    end
    object QrR54VLRPROD: TWideStringField
      FieldName = 'VLRPROD'
      Size = 12
    end
    object QrR54VLRDESC: TWideStringField
      FieldName = 'VLRDESC'
      Size = 12
    end
    object QrR54BASECALC: TWideStringField
      FieldName = 'BASECALC'
      Size = 12
    end
    object QrR54BASESUBTRI: TWideStringField
      FieldName = 'BASESUBTRI'
      Size = 12
    end
    object QrR54VLRIPI: TWideStringField
      FieldName = 'VLRIPI'
      Size = 12
    end
    object QrR54ALIQICMS: TWideStringField
      FieldName = 'ALIQICMS'
      Size = 4
    end
  end
  object DsR54: TDataSource
    DataSet = QrR54
    Left = 896
    Top = 272
  end
  object DsSintegra54: TDataSource
    DataSet = QrSintegra54
    Left = 896
    Top = 244
  end
  object QrR53: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _r53'
      'ORDER BY EMISSAO, NUMERO, Serie, CNPJ')
    Left = 804
    Top = 272
    object QrR53TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrR53CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrR53INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrR53EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrR53UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrR53MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrR53SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrR53NUMERO: TWideStringField
      FieldName = 'NUMERO'
      Size = 6
    end
    object QrR53CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrR53EMITENTE: TWideStringField
      FieldName = 'EMITENTE'
      Size = 1
    end
    object QrR53BASECALC: TWideStringField
      FieldName = 'BASECALC'
      Size = 13
    end
    object QrR53ICMSRETIDO: TWideStringField
      FieldName = 'ICMSRETIDO'
      Size = 13
    end
    object QrR53DESPACESS: TWideStringField
      FieldName = 'DESPACESS'
      Size = 13
    end
    object QrR53SITUACAO: TWideStringField
      FieldName = 'SITUACAO'
      Size = 1
    end
    object QrR53CODANTEC: TWideStringField
      FieldName = 'CODANTEC'
      Size = 1
    end
    object QrR53BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
      Size = 16
    end
  end
  object DsR53: TDataSource
    DataSet = QrR53
    Left = 832
    Top = 272
  end
  object DsSintegra53: TDataSource
    DataSet = QrSintegra53
    Left = 832
    Top = 244
  end
  object QrSintegra53: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra53'
      'ORDER BY EMISSAO, NUMERO, Serie, CNPJ')
    Left = 804
    Top = 244
    object QrSintegra53TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSintegra53CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSintegra53INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrSintegra53EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrSintegra53UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSintegra53MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrSintegra53SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrSintegra53NUMERO: TWideStringField
      FieldName = 'NUMERO'
      Size = 6
    end
    object QrSintegra53CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrSintegra53EMITENTE: TWideStringField
      FieldName = 'EMITENTE'
      Size = 1
    end
    object QrSintegra53BASECALC: TWideStringField
      FieldName = 'BASECALC'
      Size = 13
    end
    object QrSintegra53ICMSRETIDO: TWideStringField
      FieldName = 'ICMSRETIDO'
      Size = 13
    end
    object QrSintegra53DESPACESS: TWideStringField
      FieldName = 'DESPACESS'
      Size = 13
    end
    object QrSintegra53SITUACAO: TWideStringField
      FieldName = 'SITUACAO'
      Size = 1
    end
    object QrSintegra53CODANTEC: TWideStringField
      FieldName = 'CODANTEC'
      Size = 1
    end
    object QrSintegra53BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
      Size = 29
    end
  end
  object QrSintegra70: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra70'
      'ORDER BY EMISSAO, NUMNF, Serie, CNPJ')
    Left = 676
    Top = 304
    object QrSintegra70TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSintegra70CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSintegra70INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrSintegra70EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrSintegra70UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSintegra70MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrSintegra70SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 1
    end
    object QrSintegra70SUBSERIE: TWideStringField
      FieldName = 'SUBSERIE'
      Size = 2
    end
    object QrSintegra70NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrSintegra70CODNATOP: TWideStringField
      FieldName = 'CODNATOP'
      Size = 4
    end
    object QrSintegra70VALORTOT: TWideStringField
      FieldName = 'VALORTOT'
      Size = 13
    end
    object QrSintegra70BASEICMS: TWideStringField
      FieldName = 'BASEICMS'
      Size = 14
    end
    object QrSintegra70VALORICMS: TWideStringField
      FieldName = 'VALORICMS'
      Size = 14
    end
    object QrSintegra70VLRISENTO: TWideStringField
      FieldName = 'VLRISENTO'
      Size = 14
    end
    object QrSintegra70OUTROS: TWideStringField
      FieldName = 'OUTROS'
      Size = 14
    end
    object QrSintegra70CIF_FOB: TWideStringField
      FieldName = 'CIF_FOB'
      Size = 1
    end
    object QrSintegra70SITUACAO: TWideStringField
      FieldName = 'SITUACAO'
      Size = 1
    end
  end
  object QrR70: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _r70'
      'ORDER BY EMISSAO, NUMNF, Serie, CNPJ')
    Left = 676
    Top = 332
    object QrR70TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrR70CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrR70INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrR70EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrR70UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrR70MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrR70SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 1
    end
    object QrR70SUBSERIE: TWideStringField
      FieldName = 'SUBSERIE'
      Size = 2
    end
    object QrR70NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrR70CODNATOP: TWideStringField
      FieldName = 'CODNATOP'
      Size = 4
    end
    object QrR70VALORTOT: TWideStringField
      FieldName = 'VALORTOT'
      Size = 13
    end
    object QrR70BASEICMS: TWideStringField
      FieldName = 'BASEICMS'
      Size = 14
    end
    object QrR70VALORICMS: TWideStringField
      FieldName = 'VALORICMS'
      Size = 14
    end
    object QrR70VLRISENTO: TWideStringField
      FieldName = 'VLRISENTO'
      Size = 14
    end
    object QrR70OUTROS: TWideStringField
      FieldName = 'OUTROS'
      Size = 14
    end
    object QrR70CIF_FOB: TWideStringField
      FieldName = 'CIF_FOB'
      Size = 1
    end
    object QrR70SITUACAO: TWideStringField
      FieldName = 'SITUACAO'
      Size = 1
    end
  end
  object DsSintegra70: TDataSource
    DataSet = QrSintegra70
    Left = 704
    Top = 304
  end
  object DsR70: TDataSource
    DataSet = QrR70
    Left = 704
    Top = 332
  end
  object QrR71: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _r71'
      'ORDER BY EMISSAOC, NUMEROC, SerieC, CNPJ')
    Left = 740
    Top = 332
    object QrR71TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrR71CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrR71INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrR71EMISSAOC: TDateField
      FieldName = 'EMISSAOC'
    end
    object QrR71UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrR71MODELOC: TWideStringField
      FieldName = 'MODELOC'
      Size = 2
    end
    object QrR71SERIEC: TWideStringField
      FieldName = 'SERIEC'
      Size = 1
    end
    object QrR71SUBSERIE: TWideStringField
      FieldName = 'SUBSERIE'
      Size = 2
    end
    object QrR71NUMEROC: TWideStringField
      FieldName = 'NUMEROC'
      Size = 6
    end
    object QrR71UFREM: TWideStringField
      FieldName = 'UFREM'
      Size = 2
    end
    object QrR71CNPJREM: TWideStringField
      FieldName = 'CNPJREM'
      Size = 14
    end
    object QrR71INSCESTREM: TWideStringField
      FieldName = 'INSCESTREM'
      Size = 14
    end
    object QrR71EMISSAON: TDateField
      FieldName = 'EMISSAON'
    end
    object QrR71MODELON: TWideStringField
      FieldName = 'MODELON'
      Size = 2
    end
    object QrR71SERIEN: TWideStringField
      FieldName = 'SERIEN'
      Size = 3
    end
    object QrR71NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrR71TOTALNF: TWideStringField
      FieldName = 'TOTALNF'
      Size = 14
    end
    object QrR71BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
      Size = 12
    end
  end
  object QrSintegra71: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra71'
      'ORDER BY EMISSAOC, NUMEROC, SerieC, CNPJ')
    Left = 740
    Top = 304
    object QrSintegra71TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSintegra71CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSintegra71INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrSintegra71EMISSAOC: TDateField
      FieldName = 'EMISSAOC'
    end
    object QrSintegra71UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSintegra71MODELOC: TWideStringField
      FieldName = 'MODELOC'
      Size = 2
    end
    object QrSintegra71SERIEC: TWideStringField
      FieldName = 'SERIEC'
      Size = 1
    end
    object QrSintegra71SUBSERIE: TWideStringField
      FieldName = 'SUBSERIE'
      Size = 2
    end
    object QrSintegra71NUMEROC: TWideStringField
      FieldName = 'NUMEROC'
      Size = 6
    end
    object QrSintegra71UFREM: TWideStringField
      FieldName = 'UFREM'
      Size = 2
    end
    object QrSintegra71CNPJREM: TWideStringField
      FieldName = 'CNPJREM'
      Size = 14
    end
    object QrSintegra71INSCESTREM: TWideStringField
      FieldName = 'INSCESTREM'
      Size = 14
    end
    object QrSintegra71EMISSAON: TDateField
      FieldName = 'EMISSAON'
    end
    object QrSintegra71MODELON: TWideStringField
      FieldName = 'MODELON'
      Size = 2
    end
    object QrSintegra71SERIEN: TWideStringField
      FieldName = 'SERIEN'
      Size = 3
    end
    object QrSintegra71NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrSintegra71TOTALNF: TWideStringField
      FieldName = 'TOTALNF'
      Size = 14
    end
    object QrSintegra71BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
      Size = 12
    end
  end
  object DsSintegra71: TDataSource
    DataSet = QrSintegra71
    Left = 768
    Top = 304
  end
  object DsR71: TDataSource
    DataSet = QrR71
    Left = 768
    Top = 332
  end
  object QrR74: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _r74'
      'ORDER BY DATA, CNPJ, CODIPROD')
    Left = 804
    Top = 332
    object QrR74TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrR74DATA: TDateField
      FieldName = 'DATA'
    end
    object QrR74CODIPROD: TWideStringField
      FieldName = 'CODIPROD'
      Size = 14
    end
    object QrR74QTDADE: TWideStringField
      FieldName = 'QTDADE'
      Size = 13
    end
    object QrR74VLRPROD: TWideStringField
      FieldName = 'VLRPROD'
      Size = 13
    end
    object QrR74CODPOSSEM: TWideStringField
      FieldName = 'CODPOSSEM'
      Size = 1
    end
    object QrR74CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrR74INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrR74UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrR74BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
      Size = 45
    end
  end
  object QrSintegra74: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra74'
      'ORDER BY DATA, CNPJ, CODIPROD')
    Left = 804
    Top = 304
    object QrSintegra74TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSintegra74DATA: TDateField
      FieldName = 'DATA'
    end
    object QrSintegra74CODIPROD: TWideStringField
      FieldName = 'CODIPROD'
      Size = 14
    end
    object QrSintegra74QTDADE: TWideStringField
      FieldName = 'QTDADE'
      Size = 13
    end
    object QrSintegra74VLRPROD: TWideStringField
      FieldName = 'VLRPROD'
      Size = 13
    end
    object QrSintegra74CODPOSSEM: TWideStringField
      FieldName = 'CODPOSSEM'
      Size = 1
    end
    object QrSintegra74CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSintegra74INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrSintegra74UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSintegra74BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
      Size = 45
    end
  end
  object DsSintegra74: TDataSource
    DataSet = QrSintegra74
    Left = 832
    Top = 304
  end
  object DsR74: TDataSource
    DataSet = QrR74
    Left = 832
    Top = 332
  end
  object QrR75: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _r75'
      '/*'
      'ORDER BY DATAINI, DATAFIN, CODPROD,'
      'DESCRICAO, CODNCM, UNIDADE'
      '*/'
      'ORDER BY DATAINI, DESCRICAO'
      '')
    Left = 868
    Top = 332
    object QrR75TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrR75DATAINI: TDateField
      FieldName = 'DATAINI'
    end
    object QrR75DATAFIN: TDateField
      FieldName = 'DATAFIN'
    end
    object QrR75CODPROD: TWideStringField
      FieldName = 'CODPROD'
      Size = 14
    end
    object QrR75CODNCM: TWideStringField
      FieldName = 'CODNCM'
      Size = 8
    end
    object QrR75DESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Size = 53
    end
    object QrR75UNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 6
    end
    object QrR75ALIQIPI: TWideStringField
      FieldName = 'ALIQIPI'
      Size = 5
    end
    object QrR75ALIQICMS: TWideStringField
      FieldName = 'ALIQICMS'
      Size = 4
    end
    object QrR75REDBASEICM: TWideStringField
      FieldName = 'REDBASEICM'
      Size = 5
    end
    object QrR75BASESUBTRI: TWideStringField
      FieldName = 'BASESUBTRI'
      Size = 13
    end
  end
  object QrSintegra75: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra75'
      '/*'
      'ORDER BY DATAINI, DATAFIN, CODPROD,'
      'DESCRICAO, CODNCM, UNIDADE'
      '*/'
      'ORDER BY DATAINI, DESCRICAO'
      ''
      '')
    Left = 868
    Top = 304
    object QrSintegra75TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSintegra75DATAINI: TDateField
      FieldName = 'DATAINI'
    end
    object QrSintegra75DATAFIN: TDateField
      FieldName = 'DATAFIN'
    end
    object QrSintegra75CODPROD: TWideStringField
      FieldName = 'CODPROD'
      Size = 14
    end
    object QrSintegra75CODNCM: TWideStringField
      FieldName = 'CODNCM'
      Size = 8
    end
    object QrSintegra75DESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Size = 53
    end
    object QrSintegra75UNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 6
    end
    object QrSintegra75ALIQIPI: TWideStringField
      FieldName = 'ALIQIPI'
      Size = 5
    end
    object QrSintegra75ALIQICMS: TWideStringField
      FieldName = 'ALIQICMS'
      Size = 4
    end
    object QrSintegra75REDBASEICM: TWideStringField
      FieldName = 'REDBASEICM'
      Size = 5
    end
    object QrSintegra75BASESUBTRI: TWideStringField
      FieldName = 'BASESUBTRI'
      Size = 13
    end
  end
  object DsSintegra75: TDataSource
    DataSet = QrSintegra75
    Left = 896
    Top = 304
  end
  object DsR75: TDataSource
    DataSet = QrR75
    Left = 896
    Top = 332
  end
  object QrSintegra85: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra85'
      'ORDER BY DATADECLAR, DECLARACAO')
    Left = 676
    Top = 364
    object QrSintegra85TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSintegra85DECLARACAO: TWideStringField
      FieldName = 'DECLARACAO'
      Size = 11
    end
    object QrSintegra85DATADECLAR: TDateField
      FieldName = 'DATADECLAR'
    end
    object QrSintegra85NATUREZA: TWideStringField
      FieldName = 'NATUREZA'
      Size = 1
    end
    object QrSintegra85REGISTRO: TWideStringField
      FieldName = 'REGISTRO'
      Size = 12
    end
    object QrSintegra85DATAREG: TDateField
      FieldName = 'DATAREG'
    end
    object QrSintegra85CONHECIM: TWideStringField
      FieldName = 'CONHECIM'
      Size = 16
    end
    object QrSintegra85DATACONHEC: TDateField
      FieldName = 'DATACONHEC'
    end
    object QrSintegra85TIPOCONHEC: TWideStringField
      FieldName = 'TIPOCONHEC'
      Size = 2
    end
    object QrSintegra85PAIS: TWideStringField
      FieldName = 'PAIS'
      Size = 4
    end
    object QrSintegra85RESERVADO: TWideStringField
      FieldName = 'RESERVADO'
      Size = 8
    end
    object QrSintegra85DATAAVERB: TDateField
      FieldName = 'DATAAVERB'
    end
    object QrSintegra85NUMNFEXP: TWideStringField
      FieldName = 'NUMNFEXP'
      Size = 6
    end
    object QrSintegra85EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrSintegra85MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrSintegra85SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrSintegra85BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
      Size = 19
    end
  end
  object DsSintegra85: TDataSource
    DataSet = QrSintegra85
    Left = 704
    Top = 364
  end
  object QrR85: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _r85'
      'ORDER BY DATADECLAR, DECLARACAO')
    Left = 676
    Top = 392
    object QrR85TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrR85DECLARACAO: TWideStringField
      FieldName = 'DECLARACAO'
      Size = 11
    end
    object QrR85DATADECLAR: TDateField
      FieldName = 'DATADECLAR'
    end
    object QrR85NATUREZA: TWideStringField
      FieldName = 'NATUREZA'
      Size = 1
    end
    object QrR85REGISTRO: TWideStringField
      FieldName = 'REGISTRO'
      Size = 12
    end
    object QrR85DATAREG: TDateField
      FieldName = 'DATAREG'
    end
    object QrR85CONHECIM: TWideStringField
      FieldName = 'CONHECIM'
      Size = 16
    end
    object QrR85DATACONHEC: TDateField
      FieldName = 'DATACONHEC'
    end
    object QrR85TIPOCONHEC: TWideStringField
      FieldName = 'TIPOCONHEC'
      Size = 2
    end
    object QrR85PAIS: TWideStringField
      FieldName = 'PAIS'
      Size = 4
    end
    object QrR85RESERVADO: TWideStringField
      FieldName = 'RESERVADO'
      Size = 8
    end
    object QrR85DATAAVERB: TDateField
      FieldName = 'DATAAVERB'
    end
    object QrR85NUMNFEXP: TWideStringField
      FieldName = 'NUMNFEXP'
      Size = 6
    end
    object QrR85EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrR85MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrR85SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrR85BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
      Size = 19
    end
  end
  object DsR85: TDataSource
    DataSet = QrR85
    Left = 704
    Top = 392
  end
  object QrSintegra86: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra86'
      'ORDER BY DATAREG, CNPJREM, NUMNF')
    Left = 740
    Top = 364
    object QrSintegra86TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSintegra86REGEXPORT: TWideStringField
      FieldName = 'REGEXPORT'
      Size = 12
    end
    object QrSintegra86DATAREG: TDateField
      FieldName = 'DATAREG'
    end
    object QrSintegra86CNPJREM: TWideStringField
      FieldName = 'CNPJREM'
      Size = 14
    end
    object QrSintegra86INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrSintegra86UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSintegra86NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrSintegra86EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrSintegra86MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrSintegra86SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrSintegra86CODPROD: TWideStringField
      FieldName = 'CODPROD'
      Size = 14
    end
    object QrSintegra86QTDADE: TWideStringField
      FieldName = 'QTDADE'
      Size = 11
    end
    object QrSintegra86VLRUNIT: TWideStringField
      FieldName = 'VLRUNIT'
      Size = 12
    end
    object QrSintegra86VLRPROD: TWideStringField
      FieldName = 'VLRPROD'
      Size = 12
    end
    object QrSintegra86RELACIONA: TWideStringField
      FieldName = 'RELACIONA'
      Size = 1
    end
    object QrSintegra86BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
      Size = 5
    end
  end
  object DsSintegra86: TDataSource
    DataSet = QrSintegra86
    Left = 768
    Top = 364
  end
  object DsR86: TDataSource
    DataSet = QrR86
    Left = 768
    Top = 392
  end
  object QrR86: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _r86'
      'ORDER BY DATAREG, CNPJREM, NUMNF')
    Left = 740
    Top = 392
    object QrR86TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrR86REGEXPORT: TWideStringField
      FieldName = 'REGEXPORT'
      Size = 12
    end
    object QrR86DATAREG: TDateField
      FieldName = 'DATAREG'
    end
    object QrR86CNPJREM: TWideStringField
      FieldName = 'CNPJREM'
      Size = 14
    end
    object QrR86INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrR86UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrR86NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrR86EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrR86MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrR86SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrR86CODPROD: TWideStringField
      FieldName = 'CODPROD'
      Size = 14
    end
    object QrR86QTDADE: TWideStringField
      FieldName = 'QTDADE'
      Size = 11
    end
    object QrR86VLRUNIT: TWideStringField
      FieldName = 'VLRUNIT'
      Size = 12
    end
    object QrR86VLRPROD: TWideStringField
      FieldName = 'VLRPROD'
      Size = 12
    end
    object QrR86RELACIONA: TWideStringField
      FieldName = 'RELACIONA'
      Size = 1
    end
    object QrR86BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
      Size = 5
    end
  end
  object QrSintegra88: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra88'
      'ORDER BY CNPJ, MODELO, SERIE, NUMERO')
    Left = 804
    Top = 364
    object QrSintegra88TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrSintegra88SUBTIPO: TWideStringField
      FieldName = 'SUBTIPO'
      Size = 3
    end
    object QrSintegra88CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrSintegra88MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrSintegra88SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrSintegra88NUMERO: TWideStringField
      FieldName = 'NUMERO'
      Size = 6
    end
    object QrSintegra88CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrSintegra88CST: TWideStringField
      FieldName = 'CST'
      Size = 3
    end
    object QrSintegra88NUMITEM: TWideStringField
      FieldName = 'NUMITEM'
      Size = 3
    end
    object QrSintegra88CODPROD: TWideStringField
      FieldName = 'CODPROD'
      Size = 14
    end
    object QrSintegra88NUMSER: TWideStringField
      FieldName = 'NUMSER'
    end
    object QrSintegra88BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
      Size = 52
    end
  end
  object QrR88: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _r88'
      'ORDER BY CNPJ, MODELO, SERIE, NUMERO')
    Left = 804
    Top = 392
    object QrR88TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrR88SUBTIPO: TWideStringField
      FieldName = 'SUBTIPO'
      Size = 3
    end
    object QrR88CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrR88MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrR88SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrR88NUMERO: TWideStringField
      FieldName = 'NUMERO'
      Size = 6
    end
    object QrR88CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrR88CST: TWideStringField
      FieldName = 'CST'
      Size = 3
    end
    object QrR88NUMITEM: TWideStringField
      FieldName = 'NUMITEM'
      Size = 3
    end
    object QrR88CODPROD: TWideStringField
      FieldName = 'CODPROD'
      Size = 14
    end
    object QrR88NUMSER: TWideStringField
      FieldName = 'NUMSER'
    end
    object QrR88BRANCOS: TWideStringField
      FieldName = 'BRANCOS'
      Size = 52
    end
  end
  object DsR88: TDataSource
    DataSet = QrR88
    Left = 832
    Top = 392
  end
  object DsSintegra88: TDataSource
    DataSet = QrSintegra88
    Left = 832
    Top = 364
  end
  object QrSintegra90: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM sintegra90')
    Left = 868
    Top = 364
  end
  object DsSintegra90: TDataSource
    DataSet = QrSintegra90
    Left = 896
    Top = 364
  end
  object DsR90: TDataSource
    DataSet = QrR90
    Left = 896
    Top = 392
  end
  object QrR90: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _r90')
    Left = 868
    Top = 392
  end
  object QrTotN: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(itsi.prod_vProd) prod_vProd,'
      
        'SUM(itsi.prod_vProd + itsi.prod_vFrete + itsi.prod_vSeg + itsi.p' +
        'rod_vDesc + itsi.prod_vOutro) prod_vNF,'
      'SUM(itsn.ICMS_vBC) ICMS_vBC,'
      'SUM(itsn.ICMS_vICMS) ICMS_vICMS'
      'FROM nfeitsi itsi'
      'LEFT JOIN nfeitsn itsn ON '
      '      itsi.FatID=itsn.FatID'
      '  AND itsi.FatNum=itsn.FatNum'
      '  AND itsi.Empresa=itsn.Empresa'
      '  AND itsi.nItem=itsn.nItem'
      'WHERE itsi.FatID=:P0'
      'AND itsi.FatNum=:P1'
      'AND itsi.Empresa=:P2')
    Left = 88
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrTotNprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrTotNprod_vNF: TFloatField
      FieldName = 'prod_vNF'
    end
    object QrTotNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrTotNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
  end
  object QrNatOper: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodEnt '
      'FROM natoper'
      'WHERE Codigo=:P0'
      'OR Codigo=:P1')
    Left = 676
    Top = 456
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNatOperCodEnt: TWideStringField
      FieldName = 'CodEnt'
      Size = 6
    end
  end
  object QrDifere: TmySQLQuery
    Database = Dmod.ZZDB
    AfterOpen = QrDifereAfterOpen
    SQL.Strings = (
      'SELECT * '
      'FROM sintegraer')
    Left = 676
    Top = 428
    object QrDifereTipoReg: TSmallintField
      FieldName = 'TipoReg'
    end
    object QrDifereItemReg: TIntegerField
      FieldName = 'ItemReg'
    end
    object QrDifereCampo: TWideStringField
      FieldName = 'Campo'
    end
    object QrDifereValMy: TWideStringField
      FieldName = 'ValMy'
      Size = 255
    end
    object QrDifereValCo: TWideStringField
      FieldName = 'ValCo'
      Size = 255
    end
    object QrDifereAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsDifere: TDataSource
    DataSet = QrDifere
    Left = 704
    Top = 428
  end
  object PmExclCo: TPopupMenu
    Left = 140
    Top = 592
    object Excluiitematualdaminhatabela1: TMenuItem
      Caption = 'Exclui item atual da minha tabela'
      OnClick = Excluiitematualdaminhatabela1Click
    end
  end
  object QrLocNFa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.IDCtrl,'
      'nfa.ide_dEmi, nfa.emit_xNome, nfa.ICMSTot_vProd,'
      'nfa.ICMSTot_vNF'
      'FROM nfecaba nfa'
      'LEFT JOIN entidades ent ON ent.Codigo=nfa.CodInfoEmit'
      'WHERE nfa.ide_nNF=:P0'
      'AND IF(ent.Tipo=0, ent.CNPJ, ent.CPF)=:P1'
      '')
    Left = 704
    Top = 456
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocNFaFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLocNFaFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrLocNFaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrLocNFaIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrLocNFaide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrLocNFaemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrLocNFaICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrLocNFaICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
  end
  object QrTotO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(itsi.prod_vProd) prod_vProd,'
      
        'SUM(itsi.prod_vProd + itsi.prod_vFrete + itsi.prod_vSeg + itsi.p' +
        'rod_vDesc + itsi.prod_vOutro) prod_vNF,'
      'SUM(IF(itso.IPI_vIPI, itso.IPI_vBC, 0)) IPI_vBC,'
      'SUM(itso.IPI_vIPI) IPI_vIPI'
      'FROM nfeitsi itsi'
      'LEFT JOIN nfeitsn itsn ON '
      '      itsi.FatID=itsn.FatID'
      '  AND itsi.FatNum=itsn.FatNum'
      '  AND itsi.Empresa=itsn.Empresa'
      '  AND itsi.nItem=itsn.nItem'
      'LEFT JOIN nfeitso itso ON'
      '      itsn.FatID=itso.FatID'
      '  AND itsn.FatNum=itso.FatNum'
      '  AND itsn.Empresa=itso.Empresa'
      '  AND itsn.nItem=itso.nItem'
      'WHERE itsi.FatID=:P0'
      'AND itsi.FatNum=:P1'
      'AND itsi.Empresa=:P2')
    Left = 116
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrTotOprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrTotOprod_vNF: TFloatField
      FieldName = 'prod_vNF'
    end
    object QrTotOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrTotOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
  end
  object DsCampos: TDataSource
    DataSet = TbCampos
    Left = 760
    Top = 428
  end
  object TbCampos: TmySQLTable
    Database = Dmod.ZZDB
    TableName = 'sintegraec'
    Left = 732
    Top = 428
    object TbCamposAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object TbCamposCampo: TWideStringField
      FieldName = 'Campo'
    end
  end
  object QrLoad_S50: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM load_s50')
    Left = 44
    Top = 248
    object QrLoad_S50TIPO: TSmallintField
      FieldName = 'TIPO'
    end
    object QrLoad_S50CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrLoad_S50IE: TWideStringField
      FieldName = 'IE'
      Size = 14
    end
    object QrLoad_S50EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrLoad_S50UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrLoad_S50MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrLoad_S50SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrLoad_S50NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrLoad_S50CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrLoad_S50EMITENTE: TWideStringField
      FieldName = 'EMITENTE'
      Size = 1
    end
    object QrLoad_S50ValorNF: TFloatField
      FieldName = 'ValorNF'
    end
    object QrLoad_S50BCICMS: TFloatField
      FieldName = 'BCICMS'
    end
    object QrLoad_S50ValICMS: TFloatField
      FieldName = 'ValICMS'
    end
    object QrLoad_S50ValIsento: TFloatField
      FieldName = 'ValIsento'
    end
    object QrLoad_S50ValNCICMS: TFloatField
      FieldName = 'ValNCICMS'
    end
    object QrLoad_S50AliqICMS: TFloatField
      FieldName = 'AliqICMS'
    end
    object QrLoad_S50Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrLoad_S50Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrLocNF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, IDCtrl'
      'FROM nfecaba'
      'WHERE FatID IN (51,151)'
      'AND Empresa=:P0'
      'AND ide_mod=:P1'
      'AND ide_serie=:P2'
      'AND ide_nNF=:P3'
      'AND CodinfoEmit=:P4')
    Left = 44
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrLocNFFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLocNFFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrLocNFEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrLocNFIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object QrLocGGX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, gg1.PrdGrupTip '
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE ggx.Controle =:P0')
    Left = 72
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocGGXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrLocGGXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocGGXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrLocGGXPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
  end
  object QrLocGG1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.PrdGrupTip'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE gg1.Nivel1 =:P0')
    Left = 100
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocGG1GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrLocGG1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocGG1NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrLocGG1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
  end
  object QrLocPrdNom: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_CodUsu, ggx.GraGru1, ggx.Controle GraGruX, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.PrdGrupTip'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE gg1.Nome = :P0')
    Left = 128
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPrdNomGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocPrdNomGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
  end
  object QrLocPrdTmp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_CodUsu, ggx.GraGru1, ggx.Controle GraGruX, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, gg1.PrdGrupTip'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE gg1.NomeTemp = :P0')
    Left = 156
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPrdTmpCU_CodUsu: TIntegerField
      FieldName = 'CU_CodUsu'
      Required = True
    end
    object QrLocPrdTmpGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrLocPrdTmpGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocPrdTmpNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrLocPrdTmpPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
  end
  object QrLoad_S54: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM load_s54'
      'WHERE CNPJ=:P0'
      'AND Modelo=:P1'
      'AND Serie=:P2'
      'AND NUMNF=:P3')
    Left = 72
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrLoad_S54TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrLoad_S54CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrLoad_S54MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrLoad_S54SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QrLoad_S54NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrLoad_S54CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrLoad_S54CST: TWideStringField
      FieldName = 'CST'
      Size = 3
    end
    object QrLoad_S54NUMITEM: TWideStringField
      FieldName = 'NUMITEM'
      Size = 3
    end
    object QrLoad_S54CODPROD: TWideStringField
      FieldName = 'CODPROD'
      Size = 14
    end
    object QrLoad_S54QTDADE: TFloatField
      FieldName = 'QTDADE'
    end
    object QrLoad_S54VLRPROD: TFloatField
      FieldName = 'VLRPROD'
    end
    object QrLoad_S54VLRDESC: TFloatField
      FieldName = 'VLRDESC'
    end
    object QrLoad_S54BASECALC: TFloatField
      FieldName = 'BASECALC'
    end
    object QrLoad_S54BASESUBTRI: TFloatField
      FieldName = 'BASESUBTRI'
    end
    object QrLoad_S54VLRIPI: TFloatField
      FieldName = 'VLRIPI'
    end
    object QrLoad_S54ALIQICMS: TFloatField
      FieldName = 'ALIQICMS'
    end
    object QrLoad_S54Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrLoad_S75: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT *'
      'FROM load_s75'
      'WHERE CODPROD=:P0')
    Left = 100
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLoad_S75GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLoad_S75TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrLoad_S75DATAINI: TDateField
      FieldName = 'DATAINI'
    end
    object QrLoad_S75DATAFIN: TDateField
      FieldName = 'DATAFIN'
    end
    object QrLoad_S75CODPROD: TWideStringField
      FieldName = 'CODPROD'
      Size = 14
    end
    object QrLoad_S75CODNCM: TWideStringField
      FieldName = 'CODNCM'
      Size = 8
    end
    object QrLoad_S75DESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Size = 53
    end
    object QrLoad_S75UNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 6
    end
    object QrLoad_S75ALIQIPI: TFloatField
      FieldName = 'ALIQIPI'
    end
    object QrLoad_S75ALIQICMS: TFloatField
      FieldName = 'ALIQICMS'
    end
    object QrLoad_S75REDBASEICM: TFloatField
      FieldName = 'REDBASEICM'
    end
    object QrLoad_S75BASESUBTRI: TFloatField
      FieldName = 'BASESUBTRI'
    end
    object QrLoad_S75Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLoad_S75Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
  object QrSum_S54: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT SUM(VLRPROD) VLRPROD'
      'FROM load_s54'
      'WHERE CNPJ=:P0'
      'AND Modelo=:P1'
      'AND Serie=:P2'
      'AND NUMNF=:P3')
    Left = 72
    Top = 276
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSum_S54VLRPROD: TFloatField
      FieldName = 'VLRPROD'
    end
  end
  object QrSum_S50: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT SUM(ValorNF) ValorNF'
      'FROM load_s50'
      'WHERE CNPJ=:P0'
      'AND Modelo=:P1'
      'AND Serie=:P2'
      'AND NUMNF=:P3')
    Left = 44
    Top = 276
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSum_S50ValorNF: TFloatField
      FieldName = 'ValorNF'
    end
  end
  object QrLoad_S70: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT *'
      'FROM load_s70')
    Left = 44
    Top = 312
    object QrLoad_S70TIPO: TWideStringField
      FieldName = 'TIPO'
      Size = 2
    end
    object QrLoad_S70CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrLoad_S70INSCEST: TWideStringField
      FieldName = 'INSCEST'
      Size = 14
    end
    object QrLoad_S70EMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object QrLoad_S70UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrLoad_S70MODELO: TWideStringField
      FieldName = 'MODELO'
      Size = 2
    end
    object QrLoad_S70SERIE: TWideStringField
      FieldName = 'SERIE'
      Size = 1
    end
    object QrLoad_S70SUBSERIE: TWideStringField
      FieldName = 'SUBSERIE'
      Size = 2
    end
    object QrLoad_S70NUMNF: TWideStringField
      FieldName = 'NUMNF'
      Size = 6
    end
    object QrLoad_S70CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 4
    end
    object QrLoad_S70VALORTOT: TFloatField
      FieldName = 'VALORTOT'
    end
    object QrLoad_S70BASEICMS: TFloatField
      FieldName = 'BASEICMS'
    end
    object QrLoad_S70VALORICMS: TFloatField
      FieldName = 'VALORICMS'
    end
    object QrLoad_S70VLRISENTO: TFloatField
      FieldName = 'VLRISENTO'
    end
    object QrLoad_S70OUTROS: TFloatField
      FieldName = 'OUTROS'
    end
    object QrLoad_S70CIF_FOB: TSmallintField
      FieldName = 'CIF_FOB'
    end
    object QrLoad_S70SITUACAO: TWideStringField
      FieldName = 'SITUACAO'
      Size = 1
    end
    object QrLoad_S70Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLoad_S70Transporta: TIntegerField
      FieldName = 'Transporta'
    end
  end
end
