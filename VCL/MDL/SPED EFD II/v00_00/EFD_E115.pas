unit EFD_E115;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, dmkRadioGroup, UnDmkEnums;

type
  TFmEFD_E115 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdE110: TdmkEdit;
    Label6: TLabel;
    Label1: TLabel;
    TPDT_INI: TdmkEditDateTimePicker;
    TPDT_FIN: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label7: TStaticText;
    EdCOD_INF_ADIC: TdmkEdit;
    Label8: TStaticText;
    EdVL_INF_ADIC: TdmkEdit;
    EdLinArq: TdmkEdit;
    Label9: TLabel;
    StaticText3: TStaticText;
    EdDESCR_COMPL_AJ: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEFD_E115: TFmEFD_E115;

implementation

uses UnMyObjects, Module, EFD_E001, UMySQLModule;

{$R *.DFM}

procedure TFmEFD_E115.BtOKClick(Sender: TObject);
const
  REG = 'E115';
var
  E110, ImporExpor, AnoMes, Empresa, LinArq(*, MES_REF*): Integer;
  COD_INF_ADIC, DESCR_COMPL_AJ: String;
  VL_INF_ADIC: Double;
begin
  E110 := EdE110.ValueVariant;
  //
  COD_INF_ADIC := EdCOD_INF_ADIC.Text;
  DESCR_COMPL_AJ := EdDESCR_COMPL_AJ.Text;
  VL_INF_ADIC := EdVL_INF_ADIC.ValueVariant;
  //
  ImporExpor := EdImporExpor.ValueVariant;
  AnoMes := EdAnoMes.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  //
  LinArq := EdLinArq.ValueVariant;
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_E115', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  ImgTipo.SQLType, LinArq, siPositivo, EdLinArq);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'efd_E115', False, [
  'REG', 'COD_INF_ADIC', 'VL_INF_ADIC',
  'DESCR_COMPL_AJ'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'E110'], [
  REG, COD_INF_ADIC, VL_INF_ADIC,
  DESCR_COMPL_AJ], [
  ImporExpor, AnoMes, Empresa, E110, LinArq], True) then
  begin
    FmEFD_E001.ReopenEFD_E115(LinArq);
    Close;
  end;
end;

procedure TFmEFD_E115.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_E115.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_E115.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
