object FmSPEDEstqIts: TFmSPEDEstqIts
  Left = 339
  Top = 185
  Caption = 'SPE-D_STQ-003 :: Insere Item em Confer'#234'ncia de Estoque SPED'
  ClientHeight = 527
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 479
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 3
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Fechar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Insere Item em Confer'#234'ncia de Estoque SPED'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 4
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 700
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 701
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 620
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 449
    Width = 784
    Height = 30
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 784
    Height = 64
    Align = alTop
    Caption = ' Dados da confer'#234'ncia: '
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 92
      Top = 20
      Width = 45
      Height = 13
      Caption = 'Entidade:'
    end
    object Label13: TLabel
      Left = 680
      Top = 20
      Width = 41
      Height = 13
      Caption = 'Per'#237'odo:'
      FocusControl = DBEdit5
    end
    object Label1: TLabel
      Left = 12
      Top = 20
      Width = 20
      Height = 13
      Caption = 'I#E:'
      FocusControl = DBEdImporExport
    end
    object Label3: TLabel
      Left = 40
      Top = 20
      Width = 40
      Height = 13
      Caption = 'ID A#M:'
      FocusControl = DBEdAnoMes
    end
    object DBEdEmpresa: TdmkDBEdit
      Left = 92
      Top = 36
      Width = 56
      Height = 21
      DataField = 'Empresa'
      DataSource = FmSPEDEstqGer.DsSPEDEstqCab
      TabOrder = 0
      QryCampo = 'Empresa'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdit4: TDBEdit
      Left = 148
      Top = 36
      Width = 529
      Height = 21
      DataField = 'NO_ENT'
      DataSource = FmSPEDEstqGer.DsSPEDEstqCab
      TabOrder = 1
    end
    object DBEdit5: TDBEdit
      Left = 680
      Top = 36
      Width = 95
      Height = 21
      DataField = 'MES_ANO'
      DataSource = FmSPEDEstqGer.DsSPEDEstqCab
      TabOrder = 2
    end
    object DBEdImporExport: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 20
      Height = 21
      DataField = 'ImporExpor'
      DataSource = FmSPEDEstqGer.DsSPEDEstqCab
      TabOrder = 3
      QryCampo = 'ImporExpor'
      UpdCampo = 'ImporExpor'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdAnoMes: TdmkDBEdit
      Left = 36
      Top = 36
      Width = 52
      Height = 21
      DataField = 'AnoMes'
      DataSource = FmSPEDEstqGer.DsSPEDEstqCab
      TabOrder = 4
      QryCampo = 'AnoMes'
      UpdCampo = 'AnoMes'
      UpdType = utYes
      Alignment = taLeftJustify
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 112
    Width = 784
    Height = 337
    Align = alTop
    Caption = ' Dados do item de estoque:'
    TabOrder = 1
    object RGSit_Prod: TdmkRadioGroup
      Left = 2
      Top = 61
      Width = 780
      Height = 65
      Align = alTop
      Caption = ' Situa'#231#227'o do item: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        '0 = Indefinido'
        '1 = Da empresa em seu poder'
        '2 = Da empresa com terceiros'
        '3 = De Terceiros com a Empresa'
        '4 = Estoque pr'#243'prio em tr'#226'nsito'
        '5 = Estoque pr'#243'prio inaproveit'#225'vel')
      TabOrder = 1
      OnClick = RGSit_ProdClick
      QryCampo = 'Sit_Prod'
      UpdCampo = 'Sit_Prod'
      UpdType = utYes
      OldValor = 0
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 46
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        Enabled = False
      end
      object Label9: TLabel
        Left = 92
        Top = 4
        Width = 40
        Height = 13
        Caption = 'Produto:'
      end
      object Label7: TLabel
        Left = 632
        Top = 4
        Width = 95
        Height = 13
        Caption = 'Unidade de medida:'
        Enabled = False
        FocusControl = DBEdit1
      end
      object EdControle: TdmkEdit
        Left = 8
        Top = 20
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdProduto: TdmkEditCB
        Left = 92
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GraGruX'
        UpdCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBProduto
        IgnoraDBLookupComboBox = False
      end
      object CBProduto: TdmkDBLookupComboBox
        Left = 148
        Top = 20
        Width = 477
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsProdutos
        TabOrder = 2
        dmkEditCB = EdProduto
        QryCampo = 'GraGruX'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object DBEdit1: TDBEdit
        Left = 632
        Top = 20
        Width = 56
        Height = 21
        DataField = 'SIGLA'
        DataSource = DsProdutos
        Enabled = False
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 688
        Top = 20
        Width = 85
        Height = 21
        DataField = 'NO_MED'
        DataSource = DsProdutos
        Enabled = False
        TabOrder = 4
      end
    end
    object PnTerceiro: TPanel
      Left = 2
      Top = 126
      Width = 780
      Height = 47
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 2
      object Label4: TLabel
        Left = 8
        Top = 4
        Width = 325
        Height = 13
        Caption = 
          'Terceiro: (Somente e obrigat'#243'rio para situa'#231#227'o do item igual a 2' +
          ' ou 3)'
      end
      object EdTerceiro: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Terceiro'
        UpdCampo = 'Terceiro'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBTerceiro
        IgnoraDBLookupComboBox = False
      end
      object CBTerceiro: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 477
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsTerceiros
        TabOrder = 1
        dmkEditCB = EdTerceiro
        QryCampo = 'Terceiro'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object Panel4: TPanel
      Left = 2
      Top = 173
      Width = 780
      Height = 162
      Align = alClient
      TabOrder = 3
      object GroupBox2: TGroupBox
        Left = 1
        Top = 1
        Width = 116
        Height = 160
        Align = alLeft
        Caption = ' Estoque inicial: '
        TabOrder = 0
        object Label6: TLabel
          Left = 8
          Top = 16
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object Label8: TLabel
          Left = 8
          Top = 96
          Width = 50
          Height = 13
          Caption = 'Valor total:'
        end
        object Label18: TLabel
          Left = 8
          Top = 56
          Width = 58
          Height = 13
          Caption = 'Valor m'#233'dio:'
        end
        object EdEstqIniQtd: TdmkEdit
          Left = 8
          Top = 32
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'EstqIniQtd'
          UpdCampo = 'EstqIniQtd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdEstqIniQtdChange
          OnExit = EdEstqIniQtdExit
        end
        object EdEstqIniVal: TdmkEdit
          Left = 8
          Top = 112
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'EstqIniVal'
          UpdCampo = 'EstqIniVal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnEnter = EdEstqIniValEnter
          OnExit = EdEstqIniValExit
        end
        object EdEstqIniPrc: TdmkEdit
          Left = 8
          Top = 72
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'EstqIniPrc'
          UpdCampo = 'EstqIniPrc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnEnter = EdEstqIniPrcEnter
          OnExit = EdEstqIniPrcExit
        end
      end
      object GroupBox4: TGroupBox
        Left = 117
        Top = 1
        Width = 116
        Height = 160
        Align = alLeft
        Caption = ' Compras no per'#237'odo: '
        TabOrder = 1
        object Label10: TLabel
          Left = 8
          Top = 16
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object Label11: TLabel
          Left = 8
          Top = 96
          Width = 50
          Height = 13
          Caption = 'Valor total:'
        end
        object Label19: TLabel
          Left = 8
          Top = 56
          Width = 58
          Height = 13
          Caption = 'Valor m'#233'dio:'
        end
        object EdComprasQtd: TdmkEdit
          Left = 8
          Top = 32
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'ComprasQtd'
          UpdCampo = 'ComprasQtd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdComprasQtdChange
          OnExit = EdComprasQtdExit
        end
        object EdComprasVal: TdmkEdit
          Left = 8
          Top = 112
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ComprasVal'
          UpdCampo = 'ComprasVal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnEnter = EdComprasValEnter
          OnExit = EdComprasValExit
        end
        object EdComprasPrc: TdmkEdit
          Left = 8
          Top = 72
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'ComprasPrc'
          UpdCampo = 'ComprasPrc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnEnter = EdComprasPrcEnter
          OnExit = EdComprasPrcExit
        end
      end
      object GroupBox5: TGroupBox
        Left = 233
        Top = 1
        Width = 116
        Height = 160
        Align = alLeft
        Caption = ' Consumo no per'#237'odo: '
        TabOrder = 2
        object Label12: TLabel
          Left = 8
          Top = 16
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object Label14: TLabel
          Left = 8
          Top = 96
          Width = 50
          Height = 13
          Caption = 'Valor total:'
        end
        object Label20: TLabel
          Left = 8
          Top = 56
          Width = 58
          Height = 13
          Caption = 'Valor m'#233'dio:'
        end
        object EdConsumoQtd: TdmkEdit
          Left = 8
          Top = 32
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'ConsumoQtd'
          UpdCampo = 'ConsumoQtd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdConsumoQtdChange
          OnExit = EdConsumoQtdExit
        end
        object EdConsumoVal: TdmkEdit
          Left = 8
          Top = 112
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ConsumoVal'
          UpdCampo = 'ConsumoVal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnEnter = EdConsumoValEnter
          OnExit = EdConsumoValExit
        end
        object EdConsumoPrc: TdmkEdit
          Left = 8
          Top = 72
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'ConsumoPrc'
          UpdCampo = 'ConsumoPrc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnEnter = EdConsumoPrcEnter
          OnExit = EdConsumoPrcExit
        end
      end
      object GroupBox6: TGroupBox
        Left = 349
        Top = 1
        Width = 430
        Height = 160
        Align = alClient
        Caption = ' Estoque Final: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        object Label15: TLabel
          Left = 52
          Top = 56
          Width = 70
          Height = 13
          Caption = 'Quantidade:'
        end
        object Label16: TLabel
          Left = 260
          Top = 56
          Width = 63
          Height = 13
          Caption = 'Valor total:'
        end
        object Label17: TLabel
          Left = 156
          Top = 56
          Width = 71
          Height = 13
          Caption = 'Valor m'#233'dio:'
        end
        object EdEstqFimQtd: TdmkEdit
          Left = 52
          Top = 72
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'EstqFimQtd'
          UpdCampo = 'EstqFimQtd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdEstqFimQtdChange
        end
        object EdEstqFimVal: TdmkEdit
          Left = 260
          Top = 72
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'EstqFimVal'
          UpdCampo = 'EstqFimVal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnEnter = EdEstqFimValEnter
          OnExit = EdEstqFimValExit
        end
        object EdEstqFimPrc: TdmkEdit
          Left = 156
          Top = 72
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'EstqFimPrc'
          UpdCampo = 'EstqFimPrc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdEstqFimPrcChange
          OnEnter = EdEstqFimPrcEnter
          OnExit = EdEstqFimPrcExit
        end
      end
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT unm.SIGLA, unm.Nome NO_MED, ggx.Controle, CONCAT(gg1.Nome' +
        ', '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed  unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle')
    Left = 8
    Top = 12
    object QrProdutosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProdutosNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrProdutosSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrProdutosNO_MED: TWideStringField
      FieldName = 'NO_MED'
      Size = 30
    end
  end
  object DsProdutos: TDataSource
    DataSet = QrProdutos
    Left = 36
    Top = 12
  end
  object DsTerceiros: TDataSource
    DataSet = QrTerceiros
    Left = 740
    Top = 20
  end
  object QrTerceiros: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 712
    Top = 20
    object QrTerceirosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTerceirosNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
end
