unit EFD_E116;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, dmkRadioGroup, UnDmkEnums, Variants;

type
  TFmEFD_E116 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdE110: TdmkEdit;
    Label6: TLabel;
    Label1: TLabel;
    TPDT_INI: TdmkEditDateTimePicker;
    TPDT_FIN: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label7: TStaticText;
    Label8: TStaticText;
    Label10: TStaticText;
    EdVL_OR: TdmkEdit;
    EdLinArq: TdmkEdit;
    Label9: TLabel;
    TPDT_VCTO: TdmkEditDateTimePicker;
    StaticText1: TStaticText;
    EdCOD_REC: TdmkEdit;
    StaticText2: TStaticText;
    EdNUM_PROC: TdmkEdit;
    RGIND_PROC: TdmkRadioGroup;
    StaticText3: TStaticText;
    EdPROC: TdmkEdit;
    StaticText4: TStaticText;
    EdTXT_COMPL: TdmkEdit;
    StaticText5: TStaticText;
    TPMES_REF: TdmkEditDateTimePicker;
    QrTbSpedEfd005: TmySQLQuery;
    DsTbSpedEfd005: TDataSource;
    QrTbSpedEfd005CodTxt: TWideStringField;
    QrTbSpedEfd005Codigo: TIntegerField;
    QrTbSpedEfd005Nome: TWideStringField;
    QrTbSpedEfd005DataIni: TDateField;
    QrTbSpedEfd005DataFim: TDateField;
    EdCOD_OR: TdmkEditCB;
    CBCOD_OR: TdmkDBLookupComboBox;
    StaticText6: TStaticText;
    StObCOD_OR: TStaticText;
    StaticText7: TStaticText;
    StaticText8: TStaticText;
    StaticText9: TStaticText;
    StaticText10: TStaticText;
    EdE100: TdmkEdit;
    Label11: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdVL_ORKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEFD_E116: TFmEFD_E116;

implementation

uses UnMyObjects, Module, EFD_E001, UMySQLModule, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmEFD_E116.BtOKClick(Sender: TObject);
const
  REG = 'E116';
var
  E110, ImporExpor, AnoMes, Empresa, LinArq: Integer;
  COD_OR, DT_VCTO, COD_REC, NUM_PROC, IND_PROC, PROC, TXT_COMPL, MES_REF: String;
  VL_OR: Double;
var
  Ano, Mes, Dia: Word;
begin
  E110 := EdE110.ValueVariant;
  //
  COD_OR := QrTbSpedEfd005CodTxt.Value;
  VL_OR := EdVL_OR.ValueVariant;
  DT_VCTO := Geral.FDT(TPDT_VCTO.Date, 1);
  COD_REC := EdCOD_REC.Text;
  NUM_PROC := EdNUM_PROC.Text;
  IND_PROC := RGIND_PROC.Items[RGIND_PROC.ItemIndex][1];
  PROC := EdPROC.Text;
  TXT_COMPL := EdTXT_COMPL.Text;
  DecodeDate(TPMES_REF.Date, Ano, Mes, Dia);
  MES_REF := Geral.FDT(EncodeDate(Ano, Mes, 1), 1);
  //
  if MyObjects.FIC(CBCOD_OR.KeyValue = Null, EdCod_OR, 'Informe o c�digo da obriga��o a recolher!') then Exit;
  if MyObjects.FIC(VL_OR <= 0, EdVL_OR, 'Informe o valor a recolher!') then Exit;
  if MyObjects.FIC(TPDT_VCTO.Date < 2, TPDT_VCTO, 'Informe a data de vencimento!') then Exit;
  if MyObjects.FIC(COD_REC = '', EdCOD_REC, 'Informe o c�digo da receita!') then Exit;
  if MyObjects.FIC(TPMES_REF.Date < 2, TPMES_REF, 'Informe o m�s de refer�ncia!') then Exit;
  //
  ImporExpor := EdImporExpor.ValueVariant;
  AnoMes := EdAnoMes.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  //
  LinArq := EdLinArq.ValueVariant;
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_E116', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  ImgTipo.SQLType, LinArq, siPositivo, EdLinArq);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'efd_E116', False, [
  'REG', 'COD_OR',
  'VL_OR', 'DT_VCTO', 'COD_REC',
  'NUM_PROC', 'IND_PROC', 'PROC',
  'TXT_COMPL', 'MES_REF'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'E110', 'LinArq'], [
  REG, COD_OR,
  VL_OR, DT_VCTO, COD_REC,
  NUM_PROC, IND_PROC, PROC,
  TXT_COMPL, MES_REF], [
  ImporExpor, AnoMes, Empresa, E110, LinArq], True) then
  begin
    FmEFD_E001.ReopenEFD_E116(LinArq);
    Close;
  end;
end;

procedure TFmEFD_E116.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_E116.EdVL_ORKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Qry: TmySQLQuery;
  Valor: Double;
begin
  Valor := 0.00;
  if Key = VK_F4 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT VL_ICMS_RECOLHER ',
      'FROM efd_e110 ',
      'WHERE ImporExpor=' +  Geral.FF0(EdImporExpor.ValueVariant),
      'AND AnoMes=' + Geral.FF0(EdAnoMes.ValueVariant),
      'AND Empresa=' + Geral.FF0(EdEmpresa.ValueVariant),
      'AND E100=' + Geral.FF0(EdE100.ValueVariant),
      '']);
      Valor := Valor + Qry.FieldByName('VL_ICMS_RECOLHER').AsFloat;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(VL_OR) VL_OR ',
      'FROM efd_e116',
      'WHERE ImporExpor=' + Geral.FF0(EdImporExpor.ValueVariant),
      'AND AnoMes=' + Geral.FF0(EdAnoMes.ValueVariant),
      'AND Empresa=' + Geral.FF0(EdEmpresa.ValueVariant),
      'AND E110=' + Geral.FF0(EdE110.ValueVariant),
      'AND LinArq <> ' + Geral.FF0(EdLinArq.ValueVariant),
      '']);
      Valor := Valor - Qry.FieldByName('VL_OR').AsFloat;
      //
      //
      EdVL_OR.ValueVariant := Valor;
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmEFD_E116.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_E116.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
