unit EFD_E113;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, dmkRadioGroup, UnDmkEnums;

type
  TFmEFD_E113 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdE111: TdmkEdit;
    Label6: TLabel;
    Label1: TLabel;
    TPDT_INI: TdmkEditDateTimePicker;
    TPDT_FIN: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label7: TStaticText;
    EdCOD_PART: TdmkEdit;
    Label8: TStaticText;
    EdLinArq: TdmkEdit;
    Label9: TLabel;
    StaticText3: TStaticText;
    EdCOD_MOD: TdmkEdit;
    EdSER: TdmkEdit;
    StaticText1: TStaticText;
    EdSUB: TdmkEdit;
    StaticText2: TStaticText;
    EdNUM_DOC: TdmkEdit;
    StaticText5: TStaticText;
    TPDT_DOC: TdmkEditDateTimePicker;
    StaticText6: TStaticText;
    EdCOD_ITEM: TdmkEdit;
    StaticText7: TStaticText;
    EdVL_AJ_ITEM: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEFD_E113: TFmEFD_E113;

implementation

uses UnMyObjects, Module, EFD_E001, UMySQLModule;

{$R *.DFM}

procedure TFmEFD_E113.BtOKClick(Sender: TObject);
const
  REG = 'E113';
var
  E111, ImporExpor, AnoMes, Empresa, LinArq(*, MES_REF*): Integer;
  COD_PART, COD_MOD, SER, SUB, DT_DOC, COD_ITEM: String;
  NUM_DOC: Integer;
  VL_AJ_ITEM: Double;
begin
  E111 := EdE111.ValueVariant;
  //
  COD_PART := EdCOD_PART.Text;
  COD_MOD := EdCOD_MOD.Text;
  SER := EdSER.Text;
  SUB := EdSUB.Text;
  NUM_DOC := EdNUM_DOC.ValueVariant;
  DT_DOC := Geral.FDT(TPDT_DOC.Date, 1);
  COD_ITEM := EdCOD_ITEM.Text;
  VL_AJ_ITEM := EdVL_AJ_ITEM.ValueVariant;
  //
  ImporExpor := EdImporExpor.ValueVariant;
  AnoMes := EdAnoMes.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  //
  LinArq := EdLinArq.ValueVariant;
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_E113', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  ImgTipo.SQLType, LinArq, siPositivo, EdLinArq);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'efd_E113', False, [
  'REG', 'COD_PART', 'COD_MOD', 
  'SER', 'SUB', 'NUM_DOC', 
  'DT_DOC', 'COD_ITEM', 'VL_AJ_ITEM'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'E111'], [
  REG, COD_PART, COD_MOD, 
  SER, SUB, NUM_DOC, 
  DT_DOC, COD_ITEM, VL_AJ_ITEM], [
  ImporExpor, AnoMes, Empresa, E111, LinArq], True) then
  begin
    FmEFD_E001.ReopenEFD_E113(LinArq);
    Close;
  end;
end;

procedure TFmEFD_E113.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_E113.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_E113.FormCreate(Sender: TObject);
begin
  TPDT_DOC.Date := Date;
end;

procedure TFmEFD_E113.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
