unit EFD_E530;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup, dmkMemo;

type
  TFmEFD_E530 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdE520: TdmkEdit;
    Label6: TLabel;
    Label1: TLabel;
    TPDT_INI: TdmkEditDateTimePicker;
    TPDT_FIN: TdmkEditDateTimePicker;
    Label2: TLabel;
    EdLinArq: TdmkEdit;
    Label21: TLabel;
    Panel5: TPanel;
    RGIND_AJ: TdmkRadioGroup;
    Label7: TStaticText;
    EdVL_AJ: TdmkEdit;
    QrTbSPEDEFD019: TmySQLQuery;
    QrTbSPEDEFD019CodTxt: TWideStringField;
    QrTbSPEDEFD019Nome: TWideStringField;
    DsTbSPEDEFD019: TDataSource;
    EdCOD_AJ: TdmkEditCB;
    Label20: TLabel;
    CBCOD_AJ: TdmkDBLookupComboBox;
    DBMeCOD_AJ: TDBMemo;
    RGIND_DOC: TdmkRadioGroup;
    Panel6: TPanel;
    StaticText1: TStaticText;
    EdNUM_DOC: TdmkEdit;
    StaticText2: TStaticText;
    MeDESCR_AJ: TdmkMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdCOD_AJRedefinido(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FAnoMes: Integer;
    //
    procedure ReopenTabelas();
  end;

  var
  FmEFD_E530: TFmEFD_E530;

implementation

uses UnMyObjects, Module, EFD_E001, UMySQLModule, UnDmkProcFunc, DmkDAC_PF,
  ModuleGeral;

{$R *.DFM}

procedure TFmEFD_E530.BtOKClick(Sender: TObject);
const
  REG = 'E530';
var
  IND_AJ, COD_AJ, IND_DOC, NUM_DOC, DESCR_AJ: String;
  ImporExpor, AnoMes, Empresa, LinArq, E520: Integer;
  VL_AJ: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  LinArq         := EdLinArq.ValueVariant;
  //
  if MyObjects.FIC(RGIND_AJ.ItemIndex > 1, RGIND_AJ, 'Informe o indicador de ajuste!') then Exit;
  if MyObjects.FIC(RGIND_DOC.ItemIndex = -1, RGIND_AJ, 'Informe o indicador de origem do documento!') then Exit;
  //
  E520           := EdE520.ValueVariant;
  IND_AJ         := RGIND_AJ.Items[RGIND_AJ.ItemIndex][1];
  VL_AJ          := EdVL_AJ.ValueVariant;
  COD_AJ         := EdCOD_AJ.ValueVariant;
  IND_DOC        := RGIND_DOC.Items[RGIND_DOC.ItemIndex][1];
  NUM_DOC        := EdNUM_DOC.Text;
  DESCR_AJ       := MeDESCR_AJ.Text;
  //
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_e530', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  ImgTipo.SQLType, LinArq, siPositivo, EdLinArq);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efd_e530', False, [
  'REG', 'IND_AJ', 'VL_AJ',
  'COD_AJ', 'IND_DOC', 'NUM_DOC',
  'DESCR_AJ'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq', 'E520'], [
  REG, IND_AJ, VL_AJ,
  COD_AJ, IND_DOC, NUM_DOC,
  DESCR_AJ], [
  ImporExpor, AnoMes, Empresa, LinArq, E520], True) then
  begin
    FmEFD_E001.AtualizaValoresE520deE530();
    FmEFD_E001.ReopenEFD_E500(LinArq);
    Close;
  end;
end;

procedure TFmEFD_E530.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_E530.EdCOD_AJRedefinido(Sender: TObject);
begin
  if EdCOD_AJ.Text = '' then
    DBMeCOD_AJ.DataField := ''
  else
    DBMeCOD_AJ.DataField := 'Nome';
end;

procedure TFmEFD_E530.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_E530.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEFD_E530.ReopenTabelas;
var
  Ini, Fim, SQL_Periodo_Valido: String;
begin
  Ini := Geral.FDT(DmkPF.DatadeAnoMes(FAnoMes, 1, 0), 1);
  Fim := Geral.FDT(DmkPF.DatadeAnoMes(FAnoMes + 1, 1, -1), 1);
  SQL_Periodo_Valido :=
  'WHERE DataIni <= "' + Ini + '" ' + sLineBreak +
  'AND (DataFim >="' + Fim + '" OR DataFim<2)  ';

  // Tabela de ajustes da apura��o do IPI > QrTbSPEDEFD019
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD019, DModG.AllID_DB, [
  'SELECT *  ',
  'FROM tbspedefd019 ',
  SQL_Periodo_Valido,
  'ORDER BY Nome ',
  '']);
  //
end;

end.
