unit UnSPED_EFD_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) dmkEdit, dmkRadioGroup,
  dmkMemo, AdvToolBar, dmkCheckGroup, UnDmkProcFunc, TypInfo;

const
  FImporExpor = 2; // 2 = Exporta - N�o mexer!!!

type
  TEstagioVS_SPED = (evsspedNone=0, evsspedEncerraVS=1, evsspedGeraSPED=2,
                     evsspedExportaSPED=3);
  TCorrApoFormaLctoPosNegSPED = (caflpnIndef=0, caflpnMovNormEsquecido=1,
                               caflpnDesfazimentoMov=2);
  TUnSPED_EFD_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure BaixaLayoutSPED(Memo: TMemo);
    function  DefineDatasPeriodoSPED(const Empresa, AnoMes: Integer;
              const Registro: String; const LastDtIni, LastDtFim: TDateTime;
              var NewDtIni, NewDtFim: TDateTime): Boolean;
    function  DefineDatasPeriCorrApoSPED(const Empresa: Integer; const Registro:
              String; const DtLancto, DtCorrApo: TDateTime;
              var ThatDtIni, ThatDtFim: TDateTime): Boolean;
    function  DefineQuantidadePositivaOuNegativa(const regOri, REG: String;
              const ValAntes, ValNovo: Double; const FormaLcto:
              TCorrApoFormaLctoPosNegSPED; var QTD_COR_POS, QTD_COR_NEG:
              Double): Boolean;
    function  EncerraMesSPED(AnoMes, Empresa: Integer; Campo: String): Boolean;
    function  InsereItensAtuais_K280(TipoPeriodoFiscal: TTipoPeriodoFiscal;
              ImporExpor, AnoMes, Empresa, K100: Integer; const RegPai,
              RegAvo: String; const DataSPED, DtCorrApo: TDateTime;
              const MovimID, Codigo, MovimCod, Controle, GraGruX, ClientMO,
              FornecMO, EntiSitio: Integer; LocalKnd: TSPEDLocalKnd;
              const Qtde: Double; const OriBalID: TSPED_EFD_Bal;
              const SPED_EFD_K270_08_Origem: TSPED_EFD_K270_08_Origem;
              const OriESTSTabSorc: TEstqSPEDTabSorc; const OriOrigemOpeProc:
              TOrigemOpeProc; const OriOrigemIDKnd: TOrigemIDKnd;
              const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd;
              const Agrega: Boolean; var LinArq: Integer): Boolean;
    function  LiberaAcaoVS_SPED(AnoMes, Empresa: Integer; EstagioVS_SPED:
              TEstagioVS_SPED): Boolean;
    procedure MostraFormEFD_C001();
    procedure MostraFormEFD_D001();
    procedure MostraFormEFD_E001();
    procedure MostraFormSPED_EFD_Exporta();
    function  ObtemIND_ESTeCOD_PART(const Empresa, ClientMO, Terceiro, GraGruX:
              Integer; var IND_EST, COD_PART: String; var Entidade: Integer):
              String;
    function  ObtemSPED_EFD_K270_08_Origem(const REG: String; var Origem:
              TSPED_EFD_K270_08_Origem): Boolean;
    function  ObtemTipoPeriodoRegistro(Empresa: Integer; Registro, ProcOri: String):
              Integer;
    function  ObtemPeriodoSPEDdeData(Data: TDateTime; TipoPeriodoFiscal:
              TTipoPeriodoFiscal): Integer;
    function  ObtemDatasDePeriodoSPED(const Ano, Mes, Periodo: Integer;
              const TipoPeriodoFiscal: TTipoPeriodoFiscal; var DataIni, DataFim:
              TDateTime): Boolean;
    procedure ReopenTbSpedEfdXXX(Query: TmySQLQuery; DtIni, DtFim: TDateTime;
              TabName, FldOrd: String);
    function  SPEDEFDEnce_AnoMes(Campo: String): Integer;
    function  SPEDEFDEnce_Periodo(Campo: String): Integer;
    function  SPEDEFDEnce_DataMinMovim(Campo: String): TDateTime;
    function  SQLPeriodoFiscal(TipoPeriodoFiscal: TTipoPeriodoFiscal; CampoPsq,
              CampoRes: String; VirgulaFinal: Boolean): String;
  end;
var
  SPED_EFD_PF: TUnSPED_EFD_PF;

implementation

uses UnMLAGeral, MyDBCheck, DmkDAC_PF, Module, ModuleGeral, UnDmkWeb, Restaura2,
  SPED_EFD_Exporta, EFD_C001, EFD_D001, EFD_E001, UMySQLModule, UnGrade_PF,
  ModVS;


{ TUnSPED_EFD_PF }

procedure TUnSPED_EFD_PF.BaixaLayoutSPED(Memo: TMemo);
var
  Res: Boolean;
  ArqNome: String;
  Versao: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT VerSPEDLay ',
      'FROM spedefdctrl ',
      '']);
    Res := DmkWeb.VerificaAtualizacaoVersao2(True, True, CO_WEBID_SPEDLAY,
             'Layout Sped', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value),
             Qry.FieldByName('VerSPEDLay').AsInteger, 0, DModG.ObtemAgora(), Memo,
             dtSQLs, Versao, ArqNome, False);
    if (Res) and (VAR_DOWNLOAD_OK) then
    begin
      //C:\Projetos\Delphi 2007\Aplicativos\Auxiliares\DermaBK\Restaura.pas
      Application.CreateForm(TFmRestaura2, FmRestaura2);
      FmRestaura2.Show;
      FmRestaura2.EdBackFile.Text := CO_DIR_RAIZ_DMK + '\' + ArqNome + '.SQL';
      //FmRestaura2.BeRestore.DatabaseName := DMod.MyDB.DatabaseName;
      FmRestaura2.FDB := DModG.AllID_DB;
      FmRestaura2.BtDiretoClick(Self);
      FmRestaura2.Close;
      //
      //Atualiza vers�o no banco de dados
      UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
        'UPDATE spedefdctrl  ',
        'SET VerSPEDLay=' + Geral.FF0(Versao),
        '']);
    end;
  finally
    Qry.Free;
  end;
end;

function TUnSPED_EFD_PF.DefineDatasPeriCorrApoSPED(const Empresa: Integer;
  const Registro: String; const DtLancto, DtCorrApo: TDateTime;
  var ThatDtIni, ThatDtFim: TDateTime): Boolean;
const
  sProcName = 'TUnSPED_EFD_PF.DefineDatasPeriCorrApoSPED()';
var
  TipoPer, PeriAtu, PeriAnt, PerLine, DiaIni, DiaFim: Integer;
  Dia, Mes, Ano: Word;
begin
  TipoPer := ObtemTipoPeriodoRegistro(Empresa, Registro, sProcName);
  PeriAtu := ObtemPeriodoSPEDdeData(DtLancto, TTipoPeriodoFiscal(TipoPer));
  PeriAnt := ObtemPeriodoSPEDdeData(DtCorrApo, TTipoPeriodoFiscal(TipoPer));
  PerLine := PeriAtu - PeriAnt;
  if not PerLine in ([1,2]) then
    Geral.MB_Aviso('Diferen�a de tempo pass�vel de recusa!');
  case TTipoPeriodoFiscal(TipoPer) of
    spedperDecendial:
    begin
      DecodeDate(DtCorrApo, Ano, Mes, Dia);
      case Dia of
        00..10:
        begin
          ThatDtIni := EncodeDate(Ano, Mes, 1);
          ThatDtFim := EncodeDate(Ano, Mes, 10);
        end;
        11..20:
        begin
          ThatDtIni := EncodeDate(Ano, Mes, 11);
          ThatDtFim := EncodeDate(Ano, Mes, 20);
        end;
        21..31:
        begin
          ThatDtIni := EncodeDate(Ano, Mes, 21);
          ThatDtFim := IncMonth(ThatDtIni, 1);
          DecodeDate(ThatDtFim, Ano, Mes, Dia);
          ThatDtFim := EncodeDate(Ano, Mes, 1) - 1;
        end;
      end;
    end;
    spedperMensal:
    begin
      DecodeDate(DtCorrApo, Ano, Mes, Dia);
      ThatDtIni := EncodeDate(Ano, Mes, 1);
      ThatDtFim := IncMonth(ThatDtIni, 1) - 1;
    end;
  end;
end;

function TUnSPED_EFD_PF.DefineDatasPeriodoSPED(const Empresa, AnoMes: Integer;
  const Registro: String; const LastDtIni, LastDtFim: TDateTime; var NewDtIni,
  NewDtFim: TDateTime): Boolean;
const
  sProcName = 'TUnSPED_EFD_PF.DefineDatasPeriodoSPED()';
  //
  procedure OverPeriodo();
  begin
    Geral.MB_Aviso('N�o h� mais per�odos dispon�veis para o m�s selecionado!');
    NewDtIni := 0;
    NewDtFim := 0;
  end;
var
  Periodo, AnoAM, MesAM, Incremento: Integer;
  AnoPe, MesPe, DiaPe: word;
begin
  Geral.AnoMesToAnoEMes(AnoMes, AnoAM, MesAM);
  NewDtIni := Geral.AnoMesToData(AnoMes, 1);
  NewDtFim := IncMonth(NewDtIni) - 1;
  Result := False;
  //
  Periodo := ObtemTipoPeriodoRegistro(Empresa, Registro, sProcName);
{
  DModG.ReopenParamsEmp(Empresa);
  Periodo := 0;
  Incremento := 0;
  //
  if Registro = 'E100' then
    Periodo := DModG.QrParamsEmpSPED_EFD_Peri_E100.Value
  else
  if Registro = 'E500' then
    Periodo := DModG.QrParamsEmpSPED_EFD_Peri_E500.Value
  else
  if Registro = 'K100' then
    Periodo := DModG.QrParamsEmpSPED_EFD_Peri_K100.Value;
  //
  if Periodo < 1 then
  begin
    Geral.MB_Erro(
    'Tipo de per�odo indefinido em "TUnSPED_EFD_PF.DefineDatasPeriodoSPED()"');
    Exit;
  end;
}
  case TTipoPeriodoFiscal(Periodo) of
    TTipoPeriodoFiscal.spedperDecendial:
    begin
      if LastDtIni = 0 then
      begin
        NewDtIni := Geral.AnoMesToData(AnoMes, 1);
        NewDtFim := NewDtIni + 9;
      end else
      begin
        DecodeDate(LastDtFim, AnoPe, MesPe, DiaPe);
        DiaPe := DiaPe + 1;
        if DiaPe > 21 then
          OverPeriodo()
        else
        begin
          NewDtIni := EncodeDate(AnoPe, MesPe, DiaPe);
          NewDtFim := NewDtIni + 9;
          if DiaPe >= 20  then
            NewDtFim :=
              Geral.AnoMesToData(Geral.IncrementaMes_AnoMes(AnoMes, 1), 1) - 1;
        end;
      end;
    end;
    TTipoPeriodoFiscal.spedperMensal:
    begin
      if LastDtIni = 0 then
      begin
        NewDtIni := Geral.AnoMesToData(AnoMes, 1);
        NewDtFim := Geral.AnoMesToData(AnoMes + 1, 1) - 1;
      end else
        OverPeriodo();
    end;
    else begin
    Geral.MB_Erro(
    'Tipo de per�odo n�o implementado em "TUnSPED_EFD_PF.DefineDatasPeriodoSPED()"');
    Exit;
    end;
  end;
end;

function TUnSPED_EFD_PF.DefineQuantidadePositivaOuNegativa(const RegOri, REG:
  String; const ValAntes, ValNovo: Double; const FormaLcto:
  TCorrApoFormaLctoPosNegSPED; var QTD_COR_POS, QTD_COR_NEG: Double): Boolean;
  //
  procedure Aviso();
  begin
    Geral.MB_Aviso('Falta de implementa��o:' + sLineBreak +
    'Fun��o: ' + 'SPED_EFD_PF.DefineQuantidadePositivaOuNegativa()' + sLineBreak +
    // GetEnumName(TypeInfo(TOrigemIDKnd),Integer(OrigemIDKnd)))
    'Forma de corre��o: ' + GetEnumName(TypeInfo(TCorrApoFormaLctoPosNegSPED), Integer(FormaLcto)) + sLineBreak +
    'Registro de origem: ' + RegOri + sLineBreak +
    'Registro de corre��o: ' + REG + sLineBreak +
    'Quantidade anterior: ' + Geral.FFT(ValAntes, 3, siNegativo) + sLineBreak +
    'Quantidade nova: ' + Geral.FFT(ValNovo, 3, siNegativo) + sLineBreak +
    'Quantidade corre��o: ' + Geral.FFT(ValNovo - ValAntes, 3, siNegativo) + sLineBreak +
    'QTD_COR_POS: ' + Geral.FFT(QTD_COR_POS, 3, siNegativo) + sLineBreak +
    'QTD_COR_NEG: ' + Geral.FFT(QTD_COR_NEG, 3, siNegativo) + sLineBreak +
    sLineBreak);
  end;
var
  ValCorApo: Double;
begin
  Result      := False;
  QTD_COR_NEG := 0;
  QTD_COR_POS := 0;
  ValCorApo   := ValNovo - ValAntes;
  case FormaLcto of
    //caflpnIndef=0,
    caflpnMovNormEsquecido:
    begin
(*
Boa tarde,

  Esqueci de lan�ar no SPED EFD do m�s anterior uma reclassifica��o de
  produto de classe A para B. No SPED atual vou lan�a-lo no K270/K275.
  Devo lan�ar as quantidades no QTD_COR_POS ou no QTD_COR_NEG?
  Devo lan�ar no K280 tamb�m?
  // Resposta
Prezado(a) Contribuinte,

se o erro apontado seria objeto de informa��o no registro K220 no per�odo
da ocorr�ncia, ent�o a corre��o ser� efetuada nos registros K270/K275,
conforme est� descrito no Guia Pr�tico. Se o produto A est� quantificado em
n�mero maior que o correto, lan�ar quantidade negativa. Por consequ�ncia, o
"B" est� menor, lan�ar quantidade positiva.

O K280 dever� ser corrigido tamb�m, considerando que o estoque final de um
per�odo � o estoque inicial do per�odo subsequente, e, portanto,
influenciar� no estoque final escriturado dos per�odos subsequentes.
*)
      if RegOri = 'K210' then
      begin
        if REG = 'K270' then QTD_COR_NEG := ValCorApo else
        //if REG = 'K275' then QTD_COR_POS := ? else
        Aviso();
      end
      else
      if RegOri = 'K215' then
      begin
        //if REG = 'K270' then QTD_COR_NEG := ? else
        if REG = 'K275' then QTD_COR_POS := ValCorApo else
        Aviso();
      end
      else
      if RegOri = 'K220' then
      begin
        if REG = 'K270' then QTD_COR_NEG := ValCorApo else
        if REG = 'K275' then QTD_COR_POS := ValCorApo else
        Aviso();
      end
      else
      if (RegOri = 'K230')
      or (RegOri = 'K250') then
      begin
        if REG = 'K270' then QTD_COR_POS := ValCorApo else
        //if REG = 'K275' then QTD_COR_NEG := ValCorApo else
        Aviso();
      end
      else
      if (RegOri = 'K235')
      or (RegOri = 'K255') then
      begin
        if REG = 'K275' then QTD_COR_NEG := ValCorApo else
        //if REG = 'K275' then QTD_COR_NEG := ValCorApo else
        Aviso();
      end
      else
      if (RegOri = 'K260') then
      begin
        if (REG = 'K270') then
        begin
          if ValCorApo < 0 then
            QTD_COR_NEG := - ValCorApo
          else
            QTD_COR_POS := ValCorApo;
        end else
        //if REG = 'K275' then QTD_COR_NEG := ValCorApo else
        Aviso();
      end
      else Aviso();
    end;
    //caflpnDesfazimentoMov:
    else Aviso();
  end;
  if (QTD_COR_NEG < 0) or (QTD_COR_POS < 0) then Aviso() else
  if (QTD_COR_NEG = 0) and (QTD_COR_POS = 0) then Aviso() else
  Result := True;
end;

function TUnSPED_EFD_PF.EncerraMesSPED(AnoMes, Empresa: Integer;
  Campo: String): Boolean;
const
  Valor = 1;
var
  Qry: TmySQLQuery;
  Nome: String;
  MovimVS, GerLibEFD, EnvioEFD: Integer;
  SQLType: TSQLType;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM spedefdence ',
    'WHERE AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
    if Qry.RecordCount > 0 then
      SQLType      := stUpd
    else
      SQLType      := stIns;
    //AnoMes         := ;
    //Empresa        := ;
    //Nome           := ;
    //MovimVS        := ;
    //GerLibEFD      := ;
    //EnvioEFD       := ;

    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'spedefdence', False, [
    Campo], [
    'AnoMes', 'Empresa'], [
    Valor], [
    AnoMes, Empresa], True);
  finally
    Qry.Free;
  end;
end;

function TUnSPED_EFD_PF.InsereItensAtuais_K280(TipoPeriodoFiscal:
  TTipoPeriodoFiscal; ImporExpor, AnoMes, Empresa, K100: Integer; const RegPai,
  RegAvo: String; const DataSPED, DtCorrApo: TDateTime; const MovimID, Codigo,
  MovimCod, Controle, GraGruX, ClientMO, FornecMO, EntiSitio: Integer;
  LocalKnd: TSPEDLocalKnd; const Qtde: Double; const OriBalID: TSPED_EFD_Bal;
  const SPED_EFD_K270_08_Origem: TSPED_EFD_K270_08_Origem;
  const OriESTSTabSorc: TEstqSPEDTabSorc; const OriOrigemOpeProc: TOrigemOpeProc;
  const OriOrigemIDKnd: TOrigemIDKnd; const OrigemSPEDEFDKnd: TOrigemSPEDEFDKnd;
  const Agrega: Boolean; var LinArq: Integer): Boolean;
  //
  function Escolhe(A, B: Integer): Integer;
  begin
    if A <> 0 then
      Result := A
    else
      Result := B;
  end;
const
  ID_SEK   = Integer(TSPED_EFD_ComoLancou.seclImportadoNormal);
  TabDst   = 'efd_k280';
  REG      = 'K280';
  ValAntes = 0;
var
  COD_ITEM, IND_EST, COD_PART, DT_EST: String;
  (*ImporExpor, AnoMes, Empresa,*)
  BalID, BalNum, BalItm, BalEnt, Grandeza, Entidade, Tipo_Item, ESTSTabSorc,
  OriOpeProc, OrigemIDKnd, OriSPEDEFDKnd: Integer;
  QTD_COR_POS, QTD_COR_NEG, Pecas, AreaM2, PesoKg: Double;
  SQLType: TSQLType;
  OriGrandeza: TGrandezaUnidMed;
  DtAtu: TDateTime;
  AMIni, AMFim, AMAtu, Local: Integer;
  SQL_POS_NEG: String;
begin
  // erro se periodo for decendial
  if TipoPeriodoFiscal <> TTipoPeriodoFiscal.spedperMensal then
  begin
    Geral.MB_Aviso('Per�odo de apura��o n�o implementado para o registro K280!'
    + sLineBreak + 'Avise a Dermatek!');
    //
    Exit;
  end;
  //
  SQLType        := stIns;
  Result         := False;
  //ImporExpor     := ImporExpor;
  //AnoMes         := FAnoMes;
  //Empresa        := FEmpresa;
  COD_ITEM       := Geral.FF0(GraGruX);
  if Qtde > 0 then
    QTD_COR_POS  := Qtde
  else
    QTD_COR_NEG  := -Qtde;
  if QTD_COR_POS > 0 then
    SQL_POS_NEG := 'AND QTD_COR_POS > 0 '
  else
    SQL_POS_NEG := 'AND QTD_COR_NEG > 0 ';
  //
  //IND_EST        := ;
  //COD_PART       := ;
  Local := 0;
  case LocalKnd of
    (*0*)slkND: ; // Zero!
    (*1*)slkQualquer:    Local := Escolhe(EntiSitio, FornecMO);
    (*2*)slkForcaFornec: Local := FornecMO;
    (*3*)slkForcaSitio:  Local := EntiSitio;
    (*4*)slkPrefFornec:  Local := Escolhe(FornecMO, EntiSitio);
    (*5*)slkPrefSitio:   Local := Escolhe(EntiSitio, FornecMO);
    (*?*)else Geral.MB_Erro(
    '"LocalKnd" n�o implementado em "UnSPED_EFD_PF.InsereItensAtuais_K280()"');
  end;
(*
"Local" indefinido em "UnSPED_EFD_PF.InsereItensAtuais_K280()"
MovimID: 24
Codigo: 21
IME-C: 1671
IME-I: 0
Reduzido: 138
ClientMO: -11
FornecMO: 0
EntiSitio: 0
*)
  if Local = 0 then Geral.MB_Erro(
    '"Local" indefinido em "UnSPED_EFD_PF.InsereItensAtuais_K280()"' +
    sLineBreak + 'MovimID: ' + Geral.FF0(MovimID) +
    sLineBreak + 'Codigo: ' + Geral.FF0(Codigo) +
    sLineBreak + 'IME-C: ' + Geral.FF0(MovimCod) +
    sLineBreak + 'IME-I: ' + Geral.FF0(Controle) +
    sLineBreak + 'Reduzido: ' + Geral.FF0(GraGruX) +
    sLineBreak + 'ClientMO: ' + Geral.FF0(ClientMO) +
    sLineBreak + 'FornecMO: ' + Geral.FF0(FornecMO) +
    sLineBreak + 'EntiSitio: ' + Geral.FF0(EntiSitio)+
    sLineBreak);
  //SPED_EFD_PF.ObtemIND_ESTeCOD_PART(Empresa, ClientMO, FornecMO, GraGruX,
    //IND_EST, COD_PART, Entidade);
  SPED_EFD_PF.ObtemIND_ESTeCOD_PART(Empresa, ClientMO, Local, GraGruX,
    IND_EST, COD_PART, Entidade);
  //BalID          := ;
  BalNum         := MovimCod;
  BalItm         := Controle;
  BalEnt         := Geral.IMV(COD_PART);
  Grade_PF.ObtemGrandezaDeGraGruX(GraGruX, OriGrandeza, Tipo_Item);
  Grandeza       := Integer(OriGrandeza);
  Pecas          := 0;
  AreaM2         := 0;
  PesoKg         := 0;
  case OriGrandeza of
    (*0*)gumPeca   : Pecas  := Qtde;
    (*1*)gumAreaM2 : AreaM2 := Qtde;
    (*2*)gumPesoKG : PesoKg := Qtde;
    else Geral.MB_Aviso(
    'Grandeza inesperada em "FmSPED_EFD_K2XX.InsereItensAtuais_K280()"');
  end;
  ESTSTabSorc    := Integer(OriESTSTabSorc);
  OriOpeProc     := Integer(OriOrigemOpeProc);
  OrigemIDKnd    := Integer(OriOrigemIDKnd);
  OriSPEDEFDKnd  := Integer(OrigemSPEDEFDKnd);
  //
  AMIni := DmkPF.DataToAnoMes(DtCorrApo);
  AMFim := DmkPF.DataToAnoMes(DataSPED);
  AMAtu := AMINi;
  while AMAtu < AMFim do
  begin
    DtAtu          := IncMonth(Geral.AnoMesToData(AMAtu, 1), 1) - 1;
    DT_EST         := Geral.FDT(DtAtu, 1);
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.QrX999, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabDst,
    'WHERE ImporExpor=' + Geral.FF0(ImporExpor),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND K100=' + Geral.FF0(K100),
    'AND RegisPai="' + RegPai + '"',
    'AND DT_EST="' + DT_EST + '"',
    'AND ESTSTabSorc=' + Geral.FF0(ESTSTabSorc),
    'AND OriOpeProc=' + Geral.FF0(OriOpeProc),
    'AND OrigemIDKnd=' + Geral.FF0(BalItm),
    'AND OriSPEDEFDKnd=' + Geral.FF0(OriSPEDEFDKnd),
    'AND BalID=' + Geral.FF0(BalID),
    'AND BalNum=' + Geral.FF0(BalNum),
    'AND BalItm=' + Geral.FF0(BalItm),
    SQL_POS_NEG,
    '']);
    //Geral.MB_SQL(Self, DmModVS.QrX999);
    (*
    if DmModVS.QrX999.RecordCount > 0 then
    begin
      LinArq  := DmModVS.QrX999.FieldByName('LinArq').AsInteger;
      SQLType := stUpd;
      if Agrega then
      begin
        QTD_COR_POS := QTD_COR_POS + DmModVS.QrX999.FieldByName('QTD_COR_POS').AsFloat;
        QTD_COR_NEG := QTD_COR_NEG + DmModVS.QrX999.FieldByName('QTD_COR_NEG').AsFloat;
      end;
    end else
    *)
    begin
      LinArq := 0;
      LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, TabDst, 'LinArq', [
      (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
      stIns, LinArq, siPositivo, nil);
      //
      SQLType := stIns;
    end;
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, TabDst, False, [
    'REG', 'RegisPai', 'RegisAvo',
    'K100', 'DT_EST', 'COD_ITEM',
    'QTD_COR_POS', 'QTD_COR_NEG', 'IND_EST',
    'COD_PART', 'BalID', 'BalNum',
    'BalItm', 'BalEnt', 'GraGruX',
    'Grandeza', 'Pecas', 'AreaM2',
    'PesoKg', 'Entidade', 'Tipo_Item',
    'ESTSTabSorc', 'OriOpeProc', 'OrigemIDKnd',
    'OriSPEDEFDKnd'], [
    'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
    REG, RegPai, RegAvo,
    K100, DT_EST, COD_ITEM,
    QTD_COR_POS, QTD_COR_NEG, IND_EST,
    COD_PART, BalID, BalNum,
    BalItm, BalEnt, GraGruX,
    Grandeza, Pecas, AreaM2,
    PesoKg, Entidade, Tipo_Item,
    ESTSTabSorc, OriOpeProc, OrigemIDKnd,
    OriSPEDEFDKnd], [
    ImporExpor, AnoMes, Empresa, LinArq], True);
    //
    AMAtu := dmkPF.IncrementaAnoMes(AMAtu, 1);
  end;
end;

function TUnSPED_EFD_PF.LiberaAcaoVS_SPED(AnoMes, Empresa: Integer;
  EstagioVS_SPED: TEstagioVS_SPED): Boolean;
var
  FldPre, FldPos, AskPre, AskPos: String;
  Continua: Boolean;
  AMPre, AMPos: Integer;
begin
  Result := False;
  AMPre := 0;
  AMPos := 0;
  case EstagioVS_SPED of
    TEstagioVS_SPED.evsspedEncerraVS:
    begin
      FldPre := 'MovimVS';
      FldPos := 'GerLibEFD';
      AskPre :=
      'O movimento de couros ainda n�o foi encerrado para o per�odo selecionado.'
      + sLineBreak +
      'Ap�s o encerramento os novos lan�amentos feitos com data anterior ao per�odo ser�o lan�ados em SPED posterior como ajuste!'
      + sLineBreak + 'Deseja encerrar o per�odo?';
    end;
    TEstagioVS_SPED.evsspedGeraSPED:
    begin
      FldPre := 'GerLibEFD';
      FldPos := 'EnvioEFD';
      AskPre :=
      'A gera��o do SPED ainda n�o foi liberada para o per�odo selecionado.'
      + sLineBreak + 'Deseja liberar?';
    end;
    TEstagioVS_SPED.evsspedExportaSPED:
    begin
      FldPre := 'EnvioEFD';
      FldPos := 'EncerraEFD';
      AskPre :=
      'A exporta��o do SPED ainda n�o foi liberada para o per�odo selecionado.'
      + sLineBreak + 'Deseja liberar?';
    end;
    //TEstagioVS_SPED.evsspedNone:
    else
    begin
      FldPre := '';
      FldPos := '';
      AskPre := '';
      AskPos := '';
    end;
  end;
  AMPre := SPEDEFDEnce_AnoMes(FldPre);
  if AMPre < AnoMes then
  begin
    if Geral.MB_Pergunta(AskPre) = ID_Yes then
    begin
      Continua := SPED_EFD_PF.EncerraMesSPED(AnoMes, Empresa, FldPre);
      if Continua then
      begin
        // reconfirmar que criou!
        AMPre := SPEDEFDEnce_AnoMes(FldPre);
        Continua := AMPre >= AnoMes;
      end;
    end else
      Continua := False;
  end;
  if Continua then
  begin
    AMPos := SPEDEFDEnce_AnoMes(FldPos);
    Continua := AMPos < AnoMes;
(*
    if not Continua then
    begin
      if Geral.MB_Pergunta(AskPos) = ID_Yes then
      begin
        Continua := SPED_EFD_PF.EncerraMesSPED(AnoMes, Empresa, FldPos);
        if Continua then
        begin
          // reconfirmar que criou!
          AMPos := SPEDEFDEnce_AnoMes(FldPos);
          Continua := AMPos >= AnoMes;
        end;
      end;
    end;
*)
  end;
  Result := Continua;
end;

procedure TUnSPED_EFD_PF.MostraFormEFD_C001();
begin
  if DBCheck.CriaFm(TFmEFD_C001, FmEFD_C001, afmoNegarComAviso) then
  begin
    FmEFD_C001.ShowModal;
    FmEFD_C001.Destroy;
  end;
end;

procedure TUnSPED_EFD_PF.MostraFormEFD_D001();
begin
  if DBCheck.CriaFm(TFmEFD_D001, FmEFD_D001, afmoNegarComAviso) then
  begin
    FmEFD_D001.ShowModal;
    FmEFD_D001.Destroy;
  end;
end;

procedure TUnSPED_EFD_PF.MostraFormEFD_E001();
begin
  if DBCheck.CriaFm(TFmEFD_E001, FmEFD_E001, afmoNegarComAviso) then
  begin
    FmEFD_E001.ShowModal;
    FmEFD_E001.Destroy;
  end;
end;

procedure TUnSPED_EFD_PF.MostraFormSPED_EFD_Exporta();
begin
  if DBCheck.CriaFm(TFmSPED_EFD_Exporta, FmSPED_EFD_Exporta, afmoNegarComAviso) then
  begin
    FmSPED_EFD_Exporta.EdEmpresa.ValueVariant := 1;
    FmSPED_EFD_Exporta.CBEmpresa.KeyValue := 1;
    FmSPED_EFD_Exporta.RGCOD_FIN.ItemIndex := 1;
    FmSPED_EFD_Exporta.RGPreenche.ItemIndex := 2;
    FmSPED_EFD_Exporta.EdMes.ValueVariant := '01/2017';
    FmSPED_EFD_Exporta.ShowModal;
    FmSPED_EFD_Exporta.Destroy;
  end;
end;

function TUnSPED_EFD_PF.ObtemTipoPeriodoRegistro(Empresa: Integer;
  Registro, ProcOri: String): Integer;
const
  sProcName = 'TUnSPED_EFD_PF.ObtemTipoPeriodoRegistro()';
begin
  DModG.ReopenParamsEmp(Empresa);
  Result := 0;
  //
  if Registro = 'E100' then
    Result := DModG.QrParamsEmpSPED_EFD_Peri_E100.Value
  else
  if Registro = 'E500' then
    Result := DModG.QrParamsEmpSPED_EFD_Peri_E500.Value
  else
  if Registro = 'K100' then
    Result := DModG.QrParamsEmpSPED_EFD_Peri_K100.Value
  else
  begin
    Geral.MB_Erro(
    'Tipo de per�odo indefinido em "' + sProcName + '"' +
    sLineBreak +
    'Origem: "' + ProcOri + '"');
    Exit;
  end;
  //
  if Result < 1 then
  begin
    Geral.MB_Erro(
    'Tipo de per�odo n�o definido na filial para:' + sLineBreak +
    'Empresa: ' + Geral.FF0(Empresa) + sLineBreak +
    'Registro: ' + Registro + sLineBreak +
    'procedimento: "' + sProcName + '"' + sLineBreak +
    'Origem: "' + ProcOri + '"' + sLineBreak +
    'Configure em Op��es >> Filiais na guia SPED EFD ');
    Exit;
  end;
end;

function TUnSPED_EFD_PF.ObtemDatasDePeriodoSPED(const Ano, Mes, Periodo: Integer;
  const TipoPeriodoFiscal: TTipoPeriodoFiscal; var DataIni, DataFim:
  TDateTime): Boolean;
begin
  Result := False;
  case TipoPeriodoFiscal of
    //spedperIndef=0,
    (*1*)spedperDiario:
    begin
      DataIni := EncodeDate(Ano, Mes, Periodo);
      DataFim := DataIni;
      Result  := True;
    end;
    {
    (*2*)spedperSemanal:
    begin
      Errado? semana pode comecar de 1 a 7!
      DataIni :=       DataIni := EncodeDate(Ano, Mes, Periodo);
      DataFim := DataIni;

      DataFim := DataIni;
      Result  := EncodeDate(Ano, Mes, (Periodo * 7) - 6);
    end;
    }
    (*3*)spedperDecendial:
    begin
      DataIni := EncodeDate(Ano, Mes, (Periodo * 10) - 9);
      if Periodo = 3 then
        DataFim := IncMonth(EncodeDate(Ano, Mes, 1), 1) - 1
      else
        DataFim := DataIni + 9;
      Result  := True;
    end;
    {
    (*4*)spedperQuinzenal:
    begin
      Meses := Geral.Periodo2000(Data);
      DecodeDate(Data, Ano, Mes, Dia);
      Periodos := (Dia div 15) + 1;
      if Periodos > 2 then
        Periodos := 2;
      Result := (Meses - 1) * 2;
      Result := Result + Periodos;
    end;
    }
    (*5*)spedperMensal:
    begin
      DataIni := EncodeDate(Ano, Mes, 1);
      DataFim := IncMonth(EncodeDate(Ano, Mes, 1), 1) - 1;
      Result  := True;
    end;
    else Geral.MB_Erro('Tipo de periodo n�o imlementado!');
  end;
end;

function TUnSPED_EFD_PF.ObtemIND_ESTeCOD_PART(const Empresa, ClientMO, Terceiro,
  GraGruX: Integer; var IND_EST, COD_PART: String; var Entidade: Integer): String;
begin
  Result   := '';
  IND_EST  := '';
  COD_PART := '';
  Entidade := 0;
  // o produto pertence a empresa declarante
  if ClientMO  = Empresa then
  begin
    if Terceiro = Empresa then
    //Da empresa em seu poder
    begin
      IND_EST  := Geral.FF0(Integer(spedipiePropInfPoderInf));//=0,
      COD_PART := '';
      Entidade := Empresa;
    end else
    //Da empresa com terceiros
    begin
      IND_EST  := Geral.FF0(Integer(spedipiePropInfPoderTer)); //=1,
      COD_PART := Geral.FF0(Terceiro);
      Entidade := Terceiro;
    end;
  end else
  // o produto N�O pertence a empresa declarante
  begin
    if Terceiro = Empresa then
    //De terceiros c/ a empresa
    begin
      IND_EST := Geral.FF0(Integer(spedipiePropTerPoderInf)); //=2,
      COD_PART := Geral.FF0(Terceiro);
      Entidade := Terceiro;
    end else
    // De terceiros com terceiros
    begin
      IND_EST  := '-';
      COD_PART := '0';
      Entidade := Terceiro;
      //
      //MeAviso.Lines.Add('COD_ITEM: ' + COD_ITEM +
      Result := 'COD_ITEM: ' + Geral.FF0(GraGruX) +
      ' De terceiros com terceiros! COD_PART: ' + Geral.FF0(Terceiro);
    end;
  end;
end;

function TUnSPED_EFD_PF.ObtemPeriodoSPEDdeData(Data: TDateTime;
  TipoPeriodoFiscal: TTipoPeriodoFiscal): Integer;
var
  Meses, Periodo, Dias, Semanas, Periodos: Integer;
  Dia, Mes, Ano: Word;
begin
  Result := 0;
{///////////////////////////////////////////////////////////////////////////////
Per�odo
                http://www31.receita.fazenda.gov.br/SicalcWeb/calcpfPer_odo.html
                extraido em 13/05/2017
Para as receitas que a aplica��o poder� efetuar o c�lculo de acr�scimos legais
  (multa de mora e juros), essa informa��o ser� obrigat�ria. Para as demais, a
  aplica��o solicitar�, em seu lugar, o "Per�odo de Apura��o".
Para tipo de per�odo igual a "Exerc�cio", dever� ser informado o ano
  correspondente, com quatro algarismos. N�o dever� ser informado o ano-base e
  sim o exerc�cio da declara��o.
Para tipo de per�odo igual a "Trimestral", dever� ser informado o trimestre
  correspondente, no formato TAAAA, onde T � igual ao n�mero do trimestre
  ( 1,2,3 ou 4) e AAAA � igual ao ano com quatro algarismos.
Para tipo de per�odo igual a "Mensal", dever� ser informado o m�s
  correspondente, no formato MMAAAA, onde MM � igual ao n�mero do m�s com dois
  algarismos(01 a 12) e AAAA � igual ao ano com quatro algarismos.
Para tipo de per�odo igual a "Quinzenal", dever� ser informada a quinzena
  correspondente, no formato QMMAAAA, onde Q � igual ao n�mero da quinzena
  (1 ou 2), MM � igual ao n�mero do m�s com dois algarismos (01 a 12) e AAAA �
  igual ao ano com quatro algarismos.
Para tipo de per�odo igual a "Decendial", dever� ser informado o dec�ndio
  correspondente, no formato DMMAAAA, onde D � igual ao n�mero do dec�ndio
  (1, 2 ou 3), MM � igual ao n�mero do m�s com dois algarismos (01 a 12) e AAAA
  � igual ao ano com quatro algarismos.
Para tipo de per�odo igual a "Semanal", dever� ser informada a semana
  correspondente, no formato SMMAAAA, onde S � igual ao n�mero da semana
  (1 a 5), MM � igual ao n�mero do m�s com dois algarismos (01 a 12) e AAAA �
  igual ao ano com quatro algarismos.
  OBS: Para os tributos de periodicidade semanal, que a pr�pria p�gina apresenta
    os valores dos acr�scimos legais, quando devidos, e do respectivo
    vencimento, a semana se inicia no domingo e termina no s�bado. Portanto, a
    primeira semana de um determinado m�s � aquela onde aparece o primeiro
    s�bado, mesmo que seja o primeiro dia do m�s.
Para tipo de per�odo igual a "Di�rio", informe o dia correspondente, no formato
  DDMMAAAA, onde DD � igual ao dia do m�s (01 a 31), MM � igual ao n�mero do m�s
  com dois algarismos (01 a 12) e AAAA � igual ao ano com quatro algarismos.e
///////////////////////////////////////////////////////////////////////////////}
  case TipoPeriodoFiscal of
    //spedperIndef=0,
    (*1*)spedperDiario   : Result := Trunc(Data) - Trunc(EncodeDate(1999, 12, 31));
    (*2*)spedperSemanal  :
    begin
      Dias := Trunc(Data) - Trunc(EncodeDate(2000, 1, 1));
      Result := (Dias div 7) + 1;
    end;
    (*3*)spedperDecendial:
    begin
      Meses := Geral.Periodo2000(Data);
      DecodeDate(Data, Ano, Mes, Dia);
      Semanas := ((Dia-1) div 10) + 1;
      if Semanas > 3 then
        Semanas := 3;
      Result := (Meses - 1) * 3;
      Result := Result + Semanas;
    end;
    (*4*)spedperQuinzenal:
    begin
      Meses := Geral.Periodo2000(Data);
      DecodeDate(Data, Ano, Mes, Dia);
      Periodos := (Dia div 15) + 1;
      if Periodos > 2 then
        Periodos := 2;
      Result := (Meses - 1) * 2;
      Result := Result + Periodos;
    end;
    (*5*)spedperMensal: Result := Geral.Periodo2000(Data);
    else Geral.MB_Erro('Tipo de periodo n�o imlementado!');
  end;
end;

function TUnSPED_EFD_PF.ObtemSPED_EFD_K270_08_Origem(const REG: String; var
  Origem: TSPED_EFD_K270_08_Origem): Boolean;
const
  sprocName = 'ObtemSPED_EFD_K270_08_Origem()';
begin
  if (REG = 'K230') or (REG = 'K235') then Origem := sek2708oriK230K235 else
  if (REG = 'K250') or (REG = 'K255') then Origem := sek2708oriK250K255 else
  if (REG = 'K210') or (REG = 'K215') then Origem := sek2708oriK210K215 else
  if (REG = 'K260') or (REG = 'K265') then Origem := sek2708oriK260K265 else
  if (REG = 'K220') (***************) then Origem := sek2708oriK220 else
  Origem := sek2708oriIndef;
  //
  Result := Integer(Origem) > 0;
  if not Result then
    Geral.MB_ERRO('"REG" n�o implementado em ' + sProcName);
end;

procedure TUnSPED_EFD_PF.ReopenTbSpedEfdXXX(Query: TmySQLQuery; DtIni,
  DtFim: TDateTime; TabName, FldOrd: String);
var
  Ini, Fim: String;
begin
  Ini := Geral.FDT(DtIni, 1);
  Fim := Geral.FDT(DtFim, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.AllID_DB, [
  'SELECT * ',
  'FROM ' + TabName,
  'WHERE DataIni <= "' + Ini + '" ',
  'AND (  ',
  '  DataFim >= "' + Fim + '" ',
  '  OR ',
  '  DataFim < "1900-01-01" ',
  ') ',
  'ORDER BY ' + FldOrd,
  '']);
end;

function TUnSPED_EFD_PF.SPEDEFDEnce_AnoMes(Campo: String): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(AnoMes) AnoMes ',
    'FROM spedefdence ',
    'WHERE ' + Campo + '=1 ', // 1=Feito
    '']);
    Result := Qry.FieldByName('AnoMes').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnSPED_EFD_PF.SPEDEFDEnce_DataMinMovim(Campo: String): TDateTime;
var
  AnoMes: Integer;
begin
  AnoMes := SPEDEFDEnce_AnoMes(Campo);
  //
  if AnoMes <> 0 then
    Result := Geral.AnoMesToData(AnoMes, 1);
end;

function TUnSPED_EFD_PF.SPEDEFDEnce_Periodo(Campo: String): Integer;
var
  AnoMes: Integer;
begin
  AnoMes := SPEDEFDEnce_AnoMes(Campo);
  //
  if AnoMes <> 0 then
    Result := Geral.AnoMesToPeriodo(AnoMes)
  else
    Result := 0;
end;

function TUnSPED_EFD_PF.SQLPeriodoFiscal(TipoPeriodoFiscal: TTipoPeriodoFiscal;
  CampoPsq, CampoRes: String; VirgulaFinal: Boolean): String;
var
  Virgula: String;
begin
  if VirgulaFinal then
    Virgula := ', '
  else
    Virgula := ' ';
  //
  case TipoPeriodoFiscal of
    //spedperIndef=0, spedperDiario=1, spedperSemanal=2,
    spedperDecendial: Result := Geral.ATS([
      'CASE ',
      '  WHEN ' + CampoPsq + ' = 0 THEN 0 ',
      '  WHEN DAYOFMONTH(' + CampoPsq + ') < 11 THEN 1 ',
      '  WHEN DAYOFMONTH(' + CampoPsq + ') < 21 THEN 2 ',
      '  ELSE 3 END ' + CampoRes]);
    //spedperQuinzenal=4,
    spedperMensal: Result := Geral.ATS([
      'CASE ',
      '  WHEN ' + CampoPsq + ' = 0 THEN 0 ',
      '  ELSE 1 END ' + CampoRes]);
    else Result := Geral.ATS([
      'CASE ',
      '  WHEN ' + CampoPsq + ' = 0 THEN #ERR ',
      '  ELSE #ERR END ' + CampoRes]);
  end;
  Result := Result + Virgula;
end;

end.


