object FmEFD_E001: TFmEFD_E001
  Left = 368
  Top = 194
  Caption = 'EFD-SPEDE-001 :: Apura'#231#227'o do ICMS e do IPI'
  ClientHeight = 834
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 113
    Width = 1008
    Height = 721
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 57
      Width = 1008
      Height = 664
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet2: TTabSheet
        Caption = ' Per'#237'odos '
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 636
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Panel9: TPanel
            Left = 103
            Top = 1
            Width = 896
            Height = 544
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object PCEx00: TPageControl
              Left = 0
              Top = 0
              Width = 896
              Height = 544
              ActivePage = TabSheet11
              Align = alClient
              TabOrder = 0
              object TabSheet1: TTabSheet
                Caption = ' Apura'#231#227'o do ICMS '
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel1: TPanel
                  Left = 0
                  Top = 0
                  Width = 888
                  Height = 473
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object GroupBox1: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 155
                    Height = 473
                    Align = alLeft
                    Caption = ' Intervalos de apura'#231#227'o: '
                    TabOrder = 0
                    ExplicitHeight = 479
                    object dmkDBGrid1: TdmkDBGrid
                      Left = 2
                      Top = 59
                      Width = 151
                      Height = 412
                      Align = alClient
                      Columns = <
                        item
                          Alignment = taCenter
                          Expanded = False
                          FieldName = 'DT_INI'
                          Title.Caption = 'In'#237'cio'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DT_FIN'
                          Title.Caption = 'Final'
                          Width = 56
                          Visible = True
                        end>
                      Color = clWindow
                      DataSource = DsEFD_E100
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Alignment = taCenter
                          Expanded = False
                          FieldName = 'DT_INI'
                          Title.Caption = 'In'#237'cio'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DT_FIN'
                          Title.Caption = 'Final'
                          Width = 56
                          Visible = True
                        end>
                    end
                    object Panel32: TPanel
                      Left = 2
                      Top = 15
                      Width = 151
                      Height = 44
                      Align = alTop
                      TabOrder = 1
                      object BtE100: TBitBtn
                        Tag = 10078
                        Left = 12
                        Top = 2
                        Width = 120
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Intervalo'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 0
                        OnClick = BtE100Click
                      end
                    end
                  end
                  object PCE100: TPageControl
                    Left = 155
                    Top = 0
                    Width = 733
                    Height = 473
                    ActivePage = TabSheet6
                    Align = alClient
                    TabOrder = 1
                    object TabSheet3: TTabSheet
                      Caption = 'E110: Apura'#231#227'o do ICMS - Opera'#231#245'es Pr'#243'prias'
                      ExplicitLeft = 0
                      ExplicitTop = 0
                      ExplicitWidth = 0
                      ExplicitHeight = 0
                      object Panel4: TPanel
                        Left = 0
                        Top = 0
                        Width = 725
                        Height = 445
                        Align = alClient
                        ParentBackground = False
                        TabOrder = 0
                        object DBEdit2: TDBEdit
                          Left = 424
                          Top = 8
                          Width = 112
                          Height = 21
                          DataField = 'VL_TOT_DEBITOS'
                          DataSource = DsEFD_E110
                          TabOrder = 0
                        end
                        object DBEdit3: TDBEdit
                          Left = 424
                          Top = 32
                          Width = 112
                          Height = 21
                          DataField = 'VL_AJ_DEBITOS'
                          DataSource = DsEFD_E110
                          TabOrder = 1
                        end
                        object DBEdit4: TDBEdit
                          Left = 424
                          Top = 56
                          Width = 112
                          Height = 21
                          DataField = 'VL_TOT_AJ_DEBITOS'
                          DataSource = DsEFD_E110
                          TabOrder = 2
                        end
                        object DBEdit6: TDBEdit
                          Left = 424
                          Top = 104
                          Width = 112
                          Height = 21
                          DataField = 'VL_TOT_CREDITOS'
                          DataSource = DsEFD_E110
                          TabOrder = 4
                        end
                        object DBEdit5: TDBEdit
                          Left = 424
                          Top = 80
                          Width = 112
                          Height = 21
                          DataField = 'VL_ESTORNOS_CRED'
                          DataSource = DsEFD_E110
                          TabOrder = 3
                        end
                        object DBEdit7: TDBEdit
                          Left = 424
                          Top = 128
                          Width = 112
                          Height = 21
                          DataField = 'VL_AJ_CREDITOS'
                          DataSource = DsEFD_E110
                          TabOrder = 5
                        end
                        object DBEdit8: TDBEdit
                          Left = 424
                          Top = 152
                          Width = 112
                          Height = 21
                          DataField = 'VL_TOT_AJ_CREDITOS'
                          DataSource = DsEFD_E110
                          TabOrder = 6
                        end
                        object DBEdit9: TDBEdit
                          Left = 424
                          Top = 176
                          Width = 112
                          Height = 21
                          DataField = 'VL_ESTORNOS_DEB'
                          DataSource = DsEFD_E110
                          TabOrder = 7
                        end
                        object DBEdit10: TDBEdit
                          Left = 424
                          Top = 200
                          Width = 112
                          Height = 21
                          DataField = 'VL_SLD_CREDOR_ANT'
                          DataSource = DsEFD_E110
                          TabOrder = 8
                        end
                        object DBEdit11: TDBEdit
                          Left = 424
                          Top = 224
                          Width = 112
                          Height = 21
                          DataField = 'VL_SLD_APURADO'
                          DataSource = DsEFD_E110
                          TabOrder = 9
                        end
                        object DBEdit12: TDBEdit
                          Left = 424
                          Top = 248
                          Width = 112
                          Height = 21
                          DataField = 'VL_TOT_DED'
                          DataSource = DsEFD_E110
                          TabOrder = 10
                        end
                        object DBEdit13: TDBEdit
                          Left = 424
                          Top = 272
                          Width = 112
                          Height = 21
                          DataField = 'VL_ICMS_RECOLHER'
                          DataSource = DsEFD_E110
                          TabOrder = 11
                        end
                        object DBEdit14: TDBEdit
                          Left = 424
                          Top = 296
                          Width = 112
                          Height = 21
                          DataField = 'VL_SLD_CREDOR_TRANSPORTAR'
                          DataSource = DsEFD_E110
                          TabOrder = 12
                        end
                        object DBEdit15: TDBEdit
                          Left = 424
                          Top = 320
                          Width = 112
                          Height = 21
                          DataField = 'DEB_ESP'
                          DataSource = DsEFD_E110
                          TabOrder = 13
                        end
                        object Label7: TStaticText
                          Left = 8
                          Top = 8
                          Width = 412
                          Height = 21
                          Alignment = taRightJustify
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = 
                            '02. Valor total dos d'#233'bitos por "Sa'#237'das e presta'#231#245'es com d'#233'bito ' +
                            'do imposto"'
                          TabOrder = 14
                        end
                        object Label8: TStaticText
                          Left = 8
                          Top = 32
                          Width = 412
                          Height = 21
                          Alignment = taRightJustify
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = 
                            '03. Valor total dos ajustes a d'#233'bito decorrentes do documento fi' +
                            'scal'
                          TabOrder = 15
                        end
                        object Label9: TStaticText
                          Left = 8
                          Top = 56
                          Width = 412
                          Height = 21
                          Alignment = taRightJustify
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = '04. Valor total de "Ajustes a d'#233'bito".'
                          TabOrder = 16
                        end
                        object Label10: TStaticText
                          Left = 8
                          Top = 80
                          Width = 412
                          Height = 21
                          Alignment = taRightJustify
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = '05. Valor total de Ajustes "Estornos de cr'#233'ditos"'
                          TabOrder = 17
                        end
                        object Label11: TStaticText
                          Left = 8
                          Top = 104
                          Width = 412
                          Height = 21
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = 
                            '06. Valor total dos cr'#233'ditos por "Entradas e aquisi'#231#245'es com cr'#233'd' +
                            'ito do imposto"'
                          TabOrder = 18
                        end
                        object Label12: TStaticText
                          Left = 8
                          Top = 128
                          Width = 412
                          Height = 21
                          Alignment = taRightJustify
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = 
                            '07. Valor total dos ajustes a cr'#233'dito decorrentes do documento f' +
                            'iscal.'
                          TabOrder = 19
                        end
                        object Label13: TStaticText
                          Left = 8
                          Top = 152
                          Width = 412
                          Height = 21
                          Alignment = taRightJustify
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = '08. Valor total de "Ajustes a cr'#233'dito"'
                          TabOrder = 20
                        end
                        object Label14: TStaticText
                          Left = 8
                          Top = 176
                          Width = 412
                          Height = 21
                          Alignment = taRightJustify
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = '09. Valor total de Ajustes "Estornos de D'#233'bitos"'
                          TabOrder = 21
                        end
                        object Label15: TStaticText
                          Left = 8
                          Top = 200
                          Width = 412
                          Height = 21
                          Alignment = taRightJustify
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = 
                            '10. Valor   total   de   "Saldo   credor   do   per'#237'odo anterior' +
                            '"'
                          TabOrder = 22
                        end
                        object Label16: TStaticText
                          Left = 8
                          Top = 224
                          Width = 412
                          Height = 21
                          Alignment = taRightJustify
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = '11. Valor do saldo devedor apurado'
                          TabOrder = 23
                        end
                        object Label17: TStaticText
                          Left = 8
                          Top = 248
                          Width = 412
                          Height = 21
                          Alignment = taRightJustify
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = '12. Valor total de "Dedu'#231#245'es"'
                          TabOrder = 24
                        end
                        object Label18: TStaticText
                          Left = 8
                          Top = 272
                          Width = 412
                          Height = 21
                          Alignment = taRightJustify
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = '13. Valor total de "ICMS a recolher" (11-12)'
                          TabOrder = 25
                        end
                        object Label19: TStaticText
                          Left = 8
                          Top = 296
                          Width = 412
                          Height = 21
                          Alignment = taRightJustify
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = 
                            '14. Valor total de "Saldo credor a transportar para o per'#237'odo se' +
                            'guinte"'
                          TabOrder = 26
                        end
                        object Label20: TStaticText
                          Left = 8
                          Top = 320
                          Width = 412
                          Height = 21
                          Alignment = taRightJustify
                          AutoSize = False
                          BorderStyle = sbsSunken
                          Caption = '15. Valores recolhidos ou a recolher, extra-apura'#231#227'o.'
                          TabOrder = 27
                        end
                      end
                    end
                    object TabSheet4: TTabSheet
                      Caption = 'E111'
                      ImageIndex = 1
                      ExplicitLeft = 0
                      ExplicitTop = 0
                      ExplicitWidth = 0
                      ExplicitHeight = 0
                      object Panel5: TPanel
                        Left = 0
                        Top = 0
                        Width = 725
                        Height = 445
                        Align = alClient
                        ParentBackground = False
                        TabOrder = 0
                        object Splitter2: TSplitter
                          Left = 1
                          Top = 282
                          Width = 723
                          Height = 5
                          Cursor = crVSplit
                          Align = alBottom
                        end
                        object Label25: TLabel
                          Left = 1
                          Top = 1
                          Width = 265
                          Height = 13
                          Align = alTop
                          Caption = 'E111: Ajuste/benef'#237'cio/incentivo da apura'#231#227'o do ICMS'
                        end
                        object DBGrid1: TDBGrid
                          Left = 1
                          Top = 14
                          Width = 723
                          Height = 268
                          Align = alClient
                          DataSource = DsEFD_E111
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'COD_AJ_APUR'
                              Title.Caption = 'C'#243'd.Ajuste'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'DESCR_COMPL_AJ'
                              Title.Caption = 'Descri'#231#227'o complementar do ajuste da apura'#231#227'o'
                              Width = 550
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'VL_AJ_APUR'
                              Title.Caption = 'Valor do ajuste'
                              Visible = True
                            end>
                        end
                        object Panel14: TPanel
                          Left = 1
                          Top = 287
                          Width = 723
                          Height = 157
                          Align = alBottom
                          ParentBackground = False
                          TabOrder = 1
                          object Label21: TLabel
                            Left = 1
                            Top = 1
                            Width = 297
                            Height = 13
                            Align = alTop
                            Caption = 'E112: Informa'#231#245'es adicionais do ajustes da apura'#231#227'o do ICMS'
                          end
                          object Splitter1: TSplitter
                            Left = 1
                            Top = 75
                            Width = 721
                            Height = 5
                            Cursor = crVSplit
                            Align = alTop
                          end
                          object Label22: TLabel
                            Left = 1
                            Top = 80
                            Width = 480
                            Height = 13
                            Align = alTop
                            Caption = 
                              'E113: Informa'#231#245'es adicionais do ajustes da apura'#231#227'o do ICMS - Id' +
                              'entifica'#231#227'o dos documentos fiscais'
                          end
                          object DBGrid4: TDBGrid
                            Left = 1
                            Top = 93
                            Width = 721
                            Height = 63
                            Align = alClient
                            DataSource = DsEFD_E113
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'COD_PART'
                                Title.Caption = 'Terceiro'
                                Width = 46
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_MOD'
                                Title.Caption = 'Mod. NF'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'SER'
                                Title.Caption = 'S'#233'rie'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'SUB'
                                Title.Caption = 'Sub'
                                Width = 25
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NUM_DOC'
                                Title.Caption = 'Num NF'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'DT_DOC'
                                Title.Caption = 'Dta NF'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'COD_ITEM'
                                Title.Caption = 'C'#243'd. Item'
                                Width = 59
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'VL_AJ_ITEM'
                                Title.Caption = 'Valor item'
                                Width = 68
                                Visible = True
                              end>
                          end
                          object DBGrid6: TDBGrid
                            Left = 1
                            Top = 14
                            Width = 721
                            Height = 61
                            Align = alTop
                            DataSource = DsEFD_E112
                            TabOrder = 1
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -12
                            TitleFont.Name = 'MS Sans Serif'
                            TitleFont.Style = []
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'NUM_DA'
                                Title.Caption = 'Num. DA'
                                Width = 56
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NUM_PROC'
                                Title.Caption = 'Num. Proc.'
                                Width = 72
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'IND_PROC'
                                Title.Caption = 'IOP'
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'PROC'
                                Title.Caption = 'Descri'#231#227'o resumida'
                                Width = 200
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'TXT_COMPL'
                                Title.Caption = 'Descri'#231#227'o complementar'
                                Width = 333
                                Visible = True
                              end>
                          end
                        end
                      end
                    end
                    object TabSheet5: TTabSheet
                      Caption = 'E115'
                      ImageIndex = 2
                      ExplicitLeft = 0
                      ExplicitTop = 0
                      ExplicitWidth = 0
                      ExplicitHeight = 0
                      object Panel15: TPanel
                        Left = 0
                        Top = 0
                        Width = 725
                        Height = 445
                        Align = alClient
                        ParentBackground = False
                        TabOrder = 0
                        object Label23: TLabel
                          Left = 1
                          Top = 1
                          Width = 278
                          Height = 13
                          Align = alTop
                          Caption = 'Informa'#231#245'es adicionais da apura'#231#227'o - Valores declarat'#243'rios'
                        end
                        object DBGrid5: TDBGrid
                          Left = 1
                          Top = 14
                          Width = 723
                          Height = 430
                          Align = alClient
                          DataSource = DsEFD_E115
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'COD_INF_ADIC'
                              Title.Caption = 'C'#243'digo'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'VL_INF_ADIC'
                              Title.Caption = 'Valor'
                              Width = 80
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'DESCR_COMPL_AJ'
                              Title.Caption = 'Descri'#231#227'o complementar do ajuste'
                              Width = 549
                              Visible = True
                            end>
                        end
                      end
                    end
                    object TabSheet6: TTabSheet
                      Caption = 'E116'
                      ImageIndex = 3
                      ExplicitLeft = 0
                      ExplicitTop = 0
                      ExplicitWidth = 0
                      ExplicitHeight = 0
                      object Panel8: TPanel
                        Left = 0
                        Top = 0
                        Width = 725
                        Height = 445
                        Align = alClient
                        ParentBackground = False
                        TabOrder = 0
                        object Label24: TLabel
                          Left = 1
                          Top = 1
                          Width = 249
                          Height = 13
                          Align = alTop
                          Caption = 'Obriga'#231#245'es do ICMS a recolher - Opera'#231#245'es pr'#243'prias'
                        end
                        object DBGrid2: TDBGrid
                          Left = 1
                          Top = 14
                          Width = 416
                          Height = 385
                          Align = alLeft
                          DataSource = DsEFD_E116
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'COD_OR'
                              Title.Caption = 'C'#243'd. OR'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'VL_OR'
                              Title.Caption = 'Valor OR'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'DT_VCTO'
                              Title.Caption = 'Vencto'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_REC'
                              Title.Caption = 'C'#243'd. Rec.'
                              Width = 58
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NUM_PROC'
                              Title.Caption = 'Num. Proc.'
                              Width = 72
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'IOP'
                              Width = 23
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'MES_REF'
                              Title.Caption = 'M'#234's ref.'
                              Width = 44
                              Visible = True
                            end>
                        end
                        object Panel13: TPanel
                          Left = 1
                          Top = 399
                          Width = 723
                          Height = 45
                          Align = alBottom
                          ParentBackground = False
                          TabOrder = 1
                          object Label3: TLabel
                            Left = 8
                            Top = 8
                            Width = 547
                            Height = 13
                            Caption = 
                              'C'#243'd. Rec.: C'#243'digo de receita referente '#224' obriga'#231#227'o, pr'#243'prio da u' +
                              'nidade da federa'#231#227'o, conforme legisla'#231#227'o estadual.'
                          end
                          object Label4: TLabel
                            Left = 8
                            Top = 24
                            Width = 474
                            Height = 13
                            Caption = 
                              'Num. Proc.: N'#250'mero do processo ou auto de infra'#231#227'o ao qual aobri' +
                              'ga'#231#227'o est'#225' vinculada, se houver.'
                          end
                        end
                        object Panel17: TPanel
                          Left = 417
                          Top = 14
                          Width = 307
                          Height = 385
                          Align = alClient
                          ParentBackground = False
                          TabOrder = 2
                          object Label5: TLabel
                            Left = 1
                            Top = 1
                            Width = 291
                            Height = 13
                            Align = alTop
                            Caption = 'Descri'#231#227'o resumida do processo que embasou o lan'#231'amento:'
                          end
                          object Label6: TLabel
                            Left = 1
                            Top = 77
                            Width = 245
                            Height = 13
                            Align = alTop
                            Caption = 'Descri'#231#227'o complementar das obriga'#231#245'es a recolher:'
                          end
                          object DBMemo1: TDBMemo
                            Left = 1
                            Top = 90
                            Width = 305
                            Height = 294
                            Align = alClient
                            DataField = 'TXT_COMPL'
                            DataSource = DsEFD_E116
                            TabOrder = 0
                          end
                          object DBMemo2: TDBMemo
                            Left = 1
                            Top = 14
                            Width = 305
                            Height = 63
                            Align = alTop
                            DataField = 'PROC'
                            DataSource = DsEFD_E116
                            TabOrder = 1
                          end
                        end
                      end
                    end
                  end
                end
                object Panel22: TPanel
                  Left = 0
                  Top = 473
                  Width = 888
                  Height = 43
                  Align = alBottom
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 1
                  ExplicitTop = 479
                  ExplicitWidth = 891
                  object BtE110: TBitBtn
                    Tag = 10079
                    Left = 4
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Apura'#231#227'o'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtE110Click
                  end
                  object BtE111: TBitBtn
                    Tag = 10080
                    Left = 124
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'A&juste'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 1
                    OnClick = BtE111Click
                  end
                  object BtE116: TBitBtn
                    Tag = 10082
                    Left = 364
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Obriga'#231#245'es'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 3
                    OnClick = BtE116Click
                  end
                  object BtE115: TBitBtn
                    Tag = 10081
                    Left = 244
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Val.Declt'#243'r'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 2
                    OnClick = BtE115Click
                  end
                end
              end
              object TabSheet7: TTabSheet
                Caption = 'Apura'#231#227'o do IPI '
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel18: TPanel
                  Left = 0
                  Top = 0
                  Width = 888
                  Height = 473
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object GroupBox3: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 229
                    Height = 473
                    Align = alLeft
                    Caption = ' E500 :: Intervalos de apura'#231#227'o: '
                    TabOrder = 0
                    ExplicitHeight = 479
                    object dmkDBGrid2: TdmkDBGridZTO
                      Left = 2
                      Top = 59
                      Width = 225
                      Height = 412
                      Align = alClient
                      DataSource = DsEFD_E500
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                      Columns = <
                        item
                          Alignment = taCenter
                          Expanded = False
                          FieldName = 'DT_INI'
                          Title.Caption = 'In'#237'cio'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DT_FIN'
                          Title.Caption = 'Final'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_IND_APUR'
                          Title.Caption = 'Ind. Apur.'
                          Width = 67
                          Visible = True
                        end>
                    end
                    object Panel33: TPanel
                      Left = 2
                      Top = 15
                      Width = 225
                      Height = 44
                      Align = alTop
                      BevelOuter = bvNone
                      TabOrder = 1
                      object BtE500: TBitBtn
                        Tag = 10078
                        Left = 56
                        Top = 1
                        Width = 120
                        Height = 40
                        Cursor = crHandPoint
                        Caption = '&Intervalo'
                        Enabled = False
                        NumGlyphs = 2
                        ParentShowHint = False
                        ShowHint = True
                        TabOrder = 0
                        OnClick = BtE500Click
                      end
                    end
                  end
                  object PCE500: TPageControl
                    Left = 229
                    Top = 0
                    Width = 659
                    Height = 473
                    ActivePage = TabSheet9
                    Align = alClient
                    TabOrder = 1
                    object TabSheet8: TTabSheet
                      Caption = 'E510 :: Consolida'#231#227'o dos valores do IPI'
                      ExplicitLeft = 0
                      ExplicitTop = 0
                      ExplicitWidth = 0
                      ExplicitHeight = 0
                      object dmkDBGridZTO2: TdmkDBGridZTO
                        Left = 0
                        Top = 0
                        Width = 651
                        Height = 445
                        Align = alClient
                        DataSource = DsEFD_E510
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -12
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        RowColors = <>
                      end
                    end
                    object TabSheet9: TTabSheet
                      Caption = 'E520 :: Apura'#231#227'o do IPI'
                      ImageIndex = 1
                      ExplicitLeft = 0
                      ExplicitTop = 0
                      ExplicitWidth = 0
                      ExplicitHeight = 0
                      object Panel19: TPanel
                        Left = 0
                        Top = 0
                        Width = 501
                        Height = 445
                        Align = alLeft
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 0
                        object dmkDBGridZTO1: TdmkDBGridZTO
                          Left = 0
                          Top = 0
                          Width = 501
                          Height = 445
                          Align = alClient
                          DataSource = DsEFD_E520
                          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'VL_SD_ANT_IPI'
                              Title.Caption = 'Sdo cred ant.'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'VL_DEB_IPI'
                              Title.Caption = 'D'#233'bitos'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'VL_CRED_IPI'
                              Title.Caption = 'Cr'#233'ditos'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'VL_OD_IPI'
                              Title.Caption = 'Outros d'#233'b.'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'VL_OC_IPI'
                              Title.Caption = 'Outros cr'#233'd.'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'VL_SC_IPI'
                              Title.Caption = 'Sdo credor'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'VL_SD_IPI'
                              Title.Caption = 'Sdo devedor'
                              Visible = True
                            end>
                        end
                      end
                      object Panel20: TPanel
                        Left = 501
                        Top = 0
                        Width = 150
                        Height = 445
                        Align = alClient
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 1
                        object Label26: TLabel
                          Left = 0
                          Top = 0
                          Width = 169
                          Height = 13
                          Align = alTop
                          Caption = ' E530 :: Ajustes da Apura'#231#227'o do IPI'
                        end
                        object dmkDBGridZTO3: TdmkDBGridZTO
                          Left = 0
                          Top = 13
                          Width = 150
                          Height = 432
                          Align = alClient
                          DataSource = DsEFD_E530
                          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                        end
                      end
                    end
                  end
                end
                object Panel23: TPanel
                  Left = 0
                  Top = 473
                  Width = 888
                  Height = 43
                  Align = alBottom
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 1
                  ExplicitTop = 479
                  ExplicitWidth = 891
                  object BtE520: TBitBtn
                    Tag = 10079
                    Left = 2
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Apura'#231#227'o'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtE520Click
                  end
                  object BtE530: TBitBtn
                    Tag = 10080
                    Left = 122
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'A&juste'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 1
                    OnClick = BtE530Click
                  end
                end
              end
              object TabSheet10: TTabSheet
                Caption = 'H - Invent'#225'rio'
                ImageIndex = 2
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel21: TPanel
                  Left = 0
                  Top = 0
                  Width = 888
                  Height = 516
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object GroupBox4: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 149
                    Height = 382
                    Align = alLeft
                    Caption = ' H005 :: Totais do Invent'#225'rio'
                    TabOrder = 0
                    object dmkDBGridZTO4: TdmkDBGridZTO
                      Left = 2
                      Top = 15
                      Width = 145
                      Height = 365
                      Align = alClient
                      DataSource = DsEFD_H005
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'DT_INV'
                          Title.Caption = 'Data'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'MOT_INV'
                          Title.Caption = 'Motivo'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_MOT_INV'
                          Title.Caption = 'Descri'#231#227'o do motivo'
                          Visible = True
                        end>
                    end
                  end
                  object Panel24: TPanel
                    Left = 0
                    Top = 472
                    Width = 888
                    Height = 44
                    Align = alBottom
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 1
                    object BtH010: TBitBtn
                      Tag = 10079
                      Left = 124
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Itens'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 1
                      OnClick = BtH010Click
                    end
                    object BtH005: TBitBtn
                      Tag = 10078
                      Left = 4
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Data'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = BtH005Click
                    end
                    object BtH020: TBitBtn
                      Tag = 10080
                      Left = 244
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Complem.'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 2
                      OnClick = BtH020Click
                    end
                  end
                  object Panel25: TPanel
                    Left = 149
                    Top = 0
                    Width = 739
                    Height = 382
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 2
                    object Label28: TLabel
                      Left = 0
                      Top = 0
                      Width = 88
                      Height = 13
                      Align = alTop
                      Caption = ' H010 :: Invent'#225'rio'
                    end
                    object DBGH010: TdmkDBGridZTO
                      Left = 0
                      Top = 13
                      Width = 739
                      Height = 369
                      Align = alClient
                      DataSource = DsEFD_H010
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'NO_PART'
                          Title.Caption = 'Terceiro'
                          Width = 120
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'COD_ITEM'
                          Title.Caption = 'C'#243'd. item'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_PRD_TAM_COR'
                          Title.Caption = 'Nome item'
                          Width = 180
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'IND_PROP'
                          Title.Caption = 'Ind.Prop.'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'QTD'
                          Title.Caption = 'Qtde'
                          Width = 68
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'UNID'
                          Title.Caption = 'Unidade'
                          Width = 46
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'VL_UNIT'
                          Title.Caption = 'Valor unit'#225'rio'
                          Width = 72
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'VL_ITEM'
                          Title.Caption = 'Valor item'
                          Width = 80
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'VL_ITEM_IR'
                          Title.Caption = 'Valor IR'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'TXT_COMPL'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'COD_CTA'
                          Title.Caption = 'Conta (Plano de contas)'
                          Width = 72
                          Visible = True
                        end>
                    end
                  end
                  object Panel26: TPanel
                    Left = 0
                    Top = 382
                    Width = 888
                    Height = 90
                    Align = alBottom
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 3
                    object Label27: TLabel
                      Left = 0
                      Top = 0
                      Width = 222
                      Height = 13
                      Align = alTop
                      Caption = ' H020: Informa'#231#227'o complementar do Invent'#225'rio'
                    end
                    object dmkDBGridZTO6: TdmkDBGridZTO
                      Left = 0
                      Top = 13
                      Width = 888
                      Height = 77
                      Align = alClient
                      DataSource = DsEFD_H020
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'LinArq'
                          Title.Caption = 'ID'
                          Width = 62
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'CST_ICMS'
                          Title.Caption = 'CST ICMS'
                          Width = 54
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'BC_ICMS'
                          Title.Caption = 'BC ICMS'
                          Width = 72
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'VL_ICMS'
                          Title.Caption = 'Valor ICMS'
                          Width = 72
                          Visible = True
                        end>
                    end
                  end
                end
              end
              object TabSheet11: TTabSheet
                Caption = 'K - Produ'#231#227'o e Estoque'
                ImageIndex = 3
                object Panel27: TPanel
                  Left = 0
                  Top = 473
                  Width = 888
                  Height = 43
                  Align = alBottom
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object BtK200: TBitBtn
                    Tag = 10079
                    Left = 2
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Estoque'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtK200Click
                  end
                  object BtK220: TBitBtn
                    Tag = 10079
                    Left = 122
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Classe*'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 1
                    OnClick = BtK220Click
                  end
                  object BtK230: TBitBtn
                    Tag = 10079
                    Left = 242
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Produ'#231#227'o'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 2
                    OnClick = BtK230Click
                  end
                  object BtVerifica: TBitBtn
                    Tag = 10079
                    Left = 482
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Verifica'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 4
                    OnClick = BtVerificaClick
                  end
                  object BtK280: TBitBtn
                    Tag = 10079
                    Left = 362
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'CAEE*'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 3
                    OnClick = BtK280Click
                  end
                end
                object PCK200: TPageControl
                  Left = 173
                  Top = 0
                  Width = 715
                  Height = 473
                  ActivePage = TabSheet25
                  Align = alClient
                  TabOrder = 1
                  OnChange = PCK200Change
                  object TabSheet12: TTabSheet
                    Caption = 'K200 - Estoque escriturado'
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object DBGK200: TdmkDBGridZTO
                      Left = 0
                      Top = 0
                      Width = 707
                      Height = 445
                      Align = alClient
                      DataSource = DsEFD_K200
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'NO_PART'
                          Title.Caption = 'Terceiro'
                          Width = 120
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'COD_ITEM'
                          Title.Caption = 'C'#243'd. item'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_PRD_TAM_COR'
                          Title.Caption = 'Nome item'
                          Width = 180
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Sigla'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'QTD'
                          Title.Caption = 'Qtde'
                          Width = 68
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_IND_EST'
                          Title.Caption = 'Ind.Prop.'
                          Visible = True
                        end>
                    end
                  end
                  object TabSheet19: TTabSheet
                    Caption = 'K210 - Desmontagem'
                    ImageIndex = 1
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object PageControl2: TPageControl
                      Left = 0
                      Top = 0
                      Width = 707
                      Height = 445
                      ActivePage = TabSheet23
                      Align = alClient
                      TabOrder = 0
                      object TabSheet22: TTabSheet
                        Caption = 'K210 / K215'
                        ExplicitLeft = 0
                        ExplicitTop = 0
                        ExplicitWidth = 281
                        ExplicitHeight = 165
                        object Splitter6: TSplitter
                          Left = 0
                          Top = 232
                          Width = 699
                          Height = 5
                          Cursor = crVSplit
                          Align = alBottom
                        end
                        object dmkDBGridZTO8: TdmkDBGridZTO
                          Left = 0
                          Top = 0
                          Width = 699
                          Height = 232
                          Align = alClient
                          DataSource = DsEFD_K210
                          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          OnDblClick = dmkDBGridZTO8DblClick
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'DT_INI_OS'
                              Title.Caption = 'Data Ini OP'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'DT_FIN_OS_Txt'
                              Title.Caption = 'Data Fim OP'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_DOC_OS'
                              Title.Caption = 'ID OP'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM_ORI'
                              Title.Caption = 'ID Item'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome Item'
                              Width = 266
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_ORI'
                              Title.Caption = 'Qtde acabada'
                              Width = 105
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'OriOpeProc'
                              Visible = True
                            end>
                        end
                        object dmkDBGridZTO9: TdmkDBGridZTO
                          Left = 0
                          Top = 237
                          Width = 699
                          Height = 180
                          Align = alBottom
                          DataSource = DsEFD_K215
                          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                          TabOrder = 1
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'ID_Item'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM_DES'
                              Title.Caption = 'Reduzido'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome / tamanho / cor'
                              Width = 336
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_DES'
                              Title.Caption = 'Qtde'
                              Width = 72
                              Visible = True
                            end>
                        end
                      end
                      object TabSheet23: TTabSheet
                        Caption = 'K270 / K275'
                        ImageIndex = 1
                        ExplicitLeft = 0
                        ExplicitTop = 0
                        ExplicitWidth = 0
                        ExplicitHeight = 0
                        object Splitter10: TSplitter
                          Left = 0
                          Top = 232
                          Width = 699
                          Height = 5
                          Cursor = crVSplit
                          Align = alBottom
                        end
                        object dmkDBGridZTO15: TdmkDBGridZTO
                          Left = 0
                          Top = 0
                          Width = 699
                          Height = 232
                          Align = alClient
                          DataSource = DsEFD_K270_210
                          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          OnDblClick = dmkDBGridZTO8DblClick
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'DT_INI_AP'
                              Title.Caption = 'Data Ini AP'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'DT_FIN_AP'
                              Title.Caption = 'Data Fim AP'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_OP_OS'
                              Title.Caption = 'ID OP'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'ID Item'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome Item'
                              Width = 266
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_POS'
                              Title.Caption = 'Qtde positiva'
                              Width = 105
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_NEG'
                              Title.Caption = 'Qtd negativa'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'ORIGEM'
                              Visible = True
                            end>
                        end
                        object dmkDBGridZTO16: TdmkDBGridZTO
                          Left = 0
                          Top = 237
                          Width = 699
                          Height = 180
                          Align = alBottom
                          DataSource = DsEFD_K275_215
                          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                          TabOrder = 1
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'ID_Item'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'Reduzido'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome / tamanho / cor'
                              Width = 336
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_NEG'
                              Title.Caption = 'Qtd negativa'
                              Width = 72
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_POS'
                              Title.Caption = 'Qtd positiva'
                              Visible = True
                            end>
                        end
                      end
                    end
                  end
                  object TabSheet13: TTabSheet
                    Caption = 'K220 - *OMIEM - Outras movimenta'#231#245'es internas entre mercadorias.'
                    ImageIndex = 2
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object Splitter8: TSplitter
                      Left = 0
                      Top = 202
                      Width = 707
                      Height = 5
                      Cursor = crVSplit
                      Align = alBottom
                    end
                    object Label31: TLabel
                      Left = 0
                      Top = 207
                      Width = 408
                      Height = 13
                      Align = alBottom
                      Caption = 
                        'K270 - Corre'#231#227'o de Apontamento - Outras movimenta'#231#245'es internas e' +
                        'ntre mercadorias. '
                    end
                    object DBGK220: TdmkDBGridZTO
                      Left = 0
                      Top = 0
                      Width = 707
                      Height = 202
                      Align = alClient
                      DataSource = DsEFD_K220
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'DT_MOV'
                          Title.Caption = 'Data mov.'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'QTD'
                          Title.Caption = 'Qtde.'
                          Width = 80
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'COD_ITEM_ORI'
                          Title.Caption = 'GGX ori'
                          Width = 44
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_PRD_TAM_COR_ORI'
                          Title.Caption = 'Nome produto origem'
                          Width = 220
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'COD_ITEM_DEST'
                          Title.Caption = 'GGX dst'
                          Width = 44
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_PRD_TAM_COR_DEST'
                          Title.Caption = 'Nome produto destino'
                          Width = 220
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ID_SEK'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'LinArq'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'MovimID'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ESOMIEM'
                          Visible = True
                        end>
                    end
                    object DBGK270_220: TdmkDBGridZTO
                      Left = 0
                      Top = 220
                      Width = 707
                      Height = 136
                      Align = alBottom
                      DataSource = DsEFD_K270_220
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                      TabOrder = 1
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'DT_INI_AP'
                          Title.Caption = 'Data mov.'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DT_INI_AP'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Sigla'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'QTD_COR_POS'
                          Title.Caption = 'Qtde.'
                          Width = 80
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'QTD_COR_NEG'
                          Title.Caption = 'Qtd desfeito'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'COD_ITEM'
                          Title.Caption = 'GGX ori'
                          Width = 44
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_PRD_TAM_COR'
                          Title.Caption = 'Nome produto origem'
                          Width = 220
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ID_SEK'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'LinArq'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'MovimID'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'COD_OP_OS'
                          Visible = True
                        end>
                    end
                    object DBGK275_220: TdmkDBGridZTO
                      Left = 0
                      Top = 356
                      Width = 707
                      Height = 89
                      Align = alBottom
                      DataSource = DsEFD_K275_220
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                      TabOrder = 2
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'Sigla'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'QTD_COR_POS'
                          Title.Caption = 'Qtde.'
                          Width = 80
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'QTD_COR_NEG'
                          Title.Caption = 'Qtd desfeito'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'COD_ITEM'
                          Title.Caption = 'GGX ori'
                          Width = 44
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_PRD_TAM_COR'
                          Title.Caption = 'Nome produto origem'
                          Width = 220
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ID_SEK'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'LinArq'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'MovimID'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ID_Item'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'COD_INS_SUBST'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ESTSTabSorc'
                          Visible = True
                        end>
                    end
                  end
                  object TabSheet16: TTabSheet
                    Caption = 'K230 a 275 - Produ'#231#227'o'
                    ImageIndex = 3
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object PCK235: TPageControl
                      Left = 0
                      Top = 0
                      Width = 707
                      Height = 445
                      ActivePage = TabSheet26
                      Align = alClient
                      TabOrder = 0
                      object TabSheet17: TTabSheet
                        Caption = 'K230 - Itens produzidos'
                        ExplicitLeft = 0
                        ExplicitTop = 0
                        ExplicitWidth = 0
                        ExplicitHeight = 0
                        object Splitter3: TSplitter
                          Left = 0
                          Top = 232
                          Width = 699
                          Height = 5
                          Cursor = crVSplit
                          Align = alBottom
                        end
                        object DBGK230: TdmkDBGridZTO
                          Left = 0
                          Top = 0
                          Width = 699
                          Height = 232
                          Align = alClient
                          DataSource = DsEFD_K230
                          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          OnDblClick = DBGK230DblClick
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'DT_INI_OP'
                              Title.Caption = 'Data Ini OP'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'DT_FIN_OP_Txt'
                              Title.Caption = 'Data Fim OP'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_DOC_OP'
                              Title.Caption = 'ID OP'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'ID Item'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome Item'
                              Width = 266
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_ENC'
                              Title.Caption = 'Qtde acabada'
                              Width = 105
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'OriOpeProc'
                              Visible = True
                            end>
                        end
                        object DBGK235: TdmkDBGridZTO
                          Left = 0
                          Top = 237
                          Width = 699
                          Height = 180
                          Align = alBottom
                          DataSource = DsEFD_K235
                          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                          TabOrder = 1
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'ID_Item'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'DT_SAIDA'
                              Title.Caption = 'Data sa'#237'da'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'Reduzido'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome / tamanho / cor'
                              Width = 336
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD'
                              Title.Caption = 'Qtde'
                              Width = 72
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_INS_SUBST'
                              Width = 90
                              Visible = True
                            end>
                        end
                      end
                      object TabSheet18: TTabSheet
                        Caption = 'K250 - Itens produzidos por Terceiros'
                        ImageIndex = 1
                        ExplicitLeft = 0
                        ExplicitTop = 0
                        ExplicitWidth = 0
                        ExplicitHeight = 0
                        object Splitter4: TSplitter
                          Left = 0
                          Top = 232
                          Width = 699
                          Height = 5
                          Cursor = crVSplit
                          Align = alBottom
                        end
                        object DBGK250: TdmkDBGridZTO
                          Left = 0
                          Top = 0
                          Width = 699
                          Height = 232
                          Align = alClient
                          DataSource = DsEFD_K250
                          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          OnDblClick = DBGK250DblClick
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'DT_PROD'
                              Title.Caption = 'Data prod.'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_DOC_OP'
                              Title.Caption = 'ID OP'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'ID Item'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome Item'
                              Width = 266
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD'
                              Title.Caption = 'Qtde acabada'
                              Width = 105
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'OriOpeProc'
                              Visible = True
                            end>
                        end
                        object DBGK255: TdmkDBGridZTO
                          Left = 0
                          Top = 237
                          Width = 699
                          Height = 180
                          Align = alBottom
                          DataSource = DsEFD_K255
                          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                          TabOrder = 1
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'ID_Item'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'DT_CONS'
                              Title.Caption = 'Data sa'#237'da'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'Reduzido'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome / tamanho / cor'
                              Width = 336
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD'
                              Title.Caption = 'Qtde'
                              Width = 72
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_INS_SUBST'
                              Width = 90
                              Visible = True
                            end>
                        end
                      end
                      object TabSheet24: TTabSheet
                        Caption = 'K260 - Reprocessamento/reparo de produto/insumo'
                        ImageIndex = 2
                        ExplicitLeft = 0
                        ExplicitTop = 0
                        ExplicitWidth = 0
                        ExplicitHeight = 0
                        object Splitter11: TSplitter
                          Left = 0
                          Top = 292
                          Width = 699
                          Height = 5
                          Cursor = crVSplit
                          Align = alBottom
                        end
                        object DBGK260: TdmkDBGridZTO
                          Left = 0
                          Top = 0
                          Width = 699
                          Height = 292
                          Align = alClient
                          DataSource = DsEFD_K260
                          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          Columns = <
                            item
                              Alignment = taCenter
                              Expanded = False
                              FieldName = 'DT_SAIDA'
                              Title.Caption = 'Data sa'#237'da'
                              Width = 62
                              Visible = True
                            end
                            item
                              Alignment = taRightJustify
                              Expanded = False
                              FieldName = 'COD_OP_OS'
                              Title.Caption = 'ID OP/OO'
                              Width = 56
                              Visible = True
                            end
                            item
                              Alignment = taRightJustify
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'ID Item'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Produto'
                              Width = 266
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_SAIDA'
                              Title.Caption = 'Qtd sa'#237'da'
                              Width = 72
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'DT_RET_Txt'
                              Title.Caption = 'Data ret.'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_RET'
                              Title.Caption = 'Qtd retorno'
                              Width = 72
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'OriOpeProc'
                              Width = 48
                              Visible = True
                            end>
                        end
                        object DBGK265: TdmkDBGridZTO
                          Left = 0
                          Top = 297
                          Width = 699
                          Height = 120
                          Align = alBottom
                          DataSource = DsEFD_K265
                          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                          TabOrder = 1
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          Columns = <
                            item
                              Alignment = taRightJustify
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'ID Item'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Produto'
                              Width = 266
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_CONS'
                              Title.Caption = 'Qtd Consumido'
                              Width = 100
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_RET'
                              Title.Caption = 'Qtd retornado'
                              Width = 100
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'OriOpeProc'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'ESTSTabSorc'
                              Width = 56
                              Visible = True
                            end>
                        end
                      end
                      object TabSheet20: TTabSheet
                        Caption = 'K270 - Corre'#231#227'o de Apontamento K230'
                        ImageIndex = 3
                        ExplicitLeft = 0
                        ExplicitTop = 0
                        ExplicitWidth = 0
                        ExplicitHeight = 0
                        object Splitter7: TSplitter
                          Left = 0
                          Top = 232
                          Width = 699
                          Height = 5
                          Cursor = crVSplit
                          Align = alBottom
                        end
                        object DBGK270_230: TdmkDBGridZTO
                          Left = 0
                          Top = 0
                          Width = 699
                          Height = 232
                          Align = alClient
                          DataSource = DsEFD_K270_230
                          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          OnDblClick = DBGK270_230DblClick
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'DT_INI_AP'
                              Title.Caption = 'Data Ini OP'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'DT_INI_AP'
                              Title.Caption = 'Data Fim OP'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_OP_OS'
                              Title.Caption = 'ID OP'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'ID Item'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome Item'
                              Width = 266
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_NEG'
                              Title.Caption = 'Qtd desfeito'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_POS'
                              Title.Caption = 'Qtde produzido'
                              Width = 105
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'ORIGEM'
                              Visible = True
                            end>
                        end
                        object dmkDBGridZTO11: TdmkDBGridZTO
                          Left = 0
                          Top = 237
                          Width = 699
                          Height = 180
                          Align = alBottom
                          DataSource = DsEFD_K275_235
                          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                          TabOrder = 1
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'ID_Item'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'Reduzido'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome / tamanho / cor'
                              Width = 336
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_POS'
                              Title.Caption = 'Qtde retorno estq'
                              Width = 72
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_NEG'
                              Title.Caption = 'Qtd consumido'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_INS_SUBST'
                              Width = 90
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'ESTSTabSorc'
                              Visible = True
                            end>
                        end
                      end
                      object TabSheet21: TTabSheet
                        Caption = 'K270 - Corre'#231#227'o de Apontamento K250'
                        ImageIndex = 3
                        ExplicitLeft = 0
                        ExplicitTop = 0
                        ExplicitWidth = 0
                        ExplicitHeight = 0
                        object Splitter9: TSplitter
                          Left = 0
                          Top = 232
                          Width = 699
                          Height = 5
                          Cursor = crVSplit
                          Align = alBottom
                        end
                        object dmkDBGridZTO13: TdmkDBGridZTO
                          Left = 0
                          Top = 0
                          Width = 699
                          Height = 232
                          Align = alClient
                          DataSource = DsEFD_K270_250
                          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          OnDblClick = DBGK270_230DblClick
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'DT_INI_AP'
                              Title.Caption = 'Data Ini OP'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'DT_INI_AP'
                              Title.Caption = 'Data Fim OP'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_OP_OS'
                              Title.Caption = 'ID OP'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'ID Item'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome Item'
                              Width = 266
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_NEG'
                              Title.Caption = 'Qtd Desfeito'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_POS'
                              Title.Caption = 'Qtde produzido'
                              Width = 105
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'ORIGEM'
                              Visible = True
                            end>
                        end
                        object dmkDBGridZTO14: TdmkDBGridZTO
                          Left = 0
                          Top = 237
                          Width = 699
                          Height = 180
                          Align = alBottom
                          DataSource = DsEFD_K275_255
                          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                          TabOrder = 1
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'ID_Item'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'Reduzido'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome / tamanho / cor'
                              Width = 336
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_POS'
                              Title.Caption = 'Retorno estq'
                              Width = 72
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_NEG'
                              Title.Caption = 'Qtd consumido'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_INS_SUBST'
                              Width = 90
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'ESTSTabSorc'
                              Visible = True
                            end>
                        end
                      end
                      object TabSheet26: TTabSheet
                        Caption = 'K270 - Corre'#231#227'o de Apontamento K260'
                        ImageIndex = 5
                        ExplicitLeft = 0
                        ExplicitTop = 0
                        ExplicitWidth = 0
                        ExplicitHeight = 0
                        object Splitter12: TSplitter
                          Left = 0
                          Top = 232
                          Width = 699
                          Height = 5
                          Cursor = crVSplit
                          Align = alBottom
                          ExplicitTop = 0
                        end
                        object dmkDBGridZTO10: TdmkDBGridZTO
                          Left = 0
                          Top = 237
                          Width = 699
                          Height = 180
                          Align = alBottom
                          DataSource = DsEFD_K275_265
                          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'ID_Item'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'Reduzido'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome / tamanho / cor'
                              Width = 336
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_POS'
                              Title.Caption = 'Retorno estq'
                              Width = 72
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_NEG'
                              Title.Caption = 'Qtd consumido'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_INS_SUBST'
                              Width = 90
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'ESTSTabSorc'
                              Visible = True
                            end>
                        end
                        object dmkDBGridZTO12: TdmkDBGridZTO
                          Left = 0
                          Top = 0
                          Width = 699
                          Height = 232
                          Align = alClient
                          DataSource = DsEFD_K270_260
                          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                          TabOrder = 1
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -12
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          RowColors = <>
                          OnDblClick = DBGK270_230DblClick
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'DT_INI_AP'
                              Title.Caption = 'Data Ini OP'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'DT_INI_AP'
                              Title.Caption = 'Data Fim OP'
                              Width = 62
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_OP_OS'
                              Title.Caption = 'ID OP'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'COD_ITEM'
                              Title.Caption = 'ID Item'
                              Width = 56
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_PRD_TAM_COR'
                              Title.Caption = 'Nome Item'
                              Width = 266
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Sigla'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_NEG'
                              Title.Caption = 'Qtd Desfeito'
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'QTD_COR_POS'
                              Title.Caption = 'Qtde produzido'
                              Width = 105
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'ORIGEM'
                              Visible = True
                            end>
                        end
                      end
                    end
                  end
                  object TabSheet25: TTabSheet
                    Caption = 'K280 - Corre'#231#227'o de estoque'
                    ImageIndex = 4
                    object DBGK280: TdmkDBGridZTO
                      Left = 0
                      Top = 0
                      Width = 707
                      Height = 445
                      Align = alClient
                      DataSource = DsEFD_K280
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -12
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'NO_PART'
                          Title.Caption = 'Terceiro'
                          Width = 120
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'COD_ITEM'
                          Title.Caption = 'C'#243'd. item'
                          Width = 56
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_PRD_TAM_COR'
                          Title.Caption = 'Nome item'
                          Width = 180
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Sigla'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'QTD_COR_NEG'
                          Title.Caption = 'Qtde neg.'
                          Width = 68
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'QTD_COR_POS'
                          Title.Caption = 'Qtd pos.'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_IND_EST'
                          Title.Caption = 'Ind.Prop.'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DT_EST'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_OriSPEDEFDKnd'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'RegisPai'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'RegisAvo'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'BalID'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'BalNum'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'BalItm'
                          Visible = True
                        end>
                    end
                  end
                end
                object Panel30: TPanel
                  Left = 0
                  Top = 0
                  Width = 173
                  Height = 473
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 2
                  object Panel31: TPanel
                    Left = 0
                    Top = 0
                    Width = 173
                    Height = 44
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 0
                    object BtK100: TBitBtn
                      Tag = 10078
                      Left = 24
                      Top = 0
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Intervalo'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = BtK100Click
                    end
                  end
                  object dmkDBGridZTO5: TdmkDBGridZTO
                    Left = 0
                    Top = 44
                    Width = 173
                    Height = 429
                    Align = alClient
                    DataSource = DsEFD_K100
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                    TabOrder = 1
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -12
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'DT_INI'
                        Title.Caption = 'In'#237'cio'
                        Width = 56
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DT_FIN'
                        Title.Caption = 'Final'
                        Visible = True
                      end>
                  end
                end
              end
              object TabSheet14: TTabSheet
                Caption = 'Verifica'#231#227'o K'
                ImageIndex = 4
                object Splitter5: TSplitter
                  Left = 0
                  Top = 310
                  Width = 888
                  Height = 5
                  Cursor = crVSplit
                  Align = alBottom
                  ExplicitTop = 311
                end
                object dmkDBGridZTO7: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 888
                  Height = 310
                  Align = alClient
                  DataSource = DsEFD_K_ConfG
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  OnCellClick = dmkDBGridZTO7CellClick
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Reduzido'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PRD_TAM_COR'
                      Title.Caption = 'Nome / tamanho / cor'
                      Width = 300
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Sigla'
                      Width = 32
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Sdo_Ini'
                      Title.Caption = 'Sdo Inicial'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Compra'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'EntrouDesmonte'
                      Title.Caption = 'Entrou Desmonte'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Producao'
                      Title.Caption = 'Produ'#231#227'o'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'EntrouClasse'
                      Title.Caption = 'Entrou classe'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ProduReforma'
                      Title.Caption = 'Prod. reforma'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SaiuDesmonte'
                      Title.Caption = 'Saiu Desmonte'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Consumo'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SaiuClasse'
                      Title.Caption = 'Saiu classe'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'InsumReforma'
                      Title.Caption = 'Insum. reforma'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Venda'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Final'
                      Width = 72
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ETE'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SubProduto'
                      Title.Caption = 'Sub-produto'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'BalancoIndev'
                      Title.Caption = 'Balan'#231'o Indev.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Indevido'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ErrEmpresa'
                      Title.Caption = 'Erro Empresa'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Diferenca'
                      Title.Caption = 'Diferen'#231'a'
                      Width = 72
                      Visible = True
                    end>
                end
                object Panel28: TPanel
                  Left = 0
                  Top = 472
                  Width = 888
                  Height = 44
                  Align = alBottom
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 1
                  object BtDifGera: TBitBtn
                    Tag = 10078
                    Left = 4
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Gera'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtDifGeraClick
                  end
                  object BtDifImprime: TBitBtn
                    Tag = 5
                    Left = 124
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Imprime'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 1
                    OnClick = BtDifImprimeClick
                  end
                  object BitBtn1: TBitBtn
                    Tag = 5
                    Left = 244
                    Top = 2
                    Width = 120
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Conf. Mov'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 2
                    OnClick = BitBtn1Click
                  end
                end
                object Panel34: TPanel
                  Left = 0
                  Top = 315
                  Width = 888
                  Height = 157
                  Align = alBottom
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 2
                  object DBGPsq01: TdmkDBGridZTO
                    Left = 0
                    Top = 0
                    Width = 456
                    Height = 157
                    Align = alClient
                    DataSource = DsPsq01
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -12
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    OnDblClick = DBGPsq02DblClick
                  end
                  object DBGPsq02: TdmkDBGridZTO
                    Left = 456
                    Top = 0
                    Width = 432
                    Height = 157
                    Align = alRight
                    DataSource = DsPsq02
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 1
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -12
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Visible = False
                    RowColors = <>
                    OnDblClick = DBGPsq02DblClick
                  end
                end
              end
              object TabSheet15: TTabSheet
                Caption = '1 - Outros'
                ImageIndex = 5
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel35: TPanel
                  Left = 0
                  Top = 0
                  Width = 888
                  Height = 517
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Panel36: TPanel
                    Left = 0
                    Top = 473
                    Width = 888
                    Height = 44
                    Align = alBottom
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Bt1010: TBitBtn
                      Tag = 10079
                      Left = 4
                      Top = 2
                      Width = 120
                      Height = 40
                      Cursor = crHandPoint
                      Caption = '&Obriga'#231#227'o'
                      Enabled = False
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = Bt1010Click
                    end
                  end
                  object Panel37: TPanel
                    Left = 0
                    Top = 0
                    Width = 888
                    Height = 473
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 1
                    object CkIND_EXP: TDBCheckBox
                      Left = 8
                      Top = 4
                      Width = 732
                      Height = 17
                      Caption = '1100 - Ocorreu averba'#231#227'o (conclus'#227'o) de exporta'#231#227'o no per'#237'odo.'
                      DataField = 'IND_EXP'
                      DataSource = DsEFD_1010
                      TabOrder = 0
                      ValueChecked = 'S'
                      ValueUnchecked = 'N'
                    end
                    object CkIND_CCRF: TDBCheckBox
                      Left = 8
                      Top = 24
                      Width = 732
                      Height = 17
                      Caption = 
                        '1200 - Existem informa'#231#245'es acerca de cr'#233'ditos de ICMS a serem co' +
                        'ntrolados, definidos pela Sefaz.'
                      DataField = 'IND_CCRF'
                      DataSource = DsEFD_1010
                      Enabled = False
                      TabOrder = 1
                      ValueChecked = 'S'
                      ValueUnchecked = 'N'
                    end
                    object CkIND_COMB: TDBCheckBox
                      Left = 8
                      Top = 44
                      Width = 732
                      Height = 17
                      Caption = 
                        '1300 - '#201' comercio varejista de combust'#237'veis com movimenta'#231#227'o e/o' +
                        'u estoque no per'#237'odo.'
                      DataField = 'IND_COMB'
                      DataSource = DsEFD_1010
                      Enabled = False
                      TabOrder = 2
                      ValueChecked = 'S'
                      ValueUnchecked = 'N'
                    end
                    object CkIND_USINA: TDBCheckBox
                      Left = 8
                      Top = 64
                      Width = 732
                      Height = 17
                      Caption = 
                        '1390 - Usinas de a'#231#250'car e/'#225'lcool - O estabelecimento '#233' produtor ' +
                        'de a'#231#250'car e/ou '#225'lcool carburante com movimenta'#231#227'o e/ou estoque n' +
                        'o per'#237'odo.'
                      DataField = 'IND_USINA'
                      DataSource = DsEFD_1010
                      Enabled = False
                      TabOrder = 3
                      ValueChecked = 'S'
                      ValueUnchecked = 'N'
                    end
                    object CkIND_VA: TDBCheckBox
                      Left = 8
                      Top = 84
                      Width = 732
                      Height = 17
                      Caption = 
                        '1400 - Sendo o registro obrigat'#243'rio em sua Unidade de Federa'#231#227'o,' +
                        ' existem informa'#231#245'es a serem prestadas neste registro.'
                      DataField = 'IND_VA'
                      DataSource = DsEFD_1010
                      Enabled = False
                      TabOrder = 4
                      ValueChecked = 'S'
                      ValueUnchecked = 'N'
                    end
                    object CkIND_EE: TDBCheckBox
                      Left = 8
                      Top = 104
                      Width = 732
                      Height = 17
                      Caption = 
                        '1500 - A empresa '#233' distribuidora de energia e ocorreu fornecimen' +
                        'to de energia el'#233'trica para consumidores de outra UF.'
                      DataField = 'IND_EE'
                      DataSource = DsEFD_1010
                      Enabled = False
                      TabOrder = 5
                      ValueChecked = 'S'
                      ValueUnchecked = 'N'
                    end
                    object CkIND_CART: TDBCheckBox
                      Left = 8
                      Top = 124
                      Width = 732
                      Height = 17
                      Caption = '1600 - Realizou vendas com Cart'#227'o de Cr'#233'dito ou de d'#233'bito.'
                      DataField = 'IND_CART'
                      DataSource = DsEFD_1010
                      Enabled = False
                      TabOrder = 6
                      ValueChecked = 'S'
                      ValueUnchecked = 'N'
                    end
                    object CkIND_FORM: TDBCheckBox
                      Left = 8
                      Top = 144
                      Width = 732
                      Height = 17
                      Caption = 
                        '1700 - Foram emitidos documentos fiscais em papel no per'#237'odo em ' +
                        'unidade da federa'#231#227'o que exija o controle de utiliza'#231#227'o de docum' +
                        'entos fiscais.'
                      DataField = 'IND_FORM'
                      DataSource = DsEFD_1010
                      Enabled = False
                      TabOrder = 7
                      ValueChecked = 'S'
                      ValueUnchecked = 'N'
                    end
                    object CkIND_AER: TDBCheckBox
                      Left = 8
                      Top = 164
                      Width = 732
                      Height = 17
                      Caption = 
                        '1800 - A empresa prestou servi'#231'os de transporte a'#233'reo de cargas ' +
                        'e de passageiros.'
                      DataField = 'IND_AER'
                      DataSource = DsEFD_1010
                      Enabled = False
                      TabOrder = 8
                      ValueChecked = 'S'
                      ValueUnchecked = 'N'
                    end
                  end
                end
              end
            end
          end
          object Panel10: TPanel
            Left = 1
            Top = 1
            Width = 102
            Height = 544
            Align = alLeft
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object DBGCab: TdmkDBGrid
              Left = 0
              Top = 41
              Width = 102
              Height = 503
              Align = alClient
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'MES_ANO'
                  Title.Caption = 'MES / ANO'
                  Width = 63
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsEFD_E001
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'MES_ANO'
                  Title.Caption = 'MES / ANO'
                  Width = 63
                  Visible = True
                end>
            end
            object Panel29: TPanel
              Left = 0
              Top = 0
              Width = 102
              Height = 41
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object BtPeriodo: TBitBtn
                Tag = 10
                Left = 0
                Top = 0
                Width = 102
                Height = 41
                Cursor = crHandPoint
                Align = alClient
                Caption = '&Per'#237'odo'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtPeriodoClick
              end
            end
          end
          object TGroupBox
            Left = 1
            Top = 571
            Width = 998
            Height = 64
            Align = alBottom
            TabOrder = 2
            object Panel3: TPanel
              Left = 2
              Top = 15
              Width = 994
              Height = 47
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label29: TLabel
                Left = 149
                Top = 2
                Width = 461
                Height = 17
                Caption = 
                  '*Classe - OMIEM - Outras movimenta'#231#245'es internas entre mercadoria' +
                  's.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clSilver
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object Label30: TLabel
                Left = 148
                Top = 1
                Width = 461
                Height = 17
                Caption = 
                  '*Classe - OMIEM - Outras movimenta'#231#245'es internas entre mercadoria' +
                  's.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object Label32: TLabel
                Left = 149
                Top = 22
                Width = 377
                Height = 17
                Caption = '*CAEE - Corre'#231#227'o de Apontamento - Estoque escriturado.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clSilver
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object Label33: TLabel
                Left = 148
                Top = 21
                Width = 377
                Height = 17
                Caption = '*CAEE - Corre'#231#227'o de Apontamento - Estoque escriturado.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -15
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object Panel2: TPanel
                Left = 856
                Top = 0
                Width = 138
                Height = 47
                Align = alRight
                Alignment = taRightJustify
                BevelOuter = bvNone
                TabOrder = 0
                object BtSaida0: TBitBtn
                  Tag = 13
                  Left = 4
                  Top = 2
                  Width = 120
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Sa'#237'da'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtSaida0Click
                end
              end
            end
          end
          object Panel16: TPanel
            Left = 1
            Top = 545
            Width = 998
            Height = 26
            Align = alBottom
            TabOrder = 3
          end
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 57
      Align = alTop
      Caption = ' Empresa e per'#237'odo selecionado: '
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 40
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 23
          Height = 13
          Caption = 'Filial:'
        end
        object Label2: TLabel
          Left = 932
          Top = 0
          Width = 60
          Height = 13
          Caption = 'M'#202'S / ANO:'
          Enabled = False
          FocusControl = DBEdit1
        end
        object EdEmpresa: TdmkEditCB
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 64
          Top = 16
          Width = 865
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object DBEdit1: TDBEdit
          Left = 932
          Top = 16
          Width = 65
          Height = 21
          DataField = 'MES_ANO'
          DataSource = DsEFD_E001
          Enabled = False
          TabOrder = 2
        end
      end
    end
  end
  object Panel11: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 333
        Height = 32
        Caption = 'Apura'#231#227'o do ICMS e do IPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 333
        Height = 32
        Caption = 'Apura'#231#227'o do ICMS e do IPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 333
        Height = 32
        Caption = 'Apura'#231#227'o do ICMS e do IPI'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 65
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel12: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 48
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 31
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object DsEFD_E001: TDataSource
    DataSet = QrEFD_E001
    Left = 24
    Top = 276
  end
  object QrEFD_E001: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEFD_E001AfterOpen
    BeforeClose = QrEFD_E001BeforeClose
    AfterScroll = QrEFD_E001AfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT,'
      'CONCAT(RIGHT(e001.AnoMes, 2), "/",'
      'LEFT(LPAD(e001.AnoMes, 6, "0"), 4)) MES_ANO, e001.*'
      'FROM EFD_E001 e001'
      'LEFT JOIN entidades ent ON ent.Codigo=e001.Empresa'
      'WHERE e001.ImporExpor=1'
      'AND e001.Empresa=-11'
      'ORDER BY e001.AnoMes DESC')
    Left = 24
    Top = 228
    object QrEFD_E001NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrEFD_E001MES_ANO: TWideStringField
      FieldName = 'MES_ANO'
      Required = True
      Size = 7
    end
    object QrEFD_E001ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E001AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E001LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E001REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E001IND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtPeriodo
    Left = 68
    Top = 12
  end
  object PMPeriodo: TPopupMenu
    OnPopup = PMPeriodoPopup
    Left = 16
    Top = 632
    object Incluinovoperiodo1: TMenuItem
      Caption = 'Inclui novo periodo'
      OnClick = Incluinovoperiodo1Click
    end
    object Excluiperiodoselecionado1: TMenuItem
      Caption = '&Exclui periodo selecionado'
      Enabled = False
      OnClick = Excluiperiodoselecionado1Click
    end
    object Itemns1: TMenuItem
      Caption = '&Item(ns)'
      Enabled = False
      Visible = False
    end
  end
  object PME111: TPopupMenu
    OnPopup = PME111Popup
    Left = 924
    Top = 368
    object Incluinovoajuste1: TMenuItem
      Caption = 'E111: &Inclui novo ajuste'
      Enabled = False
      OnClick = Incluinovoajuste1Click
    end
    object Alteraajusteselecionado1: TMenuItem
      Caption = 'E111: &Altera ajuste selecionado'
      Enabled = False
      OnClick = Alteraajusteselecionado1Click
    end
    object Excluiajusteselecionado1: TMenuItem
      Caption = 'E111: &Exclui ajuste selecionado'
      Enabled = False
      OnClick = Excluiajusteselecionado1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object E113Informaesadicionais1: TMenuItem
      Caption = 'E11&2: Informa'#231#245'es adicionais'
      object Incluinovainformaoadicional1: TMenuItem
        Caption = '&Inclui nova informa'#231#227'o adicional'
        Enabled = False
        OnClick = Incluinovainformaoadicional1Click
      end
      object Alterainformaoadicionalselecionada1: TMenuItem
        Caption = '&Altera informa'#231#227'o adicional selecionada'
        Enabled = False
        OnClick = Alterainformaoadicionalselecionada1Click
      end
      object Excluiinformaoadicionalselecionada1: TMenuItem
        Caption = '&Exclui informa'#231#227'o adicional selecionada'
        Enabled = False
        OnClick = Excluiinformaoadicionalselecionada1Click
      end
    end
    object E1121: TMenuItem
      Caption = 'E11&3: Identifica'#231#227'o de documentos fiscais'
      object Incluinovaidentificaodedocumentofiscal1: TMenuItem
        Caption = '&Inclui nova identifica'#231#227'o de documento fiscal'
        Enabled = False
        OnClick = Incluinovaidentificaodedocumentofiscal1Click
      end
      object Alteraidentificaoselecionada1: TMenuItem
        Caption = '&Altera identifica'#231#227'o selecionada'
        Enabled = False
        OnClick = Alteraidentificaoselecionada1Click
      end
      object Excluiidentificaoselecionada1: TMenuItem
        Caption = '&Exclui identifica'#231#227'o selecionada'
        Enabled = False
        OnClick = Excluiidentificaoselecionada1Click
      end
    end
  end
  object PME100: TPopupMenu
    OnPopup = PME100Popup
    Left = 928
    Top = 320
    object Incluinovointervalo1: TMenuItem
      Caption = 'Inclui &novo intervalo ICMS'
      OnClick = Incluinovointervalo1Click
    end
    object Alteraintervaloselecionado1: TMenuItem
      Caption = '&Altera intervalo ICMS selecionado'
      OnClick = Alteraintervaloselecionado1Click
    end
    object Excluiintervaloselecionado1: TMenuItem
      Caption = '&Exclui intervalo ICMS selecionado'
      OnClick = Excluiintervaloselecionado1Click
    end
  end
  object QrEFD_E100: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEFD_E100AfterOpen
    BeforeClose = QrEFD_E100BeforeClose
    AfterScroll = QrEFD_E100AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e100 e100 '
      'WHERE e100.ImporExpor=3 '
      'AND e100.Empresa=-11 '
      'AND e100.AnoMes=201001'
      'ORDER BY e100.DT_INI'
      '')
    Left = 24
    Top = 324
    object QrEFD_E100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E100AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E100Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E100LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E100REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E100DT_INI: TDateField
      FieldName = 'DT_INI'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_E100DT_FIN: TDateField
      FieldName = 'DT_FIN'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsEFD_E100: TDataSource
    DataSet = QrEFD_E100
    Left = 24
    Top = 372
  end
  object QrEFD_E110: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E110BeforeClose
    AfterScroll = QrEFD_E110AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e110 e110 '
      'WHERE e110.ImporExpor=3 '
      'AND e110.Empresa=-11 '
      'AND e110.AnoMes=201001'
      'AND e110.LinArq>0')
    Left = 24
    Top = 420
    object QrEFD_E110ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E110AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E110Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E110LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E110E100: TIntegerField
      FieldName = 'E100'
    end
    object QrEFD_E110REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E110VL_TOT_DEBITOS: TFloatField
      FieldName = 'VL_TOT_DEBITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_AJ_DEBITOS: TFloatField
      FieldName = 'VL_AJ_DEBITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_TOT_AJ_DEBITOS: TFloatField
      FieldName = 'VL_TOT_AJ_DEBITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_TOT_CREDITOS: TFloatField
      FieldName = 'VL_TOT_CREDITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_ESTORNOS_CRED: TFloatField
      FieldName = 'VL_ESTORNOS_CRED'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_AJ_CREDITOS: TFloatField
      FieldName = 'VL_AJ_CREDITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_TOT_AJ_CREDITOS: TFloatField
      FieldName = 'VL_TOT_AJ_CREDITOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_ESTORNOS_DEB: TFloatField
      FieldName = 'VL_ESTORNOS_DEB'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_SLD_CREDOR_ANT: TFloatField
      FieldName = 'VL_SLD_CREDOR_ANT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_SLD_APURADO: TFloatField
      FieldName = 'VL_SLD_APURADO'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_TOT_DED: TFloatField
      FieldName = 'VL_TOT_DED'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_ICMS_RECOLHER: TFloatField
      FieldName = 'VL_ICMS_RECOLHER'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110VL_SLD_CREDOR_TRANSPORTAR: TFloatField
      FieldName = 'VL_SLD_CREDOR_TRANSPORTAR'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E110DEB_ESP: TFloatField
      FieldName = 'DEB_ESP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsEFD_E110: TDataSource
    DataSet = QrEFD_E110
    Left = 24
    Top = 468
  end
  object QrEFD_E111: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_E111BeforeClose
    AfterScroll = QrEFD_E111AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e111 e111'
      'WHERE e111.ImporExpor=3'
      'AND e111.Empresa=-11'
      'AND e111.AnoMes=201001'
      'AND e111.LinArq>0'
      '')
    Left = 24
    Top = 512
    object QrEFD_E111ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E111AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E111Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E111LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E111E110: TIntegerField
      FieldName = 'E110'
    end
    object QrEFD_E111REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E111COD_AJ_APUR: TWideStringField
      FieldName = 'COD_AJ_APUR'
      Size = 8
    end
    object QrEFD_E111DESCR_COMPL_AJ: TWideStringField
      FieldName = 'DESCR_COMPL_AJ'
      Size = 255
    end
    object QrEFD_E111VL_AJ_APUR: TFloatField
      FieldName = 'VL_AJ_APUR'
    end
  end
  object DsEFD_E111: TDataSource
    DataSet = QrEFD_E111
    Left = 24
    Top = 560
  end
  object QrEFD_E116: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e116 e116'
      'WHERE e116.ImporExpor=3'
      'AND e116.Empresa=-11'
      'AND e116.AnoMes=201001'
      'AND e116.LinArq>0'
      '')
    Left = 76
    Top = 348
    object QrEFD_E116ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E116AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E116Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E116LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E116E110: TIntegerField
      FieldName = 'E110'
    end
    object QrEFD_E116REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E116COD_OR: TWideStringField
      FieldName = 'COD_OR'
      Size = 3
    end
    object QrEFD_E116VL_OR: TFloatField
      FieldName = 'VL_OR'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEFD_E116DT_VCTO: TDateField
      FieldName = 'DT_VCTO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrEFD_E116COD_REC: TWideStringField
      FieldName = 'COD_REC'
      Size = 255
    end
    object QrEFD_E116NUM_PROC: TWideStringField
      FieldName = 'NUM_PROC'
      Size = 15
    end
    object QrEFD_E116IND_PROC: TWideStringField
      FieldName = 'IND_PROC'
      Size = 1
    end
    object QrEFD_E116PROC: TWideStringField
      FieldName = 'PROC'
      Size = 255
    end
    object QrEFD_E116TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
    object QrEFD_E116MES_REF: TDateField
      FieldName = 'MES_REF'
      DisplayFormat = 'MMYYYY'
    end
  end
  object DsEFD_E116: TDataSource
    DataSet = QrEFD_E116
    Left = 76
    Top = 396
  end
  object PMObrigacoes: TPopupMenu
    OnPopup = PMObrigacoesPopup
    Left = 504
    Top = 700
    object IncluinovaobrigaodoICMSarecolher1: TMenuItem
      Caption = 'Inclui nova obriga'#231#227'o do ICMS a recolher'
      Enabled = False
      OnClick = IncluinovaobrigaodoICMSarecolher1Click
    end
    object Alteraobrigaoselecionada1: TMenuItem
      Caption = '&Altera obriga'#231#227'o selecionada'
      Enabled = False
      OnClick = Alteraobrigaoselecionada1Click
    end
    object Excluiobrigaoselecionada1: TMenuItem
      Caption = '&Exclui obriga'#231#227'o selecionada'
      Enabled = False
      OnClick = Excluiobrigaoselecionada1Click
    end
  end
  object QrEFD_E112: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e112 e112'
      'WHERE e112.ImporExpor=3'
      'AND e112.Empresa=-11'
      'AND e112.AnoMes=201001'
      'AND e112.LinArq>0'
      '')
    Left = 76
    Top = 444
    object QrEFD_E112ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E112AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E112Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E112LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E112E111: TIntegerField
      FieldName = 'E111'
    end
    object QrEFD_E112REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E112NUM_DA: TWideStringField
      FieldName = 'NUM_DA'
      Size = 255
    end
    object QrEFD_E112NUM_PROC: TWideStringField
      FieldName = 'NUM_PROC'
      Size = 15
    end
    object QrEFD_E112IND_PROC: TWideStringField
      FieldName = 'IND_PROC'
      Size = 1
    end
    object QrEFD_E112PROC: TWideStringField
      FieldName = 'PROC'
      Size = 255
    end
    object QrEFD_E112TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
  end
  object DsEFD_E112: TDataSource
    DataSet = QrEFD_E112
    Left = 76
    Top = 492
  end
  object QrEFD_E113: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e113 e113'
      'WHERE e113.ImporExpor=3'
      'AND e113.Empresa=-11'
      'AND e113.AnoMes=201001'
      'AND e113.LinArq>0'
      '')
    Left = 76
    Top = 540
    object QrEFD_E113ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E113AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E113Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E113LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E113E111: TIntegerField
      FieldName = 'E111'
    end
    object QrEFD_E113REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E113COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_E113COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrEFD_E113SER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEFD_E113SUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEFD_E113NUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
    end
    object QrEFD_E113DT_DOC: TDateField
      FieldName = 'DT_DOC'
    end
    object QrEFD_E113COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_E113VL_AJ_ITEM: TFloatField
      FieldName = 'VL_AJ_ITEM'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsEFD_E113: TDataSource
    DataSet = QrEFD_E113
    Left = 76
    Top = 592
  end
  object QrEFD_E115: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e115 e115'
      'WHERE e115.ImporExpor=3'
      'AND e115.Empresa=-11'
      'AND e115.AnoMes=201001'
      'AND e115.LinArq>0'
      '')
    Left = 76
    Top = 252
    object QrEFD_E115ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E115AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E115Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E115LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E115E110: TIntegerField
      FieldName = 'E110'
    end
    object QrEFD_E115REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E115COD_INF_ADIC: TWideStringField
      FieldName = 'COD_INF_ADIC'
      Size = 8
    end
    object QrEFD_E115VL_INF_ADIC: TFloatField
      FieldName = 'VL_INF_ADIC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEFD_E115DESCR_COMPL_AJ: TWideStringField
      FieldName = 'DESCR_COMPL_AJ'
      Size = 255
    end
  end
  object DsEFD_E115: TDataSource
    DataSet = QrEFD_E115
    Left = 76
    Top = 300
  end
  object PMValDecltorio: TPopupMenu
    OnPopup = PMValDecltorioPopup
    Left = 504
    Top = 656
    object Incluinovovalordeclaratrio1: TMenuItem
      Caption = '&Inclui novo valor declarat'#243'rio'
      Enabled = False
      OnClick = Incluinovovalordeclaratrio1Click
    end
    object Alteravalordeclaratrioselecionado1: TMenuItem
      Caption = '&Altera valor declarat'#243'rio selecionado'
      Enabled = False
      OnClick = Alteravalordeclaratrioselecionado1Click
    end
    object Excluivalordeclaratrioselecionado1: TMenuItem
      Caption = '&Exclui valor declarat'#243'rio selecionado'
      Enabled = False
      OnClick = Excluivalordeclaratrioselecionado1Click
    end
  end
  object PME500: TPopupMenu
    OnPopup = PME500Popup
    Left = 928
    Top = 416
    object IncluinovointervaloIPI1: TMenuItem
      Caption = 'Inclui &novo intervalo IPI'
      OnClick = IncluinovointervaloIPI1Click
    end
    object AlteraintervaloIPIselecionado1: TMenuItem
      Caption = '&Altera intervalo IPI selecionado'
      OnClick = AlteraintervaloIPIselecionado1Click
    end
    object ExcluiintervaloIPIselecionado1: TMenuItem
      Caption = '&Exclui intervalo IPI selecionado'
      OnClick = ExcluiintervaloIPIselecionado1Click
    end
  end
  object QrEFD_E500: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEFD_E500AfterOpen
    BeforeClose = QrEFD_E500BeforeClose
    AfterScroll = QrEFD_E500AfterScroll
    SQL.Strings = (
      'SELECT ELT(e500.IND_APUR + 1, "Mensal",'
      '"Decendial", "???") NO_IND_APUR, e500.*'
      'FROM efd_e500 e500'
      'WHERE e500.ImporExpor=3'
      'AND e500.Empresa=-11'
      'AND e500.AnoMes=205001'
      'ORDER BY e500.DT_INI')
    Left = 128
    Top = 272
    object QrEFD_E500ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E500AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E500Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E500LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E500REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E500IND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrEFD_E500DT_INI: TDateField
      FieldName = 'DT_INI'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_E500DT_FIN: TDateField
      FieldName = 'DT_FIN'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_E500NO_IND_APUR: TWideStringField
      FieldName = 'NO_IND_APUR'
      Size = 15
    end
  end
  object DsEFD_E500: TDataSource
    DataSet = QrEFD_E500
    Left = 128
    Top = 320
  end
  object QrEFD_E510: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e510 e510'
      'WHERE e510.ImporExpor=3'
      'AND e510.Empresa=-11'
      'AND e510.AnoMes=201001'
      'AND e510.LinArq>0')
    Left = 128
    Top = 368
    object QrEFD_E510ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E510AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E510Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E510LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E510E500: TIntegerField
      FieldName = 'E500'
    end
    object QrEFD_E510REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E510CFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrEFD_E510CST_IPI: TWideStringField
      FieldName = 'CST_IPI'
      Size = 2
    end
    object QrEFD_E510VL_CONT_IPI: TFloatField
      FieldName = 'VL_CONT_IPI'
    end
    object QrEFD_E510VL_BC_IPI: TFloatField
      FieldName = 'VL_BC_IPI'
    end
    object QrEFD_E510VL_IPI: TFloatField
      FieldName = 'VL_IPI'
    end
    object QrEFD_E510Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_E510DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_E510DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_E510UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_E510UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_E510AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_E510Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_E510: TDataSource
    DataSet = QrEFD_E510
    Left = 128
    Top = 416
  end
  object QrEFD_E520: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEFD_E520AfterOpen
    BeforeClose = QrEFD_E520BeforeClose
    AfterScroll = QrEFD_E520AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e520 e520'
      'WHERE e520.ImporExpor=3'
      'AND e520.Empresa=-11'
      'AND e520.AnoMes=201001'
      'AND e520.LinArq>0')
    Left = 128
    Top = 464
    object QrEFD_E520ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E520AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E520Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E520LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E520E500: TIntegerField
      FieldName = 'E500'
    end
    object QrEFD_E520REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E520VL_SD_ANT_IPI: TFloatField
      FieldName = 'VL_SD_ANT_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEFD_E520VL_DEB_IPI: TFloatField
      FieldName = 'VL_DEB_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEFD_E520VL_CRED_IPI: TFloatField
      FieldName = 'VL_CRED_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEFD_E520VL_OD_IPI: TFloatField
      FieldName = 'VL_OD_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEFD_E520VL_OC_IPI: TFloatField
      FieldName = 'VL_OC_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEFD_E520VL_SC_IPI: TFloatField
      FieldName = 'VL_SC_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEFD_E520VL_SD_IPI: TFloatField
      FieldName = 'VL_SD_IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEFD_E520Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_E520DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_E520DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_E520UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_E520UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_E520AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_E520Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_E520: TDataSource
    DataSet = QrEFD_E520
    Left = 128
    Top = 512
  end
  object QrEFD_E530: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e530 e530'
      'WHERE e530.ImporExpor=3'
      'AND e530.Empresa=-11'
      'AND e530.AnoMes=201001'
      'AND e530.LinArq>0'
      '')
    Left = 124
    Top = 564
    object QrEFD_E530ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_E530AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_E530Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_E530LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_E530E520: TIntegerField
      FieldName = 'E520'
    end
    object QrEFD_E530REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_E530IND_AJ: TWideStringField
      FieldName = 'IND_AJ'
      Size = 1
    end
    object QrEFD_E530VL_AJ: TFloatField
      FieldName = 'VL_AJ'
    end
    object QrEFD_E530COD_AJ: TWideStringField
      FieldName = 'COD_AJ'
      Size = 3
    end
    object QrEFD_E530IND_DOC: TWideStringField
      FieldName = 'IND_DOC'
      Size = 1
    end
    object QrEFD_E530NUM_DOC: TWideStringField
      FieldName = 'NUM_DOC'
      Size = 255
    end
    object QrEFD_E530DESCR_AJ: TWideStringField
      FieldName = 'DESCR_AJ'
      Size = 255
    end
  end
  object DsEFD_E530: TDataSource
    DataSet = QrEFD_E530
    Left = 124
    Top = 612
  end
  object PME530: TPopupMenu
    OnPopup = PME530Popup
    Left = 928
    Top = 464
    object IncluiajustedaapuracaodoIPI1: TMenuItem
      Caption = '&Inclui ajuste da apuracao do IPI'
      OnClick = IncluiajustedaapuracaodoIPI1Click
    end
    object AlteraajustedaapuracaodoIPIselecionado1: TMenuItem
      Caption = '&Altera ajuste da apuracao do IPI selecionado'
      OnClick = AlteraajustedaapuracaodoIPIselecionado1Click
    end
    object ExcluiajustedaapuracaodoIPIselecionado1: TMenuItem
      Caption = '&Exclui ajuste da apuracao do IPI selecionado'
      OnClick = ExcluiajustedaapuracaodoIPIselecionado1Click
    end
  end
  object QrEFD_H005: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEFD_H005AfterOpen
    BeforeClose = QrEFD_H005BeforeClose
    AfterScroll = QrEFD_H005AfterScroll
    SQL.Strings = (
      'SELECT * FROM efd_h005')
    Left = 184
    Top = 248
    object QrEFD_H005ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_H005AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_H005Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_H005LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_H005REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_H005DT_INV: TDateField
      FieldName = 'DT_INV'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_H005VL_INV: TFloatField
      FieldName = 'VL_INV'
    end
    object QrEFD_H005MOT_INV: TWideStringField
      FieldName = 'MOT_INV'
      Size = 2
    end
    object QrEFD_H005NO_MOT_INV: TWideStringField
      FieldName = 'NO_MOT_INV'
      Size = 255
    end
    object QrEFD_H005Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_H005DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_H005DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_H005UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_H005UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_H005AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_H005Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_H005: TDataSource
    DataSet = QrEFD_H005
    Left = 184
    Top = 296
  end
  object PMH005: TPopupMenu
    OnPopup = PMH005Popup
    Left = 796
    Top = 320
    object Incluidatadeinventrio1: TMenuItem
      Caption = '&Inclui invent'#225'rio'
      OnClick = Incluidatadeinventrio1Click
    end
    object AlteraInventrio1: TMenuItem
      Caption = '&Altera Invent'#225'rio'
      OnClick = AlteraInventrio1Click
    end
    object Excluiinventrio1: TMenuItem
      Caption = '&Exclui invent'#225'rio'
      OnClick = Excluiinventrio1Click
    end
  end
  object QrEFD_H010: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEFD_H010AfterOpen
    BeforeClose = QrEFD_H010BeforeClose
    AfterScroll = QrEFD_H010AfterScroll
    SQL.Strings = (
      'SELECT * FROM efd_h010')
    Left = 184
    Top = 348
    object QrEFD_H010ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_H010AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_H010Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_H010H005: TIntegerField
      FieldName = 'H005'
    end
    object QrEFD_H010LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_H010REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_H010COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_H010UNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrEFD_H010QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrEFD_H010VL_UNIT: TFloatField
      FieldName = 'VL_UNIT'
      DisplayFormat = '#,###,###,##0.000000'
    end
    object QrEFD_H010VL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_H010IND_PROP: TWideStringField
      FieldName = 'IND_PROP'
      Size = 1
    end
    object QrEFD_H010COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_H010TXT_COMPL: TWideStringField
      FieldName = 'TXT_COMPL'
      Size = 255
    end
    object QrEFD_H010COD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEFD_H010Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_H010DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_H010DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_H010UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_H010UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_H010AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_H010Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_H010VL_ITEM_IR: TFloatField
      FieldName = 'VL_ITEM_IR'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_H010NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrEFD_H010NO_PART: TWideStringField
      FieldName = 'NO_PART'
      Size = 100
    end
    object QrEFD_H010BalID: TIntegerField
      FieldName = 'BalID'
    end
    object QrEFD_H010BalNum: TIntegerField
      FieldName = 'BalNum'
    end
    object QrEFD_H010BalItm: TIntegerField
      FieldName = 'BalItm'
    end
    object QrEFD_H010BalEnt: TIntegerField
      FieldName = 'BalEnt'
    end
  end
  object DsEFD_H010: TDataSource
    DataSet = QrEFD_H010
    Left = 184
    Top = 396
  end
  object QrEFD_H020: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM efd_h020')
    Left = 184
    Top = 444
    object QrEFD_H020ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_H020AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_H020Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_H020H010: TIntegerField
      FieldName = 'H010'
    end
    object QrEFD_H020LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_H020REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_H020CST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
    end
    object QrEFD_H020BC_ICMS: TFloatField
      FieldName = 'BC_ICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEFD_H020VL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEFD_H020Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_H020DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_H020DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_H020UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_H020UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_H020AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_H020Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_H020: TDataSource
    DataSet = QrEFD_H020
    Left = 184
    Top = 492
  end
  object PMH010: TPopupMenu
    OnPopup = PMH010Popup
    Left = 796
    Top = 368
    object Importadebalano1: TMenuItem
      Caption = 'Importa de &Balan'#231'o'
      OnClick = Importadebalano1Click
    end
    object Incluiitem1: TMenuItem
      Caption = '&Inclui item'
      OnClick = Incluiitem1Click
    end
    object Alteraitem1: TMenuItem
      Caption = '&Altera item'
      OnClick = Alteraitem1Click
    end
    object Excluiitemns1: TMenuItem
      Caption = '&Exclui item(ns)'
      OnClick = Excluiitemns1Click
    end
  end
  object PMH020: TPopupMenu
    OnPopup = PMH020Popup
    Left = 796
    Top = 416
    object Incluiinformaocomplementar1: TMenuItem
      Caption = '&Inclui informa'#231#227'o complementar'
      OnClick = Incluiinformaocomplementar1Click
    end
    object Alterainformaocomplementar1: TMenuItem
      Caption = '&Altera informa'#231#227'o complementar'
      OnClick = Alterainformaocomplementar1Click
    end
    object Excluiinformaocomplementar1: TMenuItem
      Caption = '&Exclui informa'#231#227'o complementar'
      OnClick = Excluiinformaocomplementar1Click
    end
  end
  object QrEFD_K100: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEFD_K100AfterOpen
    BeforeClose = QrEFD_K100BeforeClose
    AfterScroll = QrEFD_K100AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM efd_e100 e100 '
      'WHERE e100.ImporExpor=3 '
      'AND e100.Empresa=-11 '
      'AND e100.AnoMes=201001'
      'ORDER BY e100.DT_INI'
      '')
    Left = 236
    Top = 272
    object QrEFD_K100ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K100AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K100Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K100LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K100REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K100DT_INI: TDateField
      FieldName = 'DT_INI'
    end
    object QrEFD_K100DT_FIN: TDateField
      FieldName = 'DT_FIN'
    end
    object QrEFD_K100Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K100DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K100DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K100UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K100UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K100AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K100Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_K100: TDataSource
    DataSet = QrEFD_K100
    Left = 236
    Top = 320
  end
  object PMK100: TPopupMenu
    OnPopup = PMK100Popup
    Left = 856
    Top = 324
    object IncluinovointervalodeProduoeEstoque1: TMenuItem
      Caption = 'Inclui &novo intervalo de Produ'#231#227'o e Estoque'
      OnClick = IncluinovointervalodeProduoeEstoque1Click
    end
    object AlteraintervalodeProduoeEstoque1: TMenuItem
      Caption = '&Altera intervalo de Produ'#231#227'o e Estoque'
      OnClick = AlteraintervalodeProduoeEstoque1Click
    end
    object ExcluiintervalodeProduoeEstoque1: TMenuItem
      Caption = '&Exclui intervalo de Produ'#231#227'o e Estoque'
      OnClick = ExcluiintervalodeProduoeEstoque1Click
    end
    object Excluiregistrosd1: TMenuItem
      Caption = 'Exclui todos registros de movimento importados '
      OnClick = Excluiregistrosd1Click
    end
  end
  object PMK200: TPopupMenu
    OnPopup = PMK200Popup
    Left = 856
    Top = 372
    object ImportaK200_1: TMenuItem
      Caption = 'Importa de &Balan'#231'o (K200 e K280)'
      OnClick = ImportaK200_1Click
    end
    object IncluiK200_1: TMenuItem
      Caption = '&Inclui item'
      OnClick = IncluiK200_1Click
    end
    object AlteraK200_1: TMenuItem
      Caption = '&Altera item'
      OnClick = AlteraK200_1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object ExcluiK200_1: TMenuItem
      Caption = '&Exclui item(ns) (K200)'
      OnClick = ExcluiK200_1Click
    end
    object ExcluiK200_280_1: TMenuItem
      Caption = '&Exclui todos itens do estoque e os K2&80 retativos ao estoque'
      OnClick = ExcluiK200_280_1Click
    end
  end
  object QrEFD_K200: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(K200.COD_PART="", "", IF(ter.Tipo=0, '
      'ter.RazaoSocial, ter.Nome)) NO_PART, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, K200.* '
      'FROM efd_K200 K200 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K200.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed '
      'LEFT JOIN entidades  ter ON ter.Codigo=K200.COD_PART '
      'WHERE K200.ImporExpor=3 '
      'AND K200.Empresa=-11 '
      'AND K200.AnoMes=201601 '
      'AND K200.K100=1 ')
    Left = 236
    Top = 368
    object QrEFD_K200ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K200AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K200Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K200LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K200REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K200K100: TIntegerField
      FieldName = 'K100'
    end
    object QrEFD_K200DT_EST: TDateField
      FieldName = 'DT_EST'
    end
    object QrEFD_K200COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K200QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrEFD_K200IND_EST: TWideStringField
      FieldName = 'IND_EST'
      Size = 1
    end
    object QrEFD_K200COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_K200BalID: TIntegerField
      FieldName = 'BalID'
    end
    object QrEFD_K200BalNum: TIntegerField
      FieldName = 'BalNum'
    end
    object QrEFD_K200BalItm: TIntegerField
      FieldName = 'BalItm'
    end
    object QrEFD_K200Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K200DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K200DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K200UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K200UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K200AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K200Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_K200BalEnt: TIntegerField
      FieldName = 'BalEnt'
    end
    object QrEFD_K200NO_PART: TWideStringField
      FieldName = 'NO_PART'
      Size = 100
    end
    object QrEFD_K200NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K200NO_IND_EST: TWideStringField
      FieldName = 'NO_IND_EST'
      Size = 100
    end
    object QrEFD_K200Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K200GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K200Grandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrEFD_K200Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEFD_K200AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEFD_K200PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
  end
  object DsEFD_K200: TDataSource
    DataSet = QrEFD_K200
    Left = 236
    Top = 416
  end
  object PMK220: TPopupMenu
    OnPopup = PMK220Popup
    Left = 856
    Top = 420
    object ImportaK220_1: TMenuItem
      Caption = 'Importa &movimentos'
      OnClick = ImportaK220_1Click
    end
    object IncluiK220_1: TMenuItem
      Caption = '&Inclui item'
      OnClick = IncluiK220_1Click
    end
    object AlteraK220_1: TMenuItem
      Caption = '&Altera item'
      OnClick = AlteraK220_1Click
    end
    object ExcluiK220_1: TMenuItem
      Caption = '&Exclui item'
      OnClick = ExcluiK220_1Click
    end
    object ExcluiK220_280_1: TMenuItem
      Caption = 
        'Exclui todos itens de classe (incluindo os K2&80 relativos a cla' +
        'sse)'
      OnClick = ExcluiK220_280_1Click
    end
  end
  object DsEFD_K220: TDataSource
    DataSet = QrEFD_K220
    Left = 236
    Top = 508
  end
  object QrEFD_K220: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'CONCAT(gg1o.Nome, '
      'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), '
      'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) '
      'NO_PRD_TAM_COR_ORI, '
      'CONCAT(gg1d.Nome, '
      'IF(gtid.PrintTam=0, "", CONCAT(" ", gtid.Nome)), '
      'IF(gccd.PrintCor=0,"", CONCAT(" ", gccd.Nome))) '
      'NO_PRD_TAM_COR_DEST, '
      'K220.* '
      'FROM efd_K220 K220 '
      'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K220.COD_ITEM_ORI '
      'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC '
      'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad '
      'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI '
      'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 '
      'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed '
      ' '
      'LEFT JOIN gragrux    ggxd ON ggxd.Controle=K220.COD_ITEM_DEST '
      'LEFT JOIN gragruc    ggcd ON ggcd.Controle=ggxd.GraGruC '
      'LEFT JOIN gracorcad  gccd ON gccd.Codigo=ggcd.GraCorCad '
      'LEFT JOIN gratamits  gtid ON gtid.Controle=ggxd.GraTamI '
      'LEFT JOIN gragru1    gg1d ON gg1d.Nivel1=ggxd.GraGru1 '
      'LEFT JOIN unidmed    unmd ON unmd.Codigo=gg1d.UnidMed '
      'WHERE k220.ImporExpor=3'
      'AND k220.Empresa=-11'
      'AND k220.AnoMes=201601'
      'AND k220.K100=1'
      'ORDER BY k220.DT_MOV, COD_ITEM_ORI, COD_ITEM_DEST ')
    Left = 236
    Top = 464
    object QrEFD_K220NO_PRD_TAM_COR_ORI: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_ORI'
      Size = 157
    end
    object QrEFD_K220NO_PRD_TAM_COR_DEST: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_DEST'
      Size = 157
    end
    object QrEFD_K220ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K220AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K220Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K220LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K220REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K220K100: TIntegerField
      FieldName = 'K100'
    end
    object QrEFD_K220DT_MOV: TDateField
      FieldName = 'DT_MOV'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_K220COD_ITEM_ORI: TWideStringField
      FieldName = 'COD_ITEM_ORI'
      Size = 60
    end
    object QrEFD_K220COD_ITEM_DEST: TWideStringField
      FieldName = 'COD_ITEM_DEST'
      Size = 60
    end
    object QrEFD_K220QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrEFD_K220Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K220DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K220DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K220UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K220UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K220AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K220Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_K220MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K220ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K220Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K220MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K220CtrlDst: TIntegerField
      FieldName = 'CtrlDst'
    end
    object QrEFD_K220GGXDst: TIntegerField
      FieldName = 'GGXDst'
    end
    object QrEFD_K220GGXOri: TIntegerField
      FieldName = 'GGXOri'
    end
    object QrEFD_K220Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K220ESOMIEM: TIntegerField
      FieldName = 'ESOMIEM'
    end
  end
  object PMK230: TPopupMenu
    OnPopup = PMK230Popup
    Left = 856
    Top = 468
    object ImportaK230_1: TMenuItem
      Caption = 'Importa &movimentos'
      OnClick = ImportaK230_1Click
    end
    object Vaiparajanelademovimento1: TMenuItem
      Caption = 'Vai para janela de movimento'
      OnClick = Vaiparajanelademovimento1Click
    end
    object IncluiK230_1: TMenuItem
      Caption = '&Inclui item'
      OnClick = IncluiK230_1Click
    end
    object AlteraK230_1: TMenuItem
      Caption = '&Altera item'
      OnClick = AlteraK230_1Click
    end
    object ExcluiK230_1: TMenuItem
      Caption = '&Exclui item (por registro)'
      object K2301: TMenuItem
        Caption = 'K230'
        OnClick = K2301Click
      end
      object K2351: TMenuItem
        Caption = 'K235'
        OnClick = K2351Click
      end
      object K2501: TMenuItem
        Caption = 'K250'
        OnClick = K2501Click
      end
      object K2551: TMenuItem
        Caption = 'K255'
        OnClick = K2551Click
      end
      object K2601: TMenuItem
        Caption = 'K260'
        OnClick = K2601Click
      end
      object K2651: TMenuItem
        Caption = 'K265'
        OnClick = K2651Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object odos1: TMenuItem
        Caption = 'Todos'
        OnClick = odos1Click
      end
    end
    object Excluisegmentointeiroimportado1: TMenuItem
      Caption = 'Exclui segmento inteiro (importado)'
      OnClick = Excluisegmentointeiroimportado1Click
    end
    object ExcluiK230_280_1: TMenuItem
      Caption = 
        'Exclui todos itens de produ'#231#227'o (incluindo os K2&80 relativos '#224' p' +
        'rodu'#231#227'o)'
      OnClick = ExcluiK230_280_1Click
    end
  end
  object QrEFD_K230: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K230BeforeClose
    AfterScroll = QrEFD_K230AfterScroll
    Left = 340
    Top = 324
    object QrEFD_K230NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K230ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K230AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K230Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K230LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K230REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K230DT_INI_OP: TDateField
      FieldName = 'DT_INI_OP'
    end
    object QrEFD_K230K100: TIntegerField
      FieldName = 'K100'
    end
    object QrEFD_K230DT_FIN_OP: TDateField
      FieldName = 'DT_FIN_OP'
    end
    object QrEFD_K230COD_DOC_OP: TWideStringField
      FieldName = 'COD_DOC_OP'
      Size = 30
    end
    object QrEFD_K230COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K230QTD_ENC: TFloatField
      FieldName = 'QTD_ENC'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrEFD_K230Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K230DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K230DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K230UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K230UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K230AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K230Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_K230DT_FIN_OP_Txt: TWideStringField
      FieldName = 'DT_FIN_OP_Txt'
      Size = 10
    end
    object QrEFD_K230Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K230Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K230OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
  end
  object DsEFD_K230: TDataSource
    DataSet = QrEFD_K230
    Left = 340
    Top = 368
  end
  object QrEFD_K250: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K250BeforeClose
    AfterScroll = QrEFD_K250AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'IF(k250.DT_PROD = 0, "", '
      'DATE_FORMAT(k250.DT_PROD, "%d/%m/%Y")) DT_PROD_Txt,'
      'K250.* '
      'FROM efd_K250 k250 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K250.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k250.ImporExpor=3'
      'AND k250.Empresa=-11'
      'AND k250.AnoMes=201601'
      'AND k250.K100=1'
      'ORDER BY DT_PROD, COD_DOC_OP, COD_ITEM ')
    Left = 392
    Top = 300
    object QrEFD_K250Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K250Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K250NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K250DT_PROD_Txt: TWideStringField
      FieldName = 'DT_PROD_Txt'
      Size = 10
    end
    object QrEFD_K250ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K250AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K250Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K250LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K250REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K250K100: TIntegerField
      FieldName = 'K100'
    end
    object QrEFD_K250DT_PROD: TDateField
      FieldName = 'DT_PROD'
    end
    object QrEFD_K250COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K250QTD: TFloatField
      FieldName = 'QTD'
    end
    object QrEFD_K250Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K250DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K250DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K250UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K250UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K250AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K250Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_K250ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K250MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K250Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K250MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K250COD_DOC_OP: TWideStringField
      FieldName = 'COD_DOC_OP'
      Size = 30
    end
    object QrEFD_K250OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
  end
  object DsEFD_K250: TDataSource
    DataSet = QrEFD_K250
    Left = 392
    Top = 348
  end
  object QrEFD_K235: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR,'
      'k235.*'
      'FROM efd_k235 k235'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k235.COD_ITEM'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE k235.ImporExpor=3'
      'AND k235.Empresa=-11'
      'AND k235.AnoMes=201601'
      'AND k235.K230=1')
    Left = 340
    Top = 416
    object QrEFD_K235Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K235Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K235NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K235ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K235AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K235Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K235LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K235REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K235DT_SAIDA: TDateField
      FieldName = 'DT_SAIDA'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_K235COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K235QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_K235COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrEFD_K235Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K235DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K235DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K235UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K235UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K235AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K235Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_K235K230: TIntegerField
      FieldName = 'K230'
    end
    object QrEFD_K235ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K235ID_Item: TLargeintField
      FieldName = 'ID_Item'
    end
    object QrEFD_K235MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K235Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K235MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K235Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsEFD_K235: TDataSource
    DataSet = QrEFD_K235
    Left = 340
    Top = 464
  end
  object QrEFD_K255: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR,'
      'k235.*'
      'FROM efd_k235 k235'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k235.COD_ITEM'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE k235.ImporExpor=3'
      'AND k235.Empresa=-11'
      'AND k235.AnoMes=201601'
      'AND k235.K230=1')
    Left = 392
    Top = 396
    object QrEFD_K255Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K255Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K255NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K255ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K255AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K255Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K255LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K255REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K255DT_CONS: TDateField
      FieldName = 'DT_CONS'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEFD_K255COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K255QTD: TFloatField
      FieldName = 'QTD'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEFD_K255COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrEFD_K255Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K255DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K255DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K255UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K255UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K255AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K255Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_K255K250: TIntegerField
      FieldName = 'K250'
    end
    object QrEFD_K255ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K255ID_Item: TLargeintField
      FieldName = 'ID_Item'
    end
    object QrEFD_K255MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K255Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K255MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K255Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsEFD_K255: TDataSource
    DataSet = QrEFD_K255
    Left = 392
    Top = 444
  end
  object PMImprime: TPopupMenu
    Left = 328
    Top = 520
    object BlocoKRegistrosK2001: TMenuItem
      Caption = 'Bloco K - Registros K200'
      OnClick = BlocoKRegistrosK2001Click
    end
    object BlocoKRegistrosK2201: TMenuItem
      Caption = 'Bloco K - Registros K220'
      OnClick = BlocoKRegistrosK2201Click
    end
    object ProduoK230aK2551: TMenuItem
      Caption = 'Produ'#231#227'o (K230 a K255)'
      OnClick = ProduoK230aK2551Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Erroempresaitens1: TMenuItem
      Caption = 'Erro empresa - itens'
      OnClick = Erroempresaitens1Click
    end
  end
  object frxEFD_SPEDE_K200_001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxEFD_SPEDE_K200_001GetValue
    Left = 328
    Top = 564
    Datasets = <
      item
        DataSet = frxDsEFD_K200
        DataSetName = 'frxDsEFD_K200'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'K200 - ESTOQUE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA_SPED]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 41.574830000000000000
          Top = 51.023622047244090000
          Width = 362.834782360000000000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Top = 51.023622047244090000
          Width = 41.574805590000000000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 585.827150000000000000
          Top = 64.252010000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 525.354670000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 464.882190000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 404.409710000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Left = 404.409710000000000000
          Top = 51.023622050000000000
          Width = 181.417400940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ind'#250'stria')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Left = 585.827150000000000000
          Top = 51.023622050000000000
          Width = 94.488210940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fisco')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEFD_K200
        DataSetName = 'frxDsEFD_K200'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 41.574830000000000000
          Width = 362.834782360000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsEFD_K200
          DataSetName = 'frxDsEFD_K200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K200."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 41.574803149606300000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsEFD_K200
          DataSetName = 'frxDsEFD_K200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K200."COD_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsEFD_K200
          DataSetName = 'frxDsEFD_K200'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K200."QTD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataSet = frxDsEFD_K200
          DataSetName = 'frxDsEFD_K200'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K200."Sigla"] ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 525.354670000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'PesoKg'
          DataSet = frxDsEFD_K200
          DataSetName = 'frxDsEFD_K200'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K200."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Left = 464.882190000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'AreaM2'
          DataSet = frxDsEFD_K200
          DataSetName = 'frxDsEFD_K200'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K200."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Left = 404.409710000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'Pecas'
          DataSet = frxDsEFD_K200
          DataSetName = 'frxDsEFD_K200'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K200."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 536.693260000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 483.779840000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000000000
          Width = 404.409514720000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Top = 15.118120000000000000
          Width = 60.472433620000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 404.409710000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 464.882190000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 525.354670000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_00: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEFD_K200."IND_EST"'
        object Me_GH0: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEFD_K200."NO_IND_EST"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_00: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Me_FT0: TfrxMemoView
          Width = 404.409309690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K200."NO_IND_EST"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu0PesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 404.409710000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 464.882190000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 525.354670000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_02: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 249.448980000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsEFD_K200."COD_ITEM"'
        object Me_GH2: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEFD_K200."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEFD_K200."COD_PART"'
        object Me_GH1: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEFD_K200."NO_PART"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 370.393940000000000000
        Width = 680.315400000000000000
        object Me_FT1: TfrxMemoView
          Width = 404.409309690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K200."NO_PART"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu1PesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 404.409710000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 464.882190000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 525.354670000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FT_02: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 332.598640000000000000
        Visible = False
        Width = 680.315400000000000000
        object Me_FT2: TfrxMemoView
          Width = 404.409309690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K200."NO_PRD_TAM_COR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu2PesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 404.409710000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 464.882190000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 525.354670000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K200."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsEFD_K200: TfrxDBDataset
    UserName = 'frxDsEFD_K200'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K100=K100'
      'DT_EST=DT_EST'
      'COD_ITEM=COD_ITEM'
      'QTD=QTD'
      'IND_EST=IND_EST'
      'COD_PART=COD_PART'
      'BalID=BalID'
      'BalNum=BalNum'
      'BalItm=BalItm'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'BalEnt=BalEnt'
      'NO_PART=NO_PART'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_IND_EST=NO_IND_EST'
      'Sigla=Sigla'
      'GraGruX=GraGruX'
      'Grandeza=Grandeza'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'PesoKg=PesoKg')
    DataSet = QrEFD_K200
    BCDToCurrency = False
    Left = 328
    Top = 612
  end
  object frxEFD_SPEDE_K220_001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxEFD_SPEDE_K200_001GetValue
    Left = 328
    Top = 660
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEFD_K220
        DataSetName = 'frxDsEFD_K220'
      end
      item
        DataSet = frxDsEFD_K270_220
        DataSetName = 'frxDsEFD_K270_220'
      end
      item
        DataSet = frxDsEFD_K275_220
        DataSetName = 'frxDsEFD_K275_220'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'K220 - OUTRAS MOVIMENTA'#199#213'ES INTERNAS ENTRE MERCADORIAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA_SPED]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 79.370130000000000000
          Top = 64.252010000000000000
          Width = 232.440932680000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Left = 37.795300000000000000
          Top = 64.252010000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 585.827150000000000000
          Top = 64.252010000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Left = 585.827150000000000000
          Top = 51.023622050000000000
          Width = 94.488210940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fisco')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 37.795300000000000000
          Top = 51.023622050000000000
          Width = 274.015735830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Origem')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 311.811035830000000000
          Top = 51.023622050000000000
          Width = 274.015735830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Destino')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 353.385838980000000000
          Top = 64.252010000000000000
          Width = 232.440942440000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 311.811035830000000000
          Top = 64.252010000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Top = 51.023622050000000000
          Width = 37.795275590551180000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEFD_K220
        DataSetName = 'frxDsEFD_K220'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 79.370130000000000000
          Width = 232.440932680000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR_ORI'
          DataSet = frxDsEFD_K220
          DataSetName = 'frxDsEFD_K220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K220."NO_PRD_TAM_COR_ORI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 37.795300000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'GGXOri'
          DataSet = frxDsEFD_K220
          DataSetName = 'frxDsEFD_K220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K220."GGXOri"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsEFD_K220
          DataSetName = 'frxDsEFD_K220'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K220."QTD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsEFD_K220
          DataSetName = 'frxDsEFD_K220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K220."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 353.385836540000000000
          Width = 232.440942440000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR_DEST'
          DataSet = frxDsEFD_K220
          DataSetName = 'frxDsEFD_K220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K220."NO_PRD_TAM_COR_DEST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 311.811035830000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'GGXDst'
          DataSet = frxDsEFD_K220
          DataSetName = 'frxDsEFD_K220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K220."GGXDst"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_MOV'
          DataSet = frxDsEFD_K220
          DataSetName = 'frxDsEFD_K220'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K220."DT_MOV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 555.590910000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 502.677490000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000000000
          Width = 619.842724720000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Top = 15.118120000000000000
          Width = 60.472433620000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '['
            'SUM(<frxDsEFD_K220."QTD">,MD002,1)'
            '+SUM(<frxDsEFD_K270_220."QTD_COR_POS">,MD001)'
            '-SUM(<frxDsEFD_K270_220."QTD_COR_NEG">,MD001)'
            ']')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_00: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEFD_K220."MovimCod"'
        object Me_GH0: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'IME-C [frxDsEFD_K220."MovimCod"] - [VARF_NoMovimCod_220]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object FT_00: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        object Me_FT0: TfrxMemoView
          Width = 619.842519690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'IME-C [frxDsEFD_K220."MovimCod"] - [VARF_NoMovimCod_220]: ')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSu0PesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K220."QTD">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 366.614410000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEFD_K270_220
        DataSetName = 'frxDsEFD_K270_220'
        RowCount = 0
        object Memo12: TfrxMemoView
          Left = 79.370130000000000000
          Width = 232.440932680000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsEFD_K270_220
          DataSetName = 'frxDsEFD_K270_220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K270_220."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 37.795300000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsEFD_K270_220
          DataSetName = 'frxDsEFD_K270_220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K270_220."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsEFD_K270_220
          DataSetName = 'frxDsEFD_K270_220'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<frxDsEFD_K270_220."QTD_COR_POS">-<frxDsEFD_K270_220."QTD_COR_N' +
              'EG">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsEFD_K270_220
          DataSetName = 'frxDsEFD_K270_220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K270_220."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 353.385836540000000000
          Width = 232.440942440000000000
          Height = 13.228346460000000000
          DataSet = frxDsEFD_K220
          DataSetName = 'frxDsEFD_K220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K275_220."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 311.811035830000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataSet = frxDsEFD_K220
          DataSetName = 'frxDsEFD_K220'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K275_220."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_INI_AP'
          DataSet = frxDsEFD_K270_220
          DataSetName = 'frxDsEFD_K270_220'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K270_220."DT_INI_AP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEFD_K220."MovimCod"'
        object Memo19: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'IME-C [frxDsEFD_K220."MovimCod"] - [VARF_NoMovimCod_220]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GF_01: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 404.409710000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          Width = 619.842519690000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'IME-C [frxDsEFD_K270_220."MovimCod"] - [VARF_NoMovimCod_270]: ')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo21: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '['
            'SUM(<frxDsEFD_K270_220."QTD_COR_POS">,MD001)'
            '-SUM(<frxDsEFD_K270_220."QTD_COR_NEG">,MD001)'
            ']')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        object Memo22: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'K270 - OUTRAS MOVIMENTA'#199#213'ES INTERNAS ENTRE MERCADORIAS - CORRE'#199#195 +
              'O DE APONTAMENTO')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Top = 442.205010000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsEFD_K220: TfrxDBDataset
    UserName = 'frxDsEFD_K220'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PRD_TAM_COR_ORI=NO_PRD_TAM_COR_ORI'
      'NO_PRD_TAM_COR_DEST=NO_PRD_TAM_COR_DEST'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K100=K100'
      'DT_MOV=DT_MOV'
      'COD_ITEM_ORI=COD_ITEM_ORI'
      'COD_ITEM_DEST=COD_ITEM_DEST'
      'QTD=QTD'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'MovimID=MovimID'
      'ID_SEK=ID_SEK'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'CtrlDst=CtrlDst'
      'GGXDst=GGXDst'
      'GGXOri=GGXOri'
      'Sigla=Sigla')
    DataSet = QrEFD_K220
    BCDToCurrency = False
    Left = 328
    Top = 708
  end
  object frxEFD_SPEDE_K230_001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxEFD_SPEDE_K200_001GetValue
    Left = 412
    Top = 544
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEFD_K210
        DataSetName = 'frxDsEFD_K210'
      end
      item
        DataSet = frxDsEFD_K215
        DataSetName = 'frxDsEFD_K215'
      end
      item
        DataSet = frxDsEFD_K230
        DataSetName = 'frxDsEFD_K230'
      end
      item
        DataSet = frxDsEFD_K235
        DataSetName = 'frxDsEFD_K235'
      end
      item
        DataSet = frxDsEFD_K250
        DataSetName = 'frxDsEFD_K250'
      end
      item
        DataSet = frxDsEFD_K255
        DataSetName = 'frxDsEFD_K255'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'K230 a K255 - ITENS PRODUZIDOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA_SPED]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 158.740260000000000000
          Top = 51.023622047244090000
          Width = 427.086614173228300000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Left = 117.165430000000000000
          Top = 51.023622047244090000
          Width = 41.574805590000000000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 585.827150000000000000
          Top = 64.252010000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Left = 585.827150000000000000
          Top = 51.023622050000000000
          Width = 94.488210940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fisco')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Top = 51.023622047244090000
          Width = 37.795275590551180000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'In'#237'cio')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo14: TfrxMemoView
          Left = 37.795300000000000000
          Top = 51.023622047244090000
          Width = 37.795275590000000000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'T'#233'rmino')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo15: TfrxMemoView
          Left = 75.590600000000000000
          Top = 51.023622050000000000
          Width = 41.574805590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'OP/IME-C')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 26.456692920000000000
        Top = 381.732530000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEFD_K230
        DataSetName = 'frxDsEFD_K230'
        RowCount = 0
        object MeValCodi: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_FIN_OP'
          DataSet = frxDsEFD_K230
          DataSetName = 'frxDsEFD_K230'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K230."DT_FIN_OP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsEFD_K230
          DataSetName = 'frxDsEFD_K230'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K230."QTD_ENC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsEFD_K230
          DataSetName = 'frxDsEFD_K230'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K230."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 158.740167240000000000
          Width = 427.086604410000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsEFD_K230
          DataSetName = 'frxDsEFD_K230'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K230."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 117.165364090000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsEFD_K230
          DataSetName = 'frxDsEFD_K230'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K230."COD_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_INI_OP'
          DataSet = frxDsEFD_K230
          DataSetName = 'frxDsEFD_K230'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K230."DT_INI_OP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 75.590600000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          DataField = 'COD_DOC_OP'
          DataSet = frxDsEFD_K230
          DataSetName = 'frxDsEFD_K230'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K230."COD_DOC_OP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 619.842920000000000000
          Top = 13.228346460000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo24: TfrxMemoView
          Left = 585.827150000000000000
          Top = 13.228346460000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo25: TfrxMemoView
          Left = 75.590600000000000000
          Top = 13.228346460000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Susbtituto')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo26: TfrxMemoView
          Left = 37.795300000000000000
          Top = 13.228346460000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sa'#237'da')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo30: TfrxMemoView
          Left = 120.944960000000000000
          Top = 13.228346460000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Left = 166.299320000000000000
          Top = 13.228346460000000000
          Width = 419.527790940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material usado para produzir')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo36: TfrxMemoView
          Top = 13.228346460000000000
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 782.362710000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 729.449290000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000000000
          Width = 619.842724720000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL PRODUZIDO: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTPesoKg: TfrxMemoView
          Left = 619.842920000000000000
          Top = 15.118120000000000000
          Width = 60.472433620000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxDsEFD_K230."QTD_ENC">,MD002,1) + SUM(<frxDsEFD_K250."QT' +
              'D">,MD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 336.378170000000000000
        Width = 680.315400000000000000
        object Me_GH0: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'K230 - ITENS PRODUZIDOS (NAS INSTALA'#199#213'ES DA EMPRESA)')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 22.677170240000000000
        Top = 468.661720000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K235."QTD">,DD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Width = 616.062975040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              'K235 - ITENS CONSUMIDOS (NAS INSTALA'#199#213'ES DA EMPRESA) na OP/IME-C' +
              ' [frxDsEFD_K230."COD_DOC_OP"]: ')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD003: TfrxMasterData
        FillType = ftBrush
        Height = 26.456692920000000000
        Top = 559.370440000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEFD_K250
        DataSetName = 'frxDsEFD_K250'
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'QTD'
          DataSet = frxDsEFD_K250
          DataSetName = 'frxDsEFD_K250'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K250."QTD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsEFD_K250
          DataSetName = 'frxDsEFD_K250'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K250."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 158.740167240000000000
          Width = 427.086604410000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsEFD_K250
          DataSetName = 'frxDsEFD_K250'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K250."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 117.165364090000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsEFD_K250
          DataSetName = 'frxDsEFD_K250'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K250."COD_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_PROD'
          DataSet = frxDsEFD_K250
          DataSetName = 'frxDsEFD_K250'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K250."DT_PROD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 75.590600000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          DataField = 'COD_DOC_OP'
          DataSet = frxDsEFD_K250
          DataSetName = 'frxDsEFD_K250'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K250."COD_DOC_OP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsEFD_K250
          DataSetName = 'frxDsEFD_K250'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 619.842920000000000000
          Top = 13.228346456692910000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo39: TfrxMemoView
          Left = 585.827150000000000000
          Top = 13.228346456692910000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo40: TfrxMemoView
          Left = 75.590600000000000000
          Top = 13.228346460000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Susbtituto')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          Left = 37.795300000000000000
          Top = 13.228346460000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo42: TfrxMemoView
          Left = 120.944960000000000000
          Top = 13.228346460000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo43: TfrxMemoView
          Left = 166.299320000000000000
          Top = 13.228346460000000000
          Width = 419.527790940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material usado para produzir')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo44: TfrxMemoView
          Top = 13.228346456692910000
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 514.016080000000000000
        Width = 680.315400000000000000
        object Memo21: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'K250 - ITENS PRODUZIDOS POR TERCEIROS')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        Height = 22.677170240000000000
        Top = 646.299630000000000000
        Width = 680.315400000000000000
        object Memo22: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K255."QTD">,DD003,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Width = 616.062975040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              'K255 - ITENS CONSUMIDOS POR TERCEIROS na OP/IME-C [frxDsEFD_K250' +
              '."COD_DOC_OP"]: ')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DD002: TfrxDetailData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEFD_K235
        DataSetName = 'frxDsEFD_K235'
        RowCount = 0
        object Memo27: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_SAIDA'
          DataSet = frxDsEFD_K235
          DataSetName = 'frxDsEFD_K235'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K235."DT_SAIDA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 75.590600000000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          DataField = 'COD_INS_SUBST'
          DataSet = frxDsEFD_K235
          DataSetName = 'frxDsEFD_K235'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K235."COD_INS_SUBST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo32: TfrxMemoView
          Left = 120.944960000000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsEFD_K235
          DataSetName = 'frxDsEFD_K235'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K235."COD_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo33: TfrxMemoView
          Left = 166.299320000000000000
          Width = 419.527790940000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsEFD_K235
          DataSetName = 'frxDsEFD_K235'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K235."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo34: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsEFD_K235
          DataSetName = 'frxDsEFD_K235'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K235."QTD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsEFD_K235
          DataSetName = 'frxDsEFD_K235'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K235."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsEFD_K235
          DataSetName = 'frxDsEFD_K235'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K235."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object DD003: TfrxDetailData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 608.504330000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEFD_K255
        DataSetName = 'frxDsEFD_K255'
        RowCount = 0
        object Memo45: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_CONS'
          DataSet = frxDsEFD_K255
          DataSetName = 'frxDsEFD_K255'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K255."DT_CONS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 75.590600000000000000
          Width = 41.574790940000000000
          Height = 13.228346460000000000
          DataField = 'COD_INS_SUBST'
          DataSet = frxDsEFD_K255
          DataSetName = 'frxDsEFD_K255'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K255."COD_INS_SUBST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Left = 120.944960000000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM'
          DataSet = frxDsEFD_K255
          DataSetName = 'frxDsEFD_K255'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K255."COD_ITEM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo51: TfrxMemoView
          Left = 166.299320000000000000
          Width = 419.527790940000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsEFD_K255
          DataSetName = 'frxDsEFD_K255'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K255."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo52: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'QTD'
          DataSet = frxDsEFD_K255
          DataSetName = 'frxDsEFD_K255'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K255."QTD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsEFD_K255
          DataSetName = 'frxDsEFD_K255'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K255."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsEFD_K255
          DataSetName = 'frxDsEFD_K255'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K255."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        Height = 26.456692920000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEFD_K210
        DataSetName = 'frxDsEFD_K210'
        RowCount = 0
        object Memo55: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_FIN_OS'
          DataSet = frxDsEFD_K210
          DataSetName = 'frxDsEFD_K210'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K210."DT_FIN_OS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'QTD_ORI'
          DataSet = frxDsEFD_K210
          DataSetName = 'frxDsEFD_K210'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K210."QTD_ORI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsEFD_K210
          DataSetName = 'frxDsEFD_K210'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K210."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 158.740167240000000000
          Width = 427.086604410000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsEFD_K210
          DataSetName = 'frxDsEFD_K210'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K210."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 117.165364090000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM_ORI'
          DataSet = frxDsEFD_K210
          DataSetName = 'frxDsEFD_K210'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K210."COD_ITEM_ORI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'DT_INI_OS'
          DataSet = frxDsEFD_K210
          DataSetName = 'frxDsEFD_K210'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K210."DT_INI_OS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 75.590600000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          DataField = 'COD_DOC_OS'
          DataSet = frxDsEFD_K210
          DataSetName = 'frxDsEFD_K210'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K210."COD_DOC_OS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo62: TfrxMemoView
          Left = 619.842920000000000000
          Top = 13.228346460000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo63: TfrxMemoView
          Left = 585.827150000000000000
          Top = 13.228346460000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo64: TfrxMemoView
          Left = 75.590600000000000000
          Top = 13.228346460000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Susbtituto')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo65: TfrxMemoView
          Left = 37.795300000000000000
          Top = 13.228346460000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sa'#237'da')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo66: TfrxMemoView
          Left = 120.944960000000000000
          Top = 13.228346460000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo67: TfrxMemoView
          Left = 166.299320000000000000
          Top = 13.228346460000000000
          Width = 419.527790940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material usado para produzir')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo68: TfrxMemoView
          Top = 13.228346460000000000
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object Header3: TfrxHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        object Memo69: TfrxMemoView
          Top = 3.779530000000000000
          Width = 642.519685040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'K210 - DESMONTAGEM DE MERCADORIAS')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer3: TfrxFooter
        FillType = ftBrush
        Height = 22.677170240000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        object Memo70: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K235."QTD">,DD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Width = 616.062975040000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              'K235 - ITENS CONSUMIDOS (NAS INSTALA'#199#213'ES DA EMPRESA) na OP/IME-C' +
              ' [frxDsEFD_K230."COD_DOC_OP"]: ')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DD001: TfrxDetailData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEFD_K215
        DataSetName = 'frxDsEFD_K215'
        RowCount = 0
        object Memo72: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsEFD_K235
          DataSetName = 'frxDsEFD_K235'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 75.590600000000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          DataSet = frxDsEFD_K235
          DataSetName = 'frxDsEFD_K235'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo74: TfrxMemoView
          Left = 120.944960000000000000
          Width = 45.354320940000000000
          Height = 13.228346460000000000
          DataField = 'COD_ITEM_DES'
          DataSet = frxDsEFD_K215
          DataSetName = 'frxDsEFD_K215'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K215."COD_ITEM_DES"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo75: TfrxMemoView
          Left = 166.299320000000000000
          Width = 419.527790940000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsEFD_K215
          DataSetName = 'frxDsEFD_K215'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K215."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo76: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'QTD_DES'
          DataSet = frxDsEFD_K215
          DataSetName = 'frxDsEFD_K215'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K215."QTD_DES"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 585.827150000000000000
          Width = 34.015721180000000000
          Height = 13.228346460000000000
          DataField = 'Sigla'
          DataSet = frxDsEFD_K215
          DataSetName = 'frxDsEFD_K215'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K215."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Width = 37.795251180000000000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsEFD_K215
          DataSetName = 'frxDsEFD_K215'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K215."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
    end
  end
  object frxDsEFD_K230: TfrxDBDataset
    UserName = 'frxDsEFD_K230'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'DT_INI_OP=DT_INI_OP'
      'K100=K100'
      'DT_FIN_OP=DT_FIN_OP'
      'COD_DOC_OP=COD_DOC_OP'
      'COD_ITEM=COD_ITEM'
      'QTD_ENC=QTD_ENC'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'DT_FIN_OP_Txt=DT_FIN_OP_Txt'
      'Grandeza=Grandeza'
      'Sigla=Sigla')
    DataSet = QrEFD_K230
    BCDToCurrency = False
    Left = 412
    Top = 588
  end
  object frxDsEFD_K235: TfrxDBDataset
    UserName = 'frxDsEFD_K235'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'DT_SAIDA=DT_SAIDA'
      'COD_ITEM=COD_ITEM'
      'QTD=QTD'
      'COD_INS_SUBST=COD_INS_SUBST'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'K230=K230'
      'ID_SEK=ID_SEK'
      'ID_Item=ID_Item'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Controle=Controle')
    DataSet = QrEFD_K235
    BCDToCurrency = False
    Left = 412
    Top = 636
  end
  object frxDsEFD_K250: TfrxDBDataset
    UserName = 'frxDsEFD_K250'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'DT_PROD_Txt=DT_PROD_Txt'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K100=K100'
      'DT_PROD=DT_PROD'
      'COD_ITEM=COD_ITEM'
      'QTD=QTD'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'ID_SEK=ID_SEK'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'COD_DOC_OP=COD_DOC_OP')
    DataSet = QrEFD_K250
    BCDToCurrency = False
    Left = 412
    Top = 684
  end
  object frxDsEFD_K255: TfrxDBDataset
    UserName = 'frxDsEFD_K255'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'DT_CONS=DT_CONS'
      'COD_ITEM=COD_ITEM'
      'QTD=QTD'
      'COD_INS_SUBST=COD_INS_SUBST'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'K250=K250'
      'ID_SEK=ID_SEK'
      'ID_Item=ID_Item'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Controle=Controle')
    DataSet = QrEFD_K255
    BCDToCurrency = False
    Left = 412
    Top = 732
  end
  object QrEFD_K_ConfG: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.GraGruX,  '
      '  SUM(vmi.Movim01) Sdo_Ini, '
      '  SUM(vmi.Movim02) Compra,  '
      '  SUM(vmi.Movim03) Producao, '
      '  SUM(vmi.Movim04) EntrouClasse,  '
      '  SUM(vmi.Movim05) Consumo, '
      '  SUM(vmi.Movim06) SaiuClasse,  '
      '  SUM(vmi.Movim07) Venda, '
      '  SUM(vmi.Movim08) Final,  '
      ' '
      '  SUM(vmi.Movim01) + '
      '  SUM(vmi.Movim02) + '
      '  SUM(vmi.Movim03) + '
      '  SUM(vmi.Movim04) + '
      '  SUM(vmi.Movim05) + '
      '  SUM(vmi.Movim06) + '
      '  SUM(vmi.Movim07) - '
      '  SUM(vmi.Movim08) Diferenca,  '
      'med.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, '
      'med.Grandeza   '
      'FROM efd_k_confg vmi  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed  '
      'WHERE ImporExpor=3 '
      'AND AnoMes=201611 '
      'AND Empresa=-11 '
      'GROUP BY GraGruX '
      'ORDER BY GraGruX  ')
    Left = 820
    Top = 564
    object QrEFD_K_ConfGGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K_ConfGSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K_ConfGNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K_ConfGGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K_ConfGSdo_Ini: TFloatField
      FieldName = 'Sdo_Ini'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGCompra: TFloatField
      FieldName = 'Compra'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGProducao: TFloatField
      FieldName = 'Producao'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGEntrouClasse: TFloatField
      FieldName = 'EntrouClasse'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGConsumo: TFloatField
      FieldName = 'Consumo'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGSaiuClasse: TFloatField
      FieldName = 'SaiuClasse'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGVenda: TFloatField
      FieldName = 'Venda'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGFinal: TFloatField
      FieldName = 'Final'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGDiferenca: TFloatField
      FieldName = 'Diferenca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGIndevido: TFloatField
      FieldName = 'Indevido'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGSubProduto: TFloatField
      FieldName = 'SubProduto'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGErrEmpresa: TFloatField
      FieldName = 'ErrEmpresa'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGETE: TFloatField
      FieldName = 'ETE'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGBalancoIndev: TFloatField
      FieldName = 'BalancoIndev'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrEFD_K_ConfGSaiuDesmonte: TFloatField
      FieldName = 'SaiuDesmonte'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGEntrouDesmonte: TFloatField
      FieldName = 'EntrouDesmonte'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGProduReforma: TFloatField
      FieldName = 'ProduReforma'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrEFD_K_ConfGInsumReforma: TFloatField
      FieldName = 'InsumReforma'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object DsEFD_K_ConfG: TDataSource
    DataSet = QrEFD_K_ConfG
    Left = 816
    Top = 612
  end
  object frxEFD_SPEDE_K_VERIF_001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxEFD_SPEDE_K200_001GetValue
    Left = 504
    Top = 560
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEFD_K_ConfG
        DataSetName = 'frxDsEFD_K_ConfG'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 48.000024410000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Shape3: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 971.339210000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 744.567410000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'K - MOVIMENTA'#199#195'O POR PRODUTO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA_SPED]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 857.953310000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 41.574830000000000000
          Top = 37.795300000000000000
          Width = 143.622047244094500000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Top = 37.795300000000000000
          Width = 41.574805590000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 899.528140000000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Final')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 185.196850390000000000
          Top = 37.795300000000000000
          Width = 32.125984251968500000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 648.188998350000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vendido')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 576.377952755905500000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 468.661417322834600000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Consumido')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 396.850393700787400000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Left = 325.039370078740200000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Produzido')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Left = 253.228346456692900000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Entrada')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 217.322834645669300000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inicial')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo17: TfrxMemoView
          Left = 935.433070866141700000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Diferen'#231'a')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo28: TfrxMemoView
          Left = 755.906000000000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-produto')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo30: TfrxMemoView
          Left = 827.717070000000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Indevido')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo35: TfrxMemoView
          Left = 791.811077320000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Err. Empresa')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo38: TfrxMemoView
          Left = 720.000075670000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ETE')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo39: TfrxMemoView
          Left = 863.622078980000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Balan. indev.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo44: TfrxMemoView
          Left = 684.094930000000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Bal.corrigido')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Left = 432.755905511811000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saiu em desmonte')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo53: TfrxMemoView
          Left = 540.472440939999900000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Insu.cons.corr')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo54: TfrxMemoView
          Left = 504.566929130000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo59: TfrxMemoView
          Left = 289.133858267716500000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Entrou de desmonte')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo62: TfrxMemoView
          Left = 612.283860000000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Corr.Produ.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo65: TfrxMemoView
          Left = 360.944881890000000000
          Top = 37.795300000000000000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Corr.Produ.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 10.204724410000000000
        Top = 128.504020000000000000
        Width = 971.339210000000000000
        DataSet = frxDsEFD_K_ConfG
        DataSetName = 'frxDsEFD_K_ConfG'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 41.574830000000000000
          Width = 143.622047244094500000
          Height = 10.204724409448820000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 41.574803150000000000
          Height = 10.204724409448820000
          DataField = 'GraGruX'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 935.433070866141700000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataField = 'Diferenca'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."Diferenca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 185.196850390000000000
          Width = 32.125984251968500000
          Height = 10.204724409448820000
          DataField = 'Sigla'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 899.528140000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataField = 'Final'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."Final"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 648.188998350000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataField = 'Venda'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."Venda"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 576.377952755905500000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataField = 'SaiuClasse'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."SaiuClasse"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 468.661417322834600000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataField = 'Consumo'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."Consumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 396.850393700787400000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."EntrouClasse"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 325.039370078740200000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."Producao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 253.228346456692900000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."Compra"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 217.322834645669300000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataField = 'Sdo_Ini'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."Sdo_Ini"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 755.906000000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataField = 'SubProduto'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."SubProduto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 827.717070000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataField = 'Indevido'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."Indevido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 791.811077320000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataField = 'ErrEmpresa'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."ErrEmpresa"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 720.000075670000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataField = 'ETE'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."ETE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 863.622078980000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."BalancoIndev"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 684.094930000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 432.755905511811000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."SaiuDesmonte"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 540.472440944881900000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataField = 'InsumReforma'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."InsumReforma"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 504.566929130000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 289.133858267716500000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."EntrouDesmonte"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 612.283860000000000000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 360.944881889763800000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataField = 'ProduReforma'
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEFD_K_ConfG."ProduReforma"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 249.448980000000000000
        Width = 971.339210000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 706.772110000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 457.323066540000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 25.322844410000000000
        Top = 200.315090000000000000
        Width = 971.339210000000000000
        object Memo19: TfrxMemoView
          Left = 935.433070866141700000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."Diferenca">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 791.811077320000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."ErrEmpresa">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 648.188998350000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."Venda">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 576.377952755905500000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."SaiuClasse">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 468.661417320000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."Consumo">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 396.850393700787400000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."EntrouClasse">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 325.039370080000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."Producao">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 253.228346456692900000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."Compra">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 217.322834645669300000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."Sdo_Ini">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 755.906000000000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."SubProduto">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 720.000075670000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."Indevido">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 899.528140000000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."Final">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 863.622078980000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."BalancoIndev">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 827.717070000000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."Indevido">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 684.094930000000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 432.755905511811000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."SaiuDesmonte">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 540.472440939999900000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."Insumreforma">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 504.566929130000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000010000
          Width = 217.322834645669300000
          Height = 10.204724409448820000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 289.133858267716500000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."EntrouDesmonte">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 612.283860000000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724409448820000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 360.944881890000000000
          Top = 15.118120000000010000
          Width = 35.905511810000000000
          Height = 10.204724410000000000
          DataSet = frxDsEFD_K_ConfG
          DataSetName = 'frxDsEFD_K_ConfG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEFD_K_ConfG."ProduReforma">,MD002)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsEFD_K_ConfG: TfrxDBDataset
    UserName = 'frxDsEFD_K_ConfG'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GraGruX=GraGruX'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Grandeza=Grandeza'
      'Sdo_Ini=Sdo_Ini'
      'Compra=Compra'
      'Producao=Producao'
      'EntrouClasse=EntrouClasse'
      'Consumo=Consumo'
      'SaiuClasse=SaiuClasse'
      'Venda=Venda'
      'Final=Final'
      'Diferenca=Diferenca'
      'Indevido=Indevido'
      'SubProduto=SubProduto'
      'ErrEmpresa=ErrEmpresa'
      'ETE=ETE'
      'BalancoIndev=BalancoIndev'
      'ESTSTabSorc=ESTSTabSorc'
      'SaiuDesmonte=SaiuDesmonte'
      'EntrouDesmonte=EntrouDesmonte'
      'ProduReforma=ProduReforma'
      'InsumReforma=InsumReforma')
    DataSet = QrEFD_K_ConfG
    BCDToCurrency = False
    Left = 504
    Top = 608
  end
  object QrPsq01: TmySQLQuery
    Database = Dmod.MyDB
    Left = 888
    Top = 564
  end
  object DsPsq01: TDataSource
    DataSet = QrPsq01
    Left = 888
    Top = 616
  end
  object frxErrEmpresa: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxEFD_SPEDE_K200_001GetValue
    Left = 244
    Top = 684
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsErrEmpresa
        DataSetName = 'frxDsErrEmpresa'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Itens de Erros de Empresa')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA_SPED]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 279.685220000000000000
          Top = 51.023622050000000000
          Width = 219.212642360000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do material')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Left = 238.110390000000000000
          Top = 51.023622050000000000
          Width = 41.574805590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 619.842920000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 559.370440000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 498.897960000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Left = 498.897960000000000000
          Top = 51.023622050000000000
          Width = 181.417400940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ind'#250'stria')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 98.267780000000000000
          Top = 64.251944090000000000
          Width = 15.118095590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Left = 113.385900000000000000
          Top = 64.251944090000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Codigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 154.960730000000000000
          Top = 64.251944090000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-C')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo14: TfrxMemoView
          Left = 196.535560000000000000
          Top = 51.023622050000000000
          Width = 41.574805590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo16: TfrxMemoView
          Top = 64.252010000000000000
          Width = 15.118095590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo17: TfrxMemoView
          Left = 15.118120000000000000
          Top = 64.252010000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Codigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo18: TfrxMemoView
          Left = 56.692950000000000000
          Top = 64.252010000000000000
          Width = 41.574805590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo19: TfrxMemoView
          Left = 98.267780000000000000
          Top = 51.023622050000000000
          Width = 98.267740940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Registro')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo23: TfrxMemoView
          Top = 51.023622050000000000
          Width = 98.267740940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Origem')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        DataSet = frxDsErrEmpresa
        DataSetName = 'frxDsErrEmpresa'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 279.685220000000000000
          Width = 219.212642360000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrEmpresa."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 238.110390000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'PesoKg'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErrEmpresa."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Left = 559.370440000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'AreaM2'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErrEmpresa."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Left = 498.897960000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsErrEmpresa."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo2: TfrxMemoView
          Left = 98.267780000000000000
          Width = 15.118110240000000000
          Height = 13.228346460000000000
          DataField = 'MovimID'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."MovimID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 113.385900000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'Codigo'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 154.960730000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'MovimCod'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."MovimCod"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 196.535560000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'Empresa'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."Empresa"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Width = 15.118110240000000000
          Height = 13.228346460000000000
          DataField = 'SrcMovID'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."SrcMovID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 15.118120000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'SrcNivel1'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."SrcNivel1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 56.692950000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'SrcNivel2'
          DataSet = frxDsErrEmpresa
          DataSetName = 'frxDsErrEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrEmpresa."SrcNivel2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 415.748300000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 272.126160000000000000
          Width = 143.622076540000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'impresso em [VARF_DATA_IMP]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118120000000000000
          Width = 498.897764720000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 498.897960000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsErrEmpresa."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 559.370440000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsErrEmpresa."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 619.842920000000000000
          Top = 15.118120000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsErrEmpresa."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrErrEmpresa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vmi.Codigo, vmi.MovimID, vmi.MovimCod, vmi.Empresa,  '
      'vmi.GraGruX, Pecas, AreaM2, PesoKg, med.Grandeza, '
      'CONCAT(gg1.Nome, IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) NO_PRD_TAM_COR '
      'FROM vsmovits vmi   '
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX   '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed   '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'WHERE DataHora  BETWEEN "2016-01-01" AND "2017-12-31 23:59:59"  '
      'AND Empresa>-11  ')
    Left = 244
    Top = 588
    object QrErrEmpresaSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrErrEmpresaSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrErrEmpresaSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrErrEmpresaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrErrEmpresaMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrErrEmpresaMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrErrEmpresaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrErrEmpresaGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrErrEmpresaPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrErrEmpresaAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrErrEmpresaPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrErrEmpresaGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrErrEmpresaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
  end
  object frxDsErrEmpresa: TfrxDBDataset
    UserName = 'frxDsErrEmpresa'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'Codigo=Codigo'
      'MovimID=MovimID'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'PesoKg=PesoKg'
      'Grandeza=Grandeza'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR')
    DataSet = QrErrEmpresa
    BCDToCurrency = False
    Left = 244
    Top = 636
  end
  object QrPsq02: TmySQLQuery
    Database = Dmod.MyDB
    Left = 944
    Top = 564
  end
  object DsPsq02: TDataSource
    DataSet = QrPsq02
    Left = 944
    Top = 616
  end
  object QrEFD_1010: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM efd_1010')
    Left = 444
    Top = 372
    object QrEFD_1010ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_1010AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_1010Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_1010LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_1010REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_1010IND_EXP: TWideStringField
      FieldName = 'IND_EXP'
      Size = 1
    end
    object QrEFD_1010IND_CCRF: TWideStringField
      FieldName = 'IND_CCRF'
      Size = 1
    end
    object QrEFD_1010IND_COMB: TWideStringField
      FieldName = 'IND_COMB'
      Size = 1
    end
    object QrEFD_1010IND_USINA: TWideStringField
      FieldName = 'IND_USINA'
      Size = 1
    end
    object QrEFD_1010IND_VA: TWideStringField
      FieldName = 'IND_VA'
      Size = 1
    end
    object QrEFD_1010IND_EE: TWideStringField
      FieldName = 'IND_EE'
      Size = 1
    end
    object QrEFD_1010IND_CART: TWideStringField
      FieldName = 'IND_CART'
      Size = 1
    end
    object QrEFD_1010IND_FORM: TWideStringField
      FieldName = 'IND_FORM'
      Size = 1
    end
    object QrEFD_1010IND_AER: TWideStringField
      FieldName = 'IND_AER'
      Size = 1
    end
    object QrEFD_1010Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_1010DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_1010DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_1010UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_1010UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_1010AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_1010Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_1010: TDataSource
    DataSet = QrEFD_1010
    Left = 444
    Top = 416
  end
  object QrEFD_K210: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K210BeforeClose
    AfterScroll = QrEFD_K210AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'IF(k210.DT_FIN_OS = 0, "", '
      'DATE_FORMAT(k210.DT_FIN_OS, "%d/%m/%Y")) DT_FIN_OS_Txt, '
      'K210.* '
      'FROM efd_K210 k210 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K210.COD_ITEM_ORI '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed '
      'WHERE k210.ImporExpor=3'
      'AND k210.Empresa=-11'
      'AND k210.AnoMes=201609'
      'AND k210.K100=1'
      'ORDER BY DT_INI_OS, DT_FIN_OS, COD_DOC_OS, COD_ITEM ')
    Left = 288
    Top = 300
    object QrEFD_K210OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrEFD_K210Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K210Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K210NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K210DT_FIN_OS_Txt: TWideStringField
      FieldName = 'DT_FIN_OS_Txt'
      Size = 10
    end
    object QrEFD_K210ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K210AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K210Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K210LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K210REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K210K100: TIntegerField
      FieldName = 'K100'
    end
    object QrEFD_K210ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K210MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K210Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K210MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K210DT_INI_OS: TDateField
      FieldName = 'DT_INI_OS'
    end
    object QrEFD_K210DT_FIN_OS: TDateField
      FieldName = 'DT_FIN_OS'
    end
    object QrEFD_K210COD_DOC_OS: TWideStringField
      FieldName = 'COD_DOC_OS'
      Size = 30
    end
    object QrEFD_K210COD_ITEM_ORI: TWideStringField
      FieldName = 'COD_ITEM_ORI'
      Size = 60
    end
    object QrEFD_K210QTD_ORI: TFloatField
      FieldName = 'QTD_ORI'
    end
    object QrEFD_K210GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K210Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K210DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K210DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K210UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K210UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K210AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K210Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_K210: TDataSource
    DataSet = QrEFD_K210
    Left = 288
    Top = 344
  end
  object QrEFD_K215: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'k215.* '
      'FROM efd_k215 k215 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k215.COD_ITEM_DES '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed '
      'WHERE k215.ImporExpor=3'
      'AND k215.Empresa=-11'
      'AND k215.AnoMes=201609'
      'AND k215.K210=1')
    Left = 288
    Top = 392
    object QrEFD_K215Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K215Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K215NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K215ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K215AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K215Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K215LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K215REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K215K210: TIntegerField
      FieldName = 'K210'
    end
    object QrEFD_K215COD_ITEM_DES: TWideStringField
      FieldName = 'COD_ITEM_DES'
      Size = 60
    end
    object QrEFD_K215QTD_DES: TFloatField
      FieldName = 'QTD_DES'
    end
    object QrEFD_K215ID_Item: TLargeintField
      FieldName = 'ID_Item'
    end
    object QrEFD_K215MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K215Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K215MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K215Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEFD_K215GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K215ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrEFD_K215Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K215DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K215DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K215UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K215UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K215AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K215Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_K215: TDataSource
    DataSet = QrEFD_K215
    Left = 288
    Top = 440
  end
  object frxDsEFD_K215: TfrxDBDataset
    UserName = 'frxDsEFD_K215'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K210=K210'
      'COD_ITEM_DES=COD_ITEM_DES'
      'QTD_DES=QTD_DES'
      'ID_Item=ID_Item'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Controle=Controle'
      'GraGruX=GraGruX'
      'ESTSTabSorc=ESTSTabSorc'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrEFD_K215
    BCDToCurrency = False
    Left = 652
    Top = 632
  end
  object frxDsEFD_K210: TfrxDBDataset
    UserName = 'frxDsEFD_K210'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'DT_FIN_OS_Txt=DT_FIN_OS_Txt'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K100=K100'
      'ID_SEK=ID_SEK'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'DT_INI_OS=DT_INI_OS'
      'DT_FIN_OS=DT_FIN_OS'
      'COD_DOC_OS=COD_DOC_OS'
      'COD_ITEM_ORI=COD_ITEM_ORI'
      'QTD_ORI=QTD_ORI'
      'GraGruX=GraGruX'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrEFD_K210
    BCDToCurrency = False
    Left = 652
    Top = 584
  end
  object QrEFD_K270_230: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K270_230BeforeClose
    AfterScroll = QrEFD_K270_230AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR,  '
      'K270.* '
      'FROM efd_K270 k270 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed  '
      'WHERE k270.ImporExpor=3'
      'AND k270.Empresa=-11'
      '/*AND k270.AnoMes='
      'AND k270.K100=*/'
      'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ')
    Left = 552
    Top = 304
    object QrEFD_K270_230Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K270_230Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K270_230NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K270_230ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K270_230AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K270_230Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K270_230LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K270_230REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K270_230K100: TIntegerField
      FieldName = 'K100'
    end
    object QrEFD_K270_230ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K270_230MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K270_230Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K270_230MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K270_230DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrEFD_K270_230DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrEFD_K270_230COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrEFD_K270_230COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K270_230QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
    end
    object QrEFD_K270_230QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
    end
    object QrEFD_K270_230ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrEFD_K270_230GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K270_230Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K270_230DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K270_230DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K270_230UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K270_230UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K270_230AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K270_230Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_K270_230: TDataSource
    DataSet = QrEFD_K270_230
    Left = 552
    Top = 348
  end
  object QrEFD_K275_235: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,  '
      'CONCAT(gg1.Nome,   '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR, '
      'k275.*  '
      'FROM efd_k275 k275 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   '
      'WHERE k275.ImporExpor=3'
      'AND k275.Empresa=-11'
      '/*AND k275.AnoMes='
      'AND k275.K230=*/')
    Left = 552
    Top = 396
    object QrEFD_K275_235Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K275_235Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K275_235NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K275_235ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K275_235AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K275_235Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K275_235LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K275_235REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K275_235K270: TIntegerField
      FieldName = 'K270'
    end
    object QrEFD_K275_235ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K275_235COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K275_235QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
    end
    object QrEFD_K275_235QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
    end
    object QrEFD_K275_235COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrEFD_K275_235ID_Item: TLargeintField
      FieldName = 'ID_Item'
    end
    object QrEFD_K275_235MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K275_235Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K275_235MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K275_235Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEFD_K275_235GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K275_235ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrEFD_K275_235Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K275_235DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K275_235DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K275_235UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K275_235UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K275_235AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K275_235Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_K275_235: TDataSource
    DataSet = QrEFD_K275_235
    Left = 552
    Top = 444
  end
  object QrEFD_K270_220: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K270_220BeforeClose
    AfterScroll = QrEFD_K270_220AfterScroll
    SQL.Strings = (
      'SELECT  '
      'CONCAT(gg1o.Nome, '
      'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), '
      'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) '
      'NO_PRD_TAM_COR, unmo.Sigla, '
      'K270.* '
      'FROM efd_K270 K270 '
      'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K270.COD_ITEM'
      'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC '
      'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad '
      'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI '
      'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 '
      'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed '
      'WHERE k270.ImporExpor=3'
      'AND k270.Empresa=-11'
      'AND k270.AnoMes>0'
      'AND k270.K100>0'
      'AND k270.ORIGEM>0'
      'ORDER BY k270.MovimCod, k270.DT_INI_AP, DT_FIN_AP, COD_ITEM')
    Left = 492
    Top = 328
    object QrEFD_K270_220NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K270_220Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K270_220ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K270_220AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K270_220Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K270_220LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K270_220REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K270_220K100: TIntegerField
      FieldName = 'K100'
    end
    object QrEFD_K270_220ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K270_220MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K270_220Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K270_220MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K270_220DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrEFD_K270_220DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrEFD_K270_220COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrEFD_K270_220COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K270_220QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
    end
    object QrEFD_K270_220QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
    end
    object QrEFD_K270_220ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrEFD_K270_220GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K270_220Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K270_220DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K270_220DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K270_220UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K270_220UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K270_220AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K270_220Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_K270_220: TDataSource
    DataSet = QrEFD_K270_220
    Left = 492
    Top = 372
  end
  object QrEFD_K275_220: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT'
      'CONCAT(gg1o.Nome,'
      'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)),'
      'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome)))'
      'NO_PRD_TAM_COR, unmo.Sigla,'
      'K275.*'
      'FROM efd_K275 K275'
      'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K275.COD_ITEM'
      'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC'
      'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad'
      'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI'
      'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1'
      'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed'
      'WHERE k275.ImporExpor=3'
      'AND k275.Empresa=-11'
      'AND k275.AnoMes>0'
      'AND k275.K270>0')
    Left = 492
    Top = 420
    object QrEFD_K275_220NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K275_220Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K275_220ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K275_220AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K275_220Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K275_220LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K275_220REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K275_220K270: TIntegerField
      FieldName = 'K270'
    end
    object QrEFD_K275_220ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K275_220COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K275_220QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
    end
    object QrEFD_K275_220QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
    end
    object QrEFD_K275_220COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrEFD_K275_220ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrEFD_K275_220ID_Item: TLargeintField
      FieldName = 'ID_Item'
    end
    object QrEFD_K275_220MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K275_220Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K275_220MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K275_220Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEFD_K275_220GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K275_220ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrEFD_K275_220Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K275_220DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K275_220DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K275_220UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K275_220UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K275_220AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K275_220Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_K275_220: TDataSource
    DataSet = QrEFD_K275_220
    Left = 492
    Top = 464
  end
  object frxDsEFD_K270_230: TfrxDBDataset
    UserName = 'frxDsEFD_K270_230'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K100=K100'
      'ID_SEK=ID_SEK'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'DT_INI_AP=DT_INI_AP'
      'DT_FIN_AP=DT_FIN_AP'
      'COD_OP_OS=COD_OP_OS'
      'COD_ITEM=COD_ITEM'
      'QTD_COR_POS=QTD_COR_POS'
      'QTD_COR_NEG=QTD_COR_NEG'
      'ORIGEM=ORIGEM'
      'GraGruX=GraGruX'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrEFD_K270_230
    BCDToCurrency = False
    Left = 612
    Top = 492
  end
  object frxDsEFD_K275_235: TfrxDBDataset
    UserName = 'frxDsEFD_K275_235'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grandeza=Grandeza'
      'Sigla=Sigla'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K270=K270'
      'ID_SEK=ID_SEK'
      'COD_ITEM=COD_ITEM'
      'QTD_COR_POS=QTD_COR_POS'
      'QTD_COR_NEG=QTD_COR_NEG'
      'COD_INS_SUBST=COD_INS_SUBST'
      'ID_Item=ID_Item'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Controle=Controle'
      'GraGruX=GraGruX'
      'ESTSTabSorc=ESTSTabSorc'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrEFD_K275_235
    BCDToCurrency = False
    Left = 612
    Top = 540
  end
  object frxDsEFD_K270_220: TfrxDBDataset
    UserName = 'frxDsEFD_K270_220'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Sigla=Sigla'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K100=K100'
      'ID_SEK=ID_SEK'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'DT_INI_AP=DT_INI_AP'
      'DT_FIN_AP=DT_FIN_AP'
      'COD_OP_OS=COD_OP_OS'
      'COD_ITEM=COD_ITEM'
      'QTD_COR_POS=QTD_COR_POS'
      'QTD_COR_NEG=QTD_COR_NEG'
      'ORIGEM=ORIGEM'
      'GraGruX=GraGruX'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrEFD_K270_220
    BCDToCurrency = False
    Left = 328
    Top = 752
  end
  object frxDsEFD_K275_220: TfrxDBDataset
    UserName = 'frxDsEFD_K275_220'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'Sigla=Sigla'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'LinArq=LinArq'
      'REG=REG'
      'K270=K270'
      'ID_SEK=ID_SEK'
      'COD_ITEM=COD_ITEM'
      'QTD_COR_POS=QTD_COR_POS'
      'QTD_COR_NEG=QTD_COR_NEG'
      'COD_INS_SUBST=COD_INS_SUBST'
      'ORIGEM=ORIGEM'
      'ID_Item=ID_Item'
      'MovimID=MovimID'
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Controle=Controle'
      'GraGruX=GraGruX'
      'ESTSTabSorc=ESTSTabSorc'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrEFD_K275_220
    BCDToCurrency = False
    Left = 328
    Top = 796
  end
  object QrEFD_K270_250: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K270_250BeforeClose
    AfterScroll = QrEFD_K270_250AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, K270.*'
      'FROM efd_K270 k270'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE k270.ImporExpor=3'
      'AND k270.Empresa=-11'
      'AND k270.AnoMes=201701'
      'AND k270.K100=1'
      'AND ORIGEM=2'
      'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM')
    Left = 616
    Top = 280
    object QrEFD_K270_250Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K270_250Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K270_250NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K270_250ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K270_250AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K270_250Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K270_250LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K270_250REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K270_250K100: TIntegerField
      FieldName = 'K100'
    end
    object QrEFD_K270_250ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K270_250MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K270_250Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K270_250MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K270_250DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrEFD_K270_250DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrEFD_K270_250COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrEFD_K270_250COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K270_250QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
    end
    object QrEFD_K270_250QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
    end
    object QrEFD_K270_250ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrEFD_K270_250GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K270_250Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K270_250DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K270_250DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K270_250UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K270_250UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K270_250AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K270_250Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_K270_250: TDataSource
    DataSet = QrEFD_K270_250
    Left = 616
    Top = 328
  end
  object QrEFD_K275_255: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,  '
      'CONCAT(gg1.Nome,   '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR, '
      'k275.*  '
      'FROM efd_k275 k275 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   '
      'WHERE k275.ImporExpor=3'
      'AND k275.Empresa=-11'
      'AND k275.AnoMes=201701'
      'AND k275.K270=2')
    Left = 616
    Top = 376
    object QrEFD_K275_255Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K275_255Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K275_255NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K275_255ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K275_255AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K275_255Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K275_255LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K275_255REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K275_255K270: TIntegerField
      FieldName = 'K270'
    end
    object QrEFD_K275_255ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K275_255COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K275_255QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
    end
    object QrEFD_K275_255QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
    end
    object QrEFD_K275_255COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrEFD_K275_255ID_Item: TLargeintField
      FieldName = 'ID_Item'
    end
    object QrEFD_K275_255MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K275_255Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K275_255MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K275_255Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEFD_K275_255GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K275_255ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrEFD_K275_255Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K275_255DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K275_255DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K275_255UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K275_255UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K275_255AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K275_255Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_K275_255: TDataSource
    DataSet = QrEFD_K275_255
    Left = 616
    Top = 424
  end
  object QrEFD_K270_210: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K270_210BeforeClose
    AfterScroll = QrEFD_K270_210AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla, '
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, K270.* '
      'FROM efd_K270 k270 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed '
      'WHERE k270.ImporExpor=3 '
      'AND k270.Empresa=-11 '
      'AND k270.AnoMes=201701 '
      'AND k270.K100=1 '
      'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM ')
    Left = 616
    Top = 12
    object QrEFD_K270_210Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K270_210Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K270_210NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K270_210ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K270_210AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K270_210Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K270_210LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K270_210REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K270_210K100: TIntegerField
      FieldName = 'K100'
    end
    object QrEFD_K270_210ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K270_210MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K270_210Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K270_210MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K270_210DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrEFD_K270_210DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrEFD_K270_210COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrEFD_K270_210COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K270_210QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
    end
    object QrEFD_K270_210QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
    end
    object QrEFD_K270_210ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrEFD_K270_210GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K270_210Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K270_210DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K270_210DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K270_210UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K270_210UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K270_210AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K270_210Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_K270_210: TDataSource
    DataSet = QrEFD_K270_210
    Left = 616
    Top = 60
  end
  object QrEFD_K275_215: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K270_250BeforeClose
    AfterScroll = QrEFD_K270_250AfterScroll
    SQL.Strings = (
      'SELECT '
      'CONCAT(gg1o.Nome, '
      'IF(gtio.PrintTam=0, "", CONCAT(" ", gtio.Nome)), '
      'IF(gcco.PrintCor=0,"", CONCAT(" ", gcco.Nome))) '
      'NO_PRD_TAM_COR, unmo.Sigla, '
      'K275.* '
      'FROM efd_K275 K275 '
      'LEFT JOIN gragrux    ggxo ON ggxo.Controle=K275.COD_ITEM '
      'LEFT JOIN gragruc    ggco ON ggco.Controle=ggxo.GraGruC '
      'LEFT JOIN gracorcad  gcco ON gcco.Codigo=ggco.GraCorCad '
      'LEFT JOIN gratamits  gtio ON gtio.Controle=ggxo.GraTamI '
      'LEFT JOIN gragru1    gg1o ON gg1o.Nivel1=ggxo.GraGru1 '
      'LEFT JOIN unidmed    unmo ON unmo.Codigo=gg1o.UnidMed ')
    Left = 720
    Top = 12
    object QrEFD_K275_215NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K275_215Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K275_215ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K275_215AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K275_215Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K275_215LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K275_215REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K275_215K270: TIntegerField
      FieldName = 'K270'
    end
    object QrEFD_K275_215ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K275_215COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K275_215QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
    end
    object QrEFD_K275_215QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
    end
    object QrEFD_K275_215COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrEFD_K275_215ID_Item: TLargeintField
      FieldName = 'ID_Item'
    end
    object QrEFD_K275_215MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K275_215Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K275_215MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K275_215Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEFD_K275_215GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K275_215ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrEFD_K275_215Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K275_215DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K275_215DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K275_215UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K275_215UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K275_215AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K275_215Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_K275_215: TDataSource
    DataSet = QrEFD_K275_215
    Left = 720
    Top = 60
  end
  object QrEFD_K260: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K260BeforeClose
    AfterScroll = QrEFD_K260AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR,'
      'IF(k260.DT_RET = 0, "",'
      'DATE_FORMAT(k260.DT_RET, "%d/%m/%Y")) DT_RET_Txt,'
      'K260.*'
      'FROM efd_K260 k260'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K260.COD_ITEM'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE k260.ImporExpor=3'
      'AND k260.Empresa=-11'
      'AND k260.AnoMes=201701'
      'AND k260.K100=1'
      'ORDER BY DT_SAIDA, DT_RET, COD_OP_OS, COD_ITEM')
    Left = 676
    Top = 304
    object QrEFD_K260Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K260Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K260NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K260DT_RET_Txt: TWideStringField
      FieldName = 'DT_RET_Txt'
      Size = 10
    end
    object QrEFD_K260ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K260AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K260Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K260LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K260REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K260K100: TIntegerField
      FieldName = 'K100'
    end
    object QrEFD_K260ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K260MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K260Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K260MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K260COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrEFD_K260COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K260DT_SAIDA: TDateField
      FieldName = 'DT_SAIDA'
    end
    object QrEFD_K260QTD_SAIDA: TFloatField
      FieldName = 'QTD_SAIDA'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrEFD_K260DT_RET: TDateField
      FieldName = 'DT_RET'
    end
    object QrEFD_K260QTD_RET: TFloatField
      FieldName = 'QTD_RET'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrEFD_K260GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K260Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K260DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K260DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K260UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K260UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K260AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K260Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_K260OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
  end
  object QrEFD_K265: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR,'
      'k265.*'
      'FROM efd_k265 k265'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k265.COD_ITEM'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE k265.ImporExpor=3'
      'AND k265.Empresa=-11'
      'AND k265.AnoMes=201601'
      'AND k265.K260=1')
    Left = 672
    Top = 396
    object QrEFD_K265Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K265Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K265NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K265ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K265AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K265Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K265LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K265REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K265K260: TIntegerField
      FieldName = 'K260'
    end
    object QrEFD_K265ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K265COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K265QTD_CONS: TFloatField
      FieldName = 'QTD_CONS'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrEFD_K265QTD_RET: TFloatField
      FieldName = 'QTD_RET'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrEFD_K265ID_Item: TLargeintField
      FieldName = 'ID_Item'
    end
    object QrEFD_K265MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K265Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K265MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K265Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEFD_K265GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K265ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrEFD_K265Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K265DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K265DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K265UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K265UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K265AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K265Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_K265OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
  end
  object DsEFD_K260: TDataSource
    DataSet = QrEFD_K260
    Left = 676
    Top = 352
  end
  object DsEFD_K265: TDataSource
    DataSet = QrEFD_K265
    Left = 672
    Top = 444
  end
  object QrEFD_K280: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(K280.COD_PART="", "", IF(ter.Tipo=0, '
      'ter.RazaoSocial, ter.Nome)) NO_PART, '
      'unm.Sigla, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      
        ' ELT(k280.IND_EST + 1,"Estoque de propriedade do informante e em' +
        ' seu poder","Estoque de propriedade do informante e em posse de ' +
        'terceiros","Estoque de propriedade de terceiros e em posse do in' +
        'formante","???") NO_IND_EST,'
      'K280.* '
      'FROM efd_K280 K280 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K280.COD_ITEM '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed '
      'LEFT JOIN entidades  ter ON ter.Codigo=K280.COD_PART '
      'WHERE k280.ImporExpor=3'
      'AND k280.Empresa=-11'
      'AND k280.AnoMes=201701'
      'AND k280.K100=1'
      'ORDER BY DT_EST ')
    Left = 736
    Top = 508
    object QrEFD_K280NO_PART: TWideStringField
      FieldName = 'NO_PART'
      Size = 100
    end
    object QrEFD_K280Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K280NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K280NO_IND_EST: TWideStringField
      FieldName = 'NO_IND_EST'
      Size = 60
    end
    object QrEFD_K280ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K280AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K280Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K280LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K280REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K280K100: TIntegerField
      FieldName = 'K100'
    end
    object QrEFD_K280COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K280QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
    end
    object QrEFD_K280QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
    end
    object QrEFD_K280IND_EST: TWideStringField
      FieldName = 'IND_EST'
      Size = 1
    end
    object QrEFD_K280COD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEFD_K280BalID: TIntegerField
      FieldName = 'BalID'
    end
    object QrEFD_K280BalNum: TIntegerField
      FieldName = 'BalNum'
    end
    object QrEFD_K280BalItm: TIntegerField
      FieldName = 'BalItm'
    end
    object QrEFD_K280BalEnt: TIntegerField
      FieldName = 'BalEnt'
    end
    object QrEFD_K280GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K280Grandeza: TIntegerField
      FieldName = 'Grandeza'
    end
    object QrEFD_K280Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrEFD_K280AreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrEFD_K280PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrEFD_K280Entidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrEFD_K280Tipo_Item: TIntegerField
      FieldName = 'Tipo_Item'
    end
    object QrEFD_K280Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K280DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K280DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K280UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K280UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K280AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K280Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEFD_K280ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrEFD_K280OriOpeProc: TSmallintField
      FieldName = 'OriOpeProc'
    end
    object QrEFD_K280OrigemIDKnd: TIntegerField
      FieldName = 'OrigemIDKnd'
    end
    object QrEFD_K280DT_EST: TDateField
      FieldName = 'DT_EST'
    end
    object QrEFD_K280NO_OriSPEDEFDKnd: TWideStringField
      FieldName = 'NO_OriSPEDEFDKnd'
      Size = 30
    end
    object QrEFD_K280RegisPai: TWideStringField
      FieldName = 'RegisPai'
      Size = 4
    end
    object QrEFD_K280RegisAvo: TWideStringField
      FieldName = 'RegisAvo'
      Size = 4
    end
  end
  object DsEFD_K280: TDataSource
    DataSet = QrEFD_K280
    Left = 736
    Top = 556
  end
  object PMK280: TPopupMenu
    OnPopup = PMK280Popup
    Left = 796
    Top = 468
    object ExcluiK280_1: TMenuItem
      Caption = '&Exclui item(ns)'
      OnClick = ExcluiK280_1Click
    end
  end
  object QrEFD_K270_260: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEFD_K270_260BeforeClose
    AfterScroll = QrEFD_K270_260AfterScroll
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, K270.*'
      'FROM efd_K270 k270'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=K270.COD_ITEM'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE k270.ImporExpor=3'
      'AND k270.Empresa=-11'
      'AND k270.AnoMes=201701'
      'AND k270.K100=1'
      'AND ORIGEM=2'
      'ORDER BY DT_INI_AP, DT_FIN_AP, COD_OP_OS, COD_ITEM')
    Left = 740
    Top = 280
    object QrEFD_K270_260Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K270_260Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K270_260NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K270_260ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K270_260AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K270_260Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K270_260LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K270_260REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K270_260K100: TIntegerField
      FieldName = 'K100'
    end
    object QrEFD_K270_260ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K270_260MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K270_260Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K270_260MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K270_260DT_INI_AP: TDateField
      FieldName = 'DT_INI_AP'
    end
    object QrEFD_K270_260DT_FIN_AP: TDateField
      FieldName = 'DT_FIN_AP'
    end
    object QrEFD_K270_260COD_OP_OS: TWideStringField
      FieldName = 'COD_OP_OS'
      Size = 30
    end
    object QrEFD_K270_260COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K270_260QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
    end
    object QrEFD_K270_260QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
    end
    object QrEFD_K270_260ORIGEM: TWideStringField
      FieldName = 'ORIGEM'
      Size = 1
    end
    object QrEFD_K270_260GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K270_260Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K270_260DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K270_260DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K270_260UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K270_260UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K270_260AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K270_260Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_K270_260: TDataSource
    DataSet = QrEFD_K270_260
    Left = 740
    Top = 328
  end
  object QrEFD_K275_265: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT unm.Grandeza, unm.Sigla,  '
      'CONCAT(gg1.Nome,   '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),   '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   '
      'NO_PRD_TAM_COR, '
      'k275.*  '
      'FROM efd_k275 k275 '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=k275.COD_ITEM  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed   '
      'WHERE k275.ImporExpor=3'
      'AND k275.Empresa=-11'
      'AND k275.AnoMes=201701'
      'AND k275.K270=2')
    Left = 740
    Top = 376
    object QrEFD_K275_265Grandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEFD_K275_265Sigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrEFD_K275_265NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEFD_K275_265ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrEFD_K275_265AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrEFD_K275_265Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEFD_K275_265LinArq: TIntegerField
      FieldName = 'LinArq'
    end
    object QrEFD_K275_265REG: TWideStringField
      FieldName = 'REG'
      Size = 4
    end
    object QrEFD_K275_265K270: TIntegerField
      FieldName = 'K270'
    end
    object QrEFD_K275_265ID_SEK: TSmallintField
      FieldName = 'ID_SEK'
    end
    object QrEFD_K275_265COD_ITEM: TWideStringField
      FieldName = 'COD_ITEM'
      Size = 60
    end
    object QrEFD_K275_265QTD_COR_POS: TFloatField
      FieldName = 'QTD_COR_POS'
    end
    object QrEFD_K275_265QTD_COR_NEG: TFloatField
      FieldName = 'QTD_COR_NEG'
    end
    object QrEFD_K275_265COD_INS_SUBST: TWideStringField
      FieldName = 'COD_INS_SUBST'
      Size = 60
    end
    object QrEFD_K275_265ID_Item: TLargeintField
      FieldName = 'ID_Item'
    end
    object QrEFD_K275_265MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrEFD_K275_265Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEFD_K275_265MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrEFD_K275_265Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEFD_K275_265GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrEFD_K275_265ESTSTabSorc: TIntegerField
      FieldName = 'ESTSTabSorc'
    end
    object QrEFD_K275_265Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEFD_K275_265DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEFD_K275_265DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEFD_K275_265UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEFD_K275_265UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEFD_K275_265AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEFD_K275_265Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEFD_K275_265: TDataSource
    DataSet = QrEFD_K275_265
    Left = 740
    Top = 424
  end
end
