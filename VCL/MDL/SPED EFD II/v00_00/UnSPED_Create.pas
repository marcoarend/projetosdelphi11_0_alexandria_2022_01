unit UnSPED_Create;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnMLAGeral,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTable = (
    ntrttSPEDEFD_0190, ntrttSPEDEFD_0200);
  TAcaoCreate = (acDrop, acCreate, acFind);
  TSPED_Create = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrttSPEDEFD_0190(Qry: TmySQLQuery);
    procedure Cria_ntrttSPEDEFD_0200(Qry: TmySQLQuery);
  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  SPED_Create: TSPED_Create;

implementation

uses UnMyObjects, Module;


{ TCreateSPED }

procedure TSPED_Create.Cria_ntrttSPEDEFD_0190(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  NoUnidMed            varchar(6)                                 ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                       ');
  //Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
  //Qry.SQL.Add('  PRIMARY KEY (GraGruX)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TSPED_Create.Cria_ntrttSPEDEFD_0200(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  GraGruX              int(11)      NOT NULL                      ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                       ');
  //Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
  //Qry.SQL.Add('  PRIMARY KEY (GraGruX)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

function TSPED_Create.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
  Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes: Integer;
  NomeTab: String): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      //
      ntrttSPEDEFD_0190:     Nome := Lowercase('_SPEDEFD_0190_');
      ntrttSPEDEFD_0200:     Nome := Lowercase('_SPEDEFD_0200_');
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MensagemBox(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)',
    'Erro', MB_OK+MB_ICONERROR);
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrttSPEDEFD_0190:       Cria_ntrttSPEDEFD_0190(Qry);
    ntrttSPEDEFD_0200:       Cria_ntrttSPEDEFD_0200(Qry);
    //
    //
    else Geral.MensagemBox('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!', 'Erro', MB_OK+MB_ICONERROR);
  end;
  Result := TabNo;
end;

end.
