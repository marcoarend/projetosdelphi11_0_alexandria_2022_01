unit EFD_C100;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, UnDmkEnums;

type
  TFmEFD_C100 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    EdLinArq: TdmkEdit;
    Panel6: TPanel;
    RGIND_OPER: TdmkRadioGroup;
    RGIND_EMIT: TdmkRadioGroup;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrTerceiros: TmySQLQuery;
    DsTerceiros: TDataSource;
    QrTerceirosCodigo: TIntegerField;
    QrTerceirosNO_ENT: TWideStringField;
    Panel7: TPanel;
    QrTbSPEDEFD017: TmySQLQuery;
    QrTbSPEDEFD017CodTxt: TWideStringField;
    QrTbSPEDEFD017Nome: TWideStringField;
    DsTbSPEDEFD017: TDataSource;
    GroupBox3: TGroupBox;
    EdCOD_MOD: TdmkEditCB;
    CBCOD_MOD: TdmkDBLookupComboBox;
    QrTbSPEDEFD018: TmySQLQuery;
    DsTbSPEDEFD018: TDataSource;
    QrTbSPEDEFD018CodTxt: TWideStringField;
    QrTbSPEDEFD018Nome: TWideStringField;
    GroupBox4: TGroupBox;
    EdCOD_SIT: TdmkEditCB;
    CBCOD_SIT: TdmkDBLookupComboBox;
    Panel8: TPanel;
    Label2: TLabel;
    EdSER: TdmkEdit;
    EdSUB_: TdmkEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdCHV_NFE: TdmkEdit;
    Label9: TLabel;
    EdNUM_DOC: TdmkEdit;
    Label10: TLabel;
    TPDT_DOC: TdmkEditDateTimePicker;
    Label11: TLabel;
    TPDT_E_S: TdmkEditDateTimePicker;
    GroupBox6: TGroupBox;
    Label24: TLabel;
    EdCST_ICMS: TdmkEdit;
    EdICMS_CST_TXT: TdmkEdit;
    QrCST_ICMS: TmySQLQuery;
    QrCST_ICMSCST: TWideStringField;
    QrCST_ICMSNO_CST_A: TWideStringField;
    QrCST_ICMSNO_CST_B: TWideStringField;
    QrCST_ICMSCST_TXT: TWideStringField;
    EdALIQ_ICMS: TdmkEdit;
    EdVL_RED_BC: TdmkEdit;
    QrTbSPEDEFD002: TmySQLQuery;
    Label25: TLabel;
    Label26: TLabel;
    EdCFOP: TdmkEdit;
    EdCFOP_TXT: TdmkEdit;
    Label27: TLabel;
    QrTbSPEDEFD002CodTxt: TWideStringField;
    QrTbSPEDEFD002Nome: TWideStringField;
    QrTbSPEDEFD028: TmySQLQuery;
    QrTbSPEDEFD028CodTxt: TWideStringField;
    QrTbSPEDEFD028Nome: TWideStringField;
    EdIND_PGTO: TdmkEdit;
    EdIND_PGTO_TXT: TdmkEdit;
    Label28: TLabel;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label29: TLabel;
    EdVL_DOC: TdmkEdit;
    EdVL_DESC: TdmkEdit;
    EdVL_MERC: TdmkEdit;
    EdVL_ABAT_NT: TdmkEdit;
    EdVL_FRT: TdmkEdit;
    EdVL_OUT_DA: TdmkEdit;
    EdVL_BC_ICMS: TdmkEdit;
    EdVL_SEG: TdmkEdit;
    Panel11: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    EdVL_ICMS: TdmkEdit;
    EdVL_BC_ICMS_ST: TdmkEdit;
    EdVL_ICMS_ST: TdmkEdit;
    EdVL_PIS: TdmkEdit;
    EdVL_COFINS: TdmkEdit;
    EdVL_PIS_ST: TdmkEdit;
    EdVL_COFINS_ST: TdmkEdit;
    EdVL_IPI: TdmkEdit;
    Panel10: TPanel;
    Label33: TLabel;
    EdIND_FRT: TdmkEdit;
    EdIND_FRT_TXT: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrCST_ICMSCalcFields(DataSet: TDataSet);
    procedure EdCST_ICMSChange(Sender: TObject);
    procedure EdCFOPChange(Sender: TObject);
    procedure EdIND_PGTOChange(Sender: TObject);
    procedure EdIND_FRTChange(Sender: TObject);
  private
    { Private declarations }
    function COD_MOD_Permitido(): Boolean;
    function COD_SIT_Permitido(): Boolean;
  public
    { Public declarations }
  end;

  var
  FmEFD_C100: TFmEFD_C100;

implementation

uses UnMyObjects, Module, EFD_C001, UMySQLModule;

{$R *.DFM}

procedure TFmEFD_C100.BtOKClick(Sender: TObject);
const
  REG = 'C100';
  Importado = 0;
  //
  COD_INF = '';
var
  ImporExpor, AnoMes, Empresa, LinArq: Integer;
  //
  IND_OPER, IND_EMIT, COD_PART, COD_MOD, COD_SIT, SER, NUM_DOC,
  DT_DOC, DT_E_S, CHV_NFE, IND_PGTO, IND_FRT: String;
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST: Double;
  Terceiro: Integer;
  CST_ICMS, CFOP: String;
  ALIQ_ICMS, VL_RED_BC: Double;
begin
  Terceiro := EdTerceiro.ValueVariant;
  if MyObjects.FIC(Terceiro = 0, EdTerceiro,
    'Informe o Fornecedor!') then
    Exit;
  //
  if MyObjects.FIC(RGIND_OPER.ItemIndex < 0, RGIND_OPER,
    'Informe o indicador do tipo de opera��o!') then
    Exit;
  //
  if MyObjects.FIC(RGIND_EMIT.ItemIndex < 0, RGIND_EMIT,
    'Informe o indicador do emitente do documento fiscal!') then
    Exit;
  //
  if not COD_MOD_Permitido() then
    Exit;
  //
  if not COD_SIT_Permitido() then
    Exit;
  //
  if MyObjects.FIC(EdNUM_DOC.ValueVariant < 1, EdNUM_DOC,
    'Informe o n�mero do documento!') then
    Exit;
  //
  if MyObjects.FIC(Int(TPDT_DOC.Date) > Geral.AAAAMM_To_Date(
    EdAnoMes.ValueVariant, qdmLast), TPDT_DOC, 'Data de emiss�o inv�lida!') then
    Exit;
  //
  if MyObjects.FIC(EdVL_DOC.ValueVariant < 0.01, EdVL_DOC,
    'Informe o valor total do documento fiscal!') then
    Exit;
  //
  IND_PGTO := EdIND_PGTO.Text;
  IND_FRT := EdIND_FRT.Text;
{
  if MyObjects.FIC(EdVL_MERC.ValueVariant < 0.01, EdVL_MERC,
    'Informe o valor total fornecido/consumido!') then
    Exit;
}
  //
  IND_OPER         := FormatFloat('0', RGIND_OPER.ItemIndex);
  IND_EMIT         := FormatFloat('0', RGIND_EMIT.ItemIndex);
  COD_PART         := FormatFloat('0', Terceiro);
  COD_MOD          := EdCOD_MOD.Text;
  COD_SIT          := EdCOD_SIT.Text;
  SER              := EdSER.Text;
  NUM_DOC          := EdNUM_DOC.ValueVariant;
  CHV_NFE          := EdCHV_NFE.Text;
  DT_DOC           := Geral.FDT(TPDT_DOC.Date, 1);
  DT_E_S           := Geral.FDT(TPDT_E_S.Date, 1);
  VL_DOC           := EdVL_DOC.ValueVariant;
  VL_DESC          := EdVL_DESC.ValueVariant;
  VL_ABAT_NT       := EdVL_ABAT_NT.ValueVariant;
  VL_MERC          := EdVL_MERC.ValueVariant;
  VL_FRT           := EdVL_FRT.ValueVariant;
  VL_SEG           := EdVL_SEG.ValueVariant;
  VL_OUT_DA        := EdVL_OUT_DA.ValueVariant;
  VL_BC_ICMS       := EdVL_BC_ICMS.ValueVariant;
  VL_ICMS          := EdVL_ICMS.ValueVariant;
  VL_BC_ICMS_ST    := EdVL_BC_ICMS_ST.ValueVariant;
  VL_ICMS_ST       := EdVL_ICMS_ST.ValueVariant;
  VL_IPI           := EdVL_IPI.ValueVariant;
  VL_PIS           := EdVL_PIS.ValueVariant;
  VL_COFINS        := EdVL_COFINS.ValueVariant;
  VL_PIS_ST        := EdVL_PIS_ST.ValueVariant;
  VL_COFINS_ST     := EdVL_COFINS_ST.ValueVariant;
  CST_ICMS         := EdCST_ICMS.Text;
  CFOP             := EdCFOP.Text;
  ALIQ_ICMS        := EdALIQ_ICMS.ValueVariant;
  VL_RED_BC        := EdVL_RED_BC.ValueVariant;
  //
  ImporExpor := EdImporExpor.ValueVariant;
  AnoMes := EdAnoMes.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  //
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_c100', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
    ImgTipo.SQLType, EdLinArq.ValueVariant, siPositivo, EdLinArq);

  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'efd_c100', False, [
  'REG', 'IND_OPER', 'IND_EMIT',
  'COD_PART', 'COD_MOD', 'COD_SIT',
  'SER', 'NUM_DOC', 'CHV_NFE',
  'DT_DOC', 'DT_E_S', 'VL_DOC',
  'IND_PGTO', 'VL_DESC', 'VL_ABAT_NT',
  'VL_MERC', 'IND_FRT', 'VL_FRT',
  'VL_SEG', 'VL_OUT_DA', 'VL_BC_ICMS',
  'VL_ICMS', 'VL_BC_ICMS_ST', 'VL_ICMS_ST',
  'VL_IPI', 'VL_PIS', 'VL_COFINS',
  'VL_PIS_ST', 'VL_COFINS_ST', (*'ParTipo',
  'ParCodi', 'FatID', 'FatNum',
  'ConfVal',*) 'Terceiro', 'Importado',
  'CST_ICMS', 'CFOP', 'ALIQ_ICMS',
  'VL_RED_BC'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  REG, IND_OPER, IND_EMIT,
  COD_PART, COD_MOD, COD_SIT,
  SER, NUM_DOC, CHV_NFE,
  DT_DOC, DT_E_S, VL_DOC,
  IND_PGTO, VL_DESC, VL_ABAT_NT,
  VL_MERC, IND_FRT, VL_FRT,
  VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST,
  VL_IPI, VL_PIS, VL_COFINS,
  VL_PIS_ST, VL_COFINS_ST, (*ParTipo,
  ParCodi, FatID, FatNum,
  ConfVal,*) Terceiro, Importado,
  CST_ICMS, CFOP, ALIQ_ICMS,
  VL_RED_BC], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
    FmEFD_C001.ReopenEFD_C100(LinArq);
    Close;
  end;
end;

procedure TFmEFD_C100.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmEFD_C100.COD_MOD_Permitido(): Boolean;
var
  Modelo: Integer;
begin
  if EdCOD_MOD.Text = '1B' then
    Result := True
  else
  begin
    Modelo := Geral.IMV(EdCOD_MOD.Text);
    Result := Modelo in ([01,04,55]);
  end;
  if not Result then
    Geral.MensagemBox('C�digo do Modelo do Documento Fiscal inv�lido!',
    'Aviso', MB_OK+MB_ICONWARNING);
end;

function TFmEFD_C100.COD_SIT_Permitido(): Boolean;
var
  Sit: Integer;
begin
  Sit := Geral.IMV(EdCOD_SIT.Text);
  case RGIND_EMIT.ItemIndex of
    0: Result := (EdCOD_SIT.Text <> '') and (Sit in ([00,01,02,03,04,05,06,07,08]));
    1: Result := (EdCOD_SIT.Text <> '') and (Sit in ([00,01,02,03,06,07,08]));
  end;
  if not Result then
    Geral.MensagemBox('C�digo da Situa��o do Documento Fiscal inv�lido!',
    'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmEFD_C100.EdCFOPChange(Sender: TObject);
begin
  if QrTbSPEDEFD002.Locate('CodTxt', EdCFOP.Text, []) then
    EdCFOP_TXT.Text := QrTbSPEDEFD002Nome.Value
  else
    EdCFOP_TXT.Text := '';
end;

procedure TFmEFD_C100.EdCST_ICMSChange(Sender: TObject);
begin
  if QrCST_ICMS.Locate('CST', EdCST_ICMS.Text, []) then
    EdICMS_CST_TXT.Text := QrCST_ICMSCST_TXT.Value
  else
    EdICMS_CST_TXT.Text := '';
end;

procedure TFmEFD_C100.EdIND_FRTChange(Sender: TObject);
var
  Txt: String;
begin
  case EdIND_FRT.ValueVariant of
    0: Txt := 'Por conta de terceiros';
    1: Txt := 'Por conta do emitente';
    2: Txt := 'Por conta do destinat�rio';
    9: Txt := 'Sem cobran�a de frete';
    else Txt := '? ? ?'
  end;
  EdIND_FRT_TXT.Text := Txt;
end;

procedure TFmEFD_C100.EdIND_PGTOChange(Sender: TObject);
var
  Txt: String;
begin
  case EdIND_PGTO.ValueVariant of
    0: Txt := '� vista';
    1: Txt := 'A prazo';
    9: Txt := 'Sem pagamento';
    else Txt := '? ? ?'
  end;
  EdIND_PGTO_TXT.Text := Txt;
end;

procedure TFmEFD_C100.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdTerceiro.SetFocus;
end;

procedure TFmEFD_C100.FormCreate(Sender: TObject);
begin
  QrTerceiros.Open;
  // Se precisar IND_EMIT est� na tabela 16
  //QrTbSPEDEFD016.Open;
  // COD_MOD
  QrTbSPEDEFD017.Open;
  // COD_SIT
  QrTbSPEDEFD018.Open;
  // TbSPEDEFD024 x TbSPEDEFD025
  QrCST_ICMS.Open;
  // CFOP
  QrTbSPEDEFD002.Open;
  // Classe de consumo de �gua
  QrTbSPEDEFD028.Open;
  //
end;

procedure TFmEFD_C100.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEFD_C100.QrCST_ICMSCalcFields(DataSet: TDataSet);
begin
  QrCST_ICMSCST_TXT.Value :=
    QrCST_ICMSNO_CST_A.Value + ' ' + QrCST_ICMSNO_CST_B.Value;
end;

end.
