unit EFD_H010;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, dmkRadioGroup;

type
  TFmEFD_H010 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdH005: TdmkEdit;
    Label6: TLabel;
    TPDT_INV: TdmkEditDateTimePicker;
    Label2: TLabel;
    EdLinArq: TdmkEdit;
    Label9: TLabel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXGerBxaEstq: TSmallintField;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXEx_TIPI: TWideStringField;
    DsGraGruX: TDataSource;
    Panel5: TPanel;
    EdGraGruX: TdmkEditCB;
    Label232: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    StaticText1: TStaticText;
    EdUNID: TdmkEdit;
    StaticText2: TStaticText;
    EdQTD: TdmkEdit;
    StaticText3: TStaticText;
    EdVL_UNIT: TdmkEdit;
    StaticText4: TStaticText;
    EdVL_ITEM: TdmkEdit;
    RGIND_PROP: TdmkRadioGroup;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    DsEntidades: TDataSource;
    LaCOD_PART: TLabel;
    EdCOD_PART: TdmkEditCB;
    CBCOD_PART: TdmkDBLookupComboBox;
    StaticText5: TStaticText;
    EdTXT_COMPL: TdmkEdit;
    GBImportar1: TGroupBox;
    RGNivPlaCta: TRadioGroup;
    Panel6: TPanel;
    Label1: TLabel;
    EdGenPlaCta: TdmkEditCB;
    CBGenPlaCta: TdmkDBLookupComboBox;
    EdCOD_CTA: TdmkEdit;
    StaticText7: TStaticText;
    EdVL_ITEM_IR: TdmkEdit;
    QrNivelSel: TmySQLQuery;
    QrNivelSelCodigo: TIntegerField;
    QrNivelSelNome: TWideStringField;
    DsNivelSel: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdVL_ITEMRedefinido(Sender: TObject);
    procedure EdVL_UNITRedefinido(Sender: TObject);
    procedure EdQTDRedefinido(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGIND_PROPClick(Sender: TObject);
    procedure RGNivPlaCtaClick(Sender: TObject);
    procedure EdGenPlaCtaRedefinido(Sender: TObject);
    procedure EdCOD_CTARedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGraGruX();
  public
    { Public declarations }
    FBalID, FBalNum, FBalItm, FBalEnt: Integer;
  end;

  var
  FmEFD_H010: TFmEFD_H010;

implementation

uses UnMyObjects, Module, EFD_E001, UMySQLModule, DmkDAC_PF, MyListas,
  ModuleFin, UnFinanceiro;

{$R *.DFM}

procedure TFmEFD_H010.BtOKClick(Sender: TObject);
const
  REG = 'H010';
var
  COD_ITEM, UNID, IND_PROP, COD_PART, TXT_COMPL, COD_CTA: String;
  ImporExpor, AnoMes, Empresa, H005, LinArq: Integer;
  QTD, VL_UNIT, VL_ITEM, VL_ITEM_IR: Double;
  SQLType: TSQLType;
  //
  GraGruX, BalID, BalNum, BalItm, BalEnt: Integer;
begin
  GraGruX        := EdGraGruX.ValueVariant;
  //
  SQLType        := ImgTipo.SQLType;
  ImporExpor     := EdImporExpor.ValueVariant;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  H005           := EdH005.ValueVariant;
  LinArq         := EdLinArq.ValueVariant;
  //REG            := ;
  COD_ITEM       := Geral.FF0(GraGruX);
  UNID           := EdUNID.Text;
  QTD            := EdQTD.ValueVariant;
  VL_UNIT        := EdVL_UNIT.ValueVariant;
  VL_ITEM        := EdVL_ITEM.ValueVariant;
  IND_PROP       := Geral.FF0(RGIND_PROP.ItemIndex);
  COD_PART       := EdCOD_PART.ValueVariant;
  TXT_COMPL      := Trim(EdTXT_COMPL.Text);
  COD_CTA        := EdCOD_CTA.Text;
  VL_ITEM_IR     := EdVL_ITEM_IR.ValueVariant;
  //
  if SQLType = stIns then
  begin
    BalID          := Integer(TSPED_EFD_Bal.sebalEFD_H010);
    BalNum         := H005;
    BalItm         := LinArq;
    BalEnt         := EdCOD_PART.ValueVariant;
  end else
  begin
    BalID          := FBalID;
    BalNum         := FBalNum;
    BalItm         := FBalItm;
    BalEnt         := FBalEnt;
  end;
  //
  if COD_PART = '0' then
    COD_PART := '';
  //
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o c�digo do produto!') then
    Exit;
  if MyObjects.FIC(Trim(UNID) = '', EdGraGruX,
    'C�digo do produto sem sigla de unidade definida!') then Exit;
  if MyObjects.FIC((RGIND_PROP.ItemIndex < 0) or (RGIND_PROP.ItemIndex > 2), RGIND_PROP,
    'Informe o indicador de propriedade/posse!') then Exit;
  if MyObjects.FIC((EdCOD_PART.ValueVariant <> 0) and (RGIND_PROP.ItemIndex = 0), EdCOD_PART,
    'N�o informe o propriet�rio/possuidor quando for o informante!') then Exit;
  if MyObjects.FIC((EdCOD_PART.ValueVariant = 0) and (RGIND_PROP.ItemIndex <> 0), EdCOD_PART,
    'Informe o propriet�rio/possuidor quando n�o for o informante!') then Exit;
  //
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_h010', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
  ImgTipo.SQLType, LinArq, siPositivo, EdLinArq);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'efd_h010', False, [
  'H005', 'REG', 'COD_ITEM',
  'UNID', 'QTD', 'VL_UNIT',
  'VL_ITEM', 'IND_PROP', 'COD_PART',
  'TXT_COMPL', 'COD_CTA', 'VL_ITEM_IR',
  'BalID', 'BalNum', 'BalItm', 'BalEnt'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  H005, REG, COD_ITEM,
  UNID, QTD, VL_UNIT,
  VL_ITEM, IND_PROP, COD_PART,
  TXT_COMPL, COD_CTA, VL_ITEM_IR,
  BalID, BalNum, BalItm, BalEnt], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
    FmEFD_E001.AtualizaValoresH005deH010();
    FmEFD_E001.ReopenEFD_H010(LinArq);
    Close;
  end;
end;

procedure TFmEFD_H010.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEFD_H010.EdCOD_CTARedefinido(Sender: TObject);
var
  Nivel, Genero: Integer;
begin
  if UFinanceiro.DesmontaSPED_COD_CTA(EdCOD_CTA.Text, Nivel, Genero) then
  begin
    if (Nivel <> RGNivPlaCta.ItemIndex) or (Genero <> EdGenPlaCta.ValueVariant) then
    begin
      RGNivPlaCta.ItemIndex    := Nivel;
      EdGenPlaCta.ValueVariant := Genero;
      CBGenPlaCta.KeyValue     := Genero;
    end;
  end;
end;

procedure TFmEFD_H010.EdGenPlaCtaRedefinido(Sender: TObject);
begin
  EdCOD_CTA.Text := UFinanceiro.MontaSPED_COD_CTA(RGNivPlaCta.ItemIndex, EdGenPlaCta.ValueVariant);
end;

procedure TFmEFD_H010.EdGraGruXChange(Sender: TObject);
begin
  if EdGraGruX.ValueVariant = 0 then
    EdUNID.Text := ''
  else
    EdUNID.Text := QrGraGruXSIGLAUNIDMED.Value;
end;

procedure TFmEFD_H010.EdQTDRedefinido(Sender: TObject);
begin
  EdVL_ITEM.ValueVariant := EdQTD.ValueVariant * EdVL_UNIT.ValueVariant;
end;

procedure TFmEFD_H010.EdVL_ITEMRedefinido(Sender: TObject);
begin
  if EdQTD.ValueVariant > 0 then
    EdVL_UNIT.ValueVariant := EdVL_ITEM.ValueVariant / EdQTD.ValueVariant
  else
    EdVL_UNIT.ValueVariant := 0;
end;

procedure TFmEFD_H010.EdVL_UNITRedefinido(Sender: TObject);
begin
  EdVL_ITEM.ValueVariant := EdQTD.ValueVariant * EdVL_UNIT.ValueVariant;
end;

procedure TFmEFD_H010.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEFD_H010.FormCreate(Sender: TObject);
begin
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  FBalID  := 0;
  FBalNum := 0;
  FBalItm := 0;
  FBalEnt := 0;
  ReopenGraGruX();
end;

procedure TFmEFD_H010.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEFD_H010.ReopenGraGruX();
var
  SQL_AND, SQL_LFT: String;
begin
  SQL_AND := '';
  SQL_LFT := '';
  if TdmkAppID(CO_DMKID_APP) = dmkappB_L_U_E_D_E_R_M then
  begin
    SQL_AND := 'AND NOT (pqc.PQ IS NULL)';
    SQL_LFT := 'LEFT JOIN pqcli pqc ON pqc.PQ=gg1.Nivel1';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, ',
  'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  SQL_LFT,
  'WHERE ggx.Controle > -900000 ',
  SQL_AND,
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
end;

procedure TFmEFD_H010.RGIND_PROPClick(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (RGIND_PROP.ItemIndex > 0) and (RGIND_PROP.ItemIndex < 3);
  LaCOD_PART.Enabled := Habilita;
  EdCOD_PART.Enabled := Habilita;
  CBCOD_PART.Enabled := Habilita;
  if not Habilita then
  begin
    EdCOD_PART.ValueVariant := 0;
    CBCOD_PART.KeyValue     := 0;
  end;
end;

procedure TFmEFD_H010.RGNivPlaCtaClick(Sender: TObject);
begin
  DModFin.ReopenNivelSel(QrNivelSel, EdGenPlaCta, CBGenPlaCta, RGNivPlaCta);
end;

end.
