unit EFD_D100;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, Variants, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, UnDmkEnums, SPED_Listas;

type
  TFmEFD_D100 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdImporExpor: TdmkEdit;
    EdAnoMes: TdmkEdit;
    EdEmpresa: TdmkEdit;
    EdLinArq: TdmkEdit;
    Panel6: TPanel;
    RGIND_OPER: TdmkRadioGroup;
    RGIND_EMIT: TdmkRadioGroup;
    QrTerceiros: TmySQLQuery;
    DsTerceiros: TDataSource;
    QrTerceirosCodigo: TIntegerField;
    QrTerceirosNO_ENT: TWideStringField;
    Panel7: TPanel;
    QrTbSPEDEFD017: TmySQLQuery;
    QrTbSPEDEFD017CodTxt: TWideStringField;
    QrTbSPEDEFD017Nome: TWideStringField;
    DsTbSPEDEFD017: TDataSource;
    GroupBox3: TGroupBox;
    EdCOD_MOD: TdmkEditCB;
    CBCOD_MOD: TdmkDBLookupComboBox;
    QrTbSPEDEFD018: TmySQLQuery;
    DsTbSPEDEFD018: TDataSource;
    QrTbSPEDEFD018CodTxt: TWideStringField;
    QrTbSPEDEFD018Nome: TWideStringField;
    GroupBox4: TGroupBox;
    EdCOD_SIT: TdmkEditCB;
    CBCOD_SIT: TdmkDBLookupComboBox;
    Panel8: TPanel;
    Label2: TLabel;
    EdSER: TdmkEdit;
    EdSUB: TdmkEdit;
    Label7: TLabel;
    Label9: TLabel;
    EdNUM_DOC: TdmkEdit;
    Label10: TLabel;
    TPDT_DOC: TdmkEditDateTimePicker;
    Label11: TLabel;
    TPDT_A_P: TdmkEditDateTimePicker;
    Panel10: TPanel;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    EdVL_DOC: TdmkEdit;
    EdVL_DESC: TdmkEdit;
    EdVL_SERV: TdmkEdit;
    GroupBox6: TGroupBox;
    Label24: TLabel;
    QrCST_ICMS: TmySQLQuery;
    QrTbSPEDEFD002: TmySQLQuery;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    QrTbSPEDEFD002CodTxt: TWideStringField;
    QrTbSPEDEFD002Nome: TWideStringField;
    QrCST_ICMSCodTxt: TWideStringField;
    QrCST_ICMSNome: TWideStringField;
    DsCST_ICMS: TDataSource;
    DsTbSPEDEFD002: TDataSource;
    EdCST_ICMS: TdmkEditCB;
    CBCST_ICMS: TdmkDBLookupComboBox;
    EdALIQ_ICMS: TdmkEdit;
    EdVL_RED_BC: TdmkEdit;
    EdCFOP: TdmkEditCB;
    CBCFOP: TdmkDBLookupComboBox;
    EdCHV_CTE_REF: TdmkEdit;
    Label20: TLabel;
    QrLFretePor: TmySQLQuery;
    DsLFretePor: TDataSource;
    GroupBox7: TGroupBox;
    EdIND_FRT: TdmkEditCB;
    CBIND_FRT: TdmkDBLookupComboBox;
    Label18: TLabel;
    Label19: TLabel;
    Label15: TLabel;
    EdVL_NT: TdmkEdit;
    EdVL_ICMS: TdmkEdit;
    EdVL_BC_ICMS: TdmkEdit;
    RGTP_CTE: TdmkRadioGroup;
    QrLFretePorCodigo: TIntegerField;
    QrLFretePorNome: TWideStringField;
    EdCHV_CTE: TdmkEdit;
    Label1: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    Label8: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGTP_CTEClick(Sender: TObject);
    procedure EdCHV_CTEExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FPreparando: Boolean;
    //
    function COD_MOD_Permitido(): Boolean;
    function COD_SIT_Permitido(): Boolean;
  public
    { Public declarations }
    FAnoMes: Integer;
    procedure ReopenTabelas();
  end;

  var
  FmEFD_D100: TFmEFD_D100;

implementation

uses UnMyObjects, Module, EFD_D001, UMySQLModule, ModuleGeral, UnDmkProcFunc,
  DmkDAC_PF, UnXXe_PF;

{$R *.DFM}

procedure TFmEFD_D100.BtOKClick(Sender: TObject);
const
  REG = 'D100';
  Importado = 0;
  //
  COD_INF = '';
var
  ImporExpor, AnoMes, Empresa, LinArq: Integer;
  //
  IND_OPER, IND_EMIT, COD_PART, COD_MOD, COD_SIT, SER, SUB, CHV_CTE, DT_DOC,
  DT_A_P, CHV_CTE_REF, IND_FRT, (*COD_INF,*) COD_CTA: String;
  TP_CTE, NUM_DOC, Terceiro, (*Importado,*) CST_ICMS, CFOP: Integer;
  VL_DOC, VL_DESC, VL_SERV, VL_BC_ICMS, VL_ICMS, VL_NT, ALIQ_ICMS, VL_RED_BC: Double;
  SQLType: TSQLType;
begin
  Terceiro := EdTerceiro.ValueVariant;
  if MyObjects.FIC(Terceiro = 0, EdTerceiro,
    'Informe o Fornecedor!') then
    Exit;
  //
  if MyObjects.FIC(RGIND_OPER.ItemIndex < 0, RGIND_OPER,
    'Informe o indicador do tipo de opera��o!') then
    Exit;
  //
  if MyObjects.FIC(RGIND_EMIT.ItemIndex < 0, RGIND_EMIT,
    'Informe o indicador do emitente do documento fiscal!') then
    Exit;
  //
  if not COD_MOD_Permitido() then
    Exit;
  //
  if not COD_SIT_Permitido() then
    Exit;
  //
  if MyObjects.FIC(EdNUM_DOC.ValueVariant < 1, EdNUM_DOC,
    'Informe o n�mero do documento!') then
    Exit;
  //
  if MyObjects.FIC(Int(TPDT_DOC.Date) > Geral.AAAAMM_To_Date(
    EdAnoMes.ValueVariant, qdmLast), TPDT_DOC, 'Data de emiss�o inv�lida!') then
    Exit;
  //
  if MyObjects.FIC(Int(TPDT_A_P.Date) > Geral.AAAAMM_To_Date(
    EdAnoMes.ValueVariant, qdmLast), TPDT_A_P, 'Data de entrada/sa�da inv�lida!') then
    Exit;
  //
{
  if MyObjects.FIC(EdVL_DOC.ValueVariant < 0.01, EdVL_DOC,
    'Informe o valor total do documento fiscal!') then
    Exit;
  //
  if MyObjects.FIC(EdVL_SERV.ValueVariant < 0.01, EdVL_SERV,
    'Informe o valor total fornecido/consumido!') then
    Exit;
}
  //
  IND_OPER         := FormatFloat('0', RGIND_OPER.ItemIndex);
  IND_EMIT         := FormatFloat('0', RGIND_EMIT.ItemIndex);
  COD_PART         := FormatFloat('0', Terceiro);
  COD_MOD          := EdCOD_MOD.Text;
  COD_SIT          := EdCOD_SIT.Text;
  SER              := EdSER.Text;
  SUB              := EdSUB.Text;
  NUM_DOC          := EdNUM_DOC.ValueVariant;
  CHV_CTE          := EdCHV_CTE.Text;
  DT_DOC           := Geral.FDT(TPDT_DOC.Date, 1);
  DT_A_P           := Geral.FDT(TPDT_A_P.Date, 1);
  TP_CTE           := RGTP_CTE.ItemIndex;
  CHV_CTE_REF      := EdCHV_CTE_REF.Text;
  VL_DOC           := EdVL_DOC.ValueVariant;
  VL_DESC          := EdVL_DESC.ValueVariant;
  IND_FRT          := EdIND_FRT.ValueVariant;
  VL_SERV          := EdVL_SERV.ValueVariant;
  VL_BC_ICMS       := EdVL_BC_ICMS.ValueVariant;
  VL_ICMS          := EdVL_ICMS.ValueVariant;
  VL_NT            := EdVL_NT.ValueVariant;
  //COD_INF          := ''; // Ver o que fazer!!
(*
  VL_PIS           := EdVL_PIS.ValueVariant;
  VL_COFINS        := EdVL_COFINS.ValueVariant;
*)
  CST_ICMS         := EdCST_ICMS.ValueVariant;
  CFOP             := EdCFOP.ValueVariant;
  ALIQ_ICMS        := EdALIQ_ICMS.ValueVariant;
  VL_RED_BC        := EdVL_RED_BC.ValueVariant;
  //COD_INF: String;
  //
  ImporExpor := EdImporExpor.ValueVariant;
  AnoMes := EdAnoMes.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  //
  LinArq := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'efd_d100', 'LinArq', [
  (*'ImporExpor',*) 'AnoMes'(*, 'Empresa'*)], [(*ImporExpor,*) AnoMes(*, Empresa*)],
    ImgTipo.SQLType, EdLinArq.ValueVariant, siPositivo, EdLinArq);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'efd_d100', False, [
  'REG', 'IND_OPER', 'IND_EMIT',
  'COD_PART', 'COD_MOD', 'COD_SIT',
  'SER', 'SUB', 'NUM_DOC',
  'CHV_CTE', 'DT_DOC', 'DT_A_P',
  'TP_CTE', 'CHV_CTE_REF', 'VL_DOC',
  'VL_DESC', 'IND_FRT', 'VL_SERV',
  'VL_BC_ICMS', 'VL_ICMS', 'VL_NT',
  'COD_INF', 'COD_CTA', 'Terceiro',
  'Importado', 'CST_ICMS', 'CFOP',
  'ALIQ_ICMS', 'VL_RED_BC'], [
  'ImporExpor', 'AnoMes', 'Empresa', 'LinArq'], [
  REG, IND_OPER, IND_EMIT,
  COD_PART, COD_MOD, COD_SIT,
  SER, SUB, NUM_DOC,
  CHV_CTE, DT_DOC, DT_A_P,
  TP_CTE, CHV_CTE_REF, VL_DOC,
  VL_DESC, IND_FRT, VL_SERV,
  VL_BC_ICMS, VL_ICMS, VL_NT,
  COD_INF, COD_CTA, Terceiro,
  Importado, CST_ICMS, CFOP,
  ALIQ_ICMS, VL_RED_BC], [
  ImporExpor, AnoMes, Empresa, LinArq], True) then
  begin
    FmEFD_D001.ReopenEFD_D100(LinArq);
    Close;
  end;
(*
/*FmEFD_D001.QrEFD_D100*/
/*********** SQL ***********/
SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)
NO_TERC, d100.*
FROM efd_d100 d100
LEFT JOIN entidades ent ON ent.Codigo=d100.Terceiro
WHERE d100.ImporExpor=3
AND d100.Empresa=-11
AND d100.AnoMes=201401
/********* FIM SQL *********/

/*****Query sem parametros*******/
*)
end;

procedure TFmEFD_D100.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmEFD_D100.COD_MOD_Permitido(): Boolean;
const
  ModelosValidos: array[0..8] of String = ('07', '08', '8B', '09', '10', '11', '26', '27', '57');
var
  Modelo: String;
  I: Integer;
begin
  Modelo := EdCOD_MOD.Text;
  Result := False;
  for I := Low(ModelosValidos) to High(ModelosValidos) do
  begin
    if ModelosValidos[I] = Modelo then
    begin
      Result := True;
      Exit;
    end;
  end;
  if not Result then
    Geral.MB_Aviso('C�digo do Modelo do Documento Fiscal inv�lido!');
end;

function TFmEFD_D100.COD_SIT_Permitido(): Boolean;
var
  Sit: Integer;
begin
  Sit := Geral.IMV(EdCOD_SIT.Text);
  Result := (EdCOD_SIT.Text <> '') and (Sit in ([00,01,02,03,08]));
  if not Result then
    Geral.MensagemBox('C�digo da Situa��o do Documento Fiscal inv�lido!',
    'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmEFD_D100.EdCHV_CTEExit(Sender: TObject);
begin
  XXe_PF.XXeDesmontaEPreencheObjectsChaveCTe(FPreparando, EdCHV_CTE, EdCOD_MOD,
  CBCOD_MOD, EdSER, EdNUM_DOC, EdTerceiro, CBTerceiro, TPDT_DOC);
end;

procedure TFmEFD_D100.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdTerceiro.SetFocus;
end;

procedure TFmEFD_D100.FormCreate(Sender: TObject);
begin
  FPreparando := True;
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrLFretePor, Dmod.MyDB);
end;

procedure TFmEFD_D100.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEFD_D100.FormShow(Sender: TObject);
begin
  FPreparando := False;
end;

procedure TFmEFD_D100.ReopenTabelas();
var
  Ini, Fim, SQL_Periodo_Valido: String;
begin
  Ini := Geral.FDT(DmkPF.DatadeAnoMes(FAnoMes, 1, 0), 1);
  Fim := Geral.FDT(DmkPF.DatadeAnoMes(FAnoMes + 1, 1, -1), 1);
  SQL_Periodo_Valido :=
  'WHERE DataIni <= "' + Ini + '" ' + sLineBreak +
  'AND (DataFim >="' + Fim + '" OR DataFim<2)  ';

  // Se precisar IND_EMIT est� na tabela 16
  //QrTbSPEDEFD016.Open;
  //
  QrTerceiros.Open;
  // Se precisar IND_EMIT est� na tabela 16
  //QrTbSPEDEFD016.Open;
  // COD_MOD > QrTbSPEDEFD017
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD017, DModG.AllID_DB, [
  'SELECT *  ',
  'FROM tbspedefd017 ',
  SQL_Periodo_Valido,
  'AND CodTxt IN ("07", "08", "8B", "09", "10", "11", "26", "27", "57") ',
  'ORDER BY Nome ',
  '']);
  // COD_SIT >  QrTbSPEDEFD018
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD018, DModG.AllID_DB, [
  'SELECT *  ',
  'FROM tbspedefd018 ',
  SQL_Periodo_Valido,
  'ORDER BY Nome ',
  '']);
  // TbSPEDEFD024 x TbSPEDEFD025
  // Mudou para 130
  UnDmkDAC_PF.AbreMySQLQuery0(QrCST_ICMS, DModG.AllID_DB, [
 'SELECT *  ',
 'FROM tbspedefd130 ',
 SQL_Periodo_Valido,
 'ORDER BY Nome ',
 '']);
  // CFOP > QrTbSPEDEFD002
  UnDmkDAC_PF.AbreMySQLQuery0(QrTbSPEDEFD002, DModG.AllID_DB, [
  'SELECT *  ',
  'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
  SQL_Periodo_Valido,
  'ORDER BY Nome ',
  '']);
  //

end;

procedure TFmEFD_D100.RGTP_CTEClick(Sender: TObject);
begin
  if RGTP_CTE.ItemIndex < 0 then
    RGTP_CTE.ItemIndex := 0;
end;

(*
object Label22: TLabel
  Left = 8
  Top = 200
  Width = 59
  Height = 13
  Caption = 'Valor do PIS'
end
object Label23: TLabel
  Left = 8
  Top = 224
  Width = 81
  Height = 13
  Caption = 'Valor da COFINS'
end
object EdVL_PIS: TdmkEdit
  Left = 332
  Top = 196
  Width = 76
  Height = 21
  Alignment = taRightJustify
  TabOrder = 5
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  QryCampo = 'VL_PIS'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
  ValWarn = False
end
object EdVL_COFINS: TdmkEdit
  Left = 332
  Top = 220
  Width = 76
  Height = 21
  Alignment = taRightJustify
  TabOrder = 6
  FormatType = dmktfDouble
  MskType = fmtNone
  DecimalSize = 2
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0,00'
  QryCampo = 'VL_COFINS'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0.000000000000000000
  ValWarn = False
end
*)

end.
