unit SPED_EFD_Tabelas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP, Variants, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmSPED_EFD_Tabelas = class(TForm)
    Panel1: TPanel;
    Panel16: TPanel;
    LaAviso1: TLabel;
    PB1: TProgressBar;
    StatusBar: TStatusBar;
    IdHTTP1: TIdHTTP;
    Memo1: TMemo;
    QrTabelas: TmySQLQuery;
    QrVersao: TmySQLQuery;
    QrVersaoVersao: TWideStringField;
    PB2: TProgressBar;
    LaAviso2: TLabel;
    QrDupl: TmySQLQuery;
    QrDuplCodTxt: TWideStringField;
    Label1: TLabel;
    Panel3: TPanel;
    BtBacen_Pais: TBitBtn;
    QrSorc: TmySQLQuery;
    BtDTB_Munici: TBitBtn;
    QrDest: TmySQLQuery;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    CkForcaCad: TCheckBox;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtBacen_PaisClick(Sender: TObject);
    procedure BtDTB_MuniciClick(Sender: TObject);
  private
    { Private declarations }
    FWorkCount, FWorkCountMax: Integer;
    Fbacen_pais, Fdtb_munici: String;
    //
    function  DownloadFileHTTP(Fonte, Destino: String): Boolean;
    function  AtualizaTabela(Tabela, Versao: String; lstArq: TStringList): Boolean;
    //procedure HabilitaBtns();

  public
    { Public declarations }
  end;

  var
  FmSPED_EFD_Tabelas: TFmSPED_EFD_Tabelas;

implementation

uses Module, UnMyObjects, dmkGeral, ModuleGeral, UMySQLModule;

{$R *.DFM}

function TFmSPED_EFD_Tabelas.AtualizaTabela(Tabela, Versao: String;
  lstArq: TStringList): Boolean;
var
  Linha, NomeTb, Filer: String;
  I, P, CodTab, Controle: Integer;
  Achou, Continua: Boolean;
  lstLin: TStringList;
  Codigo: Variant;
  CodTxt, Nome, DataIni, DataFim, OGC, EX, NCM, NCM_EXCECAO, UNIDADE: String;
  DataI, DataF: TDateTime;
  ALIQ_PIS_PERC, ALIQ_PIS_QUANT, ALIQ_COFINS_PERC, ALIQ_COFINS_QUANT: Double;
begin
  Result := False;
  CodTab := Geral.IMV(Tabela);
  Achou := False;
  case Length(Tabela) of
    1: Filer := '00';
    2: Filer := '0';
    3: Filer := '';
  end;
  NomeTb := 'tbspedefd' + Filer + Tabela;
  QrTabelas.First;
  while not QrTabelas.Eof do
  begin
    if Lowercase(QrTabelas.Fields[0].AsString) =  NomeTb then
    begin
      Achou := True;
      Break;
    end;
    //
    QrTabelas.Next;
  end;
  if not Achou then
  begin
    Geral.MensagemBox('Tabela n�o localizada:' + #13#10 +
    NomeTb + #13#10 + 'AVISE A DERMATEK!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrVersao, DModG.AllID_DB, [
  'SELECT Versao FROM spedefdtabt WHERE Controle=' + Tabela,
  '']);
  //
  if (Trim(QrVersaoVersao.Value) <> Trim(Versao)) or CkForcaCad.Checked then
  begin
    MyObjects.Informa(LaAviso1, True, 'Tabela: ' +
      Tabela + '. Arquivo baixado. Atualizando base de dados');
    DModG.QrAllUpd.SQL.Clear;
    DModG.QrAllUpd.SQL.Add('DELETE FROM ' + NomeTb);
    //Seguran�a?
    //DModG.QrAllUpd.SQL.Add('WHERE DataIni<>0 ' + NomeTb);
    DModG.QrAllUpd.ExecSQL;
    PB2.Position := 0;
    PB2.Max := lstArq.Count - 1;
    for I := 1 to lstArq.Count - 1 do
    begin
      Linha := lstArq[I];
      PB2.Position := PB2.Position + 1;
      MyObjects.Informa(LaAviso2, True, 'Inserindo registro: ' + Linha);
      //
      P := pos('|', Linha);
      if P > 0 then
      begin
        lstLin := TStringList.Create;
        try
          Geral.MyExtractStrings(['|'], [' '], PChar(Linha + '|'), lstLin);
          CodTxt  := Copy(Linha, 1, P-1);
          // Evitar erro de SQL STRICT TRANS TABLES
          //Nome    := lstLin[00];
          Nome    := Copy(lstLin[00], 1, 255);
          //
          DataIni := lstLin[01];
          DataFim := '0000000000';
          if lstLin.Count > 2 then
            DataFim := lstLin[02];
          DataI := Geral.ValidaDataBR(DataIni, False, False);
          DataF := Geral.ValidaDataBR(DataFim, True, False);
          DataIni := Geral.FDT(DataI, 1);
          DataFim := Geral.FDT(DataF, 1);
          Continua := True;
          case CodTab of
            001..007: Codigo := Geral.IMV(CodTxt);
                 008: Codigo := NULL;
                 009: Codigo := Geral.IMV(CodTxt);
                 010: Codigo := NULL;
            011..013: Codigo := Geral.IMV(CodTxt);
            014, 015: Codigo := NULL;
                 016: Codigo := Geral.IMV(CodTxt);
                 017: Codigo := NULL;
            018, 019: Codigo := Geral.IMV(CodTxt);
                 020: Codigo := NULL;
            021..032: Codigo := Geral.IMV(CodTxt);
            033..060: Codigo := NULL;
          //061..120: ????
            121..124: Codigo := NULL;
                 125: Codigo := Geral.IMV(CodTxt);
            126..129: Codigo := NULL;
                 130: Codigo := Geral.IMV(CodTxt);
                 131: Codigo := NULL;
                 132: Codigo := Geral.IMV(CodTxt);
                 133: Codigo := NULL;
            134..141: Codigo := Geral.IMV(CodTxt);
                 142: Codigo := NULL;
            143..169: Codigo := Geral.IMV(CodTxt);
            else
            begin
              Memo1.Lines.Add('Tabela n�o implementada: ' + NomeTb);
              Continua := False;
            end;
          end;
          if Continua then
          begin
            //
            QrDupl.Close;
            QrDupl.SQL.Clear;
            QrDupl.SQL.Add('SELECT CodTxt');
            QrDupl.SQL.Add('FROM ' + NomeTb);
            QrDupl.SQL.Add('WHERE CodTxt="' + CodTxt + '"');
            QrDupl.SQL.Add('AND  DataIni="' + DataIni + '"');
            QrDupl.Open;
            if QrDupl.RecordCount > 0 then
            begin
              Memo1.Lines.Add('Registro duplicado na tabela ' + NomeTb + ': ' + CodTxt);
            end else begin
              case CodTab of
                155:
                begin
                  OGC := lstLin[03];
                  //
                  UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, NomeTb, False, [
                  'Codigo', 'Nome', 'DataIni',
                  'DataFim', 'OGC'], [
                  'CodTxt'], [
                  Codigo, Nome, DataIni,
                  DataFim, OGC], [
                  CodTxt], True);
                end;
                156, 162, 163:
                begin
                  NCM              :=              lstLin[03];
                  NCM_EXCECAO      :=              lstLin[04];
                  EX               :=              lstLin[05];
                  ALIQ_PIS_PERC    := Geral.DMV(lstLin[06]);
                  ALIQ_COFINS_PERC := Geral.DMV(lstLin[07]);
                  //
                  UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, NomeTb, False, [
                  'Codigo', 'Nome', 'DataIni',
                  'DataFim', 'NCM', 'NCM_EXCECAO',
                  'EX', 'ALIQ_PIS_PERC', 'ALIQ_COFINS_PERC'], [
                  'CodTxt'], [
                  Codigo, Nome, DataIni,
                  DataFim, NCM, NCM_EXCECAO,
                  EX, ALIQ_PIS_PERC, ALIQ_COFINS_PERC], [
                  CodTxt], True);
                end;
                164:
                begin
                  NCM               :=              lstLin[03];
                  NCM_EXCECAO       :=              lstLin[04];
                  EX                :=              lstLin[05];
                  UNIDADE           :=              lstLin[06];
                  ALIQ_PIS_QUANT    := Geral.DMV(lstLin[07]);
                  ALIQ_COFINS_QUANT := Geral.DMV(lstLin[08]);
                  //
                  UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, NomeTb, False, [
                  'Codigo', 'Nome', 'DataIni',
                  'DataFim', 'NCM', 'NCM_EXCECAO',
                  'EX', 'UNIDADE', 'ALIQ_PIS_QUANT',
                  'ALIQ_COFINS_QUANT'], [
                  'CodTxt'], [
                  Codigo, Nome, DataIni,
                  DataFim, NCM, NCM_EXCECAO,
                  EX, UNIDADE, ALIQ_PIS_QUANT,
                  ALIQ_COFINS_QUANT], [
                  CodTxt], True);
                end;
                165:
                begin
                  NCM               :=              lstLin[03];
                  NCM_EXCECAO       :=              lstLin[04];
                  EX                :=              lstLin[05];
                  ALIQ_PIS_PERC     := Geral.DMV(lstLin[06]);
                  ALIQ_PIS_QUANT    := Geral.DMV(lstLin[07]);
                  ALIQ_COFINS_PERC  := Geral.DMV(lstLin[08]);
                  ALIQ_COFINS_QUANT := Geral.DMV(lstLin[09]);
                  //
                  UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, NomeTb, False, [
                  'Codigo', 'Nome', 'DataIni',
                  'DataFim', 'NCM', 'NCM_EXCECAO',
                  'EX', 'ALIQ_PIS_PERC', 'ALIQ_PIS_QUANT',
                  'ALIQ_COFINS_PERC', 'ALIQ_COFINS_QUANT'], [
                  'CodTxt'], [
                  Codigo, Nome, DataIni,
                  DataFim, NCM, NCM_EXCECAO,
                  EX, ALIQ_PIS_PERC, ALIQ_PIS_QUANT,
                  ALIQ_COFINS_PERC, ALIQ_COFINS_QUANT], [
                  CodTxt], True);
                end;
                166..169:
                begin
                  NCM               :=              lstLin[03];
                  NCM_EXCECAO       :=              lstLin[04];
                  EX                :=              lstLin[05];
                  //
                  UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, NomeTb, False, [
                  'Codigo', 'Nome', 'DataIni',
                  'DataFim', 'NCM', 'NCM_EXCECAO',
                  'EX'], [
                  'CodTxt'], [
                  Codigo, Nome, DataIni,
                  DataFim, NCM, NCM_EXCECAO,
                  EX], [
                  CodTxt], True);
                end;
                else
                begin
                  if (Codigo = 1058) and (Uppercase(Nome)='BRASIL') then
                     Fbacen_pais := NomeTb;
                  if (Codigo = 5300108) and (Lowercase(Nome)='bras�lia') then
                     Fdtb_munici := NomeTb;
                  UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, NomeTb, False, [
                  'Codigo', 'Nome', 'DataIni', 'DataFim'], [
                  'CodTxt'], [
                  Codigo, Nome, DataIni, DataFim], [
                  CodTxt], True);
                end;
              end;
            end;
          end;
        finally
          if lstLin <> nil then
            lstLin.Free;
        end;
      end;
    end;
    Controle := CodTab;
    UMyMod.SQLInsUpd(DModG.QrAllUpd, stUpd, 'spedefdtabt', False, [
    'Versao'], ['Controle'], [
    Versao], [ Controle], True);
  end;
  MyObjects.Informa(LaAviso2, False, '...');
  BtBacen_Pais.Enabled := Fbacen_pais <> '';
  BtDTB_Munici.Enabled := Fdtb_munici <> '';
end;

procedure TFmSPED_EFD_Tabelas.BtBacen_PaisClick(Sender: TObject);
var
  Itens, Codigo: Integer;
  //Txt,
  CodTxt, Nome: String;
begin
  Itens := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSorc, DModG.AllID_DB, [
  'SELECT Codigo, Nome ',
  'FROM ' + Fbacen_pais,
  '']);
  //
  QrSorc.First;
  while not QrSorc.Eof do
  begin
    Codigo := QrSorc.FieldByName('Codigo').AsInteger;
    CodTxt := Geral.FF0(Codigo);
    UnDmkDAC_PF.AbreMySQLQuery0(QrDest, DModG.AllID_DB, [
    'SELECT Codigo, Nome ',
    'FROM bacen_pais',
    'WHERE Codigo=' + CodTxt,
    '']);
    //
    if QrDest.RecordCount = 0 then
    begin
      Itens := Itens + 1;
      Nome := QrSorc.FieldByName('Nome').AsString;
      if UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, 'bacen_pais', False, [
      'Nome'], ['Codigo'], [Nome], [Codigo], False) then
      begin
        MyObjects.Informa(LaAviso1, False, 'Pa�ses inclu�dos: ' + Geral.FF0(Itens) +
        '  ::  �ltimo incluido: ' + CodTxt + ' > "' + Nome + '"');
      end;
    end;
    //
    QrSorc.Next;
  end;
end;

procedure TFmSPED_EFD_Tabelas.BtDTB_MuniciClick(Sender: TObject);
var
  Itens, Codigo: Integer;
  //Txt,
  CodTxt, Nome, CodUsu, DTB_UF: String;
begin
  Itens := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSorc, DModG.AllID_DB, [
  'SELECT Codigo, Nome ',
  'FROM ' + Fdtb_munici,
  '']);
  //
  QrSorc.First;
  while not QrSorc.Eof do
  begin
    Codigo := QrSorc.FieldByName('Codigo').AsInteger;
    CodTxt := Geral.FF0(Codigo);
    UnDmkDAC_PF.AbreMySQLQuery0(QrDest, DModG.AllID_DB, [
    'SELECT Codigo, Nome ',
    'FROM dtb_munici',
    'WHERE Codigo=' + CodTxt,
    '']);
    //
    if QrDest.RecordCount = 0 then
    begin
      Itens := Itens + 1;
      Nome := QrSorc.FieldByName('Nome').AsString;
      DTB_UF := Copy(CodTxt, 1, 2);
      CodUsu := Copy(CodTxt, 3);
      if UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, 'dtb_munici', False, [
      'Nome', 'CodUsu', 'DTB_UF'], ['Codigo'], [
      Nome, CodUsu, DTB_UF], [Codigo], False) then
      begin
        MyObjects.Informa(LaAviso1, False, 'Munic�pios inclu�dos: ' + Geral.FF0(Itens) +
        '  ::  �ltimo incluido: ' + CodTxt + ' > "' + Nome + '"');
      end;
    end;
    //
    QrSorc.Next;
  end;
end;

procedure TFmSPED_EFD_Tabelas.BtOKClick(Sender: TObject);
const
  MaxTabs = 300;

  //DirF = 'http://www.sped.fazenda.gov.br/spedtabelas/appconsulta/publico/aspx/obterTabelaExterna.aspx?idTabela=';
  DirF = 'http://www.sped.fazenda.gov.br/spedtabelas/appconsulta/obterTabelaExterna.aspx?idPacote=1&idTabela=';
  //DirF = 'http://www.sped.fazenda.gov.br/spedtabelas/AppConsulta/publico/aspx/obterTabelaExterna.aspx?idTabela=';
  DirD = 'C:\Dermatek\SPED\DownLoadTabsEFD';
var
  I: Integer;
  Tabela, Fonte, Destino, Linha, Versao: String;
  lstArq(*, lstLin*): TStringList;
begin
  Fbacen_pais := '';
  Fdtb_munici := '';
  //
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrTabelas, Dmod.MyDB, [
      'SHOW TABLES ',
      'LIKE "tbspedefd%" ',
      '']);
    Memo1.Lines.Clear;
    //
    PB1.Position := 0;
    PB1.Max      := MaxTabs;
    //
    if not DirectoryExists(DirD) then
      ForceDirectories(DirD);
    for I := 0 to MaxTabs do
    begin
      PB1.Position := PB1.Position + 1;
      Tabela := FormatFloat('0', I);
      MyObjects.Informa(LaAviso1, True, 'Tabela: ' + Tabela + '. Baixando');
      //
      Fonte := DirF + Tabela;
      Destino := DirD + '\tb' + Tabela + '.txt';
      if FileExists(Destino) then
        DeleteFile(Destino);
      if DownloadFileHTTP(Fonte, Destino) then
      begin
        if FileExists(Destino) then
        begin
          MyObjects.Informa(LaAviso1, True, 'Tabela: ' +
          Tabela + '. Arquivo baixado. Verificando arquivo');
          lstArq := TStringList.Create;
          try
            lstArq.LoadFromFile(Destino);
            if lstArq.Count > 0 then
            begin
              Versao := '';
              Linha := lstArq[0];
              if (Linha <> '') and (Copy(Linha, 0, 3) <> '200') then
              begin
                if pos('vers�o', Lowercase(Linha)) > 0 then
                begin
                  Versao := Trim(Copy(Linha, 8));
                  if pos(' ', Versao) > 0 then
                    Versao := Copy(Versao, 1, pos(' ', Versao));
                  AtualizaTabela(Tabela, Versao, lstArq);
                end;
                //Memo1.Lines.Add(Tabela + ': ' + Linha + ' # ' + Versao);
              end else DeleteFile(Destino);
            end;
          finally
            if lstArq <> nil then
              lstArq.Free;
          end;
        end;
      end;
    end;
      MyObjects.Informa(LaAviso1, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSPED_EFD_Tabelas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmSPED_EFD_Tabelas.DownloadFileHTTP(Fonte, Destino: String): Boolean;
var
  MyFile: TFileStream;
begin
  //Result := False;
  FWorkCount := 0;
  FWorkCountMax := 0;
  MyFile := TFileStream.Create(Destino, fmCreate); // local no hd e nome do arquivo com a extens�o, onde vai salvar.
  try
    //IdHTTP1.Get('http://www.arquivojuridico.com/'+arquivo, MyFile); // fazendo o download do arquivo
    IdHTTP1.Get(Fonte, MyFile); // fazendo o download do arquivo
    Result := True;
  finally
    MyFile.Free;
  end;
end;

procedure TFmSPED_EFD_Tabelas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSPED_EFD_Tabelas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  Fbacen_pais     := '';
  Fdtb_munici     := '';
end;

procedure TFmSPED_EFD_Tabelas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

{
procedure TFmSPED_EFD_Tabelas.HabilitaBtns();
begin
 //
end;
}

end.
