unit SPED_Listas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, UnMyLinguas, UnInternalConsts, dmkGeral, DB,
  mySQLDbTables, UnDmkProcFunc, UnDmkEnums;

const
  //Tabelas publicas do SPED
  CO_NOME_TbSPEDEFD_TABELAS_SPED = 'spedefdtabt'; // Lista de tabelas do SPED
  CO_NOME_TbSPEDEFD_ICMS_CST = 'tbspedefd130'; // CST do ICMS
  CO_NOME_TbSPEDEFD_CFOP = 'tbspedefd002'; // tabela do CFOP
  CO_NOME_TbSPEDEFD_IPI_CST = 'tbspedefd026'; // CST do IPI
  CO_NOME_TbSPEDEFD_PIS_CST = 'tbspedefd027'; // CST do PIS
  CO_NOME_TbSPEDEFD_COFINS_CST = 'tbspedefd023'; // CST do COFINS
  CO_NOME_TbSPEDEFD_VERSAO_LAYOUT = 'tbspedefd011'; // Vers�es de layout
  //
  CO_SPED_TIPO_ITEM_K200 = '0,1,2,3,4,5,6,10';
  // IND_APUR Indicador de per�odo de apura��o do IPI:
  CO_EFD_IND_APUR_00_TXT_MENSAL = 'Mensal';
  CO_EFD_IND_APUR_01_TXT_DECENDIAL = 'Decendial';
  CO_EFD_IND_APUR_02_TXT_INDEFINIDO = '????';
  MaxEFD_IND_APUR = 02;
  sEFD_IND_APUR: array[0..MaxEFD_IND_APUR] of string = (
    CO_EFD_IND_APUR_00_TXT_MENSAL,
    CO_EFD_IND_APUR_01_TXT_DECENDIAL,
    CO_EFD_IND_APUR_02_TXT_INDEFINIDO);
  //
  //04 MOT_INV Informe o motivo do Invent�rio:
  CO_EFD_MOT_INV_00_TXT_INDEFINIDO = 'Indefinido';
  CO_EFD_MOT_INV_01_TXT_FINAL_PERIODO = 'No final no per�odo';
  CO_EFD_MOT_INV_02_TXT_MUDAN_FRM_TRIBUT = 'Na mudan�a de forma de tributa��o da mercadoria (ICMS)';
  CO_EFD_MOT_INV_03_TXT_PARADA = 'Na solicita��o da baixa cadastral, paralisa��o tempor�ria e outras situa��es';
  CO_EFD_MOT_INV_04_TXT_ALT_REGIME = 'Na altera��o de regime de pagamento � condi��o do contribuinte';
  CO_EFD_MOT_INV_05_TXT_DETERMIN_FISCO = 'Por determina��o dos fiscos.';
  MaxEFD_MOT_INV = 05;
  sEFD_MOT_INV: array[0..MaxEFD_MOT_INV] of string = (
    CO_EFD_MOT_INV_00_TXT_INDEFINIDO,
    CO_EFD_MOT_INV_01_TXT_FINAL_PERIODO,
    CO_EFD_MOT_INV_02_TXT_MUDAN_FRM_TRIBUT,
    CO_EFD_MOT_INV_03_TXT_PARADA,
    CO_EFD_MOT_INV_04_TXT_ALT_REGIME,
    CO_EFD_MOT_INV_05_TXT_DETERMIN_FISCO);
  //
  // Grandeza (Dermatek)
  CO_GRANDEZA_UNIDMED_00_TXT_PECA     = 'Pe�a';
  CO_GRANDEZA_UNIDMED_01_TXT_AREAM2   = '�rea (m�)';
  CO_GRANDEZA_UNIDMED_02_TXT_PESOKG   = 'Peso (kg)';
  CO_GRANDEZA_UNIDMED_03_TXT_VOLUMEM3 = 'Volume (m�)';
  CO_GRANDEZA_UNIDMED_04_TXT_LINEARM  = 'Linear (m)';
  CO_GRANDEZA_UNIDMED_05_TXT_OUTROS   = 'Outro (???)';
  CO_GRANDEZA_UNIDMED_06_TXT_AREAP2   = '�rea (ft�)';
  CO_GRANDEZA_UNIDMED_07_TXT_PESOTON  = 'Peso (Ton)';
  MaxGRANDEZA_UNIDMED = 07;
  sGRANDEZA_UNIDMED: array[0..MaxGRANDEZA_UNIDMED] of string = (
    CO_GRANDEZA_UNIDMED_00_TXT_PECA     ,
    CO_GRANDEZA_UNIDMED_01_TXT_AREAM2   ,
    CO_GRANDEZA_UNIDMED_02_TXT_PESOKG   ,
    CO_GRANDEZA_UNIDMED_03_TXT_VOLUMEM3 ,
    CO_GRANDEZA_UNIDMED_04_TXT_LINEARM  ,
    CO_GRANDEZA_UNIDMED_05_TXT_OUTROS   ,
    CO_GRANDEZA_UNIDMED_06_TXT_AREAP2   ,
    CO_GRANDEZA_UNIDMED_07_TXT_PESOTON  );
  //                                    `
  //
  //05 IND_EST Indicador do tipo de estoque:
  CO_EFD_IND_EST_00_TXT_PROPINFPODERINF = 'Estoque de propriedade do informante e em seu poder';
  CO_EFD_IND_EST_01_TXT_PROPINFPODERTER = 'Estoque de propriedade do informante e em posse de terceiros';
  CO_EFD_IND_EST_02_TXT_PROPTERPODERINF = 'Estoque de propriedade de terceiros e em posse do informante';
  CO_EFD_IND_EST_03_TXT_INDEFINIDO = '???';
  MaxEFD_IND_EST = 03;
  sEFD_IND_EST: array[0..MaxEFD_IND_EST] of string = (
    CO_EFD_IND_EST_00_TXT_PROPINFPODERINF,
    CO_EFD_IND_EST_01_TXT_PROPINFPODERTER,
    CO_EFD_IND_EST_02_TXT_PROPTERPODERINF,
    CO_EFD_IND_EST_03_TXT_INDEFINIDO);
  //
  CO_TXT_00_efdexer_NaoImplem      = 'ERRO N�O IMPLEMENTADO. AVISE A DERMATEK!';
  CO_TXT_01_efdexer_Unknown        = 'Campo desconhecido';
  CO_TXT_02_efdexer_ObrigSemCont   = 'Campo de conte�do obrigat�rio sem conte�do';
  CO_TXT_03_efdexer_ErrSeqGerLin   = 'Erro de sequencia na gera��o de linha';
  CO_TXT_04_efdexer_TipoFldIncorr  = 'Tipo de campo incorreto (C ou N)';
  CO_TXT_05_efdexer_DifDecimais    = 'Decimais difere do esperado';
  CO_TXT_06_efdexer_DocInvalido    = 'Documento inv�lido';
  CO_TXT_07_efdexer_TamTxtDif      = 'Tamanho do texto difere do obrigat�rio';
  CO_TXT_08_efdexer_TamTxtExce     = 'Tamanho do texto excede o m�ximo';
  CO_TXT_09_efdexer_TipObrigUnkn   = 'Tipo de obrigatoriedade desconhecida';
  CO_TXT_10_efdexer_ServicInval    = 'C�digo de servi�o LC 116/03 pode ser inv�lido';
  CO_TXT_11_efdexer_FaltaNCM       = 'C�digo NCM n�o cadastrado para o produto';
  CO_TXT_12_efdexer_ErrFldAlfa     = 'Campo n�o pode ser alfanum�rico';
  CO_TXT_13_efdexer_CFOPUnkn       = 'CFOP n�o consta na tabela SPED EFD "' + CO_NOME_TbSPEDEFD_CFOP + '"';
  CO_TXT_14_efdexer_ModFiscInval   = 'Codigo do modelo do documento fiscal inv�lido';
  CO_TXT_15_efdexer_CFOPInval      = 'CFOP inv�lido. 1� caracter deve ser 1, 2 ou 3 para entrada e 5, 6 ou 7 para sa�da';
  CO_TXT_16_efdexer_ValInval       = 'Valor inv�lido. Soma dos VL_BC_ICMS dos registros anal�ticos n�o confere com o registro mestre';
  CO_TXT_17_efdexer_CodMunIndef    = 'C�digo do munic�pio n�o definido';
  CO_TXT_18_efdexer_FldObrigValOp  = 'Campo obrigat�rio. Valor da opera��o';
  CO_TXT_19_efdexer_RegFilhObrig   = 'Registro filho obrigat�rio n�o informado';
  MaxEFD_Export_Err = 19;
  sEFD_Export_Err: array[0..MaxEFD_Export_Err] of string = (
    CO_TXT_00_efdexer_NaoImplem,
    CO_TXT_01_efdexer_Unknown,
    CO_TXT_02_efdexer_ObrigSemCont,
    CO_TXT_03_efdexer_ErrSeqGerLin,
    CO_TXT_04_efdexer_TipoFldIncorr,
    CO_TXT_05_efdexer_DifDecimais,
    CO_TXT_06_efdexer_DocInvalido,
    CO_TXT_07_efdexer_TamTxtDif,
    CO_TXT_08_efdexer_TamTxtExce,
    CO_TXT_09_efdexer_TipObrigUnkn,
    CO_TXT_10_efdexer_ServicInval,
    CO_TXT_11_efdexer_FaltaNCM,
    CO_TXT_12_efdexer_ErrFldAlfa,
    CO_TXT_13_efdexer_CFOPUnkn,
    CO_TXT_14_efdexer_ModFiscInval,
    CO_TXT_15_efdexer_CFOPInval,
    CO_TXT_16_efdexer_ValInval,
    CO_TXT_17_efdexer_CodMunIndef,
    CO_TXT_18_efdexer_FldObrigValOp,
    CO_TXT_19_efdexer_RegFilhObrig
  );
  //
  //05 Periodo de apura��o da Receita Federal:
  // parei aqui
  CO_EFD_PERIODO_00_TXT_INDEF = 'Indefinido';
  CO_EFD_PERIODO_01_TXT_DIARIO = 'Di�rio';
  CO_EFD_PERIODO_02_TXT_SEMANAL = 'Semanal';
  CO_EFD_PERIODO_03_TXT_DECENDIAL = 'Decendial';
  CO_EFD_PERIODO_04_TXT_QUINZENAL = 'Quinzenal';
  CO_EFD_PERIODO_05_TXT_MENSAL = 'Mensal';
  MaxEFD_PERIODO = 05;
  sEFD_PERIODO: array[0..MaxEFD_PERIODO] of string = (
    CO_EFD_PERIODO_00_TXT_INDEF,
    CO_EFD_PERIODO_01_TXT_DIARIO,
    CO_EFD_PERIODO_02_TXT_SEMANAL,
    CO_EFD_PERIODO_03_TXT_DECENDIAL,
    CO_EFD_PERIODO_04_TXT_QUINZENAL,
    CO_EFD_PERIODO_05_TXT_MENSAl
  );
  //
type
  //TTipoTroca = (trcOA, trcOsAs, trcPara);
  TUnAppListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  UnAppListas: TUnAppListas;

implementation

uses Module, DmkDAC_PF, ModuleGeral, UnMyObjects;

{ TUnAppListas }

end.

