unit SPEDEstqGer;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Menus, ComCtrls, Grids,
  DBGrids, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, Variants,
  dmkDBGrid, frxClass, frxDBSet, dmkCheckGroup, UnDmkEnums;

type
  TFmSPEDEstqGer = class(TForm)
    PainelDados: TPanel;
    DsSPEDEstqCab: TDataSource;
    QrSPEDEstqCab: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PnPesq: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PnCabeca: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    PMInclui: TPopupMenu;
    QrSPEDEstqIts: TmySQLQuery;
    DsSPEDEstqIts: TDataSource;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    PMAltera: TPopupMenu;
    PMExclui: TPopupMenu;
    Panel6: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Panel7: TPanel;
    QrSPEDEstqCabImporExpor: TSmallintField;
    QrSPEDEstqCabAnoMes: TIntegerField;
    QrSPEDEstqCabEmpresa: TIntegerField;
    QrSPEDEstqCabEncerrado: TSmallintField;
    NovoPerodo1: TMenuItem;
    Itemns1: TMenuItem;
    ItemSelecionado1: TMenuItem;
    Periodoselecionado1: TMenuItem;
    ItemSelecionado2: TMenuItem;
    QrSPEDEstqCabMES_ANO: TWideStringField;
    QrSPEDEstqCabNO_ENT: TWideStringField;
    QrSPEDEstqItsNivel1: TIntegerField;
    QrSPEDEstqItsNO_PRD: TWideStringField;
    QrSPEDEstqItsPrdGrupTip: TIntegerField;
    QrSPEDEstqItsUnidMed: TIntegerField;
    QrSPEDEstqItsNO_PGT: TWideStringField;
    QrSPEDEstqItsSIGLA: TWideStringField;
    QrSPEDEstqItsNO_TAM: TWideStringField;
    QrSPEDEstqItsNO_COR: TWideStringField;
    QrSPEDEstqItsGraCorCad: TIntegerField;
    QrSPEDEstqItsGraGruC: TIntegerField;
    QrSPEDEstqItsGraGru1: TIntegerField;
    QrSPEDEstqItsGraTamI: TIntegerField;
    QrSPEDEstqItsControle: TIntegerField;
    QrSPEDEstqItsImporExpor: TSmallintField;
    QrSPEDEstqItsAnoMes: TIntegerField;
    QrSPEDEstqItsEmpresa: TIntegerField;
    QrSPEDEstqItsGraGruX: TIntegerField;
    QrSPEDEstqItsSit_Prod: TSmallintField;
    QrSPEDEstqItsTerceiro: TIntegerField;
    QrSPEDEstqItsEstqIniQtd: TFloatField;
    QrSPEDEstqItsEstqIniPrc: TFloatField;
    QrSPEDEstqItsEstqIniVal: TFloatField;
    QrSPEDEstqItsComprasQtd: TFloatField;
    QrSPEDEstqItsComprasPrc: TFloatField;
    QrSPEDEstqItsComprasVal: TFloatField;
    QrSPEDEstqItsConsumoQtd: TFloatField;
    QrSPEDEstqItsConsumoPrc: TFloatField;
    QrSPEDEstqItsConsumoVal: TFloatField;
    QrSPEDEstqItsEstqFimQtd: TFloatField;
    QrSPEDEstqItsEstqFimPrc: TFloatField;
    QrSPEDEstqItsEstqFimVal: TFloatField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida0: TBitBtn;
    Panel5: TPanel;
    Panel1: TPanel;
    Panel4: TPanel;
    BtSaida1: TBitBtn;
    Panel8: TPanel;
    BtExporta1: TBitBtn;
    QrSEI: TmySQLQuery;
    QrSEINivel1: TIntegerField;
    QrSEINO_PRD: TWideStringField;
    QrSEIPrdGrupTip: TIntegerField;
    QrSEIUnidMed: TIntegerField;
    QrSEINO_PGT: TWideStringField;
    QrSEISIGLA: TWideStringField;
    QrSEINO_TAM: TWideStringField;
    QrSEINO_COR: TWideStringField;
    QrSEIGraCorCad: TIntegerField;
    QrSEIGraGruC: TIntegerField;
    QrSEIGraGru1: TIntegerField;
    QrSEIGraTamI: TIntegerField;
    QrSEIControle: TIntegerField;
    QrSEIImporExpor: TSmallintField;
    QrSEIAnoMes: TIntegerField;
    QrSEIEmpresa: TIntegerField;
    QrSEIGraGruX: TIntegerField;
    QrSEISit_Prod: TSmallintField;
    QrSEITerceiro: TIntegerField;
    QrSEIEstqIniQtd: TFloatField;
    QrSEIEstqIniPrc: TFloatField;
    QrSEIEstqIniVal: TFloatField;
    QrSEIComprasQtd: TFloatField;
    QrSEIComprasPrc: TFloatField;
    QrSEIComprasVal: TFloatField;
    QrSEIConsumoQtd: TFloatField;
    QrSEIConsumoPrc: TFloatField;
    QrSEIConsumoVal: TFloatField;
    QrSEIEstqFimQtd: TFloatField;
    QrSEIEstqFimPrc: TFloatField;
    QrSEIEstqFimVal: TFloatField;
    QrSEINO_ENT: TWideStringField;
    QrSEICNPJ_CPF: TWideStringField;
    QrSEIIE: TWideStringField;
    QrSEIEUF: TSmallintField;
    QrSEIPUF: TSmallintField;
    QrSEITipo: TSmallintField;
    QrSEIPrintTam: TSmallintField;
    QrSEIPrintCor: TSmallintField;
    QrSEINCM: TWideStringField;
    QrExport2: TmySQLQuery;
    DsExport2: TDataSource;
    frxDsExporta2: TfrxDBDataset;
    QrListErr1: TmySQLQuery;
    QrListErr1My_Idx: TIntegerField;
    QrListErr1Codigo: TWideStringField;
    QrListErr1Texto1: TWideStringField;
    DsListErr1: TDataSource;
    frxDsListErr1: TfrxDBDataset;
    QrExport2Tipo: TSmallintField;
    QrExport2IE: TWideStringField;
    QrExport2EUF: TSmallintField;
    QrExport2PUF: TSmallintField;
    QrExport2NO_ENT: TWideStringField;
    QrExport2CNPJ_CPF: TWideStringField;
    QrExport2PrintTam: TSmallintField;
    QrExport2PrintCor: TSmallintField;
    QrExport2NCM: TWideStringField;
    QrExport2Nivel1: TIntegerField;
    QrExport2NO_PRD: TWideStringField;
    QrExport2PrdGrupTip: TIntegerField;
    QrExport2UnidMed: TIntegerField;
    QrExport2NO_PGT: TWideStringField;
    QrExport2SIGLA: TWideStringField;
    QrExport2NO_TAM: TWideStringField;
    QrExport2NO_COR: TWideStringField;
    QrExport2GraCorCad: TIntegerField;
    QrExport2GraGruC: TIntegerField;
    QrExport2GraGru1: TIntegerField;
    QrExport2GraTamI: TIntegerField;
    QrExport2Controle: TIntegerField;
    QrExport2ImporExpor: TSmallintField;
    QrExport2AnoMes: TIntegerField;
    QrExport2Empresa: TIntegerField;
    QrExport2GraGruX: TIntegerField;
    QrExport2Sit_Prod: TSmallintField;
    QrExport2Terceiro: TIntegerField;
    QrExport2EstqIniQtd: TFloatField;
    QrExport2EstqIniPrc: TFloatField;
    QrExport2EstqIniVal: TFloatField;
    QrExport2ComprasQtd: TFloatField;
    QrExport2ComprasPrc: TFloatField;
    QrExport2ComprasVal: TFloatField;
    QrExport2ConsumoQtd: TFloatField;
    QrExport2ConsumoPrc: TFloatField;
    QrExport2ConsumoVal: TFloatField;
    QrExport2EstqFimQtd: TFloatField;
    QrExport2EstqFimPrc: TFloatField;
    QrExport2EstqFimVal: TFloatField;
    QrLayout001: TmySQLQuery;
    QrLayout001DataInvent: TWideStringField;
    QrLayout001MesAnoInic: TWideStringField;
    QrLayout001MesAnoFina: TWideStringField;
    QrLayout001CodigoProd: TWideStringField;
    QrLayout001SituacProd: TWideStringField;
    QrLayout001CNPJTercei: TWideStringField;
    QrLayout001IETerceiro: TWideStringField;
    QrLayout001UFTerceiro: TWideStringField;
    QrLayout001Quantidade: TWideStringField;
    QrLayout001ValorUnita: TWideStringField;
    QrLayout001ValorTotal: TWideStringField;
    QrLayout001ICMSRecupe: TWideStringField;
    QrLayout001Observacao: TWideStringField;
    QrLayout001DescriProd: TWideStringField;
    QrLayout001GrupoProdu: TWideStringField;
    QrLayout001ClassifNCM: TWideStringField;
    QrLayout001RESERVADOS: TWideStringField;
    QrLayout001UnidMedida: TWideStringField;
    QrLayout001DescrGrupo: TWideStringField;
    DsLayout001: TDataSource;
    PageControl3: TPageControl;
    TabSheet10: TTabSheet;
    MeExp: TMemo;
    TabSheet11: TTabSheet;
    dmkDBGrid4: TdmkDBGrid;
    DBGrid2: TDBGrid;
    TabSheet13: TTabSheet;
    DBGrid3: TDBGrid;
    BtImprime1: TBitBtn;
    frxSPEDEFD_PRINT_000_01: TfrxReport;
    frxSPEDEFD_PRINT_001_02: TfrxReport;
    BtImprime0: TBitBtn;
    Panel9: TPanel;
    DBGIts: TdmkDBGrid;
    Panel10: TPanel;
    DBGCab: TdmkDBGrid;
    GroupBox1: TGroupBox;
    CGAgrup0: TdmkCheckGroup;
    frxDsSPEDEstqIts: TfrxDBDataset;
    QrSPEDEstqItsNivel2: TIntegerField;
    QrSPEDEstqItsNO_GG2: TWideStringField;
    QrSPEDEstqItsExportado: TSmallintField;
    BtImporta: TBitBtn;
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaida0Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrSPEDEstqCabBeforeClose(DataSet: TDataSet);
    procedure PMAlteraPopup(Sender: TObject);
    procedure QrSPEDEstqCabAfterScroll(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure QrSPEDEstqCabAfterOpen(DataSet: TDataSet);
    procedure NovoPerodo1Click(Sender: TObject);
    procedure Itemns1Click(Sender: TObject);
    procedure PMIncluiPopup(Sender: TObject);
    procedure ItemSelecionado1Click(Sender: TObject);
    procedure ItemSelecionado2Click(Sender: TObject);
    procedure Periodoselecionado1Click(Sender: TObject);
    procedure QrSPEDEstqCabCalcFields(DataSet: TDataSet);
    procedure BtExporta1Click(Sender: TObject);
    procedure QrSPEDEstqItsBeforeClose(DataSet: TDataSet);
    procedure QrSPEDEstqItsAfterOpen(DataSet: TDataSet);
    procedure BtImprime1Click(Sender: TObject);
    procedure frxSPEDEFD_PRINT_000_01GetValue(const VarName: string;
      var Value: Variant);
    procedure QrExport2BeforeClose(DataSet: TDataSet);
    procedure QrExport2AfterScroll(DataSet: TDataSet);
    procedure BtImprime0Click(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
  private
    { Private declarations }
    FExportarItem: Boolean;
    function VeriStr(Tam, Item: Integer; Texto: String): String;
    function DefineDataFim(): TDateTime;
    function PertenceAoGrupoGG2(): Boolean;
    function PertenceAoGrupoPGT(): Boolean;
    function PertenceAoGrupoPRD(): Boolean;
  public
    { Public declarations }
    FImporExpor, FEmpresa: Integer;
    FExpImpTXT, FEmprTXT: String;
    //
    procedure ReopenSPEDEstqCab(AnoMes: Integer);
    procedure ReopenSPEDEstqIts(GraGruX, Sit_Prod, Terceiro: Integer);
    //
  end;

var
  FmSPEDEstqGer: TFmSPEDEstqGer;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, ModuleGeral, SPEDEstqCab, SPEDEstqIts, ModProd, UCreate,
CfgExpFile, SPEDEstqLoadXLS;

{$R *.DFM}

procedure TFmSPEDEstqGer.EdEmpresaChange(Sender: TObject);
begin
  if EdEmpresa.ValueVariant <> 0 then
  begin
    FEmpresa := DModG.QrEmpresasCodigo.Value;
    FEmprTXT := FormatFloat('0', FEmpresa);
  end else
  begin
    FEmpresa := 0;
    FEmprTXT := '0';
  end;
  ReopenSPEDEstqCab(0);
end;

procedure TFmSPEDEstqGer.NovoPerodo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPEDEstqCab, FmSPEDEstqCab, afmoNegarComAviso) then
  begin
    FmSPEDEstqCab.ShowModal;
    FmSPEDEstqCab.Destroy;
  end;
end;

procedure TFmSPEDEstqGer.Itemns1Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  if UmyMod.FormInsUpd_Cria(TFmSPEDEstqIts, FmSPEDEstqIts, afmoNegarComAviso,
  QrSPEDEstqIts, stIns) then
  begin
    FmSPEDEstqIts.RGSit_Prod.ItemIndex := 1;
    FmSPEDEstqIts.ShowModal;
    FmSPEDEstqIts.Destroy;
  end;
end;

procedure TFmSPEDEstqGer.ItemSelecionado1Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  if UmyMod.FormInsUpd_Cria(TFmSPEDEstqIts, FmSPEDEstqIts, afmoNegarComAviso,
  QrSPEDEstqIts, stUpd) then
  begin
    FmSPEDEstqIts.ShowModal;
    FmSPEDEstqIts.Destroy;
  end;
end;

procedure TFmSPEDEstqGer.ItemSelecionado2Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrSPEDEstqIts, TDBGrid(DBGIts),
  'spedestqits', ['Controle'], ['Controle'], istPergunta, '');
(*
  UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item?', 'spedestqits',
    'Controle', QrSPEDEstqItsControle.Value);
*)
  ReopenSPEDEstqIts(0, 0, 0);
end;

procedure TFmSPEDEstqGer.Periodoselecionado1Click(Sender: TObject);
begin
{
  UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da liturgia?', 'cadcomitens',
    'Codigo', QR.Value);
  ReopenSP;
}
end;

function TFmSPEDEstqGer.PertenceAoGrupoGG2: Boolean;
begin
  case PageControl1.ActivePageIndex of
    0: Result := Geral.IntInConjunto(2, CGAgrup0.Value);
    else Result := False;
  end;
end;

function TFmSPEDEstqGer.PertenceAoGrupoPGT(): Boolean;
begin
  case PageControl1.ActivePageIndex of
    0: Result := Geral.IntInConjunto(4, CGAgrup0.Value);
    else Result := False;
  end;
end;

function TFmSPEDEstqGer.PertenceAoGrupoPRD(): Boolean;
begin
  case PageControl1.ActivePageIndex of
    0: Result := Geral.IntInConjunto(1, CGAgrup0.Value);
    else Result := False;
  end;
end;

procedure TFmSPEDEstqGer.PMAlteraPopup(Sender: TObject);
begin
  ItemSelecionado1.Enabled :=
    (QrSPEDEstqIts.State <> dsInactive)
    and
    (QrSPEDEstqIts.RecordCount > 0);
end;

procedure TFmSPEDEstqGer.PMExcluiPopup(Sender: TObject);
begin
  Periodoselecionado1.Enabled :=
    (QrSPEDEstqCab.State <> dsInactive)
    and
    (QrSPEDEstqCab.RecordCount > 0)
    and
    (QrSPEDEstqIts.RecordCount = 0);
  //
  ItemSelecionado1.Enabled :=
    (QrSPEDEstqIts.State <> dsInactive)
    and
    (QrSPEDEstqIts.RecordCount > 0);
end;

procedure TFmSPEDEstqGer.PMIncluiPopup(Sender: TObject);
begin
  Itemns1.Enabled :=
    (QrSPEDEstqCab.State <> dsInactive) and
    (QrSPEDEstqCab.RecordCount > 0);
end;

procedure TFmSPEDEstqGer.ReopenSPEDEstqIts(GraGruX, Sit_Prod, Terceiro: Integer);
begin
  QrSPEDEstqIts.Close;
  QrSPEDEstqIts.Params[00].AsInteger := QrSPEDEstqCabImporExpor.Value;
  QrSPEDEstqIts.Params[01].AsInteger := QrSPEDEstqCabEmpresa.Value;
  QrSPEDEstqIts.Params[02].AsInteger := QrSPEDEstqCabAnoMes.Value;
  QrSPEDEstqIts.Open;
  //
  QrSPEDEstqIts.Locate('GraGruX; Sit_Prod; Terceiro',
    VarArrayOf([GraGruX, Sit_Prod, Terceiro]), []);
end;

procedure TFmSPEDEstqGer.ReopenSPEDEstqCab(AnoMes: Integer);
begin
  QrSPEDEstqCab.Close;
  if FEmpresa <> 0 then
  begin
    QrSPEDEstqCab.SQL.Clear;
    QrSPEDEstqCab.SQL.Add('SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT,');
    QrSPEDEstqCab.SQL.Add('CONCAT(RIGHT(sec.AnoMes, 2), "/",');
    QrSPEDEstqCab.SQL.Add('LEFT(LPAD(sec.AnoMes, 6, "0"), 4)) MES_ANO, sec.*');
    QrSPEDEstqCab.SQL.Add('FROM spedestqcab sec');
    QrSPEDEstqCab.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=sec.Empresa');
    QrSPEDEstqCab.SQL.Add('WHERE sec.ImporExpor=' + FormatFloat('0', FImporExpor));
    QrSPEDEstqCab.SQL.Add('AND sec.Empresa=' + FEmprTXT);
    QrSPEDEstqCab.SQL.Add('ORDER BY AnoMes DESC');
    QrSPEDEstqCab.Open;
    //
    if AnoMes > 0 then
      QrSPEDEstqCab.Locate('AnoMes', AnoMes, []);
  end;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSPEDEstqGer.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmSPEDEstqGer.BtSaida0Click(Sender: TObject);
begin
  //VAR_CADASTRO := Qr?.Value;
  Close;
end;

function TFmSPEDEstqGer.DefineDataFim(): TDateTime;
var
  Ano, Mes: Word;
  DtStr: String;
begin
  DtStr := FormatFloat('000000', QrSPEDEstqCabAnoMes.Value);
  Ano := Geral.IMV(Copy(DtStr, 1, 4));
  Mes := Geral.IMV(Copy(DtStr, 5, 2));
  Result := EncodeDate(Ano, Mes, 1);
  Result := IncMonth(Result, 1) - 1;
end;

procedure TFmSPEDEstqGer.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmSPEDEstqGer.BtExporta1Click(Sender: TObject);
  procedure ItemNaoExportado(CodMot, TxtMot: String);
  const
    Exportado = 0;
  var
    Controle: Integer;
  begin
    FExportarItem := False;
    Controle := QrSEIControle.Value;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'spedestqits', False, [
      'Exportado'], ['Controle'], [Exportado], [Controle], True);
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stIns, 'listerr1', False, [
    'Codigo', 'Texto1'], ['My_Idx'], [
    CodMot, TxtMot], [Controle], False);
    //
  end;
const
  ICMS_A_Recuperar = 0;
var
Fld001_008,
Fld009_004,
Fld013_004,
Fld017_020,
Fld037_001,
Fld038_014,
Fld052_020,
Fld072_002,
//
Fld074_021,
Fld095_017,
Fld112_017,
Fld129_017,
Fld146_060,
Fld206_080,
Fld286_004,
Fld290_010,
Fld300_030,
Fld330_003,
Fld333_030,
  Linha,
  CNPJ, IE, UF, Terceiro, NomeReduzido, Observacao, NCM, Txt: String;
  //
  //PT,
  SitProd, Exportados: Integer;
  DataFim: TDateTime;
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'spedestqits', False, [
  'Exportado'], ['ImporExpor', 'AnoMes', 'Empresa'], [1],
  [QrSPEDEstqCabImporExpor.Value, QrSPEDEstqCabAnoMes.Value,
  QrSPEDEstqCabEmpresa.Value], True);
  //
  DataFim := DefineDataFim();
  MeExp.Text := '';
  PageControl3.ActivePageIndex := 0;
  Exportados := 0;
  Observacao := '';
  DmProd.FListErr1 := UCriar.RecriaTempTable('ListErr1', DmodG.QrUpdPID1, False);
  DmProd.FLayout001 := UCriar.RecriaTempTable('Layout001', DmodG.QrUpdPID1, False);
  //
  QrSEI.Close;
  QrSEI.Params[00].AsInteger := QrSPEDEstqCabImporExpor.Value;
  QrSEI.Params[01].AsInteger := QrSPEDEstqCabEmpresa.Value;
  QrSEI.Params[02].AsInteger := QrSPEDEstqCabAnoMes.Value;
  QrSEI.Open;
  QrSEI.First;
  while not QrSEI.Eof do
  begin
    FExportarItem := True;
    // Pr�prio X Terceiro:
(*
    PT := 0;
    if QrSEIEmpresa.Value = DModG.QrEmpresasCodigo.Value then PT := 10;
    if QrSEIEntiSitio.Value = DModG.QrEmpresasCodigo.Value then PT := PT + 1;
    //
    case PT of
      // de terceiros com terceiros (inv�lido)
      0: SitProd := 0;
      // de terceiros com a empresa (3)
      1: SitProd := 3;
      // da empresa com terceiros (2)
      10,11:
      begin
        // case (4) em transito, ou (5) inaproveit�vel
        case QrSEISitProd.Value of
          1,2,3: if PT = 10 then SitProd := 2 else SitProd := 1;
          4,5: SitProd := QrSEISitProd.Value;
          else SitProd := -1; // desconhecido
        end;
      end;
      else SitProd := -1; // desconhecido!
    end;
*)
    SitProd := QrSEISit_Prod.Value;
    case SitProd of
      -1: ItemNaoExportado('037.001.-01', 'Produto de terceiro em poder de terceiro');
       0: ItemNaoExportado('037.001.000', 'Situa��o do produto n�o definida (se � pr�prio e com quem est�)');
       1..5: (* Nada *);
       else ItemNaoExportado('037.001.' + FormatFloat('000', Sitprod), 'Situa��o do produto desconhecida');
     end;
    //
    CNPJ := '';
    IE   := '';
    UF   := '';
    if SitProd in ([2,3]) then //  n�o tem terceiro
    begin
      CNPJ := Geral.SoNumero_TT(QrSEICNPJ_CPF.Value);
      IE   := Geral.SoNumero_TT(QrSEIIE.Value);
      case QrSEITipo.Value of
        0: UF := Geral.GetSiglaUF_do_CodigoUF(QrSEIEUF.Value);
        1: UF := Geral.GetSiglaUF_do_CodigoUF(QrSEIPUF.Value);
      end;
      Terceiro := FormatFloat('0', QrSEITerceiro.Value);
      if Length(CNPJ) <> 14 then ItemNaoExportado('038.014.000', 'CNPJ de terceiro (c�digo = ' + Terceiro + ') n�o definido');
      if Length(IE) = 0     then ItemNaoExportado('052.020.000', 'I.E. de terceiro (c�digo = ' + Terceiro + ') n�o definido');
      if Length(UF) <> 2    then ItemNaoExportado('072.002.000', 'UF de terceiro (c�digo = ' + Terceiro + ') n�o definido');
    end;
    if QrSEIGraGruX.Value = 0 then
      ItemNaoExportado('017.020.000', 'Produto sem c�digo de reduzido');
    if QrSEIEstqFimQtd.Value <= 0 then
      ItemNaoExportado('074.021.000', 'Quantidade do item zerado ou negativo');
    if QrSEIEstqFimPrc.Value <= 0 then
    begin
(*
      if QrSEITPC_GCP_TAB.Value = 0 then
      begin
        if QrSEI.FieldByName('TPC_GCP_TAB').AsString = '' then // C�digo nulo
          ItemNaoExportado('095.017.001', 'Pre�o n�o definido na lista')
        else
          ItemNaoExportado('095.017.001', 'Lista de pre�os n�o definida');
      end else
*)
        ItemNaoExportado('095.017.000', 'Valor unit�rio do item zerado ou negativo');
    end;
    //
    NomeReduzido := DmProd.FormaNomeProduto(QrSEINO_PRD.Value, QrSEINO_TAM.Value,
      QrSEINO_COR.Value, QrSEIPrintTam.Value, QrSEIPrintCor.Value);
    //
    if Trim(NomeReduzido) = '' then
      ItemNaoExportado('206.080.000', 'Nome do item n�o definido');
    //
    NCM := Geral.SoNumero_TT(QrSEINCM.Value);
    if Length(NCM) < 7 then
      ItemNaoExportado('290.010.000', 'NCM inv�lido: ' + QrSEINCM.Value);
    //
    if Trim(QrSEISIGLA.Value) = '' then
      ItemNaoExportado('330.003.000', 'Unidade de medida n�o definida');
    //
    Fld001_008 := VeriStr(008, 01, FormatDateTime('ddmmyyyy', DataFim));
    Fld009_004 := VeriStr(004, 02, FormatDateTime('mmyy', DataFim));
    Fld013_004 := VeriStr(004, 03, FormatDateTime('mmyy', DataFim));
    Fld017_020 := VeriStr(020, 04, Geral.CompletaString2(IntToStr(QrSEIGraGruX.Value), ' ', 20, taLeftJustify, False, ''));
    Fld037_001 := VeriStr(001, 05, FormatFloat('0', SitProd(*2011-03-18 QrSEISitProd.Value*)));
    Fld038_014 := VeriStr(014, 06, Geral.CompletaString2(CNPJ, '0', 14, taRightJustify, False, ''));
    Fld052_020 := VeriStr(020, 07, Geral.CompletaString2(IE, ' ', 20, taLeftJustify, False, ''));
    Fld072_002 := VeriStr(002, 08, Geral.CompletaString2(UF, ' ', 2, taLeftJustify, False, ''));
    //            VeriStr(
    Fld074_021 := VeriStr(021, 09, Geral.FTX(QrSEIEstqFimQtd.Value, 15, 6, siPositivo));
    Fld095_017 := VeriStr(017, 10, Geral.FTX(QrSEIEstqFimPrc.Value, 13, 4, siPositivo));
    Fld112_017 := VeriStr(017, 11, Geral.FTX(QrSEIEstqFimVal.Value, 15, 2, siPositivo));
    Fld129_017 := VeriStr(017, 12, Geral.FTX(ICMS_A_Recuperar, 15, 2, siPositivo));
    Fld146_060 := VeriStr(060, 13, Geral.CompletaString2(Observacao, ' ', 60, taLeftJustify, False, ''));
    Fld206_080 := VeriStr(080, 14, Geral.CompletaString2(NomeReduzido, ' ', 80, taLeftJustify, False, ''));
    Fld286_004 := VeriStr(004, 15, Geral.FTX(QrSEIPrdGrupTip.Value, 4, 0, siNegativo));  // por causa dos negativos!
    Fld290_010 := VeriStr(010, 16, Geral.CompletaString2(NCM, '0', 10, taLeftJustify, False, ''));
    Fld300_030 := VeriStr(030, 17, Geral.CompletaString2('', ' ', 30, taLeftJustify, False, ''));
    Fld330_003 := VeriStr(003, 18, Geral.CompletaString2(QrSEISIGLA.Value, ' ', 3, taLeftJustify, True, ''));
    Fld333_030 := VeriStr(030, 19, Geral.CompletaString2(QrSEINO_PGT.Value, ' ', 30, taLeftJustify, True, ''));
    //
    if FExportarItem then
    begin
      Linha :=
      Fld001_008 +
      Fld009_004 +
      Fld013_004 +
      Fld017_020 +
      Fld037_001 +
      Fld038_014 +
      Fld052_020 +
      Fld072_002 +
      //         +
      Fld074_021 +
      Fld095_017 +
      Fld112_017 +
      Fld129_017 +
      Fld146_060 +
      Fld206_080 +
      Fld286_004 +
      Fld290_010 +
      Fld300_030 +
      Fld330_003 +
      Fld333_030;
      if Length(Linha) <> 362 then
        Geral.MensagemBox('A linha ' + IntToStr(MeExp.Lines.Count + 1) +
        ' n�o tem 362 caracteres!' + #13#10 + 'Ela tem ' + IntToStr(Length(Linha)) +
        ' caracteres.', 'Aviso', MB_OK+MB_ICONWARNING);
      MeExp.Lines.Add(Linha);
      //
      UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stIns, 'layout001', False, [
      'DataInvent', 'MesAnoInic', 'MesAnoFina',
      'CodigoProd', 'SituacProd', 'CNPJTercei',
      'IETerceiro', 'UFTerceiro', 'Quantidade',
      'ValorUnita', 'ValorTotal', 'ICMSRecupe',
      'Observacao', 'DescriProd', 'GrupoProdu',
      'ClassifNCM', 'RESERVADOS', 'UnidMedida',
      'DescrGrupo'], [
      ], [
      Fld001_008, Fld009_004, Fld013_004,
      Fld017_020, Fld037_001, Fld038_014,
      Fld052_020, Fld072_002, Fld074_021,
      Fld095_017, Fld112_017, Fld129_017,
      Fld146_060, Fld206_080, Fld286_004,
      Fld290_010, Fld300_030, Fld330_003,
      Fld333_030
      ], [
      ], False);
      Exportados := Exportados + 1;
    end;
    //
    QrSEI.Next;
  end;
  PageControl3.ActivePageIndex := 1;
  Update;
  Application.ProcessMessages;
  QrExport2.Close;
  QrExport2.Params[00].AsInteger := QrSPEDEstqCabImporExpor.Value;
  QrExport2.Params[01].AsInteger := QrSPEDEstqCabEmpresa.Value;
  QrExport2.Params[02].AsInteger := QrSPEDEstqCabAnoMes.Value;
  QrExport2.Open;
  if QrExport2.RecordCount > 0 then
  begin
    PageControl3.ActivePageIndex := 2;
    if Exportados = 0 then
      Txt := 'Nenhum item pesquisado foi exportado'
    else begin
      if Exportados = 1 then
        Txt := 'Um item dos ' + IntToStr(QrSEI.RecordCount) +
        ' pesquisados foi exportado!' + #13#10 +
        'Os demais itens n�o foram exportados'
      else
        Txt := IntToStr(Exportados) + ' itens dos ' +
        IntToStr(QrSEI.RecordCount) + ' pesquisados foram exportados!' +
        #13#10 + 'Os demais itens n�o foram exportados';
    end;
    //
    Txt := Txt + ' por falhas que devem ser corrigidas!';
    Geral.MensagemBox(Txt, 'Aviso', MB_OK+MB_ICONWARNING);
  end;
  //
  QrLayout001.Close;
  QrLayout001.Open;
  //
  if DBCheck.CriaFm(TFmCfgExpFile, FmCfgExpFile, afmoNegarComAviso) then
  begin
    FmCfgExpFile.Memo.WantReturns := MeExp.WantReturns;
    FmCfgExpFile.Memo.WantTabs    := MeExp.WantTabs;
    FmCfgExpFile.Memo.WordWrap    := MeExp.WordWrap;
    FmCfgExpFile.Memo.Text        := MeExp.Text;
    //
    FmCfgExpFile.EdDir.Text       := 'C:\Dermatek\Inventario\' +
                                     FormatDateTime('YYYY', DataFim);
    FmCfgExpFile.EdArq.Text       := Geral.SoNumero_TT(
                                     DModG.QrEmpresasCNPJ_CPF.Value) + '_' +
                                     FormatDateTime('YYYYMM', DataFim);
    FmCfgExpFile.EdExt.Text       := 'txt';
    //
    FmCfgExpFile.ShowModal;
    FmCfgExpFile.Destroy;
  end;
end;

procedure TFmSPEDEstqGer.BtImportaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPEDEstqLoadXLS, FmSPEDEstqLoadXLS, afmoNegarComAviso) then
  begin
    FmSPEDEstqLoadXLS.ShowModal;
    FmSPEDEstqLoadXLS.Destroy;
  end;
end;

procedure TFmSPEDEstqGer.BtImprime0Click(Sender: TObject);
begin
  // Parei aqui! Fazer e usar
  //MyObjects.DesfazOrdenacaoDmkDBGrid(dmkDBGrid: TdmkDBGrid; Query: TmySQLQuery;
  //Campo: String; Valor: Variant)
  QrSPEDEstqIts.Close;
  QrSPEDEstqIts.SortFieldNames := '';
  QrSPEDEstqIts.Open;
  //
  MyObjects.frxMostra(frxSPEDEFD_PRINT_000_01, 'Estoque final em ...');
end;

procedure TFmSPEDEstqGer.BtImprime1Click(Sender: TObject);
begin
  if (PageControl3.ActivePageIndex = 1) then
    MyObjects.frxMostra(frxSPEDEFD_PRINT_001_02, 'Falhas na Exporta��o')
  else
    Geral.MensagemBox('A guia selecionada n�o possui relat�rio!',
    'Informa��o', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmSPEDEstqGer.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmSPEDEstqGer.FormCreate(Sender: TObject);
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FEmprTXT := '';
  PageControl1.ActivePageIndex := 0;
  PageControl1.Align := alClient;
  //
  QrLayout001.Close;
  QrLayout001.Database := DModG.MyPID_DB;
  //
  QrListErr1.Close;
  QrListErr1.Database := DModG.MyPID_DB;
end;

procedure TFmSPEDEstqGer.SbNumeroClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Codigo(QrCadComItensCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSPEDEstqGer.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSPEDEstqGer.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrCadComItensCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmSPEDEstqGer.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmSPEDEstqGer.QrExport2AfterScroll(DataSet: TDataSet);
begin
  QrListErr1.Close;
  QrListErr1.Params[0].AsInteger := QrExport2Controle.Value;
  QrListErr1.Open;
end;

procedure TFmSPEDEstqGer.QrExport2BeforeClose(DataSet: TDataSet);
begin
  QrListErr1.Close;
end;

procedure TFmSPEDEstqGer.QrSPEDEstqCabAfterOpen(DataSet: TDataSet);
begin
  BtInclui.Enabled := True;
  BtExclui.Enabled := True;
  BtAltera.Enabled := True;
end;

procedure TFmSPEDEstqGer.QrSPEDEstqCabAfterScroll(DataSet: TDataSet);
begin
  ReopenSPEDEstqIts(0,0,0);
end;

procedure TFmSPEDEstqGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSPEDEstqGer.SbQueryClick(Sender: TObject);
begin
{
  LocCod(QrCadComItensCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cadcomitens', Dmod.MyDB, CO_VAZIO));
}
end;

function TFmSPEDEstqGer.VeriStr(Tam, Item: Integer; Texto: String): String;
begin
  Result := Texto;
  if Length(Result) <> Tam then
  Geral.MensagemBox('O item com tamanho inv�lido: ' + #13#10 +
  'Item n�mero: ' + FormatFloat('0', Item) + #13#10 +
  'Tamanho esperado: ' + FormatFloat('0', Tam) + #13#10 +
  'Tamanho do texto informado: ' + FormatFloat('0', Length(Result)) + #13#10 +
  'Texto informado: "' + Texto + '"', 'Erro', MB_OK+MB_ICONERROR);
end;

procedure TFmSPEDEstqGer.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmSPEDEstqGer.frxSPEDEFD_PRINT_000_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VFR_EMPRESA' then Value := MLAGeral.CampoReportTxt(CBEmpresa.Text, 'TODAS')
  else if VarName = 'VARF_PERIODO' then Value := QrSPEDEstqCabMES_ANO.Value
  else if VarName = 'VARF_GG2_VISIBLE' then Value := PertenceAoGrupoGG2()
  else if VarName = 'VARF_PGT_VISIBLE' then Value := PertenceAoGrupoPGT()
  else if VarName = 'VARF_PRD_VISIBLE' then Value := PertenceAoGrupoPrd()
  else if VarName = 'DATA_EM_TXT' then Value := Geral.FDT(DefineDataFim(), 2)
{
  else if VarName = 'VFR_NO_PGT' then Value := MLAGeral.CampoReportTxt(CBPrdGrupTip.Text, 'TODOS')
  else if VarName = 'VFR_NO_PRD' then Value := MLAGeral.CampoReportTxt(CBGraGru1.Text, 'TODOS')
  else if VarName = 'DATAHORA_TXT' then Value := Geral.FDT(FAgora, 8)
  else if VarName = 'PERIODO_TXT' then Value := MLAGeral.PeriodoImp(
    TPDataIni1.Date, TPDataFim1.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'VARF_PGT_VISIBLE' then Value := PertenceAoGrupoPGT()
  else if VarName = 'VARF_PRD_VISIBLE' then Value := PertenceAoGrupoPrd()
  else if VarName = 'VFR_LISTA_PRECO' then Value := CBGraCusPrc.Text
  //
  else if VarName = 'PERIODO_TXT_5' then Value := MLAGeral.PeriodoImp(
    TPDataIni5.Date, TPDataFim5.Date, 0, 0, True, True, False, False, '', '')
  else if VarName = 'VFR_FILTRO_5' then
  begin
   Value := 'TUDO';
    case CBFiltro5.Value of
      //0: CBFiltro5.SetMaxValue; // Tudo
      1: Value := 'Baixa correta';
      2: Value := 'Sem baixa';
      3: Value := 'Baixa correta ou sem baixa';
      4: Value := 'Erro baixa';
      5: Value := 'Baixa correta ou incorreta';
      6: Value := 'Baixa incorreta ou sem baixa';
      7: ; //Tudo
    end;
  end;
}
end;

procedure TFmSPEDEstqGer.QrSPEDEstqCabBeforeClose(DataSet: TDataSet);
begin
  BtInclui.Enabled := False;
  BtExclui.Enabled := False;
  BtAltera.Enabled := False;
  //
  QrSPEDEstqIts.Close;
end;

procedure TFmSPEDEstqGer.QrSPEDEstqCabCalcFields(DataSet: TDataSet);
var
  Ano_Mes: String;
begin
  Ano_Mes := FormatFloat('000000', QrSPEDEstqCabAnoMes.Value);
  QrSPEDEstqCabMES_ANO.Value :=
    Copy(Ano_mes, 5) + '/' + Copy(Ano_mes, 1, 4);
end;

procedure TFmSPEDEstqGer.QrSPEDEstqItsAfterOpen(DataSet: TDataSet);
begin
  BtExporta1.Enabled := QrSPEDEstqIts.RecordCount > 0;
end;

procedure TFmSPEDEstqGer.QrSPEDEstqItsBeforeClose(DataSet: TDataSet);
begin
  BtExporta1.Enabled := False;
end;

end.

