unit SPEDEstqIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, dmkRadioGroup, UnDmkEnums;

type
  TFmSPEDEstqIts = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrProdutos: TmySQLQuery;
    DsProdutos: TDataSource;
    LaTipo: TdmkLabel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    GroupBox3: TGroupBox;
    DBEdEmpresa: TdmkDBEdit;
    DBEdit4: TDBEdit;
    Label13: TLabel;
    DBEdit5: TDBEdit;
    QrProdutosControle: TIntegerField;
    QrProdutosNO_PRD_TAM_COR: TWideStringField;
    Label1: TLabel;
    DBEdImporExport: TdmkDBEdit;
    Label3: TLabel;
    DBEdAnoMes: TdmkDBEdit;
    RGSit_Prod: TdmkRadioGroup;
    Panel3: TPanel;
    Label2: TLabel;
    EdControle: TdmkEdit;
    EdProduto: TdmkEditCB;
    Label9: TLabel;
    CBProduto: TdmkDBLookupComboBox;
    PnTerceiro: TPanel;
    Label4: TLabel;
    EdTerceiro: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    Panel4: TPanel;
    DsTerceiros: TDataSource;
    QrTerceiros: TmySQLQuery;
    QrTerceirosCodigo: TIntegerField;
    QrTerceirosNO_ENT: TWideStringField;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    EdEstqIniQtd: TdmkEdit;
    Label8: TLabel;
    EdEstqIniVal: TdmkEdit;
    GroupBox4: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    EdComprasQtd: TdmkEdit;
    EdComprasVal: TdmkEdit;
    GroupBox5: TGroupBox;
    Label12: TLabel;
    Label14: TLabel;
    EdConsumoQtd: TdmkEdit;
    EdConsumoVal: TdmkEdit;
    GroupBox6: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    EdEstqFimQtd: TdmkEdit;
    EdEstqFimVal: TdmkEdit;
    EdEstqFimPrc: TdmkEdit;
    Label17: TLabel;
    Label18: TLabel;
    EdEstqIniPrc: TdmkEdit;
    EdComprasPrc: TdmkEdit;
    Label19: TLabel;
    EdConsumoPrc: TdmkEdit;
    Label20: TLabel;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    QrProdutosSIGLA: TWideStringField;
    QrProdutosNO_MED: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGSit_ProdClick(Sender: TObject);
    procedure EdEstqFimPrcEnter(Sender: TObject);
    procedure EdEstqFimPrcExit(Sender: TObject);
    procedure EdEstqFimValEnter(Sender: TObject);
    procedure EdEstqFimValExit(Sender: TObject);
    procedure EdEstqIniPrcEnter(Sender: TObject);
    procedure EdComprasPrcEnter(Sender: TObject);
    procedure EdConsumoPrcEnter(Sender: TObject);
    procedure EdEstqIniPrcExit(Sender: TObject);
    procedure EdComprasPrcExit(Sender: TObject);
    procedure EdConsumoPrcExit(Sender: TObject);
    procedure EdEstqIniValEnter(Sender: TObject);
    procedure EdComprasValEnter(Sender: TObject);
    procedure EdConsumoValEnter(Sender: TObject);
    procedure EdEstqIniValExit(Sender: TObject);
    procedure EdComprasValExit(Sender: TObject);
    procedure EdConsumoValExit(Sender: TObject);
    procedure EdEstqIniQtdExit(Sender: TObject);
    procedure EdEstqIniQtdChange(Sender: TObject);
    procedure EdComprasQtdChange(Sender: TObject);
    procedure EdConsumoQtdChange(Sender: TObject);
    procedure EdEstqFimQtdChange(Sender: TObject);
    procedure EdComprasQtdExit(Sender: TObject);
    procedure EdConsumoQtdExit(Sender: TObject);
    procedure EdEstqFimPrcChange(Sender: TObject);
  private
    { Private declarations }
    FValor: Int64;
    procedure DefineEstqQtdFim();
  public
    { Public declarations }
  end;

  var
  FmSPEDEstqIts: TFmSPEDEstqIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, SPEDEstqGer;

{$R *.DFM}

procedure TFmSPEDEstqIts.BtOKClick(Sender: TObject);
var
  Controle: Integer;
  PrecTerc: Boolean;
begin
  if MyObjects.FIC(EdProduto.ValueVariant = 0, EdProduto, 'Informe o produto!')
    then Exit;
  if MyObjects.FIC(RGSit_Prod.ItemIndex = 0, RGSit_Prod, 'Informe a situa��o do item!')
    then Exit;
  PrecTerc := PnTerceiro.Enabled and (EdTerceiro.ValueVariant = 0);
  if MyObjects.FIC(PrecTerc, EdTerceiro, 'Informe o Terceiro!')
    then Exit;
  Controle := UMyMod.BuscaEmLivreY_Def_SPED_EFD('spedestqits', 'Controle',
    LaTipo.SQLType, EdControle.ValueVariant);
  //
  if UMyMod.ExecSQLInsUpdFm(Self, LaTipo.SQLType, 'spedestqits',
  Controle, Dmod.QrUpd) then
  begin
    FmSPEDEstqGer.ReopenSPEDEstqIts(EdProduto.ValueVariant,
      RGSit_Prod.ItemIndex, EdTerceiro.ValueVariant);
    if CkContinuar.Checked then
    begin
      Latipo.SQLType            := stIns;
      EdControle.ValueVariant   := 0;
      EdProduto.ValueVariant    := 0;
      CBProduto.KeyValue        := Null;
      EdTerceiro.ValueVariant   := 0;
      CBTerceiro.KeyValue       := Null;
      RGSit_Prod.ItemIndex      := 1;
      EdEstqIniQtd.ValueVariant := 0;
      EdEstqIniPrc.ValueVariant := 0;
      EdEstqIniVal.ValueVariant := 0;
      EdComprasQtd.ValueVariant := 0;
      EdComprasPrc.ValueVariant := 0;
      EdComprasVal.ValueVariant := 0;
      EdConsumoQtd.ValueVariant := 0;
      EdConsumoPrc.ValueVariant := 0;
      EdConsumoVal.ValueVariant := 0;
      EdEstqFimQtd.ValueVariant := 0;
      EdEstqFimPrc.ValueVariant := 0;
      EdEstqFimVal.ValueVariant := 0;
      //
      EdProduto.SetFocus;
    end else Close;
  end;
end;

procedure TFmSPEDEstqIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSPEDEstqIts.DefineEstqQtdFim();
begin
  EdEstqFimQtd.ValueVariant :=
  EdEstqIniQtd.ValueVariant +
  EdComprasQtd.ValueVariant -
  EdConsumoQtd.ValueVariant;
end;

procedure TFmSPEDEstqIts.EdComprasPrcEnter(Sender: TObject);
begin
  FValor := Trunc(EdComprasPrc.ValueVariant * 10000);
end;

procedure TFmSPEDEstqIts.EdComprasPrcExit(Sender: TObject);
begin
  if FValor <> Trunc(EdComprasPrc.ValueVariant * 10000) then
    EdComprasVal.ValueVariant :=
    EdComprasQtd.ValueVariant *
    EdComprasPrc.ValueVariant;
end;

procedure TFmSPEDEstqIts.EdComprasQtdChange(Sender: TObject);
begin
  EdComprasVal.ValueVariant :=
  EdComprasPrc.ValueVariant *
  EdComprasQtd.ValueVariant;
end;

procedure TFmSPEDEstqIts.EdComprasQtdExit(Sender: TObject);
begin
  DefineEstqQtdFim();
end;

procedure TFmSPEDEstqIts.EdComprasValEnter(Sender: TObject);
begin
  FValor := Trunc(EdComprasVal.ValueVariant * 100);
end;

procedure TFmSPEDEstqIts.EdComprasValExit(Sender: TObject);
begin
  if FValor <> Trunc(EdComprasVal.ValueVariant * 100) then
  begin
    if EdComprasQtd.ValueVariant = 0 then
      EdComprasPrc.ValueVariant := 0
    else
      EdComprasPrc.ValueVariant :=
      EdComprasVal.ValueVariant /
      EdComprasQtd.ValueVariant;
  end;
end;

procedure TFmSPEDEstqIts.EdConsumoPrcEnter(Sender: TObject);
begin
  FValor := Trunc(EdConsumoPrc.ValueVariant * 10000);
end;

procedure TFmSPEDEstqIts.EdConsumoPrcExit(Sender: TObject);
begin
  if FValor <> Trunc(EdConsumoPrc.ValueVariant * 10000) then
    EdConsumoVal.ValueVariant :=
    EdConsumoQtd.ValueVariant *
    EdConsumoPrc.ValueVariant;
end;

procedure TFmSPEDEstqIts.EdConsumoQtdChange(Sender: TObject);
begin
  EdConsumoVal.ValueVariant :=
  EdConsumoPrc.ValueVariant *
  EdConsumoQtd.ValueVariant;
end;

procedure TFmSPEDEstqIts.EdConsumoQtdExit(Sender: TObject);
begin
  DefineEstqQtdFim();
end;

procedure TFmSPEDEstqIts.EdConsumoValEnter(Sender: TObject);
begin
  FValor := Trunc(EdConsumoVal.ValueVariant * 100);
end;

procedure TFmSPEDEstqIts.EdConsumoValExit(Sender: TObject);
begin
  if FValor <> Trunc(EdConsumoVal.ValueVariant * 100) then
  begin
    if EdConsumoQtd.ValueVariant = 0 then
      EdConsumoPrc.ValueVariant := 0
    else
      EdConsumoPrc.ValueVariant :=
      EdConsumoVal.ValueVariant /
      EdConsumoQtd.ValueVariant;
  end;
end;

procedure TFmSPEDEstqIts.EdEstqFimPrcChange(Sender: TObject);
begin
  if EdEstqFimPrc.ValueVariant > 0.0001 then
  begin
    if EdEstqFimVal.ValueVariant < 0.01 then
      EdEstqFimQtdChange(Self);
  end;
end;

procedure TFmSPEDEstqIts.EdEstqFimPrcEnter(Sender: TObject);
begin
  FValor := Trunc(EdEstqFimPrc.ValueVariant * 10000);
end;

procedure TFmSPEDEstqIts.EdEstqFimPrcExit(Sender: TObject);
begin
  if FValor <> Trunc(EdEstqFimPrc.ValueVariant * 10000) then
    EdEstqFimVal.ValueVariant :=
    EdEstqFimQtd.ValueVariant *
    EdEstqFimPrc.ValueVariant;
end;

procedure TFmSPEDEstqIts.EdEstqFimQtdChange(Sender: TObject);
begin
  EdEstqFimVal.ValueVariant :=
  EdEstqFimPrc.ValueVariant *
  EdEstqFimQtd.ValueVariant;
end;

procedure TFmSPEDEstqIts.EdEstqFimValEnter(Sender: TObject);
begin
  FValor := Trunc(EdEstqFimVal.ValueVariant * 100);
end;

procedure TFmSPEDEstqIts.EdEstqFimValExit(Sender: TObject);
begin
  if FValor <> Trunc(EdEstqFimVal.ValueVariant * 100) then
  begin
    if EdEstqFimQtd.ValueVariant = 0 then
      EdEstqFimPrc.ValueVariant := 0
    else
      EdEstqFimPrc.ValueVariant :=
      EdEstqFimVal.ValueVariant /
      EdEstqFimQtd.ValueVariant;
  end;
end;

procedure TFmSPEDEstqIts.EdEstqIniPrcEnter(Sender: TObject);
begin
  FValor := Trunc(EdEstqIniPrc.ValueVariant * 10000);
end;

procedure TFmSPEDEstqIts.EdEstqIniPrcExit(Sender: TObject);
begin
  if FValor <> Trunc(EdEstqIniPrc.ValueVariant * 10000) then
  begin
    EdEstqIniVal.ValueVariant :=
    EdEstqIniQtd.ValueVariant *
    EdEstqIniPrc.ValueVariant;
    if EdEstqIniPrc.ValueVariant >= 0.0001 then
    begin
      if EdComprasPrc.ValueVariant < 0.0001 then
        EdComprasPrc.ValueVariant := EdEstqIniPrc.ValueVariant;
      if EdConsumoPrc.ValueVariant < 0.0001 then
        EdConsumoPrc.ValueVariant := EdEstqIniPrc.ValueVariant;
      if EdEstqFimPrc.ValueVariant < 0.0001 then
        EdEstqFimPrc.ValueVariant := EdEstqIniPrc.ValueVariant;
    end;
  end;
end;

procedure TFmSPEDEstqIts.EdEstqIniQtdChange(Sender: TObject);
begin
  EdEstqIniVal.ValueVariant :=
  EdEstqIniPrc.ValueVariant *
  EdEstqIniQtd.ValueVariant;
end;

procedure TFmSPEDEstqIts.EdEstqIniQtdExit(Sender: TObject);
begin
  DefineEstqQtdFim();
end;

procedure TFmSPEDEstqIts.EdEstqIniValEnter(Sender: TObject);
begin
  FValor := Trunc(EdEstqIniVal.ValueVariant * 100);
end;

procedure TFmSPEDEstqIts.EdEstqIniValExit(Sender: TObject);
begin
  if FValor <> Trunc(EdEstqIniVal.ValueVariant * 100) then
  begin
    if EdEstqIniQtd.ValueVariant = 0 then
      EdEstqIniPrc.ValueVariant := 0
    else
      EdEstqIniPrc.ValueVariant :=
      EdEstqIniVal.ValueVariant /
      EdEstqIniQtd.ValueVariant;
  end;
end;

procedure TFmSPEDEstqIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSPEDEstqIts.FormCreate(Sender: TObject);
begin
  QrProdutos.Open;
  QrTerceiros.Open;
end;

procedure TFmSPEDEstqIts.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmSPEDEstqIts.RGSit_ProdClick(Sender: TObject);
begin
  PnTerceiro.Enabled := RGSit_prod.ItemIndex in ([2,3]);
  if not PnTerceiro.Enabled then
  begin
    EdTerceiro.ValueVariant := 0;
    CBTerceiro.KeyValue := NULL;
  end;
end;

end.
