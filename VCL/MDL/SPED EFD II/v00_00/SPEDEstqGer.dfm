object FmSPEDEstqGer: TFmSPEDEstqGer
  Left = 368
  Top = 194
  Caption = 'SPE-D_STQ-001 :: Confere Estoque SPED'
  ClientHeight = 692
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 644
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 1006
        Height = 48
        Align = alClient
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 23
          Height = 13
          Caption = 'Filial:'
        end
        object Label2: TLabel
          Left = 932
          Top = 4
          Width = 60
          Height = 13
          Caption = 'M'#202'S / ANO:'
          Enabled = False
          FocusControl = DBEdit1
        end
        object EdEmpresa: TdmkEditCB
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 64
          Top = 20
          Width = 865
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object DBEdit1: TDBEdit
          Left = 932
          Top = 20
          Width = 65
          Height = 21
          DataField = 'MES_ANO'
          DataSource = DsSPEDEstqCab
          Enabled = False
          TabOrder = 2
        end
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 49
      Width = 1006
      Height = 594
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 1
      object TabSheet2: TTabSheet
        Caption = ' Per'#237'odos '
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 998
          Height = 566
          Align = alClient
          TabOrder = 0
          object Panel3: TPanel
            Left = 1
            Top = 521
            Width = 996
            Height = 44
            Align = alBottom
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object BtExclui: TBitBtn
              Tag = 12
              Left = 196
              Top = 2
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Exclui'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtExcluiClick
            end
            object BtAltera: TBitBtn
              Tag = 11
              Left = 104
              Top = 2
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Altera'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtAlteraClick
            end
            object BtInclui: TBitBtn
              Tag = 10
              Left = 12
              Top = 2
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Inclui'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtIncluiClick
            end
            object Panel2: TPanel
              Left = 887
              Top = 0
              Width = 109
              Height = 44
              Align = alRight
              Alignment = taRightJustify
              BevelOuter = bvNone
              TabOrder = 3
              object BtSaida0: TBitBtn
                Tag = 13
                Left = 4
                Top = 2
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Sa'#237'da'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtSaida0Click
              end
            end
            object BtImprime0: TBitBtn
              Tag = 5
              Left = 380
              Top = 2
              Width = 90
              Height = 40
              Caption = 'Im&prime'
              NumGlyphs = 2
              TabOrder = 4
              OnClick = BtImprime0Click
            end
            object BtImporta: TBitBtn
              Tag = 10074
              Left = 597
              Top = 2
              Width = 90
              Height = 40
              Caption = 'I&mporta'
              TabOrder = 5
              OnClick = BtImportaClick
            end
          end
          object Panel9: TPanel
            Left = 103
            Top = 1
            Width = 894
            Height = 520
            Align = alClient
            ParentBackground = False
            TabOrder = 1
            object DBGIts: TdmkDBGrid
              Left = 1
              Top = 1
              Width = 892
              Height = 518
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Sit_Prod'
                  Title.Caption = 'Sit'
                  Width = 20
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Terceiro'
                  Width = 46
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PrdGrupTip'
                  Title.Caption = 'TGP'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PGT'
                  Title.Caption = 'Nome PGT'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nivel2'
                  Title.Caption = 'Nivel 2'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_GG2'
                  Title.Caption = 'Descri'#231#227'o Nivel 2'
                  Width = 115
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PRD'
                  Title.Caption = 'Nome do produto (Nivel 1)'
                  Width = 219
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SIGLA'
                  Title.Caption = 'Unid.'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqFimQtd'
                  Title.Caption = 'Qtd final'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqFimPrc'
                  Title.Caption = 'V. uni. final'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqFimVal'
                  Title.Caption = 'Val. final'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ConsumoQtd'
                  Title.Caption = 'Qtd. uso'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ConsumoPrc'
                  Title.Caption = 'V. uni. uso'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ConsumoVal'
                  Title.Caption = 'Val. uso'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ComprasQtd'
                  Title.Caption = 'Qtd. compra'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ComprasPrc'
                  Title.Caption = 'V. uni. compras'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ComprasVal'
                  Title.Caption = 'Val. compras'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqIniQtd'
                  Title.Caption = 'Qtd Ini'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqIniPrc'
                  Title.Caption = 'V. uni. ini.'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqIniVal'
                  Title.Caption = 'Val. estq.'
                  Width = 56
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsSPEDEstqIts
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Sit_Prod'
                  Title.Caption = 'Sit'
                  Width = 20
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Terceiro'
                  Width = 46
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PrdGrupTip'
                  Title.Caption = 'TGP'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PGT'
                  Title.Caption = 'Nome PGT'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nivel2'
                  Title.Caption = 'Nivel 2'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_GG2'
                  Title.Caption = 'Descri'#231#227'o Nivel 2'
                  Width = 115
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PRD'
                  Title.Caption = 'Nome do produto (Nivel 1)'
                  Width = 219
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SIGLA'
                  Title.Caption = 'Unid.'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqFimQtd'
                  Title.Caption = 'Qtd final'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqFimPrc'
                  Title.Caption = 'V. uni. final'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqFimVal'
                  Title.Caption = 'Val. final'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ConsumoQtd'
                  Title.Caption = 'Qtd. uso'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ConsumoPrc'
                  Title.Caption = 'V. uni. uso'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ConsumoVal'
                  Title.Caption = 'Val. uso'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ComprasQtd'
                  Title.Caption = 'Qtd. compra'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ComprasPrc'
                  Title.Caption = 'V. uni. compras'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ComprasVal'
                  Title.Caption = 'Val. compras'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqIniQtd'
                  Title.Caption = 'Qtd Ini'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqIniPrc'
                  Title.Caption = 'V. uni. ini.'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EstqIniVal'
                  Title.Caption = 'Val. estq.'
                  Width = 56
                  Visible = True
                end>
            end
          end
          object Panel10: TPanel
            Left = 1
            Top = 1
            Width = 102
            Height = 520
            Align = alLeft
            ParentBackground = False
            TabOrder = 2
            object DBGCab: TdmkDBGrid
              Left = 1
              Top = 1
              Width = 100
              Height = 419
              Align = alClient
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'MES_ANO'
                  Title.Caption = 'MES / ANO'
                  Width = 63
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsSPEDEstqCab
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'MES_ANO'
                  Title.Caption = 'MES / ANO'
                  Width = 63
                  Visible = True
                end>
            end
            object GroupBox1: TGroupBox
              Left = 1
              Top = 420
              Width = 100
              Height = 99
              Align = alBottom
              Caption = ' Impress'#227'o: '
              TabOrder = 1
              object CGAgrup0: TdmkCheckGroup
                Left = 2
                Top = 15
                Width = 96
                Height = 82
                Align = alClient
                Caption = ' Agrupamentos: '
                Items.Strings = (
                  'Produtos'
                  'Nivel 2'
                  'Tipos de prod.')
                TabOrder = 0
                UpdType = utYes
                Value = 0
                OldValor = 0
              end
            end
          end
        end
      end
      object TabSheet1: TTabSheet
        Caption = ' Exporta Prosoft '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 998
          Height = 566
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Panel1: TPanel
            Left = 1
            Top = 521
            Width = 996
            Height = 44
            Align = alBottom
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel4: TPanel
              Left = 887
              Top = 0
              Width = 109
              Height = 44
              Align = alRight
              Alignment = taRightJustify
              BevelOuter = bvNone
              TabOrder = 0
              object BtSaida1: TBitBtn
                Tag = 13
                Left = 4
                Top = 3
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Sa'#237'da'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtSaida0Click
              end
            end
            object BtExporta1: TBitBtn
              Tag = 5
              Left = 288
              Top = 3
              Width = 90
              Height = 40
              Caption = '&Exporta'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtExporta1Click
            end
            object BtImprime1: TBitBtn
              Tag = 5
              Left = 380
              Top = 3
              Width = 90
              Height = 40
              Caption = '&Imprime'
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BtImprime1Click
            end
          end
          object Panel8: TPanel
            Left = 1
            Top = 1
            Width = 996
            Height = 520
            Align = alClient
            TabOrder = 1
            object PageControl3: TPageControl
              Left = 1
              Top = 1
              Width = 994
              Height = 518
              ActivePage = TabSheet10
              Align = alClient
              TabOrder = 0
              object TabSheet10: TTabSheet
                Caption = ' Texto de Exporta'#231#227'o '
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object MeExp: TMemo
                  Left = 0
                  Top = 0
                  Width = 986
                  Height = 490
                  Align = alClient
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                  WordWrap = False
                end
              end
              object TabSheet11: TTabSheet
                Caption = ' Falhas na exporta'#231#227'o '
                ImageIndex = 2
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGrid4: TdmkDBGrid
                  Left = 0
                  Top = 0
                  Width = 572
                  Height = 490
                  Align = alClient
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'My_Idx'
                      Title.Caption = #205'ndice'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Empresa'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'EntiSitio'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Reduzido'
                      Width = 54
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PGT'
                      Title.Caption = 'Tipo de Produto'
                      Width = 160
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QTDE'
                      Title.Caption = 'Quantidade'
                      Width = 73
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PrcCusUni'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorTot'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SIGLA'
                      Title.Caption = 'Sigla'
                      Width = 42
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PRD'
                      Title.Caption = 'Produto'
                      Width = 223
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_TAM'
                      Title.Caption = 'Tam.'
                      Width = 42
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_COR'
                      Title.Caption = 'Cor'
                      Width = 100
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PECAS'
                      Title.Caption = 'Pe'#231'as'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AREAM2'
                      Title.Caption = #193'rea m'#178
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AREAP2'
                      Title.Caption = #193'rea ft'#178
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PESO'
                      Title.Caption = 'Peso kg'
                      Visible = True
                    end>
                  Color = clWindow
                  DataSource = DsExport2
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'My_Idx'
                      Title.Caption = #205'ndice'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Empresa'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'EntiSitio'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Reduzido'
                      Width = 54
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PGT'
                      Title.Caption = 'Tipo de Produto'
                      Width = 160
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'QTDE'
                      Title.Caption = 'Quantidade'
                      Width = 73
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PrcCusUni'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorTot'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SIGLA'
                      Title.Caption = 'Sigla'
                      Width = 42
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PRD'
                      Title.Caption = 'Produto'
                      Width = 223
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_TAM'
                      Title.Caption = 'Tam.'
                      Width = 42
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_COR'
                      Title.Caption = 'Cor'
                      Width = 100
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PECAS'
                      Title.Caption = 'Pe'#231'as'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AREAM2'
                      Title.Caption = #193'rea m'#178
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AREAP2'
                      Title.Caption = #193'rea ft'#178
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PESO'
                      Title.Caption = 'Peso kg'
                      Visible = True
                    end>
                end
                object DBGrid2: TDBGrid
                  Left = 572
                  Top = 0
                  Width = 414
                  Height = 490
                  Align = alRight
                  DataSource = DsListErr1
                  TabOrder = 1
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Texto1'
                      Width = 300
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Width = 76
                      Visible = True
                    end>
                end
              end
              object TabSheet13: TTabSheet
                Caption = ' Campos exporta'#231#227'o '
                ImageIndex = 3
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object DBGrid3: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 986
                  Height = 490
                  Align = alClient
                  DataSource = DsLayout001
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'DataInvent'
                      Title.Caption = 'Dta.Invent.'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MesAnoInic'
                      Title.Caption = 'Mes I.'
                      Width = 32
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MesAnoInic'
                      Title.Caption = 'Mes F.'
                      Width = 32
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CodigoProd'
                      Title.Caption = 'Codigo do Produto'
                      Width = 128
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SituacProd'
                      Title.Caption = 'S'
                      Width = 14
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CNPJTercei'
                      Title.Caption = 'CNPJ Terceiro'
                      Width = 92
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'IETerceiro'
                      Title.Caption = 'I.E. Terceiro'
                      Width = 128
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'UFTerceiro'
                      Title.Caption = 'UF Terceiro'
                      Width = 20
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Quantidade'
                      Width = 134
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorUnita'
                      Title.Caption = 'Valor unit'#225'rio'
                      Width = 110
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValorTotal'
                      Title.Caption = 'Valor Total'
                      Width = 110
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ICMSRecupe'
                      Title.Caption = 'ICMS a recuperar'
                      Width = 110
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Observacao'
                      Title.Caption = 'Observa'#231#227'o'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DescriProd'
                      Title.Caption = 'Descri'#231#227'o do produto'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GrupoProdu'
                      Title.Caption = 'Grupo'
                      Width = 32
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ClassifNCM'
                      Title.Caption = 'Classif. NCM'
                      Width = 68
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'RESERVADOS'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'UnidMedida'
                      Title.Caption = 'Unidade'
                      Width = 32
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DescrGrupo'
                      Title.Caption = 'Descri'#231#227'o do Grupo'
                      Width = 248
                      Visible = True
                    end>
                end
              end
            end
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Confere Estoque SPED'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 699
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PnPesq: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsSPEDEstqCab: TDataSource
    DataSet = QrSPEDEstqCab
    Left = 40
    Top = 12
  end
  object QrSPEDEstqCab: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSPEDEstqCabAfterOpen
    BeforeClose = QrSPEDEstqCabBeforeClose
    AfterScroll = QrSPEDEstqCabAfterScroll
    OnCalcFields = QrSPEDEstqCabCalcFields
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NO_ENT, sec.*,'
      
        'CONCAT(RIGHT(sec.AnoMes, 2), '#39'/'#39', LEFT(LPAD(sec.AnoMes, 6, "0"),' +
        ' 4)) MES_ANO'
      'FROM spedestqcab sec'
      'LEFT JOIN entidades ent ON ent.Codigo=sec.Empresa')
    Left = 12
    Top = 12
    object QrSPEDEstqCabImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrSPEDEstqCabAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrSPEDEstqCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSPEDEstqCabEncerrado: TSmallintField
      FieldName = 'Encerrado'
    end
    object QrSPEDEstqCabMES_ANO: TWideStringField
      FieldName = 'MES_ANO'
      Required = True
      Size = 7
    end
    object QrSPEDEstqCabNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 68
    Top = 12
  end
  object PMInclui: TPopupMenu
    OnPopup = PMIncluiPopup
    Left = 36
    Top = 608
    object NovoPerodo1: TMenuItem
      Caption = 'Novo &Per'#237'odo'
      OnClick = NovoPerodo1Click
    end
    object Itemns1: TMenuItem
      Caption = '&Item(ns)'
      Enabled = False
      OnClick = Itemns1Click
    end
  end
  object QrSPEDEstqIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSPEDEstqItsAfterOpen
    BeforeClose = QrSPEDEstqItsBeforeClose
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad, '
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, gg1.Nivel2, '
      'gg2.Nome NO_GG2, sei.* '
      'FROM spedestqits sei'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=sei.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      ''
      'WHERE sei.ImporExpor=:P0'
      'AND sei.Empresa=:P1'
      'AND sei.AnoMes=:P2'
      ''
      ''
      'ORDER BY NO_PGT, gg1.PrdGrupTip, gg1.Nivel3, NO_GG2, gg1.Nivel2,'
      '                   NO_PRD, NO_TAM, NO_COR, sei.GraGruX'
      ''
      '                   ')
    Left = 96
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSPEDEstqItsNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrSPEDEstqItsNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 120
    end
    object QrSPEDEstqItsPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrSPEDEstqItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrSPEDEstqItsNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrSPEDEstqItsSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrSPEDEstqItsNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrSPEDEstqItsNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrSPEDEstqItsGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrSPEDEstqItsGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrSPEDEstqItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrSPEDEstqItsGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrSPEDEstqItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSPEDEstqItsImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrSPEDEstqItsAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrSPEDEstqItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSPEDEstqItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSPEDEstqItsSit_Prod: TSmallintField
      FieldName = 'Sit_Prod'
    end
    object QrSPEDEstqItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
      DisplayFormat = '0;-0; '
    end
    object QrSPEDEstqItsEstqIniQtd: TFloatField
      FieldName = 'EstqIniQtd'
    end
    object QrSPEDEstqItsEstqIniPrc: TFloatField
      FieldName = 'EstqIniPrc'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrSPEDEstqItsEstqIniVal: TFloatField
      FieldName = 'EstqIniVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSPEDEstqItsComprasQtd: TFloatField
      FieldName = 'ComprasQtd'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrSPEDEstqItsComprasPrc: TFloatField
      FieldName = 'ComprasPrc'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrSPEDEstqItsComprasVal: TFloatField
      FieldName = 'ComprasVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSPEDEstqItsConsumoQtd: TFloatField
      FieldName = 'ConsumoQtd'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrSPEDEstqItsConsumoPrc: TFloatField
      FieldName = 'ConsumoPrc'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrSPEDEstqItsConsumoVal: TFloatField
      FieldName = 'ConsumoVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSPEDEstqItsEstqFimQtd: TFloatField
      FieldName = 'EstqFimQtd'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrSPEDEstqItsEstqFimPrc: TFloatField
      FieldName = 'EstqFimPrc'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrSPEDEstqItsEstqFimVal: TFloatField
      FieldName = 'EstqFimVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSPEDEstqItsNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrSPEDEstqItsNO_GG2: TWideStringField
      FieldName = 'NO_GG2'
      Size = 30
    end
    object QrSPEDEstqItsExportado: TSmallintField
      FieldName = 'Exportado'
    end
  end
  object DsSPEDEstqIts: TDataSource
    DataSet = QrSPEDEstqIts
    Left = 124
    Top = 12
  end
  object PMAltera: TPopupMenu
    OnPopup = PMAlteraPopup
    Left = 132
    Top = 604
    object ItemSelecionado1: TMenuItem
      Caption = '&Item Selecionado'
      Enabled = False
      OnClick = ItemSelecionado1Click
    end
  end
  object PMExclui: TPopupMenu
    OnPopup = PMExcluiPopup
    Left = 232
    Top = 604
    object Periodoselecionado1: TMenuItem
      Caption = '&Periodo selecionado'
      OnClick = Periodoselecionado1Click
    end
    object ItemSelecionado2: TMenuItem
      Caption = '&Item(s)'
      OnClick = ItemSelecionado2Click
    end
  end
  object QrSEI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo, IE, EUF, PUF, '
      'IF(trc.Tipo=0, RazaoSocial, trc.Nome) NO_ENT,'
      'IF(trc.Tipo=0, CNPJ, trc.CPF) CNPJ_CPF,'
      'gti.PrintTam, gcc.PrintCor, gg1.NCM, '
      ''
      'gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,'
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, sei.*'
      'FROM spedestqits sei'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=sei.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'LEFT JOIN entidades  trc ON trc.Codigo=sei.Terceiro'
      'WHERE (EstqFimQtd > 0 OR EstqFimVal > 0)'
      ''
      'AND sei.ImporExpor=:P0'
      'AND sei.Empresa=:P1'
      'AND sei.AnoMes=:P2'
      'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, sei.GraGruX')
    Left = 152
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSEINivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrSEINO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 120
    end
    object QrSEIPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrSEIUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrSEINO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrSEISIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrSEINO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrSEINO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrSEIGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrSEIGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrSEIGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrSEIGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrSEIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSEIImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrSEIAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrSEIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSEIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSEISit_Prod: TSmallintField
      FieldName = 'Sit_Prod'
    end
    object QrSEITerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrSEIEstqIniQtd: TFloatField
      FieldName = 'EstqIniQtd'
    end
    object QrSEIEstqIniPrc: TFloatField
      FieldName = 'EstqIniPrc'
    end
    object QrSEIEstqIniVal: TFloatField
      FieldName = 'EstqIniVal'
    end
    object QrSEIComprasQtd: TFloatField
      FieldName = 'ComprasQtd'
    end
    object QrSEIComprasPrc: TFloatField
      FieldName = 'ComprasPrc'
    end
    object QrSEIComprasVal: TFloatField
      FieldName = 'ComprasVal'
    end
    object QrSEIConsumoQtd: TFloatField
      FieldName = 'ConsumoQtd'
    end
    object QrSEIConsumoPrc: TFloatField
      FieldName = 'ConsumoPrc'
    end
    object QrSEIConsumoVal: TFloatField
      FieldName = 'ConsumoVal'
    end
    object QrSEIEstqFimQtd: TFloatField
      FieldName = 'EstqFimQtd'
    end
    object QrSEIEstqFimPrc: TFloatField
      FieldName = 'EstqFimPrc'
    end
    object QrSEIEstqFimVal: TFloatField
      FieldName = 'EstqFimVal'
    end
    object QrSEINO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrSEICNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrSEIIE: TWideStringField
      FieldName = 'IE'
    end
    object QrSEIEUF: TSmallintField
      FieldName = 'EUF'
    end
    object QrSEIPUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrSEITipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrSEIPrintTam: TSmallintField
      FieldName = 'PrintTam'
    end
    object QrSEIPrintCor: TSmallintField
      FieldName = 'PrintCor'
    end
    object QrSEINCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
  end
  object QrExport2: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrExport2BeforeClose
    AfterScroll = QrExport2AfterScroll
    SQL.Strings = (
      'SELECT Tipo, IE, EUF, PUF, '
      'IF(trc.Tipo=0, RazaoSocial, trc.Nome) NO_ENT,'
      'IF(trc.Tipo=0, CNPJ, trc.CPF) CNPJ_CPF,'
      'gti.PrintTam, gcc.PrintCor, gg1.NCM, '
      ''
      'gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad,'
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, sei.*'
      'FROM spedestqits sei'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=sei.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'LEFT JOIN entidades  trc ON trc.Codigo=sei.Terceiro'
      'WHERE (EstqFimQtd > 0 OR EstqFimVal > 0)'
      'AND Exportado=0'
      'AND sei.ImporExpor=:P0'
      'AND sei.Empresa=:P1'
      'AND sei.AnoMes=:P2'
      'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, sei.GraGruX')
    Left = 336
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrExport2Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrExport2IE: TWideStringField
      FieldName = 'IE'
    end
    object QrExport2EUF: TSmallintField
      FieldName = 'EUF'
    end
    object QrExport2PUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrExport2NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrExport2CNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrExport2PrintTam: TSmallintField
      FieldName = 'PrintTam'
    end
    object QrExport2PrintCor: TSmallintField
      FieldName = 'PrintCor'
    end
    object QrExport2NCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrExport2Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrExport2NO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 120
    end
    object QrExport2PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrExport2UnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrExport2NO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrExport2SIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrExport2NO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrExport2NO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrExport2GraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrExport2GraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrExport2GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrExport2GraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrExport2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrExport2ImporExpor: TSmallintField
      FieldName = 'ImporExpor'
    end
    object QrExport2AnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrExport2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrExport2GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrExport2Sit_Prod: TSmallintField
      FieldName = 'Sit_Prod'
    end
    object QrExport2Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrExport2EstqIniQtd: TFloatField
      FieldName = 'EstqIniQtd'
    end
    object QrExport2EstqIniPrc: TFloatField
      FieldName = 'EstqIniPrc'
    end
    object QrExport2EstqIniVal: TFloatField
      FieldName = 'EstqIniVal'
    end
    object QrExport2ComprasQtd: TFloatField
      FieldName = 'ComprasQtd'
    end
    object QrExport2ComprasPrc: TFloatField
      FieldName = 'ComprasPrc'
    end
    object QrExport2ComprasVal: TFloatField
      FieldName = 'ComprasVal'
    end
    object QrExport2ConsumoQtd: TFloatField
      FieldName = 'ConsumoQtd'
    end
    object QrExport2ConsumoPrc: TFloatField
      FieldName = 'ConsumoPrc'
    end
    object QrExport2ConsumoVal: TFloatField
      FieldName = 'ConsumoVal'
    end
    object QrExport2EstqFimQtd: TFloatField
      FieldName = 'EstqFimQtd'
    end
    object QrExport2EstqFimPrc: TFloatField
      FieldName = 'EstqFimPrc'
    end
    object QrExport2EstqFimVal: TFloatField
      FieldName = 'EstqFimVal'
    end
  end
  object DsExport2: TDataSource
    DataSet = QrExport2
    Left = 364
    Top = 384
  end
  object frxDsExporta2: TfrxDBDataset
    UserName = 'frxDsExporta2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Tipo=Tipo'
      'IE=IE'
      'EUF=EUF'
      'PUF=PUF'
      'NO_ENT=NO_ENT'
      'CNPJ_CPF=CNPJ_CPF'
      'PrintTam=PrintTam'
      'PrintCor=PrintCor'
      'NCM=NCM'
      'Nivel1=Nivel1'
      'NO_PRD=NO_PRD'
      'PrdGrupTip=PrdGrupTip'
      'UnidMed=UnidMed'
      'NO_PGT=NO_PGT'
      'SIGLA=SIGLA'
      'NO_TAM=NO_TAM'
      'NO_COR=NO_COR'
      'GraCorCad=GraCorCad'
      'GraGruC=GraGruC'
      'GraGru1=GraGru1'
      'GraTamI=GraTamI'
      'Controle=Controle'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'GraGruX=GraGruX'
      'Sit_Prod=Sit_Prod'
      'Terceiro=Terceiro'
      'EstqIniQtd=EstqIniQtd'
      'EstqIniPrc=EstqIniPrc'
      'EstqIniVal=EstqIniVal'
      'ComprasQtd=ComprasQtd'
      'ComprasPrc=ComprasPrc'
      'ComprasVal=ComprasVal'
      'ConsumoQtd=ConsumoQtd'
      'ConsumoPrc=ConsumoPrc'
      'ConsumoVal=ConsumoVal'
      'EstqFimQtd=EstqFimQtd'
      'EstqFimPrc=EstqFimPrc'
      'EstqFimVal=EstqFimVal')
    DataSet = QrExport2
    BCDToCurrency = False
    Left = 392
    Top = 384
  end
  object QrListErr1: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM listerr1'
      'WHERE My_Idx=:P0')
    Left = 420
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrListErr1My_Idx: TIntegerField
      FieldName = 'My_Idx'
    end
    object QrListErr1Codigo: TWideStringField
      FieldName = 'Codigo'
    end
    object QrListErr1Texto1: TWideStringField
      FieldName = 'Texto1'
      Size = 255
    end
  end
  object DsListErr1: TDataSource
    DataSet = QrListErr1
    Left = 448
    Top = 384
  end
  object frxDsListErr1: TfrxDBDataset
    UserName = 'frxDsListErr1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'My_Idx=My_Idx'
      'Codigo=Codigo'
      'Texto1=Texto1')
    DataSet = QrListErr1
    BCDToCurrency = False
    Left = 476
    Top = 384
  end
  object QrLayout001: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT *'
      'FROM Layout001')
    Left = 504
    Top = 384
    object QrLayout001DataInvent: TWideStringField
      FieldName = 'DataInvent'
      Size = 8
    end
    object QrLayout001MesAnoInic: TWideStringField
      FieldName = 'MesAnoInic'
      Size = 4
    end
    object QrLayout001MesAnoFina: TWideStringField
      FieldName = 'MesAnoFina'
      Size = 4
    end
    object QrLayout001CodigoProd: TWideStringField
      FieldName = 'CodigoProd'
    end
    object QrLayout001SituacProd: TWideStringField
      FieldName = 'SituacProd'
      Size = 1
    end
    object QrLayout001CNPJTercei: TWideStringField
      FieldName = 'CNPJTercei'
      Size = 14
    end
    object QrLayout001IETerceiro: TWideStringField
      FieldName = 'IETerceiro'
    end
    object QrLayout001UFTerceiro: TWideStringField
      FieldName = 'UFTerceiro'
      Size = 2
    end
    object QrLayout001Quantidade: TWideStringField
      FieldName = 'Quantidade'
      Size = 21
    end
    object QrLayout001ValorUnita: TWideStringField
      FieldName = 'ValorUnita'
      Size = 17
    end
    object QrLayout001ValorTotal: TWideStringField
      FieldName = 'ValorTotal'
      Size = 17
    end
    object QrLayout001ICMSRecupe: TWideStringField
      FieldName = 'ICMSRecupe'
      Size = 17
    end
    object QrLayout001Observacao: TWideStringField
      FieldName = 'Observacao'
      Size = 60
    end
    object QrLayout001DescriProd: TWideStringField
      FieldName = 'DescriProd'
      Size = 80
    end
    object QrLayout001GrupoProdu: TWideStringField
      FieldName = 'GrupoProdu'
      Size = 4
    end
    object QrLayout001ClassifNCM: TWideStringField
      FieldName = 'ClassifNCM'
      Size = 10
    end
    object QrLayout001RESERVADOS: TWideStringField
      FieldName = 'RESERVADOS'
      Size = 30
    end
    object QrLayout001UnidMedida: TWideStringField
      FieldName = 'UnidMedida'
      Size = 3
    end
    object QrLayout001DescrGrupo: TWideStringField
      FieldName = 'DescrGrupo'
      Size = 30
    end
  end
  object DsLayout001: TDataSource
    DataSet = QrLayout001
    Left = 532
    Top = 384
  end
  object frxSPEDEFD_PRINT_000_01: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //'
      
        '  GH_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      
        '  GF_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      '  //'
      
        '  GH_GG2.Visible := <VARF_GG2_VISIBLE>;                         ' +
        '                            '
      '  GF_GG2.Visible := <VARF_GG2_VISIBLE>;'
      '  //        '
      
        '  GH_PRD.Visible := <VARF_PRD_VISIBLE>;                         ' +
        '                            '
      '  GF_PRD.Visible := <VARF_PRD_VISIBLE>;'
      '  //        '
      'end.')
    OnGetValue = frxSPEDEFD_PRINT_000_01GetValue
    Left = 440
    Top = 436
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsSPEDEstqIts
        DataSetName = 'frxDsSPEDEstqIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 83.149652680000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        object Shape1: TfrxShapeView
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 1009.134510000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 687.874460000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE SPED-EFD DO DIA [DATA_EM_TXT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 839.055660000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Top = 41.574830000000000000
          Width = 1009.134510000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 196.535560000000000000
          Top = 60.472480000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tam.')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 234.330860000000000000
          Top = 60.472480000000000000
          Width = 68.031496060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 302.362400000000000000
          Top = 60.472480000000000000
          Width = 26.456692910000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 328.819110000000000000
          Top = 60.472480000000000000
          Width = 170.078813390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque inicial')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 37.795300000000000000
          Top = 60.472480000000000000
          Width = 158.740167240000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Top = 60.472480000000000000
          Width = 37.795207240000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 328.819110000000000000
          Top = 71.811070000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 385.512060000000000000
          Top = 71.811070000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Unit'#225'rio')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 442.205010000000000000
          Top = 71.811070000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 498.897960000000000000
          Top = 60.472480000000000000
          Width = 170.078813390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Entradas')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 498.897960000000000000
          Top = 71.811070000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 555.590910000000000000
          Top = 71.811070000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Unit'#225'rio')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 612.283860000000000000
          Top = 71.811070000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 668.976810000000000000
          Top = 60.472480000000000000
          Width = 170.078813390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Baixas')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 668.976810000000000000
          Top = 71.811070000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 725.669760000000000000
          Top = 71.811070000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Unit'#225'rio')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 782.362710000000000000
          Top = 71.811070000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 839.055660000000000000
          Top = 60.472480000000000000
          Width = 170.078813390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque final')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 839.055660000000000000
          Top = 71.811070000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 895.748610000000000000
          Top = 71.811070000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Unit'#225'rio')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 952.441560000000000000
          Top = 71.811070000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 17.007876460000000000
        Top = 514.016080000000000000
        Width = 1009.134510000000000000
        object Memo37: TfrxMemoView
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 691.653990000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 11.338582680000000000
        ParentFont = False
        Top = 287.244280000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsSPEDEstqIts
        DataSetName = 'frxDsSPEDEstqIts'
        RowCount = 0
        object Memo45: TfrxMemoView
          Left = 196.535560000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataField = 'NO_TAM'
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."NO_TAM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 234.330860000000000000
          Width = 68.031496060000000000
          Height = 11.338582680000000000
          DataField = 'NO_COR'
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."NO_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 302.362400000000000000
          Width = 26.456692910000000000
          Height = 11.338582680000000000
          DataField = 'SIGLA'
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."SIGLA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 37.795300000000000000
          Width = 158.740167240000000000
          Height = 11.338582680000000000
          DataField = 'NO_PRD'
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Width = 37.795207240000000000
          Height = 11.338582680000000000
          DataField = 'GraGruX'
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 328.819110000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."EstqIniQtd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 385.512060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."EstqIniPrc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 442.205010000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."EstqIniVal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 498.897960000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."ComprasQtd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Left = 555.590910000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."ComprasPrc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 612.283860000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."ComprasVal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 668.976810000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."ConsumoQtd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 725.669760000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."ConsumoPrc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 782.362710000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."ConsumoVal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 839.055660000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."EstqFimQtd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 895.748610000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."EstqFimPrc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 952.441560000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsSPEDEstqIts
          DataSetName = 'frxDsSPEDEstqIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSPEDEstqIts."EstqFimVal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PGT: TfrxGroupHeader
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = []
        Height = 18.897637800000000000
        ParentFont = False
        Top = 162.519790000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsSPEDEstqIts."PrdGrupTip"'
        object frxDsSMIC2PrdGrupTip: TfrxMemoView
          Left = 132.283550000000000000
          Width = 83.149660000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsSPEDEstqIts."PrdGrupTip">)]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsSMIC2NO_PGT: TfrxMemoView
          Left = 215.433210000000000000
          Width = 793.700787400000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            ' - [frxDsSPEDEstqIts."NO_PGT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Width = 132.283550000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nivel 4 (Tipo): ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PRD: TfrxGroupFooter
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 321.260050000000000000
        Width = 1009.134510000000000000
        object Memo30: TfrxMemoView
          Width = 328.818919610000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total: [frxDsSPEDEstqIts."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 328.819110000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqIniQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 442.205010000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqIniVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 385.512060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."EstqIniQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."EstqIniVal">) / SUM(<frxDsSPEDEstqIts."EstqIniQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 498.897960000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ComprasQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 612.283860000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ComprasVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 555.590910000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."ComprasQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."ComprasVal">) / SUM(<frxDsSPEDEstqIts."ComprasQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 668.976810000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ConsumoQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 782.362710000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ConsumoVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 725.669760000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."ConsumoQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."ConsumoVal">) / SUM(<frxDsSPEDEstqIts."ConsumoQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 839.055660000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqFimQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 952.441560000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqFimVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          Left = 895.748610000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."EstqFimQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."EstqFimVal">) / SUM(<frxDsSPEDEstqIts."EstqFimQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PRD: TfrxGroupHeader
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 17.007874020000000000
        ParentFont = False
        Top = 245.669450000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsSPEDEstqIts."Nivel1"'
        object Memo34: TfrxMemoView
          Left = 132.283550000000000000
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsSPEDEstqIts."Nivel1">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Left = 215.433210000000000000
          Width = 793.700787400000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            ' - [frxDsSPEDEstqIts."NO_PRD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          Width = 132.283550000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nivel 1 (Produto): ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PGT: TfrxGroupFooter
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 396.850650000000000000
        Width = 1009.134510000000000000
        object Memo32: TfrxMemoView
          Width = 328.818919610000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total: [frxDsSPEDEstqIts."NO_PGT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 328.819110000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqIniQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 442.205010000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqIniVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 385.512060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."EstqIniQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."EstqIniVal">) / SUM(<frxDsSPEDEstqIts."EstqIniQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 498.897960000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ComprasQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 612.283860000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ComprasVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 555.590910000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."ComprasQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."ComprasVal">) / SUM(<frxDsSPEDEstqIts."ComprasQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 668.976810000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ConsumoQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 782.362710000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ConsumoVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 725.669760000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."ConsumoQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."ConsumoVal">) / SUM(<frxDsSPEDEstqIts."ConsumoQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          Left = 839.055660000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqFimQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          Left = 952.441560000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqFimVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          Left = 895.748610000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."EstqFimQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."EstqFimVal">) / SUM(<frxDsSPEDEstqIts."EstqFimQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 18.897642680000000000
        Top = 472.441250000000000000
        Width = 1009.134510000000000000
        object Memo44: TfrxMemoView
          Top = 7.559060000000000000
          Width = 328.818919610000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL PESQUISA')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 328.819110000000000000
          Top = 7.559060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqIniQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 442.205010000000000000
          Top = 7.559060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqIniVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 385.512060000000000000
          Top = 7.559060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."EstqIniQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."EstqIniVal">) / SUM(<frxDsSPEDEstqIts."EstqIniQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 498.897960000000000000
          Top = 7.559060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ComprasQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 612.283860000000000000
          Top = 7.559060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ComprasVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 555.590910000000000000
          Top = 7.559060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."ComprasQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."ComprasVal">) / SUM(<frxDsSPEDEstqIts."ComprasQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 668.976810000000000000
          Top = 7.559060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ConsumoQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 782.362710000000000000
          Top = 7.559060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ConsumoVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 725.669760000000000000
          Top = 7.559060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."ConsumoQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."ConsumoVal">) / SUM(<frxDsSPEDEstqIts."ConsumoQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          Left = 839.055660000000000000
          Top = 7.559060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqFimQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Left = 952.441560000000000000
          Top = 7.559060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqFimVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          Left = 895.748610000000000000
          Top = 7.559060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."EstqFimQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."EstqFimVal">) / SUM(<frxDsSPEDEstqIts."EstqFimQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_GG2: TfrxGroupHeader
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 17.007874020000000000
        ParentFont = False
        Top = 204.094620000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsSPEDEstqIts."Nivel2"'
        object Memo88: TfrxMemoView
          Left = 132.283550000000000000
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsSPEDEstqIts."Nivel2">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          Left = 215.433210000000000000
          Width = 793.700787400000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            ' - [frxDsSPEDEstqIts."NO_GG2"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo90: TfrxMemoView
          Width = 132.283550000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nivel 2 (Sub-grupo): ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_GG2: TfrxGroupFooter
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 359.055350000000000000
        Width = 1009.134510000000000000
        object Memo91: TfrxMemoView
          Width = 328.818919610000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total: [frxDsSPEDEstqIts."NO_GG2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          Left = 328.819110000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqIniQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          Left = 442.205010000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqIniVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          Left = 385.512060000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."EstqIniQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."EstqIniVal">) / SUM(<frxDsSPEDEstqIts."EstqIniQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          Left = 498.897960000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ComprasQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          Left = 612.283860000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ComprasVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          Left = 555.590910000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."ComprasQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."ComprasVal">) / SUM(<frxDsSPEDEstqIts."ComprasQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          Left = 668.976810000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ConsumoQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          Left = 782.362710000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."ConsumoVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          Left = 725.669760000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."ConsumoQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."ConsumoVal">) / SUM(<frxDsSPEDEstqIts."ConsumoQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          Left = 839.055660000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqFimQtd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          Left = 952.441560000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSPEDEstqIts."EstqFimVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          Left = 895.748610000000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsSPEDEstqIts."EstqFimQtd">)=0,0,SUM(<frxDsSPEDEstq' +
              'Its."EstqFimVal">) / SUM(<frxDsSPEDEstqIts."EstqFimQtd">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxSPEDEFD_PRINT_001_02: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //'
      '  GH_PGT.Visible := <VARF_PGT_VISIBLE>;'
      
        '  GF_PGT.Visible := <VARF_PGT_VISIBLE>;                         ' +
        '                            '
      '  //'
      
        '  GH_PRD.Visible := <VARF_PRD_VISIBLE>;                         ' +
        '                            '
      '  GF_PRD.Visible := <VARF_PRD_VISIBLE>;'
      '  //        '
      'end.')
    OnGetValue = frxSPEDEFD_PRINT_000_01GetValue
    Left = 468
    Top = 436
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsExporta2
        DataSetName = 'frxDsExporta2'
      end
      item
        DataSet = frxDsListErr1
        DataSetName = 'frxDsListErr1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 96.377952760000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'FALHAS NA EXPORTA'#199#195'O')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_EMPRESA]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 162.519790000000000000
          Top = 85.039370078740160000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tam.')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 200.315090000000000000
          Top = 85.039370078740160000
          Width = 68.031496060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 268.346630000000000000
          Top = 85.039370078740160000
          Width = 26.456692910000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Top = 85.039370078740160000
          Width = 162.519697240000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 294.803340000000000000
          Top = 85.039370078740160000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 623.622450000000000000
          Top = 85.039370078740160000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Codigo')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 351.496290000000000000
          Top = 85.039370078740160000
          Width = 272.126123390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Texto')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 45.354360000000000000
        Top = 434.645950000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 362.834880000000000000
          Width = 317.480520000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 11.338582680000000000
        ParentFont = False
        Top = 260.787570000000000000
        Width = 680.315400000000000000
        DataSet = frxDsExporta2
        DataSetName = 'frxDsExporta2'
        RowCount = 0
        object Memo4: TfrxMemoView
          Left = 162.519790000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataField = 'NO_TAM'
          DataSet = frxDsExporta2
          DataSetName = 'frxDsExporta2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExporta2."NO_TAM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 200.315090000000000000
          Width = 68.031496060000000000
          Height = 11.338582680000000000
          DataField = 'NO_COR'
          DataSet = frxDsExporta2
          DataSetName = 'frxDsExporta2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExporta2."NO_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 268.346630000000000000
          Width = 26.456692910000000000
          Height = 11.338582680000000000
          DataField = 'SIGLA'
          DataSet = frxDsExporta2
          DataSetName = 'frxDsExporta2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExporta2."SIGLA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Width = 162.519697240000000000
          Height = 11.338582680000000000
          DataField = 'NO_PRD'
          DataSet = frxDsExporta2
          DataSetName = 'frxDsExporta2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExporta2."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 294.803340000000000000
          Width = 56.692932910000000000
          Height = 11.338582680000000000
          DataField = 'GraGruX'
          DataSet = frxDsExporta2
          DataSetName = 'frxDsExporta2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExporta2."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 351.496290000000000000
          Width = 272.126142910000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 623.622450000000000000
          Width = 56.692932910000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_PGT: TfrxGroupHeader
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = []
        Height = 18.897637800000000000
        ParentFont = False
        Top = 177.637910000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsExporta2."PrdGrupTip"'
        object frxDsPrdEstqPrdGrupTip: TfrxMemoView
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsExporta2."PrdGrupTip">)]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsPrdEstqNO_PGT: TfrxMemoView
          Left = 166.299320000000000000
          Width = 514.016080000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            ' - [frxDsExporta2."NO_PGT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Width = 83.149660000000000000
          Height = 18.897637795275600000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Tipo: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PRD: TfrxGroupFooter
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Top = 328.819110000000000000
        Width = 680.315400000000000000
      end
      object GH_PRD: TfrxGroupHeader
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 17.007874020000000000
        ParentFont = False
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsExporta2."Nivel1"'
        object Memo34: TfrxMemoView
          Left = 83.149660000000000000
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsExporta2."Nivel1">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Left = 166.299320000000000000
          Width = 514.016080000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            ' - [frxDsExporta2."NO_PRD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          Width = 83.149660000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Produto: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF_PGT: TfrxGroupFooter
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Top = 351.496290000000000000
        Width = 680.315400000000000000
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Top = 411.968770000000000000
        Width = 680.315400000000000000
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        DataSet = frxDsListErr1
        DataSetName = 'frxDsListErr1'
        RowCount = 0
        object Memo11: TfrxMemoView
          Left = 351.496290000000000000
          Width = 272.126142910000000000
          Height = 11.338582680000000000
          DataField = 'Texto1'
          DataSet = frxDsListErr1
          DataSetName = 'frxDsListErr1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsListErr1."Texto1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 623.622450000000000000
          Width = 56.692932910000000000
          Height = 11.338582680000000000
          DataField = 'Codigo'
          DataSet = frxDsListErr1
          DataSetName = 'frxDsListErr1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsListErr1."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 162.519790000000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 200.315090000000000000
          Width = 68.031496060000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 268.346630000000000000
          Width = 26.456692910000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Width = 162.519697240000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 294.803340000000000000
          Width = 56.692932910000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsSPEDEstqIts: TfrxDBDataset
    UserName = 'frxDsSPEDEstqIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nivel1=Nivel1'
      'NO_PRD=NO_PRD'
      'PrdGrupTip=PrdGrupTip'
      'UnidMed=UnidMed'
      'NO_PGT=NO_PGT'
      'SIGLA=SIGLA'
      'NO_TAM=NO_TAM'
      'NO_COR=NO_COR'
      'GraCorCad=GraCorCad'
      'GraGruC=GraGruC'
      'GraGru1=GraGru1'
      'GraTamI=GraTamI'
      'Controle=Controle'
      'ImporExpor=ImporExpor'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'GraGruX=GraGruX'
      'Sit_Prod=Sit_Prod'
      'Terceiro=Terceiro'
      'EstqIniQtd=EstqIniQtd'
      'EstqIniPrc=EstqIniPrc'
      'EstqIniVal=EstqIniVal'
      'ComprasQtd=ComprasQtd'
      'ComprasPrc=ComprasPrc'
      'ComprasVal=ComprasVal'
      'ConsumoQtd=ConsumoQtd'
      'ConsumoPrc=ConsumoPrc'
      'ConsumoVal=ConsumoVal'
      'EstqFimQtd=EstqFimQtd'
      'EstqFimPrc=EstqFimPrc'
      'EstqFimVal=EstqFimVal'
      'Nivel2=Nivel2'
      'NO_GG2=NO_GG2'
      'Exportado=Exportado')
    DataSet = QrSPEDEstqIts
    BCDToCurrency = False
    Left = 180
    Top = 12
  end
end
