unit SPEDEstqCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, UnDmkEnums;

type
  TFmSPEDEstqCab = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdMes: TdmkEdit;
    LaMes: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdMesChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdMesExit(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitaBotaoOK();
  public
    { Public declarations }
  end;

  var
  FmSPEDEstqCab: TFmSPEDEstqCab;

implementation

uses UnMyObjects, ModuleGeral, SPEDEstqGer, UMySQLModule, Module;

{$R *.DFM}

procedure TFmSPEDEstqCab.BtOKClick(Sender: TObject);
const
  Encerrado = 0;
  ImporExpor = 2; // s� criar manualmente para exporta��o
var
  Ano, Mes, Dia: Word;
  AnoMes, Empresa: Integer;
begin
  DecodeDate(EdMes.ValueVariant, Ano, Mes, Dia);
  AnoMes := Ano * 100 + Mes;
  Empresa := DModG.QrEmpresasCodigo.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedestqcab', False, [
  'Encerrado'], [
  'ImporExpor', 'AnoMes', 'Empresa'], [
  Encerrado], [
  ImporExpor, AnoMes, Empresa], True) then
  begin
    FmSPEDEstqGer.ReopenSPEDEstqCab(AnoMes);
    Close;
  end;
end;

procedure TFmSPEDEstqCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSPEDEstqCab.EdEmpresaChange(Sender: TObject);
begin
  HabilitaBotaoOK();
end;

procedure TFmSPEDEstqCab.EdMesChange(Sender: TObject);
begin
  HabilitaBotaoOK();
end;

procedure TFmSPEDEstqCab.EdMesExit(Sender: TObject);
begin
  BtOK.Enabled := (EdEmpresa.ValueVariant <> 0) and (EdMes.ValueVariant > 2);
end;

procedure TFmSPEDEstqCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSPEDEstqCab.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmSPEDEstqCab.HabilitaBotaoOK();
begin
  BtOK.Enabled := (EdEmpresa.ValueVariant <> 0) and (EdMes.ValueVariant > 2);
end;

end.
