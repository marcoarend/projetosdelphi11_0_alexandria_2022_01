object DmModVS_CRC: TDmModVS_CRC
  Height = 676
  Width = 777
  PixelsPerInch = 96
  object QrMulFrn2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 384
    Top = 152
    object QrMulFrn2SiglaVS: TWideStringField
      FieldName = 'SiglaVS'
      Size = 60
    end
    object QrMulFrn2FrnCod: TLargeintField
      FieldName = 'FrnCod'
    end
    object QrMulFrn2Pecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrTotFrn2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 384
    Top = 200
    object QrTotFrn2Pecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrLocMFC: TMySQLQuery
    Database = Dmod.MyDB
    Left = 304
    Top = 4
    object QrLocMFCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrLocMFI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 304
    Top = 52
    object QrLocMFIControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrVSCabNFe: TMySQLQuery
    Database = Dmod.MyDB
    Left = 656
    Top = 148
    object QrVSCabNFeSerie: TSmallintField
      FieldName = 'Serie'
    end
    object QrVSCabNFenNF: TIntegerField
      FieldName = 'nNF'
    end
  end
  object QrSrcNiv2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 116
    Top = 376
  end
  object QrUniNFe: TMySQLQuery
    Database = Dmod.MyDB
    Left = 652
    Top = 4
    object QrUniNFeNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrUniNFeNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrUniNFeVSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
    end
    object QrUniNFePecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrMulNFe: TMySQLQuery
    Database = Dmod.MyDB
    Left = 652
    Top = 52
    object QrMulNFeNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrMulNFeNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrMulNFePecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrTotNFe: TMySQLQuery
    Database = Dmod.MyDB
    Left = 656
    Top = 100
    object QrTotNFePecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrUniFrn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 564
    Top = 4
    object QrUniFrnTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrUniFrnVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrUniFrnPecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrMulFrn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 564
    Top = 52
    object QrMulFrnFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrMulFrnPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrMulFrnSiglaVS: TWideStringField
      FieldName = 'SiglaVS'
      Size = 10
    end
  end
  object QrTotFrn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 568
    Top = 100
    object QrTotFrnPecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrMFDst: TMySQLQuery
    Database = Dmod.MyDB
    Left = 568
    Top = 145
  end
  object QrMFSrc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 568
    Top = 196
    object QrMFSrcControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMFSrcMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrMFSrcMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrMFSrcMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
  end
  object QrMFOrfaos: TMySQLQuery
    Database = Dmod.MyDB
    Left = 568
    Top = 244
    object QrMFOrfaosControle: TSmallintField
      FieldName = 'Controle'
    end
    object QrMFOrfaosTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrMFOrfaosVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
  end
  object QrVSMovCab: TMySQLQuery
    Database = Dmod.MyDB
    Left = 640
    Top = 376
  end
  object QrSumEmit: TMySQLQuery
    Database = Dmod.MyDB
    Left = 480
    Top = 156
    object QrSumEmitCusto: TFloatField
      FieldName = 'Custo'
    end
    object QrSumEmitPeso: TFloatField
      FieldName = 'Peso'
    end
  end
  object QrSumVMI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 484
    Top = 252
    object QrSumVMIQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrVMIs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 484
    Top = 204
    object QrVMIsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVMIsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVMIsQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrIMEI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM vsmovits'
      'WHERE Controle=:P0')
    Left = 388
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrIMEICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIMEIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrIMEIMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrIMEIMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrIMEIMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrIMEIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrIMEITerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrIMEICliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrIMEIMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrIMEILnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrIMEILnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrIMEILnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrIMEIDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrIMEIPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrIMEIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrIMEIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrIMEIPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrIMEIAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrIMEIAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrIMEIValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrIMEISrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrIMEISrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrIMEISrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrIMEISrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrIMEISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrIMEISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrIMEISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrIMEIObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrIMEISerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrIMEIFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrIMEIMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrIMEIFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrIMEICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrIMEICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrIMEIValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrIMEIDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrIMEIDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrIMEIDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrIMEIDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrIMEIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrIMEIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrIMEIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrIMEIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrIMEIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrIMEIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrIMEIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrIMEIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrIMEIAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrIMEINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrIMEIMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrIMEITpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrIMEIZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrIMEIEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrIMEINotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrIMEIFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrIMEIFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrIMEIPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrIMEIPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrIMEIPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrIMEILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrIMEIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrIMEIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrIMEIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrIMEIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrIMEIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrIMEIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrIMEICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrIMEIReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrIMEIStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrIMEIItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrIMEIVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
    end
    object QrIMEIVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, ggx.GraGruY'
      'FROM vsmovits wmi  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet  '
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa  '
      'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro '
      'WHERE wmi.Pecas > 0 '
      'AND wmi.Empresa=-11'
      'AND wmi.Pallet > 0 '
      'AND wmi.Pallet=505'
      'AND wmi.SdoVrtPeca > 0 '
      'AND MovimID IN (0,7,8,11,13,14) '
      'ORDER BY Controle ')
    Left = 220
    Top = 5
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVSMovItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSMovItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovItsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVSMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVSMovItsReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVSMovItsVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
  end
  object QrVSProQui: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM vsproqui '
      'WHERE Controle=1 ')
    Left = 476
    Top = 4
    object QrVSProQuiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSProQuiControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSProQuiSetorID: TSmallintField
      FieldName = 'SetorID'
    end
    object QrVSProQuiMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSProQuiMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSProQuiMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSProQuiEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSProQuiTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSProQuiMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSProQuiPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSProQuiPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSProQuiAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSProQuiCusPQ: TFloatField
      FieldName = 'CusPQ'
    end
    object QrVSProQuiFuloes: TIntegerField
      FieldName = 'Fuloes'
    end
    object QrVSProQuiMinDta: TDateField
      FieldName = 'MinDta'
    end
    object QrVSProQuiMaxDta: TDateField
      FieldName = 'MaxDta'
    end
    object QrVSProQuiEstqPeca: TFloatField
      FieldName = 'EstqPeca'
    end
    object QrVSProQuiEstqPeso: TFloatField
      FieldName = 'EstqPeso'
    end
    object QrVSProQuiEstqArM2: TFloatField
      FieldName = 'EstqArM2'
    end
    object QrVSProQuiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSProQuiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSProQuiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSProQuiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSProQuiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSProQuiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSProQuiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrVMI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM vsmovits')
    Left = 480
    Top = 100
    object QrVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVMIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVMIMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVMIMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVMIMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVMIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVMITerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVMICliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVMIMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVMILnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVMILnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVMIDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVMIPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVMIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVMIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVMIValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVMISrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVMISrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVMISrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVMISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVMISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVMIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVMIFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVMIMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVMICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVMICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVMISrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVMISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVMISerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVMIFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVMIValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVMIDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVMIDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVMIDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVMIDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVMIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVMIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVMIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVMIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVMIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVMIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVMIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVMIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVMIAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVMINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVMIMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVMITpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVMIZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrVMIEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrVMILnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrVMICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrVMINotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrVMIFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrVMIFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrVMIPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrVMIPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrVMIPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrVMIReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrVMIStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVMIItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrVMIVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
    end
    object QrVMIVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
  end
  object QrSCus2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emc.MPIn, SUM(emc.Custo) Custo,'
      'SUM(emc.Pecas) Pecas, SUM(emc.Peso) Peso,'
      'MIN(emc.DataEmis) MinDta, MAX(emc.DataEmis) MaxDta,'
      'COUNT(emc.MPIn) Fuloes, rec.Setor'
      'FROM emitcus emc'
      'LEFT JOIN formulas rec ON rec.Numero=emc.Formula'
      'WHERE emc.MPIn=:P0'
      'GROUP BY emc.MPIn, rec.Setor')
    Left = 476
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCus2MPIn: TIntegerField
      FieldName = 'MPIn'
      Required = True
    end
    object QrSCus2Custo: TFloatField
      FieldName = 'Custo'
    end
    object QrSCus2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSCus2Peso: TFloatField
      FieldName = 'Peso'
    end
    object QrSCus2MinDta: TDateTimeField
      FieldName = 'MinDta'
      Required = True
    end
    object QrSCus2MaxDta: TDateTimeField
      FieldName = 'MaxDta'
      Required = True
    end
    object QrSCus2Fuloes: TLargeintField
      FieldName = 'Fuloes'
      Required = True
    end
    object QrSCus2Setor: TIntegerField
      FieldName = 'Setor'
    end
  end
  object QrVMIFrete: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(CFTPA_ValorT) CustoTot '
      'FROM vsmoenvret '
      'WHERE VSVMI_Controle=6580 '
      ' ')
    Left = 656
    Top = 196
    object QrVMIFreteCustoTot: TFloatField
      FieldName = 'CustoTot'
    end
  end
  object DqAux: TMySQLDirectQuery
    Database = Dmod.MyDB
    Left = 360
    Top = 444
  end
  object DqAu2: TMySQLDirectQuery
    Database = Dmod.MyDB
    Left = 360
    Top = 492
  end
  object QrCabA: TMySQLQuery
    Database = Dmod.MyDB
    Left = 448
    Top = 444
  end
  object DsCabA: TDataSource
    DataSet = QrCabA
    Left = 448
    Top = 492
  end
  object Qr13: TMySQLQuery
    Database = Dmod.MyDB
    Left = 80
    Top = 196
    object Qr13Controle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr13GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object Qr15: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, MovimNiv, GraGruX,'
      'Pecas, PesoKg  '
      'FROM vsmovIts '
      'WHERE MovimCod=4723 '
      'AND MovimTwn =3351'
      'ORDER BY MovimID DESC')
    Left = 80
    Top = 300
    object Qr15Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object Qr15GraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object Qr15Pecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object Qr15PesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
  end
  object Qr14: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, MovimTwn '
      'FROM vsmovits '
      'WHERE MovimCod=4723'
      'AND MovimNiv=14')
    Left = 80
    Top = 248
    object Qr14Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object Qr14MovimTwn: TIntegerField
      FieldName = 'MovimTwn'
      Required = True
    end
  end
  object Qr06: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MedEClas, MovimCod, QtdAntPeso '
      'FROM vsmovits '
      'WHERE Controle=1 ')
    Left = 80
    Top = 148
    object Qr06MedEClas: TSmallintField
      FieldName = 'MedEClas'
      Required = True
    end
    object Qr06MovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object Qr06QtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      Required = True
    end
    object Qr06MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object Qr06MovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
  end
  object Qr6Sum: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, '
      'SUM(AreaP2) AreaP2  '
      'FROM vsmovits '
      'WHERE SrcNivel2=111')
    Left = 80
    Top = 100
    object Qr6SumPecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr6SumAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object Qr6SumAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
end
