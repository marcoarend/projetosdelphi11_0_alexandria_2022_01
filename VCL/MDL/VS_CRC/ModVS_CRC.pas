unit ModVS_CRC;

interface

uses
  Winapi.Windows, System.SysUtils, System.Classes, Data.DB, mySQLDbTables,
  Vcl.StdCtrls, Vcl.ComCtrls, System.TypInfo, AppListas, dmkGeral, UnDmkEnums,
  UnInternalConsts, UMySQLDB, UnProjGroup_Consts, UnProjGroup_Vars, UnAppEnums,
  mySQLDirectQuery, BlueDermConsts, dmkEdit;

type
  TDmModVS_CRC = class(TDataModule)
    QrMulFrn2: TmySQLQuery;
    QrMulFrn2SiglaVS: TWideStringField;
    QrMulFrn2FrnCod: TLargeintField;
    QrMulFrn2Pecas: TFloatField;
    QrTotFrn2: TmySQLQuery;
    QrTotFrn2Pecas: TFloatField;
    QrLocMFC: TmySQLQuery;
    QrLocMFCCodigo: TIntegerField;
    QrLocMFI: TmySQLQuery;
    QrLocMFIControle: TIntegerField;
    QrVSCabNFe: TmySQLQuery;
    QrVSCabNFeSerie: TSmallintField;
    QrVSCabNFenNF: TIntegerField;
    QrSrcNiv2: TmySQLQuery;
    QrUniNFe: TmySQLQuery;
    QrUniNFeNFeSer: TSmallintField;
    QrUniNFeNFeNum: TIntegerField;
    QrUniNFeVSMulNFeCab: TIntegerField;
    QrUniNFePecas: TFloatField;
    QrMulNFe: TmySQLQuery;
    QrMulNFeNFeSer: TSmallintField;
    QrMulNFeNFeNum: TIntegerField;
    QrMulNFePecas: TFloatField;
    QrTotNFe: TmySQLQuery;
    QrTotNFePecas: TFloatField;
    QrUniFrn: TmySQLQuery;
    QrUniFrnTerceiro: TIntegerField;
    QrUniFrnVSMulFrnCab: TIntegerField;
    QrUniFrnPecas: TFloatField;
    QrMulFrn: TmySQLQuery;
    QrMulFrnFornece: TIntegerField;
    QrMulFrnPecas: TFloatField;
    QrMulFrnSiglaVS: TWideStringField;
    QrTotFrn: TmySQLQuery;
    QrTotFrnPecas: TFloatField;
    QrMFDst: TmySQLQuery;
    QrMFSrc: TmySQLQuery;
    QrMFSrcControle: TIntegerField;
    QrMFSrcMovimID: TIntegerField;
    QrMFSrcMovimNiv: TIntegerField;
    QrMFSrcMovimCod: TIntegerField;
    QrMFOrfaos: TmySQLQuery;
    QrMFOrfaosControle: TSmallintField;
    QrMFOrfaosTerceiro: TIntegerField;
    QrMFOrfaosVSMulFrnCab: TIntegerField;
    QrVSMovCab: TmySQLQuery;
    QrSumEmit: TmySQLQuery;
    QrSumEmitCusto: TFloatField;
    QrSumEmitPeso: TFloatField;
    QrSumVMI: TmySQLQuery;
    QrSumVMIQtde: TFloatField;
    QrVMIs: TmySQLQuery;
    QrVMIsControle: TIntegerField;
    QrVMIsValorMP: TFloatField;
    QrVMIsQtde: TFloatField;
    QrIMEI: TmySQLQuery;
    QrIMEICodigo: TIntegerField;
    QrIMEIControle: TIntegerField;
    QrIMEIMovimCod: TIntegerField;
    QrIMEIMovimNiv: TIntegerField;
    QrIMEIMovimTwn: TIntegerField;
    QrIMEIEmpresa: TIntegerField;
    QrIMEITerceiro: TIntegerField;
    QrIMEICliVenda: TIntegerField;
    QrIMEIMovimID: TIntegerField;
    QrIMEILnkIDXtr: TIntegerField;
    QrIMEILnkNivXtr1: TIntegerField;
    QrIMEILnkNivXtr2: TIntegerField;
    QrIMEIDataHora: TDateTimeField;
    QrIMEIPallet: TIntegerField;
    QrIMEIGraGruX: TIntegerField;
    QrIMEIPecas: TFloatField;
    QrIMEIPesoKg: TFloatField;
    QrIMEIAreaM2: TFloatField;
    QrIMEIAreaP2: TFloatField;
    QrIMEIValorT: TFloatField;
    QrIMEISrcMovID: TIntegerField;
    QrIMEISrcNivel1: TIntegerField;
    QrIMEISrcNivel2: TIntegerField;
    QrIMEISrcGGX: TIntegerField;
    QrIMEISdoVrtPeca: TFloatField;
    QrIMEISdoVrtPeso: TFloatField;
    QrIMEISdoVrtArM2: TFloatField;
    QrIMEIObserv: TWideStringField;
    QrIMEISerieFch: TIntegerField;
    QrIMEIFicha: TIntegerField;
    QrIMEIMisturou: TSmallintField;
    QrIMEIFornecMO: TIntegerField;
    QrIMEICustoMOKg: TFloatField;
    QrIMEICustoMOTot: TFloatField;
    QrIMEIValorMP: TFloatField;
    QrIMEIDstMovID: TIntegerField;
    QrIMEIDstNivel1: TIntegerField;
    QrIMEIDstNivel2: TIntegerField;
    QrIMEIDstGGX: TIntegerField;
    QrIMEIQtdGerPeca: TFloatField;
    QrIMEIQtdGerPeso: TFloatField;
    QrIMEIQtdGerArM2: TFloatField;
    QrIMEIQtdGerArP2: TFloatField;
    QrIMEIQtdAntPeca: TFloatField;
    QrIMEIQtdAntPeso: TFloatField;
    QrIMEIQtdAntArM2: TFloatField;
    QrIMEIQtdAntArP2: TFloatField;
    QrIMEIAptoUso: TSmallintField;
    QrIMEINotaMPAG: TFloatField;
    QrIMEIMarca: TWideStringField;
    QrIMEITpCalcAuto: TIntegerField;
    QrIMEIZerado: TSmallintField;
    QrIMEIEmFluxo: TSmallintField;
    QrIMEINotFluxo: TIntegerField;
    QrIMEIFatNotaVNC: TFloatField;
    QrIMEIFatNotaVRC: TFloatField;
    QrIMEIPedItsLib: TIntegerField;
    QrIMEIPedItsFin: TIntegerField;
    QrIMEIPedItsVda: TIntegerField;
    QrIMEILk: TIntegerField;
    QrIMEIDataCad: TDateField;
    QrIMEIDataAlt: TDateField;
    QrIMEIUserCad: TIntegerField;
    QrIMEIUserAlt: TIntegerField;
    QrIMEIAlterWeb: TSmallintField;
    QrIMEIAtivo: TSmallintField;
    QrIMEICustoMOM2: TFloatField;
    QrIMEIReqMovEstq: TIntegerField;
    QrIMEIStqCenLoc: TIntegerField;
    QrIMEIItemNFe: TIntegerField;
    QrIMEIVSMorCab: TIntegerField;
    QrIMEIVSMulFrnCab: TIntegerField;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsGraGruY: TIntegerField;
    QrVSMovItsReqMovEstq: TIntegerField;
    QrVSMovItsStqCenLoc: TIntegerField;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrVSMovItsClientMO: TIntegerField;
    QrVSProQui: TmySQLQuery;
    QrVSProQuiCodigo: TIntegerField;
    QrVSProQuiControle: TIntegerField;
    QrVSProQuiSetorID: TSmallintField;
    QrVSProQuiMovimCod: TIntegerField;
    QrVSProQuiMovimNiv: TIntegerField;
    QrVSProQuiMovimTwn: TIntegerField;
    QrVSProQuiEmpresa: TIntegerField;
    QrVSProQuiTerceiro: TIntegerField;
    QrVSProQuiMovimID: TIntegerField;
    QrVSProQuiPecas: TFloatField;
    QrVSProQuiPesoKg: TFloatField;
    QrVSProQuiAreaM2: TFloatField;
    QrVSProQuiCusPQ: TFloatField;
    QrVSProQuiFuloes: TIntegerField;
    QrVSProQuiMinDta: TDateField;
    QrVSProQuiMaxDta: TDateField;
    QrVSProQuiEstqPeca: TFloatField;
    QrVSProQuiEstqPeso: TFloatField;
    QrVSProQuiEstqArM2: TFloatField;
    QrVSProQuiLk: TIntegerField;
    QrVSProQuiDataCad: TDateField;
    QrVSProQuiDataAlt: TDateField;
    QrVSProQuiUserCad: TIntegerField;
    QrVSProQuiUserAlt: TIntegerField;
    QrVSProQuiAlterWeb: TSmallintField;
    QrVSProQuiAtivo: TSmallintField;
    QrVMI: TmySQLQuery;
    QrVMICodigo: TIntegerField;
    QrVMIControle: TIntegerField;
    QrVMIMovimCod: TIntegerField;
    QrVMIMovimNiv: TIntegerField;
    QrVMIMovimTwn: TIntegerField;
    QrVMIEmpresa: TIntegerField;
    QrVMITerceiro: TIntegerField;
    QrVMICliVenda: TIntegerField;
    QrVMIMovimID: TIntegerField;
    QrVMILnkNivXtr1: TIntegerField;
    QrVMILnkNivXtr2: TIntegerField;
    QrVMIDataHora: TDateTimeField;
    QrVMIPallet: TIntegerField;
    QrVMIGraGruX: TIntegerField;
    QrVMIPecas: TFloatField;
    QrVMIPesoKg: TFloatField;
    QrVMIAreaM2: TFloatField;
    QrVMIAreaP2: TFloatField;
    QrVMIValorT: TFloatField;
    QrVMISrcMovID: TIntegerField;
    QrVMISrcNivel1: TIntegerField;
    QrVMISrcNivel2: TIntegerField;
    QrVMISdoVrtPeca: TFloatField;
    QrVMISdoVrtArM2: TFloatField;
    QrVMIObserv: TWideStringField;
    QrVMIFicha: TIntegerField;
    QrVMIMisturou: TSmallintField;
    QrVMICustoMOKg: TFloatField;
    QrVMICustoMOTot: TFloatField;
    QrVMILk: TIntegerField;
    QrVMIDataCad: TDateField;
    QrVMIDataAlt: TDateField;
    QrVMIUserCad: TIntegerField;
    QrVMIUserAlt: TIntegerField;
    QrVMIAlterWeb: TSmallintField;
    QrVMIAtivo: TSmallintField;
    QrVMISrcGGX: TIntegerField;
    QrVMISdoVrtPeso: TFloatField;
    QrVMISerieFch: TIntegerField;
    QrVMIFornecMO: TIntegerField;
    QrVMIValorMP: TFloatField;
    QrVMIDstMovID: TIntegerField;
    QrVMIDstNivel1: TIntegerField;
    QrVMIDstNivel2: TIntegerField;
    QrVMIDstGGX: TIntegerField;
    QrVMIQtdGerPeca: TFloatField;
    QrVMIQtdGerPeso: TFloatField;
    QrVMIQtdGerArM2: TFloatField;
    QrVMIQtdGerArP2: TFloatField;
    QrVMIQtdAntPeca: TFloatField;
    QrVMIQtdAntPeso: TFloatField;
    QrVMIQtdAntArM2: TFloatField;
    QrVMIQtdAntArP2: TFloatField;
    QrVMIAptoUso: TSmallintField;
    QrVMINotaMPAG: TFloatField;
    QrVMIMarca: TWideStringField;
    QrVMITpCalcAuto: TIntegerField;
    QrVMIZerado: TSmallintField;
    QrVMIEmFluxo: TSmallintField;
    QrVMILnkIDXtr: TIntegerField;
    QrVMICustoMOM2: TFloatField;
    QrVMINotFluxo: TIntegerField;
    QrVMIFatNotaVNC: TFloatField;
    QrVMIFatNotaVRC: TFloatField;
    QrVMIPedItsLib: TIntegerField;
    QrVMIPedItsFin: TIntegerField;
    QrVMIPedItsVda: TIntegerField;
    QrVMIReqMovEstq: TIntegerField;
    QrVMIStqCenLoc: TIntegerField;
    QrVMIItemNFe: TIntegerField;
    QrVMIVSMorCab: TIntegerField;
    QrVMIVSMulFrnCab: TIntegerField;
    QrSCus2: TmySQLQuery;
    QrSCus2MPIn: TIntegerField;
    QrSCus2Custo: TFloatField;
    QrSCus2Pecas: TFloatField;
    QrSCus2Peso: TFloatField;
    QrSCus2MinDta: TDateTimeField;
    QrSCus2MaxDta: TDateTimeField;
    QrSCus2Fuloes: TLargeintField;
    QrSCus2Setor: TIntegerField;
    QrVMIFrete: TmySQLQuery;
    QrVMIFreteCustoTot: TFloatField;
    DqAux: TmySQLDirectQuery;
    DqAu2: TmySQLDirectQuery;
    QrCabA: TmySQLQuery;
    DsCabA: TDataSource;
    Qr13: TMySQLQuery;
    Qr15: TMySQLQuery;
    Qr15Controle: TIntegerField;
    Qr15GraGruX: TIntegerField;
    Qr15Pecas: TFloatField;
    Qr15PesoKg: TFloatField;
    Qr13Controle: TIntegerField;
    Qr13GraGruX: TIntegerField;
    Qr14: TMySQLQuery;
    Qr14Controle: TIntegerField;
    Qr14MovimTwn: TIntegerField;
    Qr06: TMySQLQuery;
    Qr06MedEClas: TSmallintField;
    Qr06MovimCod: TIntegerField;
    Qr06QtdAntPeso: TFloatField;
    Qr6Sum: TMySQLQuery;
    Qr6SumPecas: TFloatField;
    Qr6SumAreaM2: TFloatField;
    Qr6SumAreaP2: TFloatField;
    Qr06MovimID: TIntegerField;
    Qr06MovimNiv: TIntegerField;
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    procedure PreparaPalletParaReclass_InsereBaixaAtual(Codigo, MovimCod:
              Integer; DataHora: String; NewVMI, StqCenLoc: Integer;
              IuvpeiBxa: TInsUpdVMIPrcExecID; SQLType: TSQLType);
    procedure PreparaPalletParaReclass_InsereNovoVMI_DoPallet(Pallet, Controle,
              Codigo, MovimCod: Integer; DataHora: String; Pecas, PesoKg,
              AreaM2, AreaP2: Double; SerieFch, Ficha, Terceiro, VSMulFrnCab,
              FornecMO: Integer; Marca: String; CustoMOKg, CustoMOTot, ValorMP,
              ValorT: Double; Empresa, ClientMO, GraGruX, GraGruY, TpCalcAuto,
              PedItsLib, PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
              GSPSrcNiv2, ReqMovEstq, StqCenLoc: Integer; IuvpeiInn:
              TInsUpdVMIPrcExecID; SQLType: TSQLType);
    procedure CalculaFreteVMI(Controle: Integer);
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo
    //
  public
    { Public declarations }
    //
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
    //
    function  AtualizaVSMovIts_VS_BH(MovimCod: Integer; MovimID: TEstqMovimID): Boolean;
    function  AtualizaVSMovIts_VS_WE(MovimCod: Integer; MovimID: TEstqMovimID): Boolean;
    function  AtualizaVSMovIts_VS_SP(MovimCod: Integer; MovimID: TEstqMovimID): Boolean;
    function  AtualizaVSMovIts_VS_RM(MovimCod: Integer; MovimID: TEstqMovimID): Boolean;
    function  AtualizaVSMovIts_VS_BHWE(MovimCod: Integer): Boolean;
    procedure AtualizaVSMulFrnCabNew(SinalOrig: TSinal; DefMulFrn: TEstqDefMulFldEMxx;
              MovTypeCod: Integer; MovimIDs: array of TEstqMovimID;
              MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv);
    procedure AtualizaVSMulNFeCab(SinalOrig: TSinal;
              DefMulNFe: TEstqDefMulFldEMxx; MovTypeCod: Integer;
              MovimID: TEstqMovimID; MovimNivsOri,
              MovimNivsDst: array of TEstqMovimNiv);
    procedure AtualizaSaldoVSMOEnvAvu(Codigo: Integer);
    procedure AtualizaSaldoVSMOEnvEnv(Codigo: Integer);
    procedure AtualizaSaldoVSMOEnvRet(ListaEnvEnv: TMyGrlArrInt);
    procedure AtualizaFreteVMIdeMOEnvAvu(IMEI: Integer);
    procedure AtualizaFreteVMIdeMOEnvEnv(IMEI: Integer);
    procedure AtualizaFreteVMIdeMOEnvRet(IMEI: Integer);
    procedure AtualizaImeiValorT(Controle: Integer);
    procedure CalculaFreteVMIdeMOEnvAvu(MovimCod: Integer);
    procedure CalculaFreteVMIdeMOEnvEnv(MovimCod: Integer);
    procedure CalculaFreteVMIdeMOEnvRet(MovimCod: Integer);
    procedure ExcluiAtrelamentoMOEnvAvu(QrVSMOEnvAvu, QrVSMOEnvAVMI: TmySQLQuery);
    procedure ExcluiAtrelamentoMOEnvEnv(QrVSMOEnvEnv, QrVSMOEnvEVMI: TmySQLQuery);
    procedure ExcluiAtrelamentoMOEnvRet(QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI: TmySQLQuery);
    procedure InformaSemFornecedor();
    procedure InformaSemNFe();
    function  ObtemFornecedores_Pallet(Pallet: Integer): String;
    function  ObtemGraGruYDeGraGru1(Nivel1: Integer): Integer;
    function  ObtemProximoCTe(Transportador, Serie: Integer): Integer;
    function  PreparaPalletParaReclassificar(const SQLType: TSQLType; const
              CkReclasse_Checked: Boolean; var Empresa, ClientMO, Codigo,
              MovimCod, Controle, Pallet, GraGruX, GraGruY: Integer;
              DtHr: TDateTime; const StqCenLoc: Integer; IuvpeiInn, IuvpeiBxa:
              TInsUpdVMIPrcExecID): Boolean;
    function  AtualizaVSMovIts_CusEmit(Controle: Integer): Boolean;
    procedure PesquisaNFe(EdFatID, EdFatNum, EdEmpresa, EdTerceiro, EdSerNF,
              EdnNF: TdmkEdit);
  end;

var
  DmModVS_CRC: TDmModVS_CRC;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

{ TDmModVS_CRC }

uses
  DmkDAC_PF, ModuleGeral, UnMyObjects, UnVS_CRC_PF, UMySQLModule, Module,
  UnDmkProcFunc, Principal, MyListas, MyDBCheck, NFe_PF;


procedure TDmModVS_CRC.AtualizaFreteVMIdeMOEnvAvu(IMEI: Integer);
var
  ValorFrete: Double;
begin
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT SUM(ValorFrete) ValorFrete ',
  'FROM vsmoenvavmi ',
  'WHERE VSMovIts=' + Geral.FF0(IMEI),
  '']);
  ValorFrete := USQLDB.Flu(DqAux, 'ValorFrete');
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'CusFrtAvuls'], [
  'Controle'], [
  ValorFrete], [
  IMEI], True);
  //
  AtualizaImeiValorT(IMEI);
  //
end;

procedure TDmModVS_CRC.AtualizaFreteVMIdeMOEnvEnv(IMEI: Integer);
var
  ValorFrete: Double;
begin
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT SUM(ValorFrete) ValorFrete ',
  'FROM vsmoenvevmi ',
  'WHERE VSMovIts=' + Geral.FF0(IMEI),
  '']);
  ValorFrete := USQLDB.Flu(DqAux, 'ValorFrete');
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_SEL_TAB_VMI, False, [
  'CusFrtMOEnv'], [
  'Controle'], [
  ValorFrete], [
  IMEI], True);
  //
  AtualizaImeiValorT(IMEI);
  //
end;

procedure TDmModVS_CRC.AtualizaFreteVMIdeMOEnvRet(IMEI: Integer);
var
  ValorFrete: Double;
begin
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT SUM(ValorFrete) ValorFrete ',
  'FROM vsmoenvgvmi ',
  'WHERE VSMovIts=' + Geral.FF0(IMEI),
  '']);
  ValorFrete := USQLDB.Flu(DqAux, 'ValorFrete');
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_SEL_TAB_VMI, False, [
  'CusFrtMORet'], [
  'Controle'], [
  ValorFrete], [
  IMEI], True);
  //
  AtualizaImeiValorT(IMEI);
  //
end;

procedure TDmModVS_CRC.AtualizaImeiValorT(Controle: Integer);
const
  sProcName = 'TDmModVS_CRC.AtualizaImeiValorT()';
var
  CusFrtAvuls, CusFrtMOEnv, CusFrtMORet, (*CusFrtTrnsf,*) ValorMP, CustoPQ, ValorT,
  CustoMOTot: Double;
  MovimID, MovimNiv: Integer;
  Continua: Boolean;
begin
  VS_CRC_PF.AdicionarNovosVS_emid();
  //
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT CusFrtAvuls, CusFrtMOEnv, CusFrtMORet, ',
  //'CusFrtTrnsf, ',
  'ValorMP, CustoPQ, CustoMOTot, ',
  'MovimID, MovimNiv ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  CusFrtAvuls := USQLDB.Flu(DqAux, 'CusFrtAvuls');
  CusFrtMOEnv := USQLDB.Flu(DqAux, 'CusFrtMOEnv');
  CusFrtMORet := USQLDB.Flu(DqAux, 'CusFrtMORet');
  //CusFrtTrnsf := USQLDB.Flu(DqAux, 'CusFrtTrnsf');
  ValorMP     := USQLDB.Flu(DqAux, 'ValorMP');
  CustoPQ     := USQLDB.Flu(DqAux, 'CustoPQ');
  CustoMOTot  := USQLDB.Flu(DqAux, 'CustoMOTot');
  //
  MovimID     := USQLDB.Int(DqAux, 'MovimID');
  MovimNiv    := USQLDB.Int(DqAux, 'MovimNiv');
  //
  ValorT      := 0;
  Continua    := False;
  case TEstqMovimID(MovimID) of
    //(*01*)emidCompra, Faz direto no VSInnIts!
    (*16*)emidEntradaPlC,
    (*21*)emidDevolucao,
    (*22*)emidRetrabalho:
    begin
      ValorT := CusFrtAvuls + CusFrtMOEnv + ValorMP; // + CusFrtTrnsf
      Continua := True;
    end;
    (*02*)emidVenda:
    begin
      ValorT := ValorMP - CusFrtAvuls; // + CusFrtTrnsf
      Continua := True;
    end;
    (*06*)emidIndsXX:
    begin
      ValorT := CusFrtMORet + ValorMP + CustoPQ + CustoMOTot; // + CusFrtTrnsf
      Continua := True;
    end;
    (*11*)emidEmOperacao,
    (*19*)emidEmProcWE,
    (*20*)emidFinished,
    (*33*)emidEmReprRM: // n�o testei ainda !!!
    begin
      //ValorT2     := ValorMP + CustoMOTot + CusFrtMORet;
      ValorT := CusFrtMORet + ValorMP + CustoPQ + CustoMOTot; // + CusFrtTrnsf
      Continua := True;
    end;
    (*25*)emidTransfLoc:
    begin
      ValorT := CusFrtAvuls + CusFrtMOEnv + CusFrtMORet + ValorMP; // + CusFrtTrnsf
      Continua := True;
    end;

    //emidFinished=20,
    //emidTransfLoc=25,
    (*emidEmProcCal=26, emidEmProcCur=27,
                 emidDesclasse=28, emidCaleado=29, emidEmRibPDA=30,
                 emidEmRibDTA=31, emidEmProcSP=32, emidEmReprRM=33,
                 emidCurtido=34, emidMixInsum=35, emidInnSemCob=36,
                 emidOutSemCob=3,
                 emidSPCaleirad=42,
                 *)
    else
    begin
      Geral.MB_Erro('"MovimID" ' + Geral.FF0(MovimID) +
      ' n�o implementado em ' + sProcName + sLineBreak +
      'IME-I: ' + Geral.FF0(Controle));
      Continua := False;
    end;
  end;
  if Continua then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'ValorT'], ['Controle'], [
    ValorT], [Controle], True);
  end;
end;

procedure TDmModVS_CRC.AtualizaSaldoVSMOEnvAvu(Codigo: Integer);
begin
// Nada???
end;

procedure TDmModVS_CRC.AtualizaSaldoVSMOEnvEnv(Codigo: Integer);
var
  NFEMP_SdoPeca, NFEMP_SdoPeso, NFEMP_SdoArM2: Double;
begin
  if Codigo = 0 then
    Exit;
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT NFEMP_Pecas, NFEMP_PesoKg, NFEMP_AreaM2, NFEMP_ValorT ',
  'FROM vsmoenvenv ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  NFEMP_SdoPeca  := USQLDB.Flu(DqAux, 'NFEMP_Pecas');
  NFEMP_SdoPeso  := USQLDB.Flu(DqAux, 'NFEMP_PesoKg');
  NFEMP_SdoArM2  := USQLDB.Flu(DqAux, 'NFEMP_AreaM2');
  //
(*
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT SUM(NFRMP_Pecas) NFRMP_Pecas, SUM(NFRMP_PesoKg) NFRMP_PesoKg, ',
  'SUM(NFRMP_AreaM2) NFRMP_AreaM2, SUM(NFRMP_ValorT) NFRMP_ValorT ',
  'FROM vsmoenvret ',
  'WHERE VSMOEnvEnv=' + Geral.FF0(Codigo),
  '']);
*)
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT SUM(Pecas) NFRMP_Pecas, SUM(PesoKg) NFRMP_PesoKg, ',
  'SUM(AreaM2) NFRMP_AreaM2',
  'FROM vsmoenvrvmi ',
  'WHERE VSMOEnvEnv=' + Geral.FF0(Codigo),
  '']);
  NFEMP_SdoPeca := NFEMP_SdoPeca - USQLDB.Flu(DqAux, 'NFRMP_Pecas');
  NFEMP_SdoPeso := NFEMP_SdoPeso - USQLDB.Flu(DqAux, 'NFRMP_PesoKg');
  NFEMP_SdoArM2 := NFEMP_SdoArM2 - USQLDB.Flu(DqAux, 'NFRMP_AreaM2');
  //
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmoenvenv', False, [
  'NFEMP_SdoPeca', 'NFEMP_SdoPeso', 'NFEMP_SdoArM2'], [
  'Codigo'], [
  NFEMP_SdoPeca, NFEMP_SdoPeso, NFEMP_SdoArM2], [
  Codigo], True);
end;

procedure TDmModVS_CRC.AtualizaSaldoVSMOEnvRet(ListaEnvEnv: TMyGrlArrInt);
const
  sProcName = 'TDmModVS_CRC.AtualizaSaldoVSMOEnvRet()';
var
  I: Integer;
begin
  for I := Low(ListaEnvEnv) to High(ListaEnvEnv) do
    AtualizaSaldoVSMOEnvEnv(ListaEnvEnv[I]);
end;

function TDmModVS_CRC.AtualizaVSMovIts_CusEmit(Controle: Integer): Boolean;
var
  MinDta, MaxDta: String;
  //Controle,
  Codigo, MovimCod, MovimNiv, MovimTwn, Empresa, Terceiro, MovimID, SetorID,
  FuloesCal, FuloesCur, FuloesRec, Fuloes: Integer;
  Pecas, PesoKg, AreaM2, CusPQ, EstqPeca, EstqPeso, EstqArM2: Double;
  SQLType: TSQLType;
  //
  MinDtaCal, MaxDtaCal, MinDtaCur, MaxDtaCur, MinDtaRec, MaxDtaRec: String;
  PecasCal, PesoKgCal, CusPQCal: Double;
  PecasCur, PesoKgCur, CusPQCur: Double;
  PecasRec, PesoKgRec, CusPQRec: Double;
  //
  AreaM2Cal, AreaM2Cur, AreaM2Rec: Double;
  //
  function ObtemSQLType(SetorX: Integer): TSQLType;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSProQui, Dmod.MyDB, [
    'SELECT *  ',
    'FROM vsproqui ',
    'WHERE Controle=' + Geral.FF0(Controle),
    'AND SetorID=' + Geral.FF0(SetorX),
    '']);
    if QrVSProQui.RecordCount > 0 then
      Result := stUpd
    else
      Result := stIns;
  end;
begin
  //Result := False;
  if CO_DMKID_APP = 47 then // ClaReCo
  begin
    Result := True;
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMI, Dmod.MyDB, [
  'SELECT *  ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSCus2, Dmod.MyDB, [
  'SELECT emc.MPIn, SUM(emc.Custo) Custo,',
  'SUM(emc.Pecas) Pecas, SUM(emc.Peso) Peso,',
  'MIN(emc.DataEmis) MinDta, MAX(emc.DataEmis) MaxDta,',
  'COUNT(emc.MPIn) Fuloes, rec.Setor',
  'FROM emitcus emc',
  'LEFT JOIN formulas rec ON rec.Numero=emc.Formula',
  'WHERE emc.VSMovIts=' + Geral.FF0(Controle),
  'GROUP BY rec.Setor',
  '']);
  //
  //Controle       := ;
  Codigo         := QrVMICodigo.Value;
  MovimCod       := QrVMIMovimCod.Value;
  MovimNiv       := QrVMIMovimNiv.Value;
  MovimTwn       := QrVMIMovimTwn.Value;
  Empresa        := QrVMIEmpresa.Value;
  Terceiro       := QrVMITerceiro.Value;
  MovimID        := QrVMIMovimID.Value;
  SetorID        := 0;
  Pecas          := 0.000;
  PesoKg         := 0.000;
  AreaM2         := 0.00;
  CusPQ          := 0.00;
  Fuloes         := 0;
  MinDtaCal      := '0000-00-00';
  MaxDtaCal      := '0000-00-00';
  MinDtaCur      := '0000-00-00';
  MaxDtaCur      := '0000-00-00';
  MinDta         := '0000-00-00';
  MaxDta         := '0000-00-00';
  EstqPeca       := 0.000;
  EstqPeso       := 0.000;
  EstqArM2       := 0.00;
  //
  // ini 2022-01-11
  //if TEstqMovimID(QrVMIMovimID.Value) = emidCompra then
  if (TEstqMovimID(QrVMIMovimID.Value) = emidCompra)
  or (TEstqMovimID(QrVMIMovimID.Value) = emidEmProcCal)
  or (TEstqMovimID(QrVMIMovimID.Value) = emidEmProcCur) then
  begin
    while not QrSCus2.Eof do
    begin
      //if QrSCus2Setor.Value = Dmod.QrControleSetorCal.Value then
      if QrSCus2Setor.Value = VAR_SetorCal then
      begin
        //SetorID   := Integer(TEstqSetorID.esiCaleiro);
        PecasCal  := PecasCal  + QrSCus2Pecas.Value;
        PesoKgCal := PesoKgCal + QrSCus2Peso.Value;
        CusPQCal  := CusPQCal  + QrSCus2Custo.Value;
        FuloesCal := FuloesCal + QrSCus2Fuloes.Value;
        MinDtaCal := Geral.FDT(QrSCus2MinDta.Value, 1);
        MaxDtaCal := Geral.FDT(QrSCus2MaxDta.Value, 1);
        AreaM2Cal := 0;
      end else
      //if QrSCus2Setor.Value = Dmod.QrControleSetorCur.Value then
      if QrSCus2Setor.Value = VAR_SetorCur then
      begin
        //SetorID   := Integer(TEstqSetorID.esiCurtimento);
        PecasCur  := PecasCur  + QrSCus2Pecas.Value;
        PesoKgCur := PesoKgCur + QrSCus2Peso.Value;
        CusPQCur  := CusPQCur  + QrSCus2Custo.Value;
        FuloesCur := FuloesCur + QrSCus2Fuloes.Value;
        MinDtaCur := Geral.FDT(QrSCus2MinDta.Value, 1);
        MaxDtaCur := Geral.FDT(QrSCus2MaxDta.Value, 1);
        AreaM2Cur := 0;
      end
      else Geral.MB_Aviso(
      'Setor inv�lido para caleiro ou curtimento na defini��o estoque de couros na emiss�o de pesagem!');
      //
      QrSCus2.Next;
    end;
    //
    //////// CALEIRO /////////////////////////////////////////////////////////////
    //
    SetorID  := Integer(TEstqSetorID.esiCaleiro);
    SQLType  := ObtemSQLType(SetorID);
    //
    //
    Pecas    := PecasCal;
    PesoKg   := PesoKgCal;
    AreaM2   := AreaM2Cal;
    CusPQ    := CusPQCal;
    Fuloes   := FuloesCal;
    MinDta   := MinDtaCal;
    MaxDta   := MaxDtaCal;
    EstqPeca := QrVMIPecas.Value  - Pecas;
    EstqPeso := QrVMIPesoKg.Value - PesoKg;
    EstqArM2 := QrVMIAreaM2.Value - AreaM2;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsproqui', False, [
    'Codigo', 'MovimCod', 'MovimNiv',
    'MovimTwn', 'Empresa', 'Terceiro',
    'MovimID', 'Pecas',
    'PesoKg', 'AreaM2', 'CusPQ',
    'Fuloes', 'MinDta', 'MaxDta',
    'EstqPeca', 'EstqPeso', 'EstqArM2'], [
    'Controle', 'SetorID'], [
    Codigo, MovimCod, MovimNiv,
    MovimTwn, Empresa, Terceiro,
    MovimID, Pecas,
    PesoKg, AreaM2, CusPQ,
    Fuloes, MinDta, MaxDta,
    EstqPeca, EstqPeso, EstqArM2], [
    Controle, SetorID], True);
    //
    //////// CURTIMENTO //////////////////////////////////////////////////////////
    //
    SetorID  := Integer(TEstqSetorID.esiCurtimento);
    SQLType  := ObtemSQLType(SetorID);
    Pecas    := PecasCur;
    PesoKg   := PesoKgCur;
    AreaM2   := AreaM2Cur;
    CusPQ    := CusPQCur;
    Fuloes   := FuloesCur;
    MinDta   := MinDtaCur;
    MaxDta   := MaxDtaCur;
    EstqPeca := QrVMIPecas.Value  - Pecas;
    EstqPeso := QrVMIPesoKg.Value - PesoKg;
    EstqArM2 := QrVMIAreaM2.Value - AreaM2;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsproqui', False, [
    'Codigo', 'MovimCod', 'MovimNiv',
    'MovimTwn', 'Empresa', 'Terceiro',
    'MovimID', 'Pecas',
    'PesoKg', 'AreaM2', 'CusPQ',
    'Fuloes', 'MinDta', 'MaxDta',
    'EstqPeca', 'EstqPeso', 'EstqArM2'], [
    'Controle', 'SetorID'], [
    Codigo, MovimCod, MovimNiv,
    MovimTwn, Empresa, Terceiro,
    MovimID, Pecas,
    PesoKg, AreaM2, CusPQ,
    Fuloes, MinDta, MaxDta,
    EstqPeca, EstqPeso, EstqArM2], [
    Controle, SetorID], True) then
      ; // Nao!!!!
      //VS_CRC_PF.AtualizaVSValorT(Controle);
    //
  end else
  if TEstqMovimID(QrVMIMovimID.Value) = emidEmProcWE then
  begin
    while not QrSCus2.Eof do
    begin
      //if QrSCus2Setor.Value = Dmod.QrControleSetorRec.Value then
      begin
        //SetorID   := Integer(TEstqSetorID.esiRecurtimento);
        PecasRec  := PecasRec  + QrSCus2Pecas.Value;
        PesoKgRec := PesoKgRec + QrSCus2Peso.Value;
        CusPQRec  := CusPQRec  + QrSCus2Custo.Value;
        FuloesRec := FuloesRec + QrSCus2Fuloes.Value;
        MinDtaRec := Geral.FDT(QrSCus2MinDta.Value, 1);
        MaxDtaRec := Geral.FDT(QrSCus2MaxDta.Value, 1);
        AreaM2Rec := 0;
      end;
      //else Geral.MB_Aviso(
      //'Setor inv�lido para caleiro ou curtimento na defini��o estoque de couros na emiss�o de pesagem!');
      //
      QrSCus2.Next;
    end;
    //////// RECURTIMENTO ////////////////////////////////////////////////////////
    //
    SetorID  := Integer(TEstqSetorID.esiRecurtimento);
    SQLType  := ObtemSQLType(SetorID);
    //
    Pecas    := PecasRec;
    PesoKg   := PesoKgRec;
    AreaM2   := AreaM2Rec;
    CusPQ    := CusPQRec;
    Fuloes   := FuloesRec;
    MinDta   := MinDtaRec;
    MaxDta   := MaxDtaRec;
    EstqPeca := QrVMIPecas.Value  - Pecas;
    EstqPeso := QrVMIPesoKg.Value - PesoKg;
    EstqArM2 := QrVMIAreaM2.Value - AreaM2;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsproqui', False, [
    'Codigo', 'MovimCod', 'MovimNiv',
    'MovimTwn', 'Empresa', 'Terceiro',
    'MovimID', 'Pecas',
    'PesoKg', 'AreaM2', 'CusPQ',
    'Fuloes', 'MinDta', 'MaxDta',
    'EstqPeca', 'EstqPeso', 'EstqArM2'], [
    'Controle', 'SetorID'], [
    Codigo, MovimCod, MovimNiv,
    MovimTwn, Empresa, Terceiro,
    MovimID, Pecas,
    PesoKg, AreaM2, CusPQ,
    Fuloes, MinDta, MaxDta,
    EstqPeca, EstqPeso, EstqArM2], [
    Controle, SetorID], True) then
      VS_CRC_PF.AtualizaVSValorT(Controle);
  end else
  Geral.MB_Aviso(
  'ID de Movimento n�o implementado na defini��o estoque de couros na emiss�o de pesagem!');
end;

function TDmModVS_CRC.AtualizaVSMovIts_VS_BH(MovimCod: Integer;
  MovimID: TEstqMovimID): Boolean;
const
  sProcName = 'DmModVS.AtualizaVSMovIts_VS_BH()';
var
  CustoKg, Peso, CustoPQ, ValorMP, ValorT, KgCouPQ, SumEmitCusto, SumEmitPeso: Double;
  Controle: Integer;
begin
  SumEmitCusto := 0;
  SumEmitPeso  := 0;
{$IfDef sAllVS}
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumEmit, Dmod.MyDB, [
  'SELECT SUM(Custo) Custo, ',
  'SUM(IF(Retrabalho=0, Peso, 0)) Peso ',
  'FROM emit ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  SumEmitCusto := SumEmitCusto + QrSumEmitCusto.Value;
  SumEmitPeso  := SumEmitPeso  + QrSumEmitPeso.Value;
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumEmit, Dmod.MyDB, [
  'SELECT SUM(CustoTotal) Custo, 0.000 Peso ',
  'FROM pqo ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  SumEmitCusto := SumEmitCusto + QrSumEmitCusto.Value;
  SumEmitPeso  := SumEmitPeso  + QrSumEmitPeso.Value;
  QrSumEmit.Close;
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumVMI, Dmod.MyDB, [
  'SELECT SUM(QtdAntPeso) Qtde ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv IN (' +
    Geral.FF0(Integer(eminSorcCon)) + ',' +
    Geral.FF0(Integer(eminSorcCal)) + ',' +
    Geral.FF0(Integer(eminSorcCur)) +
  ')',
  '']);
  //
  if QrSumVMIQtde.Value > 0 then
    CustoKg := SumEmitCusto / QrSumVMIQtde.Value
  else
    CustoKg := 0;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrVMIs, Dmod.MyDB, [
  'SELECT Controle, QtdAntPeso Qtde, ValorMP ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv IN (' +
    Geral.FF0(Integer(eminSorcCon)) + ',' +
    Geral.FF0(Integer(eminSorcCal)) + ',' +
    Geral.FF0(Integer(eminSorcCur)) +
  ')',
  '']);
  //
  while not QrVMIs.Eof do
  begin
    if QrSumVMIQtde.Value = 0 then
      KgCouPQ  := 0
    else
      KgCouPQ  := (QrVMIsQtde.Value / QrSumVMIQtde.Value) * SumEmitPeso;
    CustoPQ  := -QrVMIsQtde.Value * CustoKg;
    // ini 2023-04-15
    //ValorT   := CustoPQ + QrVMIsValorMP.Value;
    ValorMP  := QrVMIsValorMP.Value;
    ValorT   := ValorMP + CustoPQ;
    // fim  2023-04-15
    Controle := QrVMIsControle.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'KgCouPQ', 'CustoPQ', 'ValorT'], [
    'Controle'], [
    KgCouPQ, CustoPQ, ValorT], [
    Controle], True);
    //
    QrVMIs.Next;
  end;
end;

function TDmModVS_CRC.AtualizaVSMovIts_VS_BHWE(MovimCod: Integer): Boolean;
const
  sProcName = 'DmModVS.AtualizaVSMovIts_VS_BHWE()';
var
  MovimID: TEstqMovimID;
  NomeTab: String;
  Codigo, SrcNivel2: Integer;
  //
  function Mensagem(ID: Integer): String;
  begin
    Geral.MB_Erro('"MovimID" ' +
    GetEnumName(TypeInfo(TEstqMovimID), Integer(MovimID)) + ' [' +
    Geral.FF0(ID) + '] n�o definido em ' + sProcName);
  end;
  //
begin
  //UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovCab, Dmod.MyDB, [
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAu2, DModG.MyCompressDB, [
  'SELECT MovimID ',
  'FROM vsmovcab ',
  'WHERE Codigo=' + Geral.FF0(MovimCod),
  '']);
  //
  //MovimID := TEstqMovimID(QrVSMovCab.FieldByName('MovimID').AsInteger);
  MovimID := TEstqMovimID(USQLDB.Int(DqAu2, 'MovimID'));
  //
  VS_CRC_PF.AdicionarNovosVS_emid();
  case MovimID of
    (*01*)emidCompra     : ; // Nada!
    (*02*)emidVenda     : ; // Nada!
    (*06*)emidIndsXX     : ; // Nada!
    (*11*)emidEmOperacao : ; // Nada?
    (*16*)emidEntradaPlC : ; // Nada!
    (*21*)emidDevolucao  : ; // Nada?
    (*22*)emidRetrabalho : ; // Nada?
    (*25*)emidTransfLoc : ; // Nada?
    emidEmProcCon,
    emidEmProcCal,
    emidEmProcCur:   AtualizaVSMovIts_VS_BH(MovimCod, MovimID);
    emidEmProcWE:    AtualizaVSMovIts_VS_WE(MovimCod, MovimID);
    emidEmProcSP:    AtualizaVSMovIts_VS_SP(MovimCod, MovimID);
    emidEmReprRM:    AtualizaVSMovIts_VS_RM(MovimCod, MovimID);
    else
      Mensagem(1);
  end;
  case MovimID of
    emidEmProcCon:   VS_CRC_PF.AtualizaTotaisVSConCab(MovimCod);
    emidEmProcCal:   VS_CRC_PF.AtualizaTotaisVSCalCab(MovimCod);
    emidEmProcCur:   VS_CRC_PF.AtualizaTotaisVSCurCab(MovimCod);
    emidEmOperacao:  VS_CRC_PF.AtualizaTotaisVSOpeCab(MovimCod);
    emidEmProcWE:    VS_CRC_PF.AtualizaTotaisVSPWECab(MovimCod);
    emidEmProcSP:    VS_CRC_PF.AtualizaTotaisVSPSPCab(MovimCod);
    emidEmReprRM:    VS_CRC_PF.AtualizaTotaisVSRRMCab(MovimCod);
    (*01*)emidCompra,
    (*02*)emidVenda,
    (*06*)emidEntradaPlC,
    (*21*)emidDevolucao,
    (*22*)emidRetrabalho,
    (*25*)emidTransfLoc:
    begin
      NomeTab := VS_CRC_PF.ObtemNomeTabelaVSXxxCab(MovimID, True);
      VS_CRC_PF.AtualizaTotaisVSXxxCab(NomeTab, MovimCod);
    end;
    emidIndsXX:
    begin
      UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAu2, DModG.MyCompressDB, [
      'SELECT Codigo, SrcNivel2 ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestCurtiXX)),
      '']);
      Codigo    := USQLDB.Int(DqAu2, 'Codigo');
      SrcNivel2 := USQLDB.Int(DqAu2, 'SrcNivel2');
      //
      VS_CRC_PF.DistribuiCustoIndsVS(eminDestCurtiXX, MovimCod, Codigo, SrcNivel2);
      // Precisa???
      //VS_CRC_PF.AtualizaSaldoItmCur(OrigSrcNivel2);
      //VS_CRC_PF.AtualizaTotaisVSCurCab(MovimCod);
    end;
    else
      Mensagem(2);
  end;
end;

function TDmModVS_CRC.AtualizaVSMovIts_VS_RM(MovimCod: Integer;
  MovimID: TEstqMovimID): Boolean;
const
  sProcName = 'DmModVS.AtualizaVSMovIts_VS_RM()';
var
  CustoKg, Peso, CustoPQ, ValorT, KgCouPQ, SumEmitCusto, SumEmitPeso: Double;
  Controle: Integer;
  Campo: String;
  //Grandeza: TGrandezaUnidMed;
  //
  procedure ReopenSumVMI();
  begin
    UnDMkDAC_PF.AbreMySQLQuery0(QrSumVMI, Dmod.MyDB, [
    'SELECT SUM(-' + Campo + ') Qtde ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv IN (' +
      Geral.FF0(Integer(eminSorcRRM)) +
    ')',
    '']);
  end;
begin
  SumEmitCusto := 0;
  SumEmitPeso  := 0;
{$IfDef sAllVS}
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumEmit, Dmod.MyDB, [
  'SELECT SUM(Custo) Custo, ',
  'SUM(IF(Retrabalho=0, Peso, 0)) Peso ',
  'FROM emit ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  SumEmitCusto := SumEmitCusto + QrSumEmitCusto.Value;
  SumEmitPeso  := SumEmitPeso  + QrSumEmitPeso.Value;
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumEmit, Dmod.MyDB, [
  'SELECT SUM(CustoTotal) Custo, 0.000 Peso ',
  'FROM pqo ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  SumEmitCusto := SumEmitCusto + QrSumEmitCusto.Value;
  SumEmitPeso  := SumEmitPeso  + QrSumEmitPeso.Value;
  QrSumEmit.Close;
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
  //
  Campo := 'AreaM2';
  ReopenSumVMI();
  //
  CustoKg := 0;
  if QrSumVMIQtde.Value > 0 then
    CustoKg := SumEmitCusto / QrSumVMIQtde.Value
  else
  begin
    //Grandeza := gumPesoKG;
    Campo := 'PesoKg';
    ReopenSumVMI();
  end;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrVMIs, Dmod.MyDB, [
  'SELECT Controle, -' + Campo + ' Qtde, ValorMP ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv IN (' +
    Geral.FF0(Integer(eminSorcRRM)) +
  ')',
  '']);
  //
  while not QrVMIs.Eof do
  begin
    if QrSumVMIQtde.Value = 0 then
      KgCouPQ := 0
    else
      KgCouPQ := (QrVMIsQtde.Value / QrSumVMIQtde.Value) * SumEmitPeso;
    CustoPQ  := -QrVMIsQtde.Value * CustoKg;
    //ValorT   := CustoPQ + QrVMIsValorMP.Value;
    Controle := QrVMIsControle.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'KgCouPQ', 'CustoPQ'(*, 'ValorT'*)], [
    'Controle'], [
    KgCouPQ, CustoPQ(*, ValorT*)], [
    Controle], True);
    //
    QrVMIs.Next;
  end;
end;

function TDmModVS_CRC.AtualizaVSMovIts_VS_SP(MovimCod: Integer;
  MovimID: TEstqMovimID): Boolean;
const
  sProcName = 'DmModVS.AtualizaVSMovIts_VS_SP()';
var
  CustoKg, Peso, CustoPQ, ValorT, KgCouPQ, SumEmitCusto, SumEmitPeso: Double;
  Controle: Integer;
begin
  SumEmitCusto := 0;
  SumEmitPeso  := 0;
{$IfDef sAllVS}
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumEmit, Dmod.MyDB, [
  'SELECT SUM(Custo) Custo, ',
  'SUM(IF(Retrabalho=0, Peso, 0)) Peso ',
  'FROM emit ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  SumEmitCusto := SumEmitCusto + QrSumEmitCusto.Value;
  SumEmitPeso  := SumEmitPeso  + QrSumEmitPeso.Value;
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumEmit, Dmod.MyDB, [
  'SELECT SUM(CustoTotal) Custo, 0.000 Peso ',
  'FROM pqo ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  SumEmitCusto := SumEmitCusto + QrSumEmitCusto.Value;
  SumEmitPeso  := SumEmitPeso  + QrSumEmitPeso.Value;
  QrSumEmit.Close;
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumVMI, Dmod.MyDB, [
  'SELECT SUM(-PesoKg) Qtde ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv IN (' +
    //Geral.FF0(Integer(eminSorcCur)) + ',' +
    //Geral.FF0(Integer(eminSorcCal)) + ',' +
    Geral.FF0(Integer(eminSorcPSP)) +
  ')',
  '']);
  //Geral.MB_SQL(nil, QrSumVMI);
  //
  if QrSumVMIQtde.Value > 0 then
    CustoKg := SumEmitCusto / QrSumVMIQtde.Value
  else
    CustoKg := 0;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrVMIs, Dmod.MyDB, [
  'SELECT Controle, -PesoKg Qtde, -ValorMP ValorMP',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv IN (' +
    //Geral.FF0(Integer(eminSorcCur)) + ',' +
    //Geral.FF0(Integer(eminSorcCal)) + ',' +
    Geral.FF0(Integer(eminSorcPSP)) +
  ')',
  '']);
  //
  while not QrVMIs.Eof do
  begin
    if QrSumVMIQtde.Value = 0 then
      KgCouPQ  := 0
    else
      KgCouPQ  := (QrVMIsQtde.Value / QrSumVMIQtde.Value) * SumEmitPeso;
    CustoPQ  := -QrVMIsQtde.Value * CustoKg;
    ValorT   := CustoPQ + QrVMIsValorMP.Value;
    Controle := QrVMIsControle.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'KgCouPQ', 'CustoPQ', 'ValorT'], [
    'Controle'], [
    KgCouPQ, CustoPQ, ValorT], [
    Controle], True);
    //
    QrVMIs.Next;
  end;
end;

function TDmModVS_CRC.AtualizaVSMovIts_VS_WE(MovimCod: Integer;
  MovimID: TEstqMovimID): Boolean;
const
  sProcName = 'DmModVS.AtualizaVSMovIts_VS_WE()';
var
  CustoUnit, Peso, CustoPQ, ValorT, KgCouPQ, SumEmitCusto, SumEmitPeso: Double;
  Controle: Integer;
  Campo, Sinal: String;
  MovimNiv: TEstqMovimNiv;
  //TipoCalcCouro: TTipoCalcCouro;
  //
  procedure ReopenVMIs();
  begin
    UnDMkDAC_PF.AbreMySQLQuery0(QrVMIs, Dmod.MyDB, [
    'SELECT Controle, -' + Campo + ' Qtde, ValorMP ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv IN (' +
      Geral.FF0(Integer(MovimNiv)) +
    ')',
    '']);
  end;
  procedure ReopenSumVMI();
  begin
    UnDMkDAC_PF.AbreMySQLQuery0(QrSumVMI, Dmod.MyDB, [
    'SELECT SUM(' + Sinal + Campo + ') Qtde ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv IN (' +
      Geral.FF0(Integer(MovimNiv)) +
    ')',
    '']);
  end;
begin
  SumEmitCusto := 0;
  SumEmitPeso  := 0;
{$IfDef sAllVS}
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumEmit, Dmod.MyDB, [
  'SELECT SUM(Custo) Custo, ',
  'SUM(IF(Retrabalho=0, Peso, 0)) Peso ',
  'FROM emit ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  SumEmitCusto := SumEmitCusto + QrSumEmitCusto.Value;
  SumEmitPeso  := SumEmitPeso  + QrSumEmitPeso.Value;
  UnDMkDAC_PF.AbreMySQLQuery0(QrSumEmit, Dmod.MyDB, [
  'SELECT SUM(CustoTotal) Custo, 0.000 Peso ',
  'FROM pqo ',
  'WHERE VSMovCod=' + Geral.FF0(MovimCod),
  '']);
  SumEmitCusto := SumEmitCusto + QrSumEmitCusto.Value;
  SumEmitPeso  := SumEmitPeso  + QrSumEmitPeso.Value;
  QrSumEmit.Close;
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
  //////////////////////////////////////////////////////////////////////////////
  // Custo Baixa!
  //////////////////////////////////////////////////////////////////////////////
  Sinal := '-';
  Campo := 'AreaM2';
  MovimNiv := TEstqMovimNiv.eminSorcWEnd;
  //TipoCalcCouro := ptcAreaM2; // = 3
  ReopenSumVMI();
  //
  CustoUnit := 0;
  if QrSumVMIQtde.Value > 0 then
    CustoUnit := SumEmitCusto / QrSumVMIQtde.Value
  else
  begin
    Campo := 'PesoKg';
    MovimNiv := TEstqMovimNiv.eminSorcWEnd;
    //TipoCalcCouro := ptcAreaM2; // = 3
    ReopenSumVMI();
    if QrSumVMIQtde.Value > 0 then
      CustoUnit := SumEmitCusto / QrSumVMIQtde.Value
  end;
  //
  ReopenVMIs();
  //
  while not QrVMIs.Eof do
  begin
    if QrSumVMIQtde.Value = 0 then
      KgCouPQ := 0
    else
      KgCouPQ := (QrVMIsQtde.Value / QrSumVMIQtde.Value) * SumEmitPeso;
    CustoPQ  := -QrVMIsQtde.Value * CustoUnit;
    //ValorT   := CustoPQ + QrVMIsValorMP.Value;
    Controle := QrVMIsControle.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'KgCouPQ', 'CustoPQ'(*, 'ValorT'*)], [
    'Controle'], [
    KgCouPQ, CustoPQ(*, ValorT*)], [
    Controle], True);
    //
    QrVMIs.Next;
  end;
  //////////////////////////////////////////////////////////////////////////////
  // Custo Gerado!
  //////////////////////////////////////////////////////////////////////////////
  Sinal := ''; // +
  Campo := 'AreaM2';
  MovimNiv := TEstqMovimNiv.eminDestWEnd;
  ReopenSumVMI();
  //
  CustoUnit := 0;
  if QrSumVMIQtde.Value > 0 then
    CustoUnit := SumEmitCusto / QrSumVMIQtde.Value
  else
  begin
    Campo := 'PesoKg';
    ReopenSumVMI();
  end;
  //
  ReopenVMIs();
  //
  while not QrVMIs.Eof do
  begin
    if QrSumVMIQtde.Value = 0 then
      KgCouPQ := 0
    else
      KgCouPQ := (QrVMIsQtde.Value / QrSumVMIQtde.Value) * SumEmitPeso;
    CustoPQ  := -QrVMIsQtde.Value * CustoUnit;
    //ValorT   := CustoPQ + QrVMIsValorMP.Value;
    Controle := QrVMIsControle.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'KgCouPQ', 'CustoPQ'(*, 'ValorT'*)], [
    'Controle'], [
    KgCouPQ, CustoPQ(*, ValorT*)], [
    Controle], True);
    //
    QrVMIs.Next;
  end;
end;

procedure TDmModVS_CRC.AtualizaVSMulFrnCabNew(SinalOrig: TSinal;
  DefMulFrn: TEstqDefMulFldEMxx; MovTypeCod: Integer;
  MovimIDs: array of TEstqMovimID; MovimNivsOri,
  MovimNivsDst: array of TEstqMovimNiv);
  //
  function ReInsereNovoVSMulFrnCab(): Integer;
  const
    Pecas    = 0.000;
    //PerPecas = 0.0000;
  var
    Nome: String;
    Codigo, OriIMEC, OriIMEI: Integer;
  begin
    Result         := 0;
    Codigo         := 0;
    Nome           := '';
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFC, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vsmulfrncab',
    'WHERE MovimType=' + Geral.FF0(Integer(DefMulFrn)),
    'AND MovTypeCod=' + Geral.FF0(MovTypeCod),
    '']);
    if QrLocMFCCodigo.Value <> 0 then
    begin
      Codigo := QrLocMFCCodigo.Value;
      // Zera dados dos itens ja existentes!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulfrnits', False, [
      'Pecas'(*, 'PerPecas'*)], ['Codigo'], [
      Pecas(*, PerPecas*)], [Codigo], True) then
        Result := Codigo;
    end else
    begin
      Codigo := UMyMod.BPGS1I32('vsmulfrncab', 'Codigo', '', '', tsPos, stIns, 0);
      // Cria novo pois nao existe ainda!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmulfrncab', False, [
      'Nome', 'MovimType', 'MovTypeCod'], [
      'Codigo'], [
      Nome, DefMulFrn, MovTypeCod], [
      Codigo], True) then
        Result := Codigo;
    end;
  end;
  //
  function ReInsereNovoVSFrnMulIts(Codigo, Remistura, CodRemix, Fornece:
  Integer; OrigPecas: Double; Inverte: Boolean): Integer;
  var
    Controle: Integer;
    SQLType: TSQLType;
    Pecas: Double;
  begin
    //if Inverte and (SinalOrig = siNegativo) then
    if OrigPecas < 0 then
      Pecas := - OrigPecas
    else
      Pecas := OrigPecas;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFI, Dmod.MyDB, [
    'SELECT Controle',
    'FROM vsmulfrnits',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND Remistura=' + Geral.FF0(Remistura),
    'AND CodRemix=' + Geral.FF0(CodRemix),
    'AND Fornece=' + Geral.FF0(Fornece),
    '']);
    if QrLocMFIControle.Value <> 0 then
    begin
      SQLType := stUpd;
      Controle := QrLocMFIControle.Value
    end else
    begin
      SQLType := stIns;
      Controle :=
        UMyMod.BPGS1I32('vsmulfrnits', 'Controle', '', '', tsPos, stIns, 0);
    end;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsmulfrnits', False, [
    'Codigo', 'Remistura', 'CodRemix',
    'Fornece', 'Pecas'(*, 'PerPecas'*)], [
    'Controle'], [
    Codigo, Remistura, CodRemix,
    Fornece, Pecas(*, PerPecas*)], [
    Controle], True);
  end;
var
  SQLMovimID, SQLMovimNiv: String;
  Terceiro, VSMulFrnCab, I: Integer;
  //Terceiros: array of Integer;
  //Pecas: array of Double;
  //
  Codigo, Remistura, CodRemix, Fornece: Integer;
  Pecas, PerPecas, TotMul: Double;
  //
  SiglaVS, Nome, Percentual, FldUpd: String;
  sSrcNivel2: String;
begin
  SQLMovimID  := '';
  FldUpd      := '???';
  for I := Low(MovimIDs) to High(MovimIDs) do
  begin
    if SQLMovimID <> '' then
      SQLMovimID :=  SQLMovimID + ',';
    SQLMovimID :=  Geral.FF0(Integer(MovimIDs[I]));
  end;
  if SQLMovimID <> '' then
    SQLMovimID :=  'AND MovimID IN (' + SQLMovimID + ')';
  //
  SQLMovimNiv  := '';
  for I := Low(MovimNivsOri) to High(MovimNivsOri) do
  begin
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  SQLMovimNiv + ',';
    SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsOri[I]));
  end;
  if SQLMovimNiv <> '' then
    SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
  //
  // IMEIs de origem
  case DefMulFrn of
    TEstqDefMulFldEMxx.edmfSrcNiv2:
    begin
      UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
      '  SELECT SrcNivel2 ',
      '  FROM ' + CO_SEL_TAB_VMI,
      '  WHERE Controle=' + Geral.FF0(MovTypeCod),
      '']);
      //sSrcNivel2 := MyObjects.CordaDeQuery(QrSrcNiv2, 'SrcNivel2', EmptyStr);
      //
(*
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, VSMulFrnCab, Pecas   ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=(',
      '  SELECT SrcNivel2 ',
      '  FROM ' + CO_SEL_TAB_VMI,
      '  WHERE Controle=' + Geral.FF0(MovTypeCod),
      ') ',
      '']);
*)
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, VSMulFrnCab, Pecas   ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(QrSrcNiv2.FieldByName('SrcNivel2').AsInteger),
      '']);
      FldUpd := 'Controle';
    end;
    TEstqDefMulFldEMxx.edmfMovCod:
    begin
      UnDMkDAC_PF.AbreMySQLQuery0(QrUniFrn, Dmod.MyDB, [
      'SELECT Terceiro, VSMulFrnCab, SUM(Pecas) Pecas  ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE MovimCod=' + Geral.FF0(MovTypeCod),
      SQLMovimID,
      SQLMovimNiv,
      'GROUP BY Terceiro, VSMulFrnCab ',
      'ORDER BY Terceiro,  VSMulFrnCab ',
      ' ',
      '']);
      FldUpd := 'MovimCod';
    end;
    else
    begin
      Geral.MB_Erro('"Defini��o de fornecedor abortada!' + sLineBreak +
      '"TEstqMovimType" n�o definido: ' + Geral.FF0(Integer(DefMulFrn)));
      Exit;
    end;
  end;
  //
  //Geral.MB_SQL(nil, QrUniFrn);
  // Se tiver Item sem fornecedor, desiste!
  if (QrUniFrnTerceiro.Value = 0) and (QrUniFrnVSMulFrnCab.Value = 0) then
  begin
    if VAR_MEMO_DEF_VS_MUL_FRN <> nil then
       VAR_MEMO_DEF_VS_MUL_FRN.Text := FldUpd + ' = ' + Geral.FF0(MovTypeCod) +
       sLineBreak + VAR_MEMO_DEF_VS_MUL_FRN.Text
    else
      InformaSemFornecedor();
    Exit;
  end;
  Terceiro    := 0;
  VSMulFrnCab := 0;
  //
  // Se tiver s� um registro...
  if QrUniFrn.RecordCount = 1 then
  begin
    if QrUniFrnTerceiro.Value <> 0 then
      Terceiro := QrUniFrnTerceiro.Value
    else
      VSMulFrnCab := QrUniFrnVSMulFrnCab.Value;
  end else
  // Se tiver varios registros...
  begin
    Terceiro := 0;
    Codigo   := ReInsereNovoVSMulFrnCab();
    while not QrUniFrn.Eof do
    begin
      if (QrUniFrnTerceiro.Value <> 0) and (QrUniFrnVSMulFrnCab.Value = 0) then
      begin
        Remistura := 0; // False
        CodRemix  := 0;
        Fornece   := QrUniFrnTerceiro.Value;
        Pecas     := QrUniFrnPecas.Value;
        ReInsereNovoVSFrnMulIts(Codigo, Remistura, CodRemix, Fornece, Pecas, True);
      end else
      if (QrUniFrnTerceiro.Value = 0) and (QrUniFrnVSMulFrnCab.Value <> 0) then
      begin
        Remistura := 1; // True
        CodRemix  := QrUniFrnVSMulFrnCab.Value;
        //
        UnDMkDAC_PF.AbreMySQLQuery0(QrMulFrn, Dmod.MyDB, [
        'SELECT Fornece, SUM(Pecas) Pecas, "" SiglaVS',
        'FROM vsmulfrnits',
        'WHERE Codigo=' + Geral.FF0(CodRemix),
        'GROUP BY Fornece ',
        '']);
        TotMul := 0;
        QrMulFrn.First;
        while not QrMulFrn.Eof do
        begin
          TotMul := TotMul + QrMulFrnPecas.Value;
          //
          QrMulFrn.Next;
        end;
        QrMulFrn.First;
        while not QrMulFrn.Eof do
        begin
          //Pecas     := QrMulFrnPecas.Value;
          if TotMul <> 0 then
            Pecas := (QrMulFrnPecas.Value / TotMul) * QrUniFrnPecas.Value
          else
            Pecas := 0;
          //
          Fornece   := QrMulFrnFornece.Value;
          ReInsereNovoVSFrnMulIts(Codigo, Remistura, CodRemix, Fornece, Pecas, False);
          //
          QrMulFrn.Next;
        end;
      end;
      //
      QrUniFrn.Next;
    end;
    // Atualiza VSMulFrnCab!
    UnDMkDAC_PF.AbreMySQLQuery0(QrMulFrn, Dmod.MyDB, [
    'SELECT mfi.Fornece, SUM(mfi.Pecas) Pecas, vem.SiglaVS',
    'FROM vsmulfrnits mfi',
    'LEFT JOIN vsentimp vem ON vem.Codigo=mfi.Fornece',
    'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
    'GROUP BY mfi.Fornece',
    'ORDER BY Pecas DESC',
    '']);
    UnDMkDAC_PF.AbreMySQLQuery0(QrTotFrn, Dmod.MyDB, [
    'SELECT SUM(mfi.Pecas) Pecas ',
    'FROM vsmulfrnits mfi',
    'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
    '']);
    Nome := '';
    while not QrMulFrn.Eof do
    begin
      if QrMulFrnSiglaVS.Value = '' then
        SiglaVS := VS_CRC_PF.DefineSiglaVS_Frn(QrMulFrnFornece.Value)
      else
        SiglaVS := QrMulFrnSiglaVS.Value;
      //
      if QrTotFrnPecas.Value <> 0 then
      begin
        PerPecas   := QrMulFrnPecas.Value / QrTotFrnPecas.Value * 100;
        Percentual := Geral.FI64(Round(PerPecas));
      end else
      begin
        PerPecas   := 0;
        Percentual := '??';
      end;
      //
      Nome := Nome + SiglaVS + ' ' + Percentual + '% ';
      //
      QrMulFrn.Next;
    end;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulfrncab', False, [
    'Nome'], ['Codigo'], [
    Nome], [Codigo], True) then
      VSMulFrnCab := Codigo;
  end;
  SQLMovimNiv  := '';
  for I := Low(MovimNivsDst) to High(MovimNivsDst) do
  begin
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  SQLMovimNiv + ',';
    SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsDst[I]));
  end;
  if SQLMovimNiv <> '' then
    SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
  //
  UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE ' + CO_UPD_TAB_VMI,
  'SET Terceiro=' + Geral.FF0(Terceiro) + ', ',
  'VSMulFrnCab=' + Geral.FF0(VSMulFrnCab),
  'WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
  //SQLMovimID, PWE tem Finish
  SQLMovimNiv,
  '']);
  //  Atualizar descendentes!
  UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
  '  SELECT DISTINCT Controle',
  '  FROM ' + CO_SEL_TAB_VMI,
  '  WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
//     SQLMovimID,
     SQLMovimNiv,
  '  AND Controle <> 0 ',
  '']);
  sSrcNivel2 := MyObjects.CordaDeQuery(QrSrcNiv2, 'Controle', EmptyStr);
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrMFSrc, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE SrcNivel2 IN (',
  '  SELECT Controle',
  '  FROM ' + CO_SEL_TAB_VMI,
  '  WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
//     SQLMovimID,
     SQLMovimNiv,
  '  AND Controle <> 0 ',
  ') ',
  '']);
*)
  if Trim(sSrcNivel2) <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrMFSrc, Dmod.MyDB, [
    'SELECT MovimID, MovimNiv, MovimCod, Controle ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE SrcNivel2 IN (' + sSrcNivel2 + ') ',
    '']);
    QrMFSrc.First;
    while not QrMFSrc.Eof do
    begin
      UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE ' + CO_UPD_TAB_VMI,
      'SET Terceiro=' + Geral.FF0(Terceiro) + ', ',
      'VSMulFrnCab=' + Geral.FF0(VSMulFrnCab),
      'WHERE Controle=' + Geral.FF0(QrMFSrcControle.Value),
      '']);
      //
      QrMFSrc.Next;
    end;
  end;
(*
  // Orfaos!
  UnDMkDAC_PF.AbreMySQLQuery0(QrMFOrfaos, Dmod.MyDB, [
  'SELECT Controle, Terceiro, VSMulFrnCab',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE Controle IN ( ',
  '  SELECT SrcNivel2 ',
  '  FROM ' + CO_SEL_TAB_VMI,
  '  WHERE SrcNivel2<>0',
  '  AND Terceiro=0',
  '  AND VSMulFrnCab=0',
  ') ',
  'AND (',
  '  Terceiro<>0',
  '  OR',
  '  VSMulFrnCab<>0',
  ')',
  '']);
  QrMFOrfaos.First;
  while not QrMFOrfaos.Eof do
  begin
    UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ' + CO_UPD_TAB_VMI,
    'SET Terceiro=' + Geral.FF0(QrMFOrfaosTerceiro.Value) + ', ',
    'VSMulFrnCab=' + Geral.FF0(QrMFOrfaosVSMulFrnCab.Value),
    'WHERE SrcNivel2<>0 ',
    'AND SrcNivel2=' + Geral.FF0(QrMFOrfaosControle.Value),
    'AND Terceiro=0 ',
    'AND VSMulFrnCab=0 ',
    '']);
    //
    QrMFOrfaos.Next;
  end;
*)
end;

procedure TDmModVS_CRC.AtualizaVSMulNFeCab(SinalOrig: TSinal;
  DefMulNFe: TEstqDefMulFldEMxx; MovTypeCod: Integer; MovimID: TEstqMovimID;
  MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv);
 //
  function ReInsereNovoVSMulNFeCab(): Integer;
  const
    Pecas    = 0.000;
    //PerPecas = 0.0000;
  var
    Nome: String;
    Codigo, OriIMEC, OriIMEI: Integer;
  begin
    Result         := 0;
    Codigo         := 0;
    Nome           := '';
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFC, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vsmulnfecab',
    'WHERE MovimType=' + Geral.FF0(Integer(DefMulNFe)),
    'AND MovTypeCod=' + Geral.FF0(MovTypeCod),
    '']);
    if QrLocMFCCodigo.Value <> 0 then
    begin
      Codigo := QrLocMFCCodigo.Value;
      // Zera dados dos itens ja existentes!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulnfeits', False, [
      'Pecas'(*, 'PerPecas'*)], ['Codigo'], [
      Pecas(*, PerPecas*)], [Codigo], True) then
        Result := Codigo;
    end else
    begin
      Codigo := UMyMod.BPGS1I32('vsmulnfecab', 'Codigo', '', '', tsPos, stIns, 0);
      // Cria novo pois nao existe ainda!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmulnfecab', False, [
      'Nome', 'MovimType', 'MovTypeCod'], [
      'Codigo'], [
      Nome, DefMulNFe, MovTypeCod], [
      Codigo], True) then
        Result := Codigo;
    end;
  end;
  //
  function ReInsereNovoVSNFeMulIts(Codigo, Remistura, CodRemix, NFeSer, NFeNum:
  Integer; OrigPecas: Double; Inverte: Boolean): Integer;
  var
    Controle: Integer;
    SQLType: TSQLType;
    Pecas: Double;
  begin
    //if Inverte and (SinalOrig = siNegativo) then
    if OrigPecas < 0 then
      Pecas := - OrigPecas
    else
      Pecas := OrigPecas;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLocMFI, Dmod.MyDB, [
    'SELECT Controle',
    'FROM vsmulnfeits',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND Remistura=' + Geral.FF0(Remistura),
    'AND CodRemix=' + Geral.FF0(CodRemix),
    'AND NFeSer=' + Geral.FF0(NFeSer),
    'AND NFeNum=' + Geral.FF0(NFeNum),
    '']);
    if QrLocMFIControle.Value <> 0 then
    begin
      SQLType := stUpd;
      Controle := QrLocMFIControle.Value
    end else
    begin
      SQLType := stIns;
      Controle :=
        UMyMod.BPGS1I32('vsmulnfeits', 'Controle', '', '', tsPos, stIns, 0);
    end;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsmulnfeits', False, [
    'Codigo', 'Remistura', 'CodRemix',
    'NFeSer', 'NFeNum',
    'Pecas'(*, 'PerPecas'*)], [
    'Controle'], [
    Codigo, Remistura, CodRemix,
    NFeSer, NFeNum,
    Pecas(*, PerPecas*)], [
    Controle], True);
  end;
var
  SQLMovimID, SQLMovimNiv: String;
  VSMulNFeCab, I: Integer;
  //
  Codigo, Remistura, CodRemix, NFeSer, NFeNum: Integer;
  Pecas, PerPecas, TotMul: Double;
  //
  Nome, Percentual, FldUpd: String;
  sSrcNivel2, SiglaNFe: String;
  //
  TabCab, FldCabSer, FldCabdNum: String;
begin
  if MyObjects.FIC(DefMulNFe <> edmfMovCod, nil,
  '"ID" deve ser do tipo "edmfMovCod"!') then
    Exit;
  TabCab := VS_CRC_PF.ObtemNomeTabelaVSXxxCab(MovimID);
  VS_CRC_PF.ObtemNomeCamposNFeVSXxxCab(MovimID, FldCabSer, FldCabdNum);
  //
  FldUpd := 'MovimCod';
  NFeSer := 0;
  if MovimID = TEstqMovimID.emidIndsXX then
  begin
    NFeSer      := 0;
    NFeNum      := 0;
    VSMulNFeCab := 0;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSCabNFe, Dmod.MyDB, [
    'SELECT ' + FldCabSer + ' Serie, ' + FldCabdNum + ' nNF ',
    'FROM ' + TabCab,
    'WHERE MovimCod=' + Geral.FF0(MovTypeCod),
    '']);
    if QrVSCabNFenNF.Value <> 0 then
    begin
      NFeSer      := QrVSCabNFeSerie.Value;
      NFeNum      := QrVSCabNFenNF.Value;
      VSMulNFeCab := 0;
    end;
  end;
  if NFeNum = 0 then
  begin
    SQLMovimID :=  'AND MovimID=' + Geral.FF0(Integer(MovimID));
    //
    SQLMovimNiv  := '';
    for I := Low(MovimNivsOri) to High(MovimNivsOri) do
    begin
      if SQLMovimNiv <> '' then
        SQLMovimNiv :=  SQLMovimNiv + ',';
      SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsOri[I]));
    end;
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
    //
    // IMEIs de origem
    case DefMulNFe of
      TEstqDefMulFldEMxx.edmfSrcNiv2:
      begin
        UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
        '  SELECT SrcNivel2 ',
        '  FROM ' + CO_SEL_TAB_VMI,
        '  WHERE Controle=' + Geral.FF0(MovTypeCod),
        '']);
        //
        UnDMkDAC_PF.AbreMySQLQuery0(QrUniNFe, Dmod.MyDB, [
        'SELECT NFeSer, NFeNum, VSMulNFeCab, Pecas   ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE Controle=' + Geral.FF0(QrSrcNiv2.FieldByName('SrcNivel2').AsInteger),
        '']);
        FldUpd := 'Controle';
      end;
      TEstqDefMulFldEMxx.edmfMovCod:
      begin
        UnDMkDAC_PF.AbreMySQLQuery0(QrUniNFe, Dmod.MyDB, [
        'SELECT NFeSer, NFeNum, VSMulNFeCab, SUM(Pecas) Pecas  ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE MovimCod=' + Geral.FF0(MovTypeCod),
        SQLMovimID,
        SQLMovimNiv,
        'GROUP BY NFeNum, VSMulNFeCab ',
        'ORDER BY NFeNum, VSMulNFeCab ',
        ' ',
        '']);
        FldUpd := 'MovimCod';
      end;
      else
      begin
        Geral.MB_Erro('"Defini��o de NFe abortada!' + sLineBreak +
        '"TEstqMovimType" n�o definido: ' + Geral.FF0(Integer(DefMulNFe)));
        Exit;
      end;
    end;
    //
    //Geral.MB_SQL(nil, QrUniNFe);
    // Se tiver Item sem NFe, desiste!
    if (QrUniNFeNFeNum.Value = 0) and (QrUniNFeVSMulNFeCab.Value = 0) then
    begin
      if VAR_MEMO_DEF_VS_MUL_NFE <> nil then
         VAR_MEMO_DEF_VS_MUL_NFE.Text := FldUpd + ' = ' + Geral.FF0(MovTypeCod) +
         sLineBreak + VAR_MEMO_DEF_VS_MUL_NFE.Text
      else
        InformaSemNFe();
      Exit;
    end;
    NFeSer      := 0;
    NFeNum      := 0;
    VSMulNFeCab := 0;
    //
    // Se tiver s� um registro...
    if QrUniNFe.RecordCount = 1 then
    begin
      if QrUniNFeNFeNum.Value <> 0 then
      begin
        NFeSer := QrUniNFeNFeSer.Value;
        NFeNum := QrUniNFeNFeNum.Value;
      end else
        VSMulNFeCab := QrUniNFeVSMulNFeCab.Value;
    end else
    // Se tiver varios registros...
    begin
      NFeSer   := 0;
      NFeNum   := 0;
      Codigo   := ReInsereNovoVSMulNFeCab();
      while not QrUniNFe.Eof do
      begin
        if (QrUniNFeNFeNum.Value <> 0) and (QrUniNFeVSMulNFeCab.Value = 0) then
        begin
          Remistura := 0; // False
          CodRemix  := 0;
          NFeSer    := QrUniNFeNFeSer.Value;
          NFeNum    := QrUniNFeNFeNum.Value;
          Pecas     := QrUniNFePecas.Value;
          ReInsereNovoVSNFeMulIts(Codigo, Remistura, CodRemix, NFeSer, NFeNum, Pecas, True);
        end else
        if (QrUniNFeNFeNum.Value = 0) and (QrUniNFeVSMulNFeCab.Value <> 0) then
        begin
          Remistura := 1; // True
          CodRemix  := QrUniNFeVSMulNFeCab.Value;
          //
          UnDMkDAC_PF.AbreMySQLQuery0(QrMulNFe, Dmod.MyDB, [
          'SELECT MAX(NFeSer) NFeSer, NFeNum, SUM(Pecas) Pecas',
          'FROM vsmulnfeits',
          'WHERE Codigo=' + Geral.FF0(CodRemix),
          'GROUP BY NFeNum ',
          '']);
          TotMul := 0;
          QrMulNFe.First;
          while not QrMulNFe.Eof do
          begin
            TotMul := TotMul + QrMulNFePecas.Value;
            //
            QrMulNFe.Next;
          end;
          QrMulNFe.First;
          while not QrMulNFe.Eof do
          begin
            //Pecas     := QrMulNFePecas.Value;
            if TotMul <> 0 then
              Pecas := (QrMulNFePecas.Value / TotMul) * QrUniNFePecas.Value
            else
              Pecas := 0;
            //
            NFeSer   := QrMulNFeNFeSer.Value;
            NFeNum   := QrMulNFeNFeNum.Value;
            ReInsereNovoVSNFeMulIts(Codigo, Remistura, CodRemix, NFeSer, NFeNum, Pecas, False);
            //
            QrMulNFe.Next;
          end;
        end;
        //
        QrUniNFe.Next;
      end;
      // Atualiza VSMulNFeCab!
      UnDMkDAC_PF.AbreMySQLQuery0(QrMulNFe, Dmod.MyDB, [
      'SELECT mfi.NFeSer, mfi.NFeNum, ',
      'SUM(mfi.Pecas) Pecas ',
      'FROM vsmulnfeits mfi',
      'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
      'GROUP BY mfi.NFeNum',
      'ORDER BY Pecas DESC',
      '']);
      UnDMkDAC_PF.AbreMySQLQuery0(QrTotNFe, Dmod.MyDB, [
      'SELECT SUM(mfi.Pecas) Pecas ',
      'FROM vsmulnfeits mfi',
      'WHERE mfi.Codigo=' + Geral.FF0(Codigo),
      '']);
      Nome := '';
      while not QrMulNFe.Eof do
      begin
        SiglaNFe := VS_CRC_PF.DefineSiglaVS_NFe(QrMulNFeNFeSer.Value, QrMulNFeNFeNum.Value);
        //
        if QrTotNFePecas.Value <> 0 then
        begin
          PerPecas   := QrMulNFePecas.Value / QrTotNFePecas.Value * 100;
          Percentual := Geral.FI64(Round(PerPecas));
        end else
        begin
          PerPecas   := 0;
          Percentual := '??';
        end;
        //
        Nome := Nome + SiglaNFe + ' ' + Percentual + '% ';
        //
        QrMulNFe.Next;
      end;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsmulnfecab', False, [
      'Nome'], ['Codigo'], [
      Nome], [Codigo], True) then
        VSMulNFeCab := Codigo;
    end;
  end;
  SQLMovimNiv  := '';
  for I := Low(MovimNivsDst) to High(MovimNivsDst) do
  begin
    if SQLMovimNiv <> '' then
      SQLMovimNiv :=  SQLMovimNiv + ',';
    SQLMovimNiv :=  SQLMovimNiv + Geral.FF0(Integer(MovimNivsDst[I]));
  end;
  if SQLMovimNiv <> '' then
    SQLMovimNiv :=  'AND MovimNiv IN (' + SQLMovimNiv + ')';
  //
  UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE ' + CO_UPD_TAB_VMI,
  'SET NFeSer=' + Geral.FF0(NFeSer) + ', ',
  'NFeNum=' + Geral.FF0(NFeNum) + ', ',
  'VSMulNFeCab=' + Geral.FF0(VSMulNFeCab),
  'WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
  //SQLMovimID, PWE tem Finish
  SQLMovimNiv,
  '']);
  //  Atualizar descendentes!
  UnDMkDAC_PF.AbreMySQLQuery0(QrSrcNiv2, Dmod.MyDB, [
  '  SELECT DISTINCT Controle',
  '  FROM ' + CO_SEL_TAB_VMI,
  '  WHERE ' + FldUpd + '=' + Geral.FF0(MovTypeCod),
//     SQLMovimID,
     SQLMovimNiv,
  '  AND Controle <> 0 ',
  '']);
  sSrcNivel2 := MyObjects.CordaDeQuery(QrSrcNiv2, 'Controle', EmptyStr);
  //
  if Trim(sSrcNivel2) <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrMFSrc, Dmod.MyDB, [
    'SELECT MovimID, MovimNiv, MovimCod, Controle ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE SrcNivel2 IN (' + sSrcNivel2 + ') ',
    '']);
    QrMFSrc.First;
    while not QrMFSrc.Eof do
    begin
      case TEstqMovimID(QrMFSrcMovimID.Value) of
        (*6*)emidIndsXX:
        begin
          UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'UPDATE ' + CO_UPD_TAB_VMI,
          'SET NFeSer=' + Geral.FF0(NFeSer) + ', ',
          'NFeNum=' + Geral.FF0(NFeNum) + ', ',
          'VSMulNFeCab=' + Geral.FF0(VSMulNFeCab),
          'WHERE MovimCod=' + Geral.FF0(QrMFSrcMovimCod.Value),
          '']);
        end;
        else
        begin
          UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'UPDATE ' + CO_UPD_TAB_VMI,
          'SET NFeSer=' + Geral.FF0(NFeSer) + ', ',
          'NFeNum=' + Geral.FF0(NFeNum) + ', ',
          'VSMulNFeCab=' + Geral.FF0(VSMulNFeCab),
          'WHERE Controle=' + Geral.FF0(QrMFSrcControle.Value),
          '']);
        end;
      end;
      //
      QrMFSrc.Next;
    end;
  end;
end;

procedure TDmModVS_CRC.CalculaFreteVMI(Controle: Integer);
var
  CustoFrete: Double;
begin
  if Controle <> 0 then
  begin
    CustoFrete := 0;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrVMIFrete, Dmod.MyDB, [
    'SELECT SUM(CFTMP_ValorT) CustoTot ',
    'FROM vsmoenvenv ',
    'WHERE VSVMI_Controle=' + Geral.FF0(Controle),
    ' ',
    'UNION ',
    ' ',
    'SELECT SUM(CFTPA_ValorT) CustoTot ',
    'FROM vsmoenvret ',
    'WHERE VSVMI_Controle=' + Geral.FF0(Controle),
    '']);
    //
    QrVMIFrete.First;
    while not QrVMIFrete.Eof do
    begin
      CustoFrete := CustoFrete + QrVMIFreteCustoTot.Value;
      //
      QrVMIFrete.Next;
    end;
    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'CustoFrete'], [
    'Controle'], [
    CustoFrete], [
    Controle], True);
  end;
end;

procedure TDmModVS_CRC.CalculaFreteVMIdeMOEnvAvu(MovimCod: Integer);
begin
// Nada???
end;

procedure TDmModVS_CRC.CalculaFreteVMIdeMOEnvEnv(MovimCod: Integer(*;
  EstqEnvKnd: TEstqEnvKnd; MovimID: TEstqMovimID*));
const
  sProcName = 'TDmModVS_CRC.CalculaFreteVMIdeMOEnvEnv()';
  //
  procedure CalculaFreteEnvioIndustrializacao();
  begin
    // Nada! Calcular s� depois de industrializado!
  end;
begin
{
  case EstqEnvKnd of
    TEstqEnvKnd.eekIdustrlz: CalculaFreteEnvioIndustrializacao();
    //TEstqEnvKnd.eekTransf
    //TEstqEnvKnd = (eekIndef
    else
      Geral.MB_Erro('"EstqEnvKnd" indefinido em ' + sProcName);
  end;
}
end;

procedure TDmModVS_CRC.CalculaFreteVMIdeMOEnvRet(MovimCod: Integer);
begin
// Nada?
end;

procedure TDmModVS_CRC.DefineProperties(Filer: TFiler);
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

procedure TDmModVS_CRC.ExcluiAtrelamentoMOEnvAvu(QrVSMOEnvAvu,
  QrVSMOEnvAVMI: TmySQLQuery);
var
  VSMOEnvAvu, MovimCod: Integer;
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do atrelamento selecionado e seus itens?') = ID_YES then
  begin
    MovimCod := QrVSMOEnvAvu.FieldByName('VSVMI_MovimCod').AsInteger;
    VSMOEnvAvu := QrVSMOEnvAvu.FieldByName('Codigo').AsInteger;
    //
    Dmod.MyDB.Execute('DELETE FROM vsmoenvavmi WHERE VSMOEnvAvu=' + Geral.FF0(VSMOEnvAvu));
    //
    QrVSMOEnvAVMI.First;
    while not QrVSMOEnvAVMI.Eof do
    begin
      AtualizaFreteVMIdeMOEnvAvu(QrVSMOEnvAVMI.FieldByName('VSMovIts').AsInteger);
      QrVSMOEnvAVMI.Next;
    end;
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrVSMOEnvAvu, 'vsmoenvavu', ['Codigo'],
    ['Codigo'], True, '');
    DmModVS_CRC.CalculaFreteVMIdeMOEnvAvu(MovimCod);
    //
    DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(MovimCod);
    //
    UnDmkDAC_PF.AbreQuery(QrVSMOEnvAvu, Dmod.MyDB);
  end;
end;

procedure TDmModVS_CRC.ExcluiAtrelamentoMOEnvEnv(QrVSMOEnvEnv, QrVSMOEnvEVMI: TmySQLQuery);
var
  VSMOEnvEnv, MovimCod: Integer;
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do atrelamento selecionado e seus itens?') = ID_YES then
  begin
    MovimCod := QrVSMOEnvEnv.FieldByName('VSVMI_MovimCod').AsInteger;
    VSMOEnvEnv := QrVSMOEnvEnv.FieldByName('Codigo').AsInteger;
    //
    Dmod.MyDB.Execute('DELETE FROM vsmoenvevmi WHERE VSMOEnvEnv=' + Geral.FF0(VSMOEnvEnv));
    //
    QrVSMOEnvEVMI.First;
    while not QrVSMOEnvEVMI.Eof do
    begin
      AtualizaFreteVMIdeMOEnvEnv(QrVSMOEnvEVMI.FieldByName('VSMovIts').AsInteger);
      QrVSMOEnvEVMI.Next;
    end;
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrVSMOEnvEnv, 'vsmoenvenv', ['Codigo'],
    ['Codigo'], True, '');
    CalculaFreteVMIdeMOEnvEnv(MovimCod);
    //
    DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(MovimCod);
    //
    UnDmkDAC_PF.AbreQuery(QrVSMOEnvEnv, Dmod.MyDB);
  end;
end;

procedure TDmModVS_CRC.ExcluiAtrelamentoMOEnvRet(QrVSMOEnvRet, QrVSMOEnvGVMI,
  QrVSMOEnvRVMI: TmySQLQuery);
var
  VSMOEnvRet(*, MovimCod*): Integer;
  ListaEnvEnv: TMyGrlArrInt;
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do atrelamento selecionado e seus itens?') = ID_YES then
  begin
    //MovimCod := QrVSMOEnvEnv.FieldByName('VSVMI_MovimCod').AsInteger;
    VSMOEnvRet := QrVSMOEnvRet.FieldByName('Codigo').AsInteger;
    //
    SetLength(ListaEnvEnv, QrVSMOEnvRVMI.RecordCount);
    QrVSMOEnvRVMI.First;
    while not QrVSMOEnvRVMI.Eof do
    begin
      ListaEnvEnv[QrVSMOEnvRVMI.RecNo -1 ] := QrVSMOEnvRVMI.FieldByName('VSMOEnvEnv').AsInteger;
      //
      QrVSMOEnvRVMI.Next;
    end;
    //
    Dmod.MyDB.Execute('DELETE FROM vsmoenvgvmi WHERE VSMOEnvRet=' + Geral.FF0(VSMOEnvRet));
    Dmod.MyDB.Execute('DELETE FROM vsmoenvrvmi WHERE VSMOEnvRet=' + Geral.FF0(VSMOEnvRet));
    //
    QrVSMOEnvGVMI.First;
    while not QrVSMOEnvGVMI.Eof do
    begin
      AtualizaFreteVMIdeMOEnvRet(QrVSMOEnvGVMI.FieldByName('VSMovIts').AsInteger);
      QrVSMOEnvGVMI.Next;
    end;
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrVSMOEnvRet, 'vsmoenvret', ['Codigo'],
    ['Codigo'], True, '');
    //
    DmModVS_CRC.AtualizaSaldoVSMOEnvRet(ListaEnvEnv);
    //
    // N�o vai na soma do cabe�alho!?
    //DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(MovimCod);
    //
    UnDmkDAC_PF.AbreQuery(QrVSMOEnvRet, Dmod.MyDB);
  end;
end;

procedure TDmModVS_CRC.InformaSemFornecedor;
begin
  //if Dmod.QrControleVSWarNoFrn.Value = 1 then
  if VAR_VSWarNoFrn = 1 then
    Geral.MB_Info('Defini��o de fornecedor abortada!' + sLineBreak +
    'Existe pelo menos um item de origem sem fornecedor definido!');
end;

procedure TDmModVS_CRC.InformaSemNFe();
begin
//  Fazer!!!

  //if Dmod.QrControleVSWarSemNF.Value = 1 then
  if VAR_VSWarSemNF = 1 then
    Geral.MB_Info('Defini��o de NFe abortada!' + sLineBreak +
    'Existe pelo menos um item de origem sem NFe definida!');
end;

function TDmModVS_CRC.ObtemFornecedores_Pallet(Pallet: Integer): String;
  function SQL_Parcial_Its(Tab: TTabToWork): String;
  begin
    Result := Geral.ATS([
    'SELECT IF((vem.SiglaVS="") OR (vem.SiglaVS IS NULL),   ',
    '  CONCAT("FRN-", mfi.Fornece), vem.SiglaVS) SiglaVS,  ',
    'CAST(IF(vmi.VSMulFrnCab<>0, mfi.Fornece, vmi.Terceiro) AS SIGNED) FrnCod,   ',
    'CAST(SUM(IF(vmi.VSMulFrnCab<>0, mfi.Pecas, vmi.Pecas)) AS DECIMAL (15,3)) Pecas  ',
    'FROM ' + VS_CRC_PF.TabMovVS_Tab(tab) + ' vmi ',
    'LEFT JOIN vsmulfrnits mfi ON vmi.VSMulFrnCab=mfi.Codigo  ',
    'LEFT JOIN vsentimp vem ON vem.Codigo=IF(vmi.VSMulFrnCab<>0, mfi.Fornece, vmi.Terceiro)  ',
    'WHERE vmi.Pallet=' + Geral.FF0(Pallet),
    'AND ( ' + VS_CRC_PF.SQL_MovIDeNiv_Pos_Inn() + ') ',
    'GROUP BY FrnCod  ',
    Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
  end;
  //
  function SQL_Parcial_Sum(Tab: TTabToWork): String;
  begin
    Result := Geral.ATS([
    'SELECT ',
    'CAST(SUM(IF(vmi.VSMulFrnCab<>0, mfi.Pecas, vmi.Pecas)) AS DECIMAL (15,3)) Pecas  ',
    'FROM ' + VS_CRC_PF.TabMovVS_Tab(tab) + ' vmi ',
    'LEFT JOIN vsmulfrnits mfi ON vmi.VSMulFrnCab=mfi.Codigo  ',
    'WHERE vmi.Pallet=' + Geral.FF0(Pallet),
    'AND ( ' + VS_CRC_PF.SQL_MovIDeNiv_Pos_Inn() + ') ',
    Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
  end;
  //
var
  Fator, PerPecas: Double;
  SiglaVS, Percentual: String;
begin
  Result := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTotFrn2, Dmod.MyDB, [
  SQL_Parcial_Sum(ttwB),
  SQL_Parcial_Sum(ttwA),
  '']);
  Fator := 0;
  QrTotFrn2.First;
  while not QrTotFrn2.Eof do
  begin
    Fator := Fator + QrTotFrn2Pecas.Value;
    //
    QrTotFrn2.Next;
  end;
  if Fator > 0 then
    Fator := Fator / 100
  else
    Fator := High(Integer);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMulFrn2, Dmod.MyDB, [
  SQL_Parcial_Its(ttwB),
  SQL_Parcial_Its(ttwA),
  '',
  'ORDER BY Pecas DESC ',
  '']);
  QrMulFrn2.First;
  while not QrMulFrn2.Eof do
  begin
    SiglaVS := QrMulFrn2SiglaVS.Value;
    if QrTotFrn2Pecas.Value <> 0 then
    begin
      PerPecas   := QrMulFrn2Pecas.Value / Fator;
      Percentual := Geral.FI64(Round(PerPecas));
    end else
    begin
      PerPecas   := 0;
      Percentual := '??';
    end;
    //
    if (SiglaVS <> '') or (PerPecas <> 0) then
    begin
      if Trim(SiglaVS) = '' then
        SiglaVS := '???';
      Result := Result + SiglaVS + ' ' + Percentual + '% ';
    end;
    //
    QrMulFrn2.Next;
  end;
end;

function TDmModVS_CRC.ObtemGraGruYDeGraGru1(Nivel1: Integer): Integer;
var
  Qry: TmySQLQuery;
  GraGruY, Controle: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggy.Codigo  ',
    'FROM gragru1 gg1 ',
    'LEFT JOIN gragruy ggy ON ggy.PrdGrupTip=gg1.PrdGrupTip ',
    'WHERE gg1.Nivel1=' + Geral.FF0(Nivel1),
    '']);
    Result := Qry.FieldByName('Codigo').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TDmModVS_CRC.ObtemProximoCTe(Transportador, Serie: Integer): Integer;
var
  Atual: Integer;
begin
  Result := -1;
  if MyObjects.FIC(Transportador = 0, nil, 'Informe o transportador!')
  or MyObjects.FIC(Serie < 0, nil, 'Informe a s�rie!')
  then Exit;
  //
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT MAX(CFTMA_nCT) nCT ',
  'FROM vsmoenvavu ',
  'WHERE CFTMA_Terceiro=' + Geral.FF0(Transportador),
  'AND CFTMA_SerCT=' + Geral.FF0(Serie),
  ' ',
  'UNION ',
  ' ',
  'SELECT MAX(CFTMP_nCT) nCT ',
  'FROM vsmoenvenv ',
  'WHERE CFTMP_Terceiro=' + Geral.FF0(Transportador),
  'AND CFTMP_SerCT=' + Geral.FF0(Serie),
  ' ',
  'UNION ',
  ' ',
  'SELECT MAX(CFTPA_nCT) nCT ',
  'FROM vsmoenvret ',
  'WHERE CFTPA_Terceiro=' + Geral.FF0(Transportador),
  'AND CFTPA_SerCT=' + Geral.FF0(Serie),
  ' ',
  'ORDER BY nCT DESC ',
  '']);
  //
  Atual := USQLDB.Int(DqAux, 'nCT');
  //
  Result := Atual + 1;
end;

procedure TDmModVS_CRC.PesquisaNFe(EdFatID, EdFatNum, EdEmpresa, EdTerceiro,
  EdSerNF, EdnNF: TdmkEdit);
const
  Aviso  = '...';
  Titulo = 'Sele��o de NFe';
  Prompt = 'Selecione a NFe';
  //DataSource = nil;
var
  Codigo: Integer;
  Resp: Variant;
  SQL: array of String;
  Serie, nNF, Terceiro, xSerie, xNF: Integer;
  SQL_Serie, SQL_nNF, SQL_Terceiro: String;
begin
  SQL_Serie    := '';
  SQL_nNF      := '';
  SQL_Terceiro := '';
  //
  Serie    := EdSerNF.ValueVariant;
  nNF      := EdnNF.ValueVariant;
  Terceiro := EdTerceiro.ValueVariant;
  //
  if Serie <> -1 then
    SQL_Serie := 'AND SUBSTRING(Id, 23, 3)=' + Geral.FF0(Serie);
  if nNF <> 0 then
    SQL_nNF     := 'AND SUBSTRING(Id, 26, 9)=' + Geral.FF0(nNF);
  if Terceiro <> 0 then
    SQL_Terceiro := 'AND CodInfoEmit=' + Geral.FF0(Terceiro);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCabA, Dmod.MyDB, [
  'SELECT FatID, FatNum, Empresa, IDCtrl, ',
  'ide_dEmi, ide_hEmi, ide_Serie, ide_nNF, ',
  'ICMSTot_vNF, Id, emit_xNome, emit_CNPJ, ',
  'CodInfoEmit ',
  'FROM nfecaba ',
  'WHERE FatID=53 ',
  SQL_Serie,
  SQL_nNF,
  SQL_Terceiro,
  '']);
  if DBCheck.EscolheCodigoUniGrid(Aviso, Titulo, Prompt, DsCabA, False, False) then
  begin
    UnNFe_PF.ObtemSerieNumeroByID(QrCabA.FieldByName('Id').AsString, xSerie, xNF);
    //
    EdFatID.ValueVariant    := QrCabA.FieldByName('FatID').AsInteger;
    EdFatNum.ValueVariant   := QrCabA.FieldByName('FatNum').AsInteger;
    EdEmpresa.ValueVariant  := QrCabA.FieldByName('Empresa').AsInteger;
    EdTerceiro.ValueVariant := QrCabA.FieldByName('CodInfoEmit').AsInteger;
    //EdnItem.ValueVariant    := QrCabA.FieldByName('').AsInteger;
    //
    if xSerie <> 0 then
      EdSerNF.ValueVariant := xSerie;
    if xNF <> 0 then
      EdnNF.ValueVariant := xNF;
(*
    EdPecas.ValueVariant    := QrCabA.FieldByName('').AsFloat;
    EdPesoKg.ValueVariant   := QrCabA.FieldByName('').AsFloat;
    EdAreaM2.ValueVariant   := QrCabA.FieldByName('').AsFloat;
    EdAreaP2.ValueVariant   := QrCabA.FieldByName('').AsFloat;
    EdValorT.ValueVariant   := QrCabA.FieldByName('').AsFloat;
*)
  end;
end;

function TDmModVS_CRC.PreparaPalletParaReclassificar(const SQLType: TSQLType;
  const CkReclasse_Checked: Boolean; var Empresa, ClientMO, Codigo, MovimCod,
  Controle, Pallet, GraGruX, GraGruY: Integer; DtHr: TDateTime; const
  StqCenLoc: Integer; IuvpeiInn, IuvpeiBxa: TInsUpdVMIPrcExecID): Boolean;
  //
  function CalculaValorT(): Double;
  var
    CustoM2, ValorT: Double;
  begin
    if QrVSMovItsAreaM2.Value > 0 then
      Result := (QrVSMovItsValorT.Value / QrVSMovItsAreaM2.Value) * QrVSMovItsSdoVrtArM2.Value
    else
    if QrVSMovItsPesoKg.Value > 0 then
      Result := (QrVSMovItsValorT.Value / QrVSMovItsPesoKg.Value) * QrVSMovItsSdoVrtPeso.Value
    else
    if QrVSMovItsPecas.Value > 0 then
      Result := (QrVSMovItsValorT.Value / QrVSMovItsPecas.Value) * QrVSMovItsSdoVrtPeca.Value
    else
      Result := 0;
  end;
var
  NewVMI: Integer;
  DataHora: String;
  Pecas, PesoKg, AreaM2, AreaP2: Double;
  GSPSrcMovID: TEstqMovimID;
  SerieFch, Ficha, Terceiro, FornecMO, TpCalcAuto, PedItsLib, PedItsFin,
  PedItsVda, GSPSrcNiv2, ReqMovEstq,
  VSMulFrnCab: Integer;
  Marca: String;
  CustoMOKg, CustoMOTot, ValorMP, ValorT: Double;
var
  SQL_Rcl, SQL_15: String;
begin
  Result := False;
  if CkReclasse_Checked then
  begin
    SQL_Rcl := ' ';
    SQL_15  := ', 15';
  end else
  begin
    SQL_Rcl := 'AND vsp.GerRclCab = 0 ';
    SQL_15   := '';
  end;
  //
(*
  if MyObjects.FIC(EdPallet.ValueVariant = 0, EdPallet, 'Informe o Pallet!') then
    Exit;
  //
  Pallet   := EdPallet.ValueVariant;
*)
  UnDmkDAC_PF.AbreMySQLQUery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT wmi.*, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, ggx.GraGruY',
  'FROM ' + CO_SEL_TAB_VMI + ' wmi  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet  ',
  'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa  ',
  'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro ',
  'WHERE wmi.Pecas > 0 ',
  'AND wmi.Empresa=' + Geral.FF0(Empresa),
  'AND wmi.Pallet > 0 ',
  'AND wmi.Pallet=' + Geral.FF0(Pallet),
  'AND wmi.SdoVrtPeca > 0 ',
  SQL_Rcl,
  'AND MovimID IN (' + CO_ALL_CODS_CLASS_VS + SQL_15 + ') ',
  'ORDER BY Controle ',
  '']);
  if MyObjects.FIC(QrVSMovIts.RecordCount = 0, nil,
  'Pallet sem IME-Is com saldo!!') then
    Exit;
  //
  Pecas       := 0;
  PesoKg      := 0;
  AreaM2      := 0;
  AreaP2      := 0;
  SerieFch    := QrVSMovItsSerieFch.Value;
  Ficha       := QrVSMovItsFicha.Value;
  Terceiro    := QrVSMovItsTerceiro.Value;
  VSMulFrnCab := QrVSMovItsVSMulFrnCab.Value;
  FornecMO    := QrVSMovItsFornecMO.Value;
  Marca       := QrVSMovItsMarca.Value;;
  CustoMOKg   := 0;
  CustoMOTot  := 0;
  ValorMP     := 0;
  ValorT      := 0;

  if DtHr < 2 then
    DataHora := Geral.FDT(DModG.ObtemAgora(), 109)
  else
    DataHora := Geral.FDT(DtHr, 109);
  //Controle :=
  NewVMI := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
  Codigo := UMyMod.BPGS1I32('VSPrePalCab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  QrVSMovIts.First;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsprepalcab', False, [
  'Pallet'], ['Codigo'], [Pallet], [Codigo], True) then
  begin
    // Parei Aqui 2015-10-03
    if SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidPreReclasse, Codigo);
    while not QrVSMovIts.Eof do
    begin
      Pecas  := Pecas + QrVSMovItsSdoVrtPeca.Value;
      PesoKg := PesoKg + QrVSMovItsSdoVrtPeso.Value;
      AreaM2 := AreaM2 + QrVSMovItsSdoVrtArM2.Value;
      ValorT := ValorT + CalculaValorT();
      //AreaP2 := 0;
      if SerieFch <> QrVSMovItsSerieFch.Value then
        SerieFch := 0;
      if Ficha <> QrVSMovItsFicha.Value then
        Ficha := 0;
      if Terceiro <> QrVSMovItsTerceiro.Value then
        Terceiro := 0;
      if FornecMO <> QrVSMovItsFornecMO.Value then
        FornecMO := 0;
      if Marca <> QrVSMovItsMarca.Value then
        Marca := '';
      //
      //StqCenLoc     := QrVSMovItsStqCenLoc.Value;
      //
      PreparaPalletParaReclass_InsereBaixaAtual(Codigo, MovimCod, DataHora,
        NewVMI, StqCenLoc, IuvpeiBxa, SQLType);
      //QrVSMovIts.First;
      //QrVSMovIts.First;
      //
      QrVSMovIts.Next;
    end;
    AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    //
    TpCalcAuto    := QrVSMovItsTpCalcAuto.Value;
    // ver o que fazer!!!
    PedItsLib     := 0; // QrVSMovItsPedItsLib.Value;
    PedItsFin     := 0; // QrVSMovItsPedItsFin.Value;
    PedItsVda     := 0; // QrVSMovItsPedItsVda.Value;
    GSPSrcMovID   := emidAjuste; //
    GSPSrcNiv2    := 0; //
    ReqMovEstq    := QrVSMovItsReqMovEstq.Value;
    //
    PreparaPalletParaReclass_InsereNovoVMI_DoPallet(Pallet, NewVMI, Codigo,
      MovimCod, DataHora, Pecas, PesoKg, AreaM2, AreaP2, SerieFch, Ficha,
      Terceiro, VSMulFrnCab, FornecMO, Marca, CustoMOKg, CustoMOTot, ValorMP,
      ValorT, Empresa, ClientMO, GraGruX, GraGruY, TpCalcAuto, PedItsLib,
      PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc,
      IuvpeiInn, SQLType);
    Controle := NewVMI;
    Result := True;
  end;
end;

procedure TDmModVS_CRC.PreparaPalletParaReclass_InsereBaixaAtual(Codigo,
  MovimCod: Integer; DataHora: String; NewVMI, StqCenLoc: Integer;
  IuvpeiBxa: TInsUpdVMIPrcExecID; SQLType: TSQLType);
const
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  //
  TpCalcAuto = -1;
  //
  EdPalletX = nil;
  EdAreaM2X = nil;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = emidAjuste;
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq = 0;
  //StqCenLoc  = 0;
  //
  ItemNFe    = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  //DataHora,
  Observ, Marca: String;
  //Codigo, MovimCod,
  Pallet, Controle, Empresa, GraGruX, Terceiro, Ficha, GraGruY, FornecMO,
  SerieFch, VSMulFrnCab, ClientMO: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP,
  ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  SrcMovID, DstMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2, SrcGGX, DstNivel1, DstNivel2, DstGGX: Integer;
begin
  //Codigo         := Codigo;
  //Controle       := ??;
  //MovimCod       := 0;
  Empresa        := QrVSMovItsEmpresa.Value;
  ClientMO       := QrVSMovItsClientMO.Value;
  MovimID        := emidPreReclasse;
  MovimNiv       := eminSorcPreReclas;
  GraGruX        := QrVSMovItsGraGruX.Value;
  Pallet         := QrVSMovItsPallet.Value;
  //DataHora       := Geral.FDT(Geral.ObtemAgora(), 109);
  Pecas          := QrVSMovItsSdoVrtPeca.Value;
  PesoKg         := QrVSMovItsSdoVrtPeso.Value;
  AreaM2         := QrVSMovItsSdoVrtArM2.Value;
  AreaP2         := Geral.ConverteArea(QrVSMovItsSdoVrtArM2.Value, ctM2toP2, cfQuarto);
  // Ver depois?
  SerieFch       := QrVSMovItsSerieFch.Value;
  Ficha          := QrVSMovItsFicha.Value;
  Terceiro       := QrVSMovItsTerceiro.Value;
  VSMulFrnCab    := QrVSMovItsVSMulFrnCab.Value;
  FornecMO       := QrVSMovItsFornecMO.Value;
  Marca          := QrVSMovItsMarca.Value;
  CustoMOKg      := 0;
  CustoMOM2      := 0;
  CustoMOTot     := 0;
  ValorMP        := 0;
  ValorT         := 0;
  Observ         := '';
  GraGruY        := QrVSMovItsGraGruY.Value;
  //
(*
  if VS_CRC_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPalletX, EdFicha, EdPecas, EdAreaM2X,
  EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca) then
    Exit;
*)
  //
  SrcMovID   := TEstqMovimID(QrVSMovItsMovimID.Value);
  SrcNivel1  := QrVSMovItsCodigo.Value;
  SrcNivel2  := QrVSMovItsControle.Value;
  SrcGGX     := QrVSMovItsGraGruX.Value;
  //
  DstMovID   := emidPreReclasse;
  DstNivel1  := Codigo;
  DstNivel2  := NewVMI;
  DstGGX     := QrVSMovItsGraGruX.Value;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
  //
  if VS_CRC_PF.InsUpdVSMovIts2(SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro, MovimID, MovimNiv, Pallet, GraGruX, -Pecas, -PesoKg, -AreaM2, -AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot,
  ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpeiBxa(*Baixa na prepara��o de pallet para reclassifica��o 2/2*)) then
  begin
    //VS_CRC_PF.AtualizaTotaisVSXxxCab('VSPrePalCab', MovimCod);
    VS_CRC_PF.AtualizaSaldoIMEI(SrcNivel2, True);
    //FmVSAjsCab.LocCod(Codigo, Codigo);
    //FmVSAjsCab.ReopenVSAjsIts(Controle);
    (*
    if CkContinuar.Checked then
    begin
      if GraGruX <> 0 then
        FUltGGX := GraGruX;
      ImgTipo.SQLType            := stIns;
      EdPallet.ValueVariant      := 0;
      CBPallet.KeyValue          := Null;
      //
      EdPallet.SetFocus;
    end else
      Close;
    *)
  end;
end;

procedure TDmModVS_CRC.PreparaPalletParaReclass_InsereNovoVMI_DoPallet(Pallet,
  Controle, Codigo, MovimCod: Integer; DataHora: String; Pecas, PesoKg, AreaM2,
  AreaP2: Double; SerieFch, Ficha, Terceiro, VSMulFrnCab, FornecMO: Integer;
  Marca: String; CustoMOKg, CustoMOTot, ValorMP, ValorT: Double; Empresa,
  ClientMO, GraGruX, GraGruY, TpCalcAuto, PedItsLib, PedItsFin,
  PedItsVda: Integer; GSPSrcMovID: TEstqMovimID; GSPSrcNiv2, ReqMovEstq,
  StqCenLoc: Integer; IuvpeiInn: TInsUpdVMIPrcExecID; SQLType: TSQLType);
const
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  CustoMOM2  = 0;
  ItemNFe    = 0;
  //
  EdPalletX = nil;
  EdAreaM2X = nil;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  //DataHora, Marca,
  Observ: String;
  //Pallet, Codigo, MovimCod, Controle, Terceiro, Ficha, FornecMO, SerieFch,
  //GraGruX, GraGruY: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, CustoMOKg, CustoMOTot, ValorMP, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
begin
  //Codigo         := FCodigo;
  //Controle       := NewVMI;
  //MovimCod       := FMovimCod;
  //Empresa        := QrPalletsEmpresa.Value;
  MovimID        := emidPreReclasse;
  MovimNiv       := eminDestPreReclas;
  //GraGruX        := QrPalletsGraGruX.Value;
(*
  Pallet         := EdPallet.ValueVariant;
  DataHora       := Geral.FDT(Geral.ObtemAgora(), 109);
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  // Ver depois?
  SerieFch       := 0;
  Ficha          := 0;
  Terceiro       := 0;
  FornecMO       := 0;
  Marca          := '';
  CustoMOKg      := ;
  CustoMOTot     := ;
  ValorMP        := ;
  ValorT         := ;
*)
  Observ         := '';
  //GraGruY        := QrPalletsGraGruY.Value;
  //
(*
  if VS_CRC_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPalletX, EdFicha, EdPecas, EdAreaM2X,
  EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca) then
    Exit;
*)
  //
  //Codigo := UMyMod.BPGS1I32('VSPrePalCab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  //MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, MovimCod);
  //
  if VS_CRC_PF.InsUpdVSMovIts2(SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot,
  ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  IuvpeiInn) then
  begin
    //VS_CRC_PF.AtualizaTotaisVSXxxCab('VSPrePalCab', MovimCod);
    VS_CRC_PF.AtualizaSaldoIMEI(Controle, True);
    //FmVSAjsCab.LocCod(Codigo, Codigo);
    //FmVSAjsCab.ReopenVSAjsIts(Controle);
    (*
    if CkContinuar.Checked then
    begin
      if GraGruX <> 0 then
        FUltGGX := GraGruX;
      ImgTipo.SQLType            := stIns;
      EdPallet.ValueVariant      := 0;
      CBPallet.KeyValue          := Null;
      //
      EdPallet.SetFocus;
    end else
      Close;
    *)
  end;
end;


procedure TDmModVS_CRC.ReadPixelsPerInch(Reader: TReader);
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

procedure TDmModVS_CRC.WritePixelsPerInch(Writer: TWriter);
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

end.
