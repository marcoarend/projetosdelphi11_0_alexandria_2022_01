unit UnPrdc_tabs;

// Prdc - Predu��o Industrial
{ Colocar no MyListas:

Uses UnPrdc_Tabs;


//
function TMyListas.CriaListaTabelas:
      Prdc_Tabs.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
    Prdc_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
    Prdc_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      Prdc_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
//
function TMyListas.CriaListaCampos:
      Prdc_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    Prdc_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  Prdc_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections, SysUtils, Classes, DB,
//  UnFMX_Geral,
  UnMyLinguas, UnDmkEnums, UnProjGroup_Consts; //, UnPrdc_Consts;

type
  TUnPrdc_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>; SiglaApp: String): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  Prdc_Tabs: TUnPrdc_Tabs;

implementation

uses MyListas;

function TUnPrdc_Tabs.CarregaListaTabelas(Lista: TList<TTabelas> ; SiglaApp: String): Boolean;
begin
  try
   //if SiglaApp = 'Prdc' then
   if (CO_HARDW_APP = 'DESKTOP') then
   begin
      ///  CADASTROS
      ///
      ///  Cadastros b�sicos
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcPaiCad'), ''); //
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcRegCad'), ''); //
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcUfsCad'), ''); //
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcCliCad'), ''); //
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcCliAcc'), ''); //
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcAreCad'), ''); //
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcSegCad'), ''); //
      ///  Lan�amentos
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcCliPrd'), ''); //
    end;
    // Tamb�m no Prdc
    //if SiglaApp = 'Prdc' then
    if (CO_HARDW_APP = 'MOBILE') then
    begin
      //
      ///////////////////////////////  CADASTROS  ///////////////////////////////
      ///
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcPaiCad'), ''); //
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcRegCad'), ''); //
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcUfsCad'), ''); //
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcCliCad'), ''); //
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcCliAcc'), ''); //
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcAreCad'), ''); //
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcSegCad'), ''); //
      ///  Lan�amentos
      MyLinguas.AdTbLst(Lista, False, Lowercase('PrdcCliPrd'), ''); //

    end;
    // S� no Prdc:
    if (CO_HARDW_APP = 'MOBILE') then
    begin
      //MyLinguas.AdTbLst(Lista, False, Lowercase('Prdc'), ''); //

    end;
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnPrdc_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
{
separador = "|"
}
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('PrdcRegCad') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo|Nome|Pais');
    FListaSQL.Add('1|"Norte"|1058');
    FListaSQL.Add('2|"Nordeste"|1058');
    FListaSQL.Add('3|"Sudeste"|1058');
    FListaSQL.Add('4|"Sul"|1058');
    FListaSQL.Add('5|"Centro Oeste"|1058');
  end else
  if Uppercase(Tabela) = Uppercase('PrdcPaiCad') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo|Sigla|Nome');
    FListaSQL.Add( '973|"BO"|"Bol�via"');
    FListaSQL.Add('1058|"BR"|"Brasil"');
    FListaSQL.Add('5860|"PY"|"Paraguai"');
  end else
  if Uppercase(Tabela) = Uppercase('PrdcUfsCad') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo|Sigla|Nome|Regiao|Pais');
    FListaSQL.Add('11|"RO"|"Rond�nia"|1|1058');
    FListaSQL.Add('12|"AC"|"Acre"|1|1058');
    FListaSQL.Add('13|"AM"|"Amazonas"|1|1058');
    FListaSQL.Add('14|"RR"|"Roraima"|1|1058');
    FListaSQL.Add('15|"PA"|"Par�"|1|1058');
    FListaSQL.Add('16|"AP"|"Amap�"|1|1058');
    FListaSQL.Add('17|"TO"|"Tocantins"|1|1058');
    FListaSQL.Add('21|"MA"|"Maranh�o"|2|1058');
    FListaSQL.Add('22|"PI"|"Piau�"|2|1058');
    FListaSQL.Add('23|"CE"|"Cear�"|2|1058');
    FListaSQL.Add('24|"RN"|"Rio Grande do Norte"|2|1058');
    FListaSQL.Add('25|"PB"|"Para�ba"|2|1058');
    FListaSQL.Add('26|"PE"|"Pernambuco"|2|1058');
    FListaSQL.Add('27|"AL"|"Alagoas"|2|1058');
    FListaSQL.Add('28|"SE"|"Sergipe"|2|1058');
    FListaSQL.Add('29|"BA"|"Bahia"|2|1058');
    FListaSQL.Add('31|"MG"|"Minas Gerais"|3|1058');
    FListaSQL.Add('32|"ES"|"Esp�rito Santo"|3|1058');
    FListaSQL.Add('33|"RJ"|"Rio de Janeiro"|3|1058');
    FListaSQL.Add('35|"SP"|"S�o Paulo"|3|1058');
    FListaSQL.Add('41|"PR"|"Paran�"|4|1058');
    FListaSQL.Add('42|"SC"|"Santa Catarina"|4|1058');
    FListaSQL.Add('43|"RS"|"Rio Grande do Sul"|4|1058');
    FListaSQL.Add('50|"MS"|"Mato Grosso do Sul"|5|1058');
    FListaSQL.Add('51|"MT"|"Mato Grosso"|5|1058');
    FListaSQL.Add('52|"GO"|"Goi�s"|5|1058');
    FListaSQL.Add('53|"DF"|"Distrito Federal"|5|1058');
  end;
end;

function TUnPrdc_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('??') then
  begin
(*
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"ND"');
    FListaSQL.Add('1|"Telefone"');
*)
  end else
(*
  if Uppercase(Tabela) = Uppercase('OVdTipoOP') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('"1"|"Normal"');
    FListaSQL.Add('"6"|"Retrabalho"');
    FListaSQL.Add('"7"|"Industrializa��o"');
  end else
*)
end;

function TUnPrdc_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('PrdcRegCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('PrdcPaiCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Sigla';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('PrdcUfsCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Pais';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Sigla';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('PrdcCliCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('PrdcCliAcc') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('PrdcAreCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('PrdcSegCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('PrdcCliPrd') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Data';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Cliente';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Segmento';
    FLIndices.Add(FRIndices);
    //
  end else
end;

function TUnPrdc_Tabs.CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TUnPrdc_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    if Uppercase(Tabela) = Uppercase('PrdcRegCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pais';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('PrdcPaiCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('PrdcUfsCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Regiao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pais';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('PrdcCliCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pais';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UF';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('PrdcCliAcc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Area';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Account';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('PrdcAreCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('PrdcSegCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Unidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('PrdcCliPrd') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Data';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Hora';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Area';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Segmento';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Quantidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
  except
    raise;
    Result := False;
  end;
end;

function TUnPrdc_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
(*
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
{
      New(FRCampos);
      FRCampos.Field      := '??';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else
*)
  except
    raise;
    Result := False;
  end;
end;

function TUnPrdc_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //////////////////////////////////////////////////////////////////////////////
  ///
  /// Produ��o industrial no momento
  ///
  // PIM-CADAS-000 :: Cadastro de Pa�ses
  New(FRJanelas);
  FRJanelas.ID        := 'PIM-CADAS-000';
  FRJanelas.Nome      := 'FmPrdcPaiCad';
  FRJanelas.Descricao := 'Cadastro de Pa�ses';
  FLJanelas.Add(FRJanelas);
  //
  // PIM-CADAS-001 :: Cadastro de Regi�es
  New(FRJanelas);
  FRJanelas.ID        := 'PIM-CADAS-001';
  FRJanelas.Nome      := 'FmPrdcRegCad';
  FRJanelas.Descricao := 'Cadastro de Regi�es';
  FLJanelas.Add(FRJanelas);
  //
  // PIM-CADAS-002 :: Cadastro de UFs
  New(FRJanelas);
  FRJanelas.ID        := 'PIM-CADAS-002';
  FRJanelas.Nome      := 'FmPrdcUfsCad';
  FRJanelas.Descricao := 'Cadastro de UFs';
  FLJanelas.Add(FRJanelas);
  //
  // PIM-CADAS-003 :: Cadastro de Clientes
  New(FRJanelas);
  FRJanelas.ID        := 'PIM-CADAS-003';
  FRJanelas.Nome      := 'FmPrdcCliCad';
  FRJanelas.Descricao := 'Cadastro de Clientes';
  FLJanelas.Add(FRJanelas);
  //
  // PIM-CADAS-004 :: Cadastro de Accounts em Clientes
  New(FRJanelas);
  FRJanelas.ID        := 'PIM-CADAS-004';
  FRJanelas.Nome      := 'FmPrdcCliAcc';
  FRJanelas.Descricao := 'Cadastro de Accounts em Clientes';
  FLJanelas.Add(FRJanelas);
  //
  // PIM-CADAS-005 :: Cadastro de �reas
  New(FRJanelas);
  FRJanelas.ID        := 'PIM-CADAS-005';
  FRJanelas.Nome      := 'FmPrdcAreCad';
  FRJanelas.Descricao := 'Cadastro de �reas';
  FLJanelas.Add(FRJanelas);
  //
  // PIM-CADAS-006 :: Cadastro de Segmentos
  New(FRJanelas);
  FRJanelas.ID        := 'PIM-CADAS-006';
  FRJanelas.Nome      := 'FmPrdcSegCad';
  FRJanelas.Descricao := 'Cadastro de Segmentos';
  FLJanelas.Add(FRJanelas);
  //
  // PIM-LANCT-001 :: La�amentos de Produ��o
  New(FRJanelas);
  FRJanelas.ID        := 'PIM-LANCT-001';
  FRJanelas.Nome      := 'FmPrdcCliPrd';
  FRJanelas.Descricao := 'La�amentos de Produ��o';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
