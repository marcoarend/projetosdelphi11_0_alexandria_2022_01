unit UnPrdc_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, Variants;

type
  TUnPrdc_Jan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormPrdcAreCad(Codigo: Integer);
    procedure MostraFormPrdcCliCad(Codigo: Integer);
  end;
var
  Prdc_Jan: TUnPrdc_Jan;


implementation

uses
  dmkGeral, MyDBCheck, Module, CfgCadLista, DmkDAC_PF,
  PrdcAreCad, PrdcCliCad;

{ TUnPrdc_Jan }

procedure TUnPrdc_Jan.MostraFormPrdcAreCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPrdcAreCad, FmPrdcAreCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPrdcAreCad.LocCod(Codigo, Codigo);
    FmPrdcAreCad.ShowModal;
    FmPrdcAreCad.Destroy;
  end;
end;

procedure TUnPrdc_Jan.MostraFormPrdcCliCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPrdcCliCad, FmPrdcCliCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPrdcCliCad.LocCod(Codigo, Codigo);
    FmPrdcCliCad.ShowModal;
    FmPrdcCliCad.Destroy;
  end;
end;

end.
