unit PrdcSegCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkCheckBox;

type
  TFmPrdcSegCad = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    EdUnidade: TdmkEditCB;
    CBUnidade: TdmkDBLookupComboBox;
    SbUnidade: TSpeedButton;
    Panel5: TPanel;
    EdCodigo: TdmkEdit;
    Label2: TLabel;
    EdNO_Segmento: TdmkEdit;
    QrUnidades: TMySQLQuery;
    DsUnidades: TDataSource;
    QrUnidadesCodigo: TIntegerField;
    QrUnidadesNome: TWideStringField;
    CkAtivo: TdmkCheckBox;
    EdOrdem: TdmkEdit;
    Label3: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbUnidadeClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FCodigo: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmPrdcSegCad: TFmPrdcSegCad;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnUMedi_PF;

{$R *.DFM}

procedure TFmPrdcSegCad.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Unidade, Ordem, Ativo: Integer;
  Nome: String;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Unidade        := EdUnidade.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  Ativo          := Geral.BoolToInt(CkAtivo.Checked);
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Unidade = 0, EdUnidade, 'Defina a unidade!') then Exit;
  //
  //
  Controle := UMyMod.BPGS1I32('prdcsegcad', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'prdcsegcad', False, [
  'Codigo', 'Nome', 'Unidade',
  'Ordem', 'Ativo'], [
  'Controle'], [
  Codigo, Nome, Unidade,
  Ordem, Ativo], [
  Controle], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdUnidade.ValueVariant     := 0;
      CBUnidade.KeyValue         := Null;
      EdNome.ValueVariant        := '';
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmPrdcSegCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrdcSegCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPrdcSegCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrUnidades, Dmod.MyDB);
end;

procedure TFmPrdcSegCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrdcSegCad.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmPrdcSegCad.SbUnidadeClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UMedi_PF.MostraUnidMed(EdUnidade.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdUnidade, CBUnidade, QrUnidades, VAR_CADASTRO);
    //
    EdUnidade.SetFocus;
  end;
end;

end.
