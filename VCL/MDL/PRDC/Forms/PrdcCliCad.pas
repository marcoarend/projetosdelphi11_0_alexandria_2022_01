unit PrdcCliCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, variants, ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, dmkCheckBox;

type
  TFmPrdcCliCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrPrdcCliCad: TMySQLQuery;
    DsPrdcCliCad: TDataSource;
    QrPrdcCliAcc: TMySQLQuery;
    DsPrdcCliAcc: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrPrdcCliCadNO_Pais: TWideStringField;
    QrPrdcCliCadNO_Regiao: TWideStringField;
    QrPrdcCliCadCodigo: TIntegerField;
    QrPrdcCliCadNome: TWideStringField;
    QrPrdcCliCadPais: TIntegerField;
    QrPrdcCliCadUF: TWideStringField;
    QrPrdcCliCadAtivo: TSmallintField;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    Label10: TLabel;
    QrPrdcPaiCad: TMySQLQuery;
    DsrPrdcPaiCad: TDataSource;
    QrPrdcUfsCad: TMySQLQuery;
    DsPrdcUfsCad: TDataSource;
    EdPais: TdmkEditCB;
    CBPais: TdmkDBLookupComboBox;
    QrPrdcPaiCadCodigo: TIntegerField;
    QrPrdcPaiCadNome: TWideStringField;
    QrPrdcUfsCadSigla: TWideStringField;
    QrPrdcUfsCadNome: TWideStringField;
    CBUF: TdmkDBLookupComboBox;
    CkAtivo: TdmkCheckBox;
    QrPrdcCliAccSiglaArea: TWideStringField;
    QrPrdcCliAccNO_AREA: TWideStringField;
    QrPrdcCliAccSiglaAccount: TWideStringField;
    QrPrdcCliAccNO_Account: TWideStringField;
    QrPrdcCliAccCodigo: TIntegerField;
    QrPrdcCliAccControle: TIntegerField;
    QrPrdcCliAccArea: TIntegerField;
    QrPrdcCliAccAccount: TIntegerField;
    QrPrdcCliAccAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPrdcCliCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPrdcCliCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrPrdcCliCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrPrdcCliCadBeforeClose(DataSet: TDataSet);
    procedure EdPaisRedefinido(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraPrdcCliAcc(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPrdcCliAcc(Controle: Integer);

  end;

var
  FmPrdcCliCad: TFmPrdcCliCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, PrdcCliAcc;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPrdcCliCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPrdcCliCad.MostraPrdcCliAcc(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmPrdcCliAcc, FmPrdcCliAcc, afmoNegarComAviso) then
  begin
    FmPrdcCliAcc.ImgTipo.SQLType := SQLType;
    FmPrdcCliAcc.FQrCab := QrPrdcCliCad;
    FmPrdcCliAcc.FDsCab := DsPrdcCliCad;
    FmPrdcCliAcc.FQrIts := QrPrdcCliAcc;
    FmPrdcCliAcc.FCodigo := QrPrdcCliCadCodigo.Value;
    FmPrdcCliAcc.EdCodigo.ValueVariant := QrPrdcCliCadCodigo.Value;
    FmPrdcCliAcc.EdNO_Cliente.ValueVariant := QrPrdcCliCadNome.Value;
    if SQLType = stIns then
      //
    else
    begin
      FmPrdcCliAcc.EdControle.ValueVariant := QrPrdcCliAccControle.Value;
      //
      FmPrdcCliAcc.EdArea.ValueVariant := QrPrdcCliAccArea.Value;
      FmPrdcCliAcc.CBArea.KeyValue := QrPrdcCliAccArea.Value;
      FmPrdcCliAcc.EdAccount.ValueVariant := QrPrdcCliAccAccount.Value;
      FmPrdcCliAcc.CBAccount.KeyValue := QrPrdcCliAccAccount.Value;
    end;
    FmPrdcCliAcc.ShowModal;
    FmPrdcCliAcc.Destroy;
  end;
end;

procedure TFmPrdcCliCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrPrdcCliCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrPrdcCliCad, QrPrdcCliAcc);
end;

procedure TFmPrdcCliCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrPrdcCliCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrPrdcCliAcc);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrPrdcCliAcc);
end;

procedure TFmPrdcCliCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPrdcCliCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPrdcCliCad.DefParams;
begin
  VAR_GOTOTABELA := 'prdcclicad';
  VAR_GOTOMYSQLTABLE := QrPrdcCliCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ppc.Nome NO_Pais, prc.Nome NO_Regiao,');
  VAR_SQLx.Add('pcc.*');
  VAR_SQLx.Add('FROM prdcclicad pcc');
  VAR_SQLx.Add('LEFT JOIN prdcpaicad ppc ON ppc.Codigo=pcc.Pais');
  VAR_SQLx.Add('LEFT JOIN prdcufscad puc ON puc.Sigla=pcc.UF');
  VAR_SQLx.Add('LEFT JOIN prdcregcad prc ON prc.Codigo=puc.Regiao');
  VAR_SQLx.Add('WHERE pcc.Codigo <> 0');
  //
  VAR_SQL1.Add('AND pcc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND pcc.Nome Like :P0');
  //
end;

procedure TFmPrdcCliCad.EdPaisRedefinido(Sender: TObject);
var
  UF: String;
begin
  UF := CBUF.Text;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrdcUfsCad, Dmod.MyDB, [
  'SELECT Sigla, Nome  ',
  'FROM prdcufscad ',
  'WHERE Pais=' + Geral.FF0(EdPais.ValueVariant),
  'ORDER BY Nome ',
  ' ']);
  //
  if CBUF.KeyValue <> Null then
  begin
    if not QrPrdcUfsCad.Locate('Sigla', UF, []) then
      CBUF.KeyValue := Null;
  end;
end;

procedure TFmPrdcCliCad.ItsAltera1Click(Sender: TObject);
begin
  MostraPrdcCliAcc(stUpd);
end;

procedure TFmPrdcCliCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmPrdcCliCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPrdcCliCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPrdcCliCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'PrdcCliAcc', 'Controle', QrPrdcCliAccControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrPrdcCliAcc,
      QrPrdcCliAccControle, QrPrdcCliAccControle.Value);
    ReopenPrdcCliAcc(Controle);
  end;
}
end;

procedure TFmPrdcCliCad.ReopenPrdcCliAcc(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrdcCliAcc, Dmod.MyDB, [
  'SELECT pac.Sigla SiglaArea, pac.Nome NO_AREA, ',
  'eif.Sigla SiglaAccount,  ',
  'eif.Nome NO_Account, pca.* ',
  'FROM prdccliacc pca ',
  'LEFT JOIN prdcarecad pac ON pac.Codigo=pca.Area ',
  'LEFT JOIN excidfunc eif ON eif.Entidade=pca.Account ',
  'WHERE pca.Codigo=' + Geral.FF0(QrPrdcCliCadCodigo.Value),
  'ORDER BY pca.Account, pac.Sigla ',
  '']);
  //
  QrPrdcCliAcc.Locate('Controle', Controle, []);
end;


procedure TFmPrdcCliCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPrdcCliCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPrdcCliCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPrdcCliCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPrdcCliCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPrdcCliCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrdcCliCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPrdcCliCadCodigo.Value;
  Close;
end;

procedure TFmPrdcCliCad.ItsInclui1Click(Sender: TObject);
begin
  MostraPrdcCliAcc(stIns);
end;

procedure TFmPrdcCliCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrPrdcCliCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'prdcclicad');
end;

procedure TFmPrdcCliCad.BtConfirmaClick(Sender: TObject);
var
  Nome, UF: String;
  Codigo, Pais, Ativo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Pais           := EdPais.ValueVariant;
  UF             := CBUF.Text;
  Ativo          := Geral.BoolToInt(CkAtivo.Checked);
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Pais = 0, EdPais, 'Defina o pa�s!') then Exit;
  if QrPrdcUfsCad.RecordCount > 0 then
    if MyObjects.FIC(Length(UF) = 0, CBUF, 'Defina a UF!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('prdcclicad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'prdcclicad', False, [
  'Nome', 'Pais', 'UF',
  'Ativo'], [
  'Codigo'], [
  Nome, Pais, UF,
  Ativo], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmPrdcCliCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'prdcclicad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'prdcclicad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPrdcCliCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmPrdcCliCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmPrdcCliCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  UnDmkDAC_PF.AbreQuery(QrPrdcPaiCad, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrPrdcUfsCad, Dmod.MyDB);
end;

procedure TFmPrdcCliCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPrdcCliCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPrdcCliCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPrdcCliCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPrdcCliCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPrdcCliCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPrdcCliCad.QrPrdcCliCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPrdcCliCad.QrPrdcCliCadAfterScroll(DataSet: TDataSet);
begin
  ReopenPrdcCliAcc(0);
end;

procedure TFmPrdcCliCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrPrdcCliCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmPrdcCliCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPrdcCliCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'prdcclicad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPrdcCliCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrdcCliCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrPrdcCliCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'prdcclicad');
end;

procedure TFmPrdcCliCad.QrPrdcCliCadBeforeClose(
  DataSet: TDataSet);
begin
  QrPrdcCliAcc.Close;
end;

procedure TFmPrdcCliCad.QrPrdcCliCadBeforeOpen(DataSet: TDataSet);
begin
  QrPrdcCliCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

