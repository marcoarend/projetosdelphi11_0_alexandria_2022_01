object FmPrdcAreCad: TFmPrdcAreCad
  Left = 368
  Top = 194
  Caption = 'PIM-CADAS-005 :: Cadastro de Segmentos'
  ClientHeight = 433
  ClientWidth = 641
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 641
    Height = 337
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitWidth = 896
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 641
      Height = 101
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      ExplicitWidth = 896
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 76
        Top = 56
        Width = 26
        Height = 13
        Caption = 'Sigla:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 164
        Top = 56
        Width = 34
        Height = 13
        Caption = 'Ordem:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 349
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSigla: TdmkEdit
        Left = 76
        Top = 72
        Width = 82
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Sigla'
        UpdCampo = 'Sigla'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkAtivo: TdmkCheckBox
        Left = 252
        Top = 76
        Width = 49
        Height = 17
        Caption = 'Ativo'
        Checked = True
        State = cbChecked
        TabOrder = 4
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object EdOrdem: TdmkEdit
        Left = 164
        Top = 72
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Ordem'
        UpdCampo = 'Ordem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 274
      Width = 641
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitWidth = 896
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 501
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 756
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 641
    Height = 337
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 128
    ExplicitTop = 72
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 641
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      ExplicitWidth = 896
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 428
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Sigla:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 516
        Top = 16
        Width = 34
        Height = 13
        Caption = 'Ordem:'
        FocusControl = DBEdit2
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPrdcAreCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 349
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsPrdcAreCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 428
        Top = 32
        Width = 82
        Height = 21
        DataField = 'Sigla'
        DataSource = DsPrdcAreCad
        TabOrder = 2
      end
      object DBCheckBox1: TDBCheckBox
        Left = 576
        Top = 36
        Width = 48
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsPrdcAreCad
        TabOrder = 4
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit2: TDBEdit
        Left = 516
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Ordem'
        DataSource = DsPrdcAreCad
        TabOrder = 3
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 273
      Width = 641
      Height = 64
      Align = alBottom
      TabOrder = 0
      ExplicitWidth = 896
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 83
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
        ExplicitWidth = 199
      end
      object Panel3: TPanel
        Left = 257
        Top = 15
        Width = 382
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        ExplicitLeft = 476
        object Panel2: TPanel
          Left = 249
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitLeft = 388
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Segmento'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Artigo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 65
      Width = 641
      Height = 148
      Align = alTop
      DataSource = DsPrdcSegCad
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Segmento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Unidade'
          Title.Caption = 'Unidade'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ordem'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 641
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 896
    object GB_R: TGroupBox
      Left = 593
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 848
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 377
      Height = 52
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 632
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 228
        Height = 32
        Caption = 'Cadastro de '#193'reas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 228
        Height = 32
        Caption = 'Cadastro de '#193'reas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 228
        Height = 32
        Caption = 'Cadastro de '#193'reas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 641
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitWidth = 896
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 637
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 892
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrPrdcAreCad: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPrdcAreCadBeforeOpen
    AfterOpen = QrPrdcAreCadAfterOpen
    BeforeClose = QrPrdcAreCadBeforeClose
    AfterScroll = QrPrdcAreCadAfterScroll
    SQL.Strings = (
      'SELECT * FROM prdcsegcad'
      'WHERE Codigo > 0')
    Left = 76
    Top = 209
    object QrPrdcAreCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrdcAreCadSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
      Size = 6
    end
    object QrPrdcAreCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrPrdcAreCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPrdcAreCadOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsPrdcAreCad: TDataSource
    DataSet = QrPrdcAreCad
    Left = 72
    Top = 253
  end
  object QrPrdcSegCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT psc.*, med.Nome NO_Unidade'
      'FROM prdcsegcad psc'
      'LEFT JOIN unidmed med ON med.Codigo=psc.Unidade')
    Left = 168
    Top = 205
    object QrPrdcSegCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrdcSegCadControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPrdcSegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPrdcSegCadUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrPrdcSegCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPrdcSegCadNO_Unidade: TWideStringField
      FieldName = 'NO_Unidade'
      Size = 30
    end
    object QrPrdcSegCadOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsPrdcSegCad: TDataSource
    DataSet = QrPrdcSegCad
    Left = 168
    Top = 253
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 384
    Top = 240
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 260
    Top = 236
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
end
