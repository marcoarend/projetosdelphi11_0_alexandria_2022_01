object FmPrdcCliCad: TFmPrdcCliCad
  Left = 368
  Top = 194
  Caption = 'PIM-CADAS-003 :: Cadastro de Clientes'
  ClientHeight = 451
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 355
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 488
        Top = 16
        Width = 25
        Height = 13
        Caption = 'Pa'#237's:'
      end
      object Label10: TLabel
        Left = 660
        Top = 16
        Width = 17
        Height = 13
        Caption = 'UF:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 409
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPais: TdmkEditCB
        Left = 488
        Top = 32
        Width = 37
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Pais'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdPaisRedefinido
        DBLookupComboBox = CBPais
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPais: TdmkDBLookupComboBox
        Left = 524
        Top = 32
        Width = 133
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsrPrdcPaiCad
        TabOrder = 3
        dmkEditCB = EdPais
        QryCampo = 'Pais'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CBUF: TdmkDBLookupComboBox
        Left = 660
        Top = 32
        Width = 61
        Height = 21
        KeyField = 'Sigla'
        ListField = 'Sigla'
        ListSource = DsPrdcUfsCad
        TabOrder = 4
        QryCampo = 'UF'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CkAtivo: TdmkCheckBox
        Left = 728
        Top = 36
        Width = 97
        Height = 17
        Caption = 'Ativo'
        TabOrder = 5
        QryCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 292
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 355
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 556
        Top = 16
        Width = 37
        Height = 13
        Caption = 'Regi'#227'o:'
        FocusControl = DBEdit2
      end
      object Label5: TLabel
        Left = 684
        Top = 16
        Width = 17
        Height = 13
        Caption = 'UF:'
        FocusControl = DBEdit3
      end
      object Label3: TLabel
        Left = 392
        Top = 16
        Width = 25
        Height = 13
        Caption = 'Pa'#237's:'
        FocusControl = DBEdit1
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPrdcCliCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 313
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsPrdcCliCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit2: TDBEdit
        Left = 556
        Top = 32
        Width = 121
        Height = 21
        DataField = 'NO_Regiao'
        DataSource = DsPrdcCliCad
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 684
        Top = 32
        Width = 33
        Height = 21
        DataField = 'UF'
        DataSource = DsPrdcCliCad
        TabOrder = 3
      end
      object DBCheckBox1: TDBCheckBox
        Left = 724
        Top = 36
        Width = 53
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsPrdcCliCad
        TabOrder = 4
      end
      object DBEdit1: TDBEdit
        Left = 392
        Top = 32
        Width = 157
        Height = 21
        DataField = 'Pais'
        DataSource = DsPrdcCliCad
        TabOrder = 5
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 291
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cliente'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&'#193'rea x Account'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 65
      Width = 784
      Height = 148
      Align = alTop
      DataSource = DsPrdcCliAcc
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'SiglaAccount'
          Title.Caption = 'Account'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Account'
          Title.Caption = 'Nome do Account'
          Width = 219
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SiglaArea'
          Title.Caption = #193'rea'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_AREA'
          Title.Caption = 'Nome da '#225'rea'
          Width = 159
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 256
        Height = 32
        Caption = 'Cadastro de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 256
        Height = 32
        Caption = 'Cadastro de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 256
        Height = 32
        Caption = 'Cadastro de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrPrdcCliCad: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPrdcCliCadBeforeOpen
    AfterOpen = QrPrdcCliCadAfterOpen
    BeforeClose = QrPrdcCliCadBeforeClose
    AfterScroll = QrPrdcCliCadAfterScroll
    SQL.Strings = (
      'SELECT ppc.Nome NO_Pais, prc.Nome NO_Regiao,'
      'pcc.*'
      'FROM prdcclicad pcc'
      'LEFT JOIN prdcpaicad ppc ON ppc.Codigo=pcc.Pais'
      'LEFT JOIN prdcufscad puc ON puc.Sigla=pcc.UF'
      'LEFT JOIN prdcregcad prc ON prc.Codigo=puc.Regiao')
    Left = 92
    Top = 229
    object QrPrdcCliCadNO_Pais: TWideStringField
      FieldName = 'NO_Pais'
      Size = 30
    end
    object QrPrdcCliCadNO_Regiao: TWideStringField
      FieldName = 'NO_Regiao'
    end
    object QrPrdcCliCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrdcCliCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrPrdcCliCadPais: TIntegerField
      FieldName = 'Pais'
      Required = True
    end
    object QrPrdcCliCadUF: TWideStringField
      FieldName = 'UF'
      Size = 15
    end
    object QrPrdcCliCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsPrdcCliCad: TDataSource
    DataSet = QrPrdcCliCad
    Left = 92
    Top = 277
  end
  object QrPrdcCliAcc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pac.Sigla SiglaArea, pac.Nome NO_AREA, '
      'eif.Sigla SiglaAccount,  '
      'eif.Nome NO_Account, pca.* '
      'FROM prdccliacc pca '
      'LEFT JOIN prdcarecad pac ON pac.Codigo=pca.Area '
      'LEFT JOIN excidfunc eif ON eif.Entidade=pca.Account '
      'WHERE pca.Codigo=1'
      'ORDER BY pca.Account, pac.Sigla ')
    Left = 188
    Top = 229
    object QrPrdcCliAccSiglaArea: TWideStringField
      FieldName = 'SiglaArea'
      Size = 6
    end
    object QrPrdcCliAccNO_AREA: TWideStringField
      FieldName = 'NO_AREA'
      Size = 60
    end
    object QrPrdcCliAccSiglaAccount: TWideStringField
      FieldName = 'SiglaAccount'
      Size = 15
    end
    object QrPrdcCliAccNO_Account: TWideStringField
      FieldName = 'NO_Account'
      Size = 60
    end
    object QrPrdcCliAccCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrdcCliAccControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPrdcCliAccArea: TIntegerField
      FieldName = 'Area'
      Required = True
    end
    object QrPrdcCliAccAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
    object QrPrdcCliAccAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsPrdcCliAcc: TDataSource
    DataSet = QrPrdcCliAcc
    Left = 188
    Top = 277
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrPrdcPaiCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM prdcpaicad'
      'ORDER BY Nome')
    Left = 376
    Top = 260
    object QrPrdcPaiCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrdcPaiCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
  end
  object DsrPrdcPaiCad: TDataSource
    DataSet = QrPrdcPaiCad
    Left = 376
    Top = 309
  end
  object QrPrdcUfsCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Sigla, Nome '
      'FROM prdcufscad'
      'WHERE Pais=1058'
      'ORDER BY Nome')
    Left = 460
    Top = 260
    object QrPrdcUfsCadSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
      Size = 2
    end
    object QrPrdcUfsCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
  end
  object DsPrdcUfsCad: TDataSource
    DataSet = QrPrdcUfsCad
    Left = 460
    Top = 309
  end
end
