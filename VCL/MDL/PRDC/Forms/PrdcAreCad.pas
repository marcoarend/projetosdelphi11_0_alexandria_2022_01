unit PrdcAreCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkCheckBox;

type
  TFmPrdcAreCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrPrdcAreCad: TMySQLQuery;
    DsPrdcAreCad: TDataSource;
    QrPrdcSegCad: TMySQLQuery;
    DsPrdcSegCad: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrPrdcAreCadCodigo: TIntegerField;
    QrPrdcAreCadSigla: TWideStringField;
    QrPrdcAreCadNome: TWideStringField;
    QrPrdcAreCadAtivo: TSmallintField;
    QrPrdcSegCadCodigo: TIntegerField;
    QrPrdcSegCadControle: TIntegerField;
    QrPrdcSegCadUnidade: TIntegerField;
    QrPrdcSegCadAtivo: TSmallintField;
    QrPrdcSegCadNome: TWideStringField;
    EdSigla: TdmkEdit;
    Label3: TLabel;
    CkAtivo: TdmkCheckBox;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    QrPrdcSegCadNO_Unidade: TWideStringField;
    Label5: TLabel;
    EdOrdem: TdmkEdit;
    QrPrdcAreCadOrdem: TIntegerField;
    QrPrdcSegCadOrdem: TIntegerField;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPrdcAreCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPrdcAreCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrPrdcAreCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrPrdcAreCadBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraPrdcSegCad(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPrdcSegCad(Controle: Integer);

  end;

var
  FmPrdcAreCad: TFmPrdcAreCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, PrdcSegCad, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPrdcAreCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPrdcAreCad.MostraPrdcSegCad(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmPrdcSegCad, FmPrdcSegCad, afmoNegarComAviso) then
  begin
    FmPrdcSegCad.ImgTipo.SQLType := SQLType;
    FmPrdcSegCad.FQrCab := QrPrdcAreCad;
    FmPrdcSegCad.FDsCab := DsPrdcAreCad;
    FmPrdcSegCad.FQrIts := QrPrdcSegCad;
    FmPrdcSegCad.FCodigo := QrPrdcAreCadCodigo.Value;
    FmPrdcSegCad.EdCodigo.ValueVariant := QrPrdcAreCadCodigo.Value;
    FmPrdcSegCad.EdNO_Segmento.ValueVariant := QrPrdcAreCadNome.Value;
    if SQLType = stIns then
      //
    else
    begin
      FmPrdcSegCad.EdControle.ValueVariant := QrPrdcSegCadControle.Value;
      //
      FmPrdcSegCad.EdControle.ValueVariant := QrPrdcSegCadControle.Value;
      FmPrdcSegCad.EdNome.ValueVariant     := QrPrdcSegCadNome.Value;
      FmPrdcSegCad.EdUnidade.ValueVariant  := QrPrdcSegCadUnidade.Value;
      FmPrdcSegCad.CBUnidade.KeyValue      := QrPrdcSegCadUnidade.Value;
    end;
    FmPrdcSegCad.ShowModal;
    FmPrdcSegCad.Destroy;
  end;
end;

procedure TFmPrdcAreCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrPrdcAreCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrPrdcAreCad, QrPrdcSegCad);
end;

procedure TFmPrdcAreCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrPrdcAreCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrPrdcSegCad);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrPrdcSegCad);
end;

procedure TFmPrdcAreCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPrdcAreCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPrdcAreCad.DefParams;
begin
  VAR_GOTOTABELA := 'prdcarecad';
  VAR_GOTOMYSQLTABLE := QrPrdcAreCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM prdcarecad');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmPrdcAreCad.ItsAltera1Click(Sender: TObject);
begin
  MostraPrdcSegCad(stUpd);
end;

procedure TFmPrdcAreCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmPrdcAreCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPrdcAreCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPrdcAreCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'PrdcSegCad', 'Controle', QrPrdcSegCadControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrPrdcSegCad,
      QrPrdcSegCadControle, QrPrdcSegCadControle.Value);
    ReopenPrdcSegCad(Controle);
  end;
}
end;

procedure TFmPrdcAreCad.ReopenPrdcSegCad(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrdcSegCad, Dmod.MyDB, [
  'SELECT psc.*, med.Nome NO_Unidade ',
  'FROM prdcsegcad psc ',
  'LEFT JOIN unidmed med ON med.Codigo=psc.Unidade ',
  'WHERE psc.Codigo=' + Geral.FF0(QrPrdcAreCadCodigo.Value),
  'ORDER BY Ordem, Nome ',
  '']);
  //
  QrPrdcSegCad.Locate('Controle', Controle, []);
end;


procedure TFmPrdcAreCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPrdcAreCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPrdcAreCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPrdcAreCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPrdcAreCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPrdcAreCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrdcAreCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPrdcAreCadCodigo.Value;
  Close;
end;

procedure TFmPrdcAreCad.ItsInclui1Click(Sender: TObject);
begin
  MostraPrdcSegCad(stIns);
end;

procedure TFmPrdcAreCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrPrdcAreCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'prdcarecad');
end;

procedure TFmPrdcAreCad.BtConfirmaClick(Sender: TObject);
var
  Sigla, Nome: String;
  Codigo, Ativo, Ordem: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Sigla          := EdSigla.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  Ativo          := Geral.BoolToInt(CkAtivo.Checked);
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(Sigla) = 0, EdSigla, 'Defina uma sigla!') then Exit;
  //
  if DModG.SiglaDuplicada(Sigla, 'Sigla', 'Codigo', 'Nome', 'prdcarecad',
  Codigo) then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('prdcarecad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'prdcarecad', False, [
  'Sigla', 'Nome', 'Ordem', 'Ativo'], [
  'Codigo'], [
  Sigla, Nome, Ordem, Ativo], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmPrdcAreCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'prdcarecad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'prdcarecad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPrdcAreCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmPrdcAreCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmPrdcAreCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmPrdcAreCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPrdcAreCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPrdcAreCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPrdcAreCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPrdcAreCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPrdcAreCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPrdcAreCad.QrPrdcAreCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPrdcAreCad.QrPrdcAreCadAfterScroll(DataSet: TDataSet);
begin
  ReopenPrdcSegCad(0);
end;

procedure TFmPrdcAreCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrPrdcAreCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmPrdcAreCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPrdcAreCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'prdcarecad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPrdcAreCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrdcAreCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrPrdcAreCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'prdcarecad');
end;

procedure TFmPrdcAreCad.QrPrdcAreCadBeforeClose(
  DataSet: TDataSet);
begin
  QrPrdcSegCad.Close;
end;

procedure TFmPrdcAreCad.QrPrdcAreCadBeforeOpen(DataSet: TDataSet);
begin
  QrPrdcAreCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

