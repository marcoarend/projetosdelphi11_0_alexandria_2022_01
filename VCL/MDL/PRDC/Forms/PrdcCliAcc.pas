unit PrdcCliAcc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmPrdcCliAcc = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrPrdcAreCad: TMySQLQuery;
    DsPrdcAreCad: TDataSource;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label1: TLabel;
    EdArea: TdmkEditCB;
    CBArea: TdmkDBLookupComboBox;
    SbArea: TSpeedButton;
    Panel5: TPanel;
    Label2: TLabel;
    EdCodigo: TdmkEdit;
    EdNO_Cliente: TdmkEdit;
    EdAccount: TdmkEditCB;
    Label3: TLabel;
    CBAccount: TdmkDBLookupComboBox;
    SbAccount: TSpeedButton;
    QrExcIdFunc: TMySQLQuery;
    DsExcIdFunc: TDataSource;
    QrExcIdFuncEntidade: TIntegerField;
    QrExcIdFuncNome: TWideStringField;
    QrExcIdFuncSigla: TWideStringField;
    QrPrdcAreCadCodigo: TIntegerField;
    QrPrdcAreCadSigla: TWideStringField;
    QrPrdcAreCadNome: TWideStringField;
    QrExcIdFuncCodigo: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbAreaClick(Sender: TObject);
    procedure SbAccountClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FCodigo: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmPrdcCliAcc: TFmPrdcCliAcc;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnExesD_Jan, UnPrdc_Jan;

{$R *.DFM}

procedure TFmPrdcCliAcc.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Area, Account: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  Area           := EdArea.ValueVariant;
  Account        := EdAccount.ValueVariant;
  //
  if MyObjects.FIC(Area = 0, EdArea, 'Defina a �rea de atua��o!') then Exit;
  if MyObjects.FIC(Account = 0, EdAccount, 'Defina o account!') then Exit;
  //
  Controle := UMyMod.BPGS1I32('prdccliacc', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'prdccliacc', False, [
  'Codigo', 'Area', 'Account'], [
  'Controle'], [
  Codigo, Area, Account], [
  Controle], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdArea.ValueVariant      := 0;
      CBArea.KeyValue          := Null;
      EdAccount.ValueVariant   := 0;
      CBAccount.KeyValue       := Null;
    end else Close;
  end;
end;

procedure TFmPrdcCliAcc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrdcCliAcc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPrdcCliAcc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrPrdcAreCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrExcIdFunc, Dmod.MyDB);
end;

procedure TFmPrdcCliAcc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrdcCliAcc.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmPrdcCliAcc.SbAccountClick(Sender: TObject);
var
  ExcIdFunc: Integer;
begin
  VAR_ENTIDADE := 0;
  if EdAccount.ValueVariant = 0 then
    ExcIdFunc := 0
  else
    ExcIdFunc := QrExcIdFuncCodigo.Value;
  ExesD_Jan.MostraFormEXcIdFunc(ExcIdFunc);
  UMyMod.SetaCodigoPesquisado(EdAccount, CBAccount, QrExcIdFunc, VAR_ENTIDADE);
end;

procedure TFmPrdcCliAcc.SbAreaClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Prdc_Jan.MostraFormPrdcAreCad(EdArea.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdArea, CBArea, QrPrdcAreCad, VAR_CADASTRO);
end;


end.
