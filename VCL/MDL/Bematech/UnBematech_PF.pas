unit UnBematech_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  UnDmkProcFunc, UnProjGroup_Vars;

type
  TUnBematech_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function ModeloImpressoraTermica(Nome: String): Integer;
  end;
var
  Bematech_PF: TUnBematech_PF;

implementation

{ TUnBematech_PF }

function TUnBematech_PF.ModeloImpressoraTermica(Nome: String): Integer;
begin
  Result := -1;
  if Nome = 'MP 20 CI' then
    Result := 1
  else
  if Nome = 'MP 20 MI' then
    Result := 1
  else
  if Nome = 'MP 20 TH' then
    Result := 0
  else
  if Nome = 'MP 2000 CI' then
    Result := 0
  else
  if Nome = 'MP 2000 TH' then
    Result := 0
  else
  if Nome = 'MP 2100 TH' then
    Result := 0
  else
  if Nome = 'MP 4000 TH' then
    Result := 5
  else
  if Nome = 'MP 4200 TH' then
    Result := 7
  else
  if Nome = 'MP 2500 TH' then
    Result := 8
  else
    Geral.MB_Aviso('Modelo de impressora t�rmica incorreto ou n�o informado');
end;

end.
