unit UnTributos_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, Math, UnMsgInt,
  Db, DbCtrls,   Mask, Buttons, ZCF2, (*DBTables,*) mySQLDbTables, ComCtrls,
  (*DBIProcs,*) Registry, Grids, DBGrids, CheckLst,
  printers, CommCtrl, TypInfo, comobj, ShlObj, RichEdit, ShellAPI, Consts,
  ActiveX, OleCtrls, SHDocVw, UnDmkProcFunc,
  Variants, MaskUtils, RTLConsts, IniFiles, frxClass, frxPreview,
  mySQLExceptions,
  // Dermatek
  dmkGeral, dmkEdit, dmkDBEdit, dmkEditF7, dmkEditDateTimePicker, dmkCheckGroup,
  dmkDBLookupCombobox, dmkEditCB, dmkDBGrid, dmkDBGridDAC, dmkImage, UnDmkEnums;

type

  TUnTributos_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormTribDefCad(Codigo: Integer);
    procedure MostraFormTribDefCab(Codigo: Integer);
    procedure MostraFormTribIncIts(SQLType: TSQLType; QrTribIncIts: TmySQLQuery;
              FatID, FatNum, FatParcela, Empresa, Controle: Integer; Data, Hora:
              TDateTime; Operacao, Tributo: Integer; ValorFat, BaseCalc,
              Percent, VUsoCalc, ValrTrib: Double; Sinal: TSinal);
    procedure Tributos_DefineValores(const Operacao: Integer; const Percent:
              Double; const BaseCalc: Double; var VUsoCalc, ValrTrib: Double);
    function  Tributos_InsUpd(SQLType: TSQLType; CtrlUpd, FatID, FatNum,
              FatParcela, Empresa: Integer; Data, Hora: String; Tributo:
              Integer; ValorFat, BaseCalc, VUsoCalc, ValrTrib, Percent: Double;
              Operacao, FatorDC: Integer): Integer;
    procedure Tributos_Insere(FatID, FatNum, FatParcela, Empresa, TribDefCab:
              Integer; ValorFat, BaseCalc: Double; DataHora: TDateTime;
              Sinal: TSinal);
    procedure Tributos_GerenciaAlt(QrTribIncIts: TmySQLQuery; ValorFat,
              BaseCalc: Double);

    end;

var
  Tributos_PF: TUnTributos_PF;

implementation

uses MyDBCheck, TribDefCad, TribDefCab, TribDefIts, TribIncIts, Module,
  DmkDAC_PF, UMySQLModule;

{ TUnTributos_PF }

procedure TUnTributos_PF.MostraFormTribDefCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmTribDefCab, FmTribDefCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmTribDefCab.LocCod(Codigo, Codigo);
      //FmTribDefCab.ReopenVSPaClaCab();
    end;
    //
    FmTribDefCab.ShowModal;
    FmTribDefCab.Destroy;
  end;
end;

procedure TUnTributos_PF.MostraFormTribDefCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmTribDefCad, FmTribDefCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmTribDefCad.LocCod(Codigo, Codigo);
      //FmTribDefCad.ReopenVSPaClaCab();
    end;
    //
    FmTribDefCad.ShowModal;
    FmTribDefCad.Destroy;
  end;
end;

procedure TUnTributos_PF.MostraFormTribIncIts(SQLType: TSQLType; QrTribIncIts:
  TmySQLQuery; FatID, FatNum, FatParcela, Empresa, Controle: Integer; Data,
  Hora: TDateTime; Operacao, Tributo: Integer; ValorFat, BaseCalc, Percent,
  VUsoCalc, ValrTrib: Double; Sinal: TSinal);
begin
  if DBCheck.CriaFm(TFmTribIncIts, FmTribIncIts, afmoNegarComAviso) then
  begin
    FmTribIncIts.ImgTipo.SQLType := SQLType;
    //FmTribIncIts.FQrCab := QrTribIncCab;
    //FmTribIncIts.FDsCab := DsTribIncCab;
    FmTribIncIts.FQrIts := QrTribIncIts;
    //
    FmTribIncIts.EdFatID.ValueVariant          := FatID;
    FmTribIncIts.EdFatNum.ValueVariant         := FatNum;
    FmTribIncIts.EdFatParcela.ValueVariant     := FatParcela;
    FmTribIncIts.EdEmpresa.ValueVariant        := Empresa;
    FmTribIncIts.TPData.Date                   := Data;
    FmTribIncIts.EdHora.ValueVariant           := Hora;
    FmTribIncIts.EdValorFat.ValueVariant       := ValorFat;
    FmTribIncIts.EdBaseCalc.ValueVariant       := BaseCalc;
    if SQLType = stIns then
      //
    else
    begin
      FmTribIncIts.EdControle.ValueVariant       := Controle;
      //
      FmTribIncIts.RGOperacao.ItemIndex          := Operacao;
      FmTribIncIts.EdTributo.ValueVariant        := Tributo;
      FmTribIncIts.CBTributo.KeyValue            := Tributo;
      FmTribIncIts.EdVUsoCalc.ValueVariant       := VUsoCalc;
      FmTribIncIts.EdPercent.ValueVariant        := Percent;
      FmTribIncIts.EdValrTrib.ValueVariant       := ValrTrib;
      //
      FmTribIncIts.CkContinuar.Checked           := False;
    end;
    FmTribIncIts.ShowModal;
    FmTribIncIts.Destroy;
  end;
end;

procedure TUnTributos_PF.Tributos_DefineValores(const Operacao: Integer;
  const Percent, BaseCalc: Double; var VUsoCalc, ValrTrib: Double);
begin
  case TTribOperacao(Operacao) of
    (*0*)tribcalcNone: VUsoCalc := 0;
    (*1*)tribcalcInclusoBase: VUsoCalc := BaseCalc;
    (*2*)tribcalcExclusoBase: VUsoCalc := BaseCalc;
    {
    (*3*)tribcalcInclusoNivel:
    (*4*)tribcalcExclusoNivel:
    }
    else
    begin
      VUsoCalc := 0;
      Geral.MB_Aviso('Opera��o n�o implementada em "VS_PF.InsereTributos()"');
    end;
  end;
  ValrTrib := Percent * VUsoCalc / 100;
  ValrTrib := Geral.RoundC(ValrTrib, 2);
end;

procedure TUnTributos_PF.Tributos_GerenciaAlt(QrTribIncIts: TmySQLQuery;
ValorFat, BaseCalc: Double);
const
  EditManual = 0;
  SQLType = TSQLType.stUpd;
var
  Data, Hora: String;
  FatID, FatNum, FatParcela, Empresa, Controle, Operacao, Tributo, FatorDC: Integer;
  VUsoCalc, ValrTrib, Percent, OldValorFat, OldBaseCalc: Double;
begin
  if Geral.MB_Pergunta('Deseja alterar os valores dos impostos?') = ID_YES then
  begin
    QrTribIncIts.First;
    while not QrTribIncIts.Eof do
    begin
      Controle       := QrTribIncIts.FieldByName('Controle').AsInteger;
      //
      FatID          := QrTribIncIts.FieldByName('FatID').AsInteger;
      FatNum         := QrTribIncIts.FieldByName('FatNum').AsInteger;
      FatParcela     := QrTribIncIts.FieldByName('FatParcela').AsInteger;
      Empresa        := QrTribIncIts.FieldByName('Empresa').AsInteger;
      Tributo        := QrTribIncIts.FieldByName('Tributo').AsInteger;
      FatorDC        := QrTribIncIts.FieldByName('FatorDC').AsInteger;
      Data           := Geral.FDT(QrTribIncIts.FieldByName('Data').AsDateTime, 1);
      Hora           := Geral.FDT(QrTribIncIts.FieldByName('Hora').AsDateTime, 100);
      //
      OldValorFat    := QrTribIncIts.FieldByName('ValorFat').AsFloat;
      OldBaseCalc    := QrTribIncIts.FieldByName('BaseCalc').AsFloat;
      //VUsoCalc       := ;
      //ValrTrib       := ;
      Percent        := QrTribIncIts.FieldByName('Percent').AsFloat;
      Operacao       := QrTribIncIts.FieldByName('Operacao').AsInteger;
      //
      Tributos_DefineValores(Operacao, Percent, BaseCalc, VUsoCalc, ValrTrib);
      //
      Tributos_PF.Tributos_InsUpd(SQLType, Controle, FatID, FatNum,
      FatParcela, Empresa, Data, Hora, Tributo, ValorFat, BaseCalc, VUsoCalc,
      ValrTrib, Percent, Operacao, FatorDC);
      //
      QrTribIncIts.Next;
    end;
  end;
end;

procedure TUnTributos_PF.Tributos_Insere(FatID, FatNum, FatParcela, Empresa,
  TribDefCab: Integer; ValorFat, BaseCalc: Double; DataHora: TDateTime;
  Sinal: TSinal);
var
  Data, Hora: String;
  Controle, Tributo, Operacao, FatorDC: Integer;
  ValrTrib, VUsoCalc, Percent, ValTriNiv: Double;
  //
  Qry1, Qry2: TmySQLQuery;
begin
  if Sinal = siNegativo then
    FatorDC := -1
  else
    FatorDC := 1;
  //
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry2 := TmySQLQuery.Create(Dmod);
    try
      Data           := Geral.FDT(DataHora, 1);
      Hora           := Geral.FDT(DataHora, 100);
      //VNivCalc       := BaseCalc;
      ValTriNiv      := 0;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT DISTINCT Nivel ',
      'FROM tribdefits tdi ',
      'WHERE tdi.Codigo=' + Geral.FF0(TribDefCab),
      'ORDER BY Nivel DESC',
      '']);
      Qry1.First;
      while not Qry1.Eof do
      begin
        //VNivCalc  := VNivCalc - ValTriNiv;
        ValTriNiv := 0;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
        'SELECT tdi.*',
        'FROM tribdefits tdi ',
        'WHERE tdi.Codigo=' + Geral.FF0(TribDefCab),
        'AND Nivel=' + Geral.FF0(Qry1.FieldByName('Nivel').AsInteger),
        'ORDER BY Controle DESC',
        '']);
        Qry2.First;
        while not Qry2.Eof do
        begin
          Controle       := 0;
          Percent        := Qry2.FieldByName('Percent').AsFloat;
          Tributo        := Qry2.FieldByName('Tributo').AsInteger;
          Operacao       := Qry2.FieldByName('Operacao').AsInteger;

          Tributos_DefineValores(Operacao, Percent, BaseCalc, VUsoCalc, ValrTrib);
{
          case TTribOperacao(Operacao) of
            (*0*)tribcalcNone: VUsoCalc := 0;
            (*1*)tribcalcInclusoBase: VUsoCalc := BaseCalc;
            (*2*)tribcalcExclusoBase: VUsoCalc := BaseCalc;

            (*3*)//tribcalcInclusoNivel:
            (*4*)//tribcalcExclusoNivel:

            else
            begin
              VUsoCalc := 0;
              Geral.MB_Aviso('Opera��o n�o implementada em "VS_PF.InsereTributos()"');
            end;
          end;
          ValrTrib := Percent * VUsoCalc / 100;
          ValrTrib := Geral.RoundC(ValrTrib, 2);
          //
}
          Tributos_InsUpd(stIns, Controle, FatID, FatNum, FatParcela, Empresa,
          Data, Hora, Tributo, ValorFat, BaseCalc, VUsoCalc, ValrTrib, Percent,
          Operacao, FatorDC);
{
          Controle := UMyMod.BPGS1I32('tribincits', 'Controle', '', '', tsPos, stIns, Controle);
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'tribincits', False, [
          'FatID', 'FatNum', 'FatParcela',
          'Empresa', 'Data', 'Hora',
          'Tributo', 'ValorFat', 'BaseCalc',
          'VUsoCalc', 'ValrTrib', 'Percent',
          'Operacao', 'FatorDC'], [
          'Controle'], [
          FatID, FatNum, FatParcela,
          Empresa, Data, Hora,
          Tributo, ValorFat, BaseCalc,
          VUsoCalc, ValrTrib, Percent,
          Operacao, FatorDC], [
          Controle], True) then
          begin
            //
          end;
}
          //
          Qry2.Next;
        end;
        Qry1.Next;
      end;
    finally
      Qry2.Free;
    end;
  finally
    Qry1.Free;
  end;
end;

function TUnTributos_PF.Tributos_InsUpd(SQLType: TSQLType; CtrlUpd, FatID,
  FatNum, FatParcela, Empresa: Integer; Data, Hora: String; Tributo: Integer;
  ValorFat, BaseCalc, VUsoCalc, ValrTrib, Percent: Double; Operacao, FatorDC:
  Integer): Integer;
var
  Controle: Integer;
begin
  if SQLType = stUpd then
    Result := CtrlUpd
  else
    Result := 0;
  //
  Controle := UMyMod.BPGS1I32('tribincits', 'Controle', '', '', tsPos, SQLType,
    Result);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'tribincits', False, [
  'FatID', 'FatNum', 'FatParcela',
  'Empresa', 'Data', 'Hora',
  'Tributo', 'ValorFat', 'BaseCalc',
  'VUsoCalc', 'ValrTrib', 'Percent',
  'Operacao', 'FatorDC'], [
  'Controle'], [
  FatID, FatNum, FatParcela,
  Empresa, Data, Hora,
  Tributo, ValorFat, BaseCalc,
  VUsoCalc, ValrTrib, Percent,
  Operacao, FatorDC], [
  Controle], True) then
  begin
    Result := Controle;
  end;
end;

end.
