unit TribDefCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmTribDefCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrTribDefCab: TmySQLQuery;
    DsTribDefCab: TDataSource;
    QrTribDefIts: TmySQLQuery;
    DsTribDefIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrTribDefItsCodigo: TIntegerField;
    QrTribDefItsControle: TIntegerField;
    QrTribDefItsNivel: TIntegerField;
    QrTribDefItsTributo: TIntegerField;
    QrTribDefItsOperacao: TSmallintField;
    QrTribDefItsPercent: TFloatField;
    QrTribDefItsLk: TIntegerField;
    QrTribDefItsDataCad: TDateField;
    QrTribDefItsDataAlt: TDateField;
    QrTribDefItsUserCad: TIntegerField;
    QrTribDefItsUserAlt: TIntegerField;
    QrTribDefItsAlterWeb: TSmallintField;
    QrTribDefItsAtivo: TSmallintField;
    QrTribDefItsNO_Tributo: TWideStringField;
    QrTribDefItsNO_Operacao: TWideStringField;
    QrTribDefCabCodigo: TIntegerField;
    QrTribDefCabNome: TWideStringField;
    QrTribDefCabLk: TIntegerField;
    QrTribDefCabDataCad: TDateField;
    QrTribDefCabDataAlt: TDateField;
    QrTribDefCabUserCad: TIntegerField;
    QrTribDefCabUserAlt: TIntegerField;
    QrTribDefCabAlterWeb: TSmallintField;
    QrTribDefCabAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTribDefCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTribDefCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTribDefCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrTribDefCabBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraTribDefIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenTribDefIts(Controle: Integer);

  end;

var
  FmTribDefCab: TFmTribDefCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, TribDefIts, UnVS_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTribDefCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTribDefCab.MostraTribDefIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmTribDefIts, FmTribDefIts, afmoNegarComAviso) then
  begin
    FmTribDefIts.ImgTipo.SQLType := SQLType;
    FmTribDefIts.FQrCab := QrTribDefCab;
    FmTribDefIts.FDsCab := DsTribDefCab;
    FmTribDefIts.FQrIts := QrTribDefIts;
    if SQLType = stIns then
      //
    else
    begin
      FmTribDefIts.EdControle.ValueVariant := QrTribDefItsControle.Value;
      //
      FmTribDefIts.EdTributo.ValueVariant := QrTribDefItsTributo.Value;
      FmTribDefIts.CBTributo.KeyValue     := QrTribDefItsTributo.Value;
      FmTribDefIts.EdNivel.ValueVariant   := QrTribDefItsNivel.Value;
      FmTribDefIts.EdPercent.ValueVariant := QrTribDefItsPercent.Value;
      FmTribDefIts.RGOperacao.ItemIndex   := QrTribDefItsOperacao.Value;
    end;
    FmTribDefIts.ShowModal;
    FmTribDefIts.Destroy;
  end;
end;

procedure TFmTribDefCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTribDefCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrTribDefCab, QrTribDefIts);
end;

procedure TFmTribDefCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrTribDefCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrTribDefIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrTribDefIts);
end;

procedure TFmTribDefCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTribDefCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTribDefCab.DefParams;
begin
  VAR_GOTOTABELA := 'tribdefcab';
  VAR_GOTOMYSQLTABLE := QrTribDefCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM tribdefcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTribDefCab.ItsAltera1Click(Sender: TObject);
begin
  MostraTribDefIts(stUpd);
end;

procedure TFmTribDefCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmTribDefCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTribDefCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTribDefCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if VS_PF.ExcluiVSNaoVMI('Confirma a exclus�o do item selecionado?',
  'tribdefits', 'Controle', QrTribDefItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrTribDefIts,
      QrTribDefItsControle, QrTribDefItsControle.Value);
    ReopenTribDefIts(Controle);
  end;
end;

procedure TFmTribDefCab.ReopenTribDefIts(Controle: Integer);
var
  ATT_Operacao: String;
begin
  ATT_Operacao := dmkPF.ArrayToTexto('tdi.Operacao', 'NO_Operacao', pvPos, True,
    sListaTribOperacao);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTribDefIts, Dmod.MyDB, [
  'SELECT tdi.*, ',
  ATT_Operacao,
  'tdd.Nome NO_Tributo ',
  'FROM tribdefits tdi ',
  'LEFT JOIN tribdefcad tdd ON tdd.Codigo=tdi.Tributo ',
  'WHERE tdi.Codigo=' + Geral.FF0(QrTribDefCabCodigo.Value),
  'ORDER BY Nivel, Controle',
  '']);
  //
  QrTribDefIts.Locate('Controle', Controle, []);
end;


procedure TFmTribDefCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTribDefCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTribDefCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTribDefCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTribDefCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTribDefCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTribDefCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTribDefCabCodigo.Value;
  Close;
end;

procedure TFmTribDefCab.ItsInclui1Click(Sender: TObject);
begin
  MostraTribDefIts(stIns);
end;

procedure TFmTribDefCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTribDefCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'tribdefcab');
end;

procedure TFmTribDefCab.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('tribdefcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'tribdefcab', False, [
  'Nome'], [
  'Codigo'], [
  Nome], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmTribDefCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'tribdefcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'tribdefcab', 'Codigo');
end;

procedure TFmTribDefCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmTribDefCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTribDefCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmTribDefCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTribDefCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTribDefCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTribDefCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrTribDefCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTribDefCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTribDefCab.QrTribDefCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTribDefCab.QrTribDefCabAfterScroll(DataSet: TDataSet);
begin
  ReopenTribDefIts(0);
end;

procedure TFmTribDefCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTribDefCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmTribDefCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTribDefCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'tribdefcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTribDefCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTribDefCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTribDefCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'tribdefcab');
end;

procedure TFmTribDefCab.QrTribDefCabBeforeClose(
  DataSet: TDataSet);
begin
  QrTribDefIts.Close;
end;

procedure TFmTribDefCab.QrTribDefCabBeforeOpen(DataSet: TDataSet);
begin
  QrTribDefCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

