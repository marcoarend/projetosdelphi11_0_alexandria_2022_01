unit UnTributos_Tabs;

{ Colocar no MyListas:

Uses UnTributos_Tabs;


//
function TMyListas.CriaListaTabelas:
      Tributos_Tabs.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
    Tributos_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
    Tributos_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      Tributos_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      Tributos_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    Tributos_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  Tributos_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms,
  UnInternalConsts, dmkGeral, UnDmkEnums;

type
  TUnTributos_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  Tributos_Tabs: TUnTributos_Tabs;

implementation

function TUnTributos_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, Lowercase('TribDefCad'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('TribDefCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('TribDefIts'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('TribIncIts'), '');
    //
    //MyLinguas.AdTbLst(Lista, False, Lowercase('???'), '_lst_sample');
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnTributos_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
{
separador = "|"
}
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('????') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo|Nome');
    //FListaSQL.Add('0|   ""');
  end else
end;

function TUnTributos_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
{
  if Uppercase(Tabela) = Uppercase('??') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"ND"');
    FListaSQL.Add('1|"Telefone"');
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades"|"Cadastro de pessoas f�sicas e jur�dicas (clientes| fornecedores| etc.)"');
    //FListaSQL.Add('""|""');
  end;
}
end;


function TUnTributos_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('TribDefCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('TribDefCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('TribDefIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('TribIncIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
end;

function TUnTributos_Tabs.CarregaListaFRQeiLnk(TabelaCria: String;
  FRQeiLnk: TQeiLnk; FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TUnTributos_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    //end else
    if Uppercase(Tabela) = Uppercase('TribDefCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'I??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'I???';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('TribDefCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('TribDefIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tributo';  // TribDefCad
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Operacao';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  // 1=Soma, 2=Multiplica, 3=Divide
      FRCampos.Extra      := '';   // tribcalcNone=0, tribcalcSoma=1, tribcalcMult=2, tribcalcDivi=3
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Percent';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('TribIncIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'FatID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatParcela';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Data';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Y10
      New(FRCampos);
      FRCampos.Field      := 'Hora';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tributo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorFat';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BaseCalc';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VUsoCalc';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValrTrib';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Percent';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Operacao';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EditManual';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorDC';
      FRCampos.Tipo       := 'tinyint(1)'; // 1=credito, -1=Debito
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
  except
    raise;
    Result := False;
  end;
end;

function TUnTributos_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnTributos_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  //
  // TRI-IMPOS-001 :: Cadastro de Tributos
  New(FRJanelas);
  FRJanelas.ID        := 'TRI-IMPOS-001';
  FRJanelas.Nome      := 'FmTribDefCad';
  FRJanelas.Descricao := 'Cadastro de Tributos';
  FLJanelas.Add(FRJanelas);
  //
  // TRI-IMPOS-002 :: Configura��o de Tributos
  New(FRJanelas);
  FRJanelas.ID        := 'TRI-IMPOS-002';
  FRJanelas.Nome      := 'FmTribDefCab';
  FRJanelas.Descricao := 'Cadastro de Tributos';
  FLJanelas.Add(FRJanelas);
  //
  // TRI-IMPOS-003 :: Item de Configura��o de Tributos
  New(FRJanelas);
  FRJanelas.ID        := 'TRI-IMPOS-003';
  FRJanelas.Nome      := 'FmTribDefIts';
  FRJanelas.Descricao := 'Item de Configura��o de Tributos';
  FLJanelas.Add(FRJanelas);
  //
  // TRI-IMPOS-004 :: Item de Tributo
  New(FRJanelas);
  FRJanelas.ID        := 'TRI-IMPOS-004';
  FRJanelas.Nome      := 'FmTribIncIts';
  FRJanelas.Descricao := 'Item de Tributo';
  FLJanelas.Add(FRJanelas);
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
