unit TribDefIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmTribDefIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBTributo: TdmkDBLookupComboBox;
    EdTributo: TdmkEditCB;
    Label1: TLabel;
    SBTributo: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrTribDefCad: TmySQLQuery;
    QrTribDefCadCodigo: TIntegerField;
    QrTribDefCadNome: TWideStringField;
    DsTribDefCad: TDataSource;
    EdNivel: TdmkEdit;
    Label2: TLabel;
    RGOperacao: TdmkRadioGroup;
    EdPercent: TdmkEdit;
    Label4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBTributoClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenTribDefIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmTribDefIts: TFmTribDefIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnTributos_PF;

{$R *.DFM}

procedure TFmTribDefIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Nivel, Tributo, Operacao: Integer;
  Percent: Double;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Nivel          := EdNivel.ValueVariant;
  Tributo        := EdTributo.ValueVariant;
  Operacao       := RGOperacao.ItemIndex;
  Percent        := EdPercent.ValueVariant;
  //
  if MyObjects.FIC(Tributo = 0, EdTributo, 'Informe o tributo!') then
    Exit;
  if MyObjects.FIC(Operacao = 0, RGOperacao, 'Informe a opera��o!') then
    Exit;
  if MyObjects.FIC(Percent = 0, EdPercent, 'Informe o percentual!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('tribdefits', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'tribdefits', False, [
  'Codigo', 'Nivel', 'Tributo',
  'Operacao', 'Percent'], [
  'Controle'], [
  Codigo, Nivel, Tributo,
  Operacao, Percent], [
  Controle], True) then
  begin
    ReopenTribDefIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdTributo.ValueVariant   := 0;
      CBTributo.KeyValue       := Null;
      //EdNivel.ValueVariant     := 0;
      RGOperacao.ItemIndex     := 0;
      EdPercent.ValueVariant   := 0;
      //
      EdTributo.SetFocus;
    end else Close;
  end;
end;

procedure TFmTribDefIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTribDefIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmTribDefIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.ConfiguraRadioGroup(RGOperacao, sListaTribOperacao, 2, 0);
  //
  UnDmkDAC_PF.AbreQuery(QrTribDefCad, Dmod.MyDB);
end;

procedure TFmTribDefIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTribDefIts.ReopenTribDefIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTribDefIts.SBTributoClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Tributos_PF.MostraFormTribDefCad(EdTributo.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdTributo, CBTributo, QrTribDefCad, VAR_CADASTRO);
end;

end.
