unit TribIncIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.ComCtrls,
  dmkEditDateTimePicker;

type
  TFmTribIncIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBTributo: TdmkDBLookupComboBox;
    EdTributo: TdmkEditCB;
    Label1: TLabel;
    SBTributo: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrTribDefCad: TmySQLQuery;
    QrTribDefCadCodigo: TIntegerField;
    QrTribDefCadNome: TWideStringField;
    DsTribDefCad: TDataSource;
    RGOperacao: TdmkRadioGroup;
    Label19: TLabel;
    EdValorFat: TdmkEdit;
    EdBaseCalc: TdmkEdit;
    Label20: TLabel;
    Label21: TLabel;
    EdPercent: TdmkEdit;
    Label2: TLabel;
    EdValrTrib: TdmkEdit;
    EdVUsoCalc: TdmkEdit;
    Label4: TLabel;
    EdFatID: TdmkEdit;
    Label3: TLabel;
    EdFatNum: TdmkEdit;
    Label5: TLabel;
    EdFatParcela: TdmkEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdEmpresa: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    Label9: TLabel;
    Label10: TLabel;
    EdHora: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBTributoClick(Sender: TObject);
    procedure EdVUsoCalcRedefinido(Sender: TObject);
    procedure EdPercentRedefinido(Sender: TObject);
    procedure EdValrTribRedefinido(Sender: TObject);
  private
    { Private declarations }
    FCriando: Boolean;
    //
    procedure ReopenTribIncIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FSinal: TSinal;
  end;

  var
  FmTribIncIts: TFmTribIncIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnTributos_PF;

{$R *.DFM}

procedure TFmTribIncIts.BtOKClick(Sender: TObject);
var
  Data, Hora: String;
  FatID, FatNum, FatParcela, Empresa, Controle, Tributo, Operacao, EditManual,
  FatorDC: Integer;
  ValorFat, BaseCalc, ValrTrib, Percent, VUsoCalc: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  //
  FatID          := EdFatID.ValueVariant;
  FatNum         := EdFatNum.ValueVariant;
  FatParcela     := EdFatParcela.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  Controle       := EdControle.ValueVariant;
  Data           := Geral.FDT(TPData.Date, 1);
  Hora           := EdHora.Text;
  ValorFat       := EdValorFat.ValueVariant;
  BaseCalc       := EdBaseCalc.ValueVariant;
  ValrTrib       := EdValrTrib.ValueVariant;
  Percent        := EdPercent.ValueVariant;
  Tributo        := EdTributo.ValueVariant;
  VUsoCalc       := EdVUsoCalc.ValueVariant;
  Operacao       := RGOperacao.ItemIndex;
  EditManual     := 1;
  FatorDC        := Integer(FSinal) * -1;
  //
  Controle := Tributos_PF.Tributos_InsUpd(SQLType, Controle, FatID, FatNum,
  FatParcela, Empresa, Data, Hora, Tributo, ValorFat, BaseCalc, VUsoCalc,
  ValrTrib, Percent, Operacao, FatorDC);
(*
  Controle := UMyMod.BPGS1I32('tribincits', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'tribincits', False, [
  'FatID', 'FatNum', 'FatParcela',
  'Empresa', 'Data', 'Hora',
  'ValorFat', 'BaseCalc', 'ValrTrib',
  'Percent', 'Tributo', 'Operacao',
  'VUsoCalc', 'EditManual', 'FatorDC'], [
  'Controle'], [
  FatID, FatNum, FatParcela,
  Empresa, Data, Hora,
  ValorFat, BaseCalc, ValrTrib,
  Percent, Tributo, Operacao,
  VUsoCalc, EditManual, FatorDC], [
  Controle], True) then
*)
  if Controle <> 0 then
  begin
    ReopenTribIncIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      //
      EdTributo.ValueVariant     := 0;
      CBTributo.KeyValue         := Null;
      //
      EdVUsoCalc.ValueVariant    := 0;
      EdPercent.ValueVariant     := 0;
      EdValrTrib.ValueVariant    := 0;
      //
      EdTributo.SetFocus;
    end else Close;
  end;
end;

procedure TFmTribIncIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTribIncIts.EdPercentRedefinido(Sender: TObject);
begin
  if FCriando then Exit;
  //
  EdValrTrib.ValueVariant :=
  EdVUsoCalc.ValueVariant * EdPercent.ValueVariant / 100;
end;

procedure TFmTribIncIts.EdValrTribRedefinido(Sender: TObject);
begin
  if FCriando then Exit;
  //
  if EdVUsoCalc.ValueVariant > 0 then
    EdPercent.ValueVariant :=
      (EdValrTrib.ValueVariant * 100) / EdVUsoCalc.ValueVariant
  else
    EdPercent.ValueVariant := 0;
end;

procedure TFmTribIncIts.EdVUsoCalcRedefinido(Sender: TObject);
begin
  if FCriando then Exit;
  //
  EdValrTrib.ValueVariant :=
  EdVUsoCalc.ValueVariant * EdPercent.ValueVariant / 100;
end;

procedure TFmTribIncIts.FormActivate(Sender: TObject);
begin
  FCriando := False;
  MyObjects.CorIniComponente();
end;

procedure TFmTribIncIts.FormCreate(Sender: TObject);
begin
  FCriando := True;
  ImgTipo.SQLType := stLok;
  //
  MyObjects.ConfiguraRadioGroup(RGOperacao, sListaTribOperacao, 2, 0);
  //
  UnDmkDAC_PF.AbreQuery(QrTribDefCad, Dmod.MyDB);
end;

procedure TFmTribIncIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTribIncIts.ReopenTribIncIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTribIncIts.SBTributoClick(Sender: TObject);
var
  Tributo: Integer;
begin
  VAR_CADASTRO := 0;
  Tributo := EdTributo.ValueVariant;
  Tributos_PF.MostraFormTribDefCad(Tributo);
  UMyMod.SetaCodigoPesquisado(EdTributo, CBTributo, QrTribDefCad, VAR_CADASTRO);
end;

end.
