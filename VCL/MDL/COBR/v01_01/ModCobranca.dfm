object DmModCobranca: TDmModCobranca
  OldCreateOrder = False
  Height = 678
  Width = 1003
  object QrCtrlGeral: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctrlgeral')
    Left = 156
    Top = 52
    object QrCtrlGeralUltPsqCobr: TDateField
      FieldName = 'UltPsqCobr'
    end
  end
  object QrPesq3: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = QrPesq3CalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM _inadimp_')
    Left = 64
    Top = 4
    object QrPesq3Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = '_inadimp_.Empresa'
    end
    object QrPesq3ForneceI: TIntegerField
      FieldName = 'ForneceI'
      Origin = '_inadimp_.ForneceI'
    end
    object QrPesq3Apto: TIntegerField
      FieldName = 'Apto'
      Origin = '_inadimp_.Apto'
    end
    object QrPesq3Mez: TIntegerField
      FieldName = 'Mez'
      Origin = '_inadimp_.Mez'
    end
    object QrPesq3Vencimento: TDateField
      FieldName = 'Vencimento'
      Origin = '_inadimp_.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq3FatNum: TFloatField
      FieldName = 'FatNum'
      Origin = '_inadimp_.FatNum'
    end
    object QrPesq3Propriet: TIntegerField
      FieldName = 'Propriet'
      Origin = '_inadimp_.Propriet'
    end
    object QrPesq3Unidade: TWideStringField
      FieldName = 'Unidade'
      Origin = '_inadimp_.Unidade'
      Size = 10
    end
    object QrPesq3Imobiliaria: TIntegerField
      FieldName = 'Imobiliaria'
      Origin = '_inadimp_.Imobiliaria'
    end
    object QrPesq3Procurador: TIntegerField
      FieldName = 'Procurador'
      Origin = '_inadimp_.Procurador'
    end
    object QrPesq3Usuario: TIntegerField
      FieldName = 'Usuario'
      Origin = '_inadimp_.Usuario'
    end
    object QrPesq3Juridico: TSmallintField
      FieldName = 'Juridico'
      Origin = '_inadimp_.Juridico'
    end
    object QrPesq3Data: TDateField
      FieldName = 'Data'
      Origin = '_inadimp_.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq3CliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = '_inadimp_.CliInt'
    end
    object QrPesq3CREDITO: TFloatField
      FieldName = 'CREDITO'
      Origin = '_inadimp_.CREDITO'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3PAGO: TFloatField
      FieldName = 'PAGO'
      Origin = '_inadimp_.PAGO'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3Juros: TFloatField
      FieldName = 'Juros'
      Origin = '_inadimp_.Juros'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3Multa: TFloatField
      FieldName = 'Multa'
      Origin = '_inadimp_.Multa'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3TOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = '_inadimp_.TOTAL'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3SALDO: TFloatField
      FieldName = 'SALDO'
      Origin = '_inadimp_.SALDO'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3PEND_VAL: TFloatField
      FieldName = 'PEND_VAL'
      Origin = '_inadimp_.PEND_VAL'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesq3Descricao: TWideStringField
      FieldName = 'Descricao'
      Origin = '_inadimp_.Descricao'
      Size = 100
    end
    object QrPesq3MEZ_TXT: TWideStringField
      FieldName = 'MEZ_TXT'
      Origin = '_inadimp_.MEZ_TXT'
      Size = 7
    end
    object QrPesq3Compensado: TDateField
      FieldName = 'Compensado'
      Origin = '_inadimp_.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq3Controle: TIntegerField
      FieldName = 'Controle'
      Origin = '_inadimp_.Controle'
    end
    object QrPesq3VCTO_TXT: TWideStringField
      FieldName = 'VCTO_TXT'
      Origin = '_inadimp_.VCTO_TXT'
      Size = 10
    end
    object QrPesq3NOMEEMPCOND: TWideStringField
      FieldName = 'NOMEEMPCOND'
      Origin = '_inadimp_.NOMEEMPCOND'
      Size = 100
    end
    object QrPesq3NOMEPRPIMOV: TWideStringField
      FieldName = 'NOMEPRPIMOV'
      Origin = '_inadimp_.NOMEPRPIMOV'
      Size = 100
    end
    object QrPesq3NewVencto: TDateField
      FieldName = 'NewVencto'
      Origin = '_inadimp_.NewVencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq3Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = '_inadimp_.Ativo'
    end
    object QrPesq3Juridico_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Juridico_TXT'
      Size = 2
      Calculated = True
    end
    object QrPesq3Juridico_DESCRI: TWideStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'Juridico_DESCRI'
      Size = 50
      Calculated = True
    end
  end
  object DsPesq3: TDataSource
    DataSet = QrPesq3
    Left = 156
    Top = 4
  end
  object QrCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Cliente'
      'FROM Cond')
    Left = 64
    Top = 52
    object QrCondCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondCliente: TIntegerField
      FieldName = 'Cliente'
    end
  end
  object QrLista: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT cnd.Cliente, dci.Controle DiarCEMIts, dci.SMS, '
      'dci.Email, dci.Carta, dci.Dias, dci.Dias + dci.DdNoEnv Fim,'
      
        'DATE_SUB(SYSDATE(), INTERVAL dci.Dias + dci.DdNoEnv DAY)  DATA_I' +
        ','
      'DATE_SUB(SYSDATE(), INTERVAL dci.Dias DAY) DATA_F,'
      'imv.InadCEMCad, ina.* '
      'FROM _inadimp_ ina'
      'LEFT JOIN syndic.condimov imv ON imv.Conta=ina.Apto'
      'LEFT JOIN syndic.cond cnd ON cnd.Codigo=imv.Codigo'
      'LEFT JOIN syndic.diarcemits dci ON dci.Codigo=imv.InadCEMCad'
      'WHERE imv.InadCEMCad > 0'
      'AND dci.Dias > 0'
      'AND ina.Vencimento BETWEEN '
      '  DATE_SUB(SYSDATE(), INTERVAL (dci.Dias + dci.DdNoEnv) DAY) '
      '  AND DATE_SUB(SYSDATE(), INTERVAL dci.Dias DAY)  '
      'ORDER BY Vencimento;'
      ''
      '')
    Left = 64
    Top = 100
    object QrListaDiarCEMIts: TIntegerField
      FieldName = 'DiarCEMIts'
    end
    object QrListaSMS: TIntegerField
      FieldName = 'SMS'
    end
    object QrListaEmail: TIntegerField
      FieldName = 'Email'
    end
    object QrListaCarta: TIntegerField
      FieldName = 'Carta'
    end
    object QrListaInadCEMCad: TIntegerField
      FieldName = 'InadCEMCad'
    end
    object QrListaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrListaForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrListaApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrListaMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrListaVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrListaFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrListaPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrListaUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrListaImobiliaria: TIntegerField
      FieldName = 'Imobiliaria'
    end
    object QrListaProcurador: TIntegerField
      FieldName = 'Procurador'
    end
    object QrListaUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrListaJuridico: TSmallintField
      FieldName = 'Juridico'
    end
    object QrListaData: TDateField
      FieldName = 'Data'
    end
    object QrListaCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrListaCREDITO: TFloatField
      FieldName = 'CREDITO'
    end
    object QrListaPAGO: TFloatField
      FieldName = 'PAGO'
    end
    object QrListaJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrListaMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrListaTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object QrListaSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrListaPEND_VAL: TFloatField
      FieldName = 'PEND_VAL'
    end
    object QrListaDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrListaMEZ_TXT: TWideStringField
      FieldName = 'MEZ_TXT'
      Size = 7
    end
    object QrListaCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrListaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrListaVCTO_TXT: TWideStringField
      FieldName = 'VCTO_TXT'
      Size = 10
    end
    object QrListaNOMEEMPCOND: TWideStringField
      FieldName = 'NOMEEMPCOND'
      Size = 100
    end
    object QrListaNOMEPRPIMOV: TWideStringField
      FieldName = 'NOMEPRPIMOV'
      Size = 100
    end
    object QrListaNewVencto: TDateField
      FieldName = 'NewVencto'
    end
    object QrListaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrListaDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrListaFim: TLargeintField
      FieldName = 'Fim'
    end
    object QrListaCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrListaDATA_I: TWideStringField
      FieldName = 'DATA_I'
      Size = 10
    end
    object QrListaDATA_F: TWideStringField
      FieldName = 'DATA_F'
      Size = 10
    end
  end
  object QrLocBol: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM diarcembol'
      'WHERE Vencimento=:P0'
      'AND Data=:P0'
      'AND Mez=:p0'
      'AND Depto=:P0'
      'AND FatNum=:P0')
    Left = 156
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocBolCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrProtTip: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT imv.Conta, imv.Protocolo, ptc.Tipo'
      'FROM condimov imv'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=imv.Protocolo'
      'WHERE imv.Conta=:P0')
    Left = 64
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProtTipConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrProtTipProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrProtTipTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrEnvEmail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT bol.Vencimento, bol.Data, bol.Mez, bol.FatNum,  '
      'bol.Depto, bol.Propriet, bol.Usuario, bol.Empresa, '
      'bol.EmprEnti, cim.Unidade NO_DEPTO,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'env.*  '
      'FROM diarcemenv env '
      'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=bol.EmprEnti'
      'LEFT JOIN condimov cim ON cim.Conta=bol.Depto'
      'WHERE env.Metodo=2'
      'AND bol.Juridica=0'
      'AND env.ConsidEnvi < "1900-01-01" '
      'AND env.AutoCancel < "1900-01-01" '
      'AND env.DtaLimite >= SYSDATE() '
      '')
    Left = 156
    Top = 148
    object QrEnvEmailVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrEnvEmailData: TDateField
      FieldName = 'Data'
    end
    object QrEnvEmailMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEnvEmailFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEnvEmailDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEnvEmailPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrEnvEmailUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrEnvEmailEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEnvEmailEmprEnti: TIntegerField
      FieldName = 'EmprEnti'
    end
    object QrEnvEmailCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnvEmailDiarCEMIts: TIntegerField
      FieldName = 'DiarCEMIts'
    end
    object QrEnvEmailMetodo: TSmallintField
      FieldName = 'Metodo'
    end
    object QrEnvEmailCadastro: TIntegerField
      FieldName = 'Cadastro'
    end
    object QrEnvEmailReInclu: TIntegerField
      FieldName = 'ReInclu'
    end
    object QrEnvEmailDtaInicio: TDateField
      FieldName = 'DtaInicio'
    end
    object QrEnvEmailDtaLimite: TDateField
      FieldName = 'DtaLimite'
    end
    object QrEnvEmailTentativas: TIntegerField
      FieldName = 'Tentativas'
    end
    object QrEnvEmailLastExec: TDateTimeField
      FieldName = 'LastExec'
    end
    object QrEnvEmailConsidEnvi: TDateTimeField
      FieldName = 'ConsidEnvi'
    end
    object QrEnvEmailLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEnvEmailDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEnvEmailDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEnvEmailUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEnvEmailUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEnvEmailAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEnvEmailAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEnvEmailLastLotImp: TLargeintField
      FieldName = 'LastLotImp'
    end
    object QrEnvEmailAutoCancel: TDateTimeField
      FieldName = 'AutoCancel'
    end
    object QrEnvEmailLastVerify: TDateTimeField
      FieldName = 'LastVerify'
    end
    object QrEnvEmailNaoEnvTip: TIntegerField
      FieldName = 'NaoEnvTip'
    end
    object QrEnvEmailNaoEnvMot: TIntegerField
      FieldName = 'NaoEnvMot'
    end
    object QrEnvEmailNaoEnvLst: TSmallintField
      FieldName = 'NaoEnvLst'
    end
    object QrEnvEmailNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrEnvEmailNO_DEPTO: TWideStringField
      FieldName = 'NO_DEPTO'
      Size = 10
    end
  end
  object QrEmails: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Pronome, Nome, Emeio '
      'FROM condemeios'
      'WHERE Conta>0')
    Left = 64
    Top = 196
    object QrEmailsPronome: TWideStringField
      FieldName = 'Pronome'
    end
    object QrEmailsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEmailsEmeio: TWideStringField
      FieldName = 'Emeio'
      Size = 100
    end
  end
  object QrCOMs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT sms.Porta, sms.PIN'
      'FROM diarcemenv env '
      'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo'
      'LEFT JOIN txtsms sms ON sms.Codigo=env.Cadastro'
      'WHERE env.Metodo=1'
      'AND bol.Juridica=0'
      'AND env.ConsidEnvi < "1900-01-01" '
      'AND env.AutoCancel < "1900-01-01" '
      'AND env.DtaLimite >= SYSDATE() '
      'AND (((sms.AMIni < sms.AMFim) AND '
      '  ("11:00:00" BETWEEN sms.AMIni AND sms.AMFim))'
      '  OR ('
      '  (sms.PMIni < sms.PMFim) AND ('
      '  "11:00:00" BETWEEN sms.PMIni AND sms.PMFim))'
      ')')
    Left = 156
    Top = 196
    object QrCOMsPorta: TWideStringField
      FieldName = 'Porta'
      Size = 255
    end
    object QrCOMsPIN: TWideStringField
      FieldName = 'PIN'
    end
  end
  object QrEnvSMS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT bol.Vencimento, bol.Data, bol.Mez, bol.FatNum,  '
      
        'bol.Depto, bol.Propriet, bol.Usuario, bol.Empresa,  bol.EmprEnti' +
        ', '
      'sms.Nome TEXTO, sms.PIN, sms.Porta,  sms.DiasSem, '
      'sms.AMIni, sms.AMFim, sms.PMIni, sms.PMFim, env.*  '
      'FROM diarcemenv env '
      'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo'
      'LEFT JOIN txtsms sms ON sms.Codigo=env.Cadastro'
      'WHERE env.Metodo=1'
      'AND bol.Juridica=0'
      'AND env.ConsidEnvi < "1900-01-01" '
      'AND env.AutoCancel < "1900-01-01" '
      'AND env.DtaLimite >= SYSDATE() ')
    Left = 64
    Top = 244
    object QrEnvSMSVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrEnvSMSData: TDateField
      FieldName = 'Data'
    end
    object QrEnvSMSMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEnvSMSFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEnvSMSDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEnvSMSPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrEnvSMSUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrEnvSMSEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEnvSMSEmprEnti: TIntegerField
      FieldName = 'EmprEnti'
    end
    object QrEnvSMSTEXTO: TWideStringField
      FieldName = 'TEXTO'
      Size = 140
    end
    object QrEnvSMSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnvSMSDiarCEMIts: TIntegerField
      FieldName = 'DiarCEMIts'
    end
    object QrEnvSMSMetodo: TSmallintField
      FieldName = 'Metodo'
    end
    object QrEnvSMSCadastro: TIntegerField
      FieldName = 'Cadastro'
    end
    object QrEnvSMSReInclu: TIntegerField
      FieldName = 'ReInclu'
    end
    object QrEnvSMSDtaInicio: TDateField
      FieldName = 'DtaInicio'
    end
    object QrEnvSMSDtaLimite: TDateField
      FieldName = 'DtaLimite'
    end
    object QrEnvSMSTentativas: TIntegerField
      FieldName = 'Tentativas'
    end
    object QrEnvSMSLastExec: TDateTimeField
      FieldName = 'LastExec'
    end
    object QrEnvSMSConsidEnvi: TDateTimeField
      FieldName = 'ConsidEnvi'
    end
    object QrEnvSMSLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEnvSMSDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEnvSMSDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEnvSMSUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEnvSMSUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEnvSMSAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEnvSMSAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEnvSMSLastLotImp: TLargeintField
      FieldName = 'LastLotImp'
    end
    object QrEnvSMSAutoCancel: TDateTimeField
      FieldName = 'AutoCancel'
    end
    object QrEnvSMSLastVerify: TDateTimeField
      FieldName = 'LastVerify'
    end
    object QrEnvSMSNaoEnvTip: TIntegerField
      FieldName = 'NaoEnvTip'
    end
    object QrEnvSMSNaoEnvMot: TIntegerField
      FieldName = 'NaoEnvMot'
    end
    object QrEnvSMSNaoEnvLst: TSmallintField
      FieldName = 'NaoEnvLst'
    end
    object QrEnvSMSPIN: TWideStringField
      FieldName = 'PIN'
    end
    object QrEnvSMSPorta: TWideStringField
      FieldName = 'Porta'
      Size = 255
    end
    object QrEnvSMSDiasSem: TIntegerField
      FieldName = 'DiasSem'
    end
    object QrEnvSMSAMIni: TTimeField
      FieldName = 'AMIni'
    end
    object QrEnvSMSAMFim: TTimeField
      FieldName = 'AMFim'
    end
    object QrEnvSMSPMIni: TTimeField
      FieldName = 'PMIni'
    end
    object QrEnvSMSPMFim: TTimeField
      FieldName = 'PMFim'
    end
  end
  object QrEnti: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT imv.SMSCelTipo, imv.SMSCelNumr, imv.SMSCelNome,'
      'imv.Usuario, imv.Propriet,'
      'IF(usu.Tipo=0, usu.ECel, usu.PCel) CELULAR_USU, '
      'IF(prp.Tipo=0, prp.ECel, prp.PCel) CELULAR_PRP, '
      'IF(ter.Tipo=0, ter.ECel, ter.PCel) CELULAR_TER '
      'FROM condimov imv'
      'LEFT JOIN entidades usu ON usu.Codigo=imv.Usuario'
      'LEFT JOIN entidades prp ON prp.Codigo=imv.Propriet'
      'LEFT JOIN entidades ter ON ter.Codigo=imv.SMSCelEnti'
      'WHERE imv.Conta=808'
      '')
    Left = 156
    Top = 244
    object QrEntiUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrEntiPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrEntiCELULAR_USU: TWideStringField
      FieldName = 'CELULAR_USU'
    end
    object QrEntiCELULAR_PRP: TWideStringField
      FieldName = 'CELULAR_PRP'
    end
    object QrEntiCELULAR_TER: TWideStringField
      FieldName = 'CELULAR_TER'
    end
    object QrEntiSMSCelNome: TWideStringField
      FieldName = 'SMSCelNome'
      Size = 60
    end
    object QrEntiSMSCelNumr: TWideStringField
      FieldName = 'SMSCelNumr'
      Size = 18
    end
    object QrEntiSMSCelTipo: TSmallintField
      FieldName = 'SMSCelTipo'
    end
  end
  object QrLocEnv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM diarcemenv'
      'WHERE Codigo<> 0'
      'AND DiarCEMIts <> 0'
      'AND Metodo<>0'
      '')
    Left = 64
    Top = 292
    object QrLocEnvControle: TIntegerField
      FieldName = 'Controle'
    end
  end
end
