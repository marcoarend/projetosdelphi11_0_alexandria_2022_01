unit ModCobranca;

interface

uses
  Windows, SysUtils, Classes, ComCtrls, StdCtrls, dmkGeral, UnInternalConsts, DB,
  ActiveX, AxSms_TLB, // TLB da ActiveExperts > http://www.activexperts.com
  mySQLDbTables, UnProtocol_Tabs, UnDmkEnums;

type
  TDmModCobranca = class(TDataModule)
    QrCtrlGeral: TmySQLQuery;
    QrCtrlGeralUltPsqCobr: TDateField;
    QrPesq3: TmySQLQuery;
    QrPesq3Empresa: TIntegerField;
    QrPesq3ForneceI: TIntegerField;
    QrPesq3Apto: TIntegerField;
    QrPesq3Mez: TIntegerField;
    QrPesq3Vencimento: TDateField;
    QrPesq3FatNum: TFloatField;
    QrPesq3Propriet: TIntegerField;
    QrPesq3Unidade: TWideStringField;
    QrPesq3Imobiliaria: TIntegerField;
    QrPesq3Procurador: TIntegerField;
    QrPesq3Usuario: TIntegerField;
    QrPesq3Juridico: TSmallintField;
    QrPesq3Data: TDateField;
    QrPesq3CliInt: TIntegerField;
    QrPesq3CREDITO: TFloatField;
    QrPesq3PAGO: TFloatField;
    QrPesq3Juros: TFloatField;
    QrPesq3Multa: TFloatField;
    QrPesq3TOTAL: TFloatField;
    QrPesq3SALDO: TFloatField;
    QrPesq3PEND_VAL: TFloatField;
    QrPesq3Descricao: TWideStringField;
    QrPesq3MEZ_TXT: TWideStringField;
    QrPesq3Compensado: TDateField;
    QrPesq3Controle: TIntegerField;
    QrPesq3VCTO_TXT: TWideStringField;
    QrPesq3NOMEEMPCOND: TWideStringField;
    QrPesq3NOMEPRPIMOV: TWideStringField;
    QrPesq3NewVencto: TDateField;
    QrPesq3Ativo: TSmallintField;
    QrPesq3Juridico_TXT: TWideStringField;
    QrPesq3Juridico_DESCRI: TWideStringField;
    DsPesq3: TDataSource;
    QrCond: TmySQLQuery;
    QrCondCodigo: TIntegerField;
    QrCondCliente: TIntegerField;
    QrLista: TmySQLQuery;
    QrListaDiarCEMIts: TIntegerField;
    QrListaSMS: TIntegerField;
    QrListaEmail: TIntegerField;
    QrListaCarta: TIntegerField;
    QrListaInadCEMCad: TIntegerField;
    QrListaEmpresa: TIntegerField;
    QrListaForneceI: TIntegerField;
    QrListaApto: TIntegerField;
    QrListaMez: TIntegerField;
    QrListaVencimento: TDateField;
    QrListaFatNum: TFloatField;
    QrListaPropriet: TIntegerField;
    QrListaUnidade: TWideStringField;
    QrListaImobiliaria: TIntegerField;
    QrListaProcurador: TIntegerField;
    QrListaUsuario: TIntegerField;
    QrListaJuridico: TSmallintField;
    QrListaData: TDateField;
    QrListaCliInt: TIntegerField;
    QrListaCREDITO: TFloatField;
    QrListaPAGO: TFloatField;
    QrListaJuros: TFloatField;
    QrListaMulta: TFloatField;
    QrListaTOTAL: TFloatField;
    QrListaSALDO: TFloatField;
    QrListaPEND_VAL: TFloatField;
    QrListaDescricao: TWideStringField;
    QrListaMEZ_TXT: TWideStringField;
    QrListaCompensado: TDateField;
    QrListaControle: TIntegerField;
    QrListaVCTO_TXT: TWideStringField;
    QrListaNOMEEMPCOND: TWideStringField;
    QrListaNOMEPRPIMOV: TWideStringField;
    QrListaNewVencto: TDateField;
    QrListaAtivo: TSmallintField;
    QrListaDias: TIntegerField;
    QrListaFim: TLargeintField;
    QrListaCliente: TIntegerField;
    QrLocBol: TmySQLQuery;
    QrLocBolCodigo: TIntegerField;
    QrProtTip: TmySQLQuery;
    QrProtTipConta: TIntegerField;
    QrProtTipProtocolo: TIntegerField;
    QrProtTipTipo: TIntegerField;
    QrEnvEmail: TmySQLQuery;
    QrEnvEmailVencimento: TDateField;
    QrEnvEmailData: TDateField;
    QrEnvEmailMez: TIntegerField;
    QrEnvEmailFatNum: TFloatField;
    QrEnvEmailDepto: TIntegerField;
    QrEnvEmailPropriet: TIntegerField;
    QrEnvEmailUsuario: TIntegerField;
    QrEnvEmailEmpresa: TIntegerField;
    QrEnvEmailEmprEnti: TIntegerField;
    QrEnvEmailCodigo: TIntegerField;
    QrEnvEmailDiarCEMIts: TIntegerField;
    QrEnvEmailMetodo: TSmallintField;
    QrEnvEmailCadastro: TIntegerField;
    QrEnvEmailReInclu: TIntegerField;
    QrEnvEmailDtaInicio: TDateField;
    QrEnvEmailDtaLimite: TDateField;
    QrEnvEmailTentativas: TIntegerField;
    QrEnvEmailLastExec: TDateTimeField;
    QrEnvEmailConsidEnvi: TDateTimeField;
    QrEnvEmailLk: TIntegerField;
    QrEnvEmailDataCad: TDateField;
    QrEnvEmailDataAlt: TDateField;
    QrEnvEmailUserCad: TIntegerField;
    QrEnvEmailUserAlt: TIntegerField;
    QrEnvEmailAlterWeb: TSmallintField;
    QrEnvEmailAtivo: TSmallintField;
    QrEnvEmailLastLotImp: TLargeintField;
    QrEnvEmailAutoCancel: TDateTimeField;
    QrEnvEmailLastVerify: TDateTimeField;
    QrEnvEmailNaoEnvTip: TIntegerField;
    QrEnvEmailNaoEnvMot: TIntegerField;
    QrEnvEmailNaoEnvLst: TSmallintField;
    QrEnvEmailNO_EMPRESA: TWideStringField;
    QrEnvEmailNO_DEPTO: TWideStringField;
    QrEmails: TmySQLQuery;
    QrEmailsPronome: TWideStringField;
    QrEmailsNome: TWideStringField;
    QrEmailsEmeio: TWideStringField;
    QrCOMs: TmySQLQuery;
    QrCOMsPorta: TWideStringField;
    QrCOMsPIN: TWideStringField;
    QrEnvSMS: TmySQLQuery;
    QrEnvSMSVencimento: TDateField;
    QrEnvSMSData: TDateField;
    QrEnvSMSMez: TIntegerField;
    QrEnvSMSFatNum: TFloatField;
    QrEnvSMSDepto: TIntegerField;
    QrEnvSMSPropriet: TIntegerField;
    QrEnvSMSUsuario: TIntegerField;
    QrEnvSMSEmpresa: TIntegerField;
    QrEnvSMSEmprEnti: TIntegerField;
    QrEnvSMSTEXTO: TWideStringField;
    QrEnvSMSCodigo: TIntegerField;
    QrEnvSMSDiarCEMIts: TIntegerField;
    QrEnvSMSMetodo: TSmallintField;
    QrEnvSMSCadastro: TIntegerField;
    QrEnvSMSReInclu: TIntegerField;
    QrEnvSMSDtaInicio: TDateField;
    QrEnvSMSDtaLimite: TDateField;
    QrEnvSMSTentativas: TIntegerField;
    QrEnvSMSLastExec: TDateTimeField;
    QrEnvSMSConsidEnvi: TDateTimeField;
    QrEnvSMSLk: TIntegerField;
    QrEnvSMSDataCad: TDateField;
    QrEnvSMSDataAlt: TDateField;
    QrEnvSMSUserCad: TIntegerField;
    QrEnvSMSUserAlt: TIntegerField;
    QrEnvSMSAlterWeb: TSmallintField;
    QrEnvSMSAtivo: TSmallintField;
    QrEnvSMSLastLotImp: TLargeintField;
    QrEnvSMSAutoCancel: TDateTimeField;
    QrEnvSMSLastVerify: TDateTimeField;
    QrEnvSMSNaoEnvTip: TIntegerField;
    QrEnvSMSNaoEnvMot: TIntegerField;
    QrEnvSMSNaoEnvLst: TSmallintField;
    QrEnvSMSPIN: TWideStringField;
    QrEnvSMSPorta: TWideStringField;
    QrEnvSMSDiasSem: TIntegerField;
    QrEnvSMSAMIni: TTimeField;
    QrEnvSMSAMFim: TTimeField;
    QrEnvSMSPMIni: TTimeField;
    QrEnvSMSPMFim: TTimeField;
    QrEnti: TmySQLQuery;
    QrEntiUsuario: TIntegerField;
    QrEntiPropriet: TIntegerField;
    QrEntiCELULAR_USU: TWideStringField;
    QrEntiCELULAR_PRP: TWideStringField;
    QrEntiCELULAR_TER: TWideStringField;
    QrEntiSMSCelNome: TWideStringField;
    QrEntiSMSCelNumr: TWideStringField;
    QrEntiSMSCelTipo: TSmallintField;
    QrLocEnv: TmySQLQuery;
    QrLocEnvControle: TIntegerField;
    QrListaDATA_I: TWideStringField;
    QrListaDATA_F: TWideStringField;
    procedure QrPesq3CalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FInadimp: String;
    //
    procedure ComplementaQuery3_InadimpDeCond(SQL: TStringList;
              TabLctA: String;
              CondCod, CodEnt, Propriet, CondImov: Integer;
              RGDocumentos, PCAgrupamentos: Integer;
              QrCondGriImv: TmySQLQuery;
              EdMezIni, EdMezFim: String;
              CkNaoMensais: Boolean;
              TPIniDtaDate, TPFimDtaDate: TDateTime; CkIniDtaChecked, CkFimDtaChecked, GBEmissaoVisible: Boolean;
              TPIniVctDate, TPFimVctDate: TDateTime; CkIniVctChecked, CkFimVctChecked, GBVenctoVisible: Boolean;
              FTemDtP: Boolean; FDtaPgt: String);
    function  SubstituiVariaveis(ModoEnvio: Integer; Texto: WideString): String;
    procedure PrintResult(strFunction: string; objGsm: Gsm; Memo: TMemo);

  public
    { Public declarations }
    procedure AtualizaDiarCEMEnv_NaoEnv(Codigo, DiarCEMIts, Metodo, NaoEnvTip,
              NaoEnvLst, NaoEnvMot: Integer; NaoEnvVal, LastExec: String);
    procedure SalvaSMSRecebidoNoBD(FromAddr, SMS: WideString);
    //
    procedure VerificaInadimplencia(WndCalled: TWndCalled;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
              CkForcaPsqChecked: Boolean; Empr, Enti: Integer);
    procedure PesquisaCondominio(CondCod, CodEnt: Integer);
    function  Pesquisa3_InadimpDeCond(TabLctA: String;
              CondCod, CodEnt, Propriet, CondImov,
              RGDocumentos, PCAgrupamentos: Integer;
              QrCondGriImv: TmySQLQuery;
              EdMezIni, EdMezFim: String;
              CkNaoMensais: Boolean;
              TPIniDtaDate, TPFimDtaDate: TDateTime; CkIniDtaChecked, CkFimDtaChecked, GBEmissaoVisible: Boolean;
              TPIniVctDate, TPFimVctDate: TDateTime; CkIniVctChecked, CkFimVctChecked, GBVenctoVisible: Boolean;
              CkFimPgtChecked: Boolean;
              TPFimPgtDate: TDateTime;
              RGSomas, RGSituacao, RGOrdem1, RGOrdem2, RGOrdem3, RGOrdem4: Integer): String;
    function  Pesquisa4_InadimpDeCond(TabLctA: String;
              CondCod, CodEnt, Propriet, CondImov,
              RGDocumentos, PCAgrupamentos: Integer;
              QrCondGriImv: TmySQLQuery;
              EdMezIni, EdMezFim: String;
              CkNaoMensais: Boolean;
              TPIniDtaDate, TPFimDtaDate: TDateTime; CkIniDtaChecked, CkFimDtaChecked, GBEmissaoVisible: Boolean;
              TPIniVctDate, TPFimVctDate: TDateTime; CkIniVctChecked, CkFimVctChecked, GBVenctoVisible: Boolean;
              CkFimPgtChecked: Boolean;
              TPFimPgtDate: TDateTime;
              RGSomas, RGSituacao, RGOrdem1, RGOrdem2, RGOrdem3, RGOrdem4: Integer): String;
    procedure AgendarEnvioDeMensagensAInadimplentes(WndCalled: TWndCalled;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel);
    procedure EnviaAgendados_Email(WndCalled: TWndCalled;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel);
    procedure EnviaAgendados_SMS(WndCalled: TWndCalled;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel; Memo: TMemo);
    function  AparelhoGSM_Conecta(Memo: TMemo; Porta, PIN: String): Boolean;
    procedure AparelhoGSM_Desconecta();
    function  AparelhoGSM_ComandoAT(Comando: String; Memo: TMemo): String;
    function  AparelhoGSM_RecebeSMSs(Memo: TMemo): Boolean;
    function  AparelhoGSM_RecebeRelatorios(Memo: TMemo): Boolean;
    function  AparelhoGSM_EnviaSMS(NumDest, Mensagem: String;
              Memo: TMemo): Integer;

  end;

var
  DmModCobranca: TDmModCobranca;

implementation

uses UnDiario_Tabs, Module, DmkDAC_PF, UnDmkProcFunc, UMySQLModule, CreateApp,
  ModuleGeral, UnMyObjects, UnMailEnv;

{$R *.dfm}

var
  objGsm: Gsm;
  objSms: Message;
  objGsmDeliveryReport: IGsmDeliveryReport;
  objConstants: Constants;

{ TDmModCobranca }

const
  FLog = 'C:\Dermatek\Cobranca\Log.log';
  //
  Ordens: array[0..5] of String = ('Vencimento', 'NOMEEMPCOND', 'NOMEPRPIMOV', 'Unidade', 'Mez', 'Data');
  //_Labels_: array[0..5] of String = ('Vencimento', 'NOMEEMPCOND', 'NOMEPRPIMOV', 'Unidade', 'MEZ_TXT', 'Data');

procedure TDmModCobranca.AgendarEnvioDeMensagensAInadimplentes(WndCalled: TWndCalled;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel);
  procedure TentaIncluirTarefa(Metodo, Cadastro: Integer);
  var
    Vencimento, Data, DtaInicio, DtaLimite(*, Envio*), AutoCancel: String;
    Codigo, Depto, Mez, DiarCEMIts, ReInclu, Propriet, Usuario, Empresa,
    EmprEnti, Juridica, Ativo, Controle: Integer;
    FatNum: Double;
  begin
    Depto          := QrListaApto.Value;
    Mez            := QrListaMez.Value;
    Vencimento     := Geral.FDT(QrListaVencimento.Value, 1);
    Data           := Geral.FDT(QrListaData.Value, 1);
    FatNum         := QrListaFatNum.Value;
    Propriet       := QrListaPropriet.Value;
    Usuario        := QrListaUsuario .Value;
    Empresa        := QrListaEmpresa .Value;
    EmprEnti       := QrListaCliente.Value;
    AutoCancel     := '0000-00-00 00:00:00';
    Ativo          := 1;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocBol, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM diarcembol ',
    'WHERE Vencimento="' + Vencimento + '" ',
    'AND Data="' + Data + '" ',
    'AND Mez=' + Geral.FF0(Mez),
    'AND Depto=' + Geral.FF0(Depto),
    'AND FatNum=' + Geral.FFI(FatNum),
    '']);
    Codigo :=  QrLocBolCodigo.Value;
    if Codigo = 0 then
    begin
      Juridica := 0;
      Codigo := UMyMod.BPGS1I32('diarcembol', 'Codigo', '', '', tsPos, stIns, 0);

      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'diarcembol', False, [
      'Vencimento', 'Data', 'Mez',
      'Depto', 'FatNum', 'Propriet',
      'Usuario', 'Empresa', 'EmprEnti',
      'Juridica'(*, 'UltAtzVal'*)], [
      'Codigo'], [
      Vencimento, Data, Mez,
      Depto, FatNum, Propriet,
      Usuario, Empresa, EmprEnti,
      Juridica(*, UltAtzVal*)], [
      Codigo], True);
    end;
    //
    DiarCEMIts     := QrListaDiarCEMIts.Value;
    ReInclu        := 1;
    //Metodo         := QrListaMetodo.Value;
    //Cadastro       := QrListaCadast.Value;
    //Envio          := QrListaEnvio .Value;
    DtaInicio      := Geral.FDT(QrListaVencimento.Value + QrListaDias.Value, 1);
    DtaLimite      := Geral.FDT(QrListaVencimento.Value + QrListaFim.Value, 1);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocEnv, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM diarcemenv ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND DiarCEMIts=' + Geral.FF0(DiarCEMIts),
    'AND Metodo=' + Geral.FF0(Metodo),
    '']);
    if QrLocEnvControle.Value <> 0 then
      Controle := QrLocEnvControle.Value
    else
      Controle := UMyMod.BPGS1I32('diarcemenv', 'Controle', '', '', tsPos, stIns, 0);
    //
    UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'diarcemenv', False, [
    'Controle',
    'ReInclu', 'Cadastro', 'DtaInicio',
    'DtaLimite' (*, 'Envio'*), 'AutoCancel',
    'Ativo'], [
    'Codigo', 'DiarCEMIts', 'Metodo'], [
    'AutoCancel', 'Ativo'], [
    Controle,
    ReInclu, Cadastro, DtaInicio,
    DtaLimite (*, Envio*), AutoCancel,
    Ativo], [
    Codigo, DiarCEMIts, Metodo], [
    AutoCancel, Ativo], True, 'ReInclu', 1);
    //
  end;
var
  SMS, Email, Carta: Integer;
  CadastraCarta: Boolean;
  AutoCancel: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Otimizando pesquisa');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLista, DModG.MyPID_DB, [
  'SELECT cnd.Cliente, dci.Controle DiarCEMIts, dci.SMS,  ',
  'dci.Email, dci.Carta, dci.Dias, dci.Dias + dci.DdNoEnv Fim, ',
  'DATE_SUB(SYSDATE(), INTERVAL dci.Dias + dci.DdNoEnv DAY)  DATA_I, ',
  'DATE_SUB(SYSDATE(), INTERVAL dci.Dias DAY) DATA_F, ',
  'imv.InadCEMCad, ina.*  ',
  'FROM _inadimp_ ina ',
  'LEFT JOIN syndic.condimov imv ON imv.Conta=ina.Apto ',
  'LEFT JOIN syndic.cond cnd ON cnd.Codigo=imv.Codigo ',
  'LEFT JOIN syndic.diarcemits dci ON dci.Codigo=imv.InadCEMCad ',
  'WHERE imv.InadCEMCad > 0 ',
  'AND dci.Dias > 0 ',
  'AND ina.Vencimento BETWEEN  ',
  '  DATE_SUB(SYSDATE(), INTERVAL (dci.Dias + dci.DdNoEnv) DAY)  ',
  '  AND DATE_SUB(SYSDATE(), INTERVAL dci.Dias DAY)   ',
  'ORDER BY Vencimento; ',
  '']);
  if QrLista.RecordCount > 0 then // > evitar AutoCancel enganado!
  begin
    PB1.Position := 0;
    PB1.Max := QrLista.RecordCount;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando cancelamento de pagos e n�o enviados');
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE diarcemenv ',
    'SET Ativo=0 ',
    'WHERE ConsidEnvi < "1900-01-01" ',
    'AND AutoCancel < "1900-01-01" ',
    'AND DtaLimite > SYSDATE() ',
    '']);
    //
    QrLista.First;
    while not QrLista.Eof do
    begin
      CadastraCarta := False;
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //
      // SMS
      SMS := QrListaSMS.Value;
      if SMS <> 0 then
        TentaIncluirTarefa(CO_DIARIO_ENVIO_MENSAGEM_SMS, SMS);
      // Email
      Email := QrListaEmail.Value;
      if Email <> 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrProtTip, Dmod.MyDB, [
        'SELECT imv.Conta, imv.Protocolo, ptc.Tipo ',
        'FROM condimov imv ',
        'LEFT JOIN protocolos ptc ON ptc.Codigo=imv.Protocolo ',
        'WHERE imv.Conta=' + Geral.FF0(QrListaApto.Value),
        '']);
        if (QrProtTipProtocolo.Value <> 0) and (QrProtTipTipo.Value =
        CO_TAREFA_ENVIO_PROTOCOLOS_CE_CIRCUL_ELETRON_EMAIL) then
          TentaIncluirTarefa(CO_DIARIO_ENVIO_MENSAGEM_EMAIL, Email)
        else
          CadastraCarta := True;
      end else
        CadastraCarta := True;
      //
      if CadastraCarta then
      begin
        // Carta
        Carta := QrListaCarta.Value;
        if Carta <> 0 then
          TentaIncluirTarefa(CO_DIARIO_ENVIO_MENSAGEM_CARTA, Carta);
      end;
      //
      QrLista.Next;
    end;
    AutoCancel := Geral.FDT(DModG.ObtemAgora(), 109);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Cancelamento envio de pagos e n�o enviados');
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE diarcemenv ',
    'SET AutoCancel="' + AutoCancel + '"',
    'WHERE ConsidEnvi < "1900-01-01" ',
    'AND AutoCancel < "1900-01-01" ',
    'AND Ativo=0 ',
    'AND DtaLimite > SYSDATE() ',
    '']);
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

function TDmModCobranca.AparelhoGSM_ComandoAT(Comando: String; Memo: TMemo): String;
var
  sResp: String;
  Conta: Integer;
begin
  objGsm.SendCommand(Comando);
  sResp := objGsm.ReadResponse(1000);
  Conta := 0;
  while (sResp <> 'OK') and (Conta < 5) do
  begin
    Memo.Lines.Add('Resposta: ' + sResp);
    sResp := objGsm.ReadResponse(1000);
    Conta := Conta + 1;
  end;
  objGsm.SendCommand('');//  ' Send USSD Command
  sResp := objGsm.ReadResponse(1000);
  Conta := 0;
  while (sResp = '') and (Conta < 60) do
  begin
    sResp := objGsm.ReadResponse(1000);
    Conta := Conta + 1;
    Memo.Lines.Add('Conta: ' + Geral.FF0(Conta) + '...');
  end;
  Memo.Lines.Add('Resposta: ' + sResp);
  Result := sResp;
end;

function TDmModCobranca.AparelhoGSM_Conecta(Memo: TMemo; Porta, PIN: String): Boolean;
var
  strLogPath: string;
  strDeviceName: string;
  strPincode: string;
  //strToAddress: string;
  //strMessage: string;
  aTmpFolder: array[0..MAX_PATH] of Char;
begin
  CoInitialize(nil);

  objGsm := CoGsm.Create;
  objConstants := CoConstants.Create;
  objSms := CoMessage.Create;
  objGsmDeliveryReport := CoGsmDeliveryReport.Create;

  // A license key is required to unlock this component after the trial period
  // has expired. Assign the LicenseKey property every time a new instance of
  // this component is created (see code below). Alternatively, the LicenseKey
  // property can be set automatically. This requires the license key to be
  // stored in the registry. For details, see manual, chapter
  // "Product Activation".
{
  objGsm.LicenseKey := '136FB-92150-6FFF1';
}
  objGsm.LicenseKey := '850D7-9112E-E996E';

  if Memo <> nil then
  Memo.Lines.Add('ActiveXperts SMS Component ' + objGsm.Version + sLineBreak +
    'Build: ' + objGsm.Build + sLineBreak +
    'Module: ' + objGsm.Module + sLineBreak +
    'License Status: ' + objGsm.LicenseStatus + sLineBreak +
    'License Key: ' + objGsm.LicenseKey + sLineBreak);

  // Set a logfile, this is optional but helps troubleshooting.
  GetTempPath(MAX_PATH, @aTmpFolder);
  strLogPath := StrPas(aTmpFolder);
  strLogPath := strLogPath + 'Gsm.Log';
  objGsm.LogFile := strLogPath;

  if Memo <> nil then
  Memo.Lines.Add('Log file used: ' + objGsm.LogFile + sLineBreak);

  strDeviceName := Porta;
  // Open the selected device
  objGsm.Open(strDeviceName, '', objConstants.GSM_BAUDRATE_DEFAULT);
  Result := objGsm.LastError = 0;
  PrintResult('Open', objGsm, Memo);
  if objGsm.LastError = 36101 then
  begin
    strPincode := PIN;
    if strPincode = '' then
    begin
      if Memo <> nil then
        Memo.Lines.Add('PIN code n�o informado!');
     objGsm.Close();
    end;
    //Readln(strPincode);
    objGsm.Open(strDeviceName, strPincode, objConstants.GSM_BAUDRATE_DEFAULT);
    Result := objGsm.LastError = 0;
    PrintResult('Open', objGsm, Memo);
  end;
end;

procedure TDmModCobranca.AparelhoGSM_Desconecta();
begin
  // Close the device so other application can use it.
  objGsm.Close();
end;

function TDmModCobranca.AparelhoGSM_EnviaSMS(NumDest, Mensagem: String; Memo: TMemo): Integer;
var
  Telefone: String;
begin
  Result := -1;
  if pos('0', NumDest) = 1 then
    Telefone := Copy(NumDest, 2)
  else
    Telefone := NumDest;
  //
  Telefone := '55' + Telefone;
  //
  //if Geral.MB_Pergunta('Envia SMS para ' + Telefone + ' ?') = ID_YES then
  begin
    if Memo <> nil then
      Memo.Lines.Add('');

    // Create an SMS message
    //Memo5.Lines.Add('Enter a phone nr. to send a message to: ');
    //Readln(strToAddress);
    //strToAddress := CelDest;
    //Memo5.Lines.Add('Enter the message body: ');
    //Readln(strMessage);
    //strMessage := Mensagem;
    //
    objSms.ToAddress := Telefone;
    objSms.Body := Mensagem;

    if Memo <> nil then
      Memo.Lines.Add('Enviando mensagem...');

    // Send the SMS message through the GSM device.
    objGsm.SendSms(objSms, objConstants.MULTIPART_ACCEPT, 0);
    Result := objGsm.LastError;
    PrintResult('Envio de SMS', objGsm, Memo);

    if Memo <> nil then
      Memo.Lines.Add('-�-�-�-�-�-�-�-�-�-�-�-�-�-�');
  end;
end;

function TDmModCobranca.AparelhoGSM_RecebeRelatorios(Memo: TMemo): Boolean;
begin
  // Receive all SMS messages and status reports from SIM.
  objGsm.Receive(objConstants.GSM_MESSAGESTATE_ALL, false,
     objConstants.GSM_STORAGETYPE_ALL, 0);
  PrintResult('Recebimento de relat�rios', objGsm, Memo);

  Memo.Lines.Add('');

  // Iterate over all SMS messages
  objGsmDeliveryReport := objGsm.GetFirstReport();
  if objGsm.LastError <> 0 then
     Memo.Lines.Add(objGsm.GetErrorDescription(objGsm.LastError));
  while objGsm.LastError = 0 do
  begin
    Memo.Lines.Add('  Mensagem de: ' + objGsmDeliveryReport.FromAddress);
    Memo.Lines.Add('  Texto do Relat�rio: ' + objGsmDeliveryReport.Reference);
    //SalvaSMSRecebidoNoBD(objSms.FromAddress, objSms.Reference);
    objGsm.DeleteReport(objGsmDeliveryReport);
    objGsmDeliveryReport := objGsm.GetNextReport();
  end;

  Memo.Lines.Add('');
end;

function TDmModCobranca.AparelhoGSM_RecebeSMSs(Memo: TMemo): Boolean;
begin
  // Receive all SMS messages and status reports from SIM.
  objGsm.Receive(objConstants.GSM_MESSAGESTATE_ALL, false,
     objConstants.GSM_STORAGETYPE_ALL, 0);
  PrintResult('Recebimento', objGsm, Memo);

  Memo.Lines.Add('');

  // Iterate over all SMS messages
  objSms := objGsm.GetFirstSms();
  if objGsm.LastError <> 0 then
     Memo.Lines.Add(objGsm.GetErrorDescription(objGsm.LastError));
  while objGsm.LastError = 0 do
  begin
    Memo.Lines.Add('  Mensagem de: ' + objSms.FromAddress);
    Memo.Lines.Add('  Texto da mensagem: ' + objSms.Body);
    SalvaSMSRecebidoNoBD(objSms.FromAddress, objSms.Body);
    objGsm.DeleteSms(objSms);
    objSms := objGsm.GetNextSms();
  end;

  Memo.Lines.Add('');
end;

procedure TDmModCobranca.AtualizaDiarCEMEnv_NaoEnv(Codigo, DiarCEMIts, Metodo,
  NaoEnvTip, NaoEnvLst, NaoEnvMot: Integer; NaoEnvVal, LastExec: String);
var
  ConsidEnvi, Execucao: String;
begin
  if NaoEnvTip = CO_COBRANCA_NAO_ENV_TIP_NO_ERRO_YET then
  begin
    ConsidEnvi := LastExec;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'diarcemenv', False, [
    'ConsidEnvi', CO_JOKE_SQL, 'LastExec',
    'NaoEnvTip', 'NaoEnvLst', 'NaoEnvMot'], [
    'Codigo', 'DiarCEMIts', 'Metodo'], [
    ConsidEnvi, 'Tentativas=Tentativas+1', LastExec,
    NaoEnvTip, NaoEnvLst, NaoEnvMot], [
    Codigo, DiarCEMIts, Metodo], True);
    //
  end else begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'diarcemenv', False, [
    CO_JOKE_SQL, 'LastExec',
    'NaoEnvTip', 'NaoEnvLst', 'NaoEnvMot'], [
    'Codigo', 'DiarCEMIts', 'Metodo'], [
    'Tentativas=Tentativas+1', LastExec,
    NaoEnvTip, NaoEnvLst, NaoEnvMot], [
    Codigo, DiarCEMIts, Metodo], True) then
    begin
      Execucao := LastExec;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'diarcemret', False, [
      'NaoEnvTip', 'NaoEnvLst', 'NaoEnvMot',
      'NaoEnvVal'], [
      'Codigo', 'DiarCEMIts', 'Metodo', 'Execucao'], [
      NaoEnvTip, NaoEnvLst, NaoEnvMot,
      NaoEnvVal], [
      Codigo, DiarCEMIts, Metodo, Execucao], True);
    end;
  end;
end;
{
SendSms, result: 36186 (Short message transfer rejected)   > Numero de celular destino inv�lido!
SendSms, result: 36253 (Unknown error)                     > Numero de celular destino inv�lido!
}
procedure TDmModCobranca.ComplementaQuery3_InadimpDeCond(SQL: TStringList;
  TabLctA: String; CondCod, CodEnt, Propriet, CondImov, RGDocumentos,
  PCAgrupamentos: Integer; QrCondGriImv: TmySQLQuery; EdMezIni,
  EdMezFim: String; CkNaoMensais: Boolean; TPIniDtaDate,
  TPFimDtaDate: TDateTime; CkIniDtaChecked, CkFimDtaChecked,
  GBEmissaoVisible: Boolean; TPIniVctDate, TPFimVctDate: TDateTime;
  CkIniVctChecked, CkFimVctChecked, GBVenctoVisible, FTemDtP: Boolean;
  FDtaPgt: String);
var
  C1, P1, A1: Integer;
  Txt, Lig: String;
begin
  SQL.Add('FROM ' + TabLctA + ' lan');
  SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
  SQL.Add('LEFT JOIN ' + TMeuDB + '.condimov  imv ON imv.Conta=lan.Depto');
  SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades emp ON emp.Codigo=car.ForneceI');
  SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades prp ON prp.Codigo=lan.ForneceI');
  SQL.Add('');
  //
  //SQL.Add('WHERE lan.FatID in (' + FmPrincipal.GetFatIDs + ')');
  case RGDocumentos of
    0: SQL.Add('WHERE lan.FatID in (600,601)');
    1: SQL.Add('WHERE lan.FatID in (610)');
    2: SQL.Add('WHERE lan.FatID in (600,601,610)');
  end;
  //
  // Reparcelados n�o s�o d�bitos
  SQL.Add('AND lan.Reparcel=0');
  SQL.Add('AND lan.Tipo=2');
  SQL.Add('');

  //

  if PCAgrupamentos = 0 then
  begin
    {CondCod := Geral.IMV(EdEmpresa.Text);
    if CondCod <> 0 then
      CondEnt := DModG.QrEmpresasCodigo.Value
    else
      CondEnt := 0;
    }
    //Propriet := Geral.IMV(EdPropriet.Text);
    //CondImov := Geral.IMV(EdCondImov.Text);
    //
    if CodEnt > 0 then
      SQL.Add('AND car.ForneceI=' + IntToStr(CodEnt));
    if Propriet > 0 then
      SQL.Add('AND lan.ForneceI=' + IntToStr(Propriet));
    if CondImov > 0 then
      SQL.Add('AND lan.Depto=' + IntToStr(CondImov));
  end else
  // Agrupar por grupos de uhs (donos ou imobili�rias ou ...)
  if (QrCondGriImv.State = dsBrowse) and (QrCondGriImv.RecordCount > 0) then
  begin
    Txt := 'AND (';
    QrCondGriImv.First;
    while not QrCondGriImv.Eof do
    begin
      if QrCondGriImv.RecNo > 1 then
        Txt := Txt + Chr(13) + Chr(10) + '  OR' + Chr(13) + Chr(10);
      //
      C1 := QrCondGriImv.FieldByName('Cond').AsInteger;
      P1 := QrCondGriImv.FieldByName('Propr').AsInteger;
      A1 := QrCondGriImv.FieldByName('Apto').AsInteger;
      Lig := '';
      if C1 <> 0 then
      begin
        Txt := Txt + 'imv.Codigo=' + IntToStr(C1);
        Lig := ' AND ';
      end;
      if P1 <> 0 then
      begin
        Txt := Txt + Lig + 'prp.Codigo=' + IntToStr(P1);
        Lig := ' AND ';
      end;
      if A1 <> 0 then
      begin
        Txt := Txt + Lig + 'imv.Conta=' + IntToStr(A1);
        //Lig := ' AND ';
      end;
      //
      Txt := Txt + Chr(13) + Chr(10);
      QrCondGriImv.Next;
    end;
    Txt := Txt + ')';
  end;
  SQL.Add(Txt);
  //

  if (EdMezIni <> '') or (EdMezFim <> '') then
    SQL.Add(dmkPF.SQL_Mensal('lan.Mez', EdMezIni, EdMezFim, CkNaoMensais));

  SQL.Add(dmkPF.SQL_Periodo('AND lan.Data ', TPIniDtaDate,
    TPFimDtaDate,
    CkIniDtaChecked and GBEmissaoVisible,
    CkFimDtaChecked and GBEmissaoVisible));

  SQL.Add(dmkPF.SQL_Periodo('AND lan.Vencimento ', TPIniVctDate,
    TPFimVctDate,
    CkIniVctChecked and GBVenctoVisible,
    CkFimVctChecked and GBVenctoVisible));

  (* Data que deseja ver os d�bitos ou hoje se n�o definido*)
  if FTemDtP then
    SQL.Add(
    'AND (' +
    '  (lan.Compensado > "' + FDtaPgt + '")' +
    '  OR (lan.Compensado < 2)' +
    '  )')
  else
    SQL.Add(
    'AND ' +
    '   (lan.Compensado < 2)' +
    '  ');
  SQL.Add('');
end;

procedure TDmModCobranca.EnviaAgendados_Email(WndCalled: TWndCalled;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel);
var
  Hoje, Celular, Mensagem, LastExec: String;
  //CodRetorno,
  Codigo, DiarCEMIts, Metodo,
  LastLstErr, LastCodRet: Integer;
  //
  Depto, PreEmail: Integer;
  Anexos, TextoTags: array of String;
  Enviou: Boolean;
  //
  NaoEnvTip, NaoEnvLst, NaoEnvMot: Integer;
  NaoEnvVal: String;
begin
  //if Tem Como Enviar Emails ? then
  begin
    Hoje       := Geral.FDT(Trunc(Date), 1);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEnvEmail, Dmod.MyDB, [
    'SELECT bol.Vencimento, bol.Data, bol.Mez, bol.FatNum,  ',
    'bol.Depto, bol.Propriet, bol.Usuario, bol.Empresa, ',
    'bol.EmprEnti, cim.Unidade NO_DEPTO,',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,',
    'env.*  ',
    'FROM diarcemenv env ',
    'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo',
    'LEFT JOIN entidades ent ON ent.Codigo=bol.EmprEnti',
    'LEFT JOIN condimov cim ON cim.Conta=bol.Depto',
    'WHERE env.Metodo=' + Geral.FF0(CO_DIARIO_ENVIO_MENSAGEM_EMAIL),
    'AND bol.Juridica=0',
    'AND env.ConsidEnvi < "1900-01-01" ',
    'AND env.AutoCancel < "1900-01-01" ',
    'AND env.DtaLimite >= SYSDATE() ',
    '']);
    //
    PB1.Position := 0;
    PB1.Max := QrEnvEmail.RecordCount;
    QrEnvEmail.First;
    while not QrEnvEmail.Eof do
    begin
      Codigo     := QrEnvEmailCodigo.Value;
      DiarCEMIts := QrEnvEmailDiarCEMIts.Value;
      Metodo     := QrEnvEmailMetodo.Value;
      LastExec   := Geral.FDT(DModG.ObtemAgora(), 109);
      Depto      := QrEnvEmailDepto.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrEmails, Dmod.MyDB, [
      'SELECT Pronome, Nome, Emeio  ',
      'FROM condemeios ',
      'WHERE Conta=' + Geral.FF0(Depto),
      '']);
      if QrEmails.RecordCount > 0 then
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Enviando email da UH ID ' + Geral.FF0(Depto));
        QrEmails.First;
        while not QrEmails.Eof do
        begin
          //Anexos := [];
          SetLength(TextoTags, 12);
          {Time}           TextoTags[00] := Geral.FDT(Now(), 100);
          {Date}           TextoTags[01] := Geral.FDT(Date, 2);
          {Administradora} TextoTags[02] := DmodG.QrDonoNOMEDONO.Value;
          {Saudacao}       TextoTags[03] := '';
          {Pronome}        TextoTags[04] := QrEmailsPronome.Value;
          {Nome}           TextoTags[05] := QrEmailsNome.Value;
          {Condominio}     TextoTags[06] := QrEnvEmailNO_EMPRESA.Value;
          {Valor}          TextoTags[07] := '(solicite c�lculo do valor � administradora)';
          {Vencimento}     TextoTags[08] := Geral.FDT(QrEnvEmailVencimento.Value, 2);
          {LinkConfirma}   TextoTags[09] := '';
          {LinkRecebeu}    TextoTags[10] := '';
          {UH}             TextoTags[11] := QrEnvEmailNO_DEPTO.Value;
          PreEmail := QrEnvEmailCadastro.Value;

    ////////////////////////////////////////////////////////////////////////////////
          // O ENVIO DO EMAIL � AQUI!
          Enviou := UMailEnv.Monta_e_Envia_Mail([Depto, PreEmail], meCobr,
            (*Anexos*)[], TextoTags, False);
    ////////////////////////////////////////////////////////////////////////////////

          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando registro no BD');
          if not Enviou then
          begin
            NaoEnvTip := CO_COBRANCA_NAO_ENV_TIP_MAIL_NO_ENV;
            NaoEnvLst := CO_DIARIO_LISTA_ERROS_DMK_SEND_MSGS;
            NaoEnvMot := CO_COBRANCA_NAO_ENV_MOT_GENERIC_ERR;
            NaoEnvVal := '';
          end
          else // Enviado com sucesso!
          begin
            NaoEnvTip := CO_COBRANCA_NAO_ENV_TIP_NO_ERRO_YET;
            NaoEnvLst := CO_DIARIO_LISTA_ERROS_DMK_SEM_ERROS;
            NaoEnvMot := CO_COBRANCA_NAO_ENV_MOT_ENVIO_FEITO;
            NaoEnvVal := '';
          end;
          //
          QrEmails.Next;
        end;
      end else
      begin
        NaoEnvTip := CO_COBRANCA_NAO_ENV_TIP_SEM_RECIPIE;
        NaoEnvLst := CO_DIARIO_LISTA_ERROS_DMK_SEND_MSGS;
        NaoEnvMot := CO_COBRANCA_NAO_ENV_MOT_NO_CAD_MAIL;
        NaoEnvVal := '';
      end;
      //
      DmModCobranca.AtualizaDiarCEMEnv_NaoEnv(Codigo, DiarCEMIts, Metodo,
        NaoEnvTip, NaoEnvLst, NaoEnvMot, NaoEnvVal, LastExec);
      //
      QrEnvEmail.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
end;

procedure TDmModCobranca.EnviaAgendados_SMS(WndCalled: TWndCalled;
  PB1: TProgressBar; LaAviso1, LaAviso2: TLabel; Memo: TMemo);
var
  Celular, Mensagem, LastExec: String;
  //CodRetorno,
  Codigo, DiarCEMIts, Metodo, CodRetorno,
  NaoEnvTip, NaoEnvLst, NaoEnvMot: Integer;
  Hora, NaoEnvVal: String;
begin
  Hora := Geral.FDT(DModG.ObtemAgora(), 100);
  UnDmkDAC_PF.AbreMySQLQuery0(QrCOMs, Dmod.MyDB, [
  'SELECT DISTINCT sms.Porta, SMS.PIN ',
  'FROM diarcemenv env ',
  'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo ',
  'LEFT JOIN txtsms sms ON sms.Codigo=env.Cadastro ',
  'WHERE env.Metodo=' + Geral.FF0(CO_DIARIO_ENVIO_MENSAGEM_SMS),
  'AND bol.Juridica=0 ',
  'AND env.ConsidEnvi < "1900-01-01" ',
  'AND env.AutoCancel < "1900-01-01" ',
  'AND env.DtaLimite >= SYSDATE() ',
  'AND (((sms.AMIni < sms.AMFim) AND ',
  '  ("' + Hora + '" BETWEEN sms.AMIni AND sms.AMFim)) ',
  '  OR ( ',
  '  (sms.PMIni < sms.PMFim) AND ( ',
  '  "' + Hora + '" BETWEEN sms.PMIni AND sms.PMFim)) ',
  ') ',
  '']);
  //
  QrCOMs.First;
  while not QrCOMs.Eof do
  begin
    if AparelhoGSM_Conecta(Memo,  QrCOMsPorta.Value,  QrCOMsPIN.Value) then
    begin
      try
        Hora := Geral.FDT(DModG.ObtemAgora(), 100);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrEnvSMS, Dmod.MyDB, [
        'SELECT bol.Vencimento, bol.Data, bol.Mez, bol.FatNum, bol.Depto, ',
        'bol.Propriet, bol.Usuario, bol.Empresa, bol.EmprEnti, ',
        'sms.Nome TEXTO, sms.PIN, sms.Porta,  sms.DiasSem, ',
        'sms.AMIni, sms.AMFim, sms.PMIni, sms.PMFim, env.* ',
        'FROM diarcemenv env ',
        'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo',
        'LEFT JOIN txtsms sms ON sms.Codigo=env.Cadastro',
        'WHERE env.Metodo=' + Geral.FF0(CO_DIARIO_ENVIO_MENSAGEM_SMS),
        'AND bol.Juridica=0',
        'AND env.ConsidEnvi < "1900-01-01" ',
        'AND env.AutoCancel < "1900-01-01" ',
        'AND env.DtaLimite >= SYSDATE() ',
        'AND sms.Porta="' + QrCOMsPorta.Value + '" ',
        'AND (((sms.AMIni < sms.AMFim) AND ',
        '  ("' + Hora + '" BETWEEN sms.AMIni AND sms.AMFim)) ',
        '  OR ( ',
        '  (sms.PMIni < sms.PMFim) AND ( ',
        '  "' + Hora + '" BETWEEN sms.PMIni AND sms.PMFim)) ',
        ') ',
        '']);
        //
        QrEnvSMS.First;
        PB1.Position := 0;
        PB1.Max := QrEnvSMS.RecordCount;
        while not QrEnvSMS.Eof do
        begin
          Codigo     := QrEnvSMSCodigo.Value;
          DiarCEMIts := QrEnvSMSDiarCEMIts.Value;
          Metodo     := QrEnvSMSMetodo.Value;
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QrEnti, Dmod.MyDB, [
          'SELECT imv.SMSCelTipo, imv.SMSCelNumr, imv.SMSCelNome,',
          'imv.Usuario, imv.Propriet,',
          'IF(usu.Tipo=0, usu.ECel, usu.PCel) CELULAR_USU, ',
          'IF(prp.Tipo=0, prp.ECel, prp.PCel) CELULAR_PRP, ',
          'IF(ter.Tipo=0, ter.ECel, ter.PCel) CELULAR_TER ',
          'FROM condimov imv',
          'LEFT JOIN entidades usu ON usu.Codigo=imv.Usuario',
          'LEFT JOIN entidades prp ON prp.Codigo=imv.Propriet',
          'LEFT JOIN entidades ter ON ter.Codigo=imv.SMSCelEnti',
          'WHERE imv.Conta=' + Geral.FF0(QrEnvSMSDepto.Value),
          '']);
          //
          case QrEntiSMSCelTipo.Value of
            0: // Morador ou proprietario
            begin
              if QrEntiUsuario.Value <> 0 then
                Celular := QrEntiCELULAR_USU.Value
              else
                Celular := QrEntiCELULAR_PRP.Value;
            end;
            1: Celular := QrEntiCELULAR_USU.Value; // Morador
            2: Celular := QrEntiCELULAR_PRP.Value; // Proprietario
            3: Celular := QrEntiCELULAR_TER.Value; // Terceiro
            4: Celular := QrEntiSMSCelNumr.Value;  // "Descrito Aqui" no condimov
            5: Celular := '';
          end;
          LastExec := Geral.FDT(DModG.ObtemAgora(), 109);
          if Celular = '' then
          begin
            NaoEnvTip := CO_COBRANCA_NAO_ENV_TIP_SEM_RECIPIE;
            NaoEnvLst := CO_DIARIO_LISTA_ERROS_DMK_SEND_MSGS;
            NaoEnvMot := CO_COBRANCA_NAO_ENV_MOT_NO_CAD_FONE;
            NaoEnvVal := Geral.FF0(QrEntiSMSCelTipo.Value);
            //
          end else
          begin
            Mensagem := SubstituiVariaveis(
              CO_DIARIO_ENVIO_MENSAGEM_SMS, QrEnvSMSTEXTO.Value);
            MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
              'Enviando mensagem para ' + Celular + ' > ' + Mensagem);
            //
  ////////////////////////////////////////////////////////////////////////////////
            // O ENVIO DO SMS � AQUI!
            CodRetorno := AparelhoGSM_EnviaSMS(Celular, Mensagem, Memo);
            //
  ////////////////////////////////////////////////////////////////////////////////

            if CodRetorno <> 0 then
            begin
              NaoEnvTip := CO_COBRANCA_NAO_ENV_TIP_SMS_NAO_ENV;
              NaoEnvLst := CO_DIARIO_LISTA_ERROS_ACTIVE_XPERTS;
              NaoEnvMot := CodRetorno;
              NaoEnvVal := objGsm.GetErrorDescription(CodRetorno);
            end
            else // Enviado com sucesso!
            begin
              NaoEnvTip := CO_COBRANCA_NAO_ENV_TIP_NO_ERRO_YET;
              NaoEnvLst := CO_DIARIO_LISTA_ERROS_DMK_SEM_ERROS;
              NaoEnvMot := CodRetorno;
              NaoEnvVal := '';
            end;
          end;
          DmModCobranca.AtualizaDiarCEMEnv_NaoEnv(Codigo, DiarCEMIts, Metodo,
            NaoEnvTip, NaoEnvLst, NaoEnvMot, NaoEnvVal, LastExec);
          MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, False, '...');
          QrEnvSMS.Next;
        end;
        //
      finally
        AparelhoGSM_Desconecta();
      end;
    end;
    QrCOMs.Next;
  end;
end;

function TDmModCobranca.Pesquisa3_InadimpDeCond(TabLctA: String; CondCod,
  CodEnt, Propriet, CondImov, RGDocumentos, PCAgrupamentos: Integer;
  QrCondGriImv: TmySQLQuery; EdMezIni, EdMezFim: String; CkNaoMensais: Boolean;
  TPIniDtaDate, TPFimDtaDate: TDateTime; CkIniDtaChecked, CkFimDtaChecked,
  GBEmissaoVisible: Boolean; TPIniVctDate, TPFimVctDate: TDateTime;
  CkIniVctChecked, CkFimVctChecked, GBVenctoVisible, CkFimPgtChecked: Boolean;
  TPFimPgtDate: TDateTime; RGSomas, RGSituacao, RGOrdem1, RGOrdem2, RGOrdem3,
  RGOrdem4: Integer): String;
  //
  function OrdemPesq: String;
  begin
    Result := 'ORDER BY ' +
    Ordens[RGOrdem1] + ',' +
    Ordens[RGOrdem2] + ',' +
    Ordens[RGOrdem3] + ',' +
    Ordens[RGOrdem4];
  end;
var
  FTemDtP: Boolean;
  FDtaPgt: String;
  SQL: TStringList;
begin
  SQL := TStringList.Create;
  try
    (* Data que deseja ver os d�bitos ou hoje se n�o definido*)
    if CkFimPgtChecked and GBVenctoVisible then
    begin
      FTemDtP := True;
      FDtaPgt := Geral.FDT(TPFimPgtDate, 1);
      if RGSomas = 1 then
      begin
        Geral.MB_Aviso('N�o � poss�vel ao mesmo tempo somar ' +
        '(bloquetos) e selecionar uma data limite para pesquisa!');
        Exit;
      end;
    end else begin
      FTemDtP := False;
      FDtaPgt := Geral.FDT(Date, 1);
    end;
    //
    // In�cio Chave Prim�ria
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('SELECT imv.Codigo Empresa, ');
    SQL.Add('lan.ForneceI,'); // Propriet�rio no Lct
    SQL.Add('imv.Conta Apto, ');
    SQL.Add('lan.Mez, ');
    SQL.Add('lan.Vencimento, ');
    SQL.Add('lan.FatNum, ');
    // Fim Chave Prim�ria
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('imv.Propriet, imv.Unidade, ');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('imv.Imobiliaria, imv.Procurador, ');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('imv.Usuario, imv.Juridico, lan.Data, lan.CliInt,');
    case RGSomas of
      0:
      begin
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('lan.Credito, lan.Pago,');
        SQL.Add('');
        SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000, 2), 0) Juros,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 0) Multa,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito TOTAL,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago SALDO,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('IF(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago <0,0,');
        SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) PEND_VAL,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('CONCAT("Bloqueto n� ", FatNum) Descricao,');
        SQL.Add('');
      end;
      1:
      begin
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('SUM(lan.Credito) CREDITO, SUM(lan.Pago) PAGO,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000, 2), 0)) Juros,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 0)) Multa,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito) TOTAL,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) SALDO,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('IF(SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) <0,0,');
        SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago)) PEND_VAL,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('CONCAT("Bloqueto n� ", FatNum) Descricao,');
        SQL.Add('');
      end;
      else
      begin
        SQL.Add('?credito?');
        SQL.Add('?Descricao?');
      end;
    end;
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('CONCAT(RIGHT(Mez, 2), "/",  LEFT(Mez + 200000, 4)) MEZ_TXT,');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('lan.Compensado, lan.Controle,');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('IF(lan.Vencimento=0, "", DATE_FORMAT(lan.Vencimento, "%d/%m/%y")) VCTO_TXT,');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMPCOND,');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEPRPIMOV,');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('lan.Vencimento NewVencto, lan.Ativo'); // NewVencto > Para segunda via de bloqueto

    //ComplementaQuery3(QrPesq3);
      ComplementaQuery3_InadimpDeCond(SQL,
      TabLctA,
      CondCod, CodEnt, Propriet, CondImov,
      RGDocumentos, PCAgrupamentos,
      QrCondGriImv,
      EdMezIni, EdMezFim,
      CkNaoMensais,
      TPIniDtaDate, TPFimDtaDate, CkIniDtaChecked, CkFimDtaChecked, GBEmissaoVisible,
      TPIniVctDate, TPFimVctDate, CkIniVctChecked, CkFimVctChecked, GBVenctoVisible,
      FTemDtP, FDtaPgt);

    if RGSituacao = 1 then
      SQL.Add('AND lan.Vencimento < SYSDATE()');

    case RGSomas of
      0: SQL.Add('');
      1: SQL.Add('GROUP BY lan.ForneceI, lan.Depto, lan.FatNum, lan.Mez, lan.Vencimento');
      else SQL.Add('?GROUP BY?');
    end;
    SQL.Add(OrdemPesq);
    //
    Result := SQL.Text;
    //dmkPF.LeMeuTexto(Result);
  finally
    SQL.Free;
  end;
end;

function TDmModCobranca.Pesquisa4_InadimpDeCond(TabLctA: String; CondCod,
  CodEnt, Propriet, CondImov, RGDocumentos, PCAgrupamentos: Integer;
  QrCondGriImv: TmySQLQuery; EdMezIni, EdMezFim: String; CkNaoMensais: Boolean;
  TPIniDtaDate, TPFimDtaDate: TDateTime; CkIniDtaChecked, CkFimDtaChecked,
  GBEmissaoVisible: Boolean; TPIniVctDate, TPFimVctDate: TDateTime;
  CkIniVctChecked, CkFimVctChecked, GBVenctoVisible, CkFimPgtChecked: Boolean;
  TPFimPgtDate: TDateTime; RGSomas, RGSituacao, RGOrdem1, RGOrdem2, RGOrdem3,
  RGOrdem4: Integer): String;
  //
  function OrdemPesq: String;
  begin
    Result := 'ORDER BY ' +
    Ordens[RGOrdem1] + ',' +
    Ordens[RGOrdem2] + ',' +
    Ordens[RGOrdem3] + ',' +
    Ordens[RGOrdem4];
  end;
var
  FTemDtP: Boolean;
  FDtaPgt: String;
  SQL: TStringList;
begin
  SQL := TStringList.Create;
  try
    (* Data que deseja ver os d�bitos ou hoje se n�o definido*)
    if CkFimPgtChecked and GBVenctoVisible then
    begin
      FTemDtP := True;
      FDtaPgt := Geral.FDT(TPFimPgtDate, 1);
      if RGSomas = 1 then
      begin
        Geral.MB_Aviso('N�o � poss�vel ao mesmo tempo somar ' +
        '(bloquetos) e selecionar uma data limite para pesquisa!');
        Exit;
      end;
    end else begin
      FTemDtP := False;
      FDtaPgt := Geral.FDT(Date, 1);
    end;
    //
    // In�cio Chave Prim�ria
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('SELECT imv.Codigo Empresa, ');
    SQL.Add('lan.ForneceI,'); // Propriet�rio no Lct
    SQL.Add('imv.Conta Apto, ');
    SQL.Add('lan.Mez, ');
    SQL.Add('lan.Vencimento, ');
    SQL.Add('lan.FatID, ');
    SQL.Add('lan.FatNum, ');
    // Fim Chave Prim�ria
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('imv.Propriet, imv.Unidade, ');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('imv.Imobiliaria, imv.Procurador, ');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('imv.Usuario, imv.Juridico, lan.Data, lan.CliInt,');
    case RGSomas of
      0:
      begin
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('lan.Credito, lan.Pago,');
        SQL.Add('');
        SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000, 2), 0) Juros,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 0) Multa,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito TOTAL,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago SALDO,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('IF(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago <0,0,');
        SQL.Add('IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) PEND_VAL,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('CONCAT("Bloqueto n� ", FatNum) Descricao,');
        SQL.Add('');
      end;
      1:
      begin
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('SUM(lan.Credito) CREDITO, SUM(lan.Pago) PAGO,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000, 2), 0)) Juros,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 0)) Multa,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito) TOTAL,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) SALDO,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('IF(SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) <0,0,');
        SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
        SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
        SQL.Add('  lan.MoraDia  / 3000 +');
        SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago)) PEND_VAL,');
        SQL.Add('');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
        SQL.Add('CONCAT("Bloqueto n� ", FatNum) Descricao,');
        SQL.Add('');
      end;
      else
      begin
        SQL.Add('?credito?');
        SQL.Add('?Descricao?');
      end;
    end;
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('CONCAT(RIGHT(Mez, 2), "/",  LEFT(Mez + 200000, 4)) MEZ_TXT,');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('lan.Compensado, lan.Controle,');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('IF(lan.Vencimento=0, "", DATE_FORMAT(lan.Vencimento, "%d/%m/%y")) VCTO_TXT,');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMPCOND,');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEPRPIMOV,');
////////////////////////////////////////////////////////////////////////////////
///
///  PARTE NOVA! Difere o Pesq3 do Pesq4
///
////////////////////////////////////////////////////////////////////////////////
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    //SQL.Add('lan.Vencimento NewVencto, lan.Ativo'); // NewVencto > Para segunda via de bloqueto
    SQL.Add('lan.Vencimento NewVencto, ');// lan.Ativo'); // NewVencto > Para segunda via de bloqueto
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('"" Infl1Indice, 0 Infl1PeriI, 0 Infl1PeriF, 0 Infl1FatorI,');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('0 nfl1FatorF, 0 Infl1ValorT, 0 Infl1Percen, 0 Infl1ValorS,');
    //
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('"" Infl2Indice, 0 Infl2PeriI, 0 Infl2PeriF, 0 Infl2FatorI,');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('0 nfl1FatorF, 0 Infl2ValorT, 0 Infl2Percen, 0 Infl2ValorS,');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('0 Infl2ValorS, ');
    // CUIDADO QUANDO MEXER! ORDEM DOS CAMPOS DEVE PERMANECER > COBRARGER!
    SQL.Add('lan.Ativo');
////////////////////////////////////////////////////////////////////////////////
///
///  FIM PARTE NOVA! Difere o Pesq3 do Pesq4
///
////////////////////////////////////////////////////////////////////////////////
    //ComplementaQuery3(QrPesq3);
      ComplementaQuery3_InadimpDeCond(SQL,
      TabLctA,
      CondCod, CodEnt, Propriet, CondImov,
      RGDocumentos, PCAgrupamentos,
      QrCondGriImv,
      EdMezIni, EdMezFim,
      CkNaoMensais,
      TPIniDtaDate, TPFimDtaDate, CkIniDtaChecked, CkFimDtaChecked, GBEmissaoVisible,
      TPIniVctDate, TPFimVctDate, CkIniVctChecked, CkFimVctChecked, GBVenctoVisible,
      FTemDtP, FDtaPgt);

    if RGSituacao = 1 then
      SQL.Add('AND lan.Vencimento < SYSDATE()');

    case RGSomas of
      0: SQL.Add('');
      1: SQL.Add('GROUP BY lan.ForneceI, lan.Depto, lan.FatNum, lan.Mez, lan.Vencimento');
      else SQL.Add('?GROUP BY?');
    end;
    SQL.Add(OrdemPesq);
    //
    Result := SQL.Text;
    //dmkPF.LeMeuTexto(Result);
  finally
    SQL.Free;
  end;
end;

procedure TDmModCobranca.PesquisaCondominio(CondCod, CodEnt: Integer);
const
  Propriet         = 0;     // 0=Todos
  CondImov         = 0;     // 0=Todos
  RGDocumentos     = 2;     // 0=Boletos 1=Reparcel 2=Ambos
  PCAgrupamentos   = 0;     // 0=Normal 1=Grupos de UHs
  QrCondGriImv     = nil;   // Somente para "Grupos de UHs" > PCAgrupamentos = 1
  EdMezIni         = '';    // Compet�ncia inicial
  EdMezFim         = '';    // Compet�ncia final
  CkNaoMensais     = False; // Somente lanctos com per�odo informado!
  TPIniDtaDate     = 0;     ////////////////////////////////////////////////////
  TPFimDtaDate     = 0;     //                                                //
  CkIniDtaChecked  = False; //                                                //
  CkFimDtaChecked  = False; //                                                //
  GBEmissaoVisible = False; //                                                //
  TPIniVctDate     = 0;     //           Per�odos de datas                    //
  TPFimVctDate     = 0;     //                                                //
  CkIniVctChecked  = False; //                                                //
  CkFimVctChecked  = False; //                                                //
  GBVenctoVisible  = False; //                                                //
  CkFimPgtChecked  = False; //                                                //
  TPFimPgtDate     = 0;     ////////////////////////////////////////////////////
  RGSomas          = 1;     // 0=Nenhuma 1=Boletos
  RGSituacao       = 1;     // 0=Abertos 1=Atrasados
  RGOrdem1         = 3;     ////////////////////////////////////////////////////
  RGOrdem2         = 4;     //   Ordem dos resultados                         //
  RGOrdem3         = 0;     //   *** sem import�ncia, pois � inclus�o ***     //
  RGOrdem4         = 5;     ////////////////////////////////////////////////////
var
  TabLctA: String;
begin
  //Loop
  //TabLctA   := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CondCod);
  TabLctA   := DmkPF.NomeTabelaLtc(CondCod, ttA);
  //
  QrPesq3.SQL.Text := 'INSERT INTO ' + FInadimp + #13#10 +
  Pesquisa3_InadimpDeCond(TabLctA,
  CondCod, CodEnt, Propriet, CondImov,
  RGDocumentos, PCAgrupamentos,
  QrCondGriImv,
  EdMezIni, EdMezFim,
  CkNaoMensais,
  TPIniDtaDate, TPFimDtaDate, CkIniDtaChecked, CkFimDtaChecked, GBEmissaoVisible,
  TPIniVctDate, TPFimVctDate, CkIniVctChecked, CkFimVctChecked, GBVenctoVisible,
  CkFimPgtChecked,
  TPFimPgtDate,
  RGSomas, RGSituacao, RGOrdem1, RGOrdem2, RGOrdem3, RGOrdem4);
  //QrPesq3.SQL.Text := QrPesq3.SQL.Text;
  UnDmkDAC_PF.ExecutaQuery(QrPesq3, DModG.MyPID_DB);
end;


procedure TDmModCobranca.PrintResult(strFunction: string; objGsm: Gsm; Memo: TMemo);
begin
  if Memo <> nil then
  Memo.Lines.Add(strFunction + ', result: ' + IntToStr(objGsm.LastError) + ' (' +
    objGsm.GetErrorDescription(objGsm.LastError) + ')');
end;

procedure TDmModCobranca.QrPesq3CalcFields(DataSet: TDataSet);
begin
  QrPesq3Juridico_TXT.Value := dmkPF.DefineJuricoSigla(QrPesq3Juridico.Value);
  QrPesq3Juridico_DESCRI.Value := dmkPF.DefineJuricoSigla(QrPesq3Juridico.Value);
end;

procedure TDmModCobranca.SalvaSMSRecebidoNoBD(FromAddr, SMS: WideString);
var
  Nome, DataObtida, DataLida: String;
  UserLida: Integer;
  Codigo: Int64;
begin
  Codigo     := UMyMod.BPGS1I64('smsrecebe', 'Codigo', '', '', tsPos, stIns, 0);
  Nome       := SMS;
  DataObtida := Geral.FDT(DModG.ObtemAgora(), 109);
  DataLida   := '0000-00-00 00:00:00';
  UserLida   := 0;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'smsrecebe', False, [
  'Nome', 'FromAddr', 'DataObtida',
  'DataLida', 'UserLida'], [
  'Codigo'], [
  Nome, FromAddr, DataObtida,
  DataLida, UserLida], [
  Codigo], True);
end;

function TDmModCobranca.SubstituiVariaveis(ModoEnvio: Integer;
  Texto: WideString): String;
begin
  Result := Texto;
  //
  case ModoEnvio of
    CO_DIARIO_ENVIO_MENSAGEM_SMS:
    begin
      // Falta fazer
   {sListaVarsSMS: array[0..MaxVarsSMS] of String = (
  '[DATA_VCT] Data do vencimento',
  '');}
      Result := Geral.Substitui(Result, '[DATA_VCT]', Geral.FDT(QrEnvSMSVencimento.Value, 2));
    end;
    CO_DIARIO_ENVIO_MENSAGEM_EMAIL:
    begin
      // Falta fazer
    end;
    CO_DIARIO_ENVIO_MENSAGEM_CARTA:
    begin
      // Falta fazer
    end;
  end;
end;

procedure TDmModCobranca.VerificaInadimplencia(WndCalled: TWndCalled;
PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
CkForcaPsqChecked: Boolean; Empr, Enti: Integer);
var
  Hoje: Integer;
  UltPsqCobr, Msg: String;
  Empresa, Entidade: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtrlGeral, Dmod.MyDB, [
  'SELECT UltPsqCobr ',
  'FROM ctrlgeral ',
  '']);
  Hoje := Trunc(Date);
  if QrCtrlGeralUltPsqCobr.Value - Hoje >= 1 then
  begin
    Msg := 'A data de hoje "' + Geral.FDT(Hoje, 3) +
    '" � menor que a data da �ltima verifica��o: "' +
    Geral.FDT(QrCtrlGeralUltPsqCobr.Value, 3) + '.' + sLineBreak +
    'Verifique se a data do computador est� correta!';
    case WndCalled of
      wcForm: Geral.MB_Aviso(Msg);
      wcService: dmkPF.GravaEmLog(FLog, Msg);
    end;
  end else
  begin
    if CkForcaPsqChecked or (Hoje > QrCtrlGeralUltPsqCobr.Value) then
    begin
      QrPesq3.Close;
      FInadimp  := UnCreateApp.RecriaTempTableNovo(ntrtt_Inadimp, DmodG.QrUpdPID1, False);
      //CondCod   := EdEmpresa.ValueVariant;
      if Empr <> 0 then
      begin
        Msg := 'Pesquisando condom�nio n� ' + Geral.FF0(Empresa);
        case WndCalled of
          {$IfNDef ServicoDoWindows}
          wcForm: MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, Msg);
          {$EndIf}
          wcService: dmkPF.GravaEmLog(FLog, Msg);
        end;
        //
        Empresa := Empr;
        Entidade := Enti;
        PesquisaCondominio(Empresa, Entidade);
      end else
      begin
        UnDmkDAC_PF.AbreQuery(QrCond, Dmod.MyDB);
        PB1.Position := 0;
        PB1.Max := QrCond.RecordCount;
        QrCond.First;
        while not QrCond.Eof do
        begin
          Empresa := QrCondCodigo.Value;
          Entidade := QrCondCliente.Value;
          //
          Msg := 'Pesquisando condom�nio n� ' + Geral.FF0(Empresa);
          case WndCalled of
            {$IfNDef ServicoDoWindows}
            wcForm: MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, Msg);
            {$EndIf}
            wcService: dmkPF.GravaEmLog(FLog, Msg);
          end;
          //
          PesquisaCondominio(Empresa, Entidade);
          //
          QrCond.Next;
        end;
      end;
      UnDmkDAC_PF.AbreMySQLQuery0(QrPesq3, DModG.MyPID_DB, [
      'SELECT * FROM  ' + FInadimp,
      'ORDER BY NOMEEMPCOND, Unidade, Mez, Vencimento, Data',
      '']);
      //
      UltPsqCobr := Geral.FDT(Hoje, 1);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctrlgeral', False, [
      'UltPsqCobr'], [], [UltPsqCobr], [], False);
      //
      PB1.Position := 0;
      Msg := 'Pesquisando condom�nio n� ' + Geral.FF0(Empresa);
      {$IfNDef ServicoDoWindows}
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      {$EndIf}
    end;
  end;
end;

end.
