unit SrvTomTaxCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkDBLookupComboBox, dmkEditCB;

type
  TFmSrvTomTaxCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrSrvTomTaxCad: TMySQLQuery;
    QrSrvTomTaxCadCodigo: TIntegerField;
    QrSrvTomTaxCadNome: TWideStringField;
    DsSrvTomTaxCad: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrSrvTomTaxCadSigla: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    EdSigla: TdmkEdit;
    Label4: TLabel;
    RGTIFEM: TdmkRadioGroup;
    DBRGTIFEM: TDBRadioGroup;
    QrSrvTomTaxCadTIFEM: TSmallintField;
    Label181: TLabel;
    SbGenCtbD: TSpeedButton;
    Label5: TLabel;
    SbGenCtbC: TSpeedButton;
    Label6: TLabel;
    Label8: TLabel;
    QrSrvTomTaxCadGenCtbD: TIntegerField;
    QrSrvTomTaxCadGenCtbC: TIntegerField;
    QrSrvTomTaxCadNO_GenCtbD: TWideStringField;
    QrSrvTomTaxCadNO_GenCtbC: TWideStringField;
    EdGenCtbD: TdmkEditCB;
    CBGenCtbD: TdmkDBLookupComboBox;
    EdGenCtbC: TdmkEditCB;
    CBGenCtbC: TdmkDBLookupComboBox;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    QrContasD: TMySQLQuery;
    QrContasDCodigo: TIntegerField;
    QrContasDNome: TWideStringField;
    DsContasD: TDataSource;
    QrContasC: TMySQLQuery;
    QrContasCCodigo: TIntegerField;
    QrContasCNome: TWideStringField;
    DsContasC: TDataSource;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSrvTomTaxCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSrvTomTaxCadBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbGenCtbDClick(Sender: TObject);
    procedure SbGenCtbCClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmSrvTomTaxCad: TFmSrvTomTaxCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnSrvTom_PF, UnFinanceiroJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSrvTomTaxCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSrvTomTaxCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSrvTomTaxCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSrvTomTaxCad.DefParams;
begin
  VAR_GOTOTABELA := 'srvtomtaxcad';
  VAR_GOTOMYSQLTABLE := QrSrvTomTaxCad;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ctd.Nome NO_GenCtbD, ');
  VAR_SQLx.Add('ctc.Nome NO_GenCtbC, tax.*');
  VAR_SQLx.Add('FROM srvtomtaxcad tax');
  VAR_SQLx.Add('LEFT JOIN contas ctd ON ctd.Codigo=tax.GenCtbD');
  VAR_SQLx.Add('LEFT JOIN contas ctc ON ctc.Codigo=tax.GenCtbC');
  VAR_SQLx.Add('WHERE tax.Codigo > 0');  //
  VAR_SQL1.Add('AND tax.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND tax.Nome Like :P0');
  //
end;

procedure TFmSrvTomTaxCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSrvTomTaxCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSrvTomTaxCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmSrvTomTaxCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSrvTomTaxCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSrvTomTaxCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSrvTomTaxCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSrvTomTaxCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvTomTaxCad.BtAlteraClick(Sender: TObject);
begin
  if (QrSrvTomTaxCad.State <> dsInactive) and (QrSrvTomTaxCad.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrSrvTomTaxCad, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'srvtomtaxcad');
  end;
end;

procedure TFmSrvTomTaxCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSrvTomTaxCadCodigo.Value;
  Close;
end;

procedure TFmSrvTomTaxCad.BtConfirmaClick(Sender: TObject);
var
  Nome, Sigla: String;
  Codigo, TIFEM, GenCtbD, GenCtbC: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Sigla          := EdSigla.ValueVariant;
  TIFEM          := RGTIFEM.ItemIndex;
  if TIFEM < 0 then
    TIFEM := 0;
  GenCtbD := EdGenCtbD.ValueVariant;
  GenCtbC := EdGenCtbC.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  if MyObjects.FIC(Length(Sigla) = 0, EdSigla, 'Defina uma Sigla!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('srvtomtaxcad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'srvtomtaxcad', False, [
  'Nome', 'Sigla', 'TIFEM',
  'GenCtbD', 'GenCtbC'], [
  'Codigo'], [
  Nome, Sigla, TIFEM,
  GenCtbD, GenCtbC], [
  Codigo], True) then
  begin
    RGTIFEM.ItemIndex := 0;
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmSrvTomTaxCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'srvtomtaxcad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmSrvTomTaxCad.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrSrvTomTaxCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'srvtomtaxcad');
  RGTIFEM.ItemIndex := 0;
end;

procedure TFmSrvTomTaxCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  MyObjects.PreencheComponente(RGTIFEM, sTipImpostoFddEstMun, 4);
  MyObjects.PreencheComponente(DBRGTIFEM, sTipImpostoFddEstMun, 4);
  UMyMod.AbreQuery(QrContasD, Dmod.MyDB);
  UMyMod.AbreQuery(QrContasC, Dmod.MyDB);
  //
  CriaOForm;
end;

procedure TFmSrvTomTaxCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSrvTomTaxCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvTomTaxCad.SbGenCtbCClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  FinanceiroJan.MostraFormContas(EdGenCtbD.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdGenCtbD, CBGenCtbD, QrContasD, VAR_CADASTRO);
{$EndIf}
end;

procedure TFmSrvTomTaxCad.SbGenCtbDClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  FinanceiroJan.MostraFormContas(EdGenCtbC.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdGenCtbC, CBGenCtbC, QrContasC, VAR_CADASTRO);
{$EndIf}
end;

procedure TFmSrvTomTaxCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSrvTomTaxCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrSrvTomTaxCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvTomTaxCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSrvTomTaxCad.QrSrvTomTaxCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSrvTomTaxCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSrvTomTaxCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSrvTomTaxCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'srvtomtaxcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSrvTomTaxCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvTomTaxCad.QrSrvTomTaxCadBeforeOpen(DataSet: TDataSet);
begin
  QrSrvTomTaxCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

