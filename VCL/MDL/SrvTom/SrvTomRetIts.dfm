object FmSrvTomRetIts: TFmSrvTomRetIts
  Left = 339
  Top = 185
  Caption = 
    'SRV-TOMAD-004 :: Itens de Reten'#231#227'o de Imposto de Servi'#231'os Tomado' +
    's'
  ClientHeight = 549
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 633
        Height = 32
        Caption = 'Itens de Reten'#231#227'o de Imposto de Servi'#231'os Tomados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 633
        Height = 32
        Caption = 'Itens de Reten'#231#227'o de Imposto de Servi'#231'os Tomados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 633
        Height = 32
        Caption = 'Itens de Reten'#231#227'o de Imposto de Servi'#231'os Tomados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 435
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 479
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 29
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label6: TLabel
      Left = 8
      Top = 8
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label7: TLabel
      Left = 148
      Top = 8
      Width = 40
      Height = 13
      Caption = 'Imposto:'
    end
    object EdCodigo: TdmkEdit
      Left = 48
      Top = 4
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNO_Imposto: TdmkEdit
      Left = 192
      Top = 4
      Width = 253
      Height = 21
      Enabled = False
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object Ed_Sel_: TdmkEditCB
      Left = 8
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
  end
  object DGDados: TDBGrid
    Left = 0
    Top = 77
    Width = 1008
    Height = 358
    Align = alClient
    DataSource = DsSrvTomInnCab
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Codigo'
        Title.Caption = 'C'#243'digo'
        Width = 42
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NfsDataEmissao'
        Title.Caption = 'Data emiss'#227'o'
        Width = 89
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NfsNumero'
        Title.Caption = 'N'#176' NFS'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Prestador'
        Width = 52
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_PRESTADOR'
        Title.Caption = 'Nome Prestador'
        Width = 108
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Title.Caption = 'Descri'#231#227'o'
        Width = 114
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DpsValorServicos'
        Title.Caption = '$ Servi'#231'os'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NfsValorLiquidoNfse'
        Title.Caption = '$ L'#237'q. NFS'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NfsValorIss'
        Title.Caption = '$ ISS'
        Width = 52
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RetValorISS'
        Title.Caption = '$ ISS'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RetValorPis'
        Title.Caption = '$ PIS'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RetValorCofins'
        Title.Caption = '$ COFINS'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RetValorCsll'
        Title.Caption = '$ CSLL'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RetValorIr'
        Title.Caption = '$ IR'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RetValorInss'
        Title.Caption = '$ INSS'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RetOutrasRetencoes'
        Title.Caption = '$ Outras Ret.'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DpsResponsavelRetencao'
        Title.Caption = 'Resp.Ret.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DpsDescontoCondicionado'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DpsDescontoIncondicionado'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DpsDiscriminacao'
        Width = 300
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DpsRpsIDNumero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DpsRpsIDSerie'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DpsValorDeducoes'
        Title.Caption = '$ Dedu'#231#245'es'
        Width = 60
        Visible = True
      end>
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM enticargos'
      'ORDER BY Nome')
    Left = 216
    Top = 44
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 216
    Top = 92
  end
  object QrSrvTomInnCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sic.*, emp.Filial,'
      'IF(emp.Tipo=0, emp.RAZAOSOCIAL, emp.Nome) NO_EMPRESA,'
      'IF(prs.Tipo=0, prs.RAZAOSOCIAL, prs.Nome) NO_PRESTADOR,'
      'IF(itm.Tipo=0, itm.RAZAOSOCIAL, itm.Nome) NO_INTERMEDIARIO,'
      'lsr.Nome NO_DpsItemListaServico, cna.Nome NO_cnae21Cad,'
      'ts1.Nome NO_NfsRpsIDTipo, ts3.Nome NO_DpsIssRetido,'
      'tsr.Nome NO_DpsResponsavelRetencao'
      'FROM srvtominncab sic'
      'LEFT JOIN entidades emp ON emp.Codigo=sic.Empresa'
      'LEFT JOIN entidades prs ON prs.Codigo=sic.Prestador'
      'LEFT JOIN entidades itm ON itm.Codigo=sic.Intermediario'
      
        'LEFT JOIN locbdermall.listserv  lsr ON lsr.Codigo=sic.DpsItemLis' +
        'taServico'
      
        'LEFT JOIN locbdermall.cnae21Cad cna ON cna.Codigo=sic.DpsCodigoC' +
        'nae'
      
        'LEFT JOIN locbdermall.tstiporps ts1 ON ts1.Codigo=sic.NfsRpsIDTi' +
        'po'
      
        'LEFT JOIN locbdermall.tssimnao  ts3 ON ts3.Codigo=sic.DpsIssReti' +
        'do'
      
        'LEFT JOIN tsresponsavelretencao tsr ON tsr.Codigo=sic.DpsRespons' +
        'avelRetencao'
      'WHERE sic.Codigo > 0')
    Left = 292
    Top = 233
    object QrSrvTomInnCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSrvTomInnCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrSrvTomInnCabPrestador: TIntegerField
      FieldName = 'Prestador'
      Required = True
    end
    object QrSrvTomInnCabIntermediario: TIntegerField
      FieldName = 'Intermediario'
      Required = True
    end
    object QrSrvTomInnCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSrvTomInnCabStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrSrvTomInnCabNfsNumero: TLargeintField
      FieldName = 'NfsNumero'
      Required = True
    end
    object QrSrvTomInnCabNfsRpsIDSerie: TWideStringField
      FieldName = 'NfsRpsIDSerie'
      Size = 5
    end
    object QrSrvTomInnCabNfsRpsIDTipo: TSmallintField
      FieldName = 'NfsRpsIDTipo'
      Required = True
    end
    object QrSrvTomInnCabNfsRpsIDNumero: TIntegerField
      FieldName = 'NfsRpsIDNumero'
      Required = True
    end
    object QrSrvTomInnCabNfsCodigoVerificacao: TWideStringField
      FieldName = 'NfsCodigoVerificacao'
      Size = 9
    end
    object QrSrvTomInnCabNfsDataEmissao: TDateTimeField
      FieldName = 'NfsDataEmissao'
      Required = True
    end
    object QrSrvTomInnCabNfsNfseSubstituida: TLargeintField
      FieldName = 'NfsNfseSubstituida'
      Required = True
    end
    object QrSrvTomInnCabNfsOutrasInformacoes: TWideStringField
      FieldName = 'NfsOutrasInformacoes'
      Size = 255
    end
    object QrSrvTomInnCabNfsBaseCalculo: TFloatField
      FieldName = 'NfsBaseCalculo'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabNfsAliquota: TFloatField
      FieldName = 'NfsAliquota'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabNfsValorIss: TFloatField
      FieldName = 'NfsValorIss'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabNfsValorLiquidoNfse: TFloatField
      FieldName = 'NfsValorLiquidoNfse'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabNfsValorCredito: TFloatField
      FieldName = 'NfsValorCredito'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsRpsIDNumero: TIntegerField
      FieldName = 'DpsRpsIDNumero'
      Required = True
    end
    object QrSrvTomInnCabDpsRpsIDSerie: TWideStringField
      FieldName = 'DpsRpsIDSerie'
      Size = 5
    end
    object QrSrvTomInnCabDpsValorServicos: TFloatField
      FieldName = 'DpsValorServicos'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsValorDeducoes: TFloatField
      FieldName = 'DpsValorDeducoes'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsValorPis: TFloatField
      FieldName = 'DpsValorPis'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsValorCofins: TFloatField
      FieldName = 'DpsValorCofins'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsValorInss: TFloatField
      FieldName = 'DpsValorInss'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsValorIr: TFloatField
      FieldName = 'DpsValorIr'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsValorCsll: TFloatField
      FieldName = 'DpsValorCsll'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsOutrasRetencoes: TFloatField
      FieldName = 'DpsOutrasRetencoes'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsDescontoIncondicionado: TFloatField
      FieldName = 'DpsDescontoIncondicionado'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsDescontoCondicionado: TFloatField
      FieldName = 'DpsDescontoCondicionado'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsIssRetido: TSmallintField
      FieldName = 'DpsIssRetido'
      Required = True
    end
    object QrSrvTomInnCabDpsResponsavelRetencao: TIntegerField
      FieldName = 'DpsResponsavelRetencao'
    end
    object QrSrvTomInnCabDpsItemListaServico: TWideStringField
      FieldName = 'DpsItemListaServico'
      Size = 5
    end
    object QrSrvTomInnCabDpsCodigoCnae: TWideStringField
      FieldName = 'DpsCodigoCnae'
    end
    object QrSrvTomInnCabDpsCodigoTributacaoMunicipio: TWideStringField
      FieldName = 'DpsCodigoTributacaoMunicipio'
    end
    object QrSrvTomInnCabDpsDiscriminacao: TWideMemoField
      FieldName = 'DpsDiscriminacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrSrvTomInnCabDpsCodigoMunicipio: TIntegerField
      FieldName = 'DpsCodigoMunicipio'
      Required = True
    end
    object QrSrvTomInnCabDpsCodigoPais: TIntegerField
      FieldName = 'DpsCodigoPais'
      Required = True
    end
    object QrSrvTomInnCabDpsExigibilidadeIss: TSmallintField
      FieldName = 'DpsExigibilidadeIss'
      Required = True
    end
    object QrSrvTomInnCabDpsNaturezaOperacao: TIntegerField
      FieldName = 'DpsNaturezaOperacao'
      Required = True
    end
    object QrSrvTomInnCabDpsMunicipioIncidencia: TIntegerField
      FieldName = 'DpsMunicipioIncidencia'
      Required = True
    end
    object QrSrvTomInnCabDpsNumeroProcesso: TWideStringField
      FieldName = 'DpsNumeroProcesso'
      Size = 30
    end
    object QrSrvTomInnCabDpsConstrucaoCivilCodigoObra: TWideStringField
      FieldName = 'DpsConstrucaoCivilCodigoObra'
      Size = 15
    end
    object QrSrvTomInnCabDpsConstrucaoCivilArt: TWideStringField
      FieldName = 'DpsConstrucaoCivilArt'
      Size = 15
    end
    object QrSrvTomInnCabDpsRegimeEspecialTributacao: TSmallintField
      FieldName = 'DpsRegimeEspecialTributacao'
      Required = True
    end
    object QrSrvTomInnCabDpsOptanteSimplesNacional: TSmallintField
      FieldName = 'DpsOptanteSimplesNacional'
      Required = True
    end
    object QrSrvTomInnCabDpsIncentivoFiscal: TSmallintField
      FieldName = 'DpsIncentivoFiscal'
      Required = True
    end
    object QrSrvTomInnCabDpsInfID_ID: TWideStringField
      FieldName = 'DpsInfID_ID'
      Size = 255
    end
    object QrSrvTomInnCabLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrSrvTomInnCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvTomInnCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvTomInnCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrSrvTomInnCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrSrvTomInnCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrSrvTomInnCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrSrvTomInnCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrSrvTomInnCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrSrvTomInnCabFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrSrvTomInnCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrSrvTomInnCabNO_PRESTADOR: TWideStringField
      FieldName = 'NO_PRESTADOR'
      Size = 100
    end
    object QrSrvTomInnCabNO_INTERMEDIARIO: TWideStringField
      FieldName = 'NO_INTERMEDIARIO'
      Size = 100
    end
    object QrSrvTomInnCabNO_DpsItemListaServico: TWideStringField
      FieldName = 'NO_DpsItemListaServico'
      Size = 255
    end
    object QrSrvTomInnCabNO_cnae21Cad: TWideStringField
      FieldName = 'NO_cnae21Cad'
      Size = 255
    end
    object QrSrvTomInnCabNO_NfsRpsIDTipo: TWideStringField
      FieldName = 'NO_NfsRpsIDTipo'
      Size = 60
    end
    object QrSrvTomInnCabNO_DpsIssRetido: TWideStringField
      FieldName = 'NO_DpsIssRetido'
      Size = 6
    end
    object QrSrvTomInnCabNO_DpsResponsavelRetencao: TWideStringField
      FieldName = 'NO_DpsResponsavelRetencao'
      Size = 60
    end
  end
  object DsSrvTomInnCab: TDataSource
    DataSet = QrSrvTomInnCab
    Left = 292
    Top = 277
  end
end
