unit SrvTomInnCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkMemo, dmkLabelRotate, dmkDBLookupComboBox,
  dmkEditCB, Vcl.ComCtrls, dmkEditDateTimePicker, UnInternalConsts3;

type
  TFmSrvTomInnCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrSrvTomInnCab: TMySQLQuery;
    DsSrvTomInnCab: TDataSource;
    Qr_: TMySQLQuery;
    Ds_: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrSrvTomInnCabCodigo: TIntegerField;
    QrSrvTomInnCabEmpresa: TIntegerField;
    QrSrvTomInnCabPrestador: TIntegerField;
    QrSrvTomInnCabIntermediario: TIntegerField;
    QrSrvTomInnCabNome: TWideStringField;
    QrSrvTomInnCabStatus: TSmallintField;
    QrSrvTomInnCabNfsNumero: TLargeintField;
    QrSrvTomInnCabNfsRpsIDSerie: TWideStringField;
    QrSrvTomInnCabNfsRpsIDTipo: TSmallintField;
    QrSrvTomInnCabNfsRpsIDNumero: TIntegerField;
    QrSrvTomInnCabNfsCodigoVerificacao: TWideStringField;
    QrSrvTomInnCabNfsDataEmissao: TDateTimeField;
    QrSrvTomInnCabNfsNfseSubstituida: TLargeintField;
    QrSrvTomInnCabNfsOutrasInformacoes: TWideStringField;
    QrSrvTomInnCabNfsBaseCalculo: TFloatField;
    QrSrvTomInnCabNfsAliquota: TFloatField;
    QrSrvTomInnCabNfsValorIss: TFloatField;
    QrSrvTomInnCabNfsValorLiquidoNfse: TFloatField;
    QrSrvTomInnCabNfsValorCredito: TFloatField;
    QrSrvTomInnCabDpsRpsIDNumero: TIntegerField;
    QrSrvTomInnCabDpsRpsIDSerie: TWideStringField;
    QrSrvTomInnCabDpsValorServicos: TFloatField;
    QrSrvTomInnCabDpsValorDeducoes: TFloatField;
    QrSrvTomInnCabDpsValorPis: TFloatField;
    QrSrvTomInnCabDpsValorCofins: TFloatField;
    QrSrvTomInnCabDpsValorInss: TFloatField;
    QrSrvTomInnCabDpsValorIr: TFloatField;
    QrSrvTomInnCabDpsValorCsll: TFloatField;
    QrSrvTomInnCabDpsOutrasRetencoes: TFloatField;
    QrSrvTomInnCabDpsDescontoIncondicionado: TFloatField;
    QrSrvTomInnCabDpsDescontoCondicionado: TFloatField;
    QrSrvTomInnCabDpsIssRetido: TSmallintField;
    QrSrvTomInnCabDpsItemListaServico: TWideStringField;
    QrSrvTomInnCabDpsCodigoCnae: TWideStringField;
    QrSrvTomInnCabDpsCodigoTributacaoMunicipio: TWideStringField;
    QrSrvTomInnCabDpsDiscriminacao: TWideMemoField;
    QrSrvTomInnCabDpsCodigoMunicipio: TIntegerField;
    QrSrvTomInnCabDpsCodigoPais: TIntegerField;
    QrSrvTomInnCabDpsExigibilidadeIss: TSmallintField;
    QrSrvTomInnCabDpsNaturezaOperacao: TIntegerField;
    QrSrvTomInnCabDpsMunicipioIncidencia: TIntegerField;
    QrSrvTomInnCabDpsNumeroProcesso: TWideStringField;
    QrSrvTomInnCabDpsConstrucaoCivilCodigoObra: TWideStringField;
    QrSrvTomInnCabDpsConstrucaoCivilArt: TWideStringField;
    QrSrvTomInnCabDpsRegimeEspecialTributacao: TSmallintField;
    QrSrvTomInnCabDpsOptanteSimplesNacional: TSmallintField;
    QrSrvTomInnCabDpsIncentivoFiscal: TSmallintField;
    QrSrvTomInnCabDpsInfID_ID: TWideStringField;
    QrSrvTomInnCabLk: TIntegerField;
    QrSrvTomInnCabDataCad: TDateField;
    QrSrvTomInnCabDataAlt: TDateField;
    QrSrvTomInnCabUserCad: TIntegerField;
    QrSrvTomInnCabUserAlt: TIntegerField;
    QrSrvTomInnCabAlterWeb: TSmallintField;
    QrSrvTomInnCabAWServerID: TIntegerField;
    QrSrvTomInnCabAWStatSinc: TSmallintField;
    QrSrvTomInnCabAtivo: TSmallintField;
    QrEmissS: TMySQLQuery;
    QrEmissSData: TDateField;
    QrEmissSTipo: TSmallintField;
    QrEmissSCarteira: TIntegerField;
    QrEmissSAutorizacao: TIntegerField;
    QrEmissSGenero: TIntegerField;
    QrEmissSDescricao: TWideStringField;
    QrEmissSNotaFiscal: TIntegerField;
    QrEmissSDebito: TFloatField;
    QrEmissSCredito: TFloatField;
    QrEmissSCompensado: TDateField;
    QrEmissSDocumento: TFloatField;
    QrEmissSSit: TIntegerField;
    QrEmissSVencimento: TDateField;
    QrEmissSLk: TIntegerField;
    QrEmissSFatID: TIntegerField;
    QrEmissSFatParcela: TIntegerField;
    QrEmissSNOMESIT: TWideStringField;
    QrEmissSNOMETIPO: TWideStringField;
    QrEmissSNOMECARTEIRA: TWideStringField;
    QrEmissSID_Sub: TSmallintField;
    QrEmissSSub: TIntegerField;
    QrEmissSFatura: TWideStringField;
    QrEmissSBanco: TIntegerField;
    QrEmissSLocal: TIntegerField;
    QrEmissSCartao: TIntegerField;
    QrEmissSLinha: TIntegerField;
    QrEmissSPago: TFloatField;
    QrEmissSMez: TIntegerField;
    QrEmissSFornecedor: TIntegerField;
    QrEmissSCliente: TIntegerField;
    QrEmissSControle: TIntegerField;
    QrEmissSID_Pgto: TIntegerField;
    QrEmissSCliInt: TIntegerField;
    QrEmissSQtde: TFloatField;
    QrEmissSFatID_Sub: TIntegerField;
    QrEmissSOperCount: TIntegerField;
    QrEmissSLancto: TIntegerField;
    QrEmissSForneceI: TIntegerField;
    QrEmissSMoraDia: TFloatField;
    QrEmissSMulta: TFloatField;
    QrEmissSProtesto: TDateField;
    QrEmissSDataDoc: TDateField;
    QrEmissSCtrlIni: TIntegerField;
    QrEmissSNivel: TIntegerField;
    QrEmissSVendedor: TIntegerField;
    QrEmissSAccount: TIntegerField;
    QrEmissSICMS_P: TFloatField;
    QrEmissSICMS_V: TFloatField;
    QrEmissSDuplicata: TWideStringField;
    QrEmissSDepto: TIntegerField;
    QrEmissSDescoPor: TIntegerField;
    QrEmissSDataCad: TDateField;
    QrEmissSDataAlt: TDateField;
    QrEmissSUserCad: TIntegerField;
    QrEmissSUserAlt: TIntegerField;
    QrEmissSEmitente: TWideStringField;
    QrEmissSContaCorrente: TWideStringField;
    QrEmissSCNPJCPF: TWideStringField;
    QrEmissSFatNum: TFloatField;
    QrEmissSAgencia: TIntegerField;
    QrEmissSSerieNF: TWideStringField;
    QrEmissSGenCtb: TIntegerField;
    QrEmissSGenCtbD: TIntegerField;
    QrEmissSGenCtbC: TIntegerField;
    DsEmissS: TDataSource;
    QrSumS: TMySQLQuery;
    QrSumSDebito: TFloatField;
    QrSrvTomInnCabFilial: TIntegerField;
    QrSrvTomInnCabNO_EMPRESA: TWideStringField;
    QrSrvTomInnCabDpsResponsavelRetencao: TIntegerField;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Label3: TLabel;
    Label21: TLabel;
    Label4: TLabel;
    Label31: TLabel;
    Label16: TLabel;
    Panel11: TPanel;
    Panel6: TPanel;
    Panel8: TPanel;
    Label17: TLabel;
    SbIntermediario: TSpeedButton;
    Label34: TLabel;
    Label18: TLabel;
    Label36: TLabel;
    Label19: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label42: TLabel;
    Panel9: TPanel;
    dmkLabelRotate1: TdmkLabelRotate;
    Panel15: TPanel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdNome: TdmkDBEdit;
    Panel16: TPanel;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label1: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label8: TLabel;
    Label40: TLabel;
    Panel17: TPanel;
    GroupBox5: TGroupBox;
    Label35: TLabel;
    Label23: TLabel;
    QrSrvTomInnCabNO_PRESTADOR: TWideStringField;
    QrSrvTomInnCabNO_INTERMEDIARIO: TWideStringField;
    QrSrvTomInnCabNO_DpsItemListaServico: TWideStringField;
    QrSrvTomInnCabNO_cnae21Cad: TWideStringField;
    QrSrvTomInnCabNO_NfsRpsIDTipo: TWideStringField;
    QrSrvTomInnCabNO_DpsIssRetido: TWideStringField;
    QrSrvTomInnCabNO_DpsResponsavelRetencao: TWideStringField;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBMemo1: TDBMemo;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    GroupBox6: TGroupBox;
    GroupBox8: TGroupBox;
    GridF: TDBGrid;
    QrSrvTomInnCabTIFEM_0: TIntegerField;
    QrSrvTomInnCabTIFEM_1: TIntegerField;
    QrSrvTomInnCabTIFEM_2: TIntegerField;
    QrSrvTomInnCabTIFEM_3: TIntegerField;
    QrSrvTomInnCabTIFEM_4: TIntegerField;
    QrSrvTomInnCabTIFEM_5: TIntegerField;
    QrSrvTomInnCabTIFEM_6: TIntegerField;
    QrSrvTomInnCabTIFEM_7: TIntegerField;
    QrSrvTomInnCabRetValorPis: TFloatField;
    QrSrvTomInnCabRetValorCofins: TFloatField;
    QrSrvTomInnCabRetValorInss: TFloatField;
    QrSrvTomInnCabRetValorIr: TFloatField;
    QrSrvTomInnCabRetValorCsll: TFloatField;
    QrSrvTomInnCabTIFEM_8: TIntegerField;
    QrSrvTomInnCabRetValorISS: TFloatField;
    QrSrvTomInnCabRetValorTotal: TFloatField;
    QrSrvTomInnCabRetOutrasRetencoes: TFloatField;
    QrSrvTomInnCabValorLiqSemRet: TFloatField;
    Panel10: TPanel;
    GroupBox4: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label41: TLabel;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    Label2: TLabel;
    DBEdit37: TDBEdit;
    Label20: TLabel;
    Label22: TLabel;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSrvTomInnCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSrvTomInnCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrSrvTomInnCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrSrvTomInnCabBeforeClose(DataSet: TDataSet);
  private
    FTabLctA: String;
    FEmpresa: Integer;
    //FTravando: Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //procedure MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);
    procedure MostraFormSrvTomInnCad(SQLType: TSQLType);
    procedure IncluiLanctoS(Sender: TObject);
    procedure ExcluiLanctoS();
    procedure DefineVarDup();
    function  CalculaDiferencas(): Double;

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenEmissS(FatParcela: Integer);

  end;

var
  FmSrvTomInnCab: TFmSrvTomInnCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnFinanceiro, UnEmpresas,
 ModuleGeral, SrvTomInnCad, UnSrvTom_PF, UnPagtos;

{$R *.DFM}

const
  FThis_FATID = VAR_FATID_1030;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSrvTomInnCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSrvTomInnCab.MostraFormSrvTomInnCad(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  Codigo := QrSrvTomInnCabCodigo.Value;
  //
  if DBCheck.CriaFm(TFmSrvTomInnCad, FmSrvTomInnCad, afmoNegarComAviso) then
  begin
    FmSrvTomInnCad.ImgTipo.SQLType := SQLType;
    if SQLType = stUpd then
    begin
      with FmSrvTomInnCad do
      begin
        EdCodigo.ValueVariant            := QrSrvTomInnCabCodigo.Value;
        EdNome.ValueVariant              := QrSrvTomInnCabNome.Value;
        EdEmpresa.ValueVariant           := QrSrvTomInnCabFilial.Value;
        CBEmpresa.KeyValue               := QrSrvTomInnCabFilial.Value;

        EdIntermediario.ValueVariant               := QrSrvTomInnCabIntermediario.Value;
        CBIntermediario.KeyValue                   := QrSrvTomInnCabIntermediario.Value;
        EdPrestador.ValueVariant                   := QrSrvTomInnCabPrestador.Value;
        CBPrestador.KeyValue                       := QrSrvTomInnCabPrestador.Value;
        EdNfsCodigoVerificacao.ValueVariant        := QrSrvTomInnCabNfsCodigoVerificacao.Value;
        EdNfsOutrasInformacoes.ValueVariant        := QrSrvTomInnCabNfsOutrasInformacoes.Value;
        //EdDpsRpsIDSerie.ValueVariant         := QrSrvTomInnCabpsRpsIDSerie
        //?????????????                          := QrSrvTomInnCabDpsRpsIDNumero
        EdItemListaServico.ValueVariant            := QrSrvTomInnCabDpsItemListaServico.Value;
        CBItemListaServico.KeyValue                := QrSrvTomInnCabDpsItemListaServico.Value;
        EdCodigoCnae.ValueVariant                  := QrSrvTomInnCabDpsCodigoCnae.Value;
        CBCodigoCnae.KeyValue                      := QrSrvTomInnCabDpsCodigoCnae.Value;
        EdCodigoTributacaoMunicipio.ValueVariant   := QrSrvTomInnCabDpsCodigoTributacaoMunicipio.Value;
        MeDiscriminacao.Text                       := QrSrvTomInnCabDpsDiscriminacao.Value;
        EdNumeroProcesso     .ValueVariant         := QrSrvTomInnCabDpsNumeroProcesso.Value;
        EdConstrucaoCivilCodigoObra.Text           := QrSrvTomInnCabDpsConstrucaoCivilCodigoObra.Value;
        EdConstrucaoCivilArt.Text                  := QrSrvTomInnCabDpsConstrucaoCivilArt.Value;
        // ??????????                           := QrSrvTomInnCabDpsInfID_ID

        //Integer(tsStatusRps_Normal)                := QrSrvTomInnCabStatus.Value;
        EdRpsIDTipo.ValueVariant                   := QrSrvTomInnCabNfsRpsIDTipo.Value;
        EdNfsNumero.ValueVariant                   := QrSrvTomInnCabNfsNumero.Value;
        EdRpsIDSerie.ValueVariant                  := QrSrvTomInnCabNfsRpsIDSerie.Value;
        EdRpsIDNumero.ValueVariant                 := QrSrvTomInnCabNfsRpsIDNumero.Value;
        EdIssRetido.ValueVariant                   := QrSrvTomInnCabDpsIssRetido.Value;
        EdResponsavelRetencao.ValueVariant         := QrSrvTomInnCabDpsResponsavelRetencao.Value;
        CBResponsavelRetencao.KeyValue             := QrSrvTomInnCabDpsResponsavelRetencao.Value;
        //EdCodigoMunicipio.ValueVariant             :QrSrvTomInnCab//DpsCodigoMunicipio
        //EdCodigoPais         .ValueVariant         :QrSrvTomInnCab//DpsCodigoPais
        EdExigibilidadeISS   .ValueVariant         := QrSrvTomInnCabDpsExigibilidadeISS.Value;
        CBExigibilidadeISS   .KeyValue             := QrSrvTomInnCabDpsExigibilidadeISS.Value;
        //QuemPagaISS.ValueVariant                  = EQrSrvTomInnCab//QuemPagaISS               :
        //-1                                         := QrSrvTomInnCabDpsNaturezaOperacao.Value;
        //-1                                         := QrSrvTomInnCabDpsMunicipioIncidencia.Value;
        EdRegimeEspecialTributacao.ValueVariant    := QrSrvTomInnCabDpsRegimeEspecialTributacao.Value;
        CBRegimeEspecialTributacao.KeyValue        := QrSrvTomInnCabDpsRegimeEspecialTributacao.Value;
        EdOptanteSimplesNacional.ValueVariant      := QrSrvTomInnCabDpsOptanteSimplesNacional.Value;
        EdIncentivoFiscal.ValueVariant             := QrSrvTomInnCabDpsIncentivoFiscal.Value;
        //                                              QrSrvTomInnCab
        //
        TPRpsDataEmissao.Date                      := QrSrvTomInnCabNfsDataEmissao.Value;
        EdRpsHoraEmissao.ValueVariant              := QrSrvTomInnCabNfsDataEmissao.Value;
        //                                              QrSrvTomInnCab //RpsID          :=
        //= TPCompetencia.Date                         :QrSrvTomInnCab//DpsCompetencia
        //= EdSubstNumero.ValueVariant                 :QrSrvTomInnCab//DpsSubstNumero
        //= EdSubstSerie.Text                          :QrSrvTomInnCab//DpsSubstSerie
        //= EdSubstTipo.ValueVariant                   :QrSrvTomInnCab//DpsSubstTipo
        EdValorServicos.ValueVariant               := QrSrvTomInnCabDpsValorServicos.Value;
        EdValorDeducoes.ValueVariant               := QrSrvTomInnCabDpsValorDeducoes.Value;
(*
        EdValorPis.ValueVariant                    := QrSrvTomInnCabDpsValorPis.Value;
        EdValorCofins.ValueVariant                 := QrSrvTomInnCabDpsValorCofins.Value;
        EdValorInss.ValueVariant                   := QrSrvTomInnCabDpsValorInss.Value;
        EdValorIr.ValueVariant                     := QrSrvTomInnCabDpsValorIr.Value;
        EdValorCsll.ValueVariant                   := QrSrvTomInnCabDpsValorCsll.Value;
        EdOutrasRetencoes.ValueVariant             := QrSrvTomInnCabDpsOutrasRetencoes.Value;
*)
        //                                         := QrSrvTomInnCabNfsNfseSubstituida.Value;
                                       //
        EdRetValorIss.ValueVariant                 := QrSrvTomInnCabRetValorIss.Value;
        EdRetValorPis.ValueVariant                 := QrSrvTomInnCabRetValorPis.Value;
        EdRetValorCofins.ValueVariant              := QrSrvTomInnCabRetValorCofins.Value;
        EdRetValorInss.ValueVariant                := QrSrvTomInnCabRetValorInss.Value;
        EdRetValorIr.ValueVariant                  := QrSrvTomInnCabRetValorIr.Value;
        EdRetValorCsll.ValueVariant                := QrSrvTomInnCabRetValorCsll.Value;
        EdRetOutrasRetencoes.ValueVariant          := QrSrvTomInnCabRetOutrasRetencoes.Value;
        //
        //tifemIndef=0, tifemPIS=1, tifemCOFINS=2, tifemIPI=3, tifemIR=4, tifemCSLL=5, tifemINSS=6, tifemISSQN=7, tifemICMSS=8);
        EdRetOutrasRetencoes.Enabled                 := QrSrvTomInnCabTIFEM_0.Value = 0;
        EdRetValorPis.Enabled                        := QrSrvTomInnCabTIFEM_1.Value = 0;
        EdRetValorCofins.Enabled                     := QrSrvTomInnCabTIFEM_2.Value = 0;
        //RetEdValorIPI.Enabled                        := QrSrvTomInnCabTIFEM_3.Value = 0;
        EdRetValorIr.Enabled                         := QrSrvTomInnCabTIFEM_4.Value = 0;
        EdRetValorCsll.Enabled                       := QrSrvTomInnCabTIFEM_5.Value = 0;
        EdRetValorInss.Enabled                       := QrSrvTomInnCabTIFEM_6.Value = 0;
        EdRetValorISS.Enabled                        := QrSrvTomInnCabTIFEM_7.Value = 0;
        //RetEdValorICMS.Enabled                       := QrSrvTomInnCabTIFEM_8.Value = 0;
        //
        //
        EdNfsBaseCalculo.ValueVariant              := QrSrvTomInnCabNfsBaseCalculo.Value;
        EdAliquota.ValueVariant                    := QrSrvTomInnCabNfsAliquota.Value;
        EdValorIss.ValueVariant                    := QrSrvTomInnCabNfsValorIss.Value;
        //00                                       := QrSrvTomInnCabNfsValorCredito.Value;
        EdDescontoIncondicionado.ValueVariant      := QrSrvTomInnCabDpsDescontoIncondicionado.Value;
        EdDescontoCondicionado.ValueVariant        := QrSrvTomInnCabDpsDescontoCondicionado.Value;
        // devem ser por �ltimo!
        EdValorLiqSemRet.ValueVariant              := QrSrvTomInnCabValorLiqSemRet.Value;
        EdRetValorTotal.ValueVariant               := QrSrvTomInnCabRetValorTotal.Value;
        EdNfsValorLiquidoNfse.ValueVariant         := QrSrvTomInnCabNfsValorLiquidoNfse.Value;
      end;
    end;
    FmSrvTomInnCad.ShowModal;
    FmSrvTomInnCad.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
end;

{
procedure TFmSrvTomInnCab.MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmCadastro_Com_Itens_ITS, FmCadastro_Com_Itens_ITS, afmoNegarComAviso) then
  begin
    FmCadastro_Com_Itens_ITS.ImgTipo.SQLType := SQLType;
    FmCadastro_Com_Itens_ITS.FQrCab := QrSrvTomInnCab;
    FmCadastro_Com_Itens_ITS.FDsCab := DsSrvTomInnCab;
    FmCadastro_Com_Itens_ITS.FQrIts := QrCadastro_Com_Itens_ITS;
    FmCadastro_Com_Itens_ITS.FCodigo := QrSrvTomInnCabCodigo.Value;
    if SQLType = stIns then
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := False
    else
    begin
      FmCadastro_Com_Itens_ITS.EdControle.ValueVariant := QrCadastro_Com_Itens_ITSControle.Value;
      //
      FmCadastro_Com_Itens_ITS.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrCadastro_Com_Itens_ITSCNPJ_CPF.Value);
      FmCadastro_Com_Itens_ITS.EdNomeEmiSac.Text := QrCadastro_Com_Itens_ITSNome.Value;
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := True;
    end;
    FmCadastro_Com_Itens_ITS.ShowModal;
    FmCadastro_Com_Itens_ITS.Destroy;
  end;
end;
}

procedure TFmSrvTomInnCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrSrvTomInnCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrSrvTomInnCab, QrEmissS);
end;

procedure TFmSrvTomInnCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrSrvTomInnCab);
(*
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrCadastro_Com_Itens_ITS);
*)
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrEmissS);
end;

procedure TFmSrvTomInnCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSrvTomInnCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSrvTomInnCab.DefParams;
begin
  VAR_GOTOTABELA := 'srvtominncab';
  VAR_GOTOMYSQLTABLE := QrSrvTomInnCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT sic.*, emp.Filial,');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RAZAOSOCIAL, emp.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(prs.Tipo=0, prs.RAZAOSOCIAL, prs.Nome) NO_PRESTADOR,');
  VAR_SQLx.Add('IF(itm.Tipo=0, itm.RAZAOSOCIAL, itm.Nome) NO_INTERMEDIARIO,');
  VAR_SQLx.Add('lsr.Nome NO_DpsItemListaServico, cna.Nome NO_cnae21Cad,');
  VAR_SQLx.Add('ts1.Nome NO_NfsRpsIDTipo, ts3.Nome NO_DpsIssRetido,');
  VAR_SQLx.Add('tsr.Nome NO_DpsResponsavelRetencao');
  VAR_SQLx.Add('FROM srvtominncab sic');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=sic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades prs ON prs.Codigo=sic.Prestador');
  VAR_SQLx.Add('LEFT JOIN entidades itm ON itm.Codigo=sic.Intermediario');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.listserv  lsr ON lsr.Codigo=sic.DpsItemListaServico');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.cnae21Cad cna ON cna.CodAlf=sic.DpsCodigoCnae');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.tstiporps ts1 ON ts1.Codigo=sic.NfsRpsIDTipo');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.tssimnao  ts3 ON ts3.Codigo=sic.DpsIssRetido');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.tsresponsavelretencao tsr ON tsr.Codigo=sic.DpsResponsavelRetencao');
  VAR_SQLx.Add('WHERE sic.Codigo > 0');
  //
  VAR_SQL1.Add('AND sic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sic.Nome Like :P0');
  //
end;

procedure TFmSrvTomInnCab.ExcluiLanctoS();
begin
  if UFinanceiro.ExcluiLct_FatParcela(QrEmissS, QrEmissSFatID.Value,
    QrEmissSFatNum.Value, QrEmissSFatParcela.Value, QrEmissSCarteira.Value,
    QrEmissSSit.Value, QrEmissSTipo.Value, dmkPF.MotivDel_ValidaCodigo(330),
    FTabLctA)
  then
    ReopenEmissS(QrEmissSFatParcela.Value);
end;

procedure TFmSrvTomInnCab.IncluiLanctoS(Sender: TObject);
const
  Qtde = 0.000;
  Qtd2 = 0.000;
var
  Terceiro, Cod, Genero, Empresa, NF: Integer;
  SerieNF: String;
  Valor: Double;
  GenCtbD, GenCtbC: Integer;
  TypCtbCadMoF: TTypCtbCadMoF;
  Descricao: String;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
(* ini 2022-03-23
  if FmPrincipal.AvisaFaltaDeContaDoPlano(FThis_FATID) then
    Exit;
*)
  //
  DefineVarDup;
  //
  Genero   := Dmod.QrControleCtaPQCompr.Value;
  Cod      := QrSrvTomInnCabCodigo.Value;
  Terceiro := QrSrvTomInnCabPrestador.Value;
  Valor    := CalculaDiferencas();
  Empresa  := QrSrvTomInnCabEmpresa.Value;
  SerieNF  := '';
  NF       := QrSrvTomInnCabNfsNumero.Value;
  //
  if Valor = 0 then
    Valor := QrSrvTomInnCabNfsValorLiquidoNfse.Value;
  //

  GenCtbD := 0;
  GenCtbC := 0;
  //
  SrvTom_PF.DefineContasContabeisServicoTomado(GenCtbD, GenCtbC);
  //
  Descricao := QrSrvTomInnCabNome.Value;
  if Trim(Descricao) = EmptyStr then
    Descricao := 'Servi�o tomado';
  UPagtos.Pagto(QrEmissS, tpDeb, Cod, Terceiro, FThis_FATID, 0(*GenCtbD*), 0(*GenCtbC*), stIns,
    'Servi�o Tomado', Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0,
    0, True, False, 0, 0, 0, 0, NF, FTabLctA, Descricao, SerieNF, Qtde, Qtd2, GenCtbD, GenCtbC);
// fim 2022-03-23
  ReopenEmissS(0);
end;

procedure TFmSrvTomInnCab.ItsAltera1Click(Sender: TObject);
begin
//  MostraCadastro_Com_Itens_ITS(stUpd);
end;

procedure TFmSrvTomInnCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmSrvTomInnCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSrvTomInnCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSrvTomInnCab.ReopenEmissS(FatParcela: Integer);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissS, Dmod.MyDB, [
    'SELECT la.*, ca.Nome NOMECARTEIRA ',
    'FROM ' + FTabLctA + ' la  ',
    'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
    'WHERE la.FatID=' + Geral.FF0(FThis_FATID),
    'AND la.FatNum=' + Geral.FF0(QrSrvTomInnCabCodigo.Value),
    'AND la.ID_Pgto = 0 ',
    'ORDER BY FatParcela ',
    '']);
end;

procedure TFmSrvTomInnCab.ItsExclui1Click(Sender: TObject);
begin
  ExcluiLanctoS();
end;

procedure TFmSrvTomInnCab.DefineONomeDoForm;
begin
end;

procedure TFmSrvTomInnCab.DefineVarDup();
begin
  IC3_ED_FatNum := QrSrvTomInnCabCodigo.Value;
  IC3_ED_NF     := QrSrvTomInnCabNfsNumero.Value;
  IC3_ED_Data   := Int(QrSrvTomInnCabNfsDataEmissao.Value);
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSrvTomInnCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSrvTomInnCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSrvTomInnCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSrvTomInnCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSrvTomInnCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvTomInnCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSrvTomInnCabCodigo.Value;
  Close;
end;

procedure TFmSrvTomInnCab.ItsInclui1Click(Sender: TObject);
begin
  IncluiLanctoS(Sender);
end;

procedure TFmSrvTomInnCab.CabAltera1Click(Sender: TObject);
begin
  MostraFormSrvTomInnCad(stUpd);
end;

procedure TFmSrvTomInnCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'srvtominncab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'srvtominncab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmSrvTomInnCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmSrvTomInnCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmSrvTomInnCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  UEmpresas.ForcaDefinicaodeEmpresa(FEmpresa, (*HaltIfIndef*) True);
  //FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, DModG.QrFiliLogFilial.Value);
end;

procedure TFmSrvTomInnCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSrvTomInnCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvTomInnCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSrvTomInnCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrSrvTomInnCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvTomInnCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSrvTomInnCab.QrSrvTomInnCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSrvTomInnCab.QrSrvTomInnCabAfterScroll(DataSet: TDataSet);
begin
  if QrSrvTomInnCabEmpresa.Value <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrSrvTomInnCabFilial.Value)
  else
    FTabLctA := sTabLctErr;
  ReopenEmissS(0);
end;

procedure TFmSrvTomInnCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrSrvTomInnCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmSrvTomInnCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSrvTomInnCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'srvtominncab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSrvTomInnCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvTomInnCab.CabInclui1Click(Sender: TObject);
begin
(*
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrSrvTomInnCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'srvtominncab');
*)
  MostraFormSrvTomInnCad(stIns);
end;

function TFmSrvTomInnCab.CalculaDiferencas(): Double;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  Result := 0.00;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumS, Dmod.MyDB, [
      'SELECT SUM(Debito) Debito ',
      'FROM ' + FTabLctA,
      'WHERE FatID=' + Geral.FF0(FThis_FATID),
      'AND FatNum=' + Geral.FF0(QrSrvTomInnCabCodigo.Value),
      'AND ID_Pgto = 0 ',
      '']);
    //
    if QrSumSDebito.Value <> 0 then
      Result := QrSumSDebito.Value - QrSrvTomInnCabNfsValorLiquidoNfse.Value;
  end;
end;

procedure TFmSrvTomInnCab.QrSrvTomInnCabBeforeClose(
  DataSet: TDataSet);
begin
//  QrCadastro_Com_Itens_ITS.Close;
end;

procedure TFmSrvTomInnCab.QrSrvTomInnCabBeforeOpen(DataSet: TDataSet);
begin
  QrSrvTomInnCabCodigo.DisplayFormat := FFormatFloat;
end;

(*
object GroupBox2: TGroupBox
  Left = 6
  Top = 4
  Width = 411
  Height = 60
  Caption = ' Recibo Provis'#243'rio de Servi'#231'o (RPS) Substitu'#237'do: '
  Enabled = False
  TabOrder = 0
  object Label2: TLabel
    Left = 8
    Top = 16
    Width = 40
    Height = 13
    Caption = 'N'#250'mero:'
    Enabled = False
  end
  object Label20: TLabel
    Left = 106
    Top = 16
    Width = 27
    Height = 13
    Caption = 'S'#233'rie:'
    Enabled = False
  end
  object Label22: TLabel
    Left = 170
    Top = 16
    Width = 64
    Height = 13
    Caption = 'Tipo de RPS:'
    Enabled = False
  end
  object dmkEdit1: TdmkEdit
    Left = 8
    Top = 32
    Width = 96
    Height = 21
    Enabled = False
    TabOrder = 0
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    QryCampo = 'NumOC'
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
    ValWarn = False
  end
  object EdSubstSerie: TdmkEdit
    Left = 106
    Top = 32
    Width = 64
    Height = 21
    Enabled = False
    TabOrder = 1
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    QryCampo = 'NumOC'
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
    ValWarn = False
  end
  object dmkEdit2: TdmkEdit
    Left = 172
    Top = 32
    Width = 223
    Height = 21
    Enabled = False
    TabOrder = 2
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    QryCampo = 'NumOC'
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
    ValWarn = False
  end
end
*)
end.

