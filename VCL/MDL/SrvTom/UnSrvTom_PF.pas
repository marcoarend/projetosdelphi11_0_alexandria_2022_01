unit UnSrvTom_PF;
interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, UnMsgInt, Db,
  DbCtrls, Buttons, ZCF2, mySQLDbTables, ComCtrls, Grids, DBGrids, CommCtrl,
  Consts, UnDmkProcFunc, Variants, MaskUtils, frxClass, frxPreview,
  mySQLExceptions,
  // Dermatek
  dmkGeral, dmkEdit, dmkDBEdit, dmkEditF7, dmkEditDateTimePicker, dmkCheckGroup,
  dmkDBLookupCombobox, dmkEditCB, dmkDBGrid, dmkDBGridDAC, dmkImage, UnDmkEnums;

type
  TUnSrvTom_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AtiualizaRetValorTotal(Codigo, TIFEM: Integer);
    procedure DefineContasContabeisServicoTomado(var GenCtbD, GenCtbC: Integer);
  end;

var
  SrvTom_PF: TUnSrvTom_PF;

const
      //TTipImpostoFddEstMun = (
  CO_TXT_tifemIndef   = 'N/D';
  CO_TXT_tifemPIS     = 'PIS';
  CO_TXT_tifemCOFINS  = 'COFINS';
  CO_TXT_tifemIPI     = 'IPI';
  CO_TXT_tifemIR      = 'IR';
  CO_TXT_tifemCSLL    = 'CSLL';
  CO_TXT_tifemINSS    = 'INSS';
  CO_TXT_tifemISSQN   = 'ISSQN';
  CO_TXT_tifemICMS    = 'ICMS';
  MaxTipImpostoFddEstMun = Integer(High(TTipImpostoFddEstMun));
  sTipImpostoFddEstMun: array[0..MaxTipImpostoFddEstMun] of string = (
  CO_TXT_tifemIndef   , // =0,
  CO_TXT_tifemPIS     , // =1,
  CO_TXT_tifemCOFINS  , // =2,
  CO_TXT_tifemIPI     , // =3,
  CO_TXT_tifemIR      , // =4,
  CO_TXT_tifemCSLL    , // =5,
  CO_TXT_tifemINSS    , // =6,
  CO_TXT_tifemISSQN   , // =7,
  CO_TXT_tifemICMS      // =8
  );
  //
  CO_NomeFld_tifemIndef   = 'RetOutrasRetencoes';
  CO_NomeFld_tifemPIS     = 'RetValorPis';
  CO_NomeFld_tifemCOFINS  = 'RetValorCofins';
  CO_NomeFld_tifemIPI     = 'RetValorIPI';
  CO_NomeFld_tifemIR      = 'RetValorIr';
  CO_NomeFld_tifemCSLL    = 'RetValorCsll';
  CO_NomeFld_tifemINSS    = 'RetValorInss';
  CO_NomeFld_tifemISSQN   = 'RetValorIss';
  CO_NomeFld_tifemICMS    = 'RetValorICMS';
  MaxNomFldsImpostoFddEstMun = Integer(High(TTipImpostoFddEstMun));
  sNomFldsImpostoFddEstMun: array[0..MaxNomFldsImpostoFddEstMun] of string = (
  CO_NomeFld_tifemIndef   , // =0,
  CO_NomeFld_tifemPIS     , // =1,
  CO_NomeFld_tifemCOFINS  , // =2,
  CO_NomeFld_tifemIPI     , // =3,
  CO_NomeFld_tifemIR      , // =4,
  CO_NomeFld_tifemCSLL    , // =5,
  CO_NomeFld_tifemINSS    , // =6,
  CO_NomeFld_tifemISSQN   , // =7,
  CO_NomeFld_tifemICMS      // =8
  );
  //

implementation

uses MyDBCheck, Module, DmkDAC_PF, UMySQLModule, UnMyObjects, ModuleFin,
  UnFinanceiro;


{ TUnSrvTom_PF }

procedure TUnSrvTom_PF.AtiualizaRetValorTotal(Codigo, TIFEM: Integer);
const
  sProcName = 'TUnSrvTom_PF.AtiualizaRetValorTotal()';
var
  CampoVal: String;
  RetValorTotal: Double;
begin
  if TIFEM >= MaxNomFldsImpostoFddEstMun then
  begin
    Geral.MB_Erro('"TIFEM" n�o habilitado: ID = ' + Geral.FF0(TIFEM) +
    ' em ' + sProcName);
  end else
  begin
    CampoVal := sNomFldsImpostoFddEstMun[TIFEM];
    UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrAux, Dmod.MyDB, [
    'SELECT SUM(sic.' + CampoVal + ') RetValorTotal ',
    'FROM srvtominncab sic ',
    'LEFT JOIN srvtomretits sri ON sri.SrvTomInnCab=sic.Codigo ',
    'WHERE sri.Codigo=' + Geral.FF0(Codigo),
    '']);
    //
    RetValorTotal := DModFin.QrAux.FieldByName('RetValorTotal').AsFloat;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvtomretcab', False, [
    'RetValorTotal'], ['Codigo'], [RetValorTotal], [Codigo], True);
  end;
end;

procedure TUnSrvTom_PF.DefineContasContabeisServicoTomado( var GenCtbD, GenCtbC:
  Integer);
begin
  //tccmfServicoTomado
  GenCtbD := 0;
  GenCtbC := 0;
end;

end.
