object FmSrvTomInnCab: TFmSrvTomInnCab
  Left = 368
  Top = 194
  Caption = 'SRV-TOMAD-001 :: Servi'#231'o Tomado'
  ClientHeight = 651
  ClientWidth = 994
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 994
    Height = 555
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 994
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 492
      Width = 994
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 854
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 994
    Height = 555
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 491
      Width = 994
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 297
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 471
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&NFSe'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lct Fin.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 994
      Height = 349
      Align = alTop
      TabOrder = 1
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 990
        Height = 50
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label3: TLabel
          Left = 8
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label21: TLabel
          Left = 594
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Emiss'#227'o:'
        end
        object Label4: TLabel
          Left = 803
          Top = 4
          Width = 65
          Height = 13
          Caption = 'Compet'#234'ncia:'
          Enabled = False
        end
        object Label31: TLabel
          Left = 8
          Top = 26
          Width = 44
          Height = 13
          Caption = 'Hist'#243'rico:'
        end
        object Label16: TLabel
          Left = 883
          Top = 29
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 928
          Top = 25
          Width = 55
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsSrvTomInnCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 5
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdit2: TDBEdit
          Left = 92
          Top = 0
          Width = 497
          Height = 21
          DataField = 'NO_EMPRESA'
          DataSource = DsSrvTomInnCab
          TabOrder = 1
        end
        object DBEdNome: TdmkDBEdit
          Left = 60
          Top = 25
          Width = 817
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsSrvTomInnCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit13: TDBEdit
          Left = 872
          Top = 0
          Width = 113
          Height = 21
          TabOrder = 3
        end
        object DBEdit1: TDBEdit
          Left = 58
          Top = 0
          Width = 31
          Height = 21
          DataField = 'Filial'
          DataSource = DsSrvTomInnCab
          TabOrder = 0
        end
        object DBEdit12: TDBEdit
          Left = 640
          Top = 0
          Width = 157
          Height = 21
          DataField = 'NfsDataEmissao'
          DataSource = DsSrvTomInnCab
          TabOrder = 2
        end
      end
      object Panel11: TPanel
        Left = 2
        Top = 273
        Width = 990
        Height = 0
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
      end
      object Panel6: TPanel
        Left = 2
        Top = 65
        Width = 990
        Height = 142
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 564
          Height = 142
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label17: TLabel
            Left = 8
            Top = 52
            Width = 63
            Height = 13
            Caption = 'Intermedi'#225'rio:'
          end
          object SbIntermediario: TSpeedButton
            Left = 539
            Top = 48
            Width = 21
            Height = 21
            Caption = '...'
          end
          object Label34: TLabel
            Left = 8
            Top = 27
            Width = 100
            Height = 13
            Caption = 'Prestador do servi'#231'o:'
          end
          object Label18: TLabel
            Left = 8
            Top = 75
            Width = 99
            Height = 13
            Caption = 'Servi'#231'o (LC 116/03):'
          end
          object Label36: TLabel
            Left = 8
            Top = 98
            Width = 83
            Height = 13
            Caption = 'C'#243'digo do CNAE:'
          end
          object Label19: TLabel
            Left = 8
            Top = 5
            Width = 70
            Height = 13
            Caption = 'N'#250'mero NFSe:'
          end
          object Label28: TLabel
            Left = 8
            Top = 122
            Width = 64
            Height = 13
            Caption = 'Tipo de RPS:'
          end
          object Label29: TLabel
            Left = 323
            Top = 122
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
          end
          object Label30: TLabel
            Left = 469
            Top = 122
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object Label42: TLabel
            Left = 212
            Top = 5
            Width = 109
            Height = 13
            Caption = 'C'#243'digo de verifica'#231#227'o: '
          end
          object DBEdit3: TDBEdit
            Left = 110
            Top = 24
            Width = 56
            Height = 21
            DataField = 'Prestador'
            DataSource = DsSrvTomInnCab
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 169
            Top = 24
            Width = 391
            Height = 21
            DataField = 'NO_PRESTADOR'
            DataSource = DsSrvTomInnCab
            TabOrder = 3
          end
          object DBEdit5: TDBEdit
            Left = 110
            Top = 48
            Width = 56
            Height = 21
            DataField = 'Intermediario'
            DataSource = DsSrvTomInnCab
            TabOrder = 4
          end
          object DBEdit6: TDBEdit
            Left = 169
            Top = 48
            Width = 391
            Height = 21
            DataField = 'NO_INTERMEDIARIO'
            DataSource = DsSrvTomInnCab
            TabOrder = 5
          end
          object DBEdit7: TDBEdit
            Left = 110
            Top = 0
            Width = 96
            Height = 21
            DataField = 'NfsNumero'
            DataSource = DsSrvTomInnCab
            TabOrder = 0
          end
          object DBEdit8: TDBEdit
            Left = 500
            Top = 118
            Width = 59
            Height = 21
            DataField = 'NfsRpsIDSerie'
            DataSource = DsSrvTomInnCab
            TabOrder = 13
          end
          object DBEdit9: TDBEdit
            Left = 110
            Top = 118
            Width = 20
            Height = 21
            DataField = 'NfsRpsIDTipo'
            DataSource = DsSrvTomInnCab
            TabOrder = 10
          end
          object DBEdit10: TDBEdit
            Left = 370
            Top = 118
            Width = 97
            Height = 21
            DataField = 'NfsRpsIDNumero'
            DataSource = DsSrvTomInnCab
            TabOrder = 12
          end
          object DBEdit11: TDBEdit
            Left = 324
            Top = 0
            Width = 237
            Height = 21
            DataField = 'NfsCodigoVerificacao'
            DataSource = DsSrvTomInnCab
            TabOrder = 1
          end
          object DBEdit29: TDBEdit
            Left = 110
            Top = 72
            Width = 56
            Height = 21
            DataField = 'DpsItemListaServico'
            DataSource = DsSrvTomInnCab
            TabOrder = 6
          end
          object DBEdit30: TDBEdit
            Left = 110
            Top = 96
            Width = 56
            Height = 21
            DataField = 'DpsCodigoCnae'
            DataSource = DsSrvTomInnCab
            TabOrder = 8
          end
          object DBEdit31: TDBEdit
            Left = 168
            Top = 72
            Width = 391
            Height = 21
            DataField = 'NO_DpsItemListaServico'
            DataSource = DsSrvTomInnCab
            TabOrder = 7
          end
          object DBEdit32: TDBEdit
            Left = 168
            Top = 96
            Width = 391
            Height = 21
            DataField = 'NO_cnae21Cad'
            DataSource = DsSrvTomInnCab
            TabOrder = 9
          end
          object DBEdit33: TDBEdit
            Left = 132
            Top = 118
            Width = 185
            Height = 21
            DataField = 'NO_NfsRpsIDTipo'
            DataSource = DsSrvTomInnCab
            TabOrder = 11
          end
        end
        object Panel9: TPanel
          Left = 564
          Top = 0
          Width = 426
          Height = 142
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object dmkLabelRotate1: TdmkLabelRotate
            Left = 0
            Top = 0
            Width = 17
            Height = 142
            Angle = ag90
            Caption = 'Discrimina'#231#227'o:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Arial'
            Font.Style = []
            Align = alLeft
          end
          object DBMemo1: TDBMemo
            Left = 17
            Top = 0
            Width = 409
            Height = 142
            Align = alClient
            DataField = 'DpsDiscriminacao'
            DataSource = DsSrvTomInnCab
            TabOrder = 0
          end
        end
      end
      object Panel15: TPanel
        Left = 2
        Top = 207
        Width = 990
        Height = 66
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
        object Panel16: TPanel
          Left = 0
          Top = 0
          Width = 564
          Height = 66
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox3: TGroupBox
            Left = 4
            Top = 4
            Width = 560
            Height = 60
            Caption = ' Valor do Servi'#231'o e ISS: '
            TabOrder = 0
            object Label5: TLabel
              Left = 8
              Top = 16
              Width = 48
              Height = 13
              Caption = '$ Servi'#231'o:'
            end
            object Label6: TLabel
              Left = 258
              Top = 16
              Width = 61
              Height = 13
              Caption = '$ Dedu'#231#245'es:'
            end
            object Label1: TLabel
              Left = 419
              Top = 16
              Width = 51
              Height = 13
              Caption = '% Aliq ISS:'
            end
            object Label32: TLabel
              Left = 92
              Top = 16
              Width = 81
              Height = 13
              Caption = '$ Desco. incond:'
            end
            object Label33: TLabel
              Left = 173
              Top = 16
              Width = 81
              Height = 13
              Caption = '$ Desco. condic:'
            end
            object Label8: TLabel
              Left = 474
              Top = 16
              Width = 29
              Height = 13
              Caption = '$ ISS:'
            end
            object Label40: TLabel
              Left = 336
              Top = 16
              Width = 73
              Height = 13
              Caption = '$ Base c'#225'lculo:'
            end
            object DBEdit14: TDBEdit
              Left = 8
              Top = 31
              Width = 80
              Height = 21
              DataField = 'DpsValorServicos'
              DataSource = DsSrvTomInnCab
              TabOrder = 0
            end
            object DBEdit15: TDBEdit
              Left = 92
              Top = 31
              Width = 80
              Height = 21
              DataField = 'DpsDescontoIncondicionado'
              DataSource = DsSrvTomInnCab
              TabOrder = 1
            end
            object DBEdit16: TDBEdit
              Left = 260
              Top = 31
              Width = 75
              Height = 21
              DataField = 'DpsValorDeducoes'
              DataSource = DsSrvTomInnCab
              TabOrder = 3
            end
            object DBEdit17: TDBEdit
              Left = 336
              Top = 31
              Width = 80
              Height = 21
              DataField = 'NfsBaseCalculo'
              DataSource = DsSrvTomInnCab
              TabOrder = 4
            end
            object DBEdit18: TDBEdit
              Left = 420
              Top = 31
              Width = 52
              Height = 21
              DataField = 'NfsAliquota'
              DataSource = DsSrvTomInnCab
              TabOrder = 5
            end
            object DBEdit19: TDBEdit
              Left = 476
              Top = 31
              Width = 80
              Height = 21
              DataField = 'NfsValorIss'
              DataSource = DsSrvTomInnCab
              TabOrder = 6
            end
            object DBEdit20: TDBEdit
              Left = 176
              Top = 31
              Width = 80
              Height = 21
              DataField = 'DpsDescontoCondicionado'
              DataSource = DsSrvTomInnCab
              TabOrder = 2
            end
          end
        end
        object Panel17: TPanel
          Left = 564
          Top = 0
          Width = 426
          Height = 66
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object GroupBox5: TGroupBox
            Left = 6
            Top = 3
            Width = 411
            Height = 60
            Caption = ' Reten'#231#227'o do ISS: '
            TabOrder = 0
            object Label35: TLabel
              Left = 134
              Top = 16
              Width = 65
              Height = 13
              Caption = 'Respons'#225'vel:'
            end
            object Label23: TLabel
              Left = 8
              Top = 16
              Width = 37
              Height = 13
              Caption = 'Retido?'
            end
            object DBEdit28: TDBEdit
              Left = 134
              Top = 31
              Width = 35
              Height = 21
              DataField = 'DpsResponsavelRetencao'
              DataSource = DsSrvTomInnCab
              TabOrder = 2
            end
            object DBEdit34: TDBEdit
              Left = 32
              Top = 31
              Width = 97
              Height = 21
              DataField = 'NO_DpsIssRetido'
              DataSource = DsSrvTomInnCab
              TabOrder = 1
            end
            object DBEdit35: TDBEdit
              Left = 170
              Top = 31
              Width = 227
              Height = 21
              DataField = 'NO_DpsResponsavelRetencao'
              DataSource = DsSrvTomInnCab
              TabOrder = 3
            end
            object DBEdit36: TDBEdit
              Left = 8
              Top = 31
              Width = 23
              Height = 21
              DataField = 'DpsIssRetido'
              DataSource = DsSrvTomInnCab
              TabOrder = 0
            end
          end
        end
      end
      object Panel10: TPanel
        Left = 2
        Top = 273
        Width = 990
        Height = 74
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 4
        object GroupBox4: TGroupBox
          Left = 4
          Top = 2
          Width = 977
          Height = 60
          Caption = ' Tributos Federais:  '
          TabOrder = 0
          object Label10: TLabel
            Left = 84
            Top = 16
            Width = 29
            Height = 13
            Caption = '$ PIS:'
          end
          object Label11: TLabel
            Left = 160
            Top = 16
            Width = 51
            Height = 13
            Caption = '$ COFINS:'
          end
          object Label12: TLabel
            Left = 236
            Top = 16
            Width = 37
            Height = 13
            Caption = '$ INSS:'
          end
          object Label13: TLabel
            Left = 312
            Top = 16
            Width = 23
            Height = 13
            Caption = '$ IR:'
          end
          object Label14: TLabel
            Left = 388
            Top = 16
            Width = 38
            Height = 13
            Caption = '$ CSLL:'
          end
          object Label15: TLabel
            Left = 464
            Top = 16
            Width = 61
            Height = 13
            Caption = '$ Outras ret.:'
          end
          object Label41: TLabel
            Left = 876
            Top = 12
            Width = 76
            Height = 13
            Caption = '$ Liquido NFSe:'
          end
          object Label2: TLabel
            Left = 8
            Top = 16
            Width = 29
            Height = 13
            Caption = '$ ISS:'
          end
          object Label20: TLabel
            Left = 716
            Top = 12
            Width = 73
            Height = 13
            Caption = '$ Base c'#225'lculo:'
          end
          object Label22: TLabel
            Left = 794
            Top = 12
            Width = 64
            Height = 13
            Caption = '$ Reten'#231#245'es:'
          end
          object DBEdit21: TDBEdit
            Left = 84
            Top = 31
            Width = 72
            Height = 21
            DataField = 'RetValorPis'
            DataSource = DsSrvTomInnCab
            TabOrder = 1
          end
          object DBEdit22: TDBEdit
            Left = 160
            Top = 31
            Width = 72
            Height = 21
            DataField = 'RetValorCofins'
            DataSource = DsSrvTomInnCab
            TabOrder = 2
          end
          object DBEdit23: TDBEdit
            Left = 236
            Top = 31
            Width = 72
            Height = 21
            DataField = 'RetValorInss'
            DataSource = DsSrvTomInnCab
            TabOrder = 3
          end
          object DBEdit24: TDBEdit
            Left = 312
            Top = 31
            Width = 72
            Height = 21
            DataField = 'RetValorIr'
            DataSource = DsSrvTomInnCab
            TabOrder = 4
          end
          object DBEdit25: TDBEdit
            Left = 388
            Top = 31
            Width = 72
            Height = 21
            DataField = 'RetValorCsll'
            DataSource = DsSrvTomInnCab
            TabOrder = 5
          end
          object DBEdit26: TDBEdit
            Left = 464
            Top = 31
            Width = 72
            Height = 21
            DataField = 'RetOutrasRetencoes'
            DataSource = DsSrvTomInnCab
            TabOrder = 6
          end
          object DBEdit27: TDBEdit
            Left = 876
            Top = 27
            Width = 89
            Height = 21
            DataField = 'NfsValorLiquidoNfse'
            DataSource = DsSrvTomInnCab
            TabOrder = 9
          end
          object DBEdit37: TDBEdit
            Left = 8
            Top = 31
            Width = 72
            Height = 21
            DataField = 'RetValorISS'
            DataSource = DsSrvTomInnCab
            TabOrder = 0
          end
          object DBEdit38: TDBEdit
            Left = 716
            Top = 27
            Width = 76
            Height = 21
            DataField = 'ValorLiqSemRet'
            DataSource = DsSrvTomInnCab
            TabOrder = 7
          end
          object DBEdit39: TDBEdit
            Left = 796
            Top = 27
            Width = 76
            Height = 21
            DataField = 'RetValorTotal'
            DataSource = DsSrvTomInnCab
            TabOrder = 8
          end
        end
      end
    end
    object GroupBox6: TGroupBox
      Left = 0
      Top = 337
      Width = 994
      Height = 154
      Align = alBottom
      Caption = ' Pagamentos:'
      TabOrder = 2
      object GroupBox8: TGroupBox
        Left = 2
        Top = 15
        Width = 990
        Height = 137
        Align = alClient
        Caption = ' Fornecedor: '
        TabOrder = 0
        object GridF: TDBGrid
          Left = 2
          Top = 15
          Width = 986
          Height = 120
          Align = alClient
          Color = clWhite
          DataSource = DsEmissS
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatParcela'
              Title.Caption = 'N'#186
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              ReadOnly = True
              Title.Caption = 'Situa'#231#227'o'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMETIPO'
              Title.Caption = 'Tipo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 300
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 994
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 730
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 193
        Height = 32
        Caption = 'Servi'#231'o Tomado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 193
        Height = 32
        Caption = 'Servi'#231'o Tomado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 193
        Height = 32
        Caption = 'Servi'#231'o Tomado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 994
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 990
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrSrvTomInnCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrSrvTomInnCabBeforeOpen
    AfterOpen = QrSrvTomInnCabAfterOpen
    BeforeClose = QrSrvTomInnCabBeforeClose
    AfterScroll = QrSrvTomInnCabAfterScroll
    SQL.Strings = (
      'SELECT sic.*, emp.Filial,'
      'IF(emp.Tipo=0, emp.RAZAOSOCIAL, emp.Nome) NO_EMPRESA,'
      'IF(prs.Tipo=0, prs.RAZAOSOCIAL, prs.Nome) NO_PRESTADOR,'
      'IF(itm.Tipo=0, itm.RAZAOSOCIAL, itm.Nome) NO_INTERMEDIARIO,'
      'lsr.Nome NO_DpsItemListaServico, cna.Nome NO_cnae21Cad,'
      'ts1.Nome NO_NfsRpsIDTipo, ts3.Nome NO_DpsIssRetido,'
      'tsr.Nome NO_DpsResponsavelRetencao'
      'FROM srvtominncab sic'
      'LEFT JOIN entidades emp ON emp.Codigo=sic.Empresa'
      'LEFT JOIN entidades prs ON prs.Codigo=sic.Prestador'
      'LEFT JOIN entidades itm ON itm.Codigo=sic.Intermediario'
      
        'LEFT JOIN locbdermall.listserv  lsr ON lsr.Codigo=sic.DpsItemLis' +
        'taServico'
      
        'LEFT JOIN locbdermall.cnae21Cad cna ON cna.Codigo=sic.DpsCodigoC' +
        'nae'
      
        'LEFT JOIN locbdermall.tstiporps ts1 ON ts1.Codigo=sic.NfsRpsIDTi' +
        'po'
      
        'LEFT JOIN locbdermall.tssimnao  ts3 ON ts3.Codigo=sic.DpsIssReti' +
        'do'
      
        'LEFT JOIN tsresponsavelretencao tsr ON tsr.Codigo=sic.DpsRespons' +
        'avelRetencao'
      'WHERE sic.Codigo > 0')
    Left = 472
    Top = 5
    object QrSrvTomInnCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSrvTomInnCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrSrvTomInnCabPrestador: TIntegerField
      FieldName = 'Prestador'
      Required = True
    end
    object QrSrvTomInnCabIntermediario: TIntegerField
      FieldName = 'Intermediario'
      Required = True
    end
    object QrSrvTomInnCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSrvTomInnCabStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrSrvTomInnCabNfsNumero: TLargeintField
      FieldName = 'NfsNumero'
      Required = True
    end
    object QrSrvTomInnCabNfsRpsIDSerie: TWideStringField
      FieldName = 'NfsRpsIDSerie'
      Size = 5
    end
    object QrSrvTomInnCabNfsRpsIDTipo: TSmallintField
      FieldName = 'NfsRpsIDTipo'
      Required = True
    end
    object QrSrvTomInnCabNfsRpsIDNumero: TIntegerField
      FieldName = 'NfsRpsIDNumero'
      Required = True
    end
    object QrSrvTomInnCabNfsCodigoVerificacao: TWideStringField
      FieldName = 'NfsCodigoVerificacao'
      Size = 9
    end
    object QrSrvTomInnCabNfsDataEmissao: TDateTimeField
      FieldName = 'NfsDataEmissao'
      Required = True
    end
    object QrSrvTomInnCabNfsNfseSubstituida: TLargeintField
      FieldName = 'NfsNfseSubstituida'
      Required = True
    end
    object QrSrvTomInnCabNfsOutrasInformacoes: TWideStringField
      FieldName = 'NfsOutrasInformacoes'
      Size = 255
    end
    object QrSrvTomInnCabNfsBaseCalculo: TFloatField
      FieldName = 'NfsBaseCalculo'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabNfsAliquota: TFloatField
      FieldName = 'NfsAliquota'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabNfsValorIss: TFloatField
      FieldName = 'NfsValorIss'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabNfsValorLiquidoNfse: TFloatField
      FieldName = 'NfsValorLiquidoNfse'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabNfsValorCredito: TFloatField
      FieldName = 'NfsValorCredito'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsRpsIDNumero: TIntegerField
      FieldName = 'DpsRpsIDNumero'
      Required = True
    end
    object QrSrvTomInnCabDpsRpsIDSerie: TWideStringField
      FieldName = 'DpsRpsIDSerie'
      Size = 5
    end
    object QrSrvTomInnCabDpsValorServicos: TFloatField
      FieldName = 'DpsValorServicos'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsValorDeducoes: TFloatField
      FieldName = 'DpsValorDeducoes'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsValorPis: TFloatField
      FieldName = 'DpsValorPis'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsValorCofins: TFloatField
      FieldName = 'DpsValorCofins'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsValorInss: TFloatField
      FieldName = 'DpsValorInss'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsValorIr: TFloatField
      FieldName = 'DpsValorIr'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsValorCsll: TFloatField
      FieldName = 'DpsValorCsll'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsOutrasRetencoes: TFloatField
      FieldName = 'DpsOutrasRetencoes'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsDescontoIncondicionado: TFloatField
      FieldName = 'DpsDescontoIncondicionado'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsDescontoCondicionado: TFloatField
      FieldName = 'DpsDescontoCondicionado'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabDpsIssRetido: TSmallintField
      FieldName = 'DpsIssRetido'
      Required = True
    end
    object QrSrvTomInnCabDpsResponsavelRetencao: TIntegerField
      FieldName = 'DpsResponsavelRetencao'
    end
    object QrSrvTomInnCabDpsItemListaServico: TWideStringField
      FieldName = 'DpsItemListaServico'
      Size = 5
    end
    object QrSrvTomInnCabDpsCodigoCnae: TWideStringField
      FieldName = 'DpsCodigoCnae'
    end
    object QrSrvTomInnCabDpsCodigoTributacaoMunicipio: TWideStringField
      FieldName = 'DpsCodigoTributacaoMunicipio'
    end
    object QrSrvTomInnCabDpsDiscriminacao: TWideMemoField
      FieldName = 'DpsDiscriminacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrSrvTomInnCabDpsCodigoMunicipio: TIntegerField
      FieldName = 'DpsCodigoMunicipio'
      Required = True
    end
    object QrSrvTomInnCabDpsCodigoPais: TIntegerField
      FieldName = 'DpsCodigoPais'
      Required = True
    end
    object QrSrvTomInnCabDpsExigibilidadeIss: TSmallintField
      FieldName = 'DpsExigibilidadeIss'
      Required = True
    end
    object QrSrvTomInnCabDpsNaturezaOperacao: TIntegerField
      FieldName = 'DpsNaturezaOperacao'
      Required = True
    end
    object QrSrvTomInnCabDpsMunicipioIncidencia: TIntegerField
      FieldName = 'DpsMunicipioIncidencia'
      Required = True
    end
    object QrSrvTomInnCabDpsNumeroProcesso: TWideStringField
      FieldName = 'DpsNumeroProcesso'
      Size = 30
    end
    object QrSrvTomInnCabDpsConstrucaoCivilCodigoObra: TWideStringField
      FieldName = 'DpsConstrucaoCivilCodigoObra'
      Size = 15
    end
    object QrSrvTomInnCabDpsConstrucaoCivilArt: TWideStringField
      FieldName = 'DpsConstrucaoCivilArt'
      Size = 15
    end
    object QrSrvTomInnCabDpsRegimeEspecialTributacao: TSmallintField
      FieldName = 'DpsRegimeEspecialTributacao'
      Required = True
    end
    object QrSrvTomInnCabDpsOptanteSimplesNacional: TSmallintField
      FieldName = 'DpsOptanteSimplesNacional'
      Required = True
    end
    object QrSrvTomInnCabDpsIncentivoFiscal: TSmallintField
      FieldName = 'DpsIncentivoFiscal'
      Required = True
    end
    object QrSrvTomInnCabDpsInfID_ID: TWideStringField
      FieldName = 'DpsInfID_ID'
      Size = 255
    end
    object QrSrvTomInnCabLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrSrvTomInnCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvTomInnCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvTomInnCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrSrvTomInnCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrSrvTomInnCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrSrvTomInnCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrSrvTomInnCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrSrvTomInnCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrSrvTomInnCabFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrSrvTomInnCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrSrvTomInnCabNO_PRESTADOR: TWideStringField
      FieldName = 'NO_PRESTADOR'
      Size = 100
    end
    object QrSrvTomInnCabNO_INTERMEDIARIO: TWideStringField
      FieldName = 'NO_INTERMEDIARIO'
      Size = 100
    end
    object QrSrvTomInnCabNO_DpsItemListaServico: TWideStringField
      FieldName = 'NO_DpsItemListaServico'
      Size = 255
    end
    object QrSrvTomInnCabNO_cnae21Cad: TWideStringField
      FieldName = 'NO_cnae21Cad'
      Size = 255
    end
    object QrSrvTomInnCabNO_NfsRpsIDTipo: TWideStringField
      FieldName = 'NO_NfsRpsIDTipo'
      Size = 60
    end
    object QrSrvTomInnCabNO_DpsIssRetido: TWideStringField
      FieldName = 'NO_DpsIssRetido'
      Size = 6
    end
    object QrSrvTomInnCabNO_DpsResponsavelRetencao: TWideStringField
      FieldName = 'NO_DpsResponsavelRetencao'
      Size = 60
    end
    object QrSrvTomInnCabTIFEM_0: TIntegerField
      FieldName = 'TIFEM_0'
    end
    object QrSrvTomInnCabTIFEM_1: TIntegerField
      FieldName = 'TIFEM_1'
    end
    object QrSrvTomInnCabTIFEM_2: TIntegerField
      FieldName = 'TIFEM_2'
    end
    object QrSrvTomInnCabTIFEM_3: TIntegerField
      FieldName = 'TIFEM_3'
    end
    object QrSrvTomInnCabTIFEM_4: TIntegerField
      FieldName = 'TIFEM_4'
    end
    object QrSrvTomInnCabTIFEM_5: TIntegerField
      FieldName = 'TIFEM_5'
    end
    object QrSrvTomInnCabTIFEM_6: TIntegerField
      FieldName = 'TIFEM_6'
    end
    object QrSrvTomInnCabTIFEM_7: TIntegerField
      FieldName = 'TIFEM_7'
    end
    object QrSrvTomInnCabTIFEM_8: TIntegerField
      FieldName = 'TIFEM_8'
    end
    object QrSrvTomInnCabRetValorPis: TFloatField
      FieldName = 'RetValorPis'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabRetValorCofins: TFloatField
      FieldName = 'RetValorCofins'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabRetValorInss: TFloatField
      FieldName = 'RetValorInss'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabRetValorIr: TFloatField
      FieldName = 'RetValorIr'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabRetValorCsll: TFloatField
      FieldName = 'RetValorCsll'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabRetValorISS: TFloatField
      FieldName = 'RetValorISS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabRetValorTotal: TFloatField
      FieldName = 'RetValorTotal'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabRetOutrasRetencoes: TFloatField
      FieldName = 'RetOutrasRetencoes'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomInnCabValorLiqSemRet: TFloatField
      FieldName = 'ValorLiqSemRet'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsSrvTomInnCab: TDataSource
    DataSet = QrSrvTomInnCab
    Left = 472
    Top = 49
  end
  object Qr_: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_its'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 188
    Top = 237
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object Ds_: TDataSource
    DataSet = Qr_
    Left = 188
    Top = 281
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Inclui lan'#231'amento financeiro a pagar ao formecedor'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = 
        '&Altera o lan'#231'amento financeiro a pagar ao formecedor selecionad' +
        'o'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = 
        '&Exclui o lan'#231'amento financeiro a pagar ao formecedor selecionad' +
        'o'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrEmissS: TMySQLQuery
    Database = Dmod.MyDB
    Left = 560
    Top = 65524
    object QrEmissSData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissSTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmissSCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrEmissSAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrEmissSGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrEmissSDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 128
    end
    object QrEmissSNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrEmissSDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissSCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrEmissSCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissSDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrEmissSSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrEmissSVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissSLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmissSFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrEmissSFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrEmissSNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrEmissSNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 10
      Calculated = True
    end
    object QrEmissSNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrEmissSID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrEmissSSub: TIntegerField
      FieldName = 'Sub'
    end
    object QrEmissSFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrEmissSBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrEmissSLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrEmissSCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrEmissSLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrEmissSPago: TFloatField
      FieldName = 'Pago'
    end
    object QrEmissSMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEmissSFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrEmissSCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEmissSControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmissSID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrEmissSCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmissSQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmissSFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrEmissSOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrEmissSLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrEmissSForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrEmissSMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrEmissSMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrEmissSProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrEmissSDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrEmissSCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrEmissSNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrEmissSVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrEmissSAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrEmissSICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrEmissSICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrEmissSDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrEmissSDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEmissSDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrEmissSDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmissSDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmissSUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmissSUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmissSEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrEmissSContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrEmissSCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrEmissSFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEmissSAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrEmissSSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrEmissSGenCtb: TIntegerField
      FieldName = 'GenCtb'
    end
    object QrEmissSGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrEmissSGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
  end
  object DsEmissS: TDataSource
    DataSet = QrEmissS
    Left = 560
    Top = 36
  end
  object QrSumS: TMySQLQuery
    Database = Dmod.MyDB
    Left = 564
    Top = 89
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumSDebito: TFloatField
      FieldName = 'Debito'
    end
  end
end
