object FmSrvTomInnCad: TFmSrvTomInnCad
  Left = 339
  Top = 185
  Caption = 'SRV-TOMAD-002 :: Edi'#231#227'o de Servi'#231'o Tomado'
  ClientHeight = 578
  ClientWidth = 993
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 993
    Height = 419
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 993
      Height = 419
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 993
        Height = 419
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Dados principais'
          object Panel12: TPanel
            Left = 0
            Top = 0
            Width = 985
            Height = 391
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox1: TGroupBox
              Left = 0
              Top = 0
              Width = 985
              Height = 391
              Align = alClient
              TabOrder = 0
              object Panel7: TPanel
                Left = 2
                Top = 15
                Width = 981
                Height = 50
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label9: TLabel
                  Left = 8
                  Top = 4
                  Width = 44
                  Height = 13
                  Caption = 'Empresa:'
                end
                object Label21: TLabel
                  Left = 594
                  Top = 4
                  Width = 42
                  Height = 13
                  Caption = 'Emiss'#227'o:'
                end
                object Label1: TLabel
                  Left = 803
                  Top = 4
                  Width = 65
                  Height = 13
                  Caption = 'Compet'#234'ncia:'
                  Enabled = False
                end
                object Label31: TLabel
                  Left = 8
                  Top = 26
                  Width = 44
                  Height = 13
                  Caption = 'Hist'#243'rico:'
                end
                object Label16: TLabel
                  Left = 883
                  Top = 29
                  Width = 36
                  Height = 13
                  Caption = 'C'#243'digo:'
                end
                object EdEmpresa: TdmkEditCB
                  Left = 58
                  Top = 0
                  Width = 44
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdEmpresaChange
                  DBLookupComboBox = CBEmpresa
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBEmpresa: TdmkDBLookupComboBox
                  Left = 102
                  Top = 0
                  Width = 490
                  Height = 21
                  KeyField = 'Filial'
                  ListField = 'NOMEFILIAL'
                  ListSource = DModG.DsEmpresas
                  TabOrder = 1
                  dmkEditCB = EdEmpresa
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object TPRpsDataEmissao: TdmkEditDateTimePicker
                  Left = 638
                  Top = 0
                  Width = 110
                  Height = 21
                  Date = 41131.000000000000000000
                  Time = 0.724786689817847200
                  TabOrder = 2
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object EdRpsHoraEmissao: TdmkEdit
                  Left = 752
                  Top = 0
                  Width = 49
                  Height = 21
                  TabOrder = 3
                  FormatType = dmktfTime
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfLong
                  HoraFormat = dmkhfLong
                  Texto = '00:00:00'
                  UpdCampo = 'HoraIni'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object TPCompetencia: TdmkEditDateTimePicker
                  Left = 870
                  Top = 0
                  Width = 111
                  Height = 21
                  Date = 44647.000000000000000000
                  Time = 0.724786689817847200
                  Enabled = False
                  TabOrder = 4
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object EdNome: TdmkEdit
                  Left = 56
                  Top = 24
                  Width = 821
                  Height = 21
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCodigo: TdmkEdit
                  Left = 926
                  Top = 25
                  Width = 55
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 6
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utInc
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
              end
              object Panel11: TPanel
                Left = 2
                Top = 277
                Width = 981
                Height = 0
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
              end
              object Panel5: TPanel
                Left = 2
                Top = 65
                Width = 981
                Height = 142
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 2
                object Panel8: TPanel
                  Left = 0
                  Top = 0
                  Width = 564
                  Height = 142
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label17: TLabel
                    Left = 8
                    Top = 52
                    Width = 63
                    Height = 13
                    Caption = 'Intermedi'#225'rio:'
                  end
                  object SbIntermediario: TSpeedButton
                    Left = 539
                    Top = 48
                    Width = 21
                    Height = 21
                    Caption = '...'
                    OnClick = SbIntermediarioClick
                  end
                  object Label34: TLabel
                    Left = 8
                    Top = 27
                    Width = 100
                    Height = 13
                    Caption = 'Prestador do servi'#231'o:'
                  end
                  object SbPrestador: TSpeedButton
                    Left = 539
                    Top = 25
                    Width = 21
                    Height = 20
                    Caption = '...'
                    OnClick = SbPrestadorClick
                  end
                  object Label18: TLabel
                    Left = 8
                    Top = 75
                    Width = 99
                    Height = 13
                    Caption = 'Servi'#231'o (LC 116/03):'
                  end
                  object SbItemListaServico: TSpeedButton
                    Left = 539
                    Top = 71
                    Width = 21
                    Height = 20
                    Caption = '...'
                    OnClick = SbItemListaServicoClick
                  end
                  object Label36: TLabel
                    Left = 8
                    Top = 98
                    Width = 83
                    Height = 13
                    Caption = 'C'#243'digo do CNAE:'
                  end
                  object SbCodigoCnae: TSpeedButton
                    Left = 539
                    Top = 94
                    Width = 21
                    Height = 21
                    Caption = '...'
                    OnClick = SbCodigoCnaeClick
                  end
                  object Label19: TLabel
                    Left = 8
                    Top = 5
                    Width = 70
                    Height = 13
                    Caption = 'N'#250'mero NFSe:'
                  end
                  object Label28: TLabel
                    Left = 8
                    Top = 122
                    Width = 64
                    Height = 13
                    Caption = 'Tipo de RPS:'
                  end
                  object SbRpsIDTipo: TSpeedButton
                    Left = 299
                    Top = 118
                    Width = 21
                    Height = 21
                    Hint = 'Inclui item de carteira'
                    Caption = '...'
                    Enabled = False
                  end
                  object Label29: TLabel
                    Left = 323
                    Top = 122
                    Width = 40
                    Height = 13
                    Caption = 'N'#250'mero:'
                  end
                  object Label30: TLabel
                    Left = 469
                    Top = 122
                    Width = 27
                    Height = 13
                    Caption = 'S'#233'rie:'
                  end
                  object Label42: TLabel
                    Left = 212
                    Top = 5
                    Width = 109
                    Height = 13
                    Caption = 'C'#243'digo de verifica'#231#227'o: '
                  end
                  object EdIntermediario: TdmkEditCB
                    Left = 110
                    Top = 48
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 4
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBIntermediario
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBIntermediario: TdmkDBLookupComboBox
                    Left = 166
                    Top = 48
                    Width = 371
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'NOMEENTIDADE'
                    ListSource = DsIntermediarios
                    TabOrder = 5
                    dmkEditCB = EdIntermediario
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                    LocF7PreDefProc = f7pNone
                  end
                  object EdPrestador: TdmkEditCB
                    Left = 110
                    Top = 25
                    Width = 56
                    Height = 20
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBPrestador
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBPrestador: TdmkDBLookupComboBox
                    Left = 166
                    Top = 25
                    Width = 371
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'NOMEENTIDADE'
                    ListSource = DsPrestadores
                    TabOrder = 3
                    dmkEditCB = EdPrestador
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                    LocF7PreDefProc = f7pNone
                  end
                  object EdItemListaServico: TdmkEditCB
                    Left = 110
                    Top = 71
                    Width = 56
                    Height = 20
                    TabOrder = 6
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                    DBLookupComboBox = CBItemListaServico
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBItemListaServico: TdmkDBLookupComboBox
                    Left = 166
                    Top = 71
                    Width = 371
                    Height = 21
                    KeyField = 'CodAlf'
                    ListField = 'Nome'
                    ListSource = DsListServ
                    TabOrder = 7
                    dmkEditCB = EdItemListaServico
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                    LocF7PreDefProc = f7pNone
                  end
                  object EdCodigoCnae: TdmkEditCB
                    Left = 110
                    Top = 94
                    Width = 56
                    Height = 21
                    TabOrder = 8
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    QryCampo = 'CodigoCnae'
                    UpdCampo = 'CodigoCnae'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                    DBLookupComboBox = CBCodigoCnae
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBCodigoCnae: TdmkDBLookupComboBox
                    Left = 166
                    Top = 94
                    Width = 371
                    Height = 21
                    KeyField = 'CodAlf'
                    ListField = 'Nome'
                    ListSource = DsCNAE21Cad
                    TabOrder = 9
                    dmkEditCB = EdCodigoCnae
                    QryCampo = 'CodigoCnae'
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                    LocF7PreDefProc = f7pNone
                  end
                  object EdNfsNumero: TdmkEdit
                    Left = 110
                    Top = 0
                    Width = 96
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utInc
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object EdRpsIdSerie: TdmkEdit
                    Left = 500
                    Top = 118
                    Width = 59
                    Height = 21
                    TabOrder = 13
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    QryCampo = 'NumOC'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object EdRpsIDTipo: TdmkEditCB
                    Left = 110
                    Top = 118
                    Width = 20
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 10
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBRpsIDTipo
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBRpsIDTipo: TdmkDBLookupComboBox
                    Left = 130
                    Top = 118
                    Width = 166
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsTsTipoRps1
                    TabOrder = 11
                    dmkEditCB = EdRpsIDTipo
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                    LocF7PreDefProc = f7pNone
                  end
                  object EdRpsIdNumero: TdmkEdit
                    Left = 370
                    Top = 118
                    Width = 96
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 12
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utInc
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object EdNfsCodigoVerificacao: TdmkEdit
                    Left = 324
                    Top = 0
                    Width = 213
                    Height = 21
                    TabOrder = 1
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                end
                object Panel9: TPanel
                  Left = 564
                  Top = 0
                  Width = 417
                  Height = 142
                  Align = alClient
                  BevelOuter = bvNone
                  Caption = 'Panel9'
                  TabOrder = 1
                  object dmkLabelRotate1: TdmkLabelRotate
                    Left = 0
                    Top = 0
                    Width = 17
                    Height = 142
                    Angle = ag90
                    Caption = 'Discrimina'#231#227'o:'
                    Font.Charset = ANSI_CHARSET
                    Font.Color = clBlack
                    Font.Height = -12
                    Font.Name = 'Arial'
                    Font.Style = []
                    Align = alLeft
                  end
                  object MeDiscriminacao: TdmkMemo
                    Left = 17
                    Top = 0
                    Width = 400
                    Height = 142
                    TabStop = False
                    Align = alClient
                    TabOrder = 0
                    WantReturns = False
                    OnKeyDown = MeDiscriminacaoKeyDown
                    UpdType = utYes
                  end
                end
              end
              object Panel15: TPanel
                Left = 2
                Top = 207
                Width = 981
                Height = 70
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 3
                object Panel16: TPanel
                  Left = 0
                  Top = 0
                  Width = 564
                  Height = 70
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 0
                  object GroupBox3: TGroupBox
                    Left = 4
                    Top = 4
                    Width = 560
                    Height = 60
                    Caption = ' Valor do Servi'#231'o e ISS: '
                    TabOrder = 0
                    object Label5: TLabel
                      Left = 8
                      Top = 16
                      Width = 48
                      Height = 13
                      Caption = '$ Servi'#231'o:'
                    end
                    object Label7: TLabel
                      Left = 419
                      Top = 16
                      Width = 51
                      Height = 13
                      Caption = '% Aliq ISS:'
                    end
                    object Label32: TLabel
                      Left = 92
                      Top = 16
                      Width = 81
                      Height = 13
                      Caption = '$ Desco. incond:'
                    end
                    object Label8: TLabel
                      Left = 474
                      Top = 16
                      Width = 29
                      Height = 13
                      Caption = '$ ISS:'
                    end
                    object Label40: TLabel
                      Left = 336
                      Top = 16
                      Width = 73
                      Height = 13
                      Caption = '$ Base c'#225'lculo:'
                    end
                    object Label6: TLabel
                      Left = 258
                      Top = 16
                      Width = 61
                      Height = 13
                      Caption = '$ Dedu'#231#245'es:'
                    end
                    object Label33: TLabel
                      Left = 175
                      Top = 16
                      Width = 81
                      Height = 13
                      Caption = '$ Desco. condic:'
                    end
                    object EdValorServicos: TdmkEdit
                      Left = 8
                      Top = 31
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utInc
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                      OnChange = EdValorServicosChange
                    end
                    object EdAliquota: TdmkEdit
                      Left = 419
                      Top = 31
                      Width = 52
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 5
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utInc
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                      OnChange = EdAliquotaChange
                    end
                    object EdDescontoIncondicionado: TdmkEdit
                      Left = 92
                      Top = 31
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 1
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utInc
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                      OnChange = EdDescontoIncondicionadoChange
                    end
                    object EdValorIss: TdmkEdit
                      Left = 474
                      Top = 31
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 6
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utInc
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object EdNfsBaseCalculo: TdmkEdit
                      Left = 336
                      Top = 31
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 4
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utInc
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                      OnChange = EdValorDeducoesChange
                    end
                    object EdValorDeducoes: TdmkEdit
                      Left = 258
                      Top = 31
                      Width = 75
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 3
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utInc
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                      OnChange = EdValorDeducoesChange
                    end
                    object EdDescontoCondicionado: TdmkEdit
                      Left = 175
                      Top = 31
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 2
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utInc
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                      OnChange = EdDescontoCondicionadoChange
                    end
                  end
                end
                object Panel17: TPanel
                  Left = 564
                  Top = 0
                  Width = 417
                  Height = 70
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object GroupBox5: TGroupBox
                    Left = 10
                    Top = 4
                    Width = 411
                    Height = 60
                    Caption = ' Reten'#231#227'o do ISS: '
                    TabOrder = 0
                    object SbResponsavelRetencao: TSpeedButton
                      Left = 378
                      Top = 31
                      Width = 20
                      Height = 21
                      Hint = 'Inclui item de carteira'
                      Caption = '...'
                    end
                    object Label35: TLabel
                      Left = 134
                      Top = 16
                      Width = 65
                      Height = 13
                      Caption = 'Respons'#225'vel:'
                    end
                    object Label20: TLabel
                      Left = 8
                      Top = 16
                      Width = 37
                      Height = 13
                      Caption = 'Retido?'
                    end
                    object EdResponsavelRetencao: TdmkEditCB
                      Left = 134
                      Top = 31
                      Width = 35
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 2
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnChange = EdResponsavelRetencaoChange
                      DBLookupComboBox = CBResponsavelRetencao
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBResponsavelRetencao: TdmkDBLookupComboBox
                      Left = 173
                      Top = 31
                      Width = 200
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsTsResponsavelRetencao
                      TabOrder = 3
                      dmkEditCB = EdResponsavelRetencao
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                    object EdIssRetido: TdmkEditCB
                      Left = 8
                      Top = 31
                      Width = 23
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'OptanteSimplesNacional'
                      UpdCampo = 'OptanteSimplesNacional'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnChange = EdIssRetidoChange
                      DBLookupComboBox = CBIssRetido
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBIssRetido: TdmkDBLookupComboBox
                      Left = 31
                      Top = 31
                      Width = 96
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsTsSimNao3
                      TabOrder = 1
                      dmkEditCB = EdIssRetido
                      QryCampo = 'OptanteSimplesNacional'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                      LocF7PreDefProc = f7pNone
                    end
                  end
                end
              end
              object Panel10: TPanel
                Left = 2
                Top = 277
                Width = 981
                Height = 112
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 4
                object GroupBox4: TGroupBox
                  Left = 4
                  Top = 0
                  Width = 973
                  Height = 60
                  Caption = ' Reten'#231#227'o de Tributos:  '
                  TabOrder = 0
                  object Label10: TLabel
                    Left = 84
                    Top = 16
                    Width = 29
                    Height = 13
                    Caption = '$ PIS:'
                  end
                  object Label11: TLabel
                    Left = 160
                    Top = 16
                    Width = 51
                    Height = 13
                    Caption = '$ COFINS:'
                  end
                  object Label12: TLabel
                    Left = 236
                    Top = 16
                    Width = 37
                    Height = 13
                    Caption = '$ INSS:'
                  end
                  object Label13: TLabel
                    Left = 312
                    Top = 16
                    Width = 23
                    Height = 13
                    Caption = '$ IR:'
                  end
                  object Label14: TLabel
                    Left = 388
                    Top = 16
                    Width = 38
                    Height = 13
                    Caption = '$ CSLL:'
                  end
                  object Label15: TLabel
                    Left = 464
                    Top = 16
                    Width = 61
                    Height = 13
                    Caption = '$ Outras ret.:'
                  end
                  object Label41: TLabel
                    Left = 872
                    Top = 16
                    Width = 76
                    Height = 13
                    Caption = '$ Liquido NFSe:'
                  end
                  object Label2: TLabel
                    Left = 8
                    Top = 16
                    Width = 29
                    Height = 13
                    Caption = '$ ISS:'
                  end
                  object Label3: TLabel
                    Left = 794
                    Top = 16
                    Width = 64
                    Height = 13
                    Caption = '$ Reten'#231#245'es:'
                  end
                  object Label4: TLabel
                    Left = 712
                    Top = 16
                    Width = 73
                    Height = 13
                    Caption = '$ Base c'#225'lculo:'
                  end
                  object EdRetValorPis: TdmkEdit
                    Left = 84
                    Top = 31
                    Width = 72
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utInc
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdRetValorPisChange
                  end
                  object EdRetValorCofins: TdmkEdit
                    Left = 160
                    Top = 31
                    Width = 72
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utInc
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdRetValorCofinsChange
                  end
                  object EdRetValorInss: TdmkEdit
                    Left = 236
                    Top = 31
                    Width = 72
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 3
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utInc
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdRetValorInssChange
                  end
                  object EdRetValorIr: TdmkEdit
                    Left = 312
                    Top = 31
                    Width = 72
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 4
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utInc
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdRetValorIrChange
                  end
                  object EdRetValorCsll: TdmkEdit
                    Left = 388
                    Top = 31
                    Width = 72
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 5
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utInc
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdRetValorCsllChange
                  end
                  object EdRetOutrasRetencoes: TdmkEdit
                    Left = 464
                    Top = 31
                    Width = 72
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 6
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utInc
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdRetOutrasRetencoesChange
                  end
                  object EdNfsValorLiquidoNfse: TdmkEdit
                    Left = 872
                    Top = 31
                    Width = 89
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 9
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utInc
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdRetValorIss: TdmkEdit
                    Left = 8
                    Top = 31
                    Width = 72
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utInc
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdRetValorIssChange
                  end
                  object EdRetValorTotal: TdmkEdit
                    Left = 794
                    Top = 31
                    Width = 75
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 8
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utInc
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdRetValorTotalChange
                  end
                  object EdValorLiqSemRet: TdmkEdit
                    Left = 712
                    Top = 31
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 7
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utInc
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnChange = EdValorLiqSemRetChange
                  end
                end
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Outras informa'#231#245'es'
          ImageIndex = 1
          object Panel13: TPanel
            Left = 0
            Top = 0
            Width = 985
            Height = 391
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label43: TLabel
              Left = 12
              Top = 150
              Width = 94
              Height = 13
              Caption = 'Outras informa'#231#245'es:'
            end
            object Panel14: TPanel
              Left = 0
              Top = 0
              Width = 985
              Height = 81
              Align = alTop
              TabOrder = 0
              object GroupBox6: TGroupBox
                Left = 8
                Top = 5
                Width = 646
                Height = 72
                Caption = ' Exigibilidade do ISS:'
                TabOrder = 0
                object SpeedButton6: TSpeedButton
                  Left = 618
                  Top = 16
                  Width = 21
                  Height = 21
                  Hint = 'Inclui item de carteira'
                  Caption = '...'
                end
                object Label39: TLabel
                  Left = 8
                  Top = 20
                  Width = 158
                  Height = 13
                  Caption = 'C'#243'digo de natureza da opera'#231#227'o:'
                end
                object Label38: TLabel
                  Left = 8
                  Top = 46
                  Width = 362
                  Height = 13
                  Caption = 
                    'N'#250'mero do processo judicial ou administrativo de suspens'#227'o da ex' +
                    'igibilidade:'
                end
                object EdExigibilidadeIss: TdmkEditCB
                  Left = 170
                  Top = 16
                  Width = 23
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'ExigibilidadeIss'
                  UpdCampo = 'ExigibilidadeIss'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBExigibilidadeIss
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBExigibilidadeIss: TdmkDBLookupComboBox
                  Left = 193
                  Top = 16
                  Width = 422
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsTsExigibilidadeIss
                  TabOrder = 1
                  dmkEditCB = EdExigibilidadeIss
                  QryCampo = 'ExigibilidadeIss'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object EdNumeroProcesso: TdmkEdit
                  Left = 370
                  Top = 42
                  Width = 269
                  Height = 21
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'NumeroProcesso'
                  UpdCampo = 'NumeroProcesso'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object GroupBox8: TGroupBox
                Left = 657
                Top = 5
                Width = 320
                Height = 72
                Caption = ' Constru'#231#227'o Civil: '
                TabOrder = 1
                object Label23: TLabel
                  Left = 8
                  Top = 20
                  Width = 77
                  Height = 13
                  Caption = 'C'#243'digo de Obra:'
                end
                object Label24: TLabel
                  Left = 8
                  Top = 42
                  Width = 61
                  Height = 13
                  Caption = 'C'#243'digo ART:'
                end
                object EdConstrucaoCivilCodigoObra: TdmkEdit
                  Left = 86
                  Top = 14
                  Width = 225
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'ConstrucaoCivilCodigoObra'
                  UpdCampo = 'ConstrucaoCivilCodigoObra'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdConstrucaoCivilArt: TdmkEdit
                  Left = 86
                  Top = 38
                  Width = 225
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'ConstrucaoCivilArt'
                  UpdCampo = 'ConstrucaoCivilArt'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
            object Panel6: TPanel
              Left = 0
              Top = 81
              Width = 985
              Height = 63
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object GroupBox7: TGroupBox
                Left = 8
                Top = 0
                Width = 201
                Height = 60
                Caption = ' Munic'#237'pio de incid'#234'ncia do ISS: '
                Enabled = False
                TabOrder = 0
                object SbMunicipioIncidencia: TSpeedButton
                  Left = 174
                  Top = 31
                  Width = 21
                  Height = 21
                  Hint = 'Inclui item de carteira'
                  Caption = '...'
                  Enabled = False
                end
                object Label22: TLabel
                  Left = 4
                  Top = 16
                  Width = 110
                  Height = 13
                  Caption = 'C'#243'digo DTB munic'#237'pio:'
                  Enabled = False
                end
                object EdQuemPagaISS: TdmkEditCB
                  Left = 4
                  Top = 31
                  Width = 29
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBQuemPagaISS
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBQuemPagaISS: TdmkDBLookupComboBox
                  Left = 35
                  Top = 31
                  Width = 138
                  Height = 21
                  Enabled = False
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsTsResponsavelRetencao
                  TabOrder = 1
                  dmkEditCB = EdQuemPagaISS
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
              end
              object GroupBox11: TGroupBox
                Left = 405
                Top = 0
                Width = 167
                Height = 60
                Caption = ' Optante pelo Simples Nacional: '
                TabOrder = 1
                object SpeedButton8: TSpeedButton
                  Left = 138
                  Top = 31
                  Width = 20
                  Height = 21
                  Hint = 'Inclui item de carteira'
                  Caption = '...'
                  Enabled = False
                end
                object Label25: TLabel
                  Left = 8
                  Top = 16
                  Width = 35
                  Height = 13
                  Caption = 'Optou?'
                end
                object EdOptanteSimplesNacional: TdmkEditCB
                  Left = 8
                  Top = 31
                  Width = 23
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'OptanteSimplesNacional'
                  UpdCampo = 'OptanteSimplesNacional'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBOptanteSimplesNacional
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBOptanteSimplesNacional: TdmkDBLookupComboBox
                  Left = 31
                  Top = 31
                  Width = 106
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsTsSimNao1
                  TabOrder = 1
                  dmkEditCB = EdOptanteSimplesNacional
                  QryCampo = 'OptanteSimplesNacional'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
              end
              object GroupBox12: TGroupBox
                Left = 574
                Top = 0
                Width = 168
                Height = 60
                Caption = ' Incentivo Fiscal: '
                TabOrder = 2
                object SpeedButton9: TSpeedButton
                  Left = 138
                  Top = 31
                  Width = 20
                  Height = 21
                  Hint = 'Inclui item de carteira'
                  Caption = '...'
                  Enabled = False
                end
                object Label26: TLabel
                  Left = 8
                  Top = 16
                  Width = 27
                  Height = 13
                  Caption = 'Tem?'
                end
                object EdIncentivoFiscal: TdmkEditCB
                  Left = 8
                  Top = 31
                  Width = 23
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'IncentivoFiscal'
                  UpdCampo = 'IncentivoFiscal'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBIncentivoFiscal
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBIncentivoFiscal: TdmkDBLookupComboBox
                  Left = 31
                  Top = 31
                  Width = 106
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsTsSimNao2
                  TabOrder = 1
                  dmkEditCB = EdIncentivoFiscal
                  QryCampo = 'IncentivoFiscal'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
              end
              object GroupBox9: TGroupBox
                Left = 744
                Top = 0
                Width = 238
                Height = 60
                Caption = ' Regime Especial de Tributa'#231#227'o:'
                TabOrder = 3
                object SpeedButton7: TSpeedButton
                  Left = 213
                  Top = 31
                  Width = 21
                  Height = 21
                  Hint = 'Inclui item de carteira'
                  Caption = '...'
                  Enabled = False
                end
                object Label27: TLabel
                  Left = 8
                  Top = 16
                  Width = 114
                  Height = 13
                  Caption = 'C'#243'digo da identifica'#231#227'o:'
                end
                object EdRegimeEspecialTributacao: TdmkEditCB
                  Left = 8
                  Top = 31
                  Width = 23
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'RegimeEspecialTributacao'
                  UpdCampo = 'RegimeEspecialTributacao'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBRegimeEspecialTributacao
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBRegimeEspecialTributacao: TdmkDBLookupComboBox
                  Left = 31
                  Top = 31
                  Width = 179
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsTsRegimeEspecialTributacao
                  TabOrder = 1
                  dmkEditCB = EdRegimeEspecialTributacao
                  QryCampo = 'RegimeEspecialTributacao'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
              end
              object GroupBox10: TGroupBox
                Left = 212
                Top = 0
                Width = 190
                Height = 60
                Caption = ' Tributa'#231#227'o: '
                TabOrder = 4
                object Label37: TLabel
                  Left = 12
                  Top = 14
                  Width = 165
                  Height = 13
                  Caption = 'Codigo de tributa'#231#227'o do munic'#237'pio:'
                end
                object EdCodigoTributacaoMunicipio: TdmkEdit
                  Left = 12
                  Top = 30
                  Width = 170
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'CodigoTributacaoMunicipio'
                  UpdCampo = 'CodigoTributacaoMunicipio'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
            object EdNfsOutrasInformacoes: TdmkEdit
              Left = 112
              Top = 148
              Width = 869
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 899
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 307
        Height = 31
        Caption = 'Edi'#231#227'o de Servi'#231'o Tomado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 307
        Height = 31
        Caption = 'Edi'#231#227'o de Servi'#231'o Tomado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 307
        Height = 31
        Caption = 'Edi'#231#227'o de Servi'#231'o Tomado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 466
    Width = 993
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 989
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 509
    Width = 993
    Height = 69
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 850
      Top = 15
      Width = 141
      Height = 52
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 848
      Height = 52
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPrestadores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE,'
      'IF(Tipo=0, ECep, PCep) CEP'
      'FROM entidades'
      'WHERE Fornece8="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 340
    Top = 8
    object QrPrestadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrPrestadoresCEP: TIntegerField
      FieldName = 'CEP'
    end
  end
  object DsPrestadores: TDataSource
    DataSet = QrPrestadores
    Left = 340
    Top = 56
  end
  object VUEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = Panel3
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 104
    Top = 65524
  end
  object QrIntermediarios: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE'
      '')
    Left = 672
    Top = 12
    object QrIntermediariosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIntermediariosNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsIntermediarios: TDataSource
    DataSet = QrIntermediarios
    Left = 700
    Top = 12
  end
  object QrTsTipoRps1: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tstiporps'
      'ORDER BY Nome')
    Left = 836
    Top = 128
    object QrTsTipoRps1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsTipoRps1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTsTipoRps1: TDataSource
    DataSet = QrTsTipoRps1
    Left = 864
    Top = 128
  end
  object QrTsResponsavelRetencao: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tsresponsavelretencao'
      'ORDER BY Nome')
    Left = 836
    Top = 156
    object QrTsResponsavelRetencaoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsResponsavelRetencaoNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTsResponsavelRetencao: TDataSource
    DataSet = QrTsResponsavelRetencao
    Left = 864
    Top = 156
  end
  object QrTsExigibilidadeIss: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tsexigibilidadeiss'
      'ORDER BY Nome')
    Left = 836
    Top = 184
    object QrTsExigibilidadeIssCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsExigibilidadeIssNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsTsExigibilidadeIss: TDataSource
    DataSet = QrTsExigibilidadeIss
    Left = 864
    Top = 184
  end
  object QrCNAE21Cad: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, CodAlf, Nome'
      'FROM cnae21Cad'
      'ORDER BY Nome')
    Left = 840
    Top = 12
    object QrCNAE21CadCodAlf: TWideStringField
      FieldName = 'CodAlf'
    end
    object QrCNAE21CadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCNAE21CadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsCNAE21Cad: TDataSource
    DataSet = QrCNAE21Cad
    Left = 840
    Top = 64
  end
  object QrDTB_Munici: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 836
    Top = 212
    object QrDTB_MuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDTB_MuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsDTB_Munici: TDataSource
    DataSet = QrDTB_Munici
    Left = 864
    Top = 212
  end
  object QrTsRegimeEspecialTributacao: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tsregimeespecialtributacao'
      'ORDER BY Nome')
    Left = 632
    Top = 144
    object QrTsRegimeEspecialTributacaoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsRegimeEspecialTributacaoNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object QrTsSimNao2: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tssimnao'
      'ORDER BY Nome')
    Left = 892
    Top = 184
    object QrTsSimNao2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsSimNao2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 6
    end
  end
  object DsTsRegimeEspecialTributacao: TDataSource
    DataSet = QrTsRegimeEspecialTributacao
    Left = 636
    Top = 196
  end
  object QrTsSimNao1: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tssimnao'
      'ORDER BY Nome')
    Left = 892
    Top = 156
    object QrTsSimNao1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsSimNao1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 6
    end
  end
  object DsTsSimNao1: TDataSource
    DataSet = QrTsSimNao1
    Left = 920
    Top = 156
  end
  object DsTsSimNao2: TDataSource
    DataSet = QrTsSimNao2
    Left = 920
    Top = 184
  end
  object QrTsTipoRps2: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tstiporps'
      'ORDER BY Nome')
    Left = 892
    Top = 128
    object QrTsTipoRps2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsTipoRps2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTsTipoRps2: TDataSource
    DataSet = QrTsTipoRps2
    Left = 920
    Top = 128
  end
  object QrTsSimNao3: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tssimnao'
      'ORDER BY Nome')
    Left = 892
    Top = 212
    object QrTsSimNao3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsSimNao3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 6
    end
  end
  object DsTsSimNao3: TDataSource
    DataSet = QrTsSimNao3
    Left = 920
    Top = 212
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 16
    Top = 65524
  end
  object QrListServ: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, CodAlf, Nome'
      'FROM listserv'
      'ORDER BY Nome')
    Left = 420
    Top = 8
    object QrListServNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrListServCodAlf: TWideStringField
      FieldName = 'CodAlf'
      Required = True
    end
    object QrListServCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsListServ: TDataSource
    DataSet = QrListServ
    Left = 420
    Top = 56
  end
end
