object FmSrvTomRetCab: TFmSrvTomRetCab
  Left = 368
  Top = 194
  Caption = 'SRV-TOMAD-003 :: Reten'#231#227'o de Imposto de Servi'#231'o Tomado'
  ClientHeight = 591
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 495
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 209
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 74
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label21: TLabel
        Left = 554
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object Label4: TLabel
        Left = 666
        Top = 16
        Width = 65
        Height = 13
        Caption = 'Compet'#234'ncia:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 72
        Width = 761
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGTIFEM: TdmkRadioGroup
        Left = 16
        Top = 96
        Width = 761
        Height = 65
        Caption = ' Tipo de imposto (federal, estadual ou municipal)'
        Items.Strings = (
          'TTipImpostoFddEstMun...')
        TabOrder = 6
        QryCampo = 'TIFEM'
        UpdCampo = 'TIFEM'
        UpdType = utYes
        OldValor = 0
      end
      object EdEmpresa: TdmkEditCB
        Left = 74
        Top = 32
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 118
        Top = 32
        Width = 431
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPData: TdmkEditDateTimePicker
        Left = 554
        Top = 32
        Width = 110
        Height = 21
        Date = 41131.000000000000000000
        Time = 0.724786689817847200
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPCompetencia: TdmkEditDateTimePicker
        Left = 666
        Top = 32
        Width = 110
        Height = 21
        Date = 41131.000000000000000000
        Time = 0.724786689817847200
        TabOrder = 4
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 432
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 495
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 321
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 245
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 169
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label2: TLabel
        Left = 72
        Top = 224
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label6: TLabel
        Left = 66
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label8: TLabel
        Left = 546
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object Label10: TLabel
        Left = 658
        Top = 16
        Width = 65
        Height = 13
        Caption = 'Compet'#234'ncia:'
      end
      object Label11: TLabel
        Left = 8
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 672
        Top = 56
        Width = 65
        Height = 13
        Caption = '$ Total retido:'
        FocusControl = DBEdit6
      end
      object dmkEdit2: TdmkEdit
        Left = 8
        Top = 72
        Width = 661
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Codigo'
        DataSource = DsSrvTomRetCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 66
        Top = 32
        Width = 43
        Height = 21
        DataField = 'Empresa'
        DataSource = DsSrvTomRetCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 112
        Top = 32
        Width = 429
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsSrvTomRetCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 544
        Top = 32
        Width = 112
        Height = 21
        DataField = 'Data'
        DataSource = DsSrvTomRetCab
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 660
        Top = 32
        Width = 112
        Height = 21
        DataField = 'Competencia'
        DataSource = DsSrvTomRetCab
        TabOrder = 5
      end
      object DBRGTIFEM: TDBRadioGroup
        Left = 8
        Top = 96
        Width = 761
        Height = 65
        Caption = ' Tipo de imposto (federal, estadual ou municipal)'
        DataField = 'TIFEM'
        DataSource = DsSrvTomRetCab
        TabOrder = 6
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object DBEdit6: TDBEdit
        Left = 672
        Top = 72
        Width = 100
        Height = 21
        DataField = 'RetValorTotal'
        DataSource = DsSrvTomRetCab
        TabOrder = 7
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 431
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Reten'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtFin: TBitBtn
          Tag = 110
          Left = 250
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lct Fin.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtFinClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 169
      Width = 1008
      Height = 152
      Align = alTop
      DataSource = DsSrvTomRetIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NfsDataEmissao'
          Title.Caption = 'Data emiss'#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NfsNumero'
          Title.Caption = 'N'#176' NFS'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Prestador'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRESTADOR'
          Title.Caption = 'Nome Prestador'
          Width = 143
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 154
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DpsValorServicos'
          Title.Caption = '$ Servi'#231'os'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DpsValorDeducoes'
          Title.Caption = '$ Dedu'#231#245'es'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NfsValorLiquidoNfse'
          Title.Caption = '$ L'#237'q. NFS'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NfsValorIss'
          Title.Caption = '$ ISS'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RetValorISS'
          Title.Caption = '$ ISS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RetValorPis'
          Title.Caption = '$ PIS'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RetValorCofins'
          Title.Caption = '$ COFINS'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RetValorCsll'
          Title.Caption = '$ CSLL'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RetValorIr'
          Title.Caption = '$ IR'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RetValorInss'
          Title.Caption = '$ INSS'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RetOutrasRetencoes'
          Title.Caption = '$ Outras Ret.'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DpsResponsavelRetencao'
          Title.Caption = 'Resp.Ret.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DpsDescontoCondicionado'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DpsDescontoIncondicionado'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DpsDiscriminacao'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DpsRpsIDNumero'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DpsRpsIDSerie'
          Visible = True
        end>
    end
    object GBPagamentos: TGroupBox
      Left = 0
      Top = 326
      Width = 1008
      Height = 71
      Align = alTop
      Caption = ' Pagamentos:'
      TabOrder = 3
      object GroupBox8: TGroupBox
        Left = 2
        Top = 15
        Width = 1004
        Height = 54
        Align = alClient
        Caption = ' Fornecedor: '
        TabOrder = 0
        ExplicitHeight = 88
        object GridF: TDBGrid
          Left = 2
          Top = 15
          Width = 1000
          Height = 37
          Align = alClient
          Color = clWhite
          DataSource = DsEmissI
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatParcela'
              Title.Caption = 'N'#186
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              ReadOnly = True
              Title.Caption = 'Situa'#231#227'o'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMETIPO'
              Title.Caption = 'Tipo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 300
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 499
        Height = 32
        Caption = 'Reten'#231#227'o de Imposto de Servi'#231'o Tomado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 499
        Height = 32
        Caption = 'Reten'#231#227'o de Imposto de Servi'#231'o Tomado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 499
        Height = 32
        Caption = 'Reten'#231#227'o de Imposto de Servi'#231'o Tomado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrSrvTomRetCab: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeOpen = QrSrvTomRetCabBeforeOpen
    AfterOpen = QrSrvTomRetCabAfterOpen
    BeforeClose = QrSrvTomRetCabBeforeClose
    AfterScroll = QrSrvTomRetCabAfterScroll
    SQL.Strings = (
      'SELECT trc.*,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, '
      'IF(prs.Tipo=0, prs.RazaoSocial, prs.Nome) NO_PRESTADOR, '
      'tax.Nome NO_SrvTomTaxCad'
      'FROM srvtomretcab trc'
      'LEFT JOIN entidades    emp ON emp.Codigo=trc.Empresa'
      'LEFT JOIN entidades    prs ON prs.Codigo=trc.Prestador'
      'LEFT JOIN SrvTomTaxCad tax ON tax.Codigo=trc.SrvTomTaxCad'
      'WHERE trc.Codigo=1')
    Left = 244
    Top = 69
    object QrSrvTomRetCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvTomRetCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSrvTomRetCabFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrSrvTomRetCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 0
    end
    object QrSrvTomRetCabTIFEM: TSmallintField
      FieldName = 'TIFEM'
    end
    object QrSrvTomRetCabData: TDateField
      FieldName = 'Data'
    end
    object QrSrvTomRetCabCompetencia: TDateField
      FieldName = 'Competencia'
    end
    object QrSrvTomRetCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 120
    end
    object QrSrvTomRetCabNO_TIFEM: TWideStringField
      FieldName = 'NO_TIFEM'
      Size = 100
    end
    object QrSrvTomRetCabRetValorTotal: TFloatField
      FieldName = 'RetValorTotal'
    end
  end
  object DsSrvTomRetCab: TDataSource
    DataSet = QrSrvTomRetCab
    Left = 244
    Top = 121
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 668
    Top = 496
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 536
    Top = 496
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrSrvTomRetIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sic.*, emp.Filial,'
      'IF(emp.Tipo=0, emp.RAZAOSOCIAL, emp.Nome) NO_EMPRESA,'
      'IF(prs.Tipo=0, prs.RAZAOSOCIAL, prs.Nome) NO_PRESTADOR,'
      'IF(itm.Tipo=0, itm.RAZAOSOCIAL, itm.Nome) NO_INTERMEDIARIO,'
      'lsr.Nome NO_DpsItemListaServico, cna.Nome NO_cnae21Cad,'
      'ts1.Nome NO_NfsRpsIDTipo, ts3.Nome NO_DpsIssRetido,'
      'tsr.Nome NO_DpsResponsavelRetencao'
      'FROM srvtominncab sic'
      'LEFT JOIN entidades emp ON emp.Codigo=sic.Empresa'
      'LEFT JOIN entidades prs ON prs.Codigo=sic.Prestador'
      'LEFT JOIN entidades itm ON itm.Codigo=sic.Intermediario'
      
        'LEFT JOIN locbdermall.listserv  lsr ON lsr.Codigo=sic.DpsItemLis' +
        'taServico'
      
        'LEFT JOIN locbdermall.cnae21Cad cna ON cna.Codigo=sic.DpsCodigoC' +
        'nae'
      
        'LEFT JOIN locbdermall.tstiporps ts1 ON ts1.Codigo=sic.NfsRpsIDTi' +
        'po'
      
        'LEFT JOIN locbdermall.tssimnao  ts3 ON ts3.Codigo=sic.DpsIssReti' +
        'do'
      
        'LEFT JOIN tsresponsavelretencao tsr ON tsr.Codigo=sic.DpsRespons' +
        'avelRetencao'
      'WHERE sic.Codigo > 0')
    Left = 444
    Top = 69
    object QrSrvTomRetItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSrvTomRetItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrSrvTomRetItsPrestador: TIntegerField
      FieldName = 'Prestador'
      Required = True
    end
    object QrSrvTomRetItsIntermediario: TIntegerField
      FieldName = 'Intermediario'
      Required = True
    end
    object QrSrvTomRetItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSrvTomRetItsStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrSrvTomRetItsNfsNumero: TLargeintField
      FieldName = 'NfsNumero'
      Required = True
    end
    object QrSrvTomRetItsNfsRpsIDSerie: TWideStringField
      FieldName = 'NfsRpsIDSerie'
      Size = 5
    end
    object QrSrvTomRetItsNfsRpsIDTipo: TSmallintField
      FieldName = 'NfsRpsIDTipo'
      Required = True
    end
    object QrSrvTomRetItsNfsRpsIDNumero: TIntegerField
      FieldName = 'NfsRpsIDNumero'
      Required = True
    end
    object QrSrvTomRetItsNfsCodigoVerificacao: TWideStringField
      FieldName = 'NfsCodigoVerificacao'
      Size = 9
    end
    object QrSrvTomRetItsNfsDataEmissao: TDateTimeField
      FieldName = 'NfsDataEmissao'
      Required = True
    end
    object QrSrvTomRetItsNfsNfseSubstituida: TLargeintField
      FieldName = 'NfsNfseSubstituida'
      Required = True
    end
    object QrSrvTomRetItsNfsOutrasInformacoes: TWideStringField
      FieldName = 'NfsOutrasInformacoes'
      Size = 255
    end
    object QrSrvTomRetItsNfsBaseCalculo: TFloatField
      FieldName = 'NfsBaseCalculo'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsNfsAliquota: TFloatField
      FieldName = 'NfsAliquota'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsNfsValorIss: TFloatField
      FieldName = 'NfsValorIss'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsNfsValorLiquidoNfse: TFloatField
      FieldName = 'NfsValorLiquidoNfse'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsNfsValorCredito: TFloatField
      FieldName = 'NfsValorCredito'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsDpsRpsIDNumero: TIntegerField
      FieldName = 'DpsRpsIDNumero'
      Required = True
    end
    object QrSrvTomRetItsDpsRpsIDSerie: TWideStringField
      FieldName = 'DpsRpsIDSerie'
      Size = 5
    end
    object QrSrvTomRetItsDpsValorServicos: TFloatField
      FieldName = 'DpsValorServicos'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsDpsValorDeducoes: TFloatField
      FieldName = 'DpsValorDeducoes'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsDpsValorPis: TFloatField
      FieldName = 'DpsValorPis'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsDpsValorCofins: TFloatField
      FieldName = 'DpsValorCofins'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsDpsValorInss: TFloatField
      FieldName = 'DpsValorInss'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsDpsValorIr: TFloatField
      FieldName = 'DpsValorIr'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsDpsValorCsll: TFloatField
      FieldName = 'DpsValorCsll'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsDpsOutrasRetencoes: TFloatField
      FieldName = 'DpsOutrasRetencoes'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsDpsDescontoIncondicionado: TFloatField
      FieldName = 'DpsDescontoIncondicionado'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsDpsDescontoCondicionado: TFloatField
      FieldName = 'DpsDescontoCondicionado'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsDpsIssRetido: TSmallintField
      FieldName = 'DpsIssRetido'
      Required = True
    end
    object QrSrvTomRetItsDpsResponsavelRetencao: TIntegerField
      FieldName = 'DpsResponsavelRetencao'
    end
    object QrSrvTomRetItsDpsItemListaServico: TWideStringField
      FieldName = 'DpsItemListaServico'
      Size = 5
    end
    object QrSrvTomRetItsDpsCodigoCnae: TWideStringField
      FieldName = 'DpsCodigoCnae'
    end
    object QrSrvTomRetItsDpsCodigoTributacaoMunicipio: TWideStringField
      FieldName = 'DpsCodigoTributacaoMunicipio'
    end
    object QrSrvTomRetItsDpsDiscriminacao: TWideMemoField
      FieldName = 'DpsDiscriminacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrSrvTomRetItsDpsCodigoMunicipio: TIntegerField
      FieldName = 'DpsCodigoMunicipio'
      Required = True
    end
    object QrSrvTomRetItsDpsCodigoPais: TIntegerField
      FieldName = 'DpsCodigoPais'
      Required = True
    end
    object QrSrvTomRetItsDpsExigibilidadeIss: TSmallintField
      FieldName = 'DpsExigibilidadeIss'
      Required = True
    end
    object QrSrvTomRetItsDpsNaturezaOperacao: TIntegerField
      FieldName = 'DpsNaturezaOperacao'
      Required = True
    end
    object QrSrvTomRetItsDpsMunicipioIncidencia: TIntegerField
      FieldName = 'DpsMunicipioIncidencia'
      Required = True
    end
    object QrSrvTomRetItsDpsNumeroProcesso: TWideStringField
      FieldName = 'DpsNumeroProcesso'
      Size = 30
    end
    object QrSrvTomRetItsDpsConstrucaoCivilCodigoObra: TWideStringField
      FieldName = 'DpsConstrucaoCivilCodigoObra'
      Size = 15
    end
    object QrSrvTomRetItsDpsConstrucaoCivilArt: TWideStringField
      FieldName = 'DpsConstrucaoCivilArt'
      Size = 15
    end
    object QrSrvTomRetItsDpsRegimeEspecialTributacao: TSmallintField
      FieldName = 'DpsRegimeEspecialTributacao'
      Required = True
    end
    object QrSrvTomRetItsDpsOptanteSimplesNacional: TSmallintField
      FieldName = 'DpsOptanteSimplesNacional'
      Required = True
    end
    object QrSrvTomRetItsDpsIncentivoFiscal: TSmallintField
      FieldName = 'DpsIncentivoFiscal'
      Required = True
    end
    object QrSrvTomRetItsDpsInfID_ID: TWideStringField
      FieldName = 'DpsInfID_ID'
      Size = 255
    end
    object QrSrvTomRetItsLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrSrvTomRetItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvTomRetItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvTomRetItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrSrvTomRetItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrSrvTomRetItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrSrvTomRetItsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrSrvTomRetItsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrSrvTomRetItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrSrvTomRetItsFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrSrvTomRetItsNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrSrvTomRetItsNO_PRESTADOR: TWideStringField
      FieldName = 'NO_PRESTADOR'
      Size = 100
    end
    object QrSrvTomRetItsNO_INTERMEDIARIO: TWideStringField
      FieldName = 'NO_INTERMEDIARIO'
      Size = 100
    end
    object QrSrvTomRetItsNO_DpsItemListaServico: TWideStringField
      FieldName = 'NO_DpsItemListaServico'
      Size = 255
    end
    object QrSrvTomRetItsNO_cnae21Cad: TWideStringField
      FieldName = 'NO_cnae21Cad'
      Size = 255
    end
    object QrSrvTomRetItsNO_NfsRpsIDTipo: TWideStringField
      FieldName = 'NO_NfsRpsIDTipo'
      Size = 60
    end
    object QrSrvTomRetItsNO_DpsIssRetido: TWideStringField
      FieldName = 'NO_DpsIssRetido'
      Size = 6
    end
    object QrSrvTomRetItsNO_DpsResponsavelRetencao: TWideStringField
      FieldName = 'NO_DpsResponsavelRetencao'
      Size = 60
    end
    object QrSrvTomRetItsTIFEM_0: TIntegerField
      FieldName = 'TIFEM_0'
    end
    object QrSrvTomRetItsTIFEM_1: TIntegerField
      FieldName = 'TIFEM_1'
    end
    object QrSrvTomRetItsTIFEM_2: TIntegerField
      FieldName = 'TIFEM_2'
    end
    object QrSrvTomRetItsTIFEM_3: TIntegerField
      FieldName = 'TIFEM_3'
    end
    object QrSrvTomRetItsTIFEM_4: TIntegerField
      FieldName = 'TIFEM_4'
    end
    object QrSrvTomRetItsTIFEM_5: TIntegerField
      FieldName = 'TIFEM_5'
    end
    object QrSrvTomRetItsTIFEM_6: TIntegerField
      FieldName = 'TIFEM_6'
    end
    object QrSrvTomRetItsTIFEM_7: TIntegerField
      FieldName = 'TIFEM_7'
    end
    object QrSrvTomRetItsTIFEM_8: TIntegerField
      FieldName = 'TIFEM_8'
    end
    object QrSrvTomRetItsRetValorPis: TFloatField
      FieldName = 'RetValorPis'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsRetValorCofins: TFloatField
      FieldName = 'RetValorCofins'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsRetValorInss: TFloatField
      FieldName = 'RetValorInss'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsRetValorIr: TFloatField
      FieldName = 'RetValorIr'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsRetValorCsll: TFloatField
      FieldName = 'RetValorCsll'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsRetValorISS: TFloatField
      FieldName = 'RetValorISS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsRetValorTotal: TFloatField
      FieldName = 'RetValorTotal'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsRetOutrasRetencoes: TFloatField
      FieldName = 'RetOutrasRetencoes'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsValorLiqSemRet: TFloatField
      FieldName = 'ValorLiqSemRet'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvTomRetItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsSrvTomRetIts: TDataSource
    DataSet = QrSrvTomRetIts
    Left = 444
    Top = 113
  end
  object PMFin: TPopupMenu
    OnPopup = PMFinPopup
    Left = 780
    Top = 496
    object FinInclui1: TMenuItem
      Caption = '&Inclui lan'#231'amento financeiro a pagar ao formecedor'
      Enabled = False
      OnClick = FinInclui1Click
    end
    object FimAltera1: TMenuItem
      Caption = 
        '&Altera o lan'#231'amento financeiro a pagar ao formecedor selecionad' +
        'o'
      Enabled = False
      OnClick = FimAltera1Click
    end
    object FinExclui1: TMenuItem
      Caption = 
        '&Exclui o lan'#231'amento financeiro a pagar ao formecedor selecionad' +
        'o'
      Enabled = False
      OnClick = FinExclui1Click
    end
  end
  object QrEmissI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 344
    Top = 56
    object QrEmissIData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissITipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmissICarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrEmissIAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrEmissIGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrEmissIDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 128
    end
    object QrEmissINotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrEmissIDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissICredito: TFloatField
      FieldName = 'Credito'
    end
    object QrEmissICompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissIDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrEmissISit: TIntegerField
      FieldName = 'Sit'
    end
    object QrEmissIVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmissIFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrEmissIFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrEmissINOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrEmissINOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 10
      Calculated = True
    end
    object QrEmissINOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrEmissIID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrEmissISub: TIntegerField
      FieldName = 'Sub'
    end
    object QrEmissIFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrEmissIBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrEmissILocal: TIntegerField
      FieldName = 'Local'
    end
    object QrEmissICartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrEmissILinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrEmissIPago: TFloatField
      FieldName = 'Pago'
    end
    object QrEmissIMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEmissIFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrEmissICliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEmissIControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmissIID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrEmissICliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmissIQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmissIFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrEmissIOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrEmissILancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrEmissIForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrEmissIMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrEmissIMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrEmissIProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrEmissIDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrEmissICtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrEmissINivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrEmissIVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrEmissIAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrEmissIICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrEmissIICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrEmissIDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrEmissIDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEmissIDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrEmissIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmissIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmissIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmissIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmissIEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrEmissIContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrEmissICNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrEmissIFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEmissIAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrEmissISerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrEmissIGenCtb: TIntegerField
      FieldName = 'GenCtb'
    end
    object QrEmissIGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrEmissIGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
  end
  object DsEmissI: TDataSource
    DataSet = QrEmissI
    Left = 344
    Top = 104
  end
  object QrSumI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 344
    Top = 153
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumIDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrSrvTomTaxCad: TMySQLQuery
    SQL.Strings = (
      'SELECT GenCtbD, GenCtbC'
      'FROM SrvTomTaxCad'
      'WHERE TIFEM=1')
    Left = 880
    Top = 144
    object QrSrvTomTaxCadGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrSrvTomTaxCadGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
  end
end
