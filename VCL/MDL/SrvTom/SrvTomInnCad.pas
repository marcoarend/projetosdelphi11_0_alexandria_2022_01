unit SrvTomInnCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkValUsu, dmkLabelRotate, dmkMemo, dmkRadioGroup, DmkDAC_PF,
  //NFSe_PF_0000,
  //DMKpnfsConversao, 2023-12-19
  dmkCompoStore, UnDmkEnums, dmkCheckBox;

type
  TFmSrvTomInnCad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrPrestadores: TMySQLQuery;
    QrPrestadoresCodigo: TIntegerField;
    QrPrestadoresNOMEENTIDADE: TWideStringField;
    DsPrestadores: TDataSource;
    VUEmpresa: TdmkValUsu;
    QrIntermediarios: TmySQLQuery;
    DsIntermediarios: TDataSource;
    QrIntermediariosCodigo: TIntegerField;
    QrIntermediariosNOMEENTIDADE: TWideStringField;
    QrTsTipoRps1: TmySQLQuery;
    DsTsTipoRps1: TDataSource;
    QrTsTipoRps1Codigo: TIntegerField;
    QrTsTipoRps1Nome: TWideStringField;
    QrTsResponsavelRetencao: TmySQLQuery;
    DsTsResponsavelRetencao: TDataSource;
    QrTsResponsavelRetencaoCodigo: TIntegerField;
    QrTsResponsavelRetencaoNome: TWideStringField;
    QrTsExigibilidadeIss: TmySQLQuery;
    DsTsExigibilidadeIss: TDataSource;
    QrTsExigibilidadeIssCodigo: TIntegerField;
    QrTsExigibilidadeIssNome: TWideStringField;
    QrCNAE21Cad: TmySQLQuery;
    DsCNAE21Cad: TDataSource;
    QrDTB_Munici: TmySQLQuery;
    QrDTB_MuniciCodigo: TIntegerField;
    QrDTB_MuniciNome: TWideStringField;
    DsDTB_Munici: TDataSource;
    QrCNAE21CadCodAlf: TWideStringField;
    QrCNAE21CadNome: TWideStringField;
    QrTsRegimeEspecialTributacao: TmySQLQuery;
    QrTsRegimeEspecialTributacaoCodigo: TIntegerField;
    QrTsRegimeEspecialTributacaoNome: TWideStringField;
    QrTsSimNao2: TmySQLQuery;
    QrTsSimNao2Codigo: TIntegerField;
    QrTsSimNao2Nome: TWideStringField;
    DsTsRegimeEspecialTributacao: TDataSource;
    QrTsSimNao1: TmySQLQuery;
    QrTsSimNao1Codigo: TIntegerField;
    QrTsSimNao1Nome: TWideStringField;
    DsTsSimNao1: TDataSource;
    DsTsSimNao2: TDataSource;
    QrTsTipoRps2: TmySQLQuery;
    DsTsTipoRps2: TDataSource;
    QrTsTipoRps2Codigo: TIntegerField;
    QrTsTipoRps2Nome: TWideStringField;
    QrTsSimNao3: TmySQLQuery;
    DsTsSimNao3: TDataSource;
    QrTsSimNao3Codigo: TIntegerField;
    QrTsSimNao3Nome: TWideStringField;
    CSTabSheetChamou: TdmkCompoStore;
    QrCNAE21CadCodigo: TIntegerField;
    QrPrestadoresCEP: TIntegerField;
    QrListServ: TMySQLQuery;
    QrListServNome: TWideStringField;
    QrListServCodAlf: TWideStringField;
    QrListServCodigo: TIntegerField;
    DsListServ: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel12: TPanel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    TabSheet2: TTabSheet;
    Panel13: TPanel;
    Panel14: TPanel;
    GroupBox6: TGroupBox;
    SpeedButton6: TSpeedButton;
    Label39: TLabel;
    Label38: TLabel;
    EdExigibilidadeIss: TdmkEditCB;
    CBExigibilidadeIss: TdmkDBLookupComboBox;
    EdNumeroProcesso: TdmkEdit;
    GroupBox8: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    EdConstrucaoCivilCodigoObra: TdmkEdit;
    EdConstrucaoCivilArt: TdmkEdit;
    Panel6: TPanel;
    GroupBox7: TGroupBox;
    SbMunicipioIncidencia: TSpeedButton;
    Label22: TLabel;
    EdQuemPagaISS: TdmkEditCB;
    CBQuemPagaISS: TdmkDBLookupComboBox;
    GroupBox11: TGroupBox;
    SpeedButton8: TSpeedButton;
    Label25: TLabel;
    EdOptanteSimplesNacional: TdmkEditCB;
    CBOptanteSimplesNacional: TdmkDBLookupComboBox;
    GroupBox12: TGroupBox;
    SpeedButton9: TSpeedButton;
    Label26: TLabel;
    EdIncentivoFiscal: TdmkEditCB;
    CBIncentivoFiscal: TdmkDBLookupComboBox;
    GroupBox9: TGroupBox;
    SpeedButton7: TSpeedButton;
    Label27: TLabel;
    EdRegimeEspecialTributacao: TdmkEditCB;
    CBRegimeEspecialTributacao: TdmkDBLookupComboBox;
    GroupBox10: TGroupBox;
    Label37: TLabel;
    EdCodigoTributacaoMunicipio: TdmkEdit;
    Panel15: TPanel;
    Panel16: TPanel;
    Panel17: TPanel;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label7: TLabel;
    Label32: TLabel;
    Label8: TLabel;
    EdValorServicos: TdmkEdit;
    EdAliquota: TdmkEdit;
    EdDescontoIncondicionado: TdmkEdit;
    EdValorIss: TdmkEdit;
    EdNome: TdmkEdit;
    Label31: TLabel;
    EdCodigo: TdmkEdit;
    Label16: TLabel;


    EdItemListaServico: TdmkEditCB;
    CBItemListaServico: TdmkDBLookupComboBox;
    SbItemListaServico: TSpeedButton;
    Label36: TLabel;
    EdCodigoCnae: TdmkEditCB;
    CBCodigoCnae: TdmkDBLookupComboBox;
    SbCodigoCnae: TSpeedButton;
    TPCompetencia: TdmkEditDateTimePicker;
    TPRpsDataEmissao: TdmkEditDateTimePicker;
    EdRpsHoraEmissao: TdmkEdit;
    EdPrestador: TdmkEditCB;
    CBPrestador: TdmkDBLookupComboBox;
    SbPrestador: TSpeedButton;
    EdIntermediario: TdmkEditCB;
    CBIntermediario: TdmkDBLookupComboBox;
    SbIntermediario: TSpeedButton;
    Label34: TLabel;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Label9: TLabel;
    Label21: TLabel;
    Panel11: TPanel;
    Label1: TLabel;
    Panel5: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    dmkLabelRotate1: TdmkLabelRotate;
    MeDiscriminacao: TdmkMemo;
    Label19: TLabel;
    EdNfsNumero: TdmkEdit;
    EdRpsIdSerie: TdmkEdit;
    Label28: TLabel;
    EdRpsIDTipo: TdmkEditCB;
    CBRpsIDTipo: TdmkDBLookupComboBox;
    SbRpsIDTipo: TSpeedButton;
    Label29: TLabel;
    EdRpsIdNumero: TdmkEdit;
    Label30: TLabel;
    EdNfsBaseCalculo: TdmkEdit;
    Label40: TLabel;
    EdNfsCodigoVerificacao: TdmkEdit;
    Label42: TLabel;
    Label43: TLabel;
    EdNfsOutrasInformacoes: TdmkEdit;
    GroupBox5: TGroupBox;
    SbResponsavelRetencao: TSpeedButton;
    Label35: TLabel;
    Label20: TLabel;
    EdResponsavelRetencao: TdmkEditCB;
    CBResponsavelRetencao: TdmkDBLookupComboBox;
    EdIssRetido: TdmkEditCB;
    CBIssRetido: TdmkDBLookupComboBox;
    Panel10: TPanel;
    GroupBox4: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label41: TLabel;
    EdRetValorPis: TdmkEdit;
    EdRetValorCofins: TdmkEdit;
    EdRetValorInss: TdmkEdit;
    EdRetValorIr: TdmkEdit;
    EdRetValorCsll: TdmkEdit;
    EdRetOutrasRetencoes: TdmkEdit;
    EdNfsValorLiquidoNfse: TdmkEdit;
    EdRetValorIss: TdmkEdit;
    Label2: TLabel;
    EdValorDeducoes: TdmkEdit;
    Label6: TLabel;
    Label3: TLabel;
    EdRetValorTotal: TdmkEdit;
    EdDescontoCondicionado: TdmkEdit;
    Label33: TLabel;
    EdValorLiqSemRet: TdmkEdit;
    Label4: TLabel;

    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbPrestadorClick(Sender: TObject);
    procedure SbItemListaServicoClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbIntermediarioClick(Sender: TObject);
    procedure SbCodigoCnaeClick(Sender: TObject);
    procedure MeDiscriminacaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdValorServicosChange(Sender: TObject);
    procedure EdAliquotaChange(Sender: TObject);
    procedure EdValorDeducoesChange(Sender: TObject);
    procedure EdDescontoIncondicionadoChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdDescontoCondicionadoChange(Sender: TObject);
    procedure EdIssRetidoChange(Sender: TObject);
    procedure EdResponsavelRetencaoChange(Sender: TObject);
    procedure EdRetValorIssChange(Sender: TObject);
    procedure EdRetValorPisChange(Sender: TObject);
    procedure EdRetValorCofinsChange(Sender: TObject);
    procedure EdRetValorInssChange(Sender: TObject);
    procedure EdRetValorIrChange(Sender: TObject);
    procedure EdRetValorCsllChange(Sender: TObject);
    procedure EdRetOutrasRetencoesChange(Sender: TObject);
    procedure EdValorLiqSemRetChange(Sender: TObject);
    procedure EdRetValorTotalChange(Sender: TObject);
  private
    { Private declarations }
    FAtivado: Boolean;
    FServico: Integer;
    //
    (***
    procedure ConfiguraItensMeuServico();
    ***)
    //
    procedure CalculaISS();
    procedure FechaForm();
    procedure CalculaRetencoes();
    procedure IncluiServico();
  public
    { Public declarations }
    FSerie: String;
    FNFSeFatCab, FNumero: Integer;
    FGeraNFSe: Boolean;
    FQrNFSeDPSCab: TmySQLQuery;
    //FFormaGerarNFSe: TnfseFormaGerarNFSe;  2023-12-19
  end;

  var
  FmSrvTomInnCad: TFmSrvTomInnCad;

implementation

uses MyDBCheck, UnMyObjects, Module, ModuleGeral, UMySQLModule,
  CNAE21,
  //NFSe_PF_0201,
  //NFSe_0000_Module,
  NFSe_PF_0000,
  MyGlyfs, Principal;

{$R *.DFM}

procedure TFmSrvTomInnCad.BtOKClick(Sender: TObject);
begin
  IncluiServico();
end;

procedure TFmSrvTomInnCad.BtSaidaClick(Sender: TObject);
begin
  FechaForm();
end;

procedure TFmSrvTomInnCad.CalculaISS();
var
  Base, Aliq, Desc, Dedu, Brut, Valr: Double;
begin
{ Base de c�lculo:
  P�g 20 Manual 2.01 ABRASF
(Valor dos servi�os - Valor
das dedu��es - descontos
incondicionados)}
  Brut := EdValorServicos.ValueVariant;
  Desc := EdDescontoIncondicionado.ValueVariant;
  Dedu := EdValorDeducoes.ValueVariant;
  Base := (Brut - Dedu - Desc) / 100;
  Aliq := EdAliquota.ValueVariant;
  Valr := Geral.RoundC(Base * Aliq, 2);
  //
  EdNfsBaseCalculo.ValueVariant := Base * 100;
  EdValorIss.ValueVariant       := Valr;
end;

procedure TFmSrvTomInnCad.CalculaRetencoes();
var
  ValorServicos, DescontoIncondicionado, DescontoCondicionado, ValorLiqSemRet,
  RetValorIss, RetValorPis, RetValorCofins, RetValorInss, RetValorIr,
  RetValorCsll, RetOutrasRetencoes, RetValorTotal: Double;
begin
  ValorServicos                := EdValorServicos.ValueVariant;
  DescontoIncondicionado       := EdDescontoIncondicionado.ValueVariant;
  DescontoCondicionado         := EdDescontoCondicionado.ValueVariant;
  ValorLiqSemRet               := ValorServicos - DescontoIncondicionado - DescontoCondicionado;

  RetValorIss                  := EdRetValorIss.ValueVariant;
  RetValorPis                  := EdRetValorPis.ValueVariant;
  RetValorCofins               := EdRetValorCofins.ValueVariant;
  RetValorInss                 := EdRetValorInss.ValueVariant;
  RetValorIr                   := EdRetValorIr.ValueVariant;
  RetValorCsll                 := EdRetValorCsll.ValueVariant;
  RetOutrasRetencoes           := EdRetOutrasRetencoes.ValueVariant;
  //RetValorTotal                := EdRetValorTotal.ValueVariant;
  //
  RetValorTotal :=
    RetValorIss + RetValorPis + RetValorCofins + RetValorInss + RetValorIr +
    RetValorCsll + RetOutrasRetencoes;

  EdValorLiqSemRet.ValueVariant      := ValorLiqSemRet;
  EdRetValorTotal.ValueVariant       := RetValorTotal;
  EdNfsValorLiquidoNfse.ValueVariant := ValorLiqSemRet - RetValorTotal;

{ Base de c�lculo:
  P�g 20 Manual 2.01 ABRASF
(Valor dos servi�os - Valor
das dedu��es - descontos
incondicionados)}
(***
  Brut := EdValorServicos.ValueVariant;
  Desc := EdDescontoIncondicionado.ValueVariant;
  Dedu := EdValorDeducoes.ValueVariant;
  Base := (Brut - Dedu - Desc) / 100;
  //
  AlqPIS              := EdAlqPIS.ValueVariant;
  AlqCofins           := EdAlqCofins.ValueVariant;
  AlqINSS             := EdAlqINSS.ValueVariant;
  AlqIR               := EdAlqIR.ValueVariant;
  AlqCSLL             := EdAlqCSLL.ValueVariant;
  AlqOutrasRetencoes  := EdAlqOutrasRetencoes.ValueVariant;
  //
  Valr := Geral.RoundC(Base * AlqPIS, 2);
  EdValorPis.ValueVariant := Valr;
  //
  Valr := Geral.RoundC(Base * AlqCofins, 2);
  EdValorCofins.ValueVariant := Valr;
  //
  Valr := Geral.RoundC(Base * AlqINSS, 2);
  EdValorINSS.ValueVariant := Valr;
  //
  Valr := Geral.RoundC(Base * AlqIR, 2);
  EdValorIR.ValueVariant := Valr;
  //
  Valr := Geral.RoundC(Base * AlqCSLL, 2);
  EdValorCSLL.ValueVariant := Valr;
  //
  Valr := Geral.RoundC(Base * AlqOutrasRetencoes, 2);
  EdOutrasRetencoes.ValueVariant := Valr;
  //
***)
end;

procedure TFmSrvTomInnCad.EdAliquotaChange(Sender: TObject);
begin
  CalculaISS();
end;

procedure TFmSrvTomInnCad.EdDescontoCondicionadoChange(Sender: TObject);
begin
  CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.EdDescontoIncondicionadoChange(Sender: TObject);
begin
  if EdDescontoIncondicionado.Focused then
  begin
    CalculaISS();
    //CalculaRetencoes();
  end;
  CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.EdEmpresaChange(Sender: TObject);
var
  Empresa: Integer;
begin
  (*Empresa*)    DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DModG.ReopenParamsEmp(Empresa);
end;

procedure TFmSrvTomInnCad.EdIssRetidoChange(Sender: TObject);
begin
  CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.EdResponsavelRetencaoChange(Sender: TObject);
begin
  CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.EdRetOutrasRetencoesChange(Sender: TObject);
begin
  CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.EdRetValorCofinsChange(Sender: TObject);
begin
  CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.EdRetValorCsllChange(Sender: TObject);
begin
  CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.EdRetValorInssChange(Sender: TObject);
begin
  CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.EdRetValorIrChange(Sender: TObject);
begin
  CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.EdRetValorIssChange(Sender: TObject);
begin
  CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.EdRetValorPisChange(Sender: TObject);
begin
  CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.EdRetValorTotalChange(Sender: TObject);
begin
  if EdRetValorTotal.Focused then
      CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.EdValorDeducoesChange(Sender: TObject);
begin
  if EdValorDeducoes.Focused then
  begin
    CalculaISS();
  end;
end;

procedure TFmSrvTomInnCad.EdValorLiqSemRetChange(Sender: TObject);
begin
  if EdValorLiqSemRet.Focused then
      CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.EdValorServicosChange(Sender: TObject);
begin
  if EdValorServicos.Focused then
  begin
    CalculaISS();
  end;
  CalculaRetencoes();
end;

procedure TFmSrvTomInnCad.FechaForm;
begin
  Close;
end;

procedure TFmSrvTomInnCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  try
    if FAtivado = False then
    begin
      FAtivado := True;
      EdEmpresa.SetFocus;
    end;
  except
    // nada
  end;
end;

procedure TFmSrvTomInnCad.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  FAtivado := False;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa, DModG.QrEmpresas, 'Filial');
  FServico := 0;
  FNFSeFatCab := 0;
  FGeraNFSe := True;
  //
  FSerie  := '';
  FNumero := 0;
  //
  UMyMod.AbreQuery(QrPrestadores, Dmod.MyDB);
  UMyMod.AbreQuery(QrIntermediarios, Dmod.MyDB);
  //UMyMod.AbreQuery(QrNFSeSrvCad, Dmod.MyDB);
  //
  UMyMod.AbreQuery(QrListServ, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrCNAE21Cad, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsResponsavelRetencao, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsTipoRps1, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsTipoRps2, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsExigibilidadeIss, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsSimNao1, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsSimNao2, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsSimNao3, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsRegimeEspecialTributacao, DmodG.AllID_DB);
  //
  Agora := DModG.ObtemAgora();
  TPCompetencia.Date := Agora;
  TPRpsDataEmissao.Date := Agora;
  EdRpsHoraEmissao.Text := Geral.FDT(Agora, 100);
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmSrvTomInnCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvTomInnCad.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmSrvTomInnCad.IncluiServico();
var
  Nome, NfsRpsIDSerie, NfsCodigoVerificacao, NfsDataEmissao, NfsOutrasInformacoes,
  DpsRpsIDSerie, DpsItemListaServico, DpsCodigoCnae, DpsCodigoTributacaoMunicipio,
  DpsDiscriminacao, DpsNumeroProcesso, DpsConstrucaoCivilCodigoObra,
  DpsConstrucaoCivilArt, DpsInfID_ID: String;
  Codigo, Empresa, Prestador, Intermediario, Status, NfsRpsIDTipo, NfsRpsIDNumero,
  DpsRpsIDNumero, DpsIssRetido, DpsResponsavelRetencao, DpsCodigoMunicipio,
  DpsCodigoPais, DpsExigibilidadeIss, DpsNaturezaOperacao, DpsMunicipioIncidencia,
  DpsRegimeEspecialTributacao, DpsOptanteSimplesNacional, DpsIncentivoFiscal: Integer;
  NfsNumero, NfsNfseSubstituida, NfsBaseCalculo, NfsAliquota, NfsValorIss,
  NfsValorLiquidoNfse, NfsValorCredito, DpsValorServicos, DpsValorDeducoes,
  DpsValorPis, DpsValorCofins, DpsValorInss, DpsValorIr, DpsValorCsll,
  DpsOutrasRetencoes, DpsDescontoIncondicionado, DpsDescontoCondicionado,
  RetValorIss, RetValorPis, RetValorCofins, RetValorInss, RetValorIr,
  RetValorCsll, RetOutrasRetencoes, RetValorTotal, ValorLiqSemRet: Double;
  SQLType: TSQLType;
  DataHora: TDateTime;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  if MyObjects.FIC_Edit_Int(EdPrestador, 'Informe o prestador (Fornecedor)!', Prestador) then Exit;
  //

  Intermediario                := EdIntermediario.ValueVariant;
  Prestador                    := EdPrestador.ValueVariant;
  NfsCodigoVerificacao         := EdNfsCodigoVerificacao.ValueVariant;
  NfsOutrasInformacoes         := EdNfsOutrasInformacoes.ValueVariant;
  DpsRpsIDSerie                := ''; //EdDpsRpsIDSerie.ValueVariant;
  DpsRpsIDNumero               := 0; //?????????????
  DpsItemListaServico          := EdItemListaServico.ValueVariant;
  DpsCodigoCnae                := EdCodigoCnae.ValueVariant;
  DpsCodigoTributacaoMunicipio := EdCodigoTributacaoMunicipio.ValueVariant;
  DpsDiscriminacao             := MeDiscriminacao.Text;
  DpsNumeroProcesso            := EdNumeroProcesso     .ValueVariant;
  DpsConstrucaoCivilCodigoObra := EdConstrucaoCivilCodigoObra.Text;
  DpsConstrucaoCivilArt        := EdConstrucaoCivilArt.Text;
  DpsInfID_ID                  := ''; // ??????????
  //
  Status                       := Integer(tsStatusRps_Normal);
  NfsRpsIDTipo                 := EdRpsIDTipo.ValueVariant;
  NfsNumero                    := EdNfsNumero.ValueVariant;
  NfsRpsIDSerie                := EdRpsIDSerie.ValueVariant;
  NfsRpsIDNumero               := EdRpsIDNumero.ValueVariant;
  DpsIssRetido                 := EdIssRetido.ValueVariant;
  DpsResponsavelRetencao       := EdResponsavelRetencao.ValueVariant;
  //DpsCodigoMunicipio           := EdCodigoMunicipio.ValueVariant;
  //DpsCodigoPais                := EdCodigoPais         .ValueVariant;
  DpsExigibilidadeISS          := EdExigibilidadeISS   .ValueVariant;
  //QuemPagaISS               := EdQuemPagaISS.ValueVariant;
  DpsNaturezaOperacao          := -1;
  DpsMunicipioIncidencia       := -1;
  DpsRegimeEspecialTributacao  := EdRegimeEspecialTributacao.ValueVariant;
  DpsOptanteSimplesNacional    := EdOptanteSimplesNacional.ValueVariant;
  DpsIncentivoFiscal           := EdIncentivoFiscal.ValueVariant;

  //RpsID          := ;
  DataHora := Trunc(TPRpsDataEmissao.Date) + EdRpsHoraEmissao.ValueVariant;
  NfsDataEmissao               := Geral.FDT(DataHora, 109);
  //DpsCompetencia               := TPCompetencia.Date;
  //DpsSubstNumero               := EdSubstNumero.ValueVariant;
  //DpsSubstSerie                := EdSubstSerie.Text;
  //DpsSubstTipo                 := EdSubstTipo.ValueVariant;
  DpsValorServicos             := EdValorServicos.ValueVariant;
  DpsValorDeducoes             := EdValorDeducoes.ValueVariant;
  DpsValorPis                  := 0.00; // EdValorPis.ValueVariant;
  DpsValorCofins               := 0.00; // EdValorCofins.ValueVariant;
  DpsValorInss                 := 0.00; // EdValorInss.ValueVariant;
  DpsValorIr                   := 0.00; // EdValorIr.ValueVariant;
  DpsValorCsll                 := 0.00; // EdValorCsll.ValueVariant;
  DpsOutrasRetencoes           := 0.00; // EdOutrasRetencoes.ValueVariant;
  NfsNfseSubstituida           := 0.00;
  NfsBaseCalculo               := EdNfsBaseCalculo.ValueVariant;
  NfsAliquota                  := EdAliquota.ValueVariant;
  NfsValorIss                  := EdValorIss.ValueVariant;
  NfsValorLiquidoNfse          := EdNfsValorLiquidoNfse.ValueVariant;
  NfsValorCredito              := 0.00;
  DpsDescontoIncondicionado    := EdDescontoIncondicionado.ValueVariant;
  DpsDescontoCondicionado      := EdDescontoCondicionado.ValueVariant;
  //
  RetValorIss                  := EdRetValorIss.ValueVariant;
  RetValorPis                  := EdRetValorPis.ValueVariant;
  RetValorCofins               := EdRetValorCofins.ValueVariant;
  RetValorInss                 := EdRetValorInss.ValueVariant;
  RetValorIr                   := EdRetValorIr.ValueVariant;
  RetValorCsll                 := EdRetValorCsll.ValueVariant;
  RetOutrasRetencoes           := EdRetOutrasRetencoes.ValueVariant;
  RetValorTotal                := EdRetValorTotal.ValueVariant;
  //
  ValorLiqSemRet               := EdValorLiqSemRet.ValueVariant;
  //
  Codigo := UMyMod.BPGS1I32('srvtominncab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'srvtominncab', False, [
  'Empresa', 'Prestador', 'Intermediario',
  'Nome', 'Status', 'NfsNumero',
  'NfsRpsIDSerie', 'NfsRpsIDTipo', 'NfsRpsIDNumero',
  'NfsCodigoVerificacao', 'NfsDataEmissao', 'NfsNfseSubstituida',
  'NfsOutrasInformacoes', 'NfsBaseCalculo', 'NfsAliquota',
  'NfsValorIss', 'NfsValorLiquidoNfse', 'NfsValorCredito',
  'DpsRpsIDNumero', 'DpsRpsIDSerie', 'DpsValorServicos',
  'DpsValorDeducoes', 'DpsValorPis', 'DpsValorCofins',
  'DpsValorInss', 'DpsValorIr', 'DpsValorCsll',
  'DpsOutrasRetencoes', 'DpsDescontoIncondicionado', 'DpsDescontoCondicionado',
  'DpsIssRetido', 'DpsResponsavelRetencao', 'DpsItemListaServico',
  'DpsCodigoCnae', 'DpsCodigoTributacaoMunicipio', 'DpsDiscriminacao',
  'DpsCodigoMunicipio', 'DpsCodigoPais', 'DpsExigibilidadeIss',
  'DpsNaturezaOperacao', 'DpsMunicipioIncidencia', 'DpsNumeroProcesso',
  'DpsConstrucaoCivilCodigoObra', 'DpsConstrucaoCivilArt', 'DpsRegimeEspecialTributacao',
  'DpsOptanteSimplesNacional', 'DpsIncentivoFiscal', 'DpsInfID_ID',
  'RetValorIss', 'RetValorPis', 'RetValorCofins',
  'RetValorInss', 'RetValorIr', 'RetValorCsll',
  'RetOutrasRetencoes', 'RetValorTotal', 'ValorLiqSemRet'], [
  'Codigo'], [
  Empresa, Prestador, Intermediario,
  Nome, Status, NfsNumero,
  NfsRpsIDSerie, NfsRpsIDTipo, NfsRpsIDNumero,
  NfsCodigoVerificacao, NfsDataEmissao, NfsNfseSubstituida,
  NfsOutrasInformacoes, NfsBaseCalculo, NfsAliquota,
  NfsValorIss, NfsValorLiquidoNfse, NfsValorCredito,
  DpsRpsIDNumero, DpsRpsIDSerie, // n�o habilitados
  DpsValorServicos,
  DpsValorDeducoes, DpsValorPis, DpsValorCofins,
  DpsValorInss, DpsValorIr, DpsValorCsll,
  DpsOutrasRetencoes, DpsDescontoIncondicionado, DpsDescontoCondicionado,
  DpsIssRetido, DpsResponsavelRetencao, DpsItemListaServico,
  DpsCodigoCnae, DpsCodigoTributacaoMunicipio, DpsDiscriminacao,
  DpsCodigoMunicipio, DpsCodigoPais, DpsExigibilidadeIss,
  DpsNaturezaOperacao, DpsMunicipioIncidencia, DpsNumeroProcesso,
  DpsConstrucaoCivilCodigoObra, DpsConstrucaoCivilArt, DpsRegimeEspecialTributacao,
  DpsOptanteSimplesNacional, DpsIncentivoFiscal, DpsInfID_ID,
  RetValorIss, RetValorPis, RetValorCofins,
  RetValorInss, RetValorIr, RetValorCsll,
  RetOutrasRetencoes, RetValorTotal, ValorLiqSemRet], [
  Codigo], True) then
  begin
    Close;
  end;
end;

procedure TFmSrvTomInnCad.MeDiscriminacaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if Key in ([9, 10, 13]) then
   Key := 0;
end;

procedure TFmSrvTomInnCad.SbPrestadorClick(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdPrestador.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  UMyMod.AbreQuery(QrPrestadores, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPrestador, CBPrestador, QrPrestadores, VAR_CADASTRO, 'Codigo');
    EdPrestador.SetFocus;
  end;
end;

procedure TFmSrvTomInnCad.SbCodigoCnaeClick(Sender: TObject);
var
  CNAE: String;
  CNAEInt: Integer;
begin
(***
  VAR_CADASTROX := '';
  CNAE          := EdCodigoCnae.ValueVariant;
  //
  if CNAE <> '' then
    CNAEInt := QrCNAE21CadCodigo.Value
  else
    CNAEInt := 0;
  //
  UnNFSe_PF_0000.MostraFormCNAE21Cad(CNAEInt);
  //
  UMyMod.AbreQuery(QrCNAE21Cad, DmodG.AllID_DB);
  //
  if VAR_CADASTROX <> '' then
  begin
    UMyMod.SetaCodTxtPesquisado(EdCodigoCnae, CBCodigoCnae, QrCNAE21Cad, VAR_CADASTROX, 'CodTxt');
    EdCodigoCnae.SetFocus;
  end;
***)
end;

procedure TFmSrvTomInnCad.SbIntermediarioClick(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdIntermediario.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  UMyMod.AbreQuery(QrIntermediarios, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdIntermediario, CBIntermediario, QrIntermediarios, VAR_CADASTRO, 'Codigo');
    EdIntermediario.SetFocus;
  end;
end;

procedure TFmSrvTomInnCad.SbItemListaServicoClick(Sender: TObject);
var
  Servico: String;
  ServicoInt: Integer;
begin
  VAR_CADASTROX := '';
  Servico       := EdItemListaServico.ValueVariant;
  //
  if Servico <> '' then
    ServicoInt := QrListServCodigo.Value
  else
    ServicoInt := 0;
  //
  UnNFSe_PF_0000.MostraFormListServ(ServicoInt);
  //
  UMyMod.AbreQuery(QrListServ, DModG.AllID_DB);
  //
  if VAR_CADASTROX <> '' then
  begin
    UMyMod.SetaCodTxtPesquisado(EdItemListaServico, CBItemListaServico, QrListServ, VAR_CADASTROX, 'Codigo');
    EdItemListaServico.SetFocus;
  end;
end;

(*
object GroupBox2: TGroupBox
  Left = 6
  Top = 4
  Width = 411
  Height = 60
  Caption = ' Recibo Provis'#243'rio de Servi'#231'o (RPS) Substitu'#237'do: '
  Enabled = False
  TabOrder = 0
  object Label2: TLabel
    Left = 8
    Top = 16
    Width = 40
    Height = 13
    Caption = 'N'#250'mero:'
    Enabled = False
  end
  object Label3: TLabel
    Left = 106
    Top = 16
    Width = 27
    Height = 13
    Caption = 'S'#233'rie:'
    Enabled = False
  end
  object Label4: TLabel
    Left = 170
    Top = 16
    Width = 64
    Height = 13
    Caption = 'Tipo de RPS:'
    Enabled = False
  end
  object SbSubstTipo: TSpeedButton
    Left = 378
    Top = 31
    Width = 20
    Height = 21
    Hint = 'Inclui item de carteira'
    Caption = '...'
    Enabled = False
  end
  object EdSubstNumero: TdmkEdit
    Left = 8
    Top = 31
    Width = 96
    Height = 21
    Alignment = taRightJustify
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    FormatType = dmktfInteger
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    Texto = '0'
    UpdType = utInc
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = 0
    ValWarn = False
  end
  object EdSubstSerie: TdmkEdit
    Left = 106
    Top = 31
    Width = 60
    Height = 21
    Enabled = False
    TabOrder = 1
    FormatType = dmktfString
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    QryCampo = 'NumOC'
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
    ValWarn = False
  end
  object EdSubstTipo: TdmkEditCB
    Left = 174
    Top = 31
    Width = 19
    Height = 21
    Alignment = taRightJustify
    Enabled = False
    TabOrder = 2
    FormatType = dmktfInteger
    MskType = fmtNone
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ValMin = '-2147483647'
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    Texto = '0'
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = 0
    ValWarn = False
    DBLookupComboBox = CBSubstTipo
    IgnoraDBLookupComboBox = False
    AutoSetIfOnlyOneReg = setregOnlyManual
  end
  object CBSubstTipo: TdmkDBLookupComboBox
    Left = 193
    Top = 31
    Width = 186
    Height = 21
    Enabled = False
    KeyField = 'Codigo'
    ListField = 'Nome'
    ListSource = DsTsTipoRps2
    TabOrder = 3
    dmkEditCB = EdSubstTipo
    UpdType = utYes
    LocF7SQLMasc = '$#'
    LocF7PreDefProc = f7pNone
  end
end
*)

end.
