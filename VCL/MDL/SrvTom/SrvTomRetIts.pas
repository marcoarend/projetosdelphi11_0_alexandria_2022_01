unit SrvTomRetIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Grids, Vcl.DBGrids;

type
  TFmSrvTomRetIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesCodUsu: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    Panel3: TPanel;
    Label6: TLabel;
    EdCodigo: TdmkEdit;
    Label7: TLabel;
    EdNO_Imposto: TdmkEdit;
    Ed_Sel_: TdmkEditCB;
    QrSrvTomInnCab: TMySQLQuery;
    QrSrvTomInnCabCodigo: TIntegerField;
    QrSrvTomInnCabEmpresa: TIntegerField;
    QrSrvTomInnCabPrestador: TIntegerField;
    QrSrvTomInnCabIntermediario: TIntegerField;
    QrSrvTomInnCabNome: TWideStringField;
    QrSrvTomInnCabStatus: TSmallintField;
    QrSrvTomInnCabNfsNumero: TLargeintField;
    QrSrvTomInnCabNfsRpsIDSerie: TWideStringField;
    QrSrvTomInnCabNfsRpsIDTipo: TSmallintField;
    QrSrvTomInnCabNfsRpsIDNumero: TIntegerField;
    QrSrvTomInnCabNfsCodigoVerificacao: TWideStringField;
    QrSrvTomInnCabNfsDataEmissao: TDateTimeField;
    QrSrvTomInnCabNfsNfseSubstituida: TLargeintField;
    QrSrvTomInnCabNfsOutrasInformacoes: TWideStringField;
    QrSrvTomInnCabNfsBaseCalculo: TFloatField;
    QrSrvTomInnCabNfsAliquota: TFloatField;
    QrSrvTomInnCabNfsValorIss: TFloatField;
    QrSrvTomInnCabNfsValorLiquidoNfse: TFloatField;
    QrSrvTomInnCabNfsValorCredito: TFloatField;
    QrSrvTomInnCabDpsRpsIDNumero: TIntegerField;
    QrSrvTomInnCabDpsRpsIDSerie: TWideStringField;
    QrSrvTomInnCabDpsValorServicos: TFloatField;
    QrSrvTomInnCabDpsValorDeducoes: TFloatField;
    QrSrvTomInnCabDpsValorPis: TFloatField;
    QrSrvTomInnCabDpsValorCofins: TFloatField;
    QrSrvTomInnCabDpsValorInss: TFloatField;
    QrSrvTomInnCabDpsValorIr: TFloatField;
    QrSrvTomInnCabDpsValorCsll: TFloatField;
    QrSrvTomInnCabDpsOutrasRetencoes: TFloatField;
    QrSrvTomInnCabDpsDescontoIncondicionado: TFloatField;
    QrSrvTomInnCabDpsDescontoCondicionado: TFloatField;
    QrSrvTomInnCabDpsIssRetido: TSmallintField;
    QrSrvTomInnCabDpsResponsavelRetencao: TIntegerField;
    QrSrvTomInnCabDpsItemListaServico: TWideStringField;
    QrSrvTomInnCabDpsCodigoCnae: TWideStringField;
    QrSrvTomInnCabDpsCodigoTributacaoMunicipio: TWideStringField;
    QrSrvTomInnCabDpsDiscriminacao: TWideMemoField;
    QrSrvTomInnCabDpsCodigoMunicipio: TIntegerField;
    QrSrvTomInnCabDpsCodigoPais: TIntegerField;
    QrSrvTomInnCabDpsExigibilidadeIss: TSmallintField;
    QrSrvTomInnCabDpsNaturezaOperacao: TIntegerField;
    QrSrvTomInnCabDpsMunicipioIncidencia: TIntegerField;
    QrSrvTomInnCabDpsNumeroProcesso: TWideStringField;
    QrSrvTomInnCabDpsConstrucaoCivilCodigoObra: TWideStringField;
    QrSrvTomInnCabDpsConstrucaoCivilArt: TWideStringField;
    QrSrvTomInnCabDpsRegimeEspecialTributacao: TSmallintField;
    QrSrvTomInnCabDpsOptanteSimplesNacional: TSmallintField;
    QrSrvTomInnCabDpsIncentivoFiscal: TSmallintField;
    QrSrvTomInnCabDpsInfID_ID: TWideStringField;
    QrSrvTomInnCabLk: TIntegerField;
    QrSrvTomInnCabDataCad: TDateField;
    QrSrvTomInnCabDataAlt: TDateField;
    QrSrvTomInnCabUserCad: TIntegerField;
    QrSrvTomInnCabUserAlt: TIntegerField;
    QrSrvTomInnCabAlterWeb: TSmallintField;
    QrSrvTomInnCabAWServerID: TIntegerField;
    QrSrvTomInnCabAWStatSinc: TSmallintField;
    QrSrvTomInnCabAtivo: TSmallintField;
    QrSrvTomInnCabFilial: TIntegerField;
    QrSrvTomInnCabNO_EMPRESA: TWideStringField;
    QrSrvTomInnCabNO_PRESTADOR: TWideStringField;
    QrSrvTomInnCabNO_INTERMEDIARIO: TWideStringField;
    QrSrvTomInnCabNO_DpsItemListaServico: TWideStringField;
    QrSrvTomInnCabNO_cnae21Cad: TWideStringField;
    QrSrvTomInnCabNO_NfsRpsIDTipo: TWideStringField;
    QrSrvTomInnCabNO_DpsIssRetido: TWideStringField;
    QrSrvTomInnCabNO_DpsResponsavelRetencao: TWideStringField;
    DsSrvTomInnCab: TDataSource;
    DGDados: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo, FEmpresa, FTIFEM: Integer;
    FCampo: String;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    procedure ReopenAptos();
  end;

  var
  FmSrvTomRetIts: TFmSrvTomRetIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnSrvTom_PF;

{$R *.DFM}

procedure TFmSrvTomRetIts.BtOKClick(Sender: TObject);
  procedure InsereItemAtual();
  var
    Codigo, Controle, SrvTomInnCab: Integer;
    SQLTypeOri, SQLTypeDst: TSQLType;
  begin
    SQLTypeOri     := stUpd;
    SQLTypeDst     := stIns;
    //
    Codigo         := FCodigo;
    Controle       := 0;
    SrvTomInnCab   := QrSrvTomInnCabCodigo.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeOri, 'srvtominncab', False, [
    FCampo], ['Codigo'], [
    FCodigo], [SrvTomInnCab], True) then
    begin
      Controle := UMyMod.BPGS1I32('srvtomretits', 'Controle', '', '', tsPos, SQLTypeDst, Codigo);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTypeDst, 'srvtomretits', False, [
      'Codigo', 'SrvTomInnCab'], [
      'Controle'], [
      Codigo, SrvTomInnCab], [
      Controle], True) then
      ;
    end;
  end;
var
  I, Codigo, Controle: Integer;
  Nome: String;
begin
  if DGDados.SelectedRows.Count < 1 then
  begin
    Geral.MB_Aviso('Nenhum item foi selecionado!');
    Exit;
  end else
  begin
    with DGDados.DataSource.DataSet do
    for I := 0 to DGDados.SelectedRows.Count-1 do
    begin
      GotoBookmark(DGDados.SelectedRows.Items[I]);
      InsereItemAtual();
    end;
    SrvTom_PF.AtiualizaRetValorTotal(Codigo, FTIFEM);
    Close;
  end;
end;

procedure TFmSrvTomRetIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvTomRetIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSrvTomRetIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmSrvTomRetIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvTomRetIts.ReopenAptos();
const
  sProcName = 'TFmSrvTomRetIts.ReopenAptos()';
  Sim = 1;
var
  CampoIdx, CampoVal: String;
begin
  if FTIFEM >= MaxNomFldsImpostoFddEstMun then
  begin
    Geral.MB_Erro('"TIFEM" n�o habilitado: ID = ' + Geral.FF0(FTIFEM) +
    ' em ' + sProcName);
  end else
  begin
    CampoVal := sNomFldsImpostoFddEstMun[FTIFEM];
    CampoIdx := 'TIFEM_' + IntToStr(FTIFEM);
    UnDmkDAC_PF.AbreMySQLQuery0(QrSrvTomInnCab, Dmod.MyDB, [
    'SELECT sic.*, emp.Filial,',
    'IF(emp.Tipo=0, emp.RAZAOSOCIAL, emp.Nome) NO_EMPRESA,',
    'IF(prs.Tipo=0, prs.RAZAOSOCIAL, prs.Nome) NO_PRESTADOR,',
    'IF(itm.Tipo=0, itm.RAZAOSOCIAL, itm.Nome) NO_INTERMEDIARIO,',
    'lsr.Nome NO_DpsItemListaServico, cna.Nome NO_cnae21Cad,',
    'ts1.Nome NO_NfsRpsIDTipo, ts3.Nome NO_DpsIssRetido,',
    'tsr.Nome NO_DpsResponsavelRetencao',
    'FROM srvtominncab sic',
    'LEFT JOIN entidades emp ON emp.Codigo=sic.Empresa',
    'LEFT JOIN entidades prs ON prs.Codigo=sic.Prestador',
    'LEFT JOIN entidades itm ON itm.Codigo=sic.Intermediario',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.listserv  lsr ON lsr.Codigo=sic.DpsItemListaServico',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.cnae21Cad cna ON cna.CodAlf=sic.DpsCodigoCnae',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.tstiporps ts1 ON ts1.Codigo=sic.NfsRpsIDTipo',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.tssimnao  ts3 ON ts3.Codigo=sic.DpsIssRetido',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.tsresponsavelretencao tsr ON tsr.Codigo=sic.DpsResponsavelRetencao',
    'WHERE DpsResponsavelRetencao=1 ',
    'AND DpsIssRetido=1 ',
    'AND sic.' + CampoIdx + ' = 0 ',
    'AND sic.' + CampoVal + ' <> 0 ',
    '']);
    //
    //Geral.MB_Teste(QrSrvTomInnCab.SQL.Text);
  end;
end;

end.
