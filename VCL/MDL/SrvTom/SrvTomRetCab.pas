unit SrvTomRetCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, UnInternalConsts3;

type
  TFmSrvTomRetCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label2: TLabel;
    QrSrvTomRetCab: TMySQLQuery;
    DsSrvTomRetCab: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrSrvTomRetIts: TMySQLQuery;
    QrSrvTomRetItsCodigo: TIntegerField;
    QrSrvTomRetItsEmpresa: TIntegerField;
    QrSrvTomRetItsPrestador: TIntegerField;
    QrSrvTomRetItsIntermediario: TIntegerField;
    QrSrvTomRetItsNome: TWideStringField;
    QrSrvTomRetItsStatus: TSmallintField;
    QrSrvTomRetItsNfsNumero: TLargeintField;
    QrSrvTomRetItsNfsRpsIDSerie: TWideStringField;
    QrSrvTomRetItsNfsRpsIDTipo: TSmallintField;
    QrSrvTomRetItsNfsRpsIDNumero: TIntegerField;
    QrSrvTomRetItsNfsCodigoVerificacao: TWideStringField;
    QrSrvTomRetItsNfsDataEmissao: TDateTimeField;
    QrSrvTomRetItsNfsNfseSubstituida: TLargeintField;
    QrSrvTomRetItsNfsOutrasInformacoes: TWideStringField;
    QrSrvTomRetItsNfsBaseCalculo: TFloatField;
    QrSrvTomRetItsNfsAliquota: TFloatField;
    QrSrvTomRetItsNfsValorIss: TFloatField;
    QrSrvTomRetItsNfsValorLiquidoNfse: TFloatField;
    QrSrvTomRetItsNfsValorCredito: TFloatField;
    QrSrvTomRetItsDpsRpsIDNumero: TIntegerField;
    QrSrvTomRetItsDpsRpsIDSerie: TWideStringField;
    QrSrvTomRetItsDpsValorServicos: TFloatField;
    QrSrvTomRetItsDpsValorDeducoes: TFloatField;
    QrSrvTomRetItsDpsValorPis: TFloatField;
    QrSrvTomRetItsDpsValorCofins: TFloatField;
    QrSrvTomRetItsDpsValorInss: TFloatField;
    QrSrvTomRetItsDpsValorIr: TFloatField;
    QrSrvTomRetItsDpsValorCsll: TFloatField;
    QrSrvTomRetItsDpsOutrasRetencoes: TFloatField;
    QrSrvTomRetItsDpsDescontoIncondicionado: TFloatField;
    QrSrvTomRetItsDpsDescontoCondicionado: TFloatField;
    QrSrvTomRetItsDpsIssRetido: TSmallintField;
    QrSrvTomRetItsDpsResponsavelRetencao: TIntegerField;
    QrSrvTomRetItsDpsItemListaServico: TWideStringField;
    QrSrvTomRetItsDpsCodigoCnae: TWideStringField;
    QrSrvTomRetItsDpsCodigoTributacaoMunicipio: TWideStringField;
    QrSrvTomRetItsDpsDiscriminacao: TWideMemoField;
    QrSrvTomRetItsDpsCodigoMunicipio: TIntegerField;
    QrSrvTomRetItsDpsCodigoPais: TIntegerField;
    QrSrvTomRetItsDpsExigibilidadeIss: TSmallintField;
    QrSrvTomRetItsDpsNaturezaOperacao: TIntegerField;
    QrSrvTomRetItsDpsMunicipioIncidencia: TIntegerField;
    QrSrvTomRetItsDpsNumeroProcesso: TWideStringField;
    QrSrvTomRetItsDpsConstrucaoCivilCodigoObra: TWideStringField;
    QrSrvTomRetItsDpsConstrucaoCivilArt: TWideStringField;
    QrSrvTomRetItsDpsRegimeEspecialTributacao: TSmallintField;
    QrSrvTomRetItsDpsOptanteSimplesNacional: TSmallintField;
    QrSrvTomRetItsDpsIncentivoFiscal: TSmallintField;
    QrSrvTomRetItsDpsInfID_ID: TWideStringField;
    QrSrvTomRetItsLk: TIntegerField;
    QrSrvTomRetItsDataCad: TDateField;
    QrSrvTomRetItsDataAlt: TDateField;
    QrSrvTomRetItsUserCad: TIntegerField;
    QrSrvTomRetItsUserAlt: TIntegerField;
    QrSrvTomRetItsAlterWeb: TSmallintField;
    QrSrvTomRetItsAWServerID: TIntegerField;
    QrSrvTomRetItsAWStatSinc: TSmallintField;
    QrSrvTomRetItsAtivo: TSmallintField;
    QrSrvTomRetItsFilial: TIntegerField;
    QrSrvTomRetItsNO_EMPRESA: TWideStringField;
    QrSrvTomRetItsNO_PRESTADOR: TWideStringField;
    QrSrvTomRetItsNO_INTERMEDIARIO: TWideStringField;
    QrSrvTomRetItsNO_DpsItemListaServico: TWideStringField;
    QrSrvTomRetItsNO_cnae21Cad: TWideStringField;
    QrSrvTomRetItsNO_NfsRpsIDTipo: TWideStringField;
    QrSrvTomRetItsNO_DpsIssRetido: TWideStringField;
    QrSrvTomRetItsNO_DpsResponsavelRetencao: TWideStringField;
    DsSrvTomRetIts: TDataSource;
    RGTIFEM: TdmkRadioGroup;
    Label3: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label21: TLabel;
    TPData: TdmkEditDateTimePicker;
    Label4: TLabel;
    TPCompetencia: TdmkEditDateTimePicker;
    QrSrvTomRetCabCodigo: TIntegerField;
    QrSrvTomRetCabEmpresa: TIntegerField;
    QrSrvTomRetCabFilial: TIntegerField;
    QrSrvTomRetCabNome: TWideStringField;
    QrSrvTomRetCabTIFEM: TSmallintField;
    QrSrvTomRetCabData: TDateField;
    QrSrvTomRetCabCompetencia: TDateField;
    QrSrvTomRetCabNO_EMPRESA: TWideStringField;
    QrSrvTomRetCabNO_TIFEM: TWideStringField;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    dmkEdit2: TdmkEdit;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBRGTIFEM: TDBRadioGroup;
    Splitter1: TSplitter;
    GBPagamentos: TGroupBox;
    GroupBox8: TGroupBox;
    GridF: TDBGrid;
    PMFin: TPopupMenu;
    FinInclui1: TMenuItem;
    FimAltera1: TMenuItem;
    FinExclui1: TMenuItem;
    BtFin: TBitBtn;
    QrSrvTomRetItsTIFEM_0: TIntegerField;
    QrSrvTomRetItsTIFEM_1: TIntegerField;
    QrSrvTomRetItsTIFEM_2: TIntegerField;
    QrSrvTomRetItsTIFEM_3: TIntegerField;
    QrSrvTomRetItsTIFEM_4: TIntegerField;
    QrSrvTomRetItsTIFEM_5: TIntegerField;
    QrSrvTomRetItsTIFEM_6: TIntegerField;
    QrSrvTomRetItsTIFEM_7: TIntegerField;
    QrSrvTomRetItsTIFEM_8: TIntegerField;
    QrSrvTomRetItsRetValorPis: TFloatField;
    QrSrvTomRetItsRetValorCofins: TFloatField;
    QrSrvTomRetItsRetValorInss: TFloatField;
    QrSrvTomRetItsRetValorIr: TFloatField;
    QrSrvTomRetItsRetValorCsll: TFloatField;
    QrSrvTomRetItsRetValorISS: TFloatField;
    QrSrvTomRetItsRetValorTotal: TFloatField;
    QrSrvTomRetItsRetOutrasRetencoes: TFloatField;
    QrSrvTomRetItsValorLiqSemRet: TFloatField;
    QrSrvTomRetItsControle: TIntegerField;
    QrSrvTomRetCabRetValorTotal: TFloatField;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    QrEmissI: TMySQLQuery;
    QrEmissIData: TDateField;
    QrEmissITipo: TSmallintField;
    QrEmissICarteira: TIntegerField;
    QrEmissIAutorizacao: TIntegerField;
    QrEmissIGenero: TIntegerField;
    QrEmissIDescricao: TWideStringField;
    QrEmissINotaFiscal: TIntegerField;
    QrEmissIDebito: TFloatField;
    QrEmissICredito: TFloatField;
    QrEmissICompensado: TDateField;
    QrEmissIDocumento: TFloatField;
    QrEmissISit: TIntegerField;
    QrEmissIVencimento: TDateField;
    QrEmissILk: TIntegerField;
    QrEmissIFatID: TIntegerField;
    QrEmissIFatParcela: TIntegerField;
    QrEmissINOMESIT: TWideStringField;
    QrEmissINOMETIPO: TWideStringField;
    QrEmissINOMECARTEIRA: TWideStringField;
    QrEmissIID_Sub: TSmallintField;
    QrEmissISub: TIntegerField;
    QrEmissIFatura: TWideStringField;
    QrEmissIBanco: TIntegerField;
    QrEmissILocal: TIntegerField;
    QrEmissICartao: TIntegerField;
    QrEmissILinha: TIntegerField;
    QrEmissIPago: TFloatField;
    QrEmissIMez: TIntegerField;
    QrEmissIFornecedor: TIntegerField;
    QrEmissICliente: TIntegerField;
    QrEmissIControle: TIntegerField;
    QrEmissIID_Pgto: TIntegerField;
    QrEmissICliInt: TIntegerField;
    QrEmissIQtde: TFloatField;
    QrEmissIFatID_Sub: TIntegerField;
    QrEmissIOperCount: TIntegerField;
    QrEmissILancto: TIntegerField;
    QrEmissIForneceI: TIntegerField;
    QrEmissIMoraDia: TFloatField;
    QrEmissIMulta: TFloatField;
    QrEmissIProtesto: TDateField;
    QrEmissIDataDoc: TDateField;
    QrEmissICtrlIni: TIntegerField;
    QrEmissINivel: TIntegerField;
    QrEmissIVendedor: TIntegerField;
    QrEmissIAccount: TIntegerField;
    QrEmissIICMS_P: TFloatField;
    QrEmissIICMS_V: TFloatField;
    QrEmissIDuplicata: TWideStringField;
    QrEmissIDepto: TIntegerField;
    QrEmissIDescoPor: TIntegerField;
    QrEmissIDataCad: TDateField;
    QrEmissIDataAlt: TDateField;
    QrEmissIUserCad: TIntegerField;
    QrEmissIUserAlt: TIntegerField;
    QrEmissIEmitente: TWideStringField;
    QrEmissIContaCorrente: TWideStringField;
    QrEmissICNPJCPF: TWideStringField;
    QrEmissIFatNum: TFloatField;
    QrEmissIAgencia: TIntegerField;
    QrEmissISerieNF: TWideStringField;
    QrEmissIGenCtb: TIntegerField;
    QrEmissIGenCtbD: TIntegerField;
    QrEmissIGenCtbC: TIntegerField;
    DsEmissI: TDataSource;
    QrSumI: TMySQLQuery;
    QrSumIDebito: TFloatField;
    QrSrvTomTaxCad: TMySQLQuery;
    QrSrvTomTaxCadGenCtbD: TIntegerField;
    QrSrvTomTaxCadGenCtbC: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSrvTomRetCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSrvTomRetCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrSrvTomRetCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure QrSrvTomRetCabBeforeClose(DataSet: TDataSet);
    procedure BtFinClick(Sender: TObject);
    procedure FimAltera1Click(Sender: TObject);
    procedure FinInclui1Click(Sender: TObject);
    procedure FinExclui1Click(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure PMFinPopup(Sender: TObject);
  private
    FTabLctA: String;
    //FEmpresa: Integer;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraSrvTomRetIts(SQLType: TSQLType);
    //
    procedure IncluiLanctoI(Sender: TObject);
    procedure ExcluiLanctoI();
    procedure DefineVarDup();
    function  CalculaDiferencas(): Double;
    procedure ReopenEmissI(FatParcela: Integer);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure ReopenSrvTomRetIts(Codigo: Integer);

  end;

var
  FmSrvTomRetCab: TFmSrvTomRetCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, DmkDAC_PF,
  SrvTomRetIts, UnSrvTom_PF, UnFinanceiro, UnPagtos;

{$R *.DFM}

const
  FThis_FATID = VAR_FATID_1031;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSrvTomRetCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSrvTomRetCab.FinInclui1Click(Sender: TObject);
begin
  IncluiLanctoI(Sender);
end;

procedure TFmSrvTomRetCab.IncluiLanctoI(Sender: TObject);
const
  Qtde = 0.000;
  Qtd2 = 0.000;
var
  Terceiro, Cod, Genero, Empresa, NF: Integer;
  SerieNF: String;
  Valor: Double;
  GenCtbD, GenCtbC: Integer;
  TypCtbCadMoF: TTypCtbCadMoF;
  Descricao: String;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
(* ini 2022-03-23
  if FmPrincipal.AvisaFaltaDeContaDoPlano(FThis_FATID) then
    Exit;
*)
  //
  DefineVarDup;
  //
  Genero   := Dmod.QrControleCtaPQCompr.Value;
  Cod      := QrSrvTomRetCabCodigo.Value;
  Terceiro := 0; //QrSrvTomRetCabPrestador.Value;
  Valor    := CalculaDiferencas();
  Empresa  := QrSrvTomRetCabEmpresa.Value;
  SerieNF  := '';
  NF       := 0; //QrSrvTomRetCabNfsNumero.Value;
  //
  if Valor = 0 then
    Valor := QrSrvTomRetCabRetValorTotal.Value;
  //

  //GenCtbD := 0;
  //GenCtbC := 0;
  //SrvTom_PF.DefineContasContabeisServicoTomado(GenCtbD, GenCtbC);
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvTomTaxCad, Dmod.MyDB, [
  'SELECT GenCtbD, GenCtbC ',
  'FROM SrvTomTaxCad ',
  'WHERE TIFEM=' + Geral.FF0(QrSrvTomRetCabTIFEM.Value),
  '']);
  GenCtbD := QrSrvTomTaxCadGenCtbD.Value;
  GenCtbC := QrSrvTomTaxCadGenCtbC.Value;
  //
  //
  Descricao := QrSrvTomRetCabNome.Value;
  if Trim(Descricao) = EmptyStr then
    Descricao := 'Servi�o tomado';
  UPagtos.Pagto(QrEmissI, tpDeb, Cod, Terceiro, FThis_FATID, 0(*GenCtbD*), 0(*GenCtbC*), stIns,
    'Servi�o Tomado', Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0,
    0, True, False, 0, 0, 0, 0, NF, FTabLctA, Descricao, SerieNF, Qtde, Qtd2, GenCtbD, GenCtbC);
// fim 2022-03-23
  ReopenEmissI(0);
end;

procedure TFmSrvTomRetCab.FimAltera1Click(Sender: TObject);
begin
//
end;

procedure TFmSrvTomRetCab.FinExclui1Click(Sender: TObject);
begin
  if UFinanceiro.ExcluiLct_FatParcela(QrEmissI, QrEmissIFatID.Value,
    QrEmissIFatNum.Value, QrEmissIFatParcela.Value, QrEmissICarteira.Value,
    QrEmissISit.Value, QrEmissITipo.Value, dmkPF.MotivDel_ValidaCodigo(331),
    FTabLctA)
  then
    ReopenEmissI(QrEmissIFatParcela.Value);
end;

procedure TFmSrvTomRetCab.MostraSrvTomRetIts(SQLType: TSQLType);
const
  sProcName = 'TFmSrvTomRetCab.MostraSrvTomRetIts()';
var
  TIFEM: Integer;
  CampoIdx: String;
begin
  TIFEM := QrSrvTomRetCabTIFEM.Value;
  if TIFEM >= MaxNomFldsImpostoFddEstMun then
  begin
    Geral.MB_Erro('"TIFEM" n�o habilitado: ID = ' + Geral.FF0(TIFEM) +
    ' em ' + sProcName);
  end else
  begin
    if DBCheck.CriaFm(TFmSrvTomRetIts, FmSrvTomRetIts, afmoNegarComAviso) then
    begin
      FmSrvTomRetIts.ImgTipo.SQLType := SQLType;
      FmSrvTomRetIts.FQrCab := QrSrvTomRetCab;
      FmSrvTomRetIts.FDsCab := DsSrvTomRetCab;
      FmSrvTomRetIts.FQrIts := QrSrvTomRetIts;
      FmSrvTomRetIts.FCodigo               := QrSrvTomRetCabCodigo.Value;
      FmSrvTomRetIts.EdCodigo.ValueVariant := QrSrvTomRetCabCodigo.Value;
      FmSrvTomRetIts.EdNO_Imposto.Text     := QrSrvTomRetCabNO_TIFEM.Value;
      FmSrvTomRetIts.FTIFEM                := QrSrvTomRetCabTIFEM.Value;
      FmSrvTomRetIts.FCampo                := 'TIFEM_' + IntToStr(TIFEM);
      FmSrvTomRetIts.ReopenAptos();
      if SQLType = stIns then
        //
      else
      begin
        // N�o editar!
      end;
      FmSrvTomRetIts.ShowModal;
      FmSrvTomRetIts.Destroy;
    end;
  end;
end;

procedure TFmSrvTomRetCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrSrvTomRetCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrSrvTomRetCab, QrSrvTomRetIts);
end;

procedure TFmSrvTomRetCab.PMFinPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(FinInclui1, QrSrvTomRetCab);
  //MyObjects.HabilitaMenuItemItsUpd(FinAltera1, QrEmissI);
  MyObjects.HabilitaMenuItemItsDel(FinExclui1, QrEmissI);
end;

procedure TFmSrvTomRetCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrSrvTomRetCab);
  //MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrEmissI);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrSrvTomRetIts);
end;

procedure TFmSrvTomRetCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSrvTomRetCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSrvTomRetCab.DefParams;
var
  ATT_TIFEM: String;
begin
  ATT_TIFEM := dmkPF.ArrayToTexto('trc.TIFEM', 'NO_TIFEM', pvPos, True,
  sTipImpostoFddEstMun);
  //
  VAR_GOTOTABELA := 'srvtomretcab';
  VAR_GOTOMYSQLTABLE := QrSrvTomRetCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ' + ATT_TIFEM);
  VAR_SQLx.Add(' trc.*, emp.Filial, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA');
  VAR_SQLx.Add('FROM srvtomretcab trc');
  VAR_SQLx.Add('LEFT JOIN entidades    emp ON emp.Codigo=trc.Empresa');
  VAR_SQLx.Add('WHERE trc.Codigo > 0');
  //
  VAR_SQL1.Add('AND trc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND trc.Nome Like :P0');
  //
  //Geral.MB_Teste(QrSrvTomRetCab.SQL.Text);
end;

procedure TFmSrvTomRetCab.ExcluiLanctoI;
begin

end;

procedure TFmSrvTomRetCab.ItsAltera1Click(Sender: TObject);
begin
//  MostraSrvTomRetIts(stUpd);
end;

procedure TFmSrvTomRetCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmSrvTomRetCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSrvTomRetCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSrvTomRetCab.ItsExclui1Click(Sender: TObject);
const
  sProcName = 'TFmSrvTomRetCab.ItsExclui1Click()';
var
  TIFEM: Integer;
  CampoIdx: String;
  Codigo: Integer;
  //
  function RemoveSrvTomInnCab(): Boolean;
  const
    ValorIdx = 0; // Limpa
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvtominncab', False, [
    CampoIdx], ['Codigo'], [ValorIdx], [Codigo], True);
  end;
  //
  procedure ExcluiSrvTomRetIts();
  begin
    Dmod.MyDB.Execute('DELETE FROM srvtomretits WHERE Controle=' + Geral.FF0(QrSrvTomRetItsControle.Value));
  end;
  //
var
  q: TSelType;
  I: Integer;
begin
  TIFEM := QrSrvTomRetCabTIFEM.Value;
  if TIFEM >= MaxNomFldsImpostoFddEstMun then
  begin
    Geral.MB_Erro('"TIFEM" n�o habilitado: ID = ' + Geral.FF0(TIFEM) +
    ' em ' + sProcName);
  end else
  begin
    try
      CampoIdx := 'TIFEM_' + IntToStr(TIFEM);
      Codigo := QrSrvTomRetItsCodigo.Value;
      // ...
      DBCheck.Quais_Selecionou(QrSrvTomRetIts, TDBGrid(DGDados), q);
      case q of
        istAtual:
        begin
          if RemoveSrvTomInnCab() then
            ExcluiSrvTomRetIts();
        end;
        istSelecionados:
        begin
          with DGDados.DataSource.DataSet do
          for I := 0 to DGDados.SelectedRows.Count-1 do
          begin
            //GotoBookmark(pointer(DBGIMEI.SelectedRows.Items[I]));
            if RemoveSrvTomInnCab() then
              ExcluiSrvTomRetIts();
          end;
        end;
        istTodos:
        begin
          QrSrvTomRetIts.First;
          while not QrSrvTomRetIts.Eof do
          begin
            if RemoveSrvTomInnCab() then
              ExcluiSrvTomRetIts();
            //
            QrSrvTomRetIts.Next;
          end;
        end;
      end;
    finally
      SrvTom_PF.AtiualizaRetValorTotal(Codigo, QrSrvTomRetCabTIFEM.Value);
      LocCod(Codigo, Codigo);
    end;
  end;
end;

procedure TFmSrvTomRetCab.ReopenEmissI(FatParcela: Integer);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissI, Dmod.MyDB, [
    'SELECT la.*, ca.Nome NOMECARTEIRA ',
    'FROM ' + FTabLctA + ' la  ',
    'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
    'WHERE la.FatID=' + Geral.FF0(FThis_FATID),
    'AND la.FatNum=' + Geral.FF0(QrSrvTomRetCabCodigo.Value),
    'AND la.ID_Pgto = 0 ',
    'ORDER BY FatParcela ',
    '']);
end;

procedure TFmSrvTomRetCab.ReopenSrvTomRetIts(Codigo: Integer);
const
  sProcName = 'TFmSrvTomRetCab.ReopenSrvTomRetIts()';
var
  TIFEM: Integer;
  CampoIdx, sCodigo: String;
begin
  TIFEM := QrSrvTomRetCabTIFEM.Value;
  if TIFEM >= MaxNomFldsImpostoFddEstMun then
  begin
    Geral.MB_Erro('"TIFEM" n�o habilitado: ID = ' + Geral.FF0(TIFEM) +
    ' em ' + sProcName);
  end else
  begin
    sCodigo := Geral.FF0(QrSrvTomRetCabCodigo.Value);
    CampoIdx := 'TIFEM_' + IntToStr(TIFEM);
    UnDmkDAC_PF.AbreMySQLQuery0(QrSrvTomRetIts, Dmod.MyDB, [
    'SELECT sri.Controle, sic.*, emp.Filial,',
    'IF(emp.Tipo=0, emp.RAZAOSOCIAL, emp.Nome) NO_EMPRESA,',
    'IF(prs.Tipo=0, prs.RAZAOSOCIAL, prs.Nome) NO_PRESTADOR,',
    'IF(itm.Tipo=0, itm.RAZAOSOCIAL, itm.Nome) NO_INTERMEDIARIO,',
    'lsr.Nome NO_DpsItemListaServico, cna.Nome NO_cnae21Cad,',
    'ts1.Nome NO_NfsRpsIDTipo, ts3.Nome NO_DpsIssRetido,',
    'tsr.Nome NO_DpsResponsavelRetencao',
    'FROM srvtominncab sic',
    //'LEFT JOIN srvtomretits sri ON sri.SrvTomInnCab=sic.Codigo ',
    'LEFT JOIN srvtomretits sri ON sri.SrvTomInnCab=sic.Codigo AND sri.Codigo=' + sCodigo,
    'LEFT JOIN entidades emp ON emp.Codigo=sic.Empresa',
    'LEFT JOIN entidades prs ON prs.Codigo=sic.Prestador',
    'LEFT JOIN entidades itm ON itm.Codigo=sic.Intermediario',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.listserv  lsr ON lsr.Codigo=sic.DpsItemListaServico',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.cnae21Cad cna ON cna.CodAlf=sic.DpsCodigoCnae',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.tstiporps ts1 ON ts1.Codigo=sic.NfsRpsIDTipo',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.tssimnao  ts3 ON ts3.Codigo=sic.DpsIssRetido',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.tsresponsavelretencao tsr ON tsr.Codigo=sic.DpsResponsavelRetencao',
    'WHERE sic.' + CampoIdx + ' = ' + Geral.FF0(QrSrvTomRetCabCodigo.Value),
    //'OR sri.Codigo=' + sCodigo,
    '']);
    //
    //Geral.MB_Teste(QrSrvTomRetIts.SQL.Text);
    QrSrvTomRetIts.Locate('Codigo', Codigo, []);
  end;
end;


procedure TFmSrvTomRetCab.DefineONomeDoForm;
begin
end;

procedure TFmSrvTomRetCab.DefineVarDup;
begin
  IC3_ED_FatNum := QrSrvTomRetCabCodigo.Value;
  IC3_ED_NF     := 0; //QrSrvTomRetCabNfsNumero.Value;
  IC3_ED_Data   := Int(QrSrvTomRetCabData.Value);
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSrvTomRetCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSrvTomRetCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSrvTomRetCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSrvTomRetCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSrvTomRetCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvTomRetCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSrvTomRetCabCodigo.Value;
  Close;
end;

procedure TFmSrvTomRetCab.ItsInclui1Click(Sender: TObject);
begin
  MostraSrvTomRetIts(stIns);
end;

procedure TFmSrvTomRetCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrSrvTomRetCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'srvtomretcab');
  RGTIFEM.Enabled := QrSrvTomRetIts.RecordCount = 0;
end;

procedure TFmSrvTomRetCab.BtConfirmaClick(Sender: TObject);
var
  Nome, Data, Competencia: String;
  Codigo, Empresa, TIFEM: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  //
  TIFEM          := RGTIFEM.ItemIndex;
  Data           := Geral.FDT(TPData.Date, 1);
  Competencia    := Geral.FDT(TPCompetencia.Date, 1);;
  //
  if MyObjects.FIC(TIFEM < 0, RGTIFEM, 'Informe o tipo de imposto!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('srvtomretcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'srvtomretcab', False, [
  'Empresa', 'Nome', 'TIFEM',
  'Data', 'Competencia'], [
  'Codigo'], [
  Empresa, Nome, TIFEM,
  Data, Competencia], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then
      Close
    else
      MostraSrvTomRetIts(stIns);
  end;
end;

procedure TFmSrvTomRetCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'srvtomretcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'srvtomretcab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmSrvTomRetCab.BtFinClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFin, BtFin);
end;

procedure TFmSrvTomRetCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmSrvTomRetCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmSrvTomRetCab.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBPagamentos.Align := alClient;
  MyObjects.PreencheComponente(RGTIFEM, sTipImpostoFddEstMun, 4);
  MyObjects.PreencheComponente(DBRGTIFEM, sTipImpostoFddEstMun, 4);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa, DModG.QrEmpresas, 'Filial');
  //
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  TPCompetencia.Date := Agora;
  //
  CriaOForm;
  FSeq := 0;
end;

procedure TFmSrvTomRetCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSrvTomRetCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvTomRetCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSrvTomRetCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrSrvTomRetCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvTomRetCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSrvTomRetCab.QrSrvTomRetCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSrvTomRetCab.QrSrvTomRetCabAfterScroll(DataSet: TDataSet);
begin
  if QrSrvTomRetCabEmpresa.Value <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrSrvTomRetCabFilial.Value)
  else
    FTabLctA := sTabLctErr;
  //
  ReopenSrvTomRetIts(0);
  ReopenEmissI(0);
end;

procedure TFmSrvTomRetCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrSrvTomRetCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmSrvTomRetCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSrvTomRetCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'srvtomretcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSrvTomRetCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvTomRetCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrSrvTomRetCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'srvtomretcab');
  RGTIFEM.Enabled := True;
end;

function TFmSrvTomRetCab.CalculaDiferencas: Double;
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  Result := 0.00;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumI, Dmod.MyDB, [
      'SELECT SUM(Debito) Debito ',
      'FROM ' + FTabLctA,
      'WHERE FatID=' + Geral.FF0(FThis_FATID),
      'AND FatNum=' + Geral.FF0(QrSrvTomRetCabCodigo.Value),
      'AND ID_Pgto = 0 ',
      '']);
    //
    if QrSumIDebito.Value <> 0 then
      Result := QrSumIDebito.Value - QrSrvTomRetCabRetValorTotal.Value;
  end;
end;

procedure TFmSrvTomRetCab.QrSrvTomRetCabBeforeClose(
  DataSet: TDataSet);
begin
  QrSrvTomRetIts.Close;
  QrEmissI.Close;
end;

procedure TFmSrvTomRetCab.QrSrvTomRetCabBeforeOpen(DataSet: TDataSet);
begin
  QrSrvTomRetCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

