unit UnSrvTom_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, Variants, UnAppEnums;

type
  TUnSrvTom_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormSrvTomInnCab(Codigo: Integer);
    procedure MostraFormSrvTomTaxCad(Codigo: Integer);
    procedure MostraFormSrvTomRetCab(Codigo: Integer);
  end;

var
  SrvTom_Jan: TUnSrvTom_Jan;


implementation

uses
  MyDBCheck, Module, UnVS_PF, CfgCadLista,
  SrvTomInnCab, SrvTomTaxCad, SrvTomRetCab;

{ TUnApp_Jan }

procedure TUnSrvTom_Jan.MostraFormSrvTomInnCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmSrvTomInnCab, FmSrvTomInnCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmSrvTomInnCab.LocCod(Codigo, Codigo);
    FmSrvTomInnCab.ShowModal;
    FmSrvTomInnCab.Destroy;
  end;
end;

procedure TUnSrvTom_Jan.MostraFormSrvTomRetCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmSrvTomRetCab, FmSrvTomRetCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmSrvTomRetCab.LocCod(Codigo, Codigo);
    FmSrvTomRetCab.ShowModal;
    FmSrvTomRetCab.Destroy;
  end;
end;

procedure TUnSrvTom_Jan.MostraFormSrvTomTaxCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmSrvTomTaxCad, FmSrvTomTaxCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmSrvTomTaxCad.LocCod(Codigo, Codigo);
    FmSrvTomTaxCad.ShowModal;
    FmSrvTomTaxCad.Destroy;
  end;
end;

end.
