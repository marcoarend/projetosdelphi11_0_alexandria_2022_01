object FmChmOcoPsq: TFmChmOcoPsq
  Left = 339
  Top = 185
  Caption = 'CHM-OCORR-006 :: Pesquisa em Chamados de Ocorr'#234'ncias'
  ClientHeight = 662
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 488
        Height = 32
        Caption = 'Pesquisa em Chamados de Ocorr'#234'ncias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 488
        Height = 32
        Caption = 'Pesquisa em Chamados de Ocorr'#234'ncias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 488
        Height = 32
        Caption = 'Pesquisa em Chamados de Ocorr'#234'ncias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 500
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 245
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 8
        Top = 4
        Width = 196
        Height = 13
        Caption = 'Titulo / Descri'#231#227'o do que fazer / Motivo: '
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 8
        Top = 44
        Width = 90
        Height = 13
        Caption = 'Autor do chamado:'
      end
      object Label6: TLabel
        Left = 8
        Top = 84
        Width = 29
        Height = 13
        Caption = 'Onde:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdTextos: TdmkEdit
        Left = 8
        Top = 20
        Width = 629
        Height = 21
        MaxLength = 100
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object PnPeriodoAgendado: TPanel
        Left = 8
        Top = 176
        Width = 257
        Height = 65
        BevelKind = bkFlat
        BevelOuter = bvNone
        TabOrder = 5
        object Label53: TLabel
          Left = 9
          Top = 21
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label10: TLabel
          Left = 121
          Top = 21
          Width = 48
          Height = 13
          Caption = 'Data final:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 157
          Height = 13
          Caption = 'Per'#237'odo de execu'#231#227'o agendado:'
        end
        object TPQuandoIni: TdmkEditDateTimePicker
          Left = 9
          Top = 36
          Width = 108
          Height = 21
          Date = 0.639644131944805800
          Time = 0.639644131944805800
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'QuandoIni'
          UpdCampo = 'QuandoIni'
          UpdType = utYes
          DatePurpose = dmkdpSPED_EFD_MIN
        end
        object TPQuandoFim: TdmkEditDateTimePicker
          Left = 121
          Top = 36
          Width = 108
          Height = 21
          Date = 0.639644131944805800
          Time = 0.639644131944805800
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'QuandoFim'
          UpdCampo = 'QuandoFim'
          UpdType = utYes
          DatePurpose = dmkdpSPED_EFD_MIN
        end
      end
      object PnPeriodoEncerramento: TPanel
        Left = 268
        Top = 176
        Width = 257
        Height = 65
        BevelKind = bkFlat
        BevelOuter = bvNone
        TabOrder = 6
        object Label2: TLabel
          Left = 9
          Top = 21
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label3: TLabel
          Left = 121
          Top = 21
          Width = 48
          Height = 13
          Caption = 'Data final:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label4: TLabel
          Left = 8
          Top = 4
          Width = 124
          Height = 13
          Caption = 'Per'#237'odo de encerramento:'
        end
        object TPLstFechDtHIni: TdmkEditDateTimePicker
          Left = 9
          Top = 36
          Width = 108
          Height = 21
          Date = 0.639644131944805800
          Time = 0.639644131944805800
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'QuandoIni'
          UpdCampo = 'QuandoIni'
          UpdType = utYes
          DatePurpose = dmkdpSPED_EFD_MIN
        end
        object TPLstFechDtHFim: TdmkEditDateTimePicker
          Left = 121
          Top = 36
          Width = 108
          Height = 21
          Date = 0.639644131944805800
          Time = 0.639644131944805800
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'QuandoFim'
          UpdCampo = 'QuandoFim'
          UpdType = utYes
          DatePurpose = dmkdpSPED_EFD_MIN
        end
      end
      object RGStatusAtividade: TRadioGroup
        Left = 8
        Top = 124
        Width = 629
        Height = 45
        Caption = ' Status de atividade do chamado: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          'Aberto'
          'Encerrado'
          'Qualquer')
        TabOrder = 4
      end
      object BtReabre: TBitBtn
        Tag = 22
        Left = 528
        Top = 190
        Width = 109
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 7
        OnClick = BtReabreClick
      end
      object EdWhoGerEnti: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'WhoGerEnti'
        UpdCampo = 'WhoGerEnti'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBWhoGerEnti
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBWhoGerEnti: TdmkDBLookupComboBox
        Left = 68
        Top = 60
        Width = 569
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsWhoGerEnti
        TabOrder = 2
        dmkEditCB = EdWhoGerEnti
        QryCampo = 'WhoGerEnti'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdOnde: TdmkEdit
        Left = 8
        Top = 100
        Width = 629
        Height = 21
        MaxLength = 100
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object dmkDBGridZTO1: TdmkDBGridZTO
      Left = 0
      Top = 245
      Width = 1008
      Height = 255
      Align = alClient
      DataSource = DsChmOcoCad
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = dmkDBGridZTO1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QuandoIni'
          Title.Caption = 'Periodo in'#237'cio'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QuandoFim'
          Title.Caption = 'Periodo fim'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LstFechDtH_TXT'
          Title.Caption = 'Encerrado'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PorqueDescr'
          Title.Caption = 'Porque'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ChmHowCad'
          Title.Caption = 'Fluxo'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_WhoGerEnti'
          Title.Caption = 'Gerenciador'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ONDE'
          Title.Caption = 'Onde'
          Width = 200
          Visible = True
        end>
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 548
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 592
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrWhoGerEnti: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE, Codigo'
      'FROM entidades '
      'WHERE Tipo=1'
      'ORDER BY NOMEENTIDADE')
    Left = 372
    Top = 50
    object QrWhoGerEntiNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrWhoGerEntiCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsWhoGerEnti: TDataSource
    DataSet = QrWhoGerEnti
    Left = 372
    Top = 94
  end
  object QrOnde: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ovdlocal'
      'ORDER BY Nome'
      '')
    Left = 428
    Top = 30
    object QrOndeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOndeNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsOnde: TDataSource
    DataSet = QrOnde
    Left = 428
    Top = 74
  end
  object QrChmHowCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM chmhowcad'
      'ORDER BY Nome'
      '')
    Left = 484
    Top = 54
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsChmHowCad: TDataSource
    DataSet = QrChmHowCad
    Left = 484
    Top = 98
  end
  object QrChmOcoCad: TMySQLQuery
    Database = Dmod.ZZDB
    AfterOpen = QrChmOcoCadAfterOpen
    SQL.Strings = (
      'SELECT oca.*, chc.Nome NO_ChmHowCad, sen.Login NO_UserLog,'
      'IF(wge.Tipo=0, wge.RazaoSocial, wge.Nome) NO_WhoGerEnti, '
      'IF(oca.chmtitoco<>0, tit.NOme, oca.Nome) NO_Titulo,'
      'CASE KndOnde'
      '  WHEN 0 THEN oca.OndeDescr'
      '  /*WHEN 1 THEN scc.Nome*/'
      '  WHEN 2 THEN ovl.Nome'
      '  ELSE "????"'
      'END NO_ONDE,'
      'IF(oca.WhoGerEnti<>0, tit.NOme, oca.Nome) NO_chmocowho'
      'FROM chmococad oca'
      'LEFT JOIN chmtitoco tit ON tit.Codigo=oca.chmtitoco'
      'LEFT JOIN entidades wge ON wge.Codigo=oca.WhoGerEnti'
      '/*LEFT JOIN stqcencad scc ON scc.Codigo=oca.CodOnde*/'
      'LEFT JOIN ovdlocal  ovl ON ovl.Codigo=oca.CodOnde'
      'LEFT JOIN chmhowcad chc ON chc.Codigo=oca.ChmHowCad'
      'LEFT JOIN senhas    sen ON sen.Numero=oca.Usercad'
      'WHERE oca.Codigo > 0')
    Left = 60
    Top = 277
    object QrChmOcoCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrChmOcoCadKndNome: TSmallintField
      FieldName = 'KndNome'
      Required = True
    end
    object QrChmOcoCadChmTitOco: TIntegerField
      FieldName = 'ChmTitOco'
      Required = True
    end
    object QrChmOcoCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrChmOcoCadDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrChmOcoCadWhoGerEnti: TIntegerField
      FieldName = 'WhoGerEnti'
      Required = True
    end
    object QrChmOcoCadWhoDoQtdSel: TIntegerField
      FieldName = 'WhoDoQtdSel'
      Required = True
    end
    object QrChmOcoCadWhoDoQtdMin: TIntegerField
      FieldName = 'WhoDoQtdMin'
      Required = True
    end
    object QrChmOcoCadWhoDoQtdMax: TIntegerField
      FieldName = 'WhoDoQtdMax'
      Required = True
    end
    object QrChmOcoCadKndOnde: TSmallintField
      FieldName = 'KndOnde'
      Required = True
    end
    object QrChmOcoCadCodOnde: TIntegerField
      FieldName = 'CodOnde'
      Required = True
    end
    object QrChmOcoCadOndeDescr: TWideStringField
      FieldName = 'OndeDescr'
      Required = True
      Size = 60
    end
    object QrChmOcoCadPorqueDescr: TWideStringField
      FieldName = 'PorqueDescr'
      Size = 255
    end
    object QrChmOcoCadQuandoIni: TDateTimeField
      FieldName = 'QuandoIni'
      Required = True
    end
    object QrChmOcoCadQuandoFim: TDateTimeField
      FieldName = 'QuandoFim'
      Required = True
    end
    object QrChmOcoCadChmHowCad: TIntegerField
      FieldName = 'ChmHowCad'
      Required = True
    end
    object QrChmOcoCadHowManyUnd: TWideStringField
      FieldName = 'HowManyUnd'
      Size = 6
    end
    object QrChmOcoCadHowManyQtd: TFloatField
      FieldName = 'HowManyQtd'
      Required = True
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrChmOcoCadHowMuchMoed: TWideStringField
      FieldName = 'HowMuchMoed'
      Size = 3
    end
    object QrChmOcoCadHowMuchValr: TFloatField
      FieldName = 'HowMuchValr'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrChmOcoCadPriAbrtDtH: TDateTimeField
      FieldName = 'PriAbrtDtH'
      Required = True
    end
    object QrChmOcoCadLstRAbrDtH: TDateTimeField
      FieldName = 'LstRAbrDtH'
      Required = True
    end
    object QrChmOcoCadLstFechDtH: TDateTimeField
      FieldName = 'LstFechDtH'
      Required = True
    end
    object QrChmOcoCadImportancia: TSmallintField
      FieldName = 'Importancia'
      Required = True
    end
    object QrChmOcoCadUrgencia: TSmallintField
      FieldName = 'Urgencia'
      Required = True
    end
    object QrChmOcoCadLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrChmOcoCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChmOcoCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChmOcoCadUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrChmOcoCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrChmOcoCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrChmOcoCadAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrChmOcoCadAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrChmOcoCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrChmOcoCadNO_ChmHowCad: TWideStringField
      FieldName = 'NO_ChmHowCad'
      Size = 60
    end
    object QrChmOcoCadNO_UserLog: TWideStringField
      FieldName = 'NO_UserLog'
      Size = 30
    end
    object QrChmOcoCadNO_WhoGerEnti: TWideStringField
      FieldName = 'NO_WhoGerEnti'
      Size = 100
    end
    object QrChmOcoCadNO_Titulo: TWideStringField
      FieldName = 'NO_Titulo'
      Size = 60
    end
    object QrChmOcoCadNO_ONDE: TWideStringField
      FieldName = 'NO_ONDE'
      Size = 100
    end
    object QrChmOcoCadNO_chmocowho: TWideStringField
      FieldName = 'NO_chmocowho'
      Size = 60
    end
    object QrChmOcoCadKndIsWhoDo: TIntegerField
      FieldName = 'KndIsWhoDo'
    end
    object QrChmOcoCadONDE_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'ONDE_TXT'
      Size = 255
      Calculated = True
    end
    object QrChmOcoCadLstFechDtH_TXT: TWideStringField
      FieldName = 'LstFechDtH_TXT'
      Size = 19
    end
  end
  object DsChmOcoCad: TDataSource
    DataSet = QrChmOcoCad
    Left = 60
    Top = 321
  end
end
