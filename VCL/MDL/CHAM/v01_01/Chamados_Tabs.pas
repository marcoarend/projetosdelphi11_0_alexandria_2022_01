unit Chamados_Tabs;
// Chamados - Custos e Resultados Operacionais
{ Colocar no MyListas:

Uses UnChamados_Tabs;


//
function TMyListas.CriaListaTabelas:
      Chamados_Tabs.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
    Chamados_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:

    Chamados_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      Chamados_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      Chamados_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    Chamados_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  Chamados_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections, SysUtils, Classes, DB,
//  UnFMX_Geral,
  UnMyLinguas, UnDmkEnums;

type
  TUnChamados_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>; SiglaApp: String): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  Chamados_Tb: TUnChamados_Tabs;

implementation

uses MyListas;

function TUnChamados_Tabs.CarregaListaTabelas(Lista: TList<TTabelas> ; SiglaApp: String): Boolean;
begin
  try
   if (CO_HARDW_APP = 'DESKTOP') then
   begin
      // Cadastros bases
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmHowCad'), '');  // How: Como fazer - Cabe�alho de cadastro
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmHowEtp'), '');  // How: Como fazer - Etapas - Itens de cadastro
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmTitOco'), '_lst_sample');  // Tit: T�tulos de Chamado de ocorr�cia
      // Lan�amentos
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmOcoCad'), '');  // Oco: Cadastros de Chamados de Ocorr�ncias
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmOcoWho'), '');  // Oco: Quem foi chamado (1+) no Chamado de ocorr�cia
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmOcoEtp'), '');  // Oco: Etapas da execu��o
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmOcoDon'), '');  // Oco: Etapas da execu��o Executadas por Quem
      //MyLinguas.AdTbLst(Lista, False, Lowercase('ChmOcoPux'), '');  // Oco: Notifica��es de push recebidas
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmDevAcs'), '');  // Log de acessos
    end;
   if (CO_HARDW_APP = 'MOBILE') then
   begin
      // Cadastros bases
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmHowCad'), '');  // How: Como fazer - Cabe�alho de cadastro
      //MyLinguas.AdTbLst(Lista, False, Lowercase('ChmHowEtp'), '');  // How: Como fazer - Etapas - Itens de cadastro
      //MyLinguas.AdTbLst(Lista, False, Lowercase('ChmTitOco'), '_lst_sample');  // Tit: T�tulos de Chamado de ocorr�cia
      // Lan�amentos
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmOcoCad'), '');  // Oco: Cadastros de Chamados de Ocorr�ncias
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmOcoWho'), '');  // Oco: Quem foi chamado (1+) no Chamado de ocorr�cia
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmOcoEtp'), '');  // Oco: Etapas da execu��o
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmOcoDon'), '');  // Oco: Etapas da execu��o Executadas por Quem
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmOcoPux'), '');  // Oco: Notifica��es de push recebidas
      MyLinguas.AdTbLst(Lista, False, Lowercase('ChmDevAcs'), '');  // Log de acessos
    end;
    Result := True;
  except
    raise;
    Result := False;
  end;
{
  VAR_SQLx.Add('LEFT JOIN entidades wge ON wge.Codigo=oca.WhoGerEnti');
  VAR_SQLx.Add('LEFT JOIN ovdlocal  ovl ON ovl.Codigo=oca.CodOnde');
  'LEFT JOIN entidades wde ON wde.Codigo=ccw.WhoDoEnti ',
}
end;

function TUnChamados_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
{
separador = "|"
}
  Result := True;
  //end else
{
  if Uppercase(Tabela) = Uppercase('CtrlGeral') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo');
    FListaSQL.Add('1');
  end;
}
end;

function TUnChamados_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('??') then
  begin
(*
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"ND"');
    FListaSQL.Add('1|"Telefone"');
*)
  end else
end;

function TUnChamados_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('ChmHowCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ChmHowEtp') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ChmOcoCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ChmOcoWho') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ChmOcoEtp') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ChmOcoDon') then
  begin
{
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    if (CO_HARDW_APP = 'DESKTOP') then
      FRIndices.Column_name   := 'Controle'
    else
    //if (CO_HARDW_APP = 'MOBILE') then
      FRIndices.Column_name   := 'CtrlInMob';
    FLIndices.Add(FRIndices);
    //
}
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'ChmOcoEtp';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'ChmOcoWho';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ChmOcoPux') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ChmDevAcs') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    if (CO_HARDW_APP = 'DESKTOP') then
      FRIndices.Column_name   := 'Codigo'
    else
    if (CO_HARDW_APP = 'MOBILE') then
      FRIndices.Column_name   := 'CodInMob';
    FLIndices.Add(FRIndices);
    //
  end else
end;

function TUnChamados_Tabs.CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TUnChamados_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    //end else
    if Uppercase(Tabela) = Uppercase('ChmHowCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?????';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemItns';
      FRCampos.Tipo       := 'tinyint(1)'; // 0=Nao, 1=Sim
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ChmHowEtp') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?????';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ChmOcoCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //  What - O qu�?
      //
      New(FRCampos);
      FRCampos.Field      := 'KndNome';
      FRCampos.Tipo       := 'tinyint(1)'; // 0=Livre, 1= tab ChmTitOco
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ChmTitOco';
      FRCampos.Tipo       := 'int(11)'; // ChmTitOco.Codigo (se KndNome=1)
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?????';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Who - Quem?
      //
      New(FRCampos);
      FRCampos.Field      := 'WhoGerEnti';
      FRCampos.Tipo       := 'int(11)'; // Entidade
      FRCampos.Null       := 'NO';      // UserCad tamb�m ser� usado!
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);                        // Abrang�ncia de Sele��o
      FRCampos.Field      := 'KndIsWhoDo';  // 0   =Ninguem
      FRCampos.Tipo       := 'int(11)';     // 1024=Espec�fico
      FRCampos.Null       := 'NO';          // 2048=Dentre
      FRCampos.Key        := '';            // 3072=Alguns
      FRCampos.Default    := '0';           // 4096=Todos
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WhoDoQtdSel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WhoDoQtdMin';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WhoDoQtdMax';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //  Where - Onde?
      //
      New(FRCampos);
      FRCampos.Field      := 'KndOnde';
      FRCampos.Tipo       := 'tinyint(1)'; // 0 = Indefinido,
      FRCampos.Null       := 'NO';         // 1 = Livre,
      FRCampos.Key        := '';           // 2 = tab EstqCenCad
      FRCampos.Default    := '0';          // 3 = tab OVdLocal
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
//
      New(FRCampos);
      FRCampos.Field      := 'CodOnde';
      FRCampos.Tipo       := 'int(11)'; // 0=Indefinido 1=Livre, 3= tab EstqCenCad.Codigo 3= tab OVdLocal.Codigo
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OndeDescr';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?????';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Why - Por que?
      //
      New(FRCampos);
      FRCampos.Field      := 'PorqueDescr';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // When - Quando?
      //
      New(FRCampos);
      FRCampos.Field      := 'QuandoIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QuandoFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // How - Como?
      //
      New(FRCampos);
      FRCampos.Field      := 'ChmHowCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
      // How Many- Quantos?
      //
      New(FRCampos);
      FRCampos.Field      := 'HowManyUnd';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HowManyQtd';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
      // How Much- Quanto custa?
      //
      New(FRCampos);
      FRCampos.Field      := 'HowMuchMoed';
      FRCampos.Tipo       := 'varchar(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HowMuchValr';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
      // Gerenciamento
      //
      New(FRCampos);
      FRCampos.Field      := 'PriAbrtDtH';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstRAbrDtH';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstFechDtH';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Importancia';
      FRCampos.Tipo       := 'tinyint(3)'; // 0-99
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '50';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Urgencia';
      FRCampos.Tipo       := 'tinyint(3)'; // 0-99
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '50';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CloseUser';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CloseDtHr';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ChmOcoWho') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';  // ChmOcoCad.Codigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);                        // Pertin�ncia
      FRCampos.Field      := 'KndWhoPrtnc'; // 0   =Indef,
      FRCampos.Tipo       := 'int(11)';  // 1024=Indicado
      FRCampos.Null       := 'NO';          // 2048=Preferencial
      FRCampos.Key        := '';            // 3072=Imprescind�vel
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WhoDoEnti';
      FRCampos.Tipo       := 'int(11)'; // Entidade
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WhoDoMobile';
      FRCampos.Tipo       := 'int(11)'; // excmobdevcad.Codigo
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SntNotif';  // �ltimo data hora de suceso de envio de notificacao no desktop
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CfmRcbNotif';  // �ltimo data hora de confirma��o de recebimento de notificacao no mobile
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CfmDtHrLido';  // �ltimo data hora de confirma��o de leitura da notificacao no mobile
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DoneDtHr';  // �ltimo data hora feito
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MobUpWeb';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Realizado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ObsEncerr';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReopenDtHr';  // �ltimo data hora reaberto
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ChmOcoEtp') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CadEtpCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CadEtpCtrl';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?????';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QuandoIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QuandoFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DoneDtHr';  // �ltimo data hora feito
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CloseUser';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CloseDtHr';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ChmOcoDon') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
{
      New(FRCampos);
      FRCampos.Field      := 'CtrlInMob';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      if (CO_HARDW_APP = 'DESKTOP') then
        FRCampos.Key        := ''
      else
      //if (CO_HARDW_APP = 'MOBILE'then
        FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
}
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
{
      if (CO_HARDW_APP = 'DESKTOP') then
        FRCampos.Key        := 'PRI'
      else
      //if (CO_HARDW_APP = 'MOBILE') then
        FRCampos.Key        := '';
}
      FRCampos.Key        := 'PRI';
      //
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ChmOcoEtp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ChmOcoWho';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observacao';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DoneDtHr';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MobUpWeb';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CloseUser';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CloseDtHr';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ChmOcoPux') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtLocal';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QuandoIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QuandoFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrPush';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrCatx';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrLido';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ChmDevAcs') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      if (CO_HARDW_APP = 'DESKTOP') then
        FRCampos.Key        := 'PRI'
      else
      if (CO_HARDW_APP = 'MOBILE') then
        FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodInMob';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      if (CO_HARDW_APP = 'DESKTOP') then
        FRCampos.Key        := ''
      else
      if (CO_HARDW_APP = 'MOBILE') then
        FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MobDevCad'; //EXcMobDevCad.Codigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DeviceID';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PushToken';
      FRCampos.Tipo       := 'varchar(4080)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaAcsIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
  except
    raise;
    Result := False;
  end;
end;

function TUnChamados_Tabs.CompletaListaFRCampos(Tabela: String;
  FRCampos: TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
{
      New(FRCampos);
      FRCampos.Field      := '??';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else
    if Uppercase(Tabela) = Uppercase('???') then
    begin
      New(FRCampos);
      FRCampos.Field      := '???';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);

    end else
  except
    raise;
    Result := False;
  end;
end;

function TUnChamados_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // CHM-COMO?-001 :: Cadatros de Procedimentos
  New(FRJanelas);
  FRJanelas.ID        := 'CHM-COMO?-001';
  FRJanelas.Nome      := 'ChmHowCad';
  FRJanelas.Descricao := 'Cadatros de Procedimentos';
  FLJanelas.Add(FRJanelas);
  //
  // CHM-COMO?-002 :: Cadatro de Item de Procedimento
  New(FRJanelas);
  FRJanelas.ID        := 'CHM-COMO?-002';
  FRJanelas.Nome      := 'ChmHowEtp';
  FRJanelas.Descricao := 'Importa��o de dados CSV';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  ///
  //
  // CHM-OCORR-001 :: Cadastros de Chamados de Ocorr�ncias
  New(FRJanelas);
  FRJanelas.ID        := 'CHM-OCORR-001';
  FRJanelas.Nome      := 'ChmOcoCad';
  FRJanelas.Descricao := 'Cadastros de Chamados de Ocorr�ncias';
  FLJanelas.Add(FRJanelas);
  //
  // CHM-OCORR-002 :: Pessoa / Equipamento Designado
  New(FRJanelas);
  FRJanelas.ID        := 'CHM-OCORR-002';
  FRJanelas.Nome      := 'ChmOcoWho';
  FRJanelas.Descricao := 'Pessoa / Equipamento Designado';
  FLJanelas.Add(FRJanelas);
  //
  // CHM-OCORR-003 :: Etapa da Execu��o
  New(FRJanelas);
  FRJanelas.ID        := 'CHM-OCORR-003';
  FRJanelas.Nome      := 'ChmOcoEtp';
  FRJanelas.Descricao := 'Etapa da Execu��o';
  FLJanelas.Add(FRJanelas);
  //
  // CHM-OCORR-004 :: Execu��o de Etapa
  New(FRJanelas);
  FRJanelas.ID        := 'CHM-OCORR-004';
  FRJanelas.Nome      := 'ChmOcoDon';
  FRJanelas.Descricao := 'Execu��o de Etapa';
  FLJanelas.Add(FRJanelas);
  //
  // CHM-OCORR-005 :: Log de Device para Token
  New(FRJanelas);
  FRJanelas.ID        := 'CHM-OCORR-005';
  FRJanelas.Nome      := 'ChmDevAcs';
  FRJanelas.Descricao := 'Log de Device para Token';
  FLJanelas.Add(FRJanelas);
  //
  // CHM-OCORR-006 :: Pesquisa em Chamados de Ocorr�ncias
  New(FRJanelas);
  FRJanelas.ID        := 'CHM-OCORR-006';
  FRJanelas.Nome      := 'ChmOcoPsq';
  FRJanelas.Descricao := 'Pesquisa em Chamados de Ocorr�ncias';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  ///
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
