unit ChmOcoPsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkEditDateTimePicker, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB;

type
  TFmChmOcoPsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    EdTextos: TdmkEdit;
    Label9: TLabel;
    QrWhoGerEnti: TMySQLQuery;
    QrWhoGerEntiNOMEENTIDADE: TWideStringField;
    QrWhoGerEntiCodigo: TIntegerField;
    DsWhoGerEnti: TDataSource;
    QrOnde: TMySQLQuery;
    QrOndeCodigo: TIntegerField;
    QrOndeNome: TWideStringField;
    DsOnde: TDataSource;
    QrChmHowCad: TMySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsChmHowCad: TDataSource;
    PnPeriodoAgendado: TPanel;
    Label53: TLabel;
    TPQuandoIni: TdmkEditDateTimePicker;
    Label10: TLabel;
    TPQuandoFim: TdmkEditDateTimePicker;
    Label1: TLabel;
    PnPeriodoEncerramento: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    TPLstFechDtHIni: TdmkEditDateTimePicker;
    TPLstFechDtHFim: TdmkEditDateTimePicker;
    RGStatusAtividade: TRadioGroup;
    dmkDBGridZTO1: TdmkDBGridZTO;
    BtReabre: TBitBtn;
    QrChmOcoCad: TMySQLQuery;
    QrChmOcoCadCodigo: TIntegerField;
    QrChmOcoCadKndNome: TSmallintField;
    QrChmOcoCadChmTitOco: TIntegerField;
    QrChmOcoCadNome: TWideStringField;
    QrChmOcoCadDescricao: TWideStringField;
    QrChmOcoCadWhoGerEnti: TIntegerField;
    QrChmOcoCadWhoDoQtdSel: TIntegerField;
    QrChmOcoCadWhoDoQtdMin: TIntegerField;
    QrChmOcoCadWhoDoQtdMax: TIntegerField;
    QrChmOcoCadKndOnde: TSmallintField;
    QrChmOcoCadCodOnde: TIntegerField;
    QrChmOcoCadOndeDescr: TWideStringField;
    QrChmOcoCadPorqueDescr: TWideStringField;
    QrChmOcoCadQuandoIni: TDateTimeField;
    QrChmOcoCadQuandoFim: TDateTimeField;
    QrChmOcoCadChmHowCad: TIntegerField;
    QrChmOcoCadHowManyUnd: TWideStringField;
    QrChmOcoCadHowManyQtd: TFloatField;
    QrChmOcoCadHowMuchMoed: TWideStringField;
    QrChmOcoCadHowMuchValr: TFloatField;
    QrChmOcoCadPriAbrtDtH: TDateTimeField;
    QrChmOcoCadLstRAbrDtH: TDateTimeField;
    QrChmOcoCadLstFechDtH: TDateTimeField;
    QrChmOcoCadImportancia: TSmallintField;
    QrChmOcoCadUrgencia: TSmallintField;
    QrChmOcoCadLk: TIntegerField;
    QrChmOcoCadDataCad: TDateField;
    QrChmOcoCadDataAlt: TDateField;
    QrChmOcoCadUserCad: TIntegerField;
    QrChmOcoCadUserAlt: TIntegerField;
    QrChmOcoCadAlterWeb: TSmallintField;
    QrChmOcoCadAWServerID: TIntegerField;
    QrChmOcoCadAWStatSinc: TSmallintField;
    QrChmOcoCadAtivo: TSmallintField;
    QrChmOcoCadNO_ChmHowCad: TWideStringField;
    QrChmOcoCadNO_UserLog: TWideStringField;
    QrChmOcoCadNO_WhoGerEnti: TWideStringField;
    QrChmOcoCadNO_Titulo: TWideStringField;
    QrChmOcoCadNO_ONDE: TWideStringField;
    QrChmOcoCadNO_chmocowho: TWideStringField;
    QrChmOcoCadKndIsWhoDo: TIntegerField;
    QrChmOcoCadONDE_TXT: TStringField;
    QrChmOcoCadLstFechDtH_TXT: TWideStringField;
    DsChmOcoCad: TDataSource;
    Label5: TLabel;
    EdWhoGerEnti: TdmkEditCB;
    CBWhoGerEnti: TdmkDBLookupComboBox;
    EdOnde: TdmkEdit;
    Label6: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrChmOcoCadAfterOpen(DataSet: TDataSet);
    procedure dmkDBGridZTO1DblClick(Sender: TObject);
  private
    { Private declarations }
    procedure SelecionaCodigo();
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmChmOcoPsq: TFmChmOcoPsq;

implementation

uses UnMyObjects, DmkDAC_PF, Module;

{$R *.DFM}

procedure TFmChmOcoPsq.BtOKClick(Sender: TObject);
begin
  SelecionaCodigo();
end;

procedure TFmChmOcoPsq.BtReabreClick(Sender: TObject);
var
  Texto, sDataIni, sDataFim, SQL_Textos, SQL_STATUS, SQL_Periodo, SQL_Onde,
  SQL_WhoGerEnti: String;
  QuandoIni, QuandoFim, PrINIni, PrINFim: TDateTime;
  WhoGerEnti: Integer;
begin
  if MyObjects.FIC(RGStatusAtividade.ItemIndex = 0, RGStatusAtividade,
    'Selecione o "Status de atividade do chamado" ') then Exit;
  //
  SQL_Textos  := EmptyStr;
  SQL_STATUS  := EmptyStr;
  SQL_Periodo := EmptyStr;
  SQL_WhoGerEnti := EmptyStr;
  //
  if Trim(EdTextos.ValueVariant) <> EmptyStr then
  begin
    Texto := EdTextos.ValueVariant;
    SQL_Textos := Geral.ATS([
    'AND ( ',
    '  (IF(oca.chmtitoco<>0, tit.Nome, oca.Nome) LIKE "%' + Texto + '%") ',
    '  OR ',
    '  (oca.Descricao LIKE "%' + Texto + '%") ',
    '  OR ',
    '  (oca.OndeDescr LIKE "%' + Texto + '%") ',
    '  OR ',
    '  (oca.PorqueDescr LIKE "%' + Texto + '%") ',
    '  OR ',
    '  (chc.Nome LIKE "%' + Texto + '%") ',
    ') ']);
  end;
  //
  case RGStatusAtividade.ItemIndex of
    //0: //Indefinido - Nada!
    // Aberto
    1: SQL_STATUS :=  'AND oca.LstFechDtH < "1900-01-01"  ';
    // Encerrado
    2: SQL_STATUS :=  'AND oca.LstFechDtH > "1900-01-01"  ';
    //Qualquer
    3: SQL_STATUS :=  '';
    else SQL_STATUS :=  '**** Status Indefinido ****';
  end;
  //
  QuandoIni := Trunc(TPQuandoIni.Date);
  QuandoFim := Trunc(TPQuandoFIm.Date);
  if (QuandoIni > 2) or (QuandoFim > 2) then
  begin
    PrINIni := QuandoIni;
    PrINFim := QuandoFim;
    if PrINIni < 2 then
      PrINIni := PrINFim;
    if PrINFim < 2 then
      PrINFim := PrINIni;
    //
    sDataIni := Geral.FDT(PrINIni, 1);
    sDataFim := Geral.FDT(PrINFim, 1);
(*
    'AND ( ',
    '  (QuandoIni BETWEEN "2019-07-01" AND "2020-07-01 23:59:59") ',
    '  OR  ',
    '  (QuandoFim BETWEEN "2019-07-01" AND "2020-07-01 23:59:59") ',
    ') ',
*)
    SQL_Periodo := 'AND ( ';
    if (QuandoIni > 2) then
      SQL_Periodo := SQL_Periodo + sLineBreak +
      //'  (QuandoIni BETWEEN "' + sDataIni + '" AND "' + sDataFim + ' 23:59:59") ';
      '  ("' + sDataIni + '" BETWEEN QuandoIni AND QuandoFim) ';
    if (QuandoIni > 2) and (QuandoFim > 2) then
      SQL_Periodo := SQL_Periodo + sLineBreak + '  OR  ';
    if (QuandoFim > 2) then
      SQL_Periodo := SQL_Periodo + sLineBreak +
      //'  (QuandoFim BETWEEN "' + sDataIni + '" AND "' + sDataFim + ' 23:59:59") ';
      '  ("' + sDataFim + '" BETWEEN QuandoIni AND QuandoFim) ';
    SQL_Periodo := SQL_Periodo + sLineBreak + ') ';
  end;
  WhoGerEnti := EdWhoGerEnti.ValueVariant;
  if WhoGerEnti <> 0 then
    SQL_WhoGerEnti := 'AND oca.WhoGerEnti=' + Geral.FF0(WhoGerEnti);
  //
  if Trim(EdOnde.ValueVariant) <> EmptyStr then
  begin
    Texto := EdOnde.ValueVariant;
    SQL_Onde := Geral.ATS([
      'AND CASE KndOnde ',
      '  WHEN 1 THEN oca.OndeDescr ',
      '  /*WHEN 2 THEN scc.Nome*/ ',
      '  WHEN 3 THEN ovl.Nome ',
      '  ELSE "????" ',
      'END LIKE "%' + Texto + '%"'
      ]);
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrChmOcoCad, Dmod.MyDB, [
  'SELECT oca.Nome, ',
  'chc.Nome NO_ChmHowCad, sen.Login NO_UserLog, ',
  'IF(oca.LstFechDtH  <= "1899-12-30", "",  ',
  '  DATE_FORMAT(oca.LstFechDtH, "%d/%m/%Y %H:%i:%s")) LstFechDtH_TXT,  ',
  'IF(wge.Tipo=0, wge.RazaoSocial, wge.Nome) NO_WhoGerEnti,  ',
  'IF(oca.chmtitoco<>0, tit.Nome, oca.Nome) NO_Titulo, ',
  'CASE KndOnde ',
  '  WHEN 1 THEN oca.OndeDescr ',
  '  /*WHEN 2 THEN scc.Nome*/ ',
  '  WHEN 3 THEN ovl.Nome ',
  '  ELSE "????" ',
  'END NO_ONDE, ',
  'IF(oca.WhoGerEnti<>0, IF(wge.Tipo=0, wge.RazaoSocial, wge.Nome), sen.Login) NO_chmocowho, ',
  'oca.* ',
  'FROM chmococad oca ',
  'LEFT JOIN chmtitoco tit ON tit.Codigo=oca.chmtitoco ',
  'LEFT JOIN entidades wge ON wge.Codigo=oca.WhoGerEnti ',
  '/*LEFT JOIN stqcencad scc ON scc.Codigo=oca.CodOnde*/ ',
  'LEFT JOIN ovdlocal  ovl ON ovl.Codigo=oca.CodOnde ',
  'LEFT JOIN chmhowcad chc ON chc.Codigo=oca.ChmHowCad ',
  'LEFT JOIN senhas    sen ON sen.Numero=oca.Usercad ',
  'WHERE oca.Codigo > 0 ',
  SQL_Textos,
  SQL_Status,
  SQL_Periodo,
  SQL_WhoGerEnti,
  SQL_Onde,
  EmptyStr]);
end;

procedure TFmChmOcoPsq.BtSaidaClick(Sender: TObject);
begin
  FCodigo := 0;
  Close;
end;

procedure TFmChmOcoPsq.dmkDBGridZTO1DblClick(Sender: TObject);
begin
  SelecionaCodigo();
end;

procedure TFmChmOcoPsq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmChmOcoPsq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrWhoGerEnti, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrChmHowCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOnde, Dmod.MyDB);
end;

procedure TFmChmOcoPsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmChmOcoPsq.QrChmOcoCadAfterOpen(DataSet: TDataSet);
begin
  BtOK.Enabled := QrChmOcoCad.RecordCount > 0;
end;

procedure TFmChmOcoPsq.SelecionaCodigo();
begin
  if (QrChmHowCad.State <> dsInactive) and (QrChmHowCad.RecordCount > 0) then
  begin
    FCodigo := QrChmOcoCadCodigo.Value;
    Close;
  end;
end;

end.
