object FmChmOcoCad: TFmChmOcoCad
  Left = 368
  Top = 194
  Caption = 'CHM-OCORR-001 :: Cadastros de Chamados de Ocorr'#234'ncias'
  ClientHeight = 648
  ClientWidth = 1091
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1091
    Height = 552
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 0
      Top = 359
      Width = 1091
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ExplicitLeft = 8
      ExplicitTop = 437
      ExplicitWidth = 1008
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 488
      Width = 1091
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 568
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Chamado'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCabClick
        end
        object BtWho: TBitBtn
          Tag = 110
          Left = 246
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Designados'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtWhoClick
        end
        object BtEtp: TBitBtn
          Tag = 110
          Left = 125
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Etapas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtEtpClick
        end
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 1091
      Height = 241
      Align = alTop
      TabOrder = 1
      object Label5: TLabel
        Left = 8
        Top = 0
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 68
        Top = 0
        Width = 29
        Height = 13
        Caption = 'Titulo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label18: TLabel
        Left = 704
        Top = 0
        Width = 113
        Height = 13
        Caption = 'Descri'#231#227'o do que fazer:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label19: TLabel
        Left = 240
        Top = 40
        Width = 90
        Height = 13
        Caption = 'Autor do chamado:'
      end
      object Label20: TLabel
        Left = 532
        Top = 80
        Width = 61
        Height = 13
        Caption = 'M'#237'nimo P/D:'
      end
      object Label21: TLabel
        Left = 616
        Top = 80
        Width = 62
        Height = 13
        Caption = 'M'#225'ximo P/D:'
      end
      object LaDBCodOnde: TLabel
        Left = 432
        Top = 120
        Width = 29
        Height = 13
        Caption = 'Local:'
      end
      object Label23: TLabel
        Left = 8
        Top = 160
        Width = 97
        Height = 13
        Caption = 'Motivo do chamado:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label24: TLabel
        Left = 697
        Top = 161
        Width = 87
        Height = 13
        Caption = 'Data / hora inicial:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label25: TLabel
        Left = 849
        Top = 161
        Width = 80
        Height = 13
        Caption = 'Data / hora final:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label26: TLabel
        Left = 8
        Top = 200
        Width = 213
        Height = 13
        Caption = 'Fluxo de procedimentos a serem executados:'
      end
      object Label27: TLabel
        Left = 552
        Top = 200
        Width = 57
        Height = 13
        Caption = 'Un. medida:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label28: TLabel
        Left = 620
        Top = 200
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label29: TLabel
        Left = 712
        Top = 200
        Width = 36
        Height = 13
        Caption = 'Moeda:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label30: TLabel
        Left = 780
        Top = 200
        Width = 27
        Height = 13
        Caption = 'Valor:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label31: TLabel
        Left = 872
        Top = 200
        Width = 58
        Height = 13
        Caption = 'Import'#226'ncia:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label32: TLabel
        Left = 936
        Top = 200
        Width = 46
        Height = 13
        Caption = 'Urg'#234'ncia:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label33: TLabel
        Left = 432
        Top = 80
        Width = 50
        Height = 13
        Caption = 'Total P/D:'
        FocusControl = DBEdit3
      end
      object Label22: TLabel
        Left = 8
        Top = 40
        Width = 43
        Height = 13
        Caption = 'Abertura:'
        FocusControl = DBEdit6
      end
      object Label34: TLabel
        Left = 124
        Top = 40
        Width = 59
        Height = 13
        Caption = 'Fechamento'
        FocusControl = DBEdit18
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsChmOcoCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 64
        Top = 16
        Width = 633
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsChmOcoCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBMemo1: TDBMemo
        Left = 704
        Top = 16
        Width = 293
        Height = 101
        DataField = 'Descricao'
        DataSource = DsChmOcoCad
        TabOrder = 2
      end
      object DBEdit1: TDBEdit
        Left = 240
        Top = 56
        Width = 56
        Height = 21
        DataField = 'WhoGerEnti'
        DataSource = DsChmOcoCad
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 296
        Top = 56
        Width = 401
        Height = 21
        DataField = 'NO_WhoGerEnti'
        DataSource = DsChmOcoCad
        TabOrder = 4
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 8
        Top = 80
        Width = 417
        Height = 38
        Caption = '  Abrang'#234'ncia de Sele'#231#227'o de executores:  '
        Columns = 5
        DataField = 'KndIsWhoDo'
        DataSource = DsChmOcoCad
        Items.Strings = (
          'Ningu'#233'm'
          'Espec'#237'fico'
          'Dentre'
          'Alguns'
          'Todos')
        TabOrder = 5
        Values.Strings = (
          '0'
          '1024'
          '2048'
          '3072'
          '4096'
          '5120'
          '6144')
      end
      object DBEdit3: TDBEdit
        Left = 432
        Top = 96
        Width = 96
        Height = 21
        DataField = 'WhoDoQtdSel'
        DataSource = DsChmOcoCad
        TabOrder = 6
      end
      object DBEdit4: TDBEdit
        Left = 532
        Top = 96
        Width = 80
        Height = 21
        DataField = 'WhoDoQtdMin'
        DataSource = DsChmOcoCad
        TabOrder = 7
      end
      object DBEdit5: TDBEdit
        Left = 616
        Top = 96
        Width = 80
        Height = 21
        DataField = 'WhoDoQtdMax'
        DataSource = DsChmOcoCad
        TabOrder = 8
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 8
        Top = 120
        Width = 417
        Height = 38
        Caption = ' Origem de local: '
        Columns = 4
        DataField = 'KndOnde'
        DataSource = DsChmOcoCad
        Items.Strings = (
          'Indefinido'
          'Livre'
          'Estoque'
          'Prestador')
        TabOrder = 9
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7')
      end
      object EdDBOndeDescr: TDBEdit
        Left = 476
        Top = 108
        Width = 565
        Height = 21
        DataField = 'OndeDescr'
        DataSource = DsChmOcoCad
        TabOrder = 10
      end
      object EdDBCodOnde: TDBEdit
        Left = 432
        Top = 136
        Width = 56
        Height = 21
        DataField = 'CodOnde'
        DataSource = DsChmOcoCad
        TabOrder = 11
        OnChange = EdDBCodOndeChange
      end
      object CBDBCodOnde: TDBEdit
        Left = 492
        Top = 136
        Width = 505
        Height = 21
        DataField = 'NO_ONDE'
        DataSource = DsChmOcoCad
        TabOrder = 12
      end
      object DBEdit7: TDBEdit
        Left = 8
        Top = 176
        Width = 685
        Height = 21
        DataField = 'PorqueDescr'
        DataSource = DsChmOcoCad
        TabOrder = 13
      end
      object DBEdit8: TDBEdit
        Left = 697
        Top = 176
        Width = 148
        Height = 21
        DataField = 'QuandoIni'
        DataSource = DsChmOcoCad
        TabOrder = 14
      end
      object DBEdit9: TDBEdit
        Left = 849
        Top = 176
        Width = 148
        Height = 21
        DataField = 'QuandoFim'
        DataSource = DsChmOcoCad
        TabOrder = 15
      end
      object DBEdit10: TDBEdit
        Left = 8
        Top = 216
        Width = 56
        Height = 21
        DataField = 'ChmHowCad'
        DataSource = DsChmOcoCad
        TabOrder = 16
      end
      object DBEdit11: TDBEdit
        Left = 64
        Top = 216
        Width = 485
        Height = 21
        DataField = 'NO_ChmHowCad'
        DataSource = DsChmOcoCad
        TabOrder = 17
      end
      object DBEdit12: TDBEdit
        Left = 552
        Top = 216
        Width = 65
        Height = 21
        DataField = 'HowManyUnd'
        DataSource = DsChmOcoCad
        TabOrder = 18
      end
      object DBEdit13: TDBEdit
        Left = 620
        Top = 216
        Width = 88
        Height = 21
        DataField = 'HowManyQtd'
        DataSource = DsChmOcoCad
        TabOrder = 19
      end
      object DBEdit14: TDBEdit
        Left = 712
        Top = 216
        Width = 65
        Height = 21
        DataField = 'HowMuchMoed'
        DataSource = DsChmOcoCad
        TabOrder = 20
      end
      object DBEdit15: TDBEdit
        Left = 780
        Top = 216
        Width = 88
        Height = 21
        DataField = 'HowMuchValr'
        DataSource = DsChmOcoCad
        TabOrder = 21
      end
      object DBEdit16: TDBEdit
        Left = 872
        Top = 216
        Width = 61
        Height = 21
        DataField = 'Importancia'
        DataSource = DsChmOcoCad
        TabOrder = 22
      end
      object DBEdit17: TDBEdit
        Left = 936
        Top = 216
        Width = 61
        Height = 21
        DataField = 'Urgencia'
        DataSource = DsChmOcoCad
        TabOrder = 23
      end
      object DBEdit6: TDBEdit
        Left = 8
        Top = 56
        Width = 112
        Height = 21
        DataField = 'LstRAbrDtH'
        DataSource = DsChmOcoCad
        TabOrder = 24
      end
      object DBEdit18: TDBEdit
        Left = 124
        Top = 56
        Width = 112
        Height = 21
        DataField = 'LstFechDtH_TXT'
        DataSource = DsChmOcoCad
        TabOrder = 25
      end
    end
    object Panel7: TPanel
      Left = 0
      Top = 364
      Width = 1091
      Height = 124
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object Splitter2: TSplitter
        Left = 746
        Top = 0
        Width = 5
        Height = 124
        Align = alRight
        ExplicitLeft = 653
        ExplicitTop = 9
      end
      object GBWHo: TGroupBox
        Left = 0
        Top = 0
        Width = 746
        Height = 124
        Align = alClient
        Caption = ' Designados:'
        TabOrder = 0
        object DGWho: TDBGrid
          Left = 2
          Top = 15
          Width = 742
          Height = 107
          Align = alClient
          DataSource = DsChmOcoWho
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'WhoDoMobile'
              Title.Caption = 'ID mobile'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_WhoDoMobile'
              Title.Caption = 'Usu'#225'rio padr'#227'o mobile'
              Width = 214
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'WhoDoEnti'
              Title.Caption = 'ID Pessoa'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_WhoGerEnti'
              Title.Caption = 'Nome Pessoa'
              Width = 214
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SntNotif_TXT'
              Title.Caption = 'Notificado'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CfmRcbNotif_TXT'
              Title.Caption = 'Visualizado'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DoneDtHr_TXT'
              Title.Caption = 'Finalizado'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_REALIZADO'
              Title.Caption = 'Fez?'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReopenDtHr_TXT'
              Title.Caption = 'Reaberto em'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ObsEncerr'
              Title.Caption = 'Observa'#231#245'es de encerramento'
              Width = 600
              Visible = True
            end>
        end
      end
      object GroupBox1: TGroupBox
        Left = 751
        Top = 0
        Width = 340
        Height = 124
        Align = alRight
        Caption = ' Ocorrencias realizadas pelo designado selecionado: '
        TabOrder = 1
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 336
          Height = 107
          Align = alClient
          DataSource = DsChmOcoDon
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DON_DoneDtHr'
              Title.Caption = 'Data/hora realizado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ETP_Nome'
              Title.Caption = 'T'#237'tulo da etapa'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DON_Observacao'
              Width = 300
              Visible = True
            end>
        end
      end
    end
    object PnEtapas: TPanel
      Left = 0
      Top = 241
      Width = 1091
      Height = 72
      Align = alTop
      TabOrder = 3
      object Splitter3: TSplitter
        Left = 709
        Top = 1
        Width = 5
        Height = 70
        Align = alRight
        ExplicitLeft = 653
        ExplicitTop = 9
      end
      object GBEtp: TGroupBox
        Left = 1
        Top = 1
        Width = 708
        Height = 70
        Align = alClient
        Caption = ' Etapas: '
        TabOrder = 0
        object DBGEtp: TDBGrid
          Left = 2
          Top = 15
          Width = 704
          Height = 53
          Align = alClient
          DataSource = DsChmOcoEtp
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'T'#237'tulo da etapa'
              Width = 640
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QuandoIni_TXT'
              Title.Caption = 'In'#237'cio'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QuandoFim_TXT'
              Title.Caption = 'Final'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AlterWeb'
              Visible = True
            end>
        end
      end
      object GBDescrEtapa: TGroupBox
        Left = 714
        Top = 1
        Width = 376
        Height = 70
        Align = alRight
        Caption = ' Descri'#231#227'o da etapa selecionada: '
        TabOrder = 1
        object DBMemo2: TDBMemo
          Left = 2
          Top = 15
          Width = 372
          Height = 53
          Align = alClient
          DataField = 'Descricao'
          DataSource = DsChmOcoEtp
          TabOrder = 0
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1091
    Height = 552
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 489
      Width = 1091
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 951
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object Panel8: TPanel
        Left = 140
        Top = 24
        Width = 229
        Height = 29
        BevelOuter = bvNone
        TabOrder = 2
        object Label1: TLabel
          Left = 17
          Top = 6
          Width = 176
          Height = 16
          Caption = 'Ctrl + enter = Quebra de linha.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label2: TLabel
          Left = 16
          Top = 5
          Width = 176
          Height = 16
          Caption = 'Ctrl + enter = Quebra de linha.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object PnAltera: TPanel
      Left = 0
      Top = 0
      Width = 1091
      Height = 241
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 0
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 68
        Top = 0
        Width = 29
        Height = 13
        Caption = 'Titulo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 704
        Top = 0
        Width = 113
        Height = 13
        Caption = 'Descri'#231#227'o do que fazer:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 8
        Top = 40
        Width = 90
        Height = 13
        Caption = 'Autor do chamado:'
      end
      object LaWhoDoQtdMin: TLabel
        Left = 532
        Top = 80
        Width = 61
        Height = 13
        Caption = 'M'#237'nimo P/D:'
      end
      object LaWhoDoQtdMax: TLabel
        Left = 616
        Top = 80
        Width = 62
        Height = 13
        Caption = 'M'#225'ximo P/D:'
      end
      object LaCodOnde: TLabel
        Left = 432
        Top = 120
        Width = 29
        Height = 13
        Caption = 'Local:'
      end
      object Label8: TLabel
        Left = 8
        Top = 160
        Width = 97
        Height = 13
        Caption = 'Motivo do chamado:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 9
        Top = 201
        Width = 87
        Height = 13
        Caption = 'Data / hora inicial:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label10: TLabel
        Left = 161
        Top = 201
        Width = 80
        Height = 13
        Caption = 'Data / hora final:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 520
        Top = 160
        Width = 213
        Height = 13
        Caption = 'Fluxo de procedimentos a serem executados:'
      end
      object Label12: TLabel
        Left = 312
        Top = 200
        Width = 57
        Height = 13
        Caption = 'Un. medida:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 380
        Top = 200
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 472
        Top = 200
        Width = 36
        Height = 13
        Caption = 'Moeda:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label15: TLabel
        Left = 540
        Top = 200
        Width = 27
        Height = 13
        Caption = 'Valor:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 632
        Top = 200
        Width = 58
        Height = 13
        Caption = 'Import'#226'ncia:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label17: TLabel
        Left = 696
        Top = 200
        Width = 46
        Height = 13
        Caption = 'Urg'#234'ncia:'
        Color = clBtnFace
        ParentColor = False
      end
      object SbChmHowCad: TSpeedButton
        Left = 652
        Top = 175
        Width = 22
        Height = 22
        Caption = '...'
        OnClick = SbChmHowCadClick
      end
      object Label35: TLabel
        Left = 761
        Top = 201
        Width = 69
        Height = 13
        Caption = 'Encerramento:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 68
        Top = 16
        Width = 629
        Height = 21
        MaxLength = 100
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object MeDescricao: TdmkMemo
        Left = 704
        Top = 16
        Width = 293
        Height = 101
        MaxLength = 255
        TabOrder = 2
        WantReturns = False
        OnKeyDown = MeDescricaoKeyDown
        QryCampo = 'Descricao'
        UpdCampo = 'Descricao'
        UpdType = utYes
      end
      object RGKndIsWhoDo: TdmkRadioGroup
        Left = 8
        Top = 80
        Width = 517
        Height = 38
        Caption = ' Abrang'#234'ncia de Sele'#231#227'o de executores: '
        Columns = 5
        ItemIndex = 0
        Items.Strings = (
          'Ningu'#233'm'
          'Espec'#237'fico'
          'Dentre'
          'Alguns'
          'Todos')
        TabOrder = 3
        OnClick = RGKndIsWhoDoClick
        QryCampo = 'KndIsWhoDo'
        UpdCampo = 'KndIsWhoDo'
        UpdType = utYes
        OldValor = 0
      end
      object EdWhoGerEnti: TdmkEditCB
        Left = 8
        Top = 56
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'WhoGerEnti'
        UpdCampo = 'WhoGerEnti'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBWhoGerEnti
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBWhoGerEnti: TdmkDBLookupComboBox
        Left = 68
        Top = 56
        Width = 629
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsWhoGerEnti
        TabOrder = 5
        dmkEditCB = EdWhoGerEnti
        QryCampo = 'WhoGerEnti'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdWhoDoQtdMin: TdmkEdit
        Left = 532
        Top = 96
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'WhoDoQtdMin'
        UpdCampo = 'WhoDoQtdMin'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdWhoDoQtdMax: TdmkEdit
        Left = 616
        Top = 96
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'WhoDoQtdMax'
        UpdCampo = 'WhoDoQtdMax'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object RGKndOnde: TdmkRadioGroup
        Left = 8
        Top = 120
        Width = 417
        Height = 38
        Caption = ' Origem de local: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          'Livre'
          'Estoque'
          'Prestador')
        TabOrder = 8
        OnClick = RGKndOndeClick
        QryCampo = 'KndOnde'
        UpdCampo = 'KndOnde'
        UpdType = utYes
        OldValor = 0
      end
      object EdCodOnde: TdmkEditCB
        Left = 432
        Top = 136
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodOnde'
        UpdCampo = 'CodOnde'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCodOnde
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCodOnde: TdmkDBLookupComboBox
        Left = 492
        Top = 136
        Width = 505
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOnde
        TabOrder = 10
        dmkEditCB = EdCodOnde
        QryCampo = 'CodOnde'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdOndeDescr: TdmkEdit
        Left = 468
        Top = 120
        Width = 565
        Height = 21
        MaxLength = 60
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'OndeDescr'
        UpdCampo = 'OndeDescr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPorqueDescr: TdmkEdit
        Left = 8
        Top = 176
        Width = 509
        Height = 21
        MaxLength = 255
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'PorqueDescr'
        UpdCampo = 'PorqueDescr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object TPQuandoIni: TdmkEditDateTimePicker
        Left = 9
        Top = 216
        Width = 108
        Height = 21
        Date = 0.639644131944805800
        Time = 0.639644131944805800
        TabOrder = 15
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'QuandoIni'
        UpdCampo = 'QuandoIni'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdQuandoIni: TdmkEdit
        Left = 117
        Top = 216
        Width = 40
        Height = 21
        TabOrder = 16
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'QuandoIni'
        UpdCampo = 'QuandoIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPQuandoFim: TdmkEditDateTimePicker
        Left = 161
        Top = 216
        Width = 108
        Height = 21
        Date = 0.639644131944805800
        Time = 0.639644131944805800
        TabOrder = 17
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'QuandoFim'
        UpdCampo = 'QuandoFim'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdQuandoFim: TdmkEdit
        Left = 269
        Top = 216
        Width = 40
        Height = 21
        TabOrder = 18
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'QuandoFim'
        UpdCampo = 'QuandoFim'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdChmHowCad: TdmkEditCB
        Left = 520
        Top = 176
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ChmHowCad'
        UpdCampo = 'ChmHowCad'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBChmHowCad
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBChmHowCad: TdmkDBLookupComboBox
        Left = 580
        Top = 176
        Width = 417
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsChmHowCad
        TabOrder = 14
        dmkEditCB = EdChmHowCad
        QryCampo = 'ChmHowCad'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdHowManyUnd: TdmkEdit
        Left = 312
        Top = 216
        Width = 65
        Height = 21
        TabOrder = 19
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'HowManyUnd'
        UpdCampo = 'HowManyUnd'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdHowManyQtd: TdmkEdit
        Left = 380
        Top = 216
        Width = 88
        Height = 21
        Alignment = taRightJustify
        TabOrder = 20
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'HowManyQtd'
        UpdCampo = 'HowManyQtd'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdHowMuchMoed: TdmkEdit
        Left = 472
        Top = 216
        Width = 65
        Height = 21
        TabOrder = 21
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'HowMuchMoed'
        UpdCampo = 'HowMuchMoed'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdHowMuchValr: TdmkEdit
        Left = 540
        Top = 216
        Width = 88
        Height = 21
        Alignment = taRightJustify
        TabOrder = 22
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'HowMuchValr'
        UpdCampo = 'HowMuchValr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdImportancia: TdmkEdit
        Left = 632
        Top = 216
        Width = 61
        Height = 21
        Alignment = taRightJustify
        TabOrder = 23
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '50'
        QryCampo = 'Importancia'
        UpdCampo = 'Importancia'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 50.000000000000000000
        ValWarn = False
      end
      object EdUrgencia: TdmkEdit
        Left = 696
        Top = 216
        Width = 61
        Height = 21
        Alignment = taRightJustify
        TabOrder = 24
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '50'
        QryCampo = 'Urgencia'
        UpdCampo = 'Urgencia'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 50.000000000000000000
        ValWarn = False
      end
      object TPLstFechDtH: TdmkEditDateTimePicker
        Left = 761
        Top = 216
        Width = 108
        Height = 21
        Date = 0.639644131944805800
        Time = 0.639644131944805800
        TabOrder = 25
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'LstFechDtH'
        UpdCampo = 'LstFechDtH'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdLstFechDtH: TdmkEdit
        Left = 869
        Top = 216
        Width = 40
        Height = 21
        TabOrder = 26
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'LstFechDtH'
        UpdCampo = 'LstFechDtH'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1091
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1043
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 827
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 495
        Height = 32
        Caption = 'Cadastros de Chamados de Ocorr'#234'ncias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 495
        Height = 32
        Caption = 'Cadastros de Chamados de Ocorr'#234'ncias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 495
        Height = 32
        Caption = 'Cadastros de Chamados de Ocorr'#234'ncias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1091
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1087
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 56
    Top = 48
  end
  object QrChmOcoCad: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeOpen = QrChmOcoCadBeforeOpen
    AfterOpen = QrChmOcoCadAfterOpen
    BeforeClose = QrChmOcoCadBeforeClose
    AfterScroll = QrChmOcoCadAfterScroll
    OnCalcFields = QrChmOcoCadCalcFields
    SQL.Strings = (
      'SELECT oca.*, chc.Nome NO_ChmHowCad, sen.Login NO_UserLog,'
      'IF(wge.Tipo=0, wge.RazaoSocial, wge.Nome) NO_WhoGerEnti, '
      'IF(oca.chmtitoco<>0, tit.NOme, oca.Nome) NO_Titulo,'
      'CASE KndOnde'
      '  WHEN 0 THEN oca.OndeDescr'
      '  /*WHEN 1 THEN scc.Nome*/'
      '  WHEN 2 THEN ovl.Nome'
      '  ELSE "????"'
      'END NO_ONDE,'
      'IF(oca.WhoGerEnti<>0, tit.NOme, oca.Nome) NO_chmocowho'
      'FROM chmococad oca'
      'LEFT JOIN chmtitoco tit ON tit.Codigo=oca.chmtitoco'
      'LEFT JOIN entidades wge ON wge.Codigo=oca.WhoGerEnti'
      '/*LEFT JOIN stqcencad scc ON scc.Codigo=oca.CodOnde*/'
      'LEFT JOIN ovdlocal  ovl ON ovl.Codigo=oca.CodOnde'
      'LEFT JOIN chmhowcad chc ON chc.Codigo=oca.ChmHowCad'
      'LEFT JOIN senhas    sen ON sen.Numero=oca.Usercad'
      'WHERE oca.Codigo > 0')
    Left = 196
    Top = 21
    object QrChmOcoCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrChmOcoCadKndNome: TSmallintField
      FieldName = 'KndNome'
      Required = True
    end
    object QrChmOcoCadChmTitOco: TIntegerField
      FieldName = 'ChmTitOco'
      Required = True
    end
    object QrChmOcoCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrChmOcoCadDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrChmOcoCadWhoGerEnti: TIntegerField
      FieldName = 'WhoGerEnti'
      Required = True
    end
    object QrChmOcoCadWhoDoQtdSel: TIntegerField
      FieldName = 'WhoDoQtdSel'
      Required = True
    end
    object QrChmOcoCadWhoDoQtdMin: TIntegerField
      FieldName = 'WhoDoQtdMin'
      Required = True
    end
    object QrChmOcoCadWhoDoQtdMax: TIntegerField
      FieldName = 'WhoDoQtdMax'
      Required = True
    end
    object QrChmOcoCadKndOnde: TSmallintField
      FieldName = 'KndOnde'
      Required = True
    end
    object QrChmOcoCadCodOnde: TIntegerField
      FieldName = 'CodOnde'
      Required = True
    end
    object QrChmOcoCadOndeDescr: TWideStringField
      FieldName = 'OndeDescr'
      Required = True
      Size = 60
    end
    object QrChmOcoCadPorqueDescr: TWideStringField
      FieldName = 'PorqueDescr'
      Size = 255
    end
    object QrChmOcoCadQuandoIni: TDateTimeField
      FieldName = 'QuandoIni'
      Required = True
    end
    object QrChmOcoCadQuandoFim: TDateTimeField
      FieldName = 'QuandoFim'
      Required = True
    end
    object QrChmOcoCadChmHowCad: TIntegerField
      FieldName = 'ChmHowCad'
      Required = True
    end
    object QrChmOcoCadHowManyUnd: TWideStringField
      FieldName = 'HowManyUnd'
      Size = 6
    end
    object QrChmOcoCadHowManyQtd: TFloatField
      FieldName = 'HowManyQtd'
      Required = True
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrChmOcoCadHowMuchMoed: TWideStringField
      FieldName = 'HowMuchMoed'
      Size = 3
    end
    object QrChmOcoCadHowMuchValr: TFloatField
      FieldName = 'HowMuchValr'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrChmOcoCadPriAbrtDtH: TDateTimeField
      FieldName = 'PriAbrtDtH'
      Required = True
    end
    object QrChmOcoCadLstRAbrDtH: TDateTimeField
      FieldName = 'LstRAbrDtH'
      Required = True
    end
    object QrChmOcoCadLstFechDtH: TDateTimeField
      FieldName = 'LstFechDtH'
      Required = True
    end
    object QrChmOcoCadImportancia: TSmallintField
      FieldName = 'Importancia'
      Required = True
    end
    object QrChmOcoCadUrgencia: TSmallintField
      FieldName = 'Urgencia'
      Required = True
    end
    object QrChmOcoCadLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrChmOcoCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChmOcoCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChmOcoCadUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrChmOcoCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrChmOcoCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrChmOcoCadAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrChmOcoCadAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrChmOcoCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrChmOcoCadNO_ChmHowCad: TWideStringField
      FieldName = 'NO_ChmHowCad'
      Size = 60
    end
    object QrChmOcoCadNO_UserLog: TWideStringField
      FieldName = 'NO_UserLog'
      Size = 30
    end
    object QrChmOcoCadNO_WhoGerEnti: TWideStringField
      FieldName = 'NO_WhoGerEnti'
      Size = 100
    end
    object QrChmOcoCadNO_Titulo: TWideStringField
      FieldName = 'NO_Titulo'
      Size = 60
    end
    object QrChmOcoCadNO_ONDE: TWideStringField
      FieldName = 'NO_ONDE'
      Size = 100
    end
    object QrChmOcoCadNO_chmocowho: TWideStringField
      FieldName = 'NO_chmocowho'
      Size = 60
    end
    object QrChmOcoCadKndIsWhoDo: TIntegerField
      FieldName = 'KndIsWhoDo'
    end
    object QrChmOcoCadONDE_TXT: TStringField
      FieldKind = fkCalculated
      FieldName = 'ONDE_TXT'
      Size = 255
      Calculated = True
    end
    object QrChmOcoCadLstFechDtH_TXT: TWideStringField
      FieldName = 'LstFechDtH_TXT'
      Size = 19
    end
  end
  object DsChmOcoCad: TDataSource
    DataSet = QrChmOcoCad
    Left = 196
    Top = 65
  end
  object QrChmOcoWho: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrChmOcoWhoBeforeClose
    AfterScroll = QrChmOcoWhoAfterScroll
    SQL.Strings = (
      'SELECT ccw.*, mdc.Nome NO_WhoDoMobile, mdc.PushNotifID, '
      'IF(wde.Tipo=0, wde.RazaoSocial, wde.Nome) NO_WhoGerEnti, '
      
        'IF(ccw.SntNotif<2, "", DATE_FORMAT(ccw.SntNotif, "%d/%m/%y %H:%i' +
        ':%s")) SntNotif_TXT, '
      
        'IF(ccw.CfmRcbNotif<2, "", DATE_FORMAT(ccw.CfmRcbNotif, "%d/%m/%y' +
        ' %H:%i:%s")) CfmRcbNotif_TXT, '
      
        'IF(ccw.DoneDtHr<2, "", DATE_FORMAT(ccw.DoneDtHr, "%d/%m/%y %H:%i' +
        ':%s")) DoneDtHr_TXT,'
      'IF(ccw.Realizado=0, "N'#227'o", "Sim") NO_REALIZADO '
      'FROM chmocowho ccw '
      'LEFT JOIN entidades wde ON wde.Codigo=ccw.WhoDoEnti '
      'LEFT JOIN ovcmobdevcad mdc ON mdc.Codigo=ccw.WhoDoMobile '
      'WHERE ccw.Codigo>0')
    Left = 252
    Top = 45
    object QrChmOcoWhoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrChmOcoWhoControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrChmOcoWhoOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrChmOcoWhoKndWhoPrtnc: TIntegerField
      FieldName = 'KndWhoPrtnc'
      Required = True
    end
    object QrChmOcoWhoWhoDoEnti: TIntegerField
      FieldName = 'WhoDoEnti'
      Required = True
    end
    object QrChmOcoWhoWhoDoMobile: TIntegerField
      FieldName = 'WhoDoMobile'
      Required = True
    end
    object QrChmOcoWhoLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrChmOcoWhoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChmOcoWhoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChmOcoWhoUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrChmOcoWhoUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrChmOcoWhoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrChmOcoWhoAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrChmOcoWhoAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrChmOcoWhoAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrChmOcoWhoNO_WhoDoMobile: TWideStringField
      FieldName = 'NO_WhoDoMobile'
      Size = 60
    end
    object QrChmOcoWhoNO_WhoGerEnti: TWideStringField
      FieldName = 'NO_WhoGerEnti'
      Size = 100
    end
    object QrChmOcoWhoPushNotifID: TWideStringField
      FieldName = 'PushNotifID'
      Size = 4096
    end
    object QrChmOcoWhoSntNotif: TDateTimeField
      FieldName = 'SntNotif'
    end
    object QrChmOcoWhoSntNotif_TXT: TWideStringField
      FieldName = 'SntNotif_TXT'
      Size = 19
    end
    object QrChmOcoWhoCfmRcbNotif: TDateTimeField
      FieldName = 'CfmRcbNotif'
    end
    object QrChmOcoWhoCfmRcbNotif_TXT: TWideStringField
      FieldName = 'CfmRcbNotif_TXT'
      Size = 19
    end
    object QrChmOcoWhoDoneDtHr_TXT: TWideStringField
      FieldName = 'DoneDtHr_TXT'
      Size = 19
    end
    object QrChmOcoWhoCfmDtHrLido: TDateTimeField
      FieldName = 'CfmDtHrLido'
      Required = True
    end
    object QrChmOcoWhoDoneDtHr: TDateTimeField
      FieldName = 'DoneDtHr'
      Required = True
    end
    object QrChmOcoWhoMobUpWeb: TSmallintField
      FieldName = 'MobUpWeb'
      Required = True
    end
    object QrChmOcoWhoRealizado: TSmallintField
      FieldName = 'Realizado'
      Required = True
    end
    object QrChmOcoWhoObsEncerr: TWideStringField
      FieldName = 'ObsEncerr'
      Size = 255
    end
    object QrChmOcoWhoNO_REALIZADO: TWideStringField
      FieldName = 'NO_REALIZADO'
      Required = True
      Size = 3
    end
    object QrChmOcoWhoReopenDtHr: TDateTimeField
      FieldName = 'ReopenDtHr'
    end
    object QrChmOcoWhoReopenDtHr_TXT: TWideStringField
      FieldName = 'ReopenDtHr_TXT'
      Size = 19
    end
  end
  object DsChmOcoWho: TDataSource
    DataSet = QrChmOcoWho
    Left = 252
    Top = 89
  end
  object PMWho: TPopupMenu
    OnPopup = PMWhoPopup
    Left = 852
    Top = 556
    object ItsInclui1: TMenuItem
      Caption = '&Inclui'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Reenvianotificaomobile1: TMenuItem
      Caption = 'Reenvia notifica'#231#227'o mobile'
      OnClick = Reenvianotificaomobile1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Reabrechamado1: TMenuItem
      Caption = 'Reabre chamado'
      OnClick = Reabrechamado1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 620
    Top = 564
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrWhoGerEnti: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE, Codigo'
      'FROM entidades '
      'WHERE Tipo=1'
      'ORDER BY NOMEENTIDADE')
    Left = 372
    Top = 50
    object QrWhoGerEntiNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrWhoGerEntiCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsWhoGerEnti: TDataSource
    DataSet = QrWhoGerEnti
    Left = 372
    Top = 94
  end
  object QrOnde: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ovdlocal'
      'ORDER BY Nome'
      '')
    Left = 428
    Top = 30
    object QrOndeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOndeNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsOnde: TDataSource
    DataSet = QrOnde
    Left = 428
    Top = 74
  end
  object QrChmHowCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM chmhowcad'
      'ORDER BY Nome'
      '')
    Left = 484
    Top = 54
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsChmHowCad: TDataSource
    DataSet = QrChmHowCad
    Left = 484
    Top = 98
  end
  object PMEtp: TPopupMenu
    OnPopup = PMEtpPopup
    Left = 744
    Top = 560
    object ItsInclui2: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui2Click
    end
    object ItsAltera2: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera2Click
    end
    object ItsExclui2: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui2Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object este1: TMenuItem
      Caption = 'Teste'
      Enabled = False
      OnClick = este1Click
    end
  end
  object QrChmOcoEtp: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT etp.*'
      'FROM chmocoetp etp'
      'WHERE etp.Codigo=0')
    Left = 312
    Top = 25
    object QrChmOcoEtpCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrChmOcoEtpControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrChmOcoEtpOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrChmOcoEtpNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrChmOcoEtpDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrChmOcoEtpQuandoIni: TDateTimeField
      FieldName = 'QuandoIni'
      Required = True
    end
    object QrChmOcoEtpQuandoFim: TDateTimeField
      FieldName = 'QuandoFim'
      Required = True
    end
    object QrChmOcoEtpLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrChmOcoEtpDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChmOcoEtpDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChmOcoEtpUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrChmOcoEtpUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrChmOcoEtpAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrChmOcoEtpAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrChmOcoEtpAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrChmOcoEtpAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrChmOcoEtpCadEtpCod: TIntegerField
      FieldName = 'CadEtpCod'
    end
    object QrChmOcoEtpCadEtpCtrl: TIntegerField
      FieldName = 'CadEtpCtrl'
    end
    object QrChmOcoEtpQuandoIni_TXT: TWideStringField
      FieldName = 'QuandoIni_TXT'
    end
    object QrChmOcoEtpQuandoFim_TXT: TWideStringField
      FieldName = 'QuandoFim_TXT'
    end
    object QrChmOcoEtpDoneDtHr: TDateTimeField
      FieldName = 'DoneDtHr'
      Required = True
    end
    object QrChmOcoEtpCloseUser: TIntegerField
      FieldName = 'CloseUser'
      Required = True
    end
    object QrChmOcoEtpCloseDtHr: TDateTimeField
      FieldName = 'CloseDtHr'
      Required = True
    end
  end
  object DsChmOcoEtp: TDataSource
    DataSet = QrChmOcoEtp
    Left = 312
    Top = 69
  end
  object QrChmHowEtp: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT *'
      'FROM chmhowetp'
      'ORDER BY Ordem, Controle'
      '')
    Left = 544
    Top = 34
    object QrChmHowEtpCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrChmHowEtpControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrChmHowEtpOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrChmHowEtpNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrChmHowEtpDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
  end
  object DsChmHowEtp: TDataSource
    DataSet = QrChmHowEtp
    Left = 544
    Top = 78
  end
  object QrChmOcoDon: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT don.*, don.ChmOcoWho WHO_Controle,'
      'don.Codigo, don.Controle DON_Controle,'
      'don.ChmOcoEtp ETP_Controle, don.DoneDtHr DON_DoneDtHr,'
      'don.MobUpWeb DON_MobWebUp, don.CloseUser DON_CloseUser,'
      'don.CLoseDtHr DON_CLoseDtHr, don.Observacao'
      'DON_Observacao, etp.Ordem ETP_Ordem, etp.Nome ETP_Nome,'
      'etp.Descricao ETP_Descricao, etp.QuandoIni'
      'ETP_QuandoIni, etp.QuandoFim ETP_QuandoFim,'
      'etp.CloseUser ETP_CloseUser, etp.CloseDtHr'
      'ETP_CloseDtHr, etp.DoneDtHr ETP_DoneDtHr'
      'FROM chmocodon don'
      'LEFT JOIN chmocoetp etp ON etp.Controle=don.ChmOcoEtp'
      'LEFT JOIN chmocowho who ON who.Controle=don.ChmOcoWho'
      'WHERE don.Codigo=1'
      '/*AND who.WhoDoMobile>0*/'
      'AND don.ChmOcoWho=1'
      'ORDER BY etp.Ordem, etp.Controle'
      '')
    Left = 606
    Top = 62
    object QrChmOcoDonCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrChmOcoDonControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrChmOcoDonChmOcoEtp: TIntegerField
      FieldName = 'ChmOcoEtp'
      Required = True
    end
    object QrChmOcoDonChmOcoWho: TIntegerField
      FieldName = 'ChmOcoWho'
      Required = True
    end
    object QrChmOcoDonObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 255
    end
    object QrChmOcoDonDoneDtHr: TDateTimeField
      FieldName = 'DoneDtHr'
      Required = True
    end
    object QrChmOcoDonMobUpWeb: TSmallintField
      FieldName = 'MobUpWeb'
      Required = True
    end
    object QrChmOcoDonCloseUser: TIntegerField
      FieldName = 'CloseUser'
      Required = True
    end
    object QrChmOcoDonCloseDtHr: TDateTimeField
      FieldName = 'CloseDtHr'
      Required = True
    end
    object QrChmOcoDonLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrChmOcoDonDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChmOcoDonDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChmOcoDonUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrChmOcoDonUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrChmOcoDonAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrChmOcoDonAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrChmOcoDonAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrChmOcoDonAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrChmOcoDonWHO_Controle: TIntegerField
      FieldName = 'WHO_Controle'
      Required = True
    end
    object QrChmOcoDonCodigo_1: TIntegerField
      FieldName = 'Codigo_1'
      Required = True
    end
    object QrChmOcoDonDON_Controle: TIntegerField
      FieldName = 'DON_Controle'
      Required = True
    end
    object QrChmOcoDonETP_Controle: TIntegerField
      FieldName = 'ETP_Controle'
      Required = True
    end
    object QrChmOcoDonDON_DoneDtHr: TDateTimeField
      FieldName = 'DON_DoneDtHr'
      Required = True
    end
    object QrChmOcoDonDON_MobWebUp: TSmallintField
      FieldName = 'DON_MobWebUp'
      Required = True
    end
    object QrChmOcoDonDON_CloseUser: TIntegerField
      FieldName = 'DON_CloseUser'
      Required = True
    end
    object QrChmOcoDonDON_CLoseDtHr: TDateTimeField
      FieldName = 'DON_CLoseDtHr'
      Required = True
    end
    object QrChmOcoDonDON_Observacao: TWideStringField
      FieldName = 'DON_Observacao'
      Size = 255
    end
    object QrChmOcoDonETP_Ordem: TIntegerField
      FieldName = 'ETP_Ordem'
    end
    object QrChmOcoDonETP_Nome: TWideStringField
      FieldName = 'ETP_Nome'
      Size = 60
    end
    object QrChmOcoDonETP_Descricao: TWideStringField
      FieldName = 'ETP_Descricao'
      Size = 255
    end
    object QrChmOcoDonETP_QuandoIni: TDateTimeField
      FieldName = 'ETP_QuandoIni'
    end
    object QrChmOcoDonETP_QuandoFim: TDateTimeField
      FieldName = 'ETP_QuandoFim'
    end
    object QrChmOcoDonETP_CloseUser: TIntegerField
      FieldName = 'ETP_CloseUser'
    end
    object QrChmOcoDonETP_CloseDtHr: TDateTimeField
      FieldName = 'ETP_CloseDtHr'
    end
    object QrChmOcoDonETP_DoneDtHr: TDateTimeField
      FieldName = 'ETP_DoneDtHr'
    end
  end
  object DsChmOcoDon: TDataSource
    DataSet = QrChmOcoDon
    Left = 607
    Top = 108
  end
end
