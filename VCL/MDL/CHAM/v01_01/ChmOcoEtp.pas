unit ChmOcoEtp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkMemo, Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmChmOcoEtp = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdOrdem: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label4: TLabel;
    MeDescricao: TdmkMemo;
    Label53: TLabel;
    TPQuandoIni: TdmkEditDateTimePicker;
    EdQuandoIni: TdmkEdit;
    Label10: TLabel;
    TPQuandoFim: TdmkEditDateTimePicker;
    EdQuandoFim: TdmkEdit;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmChmOcoEtp: TFmChmOcoEtp;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck,
  DmkDAC_PF, UnEntities, UnOVS_Jan, ChmOcoCad;

{$R *.DFM}

procedure TFmChmOcoEtp.BtOKClick(Sender: TObject);
var
  Nome, Descricao, QuandoIni, QuandoFim: String;
  Codigo, Controle, Ordem: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Descricao      := MeDescricao.Text;
  QuandoIni      := Geral.FDT_TP_Ed(TPQuandoIni.Date, EdQuandoIni.Text);
  QuandoFim      := Geral.FDT_TP_Ed(TPQuandoFim.Date, EdQuandoFim.Text);
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina um t�tulo!') then Exit;
  //
  Controle := UMyMod.BPGS1I32('chmocoetp', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'chmocoetp', False, [
  'Codigo', 'Ordem', 'Nome',
  'Descricao', 'QuandoIni', 'QuandoFim'], [
  'Controle'], [
  Codigo, Ordem, Nome,
  Descricao, QuandoIni, QuandoFim], [
  Controle], True) then
  begin
    Dmod.CriaItensChmOcoDon(LaAviso1, LaAviso2, Codigo);
    //
    ReopenCadastro_Com_Itens_ITS(Controle);
    FmChmOcoCad.ReordenaItens(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType             := stIns;
      EdControle.ValueVariant     := 0;
      EdOrdem.ValueVariant        := EdOrdem.ValueVariant + 1;
      EdNome.ValueVariant         := '';
      //
      EdOrdem.SetFocus;
    end else Close;
  end;
end;

procedure TFmChmOcoEtp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChmOcoEtp.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmChmOcoEtp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmChmOcoEtp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmChmOcoEtp.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
