unit ChmOcoWho;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmChmOcoWho = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdOrdem: TdmkEdit;
    Label7: TLabel;
    EdWhoDoEnti: TdmkEditCB;
    Label1: TLabel;
    SbWhoDoEnti: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    QrOVcMobDevCad: TMySQLQuery;
    DsOVcMobDevCad: TDataSource;
    CBWhoDoEnti: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdWhoDoMobile: TdmkEditCB;
    CBWhoDoMobile: TdmkDBLookupComboBox;
    SbWhoDoMobile: TSpeedButton;
    RGKndWhoPrtnc: TRadioGroup;
    QrOVcMobDevCadCodigo: TIntegerField;
    QrOVcMobDevCadNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbWhoDoEntiClick(Sender: TObject);
    procedure SbWhoDoMobileClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmChmOcoWho: TFmChmOcoWho;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck,
  DmkDAC_PF, UnEntities, UnOVS_Jan, UnOVS_PF, ChmOcoCad;

{$R *.DFM}

procedure TFmChmOcoWho.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Ordem, KndWhoPrtnc, WhoDoEnti, WhoDoMobile: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  KndWhoPrtnc    := RGKndWhoPrtnc.ItemIndex;
  WhoDoEnti      := EdWhoDoEnti.ValueVariant;
  WhoDoMobile    := EdWhoDoMobile.ValueVariant;
  //
  if MyObjects.FIC(KndWhoPrtnc = 0, RGKndWhoPrtnc,
  'Informe a pertin�ncia dessa designa��o!') then Exit;
  //
  if MyObjects.FIC((WhoDoEnti = 0) and (WhoDoMobile = 0), nil,
  'Informe a pessoa e/ou o equipamento mobile!') then Exit;
  //
  case KndWhoPrtnc of
    1: KndWhoPrtnc := 1024;
    2: KndWhoPrtnc := 2048;
    3: KndWhoPrtnc := 3072;
  end;
  //
  Controle := UMyMod.BPGS1I32('chmocowho', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'chmocowho', False, [
  'Codigo', 'Ordem', 'KndWhoPrtnc',
  'WhoDoEnti', 'WhoDoMobile'], [
  'Controle'], [
  Codigo, Ordem, KndWhoPrtnc,
  WhoDoEnti, WhoDoMobile], [
  Controle], True) then
  begin
    Dmod.CriaItensChmOcoDon(LaAviso1, LaAviso2, Codigo);
    //
    ReopenCadastro_Com_Itens_ITS(Controle);
    //
    if WhoDoMobile <> 0 then
      FmChmOcoCad.EnviaNotificacao(True);
   //
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType             := stIns;
      EdControle.ValueVariant     := 0;
      EdOrdem.ValueVariant        := EdOrdem.ValueVariant + 1;
      EdWhoDoEnti.ValueVariant    := 0;
      CBWhoDoEnti.KeyValue        := Null;
      EdWhoDoMobile.ValueVariant  := 0;
      CBWhoDoMobile.KeyValue      := 0;
      RGKndWhoPrtnc.ItemIndex     := 0;
      //
      EdOrdem.SetFocus;
    end else Close;
  end;
end;

procedure TFmChmOcoWho.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChmOcoWho.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmChmOcoWho.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOVcMobDevCad, Dmod.MyDB);
end;

procedure TFmChmOcoWho.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmChmOcoWho.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmChmOcoWho.SbWhoDoEntiClick(Sender: TObject);
begin
  Entities.CadastroDeEntidade(0, fmCadSelecionar, fmCadSelecionar, False);
  VAR_CADASTRO := 0;
  if VAR_CADASTRO <> 0 then
  begin
      UMyMod.SetaCodigoPesquisado(EdWhoDoEnti, CBWhoDoEnti, QrEntidades, VAR_CADASTRO);
      EdWhoDoEnti.SetFocus;
  end else
   UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmChmOcoWho.SbWhoDoMobileClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcMobDevCad();
  UMyMod.SetaCodigoPesquisado(EdWhoDoMobile, CBWhoDoMobile, QrOVcMobDevCad, VAR_CADASTRO);
end;

end.
