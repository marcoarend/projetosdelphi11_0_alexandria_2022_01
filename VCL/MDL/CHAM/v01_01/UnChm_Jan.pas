unit UnChm_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, Variants;

type
  TUnChm_Jan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormChmHowCad(Codigo: Integer);

    procedure MostraFormChmOcoCad();
  end;

var
  Chm_Jan: TUnChm_Jan;


implementation

uses
  dmkGeral, MyDBCheck, Module, CfgCadLista,
  ChmHowCad,
  ChmOcoCad;

{ TUnChm_Jan }

{ TUnChm_Jan }

procedure TUnChm_Jan.MostraFormChmHowCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmChmHowCad, FmChmHowCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmChmHowCad.QrChmHowCad.Locate('Codigo', Codigo, []);
    FmChmHowCad.ShowModal;
    FmChmHowCad.Destroy;
  end;
end;

procedure TUnChm_Jan.MostraFormChmOcoCad();
begin
  if DBCheck.CriaFm(TFmChmOcoCad, FmChmOcoCad, afmoNegarComAviso) then
  begin
    //if Codigo <> 0 then
      //FmChmOcoCad.QrChmOcoCad.Locate('Codigo', Codigo, []);
    FmChmOcoCad.ShowModal;
    FmChmOcoCad.Destroy;
  end;
end;

end.
