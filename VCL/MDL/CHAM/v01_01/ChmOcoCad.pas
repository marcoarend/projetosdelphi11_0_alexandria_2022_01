unit ChmOcoCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkMemo, dmkDBLookupComboBox, dmkEditCB,
  Vcl.ComCtrls, dmkEditDateTimePicker, UnProjGroup_Consts;

type
  TFmChmOcoCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrChmOcoCad: TMySQLQuery;
    DsChmOcoCad: TDataSource;
    QrChmOcoWho: TMySQLQuery;
    DsChmOcoWho: TDataSource;
    PMWho: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtWho: TBitBtn;
    QrChmOcoWhoCodigo: TIntegerField;
    QrChmOcoWhoControle: TIntegerField;
    QrChmOcoWhoOrdem: TIntegerField;
    QrChmOcoWhoKndWhoPrtnc: TIntegerField;
    QrChmOcoWhoWhoDoEnti: TIntegerField;
    QrChmOcoWhoWhoDoMobile: TIntegerField;
    QrChmOcoWhoLk: TIntegerField;
    QrChmOcoWhoDataCad: TDateField;
    QrChmOcoWhoDataAlt: TDateField;
    QrChmOcoWhoUserCad: TIntegerField;
    QrChmOcoWhoUserAlt: TIntegerField;
    QrChmOcoWhoAlterWeb: TSmallintField;
    QrChmOcoWhoAWServerID: TIntegerField;
    QrChmOcoWhoAWStatSinc: TSmallintField;
    QrChmOcoWhoAtivo: TSmallintField;
    QrChmOcoWhoNO_WhoDoMobile: TWideStringField;
    QrChmOcoWhoNO_WhoGerEnti: TWideStringField;
    QrChmOcoCadCodigo: TIntegerField;
    QrChmOcoCadKndNome: TSmallintField;
    QrChmOcoCadChmTitOco: TIntegerField;
    QrChmOcoCadNome: TWideStringField;
    QrChmOcoCadDescricao: TWideStringField;
    QrChmOcoCadWhoGerEnti: TIntegerField;
    QrChmOcoCadWhoDoQtdSel: TIntegerField;
    QrChmOcoCadWhoDoQtdMin: TIntegerField;
    QrChmOcoCadWhoDoQtdMax: TIntegerField;
    QrChmOcoCadKndOnde: TSmallintField;
    QrChmOcoCadCodOnde: TIntegerField;
    QrChmOcoCadOndeDescr: TWideStringField;
    QrChmOcoCadPorqueDescr: TWideStringField;
    QrChmOcoCadQuandoIni: TDateTimeField;
    QrChmOcoCadQuandoFim: TDateTimeField;
    QrChmOcoCadChmHowCad: TIntegerField;
    QrChmOcoCadHowManyUnd: TWideStringField;
    QrChmOcoCadHowManyQtd: TFloatField;
    QrChmOcoCadHowMuchMoed: TWideStringField;
    QrChmOcoCadHowMuchValr: TFloatField;
    QrChmOcoCadPriAbrtDtH: TDateTimeField;
    QrChmOcoCadLstRAbrDtH: TDateTimeField;
    QrChmOcoCadLstFechDtH: TDateTimeField;
    QrChmOcoCadImportancia: TSmallintField;
    QrChmOcoCadUrgencia: TSmallintField;
    QrChmOcoCadLk: TIntegerField;
    QrChmOcoCadDataCad: TDateField;
    QrChmOcoCadDataAlt: TDateField;
    QrChmOcoCadUserCad: TIntegerField;
    QrChmOcoCadUserAlt: TIntegerField;
    QrChmOcoCadAlterWeb: TSmallintField;
    QrChmOcoCadAWServerID: TIntegerField;
    QrChmOcoCadAWStatSinc: TSmallintField;
    QrChmOcoCadAtivo: TSmallintField;
    QrChmOcoCadNO_ChmHowCad: TWideStringField;
    QrChmOcoCadNO_UserLog: TWideStringField;
    QrChmOcoCadNO_WhoGerEnti: TWideStringField;
    QrChmOcoCadNO_Titulo: TWideStringField;
    QrChmOcoCadNO_ONDE: TWideStringField;
    QrChmOcoCadNO_chmocowho: TWideStringField;
    PnAltera: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label3: TLabel;
    MeDescricao: TdmkMemo;
    RGKndIsWhoDo: TdmkRadioGroup;
    QrWhoGerEnti: TMySQLQuery;
    QrWhoGerEntiNOMEENTIDADE: TWideStringField;
    QrWhoGerEntiCodigo: TIntegerField;
    DsWhoGerEnti: TDataSource;
    Label4: TLabel;
    EdWhoGerEnti: TdmkEditCB;
    CBWhoGerEnti: TdmkDBLookupComboBox;
    EdWhoDoQtdMin: TdmkEdit;
    LaWhoDoQtdMin: TLabel;
    LaWhoDoQtdMax: TLabel;
    EdWhoDoQtdMax: TdmkEdit;
    RGKndOnde: TdmkRadioGroup;
    LaCodOnde: TLabel;
    EdCodOnde: TdmkEditCB;
    CBCodOnde: TdmkDBLookupComboBox;
    QrOnde: TMySQLQuery;
    DsOnde: TDataSource;
    QrOndeCodigo: TIntegerField;
    QrOndeNome: TWideStringField;
    EdOndeDescr: TdmkEdit;
    Label8: TLabel;
    EdPorqueDescr: TdmkEdit;
    TPQuandoIni: TdmkEditDateTimePicker;
    Label53: TLabel;
    EdQuandoIni: TdmkEdit;
    TPQuandoFim: TdmkEditDateTimePicker;
    Label10: TLabel;
    EdQuandoFim: TdmkEdit;
    Label11: TLabel;
    EdChmHowCad: TdmkEditCB;
    CBChmHowCad: TdmkDBLookupComboBox;
    QrChmHowCad: TMySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsChmHowCad: TDataSource;
    Label12: TLabel;
    EdHowManyUnd: TdmkEdit;
    Label13: TLabel;
    EdHowManyQtd: TdmkEdit;
    Label14: TLabel;
    EdHowMuchMoed: TdmkEdit;
    Label15: TLabel;
    EdHowMuchValr: TdmkEdit;
    Label16: TLabel;
    EdImportancia: TdmkEdit;
    Label17: TLabel;
    EdUrgencia: TdmkEdit;
    SbChmHowCad: TSpeedButton;
    QrChmOcoCadKndIsWhoDo: TIntegerField;
    Panel6: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    LaDBCodOnde: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    DBMemo1: TDBMemo;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    Label33: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBRadioGroup2: TDBRadioGroup;
    EdDBOndeDescr: TDBEdit;
    EdDBCodOnde: TDBEdit;
    CBDBCodOnde: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    BtEtp: TBitBtn;
    PMEtp: TPopupMenu;
    ItsInclui2: TMenuItem;
    ItsAltera2: TMenuItem;
    ItsExclui2: TMenuItem;
    QrChmOcoEtp: TMySQLQuery;
    DsChmOcoEtp: TDataSource;
    QrChmOcoEtpCodigo: TIntegerField;
    QrChmOcoEtpControle: TIntegerField;
    QrChmOcoEtpOrdem: TIntegerField;
    QrChmOcoEtpNome: TWideStringField;
    QrChmOcoEtpDescricao: TWideStringField;
    QrChmOcoEtpQuandoIni: TDateTimeField;
    QrChmOcoEtpQuandoFim: TDateTimeField;
    QrChmOcoEtpLk: TIntegerField;
    QrChmOcoEtpDataCad: TDateField;
    QrChmOcoEtpDataAlt: TDateField;
    QrChmOcoEtpUserCad: TIntegerField;
    QrChmOcoEtpUserAlt: TIntegerField;
    QrChmOcoEtpAlterWeb: TSmallintField;
    QrChmOcoEtpAWServerID: TIntegerField;
    QrChmOcoEtpAWStatSinc: TSmallintField;
    QrChmOcoEtpAtivo: TSmallintField;
    Panel7: TPanel;
    GBWHo: TGroupBox;
    DGWho: TDBGrid;
    Splitter1: TSplitter;
    QrChmHowEtp: TMySQLQuery;
    DsChmHowEtp: TDataSource;
    QrChmHowEtpCodigo: TIntegerField;
    QrChmHowEtpControle: TIntegerField;
    QrChmHowEtpOrdem: TIntegerField;
    QrChmHowEtpNome: TWideStringField;
    QrChmHowEtpDescricao: TWideStringField;
    QrChmOcoEtpCadEtpCod: TIntegerField;
    QrChmOcoEtpCadEtpCtrl: TIntegerField;
    N1: TMenuItem;
    este1: TMenuItem;
    QrChmOcoEtpQuandoIni_TXT: TWideStringField;
    QrChmOcoEtpQuandoFim_TXT: TWideStringField;
    Panel8: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    N2: TMenuItem;
    Reenvianotificaomobile1: TMenuItem;
    QrChmOcoWhoPushNotifID: TWideStringField;
    QrChmOcoWhoSntNotif: TDateTimeField;
    QrChmOcoWhoSntNotif_TXT: TWideStringField;
    QrChmOcoWhoCfmRcbNotif: TDateTimeField;
    QrChmOcoWhoCfmRcbNotif_TXT: TWideStringField;
    QrChmOcoCadONDE_TXT: TStringField;
    QrChmOcoWhoDoneDtHr_TXT: TWideStringField;
    PnEtapas: TPanel;
    GBEtp: TGroupBox;
    DBGEtp: TDBGrid;
    GBDescrEtapa: TGroupBox;
    DBMemo2: TDBMemo;
    Splitter3: TSplitter;
    QrChmOcoDon: TMySQLQuery;
    QrChmOcoDonCodigo: TIntegerField;
    QrChmOcoDonControle: TIntegerField;
    QrChmOcoDonChmOcoEtp: TIntegerField;
    QrChmOcoDonChmOcoWho: TIntegerField;
    QrChmOcoDonObservacao: TWideStringField;
    QrChmOcoDonDoneDtHr: TDateTimeField;
    QrChmOcoDonMobUpWeb: TSmallintField;
    QrChmOcoDonCloseUser: TIntegerField;
    QrChmOcoDonCloseDtHr: TDateTimeField;
    QrChmOcoDonLk: TIntegerField;
    QrChmOcoDonDataCad: TDateField;
    QrChmOcoDonDataAlt: TDateField;
    QrChmOcoDonUserCad: TIntegerField;
    QrChmOcoDonUserAlt: TIntegerField;
    QrChmOcoDonAlterWeb: TSmallintField;
    QrChmOcoDonAWServerID: TIntegerField;
    QrChmOcoDonAWStatSinc: TSmallintField;
    QrChmOcoDonAtivo: TSmallintField;
    QrChmOcoDonWHO_Controle: TIntegerField;
    QrChmOcoDonCodigo_1: TIntegerField;
    QrChmOcoDonDON_Controle: TIntegerField;
    QrChmOcoDonETP_Controle: TIntegerField;
    QrChmOcoDonDON_DoneDtHr: TDateTimeField;
    QrChmOcoDonDON_MobWebUp: TSmallintField;
    QrChmOcoDonDON_CloseUser: TIntegerField;
    QrChmOcoDonDON_CLoseDtHr: TDateTimeField;
    QrChmOcoDonDON_Observacao: TWideStringField;
    QrChmOcoDonETP_Ordem: TIntegerField;
    QrChmOcoDonETP_Nome: TWideStringField;
    QrChmOcoDonETP_Descricao: TWideStringField;
    QrChmOcoDonETP_QuandoIni: TDateTimeField;
    QrChmOcoDonETP_QuandoFim: TDateTimeField;
    QrChmOcoDonETP_CloseUser: TIntegerField;
    QrChmOcoDonETP_CloseDtHr: TDateTimeField;
    QrChmOcoDonETP_DoneDtHr: TDateTimeField;
    GroupBox1: TGroupBox;
    Splitter2: TSplitter;
    DBGrid1: TDBGrid;
    DsChmOcoDon: TDataSource;
    QrChmOcoWhoCfmDtHrLido: TDateTimeField;
    QrChmOcoWhoDoneDtHr: TDateTimeField;
    QrChmOcoWhoMobUpWeb: TSmallintField;
    QrChmOcoWhoRealizado: TSmallintField;
    QrChmOcoWhoObsEncerr: TWideStringField;
    QrChmOcoWhoNO_REALIZADO: TWideStringField;
    Label22: TLabel;
    DBEdit6: TDBEdit;
    DBEdit18: TDBEdit;
    Label34: TLabel;
    TPLstFechDtH: TdmkEditDateTimePicker;
    Label35: TLabel;
    EdLstFechDtH: TdmkEdit;
    QrChmOcoCadLstFechDtH_TXT: TWideStringField;
    QrChmOcoEtpDoneDtHr: TDateTimeField;
    QrChmOcoEtpCloseUser: TIntegerField;
    QrChmOcoEtpCloseDtHr: TDateTimeField;
    Reabrechamado1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    QrChmOcoWhoReopenDtHr: TDateTimeField;
    QrChmOcoWhoReopenDtHr_TXT: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrChmOcoCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrChmOcoCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrChmOcoCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtWhoClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMWhoPopup(Sender: TObject);
    procedure QrChmOcoCadBeforeClose(DataSet: TDataSet);
    procedure RGKndOndeClick(Sender: TObject);
    procedure SbChmHowCadClick(Sender: TObject);
    procedure RGKndIsWhoDoClick(Sender: TObject);
    procedure EdDBCodOndeChange(Sender: TObject);
    procedure MeDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtEtpClick(Sender: TObject);
    procedure PMEtpPopup(Sender: TObject);
    procedure ItsInclui2Click(Sender: TObject);
    procedure ItsAltera2Click(Sender: TObject);
    procedure ItsExclui2Click(Sender: TObject);
    procedure este1Click(Sender: TObject);
    procedure Reenvianotificaomobile1Click(Sender: TObject);
    procedure QrChmOcoCadCalcFields(DataSet: TDataSet);
    procedure QrChmOcoWhoAfterScroll(DataSet: TDataSet);
    procedure QrChmOcoWhoBeforeClose(DataSet: TDataSet);
    procedure Reabrechamado1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraChmOcoWho(SQLType: TSQLType);
    procedure MostraChmOcoEtp(const SQLType: TSQLType; const Automatico:
              Boolean; var DtHrIni, DtHrFim: TDateTime);
    procedure CopiaItemHowEtp(Codigo: Integer);
    procedure InsereItensEtp(Codigo, ChmHowCad: Integer);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure EnviaNotificacao(Pergunta: Boolean);
    procedure ReopenChmOcoWho(Controle: Integer);
    procedure ReopenChmOcoEtp(Controle: Integer);
    procedure ReopenChmOcoDon(Controle: Integer);
    procedure ReordenaItens(Controle: Integer);

  end;

var
  FmChmOcoCad: TFmChmOcoCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, UnChm_Jan,
  ChmOcoWho, ChmOcoEtp, UnVCL_PushNotificatios, UnOVS_PF, ChmOcoPsq;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmChmOcoCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmChmOcoCad.MeDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = 13) and (Shift = []) then
  begin
    RGKndOnde.SetFocus;
  end;
end;

procedure TFmChmOcoCad.MostraChmOcoEtp(const SQLType: TSQLType; const
  Automatico: Boolean; var DtHrIni, DtHrFim: TDateTime);
const
  sProcFunc = 'TFmChmOcoCad.MostraChmOcoEtp()';
var
  ChmOcoDon: Integer;
begin
  if DBCheck.CriaFm(TFmChmOcoEtp, FmChmOcoEtp, afmoNegarComAviso) then
  begin
    FmChmOcoEtp.ImgTipo.SQLType := SQLType;
    FmChmOcoEtp.FQrCab := QrChmOcoCad;
    FmChmOcoEtp.FDsCab := DsChmOcoCad;
    FmChmOcoEtp.FQrIts := QrChmOcoEtp;
    if SQLType = stIns then
    begin
      FmChmOcoEtp.TPQuandoIni.Date            := DtHrIni;
      FmChmOcoEtp.EdQuandoIni.ValueVariant    := DtHrIni;
      FmChmOcoEtp.TPQuandoFim.Date            := DtHrFim;
      FmChmOcoEtp.EdQuandoFim.ValueVariant    := DtHrfim;
    end else
    begin
      FmChmOcoEtp.EdControle.ValueVariant := QrChmOcoEtpControle.Value;
      //
      FmChmOcoEtp.EdOrdem.ValueVariant        := QrChmOcoEtpOrdem.Value;
      FmChmOcoEtp.EdNome.ValueVariant         := QrChmOcoEtpNome.Value;
      FmChmOcoEtp.MeDescricao.Text            := QrChmOcoEtpDescricao.Value;
      if not Automatico then
      begin
        FmChmOcoEtp.TPQuandoIni.Date            := QrChmOcoEtpQuandoIni.Value;
        FmChmOcoEtp.EdQuandoIni.ValueVariant    := QrChmOcoEtpQuandoIni.Value;
        FmChmOcoEtp.TPQuandoFim.Date            := QrChmOcoEtpQuandoFim.Value;
        FmChmOcoEtp.EdQuandoFim.ValueVariant    := QrChmOcoEtpQuandoFim.Value;
      end else
      begin
        FmChmOcoEtp.TPQuandoIni.Date            := DtHrIni;
        FmChmOcoEtp.EdQuandoIni.ValueVariant    := DtHrIni;
        FmChmOcoEtp.TPQuandoFim.Date            := DtHrFim;
        FmChmOcoEtp.EdQuandoFim.ValueVariant    := DtHrfim;
      end;
    end;
    if Automatico then
    begin
      FmChmOcoEtp.CkContinuar.Checked := False;
      FmChmOcoEtp.CkContinuar.Enabled := False;
    end;
    FmChmOcoEtp.ShowModal;
    FmChmOcoEtp.Destroy;
  end;
  //
  ChmOcoDon := 0;
  if (QrChmOcoDon.State <> dsInactive) and (QrChmOcoDon.RecordCount > 0) then
    ChmOcoDon := QrChmOcoDonControle.Value;
  ReopenChmOcoDon(ChmOcoDon);
end;

procedure TFmChmOcoCad.MostraChmOcoWho(SQLType: TSQLType);
const
  sProcFunc = 'TFmChmOcoCad.MostraChmOcoWho()';
begin
  if DBCheck.CriaFm(TFmChmOcoWho, FmChmOcoWho, afmoNegarComAviso) then
  begin
    FmChmOcoWho.ImgTipo.SQLType := SQLType;
    FmChmOcoWho.FQrCab := QrChmOcoCad;
    FmChmOcoWho.FDsCab := DsChmOcoCad;
    FmChmOcoWho.FQrIts := QrChmOcoWho;
    if SQLType = stIns then
      //
    else
    begin
      FmChmOcoWho.EdControle.ValueVariant := QrChmOcoWhoControle.Value;
      //
      FmChmOcoWho.EdOrdem.ValueVariant        := QrChmOcoWhoOrdem.Value;
      FmChmOcoWho.EdWhoDoEnti.ValueVariant    := QrChmOcoWhoWhoDoEnti.Value;
      FmChmOcoWho.CBWhoDoEnti.KeyValue        := QrChmOcoWhoWhoDoEnti.Value;
      FmChmOcoWho.EdWhoDoMobile.ValueVariant  := QrChmOcoWhoWhoDoMobile.Value;
      FmChmOcoWho.CBWhoDoMobile.KeyValue      := QrChmOcoWhoWhoDoMobile.Value;
      case QrChmOcoWhoKndWhoPrtnc.Value of
        0: FmChmOcoWho.RGKndWhoPrtnc.ItemIndex := 0;
        1024: FmChmOcoWho.RGKndWhoPrtnc.ItemIndex := 1;
        2048: FmChmOcoWho.RGKndWhoPrtnc.ItemIndex := 2;
        3072: FmChmOcoWho.RGKndWhoPrtnc.ItemIndex := 3;
        else Geral.MB_Erro('KndWhoPrtnc n�o definido em ' + sProcFunc);
      end;
    end;
    FmChmOcoWho.ShowModal;
    FmChmOcoWho.Destroy;
  end;
end;

procedure TFmChmOcoCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrChmOcoCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrChmOcoCad, QrChmOcoWho);
end;

procedure TFmChmOcoCad.PMEtpPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui2, QrChmOcoCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera2, QrChmOcoEtp);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui2, QrChmOcoEtp);
  este1.Enabled :=
    (QrChmOcoCadChmHowCad.Value > 0)
    and
    (QrChmOcoEtp.RecordCount = 0);
end;

procedure TFmChmOcoCad.PMWhoPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrChmOcoCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrChmOcoWho);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrChmOcoWho);
  //
  MyObjects.HabilitaMenuItemItsUpd(Reenvianotificaomobile1, QrChmOcoWho);
  MyObjects.HabilitaMenuItemItsUpd(Reabrechamado1, QrChmOcoWho);
end;

procedure TFmChmOcoCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrChmOcoCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmChmOcoCad.DefParams;
begin
  VAR_GOTOTABELA := 'chmococad';
  VAR_GOTOMYSQLTABLE := QrChmOcoCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT chc.Nome NO_ChmHowCad, sen.Login NO_UserLog,');
  VAR_SQLx.Add('IF(oca.LstFechDtH  <= "1899-12-30", "", ');
  VAR_SQLx.Add('  DATE_FORMAT(oca.LstFechDtH, "%d/%m/%Y %H:%i:%s")) LstFechDtH_TXT, ');
  VAR_SQLx.Add('IF(wge.Tipo=0, wge.RazaoSocial, wge.Nome) NO_WhoGerEnti, ');
  VAR_SQLx.Add('IF(oca.chmtitoco<>0, tit.Nome, oca.Nome) NO_Titulo,');
  VAR_SQLx.Add('CASE KndOnde');
  VAR_SQLx.Add('  WHEN 1 THEN oca.OndeDescr');
  VAR_SQLx.Add('  /*WHEN 2 THEN scc.Nome*/');
  VAR_SQLx.Add('  WHEN 3 THEN ovl.Nome');
  VAR_SQLx.Add('  ELSE "????"');
  VAR_SQLx.Add('END NO_ONDE,');
  VAR_SQLx.Add('IF(oca.WhoGerEnti<>0, IF(wge.Tipo=0, wge.RazaoSocial, wge.Nome), sen.Login) NO_chmocowho,');
  VAR_SQLx.Add('oca.*');
  VAR_SQLx.Add('FROM chmococad oca');
  VAR_SQLx.Add('LEFT JOIN chmtitoco tit ON tit.Codigo=oca.chmtitoco');
  VAR_SQLx.Add('LEFT JOIN entidades wge ON wge.Codigo=oca.WhoGerEnti');
  VAR_SQLx.Add('/*LEFT JOIN stqcencad scc ON scc.Codigo=oca.CodOnde*/');
  VAR_SQLx.Add('LEFT JOIN ovdlocal  ovl ON ovl.Codigo=oca.CodOnde');
  VAR_SQLx.Add('LEFT JOIN chmhowcad chc ON chc.Codigo=oca.ChmHowCad');
  VAR_SQLx.Add('LEFT JOIN senhas    sen ON sen.Numero=oca.Usercad');
  VAR_SQLx.Add('WHERE oca.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND oca.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND IF(oca.chmtitoco<>0, tit.Nome, oca.Nome) Like :P0');
  //
end;

procedure TFmChmOcoCad.InsereItensEtp(Codigo, ChmHowCad: Integer);
var
  DtHrIni, DtHrFim, QuinzeMin: TDateTime;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrChmHowEtp, Dmod.MyDB, [
  'SELECT Codigo, Controle, Ordem, ',
  'Nome, Descricao',
  'FROM chmhowetp',
  'WHERE Codigo=' + Geral.FF0(ChmHowCad),
  'ORDER BY Ordem, Controle ',
  '']);
  //
  QrChmHowEtp.First;
  while not QrChmHowEtp.Eof do
  begin
    CopiaItemHowEtp(Codigo);
    //
    QrChmHowEtp.Next;
  end;
  //
  ReopenChmOcoEtp(0);
  if Geral.MB_Pergunta(
  'Deseja definir datas e hor�rios das etapas?') = ID_Yes then
  begin
    QrChmOcoEtp.First;
    QuinzeMin := (15/60/24);
    DtHrIni := QrChmOcoCadQuandoIni.Value;
    DtHrFim := DtHrIni + QuinzeMin;
    while not QrChmOcoEtp.Eof do
    begin
      MostraChmOcoEtp(stUpd, True, DtHrIni, DtHrFim);
      //
      DtHrIni := DtHrFim;
      DtHrFim := DtHrIni + QuinzeMin;
      //
      QrChmOcoEtp.Next;
    end;
  end;
end;

procedure TFmChmOcoCad.ItsAltera1Click(Sender: TObject);
begin
  MostraChmOcoWho(stUpd);
end;

procedure TFmChmOcoCad.ItsAltera2Click(Sender: TObject);
var
  DtHrIni, DtHrFim: TDateTime;
begin
  MostraChmOcoEtp(stUpd, False, DtHrIni, DtHrFim);
end;

procedure TFmChmOcoCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmChmOcoCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmChmOcoCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmChmOcoCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do EXECUTOR selecionado?',
  'ChmOcoWho', 'Controle', QrChmOcoWhoControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Dmod.ExcluiItensOrfaosChmOcoDon(LaAviso1, LaAviso2, QrChmOcoCadCodigo.Value);
    Controle := GOTOy.LocalizaPriorNextIntQr(QrChmOcoWho,
      QrChmOcoWhoControle, QrChmOcoWhoControle.Value);
    ReopenChmOcoWho(Controle);
  end;
end;

procedure TFmChmOcoCad.ItsExclui2Click(Sender: TObject);
{
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da ETAPA selecionada?',
  'ChmOcoEtp', 'Controle', QrChmOcoEtpControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrChmOcoEtp,
      QrChmOcoEtpControle, QrChmOcoEtpControle.Value);
    //ReopenChmOcoEtp(Controle);
    ReordenaItens(Controle);
  end;
}
begin
 DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrChmOcoEtp, TDBGrid(DBGEtp),
   'ChmOcoEtp', ['Controle'], ['Controle'], istPergunta, '');
  Dmod.ExcluiItensOrfaosChmOcoDon(LaAviso1, LaAviso2, QrChmOcoCadCodigo.Value);
end;

procedure TFmChmOcoCad.Reabrechamado1Click(Sender: TObject);
var
  ReopenDtHr: String;
  Controle: Integer;
  SQLType: TSQLType;
begin
  Screen.Cursor  := crHourGlass;
  try
    SQLType        := stUpd;
    Controle       := QrChmOcoWhoControle.Value;
    ReopenDtHr     := Geral.FDT(DModG.ObtemAgora, 109);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'chmocowho', False, [
    'ReopenDtHr'], ['Controle'], [
    ReopenDtHr], [Controle], True) then
    begin
      ReopenChmOcoWho(Controle);
      EnviaNotificacao(True);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmChmOcoCad.Reenvianotificaomobile1Click(Sender: TObject);
begin
  EnviaNotificacao(False);
end;

procedure TFmChmOcoCad.ReopenChmOcoDon(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrChmOcoDon, Dmod.MyDB, [
  'SELECT don.*, don.ChmOcoWho WHO_Controle, ',
  'don.Codigo, don.Controle DON_Controle, ',
  'don.ChmOcoEtp ETP_Controle, don.DoneDtHr DON_DoneDtHr, ',
  'don.MobUpWeb DON_MobWebUp, don.CloseUser DON_CloseUser, ',
  'don.CLoseDtHr DON_CLoseDtHr, don.Observacao ',
  'DON_Observacao, etp.Ordem ETP_Ordem, etp.Nome ETP_Nome, ',
  'etp.Descricao ETP_Descricao, etp.QuandoIni ',
  'ETP_QuandoIni, etp.QuandoFim ETP_QuandoFim, ',
  'etp.CloseUser ETP_CloseUser, etp.CloseDtHr',
  'ETP_CloseDtHr, etp.DoneDtHr ETP_DoneDtHr',
  'FROM chmocodon don',
  'LEFT JOIN chmocoetp etp ON etp.Controle=don.ChmOcoEtp',
  'LEFT JOIN chmocowho who ON who.Controle=don.ChmOcoWho',
  'WHERE don.Codigo=' + Geral.FF0(QrChmOcoCadCodigo.Value),
  '/*AND who.WhoDoMobile>0*/',
  'AND don.ChmOcoWho=' + Geral.FF0(QrChmOcoWhoControle.Value),
  'ORDER BY etp.Ordem, etp.Controle',
  '']);
  //
  QrChmOcoDon.Locate('Controle', Controle, []);
end;

procedure TFmChmOcoCad.ReopenChmOcoEtp(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrChmOcoEtp, Dmod.MyDB, [
  'SELECT ',
  'IF(etp.QuandoIni < 2, "", DATE_FORMAT(etp.QuandoIni, "%d/%m/%Y %H:%i:%s")) QuandoIni_TXT, ',
  'IF(etp.QuandoFim < 2, "", DATE_FORMAT(etp.QuandoFim, "%d/%m/%Y %H:%i:%s")) QuandoFIm_TXT, ',
  'etp.* ',
  'FROM chmocoetp etp ',
  'WHERE etp.Codigo=' + Geral.FF0(QrChmOcoCadCodigo.Value),
  'ORDER BY Ordem, Controle ',
  '']);
  //
  QrChmOcoEtp.Locate('Controle', Controle, []);
end;

procedure TFmChmOcoCad.ReopenChmOcoWho(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrChmOcoWho, Dmod.MyDB, [
  'SELECT ccw.*, mdc.Nome NO_WhoDoMobile, mdc.PushNotifID, ',
  'IF(wde.Tipo=0, wde.RazaoSocial, wde.Nome) NO_WhoGerEnti, ',
  'IF(ccw.SntNotif<2, "", DATE_FORMAT(ccw.SntNotif, "%d/%m/%y %H:%i:%s")) SntNotif_TXT, ',
  'IF(ccw.CfmRcbNotif<2, "", DATE_FORMAT(ccw.CfmRcbNotif, "%d/%m/%y %H:%i:%s")) CfmRcbNotif_TXT, ',
  'IF(ccw.DoneDtHr<2, "", DATE_FORMAT(ccw.DoneDtHr, "%d/%m/%y %H:%i:%s")) DoneDtHr_TXT, ',
  'IF(ccw.ReopenDtHr<2, "", DATE_FORMAT(ccw.ReopenDtHr, "%d/%m/%y %H:%i:%s")) ReopenDtHr_TXT, ',
  'IF(ccw.Realizado=0, "N�o", "Sim") NO_REALIZADO ',
  'FROM chmocowho ccw ',
  'LEFT JOIN entidades wde ON wde.Codigo=ccw.WhoDoEnti ',
  'LEFT JOIN ovcmobdevcad mdc ON mdc.Codigo=ccw.WhoDoMobile ',
  'WHERE ccw.Codigo=' + Geral.FF0(QrChmOcoCadCodigo.Value),
  '']);
  //
  QrChmOcoWho.Locate('Controle', Controle, []);
end;


procedure TFmChmOcoCad.ReordenaItens(Controle: Integer);
begin
  Screen.Cursor := crHourGlass;
  try
    QrChmOcoEtp.DisableControls;
    QrChmOcoEtp.First;
    while not QrChmOcoEtp.Eof do
    begin
      Dmod.MyDB.Execute('UPDATE chmocoetp SET Ordem=' +
      Geral.FF0(QrChmOcoEtp.RecNo) + ' WHERE Controle=' +
      Geral.FF0(QrChmOcoEtpControle.Value));
      //
      QrChmOcoEtp.Next;
    end;
    ReopenChmOcoEtp(Controle);
  finally
    QrChmOcoEtp.EnableControls;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmChmOcoCad.RGKndIsWhoDoClick(Sender: TObject);
begin
  LaWhoDoQtdMin.Enabled := False;
  EdWhoDoQtdMin.Enabled := False;
  LaWhoDoQtdMax.Enabled := False;
  EdWhoDoQtdMax.Enabled := False;
  //
  case RGKndIsWhoDo.ItemIndex of
    0: ; // Nada
    1:
    begin
      EdWhoDoQtdMin.ValueVariant := 1;
      EdWhoDoQtdMax.ValueVariant := 1;
    end;
    2:
    begin
      LaWhoDoQtdMin.Enabled := True;
      EdWhoDoQtdMin.Enabled := True;
      LaWhoDoQtdMax.Enabled := True;
      EdWhoDoQtdMax.Enabled := True;
    end;
    3,4:
    begin
      EdWhoDoQtdMin.ValueVariant := 0;
      EdWhoDoQtdMax.ValueVariant := 0;
    end;
  end;
end;

procedure TFmChmOcoCad.RGKndOndeClick(Sender: TObject);
const
  sProcFunc = 'TFmChmOcoCad.RGKndOndeClick()';
begin
  LaCodOnde.Caption   := 'Local:';
  EdCodOnde.Visible   := False;
  CBCodOnde.Visible   := False;
  EdOndeDescr.Visible := False;
  case RGKndOnde.ItemIndex of
    //Indefinido
    0: ;
    //Livre
    1:
    begin
      LaCodOnde.Caption   := 'Descreva o local:';
      //EdCodOnde.Visible   := False;
      //CBCodOnde.Visible   := False;
      EdOndeDescr.Visible := True;
    end;
    //Estoque
    2:
    begin
      LaCodOnde.Caption   := 'Origem de local n�o implementado! Selecione outra origem de local!';
      //EdCodOnde.Visible   := False;
      //CBCodOnde.Visible   := False;
      //EdOndeDescr.Visible := True;
    end;
    //Prestador
    3:
    begin
      LaCodOnde.Caption   := 'Selecione o local de prestador de servi�o:';
      EdCodOnde.Visible   := True;
      CBCodOnde.Visible   := True;
      //EdOndeDescr.Visible := False;
    end;
    else Geral.MB_Erro('RGKndOnde.ItemIndex indefinido em ' + sProcFunc);
  end;
end;

procedure TFmChmOcoCad.EdDBCodOndeChange(Sender: TObject);
const
  sProcFunc = 'TFmChmOcoCad.EdDBCodOndeChange()';
begin
  LaDBCodOnde.Caption   := 'Local:';
  EdDBCodOnde.Visible   := False;
  CBDBCodOnde.Visible   := False;
  EdDBOndeDescr.Visible := False;
  case QrChmOcoCadKndOnde.Value of
    //Indefinido
    0: ;
    //Livre
    1:
    begin
      LaDBCodOnde.Caption   := 'Descreva o local:';
      //EdDBCodOnde.Visible   := False;
      //CBDBCodOnde.Visible   := False;
      EdDBOndeDescr.Visible := True;
    end;
    //Estoque
    2:
    begin
      LaDBCodOnde.Caption   := 'Origem de local n�o implementado! Selecione outra origem de local!';
      //EdDBCodOnde.Visible   := False;
      //CBDBCodOnde.Visible   := False;
      //EdDBOndeDescr.Visible := True;
    end;
    //Prestador
    3:
    begin
      LaDBCodOnde.Caption   := 'Selecione o local de prestador de servi�o:';
      EdDBCodOnde.Visible   := True;
      CBDBCodOnde.Visible   := True;
      //EdDBOndeDescr.Visible := False;
    end;
    else Geral.MB_Erro('RGKndOnde.ItemIndex indefinido em ' + sProcFunc);
  end;
end;

procedure TFmChmOcoCad.EnviaNotificacao(Pergunta: Boolean);
var
  Mensagem, Retorno, SntNotif, TextoRetorno, TextoErro: String;
  Tokens, FldExtras, MsgExtras: array of String;
  Controle: Integer;
  Continua: Boolean;
begin
  if Length(QrChmOcoWhoPushNotifID.Text) > 1 then
  begin
    if Pergunta then
      Continua := Geral.MB_Pergunta('Deseja enviar notifica��o mobile agora para ' +
      QrChmOcoWhoNO_WhoDoMobile.Value + '?') = ID_YES
    else
      Continua := True;
    if Continua then
    begin
      Mensagem := QrChmOcoCadNome.Value;
      SetLength(Tokens, 1);
      Tokens[0] := QrChmOcoWhoPushNotifID.Value;
      //
      SetLength(FldExtras, 5);
      SetLength(MsgExtras, 5);
      //
(*
      FldExtras[0] := INSTRUCAO_OVS_NOME_NIVEL1; //'ovs_acao_nome';
      MsgExtras[0] := CO_TAB_ChmOcoCad; // 'chmococad';
      //
      FldExtras[1] := INSTRUCAO_OVS_VALR_NIVEL1; //'ovs_acao_id';
      MsgExtras[1] := Geral.FF0(QrChmOcoWhoCodigo.Value);
      //
////////////////////////////////////////////////////////////////////////////////
  INSTRUCAO_PUSH_NOTIFICATION_EXTRA_001 = 'extra001';
  INSTRUCAO_PUSH_NOTIFICATION_EXTRA_002 = 'extra002';
  INSTRUCAO_PUSH_NOTIFICATION_EXTRA_003 = 'extra003';
  CO_MAX_INSTRUCOES_PUSH_EXTRA = 3;
  CO_TAB_ChmOcoCad = 'chmococad';

type
  TPushExtras = array[0..CO_MAX_INSTRUCOES_PUSH_EXTRA-1] of String;
*)
      FldExtras[0] := INSTRUCAO_PUSH_NOTIFICATION_EXTRA_001;
      MsgExtras[0] := CO_TAB_ChmOcoCad; // 'chmococad';
      //
      FldExtras[1] := INSTRUCAO_PUSH_NOTIFICATION_EXTRA_002;
      MsgExtras[1] := Geral.FF0(QrChmOcoWhoCodigo.Value);
      //
      FldExtras[2] := INSTRUCAO_PUSH_NOTIFICATION_EXTRA_003;
      MsgExtras[2] := QrChmOcoCadONDE_TXT.Value;
      //
      FldExtras[3] := INSTRUCAO_PUSH_NOTIFICATION_EXTRA_004;
      MsgExtras[3] := Geral.FDT(QrChmOcoCadQuandoIni.Value, 109);
      //
      FldExtras[4] := INSTRUCAO_PUSH_NOTIFICATION_EXTRA_005;
      MsgExtras[4] := Geral.FDT(QrChmOcoCadQuandoFim.Value, 109);
      //
      if VCL_PushNotificatios.EnviaNotificacao(Mensagem, Tokens, FldExtras,
      MsgExtras, Retorno) then
      begin
        if OVS_PF.JSON_RetornoNotificacaoUnicaMobile(Retorno, TextoRetorno, TextoErro) then
        begin
          Controle := QrChmOcoWhoControle.Value;
          SntNotif := Geral.FDT(DModG.ObtemAgora(), 109);
          // Parei aqui!
          //if
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'chmocowho', False, [
          'SntNotif'], [
          'Controle'], [
          SntNotif], [
          Controle], True) then
            ReopenChmOcoWho(Controle);
        end else
          Geral.MB_Info('N�o foi poss�vel enviar a notifica��o!' + sLineBreak +
          'Motivo: ' + TextoErro);
      end;
    end;
  end else
    Geral.MB_Aviso(
    'Designado n�o habilitado para recebimento de notifica��oes mobile!');
end;

procedure TFmChmOcoCad.este1Click(Sender: TObject);
begin
  InsereItensEtp(QrChmOcoCadCodigo.Value, QrChmOcoCadChmHowCad.Value);
end;

procedure TFmChmOcoCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmChmOcoCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmChmOcoCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmChmOcoCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmChmOcoCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmChmOcoCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChmOcoCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrChmOcoCadCodigo.Value;
  Close;
end;

procedure TFmChmOcoCad.ItsInclui1Click(Sender: TObject);
begin
  MostraChmOcoWho(stIns);
end;

procedure TFmChmOcoCad.ItsInclui2Click(Sender: TObject);
var
  DtHrIni, DtHrFim: TDateTime;
begin
  MostraChmOcoEtp(stIns, False, DtHrIni, DtHrFim);
end;

procedure TFmChmOcoCad.CabAltera1Click(Sender: TObject);
const
  sProcName = 'TFmChmOcoCad.CabAltera1Click()';
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrChmOcoCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'chmococad');
  //
  case QrChmOcoCadKndIsWhoDo.Value of
       0: RGKndIsWhoDo.ItemIndex := 0;
    1024: RGKndIsWhoDo.ItemIndex := 1;
    2048: RGKndIsWhoDo.ItemIndex := 2;
    3072: RGKndIsWhoDo.ItemIndex := 3;
    4096: RGKndIsWhoDo.ItemIndex := 4;
    else Geral.MB_Erro('KndIsWhoDo n�o definido em ' + sProcName);
  end;
  EdOndeDescr.ValueVariant := QrChmOcoCadOndeDescr.Value;
  EdCodOnde.ValueVariant   := QrChmOcoCadCodOnde.Value;
  CBCodOnde.KeyValue       := QrChmOcoCadCodOnde.Value;
  //
  //TPLstFechDtH.Date := 0;
  //EdLstFechDtH.ValueVariant := 0;
  TPLstFechDtH.Enabled := True;
  EdLstFechDtH.Enabled := True;
end;

procedure TFmChmOcoCad.BtConfirmaClick(Sender: TObject);
const
  sProcFunc = 'TFmChmOcoCad.BtConfirmaClick()';
var
  Nome, Descricao, OndeDescr, PorqueDescr, QuandoIni, QuandoFim, HowManyUnd,
  HowMuchMoed, PriAbrtDtH, LstRAbrDtH, LstFechDtH, s: String;
  Codigo, KndNome, ChmTitOco, WhoGerEnti, KndIsWhoDo, (*WhoDoQtdSel,*) WhoDoQtdMin,
  WhoDoQtdMax, KndOnde, CodOnde, ChmHowCad, Importancia, Urgencia: Integer;
  HowManyQtd, HowMuchValr: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  KndNome        := 0;  // 0=Livre, 1= tab ChmTitOco
  ChmTitOco      := 0;  // ChmTitOco.Codigo
  Nome           := EdNome.Text;
  if KndNome = 0 then
    ChmTitOco := 0
  else
    Nome := '';
  Descricao      := MeDescricao.Text;
{
  s := Copy(Descricao, Length(Descricao));
  if s <> #10 then
    Descricao := Descricao + #13#10;
}
  WhoGerEnti     := EdWhoGerEnti.ValueVariant;
  KndIsWhoDo     := RGKndIsWhoDo.ItemIndex;
  case KndIsWhoDo of
    1: KndIsWhoDo := 1024;
    2: KndIsWhoDo := 2048;
    3: KndIsWhoDo := 3072;
    4: KndIsWhoDo := 4096;
  end;
  //WhoDoQtdSel    := ;
  WhoDoQtdMin    := EdWhoDoQtdMin.ValueVariant;
  WhoDoQtdMax    := EdWhoDoQtdMax.ValueVariant;
  KndOnde        := RGKndOnde.ItemIndex;
  CodOnde        := 0;
  OndeDescr      := EmptyStr;
  case KndOnde of
    //Indefinido
    0: ;
    //Livre
    1:
    begin
      OndeDescr   := EdOndeDescr.ValueVariant;
      if MyObjects.FIC(Trim(OndeDescr)=EmptyStr, EdOndeDescr, 'Informe o local!') then exit;
    end;
    //Estoque
    2:
    begin
      CodOnde        := EdCodOnde.ValueVariant;
      if MyObjects.FIC(CodOnde=0, EdCodOnde, 'Informe o local!') then exit;
    end;
    //Prestador
    3:
    begin
      CodOnde        := EdCodOnde.ValueVariant;
      if MyObjects.FIC(CodOnde=0, EdCodOnde, 'Informe o local!') then exit;
    end;
    else
    Geral.MB_Erro('ItemIndex indefinido em ' + sProcFunc);
  end;
  PorqueDescr    := EdPorqueDescr.ValueVariant;
  QuandoIni      := Geral.FDT_TP_Ed(TPQuandoIni.Date, EdQuandoIni.Text);
  QuandoFim      := Geral.FDT_TP_Ed(TPQuandoFim.Date, EdQuandoFim.Text);
  ChmHowCad      := EdChmHowCad.ValueVariant;
  HowManyUnd     := EdHowManyUnd.ValueVariant;
  HowManyQtd     := EdHowManyQtd.ValueVariant;
  HowMuchMoed    := EdHowMuchMoed.ValueVariant;
  HowMuchValr    := EdHowMuchValr.ValueVariant; //PriAbrtDtH     := ;
  //LstRAbrDtH     := ;
  LstFechDtH     := Geral.FDT_TP_Ed(TPLstFechDtH.Date, EdLstFechDtH.Text);
  Importancia    := EdImportancia.ValueVariant;
  Urgencia       := EdUrgencia.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina um t�tulo!') then Exit;
  if MyObjects.FIC(RGKndIsWhoDo.ItemIndex<1, RGKndIsWhoDo,
    'Informe a abrang�ncia de sele��o!') then Exit;
  if MyObjects.FIC(RGKndOnde.ItemIndex<1, RGKndOnde,
    'Informe a origem do local!') then Exit;
  //
  if MyObjects.FIC(TPQuandoIni.Date <2, TPQuandoIni, 'Informe a data inicial!') then Exit;
  if MyObjects.FIC(TPQuandoFim.Date <2, TPQuandoFim, 'Informe a data final!') then Exit;
  //
  if SQLType = stIns then
  begin
    PriAbrtDtH := Geral.FDT(DModG.ObtemAgora(), 109);
    LstRAbrDtH := PriAbrtDtH;
  end else
  begin
    PriAbrtDtH := Geral.FDT(QrChmOcoCadPriAbrtDtH.Value, 109);
    LstRAbrDtH := Geral.FDT(QrChmOcoCadLstRAbrDtH.Value, 109);
  end;
  Codigo := UMyMod.BPGS1I32('chmococad', 'Codigo', '', '', tsPos, SQLTYpe, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'chmococad', False, [
  'KndNome', 'ChmTitOco', 'Nome',
  'Descricao', 'WhoGerEnti', 'KndIsWhoDo',
  (*'WhoDoQtdSel',*) 'WhoDoQtdMin', 'WhoDoQtdMax',
  'KndOnde', 'CodOnde', 'OndeDescr',
  'PorqueDescr', 'QuandoIni', 'QuandoFim',
  'ChmHowCad', 'HowManyUnd', 'HowManyQtd',
  'HowMuchMoed', 'HowMuchValr', 'PriAbrtDtH',
  'LstRAbrDtH', 'LstFechDtH', 'Importancia',
  'Urgencia'], [
  'Codigo'], [
  KndNome, ChmTitOco, Nome,
  Descricao, WhoGerEnti, KndIsWhoDo,
  (*WhoDoQtdSel,*) WhoDoQtdMin, WhoDoQtdMax,
  KndOnde, CodOnde, OndeDescr,
  PorqueDescr, QuandoIni, QuandoFim,
  ChmHowCad, HowManyUnd, HowManyQtd,
  HowMuchMoed, HowMuchValr, PriAbrtDtH,
  LstRAbrDtH, LstFechDtH, Importancia,
  Urgencia], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
////////////////////////////////////////////////////////////////////////////////
    if (SQLType = stIns) and (ChmHowCad <> 0) then
      InsereItensEtp(Codigo, ChmHowCad);
////////////////////////////////////////////////////////////////////////////////
    if FSeq = 1 then Close;
  end;
end;

procedure TFmChmOcoCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'chmococad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'chmococad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmChmOcoCad.BtEtpClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEtp, BtEtp);
end;

procedure TFmChmOcoCad.BtWhoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMWho, BtWho);
end;

procedure TFmChmOcoCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmChmOcoCad.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  PnAltera.Align := alClient;
  PnEtapas.Align := alClient;
  CriaOForm;
  FSeq := 0;
  UnDmkDAC_PF.AbreQuery(QrWhoGerEnti, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrChmHowCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOnde, Dmod.MyDB);
  EdOndeDescr.Left := EdCodOnde.Left;
  EdOndeDescr.Top  := EdCodOnde.Top;
  EdDBOndeDescr.Left := EdDBCodOnde.Left;
  EdDBOndeDescr.Top  := EdDBCodOnde.Top;
  Agora               := DModG.ObtemAgora();
  TPQuandoIni.Date    := Agora;
  TPQuandoIni.Date    := Agora;
  TPQuandoFim.Date    := Agora;
  TPQuandoFim.Date    := Agora;
end;

procedure TFmChmOcoCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrChmOcoCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmChmOcoCad.SbChmHowCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Chm_Jan.MostraFormChmHowCad(EdChmHowCad.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdChmHowCad, CBChmHowCad, QrChmHowCad,
    VAR_CADASTRO);
end;

procedure TFmChmOcoCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmChmOcoCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrChmOcoCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmChmOcoCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmChmOcoCad.QrChmOcoCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmChmOcoCad.QrChmOcoCadAfterScroll(DataSet: TDataSet);
begin
  ReopenChmOcoWho(0);
  ReopenChmOcoEtp(0);
end;

procedure TFmChmOcoCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrChmOcoCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmChmOcoCad.SbQueryClick(Sender: TObject);
var
  Codigo: Integer;
begin
(*
  LocCod(QrChmOcoCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'chmococad', Dmod.MyDB, CO_VAZIO));
*)
  Codigo := 0;
  if DBCheck.CriaFm(TFmChmOcoPsq, FmChmOcoPsq, afmoNegarComAviso) then
  begin
    FmChmOcoPsq.ShowModal;
    Codigo := FmChmOcoPsq.FCodigo;
    FmChmOcoPsq.Destroy;
    //
    if Codigo <> 0 then
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmChmOcoCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmChmOcoCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrChmOcoCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'chmococad');
  //if ImgTipo.SQLType = stIns then
  begin
    TPLstFechDtH.Date := 0;
    EdLstFechDtH.ValueVariant := 0;
    TPLstFechDtH.Enabled := False;
    EdLstFechDtH.Enabled := False;
  end;
end;

procedure TFmChmOcoCad.CopiaItemHowEtp(Codigo: Integer);
var
  Nome, Descricao, QuandoIni, QuandoFim: String;
  //Codigo,
  Controle, Ordem, CadEtpCod, CadEtpCtrl: Integer;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  //Codigo         := Acima!;
  Controle       := 0;
  Ordem          := QrChmHowEtpOrdem.Value;
  Nome           := QrChmHowEtpNome.Value;
  Descricao      := QrChmHowEtpDescricao.Value;
  QuandoIni      := '0000-00-00 00:00:00';
  QuandoFim      := '0000-00-00 00:00:00';
  CadEtpCod      := QrChmHowEtpCodigo.Value;
  CadEtpCtrl     := QrChmHowEtpControle.Value;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina um t�tulo!') then Exit;
  //
  Controle := UMyMod.BPGS1I32('chmocoetp', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'chmocoetp', False, [
  'Codigo', 'Ordem', 'Nome',
  'Descricao', 'QuandoIni', 'QuandoFim'], [
  'Controle'], [
  Codigo, Ordem, Nome,
  Descricao, QuandoIni, QuandoFim], [
  Controle], True) then
  begin
    //
  end;
end;

procedure TFmChmOcoCad.QrChmOcoCadBeforeClose(
  DataSet: TDataSet);
begin
  QrChmOcoWho.Close;
  QrChmOcoEtp.Close;
end;

procedure TFmChmOcoCad.QrChmOcoCadBeforeOpen(DataSet: TDataSet);
begin
  QrChmOcoCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmChmOcoCad.QrChmOcoCadCalcFields(DataSet: TDataSet);
begin
  case QrChmOcoCadKndOnde.Value of
    0: QrChmOcoCadONDE_TXT.Value := '[N�o definido]';
    1: QrChmOcoCadONDE_TXT.Value := QrChmOcoCadOndeDescr.Value;
    2: QrChmOcoCadONDE_TXT.Value := '[???Estoque???]';
    3: QrChmOcoCadONDE_TXT.Value := QrChmOcoCadNO_ONDE.Value;
    else QrChmOcoCadONDE_TXT.Value := '[N�o implementado]';
  end;
end;

procedure TFmChmOcoCad.QrChmOcoWhoAfterScroll(DataSet: TDataSet);
begin
  ReopenChmOcoDon(0);
end;

procedure TFmChmOcoCad.QrChmOcoWhoBeforeClose(DataSet: TDataSet);
begin
  QrChmOcoDon.Close;
end;

end.

