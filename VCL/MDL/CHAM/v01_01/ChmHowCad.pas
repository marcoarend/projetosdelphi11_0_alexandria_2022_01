unit ChmHowCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmChmHowCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrChmHowCad: TMySQLQuery;
    DsChmHowCad: TDataSource;
    QrChmHowEtp: TMySQLQuery;
    DsChmHowEtp: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrChmHowCadCodigo: TIntegerField;
    QrChmHowCadNome: TWideStringField;
    QrChmHowCadTemItns: TSmallintField;
    QrChmHowCadAlterWeb: TSmallintField;
    QrChmHowCadAWServerID: TIntegerField;
    QrChmHowCadAWStatSinc: TSmallintField;
    QrChmHowCadAtivo: TSmallintField;
    QrChmHowEtpCodigo: TIntegerField;
    QrChmHowEtpControle: TIntegerField;
    QrChmHowEtpOrdem: TIntegerField;
    QrChmHowEtpNome: TWideStringField;
    QrChmHowEtpDescricao: TWideStringField;
    QrChmHowEtpAlterWeb: TSmallintField;
    QrChmHowEtpAWServerID: TIntegerField;
    QrChmHowEtpAWStatSinc: TSmallintField;
    QrChmHowEtpAtivo: TSmallintField;
    Panel6: TPanel;
    DBMeDescricao: TDBMemo;
    Panel7: TPanel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrChmHowCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrChmHowCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrChmHowCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrChmHowCadBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraChmHowEtp(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenChmHowEtp(Controle: Integer);
    procedure ReordenaItens(Controle: Integer);

  end;

var
  FmChmHowCad: TFmChmHowCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ChmHowEtp;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmChmHowCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmChmHowCad.MostraChmHowEtp(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmChmHowEtp, FmChmHowEtp, afmoNegarComAviso) then
  begin
    FmChmHowEtp.ImgTipo.SQLType := SQLType;
    FmChmHowEtp.FQrCab := QrChmHowCad;
    FmChmHowEtp.FDsCab := DsChmHowCad;
    FmChmHowEtp.FQrIts := QrChmHowEtp;
    if SQLType = stIns then
      //
    else
    begin
      FmChmHowEtp.EdControle.ValueVariant := QrChmHowEtpControle.Value;
      //
      FmChmHowEtp.EdNome.Text          := QrChmHowEtpNome.Value;
      FmChmHowEtp.MeDescricao.Text     := QrChmHowEtpDescricao.Value;
      FmChmHowEtp.EdOrdem.ValueVariant := QrChmHowEtpOrdem.Value;
    end;
    FmChmHowEtp.ShowModal;
    FmChmHowEtp.Destroy;
  end;
end;

procedure TFmChmHowCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrChmHowCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrChmHowCad, QrChmHowEtp);
end;

procedure TFmChmHowCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrChmHowCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrChmHowEtp);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrChmHowEtp);
end;

procedure TFmChmHowCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrChmHowCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmChmHowCad.DefParams;
begin
  VAR_GOTOTABELA := 'ChmHowCad';
  VAR_GOTOMYSQLTABLE := QrChmHowCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM chmhowcad');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmChmHowCad.ItsAltera1Click(Sender: TObject);
begin
  MostraChmHowEtp(stUpd);
end;

procedure TFmChmHowCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmChmHowCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmChmHowCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmChmHowCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'chmhowetp', 'Controle', QrChmHowEtpControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrChmHowEtp,
      QrChmHowEtpControle, QrChmHowEtpControle.Value);
    //ReopenChmHowEtp(Controle);
    ReordenaItens(Controle);
  end;
end;

procedure TFmChmHowCad.ReopenChmHowEtp(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrChmHowEtp, Dmod.MyDB, [
  'SELECT * ',
  'FROM chmhowetp ',
  'WHERE Codigo=' + Geral.FF0(QrChmHowCadCodigo.Value),
  'ORDER BY Ordem, Controle DESC ',
  '']);
  //
  QrChmHowEtp.Locate('Controle', Controle, []);
end;


procedure TFmChmHowCad.ReordenaItens(Controle: Integer);
begin
  Screen.Cursor := crHourGlass;
  try
    QrChmHowEtp.DisableControls;
    QrChmHowEtp.First;
    while not QrChmHowEtp.Eof do
    begin
      Dmod.MyDB.Execute('UPDATE chmhowetp SET Ordem=' +
      Geral.FF0(QrChmHowEtp.RecNo) + ' WHERE Controle=' +
      Geral.FF0(QrChmHowEtpControle.Value));
      //
      QrChmHowEtp.Next;
    end;
    ReopenChmHowEtp(Controle);
  finally
    QrChmHowEtp.EnableControls;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmChmHowCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmChmHowCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmChmHowCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmChmHowCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmChmHowCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmChmHowCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChmHowCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrChmHowCadCodigo.Value;
  Close;
end;

procedure TFmChmHowCad.ItsInclui1Click(Sender: TObject);
begin
  MostraChmHowEtp(stIns);
end;

procedure TFmChmHowCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrChmHowCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ChmHowCad');
end;

procedure TFmChmHowCad.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo(*, TemItns*): Integer;
  SQLType: TSQLType;
begin
  Nome           := EdNome.ValueVariant;
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  //TemItns        := ;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('chmhowcad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'chmhowcad', False, [
  'Nome'(*, 'TemItns'*)], [
  'Codigo'], [
  Nome(*, TemItns*)], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmChmHowCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'chmhowcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'chmhowcad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmChmHowCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmChmHowCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmChmHowCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmChmHowCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrChmHowCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmChmHowCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmChmHowCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrChmHowCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmChmHowCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmChmHowCad.QrChmHowCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmChmHowCad.QrChmHowCadAfterScroll(DataSet: TDataSet);
begin
  ReopenChmHowEtp(0);
end;

procedure TFmChmHowCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrChmHowCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmChmHowCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrChmHowCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'chmhowcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmChmHowCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmChmHowCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrChmHowCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'chmhowcad');
end;

procedure TFmChmHowCad.QrChmHowCadBeforeClose(
  DataSet: TDataSet);
begin
  QrChmHowEtp.Close;
end;

procedure TFmChmHowCad.QrChmHowCadBeforeOpen(DataSet: TDataSet);
begin
  QrChmHowCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

