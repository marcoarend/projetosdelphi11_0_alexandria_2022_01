unit ChConfigCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, Menus, dmkGeral, dmkEdit, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmChConfigCab = class(TForm)
    PainelDados: TPanel;
    DsChConfCab: TDataSource;
    QrChConfCab: TmySQLQuery;
    PainelEdita: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    QrChConfCabCodigo: TIntegerField;
    QrChConfCabNome: TWideStringField;
    Panel1: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    Label2: TLabel;
    DBEdNome: TDBEdit;
    DBGrid1: TDBGrid;
    QrChConfVal: TmySQLQuery;
    QrChConfValCampo: TIntegerField;
    QrChConfValNome: TWideStringField;
    QrChConfValCodigo: TIntegerField;
    QrChConfValCampo_1: TIntegerField;
    QrChConfValTopo: TIntegerField;
    QrChConfValMEsq: TIntegerField;
    QrChConfValLarg: TIntegerField;
    QrChConfValFTam: TIntegerField;
    QrChConfValNegr: TIntegerField;
    QrChConfValItal: TIntegerField;
    QrChConfValLk: TIntegerField;
    QrChConfValDataCad: TDateField;
    QrChConfValDataAlt: TDateField;
    QrChConfValUserCad: TIntegerField;
    QrChConfValUserAlt: TIntegerField;
    DsChConfVal: TDataSource;
    PMAltera: TPopupMenu;
    NomedaConfiguraoatual1: TMenuItem;
    Itematual1: TMenuItem;
    QrChConfValAltu: TIntegerField;
    QrChConfValNOMEALIN: TWideStringField;
    Somenteatual1: TMenuItem;
    Umaum1: TMenuItem;
    QrChConfVal2: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    IntegerField10: TIntegerField;
    IntegerField11: TIntegerField;
    IntegerField12: TIntegerField;
    DateField1: TDateField;
    DateField2: TDateField;
    IntegerField13: TIntegerField;
    IntegerField14: TIntegerField;
    StringField2: TWideStringField;
    EdAltura: TdmkEdit;
    Label6: TLabel;
    EdTopoIni: TdmkEdit;
    Label3: TLabel;
    QrChConfCabAltura: TIntegerField;
    QrChConfCabTopoIni: TIntegerField;
    Label4: TLabel;
    DBEdAltura: TDBEdit;
    Label5: TLabel;
    DBEdTopoIni: TDBEdit;
    QrChConfValNOMEFTAM: TWideStringField;
    QrChConfCabCPI: TIntegerField;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    RGCPI: TRadioGroup;
    QrChConfValPref: TWideStringField;
    QrChConfValSufi: TWideStringField;
    QrChConfValPadr: TWideStringField;
    QrChConfVal2Pref: TWideStringField;
    QrChConfVal2Sufi: TWideStringField;
    QrChConfVal2Padr: TWideStringField;
    QrChConfValAliNOME: TWideStringField;
    QrChConfValAliV: TIntegerField;
    QrChConfValAliH: TIntegerField;
    QrChConfVal2AliV: TIntegerField;
    QrChConfVal2AliH: TIntegerField;
    QrChConfCabLk: TIntegerField;
    QrChConfCabDataCad: TDateField;
    QrChConfCabDataAlt: TDateField;
    QrChConfCabUserCad: TIntegerField;
    QrChConfCabUserAlt: TIntegerField;
    QrChConfCabMEsq: TSmallintField;
    Label8: TLabel;
    DBEdMEsq: TDBEdit;
    Label11: TLabel;
    EdMEsq: TdmkEdit;
    QrMEsq: TmySQLQuery;
    QrMEsqMin: TIntegerField;
    QrChConfValTopR: TIntegerField;
    QrChConfValPrEs: TIntegerField;
    PMInclui: TPopupMenu;
    Incluinovaconfiguraomanual1: TMenuItem;
    Duplicaconfiguraoatual1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel4: TPanel;
    BtConfirma: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrChConfCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrChConfCabAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrChConfCabBeforeOpen(DataSet: TDataSet);
    procedure NomedaConfiguraoatual1Click(Sender: TObject);
    procedure QrChConfValCalcFields(DataSet: TDataSet);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Umaum1Click(Sender: TObject);
    procedure Somenteatual1Click(Sender: TObject);
    procedure Incluinovaconfiguraomanual1Click(Sender: TObject);
    procedure Duplicaconfiguraoatual1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    FItemLoc: Integer;
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenItens(Item: Integer);
    procedure AlteraCampoSelecionado(Query: TmySQLQuery; UmAUm: Boolean);
    //
  public
    { Public declarations }
    FDesiste: Boolean;
  end;

var
  FmChConfigCab: TFmChConfigCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ChConfigIts, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmChConfigCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmChConfigCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrChConfCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////
procedure TFmChConfigCab.DefParams;
begin
  VAR_GOTOTABELA := 'ChConfCab';
  VAR_GOTOMYSQLTABLE := QrChConfCab;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM chconfcab');
  //
  VAR_SQL1.Add('WHERE Codigo=:P0');
  //
  VAR_SQLa.Add('WHERE Nome Like :P0');
  //
end;

procedure TFmChConfigCab.Duplicaconfiguraoatual1Click(Sender: TObject);
var
  Nome: String;
  Altura, TopoIni, CPI, MEsq, Codigo: Integer;
  Campo, Topo, TopR, Altu, Larg, FTam, AliV, AliH, Negr, Ital, PrEs: Integer;
  Pref, Sufi, Padr: String;
begin
  if Geral.MB_Pergunta('Confirma a duplica��o da configura��o atual?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Nome    := '[DUPLICA��O] ' + QrChConfCabNome.Value;
      Altura  := QrChConfCabAltura.Value;
      TopoIni := QrChConfCabTopoIni.Value;
      CPI     := QrChConfCabCPI.Value;
      MEsq    := QrChConfCabMEsq.Value;
      Codigo  := UMyMod.BuscaEmLivreY_Def('chconfcab', 'Codigo', stIns, 0);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'chconfcab', False, [
      'Nome', 'Altura', 'TopoIni', 'CPI', 'MEsq'], [
      'Codigo'], [
      Nome, Altura, TopoIni,
      CPI, MEsq], [
      Codigo], True) then
      begin
        QrChConfVal.First;
        while not QrChConfVal.Eof do
        begin
          Campo := QrChConfValCampo.Value;
          Topo  := QrChConfValTopo .Value;
          TopR  := QrChConfValTopR .Value;
          MEsq  := QrChConfValMEsq .Value;
          Altu  := QrChConfValAltu .Value;
          Larg  := QrChConfValLarg .Value;
          FTam  := QrChConfValFTam .Value;
          AliV  := QrChConfValAliV .Value;
          AliH  := QrChConfValAliH .Value;
          Negr  := QrChConfValNegr .Value;
          Ital  := QrChConfValItal .Value;
          PrEs  := QrChConfValPrEs .Value;
          Pref  := QrChConfValPref .Value;
          Sufi  := QrChConfValSufi .Value;
          Padr  := QrChConfValPadr .Value;
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'chconfval', False, [
          'Codigo', 'Campo', 'Topo',
          'TopR', 'MEsq', 'Altu',
          'Larg', 'FTam', 'AliV',
          'AliH', 'Negr', 'Ital',
          'PrEs', 'Pref', 'Sufi',
          'Padr'], [
          ], [
          Codigo, Campo, Topo,
          TopR, MEsq, Altu,
          Larg, FTam, AliV,
          AliH, Negr, Ital,
          PrEs, Pref, Sufi,
          Padr], [
          ], True);
          //
          QrChConfVal.Next;
        end;
      end;
      //
      LocCod(Codigo, Codigo);
      Geral.MB_Info('Duplica��o realizada com sucesso!');
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmChConfigCab.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    EdNome.Text := CO_VAZIO;
    EdNome.Visible := True;
    if SQLType = stIns then
    begin
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
    end else begin
      EdCodigo.Text   := DBEdCodigo.Text;
      EdNome.Text     := DBEdNome.Text;
      EdAltura.Text   := DBEdAltura.Text;
      EdTopoIni.Text  := DBEdTopoIni.Text;
      EdMEsq.Text     := DBEdMEsq.Text;
      RGCPI.ItemIndex := Trunc((QrChConfCabCPI.Value-10)/2);
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmChConfigCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmChConfigCab.AlteraRegistro;
var
  Cheques : Integer;
begin
  Cheques := QrChConfCabCodigo.Value;
  if QrChConfCabCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Cheques, Dmod.MyDB, 'ChConfCab', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Cheques, Dmod.MyDB, 'ChConfCab', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmChConfigCab.Incluinovaconfiguraomanual1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmChConfigCab.IncluiRegistro;
var
  Cursor : TCursor;
  Cheques : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Cheques := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'ChConfCab', 'ChConfCab', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Cheques))>Length(FFormatFloat) then
    begin
      Geral.MB_Aviso(
      'Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, stIns, Cheques);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmChConfigCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmChConfigCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmChConfigCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmChConfigCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmChConfigCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmChConfigCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmChConfigCab.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmChConfigCab.BtAlteraClick(Sender: TObject);
begin
  //AlteraRegistro;
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmChConfigCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrChConfCabCodigo.Value;
  Close;
end;

procedure TFmChConfigCab.BtConfirmaClick(Sender: TObject);
var
  MEsq, Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  MEsq := Geral.IMV(EdMEsq.Text);
  QrMEsq.Close;
  QrMEsq.Params[0].AsInteger := Codigo;
  UnDmkDAC_PF.AbreQuery(QrMEsq, Dmod.MyDB);
  if (QrMEsqMin.Value + MEsq) < 0 then
  begin
    Geral.MB_Aviso('Margem esquerda inv�lida. J� h� item com '+
    'margem esquerda insuficiente para recuo: '+IntToStr(QrMEsqMin.Value)+'.');
    Exit;
  end;
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO chconfcab SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE chconfcab SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Altura=:P1, TopoIni=:P2, CPI=:P3, MEsq=:P4, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Geral.IMV(EdAltura.Text);
  Dmod.QrUpdU.Params[02].AsInteger := Geral.IMV(EdTopoIni.Text);
  Dmod.QrUpdU.Params[03].AsInteger := 10 + (RGCPI.ItemIndex * 2);
  Dmod.QrUpdU.Params[04].AsInteger := MEsq;
  //
  Dmod.QrUpdU.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[06].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[07].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ChConfCab', 'Codigo');
  MostraEdicao(False, stLok, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmChConfigCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ChConfCab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ChConfCab', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ChConfCab', 'Codigo');
end;

procedure TFmChConfigCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DBGrid1.Align     := alClient;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  CriaOForm;
end;

procedure TFmChConfigCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrChConfCabCodigo.Value,LaRegistro.Caption);
end;

procedure TFmChConfigCab.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmChConfigCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmChConfigCab.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmChConfigCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmChConfigCab.QrChConfCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmChConfigCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'ChConfCab', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmChConfigCab.QrChConfCabAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrChConfCabCodigo.Value, False);
  ReopenItens(-1000);
end;

procedure TFmChConfigCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrChConfCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ChConfCab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmChConfigCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmChConfigCab.QrChConfCabBeforeOpen(DataSet: TDataSet);
begin
  QrChConfCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmChConfigCab.ReopenItens(Item: Integer);
var
  Loc: Integer;
begin
  QrChConfVal.Close;
  QrChConfVal.Params[0].AsInteger := QrChConfCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrChConfVal, Dmod.MyDB);
  //
  if Item > 0 then Loc := Item else Loc := FItemLoc;
  QrChConfVal.Locate('Campo', Loc, []);
end;

procedure TFmChConfigCab.NomedaConfiguraoatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmChConfigCab.QrChConfValCalcFields(DataSet: TDataSet);
var
  Alin: Integer;
begin
  Alin := (QrChConfValAliV.Value * 10) + QrChConfValAliH.Value;
  case Alin of
    00: QrChConfValNOMEALIN.Value := 'Topo na esquerda';
    01: QrChConfValNOMEALIN.Value := 'Topo ao centro';
    02: QrChConfValNOMEALIN.Value := 'Topo na direita';
    10: QrChConfValNOMEALIN.Value := 'Centro a esquerda';
    11: QrChConfValNOMEALIN.Value := 'Centralizado';
    12: QrChConfValNOMEALIN.Value := 'Centro a direita';
    20: QrChConfValNOMEALIN.Value := 'Base a esquerda';
    21: QrChConfValNOMEALIN.Value := 'Base ao centro';
    22: QrChConfValNOMEALIN.Value := 'Base a direita';
  end;
  case QrChConfValFTam.Value of
    00: QrChConfValNOMEFTAM.Value := 'Comprimido';
    01: QrChConfValNOMEFTAM.Value := 'Normal';
    02: QrChConfValNOMEFTAM.Value := 'Expandido';
  end;
end;

procedure TFmChConfigCab.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Negr' then
    //MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, QrChConfValNegr.Value);
  if Column.FieldName = 'Ital' then
    //MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, QrChConfValItal.Value);
end;

procedure TFmChConfigCab.AlteraCampoSelecionado(Query: TmySQLQuery; UmAUm: Boolean);
begin
  FItemLoc := Query.FieldByName('Campo').AsInteger;
  MyObjects.FormCria(TFmChConfigIts, FmChConfigIts);
  FmChConfigIts.EdCodigo.Text := IntToStr(QrChConfCabCodigo.Value);
  FmChConfigIts.EdCampo.Text := IntToStr(Query.FieldByName('Campo').AsInteger);
  FmChConfigIts.EdCampoNome.Text := Query.FieldByName('Nome').AsString;
  if FmChConfigIts.ProcuraItem(Query.FieldByName('Codigo').AsInteger,
     Query.FieldByName('Campo').AsInteger) then
  begin
    FmChConfigIts.EdTopo.Text := IntToStr(Query.FieldByName('Topo').AsInteger);
    FmChConfigIts.EdMEsq.Text := IntToStr(Query.FieldByName('MEsq').AsInteger);
    FmChConfigIts.EdAltu.Text := IntToStr(Query.FieldByName('Altu').AsInteger);
    FmChConfigIts.EdLarg.Text := IntToStr(Query.FieldByName('Larg').AsInteger);
    FmChConfigIts.EdPadr.Text := Query.FieldByName('Padr').AsString;
    FmChConfigIts.EdPref.Text := Query.FieldByName('Pref').AsString;
    FmChConfigIts.EdSufi.Text := Query.FieldByName('Sufi').AsString;
    FmChConfigIts.RGFTam.ItemIndex := Query.FieldByName('FTam').AsInteger;
    FmChConfigIts.RGAliV.ItemIndex := Query.FieldByName('AliV').AsInteger;
    FmChConfigIts.RGAliH.ItemIndex := Query.FieldByName('AliH').AsInteger;
    FmChConfigIts.RGNegr.ItemIndex := Query.FieldByName('Negr').AsInteger;
    FmChConfigIts.RGItal.ItemIndex := Query.FieldByName('Ital').AsInteger;
  end;
  FmChConfigIts.FUmAUm := UmAUm;
  FmChConfigIts.ShowModal;
  FmChConfigIts.Destroy;
  ReopenItens(FItemLoc);
end;

procedure TFmChConfigCab.Umaum1Click(Sender: TObject);
begin
  QrChConfVal2.Close;
  QrChConfVal2.Params[0].AsInteger := QrChConfCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrChConfVal2, Dmod.MyDB);
  //
  if QrChConfVal2.Locate('Campo', QrChConfValCampo.Value, []) then
  begin
    while not QrChConfVal2.Eof do
    begin
      AlteraCampoSelecionado(QrChConfVal2, True);
      if FDesiste then Exit;
      QrChConfVal2.Next;
    end;
  end;
end;

procedure TFmChConfigCab.Somenteatual1Click(Sender: TObject);
begin
  AlteraCampoSelecionado(QrChConfVal, False);
end;

end.

