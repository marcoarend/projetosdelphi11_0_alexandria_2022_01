object FmCheque: TFmCheque
  Left = 211
  Top = 201
  Caption = 'Impress'#227'o de Cheque'
  ClientHeight = 486
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Impress'#227'o de Cheque'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 438
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object LaFatID: TLabel
      Left = 116
      Top = 8
      Width = 6
      Height = 13
      Caption = '0'
      Visible = False
    end
    object LaFatNum: TLabel
      Left = 128
      Top = 8
      Width = 6
      Height = 13
      Caption = '0'
      Visible = False
    end
    object LaCliente: TLabel
      Left = 140
      Top = 8
      Width = 6
      Height = 13
      Caption = '0'
      Visible = False
    end
    object EdManeira: TLabel
      Left = 152
      Top = 8
      Width = 6
      Height = 13
      Caption = '0'
      Visible = False
    end
    object BtSaida: TBitBtn
      Left = 692
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Sa'#237'da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
    object BtImprime: TBitBtn
      Left = 12
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Imprime'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtImprimeClick
      NumGlyphs = 2
    end
    object BtTeste: TBitBtn
      Left = 204
      Top = 4
      Width = 90
      Height = 40
      Caption = 'Teste'
      TabOrder = 2
      OnClick = BtTesteClick
    end
    object BitBtn1: TBitBtn
      Left = 300
      Top = 4
      Width = 90
      Height = 40
      Caption = 'Teste direto'
      TabOrder = 3
      OnClick = BitBtn1Click
    end
    object CkGrade: TCheckBox
      Left = 404
      Top = 12
      Width = 97
      Height = 17
      Caption = 'Grade.'
      TabOrder = 4
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 390
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 313
      Align = alClient
      TabOrder = 0
      object Panel2: TPanel
        Left = 1
        Top = 1
        Width = 790
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 34
          Height = 13
          Caption = 'Banco:'
        end
        object EdBanco: TdmkEditCB
          Left = 8
          Top = 20
          Width = 77
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBBanco
        end
        object CBBanco: TdmkDBLookupComboBox
          Left = 88
          Top = 20
          Width = 361
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsBancos
          TabOrder = 1
          dmkEditCB = EdBanco
          UpdType = utYes
        end
        object BtExclui: TBitBtn
          Left = 452
          Top = 4
          Width = 141
          Height = 40
          Caption = '&Limpa cheques'
          TabOrder = 2
          OnClick = BtExcluiClick
        end
      end
      object Panel5: TPanel
        Left = 1
        Top = 133
        Width = 790
        Height = 179
        Align = alClient
        TabOrder = 1
        object DBGrid1: TDBGrid
          Left = 1
          Top = 1
          Width = 788
          Height = 177
          Align = alClient
          DataSource = DsCheques
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Cheque'
              Title.Alignment = taCenter
              Width = 50
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Data'
              Title.Alignment = taCenter
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorNU'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor'
              Width = 83
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 283
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Vencto'
              Title.Alignment = taCenter
              Title.Caption = 'Vencimento'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cliente'
              Width = 194
              Visible = True
            end>
        end
      end
      object Panel6: TPanel
        Left = 1
        Top = 49
        Width = 790
        Height = 84
        Align = alTop
        TabOrder = 2
        object ListBox1: TListBox
          Left = 1
          Top = 1
          Width = 190
          Height = 82
          Align = alLeft
          ItemHeight = 13
          TabOrder = 0
        end
        object ListBox2: TListBox
          Left = 599
          Top = 1
          Width = 190
          Height = 82
          Align = alRight
          ItemHeight = 13
          TabOrder = 1
        end
        object RGTeste: TRadioGroup
          Left = 204
          Top = 8
          Width = 285
          Height = 41
          Caption = ' Teste: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            '8'
            '10'
            '12'
            '14')
          TabOrder = 2
        end
        object Button1: TButton
          Left = 496
          Top = 16
          Width = 75
          Height = 25
          Caption = 'Teste'
          TabOrder = 3
          OnClick = Button1Click
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 313
      Width = 792
      Height = 77
      Align = alBottom
      TabOrder = 1
      object GroupBox4: TGroupBox
        Left = 1
        Top = 1
        Width = 790
        Height = 75
        Align = alClient
        Caption = ' Configura'#231#245'es: '
        TabOrder = 0
        object Label7: TLabel
          Left = 128
          Top = 20
          Width = 81
          Height = 13
          Caption = 'Margem superior:'
        end
        object Label9: TLabel
          Left = 592
          Top = 20
          Width = 45
          Height = 13
          Caption = 'Chorinho:'
        end
        object Label2: TLabel
          Left = 700
          Top = 20
          Width = 67
          Height = 13
          Caption = 'Formatadores:'
        end
        object Label3: TLabel
          Left = 360
          Top = 20
          Width = 36
          Height = 13
          Caption = 'Cidade:'
        end
        object Label4: TLabel
          Left = 244
          Top = 20
          Width = 88
          Height = 13
          Caption = 'Margem esquerda:'
        end
        object EdTopIni: TdmkEdit
          Left = 128
          Top = 36
          Width = 109
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '0'
          OnExit = EdTopIniExit
        end
        object BtSalvar: TBitBtn
          Left = 8
          Top = 20
          Width = 109
          Height = 40
          Cursor = crHandPoint
          Caption = 'S&alvar'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtSalvarClick
          NumGlyphs = 2
        end
        object EdBomPara: TdmkEdit
          Left = 592
          Top = 36
          Width = 105
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = 'Bom para'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 'Bom para'
        end
        object EdFormatI: TdmkEdit
          Left = 700
          Top = 36
          Width = 30
          Height = 21
          Alignment = taRightJustify
          MaxLength = 2
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '#'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '#'
          OnExit = EdTopIniExit
        end
        object EdFormatF: TdmkEdit
          Left = 736
          Top = 36
          Width = 30
          Height = 21
          MaxLength = 2
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '#'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '#'
        end
        object EdCidade: TdmkEdit
          Left = 360
          Top = 36
          Width = 229
          Height = 21
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdLeftIni: TdmkEdit
          Left = 244
          Top = 36
          Width = 109
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '0'
          OnExit = EdTopIniExit
        end
      end
    end
  end
  object QrCliente: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrClienteCalcFields
    SQL.Strings = (
      'SELECT te.Codigo, te.Tipo, te.Logo, te.Respons1, te.Respons2,'
      
        'CASE WHEN te.Tipo=0 THEN te.RazaoSocial ELSE te.Nome END NOMEENT' +
        'IDADE,'
      'CASE WHEN te.Tipo=0 THEN te.ERua ELSE te.PRua END RUA,'
      'CASE WHEN te.Tipo=0 THEN te.ENumero ELSE te.PNumero END NUMERO,'
      'CASE WHEN te.Tipo=0 THEN te.ECompl ELSE te.PCompl END COMPL,'
      'CASE WHEN te.Tipo=0 THEN te.EBairro ELSE te.PBairro END BAIRRO,'
      'CASE WHEN te.Tipo=0 THEN te.ETe1 ELSE te.PTe1 END TE1,'
      'CASE WHEN te.Tipo=0 THEN te.EFax ELSE te.PFax END FAX,'
      'CASE WHEN te.Tipo=0 THEN te.CNPJ ELSE te.CPF END DOC1,'
      'CASE WHEN te.Tipo=0 THEN te.IE ELSE te.RG END DOC2,'
      'CASE WHEN te.Tipo=0 THEN te.ECidade ELSE te.PCidade END CIDADE,'
      'CASE WHEN te.Tipo=0 THEN te.ECEP ELSE te.PCEP END CEP,'
      'CASE WHEN te.Tipo=0 THEN te.EUF ELSE te.PUF END UF,'
      'uf.Nome NOMEUF'
      'FROM entidades te, UFs uf'
      
        'WHERE uf.Codigo=(CASE WHEN te.Tipo=0 THEN te.EUF ELSE te.PUF END' +
        ')'
      'AND te.Codigo=:P0'
      'ORDER BY te.Codigo')
    Left = 84
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrClienteCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrClienteTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrClienteCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrClienteNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrClienteTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrClienteLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrClienteRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrClienteRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrClienteRUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 30
    end
    object QrClienteNUMERO: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'NUMERO'
    end
    object QrClienteCOMPL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'COMPL'
      Size = 30
    end
    object QrClienteBAIRRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrClienteTE1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TE1'
    end
    object QrClienteFAX: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FAX'
    end
    object QrClienteDOC1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'DOC1'
      Size = 18
    end
    object QrClienteDOC2: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'DOC2'
    end
    object QrClienteCIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrClienteCEP: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'CEP'
    end
    object QrClienteUF: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'UF'
    end
    object QrClienteNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrClienteNUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUM_TXT'
      Size = 30
      Calculated = True
    end
    object QrClienteDOC_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_TXT'
      Size = 30
      Calculated = True
    end
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DataSource1: TDataSource
    DataSet = QrCliente
    Left = 112
    Top = 224
  end
  object DsCheques: TDataSource
    DataSet = TbCheques
    Left = 56
    Top = 252
  end
  object TbCheques: TmySQLTable
    Database = Dmod.MyLocDatabase
    OnCalcFields = TbChequesCalcFields
    TableName = 'cheques'
    Left = 56
    Top = 224
    object TbChequesConta: TIntegerField
      FieldName = 'Conta'
    end
    object TbChequesCheque: TFloatField
      FieldName = 'Cheque'
      DisplayFormat = '000000'
    end
    object TbChequesValorNU: TFloatField
      FieldName = 'ValorNU'
      DisplayFormat = '#,###,##0.00'
    end
    object TbChequesCliente: TWideStringField
      FieldName = 'Cliente'
      Size = 255
    end
    object TbChequesData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object TbChequesVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object TbChequesDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object TbChequesVALOR_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VALOR_TXT'
      Size = 50
      Calculated = True
    end
    object TbChequesTEXTO1: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXTO1'
      Size = 255
      Calculated = True
    end
    object TbChequesTEXTO2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXTO2'
      Size = 255
      Calculated = True
    end
    object TbChequesNOMECLIENTE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECLIENTE'
      Size = 255
      Calculated = True
    end
    object TbChequesDIA: TWideStringField
      DisplayWidth = 2
      FieldKind = fkCalculated
      FieldName = 'DIA'
      Size = 2
      Calculated = True
    end
    object TbChequesMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 15
      Calculated = True
    end
    object TbChequesANO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ANO'
      Size = 4
      Calculated = True
    end
    object TbChequesBOMPARA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'BOMPARA'
      Size = 100
      Calculated = True
    end
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrClienteCalcFields
    SQL.Strings = (
      'SELECT * FROM cheques'
      'ORDER BY Nome')
    Left = 84
    Top = 252
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrBancosTValorNU: TIntegerField
      FieldName = 'TValorNU'
    end
    object QrBancosTValorT1: TIntegerField
      FieldName = 'TValorT1'
    end
    object QrBancosTValorT2: TIntegerField
      FieldName = 'TValorT2'
    end
    object QrBancosTCliente: TIntegerField
      FieldName = 'TCliente'
    end
    object QrBancosTCidade: TIntegerField
      FieldName = 'TCidade'
    end
    object QrBancosTDia: TIntegerField
      FieldName = 'TDia'
    end
    object QrBancosTMes: TIntegerField
      FieldName = 'TMes'
    end
    object QrBancosTAno: TIntegerField
      FieldName = 'TAno'
    end
    object QrBancosTVencto: TIntegerField
      FieldName = 'TVencto'
    end
    object QrBancosIValorNU: TIntegerField
      FieldName = 'IValorNU'
    end
    object QrBancosIValorT1: TIntegerField
      FieldName = 'IValorT1'
    end
    object QrBancosIValorT2: TIntegerField
      FieldName = 'IValorT2'
    end
    object QrBancosICliente: TIntegerField
      FieldName = 'ICliente'
    end
    object QrBancosICidade: TIntegerField
      FieldName = 'ICidade'
    end
    object QrBancosIDia: TIntegerField
      FieldName = 'IDia'
    end
    object QrBancosIMes: TIntegerField
      FieldName = 'IMes'
    end
    object QrBancosIAno: TIntegerField
      FieldName = 'IAno'
    end
    object QrBancosIVencto: TIntegerField
      FieldName = 'IVencto'
    end
    object QrBancosWValorNU: TIntegerField
      FieldName = 'WValorNU'
    end
    object QrBancosWValorT1: TIntegerField
      FieldName = 'WValorT1'
    end
    object QrBancosWValorT2: TIntegerField
      FieldName = 'WValorT2'
    end
    object QrBancosWCliente: TIntegerField
      FieldName = 'WCliente'
    end
    object QrBancosWCidade: TIntegerField
      FieldName = 'WCidade'
    end
    object QrBancosWDia: TIntegerField
      FieldName = 'WDia'
    end
    object QrBancosWMes: TIntegerField
      FieldName = 'WMes'
    end
    object QrBancosWAno: TIntegerField
      FieldName = 'WAno'
    end
    object QrBancosWVencto: TIntegerField
      FieldName = 'WVencto'
    end
    object QrBancosFValorNU: TIntegerField
      FieldName = 'FValorNU'
    end
    object QrBancosFValorT1: TIntegerField
      FieldName = 'FValorT1'
    end
    object QrBancosFValorT2: TIntegerField
      FieldName = 'FValorT2'
    end
    object QrBancosFCliente: TIntegerField
      FieldName = 'FCliente'
    end
    object QrBancosFCidade: TIntegerField
      FieldName = 'FCidade'
    end
    object QrBancosFDia: TIntegerField
      FieldName = 'FDia'
    end
    object QrBancosFMes: TIntegerField
      FieldName = 'FMes'
    end
    object QrBancosFAno: TIntegerField
      FieldName = 'FAno'
    end
    object QrBancosFVencto: TIntegerField
      FieldName = 'FVencto'
    end
    object QrBancosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBancosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBancosUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrBancosUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrBancosAltura: TIntegerField
      FieldName = 'Altura'
    end
    object QrBancosRecuoT: TIntegerField
      FieldName = 'RecuoT'
    end
    object QrBancosRecuoL: TIntegerField
      FieldName = 'RecuoL'
    end
    object QrBancosLargura: TIntegerField
      FieldName = 'Largura'
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 112
    Top = 252
  end
  object PMImprime: TPopupMenu
    Left = 84
    Top = 308
    object Cheques1: TMenuItem
      Caption = '&Cheques'
      OnClick = Cheques1Click
    end
    object Cpias1: TMenuItem
      Caption = '&C'#243'pias'
      OnClick = Cpias1Click
    end
    object Testacheque1: TMenuItem
      Caption = '&Testa cheque'
      OnClick = Testacheque1Click
    end
    object Teste1: TMenuItem
      Caption = 'Teste'
      OnClick = Teste1Click
    end
  end
  object frxCheques: TfrxReport
    Version = '4.6.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39721.843071099500000000
    ReportOptions.LastChange = 39721.843071099500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Banda1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Banda1, Engine do'
      '  begin'
      '  Top         := VARF_BANTOP(VARF_BANTOP);'
      '  Height      := VARF_BANHEI(VARF_BANHEI);'
      '  end'
      'end;'
      ''
      'procedure Memo1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo1, Engine do'
      '  begin'
      '  Height      := VARF_VALORNUH(VARF_VALORNUH);'
      '  Top         := Banda1.Top + VARF_VALORNUT(VARF_VALORNUT);'
      '  Width       := VARF_VALORNUW(VARF_VALORNUW);'
      '  Left        := VARF_VALORNUL(VARF_VALORNUL);'
      ''
      '  Memo1.Font.Size   := VARF_VALORNUF(VARF_VALORNUF);'
      '  FrameTyp := VARF_FRAMETYP(VARF_FRAMETYP);'
      '  end'
      'end;'
      ''
      'procedure Memo2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo2, Engine do'
      '  begin'
      '  Height      := VARF_VALORT1H(VARF_VALORT1H);'
      '  Top         := Banda1.Top + VARF_VALORT1T(VARF_VALORT1T);'
      '  Width       := VARF_VALORT1W(VARF_VALORT1W);'
      '  Left        := VARF_VALORT1L(VARF_VALORT1L);'
      ''
      '  Memo2.Font.Size   := VARF_VALORT1F(VARF_VALORT1F);'
      '  FrameTyp := VARF_FRAMETYP(VARF_FRAMETYP);'
      '  end'
      'end;'
      ''
      'procedure Memo3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo3, Engine do'
      '  begin'
      '  Height      := VARF_VALORT2H(VARF_VALORT2H);'
      '  Top         := Banda1.Top + VARF_VALORT2T(VARF_VALORT2T);'
      '  Width       := VARF_VALORT2W(VARF_VALORT2W);'
      '  Left        := VARF_VALORT2L(VARF_VALORT2L);'
      ''
      '  Memo3.Font.Size   := VARF_VALORT2F(VARF_VALORT2F);'
      '  FrameTyp := VARF_FRAMETYP(VARF_FRAMETYP);'
      '  end'
      'end;'
      ''
      'procedure Memo4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo4, Engine do'
      '  begin'
      '  Height      := VARF_CLIENTEH(VARF_CLIENTEH);'
      '  Top         := Banda1.Top + VARF_CLIENTET(VARF_CLIENTET);'
      '  Width       := VARF_CLIENTEW(VARF_CLIENTEW);'
      '  Left        := VARF_CLIENTEL(VARF_CLIENTEL);'
      ''
      '  Memo4.Font.Size   := VARF_CLIENTEF(VARF_CLIENTEF);'
      '  FrameTyp := VARF_FRAMETYP(VARF_FRAMETYP);'
      '  end'
      'end;'
      ''
      'procedure Memo5OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo5, Engine do'
      '  begin'
      '  Height      := VARF_CIDADEH(VARF_CIDADEH);'
      '  Top         := Banda1.Top + VARF_CIDADET(VARF_CIDADET);'
      '  Width       := VARF_CIDADEW(VARF_CIDADEW);'
      '  Left        := VARF_CIDADEL(VARF_CIDADEL);'
      ''
      '  Memo5.Font.Size   := VARF_CIDADEF(VARF_CIDADEF);'
      '  FrameTyp := VARF_FRAMETYP(VARF_FRAMETYP);'
      '  end'
      'end;'
      ''
      'procedure Memo6OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo6, Engine do'
      '  begin'
      '  Height      := VARF_DIAH(VARF_DIAH);'
      '  Top         := Banda1.Top + VARF_DIAT(VARF_DIAT);'
      '  Width       := VARF_DIAW(VARF_DIAW);'
      '  Left        := VARF_DIAL(VARF_DIAL);'
      ''
      '  Memo6.Font.Size   := VARF_DIAF(VARF_DIAF);'
      '  FrameTyp := VARF_FRAMETYP(VARF_FRAMETYP);'
      '  end'
      'end;'
      ''
      'procedure Memo7OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo7, Engine do'
      '  begin'
      '  Height      := VARF_MESH(VARF_MESH);'
      '  Top         := Banda1.Top + VARF_MEST(VARF_MEST);'
      '  Width       := VARF_MESW(VARF_MESW);'
      '  Left        := VARF_MESL(VARF_MESL);'
      ''
      '  Memo7.Font.Size   := VARF_MESF(VARF_MESF);'
      '  FrameTyp := VARF_FRAMETYP(VARF_FRAMETYP);'
      '  end'
      'end;'
      ''
      'procedure Memo8OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo8, Engine do'
      '  begin'
      '  Height      := VARF_ANOH(VARF_ANOH);'
      '  Top         := Banda1.Top + VARF_ANOT(VARF_ANOT);'
      '  Width       := VARF_ANOW(VARF_ANOW);'
      '  Left        := VARF_ANOL(VARF_ANOL);'
      ''
      '  Memo8.Font.Size   := VARF_ANOF(VARF_ANOF);'
      '  FrameTyp := VARF_FRAMETYP(VARF_FRAMETYP);'
      '  end'
      'end;'
      ''
      'procedure Memo9OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo9, Engine do'
      '  begin'
      '  Height      := VARF_VENCTOH(VARF_VENCTOH);'
      '  Top         := Banda1.Top + VARF_VENCTOT(VARF_VENCTOT);'
      '  Width       := VARF_VENCTOW(VARF_VENCTOW);'
      '  Left        := VARF_VENCTOL(VARF_VENCTOL);'
      ''
      '  Memo9.Font.Size   := VARF_VENCTOF(VARF_VENCTOF);'
      '  FrameTyp := VARF_FRAMETYP(VARF_FRAMETYP);'
      '  end'
      'end;'
      ''
      'procedure Line1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Line1, Engine do'
      '  begin'
      
        '  if VARF_FRAMETYP(VARF_FRAMETYP) = 15 then Visible := True else' +
        ' Visible := False;'
      '  end'
      'end;'
      ''
      'procedure Line2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Line2, Engine do'
      '  begin'
      
        '  if VARF_FRAMETYP(VARF_FRAMETYP) = 15 then Visible := True else' +
        ' Visible := False;'
      '  Top :=  VARF_BANTOP(VARF_BANTOP) +  VARF_BANHEI(VARF_BANHEI);'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxChequesGetValue
    Left = 56
    Top = 280
    Datasets = <
      item
        DataSet = frxDsCheques
        DataSetName = 'frxDsCheques'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object Banda1: TfrxMasterData
        Height = 208.000000000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'Banda1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCheques
        DataSetName = 'frxDsCheques'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 576.897650000000000000
          Top = 9.070810000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo1OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[TbCheques."VALOR_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo2: TfrxMemoView
          Left = 136.897650000000000000
          Top = 41.070810000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo2OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[TbCheques."TEXTO1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 136.897650000000000000
          Top = 57.070810000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo3OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[TbCheques."TEXTO2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 136.897650000000000000
          Top = 73.070810000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo4OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[TbCheques."NOMECLIENTE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 136.897650000000000000
          Top = 93.070810000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo5OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          TagStr = 'CLIENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[CIDADE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 392.897650000000000000
          Top = 97.070810000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo6OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          TagStr = 'CLIENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[TbCheques."DIA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Left = 564.897650000000000000
          Top = 97.070810000000000000
          Width = 112.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo7OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          TagStr = 'CLIENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[TbCheques."MES"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Left = 680.897650000000000000
          Top = 97.070810000000000000
          Width = 64.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo8OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          TagStr = 'CLIENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[TbCheques."ANO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Left = 568.897650000000000000
          Top = 185.070810000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo9OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          TagStr = 'CLIENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[TbCheques."BOMPARA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Line1: TfrxLineView
          Left = 0.897650000000000000
          Top = 1.070810000000000000
          Width = 756.000000000000000000
          OnBeforePrint = 'Line1OnBeforePrint'
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Line2: TfrxLineView
          Left = 0.897650000000000000
          Top = 205.070810000000000000
          Width = 760.000000000000000000
          OnBeforePrint = 'Line2OnBeforePrint'
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
      end
    end
  end
  object frxDsCheques: TfrxDBDataset
    UserName = 'frxDsCheques'
    CloseDataSource = False
    DataSet = TbCheques
    Left = 84
    Top = 280
  end
  object frxCopias: TfrxReport
    Version = '4.6.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39721.846736828700000000
    ReportOptions.LastChange = 39721.846736828700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  with Memo1, Engine do'
      '  begin'
      '  Height      := VARF_VALORNUH(VARF_VALORNUH);'
      '  Top         := Banda1.Top + VARF_VALORNUT(VARF_VALORNUT);'
      '  Width       := VARF_VALORNUW(VARF_VALORNUW);'
      '  Left        := VARF_VALORNUL(VARF_VALORNUL);'
      ''
      '  Memo1.Font.Size   := VARF_VALORNUF(VARF_VALORNUF);'
      '  end'
      'end;'
      ''
      'procedure Memo2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo2, Engine do'
      '  begin'
      '  Height      := VARF_VALORT1H(VARF_VALORT1H);'
      '  Top         := Banda1.Top + VARF_VALORT1T(VARF_VALORT1T);'
      '  Width       := VARF_VALORT1W(VARF_VALORT1W);'
      '  Left        := VARF_VALORT1L(VARF_VALORT1L);'
      ''
      '  Memo2.Font.Size   := VARF_VALORT1F(VARF_VALORT1F);'
      '  end'
      'end;'
      ''
      'procedure Memo3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo3, Engine do'
      '  begin'
      '  Height      := VARF_VALORT2H(VARF_VALORT2H);'
      '  Top         := Banda1.Top + VARF_VALORT2T(VARF_VALORT2T);'
      '  Width       := VARF_VALORT2W(VARF_VALORT2W);'
      '  Left        := VARF_VALORT2L(VARF_VALORT2L);'
      ''
      '  Memo3.Font.Size   := VARF_VALORT2F(VARF_VALORT2F);'
      '  end'
      'end;'
      ''
      'procedure Memo4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo4, Engine do'
      '  begin'
      '  Height      := VARF_CLIENTEH(VARF_CLIENTEH);'
      '  Top         := Banda1.Top + VARF_CLIENTET(VARF_CLIENTET);'
      '  Width       := VARF_CLIENTEW(VARF_CLIENTEW);'
      '  Left        := VARF_CLIENTEL(VARF_CLIENTEL);'
      ''
      '  Memo4.Font.Size   := VARF_CLIENTEF(VARF_CLIENTEF);'
      '  end'
      'end;'
      ''
      'procedure Memo5OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo5, Engine do'
      '  begin'
      '  Height      := VARF_CIDADEH(VARF_CIDADEH);'
      '  Top         := Banda1.Top + VARF_CIDADET(VARF_CIDADET);'
      '  Width       := VARF_CIDADEW(VARF_CIDADEW);'
      '  Left        := VARF_CIDADEL(VARF_CIDADEL);'
      ''
      '  Memo5.Font.Size   := VARF_CIDADEF(VARF_CIDADEF);'
      '  end'
      'end;'
      ''
      'procedure Memo6OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo6, Engine do'
      '  begin'
      '  Height      := VARF_DIAH(VARF_DIAH);'
      '  Top         := Banda1.Top + VARF_DIAT(VARF_DIAT);'
      '  Width       := VARF_DIAW(VARF_DIAW);'
      '  Left        := VARF_DIAL(VARF_DIAL);'
      ''
      '  Memo6.Font.Size   := VARF_DIAF(VARF_DIAF);'
      '  end'
      'end;'
      ''
      'procedure Memo7OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo7, Engine do'
      '  begin'
      '  Height      := VARF_MESH(VARF_MESH);'
      '  Top         := Banda1.Top + VARF_MEST(VARF_MEST);'
      '  Width       := VARF_MESW(VARF_MESW);'
      '  Left        := VARF_MESL(VARF_MESL);'
      ''
      '  Memo7.Font.Size   := VARF_MESF(VARF_MESF);'
      '  end'
      'end;'
      ''
      'procedure Memo8OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo8, Engine do'
      '  begin'
      '  Height      := VARF_ANOH(VARF_ANOH);'
      '  Top         := Banda1.Top + VARF_ANOT(VARF_ANOT);'
      '  Width       := VARF_ANOW(VARF_ANOW);'
      '  Left        := VARF_ANOL(VARF_ANOL);'
      ''
      '  Memo8.Font.Size   := VARF_ANOF(VARF_ANOF);'
      '  end'
      'end;'
      ''
      'procedure Memo9OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo9, Engine do'
      '  begin'
      '  Height      := VARF_VENCTOH(VARF_VENCTOH);'
      '  Top         := Banda1.Top + VARF_VENCTOT(VARF_VENCTOT);'
      '  Width       := VARF_VENCTOW(VARF_VENCTOW);'
      '  Left        := VARF_VENCTOL(VARF_VENCTOL);'
      ''
      '  Memo9.Font.Size   := VARF_VENCTOF(VARF_VENCTOF);'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxChequesGetValue
    Left = 112
    Top = 280
    Datasets = <
      item
        DataSet = frxDsCheques
        DataSetName = 'frxDsCheques'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object Banda1: TfrxMasterData
        Height = 416.000000000000000000
        Top = 117.165430000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'Banda1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCheques
        DataSetName = 'frxDsCheques'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 535.000000000000000000
          Top = 5.511750000000010000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo1OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[TbCheques."VALOR_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 131.000000000000000000
          Top = 21.511750000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo2OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8 = (
            '[TbCheques."TEXTO1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 131.000000000000000000
          Top = 37.511750000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo3OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8 = (
            '[TbCheques."TEXTO2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 131.000000000000000000
          Top = 53.511750000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo4OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8 = (
            '[TbCheques."NOMECLIENTE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 131.000000000000000000
          Top = 73.511750000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo5OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          TagStr = 'CLIENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8 = (
            '[CIDADE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 387.000000000000000000
          Top = 77.511750000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo6OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          TagStr = 'CLIENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[TbCheques."DIA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 559.000000000000000000
          Top = 77.511750000000000000
          Width = 112.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo7OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          TagStr = 'CLIENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[TbCheques."MES"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 675.000000000000000000
          Top = 77.511750000000000000
          Width = 64.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo8OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          TagStr = 'CLIENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8 = (
            '[TbCheques."ANO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 563.000000000000000000
          Top = 165.511750000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo9OnBeforePrint'
          ShowHint = False
          StretchMode = smMaxHeight
          TagStr = 'CLIENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[TbCheques."BOMPARA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Left = 27.000000000000000000
          Top = 189.511750000000000000
          Width = 712.000000000000000000
          Height = 112.000000000000000000
          ShowHint = False
          Frame.Style = fsDash
          Shape = skRoundRectangle
        end
        object Shape2: TfrxShapeView
          Left = 27.000000000000000000
          Top = 269.511750000000000000
          Width = 712.000000000000000000
          Height = 112.000000000000000000
          ShowHint = False
          Frame.Style = fsDash
          Shape = skRoundRectangle
        end
        object Memo10: TfrxMemoView
          Left = 27.000000000000000000
          Top = 201.511750000000000000
          Width = 712.000000000000000000
          Height = 168.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = clWhite
          Frame.Typ = [ftLeft, ftRight]
        end
        object Memo12: TfrxMemoView
          Left = 451.000000000000000000
          Top = 191.511750000000000000
          Width = 96.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'CHEQUE:')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Left = 451.000000000000000000
          Top = 205.511750000000000000
          Width = 104.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 455.000000000000000000
          Top = 206.511750000000000000
          Width = 96.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          TagStr = 'CLIENTE'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[TbCheques."Cheque"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape4: TfrxShapeView
          Left = 35.000000000000000000
          Top = 261.511750000000000000
          Width = 696.000000000000000000
          Height = 112.000000000000000000
          ShowHint = False
          Frame.Style = fsDash
          Shape = skRoundRectangle
        end
        object Memo15: TfrxMemoView
          Left = 95.000000000000000000
          Top = 269.511750000000000000
          Width = 635.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          TagStr = 'CLIENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Memo.UTF8 = (
            '[QrBancos."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 35.000000000000000000
          Top = 275.511750000000000000
          Width = 60.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftBottom]
          Memo.UTF8 = (
            'BANCO:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 95.000000000000000000
          Top = 293.511750000000000000
          Width = 635.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          TagStr = 'CLIENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Memo.UTF8 = (
            '[TbCheques."Descricao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 35.000000000000000000
          Top = 299.511750000000000000
          Width = 60.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftBottom]
          Memo.UTF8 = (
            'USADO PARA:')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Left = 95.000000000000000000
          Top = 261.511750000000000000
          Height = 112.000000000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
      end
      object Titulo: TfrxPageHeader
        Height = 36.000000000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'TituloOnBeforePrint'
      end
    end
  end
end
