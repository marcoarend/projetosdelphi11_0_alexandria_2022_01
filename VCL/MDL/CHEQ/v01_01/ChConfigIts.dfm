object FmChConfigIts: TFmChConfigIts
  Left = 210
  Top = 181
  Caption = 
    'CHS-CONFG-002 :: Configura'#231#227'o de Item de Impress'#227'o Matricial de ' +
    'Cheque'
  ClientHeight = 395
  ClientWidth = 799
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 799
    Height = 233
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 160
      Top = 12
      Width = 23
      Height = 13
      Caption = 'Item:'
    end
    object Label2: TLabel
      Left = 92
      Top = 52
      Width = 28
      Height = 13
      Caption = 'Topo:'
    end
    object Label3: TLabel
      Left = 160
      Top = 52
      Width = 39
      Height = 13
      Caption = 'M. Esq.:'
    end
    object Label4: TLabel
      Left = 228
      Top = 52
      Width = 39
      Height = 13
      Caption = 'Largura:'
    end
    object Label6: TLabel
      Left = 296
      Top = 52
      Width = 30
      Height = 13
      Caption = 'Altura:'
    end
    object Label7: TLabel
      Left = 92
      Top = 12
      Width = 66
      Height = 13
      Caption = 'Configura'#231#227'o:'
    end
    object Label5: TLabel
      Left = 92
      Top = 180
      Width = 37
      Height = 13
      Caption = 'Padr'#227'o:'
    end
    object Label8: TLabel
      Left = 264
      Top = 180
      Width = 35
      Height = 13
      Caption = 'Prefixo:'
    end
    object Label9: TLabel
      Left = 492
      Top = 180
      Width = 32
      Height = 13
      Caption = 'Sufixo:'
    end
    object Label10: TLabel
      Left = 436
      Top = 180
      Width = 44
      Height = 13
      Caption = 'Espa'#231'os:'
    end
    object EdCampo: TdmkEdit
      Left = 160
      Top = 28
      Width = 65
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdCampoNome: TdmkEdit
      Left = 228
      Top = 28
      Width = 433
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdTopo: TdmkEdit
      Left = 92
      Top = 68
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMEsq: TdmkEdit
      Left = 160
      Top = 68
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdLarg: TdmkEdit
      Left = 228
      Top = 68
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object RGAliV: TRadioGroup
      Left = 92
      Top = 92
      Width = 280
      Height = 41
      Caption = ' Alinhamento altura: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Topo'
        'Centro'
        'Base')
      TabOrder = 8
    end
    object RGAliH: TRadioGroup
      Left = 380
      Top = 92
      Width = 280
      Height = 41
      Caption = ' Alinhamento largura: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Esquerda'
        'Centro'
        'Direita')
      TabOrder = 9
    end
    object RGItal: TRadioGroup
      Left = 92
      Top = 134
      Width = 280
      Height = 41
      Caption = ' It'#225'lico: '
      Columns = 2
      Enabled = False
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o'
        'Sim')
      TabOrder = 10
    end
    object RGNegr: TRadioGroup
      Left = 380
      Top = 134
      Width = 280
      Height = 41
      Caption = ' Negrito: '
      Columns = 2
      Enabled = False
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o'
        'Sim')
      TabOrder = 11
    end
    object EdAltu: TdmkEdit
      Left = 296
      Top = 68
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdCodigo: TdmkEdit
      Left = 92
      Top = 28
      Width = 65
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object RGFTam: TRadioGroup
      Left = 380
      Top = 50
      Width = 280
      Height = 41
      Caption = ' Tamanho da fonte: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Comprimido'
        'Normal'
        'Expandido')
      TabOrder = 7
    end
    object EdPadr: TdmkEdit
      Left = 92
      Top = 196
      Width = 168
      Height = 21
      TabOrder = 12
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdPref: TdmkEdit
      Left = 264
      Top = 196
      Width = 168
      Height = 21
      TabOrder = 13
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdSufi: TdmkEdit
      Left = 492
      Top = 196
      Width = 168
      Height = 21
      TabOrder = 15
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdPrEs: TdmkEdit
      Left = 436
      Top = 196
      Width = 53
      Height = 21
      Alignment = taRightJustify
      TabOrder = 14
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 799
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 751
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 703
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 682
        Height = 32
        Caption = 'Configura'#231#227'o de Item de Impress'#227'o Matricial de Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 682
        Height = 32
        Caption = 'Configura'#231#227'o de Item de Impress'#227'o Matricial de Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 682
        Height = 32
        Caption = 'Configura'#231#227'o de Item de Impress'#227'o Matricial de Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 281
    Width = 799
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 795
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 325
    Width = 799
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 653
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 651
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 22
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object DsTransportadoras: TDataSource
    DataSet = QrTransportadoras
    Left = 532
    Top = 58
  end
  object QrTransportadoras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE, Codigo'
      'FROM entidades '
      'WHERE Codigo>0'
      'AND Fornece2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 504
    Top = 58
    object QrTransportadorasNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrTransportadorasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM chconfval'
      'WHERE Codigo=:P0'
      'AND Campo=:P1')
    Left = 372
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
end
