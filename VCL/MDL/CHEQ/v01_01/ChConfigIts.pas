unit ChConfigIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkGeral, dmkEdit,
  dmkImage, UnDmkEnums;

type
  TFmChConfigIts = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    DsTransportadoras: TDataSource;
    EdCampo: TdmkEdit;
    EdCampoNome: TdmkEdit;
    QrTransportadoras: TmySQLQuery;
    QrTransportadorasNOMEENTIDADE: TWideStringField;
    QrTransportadorasCodigo: TIntegerField;
    Label2: TLabel;
    EdTopo: TdmkEdit;
    EdMEsq: TdmkEdit;
    Label3: TLabel;
    EdLarg: TdmkEdit;
    Label4: TLabel;
    RGAliV: TRadioGroup;
    RGAliH: TRadioGroup;
    RGItal: TRadioGroup;
    RGNegr: TRadioGroup;
    EdAltu: TdmkEdit;
    Label6: TLabel;
    EdCodigo: TdmkEdit;
    Label7: TLabel;
    QrLoc: TmySQLQuery;
    QrLocCodigo: TIntegerField;
    RGFTam: TRadioGroup;
    Label5: TLabel;
    EdPadr: TdmkEdit;
    EdPref: TdmkEdit;
    Label8: TLabel;
    EdSufi: TdmkEdit;
    Label9: TLabel;
    Label10: TLabel;
    EdPrEs: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FUmAUm: Boolean;
    function ProcuraItem(Codigo, Campo: Integer): Boolean;
  end;

var
  FmChConfigIts: TFmChConfigIts;

implementation

uses UnMyObjects, Module, Entidades, ChConfigCab, DmkDAC_PF;

{$R *.DFM}

procedure TFmChConfigIts.BtDesisteClick(Sender: TObject);
begin
  FmChConfigCab.FDesiste := True;
  Close;
end;

procedure TFmChConfigIts.BtConfirmaClick(Sender: TObject);
const
  FAlturaAgulhas = 34;
var
  Altera: Boolean;
  Topo, Altu, TopR, MEsq: Integer;
begin
  TopR := 0;
  MEsq := Geral.IMV(EdMEsq.Text);
  if MEsq + FmChConfigCab.QrChConfCabMEsq.Value < 0 then
  begin
    Geral.MB_Aviso('Margem esquerda inv�lida, seu recuo ficaria ' +
    'negativo pela margem da p�gina, que � ' +
    IntToStr(FmChConfigCab.QrChConfCabMEsq.Value) + '.');
    Exit;
  end;
  //
  Altera := ProcuraItem(Geral.IMV(EdCodigo.Text), Geral.IMV(EdCampo.Text));
  Topo := Geral.IMV(EdTopo.Text);
  Altu := Geral.IMV(EdAltu.Text);
  if Altu <= FAlturaAgulhas then TopR := Topo else
    case RGAliV.ItemIndex of
      0: TopR := Topo;
      1: TopR := Topo + Trunc((Altu - FAlturaAgulhas)/2);
      2: TopR := Topo + (Altu - FAlturaAgulhas);
    end;
  Dmod.QrUpd.SQL.Clear;
  if not Altera then
    Dmod.QrUpd.SQL.Add('INSERT INTO chconfval SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE chconfval SET ');
  Dmod.QrUpd.SQL.Add('Topo=:P00, MEsq=:P01, Altu=:P02, Larg=:P03, FTam=:P04, ');
  Dmod.QrUpd.SQL.Add('Negr=:P05, Ital=:P06, AliH=:P07, AliV=:P08, Padr=:P09, ');
  Dmod.QrUpd.SQL.Add('Pref=:P10, Sufi=:P11, TopR=:P12, PrEs=:P13, ');
  //
  if not Altera then
    Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Campo=:Pd')
  else
    Dmod.QrUpd.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc AND Campo=:Pd');
  //
  Dmod.QrUpd.Params[00].AsInteger := Topo;
  Dmod.QrUpd.Params[01].AsInteger := MEsq;
  Dmod.QrUpd.Params[02].AsInteger := Altu;
  Dmod.QrUpd.Params[03].AsInteger := Geral.IMV(EdLarg.Text);
  Dmod.QrUpd.Params[04].AsInteger := RGFTam.ItemIndex;
  Dmod.QrUpd.Params[05].AsInteger := RGNegr.ItemIndex;
  Dmod.QrUpd.Params[06].AsInteger := RGItal.ItemIndex;
  Dmod.QrUpd.Params[07].AsInteger := RGAliH.ItemIndex;
  Dmod.QrUpd.Params[08].AsInteger := RGAliV.ItemIndex;
  Dmod.QrUpd.Params[09].AsString  := EdPadr.Text;
  Dmod.QrUpd.Params[10].AsString  := EdPref.Text;
  Dmod.QrUpd.Params[11].AsString  := EdSufi.Text;
  Dmod.QrUpd.Params[12].AsInteger := TopR;
  Dmod.QrUpd.Params[13].AsInteger := Geral.IMV(EdPrEs.Text);
  //
  Dmod.QrUpd.Params[14].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[15].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[16].AsString  := EdCodigo.Text;
  Dmod.QrUpd.Params[17].AsString  := EdCampo.Text;
  Dmod.QrUpd.ExecSQL;
  if FUmAUm then FmChConfigCab.FDesiste := False;
  Close;
end;

procedure TFmChConfigIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmChConfigIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmChConfigIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

function TFmChConfigIts.ProcuraItem(Codigo, Campo: Integer): Boolean;
begin
  QrLoc.Close;
  QrLoc.Params[0].AsInteger := Codigo;
  QrLoc.Params[1].AsInteger := Campo;
  UnDmkDAC_PF.AbreQuery(QrLoc, DMod.MyDB);
  //
  if QrLoc.RecordCount > 0 then Result := True else Result := False;
end;

end.
