unit Cheque;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Variants, StdCtrls, Buttons, Db, mySQLDbTables, ComCtrls, Grids,
  DBGrids, Mask, DBCtrls, printers, Menus, frxClass, frxDBSet, dmkGeral,
  dmkEdit, dmkDBLookupComboBox, dmkEditCB;

type
  TFmCheque = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    PainelControle: TPanel;
    BtSaida: TBitBtn;
    BtImprime: TBitBtn;
    Panel1: TPanel;
    QrCliente: TmySQLQuery;
    QrClienteCNPJ_TXT: TWideStringField;
    QrClienteTE1_TXT: TWideStringField;
    QrClienteCEP_TXT: TWideStringField;
    Panel3: TPanel;
    Panel4: TPanel;
    GroupBox4: TGroupBox;
    Label7: TLabel;
    LaFatID: TLabel;
    LaFatNum: TLabel;
    EdTopIni: TdmkEdit;
    BtSalvar: TBitBtn;
    QrClienteTipo: TSmallintField;
    QrClienteLogo: TBlobField;
    QrClienteRespons1: TWideStringField;
    QrClienteRespons2: TWideStringField;
    QrClienteRUA: TWideStringField;
    QrClienteNUMERO: TLargeintField;
    QrClienteCOMPL: TWideStringField;
    QrClienteBAIRRO: TWideStringField;
    QrClienteTE1: TWideStringField;
    QrClienteFAX: TWideStringField;
    QrClienteDOC1: TWideStringField;
    QrClienteDOC2: TWideStringField;
    QrClienteCIDADE: TWideStringField;
    QrClienteCEP: TLargeintField;
    QrClienteUF: TLargeintField;
    QrClienteNOMEUF: TWideStringField;
    QrClienteNOMEENTIDADE: TWideStringField;
    QrClienteNUM_TXT: TWideStringField;
    QrClienteDOC_TXT: TWideStringField;
    Label9: TLabel;
    EdBomPara: TdmkEdit;
    DataSource1: TDataSource;
    QrClienteCodigo: TIntegerField;
    DsCheques: TDataSource;
    TbCheques: TmySQLTable;
    TbChequesConta: TIntegerField;
    TbChequesCheque: TFloatField;
    TbChequesValorNU: TFloatField;
    TbChequesCliente: TWideStringField;
    TbChequesData: TDateField;
    TbChequesVencto: TDateField;
    TbChequesDescricao: TWideStringField;
    EdFormatI: TdmkEdit;
    EdFormatF: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdCidade: TdmkEdit;
    Panel2: TPanel;
    QrBancos: TmySQLQuery;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    QrBancosTValorNU: TIntegerField;
    QrBancosTValorT1: TIntegerField;
    QrBancosTValorT2: TIntegerField;
    QrBancosTCliente: TIntegerField;
    QrBancosTCidade: TIntegerField;
    QrBancosTDia: TIntegerField;
    QrBancosTMes: TIntegerField;
    QrBancosTAno: TIntegerField;
    QrBancosTVencto: TIntegerField;
    QrBancosIValorNU: TIntegerField;
    QrBancosIValorT1: TIntegerField;
    QrBancosIValorT2: TIntegerField;
    QrBancosICliente: TIntegerField;
    QrBancosICidade: TIntegerField;
    QrBancosIDia: TIntegerField;
    QrBancosIMes: TIntegerField;
    QrBancosIAno: TIntegerField;
    QrBancosIVencto: TIntegerField;
    QrBancosWValorNU: TIntegerField;
    QrBancosWValorT1: TIntegerField;
    QrBancosWValorT2: TIntegerField;
    QrBancosWCliente: TIntegerField;
    QrBancosWCidade: TIntegerField;
    QrBancosWDia: TIntegerField;
    QrBancosWMes: TIntegerField;
    QrBancosWAno: TIntegerField;
    QrBancosWVencto: TIntegerField;
    QrBancosFValorNU: TIntegerField;
    QrBancosFValorT1: TIntegerField;
    QrBancosFValorT2: TIntegerField;
    QrBancosFCliente: TIntegerField;
    QrBancosFCidade: TIntegerField;
    QrBancosFDia: TIntegerField;
    QrBancosFMes: TIntegerField;
    QrBancosFAno: TIntegerField;
    QrBancosFVencto: TIntegerField;
    QrBancosDataCad: TDateField;
    QrBancosDataAlt: TDateField;
    QrBancosUserCad: TSmallintField;
    QrBancosUserAlt: TSmallintField;
    DsBancos: TDataSource;
    QrBancosAltura: TIntegerField;
    TbChequesVALOR_TXT: TWideStringField;
    TbChequesTEXTO1: TWideStringField;
    TbChequesTEXTO2: TWideStringField;
    TbChequesNOMECLIENTE: TWideStringField;
    TbChequesDIA: TWideStringField;
    TbChequesMES: TWideStringField;
    TbChequesANO: TWideStringField;
    TbChequesBOMPARA: TWideStringField;
    PMImprime: TPopupMenu;
    Cheques1: TMenuItem;
    Cpias1: TMenuItem;
    QrBancosRecuoT: TIntegerField;
    QrBancosRecuoL: TIntegerField;
    Testacheque1: TMenuItem;
    LaCliente: TLabel;
    EdManeira: TLabel;
    Teste1: TMenuItem;
    BtTeste: TBitBtn;
    BitBtn1: TBitBtn;
    QrBancosLargura: TIntegerField;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    Panel6: TPanel;
    Label1: TLabel;
    EdBanco: TdmkEditCB;
    CBBanco: TdmkDBLookupComboBox;
    BtExclui: TBitBtn;
    ListBox1: TListBox;
    ListBox2: TListBox;
    RGTeste: TRadioGroup;
    Button1: TButton;
    Label4: TLabel;
    EdLeftIni: TdmkEdit;
    CkGrade: TCheckBox;
    frxCheques: TfrxReport;
    frxDsCheques: TfrxDBDataset;
    frxCopias: TfrxReport;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure EdTopIniExit(Sender: TObject);
    procedure QrClienteCalcFields(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbChequesCalcFields(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure Cheques1Click(Sender: TObject);
    procedure Cpias1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure Testacheque1Click(Sender: TObject);
    procedure Teste1Click(Sender: TObject);
    procedure BtTesteClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure frxChequesGetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
    procedure ImprimeCheques(Maneira: Integer);
  public
    { Public declarations }
  end;

var
  FmCheque: TFmCheque;

implementation

uses UnMyObjects, Module, UnMLAGeral, UnInternalConsts, Principal, UCreate, ImpDOS2;

{$R *.DFM}

procedure TFmCheque.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCheque.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  TbCheques.Close;
  TbCheques.Open;
end;

procedure TFmCheque.BtSalvarClick(Sender: TObject);
var
 Path: String;
begin
  Path  := 'Dermatek\Cheques';
  Geral.WriteAppKey('TopIni',   Path, EdTopIni.Text,  ktString, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('LeftIni',  Path, EdLeftIni.Text, ktString, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('Cidade',   Path, EdCidade.Text,  ktString, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('BomPara',  Path, EdBomPara.Text, ktString, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('FormatI',  Path, EdFormatI.Text, ktString, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('FormatF',  Path, EdFormatF.Text, ktString, HKEY_LOCAL_MACHINE);
end;

procedure TFmCheque.EdTopIniExit(Sender: TObject);
begin
  EdTopIni.Text := Geral.TFT(EdTopIni.Text, 0, siPositivo);
end;

procedure TFmCheque.QrClienteCalcFields(DataSet: TDataSet);
begin
  QrClienteTE1_TXT.Value := Geral.FormataTelefone_TT(QrClienteTe1.Value);
  QrClienteCEP_TXT.Value :=Geral.FormataCEP_NT(QrClienteCEP.Value);
  QrClienteDOC_TXT.Value := Geral.FormataCNPJ_TT(QrClienteDOC1.Value);
  if QrClienteNUMERO.Value <> 0 then
    QrClienteNUM_TXT.Value := FormatFloat('#,##0', QrClienteNUMERO.Value)
  else QrClienteNUM_TXT.Value := 'S/N';
end;

procedure TFmCheque.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmCheque.FormCreate(Sender: TObject);
var
 Path: String;
begin
  Path  := 'Dermatek\Cheques';
  EdTopIni.Text  := Geral.ReadAppKey('TopIni',   Path, ktString, '0',       HKEY_LOCAL_MACHINE);
  EdLeftIni.Text := Geral.ReadAppKey('LeftIni',  Path, ktString, '0',       HKEY_LOCAL_MACHINE);
  EdCidade.Text  := Geral.ReadAppKey('Cidade',   Path, ktString, ' ',       HKEY_LOCAL_MACHINE);
  EdBomPara.Text := Geral.ReadAppKey('BomPara',  Path, ktString, 'BomPara', HKEY_LOCAL_MACHINE);
  EdFormatI.Text := Geral.ReadAppKey('FormatI',  Path, ktString, '(',       HKEY_LOCAL_MACHINE);
  EdFormatF.Text := Geral.ReadAppKey('FormatF',  Path, ktString, ')',       HKEY_LOCAL_MACHINE);
  ///
  QrBancos.Open;
  ///
  ListBox1.Items.Text := Printer.Printers.Text;
  ListBox2.Items.Text := Printer.Fonts.Text;
  ///
end;

procedure TFmCheque.TbChequesCalcFields(DataSet: TDataSet);
var
  Fator: Double;
  TextoT: String;
  i, Tam1, TamT: Integer;
  Dia, Mes, Ano: Word;
begin
  case QrBancosFValorT1.Value of
    08: Fator := 15;
    10: Fator := 12;
    12: Fator := 10;
    14: Fator := 8.5;
    else Fator := 12;
  end;
  TbChequesVALOR_TXT.Value := EdFormatI.Text+
    FormatFloat('#,###,##0.00',TbChequesValorNU.Value)+EdFormatF.Text;
  //
  TextoT := dmkPF.ExtensoMoney(Geral.TFT(FloatToStr(TbChequesValorNU.Value), 2, siPositivo));
  TamT := Length(TextoT);
  Tam1 := trunc(int(((QrBancosWValorT1.Value - QrBancosIValorT1.Value) / 100 /
          2.541) * Fator));
  i := TamT;
  if TamT > Tam1 then
  begin
    if TextoT[Tam1+1] <> ' ' then
    begin
      i := Tam1;
      while not (TextoT[i] = ' ') do i := i - 1;
    end else i := Tam1;
  end;
  TbChequesTEXTO1.Value := Copy(TextoT, 1, i);
  TbChequesTEXTO2.Value := Copy(TextoT, i+1, TamT-i);
  //
  if TbChequesCliente.Value = '[ND]' then
    TbChequesNOMECLIENTE.Value := ''
  else TbChequesNOMECLIENTE.Value := TbChequesCliente.Value;
  //
  DecodeDate(TbChequesVencto.Value, Ano, Mes, Dia);
  TbChequesDIA.Value := FormatFloat('00', Dia);
  TbChequesMES.Value := MLAGeral.VerificaMes(Mes, False);
  TbChequesANO.Value := FormatFloat('0000', Ano);
  //
  if TbChequesVencto.Value > TbChequesData.Value then
    TbChequesBOMPARA.Value := EdBomPara.Text+' '+
    FormatDateTime(VAR_FORMATDATE3, TbChequesVencto.Value)
  else
    TbChequesBOMPARA.Value := '';
end;

procedure TFmCheque.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCheque.Cheques1Click(Sender: TObject);
begin
  ImprimeCheques(0);
end;

procedure TFmCheque.Cpias1Click(Sender: TObject);
begin
  if CBBanco.KeyValue = NULL then
    Application.MessageBox('Defina o banco a que pertence o cheque!',
    'Falta de Informa��es', MB_OK+MB_ICONERROR)
  else begin
    MyObjects.frxMostra(frxCopias, 'C�pia de cheque');
  end;
end;

procedure TFmCheque.BtExcluiClick(Sender: TObject);
begin
  if Application.MessageBox('Deseja limpar toda lista?',
  'Limpeza de toda lista',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1
  +MB_APPLMODAL) = ID_YES then
  begin
    if UCriar.GerenciaTabelaLocal('Cheques', acFind) then
    begin
      Dmod.QrUpdL.SQL.Clear;
      Dmod.QrUpdL.SQL.Add('DELETE FROM cheques');
      Dmod.QrUpdL.ExecSQL;
    end;
  end;
end;

procedure TFmCheque.Testacheque1Click(Sender: TObject);
begin
  ImprimeCheques(15);
end;

procedure TFmCheque.ImprimeCheques(Maneira: Integer);
//var
  //Altura: Integer;
begin
  EdManeira.Caption := IntToStr(Maneira);
  if CBBanco.KeyValue = NULL then
    Application.MessageBox('Defina o banco a que pertence o cheque!',
    'Falta de Informa��es', MB_OK+MB_ICONERROR)
  else begin
    (*
    Altura :=
      (TbCheques.RecordCount*(QrBancosAltura.Value - QrBancosRecuoT.Value)) +
      Geral.IMV(EdTopIni.Text)+100;
    frCheques.Title := IntToStr(TbCheques.RecordCount)+' cheques';
    frCheques.Pages[0].ChangePaper(
    255{255=p�gina definida pelo Usu�rio},
    2100, {largura mm x 10}
    Altura, {altura mm x 10}
    65535, {? - cor?}
    poPortrait);
    frCheques.PrepareReport;
    frCheques.ShowPreparedReport;
    *)
    //
    MyObjects.frxMostra(frxCheques, 'Cheque(s)');
  end;
end;

procedure TFmCheque.Teste1Click(Sender: TObject);
begin
  //
  
end;

procedure TFmCheque.BtTesteClick(Sender: TObject);
begin
  Application.CreateForm(TFmImpDOS2, FmImpDOS2);
  FmImpDOS2.ShowModal;
  FmImpDOS2.Destroy;
end;

procedure TFmCheque.BitBtn1Click(Sender: TObject);
var
  Ini, Lef, Cont: Integer;
  Device: array[0..255] of char;
  Driver: array[0..255] of char;
  Port  : array[0..255] of char;
  hDMode: THandle;
  pDMode: PDEVMODE;
  //////////////////////
  T, F, I, W, j: Integer;
  //////////////////////
  H: Integer;
  Oh, Ov, Ah, Av, Bh, Bv, Ch, Cv, Dh, Dv: Integer;
  HRes, VRes: Double;
  M: String;
begin
  with Printer do begin
    PrinterIndex := ListBox1.ItemIndex;
    GetPrinter(Device, Driver, Port, hDMode);
    if hDMode <> 0 then begin
      pDMode := GlobalLock(hDMode);
      if pDMode <> nil then begin
        pDMode^.dmPaperSize   := DMPAPER_USER;
        pDMode^.dmPaperWidth  := 2100;
        pDMode^.dmPaperLength := 2970;
        pDMode^.dmFields      := pDMode^.dmFields or DM_PAPERSIZE;
      end;
      GlobalUnLock(hDMode);
    end;
(*    ShowMessage('Largura = '+IntToStr(Printer.PageWidth)+ Chr(13) + Chr(10)+
                'Altura    = '+IntToStr(Printer.PageHeight)+ Chr(13) + Chr(10)+
                'HRes        = '+IntToStr(GetDeviceCaps(Handle, LOGPIXELSX))+ Chr(13) + Chr(10)+
                'VRes        = '+IntToStr(GetDeviceCaps(Handle, LOGPIXELSY)));*)

    Orientation := poPortrait;
    Printer.Canvas.Font.Name := ListBox2.Items[ListBox2.ItemIndex];
    BeginDoc;
    try
      Ini := Geral.IMV(EdTopIni.Text);
      Lef := Geral.IMV(EdLeftIni.Text);
      TbCheques.First;
      while not TbCheques.Eof do
      begin
        Cont := (TbCheques.RecNo -1 )* QrBancosAltura.Value;
        T := 0;
        I := 0;
        W := 0;
        //////////////////////////
        for j := 0 to 9 do
        begin
          case j of
            0: T := 0;
            1: T := QrBancosTValorNU.Value;
            2: T := QrBancosTValorT1.Value;
            3: T := QrBancosTValorT2.Value;
            4: T := QrBancosTCliente.Value;
            5: T := QrBancosTCidade.Value;
            6: T := QrBancosTDia.Value;
            7: T := QrBancosTMes.Value;
            8: T := QrBancosTAno.Value;
            9: T := QrBancosTVencto.Value;
          end;
          case j of
            0: I := 0;
            1: I := QrBancosIValorNU.Value;
            2: I := QrBancosIValorT1.Value;
            3: I := QrBancosIValorT2.Value;
            4: I := QrBancosICliente.Value;
            5: I := QrBancosICidade.Value;
            6: I := QrBancosIDia.Value;
            7: I := QrBancosIMes.Value;
            8: I := QrBancosIAno.Value;
            9: I := QrBancosIVencto.Value;
          end;
          case j of
            0: W := QrBancosLargura.Value;
            1: W := QrBancosWValorNU.Value;
            2: W := QrBancosWValorT1.Value;
            3: W := QrBancosWValorT2.Value;
            4: W := QrBancosWCliente.Value;
            5: W := QrBancosWCidade.Value;
            6: W := QrBancosWDia.Value;
            7: W := QrBancosWMes.Value;
            8: W := QrBancosWAno.Value;
            9: W := QrBancosWVencto.Value;
          end;
          case j of
            0: Printer.Canvas.Font.Size := 0;
            1: Printer.Canvas.Font.Size := QrBancosFValorNU.Value;
            2: Printer.Canvas.Font.Size := QrBancosFValorT1.Value;
            3: Printer.Canvas.Font.Size := QrBancosFValorT2.Value;
            4: Printer.Canvas.Font.Size := QrBancosFCliente.Value;
            5: Printer.Canvas.Font.Size := QrBancosFCidade.Value;
            6: Printer.Canvas.Font.Size := QrBancosFDia.Value;
            7: Printer.Canvas.Font.Size := QrBancosFMes.Value;
            8: Printer.Canvas.Font.Size := QrBancosFAno.Value;
            9: Printer.Canvas.Font.Size := QrBancosFVencto.Value;
          end;
          case j of
            0: M := '';
            1: M := TbChequesVALOR_TXT.Value;
            2: M := TbChequesTEXTO1.Value;
            3: M := TbChequesTEXTO2.Value;
            4: M := TbChequesNOMECLIENTE.Value;
            5: M := EdCidade.Text;
            6: M := TbChequesDIA.Value;
            7: M := TbChequesMES.Value;
            8: M := TbChequesANO.Value;
            9: M := TbChequesBOMPARA.Value;
          end;
          /////////////////////////////////////////////
          HRes := GetDeviceCaps(Handle, LOGPIXELSX) / 254;
          VRes := GetDeviceCaps(Handle, LOGPIXELSY) / 254;
          /////////////////////////
          if j = 0 then
          begin
            H := Cont;//ini
            T := Cont + QrBancosAltura.Value - Ini;
            F := 0;
          end else begin
            H := Cont + T - Ini;
            T := T - Ini + Cont;
            F := Printer.Canvas.TextHeight('My');
          end;
          I := I - Lef;
          W := W - Lef;
          if I < 0 then
          begin
            if j > 0 then Application.MessageBox('Margem esquerda negativa!',
            'Aviso', MB_OK+MB_ICONWARNING);
            I := 0;
          end;
          if W < 0 then
          begin
            if j > 0 then Application.MessageBox('Margem direita negativa!',
            'Aviso', MB_OK+MB_ICONWARNING);
            W := 0;
          end;
          /////////////////////////
          Oh := Trunc(I * HRes);
          Ov := Trunc(H * VRes) - F;
          Ah := Trunc(I * HRes);
          Av := Trunc(T * VRes);
          Bh := Trunc(W * HRes);
          Bv := Trunc(T * VRes);
          Ch := Trunc(W * HRes);
          Cv := Trunc(H * VRes) - F;
          Dh := Trunc(I * HRes);
          Dv := Trunc(H * VRes) - F;
          /////////////////////////
          if j <> -1 then
          begin
            Canvas.TextOut(Oh, Ov, M);
            if CkGrade.Checked then
            begin
              Canvas.MoveTo(Oh, Ov);
              Canvas.LineTo(Ah, Av);
              Canvas.LineTo(Bh, Bv);
              Canvas.LineTo(Ch, Cv);
              Canvas.LineTo(Dh, Dv);
            end;  
          end;
        end;
        TbCheques.Next;
      end;
    finally
      EndDoc;
    end;
  end;
end;

procedure TFmCheque.Button1Click(Sender: TObject);
begin
  Printer.Canvas.Font.Size := StrToInt(RGTeste.Items[RGTeste.ItemIndex]);
  ShowMessage('Tamanho = '+IntToStr(Printer.Canvas.TextHeight('My')));
end;

procedure TFmCheque.frxChequesGetValue(const VarName: String;
  var Value: Variant);
var
  RecuoT, RecuoL: Extended;
begin
  if VarName = 'CIDADE' then Value := EdCidade.Text;

  // user functions

  RecuoT := QrBancosRecuoT.Value / VAR_DOTIMP;
  RecuoL := QrBancosRecuoL.Value / VAR_DOTIMP;
  if VarName = 'VARF_FRAMETYP' then Value := EdManeira.Caption;
  //////////////////////////////////////////////////////////////////////////////
  if VarName = 'VARF_INITOP' then Value := Geral.IMV(EdTopIni.Text);
  //
  if VarName = 'VARF_BANTOP' then Value := ((QrBancosAltura.Value -
      QrBancosRecuoT.Value) / VAR_DOTIMP) * (TbCheques.RecNo -1);
  if VarName = 'VARF_BANHEI' then Value := (QrBancosAltura.Value -
     QrBancosRecuoT.Value) / VAR_DOTIMP;
  //
  if VarName = 'VARF_VALORNUH' then Value := QrBancosFValorNU.Value + 8;/// VAR_DOTIMP; //Height;
  if VarName = 'VARF_VALORNUT' then Value := (QrBancosTValorNU.Value / VAR_DOTIMP) -
     (QrBancosFValorNU.Value + 8) - RecuoT; //Top;
  if VarName = 'VARF_VALORNUW' then Value := ((QrBancosWValorNU.Value -
     QrBancosIValorNU.Value) / VAR_DOTIMP) + 8; //Width;
  if VarName = 'VARF_VALORNUL' then Value := QrBancosIValorNU.Value / VAR_DOTIMP - RecuoL; //Left;
  if VarName = 'VARF_VALORNUF' then Value := QrBancosFValorNU.Value; //Font;
  //
  if VarName = 'VARF_VALORT1H' then Value := QrBancosFValorT1.Value + 8;/// VAR_DOTIMP; //Height;
  if VarName = 'VARF_VALORT1T' then Value := (QrBancosTValorT1.Value / VAR_DOTIMP) -
     (QrBancosFValorT1.Value + 8) - RecuoT; //Top;
  if VarName = 'VARF_VALORT1W' then Value := ((QrBancosWValorT1.Value -
     QrBancosIValorT1.Value) / VAR_DOTIMP) + 8; //Width;
  if VarName = 'VARF_VALORT1L' then Value := QrBancosIValorT1.Value / VAR_DOTIMP - RecuoL; //Left;
  if VarName = 'VARF_VALORT1F' then Value := QrBancosFValorT1.Value; //Font;
  //
  if VarName = 'VARF_VALORT2H' then Value := QrBancosFValorT2.Value + 8;/// VAR_DOTIMP; //Height;
  if VarName = 'VARF_VALORT2T' then Value := (QrBancosTValorT2.Value / VAR_DOTIMP) -
     (QrBancosFValorT2.Value + 8) - RecuoT; //Top;
  if VarName = 'VARF_VALORT2W' then Value := ((QrBancosWValorT2.Value -
     QrBancosIValorT2.Value) / VAR_DOTIMP) + 8; //Width;
  if VarName = 'VARF_VALORT2L' then Value := QrBancosIValorT2.Value / VAR_DOTIMP - RecuoL; //Left;
  if VarName = 'VARF_VALORT2F' then Value := QrBancosFValorT2.Value; //Font;
  //
  if VarName = 'VARF_CLIENTEH' then Value := QrBancosFCliente.Value + 8;/// VAR_DOTIMP; //Height;
  if VarName = 'VARF_CLIENTET' then Value := (QrBancosTCliente.Value / VAR_DOTIMP) -
     (QrBancosFCliente.Value + 8) - RecuoT; //Top;
  if VarName = 'VARF_CLIENTEW' then Value := ((QrBancosWCliente.Value -
     QrBancosICliente.Value) / VAR_DOTIMP) + 8; //Width;
  if VarName = 'VARF_CLIENTEL' then Value := QrBancosICliente.Value / VAR_DOTIMP - RecuoL; //Left;
  if VarName = 'VARF_CLIENTEF' then Value := QrBancosFCliente.Value; //Font;
  //
  if VarName = 'VARF_CIDADEH' then Value := QrBancosFCidade.Value + 8;/// VAR_DOTIMP; //Height;
  if VarName = 'VARF_CIDADET' then Value := (QrBancosTCidade.Value / VAR_DOTIMP) -
     (QrBancosFCidade.Value + 8) - RecuoT; //Top;
  if VarName = 'VARF_CIDADEW' then Value := ((QrBancosWCidade.Value -
     QrBancosICidade.Value) / VAR_DOTIMP) + 8; //Width;
  if VarName = 'VARF_CIDADEL' then Value := QrBancosICidade.Value / VAR_DOTIMP - RecuoL; //Left;
  if VarName = 'VARF_CIDADEF' then Value := QrBancosFCidade.Value; //Font;
  //
  if VarName = 'VARF_DIAH' then Value := QrBancosFDia.Value + 8;/// VAR_DOTIMP; //Height;
  if VarName = 'VARF_DIAT' then Value := (QrBancosTDia.Value / VAR_DOTIMP) -
     (QrBancosFDia.Value + 8) - RecuoT; //Top;
  if VarName = 'VARF_DIAW' then Value := ((QrBancosWDia.Value -
     QrBancosIDia.Value) / VAR_DOTIMP) + 8; //Width;
  if VarName = 'VARF_DIAL' then Value := QrBancosIDia.Value / VAR_DOTIMP - RecuoL; //Left;
  if VarName = 'VARF_DIAF' then Value := QrBancosFDia.Value; //Font;
  //
  if VarName = 'VARF_MESH' then Value := QrBancosFMes.Value + 8;/// VAR_DOTIMP; //Height;
  if VarName = 'VARF_MEST' then Value := (QrBancosTMes.Value / VAR_DOTIMP) -
     (QrBancosFMes.Value + 8) - RecuoT; //Top;
  if VarName = 'VARF_MESW' then Value := ((QrBancosWMes.Value -
     QrBancosIMes.Value) / VAR_DOTIMP) + 8; //Width;
  if VarName = 'VARF_MESL' then Value := QrBancosIMes.Value / VAR_DOTIMP - RecuoL; //Left;
  if VarName = 'VARF_MESF' then Value := QrBancosFMes.Value; //Font;
  //
  if VarName = 'VARF_ANOH' then Value := QrBancosFAno.Value + 8;/// VAR_DOTIMP; //Height;
  if VarName = 'VARF_ANOT' then Value := (QrBancosTAno.Value / VAR_DOTIMP) -
     (QrBancosFAno.Value + 8) - RecuoT; //Top;
  if VarName = 'VARF_ANOW' then Value := ((QrBancosWAno.Value -
     QrBancosIAno.Value) / VAR_DOTIMP) + 8; //Width;
  if VarName = 'VARF_ANOL' then Value := QrBancosIAno.Value / VAR_DOTIMP - RecuoL; //Left;
  if VarName = 'VARF_ANOF' then Value := QrBancosFAno.Value; //Font;
  //
  if VarName = 'VARF_VENCTOH' then Value := QrBancosFVencto.Value + 8;/// VAR_DOTIMP; //Height;
  if VarName = 'VARF_VENCTOT' then Value := (QrBancosTVencto.Value / VAR_DOTIMP) -
     (QrBancosFVencto.Value + 8) - RecuoT; //Top;
  if VarName = 'VARF_VENCTOW' then Value := ((QrBancosWVencto.Value -
     QrBancosIVencto.Value) / VAR_DOTIMP) + 8; //Width;
  if VarName = 'VARF_VENCTOL' then Value := QrBancosIVencto.Value / VAR_DOTIMP - RecuoL; //Left;
  if VarName = 'VARF_VENCTOF' then Value := QrBancosFVencto.Value; //Font;
  //
end;

end.

