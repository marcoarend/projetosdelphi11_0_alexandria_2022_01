object FmChequeCadNew: TFmChequeCadNew
  Left = 227
  Top = 152
  Caption = 'Edi'#231#227'o de Configura'#231#227'o de Cheque'
  ClientHeight = 514
  ClientWidth = 629
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 629
    Height = 418
    Align = alClient
    TabOrder = 0
    object Bevel1: TBevel
      Left = 116
      Top = 56
      Width = 241
      Height = 341
    end
    object Label1: TLabel
      Left = 12
      Top = 8
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 80
      Top = 8
      Width = 184
      Height = 13
      Caption = 'Nome do Banco (e outras descri'#231#245'es)::'
    end
    object Label3: TLabel
      Left = 12
      Top = 80
      Width = 100
      Height = 21
      AutoSize = False
      Caption = 'Valor'
      FocusControl = EdTValorNU
    end
    object Label39: TLabel
      Left = 12
      Top = 116
      Width = 100
      Height = 21
      AutoSize = False
      Caption = 'Texto valor linha 1'
      FocusControl = EdTValorNU
    end
    object Label4: TLabel
      Left = 12
      Top = 152
      Width = 100
      Height = 21
      AutoSize = False
      Caption = 'Texto valor linha 2'
      FocusControl = EdTValorNU
    end
    object Label5: TLabel
      Left = 12
      Top = 188
      Width = 100
      Height = 21
      AutoSize = False
      Caption = 'Favorecido'
      FocusControl = EdTValorNU
    end
    object Label6: TLabel
      Left = 12
      Top = 224
      Width = 100
      Height = 21
      AutoSize = False
      Caption = 'Cidade'
      FocusControl = EdTValorNU
    end
    object Label12: TLabel
      Left = 12
      Top = 260
      Width = 100
      Height = 21
      AutoSize = False
      Caption = 'Dia'
      FocusControl = EdTValorNU
    end
    object Label13: TLabel
      Left = 12
      Top = 296
      Width = 100
      Height = 21
      AutoSize = False
      Caption = 'M'#234's'
      FocusControl = EdTValorNU
    end
    object Label14: TLabel
      Left = 12
      Top = 332
      Width = 100
      Height = 21
      AutoSize = False
      Caption = 'Ano'
      FocusControl = EdTValorNU
    end
    object Label15: TLabel
      Left = 12
      Top = 368
      Width = 100
      Height = 21
      AutoSize = False
      Caption = 'Bom para'
      FocusControl = EdTValorNU
    end
    object Label7: TLabel
      Left = 124
      Top = 60
      Width = 28
      Height = 13
      Caption = 'Topo:'
    end
    object Label8: TLabel
      Left = 204
      Top = 60
      Width = 28
      Height = 13
      Caption = 'Inicio:'
    end
    object Label9: TLabel
      Left = 284
      Top = 60
      Width = 19
      Height = 13
      Caption = 'Fim:'
    end
    object Label10: TLabel
      Left = 368
      Top = 60
      Width = 30
      Height = 13
      Caption = 'Fonte:'
    end
    object Label11: TLabel
      Left = 348
      Top = 8
      Width = 56
      Height = 13
      Caption = 'Altura folha:'
    end
    object Label16: TLabel
      Left = 484
      Top = 8
      Width = 59
      Height = 13
      Caption = 'Recuo topo:'
    end
    object Label17: TLabel
      Left = 552
      Top = 8
      Width = 58
      Height = 13
      Caption = 'Recuo esq.:'
    end
    object Label18: TLabel
      Left = 416
      Top = 8
      Width = 65
      Height = 13
      Caption = 'Largura folha:'
    end
    object EdCodigo: TLMDEdit
      Left = 12
      Top = 24
      Width = 65
      Height = 21
      
      Caret.BlinkRate = 530
      Color = clBtnFace
      TabOrder = 0
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      ReadOnly = True
      Text = '0'
    end
    object EdNome: TEdit
      Left = 80
      Top = 24
      Width = 261
      Height = 21
      Color = clWhite
      MaxLength = 50
      TabOrder = 1
    end
    object EdTValorNU: TLMDEdit
      Left = 124
      Top = 80
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 5
      OnExit = EdTValorNUExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdIValorNU: TLMDEdit
      Left = 204
      Top = 80
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 6
      OnExit = EdIValorNUExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdWValorNU: TLMDEdit
      Left = 284
      Top = 80
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 7
      OnExit = EdWValorNUExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdWValorT1: TLMDEdit
      Left = 284
      Top = 116
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 10
      OnExit = EdWValorT1Exit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdIValorT1: TLMDEdit
      Left = 204
      Top = 116
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 9
      OnExit = EdIValorT1Exit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdTValorT1: TLMDEdit
      Left = 124
      Top = 116
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 8
      OnExit = EdTValorT1Exit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdTValorT2: TLMDEdit
      Left = 124
      Top = 152
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 11
      OnExit = EdTValorT2Exit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdIValorT2: TLMDEdit
      Left = 204
      Top = 152
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 12
      OnExit = EdIValorT2Exit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdWValorT2: TLMDEdit
      Left = 284
      Top = 152
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 13
      OnExit = EdWValorT2Exit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdWCliente: TLMDEdit
      Left = 284
      Top = 188
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 16
      OnExit = EdWClienteExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdICliente: TLMDEdit
      Left = 204
      Top = 188
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 15
      OnExit = EdIClienteExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdTCliente: TLMDEdit
      Left = 124
      Top = 188
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 14
      OnExit = EdTClienteExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdTCidade: TLMDEdit
      Left = 124
      Top = 224
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 17
      OnExit = EdTCidadeExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdICidade: TLMDEdit
      Left = 204
      Top = 224
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 18
      OnExit = EdICidadeExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdWCidade: TLMDEdit
      Left = 284
      Top = 224
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 19
      OnExit = EdWCidadeExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdWDia: TLMDEdit
      Left = 284
      Top = 260
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 22
      OnExit = EdWDiaExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdIDia: TLMDEdit
      Left = 204
      Top = 260
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 21
      OnExit = EdIDiaExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdTDia: TLMDEdit
      Left = 124
      Top = 260
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 20
      OnExit = EdTDiaExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdTMes: TLMDEdit
      Left = 124
      Top = 296
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 23
      OnExit = EdTMesExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdIMes: TLMDEdit
      Left = 204
      Top = 296
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 24
      OnExit = EdIMesExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdWMes: TLMDEdit
      Left = 284
      Top = 296
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 25
      OnExit = EdWMesExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdWAno: TLMDEdit
      Left = 284
      Top = 332
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 28
      OnExit = EdWAnoExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdIAno: TLMDEdit
      Left = 204
      Top = 332
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 27
      OnExit = EdIAnoExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdTAno: TLMDEdit
      Left = 124
      Top = 332
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 26
      OnExit = EdTAnoExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdTVencto: TLMDEdit
      Left = 124
      Top = 368
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 29
      OnExit = EdTVenctoExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdIVencto: TLMDEdit
      Left = 204
      Top = 368
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 30
      OnExit = EdIVenctoExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdWVencto: TLMDEdit
      Left = 284
      Top = 368
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 31
      OnExit = EdWVenctoExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object RGValorNU: TRadioGroup
      Left = 368
      Top = 75
      Width = 250
      Height = 33
      Columns = 4
      ItemIndex = 1
      Items.Strings = (
        '8'
        '10'
        '12'
        '14')
      TabOrder = 32
    end
    object RGValorT1: TRadioGroup
      Left = 368
      Top = 111
      Width = 250
      Height = 33
      Columns = 4
      ItemIndex = 1
      Items.Strings = (
        '8'
        '10'
        '12'
        '14')
      TabOrder = 33
    end
    object RGCliente: TRadioGroup
      Left = 368
      Top = 183
      Width = 250
      Height = 33
      Columns = 4
      ItemIndex = 1
      Items.Strings = (
        '8'
        '10'
        '12'
        '14')
      TabOrder = 35
    end
    object RGValorT2: TRadioGroup
      Left = 368
      Top = 147
      Width = 250
      Height = 33
      Columns = 4
      ItemIndex = 1
      Items.Strings = (
        '8'
        '10'
        '12'
        '14')
      TabOrder = 34
    end
    object RGDia: TRadioGroup
      Left = 368
      Top = 255
      Width = 250
      Height = 33
      Columns = 4
      ItemIndex = 1
      Items.Strings = (
        '8'
        '10'
        '12'
        '14')
      TabOrder = 37
    end
    object RGCidade: TRadioGroup
      Left = 368
      Top = 219
      Width = 250
      Height = 33
      Columns = 4
      ItemIndex = 1
      Items.Strings = (
        '8'
        '10'
        '12'
        '14')
      TabOrder = 36
    end
    object RGAno: TRadioGroup
      Left = 368
      Top = 327
      Width = 250
      Height = 33
      Columns = 4
      ItemIndex = 1
      Items.Strings = (
        '8'
        '10'
        '12'
        '14')
      TabOrder = 39
    end
    object RGMes: TRadioGroup
      Left = 368
      Top = 291
      Width = 250
      Height = 33
      Columns = 4
      ItemIndex = 1
      Items.Strings = (
        '8'
        '10'
        '12'
        '14')
      TabOrder = 38
    end
    object RGVencto: TRadioGroup
      Left = 368
      Top = 363
      Width = 250
      Height = 33
      Columns = 4
      ItemIndex = 1
      Items.Strings = (
        '8'
        '10'
        '12'
        '14')
      TabOrder = 40
    end
    object StaticText1: TStaticText
      Left = 116
      Top = 400
      Width = 241
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSingle
      Caption = 'Valores: mm x 10'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 41
    end
    object EdAltura: TLMDEdit
      Left = 348
      Top = 24
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 2
      OnExit = EdAlturaExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdRecuoT: TLMDEdit
      Left = 484
      Top = 24
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 3
      OnExit = EdRecuoTExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdRecuoL: TLMDEdit
      Left = 552
      Top = 24
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 4
      OnExit = EdRecuoLExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdLargura: TLMDEdit
      Left = 416
      Top = 24
      Width = 64
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 42
      OnExit = EdLarguraExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 466
    Width = 629
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Left = 14
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555555555555555555555555555555555555555FF5555555555555A655555
        55555555588FF55555555555AAA65555555555558888F55555555555AAA65555
        555555558888FF555555555AAAAA65555555555888888F55555555AAAAAA6555
        5555558888888FF5555552AA65AAA65555555888858888F555552A65555AA655
        55558885555888FF55555555555AAA65555555555558888F555555555555AA65
        555555555555888FF555555555555AA65555555555555888FF555555555555AA
        65555555555555888FF555555555555AA65555555555555888FF555555555555
        5AA6555555555555588855555555555555555555555555555555}
      NumGlyphs = 2
    end
    object BtDesiste: TBitBtn
      Left = 526
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
        3333333777333777FF3333993333339993333377FF3333377FF3399993333339
        993337777FF3333377F3393999333333993337F777FF333337FF993399933333
        399377F3777FF333377F993339993333399377F33777FF33377F993333999333
        399377F333777FF3377F993333399933399377F3333777FF377F993333339993
        399377FF3333777FF7733993333339993933373FF3333777F7F3399933333399
        99333773FF3333777733339993333339933333773FFFFFF77333333999999999
        3333333777333777333333333999993333333333377777333333}
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 629
    Height = 48
    Align = alTop
    Caption = 'Edi'#231#227'o de Configura'#231#227'o de Cheque'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 549
      Top = 2
      Width = 78
      Height = 44
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 557
    end
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 547
      Height = 44
      Align = alClient
      Transparent = True
      ExplicitWidth = 555
    end
  end
end
