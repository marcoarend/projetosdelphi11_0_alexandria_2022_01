unit ChequeCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral,  
   UnInternalConsts, UnMsgInt, UnInternalConsts2, UnGOTOy, UnMySQLCuringa,
     Grids, DBGrids, dbiProcs,
     
    Menus,   UMySQLModule,
   LMDCustomGroupBox,
  LMDCustomButtonGroup, LMDCustomRadioGroup, LMDRadioGroup, LMDDBRadioGroup,
  mySQLDbTables,  LMDPanelFill, LMDControl, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseLabel, LMDCustomLabel, LMDLabel;

type
  TFmChequeCad = class(TForm)
    PainelDados: TPanel;
    DsCheques: TDataSource;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PainelBotoes: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    QrCheques: TmySQLQuery;
    QrChequesCodigo: TIntegerField;
    QrChequesNome: TWideStringField;
    QrChequesTValorNU: TIntegerField;
    QrChequesTValorT1: TIntegerField;
    QrChequesTValorT2: TIntegerField;
    QrChequesTCliente: TIntegerField;
    QrChequesTCidade: TIntegerField;
    QrChequesTDia: TIntegerField;
    QrChequesTMes: TIntegerField;
    QrChequesTAno: TIntegerField;
    QrChequesTVencto: TIntegerField;
    QrChequesIValorNU: TIntegerField;
    QrChequesIValorT1: TIntegerField;
    QrChequesIValorT2: TIntegerField;
    QrChequesICliente: TIntegerField;
    QrChequesICidade: TIntegerField;
    QrChequesIDia: TIntegerField;
    QrChequesIMes: TIntegerField;
    QrChequesIAno: TIntegerField;
    QrChequesIVencto: TIntegerField;
    QrChequesWValorNU: TIntegerField;
    QrChequesWValorT1: TIntegerField;
    QrChequesWValorT2: TIntegerField;
    QrChequesWCliente: TIntegerField;
    QrChequesWCidade: TIntegerField;
    QrChequesWDia: TIntegerField;
    QrChequesWMes: TIntegerField;
    QrChequesWAno: TIntegerField;
    QrChequesWVencto: TIntegerField;
    QrChequesFValorNU: TIntegerField;
    QrChequesFValorT1: TIntegerField;
    QrChequesFValorT2: TIntegerField;
    QrChequesFCliente: TIntegerField;
    QrChequesFCidade: TIntegerField;
    QrChequesFDia: TIntegerField;
    QrChequesFMes: TIntegerField;
    QrChequesFAno: TIntegerField;
    QrChequesFVencto: TIntegerField;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label39: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Bevel1: TBevel;
    StaticText1: TStaticText;
    QrChequesAltura: TIntegerField;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    Label11: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    QrChequesRecuoT: TIntegerField;
    QrChequesRecuoL: TIntegerField;
    QrChequesDataCad: TDateField;
    QrChequesDataAlt: TDateField;
    QrChequesUserCad: TSmallintField;
    QrChequesUserAlt: TSmallintField;
    Label21: TLabel;
    DBEdit40: TDBEdit;
    QrChequesLargura: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    procedure CriaOForm;

   //Procedures do form
    procedure CriaChequeNew(Tipo: Integer);
    //
    function DefineFonte(Index: Integer): Integer;

  public
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ExcluiMovimento;

    { Public declarations }
  end;

var
  FmChequeCad: TFmChequeCad;
  Cs_ControleIts, Cs_ControleIts2: Double;

implementation

uses UnMyObjects, Module, ChequeCadNew;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmChequeCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmChequeCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrChequesCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////
procedure TFmChequeCad.DefParams;
begin
  VAR_GOTOTABELA := 'Cheques';
  VAR_GOTOMySQLTABLE := QrCheques;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := 'Codigo';
  VAR_GOTONOME := '';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;//1;
  //VAR_GOTOVAR1 := 'Empresa='+IntToStr(VAR_TERCEIRO);

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT * FROM cheques');
  //
  VAR_SQL1.Add('WHERE Codigo=:P0');
  //
  VAR_SQLa.Add('');
  //
end;

procedure TFmChequeCad.CriaOForm;
begin
  //if Screen.Width > 800 then WindowState := wsMaximized;
  DefParams;
  Va(vpLast);
end;
///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmChequeCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmChequeCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmChequeCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmChequeCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmChequeCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CONFIETIQ := QrChequesCodigo.Value;
  Close;
end;

procedure TFmChequeCad.FormCreate(Sender: TObject);
begin
  CriaOForm;
end;

procedure TFmChequeCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrChequesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmChequeCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmChequeCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmChequeCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmChequeCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrChequesCodigo.Value,
  CuringaLoc.CriaForm('Codigo', 'Descricao', 'Cheque', VAR_GOTOMySQLDBNAME, CO_VAZIO));
end;

procedure TFmChequeCad.BtExcluiClick(Sender: TObject);
begin
  ExcluiMovimento;
end;

procedure TFmChequeCad.ExcluiMovimento;
begin
end;

procedure TFmChequeCad.CriaChequeNew(Tipo: Integer);
var
  Codigo: Integer;
begin
  Application.CreateForm(TFmChequeCadNew, FmChequeCadNew);
  with FmChequeCadNew do
  begin
    if Tipo = 0 then
    begin
      LaTipo.Caption := CO_INCLUSAO;
      Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Cheques', 'Cheque', 'Codigo')
    end else begin
      LaTipo.Caption := CO_ALTERACAO;
      Codigo := QrChequesCodigo.Value;
      EdNome.Text := QrChequesNome.Value;
      EdAltura.Text := IntToStr(QrChequesAltura.Value);
      EdLargura.Text := IntToStr(QrChequesLargura.Value);
      EdRecuoT.Text := IntToStr(QrChequesRecuoT.Value);
      EdRecuoL.Text := IntToStr(QrChequesRecuoL.Value);
      //
      EdTValorNU.Text := IntToStr(QrChequesTValorNU.Value);
      EdTValorT1.Text := IntToStr(QrChequesTValorT1.Value);
      EdTValorT2.Text := IntToStr(QrChequesTValorT2.Value);
      EdTCliente.Text := IntToStr(QrChequesTCliente.Value);
      EdTCidade.Text := IntToStr(QrChequesTCidade.Value);
      EdTDia.Text := IntToStr(QrChequesTDia.Value);
      EdTMes.Text := IntToStr(QrChequesTMes.Value);
      EdTAno.Text := IntToStr(QrChequesTAno.Value);
      EdTVencto.Text := IntToStr(QrChequesTVencto.Value);
      //
      EdIValorNU.Text := IntToStr(QrChequesIValorNU.Value);
      EdIValorT1.Text := IntToStr(QrChequesIValorT1.Value);
      EdIValorT2.Text := IntToStr(QrChequesIValorT2.Value);
      EdICliente.Text := IntToStr(QrChequesICliente.Value);
      EdICidade.Text := IntToStr(QrChequesICidade.Value);
      EdIDia.Text := IntToStr(QrChequesIDia.Value);
      EdIMes.Text := IntToStr(QrChequesIMes.Value);
      EdIAno.Text := IntToStr(QrChequesIAno.Value);
      EdIVencto.Text := IntToStr(QrChequesIVencto.Value);
      //
      EdWValorNU.Text := IntToStr(QrChequesWValorNU.Value);
      EdWValorT1.Text := IntToStr(QrChequesWValorT1.Value);
      EdWValorT2.Text := IntToStr(QrChequesWValorT2.Value);
      EdWCliente.Text := IntToStr(QrChequesWCliente.Value);
      EdWCidade.Text := IntToStr(QrChequesWCidade.Value);
      EdWDia.Text := IntToStr(QrChequesWDia.Value);
      EdWMes.Text := IntToStr(QrChequesWMes.Value);
      EdWAno.Text := IntToStr(QrChequesWAno.Value);
      EdWVencto.Text := IntToStr(QrChequesWVencto.Value);
      //
      RGValorNU.ItemIndex := DefineFonte(QrChequesFValorNU.Value);
      RGValorT1.ItemIndex := DefineFonte(QrChequesFValorT1.Value);
      RGValorT2.ItemIndex := DefineFonte(QrChequesFValorT2.Value);
      RGCliente.ItemIndex := DefineFonte(QrChequesFCliente.Value);
      RGCidade.ItemIndex := DefineFonte(QrChequesFCidade.Value);
      RGDia.ItemIndex := DefineFonte(QrChequesFDia.Value);
      RGMes.ItemIndex := DefineFonte(QrChequesFMes.Value);
      RGAno.ItemIndex := DefineFonte(QrChequesFAno.Value);
      RGVencto.ItemIndex := DefineFonte(QrChequesFVencto.Value);
      //
    end;
    EdCodigo.Text := IntToStr(Codigo);
    ShowModal;
    Destroy;
  end;
  DefParams;
  LocCod(Codigo, Codigo);
end;

procedure TFmChequeCad.BtIncluiClick(Sender: TObject);
begin
  CriaChequeNew(0);
end;

procedure TFmChequeCad.BtAlteraClick(Sender: TObject);
begin
  CriaChequeNew(1);
end;

procedure TFmChequeCad.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 31);
end;

function TFmChequeCad.DefineFonte(Index: Integer): Integer;
begin
  case Index of
    08: Result := 0;
    10: Result := 1;
    12: Result := 2;
    14: Result := 3;
    else Result := 1;
  end;
end;

end.

