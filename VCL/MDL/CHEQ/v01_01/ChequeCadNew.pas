unit ChequeCadNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls,   UnInternalConsts,
      Buttons,
  DBCtrls, Db, (*DBTables,*) UnMLAGeral, UnGOTOY, UMySQLModule,
     Mask, dmkGeral, LMDCustomControl, LMDCustomPanel, LMDCustomBevelPanel,
  LMDBaseEdit, LMDCustomEdit, LMDEdit;

type
  TFmChequeCadNew = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label1: TLabel;
    EdCodigo: TLMDEdit;
    Label2: TLabel;
    EdNome: TEdit;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Label3: TLabel;
    Label39: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    EdTValorNU: TLMDEdit;
    EdIValorNU: TLMDEdit;
    EdWValorNU: TLMDEdit;
    EdWValorT1: TLMDEdit;
    EdIValorT1: TLMDEdit;
    EdTValorT1: TLMDEdit;
    EdTValorT2: TLMDEdit;
    EdIValorT2: TLMDEdit;
    EdWValorT2: TLMDEdit;
    EdWCliente: TLMDEdit;
    EdICliente: TLMDEdit;
    EdTCliente: TLMDEdit;
    EdTCidade: TLMDEdit;
    EdICidade: TLMDEdit;
    EdWCidade: TLMDEdit;
    EdWDia: TLMDEdit;
    EdIDia: TLMDEdit;
    EdTDia: TLMDEdit;
    EdTMes: TLMDEdit;
    EdIMes: TLMDEdit;
    EdWMes: TLMDEdit;
    EdWAno: TLMDEdit;
    EdIAno: TLMDEdit;
    EdTAno: TLMDEdit;
    EdTVencto: TLMDEdit;
    EdIVencto: TLMDEdit;
    EdWVencto: TLMDEdit;
    RGValorNU: TRadioGroup;
    RGValorT1: TRadioGroup;
    RGCliente: TRadioGroup;
    RGValorT2: TRadioGroup;
    RGDia: TRadioGroup;
    RGCidade: TRadioGroup;
    RGAno: TRadioGroup;
    RGMes: TRadioGroup;
    RGVencto: TRadioGroup;
    Bevel1: TBevel;
    StaticText1: TStaticText;
    Label11: TLabel;
    EdAltura: TLMDEdit;
    Label16: TLabel;
    EdRecuoT: TLMDEdit;
    Label17: TLabel;
    EdRecuoL: TLMDEdit;
    Label18: TLabel;
    EdLargura: TLMDEdit;
    procedure FormActivate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdTValorNUExit(Sender: TObject);
    procedure EdIValorNUExit(Sender: TObject);
    procedure EdWValorNUExit(Sender: TObject);
    procedure EdTValorT1Exit(Sender: TObject);
    procedure EdIValorT1Exit(Sender: TObject);
    procedure EdWValorT1Exit(Sender: TObject);
    procedure EdTValorT2Exit(Sender: TObject);
    procedure EdIValorT2Exit(Sender: TObject);
    procedure EdWValorT2Exit(Sender: TObject);
    procedure EdTClienteExit(Sender: TObject);
    procedure EdIClienteExit(Sender: TObject);
    procedure EdWClienteExit(Sender: TObject);
    procedure EdTCidadeExit(Sender: TObject);
    procedure EdICidadeExit(Sender: TObject);
    procedure EdWCidadeExit(Sender: TObject);
    procedure EdTDiaExit(Sender: TObject);
    procedure EdIDiaExit(Sender: TObject);
    procedure EdWDiaExit(Sender: TObject);
    procedure EdTMesExit(Sender: TObject);
    procedure EdIMesExit(Sender: TObject);
    procedure EdWMesExit(Sender: TObject);
    procedure EdTAnoExit(Sender: TObject);
    procedure EdIAnoExit(Sender: TObject);
    procedure EdWAnoExit(Sender: TObject);
    procedure EdTVenctoExit(Sender: TObject);
    procedure EdIVenctoExit(Sender: TObject);
    procedure EdWVenctoExit(Sender: TObject);
    procedure EdAlturaExit(Sender: TObject);
    procedure EdRecuoTExit(Sender: TObject);
    procedure EdRecuoLExit(Sender: TObject);
    procedure EdLarguraExit(Sender: TObject);
  private
    { Private declarations }
    function DefineFonte(Index: Integer): Integer;
  public
    { Public declarations }
  end;

var
  FmChequeCadNew: TFmChequeCadNew;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmChequeCadNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmChequeCadNew.BtDesisteClick(Sender: TObject);
begin
  if LaTipo.Caption = CO_INCLUSAO then
  UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Cheques', Geral.IMV(EdCodigo.Text));
  Close;
end;

procedure TFmChequeCadNew.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := StrToInt(EdCodigo.Text);
  if Trim(EdNome.Text) = '' then begin
    ShowMessage('Informe a descri��o!');
    EdNome.SetFocus;
    Exit;
  end;
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO cheques SET Nome=:P0, ')
  else
    Dmod.QrUpdU.SQL.Add('UPDATE cheques SET Nome=:P0, ');
  //
  Dmod.QrUpdU.SQL.Add('TValorNU=:P01, TValorT1=:P02, TValorT2=:P03, ');
  Dmod.QrUpdU.SQL.Add('TCliente=:P04, TCidade =:P05, TDia    =:P06, ');
  Dmod.QrUpdU.SQL.Add('TMes    =:P07, TAno    =:P08, TVencto =:P09, ');
  //
  Dmod.QrUpdU.SQL.Add('IValorNU=:P10, IValorT1=:P11, IValorT2=:P12, ');
  Dmod.QrUpdU.SQL.Add('ICliente=:P13, ICidade =:P14, IDia    =:P15, ');
  Dmod.QrUpdU.SQL.Add('IMes    =:P16, IAno    =:P17, IVencto =:P18, ');
  //
  Dmod.QrUpdU.SQL.Add('WValorNU=:P19, WValorT1=:P20, WValorT2=:P21, ');
  Dmod.QrUpdU.SQL.Add('WCliente=:P22, WCidade =:P23, WDia    =:P24, ');
  Dmod.QrUpdU.SQL.Add('WMes    =:P25, WAno    =:P26, WVencto =:P27, ');
  //
  Dmod.QrUpdU.SQL.Add('FValorNU=:P28, FValorT1=:P29, FValorT2=:P30, ');
  Dmod.QrUpdU.SQL.Add('FCliente=:P31, FCidade =:P32, FDia    =:P33, ');
  Dmod.QrUpdU.SQL.Add('FMes    =:P34, FAno    =:P35, FVencto =:P36, ');
  //
  Dmod.QrUpdU.SQL.Add('Altura=:P37, Largura=:P38, RecuoT=:P39, RecuoL=:P40, ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz ')
  else
    Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py, Codigo=:Pz ');
  //
  Dmod.QrUpdU.Params[00].AsString  := EdNome.Text;
  //
  Dmod.QrUpdU.Params[01].AsInteger := Geral.IMV(EdTValorNU.Text);
  Dmod.QrUpdU.Params[02].AsInteger := Geral.IMV(EdTValorT1.Text);
  Dmod.QrUpdU.Params[03].AsInteger := Geral.IMV(EdTValorT2.Text);
  Dmod.QrUpdU.Params[04].AsInteger := Geral.IMV(EdTCliente.Text);
  Dmod.QrUpdU.Params[05].AsInteger := Geral.IMV(EdTCidade.Text);
  Dmod.QrUpdU.Params[06].AsInteger := Geral.IMV(EdTDia.Text);
  Dmod.QrUpdU.Params[07].AsInteger := Geral.IMV(EdTMes.Text);
  Dmod.QrUpdU.Params[08].AsInteger := Geral.IMV(EdTAno.Text);
  Dmod.QrUpdU.Params[09].AsInteger := Geral.IMV(EdTVencto.Text);
  //
  Dmod.QrUpdU.Params[10].AsInteger := Geral.IMV(EdIValorNU.Text);
  Dmod.QrUpdU.Params[11].AsInteger := Geral.IMV(EdIValorT1.Text);
  Dmod.QrUpdU.Params[12].AsInteger := Geral.IMV(EdIValorT2.Text);
  Dmod.QrUpdU.Params[13].AsInteger := Geral.IMV(EdICliente.Text);
  Dmod.QrUpdU.Params[14].AsInteger := Geral.IMV(EdICidade.Text);
  Dmod.QrUpdU.Params[15].AsInteger := Geral.IMV(EdIDia.Text);
  Dmod.QrUpdU.Params[16].AsInteger := Geral.IMV(EdIMes.Text);
  Dmod.QrUpdU.Params[17].AsInteger := Geral.IMV(EdIAno.Text);
  Dmod.QrUpdU.Params[18].AsInteger := Geral.IMV(EdIVencto.Text);
  //
  Dmod.QrUpdU.Params[19].AsInteger := Geral.IMV(EdWValorNU.Text);
  Dmod.QrUpdU.Params[20].AsInteger := Geral.IMV(EdWValorT1.Text);
  Dmod.QrUpdU.Params[21].AsInteger := Geral.IMV(EdWValorT2.Text);
  Dmod.QrUpdU.Params[22].AsInteger := Geral.IMV(EdWCliente.Text);
  Dmod.QrUpdU.Params[23].AsInteger := Geral.IMV(EdWCidade.Text);
  Dmod.QrUpdU.Params[24].AsInteger := Geral.IMV(EdWDia.Text);
  Dmod.QrUpdU.Params[25].AsInteger := Geral.IMV(EdWMes.Text);
  Dmod.QrUpdU.Params[26].AsInteger := Geral.IMV(EdWAno.Text);
  Dmod.QrUpdU.Params[27].AsInteger := Geral.IMV(EdWVencto.Text);
  //
  Dmod.QrUpdU.Params[28].AsInteger := DefineFonte(RGValorNU.ItemIndex);
  Dmod.QrUpdU.Params[29].AsInteger := DefineFonte(RGValorT1.ItemIndex);
  Dmod.QrUpdU.Params[30].AsInteger := DefineFonte(RGValorT2.ItemIndex);
  Dmod.QrUpdU.Params[31].AsInteger := DefineFonte(RGCliente.ItemIndex);
  Dmod.QrUpdU.Params[32].AsInteger := DefineFonte(RGCidade.ItemIndex);
  Dmod.QrUpdU.Params[33].AsInteger := DefineFonte(RGDia.ItemIndex);
  Dmod.QrUpdU.Params[34].AsInteger := DefineFonte(RGMes.ItemIndex);
  Dmod.QrUpdU.Params[35].AsInteger := DefineFonte(RGAno.ItemIndex);
  Dmod.QrUpdU.Params[36].AsInteger := DefineFonte(RGVencto.ItemIndex);
  //
  Dmod.QrUpdU.Params[37].AsInteger := Geral.IMV(EdAltura.Text);
  Dmod.QrUpdU.Params[38].AsInteger := Geral.IMV(EdLargura.Text);
  Dmod.QrUpdU.Params[39].AsInteger := Geral.IMV(EdRecuoT.Text);
  Dmod.QrUpdU.Params[40].AsInteger := Geral.IMV(EdRecuoL.Text);
  //
  Dmod.QrUpdU.Params[41].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[42].AsInteger := VAR_USUARIO;
  //
  Dmod.QrUpdU.Params[43].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  Close;
end;

procedure TFmChequeCadNew.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

function TFmChequeCadNew.DefineFonte(Index: Integer): Integer;
begin
  case Index of
    0: Result := 08;
    1: Result := 10;
    2: Result := 12;
    3: Result := 14;
    else Result := 10;
  end;
end;

procedure TFmChequeCadNew.EdTValorNUExit(Sender: TObject);
begin
  EdTValorNU.Text := Geral.TFT(EdTValorNU.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdIValorNUExit(Sender: TObject);
begin
  EdIValorNU.Text := Geral.TFT(EdIValorNU.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdWValorNUExit(Sender: TObject);
begin
  EdWValorNU.Text := Geral.TFT(EdWValorNU.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdTValorT1Exit(Sender: TObject);
begin
  EdTValorT1.Text := Geral.TFT(EdTValorT1.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdIValorT1Exit(Sender: TObject);
begin
  EdIValorT1.Text := Geral.TFT(EdIValorT1.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdWValorT1Exit(Sender: TObject);
begin
  EdWValorT1.Text := Geral.TFT(EdWValorT1.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdTValorT2Exit(Sender: TObject);
begin
  EdTValorT2.Text := Geral.TFT(EdTValorT2.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdIValorT2Exit(Sender: TObject);
begin
  EdIValorT2.Text := Geral.TFT(EdIValorT2.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdWValorT2Exit(Sender: TObject);
begin
  EdWValorT2.Text := Geral.TFT(EdWValorT2.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdTClienteExit(Sender: TObject);
begin
  EdTCliente.Text := Geral.TFT(EdTCliente.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdIClienteExit(Sender: TObject);
begin
  EdICliente.Text := Geral.TFT(EdICliente.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdWClienteExit(Sender: TObject);
begin
  EdWCliente.Text := Geral.TFT(EdWCliente.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdTCidadeExit(Sender: TObject);
begin
  EdTCidade.Text := Geral.TFT(EdTCidade.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdICidadeExit(Sender: TObject);
begin
  EdiCidade.Text := Geral.TFT(EdICidade.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdWCidadeExit(Sender: TObject);
begin
  EdWCidade.Text := Geral.TFT(EdWCidade.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdTDiaExit(Sender: TObject);
begin
  EdTDia.Text := Geral.TFT(EdTDia.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdIDiaExit(Sender: TObject);
begin
  EdIDia.Text := Geral.TFT(EdIDia.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdWDiaExit(Sender: TObject);
begin
  EdWDia.Text := Geral.TFT(EdWDia.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdTMesExit(Sender: TObject);
begin
  EdTMes.Text := Geral.TFT(EdTMes.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdIMesExit(Sender: TObject);
begin
  EdIMes.Text := Geral.TFT(EdIMes.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdWMesExit(Sender: TObject);
begin
  EdWMes.Text := Geral.TFT(EdWMes.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdTAnoExit(Sender: TObject);
begin
  EdTAno.Text := Geral.TFT(EdTAno.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdIAnoExit(Sender: TObject);
begin
  EdIMes.Text := Geral.TFT(EdIMes.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdWAnoExit(Sender: TObject);
begin
  EdWMes.Text := Geral.TFT(EdWMes.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdTVenctoExit(Sender: TObject);
begin
  EdTVencto.Text := Geral.TFT(EdTVencto.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdIVenctoExit(Sender: TObject);
begin
  EdIVencto.Text := Geral.TFT(EdIVencto.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdWVenctoExit(Sender: TObject);
begin
  EdWVencto.Text := Geral.TFT(EdWVencto.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdAlturaExit(Sender: TObject);
begin
  EdAltura.Text := Geral.TFT(EdAltura.Text, 0, siPositivo);
end;

procedure TFmChequeCadNew.EdRecuoTExit(Sender: TObject);
begin
  EdRecuoT.Text := Geral.TFT(EdRecuoT.Text, 0, siNegativo);
end;

procedure TFmChequeCadNew.EdRecuoLExit(Sender: TObject);
begin
  EdRecuoL.Text := Geral.TFT(EdRecuoL.Text, 0, siNegativo);
end;

procedure TFmChequeCadNew.EdLarguraExit(Sender: TObject);
begin
  EdLargura.Text := Geral.TFT(EdLargura.Text, 0, siPositivo);
end;

end.
