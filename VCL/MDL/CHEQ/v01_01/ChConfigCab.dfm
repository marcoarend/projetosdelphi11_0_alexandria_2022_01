object FmChConfigCab: TFmChConfigCab
  Left = 336
  Top = 183
  Caption = 'CHS-CONFG-001 :: Configura'#231#245'es de Impress'#227'o Matricial de Cheques'
  ClientHeight = 534
  ClientWidth = 898
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 898
    Height = 438
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object Label9: TLabel
      Left = 16
      Top = 4
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label10: TLabel
      Left = 120
      Top = 4
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label6: TLabel
      Left = 584
      Top = 4
      Width = 30
      Height = 13
      Caption = 'Altura:'
    end
    object Label3: TLabel
      Left = 652
      Top = 4
      Width = 54
      Height = 13
      Caption = 'Cabe'#231'alho:'
    end
    object Label11: TLabel
      Left = 516
      Top = 4
      Width = 54
      Height = 13
      Caption = 'Marg. Esq.:'
    end
    object EdCodigo: TdmkEdit
      Left = 16
      Top = 20
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 2
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TEdit
      Left = 120
      Top = 20
      Width = 393
      Height = 21
      MaxLength = 20
      TabOrder = 1
    end
    object EdAltura: TdmkEdit
      Left = 584
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdTopoIni: TdmkEdit
      Left = 652
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object RGCPI: TRadioGroup
      Left = 720
      Top = 8
      Width = 77
      Height = 33
      Caption = ' CPI: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        '10'
        '12')
      TabOrder = 5
    end
    object EdMEsq: TdmkEdit
      Left = 516
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 368
      Width = 898
      Height = 70
      Align = alBottom
      TabOrder = 6
      object PnSaiDesis: TPanel
        Left = 752
        Top = 15
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn1: TBitBtn
          Tag = 13
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 750
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 898
    Height = 438
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 898
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 120
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 584
        Top = 4
        Width = 30
        Height = 13
        Caption = 'Altura:'
        FocusControl = DBEdAltura
      end
      object Label5: TLabel
        Left = 652
        Top = 4
        Width = 54
        Height = 13
        Caption = 'Cabe'#231'alho:'
        FocusControl = DBEdTopoIni
      end
      object Label7: TLabel
        Left = 720
        Top = 4
        Width = 20
        Height = 13
        Caption = 'CPI:'
        FocusControl = DBEdit1
      end
      object Label8: TLabel
        Left = 516
        Top = 4
        Width = 54
        Height = 13
        Caption = 'Marg. Esq.:'
        FocusControl = DBEdMEsq
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 20
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsChConfCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        MaxLength = 3
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 120
        Top = 20
        Width = 393
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsChConfCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdAltura: TDBEdit
        Left = 584
        Top = 20
        Width = 64
        Height = 21
        DataField = 'Altura'
        DataSource = DsChConfCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object DBEdTopoIni: TDBEdit
        Left = 652
        Top = 20
        Width = 64
        Height = 21
        DataField = 'TopoIni'
        DataSource = DsChConfCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
      object DBEdit1: TDBEdit
        Left = 720
        Top = 20
        Width = 64
        Height = 21
        DataField = 'CPI'
        DataSource = DsChConfCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
      end
      object DBEdMEsq: TDBEdit
        Left = 516
        Top = 20
        Width = 64
        Height = 21
        DataField = 'MEsq'
        DataSource = DsChConfCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 128
      Width = 790
      Height = 110
      DataSource = DsChConfVal
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = DBGrid1DrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Campo'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Topo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MEsq'
          Title.Caption = 'M.Esq.'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Altu'
          Title.Caption = 'Altura'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Larg'
          Title.Caption = 'Largura'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFTAM'
          Title.Caption = 'Fonte'
          Width = 81
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEALIN'
          Title.Caption = 'Alinhamento'
          Width = 121
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Negr'
          Title.Caption = 'N'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ital'
          Title.Caption = 'It'#225'lico'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsItalic]
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Padr'
          Title.Caption = 'Padr'#227'o'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pref'
          Title.Caption = 'Prefixo'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrEs'
          Title.Caption = 'Espa'#231'os'
          Width = 46
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sufi'
          Title.Caption = 'Sufixo'
          Width = 68
          Visible = True
        end>
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 374
      Width = 898
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 304
        Top = 15
        Width = 592
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel6: TPanel
          Left = 459
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 898
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 850
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 634
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 611
        Height = 32
        Caption = 'Configura'#231#245'es de Impress'#227'o Matricial de Cheques'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 611
        Height = 32
        Caption = 'Configura'#231#245'es de Impress'#227'o Matricial de Cheques'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 611
        Height = 32
        Caption = 'Configura'#231#245'es de Impress'#227'o Matricial de Cheques'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 898
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 894
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsChConfCab: TDataSource
    DataSet = QrChConfCab
    Left = 320
    Top = 65
  end
  object QrChConfCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrChConfCabBeforeOpen
    AfterOpen = QrChConfCabAfterOpen
    AfterScroll = QrChConfCabAfterScroll
    SQL.Strings = (
      'SELECT * FROM chconfcab')
    Left = 292
    Top = 65
    object QrChConfCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrChConfCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrChConfCabAltura: TIntegerField
      FieldName = 'Altura'
    end
    object QrChConfCabTopoIni: TIntegerField
      FieldName = 'TopoIni'
    end
    object QrChConfCabCPI: TIntegerField
      FieldName = 'CPI'
    end
    object QrChConfCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrChConfCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChConfCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChConfCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrChConfCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrChConfCabMEsq: TSmallintField
      FieldName = 'MEsq'
    end
  end
  object QrChConfVal: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrChConfValCalcFields
    SQL.Strings = (
      'SELECT its.*, val.*'
      'FROM chconfits its'
      'LEFT JOIN chconfval val ON val.Campo=its.Campo'
      'AND val.Codigo=:P0')
    Left = 156
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrChConfValCampo: TIntegerField
      FieldName = 'Campo'
      Required = True
    end
    object QrChConfValNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrChConfValCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrChConfValCampo_1: TIntegerField
      FieldName = 'Campo_1'
    end
    object QrChConfValTopo: TIntegerField
      FieldName = 'Topo'
    end
    object QrChConfValMEsq: TIntegerField
      FieldName = 'MEsq'
    end
    object QrChConfValAltu: TIntegerField
      FieldName = 'Altu'
    end
    object QrChConfValLarg: TIntegerField
      FieldName = 'Larg'
    end
    object QrChConfValFTam: TIntegerField
      FieldName = 'FTam'
    end
    object QrChConfValNegr: TIntegerField
      FieldName = 'Negr'
    end
    object QrChConfValItal: TIntegerField
      FieldName = 'Ital'
    end
    object QrChConfValLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrChConfValDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChConfValDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChConfValUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrChConfValUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrChConfValNOMEALIN: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEALIN'
      Size = 50
      Calculated = True
    end
    object QrChConfValNOMEFTAM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEFTAM'
      Calculated = True
    end
    object QrChConfValPref: TWideStringField
      FieldName = 'Pref'
      Size = 255
    end
    object QrChConfValSufi: TWideStringField
      FieldName = 'Sufi'
      Size = 255
    end
    object QrChConfValPadr: TWideStringField
      FieldName = 'Padr'
      Size = 255
    end
    object QrChConfValAliNOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'AliNOME'
      Size = 50
      Calculated = True
    end
    object QrChConfValAliV: TIntegerField
      FieldName = 'AliV'
    end
    object QrChConfValAliH: TIntegerField
      FieldName = 'AliH'
    end
    object QrChConfValTopR: TIntegerField
      FieldName = 'TopR'
    end
    object QrChConfValPrEs: TIntegerField
      FieldName = 'PrEs'
    end
  end
  object DsChConfVal: TDataSource
    DataSet = QrChConfVal
    Left = 184
    Top = 192
  end
  object PMAltera: TPopupMenu
    Left = 436
    Top = 260
    object Itematual1: TMenuItem
      Caption = '&Item selecionado'
      object Somenteatual1: TMenuItem
        Caption = '&Somente atual'
        OnClick = Somenteatual1Click
      end
      object Umaum1: TMenuItem
        Caption = '&Um a um'
        OnClick = Umaum1Click
      end
    end
    object NomedaConfiguraoatual1: TMenuItem
      Caption = '&Configura'#231#227'o atual'
      OnClick = NomedaConfiguraoatual1Click
    end
  end
  object QrChConfVal2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT its.*, val.*'
      'FROM chconfits its'
      'LEFT JOIN chconfval val ON val.Campo=its.Campo'
      'AND val.Codigo=:P0')
    Left = 212
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Campo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object IntegerField3: TIntegerField
      FieldName = 'Campo_1'
    end
    object IntegerField4: TIntegerField
      FieldName = 'Topo'
    end
    object IntegerField5: TIntegerField
      FieldName = 'MEsq'
    end
    object IntegerField6: TIntegerField
      FieldName = 'Altu'
    end
    object IntegerField7: TIntegerField
      FieldName = 'Larg'
    end
    object IntegerField8: TIntegerField
      FieldName = 'FTam'
    end
    object IntegerField10: TIntegerField
      FieldName = 'Negr'
    end
    object IntegerField11: TIntegerField
      FieldName = 'Ital'
    end
    object IntegerField12: TIntegerField
      FieldName = 'Lk'
    end
    object DateField1: TDateField
      FieldName = 'DataCad'
    end
    object DateField2: TDateField
      FieldName = 'DataAlt'
    end
    object IntegerField13: TIntegerField
      FieldName = 'UserCad'
    end
    object IntegerField14: TIntegerField
      FieldName = 'UserAlt'
    end
    object StringField2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEALIN'
      Size = 50
      Calculated = True
    end
    object QrChConfVal2Pref: TWideStringField
      FieldName = 'Pref'
      Size = 255
    end
    object QrChConfVal2Sufi: TWideStringField
      FieldName = 'Sufi'
      Size = 255
    end
    object QrChConfVal2Padr: TWideStringField
      FieldName = 'Padr'
      Size = 255
    end
    object QrChConfVal2AliV: TIntegerField
      FieldName = 'AliV'
    end
    object QrChConfVal2AliH: TIntegerField
      FieldName = 'AliH'
    end
  end
  object QrMEsq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Min(MEsq) Min'
      'FROM chconfval'
      'WHERE Codigo=:P0')
    Left = 292
    Top = 116
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMEsqMin: TIntegerField
      FieldName = 'Min'
      Required = True
    end
  end
  object PMInclui: TPopupMenu
    Left = 524
    Top = 404
    object Incluinovaconfiguraomanual1: TMenuItem
      Caption = '&Inclui nova configura'#231#227'o (manual)'
      OnClick = Incluinovaconfiguraomanual1Click
    end
    object Duplicaconfiguraoatual1: TMenuItem
      Caption = '&Duplica configura'#231#227'o atual'
      OnClick = Duplicaconfiguraoatual1Click
    end
  end
end
