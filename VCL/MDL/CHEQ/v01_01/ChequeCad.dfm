object FmChequeCad: TFmChequeCad
  Left = 207
  Top = 153
  Caption = 'Configura'#231#227'o de Folhas de Cheque'
  ClientHeight = 488
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 550
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label11: TLabel
    Left = 416
    Top = 8
    Width = 56
    Height = 13
    Caption = 'Altura folha:'
  end
  object Label16: TLabel
    Left = 484
    Top = 8
    Width = 59
    Height = 13
    Caption = 'Recuo topo:'
  end
  object Label17: TLabel
    Left = 552
    Top = 8
    Width = 58
    Height = 13
    Caption = 'Recuo esq.:'
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 392
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 390
      Align = alClient
      BevelOuter = bvNone
      Enabled = False
      ParentColor = True
      TabOrder = 0
      object Bevel1: TBevel
        Left = 116
        Top = 60
        Width = 241
        Height = 305
      end
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 36
        Height = 13
        Caption = 'Codigo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 92
        Top = 4
        Width = 31
        Height = 13
        Caption = 'Nome:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 12
        Top = 80
        Width = 100
        Height = 21
        AutoSize = False
        Caption = 'Valor'
        FocusControl = DBEdit1
      end
      object Label39: TLabel
        Left = 12
        Top = 112
        Width = 100
        Height = 21
        AutoSize = False
        Caption = 'Texto valor linha 1'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 12
        Top = 144
        Width = 100
        Height = 21
        AutoSize = False
        Caption = 'Texto valor linha 2'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 12
        Top = 176
        Width = 100
        Height = 21
        AutoSize = False
        Caption = 'Favorecido'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 12
        Top = 208
        Width = 100
        Height = 21
        AutoSize = False
        Caption = 'Cidade'
        FocusControl = DBEdit1
      end
      object Label12: TLabel
        Left = 12
        Top = 240
        Width = 100
        Height = 21
        AutoSize = False
        Caption = 'Dia'
        FocusControl = DBEdit1
      end
      object Label13: TLabel
        Left = 12
        Top = 272
        Width = 100
        Height = 21
        AutoSize = False
        Caption = 'M'#234's'
        FocusControl = DBEdit1
      end
      object Label14: TLabel
        Left = 12
        Top = 304
        Width = 100
        Height = 21
        AutoSize = False
        Caption = 'Ano'
        FocusControl = DBEdit1
      end
      object Label15: TLabel
        Left = 12
        Top = 336
        Width = 100
        Height = 21
        AutoSize = False
        Caption = 'Bom para'
        FocusControl = DBEdit1
      end
      object Label7: TLabel
        Left = 124
        Top = 64
        Width = 28
        Height = 13
        Caption = 'Topo:'
      end
      object Label8: TLabel
        Left = 204
        Top = 64
        Width = 28
        Height = 13
        Caption = 'Inicio:'
      end
      object Label9: TLabel
        Left = 284
        Top = 64
        Width = 19
        Height = 13
        Caption = 'Fim:'
      end
      object Label10: TLabel
        Left = 364
        Top = 64
        Width = 30
        Height = 13
        Caption = 'Fonte:'
      end
      object Label18: TLabel
        Left = 460
        Top = 4
        Width = 56
        Height = 13
        Caption = 'Altura folha:'
      end
      object Label19: TLabel
        Left = 596
        Top = 4
        Width = 59
        Height = 13
        Caption = 'Recuo topo:'
      end
      object Label20: TLabel
        Left = 664
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Recuo esq.:'
      end
      object Label21: TLabel
        Left = 528
        Top = 4
        Width = 65
        Height = 13
        Caption = 'Largura folha:'
      end
      object DBEdCodigo: TDBEdit
        Left = 12
        Top = 20
        Width = 76
        Height = 21
        TabStop = False
        Color = clWhite
        DataField = 'Codigo'
        DataSource = DsCheques
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        MaxLength = 3
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 92
        Top = 20
        Width = 365
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsCheques
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 124
        Top = 80
        Width = 64
        Height = 21
        DataField = 'TValorNU'
        DataSource = DsCheques
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 124
        Top = 112
        Width = 64
        Height = 21
        DataField = 'TValorT1'
        DataSource = DsCheques
        TabOrder = 5
      end
      object DBEdit3: TDBEdit
        Left = 124
        Top = 144
        Width = 64
        Height = 21
        DataField = 'TValorT2'
        DataSource = DsCheques
        TabOrder = 8
      end
      object DBEdit4: TDBEdit
        Left = 124
        Top = 176
        Width = 64
        Height = 21
        DataField = 'TCliente'
        DataSource = DsCheques
        TabOrder = 9
      end
      object DBEdit5: TDBEdit
        Left = 124
        Top = 208
        Width = 64
        Height = 21
        DataField = 'TCidade'
        DataSource = DsCheques
        TabOrder = 10
      end
      object DBEdit6: TDBEdit
        Left = 124
        Top = 240
        Width = 64
        Height = 21
        DataField = 'TDia'
        DataSource = DsCheques
        TabOrder = 11
      end
      object DBEdit7: TDBEdit
        Left = 124
        Top = 272
        Width = 64
        Height = 21
        DataField = 'TMes'
        DataSource = DsCheques
        TabOrder = 12
      end
      object DBEdit8: TDBEdit
        Left = 124
        Top = 304
        Width = 64
        Height = 21
        DataField = 'TAno'
        DataSource = DsCheques
        TabOrder = 13
      end
      object DBEdit9: TDBEdit
        Left = 124
        Top = 336
        Width = 64
        Height = 21
        DataField = 'TVencto'
        DataSource = DsCheques
        TabOrder = 14
      end
      object DBEdit10: TDBEdit
        Left = 204
        Top = 80
        Width = 64
        Height = 21
        DataField = 'IValorNU'
        DataSource = DsCheques
        TabOrder = 3
      end
      object DBEdit11: TDBEdit
        Left = 204
        Top = 112
        Width = 64
        Height = 21
        DataField = 'IValorT1'
        DataSource = DsCheques
        TabOrder = 6
      end
      object DBEdit12: TDBEdit
        Left = 204
        Top = 144
        Width = 64
        Height = 21
        DataField = 'IValorT2'
        DataSource = DsCheques
        TabOrder = 15
      end
      object DBEdit13: TDBEdit
        Left = 204
        Top = 176
        Width = 64
        Height = 21
        DataField = 'ICliente'
        DataSource = DsCheques
        TabOrder = 16
      end
      object DBEdit14: TDBEdit
        Left = 204
        Top = 208
        Width = 64
        Height = 21
        DataField = 'ICidade'
        DataSource = DsCheques
        TabOrder = 17
      end
      object DBEdit15: TDBEdit
        Left = 204
        Top = 240
        Width = 64
        Height = 21
        DataField = 'IDia'
        DataSource = DsCheques
        TabOrder = 18
      end
      object DBEdit16: TDBEdit
        Left = 204
        Top = 272
        Width = 64
        Height = 21
        DataField = 'IMes'
        DataSource = DsCheques
        TabOrder = 19
      end
      object DBEdit17: TDBEdit
        Left = 204
        Top = 304
        Width = 64
        Height = 21
        DataField = 'IAno'
        DataSource = DsCheques
        TabOrder = 20
      end
      object DBEdit18: TDBEdit
        Left = 204
        Top = 336
        Width = 64
        Height = 21
        DataField = 'IVencto'
        DataSource = DsCheques
        TabOrder = 21
      end
      object DBEdit19: TDBEdit
        Left = 284
        Top = 80
        Width = 64
        Height = 21
        DataField = 'WValorNU'
        DataSource = DsCheques
        TabOrder = 4
      end
      object DBEdit20: TDBEdit
        Left = 284
        Top = 112
        Width = 64
        Height = 21
        DataField = 'WValorT1'
        DataSource = DsCheques
        TabOrder = 7
      end
      object DBEdit21: TDBEdit
        Left = 284
        Top = 144
        Width = 64
        Height = 21
        DataField = 'WValorT2'
        DataSource = DsCheques
        TabOrder = 22
      end
      object DBEdit22: TDBEdit
        Left = 284
        Top = 176
        Width = 64
        Height = 21
        DataField = 'WCliente'
        DataSource = DsCheques
        TabOrder = 23
      end
      object DBEdit23: TDBEdit
        Left = 284
        Top = 208
        Width = 64
        Height = 21
        DataField = 'WCidade'
        DataSource = DsCheques
        TabOrder = 24
      end
      object DBEdit24: TDBEdit
        Left = 284
        Top = 240
        Width = 64
        Height = 21
        DataField = 'WDia'
        DataSource = DsCheques
        TabOrder = 25
      end
      object DBEdit25: TDBEdit
        Left = 284
        Top = 272
        Width = 64
        Height = 21
        DataField = 'WMes'
        DataSource = DsCheques
        TabOrder = 26
      end
      object DBEdit26: TDBEdit
        Left = 284
        Top = 304
        Width = 64
        Height = 21
        DataField = 'WAno'
        DataSource = DsCheques
        TabOrder = 27
      end
      object DBEdit27: TDBEdit
        Left = 284
        Top = 336
        Width = 64
        Height = 21
        DataField = 'WVencto'
        DataSource = DsCheques
        TabOrder = 28
      end
      object DBEdit28: TDBEdit
        Left = 364
        Top = 80
        Width = 64
        Height = 21
        DataField = 'FValorNU'
        DataSource = DsCheques
        TabOrder = 29
      end
      object DBEdit29: TDBEdit
        Left = 364
        Top = 112
        Width = 64
        Height = 21
        DataField = 'FValorT1'
        DataSource = DsCheques
        TabOrder = 30
      end
      object DBEdit30: TDBEdit
        Left = 364
        Top = 144
        Width = 64
        Height = 21
        DataField = 'FValorT2'
        DataSource = DsCheques
        TabOrder = 31
      end
      object DBEdit31: TDBEdit
        Left = 364
        Top = 176
        Width = 64
        Height = 21
        DataField = 'FCliente'
        DataSource = DsCheques
        TabOrder = 32
      end
      object DBEdit32: TDBEdit
        Left = 364
        Top = 208
        Width = 64
        Height = 21
        DataField = 'FCidade'
        DataSource = DsCheques
        TabOrder = 33
      end
      object DBEdit33: TDBEdit
        Left = 364
        Top = 240
        Width = 64
        Height = 21
        DataField = 'FDia'
        DataSource = DsCheques
        TabOrder = 34
      end
      object DBEdit34: TDBEdit
        Left = 364
        Top = 272
        Width = 64
        Height = 21
        DataField = 'FMes'
        DataSource = DsCheques
        TabOrder = 35
      end
      object DBEdit35: TDBEdit
        Left = 364
        Top = 304
        Width = 64
        Height = 21
        DataField = 'FAno'
        DataSource = DsCheques
        TabOrder = 36
      end
      object DBEdit36: TDBEdit
        Left = 364
        Top = 336
        Width = 64
        Height = 21
        DataField = 'FVencto'
        DataSource = DsCheques
        TabOrder = 37
      end
      object StaticText1: TStaticText
        Left = 116
        Top = 369
        Width = 241
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BorderStyle = sbsSingle
        Caption = 'Valores: mm x 10'
        TabOrder = 38
      end
      object DBEdit37: TDBEdit
        Left = 460
        Top = 20
        Width = 64
        Height = 21
        DataField = 'Altura'
        DataSource = DsCheques
        TabOrder = 39
      end
      object DBEdit38: TDBEdit
        Left = 596
        Top = 20
        Width = 64
        Height = 21
        DataField = 'RecuoT'
        DataSource = DsCheques
        TabOrder = 40
      end
      object DBEdit39: TDBEdit
        Left = 664
        Top = 20
        Width = 64
        Height = 21
        DataField = 'RecuoL'
        DataSource = DsCheques
        TabOrder = 41
      end
      object DBEdit40: TDBEdit
        Left = 528
        Top = 20
        Width = 64
        Height = 21
        DataField = 'Largura'
        DataSource = DsCheques
        TabOrder = 42
      end
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 440
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object LaRegistro: TLabel
      Left = 173
      Top = 1
      Width = 141
      Height = 46
      

      
      Align = alClient

      Caption = '[N]: 0'
      ExplicitWidth = 149
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 172
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object SpeedButton4: TBitBtn
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'ltimo registro'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333FF3333333333333447333333333333377FFF33333333333744473333333
          333337773FF3333333333444447333333333373F773FF3333333334444447333
          33333373F3773FF3333333744444447333333337F333773FF333333444444444
          733333373F3333773FF333334444444444733FFF7FFFFFFF77FF999999999999
          999977777777777733773333CCCCCCCCCC3333337333333F7733333CCCCCCCCC
          33333337F3333F773333333CCCCCCC3333333337333F7733333333CCCCCC3333
          333333733F77333333333CCCCC333333333337FF7733333333333CCC33333333
          33333777333333333333CC333333333333337733333333333333}
        NumGlyphs = 2
      end
      object SpeedButton3: TBitBtn
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Pr'#243'ximo registro'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333FF3333333333333003333
          3333333333773FF3333333333309003333333333337F773FF333333333099900
          33333FFFFF7F33773FF30000000999990033777777733333773F099999999999
          99007FFFFFFF33333F7700000009999900337777777F333F7733333333099900
          33333333337F3F77333333333309003333333333337F77333333333333003333
          3333333333773333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
      end
      object SpeedButton2: TBitBtn
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Registro anterior'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333FF3333333333333003333333333333F77F33333333333009033
          333333333F7737F333333333009990333333333F773337FFFFFF330099999000
          00003F773333377777770099999999999990773FF33333FFFFF7330099999000
          000033773FF33777777733330099903333333333773FF7F33333333333009033
          33333333337737F3333333333333003333333333333377333333333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
      end
      object SpeedButton1: TBitBtn
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Primeiro registro'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF3333333333333744333333333333F773333333333337
          44473333333333F777F3333333333744444333333333F7733733333333374444
          4433333333F77333733333333744444447333333F7733337F333333744444444
          433333F77333333733333744444444443333377FFFFFFF7FFFFF999999999999
          9999733777777777777333CCCCCCCCCC33333773FF333373F3333333CCCCCCCC
          C333333773FF3337F333333333CCCCCCC33333333773FF373F3333333333CCCC
          CC333333333773FF73F33333333333CCCCC3333333333773F7F3333333333333
          CCC333333333333777FF33333333333333CC3333333333333773}
        NumGlyphs = 2
      end
    end
    object Panel3: TPanel
      Left = 314
      Top = 1
      Width = 469
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      Caption = 'Panel3'
      ParentColor = True
      TabOrder = 1
      object BtSaida: TBitBtn
        Left = 372
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333303
          333333333333337FF3333333333333903333333333333377FF33333333333399
          03333FFFFFFFFF777FF3000000999999903377777777777777FF0FFFF0999999
          99037F3337777777777F0FFFF099999999907F3FF777777777770F00F0999999
          99037F773777777777730FFFF099999990337F3FF777777777330F00FFFFF099
          03337F773333377773330FFFFFFFF09033337F3FF3FFF77733330F00F0000003
          33337F773777777333330FFFF0FF033333337F3FF7F3733333330F08F0F03333
          33337F7737F7333333330FFFF003333333337FFFF77333333333000000333333
          3333777777333333333333333333333333333333333333333333}
        NumGlyphs = 2
      end
      object BtExclui: TBitBtn
        Left = 188
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Exclui banco atual'
        Caption = '&Exclui'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
          3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
          33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
          33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
          333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
          03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
          33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
          0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
          3333333337FFF7F3333333333000003333333333377777333333}
        NumGlyphs = 2
      end
      object BtAltera: TBitBtn
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera banco atual'
        Caption = '&Altera'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
      end
      object BtInclui: TBitBtn
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Caption = '&Inclui'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtIncluiClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
          07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
          0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
          33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = '                               Configura'#231#227'o de Folhas de Cheque'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 705
      Top = 1
      Width = 78
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 712
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 216
      Top = 1
      Width = 489
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 217
      ExplicitTop = 2
      ExplicitWidth = 495
      ExplicitHeight = 44
    end
    object PainelBotoes: TPanel
      Left = 1
      Top = 1
      Width = 215
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
          0003377777777777777308888888888888807F33333333333337088888888888
          88807FFFFFFFFFFFFFF7000000000000000077777777777777770F8F8F8F8F8F
          8F807F333333333333F708F8F8F8F8F8F9F07F333333333337370F8F8F8F8F8F
          8F807FFFFFFFFFFFFFF7000000000000000077777777777777773330FFFFFFFF
          03333337F3FFFF3F7F333330F0000F0F03333337F77773737F333330FFFFFFFF
          03333337F3FF3FFF7F333330F00F000003333337F773777773333330FFFF0FF0
          33333337F3F37F3733333330F08F0F0333333337F7337F7333333330FFFF0033
          33333337FFFF7733333333300000033333333337777773333333}
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
          0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
          33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333888003000003333
          300088F888883F333888E00BFBFB033333338883333F8F33333FE0BFBF000333
          330088F3338883F33388E0FBFBFBF033330088F3333FF8FFF388E0BFBF000000
          333388F3338888883F3FE0FBFBFBFBFB03CC88F33FFFFFFF8388E0BF00000000
          33CC88FF888888883388000BFB03333333338883FF83333333F3333000333334
          3343333888333338FF8F33333333334444443333333333888888333333333334
          3343333333333338F38F3333333333343343333333333338FF8F333333333344
          4444333333333388888833333333333433433333333333383383}
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333888003000003333
          300088F888883F333888E00BFBFB033333338883333F8F33333FE0BFBF000333
          330088F3338883F33388E0FBFBFBF033330088F3333FF8FFF388E0BFBF000000
          333388F3338888883F3FE0FBFBFBFBFB039988F33FFFFFFF8388E0BF00000000
          339988FF888888883388000BFB03333333338883FF8333333333333000333353
          3335333888333F8333F83333333333535535333333333F8F88F8333333333335
          33533333333333F83F8333333333333533533333333333F83F83333333333333
          553333333333333F8833333333333333553333333333333F8833}
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333FF3FF3333333333CC30003333333333773777333333333C33
          3000333FF33337F33777339933333C3333333377F33337F3333F339933333C33
          33003377333337F33377333333333C333300333F333337F33377339333333C33
          3333337FF3333733333F33993333C33333003377FF33733333773339933C3333
          330033377FF73F33337733339933C33333333FF377F373F3333F993399333C33
          330077F377F337F33377993399333C33330077FF773337F33377399993333C33
          33333777733337F333FF333333333C33300033333333373FF7773333333333CC
          3000333333333377377733333333333333333333333333333333}
        NumGlyphs = 2
      end
    end
  end
  object DsCheques: TDataSource
    AutoEdit = False
    DataSet = QrCheques
    Left = 548
    Top = 172
  end
  object QrCheques: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT * FROM cheques')
    Left = 517
    Top = 173
    object QrChequesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrChequesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrChequesTValorNU: TIntegerField
      FieldName = 'TValorNU'
    end
    object QrChequesTValorT1: TIntegerField
      FieldName = 'TValorT1'
    end
    object QrChequesTValorT2: TIntegerField
      FieldName = 'TValorT2'
    end
    object QrChequesTCliente: TIntegerField
      FieldName = 'TCliente'
    end
    object QrChequesTCidade: TIntegerField
      FieldName = 'TCidade'
    end
    object QrChequesTDia: TIntegerField
      FieldName = 'TDia'
    end
    object QrChequesTMes: TIntegerField
      FieldName = 'TMes'
    end
    object QrChequesTAno: TIntegerField
      FieldName = 'TAno'
    end
    object QrChequesTVencto: TIntegerField
      FieldName = 'TVencto'
    end
    object QrChequesIValorNU: TIntegerField
      FieldName = 'IValorNU'
    end
    object QrChequesIValorT1: TIntegerField
      FieldName = 'IValorT1'
    end
    object QrChequesIValorT2: TIntegerField
      FieldName = 'IValorT2'
    end
    object QrChequesICliente: TIntegerField
      FieldName = 'ICliente'
    end
    object QrChequesICidade: TIntegerField
      FieldName = 'ICidade'
    end
    object QrChequesIDia: TIntegerField
      FieldName = 'IDia'
    end
    object QrChequesIMes: TIntegerField
      FieldName = 'IMes'
    end
    object QrChequesIAno: TIntegerField
      FieldName = 'IAno'
    end
    object QrChequesIVencto: TIntegerField
      FieldName = 'IVencto'
    end
    object QrChequesWValorNU: TIntegerField
      FieldName = 'WValorNU'
    end
    object QrChequesWValorT1: TIntegerField
      FieldName = 'WValorT1'
    end
    object QrChequesWValorT2: TIntegerField
      FieldName = 'WValorT2'
    end
    object QrChequesWCliente: TIntegerField
      FieldName = 'WCliente'
    end
    object QrChequesWCidade: TIntegerField
      FieldName = 'WCidade'
    end
    object QrChequesWDia: TIntegerField
      FieldName = 'WDia'
    end
    object QrChequesWMes: TIntegerField
      FieldName = 'WMes'
    end
    object QrChequesWAno: TIntegerField
      FieldName = 'WAno'
    end
    object QrChequesWVencto: TIntegerField
      FieldName = 'WVencto'
    end
    object QrChequesFValorNU: TIntegerField
      FieldName = 'FValorNU'
    end
    object QrChequesFValorT1: TIntegerField
      FieldName = 'FValorT1'
    end
    object QrChequesFValorT2: TIntegerField
      FieldName = 'FValorT2'
    end
    object QrChequesFCliente: TIntegerField
      FieldName = 'FCliente'
    end
    object QrChequesFCidade: TIntegerField
      FieldName = 'FCidade'
    end
    object QrChequesFDia: TIntegerField
      FieldName = 'FDia'
    end
    object QrChequesFMes: TIntegerField
      FieldName = 'FMes'
    end
    object QrChequesFAno: TIntegerField
      FieldName = 'FAno'
    end
    object QrChequesFVencto: TIntegerField
      FieldName = 'FVencto'
    end
    object QrChequesAltura: TIntegerField
      FieldName = 'Altura'
      EditFormat = '0'
    end
    object QrChequesLargura: TIntegerField
      FieldName = 'Largura'
    end
    object QrChequesRecuoT: TIntegerField
      FieldName = 'RecuoT'
    end
    object QrChequesRecuoL: TIntegerField
      FieldName = 'RecuoL'
    end
    object QrChequesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChequesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChequesUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrChequesUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
  end
end
