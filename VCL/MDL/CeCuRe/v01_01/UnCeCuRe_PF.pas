unit UnCeCuRe_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, UnMsgInt, Db,
  DbCtrls, Buttons, ZCF2, mySQLDbTables, ComCtrls, Grids, DBGrids, CommCtrl,
  Consts, UnDmkProcFunc, Variants, MaskUtils, frxClass, frxPreview,
  mySQLExceptions,
  // Dermatek
  dmkGeral, dmkEdit, dmkDBEdit, dmkEditF7, dmkEditDateTimePicker, dmkCheckGroup,
  dmkDBLookupCombobox, dmkEditCB, dmkDBGrid, dmkDBGridDAC, dmkImage, UnDmkEnums;

type
  TUnCeCuRe_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CadastroDuplicado(Tabela: String; PagRec, Codigo, NivSup, Ordem:
             Integer; Avisa: Boolean): Boolean;
    function ObtemProximaOrdem(Tabela: String; PagRec, NivSup: Integer): Integer;
  end;

var
  CeCuRe_PF: TUnCeCuRe_PF;

implementation

uses MyDBCheck, Module, DmkDAC_PF, UMySQLModule, UnMyObjects,
  ModuleFin, UnFinanceiro;

{ TUnCeCuRe_PF }

function TUnCeCuRe_PF.CadastroDuplicado(Tabela: String; PagRec,
  Codigo, NivSup, Ordem: Integer; Avisa: Boolean): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + Lowercase(Tabela),
    'WHERE PagRec=' + Geral.FF0(PagRec),
    'AND NivSup=' + Geral.FF0(NivSup),
    'AND Ordem=' + Geral.FF0(Ordem),
    'AND Codigo=' + Geral.FF0(Codigo),
    '']);
    Result := Qry.RecordCount > 0;
    if Result and Avisa then
      Geral.MB_Aviso('Cadastro abortado!. Seria criado em duplicidade!' +
      sLineBreak + 'Altera a ordem!');
  finally
    Qry.Free;
  end;
end;

function TUnCeCuRe_PF.ObtemProximaOrdem(Tabela: String;
  PagRec, NivSup: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(Ordem) MaxOrdem ',
    'FROM ' + Lowercase(Tabela),
    'WHERE PagRec=' + Geral.FF0(PagRec),
    'AND NivSup=' + Geral.FF0(NivSup),
    '']);
    Result := Qry.FieldByName('MaxOrdem').AsInteger + 1;
    if Result = 0 then
      Result := 1;
  finally
    Qry.Free;
  end;
end;

end.
