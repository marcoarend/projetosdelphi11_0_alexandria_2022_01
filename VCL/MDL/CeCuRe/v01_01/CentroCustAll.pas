unit CentroCustAll;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, Db, mySQLDbTables,
  dmkPermissoes, dmkGeral, dmkImage, UnDMkEnums, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmCentroCustAll = class(TForm)
    DsCentroCust5: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    Splitter1: TSplitter;
    DsCentroCust4: TDataSource;
    DsCentroCust3: TDataSource;
    DsCentroCust2: TDataSource;
    DsCentroCusto: TDataSource;
    Panel3: TPanel;
    TbCentroCust5: TMySQLTable;
    TbCentroCust4: TMySQLTable;
    TbCentroCust3: TMySQLTable;
    TbCentroCust2: TMySQLTable;
    TbCentroCusto: TMySQLTable;
    CkOcultaCadSistema: TCheckBox;
    QrLoc: TmySQLQuery;
    DsExtra: TDataSource;
    TbCentroCust4NOME_PAI: TWideStringField;
    TbCentroCust3NOME_PAI: TWideStringField;
    TbCentroCust2NOME_PAI: TWideStringField;
    TbCentroCustoNOME_PAI: TWideStringField;
    PnExtra: TPanel;
    DBGExtra: TDBGrid;
    StaticText1: TStaticText;
    CkDragDrop: TCheckBox;
    mySQLUpdateSQL1: TmySQLUpdateSQL;
    QrTNiv5: TMySQLQuery;
    QrTNiv4: TMySQLQuery;
    QrTNiv3: TMySQLQuery;
    QrTNiv2: TMySQLQuery;
    QrTNiv5Codigo: TIntegerField;
    QrTNiv5Nome: TWideStringField;
    QrTNiv4Codigo: TIntegerField;
    QrTNiv4Nome: TWideStringField;
    QrTNiv3Codigo: TIntegerField;
    QrTNiv3Nome: TWideStringField;
    QrTNiv2Codigo: TIntegerField;
    QrTNiv2Nome: TWideStringField;
    Label1: TLabel;
    Label2: TLabel;
    TbCentroCustoAtivo: TSmallintField;
    Label3: TLabel;
    Label4: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    BtOK: TBitBtn;
    BtCancela: TBitBtn;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    SbNivel5: TBitBtn;
    SbNivel4: TBitBtn;
    SbNivel3: TBitBtn;
    SbNivel2: TBitBtn;
    SbNivel1: TBitBtn;
    Panel6: TPanel;
    DBGCentroCust5: TDBGrid;
    StaticText2: TStaticText;
    Panel7: TPanel;
    DBGCentroCust4: TDBGrid;
    StaticText3: TStaticText;
    Panel8: TPanel;
    DBGCentroCust3: TDBGrid;
    StaticText4: TStaticText;
    Panel9: TPanel;
    DBGCentroCust2: TDBGrid;
    StaticText5: TStaticText;
    Panel10: TPanel;
    DBGCentroCusto: TDBGrid;
    StaticText6: TStaticText;
    BtImpExp: TBitBtn;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Label5: TLabel;
    TbCentroCust5PagRec: TSmallintField;
    TbCentroCust5NivSup: TIntegerField;
    TbCentroCust5Ordem: TIntegerField;
    TbCentroCust5Codigo: TIntegerField;
    TbCentroCust5Nome: TWideStringField;
    TbCentroCust5Ativo: TSmallintField;
    TbCentroCust4Codigo: TIntegerField;
    TbCentroCust4Nome: TWideStringField;
    TbCentroCust4PagRec: TSmallintField;
    TbCentroCust4NivSup: TIntegerField;
    TbCentroCust4Ordem: TIntegerField;
    TbCentroCust4Ativo: TSmallintField;
    TbCentroCust3Codigo: TIntegerField;
    TbCentroCust3Nome: TWideStringField;
    TbCentroCust3PagRec: TSmallintField;
    TbCentroCust3NivSup: TIntegerField;
    TbCentroCust3Ordem: TIntegerField;
    TbCentroCust3Ativo: TSmallintField;
    TbCentroCust2Codigo: TIntegerField;
    TbCentroCust2Nome: TWideStringField;
    TbCentroCust2PagRec: TSmallintField;
    TbCentroCust2Ordem: TIntegerField;
    TbCentroCust2Ativo: TSmallintField;
    TbCentroCust2NivSup: TIntegerField;
    TbCentroCustoCodigo: TIntegerField;
    TbCentroCustoNome: TWideStringField;
    TbCentroCustoPagRec: TSmallintField;
    TbCentroCustoOrdem: TIntegerField;
    TbCentroCustoNivSup: TIntegerField;
    QrNNiv4: TMySQLQuery;
    QrNNiv4Codigo: TIntegerField;
    QrNNiv4Nome: TWideStringField;
    QrNNiv4NOME_PAI: TWideStringField;
    QrNNiv3: TMySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    QrNNiv3NOME_PAI: TWideStringField;
    QrNNiv2: TMySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    QrNNiv2NOME_PAI: TWideStringField;
    QrNNiv1: TMySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    QrNNiv1NOME_PAI: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbCentroCust5AfterScroll(DataSet: TDataSet);
    procedure TbCentroCust5BeforePost(DataSet: TDataSet);
    procedure TbCentroCust4BeforePost(DataSet: TDataSet);
    procedure TbCentroCust3BeforePost(DataSet: TDataSet);
    procedure TbCentroCust2BeforePost(DataSet: TDataSet);
    procedure TbCentroCustoBeforePost(DataSet: TDataSet);
    procedure TbCentroCust4AfterScroll(DataSet: TDataSet);
    procedure TbCentroCust3AfterScroll(DataSet: TDataSet);
    procedure TbCentroCust2AfterScroll(DataSet: TDataSet);
    procedure CkOcultaCadSistemaClick(Sender: TObject);
    procedure TbCentroCust4AfterEdit(DataSet: TDataSet);
    procedure TbCentroCust4AfterInsert(DataSet: TDataSet);
    procedure DBGExtraCellClick(Column: TColumn);
    procedure DBGCentroCust5DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DBGCentroCust5DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DBGCentroCust4CellClick(Column: TColumn);
    procedure DBGCentroCust3CellClick(Column: TColumn);
    procedure DBGCentroCust4DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DBGCentroCust4DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DBGCentroCust2CellClick(Column: TColumn);
    procedure DBGCentroCust3DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DBGCentroCust3DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DBGCentroCustoCellClick(Column: TColumn);
    procedure DBGCentroCust2DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DBGCentroCust2DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure TbCentroCust5BeforeInsert(DataSet: TDataSet);
    procedure TbCentroCust5AfterPost(DataSet: TDataSet);
    procedure TbCentroCust5AfterCancel(DataSet: TDataSet);
    procedure TbCentroCust4BeforeInsert(DataSet: TDataSet);
    procedure TbCentroCust4AfterPost(DataSet: TDataSet);
    procedure TbCentroCust4AfterCancel(DataSet: TDataSet);
    procedure TbCentroCust3BeforeInsert(DataSet: TDataSet);
    procedure TbCentroCust3AfterPost(DataSet: TDataSet);
    procedure TbCentroCust3AfterCancel(DataSet: TDataSet);
    procedure TbCentroCust2BeforeInsert(DataSet: TDataSet);
    procedure TbCentroCust2AfterPost(DataSet: TDataSet);
    procedure TbCentroCust2AfterCancel(DataSet: TDataSet);
    procedure DBGCentroCust4Enter(Sender: TObject);
    procedure DBGCentroCust5Enter(Sender: TObject);
    procedure DBGCentroCust3Enter(Sender: TObject);
    procedure DBGCentroCust2Enter(Sender: TObject);
    procedure DBGCentroCustoEnter(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure CkDragDropClick(Sender: TObject);
    procedure TbCentroCust5BeforeDelete(DataSet: TDataSet);
    procedure TbCentroCust4BeforeDelete(DataSet: TDataSet);
    procedure TbCentroCust3BeforeDelete(DataSet: TDataSet);
    procedure TbCentroCust2BeforeDelete(DataSet: TDataSet);
    procedure TbCentroCustoBeforeDelete(DataSet: TDataSet);
    procedure TbCentroCustoCalcFields(DataSet: TDataSet);
    procedure DBGCentroCustoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGCentroCustoColEnter(Sender: TObject);
    procedure DBGCentroCustoColExit(Sender: TObject);
    procedure TbCentroCust5CalcFields(DataSet: TDataSet);
    procedure TbCentroCust4CalcFields(DataSet: TDataSet);
    procedure TbCentroCust3CalcFields(DataSet: TDataSet);
    procedure TbCentroCust2CalcFields(DataSet: TDataSet);
    procedure DBGCentroCust5DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGCentroCust4DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGCentroCust3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGCentroCust2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGCentroCust5CellClick(Column: TColumn);
    procedure SbNivel5Click(Sender: TObject);
    procedure SbNivel4Click(Sender: TObject);
    procedure SbNivel3Click(Sender: TObject);
    procedure SbNivel2Click(Sender: TObject);
    procedure SbNivel1Click(Sender: TObject);
    procedure BtImpExpClick(Sender: TObject);
    procedure DBGCentroCust5DblClick(Sender: TObject);
    procedure DBGCentroCust4DblClick(Sender: TObject);
    procedure DBGCentroCust3DblClick(Sender: TObject);
    procedure DBGCentroCust2DblClick(Sender: TObject);
    procedure DBGCentroCustoDblClick(Sender: TObject);
  private
    { Private declarations }
    FInGrade: Boolean;
    FMySrc: TDBGrid;
    procedure RenomeiaTituloExtra(TitSon, TitPai: String);
    function AceitaDragOver(Source: TObject; Grade: String;
             Tabela: TmySQLQuery): Boolean;
    procedure VeSeAceitaDragDrop(Sender, Source: TObject; X, Y: Integer;
              TitPai, TitSon, Tabela, Campo: String);
    procedure MostraPlanos();
    procedure MostraConjuntos();
    procedure MostraGrupos();
    procedure MostraSubgrupos();
    procedure MostraContas();
  public
    { Public declarations }
  end;

  var
  FmCentroCustAll: TFmCentroCustAll;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, Principal, MyVCLSkin,
  MyDBCheck, UnFinanceiroJan, DmkDAC_PF, UnCeCuRe_Jan;

procedure TFmCentroCustAll.BtImpExpClick(Sender: TObject);
begin
  FinanceiroJan.MostraPlanoImpExp;
  //
  TbCentroCust5.Close;
  UnDmkDAC_PF.AbreTable(TbCentroCust5, Dmod.MyDB);
  //
  TbCentroCust4.Close;
  UnDmkDAC_PF.AbreTable(TbCentroCust4, Dmod.MyDB);
  //
  TbCentroCust3.Close;
  UnDmkDAC_PF.AbreTable(TbCentroCust3, Dmod.MyDB);
  //
  TbCentroCust2.Close;
  UnDmkDAC_PF.AbreTable(TbCentroCust2, Dmod.MyDB);
  //
  TbCentroCusto.Close;
  UnDmkDAC_PF.AbreTable(TbCentroCusto, Dmod.MyDB);
end;

procedure TFmCentroCustAll.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCentroCustAll.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCentroCustAll.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCentroCustAll.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FMySrc := nil;
  PnExtra.Visible := False;
  //ReopenPlano(0);
  UnDmkDAC_PF.AbreQuery(QrTNiv5, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTNiv4, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTNiv3, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTNiv2, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreTable(TbCentroCust5, Dmod.MyDB);
  UnDmkDAC_PF.AbreTable(TbCentroCust4, Dmod.MyDB);
  UnDmkDAC_PF.AbreTable(TbCentroCust3, Dmod.MyDB);
  UnDmkDAC_PF.AbreTable(TbCentroCust2, Dmod.MyDB);
  UnDmkDAC_PF.AbreTable(TbCentroCusto, Dmod.MyDB);
end;

(*procedure TFmPlano.ReopenPlano(Codigo: Integer);
begin
  QrPlano.Close;
  //QrPlano.Params[0].AsInteger := Qr?.Value;
  UnDmkDAC_PF.AbreQuery(QrPlano, Dmod.MyDB);
  //
  if Codigo <> 0 then QrPlano.Locate('Codigo', Codigo, []);
end;

procedure TFmPlano.ReopenConju(Codigo: Integer);
begin
  QrConju.Close;
  QrConju.Params[0].AsInteger := QrPlanoCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrConju, Dmod.MyDB);
  //
  if Codigo <> 0 then QrConju.Locate('Codigo', Codigo, []);
end;

procedure TFmPlano.ReopenGrupo(Codigo: Integer);
begin
  QrGrupo.Close;
  QrGrupo.Params[0].AsInteger := QrConjuCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGrupo, Dmod.MyDB);
  //
  if Codigo <> 0 then QrGrupo.Locate('Codigo', Codigo, []);
end;

procedure TFmPlano.ReopenSubGr(Codigo: Integer);
begin
  QrSubGr.Close;
  QrSubGr.Params[0].AsInteger := QrGrupoCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSubGr, Dmod.MyDB);
  //
  if Codigo <> 0 then QrSubGr.Locate('Codigo', Codigo, []);
end;

procedure TFmPlano.ReopenConta(Codigo: Integer);
begin
  QrConta.Close;
  QrConta.Params[0].AsInteger := QrSubgrCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrConta, Dmod.MyDB);
  //
  if Codigo <> 0 then QrConta.Locate('Codigo', Codigo, []);
end;*)

procedure TFmCentroCustAll.TbCentroCust5BeforePost(DataSet: TDataSet);
begin
  if (TbCentroCust5.State = dsInsert) and (TbCentroCust5Codigo.Value = 0) then
  begin
    TbCentroCust5Codigo.ReadOnly := False;
    TbCentroCust5Codigo.Value := UMyMod.BPGS1I32('centrocust5', 'Codigo', '', '', tsPos, stIns, 0);
    TbCentroCust5Codigo.ReadOnly := True;
  end;
end;

procedure TFmCentroCustAll.TbCentroCust4BeforePost(DataSet: TDataSet);
begin
  if (TbCentroCust4.State = dsInsert) then
  begin
    if TbCentroCust5Codigo.Value = 0 then
    begin
      Geral.MB_Aviso('Defina primeiro o n�vel superior!');
      TbCentroCust4.Cancel;
    end;
    if (TbCentroCust4Codigo.Value = 0) then
    begin
      TbCentroCust4Codigo.ReadOnly := False;
      TbCentroCust4Codigo.Value := UMyMod.BPGS1I32('centrocust4', 'Codigo', '', '', tsPos, stIns, 0);
      TbCentroCust4NivSup.Value := TbCentroCust5Codigo.Value;
      TbCentroCust4Codigo.ReadOnly := True;
    end;
  end else begin
    if TbCentroCust4Codigo.Value = 0 then TbCentroCust4NivSup.Value := 0;
    if UMyMod.SQLLoc1(Dmod.QrUpdU, 'centrocust5', 'Codigo', TbCentroCust4NivSup.Value, '',
    'O n�vel 5 n� ' + IntToStr(TbCentroCust4NivSup.Value) + ' n�o existe!') = 0 then
      TbCentroCust4.Cancel;
  end;
end;

procedure TFmCentroCustAll.TbCentroCust3BeforePost(DataSet: TDataSet);
begin
  if (TbCentroCust3.State = dsInsert) and (TbCentroCust3Codigo.Value = 0) then
  begin
    if TbCentroCust4Codigo.Value = 0 then
    begin
      Geral.MB_Aviso('Defina primeiro o n�vel superior!');
      TbCentroCust3.Cancel;
    end;
    TbCentroCust3Codigo.ReadOnly := False;
    TbCentroCust3Codigo.Value := UMyMod.BPGS1I32('centrocust3', 'Codigo', '', '', tsPos, stIns, 0);
    TbCentroCust3NivSup.Value := TbCentroCust4Codigo.Value;
    TbCentroCust3Codigo.ReadOnly := True;
  end;
end;

procedure TFmCentroCustAll.TbCentroCust2BeforePost(DataSet: TDataSet);
begin
  if (TbCentroCust2.State = dsInsert) and (TbCentroCust2Codigo.Value = 0) then
  begin
    if TbCentroCust3Codigo.Value = 0 then
    begin
      Geral.MB_Aviso('Defina primeiro o n�vel superior!');
      TbCentroCust2.Cancel;
    end;
    TbCentroCust2Codigo.ReadOnly := False;
    TbCentroCust2Codigo.Value := UMyMod.BPGS1I32('centrocust2', 'Codigo', '', '', tsPos, stIns, 0);
    TbCentroCust2NivSup.Value := TbCentroCust3Codigo.Value;
    TbCentroCust2Codigo.ReadOnly := True;
  end;
end;

procedure TFmCentroCustAll.TbCentroCustoBeforePost(DataSet: TDataSet);
begin
  if (TbCentroCusto.State = dsInsert) and (TbCentroCustoCodigo.Value = 0) then
  begin
    if TbCentroCust2Codigo.Value = 0 then
    begin
      Geral.MB_Aviso('Defina primeiro o n�vel superior!');
      TbCentroCusto.Cancel;
    end;
    TbCentroCustoCodigo.ReadOnly := False;
    TbCentroCustoCodigo.Value := UMyMod.BPGS1I32('centrocusto', 'Codigo', '', '', tsPos, stIns, 0);
    TbCentroCustoNivSup.Value := TbCentroCust2Codigo.Value;
    TbCentroCustoCodigo.ReadOnly := True;
  end;
end;

procedure TFmCentroCustAll.TbCentroCust5AfterScroll(DataSet: TDataSet);
begin
  TbCentroCust4.Filter := 'NivSup='+IntToStr(TbCentroCust5Codigo.Value);
  //
  QrNNiv4.Close;
  if TbCentroCust5.RecordCount > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrNNiv4, Dmod.MyDB, [
    'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI ',
    'FROM centrocust4 tb1 ',
    'LEFT JOIN centrocust5 tb2 ON tb2.Codigo=tb1.NivSup ',
    'WHERE tb1.Codigo>0 ',
    'AND tb1.NivSup NOT IN (0, ' + Geral.FF0(TbCentroCust5Codigo.Value) + ') ',
    'ORDER BY Nome ',
    '']);
  end;
end;

procedure TFmCentroCustAll.TbCentroCust4AfterScroll(DataSet: TDataSet);
begin
  QrNNiv3.Close;
  if TbCentroCust4.RecordCount > 0 then
  begin
    TbCentroCust3.Filter := 'NivSup='+IntToStr(TbCentroCust4Codigo.Value);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNNiv3, Dmod.MyDB, [
    'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI ',
    'FROM centrocust3 tb1 ',
    'LEFT JOIN centrocust4 tb2 ON tb2.Codigo=tb1.NivSup ',
    'WHERE tb1.Codigo>0 ',
    'AND tb1.NivSup NOT IN (0, ' + Geral.FF0(TbCentroCust4Codigo.Value) + ') ',
    'ORDER BY Nome ',
    '']);
  end else
    TbCentroCust3.Filter := 'NivSup=-1000';
end;

procedure TFmCentroCustAll.TbCentroCust3AfterScroll(DataSet: TDataSet);
begin
  QrNNiv2.Close;
  if TbCentroCust3.RecordCount > 0 then
  begin
    TbCentroCust2.Filter := 'NivSup='+IntToStr(TbCentroCust3Codigo.Value);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNNiv2, Dmod.MyDB, [
    'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI ',
    'FROM centrocust2 tb1 ',
    'LEFT JOIN centrocust3 tb2 ON tb2.Codigo=tb1.NivSup ',
    'WHERE tb1.Codigo>0 ',
    'AND tb1.NivSup NOT IN (0, ' + Geral.FF0(TbCentroCust3Codigo.Value) + ') ',
    'ORDER BY Nome ',
    '']);
  end else
    TbCentroCust2.Filter := 'NivSup=-1000';
end;

procedure TFmCentroCustAll.TbCentroCust2AfterScroll(DataSet: TDataSet);
begin
  QrNNiv1.Close;
  if TbCentroCust2.RecordCount > 0 then
  begin
    TbCentroCusto.Filter := 'NivSup='+IntToStr(TbCentroCust2Codigo.Value);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNNiv1, Dmod.MyDB, [
    'SELECT tb1.Codigo, tb1.Nome, tb1.Nome NOME_PAI ',
    'FROM centrocusto tb1 ',
    'LEFT JOIN centrocust2 tb2 ON tb2.Codigo=tb1.NivSup ',
    'WHERE tb1.Codigo>0 ',
    'AND tb1.NivSup NOT IN (0, ' + Geral.FF0(TbCentroCust2Codigo.Value) + ') ',
    'ORDER BY Nome ',
    '']);
  end else
    TbCentroCusto.Filter := 'NivSup=-1000';
end;

procedure TFmCentroCustAll.CkOcultaCadSistemaClick(Sender: TObject);
begin
  if CkOcultaCadSistema.Checked then
    TbCentroCust5.Filter := 'Codigo>0'
  else
    TbCentroCust5.Filter := '';
end;

procedure TFmCentroCustAll.TbCentroCust4AfterEdit(DataSet: TDataSet);
begin
  //if TbCentroCust4Codigo.Value = 0 then
    //TbCentroCust4.Cancel;
end;

procedure TFmCentroCustAll.TbCentroCust4AfterInsert(DataSet: TDataSet);
begin
  //if TbCentroCust5Codigo.Value <> 0 then
  //Abort;
end;

////////////////////////////////////////////////////////////////////////////////
//  Mandar do conjunto para o plano:
////////////////////////////////////////////////////////////////////////////////

procedure TFmCentroCustAll.DBGCentroCust4CellClick(Column: TColumn);
begin
  if Column.FieldName = '_S' then
  begin
{
    Screen.Cursor := crHourGlass;
    TbCentroCust4.Edit;
    if TbCentroCust4_S.Value = 1 then
      TbCentroCust4CtrlaSdo.Value := 0
    else
      TbCentroCust4CtrlaSdo.Value := 1;
    TbCentroCust4.Post;
    Screen.Cursor := crDefault;
}
  end else
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGCentroCust4.BeginDrag(True);
end;

procedure TFmCentroCustAll.DBGCentroCust5DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := AceitaDragOver(Source, 'DBGCentroCust4', QrNNiv4);
end;

procedure TFmCentroCustAll.DBGCentroCust5DblClick(Sender: TObject);
begin
  MostraPlanos();
end;

procedure TFmCentroCustAll.DBGCentroCust5DragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  gc : TGridCoord;
  PaiNome: string;
  SonCodi, PaiCodi: Integer;
begin
  VeSeAceitaDragDrop(Sender, Source, X, Y, 'plano', 'Conjunto', 'Conjuntos', 'Plano');
  Exit;
  gc := THackDBGrid(DBGCentroCust5).MouseCoord(X,Y);
  if (gc.X > 0) and (gc.Y > 0) then
  begin
    if TbCentroCust4.RecordCount = 0 then Exit;
    if TbCentroCust4Codigo.Value = 0 then Exit;
    PaiNome := TbCentroCust4NOME_PAI.Value;
    SonCodi := TbCentroCust4Codigo.Value;
    with THackDBGrid(DBGCentroCust5) do
    begin
      DataSource.DataSet.MoveBy (gc.Y - Row);
      PaiCodi := TbCentroCust5Codigo.Value;
      //
      if Geral.MB_Pergunta('Foi solicitada a altera��o conforme abaixo: '
      + sLineBreak + sLineBreak +
      'Conjunto: "' + TbCentroCust4Nome.Value + '" ' +
      sLineBreak + sLineBreak +
      'Do plano: "' + PaiNome + '" ' + sLineBreak +
      'Para o plano: "' + TbCentroCust5Nome.Value + '"' +
      sLineBreak + sLineBreak +
      'Confirma a altera��o?') = ID_YES then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'Conjuntos', False,
        ['Plano'], ['Codigo'], [PaiCodi], [SonCodi], True) then
        begin
          TbCentroCust5.Close;
          UnDmkDAC_PF.AbreTable(TbCentroCust5, Dmod.MyDB);
          TbCentroCust5.Locate('Codigo', PaiCodi, []);
          TbCentroCust4.Close;
          UnDmkDAC_PF.AbreTable(TbCentroCust4, Dmod.MyDB);
          TbCentroCust4.Locate('Codigo', SonCodi, []);
        end;
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//  Mandar do Grupo para o Conjunto:
////////////////////////////////////////////////////////////////////////////////

procedure TFmCentroCustAll.DBGCentroCust3CellClick(Column: TColumn);
begin
  if Column.FieldName = '_S' then
  begin
{
    Screen.Cursor := crHourGlass;
    TbCentroCust3.Edit;
    if TbCentroCust3_S.Value = 1 then
      TbCentroCust3CtrlaSdo.Value := 0
    else
      TbCentroCust3CtrlaSdo.Value := 1;
    TbCentroCust3.Post;
    Screen.Cursor := crDefault;
}
  end else
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGCentroCust3.BeginDrag(True);
end;

procedure TFmCentroCustAll.DBGCentroCust4DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := AceitaDragOver(Source, 'DBGCentroCust3', QrNNiv3);
  //Accept := (Source is TDBGrid) and (TDBGrid(Source).Name = 'DBGCentroCust3');
end;

procedure TFmCentroCustAll.DBGCentroCust4DblClick(Sender: TObject);
begin
  MostraConjuntos();
end;

procedure TFmCentroCustAll.DBGCentroCust4DragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  gc : TGridCoord;
  PaiNome: string;
  SonCodi, PaiCodi: Integer;
begin
  VeSeAceitaDragDrop(Sender, Source, X, Y, 'conjunto', 'Grupo', 'Grupos', 'Conjunto');
  Exit;
  gc := THackDBGrid(DBGCentroCust4).MouseCoord(X,Y);
  if (gc.X > 0) and (gc.Y > 0) then
  begin
    if TbCentroCust3.RecordCount = 0 then Exit;
    if TbCentroCust3Codigo.Value = 0 then Exit;
    PaiNome := TbCentroCust3NOME_PAI.Value;
    SonCodi := TbCentroCust3Codigo.Value;
    with THackDBGrid(DBGCentroCust4) do
    begin
      DataSource.DataSet.MoveBy (gc.Y - Row);
      PaiCodi := TbCentroCust4Codigo.Value;
      //
      if Geral.MB_Pergunta('Foi solicitada a altera��o conforme abaixo: '
      + sLineBreak + sLineBreak +
      'Grupo: "' + TbCentroCust3Nome.Value + '" ' +
      sLineBreak + sLineBreak +
      'Do conjunto: "' + PaiNome + '" ' + sLineBreak +
      'Para o conjunto: "' + TbCentroCust4Nome.Value + '"' +
      sLineBreak + sLineBreak +
      'Confirma a altera��o?') = ID_YES then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'Grupos', False,
        ['Conjunto'], ['Codigo'], [PaiCodi], [SonCodi], True) then
        begin
          TbCentroCust4.Close;
          UnDmkDAC_PF.AbreTable(TbCentroCust4, Dmod.MyDB);
          TbCentroCust4.Locate('Codigo', PaiCodi, []);
          TbCentroCust3.Close;
          UnDmkDAC_PF.AbreTable(TbCentroCust3, Dmod.MyDB);
          TbCentroCust3.Locate('Codigo', SonCodi, []);
        end;
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//  Mandar do Sub-grupo para o Grupo:
////////////////////////////////////////////////////////////////////////////////

procedure TFmCentroCustAll.DBGCentroCust2CellClick(Column: TColumn);
begin
  if Column.FieldName = '_S' then
  begin
(*
    Screen.Cursor := crHourGlass;
    TbCentroCust2.Edit;
    if TbCentroCust2_S.Value = 1 then
      TbCentroCust2CtrlaSdo.Value := 0
    else
      TbCentroCust2CtrlaSdo.Value := 1;
    TbCentroCust2.Post;
    Screen.Cursor := crDefault;
*)
  end else
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGCentroCust2.BeginDrag(True);
end;

procedure TFmCentroCustAll.DBGCentroCust3DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := AceitaDragOver(Source, 'DBGCentroCust2', QrNNiv2);
  //Accept := (Source is TDBGrid) and (TDBGrid(Source).Name = 'DBGCentroCust2');
end;

procedure TFmCentroCustAll.DBGCentroCust3DblClick(Sender: TObject);
begin
  MostraGrupos();
end;

procedure TFmCentroCustAll.DBGCentroCust3DragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  gc : TGridCoord;
  PaiNome: string;
  SonCodi, PaiCodi: Integer;
begin
  VeSeAceitaDragDrop(Sender, Source, X, Y, 'grupo', 'Sub-grupo', 'SubGrupos', 'Grupo');
  Exit;
  gc := THackDBGrid(DBGCentroCust3).MouseCoord(X,Y);
  if (gc.X > 0) and (gc.Y > 0) then
  begin
    if TbCentroCust2.RecordCount = 0 then Exit;
    if TbCentroCust2Codigo.Value = 0 then Exit;
    PaiNome := TbCentroCust2NOME_PAI.Value;
    SonCodi := TbCentroCust2Codigo.Value;
    with THackDBGrid(DBGCentroCust3) do
    begin
      DataSource.DataSet.MoveBy (gc.Y - Row);
      PaiCodi := TbCentroCust3Codigo.Value;
      //
      if Geral.MB_Pergunta('Foi solicitada a altera��o conforme abaixo: '
      + sLineBreak + sLineBreak +
      'Sub-grupo: "' + TbCentroCust2Nome.Value + '" ' +
      sLineBreak + sLineBreak +
      'Do grupo: "' + PaiNome + '" ' + sLineBreak +
      'Para o grupo: "' + TbCentroCust3Nome.Value + '"' +
      sLineBreak + sLineBreak +
      'Confirma a altera��o?') = ID_YES then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'SubGrupos', False,
        ['Grupo'], ['Codigo'], [PaiCodi], [SonCodi], True) then
        begin
          TbCentroCust3.Close;
          UnDmkDAC_PF.AbreTable(TbCentroCust3, Dmod.MyDB);
          TbCentroCust3.Locate('Codigo', PaiCodi, []);
          TbCentroCust2.Close;
          UnDmkDAC_PF.AbreTable(TbCentroCust2, Dmod.MyDB);
          TbCentroCust2.Locate('Codigo', SonCodi, []);
        end;
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//  Mandar da Conta para o Sub-grupo:
////////////////////////////////////////////////////////////////////////////////

procedure TFmCentroCustAll.DBGCentroCustoCellClick(Column: TColumn);
begin
(*
  if Column.FieldName = '_S' then
  begin
    Screen.Cursor := crHourGlass;
    TbCentroCusto.Edit;
    if TbCentroCusto_S.Value = 1 then
      TbCentroCustoCtrlaSdo.Value := 0
    else
      TbCentroCustoCtrlaSdo.Value := 1;
    TbCentroCusto.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_M' then
  begin
    Screen.Cursor := crHourGlass;
    TbCentroCusto.Edit;
    if TbCentroCusto_M.Value = 1 then
      TbCentroCustoMensal.Value := 'F'
    else
      TbCentroCustoMensal.Value := 'V';
    TbCentroCusto.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_C' then
  begin
    Screen.Cursor := crHourGlass;
    TbCentroCusto.Edit;
    if TbCentroCusto_C.Value = 1 then
      TbCentroCustoCredito.Value := 'F'
    else
      TbCentroCustoCredito.Value := 'V';
    TbCentroCusto.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_D' then
  begin
    Screen.Cursor := crHourGlass;
    TbCentroCusto.Edit;
    if TbCentroCusto_D.Value = 1 then
      TbCentroCustoDebito.Value := 'F'
    else
      TbCentroCustoDebito.Value := 'V';
    TbCentroCusto.Post;
    Screen.Cursor := crDefault;
  end else
*)
  if Column.FieldName = 'Ativo' then
  begin
    Screen.Cursor := crHourGlass;
    TbCentroCusto.Edit;
    if TbCentroCustoAtivo.Value = 1 then
      TbCentroCustoAtivo.Value := 0
    else
      TbCentroCustoAtivo.Value := 1;
    TbCentroCusto.Post;
    Screen.Cursor := crDefault;
  end else
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGCentroCusto.BeginDrag(True);
end;

procedure TFmCentroCustAll.DBGCentroCust2DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := AceitaDragOver(Source, 'DBGCentroCusto', QrNNiv1);
  //Accept := (Source is TDBGrid) and (TDBGrid(Source).Name = 'DBGCentroCusto');
end;

procedure TFmCentroCustAll.DBGCentroCust2DblClick(Sender: TObject);
begin
  MostraSubgrupos();
end;

procedure TFmCentroCustAll.DBGCentroCust2DragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  gc : TGridCoord;
  PaiNome: string;
  SonCodi, PaiCodi: Integer;
begin
  VeSeAceitaDragDrop(Sender, Source, X, Y, 'sub-grupo', 'Conta', 'contas', 'SubGrupo');
  Exit;
  gc := THackDBGrid(DBGCentroCust2).MouseCoord(X,Y);
  if (gc.X > 0) and (gc.Y > 0) then
  begin
    if TbCentroCusto.RecordCount = 0 then Exit;
    if TbCentroCustoCodigo.Value = 0 then Exit;
    PaiNome := TbCentroCustoNOME_PAI.Value;
    SonCodi := TbCentroCustoCodigo.Value;
    with THackDBGrid(DBGCentroCust2) do
    begin
      DataSource.DataSet.MoveBy (gc.Y - Row);
      PaiCodi := TbCentroCust2Codigo.Value;
      //
      if Geral.MB_Pergunta('Foi solicitada a altera��o conforme abaixo: '
      + sLineBreak + sLineBreak +
      'Conta: "' + TbCentroCustoNome.Value + '" ' +
      sLineBreak + sLineBreak +
      'Do Sub-grupo: "' + PaiNome + '" ' + sLineBreak +
      'Para o Sub-grupo: "' + TbCentroCust2Nome.Value + '"' +
      sLineBreak + sLineBreak +
      'Confirma a altera��o?') = ID_YES then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'Contaupos', False,
        ['Subgrupos'], ['Codigo'], [PaiCodi], [SonCodi], True) then
        begin
          TbCentroCust2.Close;
          UnDmkDAC_PF.AbreTable(TbCentroCust2, Dmod.MyDB);
          TbCentroCust2.Locate('Codigo', PaiCodi, []);
          TbCentroCusto.Close;
          UnDmkDAC_PF.AbreTable(TbCentroCusto, Dmod.MyDB);
          TbCentroCusto.Locate('Codigo', SonCodi, []);
        end;
      end;
    end;
  end;
end;

procedure TFmCentroCustAll.TbCentroCust5BeforeInsert(DataSet: TDataSet);
begin
  // Evitar exception
  //TbCentroCust4.Close;
end;

procedure TFmCentroCustAll.TbCentroCust5AfterPost(DataSet: TDataSet);
begin
  QrTNiv5.Close;
  UnDmkDAC_PF.AbreQuery(QrTNiv5, Dmod.MyDB);
  TbCentroCust4.Refresh;
  // Evitar exception
  //UnDmkDAC_PF.AbreQuery(TbCentroCust4, Dmod.MyDB);
end;

procedure TFmCentroCustAll.TbCentroCust5AfterCancel(DataSet: TDataSet);
begin
  // Evitar exception
  //TUnDmkDAC_PF.AbreQuery(bConju, Dmod.MyDB);
end;

procedure TFmCentroCustAll.TbCentroCust4BeforeInsert(DataSet: TDataSet);
begin
  // Evitar exception
  //UnDmkDAC_PF.AbreQuery(TbCentroCust3.Close;
end;

procedure TFmCentroCustAll.TbCentroCust4AfterPost(DataSet: TDataSet);
begin
  QrTNiv4.Close;
  UnDmkDAC_PF.AbreQuery(QrTNiv4, Dmod.MyDB);
  TbCentroCust3.Refresh;
  // Evitar exception
  //TUnDmkDAC_PF.AbreQuery(bGrupo, Dmod.MyDB);
end;

procedure TFmCentroCustAll.TbCentroCust4AfterCancel(DataSet: TDataSet);
begin
  // Evitar exception
  //TUnDmkDAC_PF.AbreQuery(bGrupo, Dmod.MyDB);
end;

procedure TFmCentroCustAll.TbCentroCust3BeforeInsert(DataSet: TDataSet);
begin
  // Evitar exception
  //TbCentroCust2.Close;
end;

procedure TFmCentroCustAll.TbCentroCust3AfterPost(DataSet: TDataSet);
begin
  QrTNiv3.Close;
  UnDmkDAC_PF.AbreQuery(QrTNiv3, Dmod.MyDB);
  TbCentroCust2.Refresh;
  // Evitar exception
  //UnDmkDAC_PF.AbreQuery(TbCentroCust2, Dmod.MyDB);
end;

procedure TFmCentroCustAll.TbCentroCust3AfterCancel(DataSet: TDataSet);
begin
  // Evitar exception
  //UnDmkDAC_PF.AbreQuery(TbCentroCust2, Dmod.MyDB);
end;

procedure TFmCentroCustAll.TbCentroCust2BeforeInsert(DataSet: TDataSet);
begin
  // Evitar exception
  //TbCentroCusto.Close;
end;

procedure TFmCentroCustAll.TbCentroCust2AfterPost(DataSet: TDataSet);
begin
  QrTNiv2.Close;
  UnDmkDAC_PF.AbreQuery(QrTNiv2, Dmod.MyDB);
  TbCentroCusto.Refresh;
  // Evitar exception
  //UnDmkDAC_PF.AbreQuery(TbCentroCusto, Dmod.MyDB);
end;

procedure TFmCentroCustAll.TbCentroCust2AfterCancel(DataSet: TDataSet);
begin
  // Evitar exception
  //UnDmkDAC_PF.AbreQuery(TbCentroCusto, Dmod.MyDB);
end;

procedure TFmCentroCustAll.DBGCentroCust5Enter(Sender: TObject);
begin
  DsExtra.DataSet := QrNNiv4;
  RenomeiaTituloExtra('do N�vel 4', 'do N�vle 5');
  FInGrade := True;
end;

procedure TFmCentroCustAll.DBGCentroCust4Enter(Sender: TObject);
begin
  DsExtra.DataSet := QrNNiv3;
  RenomeiaTituloExtra('do N�vel 3', 'do N�vel 4');
  FInGrade := True;
end;

procedure TFmCentroCustAll.DBGCentroCust3Enter(Sender: TObject);
begin
  DsExtra.DataSet := QrNNiv2;
  RenomeiaTituloExtra('do N�vel 2', 'do N�vel 3');
  FInGrade := True;
end;

procedure TFmCentroCustAll.DBGCentroCust2Enter(Sender: TObject);
begin
  DsExtra.DataSet := QrNNiv1;
  RenomeiaTituloExtra('do N�vel 1', 'do N�vel 2');
  FInGrade := True;
end;

procedure TFmCentroCustAll.DBGCentroCustoEnter(Sender: TObject);
begin
  DsExtra.DataSet := nil;
  RenomeiaTituloExtra('Descri��o', '');
  FInGrade := True;
end;

procedure TFmCentroCustAll.RenomeiaTituloExtra(TitSon, TitPai: String);
var
  i: Integer;
begin
  for i := 0 to DBGExtra.Columns.Count -1 do
  begin
    if DBGExtra.Columns[i].FieldName = 'Nome' then
      DBGExtra.Columns[i].Title.Caption := 'Descri��o ' + TitSon;
  end;
  StaticText1.Caption := 'Itens n�o pertencentes ' + TitPai + ' atual';
end;

procedure TFmCentroCustAll.DBGExtraCellClick(Column: TColumn);
begin
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGExtra.BeginDrag(True);
end;

function TFmCentroCustAll.AceitaDragOver(Source: TObject; Grade: String;
  Tabela: TmySQLQuery): Boolean;
begin
  Result := (Source is TDBGrid)  and
  (
    (TDBGrid(Source).Name = Grade) or
    (
      (TDBGrid(Source).Name = 'DBGExtra')
      and
      (DsExtra.DataSet = Tabela)
     )
   );
end;

procedure TFmCentroCustAll.VeSeAceitaDragDrop(Sender, Source: TObject; X, Y: Integer;
TitPai, TitSon, Tabela, Campo: String);
var
  gc : TGridCoord;
  PaiNome: string;
  SonCodi, PaiCodi: Integer;
begin
  gc := THackDBGrid(Sender).MouseCoord(X,Y);
  if (gc.X > 0) and (gc.Y > 0) then
  begin
    if THackDBGrid(Sender).DataSource.DataSet.RecordCount = 0 then Exit;
    if THackDBGrid(Sender).DataSource.DataSet.FieldByName('Codigo').AsInteger = 0 then Exit;
    PaiNome := THackDBGrid(Source).DataSource.DataSet.FieldByName('NOME_PAI').AsString;
    SonCodi := THackDBGrid(Source).DataSource.DataSet.FieldByName('Codigo').AsInteger;
    with THackDBGrid(Sender) do
    begin
      DataSource.DataSet.MoveBy(gc.Y - Row);
      PaiCodi := THackDBGrid(Sender).DataSource.DataSet.FieldByName('Codigo').AsInteger;
      //
      if Geral.MB_Pergunta('Foi solicitada a altera��o conforme abaixo: '
      + sLineBreak + sLineBreak + TitSon + ': "' +
      THackDBGrid(Source).DataSource.DataSet.FieldByName('Nome').AsString + '" ' +
      sLineBreak + sLineBreak +
      'Do ' + TitPai + ': "' + PaiNome + '" ' + sLineBreak +
      'Para o ' + TitPai + ': "' +
      THackDBGrid(Sender).DataSource.DataSet.FieldByName('Nome').AsString + '"' +
      sLineBreak + sLineBreak +
      'Confirma a altera��o?') = ID_YES then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False,
        [Campo], ['Codigo'], [PaiCodi], [SonCodi], True) then
        begin
          THackDBGrid(Sender).DataSource.DataSet.Close;
          THackDBGrid(Sender).DataSource.DataSet. Open;
          THackDBGrid(Sender).DataSource.DataSet.Locate('Codigo', PaiCodi, []);
          //
          THackDBGrid(Source).DataSource.DataSet.Close;
          THackDBGrid(Source).DataSource.DataSet. Open;
          THackDBGrid(Source).DataSource.DataSet.Locate('Codigo', SonCodi, []);
          //
          TbCentroCusto.Refresh;
          TbCentroCust2.Refresh;
          TbCentroCust3.Refresh;
          TbCentroCust4.Refresh;
          TbCentroCust5.Refresh;
        end;
      end;
    end;
  end;
end;

procedure TFmCentroCustAll.SbNivel4Click(Sender: TObject);
begin
  MostraConjuntos();
end;

procedure TFmCentroCustAll.SbNivel1Click(Sender: TObject);
begin
  MostraContas();
end;

procedure TFmCentroCustAll.SbNivel3Click(Sender: TObject);
begin
  MostraGrupos();
end;

procedure TFmCentroCustAll.SbImprimeClick(Sender: TObject);
begin
  CeCuRe_Jan.ImpressaoDoCentroDeCustos();
end;

procedure TFmCentroCustAll.MostraPlanos();
var
  Codigo: Integer;
begin
  if (TbCentroCust5.State <> dsInactive) and (TbCentroCust5.RecordCount > 0) then
    Codigo := TbCentroCust5Codigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDePlano(Codigo);
  //
  TbCentroCust5.Close;
  UnDmkDAC_PF.AbreTable(TbCentroCust5, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbCentroCust5.Locate('Codigo', Codigo, []);
end;

procedure TFmCentroCustAll.MostraConjuntos();
var
  Codigo: Integer;
begin
  if (TbCentroCust4.State <> dsInactive) and (TbCentroCust4.RecordCount > 0) then
    Codigo := TbCentroCust4Codigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDeConjutos(Codigo);
  //
  TbCentroCust4.Close;
  UnDmkDAC_PF.AbreTable(TbCentroCust4, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbCentroCust4.Locate('Codigo', Codigo, []);
end;

procedure TFmCentroCustAll.MostraGrupos();
var
  Codigo: Integer;
begin
  if (TbCentroCust3.State <> dsInactive) and (TbCentroCust3.RecordCount > 0) then
    Codigo := TbCentroCust3Codigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDeGrupos(Codigo);
  //
  TbCentroCust3.Close;
  UnDmkDAC_PF.AbreTable(TbCentroCust3, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbCentroCust3.Locate('Codigo', Codigo, []);
end;

procedure TFmCentroCustAll.MostraSubgrupos();
var
  Codigo: Integer;
begin
  if (TbCentroCust2.State <> dsInactive) and (TbCentroCust2.RecordCount > 0) then
    Codigo := TbCentroCust2Codigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDeSubGrupos(Codigo);
  //
  TbCentroCust2.Close;
  UnDmkDAC_PF.AbreTable(TbCentroCust2, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbCentroCust2.Locate('Codigo', Codigo, []);
end;

procedure TFmCentroCustAll.MostraContas();
var
  Codigo: Integer;
begin
  if (TbCentroCusto.State <> dsInactive) and (TbCentroCusto.RecordCount > 0) then
    Codigo := TbCentroCustoCodigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDeContas(Codigo);
  //
  TbCentroCusto.Close;
  UnDmkDAC_PF.AbreTable(TbCentroCusto, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbCentroCusto.Locate('Codigo', Codigo, []);
end;

procedure TFmCentroCustAll.SbNivel5Click(Sender: TObject);
begin
  MostraPlanos();
end;

procedure TFmCentroCustAll.SbNivel2Click(Sender: TObject);
begin
  MostraSubgrupos();
end;

procedure TFmCentroCustAll.CkDragDropClick(Sender: TObject);
begin
  PnExtra.Visible := CkDragDrop.Checked;
end;

procedure TFmCentroCustAll.TbCentroCust5BeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmCentroCustAll.TbCentroCust4BeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmCentroCustAll.TbCentroCust3BeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmCentroCustAll.TbCentroCust2BeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmCentroCustAll.TbCentroCustoBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmCentroCustAll.TbCentroCustoCalcFields(DataSet: TDataSet);
begin
(*
  if TbCentroCustoMensal.Value = '' then TbCentroCusto_M.Value := 0
  else TbCentroCusto_M.Value := dmkPF.V_FToInt(TbCentroCustoMensal.Value[1]);

  TbCentroCusto_S.Value := TbCentroCustoCtrlaSdo.Value;

  if TbCentroCustoCredito.Value = '' then TbCentroCusto_C.Value := 0
  else TbCentroCusto_C.Value := dmkPF.V_FToInt(TbCentroCustoCredito.Value[1]);

  if TbCentroCustoDebito.Value = '' then TbCentroCusto_D.Value := 0
  else TbCentroCusto_D.Value := dmkPF.V_FToInt(TbCentroCustoDebito.Value[1]);
*)
end;

procedure TFmCentroCustAll.DBGCentroCustoDblClick(Sender: TObject);
begin
  MostraContas();
end;

procedure TFmCentroCustAll.DBGCentroCustoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
(*
  if Column.FieldName = '_M' then
    MeuVCLSkin.DrawGrid(DBGCentroCusto, Rect, 1, TbCentroCusto_M.Value);
  if Column.FieldName = '_S' then
    MeuVCLSkin.DrawGrid(DBGCentroCusto, Rect, 1, TbCentroCusto_S.Value);
  if Column.FieldName = '_C' then
    MeuVCLSkin.DrawGrid(DBGCentroCusto, Rect, 1, TbCentroCusto_C.Value);
  if Column.FieldName = '_D' then
    MeuVCLSkin.DrawGrid(DBGCentroCusto, Rect, 1, TbCentroCusto_D.Value);
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGCentroCusto, Rect, 1, TbCentroCustoAtivo.Value);
*)
end;

procedure TFmCentroCustAll.DBGCentroCustoColEnter(Sender: TObject);
begin
  if (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_S')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_M')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_C')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_D')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = 'Ativo') then
    TDBGrid(Sender).Options := TDBGrid(Sender).Options - [dgEditing] else
    TDBGrid(Sender).Options := TDBGrid(Sender).Options + [dgEditing];
end;

procedure TFmCentroCustAll.DBGCentroCustoColExit(Sender: TObject);
begin
  THackDBGrid(Sender).Options := THackDBGrid(Sender).Options - [dgEditing];
end;

procedure TFmCentroCustAll.TbCentroCust5CalcFields(DataSet: TDataSet);
begin
  //TbCentroCust5_S.Value := TbCentroCust5CtrlaSdo.Value;
end;

procedure TFmCentroCustAll.TbCentroCust4CalcFields(DataSet: TDataSet);
begin
  //TbCentroCust4_S.Value := TbCentroCust4CtrlaSdo.Value;
end;

procedure TFmCentroCustAll.TbCentroCust3CalcFields(DataSet: TDataSet);
begin
  //TbCentroCust3_S.Value := TbCentroCust3CtrlaSdo.Value;
end;

procedure TFmCentroCustAll.TbCentroCust2CalcFields(DataSet: TDataSet);
begin
  //TbCentroCust2_S.Value := TbCentroCust2CtrlaSdo.Value;
end;

procedure TFmCentroCustAll.DBGCentroCust5DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
(*
  if Column.FieldName = '_S' then
    MeuVCLSkin.DrawGrid(DBGCentroCust5, Rect, 1, TbCentroCust5_S.Value);
*)
end;

procedure TFmCentroCustAll.DBGCentroCust4DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
(*
  if Column.FieldName = '_S' then
    MeuVCLSkin.DrawGrid(DBGCentroCust4, Rect, 1, TbCentroCust4_S.Value);
*)
end;

procedure TFmCentroCustAll.DBGCentroCust3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
(*
  if Column.FieldName = '_S' then
    MeuVCLSkin.DrawGrid(DBGCentroCust3, Rect, 1, TbCentroCust3_S.Value);
*)
end;

procedure TFmCentroCustAll.DBGCentroCust2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
(*
  if Column.FieldName = '_S' then
    MeuVCLSkin.DrawGrid(DBGCentroCust2, Rect, 1, TbCentroCust2_S.Value);
*)end;

procedure TFmCentroCustAll.DBGCentroCust5CellClick(Column: TColumn);
begin
(*
  if Column.FieldName = '_S' then
  begin
    Screen.Cursor := crHourGlass;
    TbCentroCust5.Edit;
    if TbCentroCust5_S.Value = 1 then
      TbCentroCust5CtrlaSdo.Value := 0
    else
      TbCentroCust5CtrlaSdo.Value := 1;
    TbCentroCust5.Post;
    Screen.Cursor := crDefault;
  end;
*)
end;


(*
object QrNNiv4: TMySQLQuery
  SQL.Strings = (
    'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
    'FROM conjuntos tb1'
    'LEFT JOIN plano tb2 ON tb2.Codigo=tb1.Plano'
    'WHERE tb1.Codigo>0'
    'AND tb1.Plano NOT IN (0, :P0)'
    'ORDER BY Nome')
  Left = 632
  Top = 300
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end>
  object QrNNiv4Codigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrNNiv4Nome: TWideStringField
    FieldName = 'Nome'
    Size = 50
  end
  object QrNNiv4NOME_PAI: TWideStringField
    FieldName = 'NOME_PAI'
    Size = 50
  end
end
object QrNGru: TMySQLQuery
  SQL.Strings = (
    'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
    'FROM grupos tb1'
    'LEFT JOIN conjuntos tb2 ON tb2.Codigo=tb1.Conjunto'
    'WHERE tb1.Codigo>0'
    'AND tb1.Conjunto NOT IN (0, :P0)'
    'ORDER BY Nome')
  Left = 632
  Top = 348
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end>
  object IntegerField1: TIntegerField
    FieldName = 'Codigo'
  end
  object StringField1: TWideStringField
    FieldName = 'Nome'
    Size = 50
  end
  object QrNGruNOME_PAI: TWideStringField
    FieldName = 'NOME_PAI'
    Size = 50
  end
end
object QrNSgr: TMySQLQuery
  SQL.Strings = (
    'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
    'FROM subgrupos tb1'
    'LEFT JOIN grupos tb2 ON tb2.Codigo=tb1.Grupo'
    'WHERE tb1.Codigo>0'
    'AND tb1.Grupo NOT IN (0, :P0)'
    'ORDER BY Nome')
  Left = 632
  Top = 396
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end>
  object IntegerField2: TIntegerField
    FieldName = 'Codigo'
  end
  object StringField2: TWideStringField
    FieldName = 'Nome'
    Size = 50
  end
  object QrNSgrNOME_PAI: TWideStringField
    FieldName = 'NOME_PAI'
    Size = 50
  end
end
object QrNCta: TMySQLQuery
  SQL.Strings = (
    'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
    'FROM contas tb1'
    'LEFT JOIN subgrupos tb2 ON tb2.Codigo=tb1.Subgrupo'
    'WHERE tb1.Codigo>0'
    'AND tb1.Subgrupo NOT IN (0, :P0)'
    'ORDER BY Nome')
  Left = 632
  Top = 444
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end>
  object IntegerField3: TIntegerField
    FieldName = 'Codigo'
  end
  object StringField3: TWideStringField
    FieldName = 'Nome'
    Size = 50
  end
  object QrNCtaNOME_PAI: TWideStringField
    FieldName = 'NOME_PAI'
    Size = 50
  end
end
*)

end.
