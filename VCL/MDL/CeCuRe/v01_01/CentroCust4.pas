unit CentroCust4;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkDBLookupComboBox, dmkEditCB, Vcl.Menus;

type
  TFmCentroCust4 = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCentroCusto: TMySQLQuery;
    DsCentroCusto: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrNivSup: TMySQLQuery;
    DsNivSup: TDataSource;
    dmkDBEdit1: TdmkDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    LaNivSup: TLabel;
    EdNivSup: TdmkEditCB;
    CBNivSup: TdmkDBLookupComboBox;
    SbNivSup: TSpeedButton;
    QrNivSupCodigo: TIntegerField;
    QrNivSupNome: TWideStringField;
    QrNivSupPagRec: TSmallintField;
    QrNivSupOrdem: TIntegerField;
    QrNivSupAtivo: TSmallintField;
    QrNivSupNivSup: TIntegerField;
    RGPagRec: TdmkRadioGroup;
    EdOrdem: TdmkEdit;
    dmkRadioGroup1: TDBRadioGroup;
    Label22: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    PMInclui: TPopupMenu;
    Centrodecusto1: TMenuItem;
    CentrodeResultado1: TMenuItem;
    Label20: TLabel;
    Label21: TLabel;
    QrCentroCustoReferencia: TWideStringField;
    QrCentroCustoCodigo: TIntegerField;
    QrCentroCustoNome: TWideStringField;
    QrCentroCustoPagRec: TSmallintField;
    QrCentroCustoOrdem: TIntegerField;
    QrCentroCustoAtivo: TSmallintField;
    QrCentroCustoNivSup: TIntegerField;
    QrCentroCustoNO_NivSup: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCentroCustoAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCentroCustoBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbNivSupClick(Sender: TObject);
    procedure RGPagRecClick(Sender: TObject);
    procedure Centrodecusto1Click(Sender: TObject);
    procedure CentrodeResultado1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdOrdemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure EnableNivSup(Enable: Boolean);
    //
    procedure ReopenNivSup();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCentroCust4: TFmCentroCust4;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnCeCuRe_Jan, DmkDAC_PF, UnCeCuRe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCentroCust4.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCentroCust4.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCentroCustoCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCentroCust4.DefParams;
begin
  VAR_GOTOTABELA := 'centrocust4';
  VAR_GOTOMYSQLTABLE := QrCentroCusto;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add('CONCAT(');
  VAR_SQLx.Add('  LPAD(ccr4.Ordem, 1, "0") ');
  VAR_SQLx.Add(') Referencia,');
  VAR_SQLx.Add('ccr4.*, ccr5.Nome NO_NivSup ');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('FROM centrocust4 ccr4 ');
  VAR_SQLx.Add('LEFT JOIN centrocust5 ccr5 ON ccr5.Codigo=ccr4.NivSup ');
  //
  VAR_SQLx.Add('WHERE ccr4.Codigo > 0');
  //
  VAR_SQL1.Add('AND ccr4.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND ccr1.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ccr4.Nome Like :P0');
  //
end;

procedure TFmCentroCust4.EdOrdemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  PagRec, NivSup, Ordem: Integer;
begin
  if Key = VK_F4 then
  begin
    PagRec := RGPagRec.ItemIndex;
    NivSup := EdNivSup.ValueVariant;
    //
    if MyObjects.FIC(PagRec = 0, RGPagRec, 'Defina a estrutura!') then Exit;
    if MyObjects.FIC(NivSup < 1, EdOrdem, 'Defina o n�vel superior!') then Exit;
    //
    Ordem  := CeCuRe_PF.ObtemProximaOrdem('centrocust4', PagRec, NivSup);
    if Ordem > 0 then
    begin
      EdOrdem.ValueVariant  := Ordem;
      //
      EnableNivSup(False);
    end;
  end;
end;

procedure TFmCentroCust4.EnableNivSup(Enable: Boolean);
begin
  LaNivSup.Enabled := Enable;
  EdNivSup.Enabled := Enable;
  CBNivSup.Enabled := Enable;
end;

procedure TFmCentroCust4.Centrodecusto1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCentroCusto, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'centrocust4');
  RGPagRec.ItemIndex := 1;
end;

procedure TFmCentroCust4.CentrodeResultado1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCentroCusto, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'centrocust4');
  RGPagRec.ItemIndex := 2;
end;

procedure TFmCentroCust4.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCentroCust4.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCentroCust4.ReopenNivSup();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNivSup, Dmod.MyDB, [
  'SELECT ccr5.* ',
  'FROM centrocust5 ccr5 ',
  'WHERE ccr5.PagRec=' + Geral.FF0(RGPagRec.ItemIndex),
  '']);
end;

procedure TFmCentroCust4.RGPagRecClick(Sender: TObject);
begin
  ReopenNivSup();
end;

procedure TFmCentroCust4.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCentroCust4.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCentroCust4.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCentroCust4.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCentroCust4.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCentroCust4.SbNivSupClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  CeCuRe_Jan.MostraFormCentroCust5(EdNivSup.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdNivSup, CBNivSup, QrNivSup, VAR_CADASTRO);
end;

procedure TFmCentroCust4.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCentroCust4.BtAlteraClick(Sender: TObject);
begin
  EnableNivSup(True);
  //
  if (QrCentroCusto.State <> dsInactive) and (QrCentroCusto.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCentroCusto, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'centrocust4');
  end;
end;

procedure TFmCentroCust4.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCentroCustoCodigo.Value;
  Close;
end;

procedure TFmCentroCust4.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, PagRec, Ordem, NivSup: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  PagRec         := RGPagRec.ItemIndex;
  Ordem          := EdOrdem.ValueVariant;
  NivSup         := EdNivSup.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(PagRec = 0, RGPagRec, 'Defina a estrutura!') then Exit;
  if MyObjects.FIC(Ordem < 1, EdOrdem, 'Defina a Ordem!') then Exit;
  if MyObjects.FIC(NivSup < 1, EdOrdem, 'Defina o n�vel superior!') then Exit;
  //
  if CeCuRe_PF.CadastroDuplicado('centrocust4', PagRec, Codigo, NivSup, Ordem, True) then Exit;
  //
  Codigo := UMyMod.BPGS1I32('centrocust4', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'centrocust4', False, [
  'Nome', 'PagRec', 'Ordem',
  'NivSup'], [
  'Codigo'], [
  Nome, PagRec, Ordem,
  NivSup], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCentroCust4.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'centrocust4', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCentroCust4.BtIncluiClick(Sender: TObject);
begin
  EnableNivSup(True);
  //
  MyObjects.MostraPopupDeBotao(PMInclui, BtInclui);
end;

procedure TFmCentroCust4.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  //
  ReopenNivSup();
  //
  CriaOForm;
end;

procedure TFmCentroCust4.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCentroCustoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCentroCust4.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCentroCust4.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCentroCustoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCentroCust4.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCentroCust4.QrCentroCustoAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCentroCust4.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCentroCust4.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCentroCustoCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'centrocust4', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCentroCust4.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCentroCust4.QrCentroCustoBeforeOpen(DataSet: TDataSet);
begin
  QrCentroCustoCodigo.DisplayFormat := FFormatFloat;
end;

end.

