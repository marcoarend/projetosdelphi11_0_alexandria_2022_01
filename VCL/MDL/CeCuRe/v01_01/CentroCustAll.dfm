object FmCentroCustAll: TFmCentroCustAll
  Left = 234
  Top = 170
  Caption = 'CCR-CADAS-010 :: Centros de Custos e Resultados'
  ClientHeight = 558
  ClientWidth = 1000
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 442
    Width = 1000
    Height = 46
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 27
      Width = 87
      Height = 13
      Caption = '*S: Controla saldo.'
    end
    object Label2: TLabel
      Left = 106
      Top = 27
      Width = 56
      Height = 13
      Caption = '*M: Mensal.'
    end
    object Label3: TLabel
      Left = 178
      Top = 27
      Width = 52
      Height = 13
      Caption = '*D: D'#233'bito.'
    end
    object Label4: TLabel
      Left = 236
      Top = 27
      Width = 53
      Height = 13
      Caption = '*C: Cr'#233'dito.'
    end
    object Label5: TLabel
      Left = 297
      Top = 27
      Width = 44
      Height = 13
      Caption = '*A: Ativo.'
    end
    object CkOcultaCadSistema: TCheckBox
      Left = 8
      Top = 8
      Width = 158
      Height = 17
      Caption = 'Ocultar cadastros do sistema.'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = CkOcultaCadSistemaClick
    end
    object CkDragDrop: TCheckBox
      Left = 170
      Top = 8
      Width = 228
      Height = 17
      Caption = 'Ativar arraste entre n'#237'veis (mudar n'#237'vel pai).'
      TabOrder = 1
      OnClick = CkDragDropClick
    end
    object PnSaiDesis: TPanel
      Left = 858
      Top = 0
      Width = 142
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 118
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object GBAvisos1: TGroupBox
      Left = 415
      Top = 0
      Width = 443
      Height = 46
      Align = alRight
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 439
        Height = 29
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 52
    Width = 1000
    Height = 390
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 154
      Width = 1000
      Height = 9
      Cursor = crVSplit
      Align = alTop
      Beveled = True
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 1000
      Height = 154
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter2: TSplitter
        Left = 173
        Top = 0
        Width = 10
        Height = 154
        Beveled = True
        ExplicitLeft = 246
      end
      object Splitter3: TSplitter
        Left = 429
        Top = 0
        Width = 10
        Height = 154
        Beveled = True
        ExplicitLeft = 502
      end
      object Splitter4: TSplitter
        Left = 685
        Top = 0
        Width = 10
        Height = 154
        Beveled = True
        ExplicitLeft = 758
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 173
        Height = 154
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object DBGCentroCust5: TDBGrid
          Left = 0
          Top = 17
          Width = 173
          Height = 137
          Align = alClient
          DataSource = DsCentroCust5
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGCentroCust5CellClick
          OnColEnter = DBGCentroCustoColEnter
          OnColExit = DBGCentroCustoColExit
          OnDrawColumnCell = DBGCentroCust5DrawColumnCell
          OnDblClick = DBGCentroCust5DblClick
          OnDragDrop = DBGCentroCust5DragDrop
          OnDragOver = DBGCentroCust5DragOver
          OnEnter = DBGCentroCust5Enter
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Descri'#231#227'o do n'#237'vel 5'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Cod.'
              Width = 36
              Visible = True
            end>
        end
        object StaticText2: TStaticText
          Left = 0
          Top = 0
          Width = 173
          Height = 17
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'N'#237'vel 5'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel7: TPanel
        Left = 183
        Top = 0
        Width = 246
        Height = 154
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object DBGCentroCust4: TDBGrid
          Left = 0
          Top = 17
          Width = 246
          Height = 137
          Align = alClient
          DataSource = DsCentroCust4
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGCentroCust4CellClick
          OnColEnter = DBGCentroCustoColEnter
          OnColExit = DBGCentroCustoColExit
          OnDrawColumnCell = DBGCentroCust4DrawColumnCell
          OnDblClick = DBGCentroCust4DblClick
          OnDragDrop = DBGCentroCust4DragDrop
          OnDragOver = DBGCentroCust4DragOver
          OnEnter = DBGCentroCust4Enter
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Descri'#231#227'o'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_PAI'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGray
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Nivel 5'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Cod.'
              Width = 36
              Visible = True
            end>
        end
        object StaticText3: TStaticText
          Left = 0
          Top = 0
          Width = 246
          Height = 17
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'N'#237'vel 4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel8: TPanel
        Left = 439
        Top = 0
        Width = 246
        Height = 154
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        object DBGCentroCust3: TDBGrid
          Left = 0
          Top = 17
          Width = 246
          Height = 137
          Align = alClient
          DataSource = DsCentroCust3
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGCentroCust3CellClick
          OnColEnter = DBGCentroCustoColEnter
          OnColExit = DBGCentroCustoColExit
          OnDrawColumnCell = DBGCentroCust3DrawColumnCell
          OnDblClick = DBGCentroCust3DblClick
          OnDragDrop = DBGCentroCust3DragDrop
          OnDragOver = DBGCentroCust3DragOver
          OnEnter = DBGCentroCust3Enter
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Descri'#231#227'o'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_PAI'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGray
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Conjunto'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Cod.'
              Width = 36
              Visible = True
            end>
        end
        object StaticText4: TStaticText
          Left = 0
          Top = 0
          Width = 246
          Height = 17
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'N'#237'vel 3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel9: TPanel
        Left = 695
        Top = 0
        Width = 305
        Height = 154
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 3
        object DBGCentroCust2: TDBGrid
          Left = 0
          Top = 17
          Width = 305
          Height = 137
          Align = alClient
          DataSource = DsCentroCust2
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGCentroCust2CellClick
          OnColEnter = DBGCentroCustoColEnter
          OnColExit = DBGCentroCustoColExit
          OnDrawColumnCell = DBGCentroCust2DrawColumnCell
          OnDblClick = DBGCentroCust2DblClick
          OnDragDrop = DBGCentroCust2DragDrop
          OnDragOver = DBGCentroCust2DragOver
          OnEnter = DBGCentroCust2Enter
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Descri'#231#227'o'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_PAI'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGray
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Grupo'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Cod.'
              Width = 40
              Visible = True
            end>
        end
        object StaticText5: TStaticText
          Left = 0
          Top = 0
          Width = 305
          Height = 17
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'N'#237'vel 2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
    end
    object PnExtra: TPanel
      Left = 737
      Top = 163
      Width = 263
      Height = 227
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object DBGExtra: TDBGrid
        Left = 0
        Top = 17
        Width = 263
        Height = 210
        Align = alClient
        DataSource = DsExtra
        Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGExtraCellClick
        OnColEnter = DBGCentroCustoColEnter
        OnColExit = DBGCentroCustoColExit
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Cod.'
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o CONJUNTO'
            Width = 185
            Visible = True
          end>
      end
      object StaticText1: TStaticText
        Left = 0
        Top = 0
        Width = 263
        Height = 17
        Align = alTop
        Alignment = taCenter
        Caption = 'Itens n'#227'o pertencentes '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
    end
    object Panel10: TPanel
      Left = 0
      Top = 163
      Width = 737
      Height = 227
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object DBGCentroCusto: TDBGrid
        Left = 0
        Top = 17
        Width = 737
        Height = 210
        Align = alClient
        DataSource = DsCentroCusto
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGCentroCustoCellClick
        OnColEnter = DBGCentroCustoColEnter
        OnColExit = DBGCentroCustoColExit
        OnDrawColumnCell = DBGCentroCustoDrawColumnCell
        OnDblClick = DBGCentroCustoDblClick
        OnEnter = DBGCentroCustoEnter
        Columns = <
          item
            Expanded = False
            FieldName = 'Ordem'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Caption = 'Descri'#231#227'o'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_PAI'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Caption = 'Subgrupo'
            Width = 112
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Cod.'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ativo'
            Title.Caption = 'A*'
            Width = 14
            Visible = True
          end>
      end
      object StaticText6: TStaticText
        Left = 0
        Top = 0
        Width = 737
        Height = 17
        Align = alTop
        Alignment = taCenter
        BorderStyle = sbsSunken
        Caption = 'N'#237'vel 1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1000
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 948
      Top = 0
      Width = 52
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 297
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 3
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNivel5: TBitBtn
        Tag = 350
        Left = 45
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNivel5Click
      end
      object SbNivel4: TBitBtn
        Tag = 351
        Left = 86
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNivel4Click
      end
      object SbNivel3: TBitBtn
        Tag = 352
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNivel3Click
      end
      object SbNivel2: TBitBtn
        Tag = 353
        Left = 170
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbNivel2Click
      end
      object SbNivel1: TBitBtn
        Tag = 354
        Left = 211
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 5
        OnClick = SbNivel1Click
      end
      object BtImpExp: TBitBtn
        Tag = 270
        Left = 252
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtImpExpClick
      end
    end
    object GB_M: TGroupBox
      Left = 297
      Top = 0
      Width = 651
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 401
        Height = 32
        Caption = 'Centros de Custos e Resultados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 401
        Height = 32
        Caption = 'Centros de Custos e Resultados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 401
        Height = 32
        Caption = 'Centros de Custos e Resultados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 488
    Width = 1000
    Height = 70
    Align = alBottom
    TabOrder = 3
    Visible = False
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 996
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 7
        Top = 4
        Width = 118
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
      end
      object BtCancela: TBitBtn
        Tag = 15
        Left = 195
        Top = 4
        Width = 118
        Height = 40
        Caption = '&Cancela'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
    end
  end
  object DsCentroCust5: TDataSource
    DataSet = TbCentroCust5
    Left = 17
    Top = 137
  end
  object DsCentroCust4: TDataSource
    DataSet = TbCentroCust4
    Left = 285
    Top = 141
  end
  object DsCentroCust3: TDataSource
    DataSet = TbCentroCust3
    Left = 513
    Top = 137
  end
  object DsCentroCust2: TDataSource
    DataSet = TbCentroCust2
    Left = 793
    Top = 141
  end
  object DsCentroCusto: TDataSource
    DataSet = TbCentroCusto
    Left = 60
    Top = 304
  end
  object TbCentroCust5: TMySQLTable
    Database = Dmod.MyDB
    Filter = 'Codigo>0'
    Filtered = True
    BeforeInsert = TbCentroCust5BeforeInsert
    BeforePost = TbCentroCust5BeforePost
    AfterPost = TbCentroCust5AfterPost
    AfterCancel = TbCentroCust5AfterCancel
    BeforeDelete = TbCentroCust5BeforeDelete
    AfterScroll = TbCentroCust5AfterScroll
    OnCalcFields = TbCentroCust5CalcFields
    TableName = 'centrocust5'
    Left = 17
    Top = 89
    object TbCentroCust5Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object TbCentroCust5Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object TbCentroCust5PagRec: TSmallintField
      FieldName = 'PagRec'
      Required = True
    end
    object TbCentroCust5NivSup: TIntegerField
      FieldName = 'NivSup'
      Required = True
    end
    object TbCentroCust5Ordem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object TbCentroCust5Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object TbCentroCust4: TMySQLTable
    Database = Dmod.MyDB
    Filter = 'NivSup=-1000'
    Filtered = True
    BeforeInsert = TbCentroCust4BeforeInsert
    AfterInsert = TbCentroCust4AfterInsert
    AfterEdit = TbCentroCust4AfterEdit
    BeforePost = TbCentroCust4BeforePost
    AfterPost = TbCentroCust4AfterPost
    AfterCancel = TbCentroCust4AfterCancel
    BeforeDelete = TbCentroCust4BeforeDelete
    AfterScroll = TbCentroCust4AfterScroll
    OnCalcFields = TbCentroCust4CalcFields
    TableName = 'centrocust4'
    Left = 285
    Top = 93
    object TbCentroCust4NOME_PAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PAI'
      LookupDataSet = QrTNiv5
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'NivSup'
      Size = 50
      Lookup = True
    end
    object TbCentroCust4Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'centrocust4.Codigo'
      Required = True
    end
    object TbCentroCust4Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'centrocust4.Nome'
      Required = True
      Size = 60
    end
    object TbCentroCust4PagRec: TSmallintField
      FieldName = 'PagRec'
      Origin = 'centrocust4.PagRec'
      Required = True
    end
    object TbCentroCust4NivSup: TIntegerField
      FieldName = 'NivSup'
      Origin = 'centrocust4.NivSup'
      Required = True
    end
    object TbCentroCust4Ordem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'centrocust4.Ordem'
      Required = True
    end
    object TbCentroCust4Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'centrocust4.Ativo'
      Required = True
    end
  end
  object TbCentroCust3: TMySQLTable
    Database = Dmod.MyDB
    Filter = 'NivSup=-1000'
    Filtered = True
    BeforeInsert = TbCentroCust3BeforeInsert
    BeforePost = TbCentroCust3BeforePost
    AfterPost = TbCentroCust3AfterPost
    AfterCancel = TbCentroCust3AfterCancel
    BeforeDelete = TbCentroCust3BeforeDelete
    AfterScroll = TbCentroCust3AfterScroll
    OnCalcFields = TbCentroCust3CalcFields
    TableName = 'centrocust3'
    Left = 513
    Top = 89
    object TbCentroCust3NOME_PAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PAI'
      LookupDataSet = QrTNiv4
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'NivSup'
      Size = 50
      Lookup = True
    end
    object TbCentroCust3Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object TbCentroCust3Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object TbCentroCust3PagRec: TSmallintField
      FieldName = 'PagRec'
      Required = True
    end
    object TbCentroCust3NivSup: TIntegerField
      FieldName = 'NivSup'
      Required = True
    end
    object TbCentroCust3Ordem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object TbCentroCust3Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object TbCentroCust2: TMySQLTable
    Database = Dmod.MyDB
    Filter = 'NivSup=-1000'
    Filtered = True
    BeforeInsert = TbCentroCust2BeforeInsert
    BeforePost = TbCentroCust2BeforePost
    AfterPost = TbCentroCust2AfterPost
    AfterCancel = TbCentroCust2AfterCancel
    BeforeDelete = TbCentroCust2BeforeDelete
    AfterScroll = TbCentroCust2AfterScroll
    OnCalcFields = TbCentroCust2CalcFields
    TableName = 'centrocust2'
    Left = 793
    Top = 93
    object TbCentroCust2NOME_PAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PAI'
      LookupDataSet = QrTNiv3
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'NivSup'
      Size = 50
      Lookup = True
    end
    object TbCentroCust2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object TbCentroCust2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object TbCentroCust2PagRec: TSmallintField
      FieldName = 'PagRec'
      Required = True
    end
    object TbCentroCust2Ordem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object TbCentroCust2Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object TbCentroCust2NivSup: TIntegerField
      FieldName = 'NivSup'
      Required = True
    end
  end
  object TbCentroCusto: TMySQLTable
    Database = Dmod.MyDB
    Filter = 'NivSup=-1000'
    Filtered = True
    BeforePost = TbCentroCustoBeforePost
    BeforeDelete = TbCentroCustoBeforeDelete
    OnCalcFields = TbCentroCustoCalcFields
    TableName = 'centrocusto'
    Left = 60
    Top = 256
    object TbCentroCustoNOME_PAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PAI'
      LookupDataSet = QrTNiv2
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'NivSup'
      Size = 50
      Lookup = True
    end
    object TbCentroCustoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object TbCentroCustoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object TbCentroCustoNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object TbCentroCustoPagRec: TSmallintField
      FieldName = 'PagRec'
      Required = True
    end
    object TbCentroCustoOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object TbCentroCustoNivSup: TIntegerField
      FieldName = 'NivSup'
      Required = True
    end
  end
  object QrLoc: TMySQLQuery
    Left = 256
    Top = 288
  end
  object DsExtra: TDataSource
    Left = 836
    Top = 292
  end
  object mySQLUpdateSQL1: TMySQLUpdateSQL
    Left = 412
    Top = 260
  end
  object QrTNiv5: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM centrocust5'
      'ORDER BY Nome'
      '')
    Left = 140
    Top = 92
    object QrTNiv5Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'centrocust5.Codigo'
    end
    object QrTNiv5Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'centrocust5.Nome'
      Size = 50
    end
  end
  object QrTNiv4: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM centrocust4'
      'ORDER BY Nome'
      '')
    Left = 140
    Top = 144
    object QrTNiv4Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTNiv4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrTNiv3: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM centrocust3'
      'ORDER BY Nome'
      '')
    Left = 140
    Top = 196
    object QrTNiv3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTNiv3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrTNiv2: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM centrocust2'
      'ORDER BY Nome'
      '')
    Left = 140
    Top = 248
    object QrTNiv2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 412
    Top = 92
  end
  object QrNNiv4: TMySQLQuery
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM conjuntos tb1'
      'LEFT JOIN plano tb2 ON tb2.Codigo=tb1.Plano'
      'WHERE tb1.Codigo>0'
      'AND tb1.Plano NOT IN (0, :P0)'
      'ORDER BY Nome')
    Left = 632
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNNiv4Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNNiv4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNNiv4NOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object QrNNiv3: TMySQLQuery
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM grupos tb1'
      'LEFT JOIN conjuntos tb2 ON tb2.Codigo=tb1.Conjunto'
      'WHERE tb1.Codigo>0'
      'AND tb1.Conjunto NOT IN (0, :P0)'
      'ORDER BY Nome')
    Left = 632
    Top = 348
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNNiv3NOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object QrNNiv2: TMySQLQuery
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM subgrupos tb1'
      'LEFT JOIN grupos tb2 ON tb2.Codigo=tb1.Grupo'
      'WHERE tb1.Codigo>0'
      'AND tb1.Grupo NOT IN (0, :P0)'
      'ORDER BY Nome')
    Left = 632
    Top = 396
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNNiv2NOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object QrNNiv1: TMySQLQuery
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM contas tb1'
      'LEFT JOIN subgrupos tb2 ON tb2.Codigo=tb1.Subgrupo'
      'WHERE tb1.Codigo>0'
      'AND tb1.Subgrupo NOT IN (0, :P0)'
      'ORDER BY Nome')
    Left = 632
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNNiv1NOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
end
