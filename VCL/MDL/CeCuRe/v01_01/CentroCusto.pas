unit CentroCusto;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkDBLookupComboBox, dmkEditCB, Vcl.Menus;

type
  TFmCentroCusto = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCentroCusto: TMySQLQuery;
    DsCentroCusto: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrCentroCustoCodigo: TIntegerField;
    QrCentroCustoNome: TWideStringField;
    QrCentroCustoPagRec: TSmallintField;
    QrCentroCustoOrdem: TIntegerField;
    QrCentroCustoNivSup: TIntegerField;
    QrCentroCustoAtivo: TSmallintField;
    QrNivSup: TMySQLQuery;
    DsNivSup: TDataSource;
    QrCentroCustoNO_NivSup: TWideStringField;
    QrCentroCustoNO_NivSu3: TWideStringField;
    QrCentroCustoNO_NivSu4: TWideStringField;
    dmkDBEdit1: TdmkDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    Label8: TLabel;
    Label10: TLabel;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    dmkDBEdit8: TdmkDBEdit;
    QrCentroCustoNivSu3: TIntegerField;
    QrCentroCustoNivSu4: TIntegerField;
    QrCentroCustoNivSu5: TIntegerField;
    QrCentroCustoNO_NivSu5: TWideStringField;
    LaNivSup: TLabel;
    EdNivSup: TdmkEditCB;
    CBNivSup: TdmkDBLookupComboBox;
    SbNivSup: TSpeedButton;
    Label14: TLabel;
    dmkDBEdit9: TdmkDBEdit;
    Label15: TLabel;
    dmkDBEdit10: TdmkDBEdit;
    Label16: TLabel;
    dmkDBEdit11: TdmkDBEdit;
    dmkDBEdit12: TdmkDBEdit;
    Label17: TLabel;
    Label18: TLabel;
    dmkDBEdit13: TdmkDBEdit;
    dmkDBEdit14: TdmkDBEdit;
    Label19: TLabel;
    QrNivSupCodigo: TIntegerField;
    QrNivSupNome: TWideStringField;
    QrNivSupPagRec: TSmallintField;
    QrNivSupOrdem: TIntegerField;
    QrNivSupAtivo: TSmallintField;
    QrNivSupNivSup: TIntegerField;
    QrNivSupNivSu3: TIntegerField;
    QrNivSupNO_NivSu3: TWideStringField;
    QrNivSupNivSu4: TIntegerField;
    QrNivSupNO_NivSu4: TWideStringField;
    QrNivSupNivSu5: TIntegerField;
    QrNivSupNO_NivSu5: TWideStringField;
    RGPagRec: TdmkRadioGroup;
    EdOrdem: TdmkEdit;
    dmkRadioGroup1: TDBRadioGroup;
    QrCentroCustoReferencia: TWideStringField;
    Label22: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    PMInclui: TPopupMenu;
    Centrodecusto1: TMenuItem;
    CentrodeResultado1: TMenuItem;
    Label20: TLabel;
    Label21: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCentroCustoAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCentroCustoBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbNivSupClick(Sender: TObject);
    procedure RGPagRecClick(Sender: TObject);
    procedure Centrodecusto1Click(Sender: TObject);
    procedure CentrodeResultado1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdOrdemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure EnableNivSup(Enable: Boolean);
    //
    procedure ReopenNivSup();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCentroCusto: TFmCentroCusto;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnCeCuRe_Jan, DmkDAC_PF, UnCeCuRe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCentroCusto.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCentroCusto.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCentroCustoCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCentroCusto.DefParams;
begin
  VAR_GOTOTABELA := 'centrocusto';
  VAR_GOTOMYSQLTABLE := QrCentroCusto;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add('CONCAT(');
  VAR_SQLx.Add('  LPAD(ccr4.Ordem, 1, "0"), ".",');
  VAR_SQLx.Add('  LPAD(ccr3.Ordem, 2, "0"), ".",');
  VAR_SQLx.Add('  LPAD(ccr2.Ordem, 3, "0"), ".",');
  VAR_SQLx.Add('  LPAD(ccr1.Ordem, 4, "0") ');
  VAR_SQLx.Add(') Referencia,');
  VAR_SQLx.Add('ccr1.*, ccr2.Nome NO_NivSup,');
  VAR_SQLx.Add('ccr2.NivSup NivSu3, ccr3.Nome NO_NivSu3, ');
  VAR_SQLx.Add('ccr3.NivSup NivSu4, ccr4.Nome NO_NivSu4,');
  VAR_SQLx.Add('ccr4.NivSup NivSu5, ccr5.Nome NO_NivSu5');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('FROM centrocusto ccr1');
  VAR_SQLx.Add('LEFT JOIN centrocust2 ccr2 ON ccr2.Codigo=ccr1.NivSup');
  VAR_SQLx.Add('LEFT JOIN centrocust3 ccr3 ON ccr3.Codigo=ccr2.NivSup');
  VAR_SQLx.Add('LEFT JOIN centrocust4 ccr4 ON ccr4.Codigo=ccr3.NivSup');
  VAR_SQLx.Add('LEFT JOIN centrocust5 ccr5 ON ccr5.Codigo=ccr4.NivSup');
  //
  VAR_SQLx.Add('WHERE ccr1.Codigo > 0');
  //
  VAR_SQL1.Add('AND ccr1.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND ccr1.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ccr1.Nome Like :P0');
  //
end;

procedure TFmCentroCusto.EdOrdemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  PagRec, NivSup, Ordem: Integer;
begin
  if Key = VK_F4 then
  begin
    PagRec := RGPagRec.ItemIndex;
    NivSup := EdNivSup.ValueVariant;
    //
    if MyObjects.FIC(PagRec = 0, RGPagRec, 'Defina a estrutura!') then Exit;
    if MyObjects.FIC(NivSup < 1, EdOrdem, 'Defina o n�vel superior!') then Exit;
    //
    Ordem  := CeCuRe_PF.ObtemProximaOrdem('centrocusto', PagRec, NivSup);
    if Ordem > 0 then
    begin
      EdOrdem.ValueVariant  := Ordem;
      //
      EnableNivSup(False);
    end;
  end;
end;

procedure TFmCentroCusto.EnableNivSup(Enable: Boolean);
begin
  LaNivSup.Enabled := Enable;
  EdNivSup.Enabled := Enable;
  CBNivSup.Enabled := Enable;
end;

procedure TFmCentroCusto.Centrodecusto1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCentroCusto, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'centrocusto');
  RGPagRec.ItemIndex := 1;
end;

procedure TFmCentroCusto.CentrodeResultado1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCentroCusto, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'centrocusto');
  RGPagRec.ItemIndex := 2;
end;

procedure TFmCentroCusto.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCentroCusto.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCentroCusto.ReopenNivSup();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNivSup, Dmod.MyDB, [
  'SELECT ccr2.*, ',
  'ccr2.NivSup NivSu3, ccr3.Nome NO_NivSu3,  ',
  'ccr3.NivSup NivSu4, ccr4.Nome NO_NivSu4, ',
  'ccr4.NivSup NivSu5, ccr5.Nome NO_NivSu5 ',
  ' ',
  'FROM centrocust2 ccr2 ',
  'LEFT JOIN centrocust3 ccr3 ON ccr3.Codigo=ccr2.NivSup ',
  'LEFT JOIN centrocust4 ccr4 ON ccr4.Codigo=ccr3.NivSup ',
  'LEFT JOIN centrocust5 ccr5 ON ccr5.Codigo=ccr4.NivSup ',
  'WHERE ccr5.PagRec=' + Geral.FF0(RGPagRec.ItemIndex),
  '']);
end;

procedure TFmCentroCusto.RGPagRecClick(Sender: TObject);
begin
  ReopenNivSup();
end;

procedure TFmCentroCusto.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCentroCusto.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCentroCusto.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCentroCusto.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCentroCusto.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCentroCusto.SbNivSupClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  CeCuRe_Jan.MostraFormCentroCust2(EdNivSup.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdNivSup, CBNivSup, QrNivSup, VAR_CADASTRO);
end;

procedure TFmCentroCusto.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCentroCusto.BtAlteraClick(Sender: TObject);
begin
  EnableNivSup(True);
  //
  if (QrCentroCusto.State <> dsInactive) and (QrCentroCusto.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCentroCusto, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'centrocusto');
  end;
end;

procedure TFmCentroCusto.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCentroCustoCodigo.Value;
  Close;
end;

procedure TFmCentroCusto.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, PagRec, Ordem, NivSup: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  PagRec         := RGPagRec.ItemIndex;
  Ordem          := EdOrdem.ValueVariant;
  NivSup         := EdNivSup.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(PagRec = 0, RGPagRec, 'Defina a estrutura!') then Exit;
  if MyObjects.FIC(Ordem < 1, EdOrdem, 'Defina a Ordem!') then Exit;
  if MyObjects.FIC(NivSup < 1, EdOrdem, 'Defina o n�vel superior!') then Exit;
  //
  if CeCuRe_PF.CadastroDuplicado('centrocusto', PagRec, Codigo, NivSup, Ordem, True) then Exit;
  //
  Codigo := UMyMod.BPGS1I32('centrocusto', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'centrocusto', False, [
  'Nome', 'PagRec', 'Ordem',
  'NivSup'], [
  'Codigo'], [
  Nome, PagRec, Ordem,
  NivSup], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCentroCusto.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'centrocusto', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCentroCusto.BtIncluiClick(Sender: TObject);
begin
  EnableNivSup(True);
  //
  MyObjects.MostraPopupDeBotao(PMInclui, BtInclui);
end;

procedure TFmCentroCusto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  //
  ReopenNivSup();
  //
  CriaOForm;
end;

procedure TFmCentroCusto.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCentroCustoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCentroCusto.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCentroCusto.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCentroCustoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCentroCusto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCentroCusto.QrCentroCustoAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCentroCusto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCentroCusto.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCentroCustoCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'centrocusto', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCentroCusto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCentroCusto.QrCentroCustoBeforeOpen(DataSet: TDataSet);
begin
  QrCentroCustoCodigo.DisplayFormat := FFormatFloat;
end;

end.

