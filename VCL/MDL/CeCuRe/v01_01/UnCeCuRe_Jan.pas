unit UnCeCuRe_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, Variants;

type
  TUnCeCuRe_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure ImpressaoDoCentroDeCustos();
    //
    procedure MostraFormCentroCusto(Codigo: Integer);
    procedure MostraFormCentroCust2(Codigo: Integer);
    procedure MostraFormCentroCust3(Codigo: Integer);
    procedure MostraFormCentroCust4(Codigo: Integer);
    procedure MostraFormCentroCust5(Codigo: Integer);
    procedure MostraFormCentroCustAll(Codigo: Integer);
  end;

var
  CeCuRe_Jan: TUnCeCuRe_Jan;


implementation

uses
  MyDBCheck, Module, CfgCadLista,
  CentroCusto, CentroCust2, CentroCust3, CentroCust4, CentroCustAll, CeCuReImp;

{ TUnCeCuRe_Jan }

procedure TUnCeCuRe_Jan.ImpressaoDoCentroDeCustos();
begin
  if DBCheck.CriaFm(TFmCeCuReImp, FmCeCuReImp, afmoNegarComAviso) then
  begin
    FmCeCuReImp.ShowModal;
    FmCeCuReImp.Destroy;
  end;
end;

procedure TUnCeCuRe_Jan.MostraFormCentroCust2(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCentroCust2, FmCentroCust2, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCentroCust2.LocCod(Codigo, Codigo);
    FmCentroCust2.ShowModal;
    FmCentroCust2.Destroy;
  end;
end;

procedure TUnCeCuRe_Jan.MostraFormCentroCust3(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCentroCust3, FmCentroCust3, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCentroCust3.LocCod(Codigo, Codigo);
    FmCentroCust3.ShowModal;
    FmCentroCust3.Destroy;
  end;
end;

procedure TUnCeCuRe_Jan.MostraFormCentroCust4(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCentroCust4, FmCentroCust4, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCentroCust4.LocCod(Codigo, Codigo);
    FmCentroCust4.ShowModal;
    FmCentroCust4.Destroy;
  end;
end;

procedure TUnCeCuRe_Jan.MostraFormCentroCust5(Codigo: Integer);
begin
//
end;

procedure TUnCeCuRe_Jan.MostraFormCentroCustAll(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCentroCustAll, FmCentroCustAll, afmoNegarComAviso) then
  begin
    FmCentroCustAll.ShowModal;
    FmCentroCustAll.Destroy;
  end;
end;

procedure TUnCeCuRe_Jan.MostraFormCentroCusto(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCentroCusto, FmCentroCusto, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCentroCusto.LocCod(Codigo, Codigo);
    FmCentroCusto.ShowModal;
    FmCentroCusto.Destroy;
  end;
end;

end.
