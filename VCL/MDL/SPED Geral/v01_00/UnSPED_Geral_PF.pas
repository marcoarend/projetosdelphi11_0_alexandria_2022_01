unit UnSPED_Geral_PF;


interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, UnMsgInt, Db,
  DbCtrls, Buttons, ZCF2, mySQLDbTables, ComCtrls, Grids, DBGrids, CommCtrl,
  Consts, UnDmkProcFunc, Variants, MaskUtils, frxClass, frxPreview,
  mySQLExceptions,
  // Dermatek
  dmkGeral, dmkEdit, dmkDBEdit, dmkEditF7, dmkEditDateTimePicker, dmkCheckGroup,
  dmkDBLookupCombobox, dmkEditCB, dmkDBGrid, dmkDBGridDAC, dmkImage, UnDmkEnums;

type
  TUnSPED_Geral_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function DefineAliquotaPISPeloRegimeTributario(var Aliquota: Double): Boolean;
    function DefineAliquotaCOFINSPeloRegimeTributario(var Aliquota: Double): Boolean;
  end;

var
  SPED_Geral_PF: TUnSPED_Geral_PF;

implementation

uses
  UnMyObjects, DmkDAC_PF,
  Module, ModuleGeral;

{ TUnSPED_Geral_PF }

{ TUnSPED_Geral_PF }

function TUnSPED_Geral_PF.DefineAliquotaCOFINSPeloRegimeTributario(var
  Aliquota: Double): Boolean;
begin
  Result := DModG.QrParamsSPEDRegimTributar.Value <> 0;
  case DModG.QrParamsSPEDRegimTributar.Value of
    //Indefinido
    //Lucro Real
    1: Aliquota := 7.60;  // Mudar para campo no ParamsEmp!!!!!
    //Lucro Presumido
    2: Aliquota := 3.00;
    //Lucro Arbitrado
    3: Aliquota := 7.60;
    //Simples Nacional
    4: Aliquota := 0.00;
  end;
end;

function TUnSPED_Geral_PF.DefineAliquotaPISPeloRegimeTributario(var Aliquota:
  Double): Boolean;
begin
  Result := DModG.QrParamsSPEDRegimTributar.Value <> 0;
  case DModG.QrParamsSPEDRegimTributar.Value of
    //Indefinido
    //Lucro Real
    1: Aliquota := 1.65;  // Mudar para campo no ParamsEmp!!!!!
    //Lucro Presumido
    2: Aliquota := 0.65;
    //Lucro Arbitrado
    3: Aliquota := 1.65;
    //Simples Nacional
    4: Aliquota := 0.00;
  end;
end;

end.
