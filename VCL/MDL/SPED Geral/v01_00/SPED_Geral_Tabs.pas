unit SPED_Geral_Tabs;
// antigo SPED_EFD_Tabs

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts, dmkGeral, UnDmkEnums,
  mySQLDbTables;

type
  TSPED_Geral_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Database: TmySQLDatabase; Lista:
             TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
  end;

var
  SPED_Geral_Tb: TSPED_Geral_Tabs;

implementation

uses UMySQLModule, ModuleGeral;

const
  CO_TbTxt_REC_REF_OBRIGACAO = 'REC_REF_OBRIGACAO';

function TSPED_Geral_Tabs.CarregaListaTabelas(Database: TmySQLDatabase; Lista: TList<TTabelas>): Boolean;
var
  CDR: Boolean;
begin
  CDR := False; // VAR_IS_CollectDataServerRepository;
  try
    if (DModG <> nil) and (Database = DModG.AllID_DB) then
    begin
      MyLinguas.AdTbLst(Lista, False, Lowercase('LstSpedCodsTabG'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('LstSpedCodsTabT'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods001'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods002'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods003'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods004'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods005'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods006'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods007'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods008'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods009'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods010'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods011'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods012'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods013'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods014'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods015'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods016'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods017'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods018'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods019'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods020'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods021'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods022'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods023'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods026'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods027'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods028'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods029'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods030'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods031'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods032'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods033'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods034'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods035'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods036'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods037'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods038'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods039'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods040'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods041'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods042'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods043'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods044'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods045'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods046'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods047'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods048'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods049'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods050'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods051'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods052'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods053'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods054'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods055'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods056'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods057'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods058'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods059'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods060'), LowerCase('tbSpedCods001'));
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods121'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods122'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods123'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods124'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods125'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods126'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods127'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods128'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods129'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods130'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods132'), LowerCase('tbSpedCods001'));

      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods131'), LowerCase('tbSpedCods001'));
      (*
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods155'), ''); //OGC
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods156'), ''); //NCM|NCM EXCECAO|EX|ALIQ_PIS_PERC|ALIQ_COFINS_PERC
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods157'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods158'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods159'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods160'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods161'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods162'), LowerCase('tbSpedCods156')); // <- 156
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods163'), LowerCase('tbSpedCods156')); // <- 156
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods164'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods165'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods166'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods167'), LowerCase('tbSpedCods166')); // <- 166
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods168'), LowerCase('tbSpedCods166')); // <- 166
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods169'), LowerCase('tbSpedCods166')); // <- 166
      *)
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods170'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods171'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods172'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods173'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods175'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods176'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods179'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods180'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods181'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods182'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods183'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods184'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods185'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods186'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods187'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods188'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods189'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods190'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods191'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods192'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods193'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods194'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods195'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods196'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods197'), LowerCase('tbSpedCods001'));
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods212'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods220'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods221'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods222'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods223'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods224'), LowerCase('tbSpedCods001'));
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods244'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods245'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods246'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods247'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods248'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods249'), LowerCase('tbSpedCods001'));
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods408'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods555'), LowerCase('tbSpedCods001'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods598'), LowerCase('tbSpedCods001'));
      // ini 2022-03-31
      MyLinguas.AdTbLst(Lista, False, Lowercase('tbSpedCods13449'), LowerCase('tbSpedCods001'));
      // fim 2022-03-31
    end else
    begin
      //
    end;
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TSPED_Geral_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  if Uppercase(Tabela) = Uppercase('LstSpedCodsTabG') then
  begin
    FListaSQL.Add('Codigo|Nome|UF');
    FListaSQL.Add('0|"Tabelas com grupo indefinido"|""');
    FListaSQL.Add('1|"Tabelas Globais"|""');
    FListaSQL.Add('2|"Tabelas Gen�ricas - ICMS"|""');
    FListaSQL.Add('3|"Tabelas de Caracter�sticas dos Documentos"|""');
    FListaSQL.Add('4|"Tabelas do IPI"|""');
    FListaSQL.Add('5|"Tabelas de Situa��o Tribut�ria"|""');
    FListaSQL.Add('6|"Tabelas de Classes de Consumo"|""');
    FListaSQL.Add('7|"Tabelas do Acre"|"AC"');
    FListaSQL.Add('8|"Tabelas de Alagoas"|"AL"');
    FListaSQL.Add('9|"Tabelas do Amazonas"|"AM"');
    FListaSQL.Add('10|"Tabelas do Amap�"|"AP"');
    FListaSQL.Add('11|"Tabelas da Bahia"|"BA"');
    FListaSQL.Add('12|"Tabelas do Cear�"|"CE"');
    FListaSQL.Add('13|"Tabelas do Distrito Federal"|"DF"');
    FListaSQL.Add('14|"Tabelas do Esp�rito Santo"|"ES"');
    FListaSQL.Add('15|"Tabelas de Goi�s"|"GO"');
    FListaSQL.Add('16|"Tabelas do Maranh�o"|"MA"');
    FListaSQL.Add('17|"Tabelas do Mato Grosso"|"MT"');
    FListaSQL.Add('18|"Tabelas do Mato Grosso do Sul"|"MS"');
    FListaSQL.Add('19|"Tabelas de Minas Gerais"|"MG"');
    FListaSQL.Add('20|"Tabelas do Par�"|"PA"');
    FListaSQL.Add('21|"Tabelas da Para�ba"|"PB"');
    FListaSQL.Add('22|"Tabelas de Pernambuco"|"PE"');
    FListaSQL.Add('23|"Tabelas do Paran�"|"PR"');
    FListaSQL.Add('24|"Tabelas do Piau�"|"PI"');
    FListaSQL.Add('25|"Tabelas do Rio de Janeiro"|"RJ"');
    FListaSQL.Add('26|"Tabelas do Rio Grande do Norte"|"RN"');
    FListaSQL.Add('27|"Tabelas do Rio Grande do Sul"|"RS"');
    FListaSQL.Add('28|"Tabelas de Roraima"|"RR"');
    FListaSQL.Add('29|"Tabelas de Rond�nia"|"RO"');
    FListaSQL.Add('30|"Tabelas de Santa Catarina"|"SC"');
    FListaSQL.Add('31|"Tabelas de S�o Paulo"|"SP"');
    FListaSQL.Add('32|"Tabelas de Sergipe"|"SE"');
    FListaSQL.Add('33|"Tabelas de Tocantins"|"TO"');
    //
    FListaSQL.Add('67|"Tabelas Globais"|""');
    //

  end else
  if Uppercase(Tabela) = Uppercase('LstSpedCodsTabT') then
  begin
    FListaSQL.Add('Controle|Codigo|Nome|TabelaTXT');
    FListaSQL.Add('1|1|"Tabela de Produtos para Combust�veis / Solvente"|""');
    FListaSQL.Add('2|1|"Tabela C�digo Fiscal de Opera��o e Presta��o - CFOP"|""');
    FListaSQL.Add('3|1|"Tabela G�nero do Item de Mercadoria/Servi�o"|""');
    FListaSQL.Add('4|1|"Tabela de Munc�pios do IBGE"|""');
    FListaSQL.Add('5|1|"5.4 - Tabela de C�digos das Obriga��es do ICMS a Recolher"|""');
    FListaSQL.Add('6|1|"Tabela de Pa�ses do IBGE"|""');
    FListaSQL.Add('7|1|"Tipo do Conhecimento de Transporte"|""');
    FListaSQL.Add('8|1|"4.4.6- Tabela de C�digos dos Totalizadores Parciais da REDU��O Z"|""');
    FListaSQL.Add('9|1|"UF C�digo IBGE - Sigla"|""');
    FListaSQL.Add('10|1|"UF Sigla"|""');
    FListaSQL.Add('11|1|"3.1.1 - Tabela Vers�o do Leiaute - Sped Fiscal"|""');
    FListaSQL.Add('12|1|"Tipo de Conhecimento de Transporte Eletr�nico"|""');
    FListaSQL.Add('13|1|"Tabela de Pa�ses SISCOMEX"|""');
    //
    FListaSQL.Add('189|11|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    FListaSQL.Add('190|11|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('245|11|"5.2 - Tabela de Informa��es Adicionais da Apura��o - Valores Declarat�rios - BA"|""');
    FListaSQL.Add('408|11|"5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS - BA"|""');
    //
    FListaSQL.Add('46|21|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('47|21|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    FListaSQL.Add('48|21|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('49|21|"5.5-  Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS"|""');
    FListaSQL.Add('125|21|"5.4- Tabela de C�digos das Obriga��es  de ICMS a Recolher"|""');
    FListaSQL.Add('195|21|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('33|8|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('34|8|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    FListaSQL.Add('35|8|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('196|8|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('16|3|"Tabela de Indicador de Emitente"|""');
    FListaSQL.Add('17|3|"Tabela Documentos Fiscais do ICMS"|""');
    FListaSQL.Add('18|3|"4.1.2- Tabela Situa��o do Documento"|""');
    //
    FListaSQL.Add('28|6|"4.4.2- Tabela Classes de Consumo de �gua Canalizada"|""');
    FListaSQL.Add('29|6|"4.4.3- Tabela Classes de Consumo de G�s Canalizado"|""');
    FListaSQL.Add('30|6|"4.4.4- Tabela Classes de Consumo dos Servi�os de Comunica��o e Telecomunica��o"|""');
    FListaSQL.Add('31|6|"4.4.1- Tabela Classifica��o de Itens de Energia El�trica, Servi�os de Comunica��o e Telecomunica��o"|""');
    FListaSQL.Add('32|6|"4.4.5 - Tabela - Faixa de Consumo de Energia El�trica"|""');
    //
    FListaSQL.Add('37|15|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('38|15|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    FListaSQL.Add('39|15|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('40|15|"5.5-  Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS"|""');
    FListaSQL.Add('187|15|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('43|19|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('44|19|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    FListaSQL.Add('45|19|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('194|19|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    FListaSQL.Add('248|19|"5.5-  Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS - MG"|""');
    FListaSQL.Add('249|19|"Tabela de Itens UF �ndice de Participa��o dos Munic�pios MG"|""');
    //
    FListaSQL.Add('212|22|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    //
    FListaSQL.Add('53|29|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('179|29|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('185|29|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('54|30|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('55|30|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('183|30|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('247|31|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS - SP"|""');
    FListaSQL.Add('598|31|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal SP"|""');
    FListaSQL.Add('246|31|"Tabela de Itens UF �ndice de Participa��o dos Munic�pios"|""');
    //
    FListaSQL.Add('56|32|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('57|32|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('58|32|"5.5-  Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS"|""');
    FListaSQL.Add('182|32|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('23|5|"Tabela C�digo de Situa��o Tribut�ria da COFINS"|""');
    FListaSQL.Add('26|5|"Tabela C�digo de Tributa��o do IPI - CST_IPI"|""');
    FListaSQL.Add('27|5|"Tabela C�digo de Situa��o Tribut�ria do PIS"|""');
    FListaSQL.Add('130|5|"Tabela C�digo da Situa��o Tribut�ria - CST (ICMS)"|""');
    //
    FListaSQL.Add('59|33|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('60|33|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    //
    FListaSQL.Add('188|9|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    FListaSQL.Add('220|9|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('221|9|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios - AM"|""');
    FListaSQL.Add('222|9|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal - AM"|""');
    //
    FListaSQL.Add('36|12|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('197|12|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('171|14|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('172|14|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('193|14|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    FListaSQL.Add('555|14|"Tabela de Itens UF �ndice de Participa��o dos Munic�pios ES"|""');
    //
    FListaSQL.Add('19|4|"4.5.4 - Tabela C�digo de Ajuste da Apura��o do IPI"|""');
    FListaSQL.Add('20|4|"4.5.1- Tabela de C�digos da Classe de Enquadramento do IPI"|""');
    FListaSQL.Add('21|4|"Tabela C�digo de Enquadramento Legal do IPI"|""');
    FListaSQL.Add('22|4|"4.5.2- Tabela de C�digo de Selo de Controle"|""');
    //
    FListaSQL.Add('122|16|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('121|16|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    //
    FListaSQL.Add('131|17|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('170|17|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    //
    FListaSQL.Add('41|18|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('42|18|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('192|18|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('123|20|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('124|20|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('186|20|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('50|23|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('51|23|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('191|23|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('127|24|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    //
    FListaSQL.Add('52|25|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('129|25|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('244|25|"5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS - RJ"|""');
    //
    FListaSQL.Add('126|26|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('128|26|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('223|26|"5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS - RN"|""');
    FListaSQL.Add('224|26|"Tabela de C�digos de Receita RN"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('173|27|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('175|27|"5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS"|""');
    FListaSQL.Add('176|27|"5.3 - Tabela de Ajustes e Informa��es de Valores Provenientes de Documento Fiscal"|""');
    FListaSQL.Add('180|27|"5.2 - Tabela de Iinforma��es Adicionais da Apura��o - Valores Declarat�rios"|""');
    FListaSQL.Add('184|27|"Tabela de C�digos de Receita"|"' + CO_TbTxt_REC_REF_OBRIGACAO + '"');
    //
    FListaSQL.Add('14|2|"5.5- Tabela de Tipos de Utiliza��o dos Cr�ditos Fiscais - ICMS"|""');
    FListaSQL.Add('15|2|"5.1.1- Tabela de C�digos de Ajustes da Apura��o do ICMS"|""');
    FListaSQL.Add('181|2|"Tabela de C�digos de Receita - GNRE"|""');
    //
    FListaSQL.Add('132|67|"Tabela Vers�o do Leiaute - PIS/COFINS"|""');

  end else
(*
  if Uppercase(Tabela) = Uppercase('SPEDEFDICMSIPIvers') then
  begin
    FListaSQL.Add('COD_VER|NUM_VER|DT_INI|DT_FIN');
    FListaSQL.Add('2|"1.01"|"2009-01-01"|"2009-12-31"');
    FListaSQL.Add('3|"1.02"|"2010-01-01"|"2010-12-31"');
    FListaSQL.Add('4|"1.03"|"2011-01-01"|"0000-00-00"');
  end else
*)
end;

function TSPED_Geral_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades","Cadastro de pessoas f�sicas e jur�dicas (clientes, fornecedores, etc.)"');
    //FListaSQL.Add('"",""');
  end;
end;


function TSPED_Geral_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('LstSpedCodsTabG') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('LstSpedCodsTabT') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
(* Duplicado no SPED_EFD_II_Tabs*)
  if Uppercase(Tabela) = Uppercase('tbSpedCods001') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'CodTxt';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'DataIni';
    FLIndices.Add(FRIndices);
    //
  end else
(*dim duplicado*)
end;

function TSPED_Geral_Tabs.CarregaListaFRQeiLnk(TabelaCria: String;
  FRQeiLnk: TQeiLnk; FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TSPED_Geral_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    if Uppercase(Tabela) = Uppercase('LstSpedCodsTabG') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UF';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('LstSpedCodsTabT') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TabelaTXT';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Versao';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
(* Duplicado no SPED_EFD_II_Tabs*)
    if Uppercase(Tabela) = Uppercase('tbSpedCods001') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'CodTxt';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataIni';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataFim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
(* fim duplicado *)
  except
    raise;
    Result := False;
  end;
end;

function TSPED_Geral_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // ini 2022-04-16 - deesmarcado nesta data! ver o que realmente vai usar!
  //
  // SPE-GERAL-000 :: Download de Tabelas SPED
  New(FRJanelas);
  FRJanelas.ID        := 'SPE-GERAL-000';
  FRJanelas.Nome      := 'FmSPED_EFD_DownTabs';
  FRJanelas.Descricao := 'Download de Tabelas SPED';
  FRJanelas.Modulo    := 'SPED';
  FLJanelas.Add(FRJanelas);
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
