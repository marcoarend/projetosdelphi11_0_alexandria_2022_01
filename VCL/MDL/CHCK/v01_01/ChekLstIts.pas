unit ChekLstIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, frxClass, dmkImage, UnDmkEnums;

type
  TFmChekLstIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label8: TLabel;
    EdControle: TdmkEdit;
    Label10: TLabel;
    EdOrdem: TdmkEdit;
    EdNome: TdmkEdit;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmChekLstIts: TFmChekLstIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, Principal, MyDBCheck,
ChekLstCab;

{$R *.DFM}

procedure TFmChekLstIts.BtOKClick(Sender: TObject);
var
  Controle, OrdemAtual: Integer;
begin
  if Trim(EdNome.Text) = '' then
  begin
    Application.MessageBox('Informe a descri��o do item!', 'Aviso',
    MB_YESNOCANCEL+MB_ICONWARNING);
    EdNome.SetFocus;
    Exit;
  end;
  Controle := UMyMod.BuscaEmLivreY_Def_Geral('cheklstits', 'Controle', ImgTipo.SQLType,
  EdControle.ValueVariant);
  EdControle.ValueVariant := Controle;
  //
  if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, 'cheklstits',
  Controle, Dmod.QrUpd) then
  begin
    if ImgTipo.SQLType = stIns then
      OrdemAtual := 0
    else
      OrdemAtual := FmChekLstCab.QrChekLstItsOrdem.Value;
    FmChekLstCab.CorrigeOrdem(Controle, OrdemAtual,
      EdOrdem.ValueVariant, FmChekLstCab.QrChekLstCabCodigo.Value);
    FmChekLstCab.ReopenChekLstIts(Controle);
    if CkContinuar.Checked then
    begin
      if ImgTipo.SQLType = stIns then
      begin
        EdOrdem.ValMax          := FormatFloat('0', FmChekLstCab.QrChekLstIts.RecordCount + 1);
        EdOrdem.ValueVariant    := FmChekLstCab.QrChekLstIts.RecordCount + 1;
      end;
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant   := 0;
      EdNome.ValueVariant       := '';
      EdOrdem.SetFocus;
    end else Close;
  end;
end;

procedure TFmChekLstIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChekLstIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmChekLstIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  // Vale para stUpd tamb�m?
  EdOrdem.ValMax := FormatFloat('0', FmChekLstCab.QrChekLstIts.RecordCount + 1);
  //
end;

procedure TFmChekLstIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
