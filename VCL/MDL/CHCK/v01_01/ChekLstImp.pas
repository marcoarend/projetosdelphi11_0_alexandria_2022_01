unit ChekLstImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, dmkRadioGroup, frxClass,
  frxDBSet, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmChekLstImp = class(TForm)
    Panel1: TPanel;
    RGPeriodic: TdmkRadioGroup;
    Panel3: TPanel;
    EdColunas: TdmkEdit;
    Label1: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    Label2: TLabel;
    frxDsChekLstCab: TfrxDBDataset;
    frxChekLst_01: TfrxReport;
    frxDsChekLstIts: TfrxDBDataset;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGPeriodicClick(Sender: TObject);
    procedure frxChekLst_01GetValue(const VarName: string; var Value: Variant);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmChekLstImp: TFmChekLstImp;

implementation

uses UnMyObjects, ChekLstCab;

{$R *.DFM}

procedure TFmChekLstImp.BtOKClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxChekLst_01, 'Checklist');
end;

procedure TFmChekLstImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChekLstImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmChekLstImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TPDataIni.Date := Date;
  frxDsChekLstCab.DataSet := FmChekLstCab.QrChekLstCab;
  frxDsChekLstIts.DataSet := FmChekLstCab.QrChekLstIts;
end;

procedure TFmChekLstImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmChekLstImp.frxChekLst_01GetValue(const VarName: string;
  var Value: Variant);
  function Valor(Indice: Integer): String;
  var
    I: Integer;
    Val: TDateTime;
  begin
    Val := 0;
    //DecodeDate(TPDataIni.Date, Ano, Mes, Dia);
    I := Indice - 1;
    case RGPeriodic.ItemIndex of
      1: (*A*) Val := IncMonth(TPDataIni.Date, I * 12);
      2: (*M*) Val := IncMonth(TPDataIni.Date, I);
      3: (*S*) Val := TPDataIni.Date + I * 7;
      4: (*D*) Val := TPDataIni.Date + I;
    end;
    case RGPeriodic.ItemIndex of
      1: (*A*) Result := FormatDateTime('YYYY', Val);
      2: (*M*) Result := FormatDateTime('mmm' + sLineBreak + 'YYYY', Val);
      3: (*S*) Result := FormatDateTime('dd' + sLineBreak + 'mm', Val);
      4: (*D*) Result := FormatDateTime('dd' + sLineBreak + 'mm', Val);
    end;
  end;
var
  Col: Integer;
begin
  Col := EdColunas.ValueVariant;
  //
  if AnsiCompareText(VarName, 'C01') = 0 then Value := Valor(01) else
  if AnsiCompareText(VarName, 'C02') = 0 then Value := Valor(02) else
  if AnsiCompareText(VarName, 'C03') = 0 then Value := Valor(03) else
  if AnsiCompareText(VarName, 'C04') = 0 then Value := Valor(04) else
  if AnsiCompareText(VarName, 'C05') = 0 then Value := Valor(05) else
  if AnsiCompareText(VarName, 'C06') = 0 then Value := Valor(06) else
  if AnsiCompareText(VarName, 'C07') = 0 then Value := Valor(07) else
  if AnsiCompareText(VarName, 'C08') = 0 then Value := Valor(08) else
  if AnsiCompareText(VarName, 'C09') = 0 then Value := Valor(09) else
  if AnsiCompareText(VarName, 'C10') = 0 then Value := Valor(10) else
  if AnsiCompareText(VarName, 'C11') = 0 then Value := Valor(11) else
  if AnsiCompareText(VarName, 'C12') = 0 then Value := Valor(12) else
  if AnsiCompareText(VarName, 'C13') = 0 then Value := Valor(13) else
  if AnsiCompareText(VarName, 'C14') = 0 then Value := Valor(14) else
  if AnsiCompareText(VarName, 'C15') = 0 then Value := Valor(15) else
  if AnsiCompareText(VarName, 'C16') = 0 then Value := Valor(16) else
  if AnsiCompareText(VarName, 'C17') = 0 then Value := Valor(17) else
  if AnsiCompareText(VarName, 'C18') = 0 then Value := Valor(18) else
  if AnsiCompareText(VarName, 'C19') = 0 then Value := Valor(19) else
  if AnsiCompareText(VarName, 'C20') = 0 then Value := Valor(20) else
  if AnsiCompareText(VarName, 'C21') = 0 then Value := Valor(21) else
  if AnsiCompareText(VarName, 'C22') = 0 then Value := Valor(22) else
  if AnsiCompareText(VarName, 'C23') = 0 then Value := Valor(23) else
  if AnsiCompareText(VarName, 'C24') = 0 then Value := Valor(24) else

  if AnsiCompareText(VarName, 'V01') = 0 then Value := Col >= 01 else
  if AnsiCompareText(VarName, 'V02') = 0 then Value := Col >= 02 else
  if AnsiCompareText(VarName, 'V03') = 0 then Value := Col >= 03 else
  if AnsiCompareText(VarName, 'V04') = 0 then Value := Col >= 04 else
  if AnsiCompareText(VarName, 'V05') = 0 then Value := Col >= 05 else
  if AnsiCompareText(VarName, 'V06') = 0 then Value := Col >= 06 else
  if AnsiCompareText(VarName, 'V07') = 0 then Value := Col >= 07 else
  if AnsiCompareText(VarName, 'V08') = 0 then Value := Col >= 08 else
  if AnsiCompareText(VarName, 'V09') = 0 then Value := Col >= 09 else
  if AnsiCompareText(VarName, 'V10') = 0 then Value := Col >= 10 else
  if AnsiCompareText(VarName, 'V11') = 0 then Value := Col >= 11 else
  if AnsiCompareText(VarName, 'V12') = 0 then Value := Col >= 12 else
  if AnsiCompareText(VarName, 'V13') = 0 then Value := Col >= 13 else
  if AnsiCompareText(VarName, 'V14') = 0 then Value := Col >= 14 else
  if AnsiCompareText(VarName, 'V15') = 0 then Value := Col >= 15 else
  if AnsiCompareText(VarName, 'V16') = 0 then Value := Col >= 16 else
  if AnsiCompareText(VarName, 'V17') = 0 then Value := Col >= 17 else
  if AnsiCompareText(VarName, 'V18') = 0 then Value := Col >= 18 else
  if AnsiCompareText(VarName, 'V19') = 0 then Value := Col >= 19 else
  if AnsiCompareText(VarName, 'V20') = 0 then Value := Col >= 20 else
  if AnsiCompareText(VarName, 'V21') = 0 then Value := Col >= 21 else
  if AnsiCompareText(VarName, 'V22') = 0 then Value := Col >= 22 else
  if AnsiCompareText(VarName, 'V23') = 0 then Value := Col >= 23 else
  if AnsiCompareText(VarName, 'V24') = 0 then Value := Col >= 24 else
  ;
end;

procedure TFmChekLstImp.RGPeriodicClick(Sender: TObject);
begin
  BtOK.Enabled := RGPeriodic.ItemIndex > 0;
end;

end.
