object FmChekLstImp: TFmChekLstImp
  Left = 339
  Top = 185
  Caption = 'LST-CHECK-003 :: Impress'#227'o de Check List'
  ClientHeight = 257
  ClientWidth = 433
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 433
    Height = 95
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 233
    object RGPeriodic: TdmkRadioGroup
      Left = 0
      Top = 0
      Width = 433
      Height = 45
      Align = alTop
      Caption = ' Periodicidade: '
      Columns = 5
      ItemIndex = 0
      Items.Strings = (
        '0 - A definir'
        '1 - Anual'
        '2 - Mensal '
        '3 - Semanal '
        '4 - Di'#225'rio')
      TabOrder = 0
      OnClick = RGPeriodicClick
      QryCampo = 'Periodic'
      UpdCampo = 'Periodic'
      UpdType = utYes
      OldValor = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 431
    end
    object Panel3: TPanel
      Left = 0
      Top = 45
      Width = 433
      Height = 54
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 1
      ExplicitTop = 46
      ExplicitWidth = 431
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label2: TLabel
        Left = 132
        Top = 8
        Width = 41
        Height = 13
        Caption = 'Colunas:'
      end
      object EdColunas: TdmkEdit
        Left = 132
        Top = 24
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '0'
        ValMax = '24'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '24'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 24
      end
      object TPDataIni: TdmkEditDateTimePicker
        Left = 16
        Top = 24
        Width = 112
        Height = 21
        Date = 40693.638322650460000000
        Time = 40693.638322650460000000
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 143
    Width = 433
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitLeft = -351
    ExplicitTop = 378
    ExplicitWidth = 784
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 429
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 780
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 187
    Width = 433
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitLeft = -351
    ExplicitTop = 422
    ExplicitWidth = 784
    object PnSaiDesis: TPanel
      Left = 287
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 638
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 285
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 636
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 433
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitLeft = -351
    ExplicitWidth = 784
    object GB_R: TGroupBox
      Left = 385
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 736
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 337
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 688
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 302
        Height = 32
        Caption = 'Impress'#227'o de Check List'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 302
        Height = 32
        Caption = 'Impress'#227'o de Check List'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 302
        Height = 32
        Caption = 'Impress'#227'o de Check List'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object frxDsChekLstCab: TfrxDBDataset
    UserName = 'frxDsChekLstCab'
    CloseDataSource = False
    DataSet = FmChekLstCab.QrChekLstCab
    BCDToCurrency = False
    Left = 216
    Top = 108
  end
  object frxChekLst_01: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  Me_0_01.Visible := <V01>;'
      '  Me_0_02.Visible := <V02>;'
      '  Me_0_03.Visible := <V03>;'
      '  Me_0_04.Visible := <V04>;'
      '  Me_0_05.Visible := <V05>;'
      '  Me_0_06.Visible := <V06>;'
      '  Me_0_07.Visible := <V07>;'
      '  Me_0_08.Visible := <V08>;'
      '  Me_0_09.Visible := <V09>;'
      '  Me_0_10.Visible := <V10>;'
      '  Me_0_11.Visible := <V11>;'
      '  Me_0_12.Visible := <V12>;'
      '  Me_0_13.Visible := <V13>;'
      '  Me_0_14.Visible := <V14>;'
      '  Me_0_15.Visible := <V15>;'
      '  Me_0_16.Visible := <V16>;'
      '  Me_0_17.Visible := <V17>;'
      '  Me_0_18.Visible := <V18>;'
      '  Me_0_19.Visible := <V19>;'
      '  Me_0_20.Visible := <V20>;'
      '  Me_0_21.Visible := <V21>;'
      '  Me_0_22.Visible := <V22>;'
      '  Me_0_23.Visible := <V23>;'
      '  Me_0_24.Visible := <V24>;'
      '                                                  '
      '  Me_1_01.Visible := <V01>;'
      '  Me_1_02.Visible := <V02>;'
      '  Me_1_03.Visible := <V03>;'
      '  Me_1_04.Visible := <V04>;'
      '  Me_1_05.Visible := <V05>;'
      '  Me_1_06.Visible := <V06>;'
      '  Me_1_07.Visible := <V07>;'
      '  Me_1_08.Visible := <V08>;'
      '  Me_1_09.Visible := <V09>;'
      '  Me_1_10.Visible := <V10>;'
      '  Me_1_11.Visible := <V11>;'
      '  Me_1_12.Visible := <V12>;'
      '  Me_1_13.Visible := <V13>;'
      '  Me_1_14.Visible := <V14>;'
      '  Me_1_15.Visible := <V15>;'
      '  Me_1_16.Visible := <V16>;'
      '  Me_1_17.Visible := <V17>;'
      '  Me_1_18.Visible := <V18>;'
      '  Me_1_19.Visible := <V19>;'
      '  Me_1_20.Visible := <V20>;'
      '  Me_1_21.Visible := <V21>;'
      '  Me_1_22.Visible := <V22>;'
      '  Me_1_23.Visible := <V23>;'
      '  Me_1_24.Visible := <V24>;'
      '    '
      'end.')
    OnGetValue = frxChekLst_01GetValue
    Left = 188
    Top = 108
    Datasets = <
      item
        DataSet = frxDsChekLstCab
        DataSetName = 'frxDsChekLstCab'
      end
      item
        DataSet = frxDsChekLstIts
        DataSetName = 'frxDsChekLstIts'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 22.677167800000000000
        Top = 166.299320000000000000
        Width = 680.315400000000000000
        DataSet = frxDsChekLstIts
        DataSetName = 'frxDsChekLstIts'
        RowCount = 0
        object Memo21: TfrxMemoView
          Width = 136.063080000000000000
          Height = 22.677167800000000000
          ShowHint = False
          DataSet = frxDsChekLstIts
          DataSetName = 'frxDsChekLstIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8 = (
            '[frxDsChekLstIts."Ordem"]-[frxDsChekLstIts."Nome"]')
          ParentFont = False
          WordBreak = True
        end
        object Me_1_01: TfrxMemoView
          Left = 136.063080000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_02: TfrxMemoView
          Left = 158.740260000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_03: TfrxMemoView
          Left = 181.417440000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_04: TfrxMemoView
          Left = 204.094620000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_05: TfrxMemoView
          Left = 226.771800000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_06: TfrxMemoView
          Left = 249.448980000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_07: TfrxMemoView
          Left = 272.126160000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_08: TfrxMemoView
          Left = 294.803340000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_09: TfrxMemoView
          Left = 317.480520000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_10: TfrxMemoView
          Left = 340.157700000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_11: TfrxMemoView
          Left = 362.834880000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_12: TfrxMemoView
          Left = 385.512060000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_13: TfrxMemoView
          Left = 408.189240000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_14: TfrxMemoView
          Left = 430.866420000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_15: TfrxMemoView
          Left = 453.543600000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_16: TfrxMemoView
          Left = 476.220780000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_17: TfrxMemoView
          Left = 498.897960000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_18: TfrxMemoView
          Left = 521.575140000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_19: TfrxMemoView
          Left = 544.252320000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_20: TfrxMemoView
          Left = 566.929500000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_21: TfrxMemoView
          Left = 589.606680000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_22: TfrxMemoView
          Left = 612.283860000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_23: TfrxMemoView
          Left = 634.961040000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Me_1_24: TfrxMemoView
          Left = 657.638220000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
      end
      object PageHeader3: TfrxPageHeader
        Height = 86.929175350000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'PageHeader3OnBeforePrint'
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo18: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo36: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'P'#195#161'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'Checklist')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsChekLstCab."Nome"]')
          ParentFont = False
        end
        object Me_0_01: TfrxMemoView
          Left = 136.063080000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C01]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_02: TfrxMemoView
          Left = 158.740260000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C02]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_03: TfrxMemoView
          Left = 181.417440000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C03]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_04: TfrxMemoView
          Left = 204.094620000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C04]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_05: TfrxMemoView
          Left = 226.771800000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C05]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_06: TfrxMemoView
          Left = 249.448980000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C06]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_07: TfrxMemoView
          Left = 272.126160000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C07]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_08: TfrxMemoView
          Left = 294.803340000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C08]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_09: TfrxMemoView
          Left = 317.480520000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C09]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_10: TfrxMemoView
          Left = 340.157700000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C10]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_11: TfrxMemoView
          Left = 362.834880000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C11]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_12: TfrxMemoView
          Left = 385.512060000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C12]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_13: TfrxMemoView
          Left = 408.189240000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C13]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_14: TfrxMemoView
          Left = 430.866420000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C14]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_15: TfrxMemoView
          Left = 453.543600000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C15]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_16: TfrxMemoView
          Left = 476.220780000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C16]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_17: TfrxMemoView
          Left = 498.897960000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C17]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_18: TfrxMemoView
          Left = 521.575140000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C18]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_19: TfrxMemoView
          Left = 544.252320000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C19]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_20: TfrxMemoView
          Left = 566.929500000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C20]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_21: TfrxMemoView
          Left = 589.606680000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C21]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_22: TfrxMemoView
          Left = 612.283860000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C22]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Me_0_23: TfrxMemoView
          Left = 634.961040000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C23]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Top = 64.252010000000000000
          Width = 136.063080000000000000
          Height = 22.677167800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8 = (
            'Item de checklist')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_0_24: TfrxMemoView
          Left = 657.638220000000000000
          Top = 64.252010000000000000
          Width = 22.677165350000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[C24]')
          ParentFont = False
          WordBreak = True
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118120000000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        object Memo39: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxDsChekLstIts: TfrxDBDataset
    UserName = 'frxDsChekLstIts'
    CloseDataSource = False
    DataSet = FmChekLstCab.QrChekLstIts
    BCDToCurrency = False
    Left = 244
    Top = 108
  end
end
