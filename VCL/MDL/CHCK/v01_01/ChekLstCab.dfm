object FmChekLstCab: TFmChekLstCab
  Left = 368
  Top = 194
  Caption = 'LST-CHECK-001 :: Check List'
  ClientHeight = 692
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 596
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PnCabeca: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 137
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      ParentBackground = False
      TabOrder = 0
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 137
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 156
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label3: TLabel
          Left = 72
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit1
        end
        object Label1: TLabel
          Left = 12
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label5: TLabel
          Left = 12
          Top = 44
          Width = 105
          Height = 13
          Caption = 'T'#237'tulo para impress'#227'o:'
          FocusControl = dmkDBEdit1
        end
        object DBEdNome: TdmkDBEdit
          Left = 156
          Top = 20
          Width = 585
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsChekLstCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit1: TDBEdit
          Left = 72
          Top = 20
          Width = 80
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsChekLstCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 12
          Top = 20
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsChekLstCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 2
          UpdType = utYes
          Alignment = taRightJustify
        end
        object dmkRadioGroup1: TDBRadioGroup
          Left = 12
          Top = 84
          Width = 729
          Height = 45
          Caption = ' Periodicidade: '
          Columns = 5
          DataField = 'Periodic'
          DataSource = DsChekLstCab
          Items.Strings = (
            '0 - A definir'
            '1 - Anual'
            '2 - Mensal '
            '3 - Semanal '
            '4 - Di'#225'rio')
          ParentBackground = True
          TabOrder = 3
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 12
          Top = 60
          Width = 729
          Height = 21
          Color = clWhite
          DataField = 'TituloImp'
          DataSource = DsChekLstCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 137
      Width = 1008
      Height = 316
      ActivePage = TabSheet2
      Align = alTop
      TabOrder = 1
      object TabSheet2: TTabSheet
        Caption = ' Itens do checklist'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 288
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 288
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel7'
            TabOrder = 0
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 1000
              Height = 288
              Align = alClient
              DataSource = DsChekLstIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ordem'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o do item'
                  Width = 913
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 532
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 412
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 596
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 141
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 12
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 72
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
        Enabled = False
      end
      object Label9: TLabel
        Left = 156
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 12
        Top = 44
        Width = 105
        Height = 13
        Caption = 'T'#237'tulo para impress'#227'o:'
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 156
        Top = 20
        Width = 584
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 72
        Top = 20
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object RGPeriodic: TdmkRadioGroup
        Left = 12
        Top = 92
        Width = 729
        Height = 45
        Caption = ' Periodicidade: '
        Columns = 5
        ItemIndex = 0
        Items.Strings = (
          '0 - A definir'
          '1 - Anual'
          '2 - Mensal '
          '3 - Semanal '
          '4 - Di'#225'rio')
        TabOrder = 3
        QryCampo = 'Periodic'
        UpdCampo = 'Periodic'
        UpdType = utYes
        OldValor = 0
      end
      object EdTituloImp: TdmkEdit
        Left = 12
        Top = 60
        Width = 729
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'TituloImp'
        UpdCampo = 'TituloImp'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 533
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object CGAplicacao: TdmkCheckGroup
      Left = 12
      Top = 144
      Width = 729
      Height = 105
      Caption = ' Aplica'#231#227'o: '
      TabOrder = 2
      QryCampo = 'Aplicacao'
      UpdCampo = 'Aplicacao'
      UpdType = utYes
      Value = 0
      OldValor = 0
    end
  end
  object Panel8: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 128
        Height = 32
        Caption = 'Check List'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 128
        Height = 32
        Caption = 'Check List'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 128
        Height = 32
        Caption = 'Check List'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsChekLstCab: TDataSource
    DataSet = QrChekLstCab
    Left = 40
    Top = 12
  end
  object QrChekLstCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrChekLstCabBeforeOpen
    AfterOpen = QrChekLstCabAfterOpen
    BeforeClose = QrChekLstCabBeforeClose
    AfterScroll = QrChekLstCabAfterScroll
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, Periodic'
      'FROM cheklstcab')
    Left = 12
    Top = 12
    object QrChekLstCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrChekLstCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrChekLstCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrChekLstCabPeriodic: TIntegerField
      FieldName = 'Periodic'
    end
    object QrChekLstCabAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrChekLstCabTituloImp: TWideStringField
      FieldName = 'TituloImp'
      Size = 255
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Panel2
    CanUpd01 = BtInclui
    CanDel01 = BtAltera
    Left = 68
    Top = 12
  end
  object PMInclui: TPopupMenu
    OnPopup = PMIncluiPopup
    Left = 460
    Top = 612
    object Checklistnovo1: TMenuItem
      Caption = '&Checklist novo'
      OnClick = Checklistnovo1Click
    end
    object DuplicaChecklistatual1: TMenuItem
      Caption = '&Duplica Checklist atual'
      OnClick = DuplicaChecklistatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Itemparaochecklistatual1: TMenuItem
      Caption = '&Item para o checklist atual'
      OnClick = Itemparaochecklistatual1Click
    end
  end
  object QrChekLstIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM cheklstits cli'
      'WHERE Codigo=:P0'
      'ORDER BY Ordem, Controle')
    Left = 288
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrChekLstItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrChekLstItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrChekLstItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrChekLstItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsChekLstIts: TDataSource
    DataSet = QrChekLstIts
    Left = 316
    Top = 160
  end
  object PMAltera: TPopupMenu
    OnPopup = PMAlteraPopup
    Left = 556
    Top = 608
    object Checklistatual1: TMenuItem
      Caption = '&Checklist atual'
      OnClick = Checklistatual1Click
    end
    object Itemdechecklistselecionado1: TMenuItem
      Caption = '&Item de checklist selecionado'
      Enabled = False
      OnClick = Itemdechecklistselecionado1Click
    end
  end
  object PMExclui: TPopupMenu
    OnPopup = PMExcluiPopup
    Left = 652
    Top = 608
    object Checklistatual2: TMenuItem
      Caption = '&Checklist atual'
      Enabled = False
      OnClick = Checklistatual2Click
    end
    object Itemdechecklistselecionado2: TMenuItem
      Caption = '&Item de checklist selecionado'
      Enabled = False
      OnClick = Itemdechecklistselecionado2Click
    end
  end
end
