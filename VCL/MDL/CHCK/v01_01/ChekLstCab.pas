unit ChekLstCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Menus, ComCtrls, Grids,
  DBGrids, dmkRadioGroup, UnDmkProcFunc, dmkImage, dmkCheckGroup, UnDmkEnums,
  {$IfDef CO_DMKID_APP_0024} UnApp_Consts, {$EndIf} UnProjGroup_Consts;

type
  TFmChekLstCab = class(TForm)
    PainelDados: TPanel;
    DsChekLstCab: TDataSource;
    QrChekLstCab: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrChekLstCabCodigo: TIntegerField;
    QrChekLstCabCodUsu: TIntegerField;
    QrChekLstCabNome: TWideStringField;
    PMInclui: TPopupMenu;
    QrChekLstIts: TmySQLQuery;
    DsChekLstIts: TDataSource;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    PMAltera: TPopupMenu;
    Panel6: TPanel;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label1: TLabel;
    Panel7: TPanel;
    DBGrid1: TDBGrid;
    PMExclui: TPopupMenu;
    Checklistnovo1: TMenuItem;
    Itemparaochecklistatual1: TMenuItem;
    Checklistatual1: TMenuItem;
    Itemdechecklistselecionado1: TMenuItem;
    Checklistatual2: TMenuItem;
    Itemdechecklistselecionado2: TMenuItem;
    QrChekLstItsCodigo: TIntegerField;
    QrChekLstItsOrdem: TIntegerField;
    QrChekLstItsControle: TIntegerField;
    QrChekLstItsNome: TWideStringField;
    RGPeriodic: TdmkRadioGroup;
    dmkRadioGroup1: TDBRadioGroup;
    QrChekLstCabPeriodic: TIntegerField;
    Panel8: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel9: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    CGAplicacao: TdmkCheckGroup;
    QrChekLstCabAplicacao: TIntegerField;
    EdTituloImp: TdmkEdit;
    Label4: TLabel;
    QrChekLstCabTituloImp: TWideStringField;
    Label5: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    N1: TMenuItem;
    DuplicaChecklistatual1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrChekLstCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrChekLstCabBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrChekLstCabBeforeClose(DataSet: TDataSet);
    procedure PMAlteraPopup(Sender: TObject);
    procedure QrChekLstCabAfterScroll(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure Checklistnovo1Click(Sender: TObject);
    procedure Itemparaochecklistatual1Click(Sender: TObject);
    procedure PMIncluiPopup(Sender: TObject);
    procedure Checklistatual1Click(Sender: TObject);
    procedure Itemdechecklistselecionado1Click(Sender: TObject);
    procedure Checklistatual2Click(Sender: TObject);
    procedure Itemdechecklistselecionado2Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure DuplicaChecklistatual1Click(Sender: TObject);
  private
    { Private declarations }
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    //
    procedure ReopenChekLstIts(Controle: Integer);
    procedure CorrigeOrdem(Controle, OrdemAtual, NovaOrdem, Codigo: Integer);
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmChekLstCab: TFmChekLstCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, ChekLstIts, ChekLstImp, UnAppListas,
  DmkDAC_PF, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmChekLstCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmChekLstCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrChekLstCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmChekLstCab.DefParams;
begin
  VAR_GOTOTABELA := 'cheklstcab';
  VAR_GOTOMYSQLTABLE := QrChekLstCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  //VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome, Periodic, Aplicacao');
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cheklstcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmChekLstCab.DuplicaChecklistatual1Click(Sender: TObject);
var
  Codigo, CodUsu, Controle: Integer;
  Nome: String;
begin
  QrChekLstCab.DisableControls;
  QrChekLstIts.DisableControls;
  Codigo := UMyMod.BPGS1I32('cheklstcab', 'Codigo', '', '', tsDef, stIns, 0);
  CodUsu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'cheklstcab', 'CodUsu', [], [], stIns,
    0, siPositivo, nil);
  //
  Nome := QrChekLstCabNome.Value + ' (c�pia)';
  if UnDmkDAC_PF.InsereRegistrosPorCopia(
  DModG.QrUpdPID1, 'cheklstcab', TMeuDB,
  ['Codigo'], [QrChekLstCabCodigo.Value],
  ['Codigo', 'CodUsu', 'Nome'], [Codigo, CodUsu, Nome], '', True, LaAviso1, LaAviso2) then
  begin
    try
      QrChekLstIts.First;
      while not QrChekLstIts.Eof do
      begin
        Controle := UMyMod.BuscaEmLivreY_Def_Geral('cheklstits', 'Controle',
        stIns, 0);
        //
        if UnDmkDAC_PF.InsereRegistrosPorCopia(
        DModG.QrUpdPID1, 'cheklstits', TMeuDB,
        ['Controle'], [QrChekLstItsControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2) then
        //
        QrChekLstIts.Next;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    finally
      QrChekLstCab.EnableControls;
      QrChekLstIts.EnableControls;
      LocCod(Codigo, Codigo);
    end;
  end;
end;

procedure TFmChekLstCab.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'cheklstcab',
    'CodUsu', [], [], stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmChekLstCab.Itemparaochecklistatual1Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  if UmyMod.FormInsUpd_Cria(TFmChekLstIts, FmChekLstIts, afmoNegarComAviso,
  QrChekLstIts, stIns) then
  begin
    FmChekLstIts.EdOrdem.ValueVariant := QrChekLstIts.RecordCount + 1;
    FmChekLstIts.ShowModal;
    FmChekLstIts.Destroy;
  end;
end;

procedure TFmChekLstCab.Itemdechecklistselecionado1Click(Sender: TObject);
begin
  if UmyMod.FormInsUpd_Cria(TFmChekLstIts, FmChekLstIts, afmoNegarComAviso,
    QrChekLstIts, stUpd)then
  begin
    FmChekLstIts.CkContinuar.Checked := False;
    //
    FmChekLstIts.ShowModal;
    FmChekLstIts.Destroy;
  end;
end;

procedure TFmChekLstCab.Itemdechecklistselecionado2Click(Sender: TObject);
begin
  UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item?', 'cheklstits',
    'Controle', QrChekLstItsControle.Value, Dmod.MyDB);
  CorrigeOrdem(0, 0, 0, QrChekLstCabCodigo.Value);
  ReopenChekLstIts(QrChekLstItsControle.Value);
end;

procedure TFmChekLstCab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmChekLstCab.PMAlteraPopup(Sender: TObject);
begin
  Checklistatual1.Enabled :=
    (QrChekLstCab.State <> dsInactive)
  and
    (QrChekLstCab.RecordCount > 0);
  //

  Itemdechecklistselecionado1.Enabled :=
    (PageControl1.ActivePageIndex = 0)
  and
    (QrChekLstIts.State <> dsInactive)
  and
    (QrChekLstIts.RecordCount > 0);
end;

procedure TFmChekLstCab.PMExcluiPopup(Sender: TObject);
begin
  Checklistatual1.Enabled :=
    (QrChekLstCab.State <> dsInactive)
  and
    (QrChekLstCab.RecordCount > 0);
  //

  Itemdechecklistselecionado2.Enabled :=
    (PageControl1.ActivePageIndex = 0)
  and
    (QrChekLstIts.State <> dsInactive)
  and
    (QrChekLstIts.RecordCount > 0);
end;

procedure TFmChekLstCab.PMIncluiPopup(Sender: TObject);
begin
  Itemparaochecklistatual1.Enabled :=
    (QrChekLstCab.State <> dsInactive)
  and
    (QrChekLstCab.RecordCount > 0);
end;

procedure TFmChekLstCab.Checklistatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrChekLstCab, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'cheklstcab');
end;

procedure TFmChekLstCab.Checklistatual2Click(Sender: TObject);
begin
  UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do checklist?', 'cheklstcab',
    'Codigo', QrChekLstCabCodigo.Value, DMod.MyDB);
  LocCod(0,0);
end;

procedure TFmChekLstCab.Checklistnovo1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrChekLstCab, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'cheklstcab');
end;

procedure TFmChekLstCab.CorrigeOrdem(Controle, OrdemAtual, NovaOrdem,
  Codigo: Integer);
var
  MyCursor: TCursor;
  Ordem: Integer;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if NovaOrdem > OrdemAtual then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE cheklstits SET Ordem=Ordem-1 ');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrUpd.SQL.Add('AND Ordem>:P1');
      Dmod.QrUpd.SQL.Add('AND Ordem<=:P2');
      Dmod.QrUpd.Params[00].AsInteger := Codigo;
      Dmod.QrUpd.Params[01].AsInteger := OrdemAtual;
      Dmod.QrUpd.Params[02].AsInteger := NovaOrdem;
      Dmod.QrUpd.ExecSQL;
    end;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE cheklstits SET Reordem=1 ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrUpd.Params[00].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE cheklstits SET Reordem=0, ');
    Dmod.QrUpd.SQL.Add('Ordem=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.Params[00].AsInteger := NovaOrdem;
    Dmod.QrUpd.Params[01].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Ordem, Reordem, Controle');
    Dmod.QrAux.SQL.Add('FROM cheklstits');
    Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrAux.SQL.Add('ORDER BY Ordem, Reordem, Controle');
    Dmod.QrAux.Params[00].AsInteger := Codigo;
    Dmod.QrAux.Open;
    //
    Ordem := 0;
    Dmod.QrAux.First;
    while not Dmod.QrAux.Eof do
    begin
      Ordem := Ordem + 1;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE cheklstits SET Reordem=0, ');
      Dmod.QrUpd.SQL.Add('Ordem=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
      Dmod.QrUpd.Params[00].AsInteger := Ordem;
      Dmod.QrUpd.Params[01].AsInteger := Dmod.QrAux.FieldByName('Controle').AsInteger;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrAux.Next;
    end;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TFmChekLstCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmChekLstCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmChekLstCab.ReopenChekLstIts(Controle: Integer);
begin
  QrChekLstIts.Close;
  QrChekLstIts.Params[0].AsInteger := QrChekLstCabCodigo.Value;
  QrChekLstIts.Open;
  //
  QrChekLstIts.Locate('Controle', Controle, []);
end;

procedure TFmChekLstCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmChekLstCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmChekLstCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmChekLstCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmChekLstCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmChekLstCab.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmChekLstCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrChekLstCabCodigo.Value;
  Close;
end;

procedure TFmChekLstCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def_Geral('cheklstcab', 'Codigo', ImgTipo.SQLType,
    QrChekLstCabCodigo.Value);
  EdCodUsu.ValueVariant := Codigo;  
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmChekLstCab, PainelEdit,
    'cheklstcab', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmChekLstCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'cheklstcab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cheklstcab', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cheklstcab', 'Codigo');
end;

procedure TFmChekLstCab.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmChekLstCab.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmChekLstCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 0;
  PageControl1.Align := alClient;
  PainelEdit.Align   := alClient;
  CriaOForm;
  //
  MyObjects.ConfiguraCheckGroup(CGAplicacao, sListaAplicacaoChekLstCab, 4, 0, True);
end;

procedure TFmChekLstCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrChekLstCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmChekLstCab.SbImprimeClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmChekLstImp, FmChekLstImp, afmoLiberado) then
  begin
    FmChekLstImp.RGPeriodic.ItemIndex := QrChekLstCabPeriodic.Value;
    FmChekLstImp.ShowModal;
    FmChekLstImp.Destroy;
  end;
end;

procedure TFmChekLstCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmChekLstCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrChekLstCabCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmChekLstCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmChekLstCab.QrChekLstCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmChekLstCab.QrChekLstCabAfterScroll(DataSet: TDataSet);
begin
  ReopenChekLstIts(0);
end;

procedure TFmChekLstCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmChekLstCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrChekLstCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cheklstcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmChekLstCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmChekLstCab.QrChekLstCabBeforeClose(DataSet: TDataSet);
begin
  QrChekLstIts.Close;
end;

procedure TFmChekLstCab.QrChekLstCabBeforeOpen(DataSet: TDataSet);
begin
  QrChekLstCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

