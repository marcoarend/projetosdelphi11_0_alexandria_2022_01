unit UnChekLstJan;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, dmkEdit, dmkDBLookupComboBox, dmkGeral,
  dmkEditCB, mySQLDBTables, UnDmkEnums, DmkDAC_PF, AdvToolBar;

type
  TUnChekLstJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraChekLstCab();
    procedure MostraChekLstImp();
    procedure MostraChekLstIts();
  end;

var
  ChekLstJan: TUnChekLstJan;

implementation

uses MyDBCheck, ChekLstCab, ChekLstImp, ChekLstIts;

procedure TUnChekLstJan.MostraChekLstCab();
begin
  if DBCheck.CriaFm(TFmChekLstCab, FmChekLstCab, afmoNegarComAviso) then
  begin
    FmChekLstCab.ShowModal;
    FmChekLstCab.Destroy;
  end;
end;

procedure TUnChekLstJan.MostraChekLstImp();
begin
  if DBCheck.CriaFm(TFmChekLstImp, FmChekLstImp, afmoNegarComAviso) then
  begin
    FmChekLstImp.ShowModal;
    FmChekLstImp.Destroy;
  end;
end;

procedure TUnChekLstJan.MostraChekLstIts();
begin
  if DBCheck.CriaFm(TFmChekLstIts, FmChekLstIts, afmoNegarComAviso) then
  begin
    FmChekLstIts.ShowModal;
    FmChekLstIts.Destroy;
  end;
end;

end.
