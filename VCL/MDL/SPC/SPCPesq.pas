unit SPCPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Sockets;

type
  TFmSPCPesq = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    host: TEdit;
    Conectar: TButton;
    Desconectar: TButton;
    Port: TComboBox;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Codigo: TEdit;
    Senha: TEdit;
    Consulta: TEdit;
    Sock: TTcpClient;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Nome: TEdit;
    Nascimento: TEdit;
    CPF: TEdit;
    RG: TEdit;
    UF: TEdit;
    Banco: TEdit;
    Agencia: TEdit;
    Conta: TEdit;
    Cheque: TEdit;
    Qtde: TEdit;
    BtConsultar: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox4: TGroupBox;
    Transacao: TMemo;
    GroupBox5: TGroupBox;
    Resposta: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ConectarClick(Sender: TObject);
    procedure DesconectarClick(Sender: TObject);
    procedure BtConsultarClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
    function  ZeroFill(Numero: String; Tamanho: Byte): String;
    function  SpaceFill(Numero: String; Tamanho: Byte): String;
  public
    { Public declarations }
  end;

  var
  FmSPCPesq: TFmSPCPesq;

implementation

{$R *.DFM}

function TFmSPCPesq.ZeroFill(Numero: String; Tamanho: Byte): String;
Var
  i : Byte;
begin
  for i:=1 to Tamanho - Length(Numero) do
    Numero := '0' + Numero;
  Result := Numero;
end;

function TFmSPCPesq.SpaceFill(Numero: String; Tamanho: Byte): String;
Var
  i : Byte;
begin
  for I:=1 to Tamanho - Length(Numero) do
    Numero := Numero + ' ';
  Result := Numero;
end;

procedure TFmSPCPesq.BtConsultarClick(Sender: TObject);
var
  Trans, Resp: String;
  Flag: Boolean;
begin
  if not Sock.Active then
  begin
    Application.MessageBox('Voc� deve conectar-se antes', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  Trans := 'CSR10C04               ';
  Trans := Trans + ZeroFill(Codigo.Text,8) + SpaceFill(Senha.Text,8);
  Trans := Trans + SpaceFill(Consulta.Text,8);
  If Cpf.Text <> '' Then
    Trans := Trans + 'CPF' + ZeroFill(CPF.Text,11)  + '      '
  else
    Trans := Trans + '                    ';
  If Rg.Text <> '' Then
     Trans := Trans + 'RG'  + ZeroFill(RG.Text,11)  + SpaceFill(Uf.Text,2) + '     '
  else
    Trans := Trans + '                    ';
  Trans := Trans + '                    ';
  Trans := Trans + SpaceFill(Nome.Text,50);
  Trans := Trans + ZeroFill(Nascimento.Text,8) + '31122009';
  Trans := Trans + ZeroFill(Banco.Text,3) + ZeroFill(Agencia.Text,5);
  Trans := Trans + ZeroFill(Conta.Text,16) + ZeroFill(Cheque.Text,9);
  Trans := Trans + ZeroFill(Qtde.Text,1) + 'XX' + '00000000199';
  Trans := Trans + '                                                  ';
  Trans := Trans + '0000' + '000000000' + '00000' + '000' + ' ' +  '        ';


  Transacao.Text := '';
  Transacao.Lines.Add('Enviando '+IntToStr(StrLen(Pchar(Trans))) + ' bytes');
  Transacao.Lines.Add(Trans);

  BtConsultar.Caption := 'Aguarde ...';
  Sock.Sendln(Trans, #$D);
  Flag := False;
  Sock.WaitForData(60000);
  while not Flag do
    begin
    Resp := Sock.Receiveln(#$D);
    if Copy(Port.Text,1,4) = '9162' then
      begin
      Resposta.Lines.Add('Recebeu bloco de ' + IntToStr(StrLen(Pchar(Resp))) + ' bytes');
      Resposta.Lines.Add(Resp);
      Flag := True;
      end
    else
      begin
      Resposta.Lines.Add('Recebeu bloco de ' + IntToStr(StrLen(Pchar(Resp))) + ' bytes');
      case StrToInt(Copy(Resp,40,1)) of
        0: Flag := True;
        1:begin
          Sock.Sendln('',#$D);
          Flag := False;
          end;
        9: Flag := True;
      end; {Case}
      Resposta.Lines.Add(Resp);
    end;
  end; {While}
  BtConsultar.Caption := '&Consultar';
  Consulta.Enabled  := True;
end;

procedure TFmSPCPesq.BitBtn2Click(Sender: TObject);
begin
  Resposta.Text := '';
end;

procedure TFmSPCPesq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSPCPesq.ConectarClick(Sender: TObject);
begin
  Sock.RemoteHost     := Host.Text;
  Sock.RemotePort     := Copy(Port.Text,1,4);
  If not Sock.Active then
    Sock.Active       := True;
  Host.Enabled        := False;
  Port.Enabled        := False;
  Conectar.Enabled    := False;
  Desconectar.Enabled := True;
end;

procedure TFmSPCPesq.DesconectarClick(Sender: TObject);
begin
  if Sock.Active then
    Sock.Active       := False;
  Host.Enabled        := True;
  Port.Enabled        := True;
  Conectar.Enabled    := True;
  Desconectar.Enabled := False;
end;

procedure TFmSPCPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSPCPesq.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Sock.Active then
    Sock.Active := False;
end;

procedure TFmSPCPesq.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
