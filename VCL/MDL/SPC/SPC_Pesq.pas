unit SPC_Pesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkGeral, (*Sockets,*) dmkEdit,
  dmkEditCB, DBCtrls, dmkDBLookupComboBox, DB, mySQLDbTables, ComCtrls,
  dmkEditDateTimePicker, Mask, frxClass, dmkImage, UnDmkEnums;

type
  TFmSPC_Pesq = class(TForm)
    QrSPC_Config: TmySQLQuery;
    DsSPC_Config: TDataSource;
    QrSPC_ConfigCodigo: TIntegerField;
    QrSPC_ConfigNome: TWideStringField;
    QrSPC_ConfigServidor: TWideStringField;
    QrSPC_ConfigPorta: TSmallintField;
    QrSPC_ConfigPedinte: TWideStringField;
    QrSPC_ConfigCodigSocio: TIntegerField;
    QrSPC_ConfigSenhaSocio: TWideStringField;
    QrSPC_ConfigModalidade: TIntegerField;
    QrSPC_ConfigInfoExtra: TIntegerField;
    QrSPC_ConfigValMin: TFloatField;
    QrSPC_ConfigValMax: TFloatField;
    QrSPC_ConfigBAC_CMC7: TSmallintField;
    QrSPC_ConfigTipoCred: TSmallintField;
    QrSPC_ConfigValAviso: TFloatField;
    QrSPC_ConfigVALCONSULTA: TFloatField;
    QrSPC_ConfigNOMEMODALI: TWideStringField;
    QrSPC_Modali: TmySQLQuery;
    DataSource1: TDataSource;
    DsSPC_Modali: TDataSource;
    QrSPC_ModaliCodigo: TIntegerField;
    QrSPC_ModaliNome: TWideStringField;
    StringDS: TfrxUserDataSet;
    frxReport1: TfrxReport;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtConsultar: TBitBtn;
    BitBtn2: TBitBtn;
    BtImprime: TBitBtn;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    EdHost: TdmkEdit;
    CBPorta: TComboBox;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    SpeedButton1: TSpeedButton;
    EdCodigo: TdmkEdit;
    EdSenha: TdmkEdit;
    EdConsulta: TdmkEditCB;
    CBConsulta: TdmkDBLookupComboBox;
    GroupBox4: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label36: TLabel;
    Label18: TLabel;
    EdNome: TdmkEdit;
    EdCPF: TdmkEdit;
    EdRG: TdmkEdit;
    EdUF: TdmkEdit;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    EdCheque: TdmkEdit;
    EdQtde: TdmkEdit;
    EdValor: TdmkEdit;
    GroupBox6: TGroupBox;
    EdStatusSPC_TXT: TdmkEdit;
    EdStatusSPC: TdmkEdit;
    TPNascimento: TdmkEditDateTimePicker;
    EdBanda: TdmkEdit;
    GroupBox5: TGroupBox;
    MeTransacao: TMemo;
    GroupBox7: TGroupBox;
    MeResposta: TMemo;
    Panel4: TPanel;
    Label4: TLabel;
    SpeedButton2: TSpeedButton;
    CBConfig: TdmkDBLookupComboBox;
    EdConfig: TdmkEditCB;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConsultarClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdConfigChange(Sender: TObject);
    procedure EdStatusSPCChange(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure frxReport1GetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    //sl: TStringList;
    {
    function  ZeroFill(Numero: String; Tamanho: Byte): String;
    function  SpaceFill(Numero: String; Tamanho: Byte): String;
    }
  public
    { Public declarations }
  end;

  var
  FmSPC_Pesq: TFmSPC_Pesq;

implementation

uses UnMyObjects, Principal, Module, UnInternalConsts, UnitSPC, SPC_Modali, SPC_Config,
MyDBCheck;

{$R *.DFM}

{
function TFmSPC_Pesq.ZeroFill(Numero: String; Tamanho: Byte): String;
Var
  i : Byte;
begin
  for i:=1 to Tamanho - Length(Numero) do
    Numero := '0' + Numero;
  Result := Numero;
end;

function TFmSPC_Pesq.SpaceFill(Numero: String; Tamanho: Byte): String;
Var
  i : Byte;
begin
  for I:=1 to Tamanho - Length(Numero) do
    Numero := Numero + ' ';
  Result := Numero;
end;
}

procedure TFmSPC_Pesq.SpeedButton1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Modali, FmSPC_Modali, afmoNegarComAviso) then
  begin
    VAR_CADASTRO := 0;
    FmSPC_Modali.ShowModal;
    FmSPC_Modali.Destroy;
    QrSPC_Modali.Close;
    QrSPC_Modali.Open;
    if VAR_CADASTRO <> 0 then
    begin
      EdConsulta.ValueVariant := VAR_CADASTRO;
      CBConsulta.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmSPC_Pesq.SpeedButton2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Config, FmSPC_Config, afmoNegarComAviso) then
  begin
    VAR_CADASTRO := 0;
    FmSPC_Config.ShowModal;
    FmSPC_Config.Destroy;
    QrSPC_Config.Close;
    QrSPC_Config.Open;
    if VAR_CADASTRO <> 0 then
    begin
      EdConfig.ValueVariant := VAR_CADASTRO;
      CBConfig.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmSPC_Pesq.BtConsultarClick(Sender: TObject);
var
  Solicitante, Conta, Endereco, telefone, CEP: String;
  Nascimento: TDateTime;
  Cheque, DigitoCC, DigitoCH, Qtde, Banco, Agencia: Integer;
  ValMin, ValMax, Valor: Double;
begin
  Geral.MB_Info('Procedimento desabilitado! Fale com a DERMATEK');
{
  Solicitante := VAR_LOGIN + '#' + FormatFloat('0', VAR_USUARIO);
  Nascimento  := 0;
  //
  Banco       := EdBanco.ValueVariant;
  Agencia     := EdAgencia.ValueVariant;
  Conta       := EdConta.Text;
  Cheque      := EdCheque.ValueVariant;
  //
  DigitoCC    := 0;
  DigitoCH    := 0;
  Qtde        := 1;
  Valor       := EdValor.ValueVariant;
  //
  Endereco    := '';
  Telefone    := '';
  CEP         := '';
  //
  ValMin      := 0;
  ValMax      := 0;
  //
  EdStatusSPC.Text := IntToStr(
  UnSPC.ConsultaSPC_Rapida(QrSPC_ConfigCodigo.Value, True, Geral.SoNumero_TT(EdCPF.Text),
    EdNome.Text, QrSPC_ConfigServidor.Value,
    QrSPC_ConfigPorta.Value, QrSPC_ConfigCodigSocio.Value,
    QrSPC_ConfigSenhaSocio.Value, QrSPC_ConfigModalidade.Value,
    Solicitante, TPNascimento.Date, QrSPC_ConfigBAC_CMC7.Value,
    EdBanda.Text, EdRG.Text, EdUF.Text, Nascimento, Banco, FormatFloat('0', Agencia), Conta,
    DigitoCC, Cheque, DigitoCH, Qtde, QrSPC_ConfigTipoCred.Value, Valor,
    QrSPC_ConfigValAviso.Value,QrSPC_ConfigVALCONSULTA.Value, Endereco,
    Telefone, CEP, ValMin, ValMax, MeTransacao, MeResposta, True));
}
end;

procedure TFmSPC_Pesq.BtImprimeClick(Sender: TObject);
begin
  StringDS.RangeEnd := reCount;
  StringDS.RangeEndCount := MeResposta.Lines.Count;//sl.Count;
  frxReport1.ShowReport;
end;

procedure TFmSPC_Pesq.BitBtn2Click(Sender: TObject);
begin
  MeResposta.Text := '';
end;

procedure TFmSPC_Pesq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSPC_Pesq.EdConfigChange(Sender: TObject);
begin
  EdHost.Text         := QrSPC_ConfigServidor.Value;
  CBPorta.ItemIndex   := QrSPC_ConfigPorta.Value;
  EdCodigo.Text       := FormatFloat('0', QrSPC_ConfigCodigSocio.Value);
  EdSenha.Text        := QrSPC_ConfigSenhaSocio.Value;
  EdConsulta.Text     := FormatFloat('0', QrSPC_ConfigModalidade.Value);
  CBConsulta.KeyValue := QrSPC_ConfigModalidade.Value;
end;

procedure TFmSPC_Pesq.EdStatusSPCChange(Sender: TObject);
var
  Status: Integer;
begin
  Status := EdStatusSPC.ValueVariant;
  EdStatusSPC_TXT.Text := MLAGeral.StatusSPC_TXT(Status);
  EdStatusSPC_TXT.Font.Color := MLAGeral.StatusSPC_Color(Status);
end;

procedure TFmSPC_Pesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSPC_Pesq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  TPNascimento.Date := 0;
  QrSPC_Modali.Open;
  QrSPC_Config.Open;
  //
  //sl := TStringList.Create;
end;

procedure TFmSPC_Pesq.FormDestroy(Sender: TObject);
begin
  {
  if sl <> nil
    sl.free;
  }  
end;

procedure TFmSPC_Pesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSPC_Pesq.frxReport1GetValue(const VarName: string;
  var Value: Variant);
begin
  if CompareText(VarName, 'element') = 0 then
    Value := MeResposta.Lines[StringDS.RecNo]
  else
  if CompareText(VarName, 'VARF_CPFCNPJ') = 0 then
    Value := MLAGeral.ObtemTipoDocTxtDeCPFCNPJ(EdCPF.Text) +
      Geral.FormataCNPJ_TT(EdCPF.Text);
end;

end.
