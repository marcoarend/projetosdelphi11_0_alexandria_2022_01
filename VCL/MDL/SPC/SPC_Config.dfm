object FmSPC_Config: TFmSPC_Config
  Left = 368
  Top = 194
  Caption = 'SPC-CONFI-001 :: Configura'#231#245'es de Consultas ao SPC'
  ClientHeight = 592
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 496
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 312
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox3: TGroupBox
        Left = 0
        Top = 44
        Width = 790
        Height = 64
        Align = alTop
        Caption = ' Configura'#231#227'o de consulta: '
        TabOrder = 1
        object Label3: TLabel
          Left = 384
          Top = 16
          Width = 158
          Height = 13
          Caption = 'C'#243'digo da consulta (Modalidade):'
        end
        object Label14: TLabel
          Left = 124
          Top = 16
          Width = 143
          Height = 13
          Caption = 'C'#243'digo e senha do associado:'
        end
        object Label4: TLabel
          Left = 272
          Top = 16
          Width = 83
          Height = 13
          Caption = 'Nome Solicitante:'
        end
        object Label19: TLabel
          Left = 8
          Top = 16
          Width = 111
          Height = 13
          Caption = 'Servidor [F4 = padr'#227'o]: '
        end
        object EdCodigSocio: TdmkEdit
          Left = 124
          Top = 32
          Width = 69
          Height = 21
          Alignment = taRightJustify
          MaxLength = 8
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodigSocio'
          UpdCampo = 'CodigSocio'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdSenhaSocio: TdmkEdit
          Left = 196
          Top = 32
          Width = 72
          Height = 21
          MaxLength = 8
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'SenhaSocio'
          UpdCampo = 'SenhaSocio'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdModalidade: TdmkEditCB
          Left = 384
          Top = 32
          Width = 32
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Modalidade'
          UpdCampo = 'Modalidade'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBModalidade
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBModalidade: TdmkDBLookupComboBox
          Left = 420
          Top = 32
          Width = 229
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsSPC_Modali
          TabOrder = 5
          dmkEditCB = EdModalidade
          QryCampo = 'Modalidade'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPedinte: TdmkEdit
          Left = 272
          Top = 32
          Width = 109
          Height = 21
          MaxLength = 15
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Pedinte'
          UpdCampo = 'Pedinte'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdServidor: TdmkEdit
          Left = 8
          Top = 32
          Width = 113
          Height = 21
          MaxLength = 8
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '200.246.58.2'
          QryCampo = 'Servidor'
          UpdCampo = 'Servidor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '200.246.58.2'
          ValWarn = False
          OnKeyDown = EdServidorKeyDown
        end
        object RGPorta: TdmkRadioGroup
          Left = 652
          Top = 15
          Width = 134
          Height = 46
          Caption = ' Porta: '
          ItemIndex = 0
          Items.Strings = (
            '9162 (Sem Blocagem)'
            '9164 (Com Blocagem)')
          TabOrder = 6
          QryCampo = 'Porta'
          UpdCampo = 'Porta'
          UpdType = utYes
          OldValor = 0
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 790
        Height = 44
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label7: TLabel
          Left = 8
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label9: TLabel
          Left = 68
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNome: TdmkEdit
          Left = 68
          Top = 20
          Width = 712
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 108
        Width = 790
        Height = 136
        Align = alTop
        Caption = ' Op'#231#245'es de consulta: '
        TabOrder = 2
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 135
          Height = 119
          Align = alLeft
          Caption = ' Faixa de valores*: '
          TabOrder = 0
          object Label5: TLabel
            Left = 12
            Top = 16
            Width = 64
            Height = 13
            Caption = 'Valor m'#237'nimo:'
          end
          object Label6: TLabel
            Left = 12
            Top = 56
            Width = 65
            Height = 13
            Caption = 'Valor m'#225'ximo:'
          end
          object Label8: TLabel
            Left = 4
            Top = 100
            Width = 122
            Height = 13
            Caption = '*:  Zero (0,00) para infinito'
          end
          object EdValMin: TdmkEdit
            Left = 12
            Top = 32
            Width = 105
            Height = 21
            Alignment = taRightJustify
            MaxLength = 8
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValMin'
            UpdCampo = 'ValMin'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdValMax: TdmkEdit
            Left = 12
            Top = 72
            Width = 105
            Height = 21
            Alignment = taRightJustify
            MaxLength = 8
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValMax'
            UpdCampo = 'ValMax'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object CGPedinte: TdmkCheckGroup
          Left = 137
          Top = 15
          Width = 651
          Height = 119
          Align = alClient
          Caption = ' Itens a serem informados (al'#233'm do CNPJ / CPF): '
          Columns = 3
          ItemIndex = 10
          Items.Strings = (
            'RG (Registro geral)'
            'Nome ou Raz'#227'o Social'
            'Data de nascimento'
            'Data do cheque'
            'Informa'#231#245'es do cheque'#185
            'Quantidade de cheques (1 a 9)'
            'Tipo de cr'#233'dito'#178
            'Valor (do cr'#233'dito ou do cheque)'
            'Endere'#231'o'
            'Telefone (1)'
            'CEP de origem')
          TabOrder = 1
          QryCampo = 'InfoExtra'
          UpdCampo = 'InfoExtra'
          UpdType = utYes
          Value = 1024
          OldValor = 0
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 244
        Width = 790
        Height = 60
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
        object RGBAC_CMC7: TdmkRadioGroup
          Left = 0
          Top = 0
          Width = 144
          Height = 60
          Align = alLeft
          Caption = ' '#185' Informa'#231#245'es do cheque: '
          ItemIndex = 1
          Items.Strings = (
            'BAC + cheque'
            'CMC-7 + cheque')
          TabOrder = 0
          QryCampo = 'BAC_CMC7'
          UpdCampo = 'BAC_CMC7'
          UpdType = utYes
          OldValor = 0
        end
        object RGTipoCred: TdmkRadioGroup
          Left = 144
          Top = 0
          Width = 489
          Height = 60
          Align = alLeft
          Caption = ' '#178'Tipo de cr'#233'dito: '
          Columns = 3
          ItemIndex = 5
          Items.Strings = (
            'CD - Cr'#233'dito direto'
            'CP - Cr'#233'dito pessoal'
            'CV - Cr'#233'dito de ve'#237'culos'
            'CH - Cheque'
            'OU - Outros'
            'XX - N'#227'o gravar')
          TabOrder = 1
          QryCampo = 'TipoCred'
          UpdCampo = 'TipoCred'
          UpdType = utYes
          OldValor = 0
        end
        object GroupBox8: TGroupBox
          Left = 633
          Top = 0
          Width = 157
          Height = 60
          Align = alClient
          Caption = ' Aviso de cobran'#231'a: '
          TabOrder = 2
          object Label10: TLabel
            Left = 8
            Top = 16
            Width = 139
            Height = 13
            Caption = 'Valor total igual ou superior a:'
          end
          object EdValAviso: TdmkEdit
            Left = 8
            Top = 32
            Width = 141
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValAviso'
            UpdCampo = 'ValAviso'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 1
      Top = 432
      Width = 790
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel8: TPanel
        Left = 680
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 496
    Align = alClient
    BevelOuter = bvNone
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object GroupBox5: TGroupBox
      Left = 0
      Top = 108
      Width = 792
      Height = 136
      Align = alTop
      Caption = ' Op'#231#245'es de consulta: '
      TabOrder = 0
      object GroupBox6: TGroupBox
        Left = 2
        Top = 15
        Width = 135
        Height = 119
        Align = alLeft
        Caption = ' Faixa de valores*: '
        TabOrder = 0
        object Label16: TLabel
          Left = 12
          Top = 16
          Width = 64
          Height = 13
          Caption = 'Valor m'#237'nimo:'
        end
        object Label17: TLabel
          Left = 12
          Top = 56
          Width = 65
          Height = 13
          Caption = 'Valor m'#225'ximo:'
        end
        object Label18: TLabel
          Left = 4
          Top = 100
          Width = 122
          Height = 13
          Caption = '*:  Zero (0,00) para infinito'
        end
        object DBEdit6: TDBEdit
          Left = 12
          Top = 32
          Width = 112
          Height = 21
          DataField = 'ValMin'
          DataSource = DsSPC_Config
          TabOrder = 0
        end
        object DBEdit7: TDBEdit
          Left = 12
          Top = 72
          Width = 112
          Height = 21
          DataField = 'ValMax'
          DataSource = DsSPC_Config
          TabOrder = 1
        end
      end
      object dmkCheckGroup1: TdmkDBCheckGroup
        Left = 137
        Top = 15
        Width = 653
        Height = 119
        Align = alClient
        Caption = ' Itens a serem informados (al'#233'm do CNPJ / CPF): '
        Columns = 3
        DataField = 'InfoExtra'
        DataSource = DsSPC_Config
        Items.Strings = (
          'RG (Registro geral)'
          'Nome ou Raz'#227'o Social'
          'Data de nascimento'
          'Data do cheque'
          'Informa'#231#245'es do cheque'#185
          'Quantidade de cheques (1 a 9)'
          'Tipo de cr'#233'dito'#178
          'Valor (do cr'#233'dito ou do cheque)'
          'Endere'#231'o'
          'Telefone (1)'
          'CEP de origem')
        ParentBackground = False
        TabOrder = 1
      end
    end
    object Panel7: TPanel
      Left = 0
      Top = 244
      Width = 792
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object dmkRadioGroup1: TDBRadioGroup
        Left = 0
        Top = 0
        Width = 144
        Height = 60
        Align = alLeft
        Caption = ' '#185' Informa'#231#245'es do cheque: '
        DataField = 'BAC_CMC7'
        DataSource = DsSPC_Config
        Items.Strings = (
          'BAC + cheque'
          'CMC-7 + cheque')
        TabOrder = 0
        Values.Strings = (
          '0'
          '1')
      end
      object dmkRadioGroup2: TDBRadioGroup
        Left = 144
        Top = 0
        Width = 489
        Height = 60
        Align = alLeft
        Caption = ' '#178'Tipo de cr'#233'dito: '
        Columns = 3
        DataField = 'TipoCred'
        DataSource = DsSPC_Config
        Items.Strings = (
          'CD - Cr'#233'dito direto'
          'CP - Cr'#233'dito pessoal'
          'CV - Cr'#233'dito de ve'#237'culos'
          'CH - Cheque'
          'OU - Outros'
          'XX - N'#227'o gravar')
        TabOrder = 1
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5')
      end
      object GroupBox4: TGroupBox
        Left = 633
        Top = 0
        Width = 159
        Height = 60
        Align = alClient
        Caption = ' Aviso de cobran'#231'a: '
        TabOrder = 2
        object Label11: TLabel
          Left = 8
          Top = 16
          Width = 139
          Height = 13
          Caption = 'Valor total igual ou superior a:'
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 34
          Width = 141
          Height = 21
          DataField = 'ValAviso'
          DataSource = DsSPC_Config
          TabOrder = 0
        end
      end
    end
    object GroupBox7: TGroupBox
      Left = 0
      Top = 44
      Width = 792
      Height = 64
      Align = alTop
      Caption = ' Configura'#231#227'o de consulta: '
      TabOrder = 2
      object Label15: TLabel
        Left = 384
        Top = 16
        Width = 158
        Height = 13
        Caption = 'C'#243'digo da consulta (Modalidade):'
      end
      object Label20: TLabel
        Left = 124
        Top = 16
        Width = 143
        Height = 13
        Caption = 'C'#243'digo e senha do associado:'
      end
      object Label21: TLabel
        Left = 272
        Top = 16
        Width = 83
        Height = 13
        Caption = 'Nome Solicitante:'
      end
      object Label22: TLabel
        Left = 8
        Top = 16
        Width = 45
        Height = 13
        Caption = 'Servidor: '
      end
      object DBEdit8: TDBEdit
        Left = 124
        Top = 32
        Width = 69
        Height = 21
        DataField = 'CodigSocio'
        DataSource = DsSPC_Config
        TabOrder = 0
      end
      object DBEdit9: TDBEdit
        Left = 196
        Top = 32
        Width = 73
        Height = 21
        DataField = 'SenhaSocio'
        DataSource = DsSPC_Config
        TabOrder = 1
      end
      object DBEdit10: TDBEdit
        Left = 384
        Top = 32
        Width = 32
        Height = 21
        DataField = 'Modalidade'
        DataSource = DsSPC_Config
        TabOrder = 2
      end
      object DBEdit11: TDBEdit
        Left = 420
        Top = 32
        Width = 229
        Height = 21
        DataField = 'NOMEMODALI'
        DataSource = DsSPC_Config
        TabOrder = 3
      end
      object DBEdit12: TDBEdit
        Left = 272
        Top = 32
        Width = 109
        Height = 21
        DataField = 'Pedinte'
        DataSource = DsSPC_Config
        TabOrder = 4
      end
      object DBEdit13: TDBEdit
        Left = 8
        Top = 32
        Width = 113
        Height = 21
        DataField = 'Servidor'
        DataSource = DsSPC_Config
        TabOrder = 5
      end
      object dmkRadioGroup3: TDBRadioGroup
        Left = 652
        Top = 15
        Width = 134
        Height = 46
        Caption = ' Porta: '
        DataField = 'Porta'
        DataSource = DsSPC_Config
        Items.Strings = (
          '9162 (Sem Blocagem)'
          '9164 (Com Blocagem)')
        TabOrder = 6
        Values.Strings = (
          '0'
          '1')
      end
    end
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 44
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 3
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 68
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TDBEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsSPC_Config
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 68
        Top = 20
        Width = 712
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsSPC_Config
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
    end
    object DBGEntidades: TdmkDBGrid
      Left = 0
      Top = 304
      Width = 792
      Height = 88
      Align = alTop
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEENT'
          Title.Caption = 'Nome'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsEntidades
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 4
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEENT'
          Title.Caption = 'Nome'
          Visible = True
        end>
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 426
      Width = 792
      Height = 70
      Align = alBottom
      TabOrder = 5
      object PainelControle: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaRegistro: TLabel
          Left = 172
          Top = 0
          Width = 155
          Height = 53
          Align = alClient
          Caption = '[N]: 0'
          ExplicitWidth = 26
          ExplicitHeight = 13
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 172
          Height = 53
          Align = alLeft
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object SpeedButton4: TBitBtn
            Tag = 4
            Left = 128
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = SpeedButton4Click
          end
          object SpeedButton3: TBitBtn
            Tag = 3
            Left = 88
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = SpeedButton3Click
          end
          object SpeedButton2: TBitBtn
            Tag = 2
            Left = 48
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = SpeedButton2Click
          end
          object SpeedButton1: TBitBtn
            Tag = 1
            Left = 8
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = SpeedButton1Click
          end
        end
        object Panel3: TPanel
          Left = 327
          Top = 0
          Width = 461
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object BtConfig: TBitBtn
            Tag = 10
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Config.'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtConfigClick
          end
          object Panel2: TPanel
            Left = 352
            Top = 0
            Width = 109
            Height = 53
            Align = alRight
            Alignment = taRightJustify
            BevelOuter = bvNone
            TabOrder = 1
            object BtSaida: TBitBtn
              Tag = 13
              Left = 4
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
          object BtEntidades: TBitBtn
            Tag = 132
            Left = 96
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Entidades'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtEntidadesClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 444
        Height = 32
        Caption = 'Configura'#231#245'es de Consultas ao SPC'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 444
        Height = 32
        Caption = 'Configura'#231#245'es de Consultas ao SPC'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 444
        Height = 32
        Caption = 'Configura'#231#245'es de Consultas ao SPC'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsSPC_Config: TDataSource
    DataSet = QrSPC_Config
    Left = 40
    Top = 12
  end
  object QrSPC_Config: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrSPC_ConfigBeforeOpen
    AfterOpen = QrSPC_ConfigAfterOpen
    AfterScroll = QrSPC_ConfigAfterScroll
    SQL.Strings = (
      'SELECT cfg.Codigo, cfg.Nome, cfg.Servidor, cfg.Porta,'
      'cfg.CodigSocio, cfg.SenhaSocio, cfg.Modalidade, '
      'cfg.Pedinte, cfg.InfoExtra, cfg.ValMin, cfg.ValMax, '
      'cfg.BAC_CMC7, cfg.TipoCred, cfg.ValAviso,'
      'mda.Nome NOMEMODALI'
      'FROM spc_config cfg'
      'LEFT JOIN spc_modali mda ON mda.Codigo=cfg.Modalidade'
      '')
    Left = 12
    Top = 12
    object QrSPC_ConfigNOMEMODALI: TWideStringField
      FieldName = 'NOMEMODALI'
      Size = 50
    end
    object QrSPC_ConfigCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSPC_ConfigNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrSPC_ConfigCodigSocio: TIntegerField
      FieldName = 'CodigSocio'
    end
    object QrSPC_ConfigSenhaSocio: TWideStringField
      FieldName = 'SenhaSocio'
      Size = 15
    end
    object QrSPC_ConfigModalidade: TIntegerField
      FieldName = 'Modalidade'
    end
    object QrSPC_ConfigPedinte: TWideStringField
      FieldName = 'Pedinte'
      Size = 15
    end
    object QrSPC_ConfigInfoExtra: TIntegerField
      FieldName = 'InfoExtra'
    end
    object QrSPC_ConfigValMin: TFloatField
      FieldName = 'ValMin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSPC_ConfigValMax: TFloatField
      FieldName = 'ValMax'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSPC_ConfigBAC_CMC7: TSmallintField
      FieldName = 'BAC_CMC7'
    end
    object QrSPC_ConfigTipoCred: TSmallintField
      FieldName = 'TipoCred'
    end
    object QrSPC_ConfigServidor: TWideStringField
      FieldName = 'Servidor'
      Size = 100
    end
    object QrSPC_ConfigPorta: TSmallintField
      FieldName = 'Porta'
      Required = True
    end
    object QrSPC_ConfigValAviso: TFloatField
      FieldName = 'ValAviso'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = FmEntidadesImp.RGOrigem
    Left = 68
    Top = 12
  end
  object QrSPC_Modali: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM spc_modali'
      'ORDER BY Nome')
    Left = 692
    Top = 20
    object QrSPC_ModaliCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSPC_ModaliNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsSPC_Modali: TDataSource
    DataSet = QrSPC_Modali
    Left = 720
    Top = 20
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT'
      'FROM entidades ent'
      'LEFT JOIN spc_entida spe ON spe.Entidade=ent.Codigo'
      'WHERE spe.SPC_Config=:P0')
    Left = 20
    Top = 396
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 48
    Top = 396
  end
  object PMConfig: TPopupMenu
    Left = 360
    Top = 484
    object Incluinovaconfigurao1: TMenuItem
      Caption = '&Inclui nova configura'#231#227'o'
      OnClick = Incluinovaconfigurao1Click
    end
    object Alteraconfiguraoatual1: TMenuItem
      Caption = '&Altera configura'#231#227'o atual'
      OnClick = Alteraconfiguraoatual1Click
    end
  end
  object PMEntidades: TPopupMenu
    Left = 452
    Top = 484
    object Adicionaentidades1: TMenuItem
      Caption = '&Adiciona entidades'
      OnClick = Adicionaentidades1Click
    end
    object Remove1: TMenuItem
      Caption = '&Remove...'
      OnClick = Remove1Click
    end
  end
end
