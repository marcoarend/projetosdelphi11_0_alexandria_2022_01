unit SPC_Config;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkDBLookupComboBox,
  dmkEditCB, dmkCheckGroup, ComCtrls, Grids, DBGrids, dmkDBGrid, Menus,
  MyDBCheck, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmSPC_Config = class(TForm)
    PainelDados: TPanel;
    DsSPC_Config: TDataSource;
    QrSPC_Config: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrSPC_ConfigCodigo: TIntegerField;
    QrSPC_ConfigNome: TWideStringField;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    Label14: TLabel;
    Panel4: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label9: TLabel;
    EdCodigSocio: TdmkEdit;
    EdSenhaSocio: TdmkEdit;
    EdModalidade: TdmkEditCB;
    CBModalidade: TdmkDBLookupComboBox;
    EdPedinte: TdmkEdit;
    Label4: TLabel;
    QrSPC_Modali: TmySQLQuery;
    DsSPC_Modali: TDataSource;
    QrSPC_ModaliCodigo: TIntegerField;
    QrSPC_ModaliNome: TWideStringField;
    QrSPC_ConfigCodigSocio: TIntegerField;
    QrSPC_ConfigSenhaSocio: TWideStringField;
    QrSPC_ConfigModalidade: TIntegerField;
    QrSPC_ConfigPedinte: TWideStringField;
    QrSPC_ConfigInfoExtra: TIntegerField;
    QrSPC_ConfigValMin: TFloatField;
    QrSPC_ConfigValMax: TFloatField;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    EdValMin: TdmkEdit;
    EdValMax: TdmkEdit;
    Label6: TLabel;
    CGPedinte: TdmkCheckGroup;
    Label8: TLabel;
    Panel6: TPanel;
    RGBAC_CMC7: TdmkRadioGroup;
    RGTipoCred: TdmkRadioGroup;
    QrSPC_ConfigBAC_CMC7: TSmallintField;
    QrSPC_ConfigNOMEMODALI: TWideStringField;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    dmkCheckGroup1: TdmkDBCheckGroup;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Panel7: TPanel;
    dmkRadioGroup1: TDBRadioGroup;
    dmkRadioGroup2: TDBRadioGroup;
    QrSPC_ConfigTipoCred: TSmallintField;
    EdServidor: TdmkEdit;
    Label19: TLabel;
    RGPorta: TdmkRadioGroup;
    GroupBox7: TGroupBox;
    Label15: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    QrSPC_ConfigServidor: TWideStringField;
    QrSPC_ConfigPorta: TSmallintField;
    DBEdit13: TDBEdit;
    dmkRadioGroup3: TDBRadioGroup;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBGEntidades: TdmkDBGrid;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    PMConfig: TPopupMenu;
    Incluinovaconfigurao1: TMenuItem;
    Alteraconfiguraoatual1: TMenuItem;
    PMEntidades: TPopupMenu;
    Adicionaentidades1: TMenuItem;
    Remove1: TMenuItem;
    GroupBox4: TGroupBox;
    GroupBox8: TGroupBox;
    EdValAviso: TdmkEdit;
    Label10: TLabel;
    Label11: TLabel;
    QrSPC_ConfigValAviso: TFloatField;
    DBEdit1: TDBEdit;
    GBRodaPe: TGroupBox;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtConfig: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtEntidades: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel8: TPanel;
    BtDesiste: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSPC_ConfigAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSPC_ConfigBeforeOpen(DataSet: TDataSet);
    procedure BtConfigClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure EdServidorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrSPC_ConfigAfterScroll(DataSet: TDataSet);
    procedure Incluinovaconfigurao1Click(Sender: TObject);
    procedure Alteraconfiguraoatual1Click(Sender: TObject);
    procedure BtEntidadesClick(Sender: TObject);
    procedure Adicionaentidades1Click(Sender: TObject);
    procedure Remove1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenEntidades(Codigo: Integer);
  public
    { Public declarations }
  end;

var
  FmSPC_Config: TFmSPC_Config;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, EntidadesImp;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSPC_Config.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSPC_Config.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSPC_ConfigCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSPC_Config.DefParams;
begin
  VAR_GOTOTABELA := 'SPC_Config';
  VAR_GOTOMYSQLTABLE := QrSPC_Config;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT cfg.Codigo, cfg.Nome, cfg.Servidor, cfg.Porta, ');
  VAR_SQLx.Add('cfg.CodigSocio, cfg.SenhaSocio, cfg.Modalidade,');
  VAR_SQLx.Add('cfg.Pedinte, cfg.InfoExtra, cfg.ValMin, cfg.ValMax,');
  VAR_SQLx.Add('cfg.BAC_CMC7, cfg.TipoCred, cfg.ValAviso, ');
  VAR_SQLx.Add('mda.Nome NOMEMODALI');
  VAR_SQLx.Add('FROM spc_config cfg');
  VAR_SQLx.Add('LEFT JOIN spc_modali mda ON mda.Codigo=cfg.Modalidade');
  VAR_SQLx.Add('WHERE cfg.Codigo>-1000');
  //
  VAR_SQL1.Add('AND cfg.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND cfg.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cfg.Nome Like :P0');
  //
end;

procedure TFmSPC_Config.EdServidorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdServidor.Text := '200.246.58.2';
end;

procedure TFmSPC_Config.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant := Codigo;
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmSPC_Config.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmSPC_Config.AlteraRegistro;
var
  SPC_Config : Integer;
begin
  SPC_Config := QrSPC_ConfigCodigo.Value;
  if QrSPC_ConfigCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(SPC_Config, Dmod.MyDB, 'SPC_Config', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(SPC_Config, Dmod.MyDB, 'SPC_Config', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmSPC_Config.IncluiRegistro;
var
  Cursor : TCursor;
  SPC_Config : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    SPC_Config := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'SPC_Config', 'SPC_Config', 'Codigo');
    if Length(FormatFloat(FFormatFloat, SPC_Config))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, SPC_Config);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmSPC_Config.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSPC_Config.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSPC_Config.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSPC_Config.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSPC_Config.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSPC_Config.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSPC_Config.Adicionaentidades1Click(Sender: TObject);
var
  SPC_Config, Entidade: Integer;
  ValMin, ValMax: Double;
begin
  Geral.MB_Info('Procedimento desabilitado! Fale com a DERMATEK');
{
  Entidade := 0;
  if DBCheck.CriaFm(TFmEntidadesImp, FmEntidadesImp, afmoNegarComAviso) then
  begin
    FmEntidadesImp.FLiberaSelecionar := True;
    FmEntidadesImp.ShowModal;
    if FmEntidadesImp.FCadastrar and (FmEntidadesImp.Query.RecordCount > 0) then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE spc_entida SET ');
      FmEntidadesImp.Query.First;
      //
      SPC_Config := QrSPC_ConfigCodigo.Value;
      ValMin := QrSPC_ConfigValMin.Value;
      ValMax := QrSPC_ConfigValMax.Value;
      //
      while not FmEntidadesImp.Query.Eof do
      begin
        Entidade := FmEntidadesImp.QueryCodigo.Value;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'spc_entida', False, [
          'SPC_Config', 'ValMin', 'ValMax'], ['Entidade'], [
          'SPC_Config', 'ValMin', 'ValMax'], [
          SPC_Config, ValMin, ValMax], [Entidade], [
          SPC_Config, ValMin, ValMax], True);
        //
        FmEntidadesImp.Query.Next;
      end;
      ReopenEntidades(Entidade);
    end;
    FmEntidadesImp.Destroy;
  end;
}
end;

procedure TFmSPC_Config.Alteraconfiguraoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrSPC_Config, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'SPC_Config');
end;

procedure TFmSPC_Config.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSPC_ConfigCodigo.Value;
  Close;
end;

procedure TFmSPC_Config.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('SPC_Config', 'Codigo', ImgTipo.SQLType,
    QrSPC_ConfigCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmSPC_Config, PainelEdit,
    'SPC_Config', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo,
    True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmSPC_Config.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'SPC_Config', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'SPC_Config', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'SPC_Config', 'Codigo');
end;

procedure TFmSPC_Config.BtEntidadesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntidades, BtEntidades);
end;

procedure TFmSPC_Config.BtConfigClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConfig, BtConfig);
end;

procedure TFmSPC_Config.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align   := alClient;
  DBGEntidades.Align := alClient;
  CriaOForm;
  QrSPC_Modali.Open;
end;

procedure TFmSPC_Config.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSPC_ConfigCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSPC_Config.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSPC_Config.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrSPC_ConfigCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmSPC_Config.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSPC_Config.QrSPC_ConfigAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSPC_Config.QrSPC_ConfigAfterScroll(DataSet: TDataSet);
begin
  ReopenEntidades(0);
end;

procedure TFmSPC_Config.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSPC_Config.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSPC_ConfigCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'SPC_Config', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSPC_Config.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSPC_Config.Incluinovaconfigurao1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrSPC_Config, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'SPC_Config');
end;

procedure TFmSPC_Config.QrSPC_ConfigBeforeOpen(DataSet: TDataSet);
begin
  QrSPC_ConfigCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmSPC_Config.Remove1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntidades, TDBGrid(DBGEntidades),
  'spc_entida', ['Codigo'], ['Entidade'], istPergunta, '');
end;

procedure TFmSPC_Config.ReopenEntidades(Codigo: Integer);
begin
  QrEntidades.Close;
  QrEntidades.Params[0].AsInteger := QrSPC_ConfigCodigo.Value;
  QrEntidades.Open;
  //
  QrEntidades.Locate('Codigo', Codigo, []);
end;

end.

