object FmSPCPesq: TFmSPCPesq
  Left = 339
  Top = 185
  Caption = 'Pesquisa SPC'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Pesquisa SPC'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 780
      Height = 44
      Align = alClient
      Transparent = True
      ExplicitWidth = 788
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 398
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 80
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 436
        Height = 80
        Align = alLeft
        Caption = ' Conex'#227'o: '
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 24
          Width = 42
          Height = 13
          Caption = 'Servidor:'
        end
        object Label2: TLabel
          Left = 24
          Top = 56
          Width = 28
          Height = 13
          Caption = 'Porta:'
        end
        object host: TEdit
          Left = 56
          Top = 16
          Width = 145
          Height = 21
          TabOrder = 0
          Text = '200.246.58.2'
        end
        object Conectar: TButton
          Left = 208
          Top = 24
          Width = 90
          Height = 40
          Caption = 'C&onectar'
          TabOrder = 1
          OnClick = ConectarClick
        end
        object Desconectar: TButton
          Left = 308
          Top = 24
          Width = 90
          Height = 40
          Caption = '&Desconectar'
          Enabled = False
          TabOrder = 2
          OnClick = DesconectarClick
        end
        object Port: TComboBox
          Left = 56
          Top = 48
          Width = 145
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 3
          Items.Strings = (
            '9162 ( Sem Blocagem )'
            '9164 ( Com Blocagem )')
        end
      end
      object GroupBox3: TGroupBox
        Left = 436
        Top = 0
        Width = 346
        Height = 80
        Align = alClient
        Caption = ' Configura'#231#227'o de consulta: '
        TabOrder = 1
        object Label3: TLabel
          Left = 16
          Top = 60
          Width = 94
          Height = 13
          Caption = 'C'#243'digo da consulta:'
        end
        object Label14: TLabel
          Left = 16
          Top = 32
          Width = 102
          Height = 13
          Caption = 'C'#243'digo do associado:'
        end
        object Label15: TLabel
          Left = 196
          Top = 32
          Width = 34
          Height = 13
          Caption = 'Senha:'
        end
        object Codigo: TEdit
          Left = 124
          Top = 24
          Width = 65
          Height = 21
          MaxLength = 8
          TabOrder = 0
        end
        object Senha: TEdit
          Left = 236
          Top = 24
          Width = 73
          Height = 21
          MaxLength = 8
          TabOrder = 1
        end
        object Consulta: TEdit
          Left = 188
          Top = 52
          Width = 121
          Height = 21
          MaxLength = 8
          TabOrder = 2
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 81
      Width = 782
      Height = 108
      Align = alTop
      Caption = 'Dados Consulta'
      TabOrder = 1
      object Label5: TLabel
        Left = 8
        Top = 16
        Width = 31
        Height = 13
        Caption = 'Nome:'
      end
      object Label6: TLabel
        Left = 400
        Top = 16
        Width = 59
        Height = 13
        Caption = 'Nascimento:'
      end
      object Label7: TLabel
        Left = 476
        Top = 16
        Width = 23
        Height = 13
        Caption = 'CPF:'
      end
      object Label8: TLabel
        Left = 596
        Top = 16
        Width = 19
        Height = 13
        Caption = 'RG:'
      end
      object Label9: TLabel
        Left = 8
        Top = 60
        Width = 34
        Height = 13
        Caption = 'Banco:'
      end
      object Label10: TLabel
        Left = 48
        Top = 60
        Width = 42
        Height = 13
        Caption = 'Ag'#234'ncia:'
      end
      object Label11: TLabel
        Left = 96
        Top = 60
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object Label12: TLabel
        Left = 216
        Top = 60
        Width = 40
        Height = 13
        Caption = 'Cheque:'
      end
      object Label13: TLabel
        Left = 284
        Top = 60
        Width = 26
        Height = 13
        Caption = 'Qtde:'
      end
      object Label16: TLabel
        Left = 716
        Top = 16
        Width = 17
        Height = 13
        Caption = 'UF:'
      end
      object Nome: TEdit
        Left = 8
        Top = 32
        Width = 389
        Height = 21
        MaxLength = 50
        TabOrder = 0
      end
      object Nascimento: TEdit
        Left = 400
        Top = 32
        Width = 73
        Height = 21
        MaxLength = 8
        TabOrder = 1
      end
      object CPF: TEdit
        Left = 476
        Top = 32
        Width = 113
        Height = 21
        MaxLength = 11
        TabOrder = 2
      end
      object RG: TEdit
        Left = 596
        Top = 32
        Width = 113
        Height = 21
        MaxLength = 11
        TabOrder = 3
      end
      object UF: TEdit
        Tag = 2
        Left = 716
        Top = 32
        Width = 25
        Height = 21
        MaxLength = 2
        TabOrder = 4
      end
      object Banco: TEdit
        Left = 8
        Top = 76
        Width = 37
        Height = 21
        MaxLength = 3
        TabOrder = 5
      end
      object Agencia: TEdit
        Left = 48
        Top = 76
        Width = 45
        Height = 21
        MaxLength = 5
        TabOrder = 6
      end
      object Conta: TEdit
        Left = 96
        Top = 76
        Width = 117
        Height = 21
        MaxLength = 16
        TabOrder = 7
      end
      object Cheque: TEdit
        Left = 216
        Top = 76
        Width = 65
        Height = 21
        MaxLength = 9
        TabOrder = 8
      end
      object Qtde: TEdit
        Left = 284
        Top = 76
        Width = 17
        Height = 21
        MaxLength = 1
        TabOrder = 9
      end
      object BtConsultar: TBitBtn
        Left = 328
        Top = 60
        Width = 90
        Height = 40
        Caption = '&Consultar'
        TabOrder = 10
        OnClick = BtConsultarClick
        NumGlyphs = 2
      end
      object BitBtn2: TBitBtn
        Left = 422
        Top = 59
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Limpar resposta'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 11
        OnClick = BitBtn2Click
        NumGlyphs = 2
      end
    end
    object GroupBox4: TGroupBox
      Left = 1
      Top = 189
      Width = 782
      Height = 121
      Align = alTop
      Caption = 'String de Envio'
      TabOrder = 2
      object Transacao: TMemo
        Left = 2
        Top = 15
        Width = 778
        Height = 104
        Align = alClient
        ReadOnly = True
        TabOrder = 0
      end
    end
    object GroupBox5: TGroupBox
      Left = 1
      Top = 310
      Width = 782
      Height = 87
      Align = alClient
      Caption = 'String de Resposa'
      TabOrder = 3
      object Resposta: TMemo
        Left = 2
        Top = 15
        Width = 778
        Height = 70
        Align = alClient
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
  object Sock: TTcpClient
    Left = 8
    Top = 8
  end
end
