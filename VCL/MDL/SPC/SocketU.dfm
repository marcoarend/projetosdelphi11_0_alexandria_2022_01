object FmSocketU: TFmSocketU
  Left = 308
  Top = 117
  Caption = 'Programa Exemplo Socket - Sophus Tecnologia'
  ClientHeight = 572
  ClientWidth = 551
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 361
    Height = 113
    Caption = 'Conex'#227'o'
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 24
      Width = 42
      Height = 13
      Caption = 'Servidor:'
    end
    object Label2: TLabel
      Left = 24
      Top = 56
      Width = 28
      Height = 13
      Caption = 'Porta:'
    end
    object host: TEdit
      Left = 56
      Top = 16
      Width = 145
      Height = 21
      TabOrder = 0
      Text = '200.246.58.2'
    end
    object Conectar: TButton
      Left = 8
      Top = 80
      Width = 75
      Height = 25
      Caption = 'Conectar'
      TabOrder = 1
      OnClick = ConectarClick
    end
    object Desconectar: TButton
      Left = 96
      Top = 80
      Width = 75
      Height = 25
      Caption = 'Desconectar'
      Enabled = False
      TabOrder = 2
      OnClick = DesconectarClick
    end
    object Sair: TBitBtn
      Left = 190
      Top = 80
      Width = 75
      Height = 25
      Caption = 'Sair'
      TabOrder = 3
      OnClick = SairClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000CE0E0000C40E00001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0077FF77FF700F
        77FFFF77FF77F0B0FF7777FF77FF70B307FF00000007F0B330007777770000B3
        307777770708800330777770070880F030777702070880033077702A000000B3
        307702AAAAAAA0B3307770AA000000B33077770A070880B330777770070880BB
        307777770708880BB077777777088880B0777777770000000077}
    end
    object Port: TComboBox
      Left = 56
      Top = 48
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 1
      TabOrder = 4
      Text = '9164 ( Com Blocagem )'
      Items.Strings = (
        '9162 ( Sem Blocagem )'
        '9164 ( Com Blocagem )')
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 328
    Width = 513
    Height = 121
    Caption = 'String de Envio'
    TabOrder = 4
    object Transacao: TMemo
      Left = 8
      Top = 16
      Width = 497
      Height = 97
      ReadOnly = True
      TabOrder = 0
    end
  end
  object GroupBox4: TGroupBox
    Left = 0
    Top = 456
    Width = 513
    Height = 113
    Caption = 'String de Resposa'
    TabOrder = 5
    object Resposta: TMemo
      Left = 8
      Top = 16
      Width = 497
      Height = 89
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object Consultar: TButton
    Left = 120
    Top = 304
    Width = 99
    Height = 25
    Caption = 'Consultar'
    TabOrder = 2
    OnClick = ConsultarClick
  end
  object Button3: TButton
    Left = 264
    Top = 304
    Width = 97
    Height = 25
    Caption = 'Limpar Resposta'
    TabOrder = 3
    OnClick = Button3Click
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 112
    Width = 513
    Height = 185
    Caption = 'Dados Consulta'
    TabOrder = 1
    object Label3: TLabel
      Left = 272
      Top = 32
      Width = 44
      Height = 13
      Caption = 'Consulta:'
    end
    object Label4: TLabel
      Left = 16
      Top = 96
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object Label5: TLabel
      Left = 376
      Top = 96
      Width = 59
      Height = 13
      Caption = 'Nascimento:'
    end
    object Label6: TLabel
      Left = 24
      Top = 120
      Width = 23
      Height = 13
      Caption = 'CPF:'
    end
    object Label7: TLabel
      Left = 160
      Top = 120
      Width = 19
      Height = 13
      Caption = 'RG:'
    end
    object Label8: TLabel
      Left = 16
      Top = 152
      Width = 34
      Height = 13
      Caption = 'Banco:'
    end
    object Label9: TLabel
      Left = 88
      Top = 152
      Width = 42
      Height = 13
      Caption = 'Ag'#234'ncia:'
    end
    object Label10: TLabel
      Left = 184
      Top = 152
      Width = 31
      Height = 13
      Caption = 'Conta:'
    end
    object Label11: TLabel
      Left = 328
      Top = 152
      Width = 40
      Height = 13
      Caption = 'Cheque:'
    end
    object Label12: TLabel
      Left = 448
      Top = 152
      Width = 26
      Height = 13
      Caption = 'Qtde:'
    end
    object Label13: TLabel
      Left = 272
      Top = 120
      Width = 17
      Height = 13
      Caption = 'UF:'
    end
    object Label14: TLabel
      Left = 16
      Top = 32
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label15: TLabel
      Left = 144
      Top = 32
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object Nome: TEdit
      Left = 56
      Top = 88
      Width = 313
      Height = 21
      MaxLength = 50
      TabOrder = 3
    end
    object Nascimento: TEdit
      Left = 448
      Top = 88
      Width = 57
      Height = 21
      MaxLength = 8
      TabOrder = 4
    end
    object CPF: TEdit
      Left = 56
      Top = 112
      Width = 81
      Height = 21
      MaxLength = 11
      TabOrder = 5
    end
    object RG: TEdit
      Left = 184
      Top = 112
      Width = 81
      Height = 21
      MaxLength = 11
      TabOrder = 6
    end
    object UF: TEdit
      Tag = 2
      Left = 296
      Top = 112
      Width = 25
      Height = 21
      MaxLength = 2
      TabOrder = 7
    end
    object Banco: TEdit
      Left = 56
      Top = 144
      Width = 25
      Height = 21
      MaxLength = 3
      TabOrder = 8
    end
    object Agencia: TEdit
      Left = 136
      Top = 144
      Width = 41
      Height = 21
      MaxLength = 5
      TabOrder = 9
    end
    object Conta: TEdit
      Left = 216
      Top = 144
      Width = 105
      Height = 21
      MaxLength = 16
      TabOrder = 10
    end
    object Cheque: TEdit
      Left = 376
      Top = 144
      Width = 65
      Height = 21
      MaxLength = 9
      TabOrder = 11
    end
    object Qtde: TEdit
      Left = 480
      Top = 144
      Width = 17
      Height = 21
      MaxLength = 1
      TabOrder = 12
    end
    object Codigo: TEdit
      Left = 56
      Top = 24
      Width = 65
      Height = 21
      MaxLength = 8
      TabOrder = 0
      Text = '14026'
    end
    object Senha: TEdit
      Left = 184
      Top = 24
      Width = 73
      Height = 21
      MaxLength = 8
      TabOrder = 1
      Text = '87775'
    end
    object Consulta: TEdit
      Left = 320
      Top = 24
      Width = 121
      Height = 21
      MaxLength = 8
      TabOrder = 2
    end
  end
  object Sock: TTcpClient
    Left = 388
    Top = 8
  end
end
