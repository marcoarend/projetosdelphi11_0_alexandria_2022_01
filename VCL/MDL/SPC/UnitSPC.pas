unit UnitSPC;

interface

uses
  Windows, StdCtrls, ExtCtrls, Classes, Graphics, Controls, Messages, SysUtils,
  dmkGeral, Dialogs, Forms, Menus, (*Sockets,*) UnDmkProcFunc, UnDmkEnums;

type
  TUnitSPC = class(TObject)

  private
    { Private declarations }
    function String_Fill(var Res: String; const Ini, Fim, Tam: Integer;
             const Filler, Campo, Str: String): Boolean;
    function IntegerFill(var Res: String; const Ini, Fim, Tam: Integer;
             const Filler, Campo: String; Int: Integer): Boolean;
    function Double_Fill(var Res: String; const Ini, Fim, Tam: Integer;
             const Filler, Campo: String; Val: Double; Casas: Integer): Boolean;
    function  ZeroFill(Numero: String; Tamanho: Byte; Campo: String): String;
    function  SpaceFill(Texto: String; Tamanho: Byte; Campo: String): String;
    //function  Concatena(Str, SubStr: String; TamFinal: Integer): String;
  public
    { Public declarations }
    procedure MensagemNotConnect(Servidor, Porta: String);

    function FaltaDadosDeConexao(SPC_Config: Integer; UsaConfig: Boolean;
             Servidor: String; CodigoSocio: Integer; SenhaSocio: String; CodigoConsulta:
             Integer): Boolean;
    function ConsultaSPC_Rapida(SPCConfig: Integer; UsaConfig: Boolean;
             CPF_CNPJ, NomeConsultado, Servidor: String; SPC_Porta: Integer; CodigoSocio:
             Integer; SenhaSocio: String; CodigoConsulta: Integer; Solicitante: String;
             DataCheque: TDateTime; BAC_CMC7: Integer; CMC7: String; RG, UF: String;
             Nascimento: TDateTime; Banco: Integer; Agencia, Conta: String; DigitoCC: Integer;
             Cheque: Double; DigitoCH, Qtde: Integer; TipoCredito: Integer; Valor: Double;
             ValAviso, ValConsulta: Double; Endereco, Telefone, CEP: String;
             ValMax, ValMin: Double; MeEnvio, MeRetorno: TMemo; ConsultaUnica:
             Boolean): Integer;
    function Consulta_1((*Sock: TTcpClient; ver o que fazer*) CodSocio: Integer; Senha: String;
             CodConsulta: Integer; CPF_CNPJ, RG_, Nome_, UF_: String; Nascimento: TDateTime;
             Banco_: Integer; Agencia_, Conta_: String; DigitoCC: Integer; Cheque_: Double; DigitoCH_, Qtde_: Integer;
             Porta, Solicitante: String; DataCheque: TDateTime; BAC_CMC7: Integer;
             CMC7_: String; TipoCredito: Integer; Valor: Double;
             Endereco, Telefone, CEP: String; MemoEnvio: TMemo): TStringList;
    function AceitaValorSPC(CodigoConsulta: Integer; PrecoUnitario: Double;
             QuantidadeConsultas: Integer; ValAviso: Double): Integer;
  end;

var
  UnSPC: TUnitSPC;

implementation

uses UnMyObjects, Principal, Module, UnMLAGeral, UMySQLModule;

function TUnitSPC.IntegerFill(var Res: String; const Ini, Fim, Tam: Integer;
const Filler, Campo: String; Int: Integer): Boolean;
var
  Str: String;
begin
  Str := FormatFloat('0', Int);
  Result := String_Fill(Res, Ini, Fim, Tam, Filler, Campo, Str);
end;

function TUnitSPC.Double_Fill(var Res: String; const Ini, Fim, Tam: Integer;
const Filler, Campo: String; Val: Double; Casas: Integer): Boolean;
var
  Str: String;
begin
  Str := Geral.SoNumero_TT(Geral.FFT(Val, Casas, siPositivo));
  Result := String_Fill(Res, Ini, Fim, Tam, Filler, Campo, Str);
end;

function TUnitSPC.String_Fill(var Res: String; const Ini, Fim, Tam: Integer;
const Filler, Campo, Str: String): Boolean;
var
  Txt: String;
begin
  if Tam <> Fim - Ini + 1 then
    Geral.MB_Erro('A posi��o ou o tamanho do campo "' + Campo +
    '" est� informado incorretamente na fun��o "StringFill"')
  else
  if Length(Res) <> Ini - 1 then
    Geral.MB_Erro('O tamanho inicial do texto ou a posi��o inicial do campo "' + Campo +
    '" est� informado incorretamente na fun��o "StringFill"');
  //
  Txt := Str;
  while Length(Txt) < Tam do
  begin
    if Filler = '0' then
      Txt := Filler + Txt
    else
      Txt := Txt + Filler;
  end;
  if Length(Txt) > Tam then
    Geral.MB_Erro('O texto ' + Res + ' tem mais caracteres (' +
      Geral.FF0(Length(Txt)) + ') que o m�ximo permitido (' + Geral.FF0(Tam) +
      ') para o campo "' + Campo + '" na fun��o "StringFill"');
  Res := Res + Txt;
  Result := Length(Res) =  Ini + Tam - 1;
end;

function TUnitSPC.ZeroFill(Numero: String; Tamanho: Byte; Campo: String): String;
var
  Txt: String;
begin
  Txt := Numero;
  while Length(Txt) < Tamanho do
    Txt := '0' + Txt;
  if Length(Txt) > Tamanho then
    Geral.MB_Erro('O texto (n�mero) ' + Numero + ' tem mais algarismos (' +
      Geral.FF0(Length(Txt)) + ') que o m�ximo permitido (' +
      Geral.FF0(Tamanho) + ') para o campo "' + Campo + '" na fun��o "ZeroFill"');
  Result := Txt;
end;

function TUnitSPC.SpaceFill(Texto: String; Tamanho: Byte; Campo: String): String;
var
  Txt: String;
begin
  Txt := Texto;
  while Length(Txt) < Tamanho do
    Txt := Txt + ' ';
  if Length(Txt) > Tamanho then
    Geral.MB_Erro('O texto ' + Texto + ' tem mais caracteres (' +
      Geral.FF0(Length(Txt)) + ') que o m�ximo permitido (' + Geral.FF0(Tamanho) +
      ') para o campo "' + Campo + '" na fun��o "SpaceFill"');
  Result := Txt;
end;

{
function TUnitSPC.Concatena(Str, SubStr: String; TamFinal: Integer): String;
begin
  Result := Str + SubStr;
  if Length(Result) <> TamFinal then
    Geral.MB_Erro('A inclus�o da substring "' + SubStr +
      '" na String "' + Str + '" gerou um texto com quantidade de caracteres (' +
      Geral.FF0(Length(Result)) + ') diferente do esperado (' + Geral.FF0(TamFinal) +
      ')!');
end;
}

procedure TUnitSPC.MensagemNotConnect(Servidor, Porta: String);
begin
  Geral.MB_Aviso('N�o foi poss�vel conectar � porta ' + Porta +
    ' do servidor "' + Servidor + '"!');
end;

function TUnitSPC.FaltaDadosDeConexao(SPC_Config: Integer; UsaConfig: Boolean;
Servidor: String; CodigoSocio: Integer; SenhaSocio: String; CodigoConsulta:
Integer): Boolean;
var
  Msg: String;
begin
  if (Trim(Servidor) = '') or (CodigoSocio = 0) or (Trim(SenhaSocio) = '') or
  (CodigoConsulta = 0) then
  begin
    Result := True;
    if UsaConfig and (SPC_Config = 0) then Msg :=
      'Configura��o de consulta ao SPC n�o definida no cadastro da entidade!'
    else begin
      Msg := 'N�o foi poss�vel a conex�o ao servidor do SPC pela ' +
      'aus�ncia das seguintes informa��es:';
      if Trim(Servidor) = '' then
        Msg := Msg + sLineBreak + 'Servidor';
      if CodigoSocio = 0 then
        Msg := Msg + sLineBreak + 'C�digo do s�cio';
      if Trim(SenhaSocio) = '' then
        Msg := Msg + sLineBreak + 'Senha do s�cio';
      if CodigoConsulta = 0 then
        Msg := Msg + sLineBreak + 'C�digo de consulta (Transa��o)';
    end;
    Geral.MB_Aviso(Msg);
  end else
    Result := False;
end;

function TUnitSPC.ConsultaSPC_Rapida(SPCConfig: Integer; UsaConfig: Boolean;
CPF_CNPJ, NomeConsultado, Servidor: String; SPC_Porta: Integer; CodigoSocio:
Integer; SenhaSocio: String; CodigoConsulta: Integer; Solicitante: String;
DataCheque: TDateTime; BAC_CMC7: Integer; CMC7: String; RG, UF: String;
Nascimento: TDateTime; Banco: Integer; Agencia, Conta: String; DigitoCC: Integer;
Cheque: Double; DigitoCH, Qtde: Integer; TipoCredito: Integer; Valor: Double;
ValAviso, ValConsulta: Double; Endereco, Telefone, CEP: String; ValMax, ValMin:
Double; MeEnvio, MeRetorno: TMemo; ConsultaUnica: Boolean): Integer;
var
  Reconectar: Boolean;
  Porta, Docum1, Texto, TxtXX: String;
  Lista, Linhas, Textos: TStringList;
  Continua, i, k, n, t, x: Integer;
  //
  SQL, Liga: String;
begin
  Geral.MB_Info('Procedimento "ConsultaSPC_Rapida" desabilitado! Fale com a DERMATEK!');
{
  Result := -1;
  Lista  := nil;
  //
  if (ValMin > 0) and (Valor < ValMin) then Exit;
  if (ValMax > 0) and (Valor > ValMax) then Exit;

  if FaltaDadosDeConexao(SPCConfig, UsaConfig, Servidor, CodigoSocio,
  SenhaSocio, CodigoConsulta) then
    Exit;
  //
  if ConsultaUnica then Continua :=
    AceitaValorSPC(CodigoConsulta, ValConsulta, 1, ValAviso)
  else Continua := ID_YES;
  if Continua <> ID_YES then Exit;
  Screen.Cursor := crHourGlass;
  try
    Reconectar := not FmPrincipal.SockSPC.Active;
    if not Reconectar then
    begin
      Reconectar :=  (FmPrincipal.SockSPC.RemoteHost <> Servidor)
      or (FmPrincipal.SockSPC.RemotePort <> Porta);
    end;
    if Reconectar then
    begin
      case SPC_Porta of
        0: Porta := '9162';
        1: Porta := '9164';
      end;
      FmPrincipal.SockSPC.Active := False;
      FmPrincipal.SockSPC.RemoteHost := Servidor;
      FmPrincipal.SockSPC.RemotePort := Porta;
      FmPrincipal.SockSPC.Active := True;
    end;
    if FmPrincipal.SockSPC.Active = True then
    begin
      Docum1 := Geral.SoNumero_TT(CPF_CNPJ);
      //
      //Lista := TStringList.Create;
      Lista := Consulta_1(FmPrincipal.SockSPC, CodigoSocio, SenhaSocio,
               CodigoConsulta, CPF_CNPJ, RG, NomeConsultado, UF, Nascimento,
               Banco, Agencia, Conta, DigitoCC, Cheque, DigitoCH, Qtde,
               Porta, Solicitante, DataCheque, BAC_CMC7, CMC7, TipoCredito,
               Valor, Endereco, Telefone, CEP, MeEnvio);
      //
      Linhas := TStringList.Create;
      Textos := TStringList.Create;
      try
        n := 0;
        t := Length(Lista.Text);
        while n < t do
        begin
          Linhas.Add(Copy(Lista.Text, n + 1, 996));
          n := n + 996;
        end;
        (*
        if Memo <> nil then
        begin
          Memo.Text := Linhas.Text;
          Memo.Text := Memo.Text + sLineBreak + ' ###### ----- ###### ' + #13+#10;
          Memo.Text := Memo.Text + Lista.Text;
        end;
        *)
        (*
        if MeRetorno <> nil then
        begin
          MeRetorno.Text := ' CNPJ / CPF: ' + CPF_CNPJ + sLineBreak +
                       '--------------------------------' + sLineBreak;
        end;
        *)
        for i := 0 to Linhas.Count - 1 do
        begin
          if Length(Trim(Linhas[i])) > 40 then
          begin
            for x := 0 to 11 do
            begin
              TxtXX := Copy(Linhas[i], 049 + (x * 079), 079);
              if Trim(TxtXX) <> '' then
              begin
                Textos.Add(TxtXX);
                if MeRetorno <> nil then
                  MeRetorno.Lines.Add(TxtXX);
              end;
            end;
            //  N�o usar "Geral.IMV" pois o zero � igual a 'CONSULTA CONCLU�DA'
            k := StrToInt(Linhas[i][040]);
            case k of
              //  N�o usar "Geral.IMV" pois o zero � igual a 'NADA CONSTA'
              0: Result := StrToInt(Linhas[i][041]);
            //1: EXISTEM MAIS REGISTROS
              9:
              begin
                Texto := Copy(Linhas[i], 049, 079);
                //
                Geral.MB_Aviso('N�o foi poss�vel efetuar a consulta. ' +
                  sLineBreak + 'Motivo:' + sLineBreak + Texto);
              end;
            end;
          end;
        end;
        Dmod.QrUpdZ.SQL.Clear;
        Dmod.QrUpdZ.SQL.Add('INSERT INTO spc_consul SET LastPesq=:P0, ');
        Dmod.QrUpdZ.SQL.Add('StatusSPC=:P1, Consultas=1, CPFCNPJ=:P2 ');
        Dmod.QrUpdZ.SQL.Add('ON DUPLICATE KEY UPDATE lastpesq=:p3, ');
        Dmod.QrUpdZ.SQL.Add('StatusSPC=:P4, Consultas=Consultas+1');
        Dmod.QrUpdZ.Params[00].AsString  := Geral.FDT(Date, 1);
        Dmod.QrUpdZ.Params[01].AsInteger := Result;
        Dmod.QrUpdZ.Params[02].AsString  := CPF_CNPJ;
        Dmod.QrUpdZ.Params[03].AsString  := Geral.FDT(Date, 1);
        Dmod.QrUpdZ.Params[04].AsInteger := Result;
        Dmod.QrUpdZ.ExecSQL;
        //
        Dmod.QrUpdZ.SQL.Clear;
        Dmod.QrUpdZ.SQL.Add('DELETE FROM spc_result WHERE CPFCNPJ=:P0');
        Dmod.QrUpdZ.Params[0].AsString := CPF_CNPJ;
        Dmod.QrUpdZ.ExecSQL;
        //
        SQL := '';
        Liga := '';
        for i := 0 to Textos.Count - 1 do
        begin
          SQL := SQL + Liga + '("' + Textos[i] + '","' + CPF_CNPJ + '",' +
            FormatFloat('0', i+1) + ')' + sLineBreak;
          Liga := ',';
        end;
        if SQL <> '' then
        begin
          SQL := 'INSERT INTO spc_result (Texto, CPFCNPJ, Linha) VALUES ' +
                 sLineBreak + SQL;
          try
            Dmod.MyDB.Execute(SQL);
          except
            dmkPF.LeTexto_Permanente(SQL, 'Consulta SPC');
            //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
            raise;
          end;
        end;
      finally
        Linhas.Free;
        Textos.Free;
      end;
    end else MensagemNotConnect(Servidor, Porta);
    if Lista <> nil then
      Lista.Free;
  finally
    Screen.Cursor := crDefault;
  end;
}
end;

function TUnitSPC.Consulta_1((*Sock: TTcpClient; ver o que fazer*) CodSocio: Integer; Senha: String;
CodConsulta: Integer; CPF_CNPJ, RG_, Nome_, UF_: String; Nascimento: TDateTime;
Banco_: Integer; Agencia_, Conta_: String; DigitoCC: Integer; Cheque_: Double; DigitoCH_, Qtde_: Integer;
Porta, Solicitante: String; DataCheque: TDateTime; BAC_CMC7: Integer;
CMC7_: String; TipoCredito: Integer; Valor: Double; Endereco, Telefone, CEP: String;
MemoEnvio: TMemo): TStringList;
var
  Trans, Resp, Consulta, CPF, CNPJ, RG, Nome, DataNas, Data_CH, OriInfoCH,
  Banco, Agencia, Qtde, UF, Solicit, Docum1, Docum2, TipoCred, DDD1, Tel1, CEPo,
  CMC7, CMC7_1, CMC7_2, CMC7_3: String;
  Flag: Boolean;
  Conta: Int64;
begin
  Geral.MB_Aviso('Procedure "Consulta_1" desabilitado. Fale com a DERMATEK ');
{
  Result := TStringList.Create;
  Result.Clear;
  if not Sock.Active then
  begin
    Application.MessageBox('Voc� deve conectar-se antes', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  //Codigo   := FormatFloat('0', CodSocio);
  CPF      := Geral.SoNumero_TT(CPF_CNPJ);
  if Length(CPF) = 11 then
  begin
    CNPJ := '';
    CPF  := SpaceFill('CPF' + ZeroFill(CPF, 11, 'CPF'), 20, 'CPF');
    RG   := 'RG' + Geral.SoNumero_TT(RG_);
    if Uppercase(UF_) = 'SP' then
      UF := ''
    else UF := Copy(Uppercase(UF_), 1, 2);
    //
    RG   := SpaceFill(RG + UF, 20, 'RG');
  end else begin
    CNPJ := SpaceFill('CNPJ' + CPF, 20, 'CNPJ');
    CPF  := '';
    RG   := SpaceFill('', 20, 'RG');
  end;
  Docum1   := CPF + CNPJ;
  Docum2   := RG;
  Solicit  := Copy(Solicitante, 1, 15);
  Nome     := Copy(Nome_, 1, 50);
  Banco    := FormatFloat('0', Banco_);
  Agencia  := Geral.SoNumero_TT(Agencia);
  Conta    := MLAGeral.I64MV(Geral.SoNumero_TT(Conta_));
  Qtde     := FormatFloat('0', Qtde_);
  Consulta := FormatFloat('0', CodConsulta);
  if Nascimento < 1 then
    DataNas := '00000000'
  else
    DataNas := FormatDateTime('DDMMYYYY', Nascimento);
  if Uppercase(UF_) = 'SP' then
    UF := ''
  else UF := Copy(Uppercase(UF_), 1, 2);
  case TipoCredito of
    0: TipoCred := 'CD';
    1: TipoCred := 'CP';
    2: TipoCred := 'CV';
    3: TipoCred := 'CH';
    4: TipoCred := 'OU';
    5: TipoCred := 'XX';
    else TipoCred := '??';
  end;
  Tel1 := Geral.SoNumero_TT(Telefone);
  Tel1 := ZeroFill(Tel1, 8, 'Fragmenta��o do telefone ');
  if Length(Tel1) > 8 then
  begin
    DDD1 := Copy(Tel1, 1, Length(Tel1) - 8);
    Tel1 := Copy(Tel1, Length(Tel1) - 7, 8);
  end else DDD1 := '0';
  CEPo := Geral.SoNumero_TT(CEP);
  //
  //Trans := 'CSR10C04               ';
  Trans := '';
  if not String_Fill(Trans, 001, 006, 006, ' ', '01.IDENTIFICA��O'       , 'CSR10C')      then Exit;
  if not String_Fill(Trans, 007, 008, 002, ' ', '02.VERS�O'              , '04')          then Exit;
  if not String_Fill(Trans, 009, 023, 015, ' ', '03.SOLICITANTE(pessoa)' , Solicit)       then Exit;
  //Trans := Trans + ZeroFill(Codigo,8, ) + SpaceFill(Senha, 8);
  if not IntegerFill(Trans, 024, 031, 008, '0', '04.C�DIGO DO S�CIO'     , CodSocio)      then Exit;
  if not String_Fill(Trans, 032, 039, 008, ' ', '05.SENHA DO S�CIO'      , Senha)         then Exit;
  //Trans := Trans + SpaceFill(Consulta, 8);
  if not String_Fill(Trans, 040, 047, 008, ' ', '06.CONSULTA (transa��o)', Consulta)      then Exit;
  (*
  if CPF <> '' then
  begin
    if Length(CPF) = 11 then
    begin
      // CPF + RG
      Trans := Trans + 'CPF' + ZeroFill(CPF, 11)  + '      ';
      if RG <> '' Then
         Trans := Trans + 'RG'  + ZeroFill(RG, 11)  + SpaceFill(UF, 2) + '     '
      else
        Trans := Trans + '                    ';
    end else begin
      // CNPJ
      Trans := Trans + SpaceFill('CNPJ' + ZeroFill(CPF, 14), 40);
    end;
  end else begin
    Trans := Trans + '                    ';
    Trans := Trans + '                    ';
  end;
  *)
  if not String_Fill(Trans, 048, 067, 020, ' ', '07.DOCUMENTO           ', Docum1  )      then Exit;
  if not String_Fill(Trans, 068, 087, 020, ' ', '08.DOCUMENTO           ', Docum2  )      then Exit;
  // USEFONE
  if not String_Fill(Trans, 088, 107, 020, ' ', '09.RESERVADO / USEFONE ', ''      )      then Exit;
  //Trans := Trans + SpaceFill(Nome,50);
  if not String_Fill(Trans, 108, 157, 050, ' ', '10.NOME                ', Nome    )      then Exit;
  //Trans := Trans + ZeroFill(DataNas, 8) + '31122009';
  if not String_Fill(Trans, 158, 165, 008, ' ', '11.DATA DE NASCIMENTO  ', DataNas )      then Exit;
  if not String_Fill(Trans, 166, 173, 008, ' ', '12.DATA DO CHEQUE      ', Data_CH )      then Exit;
  //Trans := Trans + ZeroFill(Banco, 3) + ZeroFill(Agencia,5);
  if BAC_CMC7 = 0 then
  begin
    // BAC
    OriInfoCH := ' ';
    if not IntegerFill(Trans, 174, 176, 003, '0', '13.1.BANCO             ', Banco_)      then Exit;
    if not IntegerFill(Trans, 177, 181, 005, '0', '13.1.AG�NCIA           ', Geral.IMV(Agencia))    then Exit;
    //Trans := Trans + ZeroFill(Conta, 16) + ZeroFill(Cheque,9);
    if not IntegerFill(Trans, 182, 196, 015, '0', '13.1.CONTA CORRENTE    ', Conta)       then Exit;
    if not IntegerFill(Trans, 197, 197, 005, '0', '13.1.DIGITO CTA CORREN.', DigitoCC)    then Exit;
    if not Double_Fill(Trans, 198, 205, 008, '0', '13.1.CHEQUE            ', Cheque_, 0)  then Exit;
    if not IntegerFill(Trans, 206, 206, 001, '0', '13.1.DIGITO CHEQUE     ', DigitoCH_)   then Exit;
  end else begin
    // CMC7
    OriInfoCH := 'C';
    CMC7  := Trim(CMC7_);
    CMC7_1 := Copy(CMC7, 01, 08);
    CMC7_2 := Copy(CMC7, 09, 10);
    CMC7_3 := Copy(CMC7, 19, 12);
    if not String_Fill(Trans, 174, 181, 008, '0', '13.2.CAMPO 1 DO CMC7   ', CMC7_1)       then Exit;
    if not String_Fill(Trans, 182, 191, 010, '0', '13.2.CAMPO 2 DO CMC7   ', CMC7_2)       then Exit;
    if not String_Fill(Trans, 192, 203, 012, '0', '13.2.CAMPO 3 DO CMC7   ', CMC7_3)       then Exit;
    if not String_Fill(Trans, 204, 206, 003, ' ', 'RESERVADO(BRANCOS)     ', '')           then Exit;
  end;
  //Trans := Trans + ZeroFill(Qtde, 1) + 'XX' + '00000000199';
  if not IntegerFill(Trans, 207, 207, 001, '0', '14.QUANTIDADE DE CHEQUE ENTRE 1 E 9', DigitoCH_)   then Exit;
  if not String_Fill(Trans, 208, 209, 002, ' ', '15.TIPO DE CREDITO                 ', TipoCred)    then Exit;
  if not Double_Fill(Trans, 210, 220, 011, '0', '16.VALOR DO CREDITO OU DO CHEQUE   ', Valor, 2)    then Exit;
  //Trans := Trans + '                                                  ';
  if not String_Fill(Trans, 221, 270, 050, ' ', '17.ENDERE�O                        ', Endereco)    then Exit;
  //Trans := Trans + '0000' + '000000000' + '00000' + '000' + ' ' +  '        ';
  if not String_Fill(Trans, 271, 274, 004, '0', '18.N�MERO DO DDD - 1               ', DDD1     )   then Exit;
  if not String_Fill(Trans, 275, 283, 009, '0', '19.N�MERO DO TELEFONE - 1          ', TEL1     )   then Exit;
  if not String_Fill(Trans, 284, 291, 008, '0', '20.CEP DE ORIGEM                   ', CEPo     )   then Exit;
  if not String_Fill(Trans, 292, 292, 001, ' ', '21.ORIGEM DAS INFORMA��ES          ', OriInfoCH)   then Exit;
  if not String_Fill(Trans, 293, 300, 008, ' ', '22.RESERVADO (BRANCOS)             ', ''       )   then Exit;
  (*
  Transacao.Text := '';
  Transacao.Lines.Add('Enviando '+IntToStr(StrLen(Pchar(Trans))) + ' bytes');
  Transacao.Lines.Add(Trans);

  BtConsultar.Caption := 'Aguarde ...';
  *)
  if MemoEnvio <> nil then
    MemoEnvio.Text := Trans;
  if Length(Trans) <> 300 then
  begin
    Geral.MB_Erro('Tamanho de registro inv�lido para envio!' + sLineBreak +
      'Tamanho gerado: ' + Geral.FF0(Length(Trans)) + sLineBreak +
      'Tamanho certo:  300');
    Exit;
  end;
  // 23.FINALIZADO <ENTER> (HEXADECIMAL "0x0d") Delphi = #$D
  Sock.Sendln(Trans, #$D);
  Flag := False;
  Sock.WaitForData(60000);
  while not Flag do
    begin
    Resp := Sock.Receiveln(#$D);
    if Copy(Porta,1,4) = '9162' then
      begin
      //Resposta.Add('Recebeu bloco de ' + IntToStr(StrLen(Pchar(Resp))) + ' bytes');
      Result.Add(Resp);
      Flag := True;
      end
    else
      begin
      //Resposta.Lines.Add('Recebeu bloco de ' + IntToStr(StrLen(Pchar(Resp))) + ' bytes');
      case StrToInt(Copy(Resp,40,1)) of
        0: Flag := True;
        1:begin
          Sock.Sendln('',#$D);
          Flag := False;
          end;
        9: Flag := True;
      end;
      Result.Add(Resp);
    end;
  end;
  //BtConsultar.Caption := '&Consultar';
  //Consulta.Enabled  := True;
}
end;

function TUnitSPC.AceitaValorSPC(CodigoConsulta: Integer; PrecoUnitario: Double;
QuantidadeConsultas: Integer; ValAviso: Double): Integer;
var
 ValorConsulta: Double;
begin
  ValorConsulta := PrecoUnitario * QuantidadeConsultas;
  if ValorConsulta >= ValAviso then
    Result := Geral.MB_Pergunta('Na configura��o de consulta da ' +
      'entidade est� configurado o c�digo de consulta (transa��o) n� ' +
      Geral.FF0(CodigoConsulta) + ' que informa que ser� cobrado o valor de ' +
      Dmod.QrControleMoeda.Value + ' ' + FormatFloat('#,###,##0.00', ValorConsulta) +
      ' para esta consulta.' + sLineBreak + sLineBreak + 'Caso queira que este aviso s� ' +
      'apare�a para valores maiores que este, aumente o valor no cadastro da ' +
      'confugura��o de consulta!' + sLineBreak + sLineBreak + 'Deseja efetuar a consulta ' +
      'assim mesmo?')
  else
    Result := ID_YES;
end;

end.

