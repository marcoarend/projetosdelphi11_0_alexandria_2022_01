object FmSPC_Pesq: TFmSPC_Pesq
  Left = 339
  Top = 185
  Caption = 'SPC-PESQU-001 :: Pesquisa SPC'
  ClientHeight = 721
  ClientWidth = 762
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 762
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 714
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 666
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 174
        Height = 32
        Caption = 'Pesquisa SPC'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 174
        Height = 32
        Caption = 'Pesquisa SPC'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 174
        Height = 32
        Caption = 'Pesquisa SPC'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 607
    Width = 762
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 758
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 762
    Height = 559
    Align = alClient
    TabOrder = 2
    object Panel3: TPanel
      Left = 2
      Top = 59
      Width = 758
      Height = 80
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 313
        Height = 80
        Align = alLeft
        Caption = ' Conex'#227'o: '
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 24
          Width = 42
          Height = 13
          Caption = 'Servidor:'
        end
        object Label2: TLabel
          Left = 24
          Top = 56
          Width = 28
          Height = 13
          Caption = 'Porta:'
        end
        object EdHost: TdmkEdit
          Left = 56
          Top = 16
          Width = 145
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CBPorta: TComboBox
          Left = 56
          Top = 48
          Width = 145
          Height = 21
          Style = csDropDownList
          TabOrder = 1
          Items.Strings = (
            '9162 ( Sem Blocagem )'
            '9164 ( Com Blocagem )')
        end
      end
      object GroupBox3: TGroupBox
        Left = 313
        Top = 0
        Width = 445
        Height = 80
        Align = alClient
        Caption = ' Configura'#231#227'o de consulta: '
        TabOrder = 1
        object Label3: TLabel
          Left = 16
          Top = 60
          Width = 94
          Height = 13
          Caption = 'C'#243'digo da consulta:'
        end
        object Label14: TLabel
          Left = 16
          Top = 32
          Width = 102
          Height = 13
          Caption = 'C'#243'digo do associado:'
        end
        object Label15: TLabel
          Left = 316
          Top = 32
          Width = 34
          Height = 13
          Caption = 'Senha:'
        end
        object SpeedButton1: TSpeedButton
          Left = 412
          Top = 52
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object EdCodigo: TdmkEdit
          Left = 124
          Top = 24
          Width = 65
          Height = 21
          MaxLength = 8
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdSenha: TdmkEdit
          Left = 356
          Top = 24
          Width = 77
          Height = 21
          MaxLength = 8
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdConsulta: TdmkEditCB
          Left = 124
          Top = 52
          Width = 41
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBConsulta
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBConsulta: TdmkDBLookupComboBox
          Left = 166
          Top = 52
          Width = 243
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsSPC_Modali
          TabOrder = 3
          dmkEditCB = EdConsulta
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 2
      Top = 139
      Width = 758
      Height = 148
      Align = alTop
      Caption = 'Dados Consulta'
      TabOrder = 1
      object Label5: TLabel
        Left = 8
        Top = 16
        Width = 31
        Height = 13
        Caption = 'Nome:'
      end
      object Label6: TLabel
        Left = 364
        Top = 16
        Width = 59
        Height = 13
        Caption = 'Nascimento:'
      end
      object Label7: TLabel
        Left = 476
        Top = 16
        Width = 23
        Height = 13
        Caption = 'CPF:'
      end
      object Label8: TLabel
        Left = 596
        Top = 16
        Width = 19
        Height = 13
        Caption = 'RG:'
      end
      object Label9: TLabel
        Left = 324
        Top = 56
        Width = 34
        Height = 13
        Caption = 'Banco:'
      end
      object Label10: TLabel
        Left = 364
        Top = 56
        Width = 42
        Height = 13
        Caption = 'Ag'#234'ncia:'
      end
      object Label11: TLabel
        Left = 412
        Top = 56
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object Label12: TLabel
        Left = 532
        Top = 56
        Width = 40
        Height = 13
        Caption = 'Cheque:'
      end
      object Label13: TLabel
        Left = 632
        Top = 56
        Width = 26
        Height = 13
        Caption = 'Qtde:'
      end
      object Label16: TLabel
        Left = 717
        Top = 16
        Width = 17
        Height = 13
        Caption = 'UF:'
      end
      object Label17: TLabel
        Left = 664
        Top = 56
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object Label36: TLabel
        Left = 8
        Top = 56
        Width = 143
        Height = 13
        Caption = 'Leitura pela banda magn'#233'tica:'
      end
      object Label18: TLabel
        Left = 300
        Top = 76
        Width = 14
        Height = 13
        Caption = 'Ou'
      end
      object EdNome: TdmkEdit
        Left = 8
        Top = 32
        Width = 353
        Height = 21
        MaxLength = 50
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCPF: TdmkEdit
        Left = 476
        Top = 32
        Width = 113
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdRG: TdmkEdit
        Left = 596
        Top = 32
        Width = 113
        Height = 21
        MaxLength = 11
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdUF: TdmkEdit
        Tag = 2
        Left = 717
        Top = 32
        Width = 28
        Height = 21
        MaxLength = 2
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdBanco: TdmkEdit
        Left = 324
        Top = 72
        Width = 37
        Height = 21
        Alignment = taRightJustify
        MaxLength = 3
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdAgencia: TdmkEdit
        Left = 364
        Top = 72
        Width = 45
        Height = 21
        Alignment = taRightJustify
        MaxLength = 5
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdConta: TdmkEdit
        Left = 412
        Top = 72
        Width = 117
        Height = 21
        MaxLength = 16
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCheque: TdmkEdit
        Left = 532
        Top = 72
        Width = 65
        Height = 21
        Alignment = taRightJustify
        MaxLength = 9
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdQtde: TdmkEdit
        Left = 632
        Top = 72
        Width = 17
        Height = 21
        Alignment = taRightJustify
        MaxLength = 1
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdValor: TdmkEdit
        Left = 664
        Top = 72
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,99'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.990000000000000000
        ValWarn = False
      end
      object GroupBox6: TGroupBox
        Left = 8
        Top = 96
        Width = 737
        Height = 49
        Caption = ' Resultado da pesquisa: '
        Enabled = False
        TabOrder = 11
        object EdStatusSPC_TXT: TdmkEdit
          Left = 36
          Top = 20
          Width = 689
          Height = 21
          TabStop = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdStatusSPC: TdmkEdit
          Left = 13
          Top = 20
          Width = 20
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-1'
          ValMax = '2'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '-1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = -1
          ValWarn = False
          OnChange = EdStatusSPCChange
        end
      end
      object TPNascimento: TdmkEditDateTimePicker
        Left = 364
        Top = 32
        Width = 109
        Height = 21
        Date = 39827.641433726850000000
        Time = 39827.641433726850000000
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdBanda: TdmkEdit
        Left = 8
        Top = 72
        Width = 285
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 34
        ParentFont = False
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GroupBox5: TGroupBox
      Left = 2
      Top = 287
      Width = 758
      Height = 121
      Align = alTop
      Caption = 'String de Envio'
      TabOrder = 2
      object MeTransacao: TMemo
        Left = 2
        Top = 15
        Width = 754
        Height = 104
        Align = alClient
        ReadOnly = True
        TabOrder = 0
      end
    end
    object GroupBox7: TGroupBox
      Left = 2
      Top = 408
      Width = 758
      Height = 149
      Align = alClient
      Caption = 'String de Resposa'
      TabOrder = 3
      object MeResposta: TMemo
        Left = 2
        Top = 15
        Width = 754
        Height = 132
        Align = alClient
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 758
      Height = 44
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
      object Label4: TLabel
        Left = 8
        Top = 4
        Width = 124
        Height = 13
        Caption = 'Configura'#231#227'o de consulta:'
      end
      object SpeedButton2: TSpeedButton
        Left = 726
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton2Click
      end
      object CBConfig: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 661
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSPC_Config
        TabOrder = 0
        dmkEditCB = EdConfig
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdConfig: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdConfigChange
        DBLookupComboBox = CBConfig
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 651
    Width = 762
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 616
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 6
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 614
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConsultar: TBitBtn
        Left = 16
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Consultar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConsultarClick
      end
      object BitBtn2: TBitBtn
        Left = 138
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Limpar resposta'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BitBtn2Click
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = 'I&mprime'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtImprimeClick
      end
    end
  end
  object QrSPC_Config: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sc.*, sm.Valor VALCONSULTA, sm.Nome NOMEMODALI'
      'FROM spc_config sc '
      'LEFT JOIN spc_modali sm ON sm.Codigo=sc.Modalidade '
      'ORDER BY sc.Nome')
    Left = 8
    Top = 8
    object QrSPC_ConfigCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSPC_ConfigNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSPC_ConfigServidor: TWideStringField
      FieldName = 'Servidor'
      Size = 100
    end
    object QrSPC_ConfigPorta: TSmallintField
      FieldName = 'Porta'
    end
    object QrSPC_ConfigPedinte: TWideStringField
      FieldName = 'Pedinte'
      Size = 15
    end
    object QrSPC_ConfigCodigSocio: TIntegerField
      FieldName = 'CodigSocio'
    end
    object QrSPC_ConfigSenhaSocio: TWideStringField
      FieldName = 'SenhaSocio'
      Size = 15
    end
    object QrSPC_ConfigModalidade: TIntegerField
      FieldName = 'Modalidade'
    end
    object QrSPC_ConfigInfoExtra: TIntegerField
      FieldName = 'InfoExtra'
    end
    object QrSPC_ConfigValMin: TFloatField
      FieldName = 'ValMin'
    end
    object QrSPC_ConfigValMax: TFloatField
      FieldName = 'ValMax'
    end
    object QrSPC_ConfigBAC_CMC7: TSmallintField
      FieldName = 'BAC_CMC7'
    end
    object QrSPC_ConfigTipoCred: TSmallintField
      FieldName = 'TipoCred'
    end
    object QrSPC_ConfigValAviso: TFloatField
      FieldName = 'ValAviso'
    end
    object QrSPC_ConfigVALCONSULTA: TFloatField
      FieldName = 'VALCONSULTA'
    end
    object QrSPC_ConfigNOMEMODALI: TWideStringField
      FieldName = 'NOMEMODALI'
      Size = 50
    end
  end
  object DsSPC_Config: TDataSource
    DataSet = QrSPC_Config
    Left = 36
    Top = 8
  end
  object QrSPC_Modali: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM spc_modali sm '
      'ORDER BY Nome'
      '')
    Left = 68
    Top = 8
    object QrSPC_ModaliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSPC_ModaliNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = QrSPC_Modali
    Left = 376
    Top = 292
  end
  object DsSPC_Modali: TDataSource
    DataSet = QrSPC_Modali
    Left = 96
    Top = 8
  end
  object StringDS: TfrxUserDataSet
    UserName = 'StringDS'
    Left = 156
    Top = 8
  end
  object frxReport1: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    EngineOptions.MaxMemSize = 10000000
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38006.786384259300000000
    ReportOptions.LastChange = 38007.009433217600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxReport1GetValue
    Left = 128
    Top = 8
    Datasets = <
      item
        DataSet = StringDS
        DataSetName = 'StringDS'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo2: TfrxMemoView
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Consulta SPC / SERASA: [VARF_CPFCNPJ]')
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 102.047310000000000000
        Width = 718.110700000000000000
        DataSet = StringDS
        DataSetName = 'StringDS'
        RowCount = 0
        object Memo3: TfrxMemoView
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Memo.UTF8W = (
            '[element]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 181.417440000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 642.419312530000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
        end
      end
    end
  end
end
