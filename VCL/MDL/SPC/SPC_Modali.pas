unit SPC_Modali;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmSPC_Modali = class(TForm)
    PainelDados: TPanel;
    DsSPC_Modali: TDataSource;
    QrSPC_Modali: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrSPC_ModaliCodigo: TIntegerField;
    QrSPC_ModaliNome: TWideStringField;
    QrSPC_ModaliValor: TFloatField;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBEdit: TGroupBox;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label4: TLabel;
    EdValor: TdmkEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBData: TGroupBox;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    Label2: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSPC_ModaliAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSPC_ModaliBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmSPC_Modali: TFmSPC_Modali;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSPC_Modali.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSPC_Modali.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSPC_ModaliCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSPC_Modali.DefParams;
begin
  VAR_GOTOTABELA := 'SPC_Modali';
  VAR_GOTOMYSQLTABLE := QrSPC_Modali;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, Nome, Valor');
  VAR_SQLx.Add('FROM spc_modali');
  VAR_SQLx.Add('WHERE Codigo >-1');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmSPC_Modali.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmSPC_Modali.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmSPC_Modali.AlteraRegistro;
var
  SPC_Modali : Integer;
begin
  SPC_Modali := QrSPC_ModaliCodigo.Value;
  if QrSPC_ModaliCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(SPC_Modali, Dmod.MyDB, 'SPC_Modali', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(SPC_Modali, Dmod.MyDB, 'SPC_Modali', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmSPC_Modali.IncluiRegistro;
var
  Cursor : TCursor;
  SPC_Modali : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    SPC_Modali := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'SPC_Modali', 'SPC_Modali', 'Codigo');
    if Length(FormatFloat(FFormatFloat, SPC_Modali))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, SPC_Modali);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmSPC_Modali.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSPC_Modali.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSPC_Modali.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSPC_Modali.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSPC_Modali.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSPC_Modali.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSPC_Modali.BtAlteraClick(Sender: TObject);
begin
  EdCodigo.Enabled := False;
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrSPC_Modali, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'SPC_Modali');
end;

procedure TFmSPC_Modali.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSPC_ModaliCodigo.Value;
  Close;
end;

procedure TFmSPC_Modali.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := EdCodigo.ValueVariant;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmSPC_Modali, PainelEdita,
    'SPC_Modali', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo,
    True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmSPC_Modali.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'SPC_Modali', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'SPC_Modali', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'SPC_Modali', 'Codigo');
end;

procedure TFmSPC_Modali.BtIncluiClick(Sender: TObject);
begin
  EdCodigo.Enabled := True;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrSPC_Modali, [PainelDados],
  [PainelEdita], EdCodigo, ImgTipo, 'SPC_Modali');
end;

procedure TFmSPC_Modali.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  GBEdit.Align  := alClient;
  GBData.Align  := alClient;
  CriaOForm;
end;

procedure TFmSPC_Modali.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSPC_ModaliCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSPC_Modali.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSPC_Modali.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrSPC_ModaliCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmSPC_Modali.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSPC_Modali.QrSPC_ModaliAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSPC_Modali.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSPC_Modali.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSPC_ModaliCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'SPC_Modali', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSPC_Modali.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSPC_Modali.QrSPC_ModaliBeforeOpen(DataSet: TDataSet);
begin
  QrSPC_ModaliCodigo.DisplayFormat := FFormatFloat;
end;

end.

