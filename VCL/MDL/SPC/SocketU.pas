unit SocketU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ScktComp, ExtCtrls, Buttons, Sockets;

type
  TFmSocketU = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    host: TEdit;
    Label2: TLabel;
    Conectar: TButton;
    Desconectar: TButton;
    GroupBox2: TGroupBox;
    GroupBox4: TGroupBox;
    Resposta: TMemo;
    Consultar: TButton;
    Transacao: TMemo;
    Button3: TButton;
    Sock: TTcpClient;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Sair: TBitBtn;
    Nome: TEdit;
    Nascimento: TEdit;
    CPF: TEdit;
    RG: TEdit;
    Label13: TLabel;
    UF: TEdit;
    Banco: TEdit;
    Agencia: TEdit;
    Conta: TEdit;
    Cheque: TEdit;
    Qtde: TEdit;
    Label14: TLabel;
    Codigo: TEdit;
    Label15: TLabel;
    Senha: TEdit;
    Consulta: TEdit;
    Port: TComboBox;
    procedure ConectarClick(Sender: TObject);
    procedure DesconectarClick(Sender: TObject);
    procedure ConsultarClick(Sender: TObject);
    procedure SairClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
    Function  ZeroFill ( Numero : String; Tamanho : Byte ) : String;
    Function  SpaceFill ( Numero : String; Tamanho : Byte ) : String;
  public
    { Public declarations }
  end;

var
  FmSocketU: TFmSocketU;

implementation

{$R *.dfm}

procedure TFmSocketU.ConectarClick(Sender: TObject);
begin
    Sock.RemoteHost := Host.Text;
    sock.RemotePort := Copy(Port.Text,1,4);
    If Not Sock.Active then
       Sock.Active   := True;
    Host.Enabled     := False;
    Port.Enabled     := False;
    Conectar.Enabled := False;
    Desconectar.Enabled := True;
end;

procedure TFmSocketU.DesconectarClick(Sender: TObject);
begin
    If Sock.Active then
       Sock.Active := False;
    Host.Enabled     := True;
    Port.Enabled     := True;
    Conectar.Enabled := True;
    Desconectar.Enabled := False;
end;

procedure TFmSocketU.ConsultarClick(Sender: TObject);
Var
  Trans : String;
  Resp  : String;
  Flag  : Boolean;
begin
  if Not Sock.Active then
     begin
     ShowMessage('Voc� deve conectar-se antes');
     exit;
     end;

  Trans := 'CSR10C04               ';
  Trans := Trans + ZeroFill(Codigo.Text,8) + SpaceFill(Senha.Text,8);
  Trans := Trans + SpaceFill(Consulta.Text,8);
  If Cpf.Text <> '' Then
    Trans := Trans + 'CPF' + ZeroFill(CPF.Text,11)  + '      '
  else
    Trans := Trans + '                    ';
  If Rg.Text <> '' Then
     Trans := Trans + 'RG'  + ZeroFill(RG.Text,11)  + SpaceFill(Uf.Text,2) + '     '
  else
    Trans := Trans + '                    ';
  Trans := Trans + '                    ';
  Trans := Trans + SpaceFill(Nome.Text,50);
  Trans := Trans + ZeroFill(Nascimento.Text,8) + '31122009';
  Trans := Trans + ZeroFill(Banco.Text,3) + ZeroFill(Agencia.Text,5);
  Trans := Trans + ZeroFill(Conta.Text,16) + ZeroFill(Cheque.Text,9);
  Trans := Trans + ZeroFill(Qtde.Text,1) + 'XX' + '00000000199';
  Trans := Trans + '                                                  ';
  Trans := Trans + '0000' + '000000000' + '00000' + '000' + ' ' +  '        ';


  Transacao.Text := '';
  Transacao.Lines.Add('Enviando '+IntToStr(StrLen(Pchar(Trans))) + ' bytes');
  Transacao.Lines.Add(Trans);

  Consultar.Caption := 'Aguarde ...';
  Sock.Sendln(Trans, #$D);
  Flag := False;
  Sock.WaitForData(60000);
  while Not Flag do
    begin
    Resp := Sock.Receiveln(#$D);
    if Copy(Port.Text,1,4) = '9162' Then
      begin
      Resposta.Lines.Add('Recebeu bloco de ' + IntToStr(StrLen(Pchar(Resp))) + ' bytes');
      Resposta.Lines.Add(Resp);
      Flag := True;
      end
    else
      begin
      Resposta.Lines.Add('Recebeu bloco de ' + IntToStr(StrLen(Pchar(Resp))) + ' bytes');
      case StrToInt(Copy(Resp,40,1)) of
        0: Flag := True;
        1:begin
          Sock.Sendln('',#$D);
          Flag := False;
          end;
        9: Flag := True;
      end; {Case}
      Resposta.Lines.Add(Resp);
    end;
  end; {While}
  Consultar.Caption := 'Consultar';
  Consulta.Enabled  := True;
end;

procedure TFmSocketU.SairClick(Sender: TObject);
begin
  If Sock.Active then
     Sock.Active := False;

  Close;
end;

procedure TFmSocketU.Button3Click(Sender: TObject);
begin
     Resposta.Text := '';
end;

Function TFmSocketU.ZeroFill ( Numero : String; Tamanho : Byte ) : String;
Var
  I : Byte;
begin
  For I:=1 To Tamanho - Length(Numero) Do
    Numero := '0' + Numero;

  Result := Numero;
end;

Function TFmSocketU.SpaceFill ( Numero : String; Tamanho : Byte ) : String;
Var
  I : Byte;
begin
  For I:=1 To Tamanho - Length(Numero) Do
    Numero := Numero + ' ';

  Result := Numero;
end;

end.
