unit DiarioAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkMemo, ComCtrls, UnDmkProcFunc,
  dmkEditDateTimePicker, dmkLabel, dmkGeral, Mask, Grids, DBGrids, Variants,
  dmkImage, Menus, UnDmkEnums;

type
  TFmDiarioAdd = class(TForm)
    Panel1: TPanel;
    QrDiarioAss: TmySQLQuery;
    DsDiarioAss: TDataSource;
    QrDiarioAssCodigo: TIntegerField;
    QrDiarioAssCodUsu: TIntegerField;
    QrDiarioAssNome: TWideStringField;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrEnt1: TmySQLQuery;
    DsEnt1: TDataSource;
    PnDeptos: TPanel;
    CBDepto: TdmkDBLookupComboBox;
    EdDepto: TdmkEditCB;
    LaDepto: TLabel;
    PnFiltro: TPanel;
    Label1: TLabel;
    EdDiarioAss: TdmkEditCB;
    CBDiarioAss: TdmkDBLookupComboBox;
    LaCliInt: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    LaEntidade1: TLabel;
    EdEntidade1: TdmkEditCB;
    CBEntidade1: TdmkDBLookupComboBox;
    PnTempo: TPanel;
    MeNome: TdmkMemo;
    QrEnt1Codigo: TIntegerField;
    QrDeptos: TmySQLQuery;
    IntegerField2: TIntegerField;
    DsDeptos: TDataSource;
    QrDeptosNome: TWideStringField;
    SpeedButton1: TSpeedButton;
    QrDiarioAssAplicacao: TIntegerField;
    Panel3: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    QrEnt: TmySQLQuery;
    QrEntContato: TWideStringField;
    QrEntEmail: TWideStringField;
    QrEntTe1: TWideStringField;
    QrEntTe2: TWideStringField;
    QrEntTe3: TWideStringField;
    QrEntCel: TWideStringField;
    QrEntFax: TWideStringField;
    QrEntTe1_TXT: TWideStringField;
    QrEntTe2_TXT: TWideStringField;
    QrEntTe3_TXT: TWideStringField;
    QrEntCel_TXT: TWideStringField;
    QrEntFax_TXT: TWideStringField;
    DsEnt: TDataSource;
    QrEntiContat: TmySQLQuery;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    QrEntiContatCargo: TIntegerField;
    QrEntiContatNOME_CARGO: TWideStringField;
    DsEntiContat: TDataSource;
    QrEntiMail: TmySQLQuery;
    QrEntiMailNOMEETC: TWideStringField;
    QrEntiMailConta: TIntegerField;
    QrEntiMailEMail: TWideStringField;
    QrEntiMailEntiTipCto: TIntegerField;
    DsEntiMail: TDataSource;
    QrEntiTel: TmySQLQuery;
    QrEntiTelNOMEETC: TWideStringField;
    QrEntiTelConta: TIntegerField;
    QrEntiTelTelefone: TWideStringField;
    QrEntiTelEntiTipCto: TIntegerField;
    QrEntiTelTEL_TXT: TWideStringField;
    QrEntiTelRamal: TWideStringField;
    DsEntiTel: TDataSource;
    PnContatos: TPanel;
    DBGEntiContat: TDBGrid;
    Panel5: TPanel;
    DBGEntiTel: TDBGrid;
    DBGEntiMail: TDBGrid;
    TabSheet3: TTabSheet;
    Panel6: TPanel;
    DBGrid1: TDBGrid;
    Panel7: TPanel;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    Panel8: TPanel;
    QrPrc: TmySQLQuery;
    DsPrc: TDataSource;
    QrPrcContat: TmySQLQuery;
    DsPrcContat: TDataSource;
    QrPrcMail: TmySQLQuery;
    DsPrcMail: TDataSource;
    QrPrcTel: TmySQLQuery;
    DsPrcTel: TDataSource;
    QrPrcContato: TWideStringField;
    QrPrcEmail: TWideStringField;
    QrPrcTe1: TWideStringField;
    QrPrcTe2: TWideStringField;
    QrPrcTe3: TWideStringField;
    QrPrcCel: TWideStringField;
    QrPrcFax: TWideStringField;
    QrPrcTe1_TXT: TWideStringField;
    QrPrcTe2_TXT: TWideStringField;
    QrPrcTe3_TXT: TWideStringField;
    QrPrcCel_TXT: TWideStringField;
    QrPrcFax_TXT: TWideStringField;
    QrPrcContatControle: TIntegerField;
    QrPrcContatNome: TWideStringField;
    QrPrcContatCargo: TIntegerField;
    QrPrcContatNOME_CARGO: TWideStringField;
    QrPrcMailNOMEETC: TWideStringField;
    QrPrcMailConta: TIntegerField;
    QrPrcMailEMail: TWideStringField;
    QrPrcMailEntiTipCto: TIntegerField;
    QrPrcTelNOMEETC: TWideStringField;
    QrPrcTelConta: TIntegerField;
    QrPrcTelTelefone: TWideStringField;
    QrPrcTelEntiTipCto: TIntegerField;
    QrPrcTelTEL_TXT: TWideStringField;
    QrPrcTelRamal: TWideStringField;
    QrPrcNO_PRC: TWideStringField;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Panel9: TPanel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    Label8: TLabel;
    DBEdit3: TDBEdit;
    Label9: TLabel;
    DBEdit4: TDBEdit;
    Label10: TLabel;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    LaEntidade2: TLabel;
    EdEntidade2: TdmkEditCB;
    CBEntidade2: TdmkDBLookupComboBox;
    RGEntidade: TRadioGroup;
    QrLoc2: TmySQLQuery;
    QrLoc2Ent1: TIntegerField;
    QrLoc2Ent2: TIntegerField;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    QrEnt2: TmySQLQuery;
    DsEnt2: TDataSource;
    QrEnt2Codigo: TIntegerField;
    QrEnt2CliInt: TIntegerField;
    QrCliIntCodigo: TIntegerField;
    QrCliIntFilial: TIntegerField;
    QrCliIntNOMEFILIAL: TWideStringField;
    QrCliIntCNPJ_CPF: TWideStringField;
    QrEnt2Creditor: TmySQLQuery;
    DsEnt2Creditor: TDataSource;
    QrEnt2CreditorCNPJ: TWideStringField;
    QrEnt2CreditorNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel10: TPanel;
    SpeedButton4: TSpeedButton;
    PMContatos: TPopupMenu;
    Incluinovocontato1: TMenuItem;
    AlteraContatoatual1: TMenuItem;
    Excluicontatos1: TMenuItem;
    PMEMails: TPopupMenu;
    NovoEMail1: TMenuItem;
    Alteraemailselecionado1: TMenuItem;
    Excluiemailselecionado1: TMenuItem;
    PMTelefones: TPopupMenu;
    Incluinovotelefone1: TMenuItem;
    Alteratelefoneatual1: TMenuItem;
    Excluitelefones1: TMenuItem;
    Panel12: TPanel;
    CkContinuar: TCheckBox;
    Panel13: TPanel;
    BtOK: TBitBtn;
    BtGerencia: TBitBtn;
    PnCadastroB: TPanel;
    BtContatos: TBitBtn;
    BtEMails: TBitBtn;
    BtTelefones: TBitBtn;
    QrEnt2CodUsu: TIntegerField;
    QrEnt1CodUsu: TIntegerField;
    QrEnt1CliInt: TIntegerField;
    QrEnt1NOMEENTIDADE: TWideStringField;
    QrEnt2NOMEENTIDADE: TWideStringField;
    Timer1: TTimer;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    EdCodigo: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    Label5: TLabel;
    Label6: TLabel;
    EdHora: TdmkEdit;
    GroupBox2: TGroupBox;
    Label111: TLabel;
    EdBinaFone: TdmkEdit;
    Label3: TLabel;
    TPBinaSoDt: TdmkEditDateTimePicker;
    Label13: TLabel;
    EdBinaSoHr: TdmkEdit;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCliIntChange(Sender: TObject);
    procedure EdCliIntExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdEntidade1Change(Sender: TObject);
    procedure QrEntCalcFields(DataSet: TDataSet);
    procedure QrEntiContatAfterScroll(DataSet: TDataSet);
    procedure QrEntiContatBeforeClose(DataSet: TDataSet);
    procedure QrEntAfterScroll(DataSet: TDataSet);
    procedure QrEntBeforeClose(DataSet: TDataSet);
    procedure QrEntiContatAfterOpen(DataSet: TDataSet);
    procedure QrPrcAfterScroll(DataSet: TDataSet);
    procedure QrPrcBeforeClose(DataSet: TDataSet);
    procedure QrPrcCalcFields(DataSet: TDataSet);
    procedure QrPrcContatAfterOpen(DataSet: TDataSet);
    procedure QrPrcContatAfterScroll(DataSet: TDataSet);
    procedure QrPrcContatBeforeClose(DataSet: TDataSet);
    procedure RGEntidadeClick(Sender: TObject);
    procedure EdDeptoChange(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure EdEntidade2Change(Sender: TObject);
    procedure BtGerenciaClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure Incluinovocontato1Click(Sender: TObject);
    procedure AlteraContatoatual1Click(Sender: TObject);
    procedure Excluicontatos1Click(Sender: TObject);
    procedure NovoEMail1Click(Sender: TObject);
    procedure Alteraemailselecionado1Click(Sender: TObject);
    procedure Excluiemailselecionado1Click(Sender: TObject);
    procedure Incluinovotelefone1Click(Sender: TObject);
    procedure Alteratelefoneatual1Click(Sender: TObject);
    procedure Excluitelefones1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BtContatosClick(Sender: TObject);
    procedure PMContatosPopup(Sender: TObject);
    procedure PMEMailsPopup(Sender: TObject);
    procedure PMTelefonesPopup(Sender: TObject);
    procedure BtTelefonesClick(Sender: TObject);
    procedure BtEMailsClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure QrEntiTelCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FCliInt: Integer;
    procedure ReabreDeptos();
    procedure EditaEntidade(Qual, Modelo: Integer);
    procedure MostraEdicao();
    procedure DefineEntiSorc(var DestQr: TmySQLQuery; var DestDs: TDataSource);
    procedure DefineEntidades(var Origem, Atual: Integer);
  public
    { Public declarations }
    FChamou,
    FProcurador, FDepto: Integer;
    FNomeTipoPRC: String;
    procedure ReopenEntidade();
    procedure ReopenEntiContat(Controle: Integer);
    procedure ReopenEntiMail(Conta: Integer);
    procedure ReopenEntiTel(Conta: Integer);

    procedure ReopenPrcContat(Controle: Integer);
    procedure ReopenPrcMail(Conta: Integer);
    procedure ReopenPrcTel(Conta: Integer);

    procedure PreencheDados(DataHora: Boolean; Data: TDate; Hora: TTime;
              DiarioAss, CliInt, Depto, Entidade1, Entidade2: Integer; Texto: WideString);
    procedure MostraEntiContat(SQLType: TSQLType);
    procedure MostraEntiMail(SQLType: TSQLType);
    procedure MostraEntiTel(SQLType: TSQLType);
  end;

  var
  FmDiarioAdd: TFmDiarioAdd;

implementation

uses DiarioAss, UnInternalConsts, MyDBCheck, UMySQLModule, Module, DiarioGer,
  Entidade2, Entidades, UnDiario_Tabs, UnMyObjects, EntiRapido0, EntiContat,
  EntiMail, EntiTel, DmkDAC_PF;

{$R *.DFM}

procedure TFmDiarioAdd.AlteraContatoatual1Click(Sender: TObject);
begin
  MostraEntiContat(stUpd);
end;

procedure TFmDiarioAdd.Alteraemailselecionado1Click(Sender: TObject);
begin
  MostraEntiMail(stUpd);
end;

procedure TFmDiarioAdd.Alteratelefoneatual1Click(Sender: TObject);
begin
  MostraEntiTel(stUpd);
end;

procedure TFmDiarioAdd.BtContatosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMContatos, BtContatos);
end;

procedure TFmDiarioAdd.BtEMailsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEMails, BtEMails);
end;

procedure TFmDiarioAdd.BtGerenciaClick(Sender: TObject);
var
  Entidade: Integer;
begin
  case RGEntidade.ItemIndex of
    0: Entidade := EdEntidade1.ValueVariant;
    1: Entidade := EdEntidade1.ValueVariant;
    else Entidade := 0;
  end;
  if DBCheck.CriaFm(TFmDiarioGer, FmDiarioGer, afmoNegarComAviso) then
  begin
    FmDiarioGer.EdDiarioAss.ValueVariant := EdDiarioAss.ValueVariant;
    FmDiarioGer.CBDiarioAss.KeyValue     := EdDiarioAss.ValueVariant;
    FmDiarioGer.EdCliInt.ValueVariant    := EdCliInt.ValueVariant;
    FmDiarioGer.CBCliInt.KeyValue        := EdCliInt.ValueVariant;
    FmDiarioGer.EdEntidade.ValueVariant  := Entidade;
    FmDiarioGer.CBEntidade.KeyValue      := Entidade;
    FmDiarioGer.ShowModal;
    FmDiarioGer.Destroy;
  end;
end;

procedure TFmDiarioAdd.BtOKClick(Sender: TObject);
var
  DiarioAss, Entidade, CliInt, Depto, Codigo, Genero: Integer;
  Nome, Data, Hora, BinaFone, BinaDtHr: String;
begin
  if not UmyMod.ObtemCodigoDeCodUsu(EdDiarioAss, DiarioAss,
    'Informe o assunto!') then Exit;
  //
  CliInt   := EdCliInt.ValueVariant;
  if CliInt <> 0 then
    CliInt := QrCliIntCodigo.Value;
  if MyObjects.FIC(Geral.IntInConjunto(1, QrDiarioAssAplicacao.Value) and
    (CliInt = 0), EdCliInt, 'Informe o campo "'+LaCliInt.Caption+'"') then Exit;
  //
  case RGEntidade.ItemIndex of
    0: Entidade := EdEntidade1.ValueVariant;
    1: Entidade := EdEntidade1.ValueVariant;
    else Entidade := 0;
  end;
  if MyObjects.FIC(Geral.IntInConjunto(2, QrDiarioAssAplicacao.Value) and
    (Entidade = 0), EdEntidade1, 'Informe o campo "Entidade:"!') then Exit;
  //
  if UpperCase(Application.Title) = 'CREDITOR' then
  begin
    Depto  := FDepto;
    Genero := CO_DIARIO_ADD_GENERO_LCTCTRL;
  end else
  begin
    Depto  := EdDepto.ValueVariant;
    Genero := CO_DIARIO_ADD_GENERO_LCTDEPTO;
  end;
  if MyObjects.FIC(Geral.IntInConjunto(4, QrDiarioAssAplicacao.Value) and
    (Depto = 0) and (PnDeptos.Visible = True), EdDepto,
    'Informe o campo "' + LaDepto.Caption+'"') then Exit;
  //
  Data := Geral.FDT(TPData.Date, 1);
  Hora := EdHora.Text;
  Nome := MeNome.Text;
  if MyObjects.FIC(Trim(Nome) = '', nil, 'Texto n�o definido!') then Exit;
  //
  BinaFone := Geral.SoNumero_TT(EdBinaFone.Text);
  BinaDtHr := Geral.FDT(TPBinaSoDt.Date, 1) + ' ' + EdBinaSoHr.Text + ':00';
  Codigo := UMyMod.BuscaEmLivreY_Def('diarioadd', 'Codigo', ImgTipo.SQLType,
    EdCodigo.ValueVariant);
  if MyObjects.FIC(Codigo = 0, nil, 'C�digo n�o definido!') then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'diarioadd', False, [
    'Nome', 'DiarioAss', 'Entidade',
    'CliInt', 'Depto', 'Data', 'Hora', 'Genero',
    'BinaFone', 'BinaDtHr'
  ], ['Codigo'], [
    Nome, DiarioAss, Entidade,
    CliInt, Depto, Data, Hora, Genero,
    BinaFone, BinaDtHr
  ], [Codigo], True) then
  begin
    case FChamou of
      // de qualquer lugar:
      -1:;
      // do gerenciador: FmDiarioGer
      1: FmDiarioGer.ReopenDiario(Codigo);
      // n�o informado:
      else//0:
      begin
        if not CkContinuar.Checked then
          Geral.MB_Erro(
            'N�o foi informada a janela que chamou a inclus�o de evento! ' +
            sLineBreak + 'Avise a DERMATEK!');
      end;
    end;
    { Trocar no futoro o FChamou pelo codigo abaixo:
    if FQry <> nil then
    begin
      UnDMKDAC_PF.AbreQuery(FQry, FQry.Database);
      FQry.Last;
    end;
    }
    if CkContinuar.Checked then
    begin
      TPData.Date := Date;
      EdHora.ValueVariant := Time;
      //
      MeNome.Text := '';
      //
      EdDepto.ValueVariant := 0;
      CbDepto.KeyValue := NULL;
      //
      EdEntidade1.ValueVariant := 0;
      CbEntidade1.KeyValue := NULL;
      //
      EdEntidade2.ValueVariant := 0;
      CbEntidade2.KeyValue := NULL;
      //
    end else
      Close;
    //
  end;
end;

procedure TFmDiarioAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDiarioAdd.BtTelefonesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTelefones, BtTelefones);
end;

procedure TFmDiarioAdd.DefineEntidades(var Origem, Atual: Integer);
begin
  case RGEntidade.ItemIndex of
    0:
    begin
      Origem := QrEnt1Codigo.Value;
      Atual  := QrEnt1Codigo.Value;
    end;
    1:
    begin
      Origem := QrEnt2Codigo.Value;
      Atual  := QrEnt2Codigo.Value;
    end;
  end;
end;

procedure TFmDiarioAdd.DefineEntiSorc(var DestQr: TmySQLQuery;
  var DestDs: TDataSource);
begin
  case RGEntidade.ItemIndex of
    0:
    begin
      DestQr := QrEnt1;
      DestDs := DsEnt1;
    end;
    1:
    begin
      DestQr := QrEnt2;
      DestDs := DsEnt2;
    end;
  end;
end;

procedure TFmDiarioAdd.EdCliIntChange(Sender: TObject);
begin
  if not EdCliInt.Focused then
    ReabreDeptos();
end;

procedure TFmDiarioAdd.EdCliIntExit(Sender: TObject);
begin
  if FCliInt <> EdCliInt.ValueVariant then
  begin
    FCliInt := EdCliInt.ValueVariant;
    ReabreDeptos();
  end;
end;

procedure TFmDiarioAdd.EdDeptoChange(Sender: TObject);
begin
  if EdDepto.ValueVariant <> 0 then
  begin
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      QrLoc2.Close;
      QrLoc2.SQL.Clear;
      QrLoc2.SQL.Add('SELECT Propriet Ent1, Usuario Ent2');
      QrLoc2.SQL.Add('FROM condimov');
      QrLoc2.SQL.Add('WHERE Conta=' + FormatFloat('0', EdDepto.ValueVariant));
      UnDmkDAC_PF.AbreQuery(QrLoc2, Dmod.MyDB);
      if (QrLoc2Ent1.Value <> 0) or (QrLoc2Ent2.Value <> 0) then
      begin
        EdEntidade1.ValueVariant := QrLoc2Ent1.Value;
        CBEntidade1.KeyValue     := QrLoc2Ent1.Value;
        EdEntidade2.ValueVariant := QrLoc2Ent2.Value;
        CBEntidade2.KeyValue     := QrLoc2Ent2.Value;
      end;
    end;
  end;  
end;

procedure TFmDiarioAdd.EdEntidade1Change(Sender: TObject);
begin
  ReopenEntidade();
end;

procedure TFmDiarioAdd.EdEntidade2Change(Sender: TObject);
begin
  ReopenEntidade();
end;

procedure TFmDiarioAdd.EditaEntidade(Qual, Modelo: Integer);
var
  Ent: Integer;
  Ed: TdmkEditCB;
  CB: TdmkDBLookupComboBox;
  Continua: Boolean;
begin
  Continua     := False;
  VAR_CADASTRO := 0;
  case Qual of
    0:
    begin
      Ent := EdEntidade1.ValueVariant;
      Ed := EdEntidade1;
      CB := CBEntidade1;
    end;
    1:
    begin
      Ent := EdEntidade2.ValueVariant;
      Ed := EdEntidade2;
      CB := CBEntidade2;
    end else
    begin
      Ent := 0;
      Ed := nil;
      CB := nil;
    end;
  end;
  case Modelo of
    0:
    begin
      if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
      begin
        FmEntidades.LocCod(Ent, Ent);
        FmEntidades.ShowModal;
        FmEntidades.Destroy;
        //
        Continua := True;
      end;
    end;
    1:
    begin
      if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
      begin
        FmEntidade2.LocCod(Ent, Ent);
        FmEntidade2.ShowModal;
        FmEntidade2.Destroy;
        //
        Continua := True;
      end;
    end;
    else
    begin
      Geral.MB_Erro('Modelo n�o implementado!');
      Exit;
    end;
  end;
  if Continua then
  begin
    QrEnt1.Close;
    UnDmkDAC_PF.AbreQuery(QrEnt1, Dmod.MyDB);
    //
    QrEnt2.Close;
    UnDmkDAC_PF.AbreQuery(QrEnt2, Dmod.MyDB);
    //
    if VAR_CADASTRO <> 0 then
    begin
      Ed.ValueVariant := VAR_CADASTRO;
      CB.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmDiarioAdd.Excluicontatos1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntiContat, DBGEntiContat,
    'enticontat', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmDiarioAdd.Excluiemailselecionado1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntimail, DBGEntiMail,
    'entimail', ['Conta'], ['Conta'], istPergunta, '');
end;

procedure TFmDiarioAdd.Excluitelefones1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntitel, DBGEntiTel,
    'entitel', ['Conta'], ['Conta'], istPergunta, '');
end;

procedure TFmDiarioAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if FProcurador <> 0 then
  begin
    QrPrc.Close;
    QrPrc.Params[0].AsInteger := FProcurador;
    UnDmkDAC_PF.AbreQuery(QrPrc, Dmod.MyDB);
    //
    if FNomeTipoPRC <> '' then
      TabSheet3.Caption := ' Dados do ' + FNomeTipoPRC + ' ';
    PageControl1.ActivePageIndex := 2;
  end;
  PnCadastroB.Visible := PageControl1.ActivePageIndex = 1;
end;

procedure TFmDiarioAdd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MostraEdicao;
  //
  PageControl1.ActivePageIndex := 0;
  FProcurador := 0;
  FNomeTipoPRC := '';
  UnDmkDAC_PF.AbreQuery(QrDiarioAss, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEnt1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    QrCliInt.Close;
    QrCliInt.SQL.Clear;
    QrCliInt.SQL.Add('SELECT DISTINCT ent.Codigo, ent.CliInt Filial,');
    QrCliInt.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,');
    QrCliInt.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF');
    QrCliInt.SQL.Add('FROM entidades ent');
    QrCliInt.SQL.Add('WHERE ent.CliInt <> 0');
    //
    QrDeptos.Close;
    QrDeptos.SQL.Clear;
    QrDeptos.SQL.Add('SELECT Conta Codigo, Unidade Nome ');
    QrDeptos.SQL.Add('FROM condimov ');
    QrDeptos.SQL.Add('WHERE Codigo=:P0');
    //
    LaDepto.Caption  := 'Unidade';
    PnDeptos.Visible := True;
  end else
  begin
    QrCliInt.Close;
    QrCliInt.SQL.Clear;
    QrCliInt.SQL.Add('SELECT DISTINCT ent.Codigo, ent.Filial,');
    QrCliInt.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,');
    QrCliInt.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF');
    QrCliInt.SQL.Add('FROM entidades ent');
    QrCliInt.SQL.Add('WHERE ent.Codigo < -10');
  end;
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  TPData.Date := Date;
  TPData.Time := Time;
  EdHora.ValueVariant := Time;
  //
end;

procedure TFmDiarioAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDiarioAdd.Incluinovocontato1Click(Sender: TObject);
begin
  MostraEntiContat(stIns);
end;

procedure TFmDiarioAdd.Incluinovotelefone1Click(Sender: TObject);
begin
  MostraEntiTel(stIns);
end;

procedure TFmDiarioAdd.PageControl1Change(Sender: TObject);
begin
  PnCadastroB.Visible := PageControl1.ActivePageIndex = 1;
end;

procedure TFmDiarioAdd.PMContatosPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita :=
    (QrEntiContat.State <> dsInactive) and (QrEntiContat.RecordCount > 0);
  AlteraContatoatual1.Enabled := Habilita;
  Habilita := Habilita and
    (QrEntiMail.State <> dsInactive) and (QrEntiMail.RecordCount = 0) and
    (QrEntiTel.State <> dsInactive) and (QrEntiTel.RecordCount = 0);
  Excluicontatos1.Enabled := Habilita;
end;

procedure TFmDiarioAdd.PMEMailsPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEntiContat.State <> dsInactive) and (QrEntiContat.RecordCount > 0);
  NovoEMail1.Enabled := Habilita;
  Habilita := (QrEntiMail.State <> dsInactive) and (QrEntiMail.RecordCount > 0);
  Alteraemailselecionado1.Enabled := Habilita;
  Excluiemailselecionado1.Enabled := Habilita;
end;

procedure TFmDiarioAdd.PMTelefonesPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEntiContat.State <> dsInactive) and (QrEntiContat.RecordCount > 0);
  Incluinovotelefone1.Enabled := Habilita;
  Habilita := (QrEntiTel.State <> dsInactive) and (QrEntiTel.RecordCount > 0);
  Alteratelefoneatual1.Enabled := Habilita;
  Excluitelefones1.Enabled := Habilita;
end;

procedure TFmDiarioAdd.PreencheDados(DataHora: Boolean; Data: TDate; Hora:
  TTime; DiarioAss, CliInt, Depto, Entidade1, Entidade2: Integer; Texto: WideString);
begin
  if DataHora then
  begin
    TPData.Date := Data;
    EdHora.ValueVariant := Hora;
  end;
  if DiarioAss <> 0 then
  begin
    EdDiarioAss.ValueVariant := DiarioAss;
    CBDiarioAss.KeyValue := DiarioAss;
  end;
  if CliInt <> 0 then
  begin
    EdCliInt.ValueVariant := CliInt;
    CBCliInt.KeyValue := CliInt;
  end;
  if Depto <> 0 then
  begin
    EdDepto.ValueVariant := Depto;
    CBDepto.KeyValue := Depto;
  end;
  if Entidade1 <> 0 then
  begin
    EdEntidade1.ValueVariant := Entidade1;
    CBEntidade1.KeyValue := Entidade1;
  end;
  if Entidade2 <> 0 then
  begin
    EdEntidade2.ValueVariant := Entidade2;
    CBEntidade2.KeyValue := Entidade2;
  end;
  if Texto <> '' then
    MeNome.Text := Texto;
end;

procedure TFmDiarioAdd.QrEntAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiContat(0);
end;

procedure TFmDiarioAdd.QrEntBeforeClose(DataSet: TDataSet);
begin
  QrEntiContat.Close();
end;

procedure TFmDiarioAdd.QrEntCalcFields(DataSet: TDataSet);
begin
  QrEntTe1_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntTe1.Value);
  QrEntTe2_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntTe2.Value);
  QrEntTe3_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntTe3.Value);
  QrEntCel_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntCel.Value);
  QrEntFax_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntFax.Value);
end;

procedure TFmDiarioAdd.QrEntiContatAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrEntiContat.RecordCount > 0;
  if Habilita then
    PageControl1.ActivePageIndex := 1
  else
    PageControl1.ActivePageIndex := 0;
  BtContatos.Enabled  := Habilita;
  BtTelefones.Enabled := Habilita;
  BtEMails.Enabled    := Habilita;
end;

procedure TFmDiarioAdd.QrEntiContatAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiMail(0);
  ReopenEntiTel(0);
end;

procedure TFmDiarioAdd.QrEntiContatBeforeClose(DataSet: TDataSet);
begin
  QrEntiTel.Close;
  QrEntiMail.Close;
  BtContatos.Enabled  := False;
  BtTelefones.Enabled := False;
  BtEMails.Enabled    := False;
end;

procedure TFmDiarioAdd.QrEntiTelCalcFields(DataSet: TDataSet);
begin
  QrEntiTelTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntiTelTelefone.Value);
end;

procedure TFmDiarioAdd.QrPrcAfterScroll(DataSet: TDataSet);
begin
  ReopenPrcContat(0);
end;

procedure TFmDiarioAdd.QrPrcBeforeClose(DataSet: TDataSet);
begin
  QrPrcContat.Close();
end;

procedure TFmDiarioAdd.QrPrcCalcFields(DataSet: TDataSet);
begin
  QrPrcTe1_TXT.Value := Geral.FormataTelefone_TT_Curto(QrPrcTe1.Value);
  QrPrcTe2_TXT.Value := Geral.FormataTelefone_TT_Curto(QrPrcTe2.Value);
  QrPrcTe3_TXT.Value := Geral.FormataTelefone_TT_Curto(QrPrcTe3.Value);
  QrPrcCel_TXT.Value := Geral.FormataTelefone_TT_Curto(QrPrcCel.Value);
  QrPrcFax_TXT.Value := Geral.FormataTelefone_TT_Curto(QrPrcFax.Value);
end;

procedure TFmDiarioAdd.QrPrcContatAfterOpen(DataSet: TDataSet);
begin
{
  if QrEntiContat.RecordCount > 0 then
    PageControl1.ActivePageIndex := 1
  else
    PageControl1.ActivePageIndex := 0;
}
end;

procedure TFmDiarioAdd.QrPrcContatAfterScroll(DataSet: TDataSet);
begin
  ReopenPrcMail(0);
  ReopenPrcTel(0);
end;

procedure TFmDiarioAdd.QrPrcContatBeforeClose(DataSet: TDataSet);
begin
  QrPrcTel.Close;
  QrPrcMail.Close;
end;

procedure TFmDiarioAdd.ReabreDeptos();
var
  Codigo: Integer;
begin
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
     EdDepto.ValueVariant := 0;
     CBDepto.KeyValue     := 0;
     //
     Codigo := QrCliIntFilial.Value;
     QrDeptos.Close;
     QrDeptos.Params[0].AsInteger := Codigo;
     UnDmkDAC_PF.AbreQuery(QrDeptos, Dmod.MyDB);
     //
     PnDeptos.Visible := True;
  end else
  begin
    //
  end;
end;

procedure TFmDiarioAdd.ReopenEntiContat(Controle: Integer);
begin
  QrEntiContat.Close;
  case RGEntidade.ItemIndex of
    0: QrEntiContat.Params[0].AsInteger := QrEnt1Codigo.Value;
    1: QrEntiContat.Params[0].AsInteger := QrEnt2Codigo.Value;
  end;
  UnDmkDAC_PF.AbreQuery(QrEntiContat, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrEntiContat.Locate('Controle', Controle, []);
end;

procedure TFmDiarioAdd.ReopenEntidade();
var
  Entidade: Integer;
begin
  case RGEntidade.ItemIndex of
    0: Entidade := EdEntidade1.ValueVariant;
    1: Entidade := EdEntidade2.ValueVariant;
    else Entidade := 0;
  end;
  QrEnt.Close;
  QrEnt.Params[0].AsInteger := Entidade;//EdEntidade.ValueVariant;
  UnDmkDAC_PF.AbreQuery(QrEnt, Dmod.MyDB);
end;

procedure TFmDiarioAdd.ReopenEntiMail(Conta: Integer);
begin
  QrEntiMail.Close;
  QrEntiMail.Params[0].AsInteger := QrEntiContatControle.Value;
  UnDmkDAC_PF.AbreQuery(QrEntiMail, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrEntiMail.Locate('Conta', Conta, []);
end;

procedure TFmDiarioAdd.ReopenEntiTel(Conta: Integer);
begin
  QrEntiTel.Close;
  QrEntiTel.Params[0].AsInteger := QrEntiContatControle.Value;
  UnDmkDAC_PF.AbreQuery(QrEntiTel, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrEntiTel.Locate('Conta', Conta, []);
end;

procedure TFmDiarioAdd.ReopenPrcContat(Controle: Integer);
begin
  QrPrcContat.Close;
  QrPrcContat.Params[0].AsInteger := FProcurador;
  UnDmkDAC_PF.AbreQuery(QrPrcContat, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrPrcContat.Locate('Controle', Controle, []);
end;

procedure TFmDiarioAdd.ReopenPrcMail(Conta: Integer);
begin
  QrPrcMail.Close;
  QrPrcMail.Params[0].AsInteger := QrPrcContatControle.Value;
  UnDmkDAC_PF.AbreQuery(QrPrcMail, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrPrcMail.Locate('Conta', Conta, []);
end;

procedure TFmDiarioAdd.ReopenPrcTel(Conta: Integer);
begin
  QrPrcTel.Close;
  QrPrcTel.Params[0].AsInteger := QrPrcContatControle.Value;
  UnDmkDAC_PF.AbreQuery(QrPrcTel, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrPrcTel.Locate('Conta', Conta, []);
end;

procedure TFmDiarioAdd.RGEntidadeClick(Sender: TObject);
var
  Cor: TColor;
begin
  ReopenEntidade();
  Cor := clGray;
  LaEntidade1.Font.Color := Cor;
  EdEntidade1.Font.Color := Cor;
  CBEntidade1.Font.Color := Cor;
  //
  LaEntidade2.Font.Color := Cor;
  EdEntidade2.Font.Color := Cor;
  CBEntidade2.Font.Color := Cor;
  //
  Cor := clRed;
  case RGEntidade.ItemIndex of
    0:
    begin
      LaEntidade1.Font.Color := Cor;
      EdEntidade1.Font.Color := Cor;
      CBEntidade1.Font.Color := Cor;
    end;
    1:
    begin
      LaEntidade2.Font.Color := Cor;
      EdEntidade2.Font.Color := Cor;
      CBEntidade2.Font.Color := Cor;
    end;
  end;
end;

procedure TFmDiarioAdd.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmDiarioAss, FmDiarioAss, afmoNegarComAviso) then
  begin
    FmDiarioAss.ShowModal;
    FmDiarioAss.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodUsuDeCodigo(EdDiarioAss, CBDiarioAss, QrDiarioAss,
      VAR_CADASTRO, 'Codigo', 'CodUsu');
  end;
end;

procedure TFmDiarioAdd.SpeedButton2Click(Sender: TObject);
begin
  EditaEntidade(0, 1);
end;

procedure TFmDiarioAdd.SpeedButton3Click(Sender: TObject);
begin
  if UpperCase(Application.Title) = 'CREDITOR' then
    EditaEntidade(1, 0)
  else
    EditaEntidade(1, 1);
end;

procedure TFmDiarioAdd.SpeedButton4Click(Sender: TObject);
var
  Entidade, Destino: Integer;
begin
  if DBCheck.CriaFm(TFmEntiRapido0, FmEntiRapido0, afmoNegarComAviso) then
  begin
    FmEntiRapido0.ShowModal;
    Entidade := FmEntiRapido0.FCodigo;
    FmEntiRapido0.Destroy;
    //
    if Entidade <> 0 then
    begin
      QrEnt1.Close;
      UnDmkDAC_PF.AbreQuery(QrEnt1, Dmod.MyDB);
      //
      QrEnt2.Close;
      UnDmkDAC_PF.AbreQuery(QrEnt2, Dmod.MyDB);
      //
      Destino := MyObjects.SelRadioGroup('Uso do cadastro rec�m feito',
      'Defina como deseja usar o cadastro rec�m feito',
      ['N�o quero usar', LaEntidade1.Caption, LaEntidade2.Caption], 1);
      case Destino of
        0: ; // nada
        1:
        begin
          EdEntidade1.ValueVariant := Entidade;
          CBEntidade1.KeyValue     := Entidade;
        end;
        2:
        begin
          EdEntidade2.ValueVariant := Entidade;
          CBEntidade2.KeyValue     := Entidade;
        end;
        else
          Geral.MB_Erro('Uso n�o implementado. Solicite � DERMATEK!');
      end;
    end;
  end;
end;

procedure TFmDiarioAdd.Timer1Timer(Sender: TObject);
begin
  PnCadastroB.Visible := PageControl1.ActivePageIndex = 1;
end;

procedure TFmDiarioAdd.MostraEdicao;
begin
  if UpperCase(Application.Title) = 'SYNDIC' then
  begin
    LaDepto.Caption      := 'Unidade';
    PnDeptos.Visible     := True;
    SpeedButton3.Visible := True;
    //
    RGEntidade.Items.Clear;
    RGEntidade.Items.Add('Contatado 1');
    RGEntidade.Items.Add('Contatado 2');
  end else
  if UpperCase(Application.Title) = 'CREDITOR' then
  begin
    PnDeptos.Visible     := False;
    SpeedButton3.Visible := False;
    //
    RGEntidade.Items.Clear;
    RGEntidade.Items.Add('Contatado 1');
  end;
  RGEntidade.ItemIndex := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrEnt2, Dmod.MyDB);
end;

procedure TFmDiarioAdd.MostraEntiContat(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiContat, FmEntiContat, afmoNegarComAviso,
  QrEntiContat, SQLType) then
  begin
    FmEntiContat.FQrEntiContat := QrEntiContat;
    //DefineEntiSorc(FmEntiContat.FQrEntidades, FmEntiContat.FDsEntidades);
    DefineEntidades(FmEntiContat.FEntidadeOri, FmEntiContat.FEntidadeAtu);
{
    FmEntiContat.FEntidadeOri  := QrEntiContatCodigo.Value;
    FmEntiContat.FEntidadeAtu  := QrEntidadesCodigo.Value;
}
    FmEntiContat.ShowModal;
    FmEntiContat.Destroy;
  end;
end;

procedure TFmDiarioAdd.MostraEntiMail(SQLType: TSQLType);
var
  DestDS: TDataSource;
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiMail, FmEntiMail, afmoNegarComAviso,
  QrEntiMail, SQLType) then
  begin
    FmEntiMail.FQrEntiMail := QrEntiMail;
    DefineEntiSorc(FmEntiMail.FQrEntidades, DestDS);
    FmEntiMail.FDsEntidades := DestDs;
    FmEntiMail.FQrEntiContat  := QrEntiContat;
    FmEntiMail.FDsEntiContat  := DsEntiContat;
    //
    FmEntiMail.DBEdCodigo.DataSource := DestDs;
    FmEntiMail.DBEdCodUsu.DataSource := DestDs;
    FmEntiMail.DBEdNome.DataSource := DestDs;
    //
    FmEntiMail.DBEdContatoCod.DataSource := DsEntiContat;
    FmEntiMail.DBEdContatoNom.DataSource := DsEntiContat;
    FmEntiMail.DBEdContatoCrg.DataSource := DsEntiContat;
    //
    FmEntiMail.ShowModal;
    FmEntiMail.Destroy;
  end;
end;

procedure TFmDiarioAdd.MostraEntiTel(SQLType: TSQLType);
var
  DestDS: TDataSource;
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiTel, FmEntiTel, afmoNegarComAviso,
  QrEntiTel, SQLType) then
  begin
    FmEntiTel.FQrEntiTel := QrEntiTel;
    DefineEntiSorc(FmEntiTel.FQrEntidades, DestDS);
    FmEntiTel.FDsEntidades := DestDs;
    FmEntiTel.FQrEntiContat  := QrEntiContat;
    FmEntiTel.FDsEntiContat  := DsEntiContat;
    //
    FmEntiTel.DBEdCodigo.DataSource := DestDs;
    FmEntiTel.DBEdCodUsu.DataSource := DestDs;
    FmEntiTel.DBEdNome.DataSource := DestDs;
    //
    FmEntiTel.DBEdContatoCod.DataSource := DsEntiContat;
    FmEntiTel.DBEdContatoNom.DataSource := DsEntiContat;
    FmEntiTel.DBEdContatoCrg.DataSource := DsEntiContat;
    //
    FmEntiTel.ShowModal;
    FmEntiTel.Destroy;
  end;
end;

procedure TFmDiarioAdd.NovoEMail1Click(Sender: TObject);
begin
  MostraEntiMail(stIns);
end;

end.
