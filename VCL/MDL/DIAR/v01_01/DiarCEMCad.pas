unit DiarCEMCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmDiarCEMCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrDiarCEMCad: TmySQLQuery;
    DsDiarCEMCad: TDataSource;
    QrDiarCEMIts: TmySQLQuery;
    DsDiarCEMIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrDiarCEMItsCodigo: TIntegerField;
    QrDiarCEMItsControle: TIntegerField;
    QrDiarCEMItsDias: TIntegerField;
    QrDiarCEMItsSMS: TIntegerField;
    QrDiarCEMItsEmail: TIntegerField;
    QrDiarCEMItsCarta: TIntegerField;
    QrDiarCEMItsNO_TXTSMS: TWideStringField;
    QrDiarCEMItsNO_PREEMAIL: TWideStringField;
    QrDiarCEMItsNO_CARTA: TWideStringField;
    QrDiarCEMCadCodigo: TIntegerField;
    QrDiarCEMCadNome: TWideStringField;
    QrDiarCEMItsDdNoEnv: TIntegerField;
    QrDiarCEMItsCartaoAR: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrDiarCEMCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrDiarCEMCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrDiarCEMCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraDiarCEMIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenDiarCEMIts(Controle: Integer);

  end;

var
  FmDiarCEMCad: TFmDiarCEMCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, DiarCEMIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmDiarCEMCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmDiarCEMCad.MostraDiarCEMIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmDiarCEMIts, FmDiarCEMIts, afmoNegarComAviso) then
  begin
    FmDiarCEMIts.ImgTipo.SQLType := SQLType;
    FmDiarCEMIts.FQrCab := QrDiarCEMCad;
    FmDiarCEMIts.FDsCab := DsDiarCEMCad;
    FmDiarCEMIts.FQrIts := QrDiarCEMIts;
    if SQLType = stIns then
      //
    else
    begin
      FmDiarCEMIts.EdControle.ValueVariant := QrDiarCEMItsControle.Value;
      FmDiarCEMIts.EdDias.ValueVariant     := QrDiarCEMItsDias.Value;
      FmDiarCEMIts.EdDDnoEnv.ValueVariant  := QrDiarCEMItsDDnoEnv.Value;

      FmDiarCEMIts.EdSMS.ValueVariant := QrDiarCEMItsSMS.Value;
      FmDiarCEMIts.CBSMS.KeyValue := QrDiarCEMItsSMS.Value;

      FmDiarCEMIts.EdEmail.ValueVariant := QrDiarCEMItsEmail.Value;
      FmDiarCEMIts.CBEmail.KeyValue := QrDiarCEMItsEmail.Value;

      FmDiarCEMIts.EdCarta.ValueVariant := QrDiarCEMItsCarta.Value;
      FmDiarCEMIts.CBCarta.KeyValue := QrDiarCEMItsCarta.Value;

      FmDiarCEMIts.RGCartaoAR.ItemIndex := QrDiarCEMItsCartaoAR.Value;

    end;
    FmDiarCEMIts.ShowModal;
    FmDiarCEMIts.Destroy;
  end;
end;

procedure TFmDiarCEMCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrDiarCEMCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrDiarCEMCad, QrDiarCEMIts);
end;

procedure TFmDiarCEMCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrDiarCEMCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrDiarCEMIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrDiarCEMIts);
end;

procedure TFmDiarCEMCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrDiarCEMCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmDiarCEMCad.DefParams;
begin
  VAR_GOTOTABELA := 'diarcemcad';
  VAR_GOTOMYSQLTABLE := QrDiarCEMCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM diarcemcad');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmDiarCEMCad.ItsAltera1Click(Sender: TObject);
begin
  MostraDiarCEMIts(stUpd);
end;

procedure TFmDiarCEMCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MensagemBox('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name, 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmDiarCEMCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmDiarCEMCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmDiarCEMCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'DiarCEMIts', 'Controle', QrDiarCEMItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrDiarCEMIts,
      QrDiarCEMItsControle, QrDiarCEMItsControle.Value);
    ReopenDiarCEMIts(Controle);
  end;
end;

procedure TFmDiarCEMCad.ReopenDiarCEMIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDiarCEMIts, Dmod.MyDB, [
  'SELECT txt.Nome NO_TXTSMS,  ',
  'pre.Nome NO_PREEMAIL, cep.Titulo NO_CARTA, dci.*  ',
  'FROM diarcemits dci ',
  'LEFT JOIN txtsms txt ON txt.Codigo=dci.SMS ',
  'LEFT JOIN preemail pre ON pre.Codigo=dci.Email ',
  'LEFT JOIN cartas cep ON cep.Codigo=dci.Carta ',
  'WHERE dci.Codigo=' + Geral.FF0(QrDiarCEMCadCodigo.Value),
  'ORDER BY dci.Dias ',
  '']);
  //
  QrDiarCEMIts.Locate('Controle', Controle, []);
end;


procedure TFmDiarCEMCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmDiarCEMCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmDiarCEMCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmDiarCEMCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmDiarCEMCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmDiarCEMCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDiarCEMCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrDiarCEMCadCodigo.Value;
  Close;
end;

procedure TFmDiarCEMCad.ItsInclui1Click(Sender: TObject);
begin
  MostraDiarCEMIts(stIns);
end;

procedure TFmDiarCEMCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrDiarCEMCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'diarcemcad');
end;

procedure TFmDiarCEMCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('diarcemcad', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrDiarCEMCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'diarcemcad',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmDiarCEMCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'diarcemcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'diarcemcad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmDiarCEMCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmDiarCEMCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmDiarCEMCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmDiarCEMCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrDiarCEMCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmDiarCEMCad.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmDiarCEMCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmDiarCEMCad.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  //LaRegistro.Caption := GOTOy.CodUsu(QrDiarCEMCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmDiarCEMCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmDiarCEMCad.QrDiarCEMCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmDiarCEMCad.QrDiarCEMCadAfterScroll(DataSet: TDataSet);
begin
  ReopenDiarCEMIts(0);
end;

procedure TFmDiarCEMCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrDiarCEMCadCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmDiarCEMCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrDiarCEMCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'diarcemcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmDiarCEMCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDiarCEMCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrDiarCEMCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'diarcemcad');
end;

procedure TFmDiarCEMCad.QrDiarCEMCadBeforeOpen(DataSet: TDataSet);
begin
  QrDiarCEMCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

