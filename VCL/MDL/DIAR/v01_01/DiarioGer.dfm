object FmDiarioGer: TFmDiarioGer
  Left = 339
  Top = 185
  Caption = 'GER-DIARI-001 :: Gerencimento do Di'#225'rio'
  ClientHeight = 576
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 414
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 632
      Height = 414
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 632
        Height = 229
        Align = alTop
        Caption = ' Pesquisa: '
        TabOrder = 0
        object PnFiltro: TPanel
          Left = 2
          Top = 15
          Width = 628
          Height = 166
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 41
            Height = 13
            Caption = 'Assunto:'
          end
          object LaCliInt: TLabel
            Left = 8
            Top = 44
            Width = 70
            Height = 13
            Caption = 'Cliente interno:'
          end
          object Label3: TLabel
            Left = 8
            Top = 84
            Width = 52
            Height = 13
            Caption = 'Contatado:'
          end
          object Label11: TLabel
            Left = 8
            Top = 124
            Width = 73
            Height = 13
            Caption = 'Parte do Texto:'
          end
          object LaLctCtrl: TLabel
            Left = 520
            Top = 124
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object EdDiarioAss: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdDiarioAssChange
            DBLookupComboBox = CBDiarioAss
            IgnoraDBLookupComboBox = False
          end
          object CBDiarioAss: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 556
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsDiarioAss
            TabOrder = 1
            dmkEditCB = EdDiarioAss
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCliInt: TdmkEditCB
            Left = 8
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCliIntChange
            OnExit = EdCliIntExit
            DBLookupComboBox = CBCliInt
            IgnoraDBLookupComboBox = False
          end
          object CBCliInt: TdmkDBLookupComboBox
            Left = 64
            Top = 60
            Width = 556
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DsCliInt
            TabOrder = 3
            dmkEditCB = EdCliInt
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdEntidade: TdmkEditCB
            Left = 8
            Top = 100
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEntidadeChange
            DBLookupComboBox = CBEntidade
            IgnoraDBLookupComboBox = False
          end
          object CBEntidade: TdmkDBLookupComboBox
            Left = 64
            Top = 100
            Width = 556
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOME_ENT'
            ListSource = DsEntidades
            TabOrder = 5
            dmkEditCB = EdEntidade
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdNome: TEdit
            Left = 8
            Top = 140
            Width = 505
            Height = 21
            TabOrder = 6
            OnChange = EdNomeChange
          end
          object EdLctCtrl: TEdit
            Left = 520
            Top = 140
            Width = 100
            Height = 21
            TabOrder = 7
            OnChange = EdNomeChange
          end
        end
        object PnDeptos: TPanel
          Left = 2
          Top = 181
          Width = 628
          Height = 46
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          Visible = False
          object PnDepto: TPanel
            Left = 272
            Top = 0
            Width = 356
            Height = 46
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            Visible = False
            object LaDepto1: TLabel
              Left = 8
              Top = 4
              Width = 70
              Height = 13
              Caption = 'Departamento:'
            end
            object EdDepto: TdmkEditCB
              Left = 8
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdDeptoChange
              DBLookupComboBox = CBDepto
              IgnoraDBLookupComboBox = False
            end
            object CBDepto: TdmkDBLookupComboBox
              Left = 64
              Top = 20
              Width = 284
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsDeptos
              TabOrder = 1
              dmkEditCB = EdDepto
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 272
            Height = 46
            Align = alLeft
            Caption = ' Per'#237'odo: '
            TabOrder = 0
            object Label10: TLabel
              Left = 127
              Top = 20
              Width = 15
              Height = 13
              Caption = 'at'#233
            end
            object TPDataIni: TdmkEditDateTimePicker
              Left = 8
              Top = 16
              Width = 116
              Height = 21
              Date = 40125.821355567130000000
              Time = 40125.821355567130000000
              TabOrder = 0
              OnClick = TPDataIniClick
              OnChange = TPDataIniChange
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object TPDataFim: TdmkEditDateTimePicker
              Left = 148
              Top = 16
              Width = 116
              Height = 21
              Date = 40125.821355567130000000
              Time = 40125.821355567130000000
              TabOrder = 1
              OnClick = TPDataFimClick
              OnChange = TPDataFimChange
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
          end
        end
      end
      object DBMemo1: TDBMemo
        Left = 0
        Top = 301
        Width = 632
        Height = 113
        TabStop = False
        Align = alClient
        DataField = 'Nome'
        DataSource = DsDiarioAdd
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 13264128
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 1
      end
      object Panel4: TPanel
        Left = 0
        Top = 229
        Width = 632
        Height = 72
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object Label2: TLabel
          Left = 4
          Top = 4
          Width = 70
          Height = 13
          Caption = 'Cliente interno:'
        end
        object Label4: TLabel
          Left = 4
          Top = 27
          Width = 45
          Height = 13
          Caption = 'Entidade:'
        end
        object LaDepto2: TLabel
          Left = 4
          Top = 52
          Width = 70
          Height = 13
          Caption = 'Departamento:'
        end
        object Label6: TLabel
          Left = 216
          Top = 52
          Width = 49
          Height = 13
          Caption = 'User cad.:'
          FocusControl = DBEdit4
        end
        object Label7: TLabel
          Left = 428
          Top = 52
          Width = 42
          Height = 13
          Caption = 'User alt.:'
          FocusControl = DBEdit5
        end
        object Label8: TLabel
          Left = 312
          Top = 52
          Width = 50
          Height = 13
          Caption = 'Data cad.:'
          FocusControl = DBEdit6
        end
        object Label9: TLabel
          Left = 524
          Top = 52
          Width = 43
          Height = 13
          Caption = 'Data alt.:'
          FocusControl = DBEdit7
        end
        object DBEdit1: TDBEdit
          Left = 80
          Top = 0
          Width = 544
          Height = 21
          DataField = 'NOME_CLI'
          DataSource = DsDiarioAdd
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 80
          Top = 24
          Width = 544
          Height = 21
          DataField = 'NOME_ENT'
          DataSource = DsDiarioAdd
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 80
          Top = 48
          Width = 133
          Height = 21
          DataField = 'NOME_DEPTO'
          DataSource = DsDiarioAdd
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 268
          Top = 48
          Width = 40
          Height = 21
          DataField = 'UserCad'
          DataSource = DsDiarioAdd
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 472
          Top = 48
          Width = 48
          Height = 21
          DataField = 'UserAlt'
          DataSource = DsDiarioAdd
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Left = 364
          Top = 48
          Width = 56
          Height = 21
          DataField = 'DataCad'
          DataSource = DsDiarioAdd
          TabOrder = 5
        end
        object DBEdit7: TDBEdit
          Left = 568
          Top = 48
          Width = 56
          Height = 21
          DataField = 'DataCad'
          DataSource = DsDiarioAdd
          TabOrder = 6
        end
      end
    end
    object Panel5: TPanel
      Left = 632
      Top = 0
      Width = 376
      Height = 414
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 376
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object RGOrderBy: TRadioGroup
          Left = 0
          Top = 0
          Width = 376
          Height = 48
          Align = alClient
          Caption = ' Ordena'#231#227'o: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Data > Hora > Entidade'
            'Entidade > Data > Hora')
          TabOrder = 0
          OnClick = RGOrderByClick
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 48
        Width = 376
        Height = 366
        Align = alClient
        DataSource = DsDiarioAdd
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Data'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Hora'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_UH_ENT'
            Title.Caption = 'Contatado'
            Width = 218
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 287
        Height = 32
        Caption = 'Gerencimento do Di'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 287
        Height = 32
        Caption = 'Gerencimento do Di'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 287
        Height = 32
        Caption = 'Gerencimento do Di'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 462
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 506
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 18
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object QrDiarioAss: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, Aplicacao'
      'FROM diarioass'
      'ORDER BY Nome')
    Left = 548
    Top = 8
    object QrDiarioAssCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDiarioAssCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrDiarioAssNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrDiarioAssAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object DsDiarioAss: TDataSource
    DataSet = QrDiarioAss
    Left = 576
    Top = 8
  end
  object QrDeptos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ?'
      'ORDER BY Nome')
    Left = 604
    Top = 8
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDeptosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsDeptos: TDataSource
    DataSet = QrDeptos
    Left = 632
    Top = 8
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo, ent.CliInt Filial,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF'
      'FROM entidades ent'
      'WHERE ent.CliInt <> 0')
    Left = 660
    Top = 8
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrCliIntNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Required = True
      Size = 100
    end
    object QrCliIntCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 688
    Top = 8
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOME_ENT'
      'FROM entidades'
      'ORDER BY NOME_ENT')
    Left = 716
    Top = 8
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 744
    Top = 8
  end
  object QrDiarioAdd: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrDiarioAddAfterOpen
    BeforeClose = QrDiarioAddBeforeClose
    SQL.Strings = (
      'SELECT CONCAT(dpt.Unidade, " - ", '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NOME_UH_ENT,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLI,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_ENT,'
      'dpt.Unidade NOME_DEPTO, dad.*'
      'FROM diarioadd dad'
      'LEFT JOIN entidades cli ON cli.Codigo=dad.CliInt'
      'LEFT JOIN entidades ent ON ent.Codigo=dad.Entidade'
      'LEFT JOIN condimov  dpt ON dpt.Conta =dad.Depto')
    Left = 8
    Top = 8
    object QrDiarioAddNOME_CLI: TWideStringField
      FieldName = 'NOME_CLI'
      Size = 100
    end
    object QrDiarioAddNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrDiarioAddNOME_DEPTO: TWideStringField
      FieldName = 'NOME_DEPTO'
      Size = 10
    end
    object QrDiarioAddCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDiarioAddNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrDiarioAddDiarioAss: TIntegerField
      FieldName = 'DiarioAss'
    end
    object QrDiarioAddEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrDiarioAddCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrDiarioAddDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrDiarioAddData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddHora: TTimeField
      FieldName = 'Hora'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrDiarioAddDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDiarioAddUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDiarioAddNOME_UH_ENT: TWideStringField
      FieldName = 'NOME_UH_ENT'
      Size = 113
    end
  end
  object DsDiarioAdd: TDataSource
    DataSet = QrDiarioAdd
    Left = 36
    Top = 8
  end
  object frxGER_DIARI_001_01: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38101.979964398100000000
    ReportOptions.LastChange = 39722.438154294000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxGER_DIARI_001_01GetValue
    Left = 64
    Top = 8
    Datasets = <
      item
        DataSet = frxDsDiarioAdd
        DataSetName = 'frxDsDiarioAdd'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 190.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object TfrxGroupHeader
        Height = 22.677180000000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDiarioAdd."NOME_UH_ENT"'
        object Memo2: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Width = 680.314960630000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDiarioAdd."NOME_UH_ENT"]')
          ParentFont = False
        end
      end
      object TfrxGroupFooter
        Height = 7.559060000000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 3.779530000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object PageHeader4: TfrxPageHeader
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Atividades Reportadas no Di'#225'rio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 207.874150000000000000
        Width = 680.315400000000000000
        Child = frxGER_DIARI_001_01.Child1
        DataSet = frxDsDiarioAdd
        DataSetName = 'frxDsDiarioAdd'
        RowCount = 0
        object Memo3: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Width = 34.015721180000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 34.015672360000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDiarioAdd."Data"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 128.503873540000000000
          Width = 30.236178980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Hora:')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 158.740064720000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = 'hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDiarioAdd."Hora"]')
          ParentFont = False
        end
      end
      object Child1: TfrxChild
        Height = 18.897650000000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        Stretched = True
        object Rich1: TfrxRichView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataField = 'Nome'
          DataSet = frxDsDiarioAdd
          DataSetName = 'frxDsDiarioAdd'
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6465
            666C616E67313034367B5C666F6E7474626C7B5C66305C666E696C205461686F
            6D613B7D7D0D0A7B5C2A5C67656E657261746F72204D7366746564697420352E
            34312E32312E323531303B7D5C766965776B696E64345C7563315C706172645C
            66305C667331365C7061720D0A7D0D0A00}
        end
      end
    end
  end
  object frxDsDiarioAdd: TfrxDBDataset
    UserName = 'frxDsDiarioAdd'
    CloseDataSource = False
    DataSet = QrDiarioAdd
    BCDToCurrency = False
    Left = 92
    Top = 8
  end
end
