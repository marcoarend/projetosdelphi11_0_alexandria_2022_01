unit Diario_PF;

interface

uses
  Windows, Forms, StdCtrls, ComCtrls, ExtCtrls, Messages, SysUtils, Classes,
  //MaskUtils, Registry, ShellAPI, TypInfo, comobj, ShlObj, Winsock, Math,
  Graphics, Controls, Dialogs, Variants, UnMsgInt, dmkGeral, UnInternalConsts,
  DB, mySQLDBTables, UnDmkProcFunc, UnDmkEnums;

type
  TUnDiario_PF = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
    procedure MostraDiarioAddByFoneComm(Data, Hora, Fone: String;
              EhServer: Boolean; Forcado: Integer);
    procedure MostraFormDiarioAdd();
  end;

var
  UnDiario_PF: TUnDiario_PF;

implementation

uses
  {$IFDef UsaDiario_01}
    DiarioTDI_01, UMySQLModule, Module, Principal, DiarioAdd, DiarioGer2,
  {$EndIF}
  {$IFDef UsaDiario_02}
    DiarioAdd_02,
  {$EndIF}
  DiarioAss,
  ModuleGeral, MyDBCheck, UnMyObjects;

{ TUnDiario_PF }

procedure TUnDiario_PF.MostraDiarioAddByFoneComm(Data, Hora, Fone: String;
EhServer: Boolean; Forcado: Integer);
  procedure IncluiChamada(Dt: TDateTime; Hr: String; Forcado: Integer);
  var
    Nome, xData, xHora, dData, dHora, IP: String;
    Codigo, Usuario, Terminal: Integer;
  begin
    Codigo   := UMyMod.BuscaEmLivreY_Def_Geral('binaligoua', 'Codigo', stIns, 0, nil);
    Nome     := Fone;
    xData    := Data;
    xHora    := Hora;
    dData    := Geral.FDT(Dt, 1);
    dHora    := Hr;
    Usuario  := 0;
    Terminal := 0;
    IP       := '';

    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'binaligoua', False, [
    'Nome', 'xData', 'xHora',
    'dData', 'dHora', 'Usuario',
    'Terminal', 'IP', 'Forcado'],
    ['Codigo'], [
    Nome, xData, xHora,
    dData, dHora, Usuario,
    Terminal, IP, Forcado],
    [Codigo], True);
  end;
const
  AceitaZero = False;
  EhBR = False;
  FormContat = 1; // Telefone
var
  Dt: TDateTime;
  Hr, Tel, BinaFone, BinaDtHr: String;
  Ano: Integer;
  CliInt, Entidade, Interloctr, PreAtend, Codigo, Terceiro01: Integer;
  Qry: TmySQLQuery;
  Hoje: TDateTime;
  Agora: TTime;
  Form: TForm;
begin
  if Length(Data) = 4 then
  begin
    Dt := DModG.ObtemAgora();
    Ano := Geral.IMV(FormatDateTime('YYYY', Dt));
    Dt := Geral.ValidaData4Num_SemAno(Data, Ano, AceitaZero, EhBR);
  end else Dt := 0;
  Tel := Geral.FormataTelefone_TT(Fone);
  if Length(Hora) = 4 then
  begin
    Hr := Copy(Hora, 1, 2) + ':' + Copy(Hora, 3);
  end else Hr := '00:00';
{$IFDef UsaDiario_01}
  if EhServer then
  begin
    IncluiChamada(Dt, Hr, Forcado);
  end else
  begin
    //FmDiarioTDI_01.Show;
    Form := MyObjects.FormTDICria(TFmDiarioTDI_01,
      FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
    CliInt := 0;
    Entidade := 0;
    Interloctr := 0;
    Terceiro01 := 0;
    if Length(Trim(Fone)) > 0 then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        Qry.DataBase := DMod.MyDB;
        //
        Qry.SQL.Add('SELECT Codigo, Controle ');
        Qry.SQL.Add('FROM entitel ');
        Qry.SQL.Add('WHERE Telefone LIKE "%' + Fone + '%" ');
        // Buscar o �ltimo cadastrado!
        Qry.SQL.Add('ORDER BY Controle DESC');
        Qry.Open;
        //
        if Qry.RecordCount > 0 then
        begin
          Qry.First;
          //n�o setou entidade! ver
          Entidade := Qry.FieldByName('Codigo').AsInteger;
          Interloctr := Qry.FieldByName('Controle').AsInteger;
        end else
        begin
          Qry.Close;
          Qry.SQL.Clear;
          Qry.SQL.Add('SELECT Codigo ');
          Qry.SQL.Add('FROM entidades ');
          Qry.SQL.Add('WHERE (ETe1 LIKE "%' + Fone + '%" ');
          Qry.SQL.Add('AND Tipo=0) ');
          Qry.SQL.Add('OR (PTe1 LIKE "%' + Fone + '%" ');
          Qry.SQL.Add('AND Tipo=1) ');
          Qry.Open;
          Entidade := Qry.FieldByName('Codigo').AsInteger;
        end;
        //
      finally
        Qry.Free;
      end;
    end;
    if (DModG.QrEmpresas.State <> dsInactive)
    and (DModG.QrEmpresas.RecordCount = 1) then
      CliInt := DModG.QrEmpresasFilial.Value;
    DModG.ObtemDataHora(Hoje, Agora);
    Data := Geral.FDT(Hoje, 1);
    Hora := FormatDateTime('hh:nn', Agora);
    BinaFone := Fone;
    BinaDtHr := Trim(Geral.FDT(Dt, 1) + ' ' + Hr + ':00');
    if BinaDtHr = '' then
      BinaDtHr := '0000-00-00 00:00:00';
    //
    PreAtend := 1;  // Excluir o registro no futuro se n�o mudar para 0 (ou seja, se n�o confirmar o atendimento)
    Codigo := UMyMod.BuscaEmLivreY_Def('diarioadd', 'Codigo', stIns, 0);

    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'diarioadd', False, [
    'Entidade', 'CliInt', 'Data', 'Hora',
    'BinaFone', 'BinaDtHr', 'Interloctr',
    'Terceiro01', 'PreAtend', 'FormContat'], [
    'Codigo'], [
    Entidade, CliInt, Data, Hora,
    BinaFone, BinaDtHr, Interloctr,
    Terceiro01, PreAtend, FormContat], [
    Codigo], True);
    //
    TFmDiarioTDI_01(Form).MostraAdd(-1, stUpd, Codigo, Dt, Hr, Tel);
  end;
{$EndIf}
{$IFDef UsaDiario_02}
  if DBCheck.CriaFm(TFmDiarioAdd_02, FmDiarioAdd_02, afmoNegarComAviso) then
  begin
    FmDiarioAdd_02.FChamou        := -1;
    FmDiarioAdd_02.ImgTipo.SQLType := stIns;
    FmDiarioAdd_02.TPBinaSoDt.Date := Dt;
    FmDiarioAdd_02.EdBinaSoHr.Text := Hr;
    FmDiarioAdd_02.EdBinaFone.Text := Tel;
    FmDiarioAdd_02.ShowModal;
    FmDiarioAdd_02.Destroy;
  end;
{$EndIf}
end;

procedure TUnDiario_PF.MostraFormDiarioAdd(Chamou: Integer);
begin
  if DBCheck.CriaFm(TFmDiarioAdd, FmDiarioAdd, afmoNegarComAviso) then
  begin
    FmDiarioAdd.FChamou         := Chamou;
    FmDiarioAdd.ImgTipo.SQLType := stIns;
    FmDiarioAdd.ShowModal;
    FmDiarioAdd.Destroy;
  end;
end;

end.
