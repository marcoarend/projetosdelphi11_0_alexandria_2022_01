unit UnDiario_Tabs;
{ Colocar no MyListas:

Uses UnDiario_Tabs;


//
function TMyListas.CriaListaTabelas:

      Diario_Tb.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
      Diario_Tb.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:

    Diario_Tb.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      Diario_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      Diario_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    Diario_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  Diario_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms,
  UnInternalConsts, dmkGeral, UnDmkEnums;

type
  TUnDiario_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  Diario_Tabs: TUnDiario_Tabs;

const
  CO_DIARIO_ADD_GENERO_NO_LINK  = 0;  // > Sem link
  CO_DIARIO_ADD_GENERO_LCTDEPTO = 1;  // > Lct.Depto
  CO_DIARIO_ADD_GENERO_LCTCTRL  = 2;   // > Lct.Controle
  CO_DIARIO_ADD_GENERO_ORD_SRV  = 3;   // > Ordem de servi�o no bugstrol
  CO_DIARIO_ADD_GENERO_POS_VDA  = 4;   // > p�s-venda no bugstrol
  CO_DIARIO_ADD_GENERO_POSGSRV  = 5;   // > p�s-garantia de servico no bugstrol
  CO_DIARIO_ADD_GENERO_POSGCXA  = 6;   // > p�s-garantia de Limpeza de Caixa D'�gua no bugstrol
  //
  MaxAllGeneros = 3;
  sListaAllGeneros: array[0..MaxAllGeneros] of String = ('Sem link',
  'U.H.', 'Lct.', 'O.S.');
  //
  //
  CO_DIARIO_ASSUNTO_FLD_NEED_ClienteInterno  = 1;
  CO_DIARIO_ASSUNTO_FLD_NEED_Entidade        = 2;
  CO_DIARIO_ASSUNTO_FLD_NEED_Departamento    = 4;
  CO_DIARIO_ASSUNTO_FLD_NEED_Terceiro01      = 8;
  CO_DIARIO_ASSUNTO_FLD_NEED_Interlocutor    = 16;
  //
  CO_DIARIO_ENVIO_MENSAGEM_SMS    = 1;
  CO_DIARIO_ENVIO_MENSAGEM_EMAIL  = 2;
  CO_DIARIO_ENVIO_MENSAGEM_CARTA  = 3;
  //
  //////////////////////////////////////////////////////////////////////////////
  ///  ERROS DE N�O ENVIO DE COBRAN�A
  ///
  // Campo diarcemenv.NaoEnvTip e diarcemret.NaoEnvTip
  CO_COBRANCA_NAO_ENV_TIP_NO_ERRO_YET = 0; // Nenhum problema constatado ainda
  CO_COBRANCA_NAO_ENV_TIP_SEM_RECIPIE = 1; // Falta de Cadastros de tel, email, endere�o, etc
  CO_COBRANCA_NAO_ENV_TIP_LCT_NAO_LOC = 2; // Lan�amento n�o localizado
  CO_COBRANCA_NAO_ENV_TIP_SMS_NAO_ENV = 3; // SMS n�o enviado
  CO_COBRANCA_NAO_ENV_TIP_MAIL_NO_ENV = 4; // EMAIL n�o enviado
  //
  // Campo diarcemenv.NaoEnvLst e diarcemret.NaoEnvLst
  CO_DIARIO_LISTA_ERROS_DMK_SEM_ERROS = 0;
  CO_DIARIO_LISTA_ERROS_DMK_SEND_MSGS = 1;
  CO_DIARIO_LISTA_ERROS_ACTIVE_XPERTS = 2; // unit AxSms_TLB
  CO_DIARIO_LISTA_ERROS_DEEP_SOFTWARE = 3; // ver se tem... componentes TnrComm, TnrGsm, etc...
  //
  // Campo diarcemenv.NaoEnvMot e diarcemret.NaoEnvMot
  (* Erros do CO_DIARIO_LISTA_ERROS_DMK_SEND_MSGS *)
  CO_COBRANCA_NAO_ENV_MOT_ENVIO_FEITO = 0;
  CO_COBRANCA_NAO_ENV_MOT_NO_CAD_FONE = 1;
  CO_COBRANCA_NAO_ENV_MOT_NO_CAD_MAIL = 2;
  CO_COBRANCA_NAO_ENV_MOT_GENERIC_ERR = 3;

  (* Erros do CO_COBRANCA_NAO_ENV_TIP_LCT_NAO_LOC *)
  CO_COBRANCA_LCT_NAO_LOC_INEXISTENTE = 1; // Lan�amento n�o existe
  CO_COBRANCA_LCT_NAO_LOC_DATA_VENCTO = 2; // Vencimento alterado
  CO_COBRANCA_LCT_NAO_LOC_MEZ_COMPETE = 3; // M�s de compet�ncia alterado
  CO_COBRANCA_LCT_NAO_LOC_PROPRIETARI = 4; // Propriet�rio alterado
  (* Erros do CO_COBRANCA_NAO_ENV_TIP_SMS_NAO_ENV *)
  // Ver tabela de c�digos de erro no site do componente ActiveXperts:
  // http://www.activexperts.com/Support/errorcodes/
{
Errors 200-699 (ActiveEmail 3.2 - SMTP)

Error	Description
354	Start mail input; end with .
421	Service not available, closing transmission channel
441	No connection to SMTP server established, please connect first
450	Requested mail action not taken: mailbox unavailable (mailbox busy)
451	Requested action aborted: local error in processing
452	Requested action not taken: insufficient system storage
454	Client not authenticated, authentication required
500	Syntax error, command unrecognized (This may include errors such as command line too long)
501	Syntax error in parameters or arguments
502	Command not implemented
503	Bad sequence of commands
504	Command parameter not implemented
505	Client does not have permission to send as this sender; change the sender e-mail address
530	Please authenticate first
535	Authentication failed
550	Requested action not taken: mailbox unavailable (mailbox not found)
551	User not local; please try
552	Requested mail action aborted: exceeded storage allocation
553	Requested action not taken: mailbox name not allowed (mailbox syntax incorrect)
554	Transaction failed
590	Unexpected response from SMTP server
591	No response from SMTP server
592	Failed to send command
	
Errors 700-999 (ActiveEmail 3.2 - POP3)	
	
Error	Description
700	No welcome message received from POP3 server (no response at all, expected responde:'+OK POP3 Ready'
701	No welcome message received from POP3 server (invalid response, '+OK POP3 READY' was expected
702	No response received on '+USER' command
703	Invalid response received on 'USER' command
704	Login failed
705	No response received on 'PASS' command
706	Invalid response received on 'PASS' command
707	No connection to POP3 server established, please connect first
708	Invalid message indicator; message should start with '+OK '.
709	Invalid message (MIME header is empty)
710	The mailbox is already in use (locked)
711	No more messages available
712	Message is flagged as deleted
713	The mailbox does not contain message(s)
715	No response received on command
716	Failed to delete message (most likely the message does not exist)
717	Failed to count messages in mailbox. STAT command failed
720	Failed to save attachment, check if the specified directory exists and that you have sufficient rights
721	Failed to save attachment, file already exists
725	There is no message with this message ID
730	Attachment not found
731	Operation timed out
732	Operation stalled; Idle timeout expired.
	
Errors 1000-1999 (Licensing)	
	
Error	Description
1001	Evaluation period has expired. You must purchase the product, or else uninstall the software
1002	Registration code is invalid
1003	You need a professional or distribution license to use this function
1010	Creation of a new thread failed
1020	Failed to create instance of object, out of memory.
1030	Exception error occurred, process has been terminated
1040	Failed to allocate memory
1101	Invalid or no account name entered
1102	Invalid or no password entered
	
Errors 10000-11999 (WinSock Errors)	
	
Error	Description
10004	A blocking operation was interrupted by a call to WSACancelBlockingCall
10009	The file handle supplied is not valid
10013	An attempt was made to access a socket in a way forbidden by its access permissions
10014	The system detected an invalid pointer address in attempting to use a pointer argument in a call
10022	An invalid argument was supplied
10024	Too many open sockets
10035	A non-blocking socket operation could not be completed immediately
10036	A blocking operation is currently executing
10037	An operation was attempted on a non-blocking socket that already had an operation in progress
10038	An operation was attempted on something that is not a socket
10039	A required address was omitted from an operation on a socket
10040	A message sent on a datagram socket was larger than the internal message buffer or some other network limit, or the buffer used to receive a datagram into was smaller than the datagram itself
10041	A protocol was specified in the socket function call that does not support the semantics of the socket type requested
10042	An unknown, invalid, or unsupported option or level was specified in a getsockopt or setsockopt call
10043	The requested protocol has not been configured into the system, or no implementation for it exists
10044	The support for the specified socket type does not exist in this address family
10045	The attempted operation is not supported for the type of object referenced
10046	The protocol family has not been configured into the system or no implementation for it exists
10047	An address incompatible with the requested protocol was used
10048	Only one usage of each socket address (protocol/network address/port) is normally permitted
10049	The requested address is not valid in its context
10050	A socket operation encountered a dead network
10051	A socket operation was attempted to an unreachable network
10052	The connection has been broken due to keep-alive activity detecting a failure while the operation was in progress
10053	An established connection was aborted by the software in your host machine
10054	An existing connection was forcibly closed by the remote host
10055	An operation on a socket could not be performed because the system lacked sufficient buffer space or because a queue was full
10056	A connect request was made on an already connected socket
10057	A request to send or receive data was disallowed because the socket is not connected and (when sending on a datagram socket using a sendto call) no address was supplied
10058	A request to send or receive data was disallowed because the socket had already been shut down in that direction with a previous shutdown call
10059	Too many references to some kernel object
10060	A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond
10061	No connection could be made because the target machine actively refused it
10062	Cannot translate name
10063	Name component or name was too long
10064	A socket operation failed because the destination host was down
10065	A socket operation was attempted to an unreachable host
10066	Cannot remove a directory that is not empty
10067	A Windows Sockets implementation may have a limit on the number of applications that may use it simultaneously
10068	Ran out of quota
10069	Ran out of disk quota
10070	File handle reference is no longer available
10071	Item is not available locally
10091	WSAStartup cannot function at this time because the underlying system it uses to provide network services is currently unavailable
10092	The Windows Sockets version requested is not supported
10093	Either the application has not called WSAStartup, or WSAStartup failed
10101	Returned by WSARecv or WSARecvFrom to indicate the remote party has initiated a graceful shutdown sequence
10102	No more results can be returned by WSALookupServiceNext
10103	A call to WSALookupServiceEnd was made while this call was still processing. The call has been canceled
10104	The procedure call table is invalid
10105	The requested service provider is invalid
10106	The requested service provider could not be loaded or initialized
10107	A system call that should never fail has failed
10108	No such service is known. The service cannot be found in the specified name space
10109	The specified class was not found
10110	No more results can be returned by WSALookupServiceNext
10111	A call to WSALookupServiceEnd was made while this call was still processing. The call has been canceled
10112	A database query failed because it was actively refused
11001	No such host is known
11002	This is usually a temporary error during hostname resolution and means that the local server did not receive a response from an authoritative server
11003	A non-recoverable error occurred during a database lookup
11004	The requested name is valid and was found in the database, but it does not have the correct associated data being resolved for
11005	At least one reserve has arrived
11006	At least one path has arrived
11007	There are no senders
11008	There are no receivers
11009	Reserve has been confirmed
11010	Error due to lack of resources
11011	Rejected for administrative reasons - bad credentials
11012	Unknown or conflicting style
11013	Problem with some part of the filterspec or providerspecific buffer in general
11014	Problem with some part of the flowspec
11015	General QOS error
11999	General WSA error
	
Errors 15000-15299 (SMS Messaging Server - CFG)	
	
Error	Description
15000	Registry entry 'ServerRoot' missing
15001	Registry entry 'Configuration' missing
15002	ActiveX component [AxMmCfg.dll] not registered on this computer. Either manually register this DLL on this machine, or reinstall the product
15003	ActiveX component [AEmail.dll] not registered on this computer. Either manually register this DLL on this machine, or reinstall the product
15004	ActiveX component [AComPort.dll] not registered on this computer. Either manually register this DLL on this machine, or reinstall the product
15005	ActiveX component [ASocket.dll] not registered on this computer. Either manually register this DLL on this machine, or reinstall the product
15006	ActiveX component [AxPop3.dll] not registered on this computer. Either manually register this DLL on this machine, or reinstall the product
15007	ActiveX component [AXmsCtrl.dll] not registered on this computer. Either manually register this DLL on this machine, or reinstall the product
15008	ActiveX component [AxEdit.dll] not registered on this computer. Either manually register this DLL on this machine, or reinstall the product
15009	Registry entry 'Server' missing
15010	Internal error: unable to create new ActiveX object
15011	Internal error: unable to create new ADODB object
15012	Registry entry 'ClientRoot' missing
15013	One or more HTML files needed by the program are missing
15020	Unable to open the Configuration Database
15021	Unable to query the Configuration Database
15022	No more records
15023	Unable to create new configuration record; maximum allowed number is reached
15024	Unable to create new configuration record
15025	Unable to modify configuration record
15026	Unable to delete configuration record
15027	Incompatible configuration database version
15050	Empty Message Database Connection String
15051	Unable to open Message Database
15052	Syntax error in database query
15053	Syntax error in querying all database fields
15054	No more messages in message Database
15060	Unable to create new message
15061	Unable to update message
15062	Unable to update BLOB field in message
15063	Unable to execute query in Message Database
15064	Message record is not formatted correctly
15065	Syntax error in the WAP Message Body
15066	Invalid Data Body. Hexadecimal format is required, e.g.: FF C0 B2
15067	Incompatible message database version
15100	Empty Archive Database Connection String
15101	Unable to open Archive Database
15102	Unable to query Archive Database
15103	No more messages in Archive Database
15104	Unable to create new message in Archive Database
15105	Unable to update message in Archive Database
15106	Unable to update BLOB field in archived message
15107	Unable to execute query in Archive Database
15108	Unable to delete message from Archive Database
15109	Incompatible archive database version
15200	Invalid schedule time
15220	Message Reference is not allowed for SMS or MMS messages
15221	Header not allowed for this type of message
15222	Priority not allowed for this type of message
15223	Carbon Copies (CC, BCC) not allowed for this type of message
15224	Read receipt not allowed for this type of message
15225	Delivery report not allowed for this type of message
15226	Bodyformat invalid for this message type
15227	Could not find the message in the message database
15228	There are no TLV's attached to this message
15229	No more TLV's attached to this message
15230	TLV object expected
15231	No TLV found with this tag value
	
Errors 15300-15399 (SMS Messaging Server - Pop3)	
	
Error	Description
15300	No welcome message received from Pop3 Server (no response at all, expected response: '+OK POP3 Ready')
15301	No welcome message received from Pop3 Server (invalid response, '+OK POP3 Ready' was expected)
15302	No reponse received on '+USER' command
15303	Invalid reponse received on '+USER' command
15304	Login failed
15305	No reponse received on '+PASSWORD' command
15306	Invalid reponse received on '+PASSWORD' command
15307	No connection to POP3 server established
15308	Invalid message indicator; message should start with '+OK '.
15309	Invalid message (MIME header is empty)
15310	Cannlot load ACTIVEXPERTS:SOCKET module
15311	No more messages available
	
Errors 15400-15499 (SMS Messaging Server - SMPP)	
	
Error	Description
15400	Failed to start the SMPP thread
15401	Timeout while trying to connect to SMPP server
15402	Failed to connect to the SMPP server
15403	Timeout while communicating with SMPP server
15404	Timeout while trying to bind to SMPP server
15405	Found SMPP client connector in an unexpected state
15406	SMPP client out of memory
15407	SMPP server closed the connection unexpectedly
15408	SMPP client receive operation timed out
15409	SMPP client received unexpected PDU length
15410	SMPP client received a malformed PDU
15411	SMPP client internal error (Invalid parameter)
15412	SMPP client operation timed out
15413	SMPP timeout in unbind operation
15414	Could not find this message reference
15415	Failed to bind to SMPP server. Check system id, system type and / or password
15416	SMPP client internal error (invalid handle value)
15417	SMPP client internal error (invalid SMS message handle)
15418	SMPP client internal error (invalid SMS vector handle)
15419	No more messages
15420	Failed to load SMPP client library
15421	Invalid password while trying to connect to SMPP server
15422	Invalid system id while trying to connect to SMPP server
15423	Invalid service type while trying to connect to SMPP server
	
Errors 15500-15550 (SMS Messaging Server - HTTP)	
	
Error	Description
15500	Error when sending through HTTP
15501	The server did not reply with the expected success response
15502	The server replied with the error response
	
Errors 21990-22099 (ActiveSocket - Generic)	
	
Error	Description
21990	No connections pending
21991	Error while accepting a remote connection
21993	Already connected
21994	TCP operation stalled, aborted.
21995	TCP operation timed out
21996	Remote party disconnected unexpectedly
21997	Remote party disconnected
21998	No active connection
21999	Error during send
22000	Invalid parameter
22001	Buffer overflow
22002	No more bytes in buffer
22003	Listen failed
22004	Must listen to be able to accept
22005	Failed to allocate buffer memory
22019	Invalid TCP port number
22020	Invalid IP address
22021	Invalid hostname
22022	Unable to resolve hostname to IP-address
22023	Unable to create a socket
22024	Unable to connect to host
22025	Socket Error occurred
22026	Requested function not supported on this OS, or Dynamic Link Library is missing
22027	Failed to create thread
22028	Failed to stop thread
22030	Professional license required to use this option
22051	One ore more exported functions could not be found
	
Errors 22200-22299 (ActiveSocket - SNMP)	
	
Error	Description
22200	SNMPAPI.DLL could not be loaded because DLL was not found
22201	MGMTAPI.DLL could not be loaded because DLL was not found
22202	One or more exported functions missing in SNMPAPI.DLL
22203	One or more exported functions missing in MGMTAPI.DLL
22204	WSNMP32.DLL could not be loaded because DLL was not found
22205	One or more exported functions missing in WSNMP32.DLL
22210	SNMP Libraries not loaded; operation not allowed
22211	Unable to intialize WinSNMP
22212	SNMP Library not initialized
22213	Unable to open SNMP session
22214	SNMP Session already opened
22215	Open an SNMP session first
22216	SNMP Memory allocation failed
22217	Invalid parameter
22218	Agent is not responding (or agent is not running)
22219	Request operation failed
22220	Invalid SNMP hostname
22221	Unable to set port other than 161
22222	OID type not supported by ActiveSocket
22223	Unable to add trap
22224	No more OID's in SNMP tree
22225	To many OID's specified for this trap
22226	The specified object ID does not exist on this agent
22227	The specified entity does not exist on this agent
22240	No traps in receive queue
22241	Invalid call. Call Get(First) before calling GetNext
22242	Value is not compatible with specified type
22243	Windows SNMP Trap service not installed or started
22244	Invalid SNMP community name specified
22245	No more variables in SNMP trap
22250	Please specify the generic trap type
22251	Please specify the specific trap type
22260	The agent could not place the results of the requested SNMP operation into a single SNMP message
22261	The requested SNMP operation identified an unknown variable
22262	The requested SNMP operation tried to change a variable but it specified either a syntax or value error
22263	The requested SNMP operation tried to change a variable that was not allowed to change
22264	Generic error received from SNMP agent
22265	The specified SNMP variable is not accessible
22266	The value specifies a type which is inconsistent with the OID variable
22267	The length of the data is invalid
22268	Malformed packet detected
22269	Failed to create new value
22270	Failed to set value, specified data is inconsistent
22271	The resource is not available
22272	No variables were updated
22273	Undo failed
22274	An authorization error was occurred
22275	The variable exists but the agent cannot modify it.
22276	Inconsistent name
	
Errors 22300-22319 (ActiveSocket - NTP)	
	
Error	Description
22300	Invalid NTP host name
22301	Create socket failed
22302	Unable to connect to NTP server
22303	Connection to NTP server established, but request failed
22304	Request failed
22305	No response received
	
Errors 22320-22339 (ActiveSocket - ICMP)	
	
Error	Description
22320	Creation of socket failed
22321	Invalid host name
22322	Timeout
22323	Destination host unreachable
22324	Destination net unreachable
22325	Internal API error
22326	Bad option
22327	Hardware error
22328	Packet too big
22329	Bad request
22330	Bad route
22331	TTL expired
22332	Source Quench
22333	Bad Destination
22339	Generic ICMP error
	
Errors 22340-22349 (ActiveSocket - WOL)	
	
Error	Description
22340	Invalid MAC address
	
Errors 22350-22359 (ActiveSocket - DNS)	
	
Error	Description
22350	Generic DNS error
22351	Unable to load DNS library
22352	Library not initialized
22353	Unable to resolve DNS host to IP address
22354	Unable to connect to DNS server
22355	No more records available
22356	Sending DNS query failed
22357	No response from DNS server
22359	DNS server was queried successfully, but specified entry was not found
	
Errors 22400-22449 (ActiveSocket - HTTP)	
	
Error	Description
22401	Create WinHTTP session failed
22402	Open a session first
22403	Connection already established
22404	Unable to establish a connection to remote web server
22405	Unable to logon to proxy
22406	Unable to send the request to the remote web server
22407	No response received from remote server
22408	Unable to query headers
22409	Unable to query authentication schemes
22410	Unable to establish connection, operation timed out
22411	Establish a connection first
22412	Unable to logon to web site
22413	Unable to read data, query for data size failed
22414	Unable to read data, internal memory allocation failed
22415	Unable to read data, read operation failed
22416	Page not found (HTTP error 404)
22417	Page request refused (HTTP error 403)
22418	Bad Request (HTTP error 400)
22419	Internal Server Error (HTTP error 500)
22420	The requested page has been moved (HTTP error 410)
22421	Not implemented (HTTP error 501)
22422	Service unavailable (HTTP error 503)
22423	Failed to write data, write operation failed
22424	Error message received from proxy server
22425	Timed out waiting for proxy
22426	Failed to import one or more functions from the WINHTTP library
22427	Method not allowed (HTTP error 405)
22449	Generic HTTP failure
	
Errors 22450-22499 (ActiveSocket - RSH)	
	
Error	Description
22450	One or more errors were found in the Secure Sockets Layer (SSL) certificate sent by the server.
22451	Timeout occured while executing command
22452	Command execution failed, see stderr for details
22455	Local user name too long
22456	Remote user name too long
22457	Command too long
22460	Permission Denied
22480	Failed to create temporary file
22481	Cannot execute shell-script. Reason: invalid or incomplete parameters
22482	WinSock 2 or higher required
22483	Host or IP address is required
22484	Invalid TCP/IP port
22485	No Username specified
22486	Either a password or a private key file is required for authentication
22487	No Command specified
22490	Unable to establish a connection to remote computer
22491	Authentication Failed
22492	Unhandled exception occurred while executing SSH script
22493	Script timed out
22494	Failed to convert temporary file to string
22495	Unknown host key. Connect to the server with 'accept host key' enabled to accept and store the host key.
22496	Host key changed. Connect to the server with 'accept host key' enabled to accept and store the new host key.
22497	Connection closed unexpectedly
	
Errors 22480-22499 (ActiveSocket - SSH)	
	
Error	Description
22480	Failed to create temporary file
22481	Cannot execute shell-script. Reason: invalid or incomplete parameters
22482	WinSock 2 or higher required
22483	Host or IP address is required
22484	Invalid TCP/IP port
22485	No Username specified
22486	Either a password or a private key file is required for authentication
22487	No Command specified
22490	Unable to establish a connection to remote computer
22491	Authentication Failed
22492	Unhandled exception occurred while executing SSH script
22493	Script timed out
22494	Failed to convert temporary file to string
22495	Unknown host key. Connect to the server with 'accept host key' enabled to accept and store the host key.
22496	Host key changed. Connect to the server with 'accept host key' enabled to accept and store the new host key.
22497	Connection closed unexpectedly
	
Errors 22500-22549 (ActiveSocket - FTP)	
	
Error	Description
22500	WININET.DLL could not be loaded because DLL was not found
22502	Authentication Failed
22509	File not found
22510	No more files in directory
22511	File does not exist
22512	Directory does not exist
22513	Access denied when trying to access the file
22514	Failed to change directory
22515	Failed to create directory
22516	Failed to delete file
22517	Failed to create file
22518	Failed to rename file or directory
22519	Failed to delete directory, or directory is not empty
22520	The local file cannot be opened or does not exist
22521	Failed to write local file, cannot download file
22523	Failed to download file from FTP server
22525	Failed to upload file to FTP server
22530	Invalid FtpFile object passed to function
22540	Not connected, please connect first
	
Errors 22600-22699 (ActiveSocket - IPtoCountry)	
	
Error	Description
22601	Failed to load IP to Country database
22602	IP address not found in database
22603	Country not found in database
	
Errors 22700-22799 (ActiveSocket - MIB)	
	
Error	Description
22701	Default MIB tree not loaded
22702	Failed to load MIB file (specified mib file not found)
22703	Failed to load MIB file (one or more import modules not found)
22704	Invalid MIB filename specified
22730	Unable to resolve specified OID
22731	Unable to resolve specified MIB entry
22733	Invalid MIB file format
22750	Open UDP Socket failed
22751	Send Access Request failed
22752	No response received
22753	Access rejected
	
Errors 22750-22799 (ActiveSocket - RADIUS)	
	
Error	Description
22750	Open UDP Socket failed
22751	Send Access Request failed
22752	No response received
22753	Access rejected
	
Errors 22800-22849 (ActiveSocket - TFTP)	
	
Error	Description
22800	No session establised to remote host
22801	Invalid packet
22802	Failed to send packet, not all bytes of the package were sent
22803	Failed to receive data
22804	Unable to create local file
22805	Failed to write request
22806	TFTP not started
22807	File not opened
22808	File does not exist on remote TFTP host
22809	Failed to copy data
22810	Failed to write to file
22811	Wrong packet number detected during transfer
22812	Failed to open local file
22813	Failed to create read request
22814	Error in packet
22815	Failed to read from file
22816	No ACK received after a few attempts
22817	Error reported by remote host
22820	The remote host does not respond. Probably, there's no TFTP daemon listening on remote host
	
Errors 22850-22899 (ActiveSocket - MSN)	
	
Error	Description
22851	Failed to connect to MSN server, invalid hostname specified
22852	Failed to connect to MSN server, invalid portnumber specified
22853	Failed to connect to MSN server, the server did not respond
22854	Failed to handshake MSN protocol
22855	Failed to send MSN client version
22860	Invalid accountname specified
22861	Invalid password specified
22862	Authentication failure, please check account and password
22863	Failed to negotiate protocol version
22864	Failed to send client information
22865	Failed to request or send authentication
22866	Failed to load cryptographic shared libraries
22867	No recipients specified
22868	Failed to send contact list
22869	Failed to connect to switchboard
22871	Failed to add recipient, invalid format or unknown error
22872	Failed to set online status
22873	Failed to set display name
22874	Failed to send message to one of the recipients, either an unknown error occurred, user is not online, or you are not authorized by this recipient to send messages.
	
Errors 22900-22939 (ActiveSocket - SCP)	
	
Error	Description
22900	Unabled to load SCP provider DLL resource data
22901	Unabled to create SCP provider DLL
22902	Unabled to register SCP provider DLL
22903	Unabled to instantiate SCP provider DLL
22910	Timeout while copying file
22911	The hostname cannot contain special characters (e.g. ':', '@')
22912	Could not find local source file or directory
22913	Error during initialization of the SSH subsystem. The ProtocolError property may contain more information about the error.
22914	Unknown error during copy
22915	Protocol error during copy. The ProtocolError property contains more information about the error.
22916	Internal error: Unexpected newline during copy
22917	Lost connection
22918	Internal error: Unexpected time format
22919	Internal error: Expected control record
22920	Internal error: Unexpected file descriptor format
22921	Expected the target to be a directory
22922	Unexpected target file name received from host. Target file name looked like '.' or '..'
22923	Expected a target path to be a directory
22924	Could not create a directory
22925	Could not create a file
22926	Error writing to file
22927	A source file or directory could not be opened
22928	A source directory was ignored since it is not a file and the 'recursive' option was not set.
22929	Error reading file
22930	Empty source path
22931	Empty destination path
22932	Unable to establish a connection to remote computer

Errors 22940-22999 (ActiveSocket - SFTP)	
	
Error	Description
22940	Unabled to load SFTP provider DLL resource data
22941	Unabled to create SFTP provider DLL
22942	Unabled to register SFTP provider DLL
22943	Unabled to instantiate SFTP provider DLL
22950	No connection to an SFTP host or the connection was lost.
22951	Unable to initialize SFTP
22952	Unknown error while trying to connect to host
22953	Error during initialization of the SSH subsystem. The ProtocolError property may contain more information about the error.
22954	Could not canonify the path name
22955	Could not execute the remote command. The ProtocolError property may contain more information about the error
22956	Multiple or wildcard arguments require the destination to be a directory
22957	Unable to open directory
22958	Unable to open the file for writing. The ProtocolError property may contain more information about the error
22959	Unable to open the local file for reading
22960	Error while writing to file. The ProtocolError property may contain more information about the error
22961	No filename was specified
22962	Could not find the file specified
22963	Multiple-level wildcards are not supported
22964	No more files in directory
22965	Cannot create directory
22966	Cannot open directory
22967	Cannot open file for reading
22970	Invalid host
22972	Failed to connect
22980	Creation of socket failed
22981	Invalid host name
22982	Timeout
22983	Destination host unreachable
22984	Destination net unreachable
22985	Internal API error
22986	Bad option
22987	Hardware error
22988	Packet too big
22989	Bad request
22990	Bad route
22991	TTL expired
22992	Source Quench
22993	Bad Destination
22994	No more hops
22995	Max hops reached
22997	Call FindFirstHop before FindNextHop
22998	Failed to load ICMP API
22999	Generic ICMP error
	
Errors 22980-22999 (ActiveSocket - TraceRoute)	
	
Error	Description
22980	Creation of socket failed
22981	Invalid host name
22982	Timeout
22983	Destination host unreachable
22984	Destination net unreachable
22985	Internal API error
22986	Bad option
22987	Hardware error
22988	Packet too big
22989	Bad request
22990	Bad route
22991	TTL expired
22992	Source Quench
22993	Bad Destination
22994	No more hops
22995	Max hops reached
22997	Call FindFirstHop before FindNextHop
22998	Failed to load ICMP API
22999	Generic ICMP error
	
Errors 23700-23899 (MMS Toolkit)	
	
Error	Description
23710	GPRS modem initialization command(s) failed
23730	No recipient address(es) specified
23731	Invalid recipient address used in either To, CC or BCC field
23732	Invalid MMS message class specified
23733	Failed to load attachment file
23734	Invalid MMS priority value specified
23735	No MMSC Server address specified
23736	No WAP gateway address specified
23737	GPRS access point name not specified
23740	Failed to create Dial Up Networking entry
23741	Generic Dial-up Networking error
23742	GPRS connection lost, reconnect first
23745	No response from WAP gateway
23746	Unexpected response from WAP gateway
23748	Invalid MRU (Maximum Receive Unit) size
23749	MMS Message size too big
23750	You need to pass a MMSMessage object as parameter to the Send command
23752	Failed to initialize Windows Sockets
23753	Memory allocation failed
23755	MMS message is empy (no content specified)
23756	No more recipients
23757	No more message slides
23758	No more content in slide
23761	MMSC response: unspecified error
23762	MMSC response: service denied
23763	MMSC response: corrupt message format
23764	MMSC response: sending address unresolved
23765	MMSC response: message not found
23766	MMSC response: network problem
23767	MMSC response: content not accepted
23768	MMSC response: unsupported message
23770	Specified configuration file cannot be loaded
23771	File is not a MMS configuration file
23772	Failed to save configuration file, most probably, you specified an invalid filename, or you do not have access to this location
23773	Precompiled MMS file not found
23774	Precompiled MMS file corrupt or failed to load
23775	Failed to save precompiled MMS file
23780	Protocol error, illegal PDU received
23781	Session has been diconnected
23782	Session has been suspended
23783	Session has been resumed
23784	The peer is congested and cannot process the SDU
23785	The session connect failed
23786	The maximum receive unit size was exceeded
23787	The maximum outstanding requests was exceeded
23788	Peer request
23789	Network error occured
23790	User request
	
Errors 25000-25299 (Network Monitor - AxNmCfg API interface)	
	
Error	Description
25000	Creation of MDAC/ADO object failed
25001	Configuration Database error: invalid OLE object
25003	Configuration Database Location not set
25004	Unable to open Configuration Database
25005	Operation failed because Configuration Database is not opened
25006	Unable to open Configuration Database (database is already opened)
25010	Configuration Database error: field missing in [Rules] table
25016	There are no (more) users
25020	Configuration Database error: unable to query [Nodes] table
25021	Configuration Database error: record not found in [Nodes] table
25022	Configuration Database error: unable to update [Nodes] table
25023	Configuration Database error: unable to find last record in [Nodes] table
25024	Configuration Database error: unable to insert new record in [Nodes] table
25025	Configuration Database error: unable to delete record(s) from [Nodes] table
25026	Configuration Database error: not allowed to create duplicate values for the ID field
25035	Configuration Database error: unable to query [ServerCredentials] table
25036	Configuration Database error: record not found in [ServerCredentials] table
25037	Configuration Database error: unable to update [ServerCredentials] table
25050	Invalid Tree Item
25051	Tree configration not loaded
25052	Tree item not found
25060	Configuration Database error: unable to query [Tree] table
25061	Configuration Database error: no records in the [Tree] table
25062	Configuration Database error: unable to update [Tree] table
25100	Configuration Database error: unable to query [SmtpSettings] table
25101	Configuration Database error: no records in the [SmtpSettings] table
25102	Configuration Database error: unable to update [SmtpSettings] table
25105	Configuration Database error: unable to query [NetPopupSettings] table
25106	Configuration Database error: no records in the [NetPopupSettings] table
25107	Configuration Database error: unable to update [NetPopupSettings] table
25110	Configuration Database error: unable to query [MetaSettings] table
25111	Configuration Database error: no records in the [MetaSettings] table
25112	Configuration Database error: unable to update [MetaSettings] table
25120	Configuration Database error: unable to find last record in [DistributionGroups] table
25121	Configuration Database error: unable to insert new record in [DistributionGroups] table
25122	Configuration Database error: unable to query [DistributionGroups] table
25123	Configuration Database error: no records in the [DistributionGroups] table
25124	Configuration Database error: unable to update [DistributionGroups] table
25125	Configuration Database error: unable to delete record(s) from [DistributionGroups] table
25140	Configuration Database error: unable to query [MessageTemplates] table
25141	Configuration Database error: no records in the [MessageTemplates] table
25142	Configuration Database error: unable to update [MessageTemplates] table
25150	Configuration Database error: unable to query [LogSettings] table
25151	Configuration Database error: no records in the [LogSettings] table
25152	Configuration Database error: unable to update [LogSettings] table
25155	Configuration Database error: unable to query [ReportSettings] table
25156	Configuration Database error: no records in the [ReportSettings] table
25157	Configuration Database error: unable to update [ReportSettings] table
25160	Configuration Database error: unable to query [MaintenanceSettings] table
25161	Configuration Database error: no records in the [MaintenanceSettings] table
25162	Configuration Database error: unable to update [MaintenanceSettings] table
25165	Configuration Database error: unable to query [XmlViews] table
25166	Configuration Database error: no records in the [XmlViews] table
25167	Configuration Database error: unable to update [XmlViews] table
25170	Configuration Database error: unable to query [SnmpTrapsSettings] table
25171	Configuration Database error: no records in the [SnmpTrapsSettings] table
25172	Configuration Database error: unable to update [SnmpTrapsSettings] table
25173	Configuration Database error: unable to query [DateTimeSettings] table
25174	Configuration Database error: no records in the [DateTimeSettings] table
25175	Configuration Database error: unable to update [DateTimeSettings] table
25176	Configuration Database error: unable to query [AutoBackupSettings] table
25177	Configuration Database error: no records in the [AutoBackupSettings] table
25178	Configuration Database error: unable to update [AutoBackupSettings] table
25180	Configuration Database error: unable to query [PagerSettings] table
25181	Configuration Database error: no records in the [PagerSettings] table
25182	Configuration Database error: unable to update [PagerSettings] table
25185	Configuration Database error: unable to query [SmsSettings] table
25186	Configuration Database error: no records in the [SmsSettings] table
25187	Configuration Database error: unable to update [SmsSettings] table
25188	Configuration Database error: unable to query [MsnSettings] table
25189	Configuration Database error: no records in the [MsnSettings] table
25190	Configuration Database error: unable to update [MsnSettings] table
25194	Configuration Database error: unable to query [AdvancedSettings] table
25195	Configuration Database error: no records in the [AdvancedSettings] table
25196	Configuration Database error: unable to update [AdvancedSettings] table
25200	Registry entry 'Engine0Root' is empty. Please make sure this entry points to the Network Monitor root directory
25201	Unable to access the CONFIG.MDB configuration file. Please make sure this file exists in the 'Configuration' directory and make sure that you have read/write access to this file
25203	Failed to convert configuration file
25204	Memory allocation failed
25205	Configuration file is not compatible with ActiveXperts Network Monitor (version information cannot be retrieved from the configuration file)
25206	Configuration file is created by a newer version of ActiveXperts Network Monitor
25207	Configuration file is created by an older version of ActiveXperts Network Monitor
25208	Path to the configuration directory is empty; please check your registry, or re-install the product
25209	ActiveX component [AxsNmCfg.dll] not registered on this computer. Either manually register this DLL on this machine, or reinstall the product
25210	System Database File does not exist
25220	You cannot add more checks or folders to the configuration because the maximum number of nodes will be exceeded
	
Errors 25300-25399 (Network Monitor - RepCfg.dll)	
	
Error	Description
25300	No database connection string specified
25301	Unable to connect to the statistical database
25302	Failed to execute SQL query in the statistical database
25303	Empty record set (not enough data to process report)
25304	Failed to load 'Checks' table from statistical database
25305	Failed to load 'CheckTypes' table from statistical database
25306	Failed to load 'Results' table from statistical database
25310	Reporting module not initialized
25311	Reporting not enabled
25312	Check does not exist
25318	Memory allocation failed
25320	E-mail not enabled for this report definition
25321	E-mail notificatons are not enabled in Network Monitor. To enable notifications, enable e-mail notifications in the 'Network Monitor Manager' application
25322	Primary SMTP server is disabled
25323	Secundary SMTP server is disabled
25330	Reporttype is not specified, specify either 'detail' or 'availability'
25331	Reportformat is not specified, specify either 'XML', 'HTML' or 'CSV' format
25333	Invalid or no time period specified
25334	No XSL stylesheet specified
25335	Specified XSL stylesheet cannot be found or does not exist
25336	Failed to copy XSL stylesheet to the output directory
25337	Separator for CSV file missing
25338	Failed to create output file
25339	Insufficient data in database to create report
25350	Failed to load the report configuration (.rep) file
25370	Invalid parameters supplied
25371	Invalid date supplied
25372	Unsupported object type
25373	Invalid filter object supplied
25374	Invalid output object supplied
25375	ActiveX component [AxMmCtl.dll] not registered on this computer. Either manually reigster this DLL on this machine, or reinstall the product
25399	Unexpected error
	
Errors 25400-25699 (Network Monitor)	
	
Error	Description
25500	Unable to retrieve source database name out of the Database Connection String
25501	Unable to Export configuration. Configuration file does not exist
25502	Unable to Export configuration. Copy File failed
25503	Unable to delete previous Shadow Configuration File
25504	Unable to copy System Configuration File to the Shadow Configuration File
25505	Unable to copy Shadow Configuration File to the Current Configuration File
25506	Unable to locate current configuration file
25507	Current configuration file is in use. Please close all applications and retry
25508	Unable to load conversion module
25520	File is not a Network Monitor 5.x file
25521	File is not a Network Monitor 6.x file
25522	File is not a Network Monitor 7.x file
	
Errors 27000-27299 (SMS Messaging Server - CFG (Obsolete 5.0 codes))	
	
Error	Description
27000	Registry entry 'ServerRoot' missing
27001	Registry entry 'Configuration' missing
27002	ActiveX component [AxSmsCfg.dll] not registered on this computer. Either manually reigster this DLL on this machine, or reinstall the product
27003	ActiveX component [AEmail.dll] not registered on this computer. Either manually reigster this DLL on this machine, or reinstall the product
27004	ActiveX component [AComPort.dll] not registered on this computer. Either manually reigster this DLL on this machine, or reinstall the product
27005	ActiveX component [ASocket.dll] not registered on this computer. Either manually reigster this DLL on this machine, or reinstall the product
27006	ActiveX component [AxPop3.dll] not registered on this computer. Either manually reigster this DLL on this machine, or reinstall the product
27007	ActiveX component [ASmsCtrl.dll] not registered on this computer. Either manually reigster this DLL on this machine, or reinstall the product
27008	ActiveX component [AxEdit.dll] not registered on this computer. Either manually reigster this DLL on this machine, or reinstall the product
27009	Registry entry 'Server' missing
27010	Internal error: unable to create new ActiveX object
27011	Internal error: unable to create new ADODB object
27012	Registry entry 'ClientRoot' missing
27013	One or more HTML files needed by the program are missing
27020	Unable to open the Configuration Database
27021	Unable to query the Configuration Database
27022	No more records
27023	Unable to create new configuration record; maximum allowed number is reached
27024	Unable to create new configuration record
27025	Unable to modify configuration record
27026	Unable to delete configuration record
27050	Empty Message Database Connection String
27051	Unable to open Message Database
27052	Syntax error in database query
27053	Syntax error in querying all database fields
27054	No more messages in message Database
27060	Unable to create new message
27061	Unable to update message
27062	Unable to execute query in Message Database
27063	Syntax error in the WAP Message Body
27100	Empty Archive Database Connection String
27101	Unable to open Archive Database
27102	Unable to query Archive Database
27103	No more messages in Archive Database
27104	Unable to create new message in Archive Database
27105	Unable to update message in Archive Database
27106	Unable to execute query in Archive Database
27107	Unable to delete message from Archive Database
27200	Invalid schedule time
	
Errors 27300-27399 (SMS Messaging Server - Pop3 (Obsolete 5.0 codes))	
	
Error	Description
27300	No welcome message received from Pop3 Server (no response at all, expected response: '+OK POP3 Ready')
27301	No welcome message received from Pop3 Server (invalid response, '+OK POP3 Ready' was expected)
27302	No reponse received on '+USER' command
27303	Invalid reponse received on '+USER' command
27304	Login failed
27305	No reponse received on '+PASSWORD' command
27306	Invalid reponse received on '+PASSWORD' command
27307	No connection to POP3 server established
27308	Invalid message indicator; message should start with '+OK '.
27309	Invalid message (MIME header is empty)
27310	Cannlot load ACTIVEXPERTS:SOCKET module
27311	No more messages available
	
Errors 29000-29199 (Scripting Toolkit - Scripting Host)	
	
Error	Description
29101	Internal error
29102	Timeout while accessing scriptfile (file is in use)
29103	Failed to create mutex
29120	There is an error in the script code
29121	Script timed out
29122	Failed to wait for mutex
29123	Fatal Exception Error occurred while executing script
29124	The specified module could not be found.
29125	The specified module is not a valid component
29130	The specified script file could not be found
29131	The specified function could not be found in the script file specified
29132	Invalid parameter count or type mismatch
	
Errors 29200-29299 (Scripting Toolkit - RemoteCommand)	
	
Error	Description
29201	Internal error
29202	Failed to create mutex
29203	Timeout while accessing file (file is in use)
29210	Unable to install service on remote computer
29211	Unable to open service on remote computer
29212	Unable to start service on remote computer
29220	Failed to create UNC name
29221	Unable to bind to MPR.DLL library
29230	Invalid hostname or ip-address specified
29231	Username missing
29232	Password missing
29233	Command missing
29234	Failed to retrieve local machine name
29235	Authentication failed, please check username and password
29236	Failed to connect to host
29237	Failed to load service executable from resource
29238	Failed to copy agent to remote host
29239	Failed to start service on remote host
29240	Failed to connect to agent on remote host
29241	Failed to stop service
29242	Timeout occurred when executing command
29243	Failed to retrieve stderr or stdout from remote host
29244	Failed to execute local command: cannot create pipe
29245	Failed to execute local command: cannot create process
29246	Execution of PowerShell scripts is disabled on this system. Check the ActiveXperts FAQ
	
Errors 30000-30199 (Serial Communications - COM IO)	
	
Error	Description
30100	Generic port error
30101	Invalid port parameter
30102	Unable to perform operation because the device does not exist
30103	Unable to perform operation because the device is already opened
30104	Generic open port error
30105	Unable to initialize port
30106	Unable to write to device
30107	Unable to write directly to device
30108	Unable to read from device
30109	Unable to retrieve device settings
30110	Unable to change device settings
30111	Unable to purge the device
30112	Port timeout
30113	Invalid baudrate selected
30114	The device is not opened
30115	No more data available in receive buffer
30120	Professional license required to use this function
	
Errors 30200-30299 (Serial Communications - TAPI)	
	
Error	Description
30200	Unable to start telephony service
30201	Cannot find specified devicename
30202	Cannot open specified device
30203	TAPI Device Manager failed to respond
30204	Windows Telephony Device is not available
30205	Windows Telephony Device is not accessible
30206	Windows Telephony Device cannot be used, invalid bearer mode
30209	Windows Telephony Device is not accessible
30210	Windows Telephony Device status cannot be retrieved
	
Errors 30300-30399 (Serial Communications - Modem errors)	
	
Error	Description
30350	No response from modem
30351	Unexpected response from modem
30352	Error response from modem
30353	No dialtone detected upon attempt to connect to remote party
30354	No carrier reported while connecting to remote party
30355	No answer reported while connecting to remote party
30356	Busy tone reported while connecting to remote party
	
Errors 31600-31799 (Remote Access Service)	
	
Error	Description
31600	An operation is pending
31601	An invalid port handle was detected
31602	The specified port is already open
31603	The caller's buffer is too small
31604	Incorrect information was specified
31605	The port information cannot be set
31606	The specified port is not connected
31607	An invalid event was detected
31608	A device was specified that does not exist
31609	A device type was specified that does not exist
31610	An invalid buffer was specified
31611	A route was specified that is not available
31612	A route was specified that is not allocated
31613	An invalid compression was specified
31614	There were insufficient buffers available
31615	The specified port was not found
31616	An asynchronous request is pending
31617	The modem (or other connecting device) is already disconnecting
31618	The specified port is not open
31619	A connection to the remote computer could not be established
31620	No endpoints could be determined
31621	The system could not open the phone book file
31622	The system could not load the phone book file
31623	The system could not find the phonebook entry for this connection
31624	The system could not update the phone book file
31625	The system found invalid information in the phone book
31626	A string could not be loaded
31627	A key could not be found
31628	The connection was terminated by the remote computer
31629	The connection was closed by the remote computer
31630	The modem (or other connecting device) was disconnected due to hardware failure
31631	The user disconnected the modem (or other connecting device)
31632	An incorrect structure size was detected
31633	The modem (or other connecting device) is already in use
31634	Your computer could not be registered on the remote network
31635	There was an unknown RAS error
31636	The device attached to the port is not the one expected
31637	A string was detected that could not be converted
31638	The remote server is not responding in a timely fashion
31639	No asynchronous net is available
31640	An error has occurred involving NetBIOS
31641	The sever cannot allocate NetBIOS resourced needed to support the client
31642	One of your computer's NetBIOS names is already registered on the remote network
31643	A network adapter at the server failed
31644	You will not receive network message popups
31645	There was an internal authentication error
31646	The account is not permitted to log on at this time of day
31647	The account is disabled
31648	The password for this account has expired
31649	The account does not have permission to dial in
31650	The remote access server is not responding
31651	The modem (or other connecting device) has reported an error
31652	There was an unrecognized response from the modem (or other connecting device)
31653	A macro required by the modem (or other connecting device) was not found
31654	A command or response in the device.INF file section
31655	The macro was not found in the device.INF file section
31656	The macro in the device.INF file section contains an undefined macro
31657	The device.INF file could not be opened
31658	The device name in the device.INF or media.INI file is too long
31659	The media.INI file refers to an unknown device name
31660	The device.INF file contains no responses for the command
31661	The device.INF file is missing a command
31662	There was an attempt to set a macro not listed in device.INF file section
31663	The media.INI file refers to an unknown device type
31664	The system has run out of memory
31665	The modem (or other connecting device) is not properly configured
31666	The modem (or other connecting device) is not functioning
31667	The system was unable to read the media.INI file
31668	The connection was terminated
31669	The usage parameter in the media.INI file is invalid
31670	The system was unable to read the section name from the media.INI file
31671	The system was unable to read the device type from the media.INI file
31672	The system was unable to read the device name from the media.INI file
31673	The system was unable to read the usage from the media.INI file
31674	The system was unable to read the maximum connection BPS rate from the media.INI file
31675	The system was unable to read the maximum carrier connection speed from the media.INI file
31676	The phone line is busy
31677	A person answered instead of a modem (or other connecting device)
31678	The remote computer did not respond. For further assistance, click More Info or search Help and Support Center for this error number
31679	The system could not detect the carrier
31680	There was no dial tone
31681	The modem (or other connecting device) reported a general error
31682	There was an error in writing the section name
31683	There was an error in writing the device type
31684	There was an error in writing the device nam
31685	There was an error in writing the maximum connection speed
31686	There was an error in writing the maximum carrier speed
31687	There was an error in writing the usage
31688	There was an error in writing the default-off
31689	There was an error in reading the default-off
31690	The ini file is empty
31691	Access was denied because the username and/or password was invalid on the domain
31692	There was a hardware failure in the modem (or other connecting device)
31693	NOT_BINARY_MACRO
31694	DCB_NOT_FOUND
31695	The state machines are not started
31696	The state machines are already started
31697	The response looping did not complete
31698	A response keyname in the device.INF file is not in the expected format
31699	The modem (or other connecting device) response caused a buffer overflow
31700	The expanded command in the device.INF file is too long
31701	The modem moved to a connection speed not supported by the COM driver
31702	Device response received when none expected
31703	The connection needs information from you, but the application does not allow user interaction
31704	The callback number is invalid
31705	The authorization state is invalid
31706	ERROR_WRITING_INITBPS
31707	There was an error related to the X.25 protocol
31708	The account has expired
31709	There was an error changing the password on the domain. The password might have been too short or might have matched a previously used password
31710	Serial overrun errors were detected while communicating with the modem
31711	A configuration error on this computer is preventing this connection. For further assistance, click More Info or search Help and Support Center for this error number
31712	The two-way port is initializing. Wait a few seconds and redial
31713	No active ISDN lines are available
31714	No ISDN channels are available to make the call
31715	Too many errors occurred because of poor phone line quality
31716	The Remote Access Service IP configuration is unusable
31717	No IP addresses are available in the static pool of Remote Access Service IP addresses
31718	The connection was terminated because the remote computer did not respond in a timely manner.
31719	The PPP connection was terminated by the remote computer
31720	No PPP protocols configured for this connection
31721	The remote computer did not respond
31722	Invalid data was received from the remote computer. This data was ignored
31723	The phone number, including prefix and suffix, is too long
31724	The IPX protocol cannot dial out on the modem (or other connecting device) because this computer is not configured for dialing out (it is an IPX router)
31725	The IPX protocol cannot dial in on the modem (or other connecting device) because this computer is not configured for dialing in (the IPX router is not installed)
31726	The IPX protocol cannot be used for dialing out on more than one modem (or other connecting device) at a time
31727	Cannot access TCPCFG.DLL
31728	The system cannot find an IP adapte
31729	SLIP cannot be used unless the IP protocol is installed
31730	Computer registration is not complete
31731	The protocol is not configured
31732	Your computer and the remote computer could not agree on PPP control protocols
31733	A RAS connection to the remote computer could not be completed. You might need to adjust the protocols on this computer
31734	The PPP link control protocol was terminated
31735	The requested address was rejected by the server
31736	The remote computer terminated the control protocol
31737	PPP Loopback was detected
31738	The server did not assign an address
31739	The authentication protocol required by the remote server cannot use the stored password. Redial, entering the password explicitly
31740	An invalid dialing rule was detected
31741	The local computer does not support the required data encryption type
31742	The remote computer does not support the required data encryption type
31743	The remote computer requires data encryption
31744	The system cannot use the IPX network number assigned by the remote computer
31745	ERROR_INVALID_SMM
31746	SMM_UNINITIALIZED
31747	ERROR_NO_MAC_FOR_PORT
31748	SMM_TIMEOUT
31749	BAD_PHONE_NUMBER
31750	WRONG_MODULE
31751	The callback number contains an invalid character. Only the following 18 characters are allowed: 0 to 9, T, P, W, (, ), -, @, and space
31752	A syntax error was encountered while processing a script
31753	The connection could not be disconnected because it was created by the multi-protocol router
31754	The system could not find the multi-link bundle
31755	The system cannot perform automated dial because this connection has a custom dialer specified
31756	This connection is already being dialed
31757	Remote Access Services could not be started automatically. Additional information is provided in the event log
31758	Internet Connection Sharing is already enabled on the connection
31759	An error occurred while the existing Internet Connection Sharing settings were being changed
31760	An error occurred while routing capabilities were being enabled
31761	An error occurred while Internet Connection Sharing was being enabled for the connection
31762	An error occurred while the local network was being configured for sharing
31763	Internet Connection Sharing cannot be enabled
31764	No smart card reader is installed
31765	Internet Connection Sharing cannot be enabled
31766	A certificate could not be found. Connections that use the L2TP protocol over IPSec require the installation of a machine certificate, also known as a computer certificate
31767	Internet Connection Sharing cannot be enabled. The LAN connection selected as the private network has more than one IP address configured. Please reconfigure the LAN connection with a single IP address before enabling Internet Connection Sharing
31768	The connection attempt failed because of failure to encrypt data
31769	The specified destination is not reachable
31770	The remote computer rejected the connection attempt
31771	The connection attempt failed because the network is busy
31772	The remote computer's network hardware is incompatible with the type of call requested
31773	The connection attempt failed because the destination number has changed
31774	The connection attempt failed because of a temporary failure. Try connecting again
31775	The call was blocked by the remote computer
31776	The call could not be connected because the remote computer has invoked the Do Not Disturb feature
31777	The connection attempt failed because the modem (or other connecting device) on the remote computer is out of order
31778	It was not possible to verify the identity of the serve
31779	To dial out using this connection you must use a smart card
31780	An attempted function is not valid for this connectio
31781	The connection requires a certificate, and no valid certificate was found
31782	Internet Connection Sharing (ICS) and Internet Connection Firewall (ICF) cannot be enabled because Routing and Remote Access has been enabled on this computer
31783	Internet Connection Sharing cannot be enabled. The LAN connection selected as the private network is either not present, or is disconnected from the network. Please ensure that the LAN adapter is connected before enabling Internet Connection Sharing
31784	You cannot dial using this connection at logon time, because it is configured to use a user name different than the one on the smart card. If you want to use it at logon time, you must configure it to use the user name on the smart card
31785	You cannot dial using this connection at logon time, because it is not configured to use a smart card. If you want to use it at logon time, you must edit the properties of this connection so that it uses a smart card
31786	The L2TP connection attempt failed because there is no valid machine certificate on your computer for security authentication
31787	The L2TP connection attempt failed because the security layer could not authenticate the remote computer
31788	The L2TP connection attempt failed because the security layer could not negotiate compatible parameters with the remote computer
31789	The L2TP connection attempt failed because the security layer encountered a processing error during initial negotiations with the remote computer
31790	The L2TP connection attempt failed because certificate validation on the remote computer failed
31791	The L2TP connection attempt failed because security policy for the connection was not foun
31792	The L2TP connection attempt failed because security negotiation timed out
31793	The L2TP connection attempt failed because an error occurred while negotiating security
31794	The Framed Protocol RADIUS attribute for this user is not PPP
31795	The Tunnel Type RADIUS attribute for this user is not correct
31796	The Service Type RADIUS attribute for this user is neither Framed nor Callback Framed
31797	A connection to the remote computer could not be established because the modem was not found or was busy
31798	A certificate could not be found that can be used with this Extensible Authentication Protocol
31799	Internet Connection Sharing (ICS) cannot be enabled due to an IP address conflict on the network. ICS requires the host be configured to use 192.168.0.1. Please ensure that no other client on the network is configured to use 192.168.0.1
	
Errors 32000-32200 (LDAP)	
	
Error	Description
32001	LDAP: Operations error
32002	LDAP: Protocol error
32003	LDAP: Time limit exceeded
32004	LDAP: Size limit exceeded
32005	LDAP: Compare false
32006	LDAP: Compare true
32007	LDAP: Authentication method not supported
32008	LDAP: Strong authentication method required
32009	LDAP: Referral V2
32010	LDAP: Partial results
32011	LDAP: Referral
32012	LDAP: Admin limit exceeded
32013	LDAP: Critical extension unavailable
32014	LDAP: Confidentiality required
32016	LDAP: No such attribute
32017	LDAP: Undefined type
32018	LDAP: Inappropriate matching
32019	LDAP: Constraint violation
32020	LDAP: Attribute or value exists
32021	LDAP: Invalid Syntax
32032	LDAP: Object not found
32033	LDAP: Alias problem
32034	LDAP: Invalid distinguished name syntax
32035	LDAP: Is leaf
32036	LDAP: Alias dereference problem
32048	LDAP: Inappropriate authentication
32049	LDAP: Invalid Credentials
32050	LDAP: Insufficint rights
32051	LDAP: Server busy
32052	LDAP: Server unavailable
32053	LDAP: Unwilling to perform
32054	LDAP: Loop detected
32064	LDAP: Naming violation
32065	LDAP: Object class violation
32066	LDAP: Not allowed on non-leaf entry
32067	LDAP: Not allowed on RDN
32068	LDAP: Already Exists
32069	LDAP: No object class modifications allowed
32070	LDAP: Results to large
32071	LDAP: Affects multiple DSAS
32080	LDAP: Generic Error
32081	LDAP: Server down
32082	LDAP: Local error
32083	LDAP: Encoding error
32084	LDAP: Decoding error
32085	LDAP: Operation timed out
32086	LDAP: Authentication unknown
32087	LDAP: Error in filter
32088	LDAP: User cancelled
32089	LDAP: Invalid parameter
32090	LDAP: Out of memory
32091	LDAP: Connection error
32092	LDAP: Operation not supported
32093	LDAP: No results returned
32094	LDAP: Control not found
32095	LDAP: More results to return
32096	LDAP: Client loop
32097	LDAP: Referral limit exceeded
32101	LDAP: Not connected to server
32102	LDAP: Failed to load WinLDAP library
	
Errors 33000-34399 (SMS and MMS Toolkit - Misc Errors)	
	
Error	Description
33003	Professional license required to use this function
33005	Invalid parameter passed to function
33035	Invalid message class
33036	Invalid message priority
33050	Failed to load provider config file, file not found
33051	Failed to load provider config file, file corrupt
33052	Failed to save provider config file, failed to write file
33060	No more messages in receive buffer
33061	Failed to query message, or invalid messagereference specified
33070	Failed to save deliveryreports. Unable to open file for writing.
33071	Failed to load deliveryreports. Unable to open file for reading.
33130	Device used is not a GSM device
33131	Device used does not support GPRS/UMTS
33140	There is no SIM card in the GSM device
33141	SIM card requires PIN code
33142	PIN code entered is invalid
33150	Selected message storage is not available on this GSM device
33151	Device used does not support the storage of SMS delivery report messages
33160	No network coverage, or network busy
33170	SMS service centre (SMSC) address not set
33171	Failed to set SMS service centre (SMSC) address
33201	Failed to connect to SMPP server, please check hostname or IP address
33202	Failed to bind to SMPP server, please check SystemID and Password
33203	Invalid SystemID specified
33204	Invalid Password specified
33205	Not bound to SMPP server, please connect first
33206	SMPP operation timed out
33207	Invalid System Type specified
33208	Failed to send message, currently bound as a receiver
33209	Invalid SMPP version
33210	Failed to cancel SMS message
33211	To receive a deliveryreport, you have to connect as transceiver, not as a transmitter
33215	Failed to submit the SMS message, SMPP provider rejected the message
33216	Recipient address blocked or nut supported by SMPP provider
33217	SMPP Throttling error, please reduce message throughput
33225	Insufficient message credits to submit SMS message
33230	Invalid TLV type specified
33231	Invalid TLV tag specified
33232	TLV parameter not found
33301	Error response received from provider
33302	Unknown response received from provider
33303	URL template for sending text messages not set
33304	URL template for sending data messages not set
33305	URL template for sending unicode messages not set
33306	The evaluation period has expired
33307	The daily limit has been reached
33308	No 'To' adress has been specified
33309	The HTTP Post operation failed
33310	The HTTP Get operation failed
33460	Invalid UCP provider
33461	No response from UCP provider
33462	Provider reported a syntax error
33463	Provider rejected the message (NACK)
33501	Error during initial negotiation phase
33503	Pager number not allowed or blocked by provider
33504	Invalid message
33505	Message was sent, but no (N)ACK received
33600	No SNPP provider specified, please check hostname or IP address
33601	Failed to connect to SNPP provider, please check hostname or IP address
33602	Invalid SNPP password
33603	Invalid Pager ID or pincode
33610	SNPP command not supported
33611	No response from SNPP provider, operation timed out
33710	No connection to GPRS/UMTS network, please connect first
33711	Failed to create Dial Up Networking entry for the GPRS/UMTS connection
33712	Generic Dial-up networking error
33720	Missing MMSC server address
33721	Missing GPRS access point name (APN)
33722	Missing WAP gateway address
33723	Invalid WAP gateway address. You should use the following format: http://10.10.10.10:8080.
33730	No response received from WAP gateway
33731	Error response received from WAP gateway
33761	MMSC response: unspecified error
33762	MMSC response: service denied
33763	MMSC response: corrupt message format
33764	MMSC response: sending address unresolved
33765	MMSC response: message not found
33766	MMSC response: network problem
33767	MMSC response: content not accepted
33768	MMSC response: unsupported message
33780	Abort: Protocol error, illegal PDU received
33781	Abort: Session has been disconnected
33782	Abort: Session has been suspended
33783	Abort: Session has been resumed
33784	Abort: The peer is congested and cannot process the SDU
33785	Abort: The session connect failed
33786	Abort: The maximum receive unit size was exceeded
33787	Abort: The maximum number of outstanding requests was exceeded
33788	Abort: Peer request
33789	Abort: Network error occurred
33790	Abort: User request
33800	MM7 Error: Client error
33801	MM7 Error: Operation restricted
33802	MM7 Error: Address error
33803	MM7 Error: Address not found
33804	MM7 Error: Multimedia content refused
33805	MM7 Error: Message ID not found
33806	MM7 Error: Linked ID not found
33807	MM7 Error: Message forumat corrupt
33808	MM7 Error: Application ID not found
33809	MM7 Error: Reply application ID not found
33820	MM7 Error: Server error
33821	MM7 Error: Not possible
33822	MM7 Error: Message Rejected
33823	MM7 Error: Multiple addresses not supported
33824	MM7 Error: Application addressing not supported
33830	MM7 Error: General service error
33831	MM7 Error: Improper identification
33832	MM7 Error: Unsupported version
33833	MM7 Error: Unsupported operation
33834	MM7 Error: Validation error
33835	MM7 Error: Service error
33836	MM7 Error: Service unavailable
33837	MM7 Error: Service denied
33838	MM7 Error: Application Denied
34010	Invalid providerdialstring
34030	Invalid sender address
34031	Invalid recipient address
34032	Invalid number format; missing prefix: '+' (for international) or 'S' (for shortcode).
34033	SMS message is too long. To send multipart SMS messages, set the multipart option
34034	SMS format is not valid, or not supported using this protocol
34040	Invalid SmsMessage object passed to the Send function
34050	Invalid message reference
34130	No recipient address(es) specified
34131	Invalid recipient address used in either To, CC or BCC field
34133	Size of MMS message exceeds the maximum message size
34134	MMS Subject is too long
34135	Invalid MMS message class
34136	Invalid MMS message priority
34137	Invalid attachment or attachment not found
34138	Invalid SMIL file or file not found
34140	No slides defined for this MMS message, message is empty
34141	Invalid MmsMessage object passed to send function
34151	Failed to save MMS file, failed to open file
34152	Failed to save MMS file, invalid or no file name specified
34153	Failed to load MMS file, failed to open file
34154	Failed to load MMS file, file is corrupt or not a MMS file
34160	No more recipients
34161	No more slides
34162	No more content found
34200	Invalid PagerMessage object passed to the Send function
34221	Invalid networkcode specified
34225	SMPP server disconnected unexpectedly
34226	Failed to send test message
34227	Timeout while trying to send test message
34301	Failed to load picturefile, failed to open file
34302	Failed to load picturefile, invalid format
34303	No picture file loaded or selected
34304	Invalid countrycode specified
	
Errors 35000-35100 (Twitter Toolkit)	
	
Error	Description
35000	An access token is required for this operation.
35001	An access token secret is required for this operation.
35002	Authentication failed. You need to have a valid access and consumer key.
35003	A valid consumer key is required for this operation.
35004	A valid consumer secret is required for this operation.
35005	An unexpected HTTP response code was returned
35006	An unexpected error occurred while requesting authentication keys
35007	Could not add this person to your friends list since you are already friends
35008	Twitter API returned an error message. Make sure to create a logfile and check this file to see the error message
35009	Your Tweet is too long. A Tweet can be 140 characters long
35010	This method requires either a valid name or a valid user id
35011	This method requires a valid query string
35012	Error while parsing the Twitter response body
35013	Error while initiazing the MSXML component and parsing the twitter response body
35014	Unexpected error
35015	There are no (more) status messages
35016	No message to Tweet
35017	There was an error while opening the key file
35018	There was an error while saving the key file
35019	Could not find the specified key file
35020	The specified key file is empty
	
Errors 36000-36999 (Mobile Messaging Toolkit)	
	
Error	Description
36000	Invalid bind type specified.
36001	Invalid protocol version specified
36002	Invalid argument
36003	The SMS message body is too long
36004	The message was not correctly formatted. A data message should be formatted in hex e.g. '05 04 0A'.
36005	Invalid bodyformat specified
36006	No more updated messages pending
36007	No more incoming messages pending
36008	No more TLV's
36009	Failed to start SMPP client connection thread
36010	Failed to connect to server; Connection timed out
36011	Failed to connect to server
36012	Operation timed out
36013	Failed to bind to server; Bind timed out
36014	Invalid connection state. Should be connected or bound for this operation
36015	Out of memory
36017	Timeout while receiving data
36018	Unexpected PDU length; Closed connection
36019	Invalid PDU format; Closed connection
36020	Invalid parameter
36021	Command PDU timed out; Closed connection
36022	Connection was closed by host.
36023	Timeout while un-binding
36024	SMS Query queue full, this message was not submitted. Wait a bit before querying other SMS messages.
36025	Failed to bind to this server
36027	Invalid password
36028	Invalid system id
36029	Invalid service type
36030	Invalid UDH. Either multipart mode is set to SARTLV or the message UDH contains conflicting elements.
36031	No TLV based multipart modes can be used when connecting as version 3.3
36032	Invalid number format. The phone number should be numeric and less than 15 characters long.
36033	SMS Submit queue full, this message was not submitted. Wait a bit before adding new SMS messages.
36092	No more ports
36093	Invalid UDH. Either the UDH length is unusually large or the message UDH contains conflicting elements.
36094	No more status reports pending
36095	No more SMS messages pending
36096	Timeout while waiting for the device to register on a GSM network
36097	Failed to query device for preferred storage options
36098	Storage type not supported on this device
36099	No more devices
36100	Could not find the number of attached telephony devices
36101	PIN required
36102	PUK required
36103	Device does not support preferred message format
36104	Out of memory
36105	No device opened.
36106	Device does not support sending SMS messages
36107	Invalid response from device
36108	Reponse timed out
36109	Data coding in SMS message should be 'Default (0)' when sending in Text mode
36110	Device does not support receiving SMS messages
36111	Invalid parameter
36112	Syntax error in command.
36113	Unknown error
36114	Error while waiting for the prompt to send SMS data
36115	Error while receiving SMS messages
36116	Timeout while receiving SMS messages
36117	Multipart messages are not supported in text mode
36118	Status reports are not supported ini text mode
36119	UDHI can't be set in text mode
36120	No SIM card inserted or the GSM command set is not supported
36121	Unknown equipment error. Check the log file.
36122	Unknown network error. Check the log file.
36123	Phone failure
36124	No connection to phone
36125	Phone adapter link reserved
36126	Operation not allowed
36127	Operation not supported
36128	PH_SIM PIN required
36129	PH_FSIM PIN required
36130	PH_FSIM PUK required
36131	SIM not inserted
36132	SIM PIN required
36133	SIM PUK required
36134	SIM failure
36135	SIM busy
36136	SIM wrong
36137	Incorrect password
36138	SIM PIN2 required
36139	SIM PUK2 required
36140	Memory full
36141	Invalid index
36142	Not found
36143	Memory failure
36144	Text string too long
36145	Invalid characters in text string
36146	Dial string too long
36147	Invalid characters in dial string
36148	No network service
36149	Network timeout
36150	Network not allowed, emergency calls only
36151	Network personalization PIN required
36152	Network personalization PUK required
36153	Network subset personalization PIN required
36154	Network subset personalization PUK required
36155	Service provider personalization PIN required
36156	Service provider personalization PUK required
36157	Corporate personalization PIN required
36158	Corporate personalization PUK required
36159	PH-SIM PUK required
36160	Unknown error
36161	Illegal MS
36162	Illegal ME
36163	GPRS services not allowed
36164	PLMN not allowed
36165	Location area not allowed
36166	Roaming not allowed in this location area
36167	Operation temporary not allowed
36168	Service operation not supported
36169	Requested service option not subscribed
36170	Service option temporary out of order
36171	Unspecified GPRS error
36172	PDP authentication failure
36173	Invalid mobile class
36174	Operation temporarily not allowed
36175	Call barred
36176	Phone is busy
36177	User abort
36178	Invalid dial string
36179	SS not executed
36180	SIM Blocked
36181	Invalid block
36182	SIM powered down
36183	Unassigned number
36184	Operator determined barring
36185	Call barred
36186	Short message transfer rejected
36187	Destination out of service
36188	Unindentified subscriber
36189	Facility rejected
36190	Unknown subscriber
36191	Network out of order
36192	Temporary failure
36193	Congestion
36194	Recources unavailable
36195	Requested facility not subscribed
36196	Requested facility not implemented
36197	Invalid short message transfer reference value
36198	Invalid message unspecified
36199	Invalid mandatory information
36200	Message type non existent or not implemented
36201	Message not compatible with short message protocol
36202	Information element non-existent or not implemente
36203	Protocol error, unspecified
36204	Internetworking , unspecified
36205	Telematic internetworking not supported
36206	Short message type 0 not supported
36207	Cannot replace short message
36208	Unspecified TP-PID error
36209	Data code scheme not supported
36210	Message class not supported
36211	Unspecified TP-DCS error
36212	Command cannot be actioned
36213	Command unsupported
36214	Unspecified TP-Command error
36215	TPDU not supported
36216	SC busy
36217	No SC subscription
36218	SC System failure
36219	Invalid SME address
36220	Destination SME barred
36221	SM Rejected-Duplicate SM
36222	TP-VPF not supported
36223	TP-VP not supported
36224	D0 SIM SMS Storage full
36225	No SMS Storage capability in SIM
36226	Error in MS
36227	Memory capacity exceeded
36228	Sim application toolkit busy
36229	SIM data download error
36230	Unspecified error cause
36231	ME Failure
36232	SMS service of ME reserved
36233	Operation not allowed
36234	Operation not supported
36235	Invalid PDU mode parameter
36236	Invalid Text mode parameter
36237	SIM not inserted
36238	SIM PIN required
36239	PH-SIM PIN required
36240	SIM failure
36241	SIM busy
36242	SIM wrong
36243	SIM PUK required
36244	SIM PIN2 required
36245	SIM PUK2 required
36246	Memory failure
36247	Invalid memory index
36248	Memory full
36249	SMSC address unknown
36250	No network service
36251	Network timeout
36252	No +CNMA expected
36253	Unknown error
36254	User abort
36255	Unable to store
36256	Invalid Status
36257	Device busy or Invalid Character in string
36258	Invalid length
36259	Invalid character in PDU
36260	Invalid parameter
36261	Invalid length or character
36262	Invalid character in text
36263	Timer expired
36264	Operation temporary not allowed
36265	SIM not ready
36266	Cell Broadcast error unknown
36267	Protocol stack busy
36268	Invalid parameter
36269	Invalid number format. The phone number should be numeric and less than 15 characters long.
36270	Application ports cannot be used when 'MessageMode' is set to 'Text'
36300	Failed to load 'WinHTTP' library
36301	WinHTTP library not loaded
36302	Create session failed
36303	Connection timed out
36304	Maximum number of redirections reached; Aborted
36305	Malformed URL
36306	Query headers failed
36307	Out of memory
36308	Failed to open file
36309	Query data size failed
36310	Memory allocation failed
36311	Read data failed
36312	Open connection failed
36313	Set credentials failed
36314	Send request failed
36315	No response received
36316	Set proxy credentials failed
36317	Proxy credentials required
36318	Could not find an authorization scheme
36319	Certificate error
36320	Invalid hostname
36321	Connect to host failed
36322	WSA Timed out
36323	Failed to bind to 'WinHTTP' library
36324	Invalid number format. The phone number should be numeric and less than 15 characters long.
36400	Invalid hostname
36401	Invalid port number
36402	Connection failed
36403	No response from server
36404	Unexpected response from server
36405	Unexpected response from server
36406	Failed to introduce on the server
36407	Not connected. An active connection is needed for this operation
36408	Failed to encrypt communication message
36409	Send command failed
36410	The selected mode of authorization is not supported
36411	Authorization failed
36414	Secure handshake failed
36416	Service not available
36417	Mailbox not available
36418	Local error
36419	Insufficient memory on the server
36420	Client not authenticated
36421	Unknown command
36422	Syntax error
36423	Command not implemented
36424	Bad command sequence
36425	Parameter not implemented
36426	Invalid sender address
36427	Mail not accepted
36428	Authentication required
36429	Authentication failed
36430	Mailbox not found
36431	User not local, please try another forward path
36432	Storage exceeded
36433	Mailbox name not allowed
36434	Transaction failed
36435	The current license does not allow sending an HTML body. Please purchase a license or use the trial version to test sending an HTML body.
36500	Already connected
36501	Invalid hostname
36502	Invalid port number
36503	Connect failed
36504	Secure handshake failed
36505	No response from server
36506	No welcome message received from server
36507	Maildrop locked
36508	Login failed
36509	No connection
36510	Unexpected response from server
36511	Invalid password
36512	Failed to get the number of mails in the maildrop
36513	Mailbox empty
36514	Delete message failed
36515	No more messages
36516	Invalid message ID
36517	Retrieve failed
36518	Unexpected socket error
36519	Idle timeout while receiving data
36520	Timeout while receiving data
36521	Error while receiving data from server
36601	Error while decoding MIME
36602	Error while opening file
36603	Error while saving file
36604	Invalid attachment ID
36605	Decode attachment failed
36705	Session object not instantiated. A usable session object can only be retrieved an instance of SmppServer.
36706	No more SMS messages submitted.
36707	No more SMS message queries.
36708	No more SMS delivery responses.
36709	Invalid port number. The port number should be between 1 and 65535. The default port number for SMPP is 2775.
36710	No more sessions connected.
36711	Could not start SMPP service. Error binding to TCP port
36712	The SMPP service is already started
36713	Timeout while trying to start the SMPP service
36714	Error while starting server thread
36715	Error while accepting incoming connection
36716	Invalid parameter
36717	The client has not requested a bind
36718	The session must be bound before completing this action.
36719	Invalid multipart mode. The interface version is set to 3.3 and the Multipart mode is set to SARTLV or PAYLOADTLV
36720	Invalid UDH. The UDH length may be too large or the may not be an UDH present
36721	There are too many pending SMS deliveries.
	
Errors 37000-37999 (ActiveEmail)	
	
Error	Description
37400	Invalid hostname
37401	Invalid port number
37402	Connection failed
37403	No response from server
37404	Unexpected response from server
37405	Unexpected response from server
37406	Failed to introduce on the server
37408	Failed to encrypt communication message
37410	The selected mode of authorization is not supported
37411	Authorization failed
37414	Secure handshake failed
37435	Failed to save the queue file
37436	Failed to rename the queue file
37500	Already connected
37501	Invalid hostname
37502	Invalid port number
37503	Connect failed
37504	Secure handshake failed
37506	No welcome message received from server
37508	Login failed
37510	Unexpected response from server
37511	Invalid password
37512	Failed to get the number of mails in the maildrop
37516	Invalid message ID
37517	Retrieve failed
37518	Unexpected socket error
37521	Error while receiving data from server
37601	Loading MIME data failed
37602	Load MIME file failed
37603	Save MIME file failed
37604	Invalid attachment
37605	Open attachment failed
37606	Load attachment failed
37607	Save attachment failed
37608	Invalid FROM e-mail address
37609	No TO recipients specified
37610	Invalid TO e-mail address
37611	Invalid CC e-mail address
37612	Invalid BCC e-mail address
37613	Invalid attachment ID
37614	Decode attachment failed
37700	Querying SChannel security context failed
37701	Could not find the certificate chain
37702	Failed to create new credentials
37703	Server disconnected unexpectedly
37704	Could not read from server
37705	Could not open 'My' certificate store
37706	Failed to initialize SChannel security context
37707	Failed to allocate send/receive buffers
37708	The secure connection was not initialized properly.
37709	EncryptMessage failure in SSPI module of the Operating System. See www.activexperts.com/support/activemail?kb=Q2750060
37710	DecryptMessage failure in SSPI module of the Operating System. See www.activexperts.com/support/activemail?kb=Q2750060
37711	Can not finish receiving and decrypting an incomplete message, there is not enough buffer space to hold the rest of the message.
37712	Cannot set secure when already connected
37713	Timeout on receive data, reply from server expected
37800	Failed to initialize Mobile Messaging toolkit
37801	Failed to deploy Mobile Messaging toolkit
37802	Unknown encoding specified
37803	No to, cc or bcc recipients specified
37804	Simultaneous use of /bt and /btf is not allowed
37805	Simultaneous use of /bh and /bhf is not allowed
37806	No body specified.
37807	Cannot set both html file and text file to read from STDIN
37808	Specified plain text file could not be opened or read
37809	Specified html file could not be opened or read
37810	Unknown priority specified
37811	The /mail-header parameter expects an even amount of parameters, i.e. key/value pairs
37812	No default device found, specify a device or port explicitly

}
  ///  FIM - ERROS DE N�O ENVIO DE COBRAN�A
  //////////////////////////////////////////////////////////////////////////////
implementation

//uses UMySQLModule;

function TUnDiario_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, Lowercase('DiarioAdd'),  '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('DiarioAss'),  '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('DiarCEMCad'), ''); // CEM = Configura��o de Envio de Mensagens
    MyLinguas.AdTbLst(Lista, False, Lowercase('DiarCEMIts'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('DiarCEMBol'), ''); // Boletos que ser�o monitorados at� o pagamento ou cobran�a judicial
    MyLinguas.AdTbLst(Lista, False, Lowercase('DiarCEMEnv'), ''); // Mensagens a serem enviadas (cruzamento de 'DiarCEMEnv' com boletos vencidos
    MyLinguas.AdTbLst(Lista, False, Lowercase('DiarCEMRet'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('DiarioOpc'),  '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('FormContat'), '_lst_sample');
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnDiario_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
{
separador = "|"
}
  Result := True;
  if Uppercase(Tabela) = Uppercase('FormContat') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"ND"');
    FListaSQL.Add('1|"Telefone"');
  end else
  if UpperCase(Tabela) = UpperCase('DiarioOpc') then
  begin
    FListaSQL.Add('Codigo');
    FListaSQL.Add('1');
  end;
end;

function TUnDiario_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  (*
  if Uppercase(Tabela) = Uppercase('FormContat') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"ND"');
    FListaSQL.Add('1|"Telefone"');
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades"|"Cadastro de pessoas f�sicas e jur�dicas (clientes| fornecedores| etc.)"');
    //FListaSQL.Add('""|""');
  end;
  *)
end;


function TUnDiario_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('DiarioAdd') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('DiarioAss') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'CodUsu';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('DiarCEMCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('DiarCEMIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('DiarCEMBol') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Vencimento';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Data';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Mez';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'Depto';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'FatNum';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('DiarCEMEnv') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'DiarCEMIts';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Metodo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('DiarCEMRet') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'DiarCEMIts';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Metodo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'Execucao';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('DiarioOpc') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
end;

function TUnDiario_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    //end else
    if Uppercase(Tabela) = Uppercase('DiarioAdd') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DiarioAss';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Depto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Data';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Hora';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'tinyint(1)';  
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Genero';  // > Link com outra tabela
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // > CO_DIARIO_ADD_GENERO_LCTDEPTO
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BinaFone';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BinaDtHr';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Interloctr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro01';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PreAtend';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FormContat';  // Fone, mail, sms, midia social
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrintOS';  // Imprime na OS? -1=Indefinido, 0=N�o e 1=Sim
      FRCampos.Tipo       := 'int(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Link2'; // segundo link - al�m do Depto
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('DiarioAss') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aplicacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('DiarCEMCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('DiarCEMIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dias';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SMS';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Email';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Carta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DdNoEnv'; // N�o enviar mais ap�s x dias se n�o enviado
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '5';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CartaoAR'; // Cart�o Aviso de Recebimento
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('DiarCEMBol') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Vencimento';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Data';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Mez';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Depto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatNum';
      FRCampos.Tipo       := 'double(20,0)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Propriet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Usuario';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmprEnti';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Juridica';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'UltAtzVal'; // para info em amail!
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);*)
      //
    end else
    if Uppercase(Tabela) = Uppercase('DiarCEMEnv') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DiarCEMIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Metodo'; // 1=SMS 2=Email 3=Carta
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cadastro'; // C�digo de cadastro do SMS ou Email ou Carta
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReInclu'; // tentativas de incluir o registro
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaInicio';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaLimite';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tentativas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LastExec';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ConsidEnvi';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LastLotImp'; // Agrupamento de impress�o
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AutoCancel';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LastVerify';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NaoEnvTip';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NaoEnvLst'; 
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NaoEnvMot';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('DiarCEMRet') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DiarCEMIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Metodo'; // 1=SMS 2=Email 3=Carta
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Execucao';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NaoEnvTip';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NaoEnvLst'; 
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NaoEnvMot';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NaoEnvVal';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('DiarioOpc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmCPreVda';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnDiario_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'DiarioAdd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DiarioAss';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnDiario_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  // ADD-DIARI-001 :: Adi��o de Evento ao Di�rio
  New(FRJanelas);
  FRJanelas.ID        := 'ADD-DIARI-001';
  FRJanelas.Nome      := 'FmDiarioAdd';
  FRJanelas.Descricao := 'Adi��o de Evento ao Di�rio';
  FLJanelas.Add(FRJanelas);
  //
  // ASS-DIARI-001 :: Cadastro de Assuntos - Di�rio
  New(FRJanelas);
  FRJanelas.ID        := 'ASS-DIARI-001';
  FRJanelas.Nome      := 'FmDiarioAss';
  FRJanelas.Descricao := 'Cadastro de Assuntos - Di�rio';
  FLJanelas.Add(FRJanelas);
  //
  // CEM-DIARI-001 :: Configura��es de Envio de Mensagens
  New(FRJanelas);
  FRJanelas.ID        := 'CEM-DIARI-001';
  FRJanelas.Nome      := 'FmDiarCEMCad';
  FRJanelas.Descricao := 'Configura��es de Envio de Mensagens';
  FLJanelas.Add(FRJanelas);
  //
  // CEM-DIARI-002 :: Item de Configura��o de Envio de Mensagens
  New(FRJanelas);
  FRJanelas.ID        := 'CEM-DIARI-002';
  FRJanelas.Nome      := 'FmDiarCEMIts';
  FRJanelas.Descricao := 'Item de Configura��o de Envio de Mensagens';
  FLJanelas.Add(FRJanelas);
  //
  // GER-DIARI-001 :: Gerencimento do Di�rio
  New(FRJanelas);
  FRJanelas.ID        := 'GER-DIARI-001';
  FRJanelas.Nome      := 'FmDiarioGer';
  FRJanelas.Descricao := 'Gerenciamento do Di�rio';
  FLJanelas.Add(FRJanelas);
  //
  // OPC-DIARI-001 :: Op��es do Di�rio
  New(FRJanelas);
  FRJanelas.ID        := 'OPC-DIARI-001';
  FRJanelas.Nome      := 'FmDiarioOpc';
  FRJanelas.Descricao := 'Op��es do Di�rio';
  FLJanelas.Add(FRJanelas);
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
