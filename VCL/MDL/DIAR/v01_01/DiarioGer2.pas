unit DiarioGer2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, Grids, DBGrids, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, Mask, ComCtrls,
  dmkEditDateTimePicker, frxClass, frxDBSet, UnDmkProcFunc, dmkImage,
  dmkCompoStore, dmkDBGridZTO, Vcl.Menus, Variants, UnDmkEnums;

type
  TFmDiarioGer2 = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    PnFiltro: TPanel;
    Label1: TLabel;
    LaCliInt: TLabel;
    LaEntidade: TLabel;
    EdDiarioAss: TdmkEditCB;
    CBDiarioAss: TdmkDBLookupComboBox;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Panel4: TPanel;
    QrDiarioAss: TmySQLQuery;
    QrDiarioAssCodigo: TIntegerField;
    QrDiarioAssCodUsu: TIntegerField;
    QrDiarioAssNome: TWideStringField;
    QrDiarioAssAplicacao: TIntegerField;
    DsDiarioAss: TDataSource;
    QrDeptos: TmySQLQuery;
    IntegerField2: TIntegerField;
    QrDeptosNome: TWideStringField;
    DsDeptos: TDataSource;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOME_ENT: TWideStringField;
    DsEntidades: TDataSource;
    Label2: TLabel;
    Label4: TLabel;
    LaDepto0: TLabel;
    QrDiarioAdd: TmySQLQuery;
    DsDiarioAdd: TDataSource;
    QrDiarioAddNOME_CLI: TWideStringField;
    QrDiarioAddNOME_ENT: TWideStringField;
    QrDiarioAddNOME_DEPTO: TWideStringField;
    QrDiarioAddCodigo: TIntegerField;
    QrDiarioAddNome: TWideStringField;
    QrDiarioAddDiarioAss: TIntegerField;
    QrDiarioAddEntidade: TIntegerField;
    QrDiarioAddCliInt: TIntegerField;
    QrDiarioAddDepto: TIntegerField;
    QrDiarioAddData: TDateField;
    QrDiarioAddHora: TTimeField;
    QrDiarioAddDataCad: TDateField;
    QrDiarioAddDataAlt: TDateField;
    QrDiarioAddUserCad: TIntegerField;
    QrDiarioAddUserAlt: TIntegerField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label7: TLabel;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    Label9: TLabel;
    DBEdit7: TDBEdit;
    frxGER_DIARI_001_01: TfrxReport;
    frxDsDiarioAdd: TfrxDBDataset;
    QrDiarioAddNOME_UH_ENT: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    QrCliIntFilial: TIntegerField;
    QrCliIntNOMEFILIAL: TWideStringField;
    QrCliIntCNPJ_CPF: TWideStringField;
    Panel5: TPanel;
    EdNome: TEdit;
    Label11: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel8: TPanel;
    BtPesquisa: TBitBtn;
    BtImprime: TBitBtn;
    GroupBox2: TGroupBox;
    Label10: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    LaTerceiro01: TLabel;
    EdInterloctr: TdmkEditCB;
    CBInterloctr: TdmkDBLookupComboBox;
    LaInterloctr: TLabel;
    EdTerceiro01: TdmkEditCB;
    CBTerceiro01: TdmkDBLookupComboBox;
    QrTerceiro01: TmySQLQuery;
    QrTerceiro01Codigo: TIntegerField;
    QrTerceiro01CliInt: TIntegerField;
    QrTerceiro01CodUsu: TIntegerField;
    QrTerceiro01NOMEENTIDADE: TWideStringField;
    DsTerceiro01: TDataSource;
    QrInterloctr: TmySQLQuery;
    QrInterloctrControle: TIntegerField;
    QrInterloctrNome: TWideStringField;
    DsInterloctr: TDataSource;
    CkInterloctr: TCheckBox;
    CSTabSheetChamou: TdmkCompoStore;
    BtOS: TBitBtn;
    PMOS: TPopupMenu;
    AdicionaconversasselecionadasaumaOS1: TMenuItem;
    QrDiarioAddGenero: TSmallintField;
    QrDiarioAddNO_GENERO: TWideStringField;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    DBMemo1: TDBMemo;
    PMDiarioAdd: TPopupMenu;
    Novodilogo1: TMenuItem;
    QrDiarioAddTipo: TSmallintField;
    QrDiarioAddBinaFone: TWideStringField;
    QrDiarioAddBinaDtHr: TDateTimeField;
    QrDiarioAddInterloctr: TIntegerField;
    QrDiarioAddTerceiro01: TIntegerField;
    QrDiarioAddPreAtend: TSmallintField;
    QrDiarioAddFormContat: TIntegerField;
    QrDiarioAddLk: TIntegerField;
    QrDiarioAddAlterWeb: TSmallintField;
    QrDiarioAddAtivo: TSmallintField;
    QrDiarioAddPrintOS: TIntegerField;
    QrDiarioAddLink2: TIntegerField;
    QrDiarioAddNO_DIARIOASS: TWideStringField;
    QrDiarioAddNOME_TERCEIRO01: TWideStringField;
    QrDiarioAddNO_INTERLOCTR: TWideStringField;
    Panel9: TPanel;
    Panel001: TPanel;
    PnDeptoCB: TPanel;
    LaDeptoA: TLabel;
    EdDeptoCB: TdmkEditCB;
    CBDepto: TdmkDBLookupComboBox;
    PnDeptoEd: TPanel;
    LaDeptoB: TLabel;
    EdDeptoEd: TdmkEdit;
    CkDepto: TCheckBox;
    Panel2: TPanel;
    GroupBox3: TGroupBox;
    Panel10: TPanel;
    RGUserGenero: TRadioGroup;
    CkUserGenero: TCheckBox;
    DBGDiarioAdd: TdmkDBGridZTO;
    Panel11: TPanel;
    RGOrderBy: TRadioGroup;
    QrDiarioAddNO_FORMCONTAT: TWideStringField;
    QrDiarioAddNO_UserCad: TWideStringField;
    QrDiarioAddNO_UserAlt: TWideStringField;
    BtOpcoes: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdCliIntChange(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure EdNomeChange(Sender: TObject);
    procedure TPDataIniChange(Sender: TObject);
    procedure TPDataIniClick(Sender: TObject);
    procedure TPDataFimChange(Sender: TObject);
    procedure TPDataFimClick(Sender: TObject);
    procedure EdDeptoCBChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCliIntExit(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure frxGER_DIARI_001_01GetValue(const VarName: string; var Value: Variant);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrDiarioAddAfterOpen(DataSet: TDataSet);
    procedure QrDiarioAddBeforeClose(DataSet: TDataSet);
    procedure RGOrderByClick(Sender: TObject);
    procedure EdTerceiro01Change(Sender: TObject);
    procedure EdInterloctrChange(Sender: TObject);
    procedure CkInterloctrClick(Sender: TObject);
    procedure EdDiarioAssEnter(Sender: TObject);
    procedure EdDiarioAssExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOSClick(Sender: TObject);
    procedure AdicionaconversasselecionadasaumaOS1Click(Sender: TObject);
    procedure RGUserGeneroClick(Sender: TObject);
    procedure CkUserGeneroClick(Sender: TObject);
    procedure EdDeptoEdChange(Sender: TObject);
    procedure Novodilogo1Click(Sender: TObject);
    procedure BtOpcoesClick(Sender: TObject);
  private
    { Private declarations }
    FAssunto: Integer;
    procedure FechaPesquisa();
    procedure CaptionDepto(Texto: String);
  public
    { Public declarations }
    FCliInt: Integer;
    procedure ReabreDeptos();
    procedure ReopenDiario(Codigo: Integer; ApenasPrepara: Boolean = False);
    procedure ReopenInterlocutor(Controle: Integer);
  end;

  var
  FmDiarioGer2: TFmDiarioGer2;

implementation

uses UMySQLModule, UnMyObjects, DmkDAC_PF, Module, MyListas, Principal, MyGlyfs,
UnDiario_Tabs, MyDBCheck, DiarioAdd2, ModuleGeral, UnDiario_PF;

{$R *.DFM}

procedure TFmDiarioGer2.AdicionaconversasselecionadasaumaOS1Click(
  Sender: TObject);
  procedure IncluiItemAtual(OSCab: Integer);
  var
    Nome, Data, Hora, BinaFone, BinaDtHr: String;
    Codigo, DiarioAss, Entidade, CliInt, Depto, Tipo, Genero, Interloctr, Terceiro01, PreAtend, FormContat, PrintOS: Integer;
  begin
    //
    Codigo         := QrDiarioAddCodigo.Value;
    if (QrDiarioAddDepto.Value <> 0) and (QrDiarioAddGenero.Value <> 0) then
    begin
       Geral.MB_Aviso('O item de conversa ID ' + Geral.FF0(Codigo) +
       ' j� possui atrelamento e n�o ser� modificado!' + sLineBreak +
       'Texto da conversa:' + sLineBreak + QrDiarioAddNome.Value);
       Exit;
     end;

(*
    Nome           := ;
    DiarioAss      := ;
    Entidade       := ;
    CliInt         := ;
*)
    Depto          := OSCab;
(*
    Data           := ;
    Hora           := ;
    Tipo           := ;
*)
    Genero         := CO_DIARIO_ADD_GENERO_ORD_SRV;
(*
    BinaFone       := ;
    BinaDtHr       := ;
    Interloctr     := ;
    Terceiro01     := ;
    PreAtend       := ;
    FormContat     := ;
    PrintOS        := ;
*)

    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'diarioadd', False, [
    (*'Nome', 'DiarioAss', 'Entidade',
    'CliInt',*) 'Depto', (*'Data',
    'Hora', 'Tipo',*) 'Genero'(*,
    'BinaFone', 'BinaDtHr', 'Interloctr',
    'Terceiro01', 'PreAtend', 'FormContat',
    'PrintOS'*)], [
    'Codigo'], [
    (*Nome, DiarioAss, Entidade,
    CliInt,*) Depto, (*Data,
    Hora, Tipo,*) Genero(*,
    BinaFone, BinaDtHr, Interloctr,
    Terceiro01, PreAtend, FormContat,
    PrintOS*)], [
    Codigo], True);
  end;
const
  Aviso   = '...';
  Titulo  = 'OS que receber� as conversas selecionadas';
  Prompt  = 'Informe o localizador:';
  Campo   = 'Descricao';
var
  Res: Variant;
  //, Controle, Dias, Aplicacao, AplicID
  I, Codigo: Integer;
  Qry: TmySQLQuery;
  //
  Entidade, Terceiro: Integer;
  Cnjnt: String;
begin
  case TdmkAppID(CO_DMKID_APP) of
    dmkappB_U_G_S_T_R_O_L:
    begin
      Entidade := EdEntidade.ValueVariant;
      Terceiro := EdTerceiro01.ValueVariant;
      Cnjnt := '';
      if (Entidade <> 0) and (Terceiro <> 0) then
        Cnjnt := 'IN (' + Geral.FF0(Entidade) + ', ' + Geral.FF0(Terceiro) + ')'
      else
      if Entidade <> 0 then
        Cnjnt := ' = ' + Geral.FF0(Entidade)
      else
      if Terceiro <> 0 then
        Cnjnt := ' = ' + Geral.FF0(Terceiro)
      else
      begin
        Geral.MB_Aviso('Defina um "' + LaEntidade.Caption +
        '" ou um "' + LaTerceiro01.Caption + '".');
        //
        Exit;
      end;
      //
      Codigo := 0;
      Res := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
      'SELECT cab.Codigo, CONCAT(LPAD(cab.Codigo, 9, "0"), ',
      '     IF(cab.Entidade   ' + Cnjnt + ', " [CL] ", ',
      '     IF(cab.EntContrat ' + Cnjnt + ', " [CO] ", ',
      '     IF(cab.EntPagante ' + Cnjnt + ', " [PA] ", ',
      '     " [??] "))), ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) ' + Campo,
      'FROM oscab cab ',
      'LEFT JOIN entidades ent ON ent.Codigo= ',
      '     IF(cab.Entidade   ' + Cnjnt + ', cab.Entidade, ',
      '     IF(cab.EntContrat ' + Cnjnt + ', cab.EntContrat, ',
      '     IF(cab.EntPagante ' + Cnjnt + ', cab.EntPagante, ',
      '     "? ? ? ? ?"))) ',
      'WHERE cab.Entidade ' + Cnjnt + ' ',
      'OR cab.EntContrat ' + Cnjnt + ' ',
      'OR cab.EntPagante ' + Cnjnt + ' ',
      'ORDER BY CODIGO DESC ',
      ''], Dmod.MyDB, False);
      if (Res <> Null) and (Res <> 0) then
      begin
        Screen.Cursor := crHourGlass;
        try
          Codigo := Res;
          if DBGDiarioAdd.SelectedRows.Count > 2 then
          begin
            with DBGDiarioAdd.DataSource.DataSet do
            for I := 0 to DBGDiarioAdd.SelectedRows.Count-1 do
            begin
              //GotoBookmark(pointer(DBGDiarioAdd.SelectedRows.Items[I]));
              GotoBookmark(DBGDiarioAdd.SelectedRows.Items[I]);
              //
              IncluiItemAtual(Codigo);
            end;
          end else
            IncluiItemAtual(Codigo);
          //
          ReopenDiario(Codigo);
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end
    else
    begin
      Geral.MB_Info('App n�o implementado para este procedimento!');
    end;
  end;
end;

procedure TFmDiarioGer2.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxGER_DIARI_001_01,[
  DModG.frxDsDono,
  frxDsDiarioAdd
  ]);
  MyObjects.frxMostra(frxGER_DIARI_001_01, 'Pesquisa de Di�rio');
end;

procedure TFmDiarioGer2.BtOpcoesClick(Sender: TObject);
begin
  Diario_PF.MostraFormDiarioOpc();
end;

procedure TFmDiarioGer2.BtOSClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOS, BtOS);
end;

procedure TFmDiarioGer2.BtPesquisaClick(Sender: TObject);
begin
  ReopenDiario(0);
end;

procedure TFmDiarioGer2.BtSaidaClick(Sender: TObject);
begin
  if TFmDiarioGer2(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmDiarioGer2(Self), TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmDiarioGer2.CaptionDepto(Texto: String);
begin
  begin
    LaDepto0.Caption := Texto + ':';
    LaDeptoA.Caption := LaDepto0.Caption;
    LaDeptoB.Caption := LaDepto0.Caption;
  end;
end;

procedure TFmDiarioGer2.CkInterloctrClick(Sender: TObject);
begin
  ReopenInterlocutor(0);
end;

procedure TFmDiarioGer2.CkUserGeneroClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer2.EdCliIntChange(Sender: TObject);
begin
  FechaPesquisa();
  if not EdCliInt.Focused then
    ReabreDeptos();
end;

procedure TFmDiarioGer2.EdCliIntExit(Sender: TObject);
begin
  if FCliInt <> EdCliInt.ValueVariant then
  begin
    FCliInt := QrCliIntCodigo.Value;
    ReabreDeptos();
  end;
end;

procedure TFmDiarioGer2.EdDeptoCBChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer2.EdDeptoEdChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer2.EdDiarioAssEnter(Sender: TObject);
begin
  FAssunto := EdDiarioAss.ValueVariant;
end;

procedure TFmDiarioGer2.EdDiarioAssExit(Sender: TObject);
begin
  if FAssunto <> EdDiarioAss.ValueVariant then
  begin
    FAssunto := EdDiarioAss.ValueVariant;
    FechaPesquisa();
  end;
end;

procedure TFmDiarioGer2.EdEntidadeChange(Sender: TObject);
begin
  FechaPesquisa();
  ReopenInterlocutor(0);
end;

procedure TFmDiarioGer2.EdInterloctrChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer2.EdNomeChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer2.EdTerceiro01Change(Sender: TObject);
begin
  FechaPesquisa();
  ReopenInterlocutor(0);
end;

procedure TFmDiarioGer2.FechaPesquisa();
begin
  QrDiarioAdd.Close;
  BtImprime.Enabled := False;
end;

procedure TFmDiarioGer2.FormActivate(Sender: TObject);
begin
  if TFmDiarioGer2(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente;
  end;
end;

procedure TFmDiarioGer2.FormCreate(Sender: TObject);
  procedure ReabreCliInt_A();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCliInt, Dmod.MyDB, [
    'SELECT DISTINCT ent.Codigo, ent.CliInt Filial, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL, ',
    'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF ',
    'FROM entidades ent ',
    'WHERE ent.CliInt <> 0 ',
    '']);
  end;
  procedure ReabreCliInt_B();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCliInt, Dmod.MyDB, [
    'SELECT DISTINCT ent.Codigo, ent.Filial, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL, ',
    'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF ',
    'FROM entidades ent ',
    'WHERE ent.Codigo < -10 ',
    '']);
  end;
begin
  FAssunto := 0;
  // Ini cfg Apps
  // B U G S T R O L
  if CO_DMKID_APP = 24 then
  begin
    LaCliInt.Caption      := 'Empresa:';
    LaEntidade.Caption    := '[Sub]Cliente:';
    LaTerceiro01.Caption  := 'Contratante:';
    LaInterloctr.Caption  := 'Interlocutor:';
  end;
  // Fim cfg Apps
  ImgTipo.SQLType := stLok;
  MyObjects.ConfiguraRadioGroup(RGUserGenero, sListaAllGeneros, MaxAllGeneros + 1, 0);
  TPDataIni.Date := 0;
  TPDataFim.Date := Date;
  //
  UnDmkDAC_PF.AbreQuery(QrDiarioAss, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTerceiro01, Dmod.MyDB);
  PnDeptoCB.Visible := False;
  PnDeptoEd.Visible := False;
  //
  CaptionDepto(sListaAllGeneros[RGUserGenero.ItemIndex]);
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    ReabreCliInt_A();
    PnDeptoCB.Visible := True;
  end else
  if (Uppercase(Application.Title) = 'CREDITOR')
  or (Uppercase(Application.Title) = 'BUGSTROL')
  or (Uppercase(Application.Title) = '_D_I_A_') then
  begin
    ReabreCliInt_B();
    PnDeptoEd.Visible := True;
  end else
    Geral.MB_Aviso('Aplicativo n�o implementado para uso do Di�rio! [1]' +
    sLineBreak + 'Solicite � DERMATEK!');
end;

procedure TFmDiarioGer2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDiarioGer2.FormShow(Sender: TObject);
begin
  //FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  ReopenDiario(0, True);
end;

procedure TFmDiarioGer2.frxGER_DIARI_001_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_PERIODO' then Value := dmkPF.PeriodoImp(TPDataIni.Date,
    TPDataFim.Date, 0, 0, True, True, False, False, '', '')
  else
  if VarName = 'VARF_EMPRESA' then
    Value := CBCliInt.Text;
end;

procedure TFmDiarioGer2.Novodilogo1Click(Sender: TObject);
var
  CliInt, Entidade1, Terceiro01: Integer;
begin
  if DBCheck.CriaFm(TFmDiarioAdd2, FmDiarioAdd2, afmoNegarComAviso) then
  begin
    FmDiarioAdd2.ImgTipo.SQLType := stIns;
    FmDiarioAdd2.FGenero := CO_DIARIO_ADD_GENERO_NO_LINK;
    FmDiarioAdd2.FDepto  := 0;
    FmDiarioAdd2.FQry    := QrDiarioAdd;
    //
    CliInt := EdCliInt.ValueVariant;
    FmDiarioAdd2.EdCliInt.ValueVariant := CliInt;
    FmDiarioAdd2.CBCliInt.KeyValue := CliInt;
    //
    Entidade1 := EdEntidade.ValueVariant;
    FmDiarioAdd2.EdEntidade1.ValueVariant := Entidade1;
    FmDiarioAdd2.CBEntidade1.KeyValue := Entidade1;
    //
    Terceiro01 := EdTerceiro01.ValueVariant;
    FmDiarioAdd2.EdTerceiro01.ValueVariant := Terceiro01;
    FmDiarioAdd2.CBTerceiro01.KeyValue := Terceiro01;
    //
    FmDiarioAdd2.LaEntidade1.Caption := '[Sub]Cliente:';
    FmDiarioAdd2.LaEntidade2.Caption := 'COntratante:';

    FmDiarioAdd2.ShowModal;
    FmDiarioAdd2.Destroy;
    //
    ReopenDiario(0);
  end;
end;

procedure TFmDiarioGer2.QrDiarioAddAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrDiarioAdd.RecordCount > 0;
  BtImprime.Enabled := Habilita;
  //
  //Habilita := Habilita and ()
  BtOS.Enabled := Habilita;
end;

procedure TFmDiarioGer2.QrDiarioAddBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
  BtOS.Enabled := False;
end;

procedure TFmDiarioGer2.ReabreDeptos();
var
  Codigo: Integer;
begin
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    EdDeptoCB.ValueVariant := 0;
    CBDepto.KeyValue     := 0;
    //
    Codigo := QrCliIntFilial.Value;
    UnDmkDAC_PF.AbreMySQLQuery0(QrDeptos, Dmod.MyDB, [
    'SELECT Conta Codigo, Unidade Nome ',
    'FROM condimov ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
  end else
  if (Uppercase(Application.Title) = 'CREDITOR')
  or (Uppercase(Application.Title) = 'BUGSTROL')
  or (Uppercase(Application.Title) = '_D_I_A_') then
  begin
    // Nada
  end else
    Geral.MB_Aviso('Aplicativo n�o implementado para uso do Di�rio! [2]' +
    sLineBreak + 'Solicite � DERMATEK!');
end;

procedure TFmDiarioGer2.ReopenDiario(Codigo: Integer; ApenasPrepara: Boolean);
var
  Empresa, DiarioAss, Entidade, Depto, Interloctr, Terceiro01, Genero: Integer;
  Nome: String;
  SQL: String;
  //
  Status: String;
begin
  Status := dmkPF.ArrayToTexto('dad.Genero', 'NO_GENERO', pvPos, True,
    sListaAllGeneros);
  //
  Screen.Cursor := crHourGlass;
  try
    UmyMod.ObtemCodigoDeCodUsu(EdDiarioAss, DiarioAss, '');
    DiarioAss  := EdDiarioAss.ValueVariant;
    Entidade   := EdEntidade.ValueVariant;
    Nome       := EdNome.Text;
    Interloctr := EdInterloctr.ValueVariant;
    Terceiro01 := EdTerceiro01.ValueVariant;
    if EdCliInt.ValueVariant <> 0 then
      Empresa := QrCliIntCodigo.Value
    else
      Empresa := 0;
    //
    if Uppercase(Application.Title) = 'SYNDIC' then
    // Depto � a UH lct.Depto=condimov.Conta
    begin
      Depto := EdDeptoCB.ValueVariant;
      SQL := Geral.ATS([
      'SELECT CONCAT(dpt.Unidade, " - ", ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NOME_UH_ENT, ',
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLI, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_ENT, ',
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NOME_TERCEIRO01, ',
      Status,
      'dpt.Unidade NOME_DEPTO, dad.*, ',
      'das.Nome NO_DIARIOASS, eco.Nome NO_INTERLOCTR, ',
      'foc.Nome NO_FORMCONTAT ',
      'FROM diarioadd dad ',
      'LEFT JOIN entidades cli ON cli.Codigo=dad.CliInt ',
      'LEFT JOIN entidades ent ON ent.Codigo=dad.Entidade ',
      'LEFT JOIN entidades ter ON ter.Codigo=dad.Terceiro01 ',
      'LEFT JOIN condimov  dpt ON dpt.Conta =dad.Depto ',
      'LEFT JOIN diarioass das ON das.Codigo=dad.DiarioAss ',
      'LEFT JOIN enticontat eco ON eco.Controle=dad.Interloctr ',
      'LEFT JOIN formcontat foc ON foc.Codigo=dad.FormContat ',
      '']);
    end else
    if UpperCase(Application.Title) = 'CREDITOR' then
    begin
      Depto := EdDeptoEd.ValueVariant;
      SQL := Geral.ATS([
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_UH_ENT, ',
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLI, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_ENT, ',
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NOME_TERCEIRO01, ',
      Status,
      '"" NOME_DEPTO, dad.*, ',
      'das.Nome NO_DIARIOASS, eco.Nome NO_INTERLOCTR, ',
      'foc.Nome NO_FORMCONTAT ',
      'FROM diarioadd dad ',
      'LEFT JOIN entidades cli ON cli.Codigo=dad.CliInt ',
      'LEFT JOIN entidades ent ON ent.Codigo=dad.Entidade ',
      'LEFT JOIN entidades ter ON ter.Codigo=dad.Terceiro01 ',
      'LEFT JOIN diarioass das ON das.Codigo=dad.DiarioAss ',
      'LEFT JOIN enticontat eco ON eco.Controle=dad.Interloctr ',
      'LEFT JOIN formcontat foc ON foc.Codigo=dad.FormContat ',
      '']);
    end else
    if (Uppercase(Application.Title) = 'BUGSTROL')
    or (Uppercase(Application.Title) = '_D_I_A_') then
    begin
      if CkUserGenero.Checked then
      begin
        Genero := RGUserGenero.ItemIndex;
        Depto  := EdDeptoEd.ValueVariant;
      end else
      begin
        Genero := 0;
        Depto  := 0;
      end;
      SQL := Geral.ATS([
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_UH_ENT, ',
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLI, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_ENT, ',
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NOME_TERCEIRO01, ',
      Status,
      'FORMAT(dad.Depto, 0) NOME_DEPTO, dad.*, ',
      'das.Nome NO_DIARIOASS, eco.Nome NO_INTERLOCTR, ',
      'foc.Nome NO_FORMCONTAT, ',
      'se1.Login NO_UserCad, se2.Login NO_UserAlt ',
      'FROM diarioadd dad ',
      'LEFT JOIN entidades cli ON cli.Codigo=dad.CliInt ',
      'LEFT JOIN entidades ent ON ent.Codigo=dad.Entidade ',
      'LEFT JOIN entidades ter ON ter.Codigo=dad.Terceiro01 ',
      'LEFT JOIN diarioass das ON das.Codigo=dad.DiarioAss ',
      'LEFT JOIN enticontat eco ON eco.Controle=dad.Interloctr ',
      'LEFT JOIN formcontat foc ON foc.Codigo=dad.FormContat ',
      'LEFT JOIN senhas se1 ON se1.Numero=dad.UserCad',
      'LEFT JOIN senhas se2 ON se2.Numero=dad.UserAlt',
      '']);
    end else begin
      Geral.MB_Aviso('Aplicativo n�o implementado para uso do Di�rio! [3]' +
        sLineBreak + 'Solicite � DERMATEK!');
      Exit;
    end;
    SQL := SQL + Geral.ATS([
      dmkPF.SQL_Periodo('WHERE dad.Data ', TPDataIni.Date,
        TPDataFim.Date, True, True),
    Geral.ATS_If(DiarioAss <> 0, ['AND dad.DiarioAss = ' + Geral.FF0(DiarioAss)]),
    Geral.ATS_If(Empresa <> 0, ['AND dad.CliInt = ' + Geral.FF0(Empresa)]),
    Geral.ATS_If(Entidade <> 0, ['AND dad.Entidade = ' + Geral.FF0(Entidade)]),
    Geral.ATS_If(CkUserGenero.Checked,
         ['AND dad.Genero = ' + Geral.FF0(Genero),
         Geral.ATS_If(CkDepto.Checked,
                 ['AND dad.Depto = ' + Geral.FF0(Depto)]
                 )]
         ),
    Geral.ATS_If(Nome <> '', ['AND dad.Nome LIKE "%' + Nome + '%"']),
    Geral.ATS_If(Interloctr <> 0, ['AND dad.Interloctr = ' + Geral.FF0(Interloctr)]),
    Geral.ATS_If(Terceiro01 <> 0, ['AND dad.Terceiro01 = ' + Geral.FF0(Terceiro01)]),
    Geral.ATS_If(RGOrderBy.ItemIndex = 0, ['ORDER BY dad.Data, dad.Hora, NOME_UH_ENT']),
    Geral.ATS_If(RGOrderBy.ItemIndex = 1, ['ORDER BY NOME_UH_ENT, dad.Data, dad.Hora']),
    '']);
    UnDmkDAC_PF.AbreMySQLQuery0(QrDiarioAdd, Dmod.MyDB, SQL);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDiarioGer2.ReopenInterlocutor(Controle: Integer);
var
  Entidade, Terceiro01, Localize: Integer;
  SQL1, Liga: String;
begin
  Localize := 0;
  if Controle <> 0 then
    Localize := Controle
  else
  if QrInterloctr.State <> dsInactive then
    Localize := QrInterloctrControle.Value;
  //
  QrInterloctr.Close;
  Entidade := EdEntidade.ValueVariant;
  Terceiro01 := EdTerceiro01.ValueVariant;
  if (Entidade <> 0) or (Terceiro01 <> 0) or (CkInterloctr.Checked = False) then
  begin
    if CkInterloctr.Checked then
    begin
      SQL1 := '';
      Liga := ' WHERE ';
      if Entidade <> 0 then
      begin
// 2013-06-29
        SQL1 := SQL1 + Liga + ' ece.Codigo=' + Geral.FF0(Entidade) + sLineBreak;
        Liga := ' OR ';
      end;
      if Terceiro01 <> 0 then
        SQL1 := SQL1 + Liga + ' ece.Codigo=' + Geral.FF0(Terceiro01) + sLineBreak;
    end else
      SQL1 := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrInterloctr, Dmod.MyDB, [
(*
    'SELECT Controle, Nome ',
    'FROM enticontat ',
*)
    'SELECT eco.Controle, eco.Nome ',
    'FROM enticontat eco ',
    'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
    SQL1 +
    'ORDER BY eco.Nome ',
    '']);
// FIM 2013-06-29
    QrInterloctr.Locate('Controle', Localize, []);
  end;
end;

procedure TFmDiarioGer2.RGOrderByClick(Sender: TObject);
begin
  if QrDiarioAdd.State <> dsInactive then
    ReopenDiario(0)
  else
    FechaPesquisa;
end;

procedure TFmDiarioGer2.RGUserGeneroClick(Sender: TObject);
begin
  CaptionDepto(sListaAllGeneros[RGUserGenero.ItemIndex]);
  FechaPesquisa();
end;

procedure TFmDiarioGer2.TPDataFimChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer2.TPDataFimClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer2.TPDataIniChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer2.TPDataIniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

end.
