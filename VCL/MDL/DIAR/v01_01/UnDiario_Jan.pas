unit UnDiario_Jan;

interface

uses mySQLDbTables, UMySQLModule, dmkGeral, ModuleGeral, dmkImage, Forms,
  Controls, Windows, SysUtils, ComCtrls, Grids, DBGrids, DB,
  UnDmkProcFunc, ExtCtrls, dmkDBGrid, Math, Dialogs, UnDmkEnums, Classes,
  dmkDBGridZTO, frxDBSet, frxClass, UnInternalConsts;

type
  TUnDiario_Jan = class(TObject)
  private
    procedure MostraCNAB_Cfg(Codigo: Integer);
  public
    procedure MostraDiarioAdd();
    procedure MostraDiarioGer2();
    procedure MostraDiarioAss();
  end;

var
  UDiario_Jan: TUnDiario_Jan;

implementation

uses MyDBCheck, UnMyObjects, BloGerenLocper, DmkDAC_PF, Module, DiarioAdd,
  DiarioGer2, DiarioAss;

{ TUnDiario_Jan }

procedure TUnDiario_Jan.MostraDiarioAss;
begin
  if DBCheck.CriaFm(TFmDiarioAss, FmDiarioAss, afmoNegarComAviso) then
  begin
    FmDiarioAss.ShowModal;
    FmDiarioAss.Destroy;
  end;
end;

procedure TUnDiario_Jan.MostraDiarioGer2();
begin
  if DBCheck.CriaFm(TFmDiarioGer2, FmDiarioGer2, afmoNegarComAviso) then
  begin
    FmDiarioGer2.ShowModal;
    FmDiarioGer2.Destroy;
  end;
end;

procedure TUnDiario_Jan.MostraCNAB_Cfg(Codigo: Integer);
begin

end;

procedure TUnDiario_Jan.MostraDiarioAdd;
begin
  if DBCheck.CriaFm(TFmDiarioAdd, FmDiarioAdd, afmoNegarComAviso) then
  begin
    FmDiarioAdd.FChamou        := -1;
    FmDiarioAdd.ImgTipo.SQLType := stIns;
    FmDiarioAdd.ShowModal;
    FmDiarioAdd.Destroy;
  end;
end;

end.
