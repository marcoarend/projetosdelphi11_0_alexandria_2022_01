object FmDiarCEMIts: TFmDiarCEMIts
  Left = 339
  Top = 185
  Caption = 'CEM-DIARI-002 :: Item de Configura'#231#227'o de Envio de Mensagens'
  ClientHeight = 482
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 341
    Width = 784
    Height = 27
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 784
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 689
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 784
    Height = 229
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object LAInfo2B: TLabel
      Left = 317
      Top = 202
      Width = 445
      Height = 16
      Caption = 
        '('#178')('#179') "Email" e "Carta" s'#227'o concorrentes entre si com prioridad' +
        'e para o email!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object LaInfo2A: TLabel
      Left = 316
      Top = 201
      Width = 445
      Height = 16
      Caption = 
        '('#178')('#179') "Email" e "Carta" s'#227'o concorrentes entre si com prioridad' +
        'e para o email!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Lainfo1B: TLabel
      Left = 317
      Top = 182
      Width = 237
      Height = 16
      Caption = '(1)('#178')('#179') Informe "0" (zero) para n'#227'o enviar.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel
      Left = 12
      Top = 24
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label7: TLabel
      Left = 120
      Top = 24
      Width = 59
      Height = 13
      Caption = 'Enviar ap'#243's '
    end
    object Label1: TLabel
      Left = 12
      Top = 56
      Width = 32
      Height = 13
      Caption = 'SMS '#185':'
    end
    object SBSMS: TSpeedButton
      Left = 744
      Top = 72
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBSMSClick
    end
    object Label9: TLabel
      Left = 12
      Top = 96
      Width = 34
      Height = 13
      Caption = 'Email '#178':'
    end
    object SBEmail: TSpeedButton
      Left = 744
      Top = 112
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBEmailClick
    end
    object Label10: TLabel
      Left = 12
      Top = 136
      Width = 34
      Height = 13
      Caption = 'Carta '#179':'
    end
    object SBCarta: TSpeedButton
      Left = 744
      Top = 152
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBCartaClick
    end
    object Lainfo1A: TLabel
      Left = 316
      Top = 181
      Width = 237
      Height = 16
      Caption = '(1)('#178')('#179') Informe "0" (zero) para n'#227'o enviar.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 392
      Top = 24
      Width = 274
      Height = 13
      Caption = 'N'#227'o enviar mais SMS e email se n'#227'o conseguir enviar em '
    end
    object Label4: TLabel
      Left = 740
      Top = 24
      Width = 22
      Height = 13
      Caption = 'dias.'
    end
    object Label8: TLabel
      Left = 216
      Top = 24
      Width = 95
      Height = 13
      Caption = 'dias do vencimento.'
    end
    object EdControle: TdmkEdit
      Left = 32
      Top = 20
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utIdx
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdDias: TdmkEdit
      Left = 180
      Top = 20
      Width = 33
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '5'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '5'
      QryCampo = 'Dias'
      UpdCampo = 'Dias'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 5
    end
    object CBSMS: TdmkDBLookupComboBox
      Left = 68
      Top = 72
      Width = 676
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsTxtSMS
      TabOrder = 4
      dmkEditCB = EdSMS
      QryCampo = 'SMS'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdSMS: TdmkEditCB
      Left = 12
      Top = 72
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'SMS'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBSMS
      IgnoraDBLookupComboBox = False
    end
    object EdEmail: TdmkEditCB
      Left = 12
      Top = 112
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Email'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBEmail
      IgnoraDBLookupComboBox = False
    end
    object CBEmail: TdmkDBLookupComboBox
      Left = 68
      Top = 112
      Width = 676
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPreEmail
      TabOrder = 6
      dmkEditCB = EdEmail
      QryCampo = 'EMail'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCarta: TdmkEditCB
      Left = 12
      Top = 152
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Carta'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCarta
      IgnoraDBLookupComboBox = False
    end
    object CBCarta: TdmkDBLookupComboBox
      Left = 68
      Top = 152
      Width = 676
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Titulo'
      ListSource = DsCartas
      TabOrder = 8
      dmkEditCB = EdCarta
      QryCampo = 'Carta'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdDdNoEnv: TdmkEdit
      Left = 668
      Top = 20
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = True
      NoForceUppercase = False
      ValMin = '5'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '36525'
      QryCampo = 'DdNoEnv'
      UpdCampo = 'DdNoEnv'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 36525
    end
    object RGCartaoAR: TdmkRadioGroup
      Left = 12
      Top = 176
      Width = 185
      Height = 48
      Caption = ' Imprime cart'#227'o AR: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o'
        'Sim')
      TabOrder = 9
      QryCampo = 'CartaoAR'
      UpdCampo = 'CartaoAR'
      UpdType = utYes
      OldValor = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 560
        Height = 32
        Caption = 'Item de Configura'#231#227'o de Envio de Mensagens'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 560
        Height = 32
        Caption = 'Item de Configura'#231#227'o de Envio de Mensagens'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 560
        Height = 32
        Caption = 'Item de Configura'#231#227'o de Envio de Mensagens'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 368
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 412
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrTxtSMS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM txtsms'
      'ORDER BY Nome')
    Left = 232
    Top = 64
    object QrTxtSMSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTxtSMSNome: TWideStringField
      FieldName = 'Nome'
      Size = 140
    end
  end
  object DsTxtSMS: TDataSource
    DataSet = QrTxtSMS
    Left = 260
    Top = 64
  end
  object QrPreEmail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM preemail'
      'ORDER BY Nome')
    Left = 288
    Top = 64
    object QrPreEmailCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreEmailNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPreEmail: TDataSource
    DataSet = QrPreEmail
    Left = 316
    Top = 64
  end
  object QrCartas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Titulo'
      'FROM cartas'
      'ORDER BY Titulo')
    Left = 344
    Top = 64
    object QrCartasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartasTitulo: TWideStringField
      FieldName = 'Titulo'
      Size = 100
    end
  end
  object DsCartas: TDataSource
    DataSet = QrCartas
    Left = 372
    Top = 64
  end
end
