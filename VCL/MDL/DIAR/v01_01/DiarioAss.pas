unit DiarioAss;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkCheckGroup, UnDmkProcFunc,
  dmkImage, UnDmkEnums;

type
  TFmDiarioAss = class(TForm)
    PainelDados: TPanel;
    DsDiarioAss: TDataSource;
    QrDiarioAss: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrDiarioAssCodigo: TIntegerField;
    QrDiarioAssCodUsu: TIntegerField;
    QrDiarioAssNome: TWideStringField;
    CGAplicacao: TdmkCheckGroup;
    DBCGAplicacao: TdmkDBCheckGroup;
    QrDiarioAssAplicacao: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrDiarioAssAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrDiarioAssBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmDiarioAss: TFmDiarioAss;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects, MyListas;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmDiarioAss.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmDiarioAss.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrDiarioAssCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmDiarioAss.DefParams;
begin
  VAR_GOTOTABELA := 'DiarioAss';
  VAR_GOTOMYSQLTABLE := QrDiarioAss;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome, Aplicacao');
  VAR_SQLx.Add('FROM diarioass');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmDiarioAss.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'diarioass', 'CodUsu', [], [], stIns, 0, siPositivo, nil);
end;

procedure TFmDiarioAss.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Erro('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmDiarioAss.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmDiarioAss.AlteraRegistro;
var
  DiarioAss : Integer;
begin
  DiarioAss := QrDiarioAssCodigo.Value;
  if QrDiarioAssCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(DiarioAss, Dmod.MyDB, 'DiarioAss', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(DiarioAss, Dmod.MyDB, 'DiarioAss', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmDiarioAss.IncluiRegistro;
var
  Cursor : TCursor;
  DiarioAss : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    DiarioAss := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'DiarioAss', 'DiarioAss', 'Codigo');
    if Length(FormatFloat(FFormatFloat, DiarioAss))>Length(FFormatFloat) then
    begin
      Geral.MB_(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, DiarioAss);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmDiarioAss.QueryPrincipalAfterOpen;
begin
end;

procedure TFmDiarioAss.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmDiarioAss.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmDiarioAss.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmDiarioAss.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmDiarioAss.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmDiarioAss.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrDiarioAss, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'diarioass');
end;

procedure TFmDiarioAss.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrDiarioAssCodigo.Value;
  Close;
end;

procedure TFmDiarioAss.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('DiarioAss', 'Codigo', ImgTipo.SQLType,
    QrDiarioAssCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmDiarioAss, PainelEdit,
    'DiarioAss', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmDiarioAss.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'DiarioAss', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'DiarioAss', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'DiarioAss', 'Codigo');
end;

procedure TFmDiarioAss.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrDiarioAss, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'diarioass');
end;

procedure TFmDiarioAss.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    CGAplicacao.Items[2] := 'Unidade';
    DBCGAplicacao.Items[2] := 'Unidade';
  end else
  // B U G S T R O L
  if CO_DMKID_APP = 24 then
  begin
    CGAplicacao.Items[0] := 'Empresa';
    DBCGAplicacao.Items[0] := 'Empresa';
    //
    CGAplicacao.Items[1] := 'Sub[Cliente]';
    DBCGAplicacao.Items[1] := 'Sub[Cliente]';
    //
    CGAplicacao.Items[2] := 'ID Link';
    DBCGAplicacao.Items[2] := 'ID Link';
    //
    CGAplicacao.Items[3] := 'Contratante';
    DBCGAplicacao.Items[3] := 'Contratante';
    //
    CGAplicacao.Items[4] := 'Interlocutor';
    DBCGAplicacao.Items[4] := 'Interlocutor';
  end;
end;

procedure TFmDiarioAss.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrDiarioAssCodigo.Value, LaRegistro.Caption);
end;

procedure TFmDiarioAss.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmDiarioAss.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmDiarioAss.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrDiarioAssCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmDiarioAss.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmDiarioAss.QrDiarioAssAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmDiarioAss.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmDiarioAss.SbQueryClick(Sender: TObject);
begin
  LocCod(QrDiarioAssCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'DiarioAss', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmDiarioAss.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDiarioAss.QrDiarioAssBeforeOpen(DataSet: TDataSet);
begin
  QrDiarioAssCodigo.DisplayFormat := FFormatFloat;
end;

end.

