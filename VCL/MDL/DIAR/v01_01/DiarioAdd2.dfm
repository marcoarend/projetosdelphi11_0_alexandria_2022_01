object FmDiarioAdd2: TFmDiarioAdd2
  Left = 339
  Top = 185
  Caption = 'ADD-DIARI-001 :: Adi'#231#227'o de Evento ao Di'#225'rio'
  ClientHeight = 713
  ClientWidth = 667
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 667
    Height = 551
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnFiltro: TPanel
      Left = 0
      Top = 61
      Width = 667
      Height = 160
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 104
        Width = 41
        Height = 13
        Caption = 'Assunto:'
      end
      object LaCliInt: TLabel
        Left = 8
        Top = 8
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object LaEntidade1: TLabel
        Left = 8
        Top = 32
        Width = 74
        Height = 13
        Caption = 'Contatado 1:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object SpeedButton1: TSpeedButton
        Left = 640
        Top = 100
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object LaEntidade2: TLabel
        Left = 8
        Top = 56
        Width = 74
        Height = 13
        Caption = 'Contatado 2:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object SpeedButton2: TSpeedButton
        Left = 640
        Top = 28
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton2Click
      end
      object SpeedButton3: TSpeedButton
        Left = 640
        Top = 52
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton3Click
      end
      object SpeedButton4: TSpeedButton
        Left = 640
        Top = 4
        Width = 21
        Height = 21
        Caption = '&R'
        OnClick = SpeedButton4Click
      end
      object LaInterloctr: TLabel
        Left = 8
        Top = 80
        Width = 68
        Height = 14
        Caption = 'Interlocutor:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object SbInterloctr: TSpeedButton
        Left = 356
        Top = 76
        Width = 21
        Height = 21
        Caption = '...'
        Visible = False
        OnClick = SbInterloctrClick
      end
      object Label14: TLabel
        Left = 380
        Top = 80
        Width = 32
        Height = 13
        Caption = 'Forma:'
      end
      object SbFormContat: TSpeedButton
        Left = 640
        Top = 76
        Width = 21
        Height = 21
        Caption = '...'
        Visible = False
        OnClick = SbFormContatClick
      end
      object EdDiarioAss: TdmkEditCB
        Left = 84
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBDiarioAss
        IgnoraDBLookupComboBox = False
      end
      object CBDiarioAss: TdmkDBLookupComboBox
        Left = 140
        Top = 100
        Width = 497
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsDiarioAss
        TabOrder = 9
        dmkEditCB = EdDiarioAss
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCliInt: TdmkEditCB
        Left = 84
        Top = 4
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = EdCliIntExit
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 140
        Top = 4
        Width = 497
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DsCliInt
        TabOrder = 1
        dmkEditCB = EdCliInt
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEntidade1: TdmkEditCB
        Left = 84
        Top = 28
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEntidade1Change
        DBLookupComboBox = CBEntidade1
        IgnoraDBLookupComboBox = False
      end
      object CBEntidade1: TdmkDBLookupComboBox
        Left = 140
        Top = 28
        Width = 497
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsEnt1
        ParentFont = False
        TabOrder = 3
        dmkEditCB = EdEntidade1
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTerceiro01: TdmkEditCB
        Left = 84
        Top = 52
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTerceiro01Change
        DBLookupComboBox = CBTerceiro01
        IgnoraDBLookupComboBox = False
      end
      object CBTerceiro01: TdmkDBLookupComboBox
        Left = 140
        Top = 52
        Width = 497
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsEnt2
        ParentFont = False
        TabOrder = 5
        dmkEditCB = EdTerceiro01
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGEntidade: TRadioGroup
        Left = 0
        Top = 128
        Width = 667
        Height = 32
        Align = alBottom
        Caption = ' Pesquisa de dados do contatado: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Contatado 1'
          'Contatado 2')
        TabOrder = 10
        OnClick = RGEntidadeClick
      end
      object EdInterloctr: TdmkEditCB
        Left = 84
        Top = 76
        Width = 56
        Height = 22
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBInterloctr
        IgnoraDBLookupComboBox = False
      end
      object CBInterloctr: TdmkDBLookupComboBox
        Left = 140
        Top = 76
        Width = 217
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        KeyField = 'Controle'
        ListField = 'Nome'
        ListSource = DsInterloctr
        ParentFont = False
        TabOrder = 7
        dmkEditCB = EdInterloctr
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFormContat: TdmkEditCB
        Left = 416
        Top = 76
        Width = 32
        Height = 22
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFormContat
        IgnoraDBLookupComboBox = False
      end
      object CBFormContat: TdmkDBLookupComboBox
        Left = 448
        Top = 76
        Width = 189
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFormContat
        TabOrder = 12
        dmkEditCB = EdFormContat
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object PnTempo: TPanel
      Left = 0
      Top = 0
      Width = 667
      Height = 61
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 281
        Height = 61
        Align = alLeft
        Caption = ' Atendimento:'
        TabOrder = 0
        object Label2: TLabel
          Left = 12
          Top = 16
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label5: TLabel
          Left = 96
          Top = 16
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label6: TLabel
          Left = 216
          Top = 16
          Width = 26
          Height = 13
          Caption = 'Hora:'
        end
        object EdCodigo: TdmkEdit
          Left = 12
          Top = 32
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clTeal
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object TPData: TdmkEditDateTimePicker
          Left = 96
          Top = 32
          Width = 116
          Height = 21
          Date = 40125.721013657410000000
          Time = 40125.721013657410000000
          TabOrder = 1
          TabStop = False
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdHora: TdmkEdit
          Left = 216
          Top = 32
          Width = 56
          Height = 21
          TabStop = False
          TabOrder = 2
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfLong
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox2: TGroupBox
        Left = 281
        Top = 0
        Width = 386
        Height = 61
        Align = alClient
        Caption = ' Detec'#231#227'o por Bina: '
        TabOrder = 1
        object Label111: TLabel
          Left = 4
          Top = 16
          Width = 96
          Height = 13
          Caption = 'Telefone que tocou:'
        end
        object Label3: TLabel
          Left = 124
          Top = 16
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label13: TLabel
          Left = 244
          Top = 16
          Width = 26
          Height = 13
          Caption = 'Hora:'
        end
        object EdBinaFone: TdmkEdit
          Left = 6
          Top = 32
          Width = 112
          Height = 21
          TabStop = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtTelLongo
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'ETe1'
          UpdCampo = 'ETe1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object TPBinaSoDt: TdmkEditDateTimePicker
          Left = 124
          Top = 32
          Width = 116
          Height = 21
          Date = 0.721013657406729200
          Time = 0.721013657406729200
          TabOrder = 1
          TabStop = False
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdBinaSoHr: TdmkEdit
          Left = 244
          Top = 32
          Width = 56
          Height = 21
          TabStop = False
          TabOrder = 2
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object MeNome: TdmkMemo
      Left = 0
      Top = 424
      Width = 667
      Height = 127
      Align = alClient
      MaxLength = 255
      TabOrder = 2
      UpdType = utYes
    end
    object Panel3: TPanel
      Left = 0
      Top = 221
      Width = 667
      Height = 203
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 667
        Height = 203
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 0
        OnChange = PageControl1Change
        object TabSheet1: TTabSheet
          Caption = ' Dados de cadastro (Formato A)'
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 659
            Height = 175
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel9: TPanel
              Left = 0
              Top = 0
              Width = 659
              Height = 96
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label4: TLabel
                Left = 8
                Top = 8
                Width = 40
                Height = 13
                Caption = 'Contato:'
                FocusControl = DBEdit1
              end
              object Label7: TLabel
                Left = 8
                Top = 32
                Width = 31
                Height = 13
                Caption = 'E-mail:'
                FocusControl = DBEdit2
              end
              object Label8: TLabel
                Left = 8
                Top = 52
                Width = 54
                Height = 13
                Caption = 'Telefone 1:'
                FocusControl = DBEdit3
              end
              object Label9: TLabel
                Left = 128
                Top = 52
                Width = 54
                Height = 13
                Caption = 'Telefone 2:'
                FocusControl = DBEdit4
              end
              object Label10: TLabel
                Left = 248
                Top = 52
                Width = 54
                Height = 13
                Caption = 'Telefone 3:'
                FocusControl = DBEdit5
              end
              object Label11: TLabel
                Left = 368
                Top = 52
                Width = 35
                Height = 13
                Caption = 'Celular:'
                FocusControl = DBEdit6
              end
              object Label12: TLabel
                Left = 488
                Top = 52
                Width = 20
                Height = 13
                Caption = 'Fax:'
                FocusControl = DBEdit7
              end
              object DBEdit1: TDBEdit
                Left = 60
                Top = 4
                Width = 596
                Height = 21
                DataField = 'Contato'
                DataSource = DsEnt
                TabOrder = 0
              end
              object DBEdit2: TDBEdit
                Left = 60
                Top = 28
                Width = 596
                Height = 21
                DataField = 'Email'
                DataSource = DsEnt
                TabOrder = 1
              end
              object DBEdit3: TDBEdit
                Left = 8
                Top = 68
                Width = 116
                Height = 21
                DataField = 'Te1_TXT'
                DataSource = DsEnt
                TabOrder = 2
              end
              object DBEdit4: TDBEdit
                Left = 128
                Top = 68
                Width = 116
                Height = 21
                DataField = 'Te2_TXT'
                DataSource = DsEnt
                TabOrder = 3
              end
              object DBEdit5: TDBEdit
                Left = 248
                Top = 68
                Width = 116
                Height = 21
                DataField = 'Te3_TXT'
                DataSource = DsEnt
                TabOrder = 4
              end
              object DBEdit6: TDBEdit
                Left = 368
                Top = 68
                Width = 116
                Height = 21
                DataField = 'Cel_TXT'
                DataSource = DsEnt
                TabOrder = 5
              end
              object DBEdit7: TDBEdit
                Left = 488
                Top = 68
                Width = 116
                Height = 21
                DataField = 'Fax_TXT'
                DataSource = DsEnt
                TabOrder = 6
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Dados de Cadastro (Formato B) '
          ImageIndex = 1
          object PnContatos: TPanel
            Left = 0
            Top = 0
            Width = 659
            Height = 175
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGEntiContat: TDBGrid
              Left = 0
              Top = 0
              Width = 376
              Height = 175
              Align = alLeft
              DataSource = DsEntiContat
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Nome do contato'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_CARGO'
                  Title.Caption = 'Cargo'
                  Width = 140
                  Visible = True
                end>
            end
            object Panel5: TPanel
              Left = 376
              Top = 0
              Width = 283
              Height = 175
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object DBGEntiTel: TDBGrid
                Left = 0
                Top = 0
                Width = 283
                Height = 84
                Align = alTop
                DataSource = DsEntiTel
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NOMEETC'
                    Title.Caption = 'Tipo de telefone'
                    Width = 84
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'TEL_TXT'
                    Title.Caption = 'Telefone'
                    Width = 120
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ramal'
                    Width = 37
                    Visible = True
                  end>
              end
              object DBGEntiMail: TDBGrid
                Left = 0
                Top = 84
                Width = 283
                Height = 91
                Align = alClient
                DataSource = DsEntiMail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NOMEETC'
                    Title.Caption = 'Tipo de e-mail'
                    Width = 84
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EMail'
                    Title.Caption = 'E-mail'
                    Width = 159
                    Visible = True
                  end>
              end
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Dados do Procurador '
          ImageIndex = 2
          object Panel6: TPanel
            Left = 0
            Top = 53
            Width = 659
            Height = 122
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 376
              Height = 122
              Align = alLeft
              DataSource = DsPrcContat
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Nome do contato'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_CARGO'
                  Title.Caption = 'Cargo'
                  Width = 140
                  Visible = True
                end>
            end
            object Panel7: TPanel
              Left = 376
              Top = 0
              Width = 283
              Height = 122
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object DBGrid2: TDBGrid
                Left = 0
                Top = 0
                Width = 283
                Height = 60
                Align = alTop
                DataSource = DsPrcTel
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NOMEETC'
                    Title.Caption = 'Tipo de telefone'
                    Width = 84
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'TEL_TXT'
                    Title.Caption = 'Telefone'
                    Width = 120
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ramal'
                    Width = 37
                    Visible = True
                  end>
              end
              object DBGrid3: TDBGrid
                Left = 0
                Top = 60
                Width = 283
                Height = 62
                Align = alClient
                DataSource = DsPrcMail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NOMEETC'
                    Title.Caption = 'Tipo de e-mail'
                    Width = 84
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EMail'
                    Title.Caption = 'E-mail'
                    Width = 159
                    Visible = True
                  end>
              end
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 659
            Height = 53
            Align = alTop
            ParentBackground = False
            TabOrder = 1
            object DBEdit8: TDBEdit
              Left = 4
              Top = 4
              Width = 320
              Height = 21
              DataField = 'NO_PRC'
              DataSource = DsPrc
              TabOrder = 0
            end
            object DBEdit9: TDBEdit
              Left = 328
              Top = 4
              Width = 160
              Height = 21
              DataField = 'Contato'
              DataSource = DsPrc
              TabOrder = 1
            end
            object DBEdit10: TDBEdit
              Left = 492
              Top = 4
              Width = 160
              Height = 21
              DataField = 'Email'
              DataSource = DsPrc
              TabOrder = 2
            end
            object DBEdit11: TDBEdit
              Left = 4
              Top = 28
              Width = 116
              Height = 21
              DataField = 'Te1_TXT'
              DataSource = DsPrc
              TabOrder = 3
            end
            object DBEdit12: TDBEdit
              Left = 136
              Top = 28
              Width = 116
              Height = 21
              DataField = 'Te2_TXT'
              DataSource = DsPrc
              TabOrder = 4
            end
            object DBEdit13: TDBEdit
              Left = 400
              Top = 28
              Width = 116
              Height = 21
              DataField = 'Cel_TXT'
              DataSource = DsPrc
              TabOrder = 5
            end
            object DBEdit14: TDBEdit
              Left = 268
              Top = 28
              Width = 116
              Height = 21
              DataField = 'Te3_TXT'
              DataSource = DsPrc
              TabOrder = 6
            end
            object DBEdit15: TDBEdit
              Left = 532
              Top = 28
              Width = 116
              Height = 21
              DataField = 'Fax_TXT'
              DataSource = DsPrc
              TabOrder = 7
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 667
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 619
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 571
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 330
        Height = 32
        Caption = 'Adi'#231#227'o de Evento ao Di'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 330
        Height = 32
        Caption = 'Adi'#231#227'o de Evento ao Di'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 330
        Height = 32
        Caption = 'Adi'#231#227'o de Evento ao Di'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 599
    Width = 667
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 546
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 239
        Height = 16
        Caption = 'Bot'#227'o "R" > Cadastro r'#225'pido de entidade.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 239
        Height = 16
        Caption = 'Bot'#227'o "R" > Cadastro r'#225'pido de entidade.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object Panel12: TPanel
      Left = 548
      Top = 15
      Width = 117
      Height = 27
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object CkContinuar: TCheckBox
        Left = 4
        Top = 6
        Width = 117
        Height = 17
        Caption = 'Continuar inserindo.'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 643
    Width = 667
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 568
      Top = 15
      Width = 97
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 0
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel10: TPanel
      Left = 189
      Top = 15
      Width = 379
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnCadastroB: TPanel
        Left = 0
        Top = 0
        Width = 379
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        Visible = False
        object BtContatos: TBitBtn
          Tag = 10092
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Contatos'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtContatosClick
        end
        object BtEMails: TBitBtn
          Tag = 66
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'E&Mails'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtEMailsClick
        end
        object BtTelefones: TBitBtn
          Tag = 92
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Telefones'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtTelefonesClick
        end
      end
    end
    object Panel13: TPanel
      Left = 2
      Top = 15
      Width = 187
      Height = 53
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
      object BtOK: TBitBtn
        Tag = 14
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtGerencia: TBitBtn
        Tag = 398
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Gerencia'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtGerenciaClick
      end
    end
  end
  object QrDiarioAss: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, Aplicacao'
      'FROM diarioass'
      'ORDER BY Nome')
    Left = 44
    Top = 8
    object QrDiarioAssCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'diarioass.Codigo'
    end
    object QrDiarioAssCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'diarioass.CodUsu'
    end
    object QrDiarioAssNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'diarioass.Nome'
      Size = 50
    end
    object QrDiarioAssAplicacao: TIntegerField
      FieldName = 'Aplicacao'
      Origin = 'diarioass.Aplicacao'
    end
  end
  object DsDiarioAss: TDataSource
    DataSet = QrDiarioAss
    Left = 72
    Top = 8
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo, ent.CliInt Filial,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF'
      'FROM entidades ent'
      'WHERE ent.CliInt <> 0')
    Left = 488
    Top = 40
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrCliIntNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Required = True
      Size = 100
    end
    object QrCliIntCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 516
    Top = 40
  end
  object QrEnt1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 544
    Top = 40
    object QrEnt1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnt1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEnt1CliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEnt1NOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsEnt1: TDataSource
    DataSet = QrEnt1
    Left = 572
    Top = 40
  end
  object QrEnt: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntBeforeClose
    AfterScroll = QrEntAfterScroll
    OnCalcFields = QrEntCalcFields
    SQL.Strings = (
      'SELECT '
      'IF(Tipo=0,EContato,PContato) Contato,'
      'IF(Tipo=0,EEmail,PEmail) Email,'
      'IF(Tipo=0,ETe1,PTe1) Te1,'
      'IF(Tipo=0,ETe2,PTe2) Te2,'
      'IF(Tipo=0,ETe3,PTe3) Te3,'
      'IF(Tipo=0,ECel,PCel) Cel,'
      'IF(Tipo=0,EFax,PFax) Fax'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 32
    Top = 476
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntContato: TWideStringField
      FieldName = 'Contato'
      Size = 60
    end
    object QrEntEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrEntTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrEntTe2: TWideStringField
      FieldName = 'Te2'
    end
    object QrEntTe3: TWideStringField
      FieldName = 'Te3'
    end
    object QrEntCel: TWideStringField
      FieldName = 'Cel'
    end
    object QrEntFax: TWideStringField
      FieldName = 'Fax'
    end
    object QrEntTe1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntTe2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te2_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntTe3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te3_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntCel_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Cel_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntFax_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Fax_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsEnt: TDataSource
    DataSet = QrEnt
    Left = 60
    Top = 476
  end
  object QrEntiContat: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEntiContatAfterOpen
    BeforeClose = QrEntiContatBeforeClose
    AfterScroll = QrEntiContatAfterScroll
    SQL.Strings = (
      'SELECT eco.Codigo, eco.Sexo,'
      'eco.Controle, eco.Nome, eco.Cargo,'
      'eca.Nome NOME_CARGO '
      'FROM enticontat eco'
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo'
      'WHERE eco.Codigo=:P0'#10
      ''
      ''
      ''
      '')
    Left = 88
    Top = 476
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrEntiContatCargo: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrEntiContatNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
    object QrEntiContatCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiContatSexo: TSmallintField
      FieldName = 'Sexo'
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 116
    Top = 476
  end
  object QrEntiMail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, ema.Conta, '
      'ema.EMail, ema.EntiTipCto'
      'FROM entimail ema'
      'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto'#13
      'WHERE ema.Controle=:P0'
      #10)
    Left = 144
    Top = 476
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiMailNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Size = 30
    end
    object QrEntiMailConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEntiMailEMail: TWideStringField
      FieldName = 'EMail'
      Required = True
      Size = 255
    end
    object QrEntiMailEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Required = True
    end
  end
  object DsEntiMail: TDataSource
    DataSet = QrEntiMail
    Left = 172
    Top = 476
  end
  object QrEntiTel: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiTelCalcFields
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, emt.Conta, '
      'emt.Telefone, emt.EntiTipCto, emt.Ramal'
      'FROM entitel emt'
      'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto'#13
      'WHERE emt.Controle=:P0')
    Left = 200
    Top = 476
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTelNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Origin = 'entitipcto.Nome'
      Size = 30
    end
    object QrEntiTelConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'entitel.Conta'
      Required = True
    end
    object QrEntiTelTelefone: TWideStringField
      FieldName = 'Telefone'
      Origin = 'entitel.Telefone'
    end
    object QrEntiTelEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'entitel.EntiTipCto'
      Required = True
    end
    object QrEntiTelTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntiTelRamal: TWideStringField
      FieldName = 'Ramal'
      Origin = 'entitel.Ramal'
    end
  end
  object DsEntiTel: TDataSource
    DataSet = QrEntiTel
    Left = 228
    Top = 476
  end
  object QrPrc: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPrcBeforeClose
    AfterScroll = QrPrcAfterScroll
    OnCalcFields = QrPrcCalcFields
    SQL.Strings = (
      'SELECT '
      'IF(Tipo=0,RazaoSocial,Nome) NO_PRC,'
      'IF(Tipo=0,EContato,PContato) Contato,'
      'IF(Tipo=0,EEmail,PEmail) Email,'
      'IF(Tipo=0,ETe1,PTe1) Te1,'
      'IF(Tipo=0,ETe2,PTe2) Te2,'
      'IF(Tipo=0,ETe3,PTe3) Te3,'
      'IF(Tipo=0,ECel,PCel) Cel,'
      'IF(Tipo=0,EFax,PFax) Fax'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 32
    Top = 504
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrcContato: TWideStringField
      FieldName = 'Contato'
      Size = 60
    end
    object QrPrcEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrPrcTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrPrcTe2: TWideStringField
      FieldName = 'Te2'
    end
    object QrPrcTe3: TWideStringField
      FieldName = 'Te3'
    end
    object QrPrcCel: TWideStringField
      FieldName = 'Cel'
    end
    object QrPrcFax: TWideStringField
      FieldName = 'Fax'
    end
    object QrPrcTe1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te1_TXT'
      Size = 30
      Calculated = True
    end
    object QrPrcTe2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te2_TXT'
      Size = 30
      Calculated = True
    end
    object QrPrcTe3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te3_TXT'
      Size = 30
      Calculated = True
    end
    object QrPrcCel_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Cel_TXT'
      Size = 30
      Calculated = True
    end
    object QrPrcFax_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Fax_TXT'
      Size = 30
      Calculated = True
    end
    object QrPrcNO_PRC: TWideStringField
      FieldName = 'NO_PRC'
      Required = True
      Size = 100
    end
  end
  object DsPrc: TDataSource
    DataSet = QrPrc
    Left = 60
    Top = 504
  end
  object QrPrcContat: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPrcContatAfterOpen
    BeforeClose = QrPrcContatBeforeClose
    AfterScroll = QrPrcContatAfterScroll
    SQL.Strings = (
      'SELECT eco.Controle, eco.Nome, eco.Cargo,'
      'eca.Nome NOME_CARGO '
      'FROM enticontat eco'
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo'
      'WHERE eco.Codigo=:P0'#10
      '')
    Left = 88
    Top = 504
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrcContatControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPrcContatNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrPrcContatCargo: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrPrcContatNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
  end
  object DsPrcContat: TDataSource
    DataSet = QrPrcContat
    Left = 116
    Top = 504
  end
  object QrPrcMail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, ema.Conta, '
      'ema.EMail, ema.EntiTipCto'
      'FROM entimail ema'
      'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto'#13
      'WHERE ema.Controle=:P0'
      #10)
    Left = 144
    Top = 504
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrcMailNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Size = 30
    end
    object QrPrcMailConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrPrcMailEMail: TWideStringField
      FieldName = 'EMail'
      Required = True
      Size = 255
    end
    object QrPrcMailEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Required = True
    end
  end
  object DsPrcMail: TDataSource
    DataSet = QrPrcMail
    Left = 172
    Top = 504
  end
  object QrPrcTel: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, emt.Conta, '
      'emt.Telefone, emt.EntiTipCto, emt.Ramal'
      'FROM entitel emt'
      'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto'#13
      'WHERE emt.Controle=:P0')
    Left = 200
    Top = 504
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrcTelNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Origin = 'entitipcto.Nome'
      Size = 30
    end
    object QrPrcTelConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'entitel.Conta'
      Required = True
    end
    object QrPrcTelTelefone: TWideStringField
      FieldName = 'Telefone'
      Origin = 'entitel.Telefone'
    end
    object QrPrcTelEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'entitel.EntiTipCto'
      Required = True
    end
    object QrPrcTelTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrPrcTelRamal: TWideStringField
      FieldName = 'Ramal'
      Origin = 'entitel.Ramal'
    end
  end
  object DsPrcTel: TDataSource
    DataSet = QrPrcTel
    Left = 228
    Top = 504
  end
  object QrLoc2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Propriet Ent1, Usuario Ent2'
      'FROM condimov'
      'WHERE Conta=:P0')
    Left = 124
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLoc2Ent1: TIntegerField
      FieldName = 'Ent1'
    end
    object QrLoc2Ent2: TIntegerField
      FieldName = 'Ent2'
    end
  end
  object QrEnt2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 600
    Top = 40
    object QrEnt2Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrEnt2CliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'entidades.CliInt'
    end
    object QrEnt2CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEnt2NOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsEnt2: TDataSource
    DataSet = QrEnt2
    Left = 628
    Top = 40
  end
  object QrEnt2Creditor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CNPJ, Nome'
      'FROM sacados'
      'ORDER BY Nome')
    Left = 232
    Top = 312
    object QrEnt2CreditorCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'sacados.CNPJ'
      Size = 15
    end
    object QrEnt2CreditorNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'sacados.Nome'
      Size = 50
    end
  end
  object DsEnt2Creditor: TDataSource
    DataSet = QrEnt2Creditor
    Left = 260
    Top = 312
  end
  object PMContatos: TPopupMenu
    OnPopup = PMContatosPopup
    Left = 216
    Top = 556
    object Incluinovocontato1: TMenuItem
      Caption = '&Inclui novo contato'
      OnClick = Incluinovocontato1Click
    end
    object AlteraContatoatual1: TMenuItem
      Caption = '&Altera Contato atual'
      OnClick = AlteraContatoatual1Click
    end
    object Excluicontatos1: TMenuItem
      Caption = '&Exclui contato(s)'
      OnClick = Excluicontatos1Click
    end
  end
  object PMEMails: TPopupMenu
    OnPopup = PMEMailsPopup
    Left = 272
    Top = 556
    object NovoEMail1: TMenuItem
      Caption = '&Novo e-mail'
      OnClick = NovoEMail1Click
    end
    object Alteraemailselecionado1: TMenuItem
      Caption = '&Altera e-mail selecionado'
      OnClick = Alteraemailselecionado1Click
    end
    object Excluiemailselecionado1: TMenuItem
      Caption = '&Exclui e-mail selecionado'
      OnClick = Excluiemailselecionado1Click
    end
  end
  object PMTelefones: TPopupMenu
    OnPopup = PMTelefonesPopup
    Left = 300
    Top = 556
    object Incluinovotelefone1: TMenuItem
      Caption = '&Inclui novo telefone'
      OnClick = Incluinovotelefone1Click
    end
    object Alteratelefoneatual1: TMenuItem
      Caption = '&Altera telefone atual'
      OnClick = Alteratelefoneatual1Click
    end
    object Excluitelefones1: TMenuItem
      Caption = '&Exclui telefone(s)'
      OnClick = Excluitelefones1Click
    end
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 332
    Top = 132
  end
  object QrInterloctr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '    SELECT Controle, Nome '
      '    FROM enticontat '
      '    ORDER BY Nome')
    Left = 392
    object QrInterloctrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrInterloctrNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsInterloctr: TDataSource
    DataSet = QrInterloctr
    Left = 420
  end
  object QrFormContat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM formcontat'
      'ORDER BY Nome')
    Left = 448
    Top = 128
    object QrFormContatCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormContatNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsFormContat: TDataSource
    DataSet = QrFormContat
    Left = 476
    Top = 128
  end
  object QrDiarioOpc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM diarioopc')
    Left = 452
    Top = 488
  end
end
