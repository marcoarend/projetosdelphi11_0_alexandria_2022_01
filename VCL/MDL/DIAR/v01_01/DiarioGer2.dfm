object FmDiarioGer2: TFmDiarioGer2
  Left = 339
  Top = 185
  Caption = 'GER-DIARI-001 :: Gerencimento do Di'#225'rio'
  ClientHeight = 631
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 245
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 645
      Height = 245
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 645
        Height = 145
        Align = alTop
        Caption = ' Pesquisa: '
        TabOrder = 0
        object PnFiltro: TPanel
          Left = 2
          Top = 15
          Width = 641
          Height = 130
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 41
            Height = 13
            Caption = 'Assunto:'
          end
          object LaCliInt: TLabel
            Left = 8
            Top = 28
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object LaEntidade: TLabel
            Left = 324
            Top = 4
            Width = 45
            Height = 13
            Caption = 'Entidade:'
          end
          object Label11: TLabel
            Left = 8
            Top = 88
            Width = 73
            Height = 13
            Caption = 'Parte do Texto:'
          end
          object LaTerceiro01: TLabel
            Left = 324
            Top = 28
            Width = 42
            Height = 13
            Caption = 'Terceiro:'
          end
          object LaInterloctr: TLabel
            Left = 8
            Top = 68
            Width = 56
            Height = 13
            Caption = 'Interlocutor:'
          end
          object EdDiarioAss: TdmkEditCB
            Left = 64
            Top = 0
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnEnter = EdDiarioAssEnter
            OnExit = EdDiarioAssExit
            DBLookupComboBox = CBDiarioAss
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBDiarioAss: TdmkDBLookupComboBox
            Left = 120
            Top = 0
            Width = 200
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsDiarioAss
            TabOrder = 1
            dmkEditCB = EdDiarioAss
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCliInt: TdmkEditCB
            Left = 64
            Top = 24
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCliIntChange
            OnExit = EdCliIntExit
            DBLookupComboBox = CBCliInt
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCliInt: TdmkDBLookupComboBox
            Left = 120
            Top = 24
            Width = 200
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DsCliInt
            TabOrder = 3
            dmkEditCB = EdCliInt
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdEntidade: TdmkEditCB
            Left = 380
            Top = 0
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEntidadeChange
            DBLookupComboBox = CBEntidade
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEntidade: TdmkDBLookupComboBox
            Left = 436
            Top = 0
            Width = 200
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOME_ENT'
            ListSource = DsEntidades
            TabOrder = 5
            dmkEditCB = EdEntidade
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdNome: TEdit
            Left = 8
            Top = 104
            Width = 353
            Height = 21
            TabOrder = 11
            OnChange = EdNomeChange
          end
          object GroupBox2: TGroupBox
            Left = 364
            Top = 88
            Width = 257
            Height = 42
            Caption = ' Per'#237'odo: '
            TabOrder = 12
            object Label10: TLabel
              Left = 119
              Top = 20
              Width = 15
              Height = 13
              Caption = 'at'#233
            end
            object TPDataIni: TdmkEditDateTimePicker
              Left = 4
              Top = 16
              Width = 112
              Height = 21
              Date = 40125.000000000000000000
              Time = 0.821355567131831800
              TabOrder = 0
              OnClick = TPDataIniClick
              OnChange = TPDataIniChange
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object TPDataFim: TdmkEditDateTimePicker
              Left = 140
              Top = 16
              Width = 112
              Height = 21
              Date = 40125.000000000000000000
              Time = 0.821355567131831800
              TabOrder = 1
              OnClick = TPDataFimClick
              OnChange = TPDataFimChange
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
          end
          object EdInterloctr: TdmkEditCB
            Left = 72
            Top = 64
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdInterloctrChange
            DBLookupComboBox = CBInterloctr
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBInterloctr: TdmkDBLookupComboBox
            Left = 128
            Top = 64
            Width = 460
            Height = 21
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsInterloctr
            TabOrder = 10
            dmkEditCB = EdInterloctr
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdTerceiro01: TdmkEditCB
            Left = 380
            Top = 24
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdTerceiro01Change
            DBLookupComboBox = CBTerceiro01
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBTerceiro01: TdmkDBLookupComboBox
            Left = 436
            Top = 24
            Width = 200
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsTerceiro01
            TabOrder = 7
            dmkEditCB = EdTerceiro01
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CkInterloctr: TCheckBox
            Left = 64
            Top = 46
            Width = 521
            Height = 17
            Caption = 
              'Mostrar apenas interlocutores relacionados as entidades informad' +
              'as acima.'
            Checked = True
            State = cbChecked
            TabOrder = 8
            OnClick = CkInterloctrClick
          end
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 145
        Width = 645
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label2: TLabel
          Left = 4
          Top = 4
          Width = 70
          Height = 13
          Caption = 'Cliente interno:'
        end
        object Label4: TLabel
          Left = 324
          Top = 3
          Width = 45
          Height = 13
          Caption = 'Entidade:'
        end
        object LaDepto0: TLabel
          Left = 4
          Top = 28
          Width = 70
          Height = 13
          Caption = 'Departamento:'
        end
        object Label6: TLabel
          Left = 216
          Top = 28
          Width = 49
          Height = 13
          Caption = 'User cad.:'
          FocusControl = DBEdit4
        end
        object Label7: TLabel
          Left = 428
          Top = 28
          Width = 42
          Height = 13
          Caption = 'User alt.:'
          FocusControl = DBEdit5
        end
        object Label8: TLabel
          Left = 312
          Top = 28
          Width = 50
          Height = 13
          Caption = 'Data cad.:'
          FocusControl = DBEdit6
        end
        object Label9: TLabel
          Left = 524
          Top = 28
          Width = 43
          Height = 13
          Caption = 'Data alt.:'
          FocusControl = DBEdit7
        end
        object DBEdit1: TDBEdit
          Left = 80
          Top = 0
          Width = 237
          Height = 21
          DataField = 'NOME_CLI'
          DataSource = DsDiarioAdd
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 400
          Top = 0
          Width = 237
          Height = 21
          DataField = 'NOME_ENT'
          DataSource = DsDiarioAdd
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 80
          Top = 24
          Width = 133
          Height = 21
          DataField = 'NOME_DEPTO'
          DataSource = DsDiarioAdd
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 268
          Top = 24
          Width = 40
          Height = 21
          DataField = 'UserCad'
          DataSource = DsDiarioAdd
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 472
          Top = 24
          Width = 48
          Height = 21
          DataField = 'UserAlt'
          DataSource = DsDiarioAdd
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Left = 364
          Top = 24
          Width = 56
          Height = 21
          DataField = 'DataCad'
          DataSource = DsDiarioAdd
          TabOrder = 5
        end
        object DBEdit7: TDBEdit
          Left = 568
          Top = 24
          Width = 56
          Height = 21
          DataField = 'DataCad'
          DataSource = DsDiarioAdd
          TabOrder = 6
        end
      end
      object Panel11: TPanel
        Left = 0
        Top = 197
        Width = 645
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object RGOrderBy: TRadioGroup
          Left = 0
          Top = 0
          Width = 645
          Height = 48
          Align = alClient
          Caption = ' Ordena'#231#227'o: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Data > Hora > Entidade'
            'Entidade > Data > Hora')
          TabOrder = 0
          OnClick = RGOrderByClick
        end
      end
    end
    object Panel5: TPanel
      Left = 645
      Top = 0
      Width = 363
      Height = 245
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object DBMemo1: TDBMemo
        Left = 0
        Top = 122
        Width = 363
        Height = 123
        TabStop = False
        Align = alClient
        DataField = 'Nome'
        DataSource = DsDiarioAdd
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 13264128
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 363
        Height = 47
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Panel001: TPanel
          Left = 0
          Top = 0
          Width = 363
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object PnDeptoCB: TPanel
            Left = 185
            Top = 0
            Width = 178
            Height = 47
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            Visible = False
            object LaDeptoA: TLabel
              Left = 8
              Top = 24
              Width = 70
              Height = 13
              Caption = 'Departamento:'
            end
            object EdDeptoCB: TdmkEditCB
              Left = 80
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdDeptoCBChange
              DBLookupComboBox = CBDepto
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBDepto: TdmkDBLookupComboBox
              Left = 136
              Top = 20
              Width = 301
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsDeptos
              TabOrder = 1
              dmkEditCB = EdDeptoCB
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
          object PnDeptoEd: TPanel
            Left = 0
            Top = 0
            Width = 185
            Height = 47
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            Visible = False
            object LaDeptoB: TLabel
              Left = 7
              Top = 25
              Width = 70
              Height = 13
              Caption = 'Departamento:'
            end
            object EdDeptoEd: TdmkEdit
              Left = 81
              Top = 21
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdDeptoEdChange
            end
          end
          object CkDepto: TCheckBox
            Left = 12
            Top = 0
            Width = 113
            Height = 17
            Caption = ' Somente o item:'
            TabOrder = 2
            OnClick = CkUserGeneroClick
          end
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 47
        Width = 363
        Height = 75
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object GroupBox3: TGroupBox
          Left = 0
          Top = 0
          Width = 363
          Height = 75
          Align = alTop
          Caption = 
            '                                                                ' +
            '    '
          TabOrder = 0
          object Panel10: TPanel
            Left = 2
            Top = 15
            Width = 359
            Height = 58
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object RGUserGenero: TRadioGroup
              Left = 0
              Top = 0
              Width = 359
              Height = 58
              Align = alClient
              Caption = ' A'#231#227'o que foi tomada:'
              ItemIndex = 0
              Items.Strings = (
                'Nenhuma'
                '? ? ? ? ?')
              TabOrder = 0
              OnClick = RGUserGeneroClick
            end
          end
          object CkUserGenero: TCheckBox
            Left = 12
            Top = -2
            Width = 205
            Height = 17
            Caption = ' Fitros de atrelamentos das conversas: '
            TabOrder = 1
            OnClick = CkUserGeneroClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
      object BtOpcoes: TBitBtn
        Tag = 348
        Left = 3
        Top = 6
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOpcoesClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 328
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 287
        Height = 32
        Caption = 'Gerencimento do Di'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 287
        Height = 32
        Caption = 'Gerencimento do Di'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 287
        Height = 32
        Caption = 'Gerencimento do Di'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 376
      Top = 0
      Width = 584
      Height = 48
      Align = alRight
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 580
        Height = 31
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 561
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 18
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtOS: TBitBtn
        Tag = 539
        Left = 268
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Conversas'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtOSClick
      end
    end
  end
  object DBGDiarioAdd: TdmkDBGridZTO
    Left = 0
    Top = 293
    Width = 1008
    Height = 268
    Align = alClient
    DataSource = DsDiarioAdd
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
    PopupMenu = PMDiarioAdd
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    RowColors = <>
    Columns = <
      item
        Expanded = False
        FieldName = 'Data'
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Hora'
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Entidade'
        Title.Caption = 'Contratante'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME_UH_ENT'
        Title.Caption = 'Nome sub-cliente'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Terceiro01'
        Title.Caption = 'Contratante'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME_TERCEIRO01'
        Title.Caption = 'Nome contratante'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Interloctr'
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_INTERLOCTR'
        Title.Caption = 'Nome Interlocutor'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_DIARIOASS'
        Title.Caption = 'Assunto'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_GENERO'
        Title.Caption = 'Atrelamento'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Depto'
        Title.Caption = 'ID atrelam.'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME_DEPTO'
        Title.Caption = 'Txt altrelam.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BinaFone'
        Title.Caption = 'Fone (Bina)'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_FORMCONTAT'
        Title.Caption = 'Forma de contato'
        Width = 89
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME_CLI'
        Title.Caption = 'Empresa'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Codigo'
        Title.Caption = 'ID conversa'
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_UserCad'
        Visible = True
      end>
  end
  object QrDiarioAss: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, Aplicacao'
      'FROM diarioass'
      'ORDER BY Nome')
    Left = 548
    Top = 8
    object QrDiarioAssCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDiarioAssCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrDiarioAssNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrDiarioAssAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object DsDiarioAss: TDataSource
    DataSet = QrDiarioAss
    Left = 576
    Top = 8
  end
  object QrDeptos: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ?'
      'ORDER BY Nome')
    Left = 604
    Top = 8
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDeptosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsDeptos: TDataSource
    DataSet = QrDeptos
    Left = 632
    Top = 8
  end
  object QrCliInt: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo, ent.CliInt Filial,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF'
      'FROM entidades ent'
      'WHERE ent.CliInt <> 0')
    Left = 660
    Top = 8
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrCliIntNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Required = True
      Size = 100
    end
    object QrCliIntCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 688
    Top = 8
  end
  object QrEntidades: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOME_ENT'
      'FROM entidades'
      'ORDER BY NOME_ENT')
    Left = 716
    Top = 8
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 744
    Top = 8
  end
  object QrDiarioAdd: TMySQLQuery
    Database = DModG.RV_CEP_DB
    AfterOpen = QrDiarioAddAfterOpen
    BeforeClose = QrDiarioAddBeforeClose
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_UH_ENT,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLI,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_ENT,'
      ' ELT(dad.Genero + 1,"Sem link","U.H.","Lct.","O.S.") NO_GENERO,'
      'FORMAT(dad.Depto, 0) NOME_DEPTO, dad.*,'
      'das.Nome NO_DIARIOASS'
      'FROM diarioadd dad'
      'LEFT JOIN entidades cli ON cli.Codigo=dad.CliInt'
      'LEFT JOIN entidades ent ON ent.Codigo=dad.Entidade'
      'LEFT JOIN diarioass das ON das.Codigo=dad.DiarioAss'
      'WHERE dad.Data  BETWEEN "0000-00-00" AND "2013-12-18 23:59:59"'
      'ORDER BY dad.Data, dad.Hora, NOME_UH_ENT'
      '')
    Left = 312
    Top = 416
    object QrDiarioAddNOME_CLI: TWideStringField
      FieldName = 'NOME_CLI'
      Size = 100
    end
    object QrDiarioAddNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrDiarioAddNOME_DEPTO: TWideStringField
      FieldName = 'NOME_DEPTO'
      Size = 10
    end
    object QrDiarioAddCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDiarioAddNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrDiarioAddDiarioAss: TIntegerField
      FieldName = 'DiarioAss'
    end
    object QrDiarioAddEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrDiarioAddCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrDiarioAddDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrDiarioAddData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddHora: TTimeField
      FieldName = 'Hora'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrDiarioAddDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDiarioAddUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDiarioAddNOME_UH_ENT: TWideStringField
      FieldName = 'NOME_UH_ENT'
      Size = 113
    end
    object QrDiarioAddGenero: TSmallintField
      FieldName = 'Genero'
    end
    object QrDiarioAddNO_GENERO: TWideStringField
      FieldName = 'NO_GENERO'
      Size = 100
    end
    object QrDiarioAddTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDiarioAddBinaFone: TWideStringField
      FieldName = 'BinaFone'
    end
    object QrDiarioAddBinaDtHr: TDateTimeField
      FieldName = 'BinaDtHr'
    end
    object QrDiarioAddInterloctr: TIntegerField
      FieldName = 'Interloctr'
    end
    object QrDiarioAddTerceiro01: TIntegerField
      FieldName = 'Terceiro01'
    end
    object QrDiarioAddPreAtend: TSmallintField
      FieldName = 'PreAtend'
    end
    object QrDiarioAddFormContat: TIntegerField
      FieldName = 'FormContat'
    end
    object QrDiarioAddLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrDiarioAddAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrDiarioAddAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrDiarioAddPrintOS: TIntegerField
      FieldName = 'PrintOS'
    end
    object QrDiarioAddLink2: TIntegerField
      FieldName = 'Link2'
    end
    object QrDiarioAddNO_DIARIOASS: TWideStringField
      FieldName = 'NO_DIARIOASS'
      Size = 50
    end
    object QrDiarioAddNOME_TERCEIRO01: TWideStringField
      FieldName = 'NOME_TERCEIRO01'
      Size = 100
    end
    object QrDiarioAddNO_INTERLOCTR: TWideStringField
      FieldName = 'NO_INTERLOCTR'
      Size = 30
    end
    object QrDiarioAddNO_FORMCONTAT: TWideStringField
      FieldName = 'NO_FORMCONTAT'
      Size = 60
    end
    object QrDiarioAddNO_UserCad: TWideStringField
      FieldName = 'NO_UserCad'
      Size = 30
    end
    object QrDiarioAddNO_UserAlt: TWideStringField
      FieldName = 'NO_UserAlt'
      Size = 30
    end
  end
  object DsDiarioAdd: TDataSource
    DataSet = QrDiarioAdd
    Left = 340
    Top = 416
  end
  object frxGER_DIARI_001_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38101.979964398100000000
    ReportOptions.LastChange = 39722.438154294000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxGER_DIARI_001_01GetValue
    Left = 368
    Top = 416
    Datasets = <
      item
        DataSet = frxDsDiarioAdd
        DataSetName = 'frxDsDiarioAdd'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 190.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      object TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDiarioAdd."NOME_UH_ENT"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Width = 680.314960630000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsDiarioAdd."NOME_UH_ENT"]')
          ParentFont = False
        end
      end
      object TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 7.559060000000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 3.779530000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Atividades Reportadas no Di'#225'rio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007874020000000000
        Top = 207.874150000000000000
        Width = 680.315400000000000000
        Child = frxGER_DIARI_001_01.Child1
        DataSet = frxDsDiarioAdd
        DataSetName = 'frxDsDiarioAdd'
        RowCount = 0
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Width = 34.015721180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data:')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 34.015672360000010000
          Width = 79.370068980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDiarioAdd."Data"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385753540000000000
          Width = 30.236178980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Hora:')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 143.621944720000000000
          Width = 45.354298980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = 'hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDiarioAdd."Hora"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 60.472431180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Interlocutor:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448882360000000000
          Width = 211.653618980000000000
          Height = 17.007874020000000000
          DataField = 'NO_INTERLOCTR'
          DataSet = frxDsDiarioAdd
          DataSetName = 'frxDsDiarioAdd'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDiarioAdd."NO_INTERLOCTR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102513540000000000
          Width = 45.354298980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Usu'#225'rio:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 506.456824720000000000
          Width = 173.858318980000000000
          Height = 17.007874020000000000
          DataField = 'NO_UserCad'
          DataSet = frxDsDiarioAdd
          DataSetName = 'frxDsDiarioAdd'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDiarioAdd."NO_UserCad"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Child1: TfrxChild
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        Stretched = True
        ToNRows = 0
        ToNRowsMode = rmCount
        object Rich1: TfrxRichView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataField = 'Nome'
          DataSet = frxDsDiarioAdd
          DataSetName = 'frxDsDiarioAdd'
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C205461686F6D613B7D7D0D0A7B5C2A5C67656E657261746F7220
            52696368656432302031302E302E31393034317D5C766965776B696E64345C75
            6331200D0A5C706172645C66305C667331365C7061720D0A7D0D0A00}
        end
      end
    end
  end
  object frxDsDiarioAdd: TfrxDBDataset
    UserName = 'frxDsDiarioAdd'
    CloseDataSource = False
    DataSet = QrDiarioAdd
    BCDToCurrency = False
    
    Left = 396
    Top = 416
  end
  object QrTerceiro01: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 772
    Top = 8
    object QrTerceiro01Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrTerceiro01CliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'entidades.CliInt'
    end
    object QrTerceiro01CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrTerceiro01NOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTerceiro01: TDataSource
    DataSet = QrTerceiro01
    Left = 800
    Top = 8
  end
  object QrInterloctr: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      '    SELECT Controle, Nome '
      '    FROM enticontat '
      '    ORDER BY Nome')
    Left = 828
    Top = 8
    object QrInterloctrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrInterloctrNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsInterloctr: TDataSource
    DataSet = QrInterloctr
    Left = 856
    Top = 8
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 424
    Top = 416
  end
  object PMOS: TPopupMenu
    Left = 304
    Top = 520
    object AdicionaconversasselecionadasaumaOS1: TMenuItem
      Caption = '&Adiciona conversas selecionadas a uma O.S.'
      OnClick = AdicionaconversasselecionadasaumaOS1Click
    end
  end
  object PMDiarioAdd: TPopupMenu
    Left = 784
    Top = 184
    object Novodilogo1: TMenuItem
      Caption = 'Novo di'#225'logo'
      OnClick = Novodilogo1Click
    end
  end
end
