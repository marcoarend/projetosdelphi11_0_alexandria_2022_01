unit DiarioAdd2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkMemo, ComCtrls, UnDmkProcFunc,
  dmkEditDateTimePicker, dmkLabel, dmkGeral, Mask, Grids, DBGrids, Variants,
  dmkImage, Menus, UnDmkEnums;

type
  TFmDiarioAdd2 = class(TForm)
    Panel1: TPanel;
    QrDiarioAss: TmySQLQuery;
    DsDiarioAss: TDataSource;
    QrDiarioAssCodigo: TIntegerField;
    QrDiarioAssCodUsu: TIntegerField;
    QrDiarioAssNome: TWideStringField;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrEnt1: TmySQLQuery;
    DsEnt1: TDataSource;
    PnFiltro: TPanel;
    Label1: TLabel;
    EdDiarioAss: TdmkEditCB;
    CBDiarioAss: TdmkDBLookupComboBox;
    LaCliInt: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    LaEntidade1: TLabel;
    EdEntidade1: TdmkEditCB;
    CBEntidade1: TdmkDBLookupComboBox;
    PnTempo: TPanel;
    MeNome: TdmkMemo;
    QrEnt1Codigo: TIntegerField;
    SpeedButton1: TSpeedButton;
    QrDiarioAssAplicacao: TIntegerField;
    Panel3: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    QrEnt: TmySQLQuery;
    QrEntContato: TWideStringField;
    QrEntEmail: TWideStringField;
    QrEntTe1: TWideStringField;
    QrEntTe2: TWideStringField;
    QrEntTe3: TWideStringField;
    QrEntCel: TWideStringField;
    QrEntFax: TWideStringField;
    QrEntTe1_TXT: TWideStringField;
    QrEntTe2_TXT: TWideStringField;
    QrEntTe3_TXT: TWideStringField;
    QrEntCel_TXT: TWideStringField;
    QrEntFax_TXT: TWideStringField;
    DsEnt: TDataSource;
    QrEntiContat: TmySQLQuery;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    QrEntiContatCargo: TIntegerField;
    QrEntiContatNOME_CARGO: TWideStringField;
    DsEntiContat: TDataSource;
    QrEntiMail: TmySQLQuery;
    QrEntiMailNOMEETC: TWideStringField;
    QrEntiMailConta: TIntegerField;
    QrEntiMailEMail: TWideStringField;
    QrEntiMailEntiTipCto: TIntegerField;
    DsEntiMail: TDataSource;
    QrEntiTel: TmySQLQuery;
    QrEntiTelNOMEETC: TWideStringField;
    QrEntiTelConta: TIntegerField;
    QrEntiTelTelefone: TWideStringField;
    QrEntiTelEntiTipCto: TIntegerField;
    QrEntiTelTEL_TXT: TWideStringField;
    QrEntiTelRamal: TWideStringField;
    DsEntiTel: TDataSource;
    PnContatos: TPanel;
    DBGEntiContat: TDBGrid;
    Panel5: TPanel;
    DBGEntiTel: TDBGrid;
    DBGEntiMail: TDBGrid;
    TabSheet3: TTabSheet;
    Panel6: TPanel;
    DBGrid1: TDBGrid;
    Panel7: TPanel;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    Panel8: TPanel;
    QrPrc: TmySQLQuery;
    DsPrc: TDataSource;
    QrPrcContat: TmySQLQuery;
    DsPrcContat: TDataSource;
    QrPrcMail: TmySQLQuery;
    DsPrcMail: TDataSource;
    QrPrcTel: TmySQLQuery;
    DsPrcTel: TDataSource;
    QrPrcContato: TWideStringField;
    QrPrcEmail: TWideStringField;
    QrPrcTe1: TWideStringField;
    QrPrcTe2: TWideStringField;
    QrPrcTe3: TWideStringField;
    QrPrcCel: TWideStringField;
    QrPrcFax: TWideStringField;
    QrPrcTe1_TXT: TWideStringField;
    QrPrcTe2_TXT: TWideStringField;
    QrPrcTe3_TXT: TWideStringField;
    QrPrcCel_TXT: TWideStringField;
    QrPrcFax_TXT: TWideStringField;
    QrPrcContatControle: TIntegerField;
    QrPrcContatNome: TWideStringField;
    QrPrcContatCargo: TIntegerField;
    QrPrcContatNOME_CARGO: TWideStringField;
    QrPrcMailNOMEETC: TWideStringField;
    QrPrcMailConta: TIntegerField;
    QrPrcMailEMail: TWideStringField;
    QrPrcMailEntiTipCto: TIntegerField;
    QrPrcTelNOMEETC: TWideStringField;
    QrPrcTelConta: TIntegerField;
    QrPrcTelTelefone: TWideStringField;
    QrPrcTelEntiTipCto: TIntegerField;
    QrPrcTelTEL_TXT: TWideStringField;
    QrPrcTelRamal: TWideStringField;
    QrPrcNO_PRC: TWideStringField;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Panel9: TPanel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    Label8: TLabel;
    DBEdit3: TDBEdit;
    Label9: TLabel;
    DBEdit4: TDBEdit;
    Label10: TLabel;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    LaEntidade2: TLabel;
    EdTerceiro01: TdmkEditCB;
    CBTerceiro01: TdmkDBLookupComboBox;
    RGEntidade: TRadioGroup;
    QrLoc2: TmySQLQuery;
    QrLoc2Ent1: TIntegerField;
    QrLoc2Ent2: TIntegerField;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    QrEnt2: TmySQLQuery;
    DsEnt2: TDataSource;
    QrEnt2Codigo: TIntegerField;
    QrEnt2CliInt: TIntegerField;
    QrCliIntCodigo: TIntegerField;
    QrCliIntFilial: TIntegerField;
    QrCliIntNOMEFILIAL: TWideStringField;
    QrCliIntCNPJ_CPF: TWideStringField;
    QrEnt2Creditor: TmySQLQuery;
    DsEnt2Creditor: TDataSource;
    QrEnt2CreditorCNPJ: TWideStringField;
    QrEnt2CreditorNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel10: TPanel;
    SpeedButton4: TSpeedButton;
    PMContatos: TPopupMenu;
    Incluinovocontato1: TMenuItem;
    AlteraContatoatual1: TMenuItem;
    Excluicontatos1: TMenuItem;
    PMEMails: TPopupMenu;
    NovoEMail1: TMenuItem;
    Alteraemailselecionado1: TMenuItem;
    Excluiemailselecionado1: TMenuItem;
    PMTelefones: TPopupMenu;
    Incluinovotelefone1: TMenuItem;
    Alteratelefoneatual1: TMenuItem;
    Excluitelefones1: TMenuItem;
    Panel12: TPanel;
    CkContinuar: TCheckBox;
    Panel13: TPanel;
    BtOK: TBitBtn;
    BtGerencia: TBitBtn;
    PnCadastroB: TPanel;
    BtContatos: TBitBtn;
    BtEMails: TBitBtn;
    BtTelefones: TBitBtn;
    QrEnt2CodUsu: TIntegerField;
    QrEnt1CodUsu: TIntegerField;
    QrEnt1CliInt: TIntegerField;
    QrEnt1NOMEENTIDADE: TWideStringField;
    QrEnt2NOMEENTIDADE: TWideStringField;
    Timer1: TTimer;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    EdCodigo: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    Label5: TLabel;
    Label6: TLabel;
    EdHora: TdmkEdit;
    GroupBox2: TGroupBox;
    Label111: TLabel;
    EdBinaFone: TdmkEdit;
    Label3: TLabel;
    TPBinaSoDt: TdmkEditDateTimePicker;
    Label13: TLabel;
    EdBinaSoHr: TdmkEdit;
    QrEntiContatCodigo: TIntegerField;
    QrEntiContatSexo: TSmallintField;
    QrInterloctr: TmySQLQuery;
    QrInterloctrControle: TIntegerField;
    QrInterloctrNome: TWideStringField;
    DsInterloctr: TDataSource;
    LaInterloctr: TLabel;
    EdInterloctr: TdmkEditCB;
    CBInterloctr: TdmkDBLookupComboBox;
    SbInterloctr: TSpeedButton;
    QrFormContat: TmySQLQuery;
    QrFormContatCodigo: TIntegerField;
    QrFormContatNome: TWideStringField;
    DsFormContat: TDataSource;
    Label14: TLabel;
    EdFormContat: TdmkEditCB;
    CBFormContat: TdmkDBLookupComboBox;
    SbFormContat: TSpeedButton;
    QrDiarioOpc: TmySQLQuery;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCliIntExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdEntidade1Change(Sender: TObject);
    procedure QrEntCalcFields(DataSet: TDataSet);
    procedure QrEntiContatAfterScroll(DataSet: TDataSet);
    procedure QrEntiContatBeforeClose(DataSet: TDataSet);
    procedure QrEntAfterScroll(DataSet: TDataSet);
    procedure QrEntBeforeClose(DataSet: TDataSet);
    procedure QrEntiContatAfterOpen(DataSet: TDataSet);
    procedure QrPrcAfterScroll(DataSet: TDataSet);
    procedure QrPrcBeforeClose(DataSet: TDataSet);
    procedure QrPrcCalcFields(DataSet: TDataSet);
    procedure QrPrcContatAfterOpen(DataSet: TDataSet);
    procedure QrPrcContatAfterScroll(DataSet: TDataSet);
    procedure QrPrcContatBeforeClose(DataSet: TDataSet);
    procedure RGEntidadeClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure EdTerceiro01Change(Sender: TObject);
    procedure BtGerenciaClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure Incluinovocontato1Click(Sender: TObject);
    procedure AlteraContatoatual1Click(Sender: TObject);
    procedure Excluicontatos1Click(Sender: TObject);
    procedure NovoEMail1Click(Sender: TObject);
    procedure Alteraemailselecionado1Click(Sender: TObject);
    procedure Excluiemailselecionado1Click(Sender: TObject);
    procedure Incluinovotelefone1Click(Sender: TObject);
    procedure Alteratelefoneatual1Click(Sender: TObject);
    procedure Excluitelefones1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BtContatosClick(Sender: TObject);
    procedure PMContatosPopup(Sender: TObject);
    procedure PMEMailsPopup(Sender: TObject);
    procedure PMTelefonesPopup(Sender: TObject);
    procedure BtTelefonesClick(Sender: TObject);
    procedure BtEMailsClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure QrEntiTelCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure SbInterloctrClick(Sender: TObject);
    procedure SbFormContatClick(Sender: TObject);
  private
    { Private declarations }
    FCliInt: Integer;
    procedure EditaEntidade(Qual, Modelo: Integer);
    procedure MostraEdicao();
    procedure DefineEntiSorc(var DestQr: TmySQLQuery; var DestDs: TDataSource);
    procedure DefineEntidades(var Origem, Atual: Integer);
    procedure ReopenInterloctr(Controle: Integer);
  public
    { Public declarations }
    //FChamou,
    FProcurador, FGenero, FDepto, FLink2: Integer;
    FNomeTipoPRC: String;
    FQry: TmySQLQuery;
    procedure ReopenEntidade();
    procedure ReopenEntiContat(Controle: Integer);
    procedure ReopenEntiMail(Conta: Integer);
    procedure ReopenEntiTel(Conta: Integer);

    procedure ReopenPrcContat(Controle: Integer);
    procedure ReopenPrcMail(Conta: Integer);
    procedure ReopenPrcTel(Conta: Integer);

    procedure PreencheDados(DataHora: Boolean; Data: TDate; Hora: TTime;
              DiarioAss, CliInt, Entidade1, Entidade2: Integer; Texto: WideString);
    procedure MostraEntiContat(SQLType: TSQLType);
    procedure MostraEntiMail(SQLType: TSQLType);
    procedure MostraEntiTel(SQLType: TSQLType);
  end;

  var
  FmDiarioAdd2: TFmDiarioAdd2;

implementation

uses DiarioAss, UnInternalConsts, MyDBCheck, UMySQLModule, Module, DmkDAC_PF,
  Entidade2, Entidades, UnDiario_Tabs, UnMyObjects, EntiRapido0, EntiContat,
  EntiMail, EntiTel, CfgCadLista, UnDiario_PF;

{$R *.DFM}

procedure TFmDiarioAdd2.AlteraContatoatual1Click(Sender: TObject);
begin
  MostraEntiContat(stUpd);
end;

procedure TFmDiarioAdd2.Alteraemailselecionado1Click(Sender: TObject);
begin
  MostraEntiMail(stUpd);
end;

procedure TFmDiarioAdd2.Alteratelefoneatual1Click(Sender: TObject);
begin
  MostraEntiTel(stUpd);
end;

procedure TFmDiarioAdd2.BtContatosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMContatos, BtContatos);
end;

procedure TFmDiarioAdd2.BtEMailsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEMails, BtEMails);
end;

procedure TFmDiarioAdd2.BtGerenciaClick(Sender: TObject);
{
var
  Entidade: Integer;
}
begin
{
  case RGEntidade.ItemIndex of
    0: Entidade := EdEntidade1.ValueVariant;
    1: Entidade := EdEntidade1.ValueVariant;
    else Entidade := 0;
  end;
  if DBCheck.CriaFm(TFmDiarioGer, FmDiarioGer, afmoNegarComAviso) then
  begin
    FmDiarioGer.EdDiarioAss.ValueVariant := EdDiarioAss.ValueVariant;
    FmDiarioGer.CBDiarioAss.KeyValue     := EdDiarioAss.ValueVariant;
    FmDiarioGer.EdCliInt.ValueVariant    := EdCliInt.ValueVariant;
    FmDiarioGer.CBCliInt.KeyValue        := EdCliInt.ValueVariant;
    FmDiarioGer.EdEntidade.ValueVariant  := Entidade;
    FmDiarioGer.CBEntidade.KeyValue      := Entidade;
    FmDiarioGer.ShowModal;
    FmDiarioGer.Destroy;
  end;
}
end;

procedure TFmDiarioAdd2.BtOKClick(Sender: TObject);
var
  DiarioAss, Entidade, CliInt, Depto, Codigo, Genero, Link2: Integer;
  Nome, Data, Hora, BinaFone, BinaDtHr: String;
var
  //Nome, Data, Hora, BinaFone, BinaDtHr: String;
  //Codigo, DiarioAss, Entidade, CliInt, Depto, Genero, PrintOS, Link2,
  //Tipo, PreAtend,
  Interloctr, Terceiro01, FormContat: Integer;
begin
  if not UmyMod.ObtemCodigoDeCodUsu(EdDiarioAss, DiarioAss,
    'Informe o assunto!') then Exit;
  //
  CliInt   := EdCliInt.ValueVariant;
  if CliInt <> 0 then
    CliInt := QrCliIntCodigo.Value;
  if MyObjects.FIC(Geral.IntInConjunto(1, QrDiarioAssAplicacao.Value) and
    (CliInt = 0), EdCliInt, 'Informe o campo "'+LaCliInt.Caption+'"') then Exit;
  //
  case RGEntidade.ItemIndex of
    0: Entidade := EdEntidade1.ValueVariant;
    1: Entidade := EdEntidade1.ValueVariant;
    else Entidade := 0;
  end;
  if MyObjects.FIC(Geral.IntInConjunto(2, QrDiarioAssAplicacao.Value) and
    (Entidade = 0), EdEntidade1, 'Informe o campo "Entidade:"!') then Exit;
  //
  Genero := FGenero;
  Depto := FDepto;
  Link2 := FLink2;
  //
  Data := Geral.FDT(TPData.Date, 1);
  Hora := EdHora.Text;
  Nome := MeNome.Text;
  if MyObjects.FIC(Trim(Nome) = '', nil, 'Texto n�o definido!') then Exit;
  //
  BinaFone := Geral.SoNumero_TT(EdBinaFone.Text);
  BinaDtHr := Geral.FDT(TPBinaSoDt.Date, 1) + ' ' + EdBinaSoHr.Text + ':00';
  Codigo := UMyMod.BuscaEmLivreY_Def('diarioadd', 'Codigo', ImgTipo.SQLType,
    EdCodigo.ValueVariant);
  if MyObjects.FIC(Codigo = 0, nil, 'C�digo n�o definido!') then Exit;
  //
  //Tipo       := ???;
  Interloctr := EdInterloctr.ValueVariant;
  Terceiro01 := EdTerceiro01.ValueVariant;
  //PreAtend   :=
  FormContat := EdFormContat.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'diarioadd', False, [
  'Nome', 'DiarioAss', 'Entidade',
  'CliInt', 'Depto', 'Data',
  'Hora', (*'Tipo',*) 'Genero',
  'BinaFone', 'BinaDtHr', 'Interloctr',
  'Terceiro01', (*'PreAtend',*) 'FormContat',
  (*'PrintOS',*) 'Link2'], [
  'Codigo'], [
  Nome, DiarioAss, Entidade,
  CliInt, Depto, Data,
  Hora, (*Tipo,*) Genero,
  BinaFone, BinaDtHr, Interloctr,
  Terceiro01, (*PreAtend,*) FormContat,
  (*PrintOS,*) Link2], [
  Codigo], True) then
  begin
    if (FQry <> nil)
    // Evitar abrir query sem estar com a SQL definida apropriadamente para o app atual
    and (FQry.State <> dsInactive) then
    begin
      UnDmkDAC_PF.AbreQuery(FQry, FQry.Database);
      //FQry.Last; // O �ltimo � o primeiro
      FQry.Locate('Codigo', Codigo, []);
    end;
    if CkContinuar.Checked then
    begin
      TPData.Date := Date;
      EdHora.ValueVariant := Time;
      //
      MeNome.Text := '';
      //
    end else
      Close;
    //
  end;
end;

procedure TFmDiarioAdd2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDiarioAdd2.BtTelefonesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTelefones, BtTelefones);
end;

procedure TFmDiarioAdd2.DefineEntidades(var Origem, Atual: Integer);
begin
  case RGEntidade.ItemIndex of
    0:
    begin
      Origem := QrEnt1Codigo.Value;
      Atual  := QrEnt1Codigo.Value;
    end;
    1:
    begin
      Origem := QrEnt2Codigo.Value;
      Atual  := QrEnt2Codigo.Value;
    end;
  end;
end;

procedure TFmDiarioAdd2.DefineEntiSorc(var DestQr: TmySQLQuery;
  var DestDs: TDataSource);
begin
  case RGEntidade.ItemIndex of
    0:
    begin
      DestQr := QrEnt1;
      DestDs := DsEnt1;
    end;
    1:
    begin
      DestQr := QrEnt2;
      DestDs := DsEnt2;
    end;
  end;
end;

procedure TFmDiarioAdd2.EdCliIntExit(Sender: TObject);
begin
  if FCliInt <> EdCliInt.ValueVariant then
    FCliInt := EdCliInt.ValueVariant;
end;

procedure TFmDiarioAdd2.EdEntidade1Change(Sender: TObject);
begin
  ReopenEntidade();
  ReopenInterloctr(0);
end;

procedure TFmDiarioAdd2.EdTerceiro01Change(Sender: TObject);
begin
  ReopenEntidade();
  ReopenInterloctr(0);
end;

procedure TFmDiarioAdd2.EditaEntidade(Qual, Modelo: Integer);
var
  Ent: Integer;
  Ed: TdmkEditCB;
  CB: TdmkDBLookupComboBox;
  Continua: Boolean;
begin
  Continua     := False;
  VAR_CADASTRO := 0;
  case Qual of
    0:
    begin
      Ent := EdEntidade1.ValueVariant;
      Ed := EdEntidade1;
      CB := CBEntidade1;
    end;
    1:
    begin
      Ent := EdTerceiro01.ValueVariant;
      Ed := EdTerceiro01;
      CB := CBTerceiro01;
    end else
    begin
      Ent := 0;
      Ed := nil;
      CB := nil;
    end;
  end;
  case Modelo of
    0:
    begin
      if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
      begin
        FmEntidades.LocCod(Ent, Ent);
        FmEntidades.ShowModal;
        FmEntidades.Destroy;
        //
        Continua := True;
      end;
    end;
    1:
    begin
      if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
      begin
        FmEntidade2.LocCod(Ent, Ent);
        FmEntidade2.ShowModal;
        FmEntidade2.Destroy;
        //
        Continua := True;
      end;
    end;
    else
    begin
      Geral.MB_Erro('Modelo n�o implementado!');
      Exit;
    end;
  end;
  if Continua then
  begin
    QrEnt1.Close;
    UnDmkDAC_PF.AbreQuery(QrEnt1, Dmod.MyDB);
    //
    QrEnt2.Close;
    UnDmkDAC_PF.AbreQuery(QrEnt2, Dmod.MyDB);
    //
    if VAR_CADASTRO <> 0 then
    begin
      Ed.ValueVariant := VAR_CADASTRO;
      CB.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmDiarioAdd2.Excluicontatos1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntiContat, DBGEntiContat,
    'enticontat', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmDiarioAdd2.Excluiemailselecionado1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntimail, DBGEntiMail,
    'entimail', ['Conta'], ['Conta'], istPergunta, '');
end;

procedure TFmDiarioAdd2.Excluitelefones1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntitel, DBGEntiTel,
    'entitel', ['Conta'], ['Conta'], istPergunta, '');
end;

procedure TFmDiarioAdd2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if FProcurador <> 0 then
  begin
    QrPrc.Close;
    QrPrc.Params[0].AsInteger := FProcurador;
    UnDmkDAC_PF.AbreQuery(QrPrc, Dmod.MyDB);
    //
    if FNomeTipoPRC <> '' then
      TabSheet3.Caption := ' Dados do ' + FNomeTipoPRC + ' ';
    PageControl1.ActivePageIndex := 2;
  end;
  PnCadastroB.Visible := PageControl1.ActivePageIndex = 1;
end;

procedure TFmDiarioAdd2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MostraEdicao;
  //
  PageControl1.ActivePageIndex := 0;
  FLink2 := 0;
  FProcurador := 0;
  FNomeTipoPRC := '';
  UnDmkDAC_PF.AbreQuery(QrDiarioAss, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEnt1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFormContat, Dmod.MyDB);
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    QrCliInt.Close;
    QrCliInt.SQL.Clear;
    QrCliInt.SQL.Add('SELECT DISTINCT ent.Codigo, ent.CliInt Filial,');
    QrCliInt.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,');
    QrCliInt.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF');
    QrCliInt.SQL.Add('FROM entidades ent');
    QrCliInt.SQL.Add('WHERE ent.CliInt <> 0');
    //
  end else
  begin
    QrCliInt.Close;
    QrCliInt.SQL.Clear;
    QrCliInt.SQL.Add('SELECT DISTINCT ent.Codigo, ent.Filial,');
    QrCliInt.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,');
    QrCliInt.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF');
    QrCliInt.SQL.Add('FROM entidades ent');
    QrCliInt.SQL.Add('WHERE ent.Codigo < -10');
  end;
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDiarioOpc, Dmod.MyDB);
  //
  TPData.Date               := Date;
  TPData.Time               := Time;
  EdHora.ValueVariant       := Time;
  EdFormContat.ValueVariant := QrDiarioOpc.FieldByName('FrmCPreVda').AsInteger;
  CBFormContat.KeyValue     := QrDiarioOpc.FieldByName('FrmCPreVda').AsInteger;
end;

procedure TFmDiarioAdd2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDiarioAdd2.FormShow(Sender: TObject);
begin
  if EdCliInt.ValueVariant = 0 then
    EdCliInt.SetFocus
  else
  if EdEntidade1.ValueVariant = 0 then
    EdEntidade1.SetFocus
  else
  if EdTerceiro01.ValueVariant = 0 then
    EdTerceiro01.SetFocus
  else
  if EdDiarioAss.ValueVariant = 0 then
    EdDiarioAss.SetFocus
  else
    MeNome.SetFocus
end;

procedure TFmDiarioAdd2.Incluinovocontato1Click(Sender: TObject);
begin
  MostraEntiContat(stIns);
end;

procedure TFmDiarioAdd2.Incluinovotelefone1Click(Sender: TObject);
begin
  MostraEntiTel(stIns);
end;

procedure TFmDiarioAdd2.PageControl1Change(Sender: TObject);
begin
  PnCadastroB.Visible := PageControl1.ActivePageIndex = 1;
end;

procedure TFmDiarioAdd2.PMContatosPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita :=
    (QrEntiContat.State <> dsInactive) and (QrEntiContat.RecordCount > 0);
  AlteraContatoatual1.Enabled := Habilita;
  Habilita := Habilita and
    (QrEntiMail.State <> dsInactive) and (QrEntiMail.RecordCount = 0) and
    (QrEntiTel.State <> dsInactive) and (QrEntiTel.RecordCount = 0);
  Excluicontatos1.Enabled := Habilita;
end;

procedure TFmDiarioAdd2.PMEMailsPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEntiContat.State <> dsInactive) and (QrEntiContat.RecordCount > 0);
  NovoEMail1.Enabled := Habilita;
  Habilita := (QrEntiMail.State <> dsInactive) and (QrEntiMail.RecordCount > 0);
  Alteraemailselecionado1.Enabled := Habilita;
  Excluiemailselecionado1.Enabled := Habilita;
end;

procedure TFmDiarioAdd2.PMTelefonesPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEntiContat.State <> dsInactive) and (QrEntiContat.RecordCount > 0);
  Incluinovotelefone1.Enabled := Habilita;
  Habilita := (QrEntiTel.State <> dsInactive) and (QrEntiTel.RecordCount > 0);
  Alteratelefoneatual1.Enabled := Habilita;
  Excluitelefones1.Enabled := Habilita;
end;

procedure TFmDiarioAdd2.PreencheDados(DataHora: Boolean; Data: TDate; Hora:
  TTime; DiarioAss, CliInt, Entidade1, Entidade2: Integer; Texto: WideString);
begin
  if DataHora then
  begin
    TPData.Date := Data;
    EdHora.ValueVariant := Hora;
  end;
  if DiarioAss <> 0 then
  begin
    EdDiarioAss.ValueVariant := DiarioAss;
    CBDiarioAss.KeyValue := DiarioAss;
  end;
  if CliInt <> 0 then
  begin
    EdCliInt.ValueVariant := CliInt;
    CBCliInt.KeyValue := CliInt;
  end;
  if Entidade1 <> 0 then
  begin
    EdEntidade1.ValueVariant := Entidade1;
    CBEntidade1.KeyValue := Entidade1;
  end;
  if Entidade2 <> 0 then
  begin
    EdTerceiro01.ValueVariant := Entidade2;
    CBTerceiro01.KeyValue := Entidade2;
  end;
  if Texto <> '' then
    MeNome.Text := Texto;
end;

procedure TFmDiarioAdd2.QrEntAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiContat(0);
end;

procedure TFmDiarioAdd2.QrEntBeforeClose(DataSet: TDataSet);
begin
  QrEntiContat.Close();
end;

procedure TFmDiarioAdd2.QrEntCalcFields(DataSet: TDataSet);
begin
  QrEntTe1_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntTe1.Value);
  QrEntTe2_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntTe2.Value);
  QrEntTe3_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntTe3.Value);
  QrEntCel_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntCel.Value);
  QrEntFax_TXT.Value := Geral.FormataTelefone_TT_Curto(QrEntFax.Value);
end;

procedure TFmDiarioAdd2.QrEntiContatAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrEntiContat.RecordCount > 0;
  if Habilita then
    PageControl1.ActivePageIndex := 1
  else
    PageControl1.ActivePageIndex := 0;
  BtContatos.Enabled  := Habilita;
  BtTelefones.Enabled := Habilita;
  BtEMails.Enabled    := Habilita;
end;

procedure TFmDiarioAdd2.QrEntiContatAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiMail(0);
  ReopenEntiTel(0);
end;

procedure TFmDiarioAdd2.QrEntiContatBeforeClose(DataSet: TDataSet);
begin
  QrEntiTel.Close;
  QrEntiMail.Close;
  BtContatos.Enabled  := False;
  BtTelefones.Enabled := False;
  BtEMails.Enabled    := False;
end;

procedure TFmDiarioAdd2.QrEntiTelCalcFields(DataSet: TDataSet);
begin
  QrEntiTelTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntiTelTelefone.Value);
end;

procedure TFmDiarioAdd2.QrPrcAfterScroll(DataSet: TDataSet);
begin
  ReopenPrcContat(0);
end;

procedure TFmDiarioAdd2.QrPrcBeforeClose(DataSet: TDataSet);
begin
  QrPrcContat.Close();
end;

procedure TFmDiarioAdd2.QrPrcCalcFields(DataSet: TDataSet);
begin
  QrPrcTe1_TXT.Value := Geral.FormataTelefone_TT_Curto(QrPrcTe1.Value);
  QrPrcTe2_TXT.Value := Geral.FormataTelefone_TT_Curto(QrPrcTe2.Value);
  QrPrcTe3_TXT.Value := Geral.FormataTelefone_TT_Curto(QrPrcTe3.Value);
  QrPrcCel_TXT.Value := Geral.FormataTelefone_TT_Curto(QrPrcCel.Value);
  QrPrcFax_TXT.Value := Geral.FormataTelefone_TT_Curto(QrPrcFax.Value);
end;

procedure TFmDiarioAdd2.QrPrcContatAfterOpen(DataSet: TDataSet);
begin
{
  if QrEntiContat.RecordCount > 0 then
    PageControl1.ActivePageIndex := 1
  else
    PageControl1.ActivePageIndex := 0;
}
end;

procedure TFmDiarioAdd2.QrPrcContatAfterScroll(DataSet: TDataSet);
begin
  ReopenPrcMail(0);
  ReopenPrcTel(0);
end;

procedure TFmDiarioAdd2.QrPrcContatBeforeClose(DataSet: TDataSet);
begin
  QrPrcTel.Close;
  QrPrcMail.Close;
end;

procedure TFmDiarioAdd2.ReopenEntiContat(Controle: Integer);
begin
  QrEntiContat.Close;
  case RGEntidade.ItemIndex of
    0: QrEntiContat.Params[0].AsInteger := QrEnt1Codigo.Value;
    1: QrEntiContat.Params[0].AsInteger := QrEnt2Codigo.Value;
  end;
  UnDmkDAC_PF.AbreQuery(QrEntiContat, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrEntiContat.Locate('Controle', Controle, []);
end;

procedure TFmDiarioAdd2.ReopenEntidade();
var
  Entidade: Integer;
begin
  case RGEntidade.ItemIndex of
    0: Entidade := EdEntidade1.ValueVariant;
    1: Entidade := EdTerceiro01.ValueVariant;
    else Entidade := 0;
  end;
  QrEnt.Close;
  QrEnt.Params[0].AsInteger := Entidade;//EdEntidade.ValueVariant;
  UnDmkDAC_PF.AbreQuery(QrEnt, Dmod.MyDB);
end;

procedure TFmDiarioAdd2.ReopenEntiMail(Conta: Integer);
begin
  QrEntiMail.Close;
  QrEntiMail.Params[0].AsInteger := QrEntiContatControle.Value;
  UnDmkDAC_PF.AbreQuery(QrEntiMail, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrEntiMail.Locate('Conta', Conta, []);
end;

procedure TFmDiarioAdd2.ReopenEntiTel(Conta: Integer);
begin
  QrEntiTel.Close;
  QrEntiTel.Params[0].AsInteger := QrEntiContatControle.Value;
  UnDmkDAC_PF.AbreQuery(QrEntiTel, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrEntiTel.Locate('Conta', Conta, []);
end;

procedure TFmDiarioAdd2.ReopenInterloctr(Controle: Integer);
var
  //DiarioAdd,
  Entidade, Terceiro01, Localize: Integer;
  SQL1: String;
begin
  Localize := 0;
  if Controle <> 0 then
    Localize := Controle
  else
  if QrInterloctr.State <> dsInactive then
    Localize := QrInterloctrControle.Value;
  //
  QrInterloctr.Close;
  //DiarioAdd := EdCodigo.ValueVariant;
  Entidade := EdEntidade1.ValueVariant;
  Terceiro01 := EdTerceiro01.ValueVariant;
  if (*(DiarioAdd <> 0) or*) (Entidade <> 0) or (Terceiro01 <> 0) then
  begin
    SQL1 := '';
    if Entidade <> 0 then
      SQL1 := SQL1 + ' OR ece.Codigo=' + Geral.FF0(Entidade) + sLineBreak;
    if Terceiro01 <> 0 then
      SQL1 := SQL1 + ' OR ece.Codigo=' + Geral.FF0(Terceiro01) + sLineBreak;
    (*if DiarioAdd <> 0 then
      SQL1 := SQL1 +
      'OR ece.Controle IN (SELECT Controle FROM enticontat WHERE DiarioAdd=' +
      Geral.FF0(DiarioAdd) +')' + sLineBreak;
    *)
    UnDmkDAC_PF.AbreMySQLQuery0(QrInterloctr, Dmod.MyDB, [
    'SELECT eco.Controle, eco.Nome ',
    'FROM enticontat eco ',
    'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
    'WHERE ece.Codigo=-999999999 ',
    SQL1 +
    'ORDER BY eco.Nome ',
    '']);
    QrInterloctr.Locate('Controle', Localize, []);
  end;
end;

procedure TFmDiarioAdd2.ReopenPrcContat(Controle: Integer);
begin
  QrPrcContat.Close;
  QrPrcContat.Params[0].AsInteger := FProcurador;
  UnDmkDAC_PF.AbreQuery(QrPrcContat, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrPrcContat.Locate('Controle', Controle, []);
end;

procedure TFmDiarioAdd2.ReopenPrcMail(Conta: Integer);
begin
  QrPrcMail.Close;
  QrPrcMail.Params[0].AsInteger := QrPrcContatControle.Value;
  UnDmkDAC_PF.AbreQuery(QrPrcMail, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrPrcMail.Locate('Conta', Conta, []);
end;

procedure TFmDiarioAdd2.ReopenPrcTel(Conta: Integer);
begin
  QrPrcTel.Close;
  QrPrcTel.Params[0].AsInteger := QrPrcContatControle.Value;
  UnDmkDAC_PF.AbreQuery(QrPrcTel, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrPrcTel.Locate('Conta', Conta, []);
end;

procedure TFmDiarioAdd2.RGEntidadeClick(Sender: TObject);
var
  Cor: TColor;
begin
  ReopenEntidade();
  Cor := clGray;
  LaEntidade1.Font.Color := Cor;
  EdEntidade1.Font.Color := Cor;
  CBEntidade1.Font.Color := Cor;
  //
  LaEntidade2.Font.Color := Cor;
  EdTerceiro01.Font.Color := Cor;
  CBTerceiro01.Font.Color := Cor;
  //
  Cor := clRed;
  case RGEntidade.ItemIndex of
    0:
    begin
      LaEntidade1.Font.Color := Cor;
      EdEntidade1.Font.Color := Cor;
      CBEntidade1.Font.Color := Cor;
    end;
    1:
    begin
      LaEntidade2.Font.Color := Cor;
      EdTerceiro01.Font.Color := Cor;
      CBTerceiro01.Font.Color := Cor;
    end;
  end;
end;

procedure TFmDiarioAdd2.SbFormContatClick(Sender: TObject);
begin
  Diario_PF.MostraFormContat(QrFormContat.Database, QrFormContatNome.Size,
    EdFormContat, CBFormContat, QrFormContat);
end;

procedure TFmDiarioAdd2.SbInterloctrClick(Sender: TObject);
begin
 //
end;

procedure TFmDiarioAdd2.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmDiarioAss, FmDiarioAss, afmoNegarComAviso) then
  begin
    FmDiarioAss.ShowModal;
    FmDiarioAss.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodUsuDeCodigo(EdDiarioAss, CBDiarioAss, QrDiarioAss,
      VAR_CADASTRO, 'Codigo', 'CodUsu');
  end;
end;

procedure TFmDiarioAdd2.SpeedButton2Click(Sender: TObject);
begin
  if UpperCase(Application.Title) = 'CREDITOR' then
    EditaEntidade(0, 0)
  else
    EditaEntidade(0, 1);
end;

procedure TFmDiarioAdd2.SpeedButton3Click(Sender: TObject);
begin
  if UpperCase(Application.Title) = 'CREDITOR' then
    EditaEntidade(1, 0)
  else
    EditaEntidade(1, 1);
end;

procedure TFmDiarioAdd2.SpeedButton4Click(Sender: TObject);
var
  Entidade, Destino: Integer;
begin
  if DBCheck.CriaFm(TFmEntiRapido0, FmEntiRapido0, afmoNegarComAviso) then
  begin
    FmEntiRapido0.ShowModal;
    Entidade := FmEntiRapido0.FCodigo;
    FmEntiRapido0.Destroy;
    //
    if Entidade <> 0 then
    begin
      QrEnt1.Close;
      UnDmkDAC_PF.AbreQuery(QrEnt1, Dmod.MyDB);
      //
      QrEnt2.Close;
      UnDmkDAC_PF.AbreQuery(QrEnt2, Dmod.MyDB);
      //
      Destino := MyObjects.SelRadioGroup('Uso do cadastro rec�m feito',
      'Defina como deseja usar o cadastro rec�m feito',
      ['N�o quero usar', LaEntidade1.Caption, LaEntidade2.Caption], 1);
      case Destino of
        0: ; // nada
        1:
        begin
          EdEntidade1.ValueVariant := Entidade;
          CBEntidade1.KeyValue     := Entidade;
        end;
        2:
        begin
          EdTerceiro01.ValueVariant := Entidade;
          CBTerceiro01.KeyValue     := Entidade;
        end;
        else
          Geral.MB_Erro('Uso n�o implementado. Solicite � DERMATEK!');
      end;
    end;
  end;
end;

procedure TFmDiarioAdd2.Timer1Timer(Sender: TObject);
begin
  PnCadastroB.Visible := PageControl1.ActivePageIndex = 1;
end;

procedure TFmDiarioAdd2.MostraEdicao;
begin
  if UpperCase(Application.Title) = 'SYNDIC' then
  begin
    SpeedButton3.Visible := True;
    //
    RGEntidade.Items.Clear;
    RGEntidade.Items.Add('Contatado 1');
    RGEntidade.Items.Add('Contatado 2');
  end else
  if UpperCase(Application.Title) = 'CREDITOR' then
  begin
    SpeedButton3.Visible := False;
    //
    RGEntidade.Items.Clear;
    RGEntidade.Items.Add('Contatado 1');
  end;
  RGEntidade.ItemIndex := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrEnt2, Dmod.MyDB);
end;

procedure TFmDiarioAdd2.MostraEntiContat(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiContat, FmEntiContat, afmoNegarComAviso,
  QrEntiContat, SQLType) then
  begin
    FmEntiContat.FQrEntiContat := QrEntiContat;
    //DefineEntiSorc(FmEntiContat.FQrEntidades, FmEntiContat.FDsEntidades);
    DefineEntidades(FmEntiContat.FEntidadeOri, FmEntiContat.FEntidadeAtu);
{
    FmEntiContat.FEntidadeOri  := QrEntiContatCodigo.Value;
    FmEntiContat.FEntidadeAtu  := QrEntidadesCodigo.Value;
}
    FmEntiContat.ShowModal;
    FmEntiContat.Destroy;
  end;
end;

procedure TFmDiarioAdd2.MostraEntiMail(SQLType: TSQLType);
var
  DestDS: TDataSource;
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiMail, FmEntiMail, afmoNegarComAviso,
  QrEntiMail, SQLType) then
  begin
    FmEntiMail.FQrEntiMail := QrEntiMail;
    DefineEntiSorc(FmEntiMail.FQrEntidades, DestDS);
    FmEntiMail.FDsEntidades := DestDs;
    FmEntiMail.FQrEntiContat  := QrEntiContat;
    FmEntiMail.FDsEntiContat  := DsEntiContat;
    //
    FmEntiMail.DBEdCodigo.DataSource := DestDs;
    FmEntiMail.DBEdCodUsu.DataSource := DestDs;
    FmEntiMail.DBEdNome.DataSource := DestDs;
    //
    FmEntiMail.DBEdContatoCod.DataSource := DsEntiContat;
    FmEntiMail.DBEdContatoNom.DataSource := DsEntiContat;
    FmEntiMail.DBEdContatoCrg.DataSource := DsEntiContat;
    //
    FmEntiMail.ShowModal;
    FmEntiMail.Destroy;
  end;
end;

procedure TFmDiarioAdd2.MostraEntiTel(SQLType: TSQLType);
var
  DestDS: TDataSource;
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiTel, FmEntiTel, afmoNegarComAviso,
  QrEntiTel, SQLType) then
  begin
    FmEntiTel.FQrEntiTel := QrEntiTel;
    DefineEntiSorc(FmEntiTel.FQrEntidades, DestDS);
    FmEntiTel.FDsEntidades := DestDs;
    FmEntiTel.FQrEntiContat  := QrEntiContat;
    FmEntiTel.FDsEntiContat  := DsEntiContat;
    //
    FmEntiTel.DBEdCodigo.DataSource := DestDs;
    FmEntiTel.DBEdCodUsu.DataSource := DestDs;
    FmEntiTel.DBEdNome.DataSource := DestDs;
    //
    FmEntiTel.DBEdContatoCod.DataSource := DsEntiContat;
    FmEntiTel.DBEdContatoNom.DataSource := DsEntiContat;
    FmEntiTel.DBEdContatoCrg.DataSource := DsEntiContat;
    //
    FmEntiTel.ShowModal;
    FmEntiTel.Destroy;
  end;
end;

procedure TFmDiarioAdd2.NovoEMail1Click(Sender: TObject);
begin
  MostraEntiMail(stIns);
end;

end.
