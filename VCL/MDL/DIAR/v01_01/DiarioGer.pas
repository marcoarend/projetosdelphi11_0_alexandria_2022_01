unit DiarioGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, Grids, DBGrids, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, Mask,
  ComCtrls, dmkEditDateTimePicker, frxClass, frxDBSet, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmDiarioGer = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    PnFiltro: TPanel;
    Label1: TLabel;
    LaCliInt: TLabel;
    Label3: TLabel;
    EdDiarioAss: TdmkEditCB;
    CBDiarioAss: TdmkDBLookupComboBox;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    PnDeptos: TPanel;
    DBMemo1: TDBMemo;
    Panel4: TPanel;
    QrDiarioAss: TmySQLQuery;
    QrDiarioAssCodigo: TIntegerField;
    QrDiarioAssCodUsu: TIntegerField;
    QrDiarioAssNome: TWideStringField;
    QrDiarioAssAplicacao: TIntegerField;
    DsDiarioAss: TDataSource;
    QrDeptos: TmySQLQuery;
    IntegerField2: TIntegerField;
    QrDeptosNome: TWideStringField;
    DsDeptos: TDataSource;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOME_ENT: TWideStringField;
    DsEntidades: TDataSource;
    Label2: TLabel;
    Label4: TLabel;
    LaDepto2: TLabel;
    QrDiarioAdd: TmySQLQuery;
    DsDiarioAdd: TDataSource;
    QrDiarioAddNOME_CLI: TWideStringField;
    QrDiarioAddNOME_ENT: TWideStringField;
    QrDiarioAddNOME_DEPTO: TWideStringField;
    QrDiarioAddCodigo: TIntegerField;
    QrDiarioAddNome: TWideStringField;
    QrDiarioAddDiarioAss: TIntegerField;
    QrDiarioAddEntidade: TIntegerField;
    QrDiarioAddCliInt: TIntegerField;
    QrDiarioAddDepto: TIntegerField;
    QrDiarioAddData: TDateField;
    QrDiarioAddHora: TTimeField;
    QrDiarioAddDataCad: TDateField;
    QrDiarioAddDataAlt: TDateField;
    QrDiarioAddUserCad: TIntegerField;
    QrDiarioAddUserAlt: TIntegerField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label7: TLabel;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    Label9: TLabel;
    DBEdit7: TDBEdit;
    PnDepto: TPanel;
    GroupBox2: TGroupBox;
    LaDepto1: TLabel;
    EdDepto: TdmkEditCB;
    CBDepto: TdmkDBLookupComboBox;
    Label10: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    frxGER_DIARI_001_01: TfrxReport;
    frxDsDiarioAdd: TfrxDBDataset;
    QrDiarioAddNOME_UH_ENT: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    QrCliIntFilial: TIntegerField;
    QrCliIntNOMEFILIAL: TWideStringField;
    QrCliIntCNPJ_CPF: TWideStringField;
    Panel5: TPanel;
    Panel6: TPanel;
    DBGrid1: TDBGrid;
    RGOrderBy: TRadioGroup;
    EdNome: TEdit;
    Label11: TLabel;
    EdLctCtrl: TEdit;
    LaLctCtrl: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel8: TPanel;
    BtPesquisa: TBitBtn;
    BtImprime: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdDiarioAssChange(Sender: TObject);
    procedure EdCliIntChange(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure EdNomeChange(Sender: TObject);
    procedure TPDataIniChange(Sender: TObject);
    procedure TPDataIniClick(Sender: TObject);
    procedure TPDataFimChange(Sender: TObject);
    procedure TPDataFimClick(Sender: TObject);
    procedure EdDeptoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCliIntExit(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure frxGER_DIARI_001_01GetValue(const VarName: string; var Value: Variant);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrDiarioAddAfterOpen(DataSet: TDataSet);
    procedure QrDiarioAddBeforeClose(DataSet: TDataSet);
    procedure RGOrderByClick(Sender: TObject);
  private
    { Private declarations }
    procedure FechaPesquisa();
  public
    { Public declarations }
    FCliInt: Integer;
    procedure ReabreDeptos();
    procedure ReopenDiario(Codigo: Integer);
  end;

  var
  FmDiarioGer: TFmDiarioGer;

implementation

uses UMySQLModule, UnMyObjects;

{$R *.DFM}

procedure TFmDiarioGer.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxGER_DIARI_001_01, 'Pesquisa de Di�rio');
end;

procedure TFmDiarioGer.BtPesquisaClick(Sender: TObject);
begin
  ReopenDiario(0);
end;

procedure TFmDiarioGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDiarioGer.EdCliIntChange(Sender: TObject);
begin
  FechaPesquisa();
  if not EdCliInt.Focused then
    ReabreDeptos();
end;

procedure TFmDiarioGer.EdCliIntExit(Sender: TObject);
begin
  if FCliInt <> EdCliInt.ValueVariant then
  begin
    FCliInt := QrCliIntCodigo.Value;
    ReabreDeptos();
  end;
end;

procedure TFmDiarioGer.EdDeptoChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer.EdDiarioAssChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer.EdEntidadeChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer.EdNomeChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer.FechaPesquisa;
begin
  QrDiarioAdd.Close;
  BtImprime.Enabled := False;
end;

procedure TFmDiarioGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmDiarioGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TPDataIni.Date := 0;
  TPDataFim.Date := Date;
  //
  QrDiarioAss.Open;
  QrEntidades.Open;
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    QrCliInt.Close;
    QrCliInt.SQL.Clear;
    QrCliInt.SQL.Add('SELECT DISTINCT ent.Codigo, ent.CliInt Filial,');
    QrCliInt.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,');
    QrCliInt.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF');
    QrCliInt.SQL.Add('FROM entidades ent');
    QrCliInt.SQL.Add('WHERE ent.CliInt <> 0');
    //
    QrDeptos.Close;
    QrDeptos.SQL.Clear;
    QrDeptos.SQL.Add('SELECT Conta Codigo, Unidade Nome ');
    QrDeptos.SQL.Add('FROM condimov ');
    QrDeptos.SQL.Add('WHERE Codigo=:P0');
    //
    LaDepto1.Caption := 'Unidade';
    LaDepto2.Caption := 'Unidade';
    PnDeptos.Visible  := True;
    LaLctCtrl.Visible := False;
    EdLctCtrl.Visible := False;
    EdNome.Width      := 612;
  end else
  begin
    QrCliInt.Close;
    QrCliInt.SQL.Clear;
    QrCliInt.SQL.Add('SELECT DISTINCT ent.Codigo, ent.Filial,');
    QrCliInt.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,');
    QrCliInt.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF');
    QrCliInt.SQL.Add('FROM entidades ent');
    QrCliInt.SQL.Add('WHERE ent.Codigo < -10');
    PnDeptos.Visible  := False;
    LaLctCtrl.Visible := True;
    EdLctCtrl.Visible := True;
    EdNome.Width      := 505;
  end;
  QrCliInt.Open;
end;

procedure TFmDiarioGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDiarioGer.frxGER_DIARI_001_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_PERIODO' then Value := dmkPF.PeriodoImp(TPDataIni.Date,
    TPDataFim.Date, 0, 0, True, True, False, False, '', '')
  else
  if VarName = 'VARF_EMPRESA' then
    Value := CBCliInt.Text;
end;

procedure TFmDiarioGer.QrDiarioAddAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := QrDiarioAdd.RecordCount > 0;
end;

procedure TFmDiarioGer.QrDiarioAddBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDiarioGer.ReabreDeptos();
var
  Codigo: Integer;
begin
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    EdDepto.ValueVariant := 0;
    CBDepto.KeyValue     := 0;
    //
    Codigo := QrCliIntFilial.Value;
    QrDeptos.Close;
    QrDeptos.Params[0].AsInteger := Codigo;
    QrDeptos.Open;
    //
    PnDeptos.Visible  := True;
    LaLctCtrl.Visible := False;
    EdLctCtrl.Visible := False;
    EdNome.Width      := 612;
  end else
  begin
    PnDeptos.Visible  := False;
    LaLctCtrl.Visible := True;
    EdLctCtrl.Visible := True;
    EdNome.Width      := 505;
  end;
end;

procedure TFmDiarioGer.ReopenDiario(Codigo: Integer);
var
  Empresa, DiarioAss, Entidade, Depto: Integer;
  Nome: String;
begin
  Screen.Cursor := crHourGlass;
  try
    UmyMod.ObtemCodigoDeCodUsu(EdDiarioAss, DiarioAss, '');
    DiarioAss := EdDiarioAss.ValueVariant;
    Empresa   := QrCliIntCodigo.Value;
    Entidade  := EdEntidade.ValueVariant;
    Depto     := EdDepto.ValueVariant;
    Nome      := EdNome.Text;
    //
    QrDiarioAdd.Close;
    QrDiarioAdd.SQL.Clear;
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      QrDiarioAdd.SQL.Add('SELECT CONCAT(dpt.Unidade, " - ",');
      QrDiarioAdd.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NOME_UH_ENT,');
      QrDiarioAdd.SQL.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLI,');
      QrDiarioAdd.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_ENT,');
      QrDiarioAdd.SQL.Add('dpt.Unidade NOME_DEPTO, dad.*');
      QrDiarioAdd.SQL.Add('FROM diarioadd dad');
      QrDiarioAdd.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=dad.CliInt');
      QrDiarioAdd.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=dad.Entidade');
      QrDiarioAdd.SQL.Add('LEFT JOIN condimov  dpt ON dpt.Conta =dad.Depto');
      QrDiarioAdd.SQL.Add(dmkPF.SQL_Periodo('WHERE dad.Data ', TPDataIni.Date,
        TPDataFim.Date, True, True));
    end else if UpperCase(Application.Title) = 'CREDITOR' then
    begin
      QrDiarioAdd.SQL.Add('SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_UH_ENT,');
      QrDiarioAdd.SQL.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLI,');
      QrDiarioAdd.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_ENT,');
      QrDiarioAdd.SQL.Add('"" NOME_DEPTO, dad.*');
      QrDiarioAdd.SQL.Add('FROM diarioadd dad');
      QrDiarioAdd.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=dad.CliInt');
      QrDiarioAdd.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=dad.Entidade');
      QrDiarioAdd.SQL.Add(dmkPF.SQL_Periodo('WHERE dad.Data ', TPDataIni.Date,
        TPDataFim.Date, True, True));
      if Length(EdLctCtrl.Text) > 0 then
        QrDiarioAdd.SQL.Add('AND dad.Depto = ' + EdLctCtrl.Text);
    end else begin
      Geral.MB_Erro('Aplicativo sem defini��o para o procedimento:' + sLineBreak +
      '"FmDiarioGer.ReopenDiario()"');
      Exit;
    end;
    if DiarioAss <> 0 then
      QrDiarioAdd.SQL.Add('AND dad.DiarioAss = ' + FormatFloat('0', DiarioAss));
    if Empresa <> 0 then
      QrDiarioAdd.SQL.Add('AND dad.CliInt = ' + FormatFloat('0', Empresa));
    if Entidade <> 0 then
      QrDiarioAdd.SQL.Add('AND dad.Entidade = ' + FormatFloat('0', Entidade));
    if Depto <> 0 then
      QrDiarioAdd.SQL.Add('AND dad.Depto = ' + FormatFloat('0', Depto));
    if Nome <> '' then
      QrDiarioAdd.SQL.Add('AND dad.Nome LIKE "%' + Nome + '%"');
    //
    case RGOrderBy.ItemIndex of
      0: QrDiarioAdd.SQL.Add('ORDER BY dad.Data, dad.Hora, NOME_UH_ENT');
      1: QrDiarioAdd.SQL.Add('ORDER BY NOME_UH_ENT, dad.Data, dad.Hora');
    end;
    QrDiarioAdd.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDiarioGer.RGOrderByClick(Sender: TObject);
begin
  if QrDiarioAdd.State <> dsInactive then
    ReopenDiario(0)
  else
    FechaPesquisa;
end;

procedure TFmDiarioGer.TPDataFimChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer.TPDataFimClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer.TPDataIniChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmDiarioGer.TPDataIniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

end.
