unit DiarioOpc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables;

type
  TFmDiarioOpc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label98: TLabel;
    EdFrmCPreVda: TdmkEditCB;
    CBFrmCPreVda: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    QrFormContat: TmySQLQuery;
    QrFormContatCodigo: TIntegerField;
    QrFormContatNome: TWideStringField;
    DsFormContat: TDataSource;
    QrDiarioOpc: TmySQLQuery;
    DsDiarioOpc: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmDiarioOpc: TFmDiarioOpc;

implementation

uses UnMyObjects, Module, UnDiario_PF, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmDiarioOpc.BtOKClick(Sender: TObject);
var
  Codigo, FrmCPreVda: Integer;
begin
  Codigo     := QrDiarioOpc.FieldByName('Codigo').AsInteger;
  FrmCPreVda := EdFrmCPreVda.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'DiarioOpc', False,
    ['FrmCPreVda'], ['Codigo'], [FrmCPreVda], [Codigo], True)
  then
    Close;
end;

procedure TFmDiarioOpc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDiarioOpc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDiarioOpc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrFormContat, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDiarioOpc, Dmod.MyDB);
  //
  EdFrmCPreVda.ValueVariant := QrDiarioOpc.FieldByName('FrmCPreVda').AsInteger;
  CBFrmCPreVda.KeyValue     := QrDiarioOpc.FieldByName('FrmCPreVda').AsInteger;
end;

procedure TFmDiarioOpc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDiarioOpc.SpeedButton1Click(Sender: TObject);
begin
  Diario_PF.MostraFormContat(QrFormContat.Database, QrFormContatNome.Size,
    EdFrmCPreVda, CBFrmCPreVda, QrFormContat);
end;

end.
