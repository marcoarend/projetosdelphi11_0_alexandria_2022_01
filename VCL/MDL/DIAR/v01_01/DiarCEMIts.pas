unit DiarCEMIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, dmkRadioGroup, UnDmkEnums;

type
  TFmDiarCEMIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdDias: TdmkEdit;
    Label7: TLabel;
    CBSMS: TdmkDBLookupComboBox;
    EdSMS: TdmkEditCB;
    Label1: TLabel;
    SBSMS: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrTxtSMS: TmySQLQuery;
    DsTxtSMS: TDataSource;
    QrPreEmail: TmySQLQuery;
    DsPreEmail: TDataSource;
    QrCartas: TmySQLQuery;
    DsCartas: TDataSource;
    QrTxtSMSCodigo: TIntegerField;
    QrTxtSMSNome: TWideStringField;
    QrPreEmailCodigo: TIntegerField;
    QrPreEmailNome: TWideStringField;
    QrCartasCodigo: TIntegerField;
    QrCartasTitulo: TWideStringField;
    Label9: TLabel;
    EdEmail: TdmkEditCB;
    CBEmail: TdmkDBLookupComboBox;
    SBEmail: TSpeedButton;
    Label10: TLabel;
    EdCarta: TdmkEditCB;
    CBCarta: TdmkDBLookupComboBox;
    SBCarta: TSpeedButton;
    Lainfo1A: TLabel;
    Lainfo1B: TLabel;
    Label2: TLabel;
    EdDdNoEnv: TdmkEdit;
    Label4: TLabel;
    Label8: TLabel;
    LaInfo2A: TLabel;
    LAInfo2B: TLabel;
    RGCartaoAR: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBSMSClick(Sender: TObject);
    procedure SBEmailClick(Sender: TObject);
    procedure SBCartaClick(Sender: TObject);
  private
    { Private declarations }
    //procedure Reopen_SorceSel_(Controle: Integer);
    procedure ReopenDiarCemIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmDiarCEMIts: TFmDiarCEMIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  Principal;

{$R *.DFM}

procedure TFmDiarCEMIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Dias, SMS, Email, Carta, DdNoEnv, CartaoAR: Integer;
begin
  Codigo   := Geral.IMV(DBEdCodigo.Text);
  Dias     := EdDias.ValueVariant;
  SMS      := EdSMS.ValueVariant;
  Email    := EdEmail.ValueVariant;
  Carta    := EdCarta.ValueVariant;
  DdNoEnv  := EdDdNoEnv.ValueVariant;
  CartaoAR := RGCartaoAR.ItemIndex;

  Controle := EdControle.ValueVariant;
  Controle := UMyMod.BPGS1I32('diarcemits', 'Controle', '', '',
    tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'diarcemits', False, [
  'Codigo', 'Dias', 'SMS',
  'Email', 'Carta', 'DdNoEnv',
  'CartaoAR'], [
  'Controle'], [
  Codigo, Dias, SMS,
  Email, Carta, DdNoEnv,
  CartaoAR], [
  Controle], True) then
  begin
    ReopenDiarCemIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;

      EdControle.ValueVariant  := 0;
      EdDias.ValueVariant      := 0;

      EdSMS.ValueVariant       := 0;
      CBSMS.KeyValue           := Null;

      EdEmail.ValueVariant     := 0;
      CBEmail.KeyValue         := Null;

      EdCarta.ValueVariant     := 0;
      CBCarta.KeyValue         := Null;
      //
      EdDias.SetFocus;
    end else Close;
  end;
end;

procedure TFmDiarCEMIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDiarCEMIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmDiarCEMIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrTxtSMS, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPreEmail, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCartas, Dmod.MyDB);
end;

procedure TFmDiarCEMIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDiarCEMIts.ReopenDiarCemIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmDiarCEMIts.SBCartaClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormCarta_App();
  UMyMod.SetaCodigoPesquisado(EdCarta, CBCarta, QrCartas, VAR_CADASTRO);
end;

procedure TFmDiarCEMIts.SBEmailClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormPreEmail();
  UMyMod.SetaCodigoPesquisado(EdEmail, CBEmail, QrPreEmail, VAR_CADASTRO);
end;

procedure TFmDiarCEMIts.SBSMSClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormTxtSMS();
  UMyMod.SetaCodigoPesquisado(EdSMS, CBSMS, QrTxtSMS, VAR_CADASTRO);
end;

end.
