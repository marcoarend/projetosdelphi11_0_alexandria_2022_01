object FmServTarifas: TFmServTarifas
  Left = 368
  Top = 194
  Caption = 'SRV-TARIF-001 :: Cadastro de Tarifas de Servi'#231'os'
  ClientHeight = 412
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 316
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitHeight = 390
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 240
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label17: TLabel
        Left = 16
        Top = 60
        Width = 53
        Height = 13
        Caption = 'Valor base:'
      end
      object Label3: TLabel
        Left = 15
        Top = 105
        Width = 117
        Height = 13
        Caption = 'Valor cobrado (por hora):'
      end
      object Label4: TLabel
        Left = 16
        Top = 150
        Width = 126
        Height = 13
        Caption = 'Dura'#231#227'o m'#237'nima (minutos):'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 350
        Height = 21
        MaxLength = 100
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdValor: TdmkEdit
        Left = 16
        Top = 77
        Width = 150
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'Valor'
        UpdCampo = 'Valor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdValorTempo: TdmkEdit
        Left = 15
        Top = 122
        Width = 150
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'ValorTempo'
        UpdCampo = 'ValorTempo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdDuracaoMin: TdmkEdit
        Left = 16
        Top = 167
        Width = 150
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'DuracaoMin'
        UpdCampo = 'DuracaoMin'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object GroupBox1: TGroupBox
        Left = 181
        Top = 60
        Width = 245
        Height = 128
        Caption = 'Qtde m'#225'xima isenta de cobran'#231'a no per'#237'odo '
        TabOrder = 5
        object Label5: TLabel
          Left = 15
          Top = 25
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object Label6: TLabel
          Left = 15
          Top = 73
          Width = 69
          Height = 13
          Caption = 'Per'#237'odo (dias):'
        end
        object EdQtdIsento: TdmkEdit
          Left = 15
          Top = 42
          Width = 150
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'QtdIsento'
          UpdCampo = 'QtdIsento'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPeriodoIsento: TdmkEdit
          Left = 15
          Top = 90
          Width = 150
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'PeriodoIsento'
          UpdCampo = 'PeriodoIsento'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object CkSemValor: TdmkCheckBox
        Left = 16
        Top = 194
        Width = 250
        Height = 17
        Caption = 'Permite isen'#231#227'o do valor caso necess'#225'rio'
        TabOrder = 6
        QryCampo = 'SemValor'
        UpdCampo = 'SemValor'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object CkAtivo: TdmkCheckBox
        Left = 16
        Top = 215
        Width = 250
        Height = 17
        Caption = 'Ativo'
        Checked = True
        State = cbChecked
        TabOrder = 7
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 253
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 327
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 316
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitHeight = 390
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 240
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label11: TLabel
        Left = 16
        Top = 60
        Width = 53
        Height = 13
        Caption = 'Valor base:'
      end
      object Label12: TLabel
        Left = 15
        Top = 105
        Width = 117
        Height = 13
        Caption = 'Valor cobrado (por hora):'
      end
      object Label13: TLabel
        Left = 16
        Top = 150
        Width = 126
        Height = 13
        Caption = 'Dura'#231#227'o m'#237'nima (minutos):'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 15
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsSrvTarifa
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 350
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsSrvTarifa
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object GroupBox2: TGroupBox
        Left = 181
        Top = 60
        Width = 245
        Height = 128
        Caption = 'Qtde m'#225'xima isenta de cobran'#231'a no per'#237'odo '
        TabOrder = 5
        object Label8: TLabel
          Left = 15
          Top = 25
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object Label10: TLabel
          Left = 15
          Top = 73
          Width = 69
          Height = 13
          Caption = 'Per'#237'odo (dias):'
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 15
          Top = 42
          Width = 150
          Height = 21
          DataField = 'QtdIsento'
          DataSource = DsSrvTarifa
          TabOrder = 0
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit2: TdmkDBEdit
          Left = 15
          Top = 90
          Width = 150
          Height = 21
          DataField = 'PeriodoIsento'
          DataSource = DsSrvTarifa
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 16
        Top = 77
        Width = 150
        Height = 21
        DataField = 'Valor'
        DataSource = DsSrvTarifa
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit4: TdmkDBEdit
        Left = 16
        Top = 122
        Width = 150
        Height = 21
        DataField = 'ValorTempo'
        DataSource = DsSrvTarifa
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit5: TdmkDBEdit
        Left = 16
        Top = 167
        Width = 150
        Height = 21
        DataField = 'DuracaoMin'
        DataSource = DsSrvTarifa
        TabOrder = 4
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBCheckBox1: TDBCheckBox
        Left = 15
        Top = 194
        Width = 250
        Height = 17
        Caption = 'Permite isen'#231#227'o do valor caso necess'#225'rio'
        DataField = 'SemValor'
        DataSource = DsSrvTarifa
        TabOrder = 6
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBCheckBox2: TDBCheckBox
        Left = 15
        Top = 215
        Width = 250
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsSrvTarifa
        TabOrder = 7
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 252
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 326
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 386
        Height = 32
        Caption = 'Cadastro de Tarifas de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 386
        Height = 32
        Caption = 'Cadastro de Tarifas de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 386
        Height = 32
        Caption = 'Cadastro de Tarifas de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrSrvTarifa: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrSrvTarifaBeforeOpen
    AfterOpen = QrSrvTarifaAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM cadastro_simples')
    Left = 584
    Top = 56
    object QrSrvTarifaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvTarifaNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSrvTarifaValor: TFloatField
      FieldName = 'Valor'
      currency = True
    end
    object QrSrvTarifaValorTempo: TFloatField
      FieldName = 'ValorTempo'
      currency = True
    end
    object QrSrvTarifaDuracaoMin: TIntegerField
      FieldName = 'DuracaoMin'
    end
    object QrSrvTarifaQtdIsento: TIntegerField
      FieldName = 'QtdIsento'
    end
    object QrSrvTarifaPeriodoIsento: TIntegerField
      FieldName = 'PeriodoIsento'
    end
    object QrSrvTarifaSemValor: TIntegerField
      FieldName = 'SemValor'
    end
    object QrSrvTarifaAtivo: TIntegerField
      FieldName = 'Ativo'
    end
  end
  object DsSrvTarifa: TDataSource
    DataSet = QrSrvTarifa
    Left = 612
    Top = 56
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 640
    Top = 56
  end
end
