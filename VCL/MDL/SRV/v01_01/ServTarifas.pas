unit ServTarifas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkCheckBox;

type
  TFmServTarifas = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrSrvTarifa: TmySQLQuery;
    DsSrvTarifa: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrSrvTarifaCodigo: TIntegerField;
    QrSrvTarifaNome: TWideStringField;
    QrSrvTarifaValor: TFloatField;
    QrSrvTarifaValorTempo: TFloatField;
    QrSrvTarifaDuracaoMin: TIntegerField;
    QrSrvTarifaQtdIsento: TIntegerField;
    QrSrvTarifaPeriodoIsento: TIntegerField;
    QrSrvTarifaSemValor: TIntegerField;
    Label17: TLabel;
    EdValor: TdmkEdit;
    EdValorTempo: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    EdDuracaoMin: TdmkEdit;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    EdQtdIsento: TdmkEdit;
    EdPeriodoIsento: TdmkEdit;
    Label6: TLabel;
    CkSemValor: TdmkCheckBox;
    CkAtivo: TdmkCheckBox;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    QrSrvTarifaAtivo: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSrvTarifaAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSrvTarifaBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmServTarifas: TFmServTarifas;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmServTarifas.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmServTarifas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSrvTarifaCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmServTarifas.DefParams;
begin
  VAR_GOTOTABELA := 'srvtarifa';
  VAR_GOTOMYSQLTABLE := QrSrvTarifa;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM srvtarifa');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmServTarifas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmServTarifas.QueryPrincipalAfterOpen;
begin
end;

procedure TFmServTarifas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmServTarifas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmServTarifas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmServTarifas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmServTarifas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmServTarifas.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmServTarifas.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrSrvTarifa, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'srvtarifa');
end;

procedure TFmServTarifas.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSrvTarifaCodigo.Value;
  Close;
end;

procedure TFmServTarifas.BtConfirmaClick(Sender: TObject);
var
  Codigo, QtdIsento, PeriodoIsento: Integer;
  Nome: String;
begin
  Nome          := EdNome.ValueVariant;
  QtdIsento     := EdQtdIsento.ValueVariant;
  PeriodoIsento := EdPeriodoIsento.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  if (QtdIsento <> 0) or (PeriodoIsento <> 0) then
  begin
    if MyObjects.FIC(QtdIsento = 0, EdQtdIsento, 'Definia a quantidade!') then Exit;
    if MyObjects.FIC(PeriodoIsento = 0, EdPeriodoIsento, 'Defina o per�odo!') then Exit;
  end;
  //
  Codigo := UMyMod.BPGS1I32('srvtarifa', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrSrvTarifaCodigo.Value);
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'srvtarifa', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmServTarifas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'srvtarifa', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmServTarifas.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrSrvTarifa, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'srvtarifa');
end;

procedure TFmServTarifas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmServTarifas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSrvTarifaCodigo.Value, LaRegistro.Caption);
end;

procedure TFmServTarifas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmServTarifas.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrSrvTarifaCodigo.Value, LaRegistro.Caption);
end;

procedure TFmServTarifas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmServTarifas.QrSrvTarifaAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmServTarifas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmServTarifas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSrvTarifaCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'srvtarifa', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmServTarifas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmServTarifas.QrSrvTarifaBeforeOpen(DataSet: TDataSet);
begin
  QrSrvTarifaCodigo.DisplayFormat := FFormatFloat;
end;

end.

