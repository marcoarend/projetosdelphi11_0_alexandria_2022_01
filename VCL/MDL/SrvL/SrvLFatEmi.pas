unit SrvLFatEmi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, dmkDBEdit, dmkValUsu, UnDmkEnums,
  UnProjGroup_Consts;

type
  TFmSrvLFatEmi = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    PnEdita: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdPagante: TDBEdit;
    DBEdNO_PAG: TDBEdit;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    CBCondicaoPG: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    Label21: TLabel;
    TPDataFat: TdmkEditDateTimePicker;
    EdHoraFat: TdmkEdit;
    EdValLiqToFat: TdmkEdit;
    Label2: TLabel;
    Label10: TLabel;
    EdValOutToFat: TdmkEdit;
    Label11: TLabel;
    EdValDesToFat: TdmkEdit;
    Label12: TLabel;
    EdValLiqOkFat: TdmkEdit;
    Label7: TLabel;
    EdCartEmis: TdmkEditCB;
    CBCartEmis: TdmkDBLookupComboBox;
    VuCondicaoPG: TdmkValUsu;
    EdNumNF: TdmkEdit;
    Label13: TLabel;
    EdSerNF: TdmkEdit;
    Label14: TLabel;
    Label8: TLabel;
    DBEdEmpresa: TDBEdit;
    Label9: TLabel;
    DBEdNO_EMP: TDBEdit;
    SbNFSe: TSpeedButton;
    Label15: TLabel;
    EdGenero: TdmkEditCB;
    CBGenero: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdValLiqToFatChange(Sender: TObject);
    procedure EdValConsuChange(Sender: TObject);
    procedure EdValUsadoChange(Sender: TObject);
    procedure EdValOutToFatChange(Sender: TObject);
    procedure EdValDesToFatChange(Sender: TObject);
    procedure EdNumNFChange(Sender: TObject);
    procedure SbNFSeClick(Sender: TObject);
    procedure DBEdEmpresaChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaValores();
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FCliente: Integer;
    FValAFat: Double;
    FDataAbriu: TDateTime;
    FEncerra: Boolean;
    FFatID: Integer; // ID Faturamento parcial e total???
  end;

  var
  FmSrvLFatEmi: TFmSrvLFatEmi;

implementation

uses UnMyObjects, Module, ModuleFatura, UMySQLModule, NFSe_PF_0201, UnGFat_Jan,
  ModuleGeral(*, UnBugs_Tabs, ModAgenda*);

{$R *.DFM}

procedure TFmSrvLFatEmi.BtOKClick(Sender: TObject);
const
  TipoFatura = tfatServico; {TTipoFatura}
  FaturaDta = dfEncerramento; {TDataFatura}
  Financeiro = tfinCred; {TTipoFinanceiro}
  //
  // N�o usa ainda
  Represen = 0;
var
  DtaFimFat, SerNF: String;
  Codigo, Estatus, CondicaoPg, CartEmis, Genero, NumNF: Integer;
  ValLiqOkFat, (*ValLiqToFat, *)ValDesToFat, ValOutToFat: Double;
  //
  Filial, Entidade, FatNum, Cliente, IDDuplicata, NumeroNF, TipoCart: Integer;
  DataAbriu, DataEncer: TDateTime;
  SerieNF: String;
  ValTotal: Double;
  //
  function AlteraOSCab(): Boolean;
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlfatcab', False, [
    'Estatus', 'ValLiqOkFat', (*'ValLiqToFat',*)
    'ValDesToFat', 'DtaFimFat', 'ValOutToFat',
    'CondicaoPg', 'CartEmis', 'Genero', 'SerNF', 'NumNF'], [
    'Codigo'], [
    Estatus, ValLiqOkFat, (*ValLiqToFat,*)
    ValDesToFat, DtaFimFat, ValOutToFat,
    CondicaoPg, CartEmis, Genero, SerNF, NumNF], [
    Codigo], True);
  end;
var
  QuestaoTyp: TAgendaQuestao;
  QuestaoCod: Integer;
begin
  Codigo         := Geral.IMV(Geral.SoNumero_TT(DBEdCodigo.Text));
  Estatus        := CO_BUG_STATUS_0800_FATURADO;
  ValLiqOkFat    := EdValLiqOkFat.ValueVariant;
  //ValLiqToFat    := EdValLiqToFat.ValueVariant;
  ValDesToFat    := EdValDesToFat.ValueVariant;
  DtaFimFat      := Geral.FDT(TPDataFat.Date, 1) + ' ' + EdHoraFat.Text;
  ValOutToFat    := EdValOutToFat.ValueVariant;
  CondicaoPg     := EdCondicaoPG.ValueVariant;
  CartEmis       := EdCartEmis.ValueVariant;
  Genero         := EdGenero.ValueVariant;
  SerNF          := EdSerNF.Text;
  NumNF          := EdNumNF.ValueVariant;
  //
  if MyObjects.FIC(CondicaoPG = 0, EdCondicaoPG, 'Informe a condi��o de pagamento!') then Exit;
  if MyObjects.FIC(CartEmis = 0, EdCartEmis, 'Informe a carteira!') then Exit;
  if MyObjects.FIC(Genero = 0, EdGenero, 'Informe a conta (do plano de contas)!') then Exit;
  if ValLiqOkFat < 0.01 then
  begin
    if FEncerra then
    begin
      if Geral.MB_Pergunta('Valor inv�lido para faturamento!' + sLineBreak +
      'Deseja encerrar assim mesmo?') <> ID_YES then
        Exit;
    end else
    begin
      Geral.MB_Aviso('Valor inv�lido para faturamento!');
      Exit;
    end;
  end;
  if (FValAFat > 0) and (ValLiqOkFat > FValAFat) then
  begin
    if Geral.MB_Pergunta('O valor preenchido � maior que o valor a ser faturado!' +
      sLineBreak + 'Deseja continuar?') <> ID_YES then
    begin
      EdValLiqToFat.SetFocus;
      Exit;
    end;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    if AlteraOSCab() then
    begin
      // N�o h� faturamento parcial: Controle e c�digo s�o o mesmo n�mero
      FatNum      := Codigo;
      IDDuplicata := Codigo;
      // FIM N�o h� faturamento parcial
      Filial      := Geral.IMV(Geral.SoNumeroESinal_TT(DBEdEmpresa.Text));
      Entidade    := DModG.ObtemEntidadeDeFilial(Filial);
      Cliente     := Geral.IMV(Geral.SoNumeroESinal_TT(DBEdPagante.Text));
      DataAbriu   := FDataAbriu;
      // 2013-06-18
      //DataEncer   := Now();
      DataEncer   := TPDataFat.Date;
      // FIM  2013-06-18
      NumeroNF    := NumNF;
      SerieNF     := SerNF;
      TipoCart    := DmFatura.QrCartEmisTipo.Value;
      ValTotal    := ValLiqOkFat;
      //
      DmFatura.EmiteFaturas(Codigo, FFatID, Entidade, FFatID, FatNum, Cliente,
        DataAbriu, DataEncer, IDDuplicata, NumeroNF, SerieNF, CartEmis,
        TipoCart, Genero, CondicaoPG, Represen, TipoFatura, FaturaDta, Financeiro,
        ValTotal, 0, 0, 0, 0, 0, 0, True, False);
      //
{ TODO : Ver se um dia vai fazer }
(*
      QuestaoTyp := qagOSBgstrl;
      QuestaoCod := Codigo;
      DmModAgenda.AtualizaStatusTodosEventosMesmoTipoCodigo(Estatus,
        QuestaoTyp, QuestaoCod);
*)
      if FQrIts <> nil then
      begin
        UMyMod.AbreQuery(FQrIts, Dmod.MyDB);
        //FQrIts.Locate('Controle', Controle, []);
      end;
      Close;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSrvLFatEmi.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLFatEmi.CalculaValores();
var
  ValLiqToFat, ValOutToFat, ValDesToFat, ValLiqOkFat: Double;
begin
  ValLiqToFat := EdValLiqToFat.ValueVariant;
  ValOutToFat := EdValOutToFat.ValueVariant;
  ValDesToFat := EdValDesToFat.ValueVariant;
  ValLiqOkFat := ValLiqToFat + ValOutToFat - ValDesToFat;
  //
  EdValLiqOkFat.ValueVariant := ValLiqOkFat;
end;

procedure TFmSrvLFatEmi.DBEdEmpresaChange(Sender: TObject);
var
  Empresa, Entidade: Integer;
begin
  if DBEDEmpresa.Text <> '' then
  begin
    Empresa  := Geral.IMV(DBEdEmpresa.Text);
    Entidade := DmodG.ObtemEntidadeDeFilial(Empresa);
    DmFatura.ReopenCartEmis(Entidade, True);
  end else
    DmFatura.QrCartEmis.Close;
end;

procedure TFmSrvLFatEmi.EdNumNFChange(Sender: TObject);
begin
  SbNFSe.Enabled := EdNumNF.ValueVariant = 0;
end;

procedure TFmSrvLFatEmi.EdValConsuChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmSrvLFatEmi.EdValDesToFatChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmSrvLFatEmi.EdValOutToFatChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmSrvLFatEmi.EdValLiqToFatChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmSrvLFatEmi.EdValUsadoChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmSrvLFatEmi.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdNumNFChange(Self);
end;

procedure TFmSrvLFatEmi.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DmFatura.ReopenPediPrzCab();
  DmFatura.ReopenContas(tfinCred);
  //
  CBCondicaoPG.ListSource := DmFatura.DsPediPrzCab;
  CBCartEmis.ListSource   := DmFatura.DsCartEmis;
  CBGenero.ListSource     := DmFatura.DsContas;
end;

procedure TFmSrvLFatEmi.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLFatEmi.SbNFSeClick(Sender: TObject);
const
  DPS           = 0;
  NFSeFatCab    = 0;
  //Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;

  Discriminacao = '';
  GeraNFSe      = True;
  SQLType       = stIns;
  Servico       = fgnLoteRPS;
  //
  TabedForm = nil;
var
  Prestador, Tomador: Integer;
  Valor: Double;
  SerieNF: String;
  NumNF: Integer;
begin
  Prestador := FEmpresa; //DmodG.QrFiliLogFilial.Value;
  Tomador   := FCliente;
  Valor     := EdValLiqOkFat.ValueVariant;
  //
  UnNFSe_PF_0201.MostraFormNFSe(SQLType,
  Prestador, Tomador, Intermediario, MeuServico, ItemListSrv,
  Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico, nil, Valor,
  SerieNF, NumNF, TabedForm);
  //
  if NumNF <> 0  then
  begin
    EdSerNF.ValueVariant := SerieNF;
    EdNumNF.ValueVariant := NumNF;
  end;
end;

procedure TFmSrvLFatEmi.SpeedButton1Click(Sender: TObject);
begin
  GFat_Jan.InsereEDefinePediPrzCab(EdCondicaoPG.ValueVariant, EdCondicaoPG,
    CBCondicaoPG, DmFatura.QrPediPrzCab);
end;

{ TODO : CONTRATO!!! }

end.
