object FmSrvLHonCab: TFmSrvLHonCab
  Left = 368
  Top = 194
  Caption = 'LOC-SERVI-012 :: Honor'#225'rios de OSs'
  ClientHeight = 631
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 109
    Width = 1008
    Height = 522
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 181
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 56
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label18: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label12: TLabel
        Left = 660
        Top = 16
        Width = 43
        Height = 13
        Caption = 'Abertura:'
      end
      object Label13: TLabel
        Left = 828
        Top = 16
        Width = 69
        Height = 13
        Caption = 'Encerramento:'
      end
      object Label11: TLabel
        Left = 924
        Top = 56
        Width = 65
        Height = 13
        Caption = 'Compet'#234'ncia:'
      end
      object Label4: TLabel
        Left = 924
        Top = 120
        Width = 42
        Height = 13
        Caption = 'Emiss'#227'o:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 72
        Width = 905
        Height = 21
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 525
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPDtHrIni: TdmkEditDateTimePicker
        Left = 660
        Top = 32
        Width = 112
        Height = 21
        Date = 40724.768997777780000000
        Time = 40724.768997777780000000
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrIni'
        UpdCampo = 'DtHrIni'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrIni: TdmkEdit
        Left = 772
        Top = 32
        Width = 52
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        QryCampo = 'DtHrIni'
        UpdCampo = 'DtHrIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtHrFim: TdmkEditDateTimePicker
        Left = 828
        Top = 32
        Width = 112
        Height = 21
        Date = 40724.768997777780000000
        Time = 40724.768997777780000000
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrFim'
        UpdCampo = 'DtHrFim'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrFim: TdmkEdit
        Left = 940
        Top = 32
        Width = 52
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        QryCampo = 'DtHrFim'
        UpdCampo = 'DtHrFim'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAnoMesCmpt: TdmkEdit
        Left = 924
        Top = 72
        Width = 65
        Height = 21
        Alignment = taCenter
        TabOrder = 8
        FormatType = dmktf_AAAAMM
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '08/2015'
        QryCampo = 'AnoMesCmpt'
        UpdCampo = 'AnoMesCmpt'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 42217d
        ValWarn = False
      end
      object RGUsaQualDta: TdmkRadioGroup
        Left = 16
        Top = 96
        Width = 905
        Height = 81
        Caption = ' Qual data usa para emiss'#227'o em massa de pagamento de honor'#225'rios:'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          
            'Dia informado no cadastro do Agente + m'#234's e ano ao lado (Emiss'#227'o' +
            ')'
          'Data da Abertura acima'
          'Data do encerramento acima'
          'Data de hoje (dia que lan'#231'ar)')
        TabOrder = 9
        QryCampo = 'UsaQualDta'
        UpdCampo = 'UsaQualDta'
        UpdType = utYes
        OldValor = 0
      end
      object EdAnoMesEmss: TdmkEdit
        Left = 924
        Top = 136
        Width = 65
        Height = 21
        Alignment = taCenter
        TabOrder = 10
        FormatType = dmktf_AAAAMM
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '08/2015'
        QryCampo = 'AnoMesEmss'
        UpdCampo = 'AnoMesEmss'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 42217d
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 459
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 109
    Width = 1008
    Height = 522
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 185
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label3: TLabel
        Left = 504
        Top = 16
        Width = 43
        Height = 13
        Caption = 'Abertura:'
      end
      object Label5: TLabel
        Left = 632
        Top = 16
        Width = 69
        Height = 13
        Caption = 'Encerramento:'
      end
      object Label6: TLabel
        Left = 16
        Top = 56
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 836
        Top = 16
        Width = 65
        Height = 13
        Caption = 'Compet'#234'ncia:'
      end
      object Label8: TLabel
        Left = 760
        Top = 16
        Width = 50
        Height = 13
        Caption = 'Valor total:'
        FocusControl = DBEdit7
      end
      object Label10: TLabel
        Left = 836
        Top = 120
        Width = 42
        Height = 13
        Caption = 'Emiss'#227'o:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsSrvLHonCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 16
        Top = 72
        Width = 885
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsSrvLHonCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsSrvLHonCab
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 369
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsSrvLHonCab
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 504
        Top = 32
        Width = 124
        Height = 21
        DataField = 'DtHrIni'
        DataSource = DsSrvLHonCab
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 632
        Top = 32
        Width = 124
        Height = 21
        DataField = 'DtHrFim_TXT'
        DataSource = DsSrvLHonCab
        TabOrder = 5
      end
      object DBEdit9: TDBEdit
        Left = 836
        Top = 32
        Width = 64
        Height = 21
        DataField = 'AnoMesCmpt_TXT'
        DataSource = DsSrvLHonCab
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 760
        Top = 32
        Width = 72
        Height = 21
        DataField = 'ValTotRat'
        DataSource = DsSrvLHonCab
        TabOrder = 7
      end
      object dmkRadioGroup1: TDBRadioGroup
        Left = 16
        Top = 96
        Width = 813
        Height = 81
        Caption = ' Qual data usa para emiss'#227'o em massa de pagamento de honor'#225'rios:'
        Columns = 2
        DataField = 'UsaQualDta'
        DataSource = DsSrvLHonCab
        Items.Strings = (
          'Indefinido'
          
            'Dia informado no cadastro do Agente + m'#234's e ano ao lado (Emiss'#227'o' +
            ')'
          'Data da Abertura acima'
          'Data do encerramento acima'
          'Data de hoje (dia que lan'#231'ar)')
        TabOrder = 8
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object DBEdit3: TDBEdit
        Left = 836
        Top = 136
        Width = 64
        Height = 21
        DataField = 'AnoMesEmss_TXT'
        DataSource = DsSrvLHonCab
        TabOrder = 9
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 458
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 300
        Top = 15
        Width = 706
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 573
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 302
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fatura'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 613
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&OSs/Agentes'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtFat: TBitBtn
          Tag = 414
          Left = 250
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cond. e Pgt.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtFatClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 185
      Width = 1008
      Height = 193
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Agentes das OSs'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DGDados: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 329
          Height = 165
          Align = alLeft
          DataSource = DsAgeOSs
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          PopupMenu = PMAgentes
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Agente'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_AGENTE'
              Title.Caption = 'Nome'
              Width = 174
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValPremio'
              Title.Caption = 'Valor'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CtrlSHA'
              Title.Caption = 'ID'
              Width = 56
              Visible = True
            end>
        end
        object GroupBox1: TGroupBox
          Left = 329
          Top = 0
          Width = 671
          Height = 165
          Align = alClient
          Caption = ' Dados do agente selecionado: '
          TabOrder = 1
          object dmkDBGridZTO1: TdmkDBGridZTO
            Left = 2
            Top = 15
            Width = 423
            Height = 148
            Align = alClient
            DataSource = DsSrvLOSOpeAge
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'OS'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID Srv'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Conta'
                Title.Caption = 'ID Item'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SrvLCad'
                Title.Caption = 'C'#243'd Srv'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_SrvlCad'
                Title.Caption = 'Nome do servi'#231'o'
                Width = 148
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPremio'
                Title.Caption = 'Valor'
                Width = 60
                Visible = True
              end>
          end
          object GroupBox16: TGroupBox
            Left = 425
            Top = 15
            Width = 244
            Height = 148
            Align = alRight
            Caption = ' Parcelas do agente selecionado: '
            TabOrder = 1
            object DBGLctFatRef: TDBGrid
              Left = 2
              Top = 15
              Width = 240
              Height = 131
              Align = alClient
              DataSource = DsLctFatRef
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'PARCELA'
                  Title.Caption = 'Parc.'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Lancto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Valor'
                  Width = 60
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencto'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 231
        Height = 32
        Caption = 'Honor'#225'rios de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 231
        Height = 32
        Caption = 'Honor'#225'rios de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 231
        Height = 32
        Caption = 'Honor'#225'rios de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 57
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 23
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrSrvLHonCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrSrvLHonCabBeforeOpen
    AfterOpen = QrSrvLHonCabAfterOpen
    BeforeClose = QrSrvLHonCabBeforeClose
    AfterScroll = QrSrvLHonCabAfterScroll
    SQL.Strings = (
      'SELECT eci.CodCliInt Filial, '
      'CONCAT(RIGHT(AnoMes, 2), "/", LEFT(AnoMes, 4)) AnoMes_TXT, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(DtHrFim <= "1900-01-01", "",'
      '  DATE_FORMAT(DtHrFim, "%d/%m/%Y %H:%i:%S")) DtHrFim_TXT,'
      'sfc.* '
      'FROM srvlhoncab sfc'
      'LEFT JOIN entidades emp ON emp.Codigo=sfc.Empresa'
      'LEFT JOIN enticliint eci ON eci.CodEnti=sfc.Empresa'
      'WHERE sfc.Codigo > 0'
      '')
    Left = 24
    Top = 297
    object QrSrvLHonCabFilial: TIntegerField
      FieldName = 'Filial'
      Required = True
    end
    object QrSrvLHonCabAnoMesCmpt_TXT: TWideStringField
      FieldName = 'AnoMesCmpt_TXT'
      Size = 7
    end
    object QrSrvLHonCabAnoMesEmss_TXT: TWideStringField
      FieldName = 'AnoMesEmss_TXT'
      Size = 7
    end
    object QrSrvLHonCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrSrvLHonCabDtHrFim_TXT: TWideStringField
      FieldName = 'DtHrFim_TXT'
      Size = 24
    end
    object QrSrvLHonCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLHonCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrSrvLHonCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSrvLHonCabAnoMesCmpt: TIntegerField
      FieldName = 'AnoMesCmpt'
    end
    object QrSrvLHonCabValTotRat: TFloatField
      FieldName = 'ValTotRat'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLHonCabDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
    object QrSrvLHonCabDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
    object QrSrvLHonCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLHonCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLHonCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLHonCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLHonCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLHonCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLHonCabAnoMesEmss: TIntegerField
      FieldName = 'AnoMesEmss'
    end
    object QrSrvLHonCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLHonCabUsaQualDta: TSmallintField
      FieldName = 'UsaQualDta'
    end
  end
  object DsSrvLHonCab: TDataSource
    DataSet = QrSrvLHonCab
    Left = 24
    Top = 341
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 476
    Top = 536
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      Visible = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 348
    Top = 540
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrAgeOSs: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrAgeOSsBeforeClose
    AfterScroll = QrAgeOSsAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT ooa.Agente,   '
      'IF(age.Tipo=0, age.RazaoSocial, age.Nome) NO_AGENTE  '
      'FROM srvlosopeage ooa  '
      'LEFT JOIN entidades age ON age.Codigo=ooa.Agente  '
      'WHERE ooa.SrvLHonAge=2')
    Left = 116
    Top = 296
    object QrAgeOSsAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrAgeOSsNO_AGENTE: TWideStringField
      FieldName = 'NO_AGENTE'
      Size = 100
    end
    object QrAgeOSsValPremio: TFloatField
      FieldName = 'ValPremio'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAgeOSsCtrlSHA: TIntegerField
      FieldName = 'CtrlSHA'
    end
  end
  object DsAgeOSs: TDataSource
    DataSet = QrAgeOSs
    Left = 116
    Top = 340
  end
  object PMFat: TPopupMenu
    OnPopup = PMFatPopup
    Left = 588
    Top = 536
    object Fatura1: TMenuItem
      Caption = '&Emite pagamento (financeiro)'
      object Agenteselecionado1: TMenuItem
        Caption = 'Agente &Selecionado'
        OnClick = Agenteselecionado1Click
      end
      object odosagentes1: TMenuItem
        Caption = '&Todos agentes n'#227'o faturados'
        OnClick = odosagentes1Click
      end
    end
    object Gerabloqueto1: TMenuItem
      Caption = 'Emite recibo(s)'
      object AgenteSelecionado2: TMenuItem
        Caption = 'Agente &Selecionado'
        OnClick = AgenteSelecionado2Click
      end
      object odosAgentes2: TMenuItem
        Caption = '&Todos Agentes'
        OnClick = odosAgentes2Click
      end
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Excluifaturamento1: TMenuItem
      Caption = 'Exclui todo pagamento'
      object AgenteSelecionado3: TMenuItem
        Caption = 'Agente &Selecionado'
        OnClick = AgenteSelecionado3Click
      end
    end
  end
  object QrSrvLOSOpeAge: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cad.Nome NO_SrvlCad, ooa.*'
      'FROM srvlosopeage ooa  '
      'LEFT JOIN srvlosopegrn oog ON oog.Controle=ooa.Controle'
      'LEFT JOIN srvlcad cad ON cad.Codigo=oog.SrvLCad'
      'WHERE ooa.SrvLHonAge=2  '
      'AND ooa.Agente=2  ')
    Left = 212
    Top = 296
    object QrSrvLOSOpeAgeSrvLCad: TIntegerField
      FieldName = 'SrvLCad'
    end
    object QrSrvLOSOpeAgeNO_SrvlCad: TWideStringField
      FieldName = 'NO_SrvlCad'
      Size = 60
    end
    object QrSrvLOSOpeAgeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLOSOpeAgeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrvLOSOpeAgeConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrSrvLOSOpeAgeAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrSrvLOSOpeAgeResponsa: TSmallintField
      FieldName = 'Responsa'
    end
    object QrSrvLOSOpeAgeValPremio: TFloatField
      FieldName = 'ValPremio'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLOSOpeAgeLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLOSOpeAgeDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLOSOpeAgeDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLOSOpeAgeUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLOSOpeAgeUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLOSOpeAgeAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLOSOpeAgeAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsSrvLOSOpeAge: TDataSource
    DataSet = QrSrvLOSOpeAge
    Left = 212
    Top = 344
  end
  object QrLctFatRef: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLctFatRefCalcFields
    SQL.Strings = (
      'SELECT lfr.* '
      'FROM lctfatref lfr '
      'WHERE lfr.Controle=0')
    Left = 384
    Top = 300
    object QrLctFatRefPARCELA: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'PARCELA'
      Calculated = True
    end
    object QrLctFatRefCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLctFatRefControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctFatRefConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrLctFatRefLancto: TLargeintField
      FieldName = 'Lancto'
    end
    object QrLctFatRefValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctFatRefVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsLctFatRef: TDataSource
    DataSet = QrLctFatRef
    Left = 384
    Top = 344
  end
  object QrSrvLFatPrz: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT opz.Parcelas, '
      'opz.Codigo, opz.Controle, opz.Condicao, '
      'opz.DescoPer, opz.Escolhido, ppc.Nome NO_CONDICAO'
      'FROM osprz opz'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=opz.Condicao'
      'WHERE opz.Codigo=:P0')
    Left = 304
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSrvLFatPrzCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osprz.Codigo'
    end
    object QrSrvLFatPrzControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osprz.Controle'
    end
    object QrSrvLFatPrzCondicao: TIntegerField
      FieldName = 'Condicao'
      Origin = 'osprz.Condicao'
    end
    object QrSrvLFatPrzNO_CONDICAO: TWideStringField
      FieldName = 'NO_CONDICAO'
      Origin = 'pediprzcab.Nome'
      Size = 50
    end
    object QrSrvLFatPrzDescoPer: TFloatField
      FieldName = 'DescoPer'
      Origin = 'osprz.DescoPer'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrSrvLFatPrzEscolhido: TSmallintField
      FieldName = 'Escolhido'
      Origin = 'osprz.Escolhido'
      MaxValue = 1
    end
    object QrSrvLFatPrzParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrSrvLFatPrzVAL_COM_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_COM_DESCO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLFatPrzVAL_PARCELA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_PARCELA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLFatPrzORC_COM_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ORC_COM_DESCO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLFatPrzORC_PARCELA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ORC_PARCELA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLFatPrzINV_COM_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'INV_COM_DESCO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLFatPrzINV_PARCELA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'INV_PARCELA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
  end
  object DsSrvLFatPrz: TDataSource
    DataSet = QrSrvLFatPrz
    Left = 304
    Top = 344
  end
  object QrNulos: TMySQLQuery
    Database = Dmod.MyDB
    Left = 468
    Top = 300
    object QrNulosCtrlSHA: TIntegerField
      FieldName = 'CtrlSHA'
    end
    object QrNulosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNulosAgente: TIntegerField
      FieldName = 'Agente'
    end
  end
  object QrAgeErr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT   '
      '  '
      'IF(aec.CondicaoPG IS NULL or aec.CondicaoPG < 1,   '
      '"ERRO", "OK") How_CondiCaoPG,  '
      '  '
      'IF(aec.GeneroHon IS NULL or aec.GeneroHon < 1,   '
      '"ERRO", "OK") How_GeneroHon,  '
      '  '
      'IF(aec.DiaPagaHon IS NULL or aec.DiaPagaHon < 1,   '
      '"ERRO", "OK") How_DiaPagaHon,  '
      '  '
      'IF(aec.UsaQualCrt IS NULL or aec.UsaQualCrt < 2,   '
      '"ERRO", "OK") How_UsaQualCrt,  '
      '  '
      'IF(aec.CartEmis IS NULL or (  '
      'aec.CartEmis < 1 AND aec.UsaQualCrt = 2),   '
      '"ERRO", "OK") How_CartEmis,  '
      '  '
      'IF(aec.UsaQualCrt IS NULL or (  '
      '0 < 1 AND aec.UsaQualCrt = 3),   '
      '"ERRO", "OK") How_CartGera,  '
      '  '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)   '
      'NO_AGENTE, sha.Entidade  '
      ''
      'FROM srvlhonage sha  '
      
        'LEFT JOIN ageentcad aec ON aec.Codigo=sha.Entidade AND aec.Empre' +
        'sa=-11  '
      'LEFT JOIN entidades ent ON ent.Codigo=sha.Entidade  '
      'WHERE sha.Codigo=2  '
      'AND (  '
      '  aec.Codigo IS NULL  '
      '  OR  '
      '  (  '
      '    aec.CondicaoPG=0  '
      '    OR  '
      '    aec.GeneroHon=0  '
      '    OR  '
      '    aec.DiaPagaHon=0  '
      '    OR   '
      '    aec.CartEmis=0  '
      '    OR  '
      '    0 < 1 AND aec.UsaQualCrt = 3  '
      '  )  '
      ')  '
      '')
    Left = 664
    Top = 288
    object QrAgeErrHow_CondiCaoPG: TWideStringField
      FieldName = 'How_CondiCaoPG'
      Required = True
      Size = 4
    end
    object QrAgeErrHow_GeneroHon: TWideStringField
      FieldName = 'How_GeneroHon'
      Required = True
      Size = 4
    end
    object QrAgeErrHow_DiaPagaHon: TWideStringField
      FieldName = 'How_DiaPagaHon'
      Required = True
      Size = 4
    end
    object QrAgeErrHow_UsaQualCrt: TWideStringField
      FieldName = 'How_UsaQualCrt'
      Required = True
      Size = 4
    end
    object QrAgeErrHow_CartEmis: TWideStringField
      FieldName = 'How_CartEmis'
      Required = True
      Size = 4
    end
    object QrAgeErrHow_CartGera: TWideStringField
      FieldName = 'How_CartGera'
      Required = True
      Size = 4
    end
    object QrAgeErrNO_AGENTE: TWideStringField
      FieldName = 'NO_AGENTE'
      Size = 100
    end
    object QrAgeErrEntidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object frxLOC_SERVI_012_Err: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxLOC_SERVI_012_ErrGetValue
    Left = 664
    Top = 380
    Datasets = <
      item
        DataSet = frxDsAgeErr
        DataSetName = 'frxDsAgeErr'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Erros que Impedem Pagamento M'#250'ltiplo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Agente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 56.692950000000000000
          Top = 64.252010000000000000
          Width = 215.433170940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 272.126160000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cond.Pag.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 340.157700000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta (plano)')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 408.189240000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dia Pag.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 476.220780000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qual cart.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 544.252320000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 612.283860000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cart. geral')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 20.787401570000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        DataSet = frxDsAgeErr
        DataSetName = 'frxDsAgeErr'
        RowCount = 0
        object MeValCodi: TfrxMemoView
          Width = 56.692913390000000000
          Height = 20.787401570000000000
          DataField = 'Entidade'
          DataSet = frxDsAgeErr
          DataSetName = 'frxDsAgeErr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAgeErr."Entidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 56.692950000000000000
          Width = 215.433170940000000000
          Height = 20.787401570000000000
          DataField = 'NO_AGENTE'
          DataSet = frxDsAgeErr
          DataSetName = 'frxDsAgeErr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAgeErr."NO_AGENTE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 272.126160000000000000
          Width = 68.031496060000000000
          Height = 20.787401570000000000
          DataField = 'How_CondiCaoPG'
          DataSet = frxDsAgeErr
          DataSetName = 'frxDsAgeErr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAgeErr."How_CondiCaoPG"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 340.157700000000000000
          Width = 68.031496060000000000
          Height = 20.787401570000000000
          DataField = 'How_GeneroHon'
          DataSet = frxDsAgeErr
          DataSetName = 'frxDsAgeErr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAgeErr."How_GeneroHon"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 408.189240000000000000
          Width = 68.031496060000000000
          Height = 20.787401570000000000
          DataField = 'How_DiaPagaHon'
          DataSet = frxDsAgeErr
          DataSetName = 'frxDsAgeErr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAgeErr."How_DiaPagaHon"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 476.220780000000000000
          Width = 68.031496060000000000
          Height = 20.787401570000000000
          DataField = 'How_UsaQualCrt'
          DataSet = frxDsAgeErr
          DataSetName = 'frxDsAgeErr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAgeErr."How_UsaQualCrt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 544.252320000000000000
          Width = 68.031496060000000000
          Height = 20.787401570000000000
          DataField = 'How_CartEmis'
          DataSet = frxDsAgeErr
          DataSetName = 'frxDsAgeErr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAgeErr."How_CartEmis"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 20.787401570000000000
          DataField = 'How_CartGera'
          DataSet = frxDsAgeErr
          DataSetName = 'frxDsAgeErr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAgeErr."How_CartGera"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsAgeErr: TfrxDBDataset
    UserName = 'frxDsAgeErr'
    CloseDataSource = False
    FieldAliases.Strings = (
      'How_CondiCaoPG=How_CondiCaoPG'
      'How_GeneroHon=How_GeneroHon'
      'How_DiaPagaHon=How_DiaPagaHon'
      'How_UsaQualCrt=How_UsaQualCrt'
      'How_CartEmis=How_CartEmis'
      'How_CartGera=How_CartGera'
      'NO_AGENTE=NO_AGENTE'
      'Entidade=Entidade')
    DataSet = QrAgeErr
    BCDToCurrency = False
    Left = 664
    Top = 336
  end
  object PMAgentes: TPopupMenu
    OnPopup = PMAgentesPopup
    Left = 112
    Top = 256
    object Irparajaneladecadastrodoagente1: TMenuItem
      Caption = '&Ir para janela de cadastro do agente'
      OnClick = Irparajaneladecadastrodoagente1Click
    end
  end
  object QrAgeEntCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM ageentcad '
      'WHERE Codigo=4 ')
    Left = 664
    Top = 428
    object QrAgeEntCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgeEntCadCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
    end
    object QrAgeEntCadGeneroHon: TIntegerField
      FieldName = 'GeneroHon'
    end
    object QrAgeEntCadDiaPagaHon: TSmallintField
      FieldName = 'DiaPagaHon'
    end
    object QrAgeEntCadUsaQualCrt: TSmallintField
      FieldName = 'UsaQualCrt'
    end
    object QrAgeEntCadCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
    object QrAgeEntCadEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrAgeEntCadControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 860
    Top = 284
  end
end
