unit SrvLOSOpeCPA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkCheckBox;

type
  TFmSrvLOSOpeCPA = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdNivel4: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label12: TLabel;
    TPDtaCompet: TdmkEditDateTimePicker;
    GroupBox3: TGroupBox;
    Panel3: TPanel;
    Label1: TLabel;
    EdHrInnDay: TdmkEdit;
    EdHrOutDay: TdmkEdit;
    Label4: TLabel;
    GroupBox4: TGroupBox;
    CkTemInterv: TdmkCheckBox;
    PnIntervalo: TPanel;
    Label2: TLabel;
    Label7: TLabel;
    EdHrOutMid: TdmkEdit;
    EdHrInnMid: TdmkEdit;
    EdValPremio: TdmkEdit;
    Label8: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CkTemIntervClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrCab: TmySQLQuery;
    FDsCab: TDataSource;
    FCodigo, FControle, FConta: Integer;
  end;

  var
  FmSrvLOSOpeCPA: TFmSrvLOSOpeCPA;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnSrvL_PF, SrvlOSCab;

{$R *.DFM}

procedure TFmSrvLOSOpeCPA.BtOKClick(Sender: TObject);
const
  ValEverAut = 0;
  FatorPrm   = 0;
var
  DtaCompet, HrInnDay, HrOutMid, HrInnMid, HrOutDay: String;
  Codigo, Controle, Conta, Nivel4, TemInterv(*, ValEverAut*): Integer;
  (*FatorPrm,*) ValPremio: Double;
  SQLType: TSQLType;
begin
  SQLTYpe        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  Controle       := FControle;
  Conta          := FConta;
  Nivel4         := EdNivel4.ValueVariant;
  DtaCompet      := Geral.FDT(TPDtaCompet.Date, 1);
  HrInnDay       := EdHrInnDay.Text;
  HrOutMid       := EdHrOutMid.Text;
  HrInnMid       := EdHrInnMid.Text;
  HrOutDay       := EdHrOutDay.Text;
  TemInterv      := Geral.BoolToInt(CkTemInterv.Checked);
  //FatorPrm       := ;
  ValPremio      := EdValPremio.ValueVariant;
  //ValEverAut     := 0;
  //
  if MyObjects.FIC(ValPremio < 0.01, EdValPremio, 'Informe o valor!') then
    Exit;
  Nivel4 := UMyMod.BPGS1I32('srvlosopecpa', 'Nivel4', '', '', tsPos, SQLType, Nivel4);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'srvlosopecpa', False, [
  'Codigo', 'Controle', 'Conta',
  'DtaCompet', 'HrInnDay', 'HrOutMid',
  'HrInnMid', 'HrOutDay', 'TemInterv',
  'FatorPrm', 'ValPremio', 'ValEverAut'], [
  'Nivel4'], [
  Codigo, Controle, Conta,
  DtaCompet, HrInnDay, HrOutMid,
  HrInnMid, HrOutDay, TemInterv,
  FatorPrm, ValPremio, ValEverAut], [
  Nivel4], True) then
  begin
    SrvL_PF.AtualizaTotaisSrvLOSOpeAge(FConta);
    SrvL_PF.AtualizaTotaisSrvOSOpeGrn(FCodigo, FControle);
    FmSrvLOSCab.ReopenEscada(FCodigo, FControle, FConta, Nivel4);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      EdNivel4.ValueVariant    := 0;
      TPDtaCompet.SetFocus;
    end else Close;
  end;
end;

procedure TFmSrvLOSOpeCPA.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLOSOpeCPA.CkTemIntervClick(Sender: TObject);
begin
  PnIntervalo.Visible := CkTemInterv.Checked;
end;

procedure TFmSrvLOSOpeCPA.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmSrvLOSOpeCPA.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmSrvLOSOpeCPA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
