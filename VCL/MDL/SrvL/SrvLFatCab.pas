unit SrvLFatCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, AppListas, dmkDBGridZTO, UnProjGroup_Consts;

type
  THackDBGrid = class(TDBGrid);
  TFmSrvLFatCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    QrSrvLFatCab: TmySQLQuery;
    DsSrvLFatCab: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrSrvLFatCabNO_EMPRESA: TWideStringField;
    QrSrvLFatCabNO_CLIENTE: TWideStringField;
    QrSrvLFatCabCodigo: TIntegerField;
    QrSrvLFatCabNome: TWideStringField;
    QrSrvLFatCabEmpresa: TIntegerField;
    QrSrvLFatCabCliente: TIntegerField;
    QrSrvLFatCabAnoMes: TIntegerField;
    QrSrvLFatCabValTotCbr: TFloatField;
    QrSrvLFatCabDtHrIni: TDateTimeField;
    QrSrvLFatCabDtHrFim: TDateTimeField;
    QrSrvLFatCabLk: TIntegerField;
    QrSrvLFatCabDataCad: TDateField;
    QrSrvLFatCabDataAlt: TDateField;
    QrSrvLFatCabUserCad: TIntegerField;
    QrSrvLFatCabUserAlt: TIntegerField;
    QrSrvLFatCabAlterWeb: TSmallintField;
    QrSrvLFatCabAtivo: TSmallintField;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENT: TWideStringField;
    QrClientesCIDADE: TWideStringField;
    QrClientesNOMEUF: TWideStringField;
    QrClientesCodUsu: TIntegerField;
    QrClientesIE: TWideStringField;
    DsClientes: TDataSource;
    EdEmpresa: TdmkEditCB;
    Label18: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label17: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label12: TLabel;
    TPDtHrIni: TdmkEditDateTimePicker;
    EdDtHrIni: TdmkEdit;
    Label13: TLabel;
    TPDtHrFim: TdmkEditDateTimePicker;
    EdDtHrFim: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label11: TLabel;
    EdAnoMes: TdmkEdit;
    Label14: TLabel;
    QrSrvLFatCabDtHrFim_TXT: TWideStringField;
    QrSrvLFatCabAnoMes_TXT: TWideStringField;
    QrSrvLOSOpeGrn: TmySQLQuery;
    QrSrvLOSOpeGrnCodigo: TIntegerField;
    QrSrvLOSOpeGrnControle: TIntegerField;
    QrSrvLOSOpeGrnSrvLCad: TIntegerField;
    QrSrvLOSOpeGrnLk: TIntegerField;
    QrSrvLOSOpeGrnDataCad: TDateField;
    QrSrvLOSOpeGrnDataAlt: TDateField;
    QrSrvLOSOpeGrnUserCad: TIntegerField;
    QrSrvLOSOpeGrnUserAlt: TIntegerField;
    QrSrvLOSOpeGrnAlterWeb: TSmallintField;
    QrSrvLOSOpeGrnAtivo: TSmallintField;
    QrSrvLOSOpeGrnNome: TWideStringField;
    QrSrvLOSOpeGrnNO_SrvLCad: TWideStringField;
    QrSrvLOSOpeGrnSrvLPrmCad: TIntegerField;
    QrSrvLOSOpeGrnNO_SrvLPrmCad: TWideStringField;
    QrSrvLOSOpeGrnNO_AgeEquiCab: TWideStringField;
    QrSrvLOSOpeGrnAgeEqiCab: TIntegerField;
    QrSrvLOSOpeGrnUnidMedCobr: TIntegerField;
    QrSrvLOSOpeGrnQtdRlzCobr: TFloatField;
    QrSrvLOSOpeGrnValFtrCobr: TFloatField;
    QrSrvLOSOpeGrnFrmFtrCobr: TSmallintField;
    QrSrvLOSOpeGrnUnidMedPaga: TIntegerField;
    QrSrvLOSOpeGrnQtdRlzPaga: TFloatField;
    QrSrvLOSOpeGrnValRatPaga: TFloatField;
    QrSrvLOSOpeGrnFrmRatPaga: TSmallintField;
    QrSrvLOSOpeGrnFrmFtrPaga: TSmallintField;
    QrSrvLOSOpeGrnValTotCobr: TFloatField;
    QrSrvLOSOpeGrnNO_FrmRatPaga: TWideStringField;
    QrSrvLOSOpeGrnSIGLA_UMC: TWideStringField;
    QrSrvLOSOpeGrnSIGLA_UMP: TWideStringField;
    QrSrvLOSOpeGrnNO_FrmFtrCobr: TWideStringField;
    QrSrvLOSOpeGrnNO_FrmFtrPaga: TWideStringField;
    QrSrvLOSOpeGrnValFtrPaga: TFloatField;
    QrSrvLOSOpeGrnValTotDesp: TFloatField;
    DsSrvLOSOpeGrn: TDataSource;
    DBEdit9: TDBEdit;
    QrSrvLFatCabValDspToDsc: TFloatField;
    QrSrvLFatCabValLiqToFat: TFloatField;
    QrSrvLFatCabValLiqOkFat: TFloatField;
    QrSrvLFatCabValDspToDpl: TFloatField;
    QrSrvLFatCabValDspOkDpl: TFloatField;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    Label10: TLabel;
    DBEdit8: TDBEdit;
    Label15: TLabel;
    DBEdit10: TDBEdit;
    Label16: TLabel;
    DBEdit11: TDBEdit;
    BtFat: TBitBtn;
    PMFat: TPopupMenu;
    Adicionanovacondiodepagamento1: TMenuItem;
    Editaacondiodepagamentoatual1: TMenuItem;
    Removecondiaodepagamentoatual1: TMenuItem;
    N15: TMenuItem;
    Fatura1: TMenuItem;
    Gerabloqueto1: TMenuItem;
    Visualizarbloquetos1: TMenuItem;
    N4: TMenuItem;
    Excluifaturamento1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel9: TPanel;
    GroupBox15: TGroupBox;
    DBGSrvLFatPrz: TDBGrid;
    GroupBox16: TGroupBox;
    DBGLctFatRef: TDBGrid;
    DGDados: TdmkDBGridZTO;
    QrSrvLFatPrz: TmySQLQuery;
    QrSrvLFatPrzCodigo: TIntegerField;
    QrSrvLFatPrzControle: TIntegerField;
    QrSrvLFatPrzCondicao: TIntegerField;
    QrSrvLFatPrzNO_CONDICAO: TWideStringField;
    QrSrvLFatPrzDescoPer: TFloatField;
    QrSrvLFatPrzEscolhido: TSmallintField;
    QrSrvLFatPrzParcelas: TIntegerField;
    QrSrvLFatPrzVAL_COM_DESCO: TFloatField;
    QrSrvLFatPrzVAL_PARCELA: TFloatField;
    QrSrvLFatPrzORC_COM_DESCO: TFloatField;
    QrSrvLFatPrzORC_PARCELA: TFloatField;
    QrSrvLFatPrzINV_COM_DESCO: TFloatField;
    QrSrvLFatPrzINV_PARCELA: TFloatField;
    DsSrvLFatPrz: TDataSource;
    QrLctFatRef: TmySQLQuery;
    QrLctFatRefPARCELA: TIntegerField;
    QrLctFatRefCodigo: TIntegerField;
    QrLctFatRefControle: TIntegerField;
    QrLctFatRefConta: TIntegerField;
    QrLctFatRefLancto: TLargeintField;
    QrLctFatRefValor: TFloatField;
    QrLctFatRefVencto: TDateField;
    DsLctFatRef: TDataSource;
    QrSrvLFatCabEntPagante: TIntegerField;
    Label21: TLabel;
    DBEdit14: TDBEdit;
    QrSrvLFatCabValOutToFat: TFloatField;
    QrSrvLFatCabValDesToFat: TFloatField;
    QrSrvLFatCabCondicaoPG: TIntegerField;
    QrSrvLFatCabCartEmis: TIntegerField;
    QrSrvLFatCabGenero: TIntegerField;
    QrSrvLFatCabSerNF: TWideStringField;
    QrSrvLFatCabNumNF: TIntegerField;
    QrSrvLFatCabDtaFimFat: TDateTimeField;
    Label22: TLabel;
    EdEntPagante: TdmkEditCB;
    CBEntPagante: TdmkDBLookupComboBox;
    QrEntPagante: TmySQLQuery;
    DsEntPagante: TDataSource;
    QrEntPaganteCodigo: TIntegerField;
    QrEntPaganteNOMEENT: TWideStringField;
    QrEntPaganteCIDADE: TWideStringField;
    QrEntPaganteNOMEUF: TWideStringField;
    QrEntPaganteCodUsu: TIntegerField;
    QrEntPaganteIE: TWideStringField;
    QrSrvLFatCabNO_ENTPAGANTE: TWideStringField;
    QrSrvLFatCabFilial: TIntegerField;
    PB1: TProgressBar;
    QrSrvLOSOpeGrnAutorizado: TSmallintField;
    N1: TMenuItem;
    QrLoc: TmySQLQuery;
    DBEdit12: TDBEdit;
    Label20: TLabel;
    DBEdit13: TDBEdit;
    Label19: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSrvLFatCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSrvLFatCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrSrvLFatCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrSrvLFatCabBeforeClose(DataSet: TDataSet);
    procedure Adicionanovacondiodepagamento1Click(Sender: TObject);
    procedure BtFatClick(Sender: TObject);
    procedure Editaacondiodepagamentoatual1Click(Sender: TObject);
    procedure Removecondiaodepagamentoatual1Click(Sender: TObject);
    procedure Fatura1Click(Sender: TObject);
    procedure Gerabloqueto1Click(Sender: TObject);
    procedure Visualizarbloquetos1Click(Sender: TObject);
    procedure Excluifaturamento1Click(Sender: TObject);
    procedure QrSrvLFatPrzCalcFields(DataSet: TDataSet);
    procedure DBGSrvLFatPrzCellClick(Column: TColumn);
    procedure DBGSrvLFatPrzColEnter(Sender: TObject);
    procedure DBGSrvLFatPrzDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGSrvLFatPrzMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure QrLctFatRefCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure PMFatPopup(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormSrvLFatOpe(SQLType: TSQLType);
    procedure MostraFormSrvLFatEmi(SQLType: TSQLType);
    procedure ReopenSrvLOSOpeGrn(Controle: Integer);
    procedure AtualizaTotaisSrvlFatCab(Codigo: Integer);
    function  CondicaoPGSelecionado(): Integer;
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //procedure ReopenSrvLFatOpe(Controle: Integer);

  end;

var
  FmSrvLFatCab: TFmSrvLFatCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, SrvLFatOpe, ModuleGeral,
  UnSrvL_PF, ModuleFin, ModuleFatura, SrvLFatEmi, MyVCLSkin, UnBloquetos,
  Principal, SrvlFatCabImp;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSrvLFatCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSrvLFatCab.MostraFormSrvLFatEmi(SQLType: TSQLType);
var
  CondicaoPG: Integer;
begin
  //fazer abertura e encerramento de faturamento!
  //ver outros campos necess�rios para faturamento como carteira etc.

{
  if (QrLctFatRef.State <> dsInactive) and
  (QrLctFatRef.RecordCount > 0) then
    SQLType := stUpd
  else
    SQLType := stIns;
  //
}

  if DBCheck.CriaFm(TFmSrvLFatEmi, FmSrvLFatEmi, afmoNegarComAviso) then
  begin
    FmSrvLFatEmi.ImgTipo.SQLType := SQLType;
    FmSrvLFatEmi.FQrCab := QrSrvLFatCab;
    FmSrvLFatEmi.FDsCab := DsSrvLFatCab;
    FmSrvLFatEmi.FQrIts := QrLctFatRef;

    FmSrvLFatEmi.FValAFat := QrSrvLFatCabValLiqToFat.Value - QrSrvLFatCabValLiqOkFat.Value;

    FmSrvLFatEmi.DBEdCodigo.DataSource  := DsSrvLFatCab;
    FmSrvLFatEmi.DBEdEmpresa.DataSource := DsSrvLFatCab;
    FmSrvLFatEmi.DBEdNO_EMP.DataSource  := DsSrvLFatCab;
    FmSrvLFatEmi.DBEdPagante.DataSource := DsSrvLFatCab;
    FmSrvLFatEmi.DBEdNO_PAG.DataSource  := DsSrvLFatCab;

    FmSrvLFatEmi.FEmpresa := QrSrvLFatCabEmpresa.Value;
    FmSrvLFatEmi.FCliente := QrSrvLFatCabEntPagante.Value;
    //FmSrvLFatEmi.FDataAbriu := QrLocCConDtHrEmi.Value;
    FmSrvLFatEmi.FFatID   := VAR_FATID_3002;

    // N�o h� faturamento parcial: Controle e c�digo s�o o mesmo n�mero
    FmSrvLFatEmi.FEncerra := True;
    FmSrvLFatEmi.EdControle.ValueVariant := QrSrvLFatCabCodigo.Value;
    // FIM N�o h� faturamento parcial!

    FmSrvLFatEmi.EdValLiqToFat.ValueVariant := QrSrvLFatCabValLiqToFat.Value - QrSrvLFatCabValLiqOkFat.Value;
    (*
    FmSrvLFatEmi.EdValOutToFat.ValueVariant := QrSrvLFatCabValOutToFat.Value;
    FmSrvLFatEmi.EdValDesToFat.ValueVariant := QrSrvLFatCabValDesToFat.Value;
    *)
    DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);

    if SQLType = stIns then
    begin
      FmSrvLFatEmi.TPDataFat.Date := Trunc(Date);
      FmSrvLFatEmi.EdHoraFat.Text := FormatDateTime('hh:nn:ss', Now());
      //
      CondicaoPG := CondicaoPGSelecionado();
      //
      UMyMod.SetaCodUsuDeCodigo(FmSrvLFatEmi.EdCondicaoPG,
        FmSrvLFatEmi.CBCondicaoPG, DmFatura.QrPediPrzCab, CondicaoPG);
      //
      FmSrvLFatEmi.EdGenero.ValueVariant := DModG.QrParamsEmpCtaServico.Value;
      FmSrvLFatEmi.CBGenero.KeyValue     := DModG.QrParamsEmpCtaServico.Value;
    end else
    begin
      //
      UMyMod.SetaCodUsuDeCodigo(FmSrvLFatEmi.EdCondicaoPG,
        FmSrvLFatEmi.CBCondicaoPG, DmFatura.QrPediPrzCab, QrSrvLFatCabCondicaoPG.Value);

      FmSrvLFatEmi.EdCartEmis.ValueVariant := QrSrvLFatCabCartEmis.Value;
      FmSrvLFatEmi.CBCartEmis.KeyValue     := QrSrvLFatCabCartEmis.Value;
      FmSrvLFatEmi.EdGenero.ValueVariant   := QrSrvLFatCabGenero.Value;
      FmSrvLFatEmi.CBGenero.KeyValue       := QrSrvLFatCabGenero.Value;

      //FmSrvLFatEmi.EdValorTotal.ValueVariant := QrSrvLFatCabValorTotal.Value;

      FmSrvLFatEmi.EdSerNF.ValueVariant := QrSrvLFatCabSerNF.Value;
      FmSrvLFatEmi.EdNumNF.ValueVariant := QrSrvLFatCabNumNF.Value;

      FmSrvLFatEmi.TPDataFat.Date := Trunc(QrSrvLFatCabDtaFimFat.Value);
      FmSrvLFatEmi.EdHoraFat.Text := FormatDateTime('hh:nn:ss', QrSrvLFatCabDtaFimFat.Value);
    end;
    FmSrvLFatEmi.ShowModal;
    FmSrvLFatEmi.Destroy;
  end;
end;

procedure TFmSrvLFatCab.MostraFormSrvLFatOpe(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmSrvLFatOpe, FmSrvLFatOpe, afmoNegarComAviso) then
  begin
    FmSrvLFatOpe.ImgTipo.SQLType := SQLType;
    FmSrvLFatOpe.FQrCab := QrSrvLFatCab;
    FmSrvLFatOpe.FDsCab := DsSrvLFatCab;
    //FmSrvLFatOpe.FQrIts := QrSrvLFatOpe;
    FmSrvLFatOpe.FQrIts := QrSrvLOSOpeGrn;
    FmSrvLFatOpe.FEmpresa := QrSrvLFatCabEmpresa.Value;
    FmSrvLFatOpe.FCliente := QrSrvLFatCabCliente.Value;
    FmSrvLFatOpe.FCodigo  := QrSrvLFatCabCodigo.Value;
    if SQLType = stIns then
      //
    else
    begin
(*
      FmSrvLFatOpe.EdControle.ValueVariant := QrSrvLFatOpeControle.Value;
*)
    end;
    FmSrvLFatOpe.ReopeSrvLOSOpeGrn();
    FmSrvLFatOpe.ShowModal;
    FmSrvLFatOpe.Destroy;
  end;
end;

procedure TFmSrvLFatCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrSrvLFatCab);
  //MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrSrvLFatCab, QrSrvLFatOpe);
  CabExclui1.Enabled := False;//MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrSrvLFatCab, QrSrvLOSOpeGrn);
end;

procedure TFmSrvLFatCab.PMFatPopup(Sender: TObject);
var
  Encerr: Boolean;
begin
  Encerr := QrSrvLFatCabDtHrFim_TXT.Value <> '';

  if not Encerr then
  begin
    MyObjects.HabilitaMenuItemItsIns(Adicionanovacondiodepagamento1, QrSrvLFatCab);
    MyObjects.HabilitaMenuItemItsUpd(Editaacondiodepagamentoatual1, QrSrvLFatPrz);
    MyObjects.HabilitaMenuItemItsDel(Removecondiaodepagamentoatual1, QrSrvLFatPrz);
    //
    MyObjects.HabilitaMenuItemItsDel(Fatura1, QrSrvLFatPrz);
    MyObjects.HabilitaMenuItemItsDel(Gerabloqueto1, QrLctFatRef);
    MyObjects.HabilitaMenuItemItsDel(Visualizarbloquetos1, QrLctFatRef);
    //
    if Fatura1.Enabled = True then
      Fatura1.Enabled := (QrSrvLFatCabValLiqToFat.Value - QrSrvLFatCabValLiqOkFat.Value) > 0;
    //
    MyObjects.HabilitaMenuItemItsDel(Excluifaturamento1, QrLctFatRef);
  end else
  begin
    Adicionanovacondiodepagamento1.Enabled := False;
    Editaacondiodepagamentoatual1.Enabled  := False;
    Removecondiaodepagamentoatual1.Enabled := False;
    Fatura1.Enabled                        := False;
    Gerabloqueto1.Enabled                  := False;
    Visualizarbloquetos1.Enabled           := False;
    Excluifaturamento1.Enabled             := False;
  end;
end;

procedure TFmSrvLFatCab.PMItsPopup(Sender: TObject);
var
  Encerr: Boolean;
begin
  Encerr := QrSrvLFatCabDtHrFim_TXT.Value <> '';

  if not Encerr then
  begin
    MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrSrvLFatCab);
    //MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrSrvLFatOpe);
    //MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrSrvLFatOpe);
    MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrSrvLOSOpeGrn);
    MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrSrvLOSOpeGrn);
  end else
  begin
    ItsInclui1.Enabled := False;
    ItsAltera1.Enabled := False;
    ItsExclui1.Enabled := False;
  end;
end;

procedure TFmSrvLFatCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSrvLFatCabCodigo.Value, LaRegistro.Caption[2]);
end;
procedure TFmSrvLFatCab.Visualizarbloquetos1Click(Sender: TObject);
begin
  //FmPrincipal.MostraBloGeren(1, VAR_FATID_3002, 0, QrOSCabCodigo.Value);
  UBloquetos.MostraBloGeren(0, VAR_FATID_3002, 0, QrSrvlFatCabCodigo.Value,
    FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPagerNovo);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSrvLFatCab.DefParams;
begin
  VAR_GOTOTABELA := 'srvlfatcab';
  VAR_GOTOMYSQLTABLE := QrSrvLFatCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT eci.CodCliInt Filial, ');
  VAR_SQLx.Add('CONCAT(RIGHT(AnoMes, 2), "/", LEFT(AnoMes, 4)) AnoMes_TXT, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE,');
  VAR_SQLx.Add('IF(epg.Tipo=0, epg.RazaoSocial, epg.Nome) NO_ENTPAGANTE, ');
  VAR_SQLx.Add('IF(DtHrFim <= "1900-01-01", "",');
  VAR_SQLx.Add('  DATE_FORMAT(DtHrFim, "%d/%m/%Y %H:%i:%S")) DtHrFim_TXT,');
  VAR_SQLx.Add('sfc.* ');
  VAR_SQLx.Add('FROM srvlfatcab sfc');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=sfc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades epg ON epg.Codigo=sfc.EntPagante');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=sfc.Cliente');
  VAR_SQLx.Add('LEFT JOIN enticliint eci ON eci.CodEnti=sfc.Empresa');
  VAR_SQLx.Add('WHERE sfc.Codigo > 0');
  //
  VAR_SQL1.Add('AND sfc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND sfc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sfc.Nome Like :P0');
  //
end;

procedure TFmSrvLFatCab.EdClienteChange(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := EdCliente.ValueVariant;
  //
  if (ImgTipo.SQLType = stIns) and (EdEntPagante.ValueVariant = 0) then
  begin
    EdEntPagante.ValueVariant := Entidade;
    CBEntPagante.KeyValue     := Entidade;
  end;
end;

procedure TFmSrvLFatCab.Editaacondiodepagamentoatual1Click(Sender: TObject);
begin
  SrvL_PF.MostraFormSrvLFatPrz(
    stUpd, QrSrvLFatCab, QrSrvLFatPrz, 0, 0, 0,
    QrSrvLFatCabCodigo.Value, QrSrvLFatCabEmpresa.Value, DsSrvLFatCab);
end;

procedure TFmSrvLFatCab.Excluifaturamento1Click(Sender: TObject);
const
  Terceiro = 0;
var
  Filial, Quitados, FatNum: Integer;
  TabLctA: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  FatNum   := QrSrvLFatCabCodigo.Value;
  //Filial := DmFatura.ObtemFilialDeEntidade(QrSrvLFatCabEmpresa.Value);
  Filial   := QrSrvLFatCabFilial.Value;
  TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  //
  Quitados := SrvL_PF.FaturamentosQuitados(TabLctA, VAR_FATID_3002, FatNum);
  //
  if Quitados > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(Quitados) +
    ' itens j� quitados que impedem a exclus�o deste faturamento');
    Exit;
  end;
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do faturamento e de TODOS seus lan�amentos financeiros atrelados?'
  ) = ID_YES then
  begin
    if UBloquetos.DesfazerBoletosFatID(QrLoc, Dmod.MyDB, VAR_FATID_3002,
      FatNum, TabLctA) then
    begin
      Screen.Cursor := crHourGlass;
      try
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
        'DELETE FROM ' + TabLctA,
        'WHERE FatID=' + Geral.FF0(VAR_FATID_3002),
        'AND FatNum=' + Geral.FF0(FatNum),
        '']);
        //
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
        'DELETE FROM lctfatref',
        'WHERE Controle=' + Geral.FF0(FatNum),
        'AND FatID=' + Geral.FF0(VAR_FATID_3002),
        '']);
        //
        (* N�o tem m�ltiplos faturamentos na mesma OS!
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
        'DELETE FROM ? ',
        'WHERE Controle=' + Geral.FF0(FatNum),
        '']);
        *)
        //
        AtualizaTotaisSrvlFatCab(FatNum);
        LocCod(FatNum, FatNum);
        //
        SrvL_PF.ReopenLctFatRef(QrLctFatRef, FatNum, 0, VAR_FATID_3002);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmSrvLFatCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormSrvLFatOpe(stUpd);
end;

procedure TFmSrvLFatCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmSrvLFatCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSrvLFatCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSrvLFatCab.ItsExclui1Click(Sender: TObject);
const
  SrvLFatCab = 0;
var
  Codigo: Integer;
  Controles: String;
  Qry: TmySQLQuery;
begin
  Controles :=
    MyObjects.GetSelecionadosBookmark_Int1_ToStr(Self, TDBGrid(DGDados), 'Controle');
  if MyObjects.FIC(Controles = '', nil, 'Nenhum item foi selecionado!') then
    Exit;
  //
  if Geral.MB_Pergunta('Confirma a retirada dos itens selecionados?') = ID_YES then
  begin
    Codigo := QrSrvLFatCabCodigo.Value;
    (*
    // SQL ao inverso da adi��o!! primeiro 'srvlosopedsp' e s� depois 'srvlosopegrn'
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopedsp', False, [
    'FrmCtbDspFat'], [
    CO_JOKE_SQL], [
    FrmCtbDspFat], [
    'Controle IN (' + Controles + ') AND FrmCtbDspFat=0'], True) then
    begin
    *)
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopegrn', False, [
      'SrvLFatCab'], [
      CO_JOKE_SQL], [
      SrvLFatCab], [
      'Controle IN (' + Controles + ')'], True) then
      begin
        SrvL_PF.TotalizaSrvlFatCab(Codigo);
        LocCod(Codigo, Codigo);
      end;
    (*
    end;
    *)
  end;
end;

(*
procedure TFmSrvLFatCab.ReopenSrvLFatOpe(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLFatOpe, Dmod.MyDB, [
  'SELECT * ',
  'FROM srvlfatope ',
  'WHERE Codigo=' + Geral.FF0(QrSrvLFatCabCodigo.Value),
  '']);
  //
  QrSrvLFatOpe.Locate('Controle', Controle, []);
end;
*)

procedure TFmSrvLFatCab.Removecondiaodepagamentoatual1Click(Sender: TObject);
begin
  if QrLctFatRef.State <> dsInactive then
  begin
    if QrLctFatRef.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak + 'Motivo: Existe faturamento para este item!');
      Exit;
    end;
  end;

  if Geral.MB_Pergunta('Confirma a exclus�o da condi��o de pagamento "' +
    QrSrvLFatPrzNO_CONDICAO.Value + '"?') = ID_YES
  then
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrSrvLFatPrz, 'SrvLFatPrz',
      ['Controle'], ['Controle'], True);
end;

procedure TFmSrvLFatCab.ReopenSrvLOSOpeGrn(Controle: Integer);
var
  ATT_FrmFtrCobr, ATT_FrmFtrPaga, ATT_FrmRatPaga: String;
begin
  ATT_FrmFtrCobr := dmkPF.ArrayToTexto('oog.FrmFtrCobr', 'NO_FrmFtrCobr',
    pvPos, True, sPrmFatorValor);
  ATT_FrmFtrPaga := dmkPF.ArrayToTexto('oog.FrmFtrPaga', 'NO_FrmFtrPaga',
    pvPos, True, sPrmFatorValor);
  ATT_FrmRatPaga := dmkPF.ArrayToTexto('oog.FrmRatPaga', 'NO_FrmRatPaga',
    pvPos, True, sPrmFormaRateio);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLOSOpeGrn, Dmod.MyDB, [
  'SELECT slc.Nome NO_SrvLCad, prm.Nome NO_SrvLPrmCad,  ',
  ATT_FrmFtrCobr,
  ATT_FrmFtrPaga,
  ATT_FrmRatPaga,
  'umc.Sigla SIGLA_UMC, ump.Sigla SIGLA_UMP, ',
  'aec.Nome NO_AgeEquiCab, oog.*  ',
  'FROM srvlosopegrn oog ',
  'LEFT JOIN srvlcad slc ON slc.Codigo=oog.SrvLCad ',
  'LEFT JOIN srvlprmcad prm ON prm.Codigo=oog.SrvLPrmCad ',
  'LEFT JOIN ageeqicab aec ON oog.AgeEqiCab=aec.Codigo ',
  'LEFT JOIN unidmed umc ON umc.Codigo=oog.UnidMedCobr ',
  'LEFT JOIN unidmed ump ON ump.Codigo=oog.UnidMedPaga ',
  'WHERE oog.SrvLFatCab=' + Geral.FF0(QrSrvLFatCabCodigo.Value),
  '']);
  //
  QrSrvLOSOpeGrn.Locate('Controle', Controle, []);
end;

procedure TFmSrvLFatCab.DBGSrvLFatPrzCellClick(Column: TColumn);
var
  Escolhido, Codigo, Controle: Integer;
begin
  if Column.FieldName = CO_FldEscolhido then
  begin
    if QrSrvLFatPrzEscolhido.Value = 0 then
      Escolhido := 1
    else
      Escolhido := 0;
    //
    Codigo   := QrSrvLFatPrzCodigo.Value;
    Controle := QrSrvLFatPrzControle.Value;
    //
    // Definir todas as condi��es da OS como n�o escolhidas
    if Escolhido = 1 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlfatprz', False, [
      'Escolhido'], ['Codigo'], [0], [Codigo], True);
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlfatprz', False, [
    'Escolhido'], [
    'Controle'], [
    Escolhido
    ], [
    Controle], True) then
    begin
      SrvL_PF.AtualizaTotaisSrvLOSCab(Codigo);
      LocCod(Codigo, Codigo);
      QrSrvLFatPrz.Locate('Controle', Controle, []);
    end;
  end;
end;

procedure TFmSrvLFatCab.DBGSrvLFatPrzColEnter(Sender: TObject);
begin
  if DBGSrvLFatPrz.Columns[THackDBGrid(DBGSrvLFatPrz).Col -1].FieldName = CO_FldEscolhido then
    DBGSrvLFatPrz.Options := DBGSrvLFatPrz.Options - [dgEditing] else
    DBGSrvLFatPrz.Options := DBGSrvLFatPrz.Options + [dgEditing];
end;

procedure TFmSrvLFatCab.DBGSrvLFatPrzDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Escolhido' then
    MeuVCLSkin.DrawGrid(DBGSrvLFatPrz, Rect, 1, QrSrvLFatPrzEscolhido.Value);
end;

procedure TFmSrvLFatCab.DBGSrvLFatPrzMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMFat, DBGSrvLFatPrz, X,Y);
end;

procedure TFmSrvLFatCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSrvLFatCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSrvLFatCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSrvLFatCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSrvLFatCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSrvLFatCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLFatCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSrvLFatCabCodigo.Value;
  Close;
end;

procedure TFmSrvLFatCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormSrvLFatOpe(stIns);
end;

procedure TFmSrvLFatCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  DModG.HabilitaEdCBEmpresa(EdEmpresa, CBEmpresa, stUpd, EdCliente);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrSrvLFatCab, [PnDados],
  [PnEdita], TPDtHrIni, ImgTipo, 'srvlfatcab');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrSrvLFatCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
  //
end;

procedure TFmSrvLFatCab.BtConfirmaClick(Sender: TObject);
var
  Nome, DtHrIni, DtHrFim: String;
  Codigo, Empresa, Cliente, AnoMes, EntPagante: Integer;
  //ValTotCbr, ValTotRat: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  EntPagante     := EdEntPagante.ValueVariant;
  DtHrIni        := Geral.FDT_TP_Ed(TPDtHrIni.Date, EdDtHrIni.Text);
  DtHrFim        := Geral.FDT_TP_Ed(TPDtHrFim.Date, EdDtHrFim.Text);
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  AnoMes         := Geral.IMV(Geral.FDT(EdAnoMes.ValueVariant, 20));
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
  if MyObjects.FIC(AnoMes = 0, EdAnoMes, 'Defina uma a compet�ncia!') then
    Exit;
  //ValTotCbr      := ;
  //ValTotRat      := ;
  //
  Codigo := UMyMod.BPGS1I32('srvlfatcab', 'Codigo', '', '', tsPos,
    ImgTipo.SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'srvlfatcab', False, [
  'Nome', 'Empresa', 'Cliente',
  'AnoMes', (*'ValTotCbr', 'ValTotRat',*)
  'DtHrIni', 'DtHrFim', 'EntPagante'], [
  'Codigo'], [
  Nome, Empresa, Cliente,
  AnoMes, (*ValTotCbr, ValTotRat,*)
  DtHrIni, DtHrFim, EntPagante], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    SrvL_PF.TotalizaSrvlFatCab(Codigo);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmSrvLFatCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'srvlfatcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'srvlfatcab', 'Codigo');
end;

procedure TFmSrvLFatCab.BtFatClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMFat, BtFat);
end;

procedure TFmSrvLFatCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmSrvLFatCab.Adicionanovacondiodepagamento1Click(Sender: TObject);
begin
  SrvL_PF.MostraFormSrvLFatPrz(
    stIns, QrSrvLFatCab, QrSrvLFatPrz, 0, 0, 0,
    QrSrvLFatCabCodigo.Value, QrSrvLFatCabEmpresa.Value, DsSrvLFatCab);
end;

procedure TFmSrvLFatCab.AtualizaTotaisSrvlFatCab(Codigo: Integer);
var
  Qry: TmySQLQuery;
  Estatus, CondicaoPG, CartEmis, Genero, NumNF: Integer;
  ValLiqOkFat, ValDesToFat, ValOutToFat: Double;
  SerNF: String;
  DtaFimFat: TDateTime;
begin
  Qry  := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    SrvL_PF.ReopenLctFatRef(Qry, Codigo, 0, VAR_FATID_3002);
    //
    if Qry.RecordCount > 0 then
    begin
      Estatus := CO_BUG_STATUS_0800_FATURADO;
      //
      ValLiqOkFat := 0;
      ValDesToFat := 0;
      ValOutToFat := 0;
      //
      Qry.First;
      while not Qry.Eof do
      begin
        ValLiqOkFat := ValLiqOkFat + Qry.FieldByName('Valor').AsFloat;
        //
        Qry.Next;
      end;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlfatcab', False,
        ['Estatus', 'ValLiqOkFat'], ['Codigo'],
        [Estatus, ValLiqOkFat], [Codigo], True);
    end else
    begin
      Estatus     := 0;
      ValLiqOkFat := 0;
      ValDesToFat := 0;
      ValOutToFat := 0;
      CondicaoPg  := 0;
      DtaFimFat   := 0;
      CartEmis    := 0;
      Genero      := 0;
      SerNF       := '';
      NumNF       := 0;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlfatcab', False, [
        'Estatus', 'ValLiqOkFat',
        'ValDesToFat', 'DtaFimFat', 'ValOutToFat',
        'CondicaoPg', 'CartEmis', 'Genero', 'SerNF', 'NumNF'], [
        'Codigo'], [
        Estatus, ValLiqOkFat,
        ValDesToFat, DtaFimFat, ValOutToFat,
        CondicaoPg, CartEmis, Genero, SerNF, NumNF], [
        Codigo], True);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmSrvLFatCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmSrvLFatCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
  EdAnoMes.ValueVariant := DModG.ObtemAgora();
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntPagante, Dmod.MyDB);
end;

procedure TFmSrvLFatCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSrvLFatCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvLFatCab.SbImprimeClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSrvlFatCabImp, FmSrvlFatCabImp, afmoNegarComAviso) then
  begin
    FmSrvlFatCabImp.FSrvLFatCab           := QrSrvLFatCabCodigo.Value;
    FmSrvlFatCabImp.FEmpresa              := QrSrvLFatCabEmpresa.Value;
    FmSrvlFatCabImp.FCliente              := QrSrvLFatCabCliente.Value;
    FmSrvlFatCabImp.FNO_Empresa           := QrSrvLFatCabNO_EMPRESA.Value;
    FmSrvlFatCabImp.FNO_Cliente           := QrSrvLFatCabNO_Cliente.Value;
    FmSrvlFatCabImp.QrSrvLFatCab.SQL.Text := QrSrvLFatCab.SQL.Text;
    FmSrvlFatCabImp.QrSrvLFatCab.Params   := QrSrvLFatCab.Params;

    //FmSrvlFatCabImp.frxDsSrvLFatCab.DataSet   := QrSrvLFatCab;
    //FmSrvlFatCabImp.frxDsSrvLOSOpeGrn.DataSet := QrSrvLOSOpeGrn;
    //
    FmSrvlFatCabImp.ImprimeFatCab();
    //FmSrvlFatCabImp.ShowModal;
    //
    FmSrvlFatCabImp.Destroy;
  end;
end;

procedure TFmSrvLFatCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSrvLFatCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrSrvLFatCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvLFatCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSrvLFatCab.QrLctFatRefCalcFields(DataSet: TDataSet);
begin
  QrLctFatRefPARCELA.Value := QrLctFatRef.RecNo;
end;

procedure TFmSrvLFatCab.QrSrvLFatCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSrvLFatCab.QrSrvLFatCabAfterScroll(DataSet: TDataSet);
const
  Controle = 0;
var
  Codigo: Integer;
begin
  Codigo := QrSrvLFatCabCodigo.Value;
  //ReopenSrvLFatOpe(0);
  ReopenSrvLOSOpeGrn(0);
  SrvL_PF.ReopenXPrz(QrSrvLFatPrz, 'SrvLFatPrz', Codigo, Controle);
  SrvL_PF.ReopenLctFatRef(QrLctFatRef, QrSrvLFatCabCodigo.Value, 0, VAR_FATID_3002);
end;

procedure TFmSrvLFatCab.Fatura1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if Myobjects.FIC(QrSrvLFatCabEntPagante.Value = 0, nil,
    'Faturamento n�o realizado! Pagante n�o informado!')
  then
    Exit;
  //
  Codigo := QrSrvLFatCabCodigo.Value;
  //
  MostraFormSrvLFatEmi(stIns);
  //
  AtualizaTotaisSrvlFatCab(Codigo);
  LocCod(Codigo, Codigo);
  //
  SrvL_PF.ReopenLctFatRef(QrLctFatRef, Codigo, 0, VAR_FATID_3002);
end;

procedure TFmSrvLFatCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrSrvLFatCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmSrvLFatCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSrvLFatCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'srvlfatcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSrvLFatCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLFatCab.Gerabloqueto1Click(Sender: TObject);
var
  Entidade, Filial: Integer;
  TabLctA: String;
begin
  try
    Screen.Cursor         := crHourGlass;
    Gerabloqueto1.Enabled := False;
    //
    Filial   := QrSrvLFatCabFilial.Value;
    Entidade := QrSrvLFatCabEmpresa.Value;
    TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
    //
    UBloquetos.BloquetosParcelasByFatNum(Entidade, VAR_FATID_3002,
      QrSrvLFatCabCodigo.Value, QrSrvLFatCabCodigo.Value, TabLctA, True, PB1);
  finally
    Gerabloqueto1.Enabled := True;
    Screen.Cursor         := crDefault;
  end;
end;

procedure TFmSrvLFatCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrSrvLFatCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'srvlfatcab');
  //
  Agora := DModG.ObtemAgora();
  TPDtHrIni.Date := Trunc(Agora);
  EdDtHrIni.ValueVariant := Agora;
  EdAnoMes.ValueVariant := DModG.ObtemAgora();
  DModG.HabilitaEdCBEmpresa(EdEmpresa, CBEmpresa, stIns, EdCliente);
end;

function TFmSrvLFatCab.CondicaoPGSelecionado(): Integer;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Condicao ',
    'FROM srvlfatprz ',
    'WHERE Codigo=' + Geral.FF0(QrSrvLFatCabCodigo.Value),
    'AND Escolhido=1',
    ' ',
    '']);
    //
    Result := Dmod.QrAux.FieldByName('Condicao').AsInteger;
  finally
    Qry.Free;
  end;
end;

procedure TFmSrvLFatCab.QrSrvLFatCabBeforeClose(
  DataSet: TDataSet);
begin
  //QrSrvLFatOpe.Close;
  QrSrvLOSOpeGrn.Close;
  QrSrvLFatPrz.Close;
  QrLctFatRef.Close;
end;

procedure TFmSrvLFatCab.QrSrvLFatCabBeforeOpen(DataSet: TDataSet);
begin
  QrSrvLFatCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmSrvLFatCab.QrSrvLFatPrzCalcFields(DataSet: TDataSet);
var
  Valor, Orcam, Inval, Fator: Double;
  Parcelas: Integer;
begin
  Parcelas := QrSrvLFatPrzParcelas.Value;
  Fator := 1 - (QrSrvLFatPrzDescoPer.Value / 100);
  //
  Orcam := QrSrvLFatCabValLiqToFat.Value; // > Valor total do orcamento, mesmo se nao confirmado!
  Inval := 0;  //QrOSCabInvalServi.Value; // > Valor total nao confirmado para execucao!
  Valor := QrSrvLFatCabValLiqToFat.Value; // > Valor total confirmado para execucao!
  //
  QrSrvLFatPrzORC_COM_DESCO.Value := Orcam * Fator;
  QrSrvLFatPrzINV_COM_DESCO.Value := Inval * Fator;
  QrSrvLFatPrzVAL_COM_DESCO.Value := Valor * Fator;
  //
  if Parcelas > 0 then
  begin
    QrSrvLFatPrzORC_PARCELA.Value := QrSrvLFatPrzORC_COM_DESCO.Value / Parcelas;
    QrSrvLFatPrzINV_PARCELA.Value := QrSrvLFatPrzINV_COM_DESCO.Value / Parcelas;
    QrSrvLFatPrzVAL_PARCELA.Value := QrSrvLFatPrzVAL_COM_DESCO.Value / Parcelas;
  end else
  begin
    QrSrvLFatPrzORC_PARCELA.Value := 0;
    QrSrvLFatPrzINV_PARCELA.Value := 0;
    QrSrvLFatPrzVAL_PARCELA.Value := 0;
  end;
end;

end.

