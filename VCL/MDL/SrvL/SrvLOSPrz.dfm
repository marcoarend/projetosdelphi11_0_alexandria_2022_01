object FmSrvLOSPrz: TFmSrvLOSPrz
  Left = 339
  Top = 185
  Caption = 'LOC-SERVI-014 :: OS - Condi'#231#245'es de Pagamento'
  ClientHeight = 309
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 376
        Height = 32
        Caption = 'OS - Condi'#231#245'es de Pagamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 376
        Height = 32
        Caption = 'OS - Condi'#231#245'es de Pagamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 376
        Height = 32
        Caption = 'OS - Condi'#231#245'es de Pagamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 147
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 147
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 61
        Width = 784
        Height = 86
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 69
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label2: TLabel
            Left = 96
            Top = 0
            Width = 48
            Height = 13
            Caption = 'Condi'#231#227'o:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label1: TLabel
            Left = 16
            Top = 0
            Width = 61
            Height = 13
            Caption = 'ID condi'#231#227'o:'
            Color = clBtnFace
            Enabled = False
            ParentColor = False
          end
          object Label6: TLabel
            Left = 688
            Top = 0
            Width = 81
            Height = 13
            Caption = '% Desconto [F4]:'
            Color = clBtnFace
            ParentColor = False
          end
          object SpeedButton6: TSpeedButton
            Left = 662
            Top = 16
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton6Click
          end
          object EdCondicao: TdmkEditCB
            Left = 96
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'DesServico'
            UpdCampo = 'DesServico'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCondicaoChange
            DBLookupComboBox = CBCondicao
            IgnoraDBLookupComboBox = False
          end
          object CBCondicao: TdmkDBLookupComboBox
            Left = 152
            Top = 16
            Width = 508
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPediPrzCab
            TabOrder = 2
            dmkEditCB = EdCondicao
            QryCampo = 'DesServico'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdControle: TdmkEdit
            Left = 16
            Top = 16
            Width = 76
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object CkContinuar: TCheckBox
            Left = 564
            Top = 48
            Width = 213
            Height = 17
            Caption = 'Continuar inserindo ap'#243's a confirma'#231#227'o.'
            TabOrder = 3
          end
          object EdDescoPer: TdmkEdit
            Left = 688
            Top = 16
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '100'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'DescoPer'
            UpdCampo = 'DescoPer'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnKeyDown = EdDescoPerKeyDown
          end
          object CkEscolhido: TdmkCheckBox
            Left = 16
            Top = 48
            Width = 265
            Height = 17
            Caption = 'O pagante escolheu esta condi'#231#227'o de pagamento.'
            TabOrder = 5
            QryCampo = 'Escolheu'
            UpdCampo = 'Escolheu'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
        end
      end
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 61
        Align = alTop
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        object Label7: TLabel
          Left = 16
          Top = 12
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label9: TLabel
          Left = 16
          Top = 36
          Width = 35
          Height = 13
          Caption = 'Cliente:'
          Color = clBtnFace
          ParentColor = False
        end
        object DBEdCliente: TDBEdit
          Left = 56
          Top = 32
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Cliente'
          TabOrder = 1
        end
        object DBEdNO_Cliente: TDBEdit
          Left = 112
          Top = 32
          Width = 657
          Height = 21
          TabStop = False
          DataField = 'NO_CLIENTE'
          TabOrder = 2
        end
        object DBEdNome: TDBEdit
          Left = 116
          Top = 8
          Width = 653
          Height = 21
          TabStop = False
          DataField = 'Nome'
          TabOrder = 3
        end
        object DBEdCodigo: TDBEdit
          Left = 56
          Top = 8
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          TabOrder = 0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 195
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 239
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPediPrzCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppc.MaxDesco, ppc.Parcelas,'
      'ppc.Codigo, ppc.CodUsu, ppc.Nome'
      'FROM pediprzcab ppc'
      'LEFT JOIN pediprzemp ppe ON ppe.Codigo=ppc.Codigo'
      'WHERE ppe.Empresa=-11'
      'ORDER BY ppc.Nome')
    Left = 280
    Top = 48
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 280
    Top = 92
  end
  object VuCondicao: TdmkValUsu
    dmkEditCB = EdCondicao
    Panel = Panel3
    QryCampo = 'Condicao'
    UpdCampo = 'Condicao'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 24
    Top = 12
  end
end
