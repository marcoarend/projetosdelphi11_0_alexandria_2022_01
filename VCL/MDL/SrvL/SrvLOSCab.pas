unit SrvLOSCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, Vcl.ComCtrls,
  dmkEditDateTimePicker, dmkDBGrid, UnProjGroup_Consts, dmkCheckBox, frxClass,
  frxDBSet;

type
  THackDBGrid = class(TDBGrid);
  TFmSrvLOSCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrSrvLOSCab: TmySQLQuery;
    DsSrvLOSCab: TDataSource;
    QrSrvLOSEsc: TmySQLQuery;
    DsSrvLOSEsc: TDataSource;
    PMSrvLOSEsc: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMSrvLOSCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtSrvLOSOpeGrn: TBitBtn;
    QrSrvLOSCabCodigo: TIntegerField;
    QrSrvLOSCabNome: TWideStringField;
    QrSrvLOSCabSrvLPre: TIntegerField;
    QrSrvLOSCabSrvLCad: TIntegerField;
    QrSrvLOSCabCliente: TIntegerField;
    QrSrvLOSCabDtAbertura: TDateTimeField;
    QrSrvLOSCabDtEncerrad: TDateTimeField;
    QrSrvLOSCabLk: TIntegerField;
    QrSrvLOSCabDataCad: TDateField;
    QrSrvLOSCabDataAlt: TDateField;
    QrSrvLOSCabUserCad: TIntegerField;
    QrSrvLOSCabUserAlt: TIntegerField;
    QrSrvLOSCabAlterWeb: TSmallintField;
    QrSrvLOSCabAtivo: TSmallintField;
    QrSrvLOSCabNO_SrvLCad: TWideStringField;
    QrSrvLOSCabNO_CLIENTE: TWideStringField;
    QrSrvLOSCabDtEncerrad_TXT: TWideStringField;
    QrSrvLCad: TmySQLQuery;
    QrSrvLCadCodigo: TIntegerField;
    QrSrvLCadNome: TWideStringField;
    DsSrvLCad: TDataSource;
    BtSrvLOSOpeAge: TBitBtn;
    PMSrvLOSOpeGrn: TPopupMenu;
    IncluiNovaOperacao1: TMenuItem;
    AlteraOperacaoAtual1: TMenuItem;
    ExcluiOperacaoAtual1: TMenuItem;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    SBCliente: TSpeedButton;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENT: TWideStringField;
    QrClientesCIDADE: TWideStringField;
    QrClientesNOMEUF: TWideStringField;
    QrClientesCodUsu: TIntegerField;
    QrClientesIE: TWideStringField;
    DsClientes: TDataSource;
    EdSrvLPre: TdmkEditCB;
    CBSrvLPre: TdmkDBLookupComboBox;
    SbSrvLPre: TSpeedButton;
    EdSrvLCad: TdmkEditCB;
    CBSrvLCad: TdmkDBLookupComboBox;
    SbSrvLCad: TSpeedButton;
    QrSrvLPOCab: TmySQLQuery;
    DsSrvLPOCab: TDataSource;
    QrSrvLPOCabCodigo: TIntegerField;
    QrSrvLPOCabNome: TWideStringField;
    Label12: TLabel;
    Label13: TLabel;
    TPDtAbertura: TdmkEditDateTimePicker;
    EdDtAbertura: TdmkEdit;
    TPDtEncerrad: TdmkEditDateTimePicker;
    EdDtEncerrad: TdmkEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    QrSrvLOSOpeGrn: TmySQLQuery;
    DsSrvLOSOpeGrn: TDataSource;
    QrSrvLOSOpeGrnCodigo: TIntegerField;
    QrSrvLOSOpeGrnControle: TIntegerField;
    QrSrvLOSOpeGrnSrvLCad: TIntegerField;
    QrSrvLOSOpeGrnLk: TIntegerField;
    QrSrvLOSOpeGrnDataCad: TDateField;
    QrSrvLOSOpeGrnDataAlt: TDateField;
    QrSrvLOSOpeGrnUserCad: TIntegerField;
    QrSrvLOSOpeGrnUserAlt: TIntegerField;
    QrSrvLOSOpeGrnAlterWeb: TSmallintField;
    QrSrvLOSOpeGrnAtivo: TSmallintField;
    QrSrvLOSOpeGrnNome: TWideStringField;
    QrSrvLOSOpeGrnNO_SrvLCad: TWideStringField;
    BtSrvLOSOpeDsp: TBitBtn;
    PMSrvLOSOpeDsp: TPopupMenu;
    IncluiNovaDespesa1: TMenuItem;
    AlteraDespesaAtual1: TMenuItem;
    ExcluiDespesaAtual1: TMenuItem;
    QrSrvLOSOpeDsp: TmySQLQuery;
    DsSrvLOSOpeDsp: TDataSource;
    QrSrvLOSOpeGrnSrvLPrmCad: TIntegerField;
    PMOSOpeAge: TPopupMenu;
    IncluiOSAge1: TMenuItem;
    RemoveOSAge1: TMenuItem;
    QrSrvLOSOpeAge: TmySQLQuery;
    QrSrvLOSOpeAgeNO_AGENTE: TWideStringField;
    QrSrvLOSOpeAgeCodigo: TIntegerField;
    QrSrvLOSOpeAgeControle: TIntegerField;
    QrSrvLOSOpeAgeAgente: TIntegerField;
    QrSrvLOSOpeAgeResponsa: TSmallintField;
    DsSrvLOSOpeAge: TDataSource;
    QrSrvLOSOpeAgeConta: TIntegerField;
    QrSrvLOSOpeGrnNO_SrvLPrmCad: TWideStringField;
    QrSrvLOSOpeGrnNO_AgeEquiCab: TWideStringField;
    QrSrvLOSOpeGrnAgeEqiCab: TIntegerField;
    QrSrvLOSOpeGrnUnidMedCobr: TIntegerField;
    QrSrvLOSOpeGrnQtdRlzCobr: TFloatField;
    QrSrvLOSOpeGrnValFtrCobr: TFloatField;
    QrSrvLOSOpeGrnFrmFtrCobr: TSmallintField;
    QrSrvLOSOpeGrnUnidMedPaga: TIntegerField;
    QrSrvLOSOpeGrnQtdRlzPaga: TFloatField;
    QrSrvLOSOpeGrnValRatPaga: TFloatField;
    QrSrvLOSOpeGrnFrmRatPaga: TSmallintField;
    QrSrvLOSOpeGrnFrmFtrPaga: TSmallintField;
    QrSrvLOSOpeGrnValTotCobr: TFloatField;
    QrSrvLOSOpeGrnValFtrPaga: TFloatField;
    QrSrvLOSOpeGrnSIGLA_UMC: TWideStringField;
    QrSrvLOSOpeGrnSIGLA_UMP: TWideStringField;
    QrSrvLOSOpeGrnNO_FrmFtrCobr: TWideStringField;
    QrSrvLOSOpeGrnNO_FrmFtrPaga: TWideStringField;
    QrSrvLOSOpeGrnNO_FrmRatPaga: TWideStringField;
    Label18: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrSrvLOSCabEmpresa: TIntegerField;
    QrSrvLOSCabNO_EMPRESA: TWideStringField;
    QrSrvLOSOpeAgeLk: TIntegerField;
    QrSrvLOSOpeAgeDataCad: TDateField;
    QrSrvLOSOpeAgeDataAlt: TDateField;
    QrSrvLOSOpeAgeUserCad: TIntegerField;
    QrSrvLOSOpeAgeUserAlt: TIntegerField;
    QrSrvLOSOpeAgeAlterWeb: TSmallintField;
    QrSrvLOSOpeAgeAtivo: TSmallintField;
    QrSrvLOSOpeAgeValPremio: TFloatField;
    QrSrvLOSOpeDspNO_GENERO: TWideStringField;
    QrSrvLOSOpeDspNO_AGENTE: TWideStringField;
    QrSrvLOSOpeDspNO_FORNECE: TWideStringField;
    QrSrvLOSOpeDspCodigo: TIntegerField;
    QrSrvLOSOpeDspControle: TIntegerField;
    QrSrvLOSOpeDspConta: TIntegerField;
    QrSrvLOSOpeDspGenero: TIntegerField;
    QrSrvLOSOpeDspAgente: TIntegerField;
    QrSrvLOSOpeDspDebito: TFloatField;
    QrSrvLOSOpeDspLk: TIntegerField;
    QrSrvLOSOpeDspDataCad: TDateField;
    QrSrvLOSOpeDspDataAlt: TDateField;
    QrSrvLOSOpeDspUserCad: TIntegerField;
    QrSrvLOSOpeDspUserAlt: TIntegerField;
    QrSrvLOSOpeDspAlterWeb: TSmallintField;
    QrSrvLOSOpeDspAtivo: TSmallintField;
    QrSrvLOSOpeDspFornece: TIntegerField;
    QrSrvLOSOpeGrnValTotDesp: TFloatField;
    Panel6: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdNome: TdmkDBEdit;
    QrSrvLOSCabValTotCobr: TFloatField;
    QrSrvLOSCabValTotDesp: TFloatField;
    QrSrvLOSCabValRatPaga: TFloatField;
    Label1: TLabel;
    DBEdit10: TDBEdit;
    Label2: TLabel;
    DBEdit11: TDBEdit;
    Label3: TLabel;
    DBEdit12: TDBEdit;
    BtSrvLOSOpeAtrDef: TBitBtn;
    PMSrvLOSOpeAtrDef: TPopupMenu;
    IncluiSrvLOSOpeAtrDef1: TMenuItem;
    AlteraSrvLOSOpeAtrDef1: TMenuItem;
    ExcluiSrvLOSOpeAtrDef1: TMenuItem;
    QrSrvLOSOpeAtrDef: TmySQLQuery;
    QrSrvLOSOpeAtrDefID_Item: TIntegerField;
    QrSrvLOSOpeAtrDefID_Sorc: TIntegerField;
    QrSrvLOSOpeAtrDefAtrCad: TIntegerField;
    QrSrvLOSOpeAtrDefATRITS: TFloatField;
    QrSrvLOSOpeAtrDefCU_CAD: TIntegerField;
    QrSrvLOSOpeAtrDefCU_ITS: TLargeintField;
    QrSrvLOSOpeAtrDefAtrTxt: TWideStringField;
    QrSrvLOSOpeAtrDefNO_CAD: TWideStringField;
    QrSrvLOSOpeAtrDefNO_ITS: TWideStringField;
    QrSrvLOSOpeAtrDefAtrTyp: TSmallintField;
    DsSrvLOSOpeAtrDef: TDataSource;
    QrSrvLOSOpeAtrDefTABELA: TWideStringField;
    QrSrvLOSOpeDspFrmCtbDspFat: TSmallintField;
    QrSrvLOSOpeDspNO_FrmCtbDspFat: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel7: TPanel;
    DBGSrvLOSOpeGrn: TDBGrid;
    Splitter1: TSplitter;
    GroupBox10: TGroupBox;
    Splitter2: TSplitter;
    TabSheet2: TTabSheet;
    DBGSrvLOSPrz: TDBGrid;
    DsSrvLOSPrz: TDataSource;
    QrSrvLOSPrz: TmySQLQuery;
    QrSrvLOSPrzCodigo: TIntegerField;
    QrSrvLOSPrzControle: TIntegerField;
    QrSrvLOSPrzCondicao: TIntegerField;
    QrSrvLOSPrzNO_CONDICAO: TWideStringField;
    QrSrvLOSPrzDescoPer: TFloatField;
    QrSrvLOSPrzEscolhido: TSmallintField;
    QrSrvLOSPrzParcelas: TIntegerField;
    QrSrvLOSPrzVAL_COM_DESCO: TFloatField;
    QrSrvLOSPrzVAL_PARCELA: TFloatField;
    QrSrvLOSPrzORC_COM_DESCO: TFloatField;
    QrSrvLOSPrzORC_PARCELA: TFloatField;
    QrSrvLOSPrzINV_COM_DESCO: TFloatField;
    QrSrvLOSPrzINV_PARCELA: TFloatField;
    BtSrvLOSPrz: TBitBtn;
    PMSrvLOSPrz: TPopupMenu;
    OSPrz1_Inclui: TMenuItem;
    OSPrz1_Altera: TMenuItem;
    OSPrz1_Exclui: TMenuItem;
    QrSrvLOSOpeGrnDtaExeIni: TDateTimeField;
    QrSrvLOSOpeGrnDtaExeFim: TDateTimeField;
    QrSrvLOSOpeGrnDtaExeFim_TXT: TWideStringField;
    QrSrvLOSOpeGrnWrnLctDay: TSmallintField;
    QrSrvLOSOpeCPA: TmySQLQuery;
    DsSrvLOSOpeCPA: TDataSource;
    QrSrvLOSOpeCPANO_ValEverAut: TWideStringField;
    QrSrvLOSOpeCPACodigo: TIntegerField;
    QrSrvLOSOpeCPAControle: TIntegerField;
    QrSrvLOSOpeCPAConta: TIntegerField;
    QrSrvLOSOpeCPANivel4: TIntegerField;
    QrSrvLOSOpeCPADtaCompet: TDateField;
    QrSrvLOSOpeCPAHrInnDay: TTimeField;
    QrSrvLOSOpeCPAHrOutMid: TTimeField;
    QrSrvLOSOpeCPAHrInnMid: TTimeField;
    QrSrvLOSOpeCPAHrOutDay: TTimeField;
    QrSrvLOSOpeCPATemInterv: TSmallintField;
    QrSrvLOSOpeCPAFatorPrm: TFloatField;
    QrSrvLOSOpeCPAValPremio: TFloatField;
    QrSrvLOSOpeCPALk: TIntegerField;
    QrSrvLOSOpeCPADataCad: TDateField;
    QrSrvLOSOpeCPADataAlt: TDateField;
    QrSrvLOSOpeCPAUserCad: TIntegerField;
    QrSrvLOSOpeCPAUserAlt: TIntegerField;
    QrSrvLOSOpeCPAAlterWeb: TSmallintField;
    QrSrvLOSOpeCPAAtivo: TSmallintField;
    QrSrvLOSOpeCPAValEverAut: TSmallintField;
    QrSrvLOSOpeCPANO_TemInterv: TWideStringField;
    QrSrvLOSOpeGrnAgeEqiCfg: TIntegerField;
    QrSrvLOSCabValTotPaga: TFloatField;
    QrSrvLOSOpeGrnValTotPaga: TFloatField;
    QrSrvLOSCabValTotLiqu: TFloatField;
    Label4: TLabel;
    DBEdit13: TDBEdit;
    Label5: TLabel;
    DBEdit14: TDBEdit;
    QrSrvLOSCabPerTotLiqu: TFloatField;
    DBGOSAge: TDBGrid;
    GroupBox1: TGroupBox;
    DBGrid2: TDBGrid;
    Splitter3: TSplitter;
    GroupBox2: TGroupBox;
    DBGSrvLOSOpeAtrDef: TDBGrid;
    GroupBox3: TGroupBox;
    DBGSrvLOSOpeDsp: TDBGrid;
    BtSrvLOSOpeCPA: TBitBtn;
    PMSrvLOSOpeCPA: TPopupMenu;
    Incluinovajornada1: TMenuItem;
    Alterajornadaatual1: TMenuItem;
    Excluijornadaatual1: TMenuItem;
    QrSrvLOSOpeGrnAutorizado: TSmallintField;
    QrEstatusOSs: TmySQLQuery;
    QrEstatusOSsCodigo: TIntegerField;
    QrEstatusOSsNome: TWideStringField;
    DsEstatusOSs: TDataSource;
    Label6: TLabel;
    EdEstatus: TdmkEditCB;
    CBEstatus: TdmkDBLookupComboBox;
    QrSrvLOSCabEstatus: TIntegerField;
    QrSrvLOSCabNO_ESTATUS: TWideStringField;
    Label8: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    QrSrvLOSOpeGrnDtaExeIni_TXT: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSrvLOSCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSrvLOSCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrSrvLOSCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMSrvLOSCabPopup(Sender: TObject);
    procedure PMSrvLOSEscPopup(Sender: TObject);
    procedure QrSrvLOSCabBeforeClose(DataSet: TDataSet);
    procedure BtSrvLOSOpeAgeClick(Sender: TObject);
    procedure BtSrvLOSOpeGrnClick(Sender: TObject);
    procedure ExcluiOperacaoAtual1Click(Sender: TObject);
    procedure AlteraOperacaoAtual1Click(Sender: TObject);
    procedure IncluiNovaOperacao1Click(Sender: TObject);
    procedure SBClienteClick(Sender: TObject);
    procedure SbSrvLPreClick(Sender: TObject);
    procedure SbSrvLCadClick(Sender: TObject);
    procedure PMSrvLOSOpeGrnPopup(Sender: TObject);
    procedure BtSrvLOSOpeDspClick(Sender: TObject);
    procedure IncluiNovaDespesa1Click(Sender: TObject);
    procedure AlteraDespesaAtual1Click(Sender: TObject);
    procedure ExcluiDespesaAtual1Click(Sender: TObject);
    procedure PMSrvLOSOpeDspPopup(Sender: TObject);
    procedure IncluiOSAge1Click(Sender: TObject);
    procedure RemoveOSAge1Click(Sender: TObject);
    procedure QrSrvLOSOpeGrnBeforeClose(DataSet: TDataSet);
    procedure QrSrvLOSOpeGrnAfterScroll(DataSet: TDataSet);
    procedure DBGOSAgeCellClick(Column: TColumn);
    procedure DBGOSAgeColEnter(Sender: TObject);
    procedure DBGOSAgeColExit(Sender: TObject);
    procedure DBGOSAgeDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure IncluiSrvLOSAtrDef1Click(Sender: TObject);
    procedure AlteraSrvLOSAtrDef1Click(Sender: TObject);
    procedure ExcluiSrvLOSAtrDef1Click(Sender: TObject);
    procedure PMSrvLOSOpeAtrDefPopup(Sender: TObject);
    procedure BtSrvLOSOpeAtrDefClick(Sender: TObject);
    procedure DBGSrvLOSPrzCellClick(Column: TColumn);
    procedure DBGSrvLOSPrzColEnter(Sender: TObject);
    procedure DBGSrvLOSPrzDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGSrvLOSPrzMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OSPrz1_IncluiClick(Sender: TObject);
    procedure OSPrz1_AlteraClick(Sender: TObject);
    procedure OSPrz1_ExcluiClick(Sender: TObject);
    procedure BtSrvLOSPrzClick(Sender: TObject);
    procedure PMSrvLOSPrzPopup(Sender: TObject);
    procedure QrSrvLOSOpeAgeBeforeClose(DataSet: TDataSet);
    procedure QrSrvLOSOpeAgeAfterScroll(DataSet: TDataSet);
    procedure BtSrvLOSOpeCPAClick(Sender: TObject);
    procedure Incluinovajornada1Click(Sender: TObject);
    procedure Alterajornadaatual1Click(Sender: TObject);
    procedure Excluijornadaatual1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure DBGSrvLOSOpeGrnCellClick(Column: TColumn);
    procedure DBGSrvLOSOpeGrnColEnter(Sender: TObject);
    procedure DBGSrvLOSOpeGrnColExit(Sender: TObject);
    procedure DBGSrvLOSOpeGrnDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure PMSrvLOSOpeCPAPopup(Sender: TObject);
    procedure PMOSOpeAgePopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure MostraFormSrvLOSEsc(SQLType: TSQLType);
    procedure MostraFormSrvLOSOpeGrn(SQLType: TSQLType);
    procedure MostraFormSrvLOSOpeDsp(SQLType: TSQLType);
    procedure MostraFormSrvLOSOpeAtrDef(SQLType: TSQLType);
    procedure MostraFormSrvLOSOpeAge(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenEscada(Codigo, Controle, Conta, Nivel4: Integer);
    procedure ReopenSrvLOSEsc(Controle: Integer);
    procedure ReopenSrvLOSOpeGrn(Controle: Integer);
    procedure ReopenSrvLOSOpeAge(Conta: Integer);
    procedure ReopenSrvLOSOpeDsp(Conta: Integer);
    procedure ReopenSrvLOSOpeAtrDef(ID_Item: Integer);
    procedure ReopenSrvLOSOpeCPA(Nivel4: Integer);

  end;

var
  FmSrvLOSCab: TFmSrvLOSCab;
const
  FFormatFloat = '00000';
  CO_FldRespo  = 'Responsa';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, UnSrvL_PF,
  SrvLOSEsc, SrvLOSOpeGrn, UnEmpg_PF, MyVCLSkin, AppListas, SrvLOSOpeDsp,
  CfgAtributos, SrvLOSOpeCPA, SrvlOSCabImp;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSrvLOSCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSrvLOSCab.MostraFormSrvLOSOpeAge(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmSrvLOSOpeCPA, FmSrvLOSOpeCPA, afmoNegarComAviso) then
  begin
    FmSrvLOSOpeCPA.ImgTipo.SQLType := SQLType;
    FmSrvLOSOpeCPA.FQrCab := QrSrvLOSOpeAge;
    FmSrvLOSOpeCPA.FDsCab := DsSrvLOSOpeAge;
    //FmSrvLOSOpeCPA.FQrIts := QrSrvLOSOpeCPA;
    FmSrvLOSOpeCPA.FCodigo   := QrSrvLOSCabCodigo.Value;
    FmSrvLOSOpeCPA.FControle := QrSrvLOSOpeGrnControle.Value;
    FmSrvLOSOpeCPA.FConta    := QrSrvLOSOpeAgeConta.Value;
    if SQLType = stIns then
    begin
      FmSrvLOSOpeCPA.TPDtaCompet.Date         := Date;
      if QrSrvLOSOpeCPA.RecordCount > 0 then
      begin
        FmSrvLOSOpeCPA.EdHrInnDay.ValueVariant  := QrSrvLOSOpeCPAHrInnDay.Value;
        FmSrvLOSOpeCPA.EdHrOutDay.ValueVariant  := QrSrvLOSOpeCPAHrOutDay.Value;
        FmSrvLOSOpeCPA.EdHrOutMid.ValueVariant  := QrSrvLOSOpeCPAHrOutMid.Value;
        FmSrvLOSOpeCPA.EdHrInnMid.ValueVariant  := QrSrvLOSOpeCPAHrInnMid.Value;
        //
        FmSrvLOSOpeCPA.CkTemInterv.Checked      := Geral.IntToBool(QrSrvLOSOpeCPATemInterv.Value);
        FmSrvLOSOpeCPA.EdValPremio.ValueVariant := QrSrvLOSOpeCPAValPremio.Value;
      end;
    end else
    begin
      FmSrvLOSOpeCPA.EdNivel4.ValueVariant    := QrSrvLOSOpeCPANivel4.Value;
      //
      FmSrvLOSOpeCPA.EdHrInnDay.ValueVariant  := QrSrvLOSOpeCPAHrInnDay.Value;
      FmSrvLOSOpeCPA.EdHrOutDay.ValueVariant  := QrSrvLOSOpeCPAHrOutDay.Value;
      FmSrvLOSOpeCPA.EdHrOutMid.ValueVariant  := QrSrvLOSOpeCPAHrOutMid.Value;
      FmSrvLOSOpeCPA.EdHrInnMid.ValueVariant  := QrSrvLOSOpeCPAHrInnMid.Value;
      //
      FmSrvLOSOpeCPA.CkTemInterv.Checked      := Geral.IntToBool(QrSrvLOSOpeCPATemInterv.Value);
      FmSrvLOSOpeCPA.TPDtaCompet.Date         := QrSrvLOSOpeCPADtaCompet.Value;
      FmSrvLOSOpeCPA.EdValPremio.ValueVariant := QrSrvLOSOpeCPAValPremio.Value;
    end;
    FmSrvLOSOpeCPA.ShowModal;
    FmSrvLOSOpeCPA.Destroy;
  end;
end;

procedure TFmSrvLOSCab.MostraFormSrvLOSOpeAtrDef(SQLType: TSQLType);
begin
  UnCfgAtributos.InsAltAtrDef_ITS(QrSrvLOSOpeGrnControle.Value,
    QrSrvLOSOpeAtrDefID_Item.Value, 'srvlosopeatrdef', 'srvlosopeatrcad',
    'srvlosopeatrits', 'srvlosopeatrtxt', SQLType, QrSrvLOSOpeAtrDefAtrCad.Value,
    Trunc(QrSrvLOSOpeAtrDefAtrIts.Value), QrSrvLOSOpeAtrDefAtrTxt.Value,
    QrSrvLOSOpeAtrDef, QrSrvLOSOpeAtrDef, True);
end;

procedure TFmSrvLOSCab.MostraFormSrvLOSEsc(SQLType: TSQLType);
begin
{
  if DBCheck.CriaFm(TFmSrvLOSEsc, FmSrvLOSEsc, afmoNegarComAviso) then
  begin
    FmSrvLOSEsc.ImgTipo.SQLType := SQLType;
    FmSrvLOSEsc.FQrCab := QrSrvLOSCab;
    FmSrvLOSEsc.FDsCab := DsSrvLOSCab;
    FmSrvLOSEsc.FQrIts := QrSrvLOSEsc;
    if SQLType = stIns then
      //
    else
    begin
      FmSrvLOSEsc.EdControle.ValueVariant := QrSrvLOSEscControle.Value;
      //
    end;
    FmSrvLOSEsc.ShowModal;
    FmSrvLOSEsc.Destroy;
  end;
}
end;

procedure TFmSrvLOSCab.MostraFormSrvLOSOpeGrn(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmSrvLOSOpeGrn, FmSrvLOSOpeGrn, afmoNegarComAviso) then
  begin
    FmSrvLOSOpeGrn.ImgTipo.SQLType := SQLType;
    FmSrvLOSOpeGrn.FQrCab          := QrSrvLOSCab;
    FmSrvLOSOpeGrn.FDsCab          := DsSrvLOSCab;
    FmSrvLOSOpeGrn.FQrIts          := QrSrvLOSOpeGrn;
    //
    if SQLType = stIns then
    begin
      FmSrvLOSOpeGrn.EdSrvLCad.ValueVariant   := QrSrvLOSCabSrvLCad.Value;
      FmSrvLOSOpeGrn.CBSrvLCad.KeyValue       := QrSrvLOSCabSrvLCad.Value;
      FmSrvLOSOpeGrn.EdAgeEqiCab.ValueVariant := -1;
      FmSrvLOSOpeGrn.CBAgeEqiCab.KeyValue     := -1;
      FmSrvLOSOpeGrn.CkContinuar.Visible      := True;
      FmSrvLOSOpeGrn.CkContinuar.Checked      := False;
    end else
    begin
      FmSrvLOSOpeGrn.CkContinuar.Visible := False;
      FmSrvLOSOpeGrn.CkContinuar.Checked := False;
      //
      FmSrvLOSOpeGrn.EdControle.ValueVariant := QrSrvLOSOpeGrnControle.Value;
      //
      FmSrvLOSOpeGrn.EdSrvLCad.ValueVariant    := QrSrvLOSOpeGrnSrvLCad.Value;
      FmSrvLOSOpeGrn.CBSrvLCad.KeyValue        := QrSrvLOSOpeGrnSrvLCad.Value;
      //
      FmSrvLOSOpeGrn.EdSrvLPrmCad.ValueVariant := QrSrvLOSOpeGrnSrvLPrmCad.Value;
      FmSrvLOSOpeGrn.CBSrvLPrmCad.KeyValue     := QrSrvLOSOpeGrnSrvLPrmCad.Value;
      //
      FmSrvLOSOpeGrn.EdAgeEqiCab.ValueVariant  := QrSrvLOSOpeGrnAgeEqiCab.Value;
      FmSrvLOSOpeGrn.CBAgeEqiCab.KeyValue      := QrSrvLOSOpeGrnAgeEqiCab.Value;
      //
      FmSrvLOSOpeGrn.EdNome.ValueVariant       := QrSrvLOSOpeGrnNome.Value;
      //
      FmSrvLOSOpeGrn.RGAutorizado.ItemIndex    := QrSrvLOSOpeGrnAutorizado.Value;
      //
      if (QrSrvLOSOpeGrnQtdRlzCobr.Value > 0)
      or (QrSrvLOSOpeGrnQtdRlzPaga.Value > 0) then
      begin
        FmSrvLOSOpeGrn.CkExecucao.Checked := True;
        //
        FmSrvLOSOpeGrn.EdQtdRlzCobr.ValueVariant  := QrSrvLOSOpeGrnQtdRlzCobr.Value;
        FmSrvLOSOpeGrn.EdUnidMedCobr.ValueVariant := QrSrvLOSOpeGrnUnidMedCobr.Value;
        //FmSrvLOSOpeGrn.EdSiglaCobr.ValueVariant   := QrSrvLOSOpeGrnSiglaCobr.Value;
        FmSrvLOSOpeGrn.CBUnidMedCobr.KeyValue     := QrSrvLOSOpeGrnUnidMedCobr.Value;
        FmSrvLOSOpeGrn.EdValFtrCobr.ValueVariant  := QrSrvLOSOpeGrnValFtrCobr.Value;
        FmSrvLOSOpeGrn.RGFrmFtrCobr.ItemIndex     := QrSrvLOSOpeGrnFrmFtrCobr.Value;
        FmSrvLOSOpeGrn.EdValTotCobr.ValueVariant  := QrSrvLOSOpeGrnValTotCobr.Value;
        //
        FmSrvLOSOpeGrn.EdQtdRlzPaga.ValueVariant  := QrSrvLOSOpeGrnQtdRlzPaga.Value;
        FmSrvLOSOpeGrn.EdUnidMedPaga.ValueVariant := QrSrvLOSOpeGrnUnidMedPaga.Value;
        //FmSrvLOSOpeGrn.EdSiglaPaga.ValueVariant   := QrSrvLOSOpeGrnSiglaPaga.Value;
        FmSrvLOSOpeGrn.CBUnidMedPaga.KeyValue     := QrSrvLOSOpeGrnUnidMedPaga.Value;
        FmSrvLOSOpeGrn.EdValFtrPaga.ValueVariant  := QrSrvLOSOpeGrnValFtrPaga.Value;
        FmSrvLOSOpeGrn.RGFrmFtrPaga.ItemIndex     := QrSrvLOSOpeGrnFrmFtrPaga.Value;
        FmSrvLOSOpeGrn.RGFrmRatPaga.ItemIndex     := QrSrvLOSOpeGrnFrmRatPaga.Value;
        FmSrvLOSOpeGrn.EdValRatPaga.ValueVariant  := QrSrvLOSOpeGrnValRatPaga.Value;
        //
        FmSrvLOSOpeGrn.TPDtaExeIni.Date           := QrSrvLOSOpeGrnDtaExeIni.Value;
        FmSrvLOSOpeGrn.EdDtaExeIni.ValueVariant   := QrSrvLOSOpeGrnDtaExeIni.Value;
        FmSrvLOSOpeGrn.TPDtaExeFim.Date           := QrSrvLOSOpeGrnDtaExeFim.Value;
        FmSrvLOSOpeGrn.EdDtaExeFim.ValueVariant   := QrSrvLOSOpeGrnDtaExeFim.Value;
      end;
(*
      FmSrvLOSOpeGrn.DataIni        := ;
      FmSrvLOSOpeGrn.DataFim        := ;
      FmSrvLOSOpeGrn.SrvLFatCab     := ;
      FmSrvLOSOpeGrn.SrvLHonCab     := ;
      FmSrvLOSOpeGrn.WrnLctDay      := ;
      FmSrvLOSOpeGrn.AgeEqiCfg      := ;
      FmSrvLOSOpeGrn.ValTotDesp     := ;
      FmSrvLOSOpeGrn.ValTotPaga     := ;
      FmSrvLOSOpeGrn.DtaLstCPA      := ;
*)
    end;
    FmSrvLOSOpeGrn.ShowModal;
    FmSrvLOSOpeGrn.Destroy;
  end;
end;

procedure TFmSrvLOSCab.OSPrz1_AlteraClick(Sender: TObject);
begin
  SrvL_PF.MostraFormSrvLOSPrz(
    stUpd, QrSrvLOSCab, QrSrvLOSPrz, (*QrSrvLOSOpeGrnLCabOrcamTotal.Value*)0, 0, 0,
    QrSrvLOSCabCodigo.Value, QrSrvLOSCabEmpresa.Value, DsSrvLOSCab);
end;

procedure TFmSrvLOSCab.OSPrz1_ExcluiClick(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da condi��o de pagamento "' +
  QrSrvLOSPrzNO_CONDICAO.Value + '"?') =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, QrSrvLOSPrz, 'SrvLOSPrz',
  ['Controle'], ['Controle'], True);
end;

procedure TFmSrvLOSCab.OSPrz1_IncluiClick(Sender: TObject);
begin
  //OSApp_PF.MostraFormOSPrz(stIns, QrOSCab, QrSrvLOSPrz);
  SrvL_PF.MostraFormSrvLOSPrz(
    stIns, QrSrvLOSCab, QrSrvLOSPrz, (*QrOSCabOrcamTotal.Value*)0, 0, 0,
    QrSrvLOSCabCodigo.Value, QrSrvLOSCabEmpresa.Value, DsSrvLOSCab);
end;

procedure TFmSrvLOSCab.MostraFormSrvLOSOpeDsp(SQLType: TSQLType);
var
  Codigo, Conta: Integer;
begin
  Codigo := QrSrvLOSCabCodigo.Value;
  //
  if DBCheck.CriaFm(TFmSrvLOSOpeDsp, FmSrvLOSOpeDsp, afmoNegarComAviso) then
  begin
    FmSrvLOSOpeDsp.ImgTipo.SQLType := SQLType;
    FmSrvLOSOpeDsp.FQrCab := QrSrvLOSOpeGrn;
    FmSrvLOSOpeDsp.FDsCab := DsSrvLOSOpeGrn;
    FmSrvLOSOpeDsp.FQrIts := QrSrvLOSOpeDsp;
    FmSrvLOSOpeDsp.FCliente := QrSrvLOSCabCliente.Value;
    if SQLType = stIns then
      //
    else
    begin
      FmSrvLOSOpeDsp.EdConta.ValueVariant       := QrSrvLOSOpeDspConta.Value;
      //
      FmSrvLOSOpeDsp.EdGenero.ValueVariant    := QrSrvLOSOpeDspGenero.Value;
      FmSrvLOSOpeDsp.CBGenero.KeyValue        := QrSrvLOSOpeDspGenero.Value;
      FmSrvLOSOpeDsp.EdAgente.ValueVariant    := QrSrvLOSOpeDspAgente.Value;
      FmSrvLOSOpeDsp.CBAgente.KeyValue        := QrSrvLOSOpeDspAgente.Value;
      FmSrvLOSOpeDsp.EdFornece.ValueVariant   := QrSrvLOSOpeDspFornece.Value;
      FmSrvLOSOpeDsp.CBFornece.KeyValue       := QrSrvLOSOpeDspFornece.Value;
      FmSrvLOSOpeDsp.EdDebito.ValueVariant    := QrSrvLOSOpeDspDebito.Value;
      FmSrvLOSOpeDsp.RGFrmCtbDspFat.ItemIndex := QrSrvLOSOpeDspFrmCtbDspFat.Value;
    end;
    FmSrvLOSOpeDsp.ShowModal;
    Conta := FmSrvLOSOpeDsp.FLastConta;
    FmSrvLOSOpeDsp.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmSrvLOSCab.PMSrvLOSOpeAtrDefPopup(Sender: TObject);
var
  Encerr: Boolean;
begin
  Encerr := QrSrvLOSCabDtEncerrad_TXT.Value <> '';
  //
  if not Encerr then
  begin
    MyObjects.HabilitaMenuItemItsIns(IncluiSrvLOSOpeAtrDef1, QrSrvLOSOpeGrn);
    MyObjects.HabilitaMenuItemItsUpd(AlteraSrvLOSOpeAtrDef1, QrSrvLOSOpeAtrDef);
    MyObjects.HabilitaMenuItemItsDel(ExcluiSrvLOSOpeAtrDef1, QrSrvLOSOpeAtrDef);
  end else
  begin
    IncluiSrvLOSOpeAtrDef1.Enabled := False;
    AlteraSrvLOSOpeAtrDef1.Enabled := False;
    ExcluiSrvLOSOpeAtrDef1.Enabled := False;
  end;
end;

procedure TFmSrvLOSCab.PMSrvLOSOpeCPAPopup(Sender: TObject);
var
  Encerr, Enab, Enab2: Boolean;
begin
  Encerr := QrSrvLOSCabDtEncerrad_TXT.Value <> '';
  //
  if not Encerr then
  begin
    Enab  := (QrSrvLOSOpeAge.State <> dsInactive) and (QrSrvLOSOpeAge.RecordCount > 0);
    Enab2 := (QrSrvLOSOpeCPA.State <> dsInactive) and (QrSrvLOSOpeCPA.RecordCount > 0);
    //
    Incluinovajornada1.Enabled  := Enab;
    Alterajornadaatual1.Enabled := Enab and Enab2;
    Excluijornadaatual1.Enabled := Enab and Enab2;
  end else
  begin
    Incluinovajornada1.Enabled  := False;
    Alterajornadaatual1.Enabled := False;
    Excluijornadaatual1.Enabled := False;
  end;
end;

procedure TFmSrvLOSCab.PMOSOpeAgePopup(Sender: TObject);
var
  Encerr, Enab, Enab2: Boolean;
begin
  Encerr := QrSrvLOSCabDtEncerrad_TXT.Value <> '';
  //
  if not Encerr then
  begin
    Enab  := (QrSrvLOSOpeGrn.State <> dsInactive) and (QrSrvLOSOpeGrn.RecordCount > 0);
    Enab2 := (QrSrvLOSOpeAge.State <> dsInactive) and (QrSrvLOSOpeAge.RecordCount > 0);
    //
    IncluiOSAge1.Enabled := Enab;
    RemoveOSAge1.Enabled := Enab and Enab2;
  end else
  begin
    IncluiOSAge1.Enabled := False;
    RemoveOSAge1.Enabled := False;
  end;
end;

procedure TFmSrvLOSCab.PMSrvLOSCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrSrvLOSCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrSrvLOSCab, QrSrvLOSEsc);
end;

procedure TFmSrvLOSCab.PMSrvLOSEscPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrSrvLOSCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrSrvLOSEsc);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrSrvLOSEsc);
end;

procedure TFmSrvLOSCab.PMSrvLOSOpeGrnPopup(Sender: TObject);
var
  Encerr: Boolean;
begin
  Encerr := QrSrvLOSCabDtEncerrad_TXT.Value <> '';
  //
  if not Encerr then
  begin
    MyObjects.HabilitaMenuItemItsIns(IncluiNovaOperacao1, QrSrvLOSCab);
    MyObjects.HabilitaMenuItemItsUpd(AlteraOperacaoAtual1, QrSrvLOSOpeGrn);
    MyObjects.HabilitaMenuItemItsDel(ExcluiOperacaoAtual1, QrSrvLOSOpeGrn);
  end else
  begin
    IncluiNovaOperacao1.Enabled  := False;
    AlteraOperacaoAtual1.Enabled := False;
    ExcluiOperacaoAtual1.Enabled := False;
  end;
end;

procedure TFmSrvLOSCab.PMSrvLOSPrzPopup(Sender: TObject);
var
  Encerr: Boolean;
begin
  Encerr := QrSrvLOSCabDtEncerrad_TXT.Value <> '';
  //
  if not Encerr then
  begin
    MyObjects.HabilitaMenuItemItsIns(OSPrz1_Inclui, QrSrvLOSCab);
    MyObjects.HabilitaMenuItemItsUpd(OSPrz1_Altera, QrSrvLOSPrz);
    MyObjects.HabilitaMenuItemItsDel(OSPrz1_Exclui, QrSrvLOSPrz);
  end else
  begin
    OSPrz1_Inclui.Enabled := False;
    OSPrz1_Altera.Enabled := False;
    OSPrz1_Exclui.Enabled := False;
  end;
end;

procedure TFmSrvLOSCab.PMSrvLOSOpeDspPopup(Sender: TObject);
var
  Encerr: Boolean;
begin
  Encerr := QrSrvLOSCabDtEncerrad_TXT.Value <> '';
  //
  if not Encerr then
  begin
    MyObjects.HabilitaMenuItemItsIns(IncluiNovaDespesa1, QrSrvLOSOpeGrn);
    MyObjects.HabilitaMenuItemItsUpd(AlteraDespesaAtual1, QrSrvLOSOpeDsp);
    MyObjects.HabilitaMenuItemItsDel(ExcluiDespesaAtual1, QrSrvLOSOpeDsp);
  end else
  begin
    IncluiNovaDespesa1.Enabled  := False;
    AlteraDespesaAtual1.Enabled := False;
    ExcluiDespesaAtual1.Enabled := False;
  end;
end;

procedure TFmSrvLOSCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSrvLOSCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSrvLOSCab.DefParams;
begin
  VAR_GOTOTABELA := 'srvloscab';
  VAR_GOTOMYSQLTABLE := QrSrvLOSCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT osc.*, cad.Nome NO_SrvLCad, sta.Nome NO_ESTATUS, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE,');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(DtEncerrad <= "1900-01-01", "", DATE_FORMAT(');
  VAR_SQLx.Add('  DtEncerrad, "%d/%m/%Y %H:%i:%S")) DtEncerrad_TXT,');
  VAR_SQLx.Add('osc.ValTotCobr-(osc.ValTotPaga+osc.ValTotDesp) ValTotLiqu,');
  VAR_SQLx.Add('(osc.ValTotCobr-(osc.ValTotPaga+osc.ValTotDesp)) / ');
  VAR_SQLx.Add('  osc.ValTotCobr * 100 PerTotLiqu');
  VAR_SQLx.Add('FROM srvloscab osc');
  VAR_SQLx.Add('LEFT JOIN srvlcad cad ON cad.Codigo=osc.SrvLCad');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=osc.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=osc.Empresa');
  VAR_SQLx.Add('LEFT JOIN estatusoss sta ON sta.Codigo=osc.Estatus ');
  VAR_SQLx.Add('WHERE osc.Codigo > 0');
  //
  VAR_SQL1.Add('AND osc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND osc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND osc.Nome Like :P0');
  //
end;

procedure TFmSrvLOSCab.DBGSrvLOSOpeGrnCellClick(Column: TColumn);
var
  //TudoFeitoM,
  Controle, Autorizado: Integer;
begin
(*
  if Uppercase(Column.FieldName) = Uppercase(sTudoFeito) then
  begin
    //                               0  1
    if Trunc(QrSrvLOSOpeGrnTudoFeitoM.Value) <> (QrSrvLOSOpeGrnTUDOFEITO.Value) then
      Geral.MB_Aviso(
      'Registros de progresso de aplica��es deste servi�o interferem nesta defini��o!'
      + sLineBreak +
      'Execute o item de servi�o "Recalcula progresso de aplica��es" e tente novamente!'
      + sLineBreak + 'Verifique tamb�m os itens de progresso de aplica��es!')
    else
    begin
      if QrSrvLOSOpeGrnTudoFeitoM.Value = 0 then
        TudoFeitoM := 1
      else
        TudoFeitoM := 0;
      //
      Controle := QrSrvLOSOpeGrnControle.Value;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopegrn', False, [
      'TudoFeitoM'], [
      'Controle'], [
      TudoFeitoM], [
      Controle], True) then
        OSApp_PF.Reopensrvlosopegrn(QrSrvLOSOpeGrn, QrOSCabCodigo.Value, QrSrvLOSOpeGrnControle.Value);
    end;
  end;
*)
  if Uppercase(Column.FieldName) = Uppercase(CO_FldAutrz) then
  begin
    if QrSrvLOSOpeGrnAutorizado.Value = 0 then
      Autorizado := 1
    else
      Autorizado := 0;
    //
    Controle := QrSrvLOSOpeGrnControle.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopegrn', False, [
    'Autorizado'], [
    'Controle'], [
    Autorizado], [
    Controle], True) then
      ReopenSrvLOSOpeGrn(Controle);
  end;
end;

procedure TFmSrvLOSCab.DBGSrvLOSOpeGrnColEnter(Sender: TObject);
var
  Campo: String;
begin
  Campo := Lowercase(DBGSrvLOSOpeGrn.Columns[THackDBGrid(DBGSrvLOSOpeGrn).Col -1].FieldName);
  if (Campo = Lowercase(CO_FldAutrz)) or (Campo = Lowercase(CO_FldTudoFeito)) then
    DBGSrvLOSOpeGrn.Options := DBGSrvLOSOpeGrn.Options - [dgEditing] else
    DBGSrvLOSOpeGrn.Options := DBGSrvLOSOpeGrn.Options + [dgEditing];
end;

procedure TFmSrvLOSCab.DBGSrvLOSOpeGrnColExit(Sender: TObject);
begin
  DBGSrvLOSOpeGrn.Options := DBGSrvLOSOpeGrn.Options - [dgEditing];
end;

procedure TFmSrvLOSCab.DBGSrvLOSOpeGrnDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Autorizado' then
    MeuVCLSkin.DrawGrid(DBGSrvLOSOpeGrn, Rect, 1, QrSrvLOSOpeGrnAutorizado.Value);
(*
  if Column.FieldName = 'TUDOFEITO' then
    MeuVCLSkin.DrawGrid(DBGSrvLOSOpeGrn, Rect, 1, Trunc(QrSrvLOSOpeGrnTUDOFEITO.Value));
*)
end;

procedure TFmSrvLOSCab.ExcluiOperacaoAtual1Click(Sender: TObject);
const
  CO_Msg = 'Exclus�o cancelada!' + sLineBreak + 'Motivo: Existem itens atrelados a este registro!';
var
  Controle: Integer;
begin
  if QrSrvLOSOpeDsp.State <> dsInactive then
  begin
    if QrSrvLOSOpeDsp.RecordCount > 0 then
    begin
      Geral.MB_Aviso(CO_Msg);
      Exit;
    end;
  end;
  if QrSrvLOSOpeAge.State <> dsInactive then
  begin
    if QrSrvLOSOpeAge.RecordCount > 0 then
    begin
      Geral.MB_Aviso(CO_Msg);
      Exit;
    end;
  end;
  if QrSrvLOSOpeCPA.State <> dsInactive then
  begin
    if QrSrvLOSOpeCPA.RecordCount > 0 then
    begin
      Geral.MB_Aviso(CO_Msg);
      Exit;
    end;
  end;

  Controle := QrSrvLOSOpeGrnControle.Value;

  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
    'srvlosopegrn', 'Controle', Controle, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrSrvLOSOpeGrn, QrSrvLOSOpeGrnControle, Controle);
    ReopenSrvLOSOpeGrn(Controle);
  end;
end;

procedure TFmSrvLOSCab.ExcluiSrvLOSAtrDef1Click(Sender: TObject);
var
  ID_Item: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do atributo selecionado?', QrSrvLOSOpeAtrDefTABELA.Value,
  'ID_Item', QrSrvLOSOpeAtrDefID_Item.Value, Dmod.MyDB) = ID_YES then
  begin
    ID_Item := GOTOy.LocalizaPriorNextIntQr(QrSrvLOSOpeAtrDef, QrSrvLOSOpeAtrDefID_Item,
    QrSrvLOSOpeAtrDefID_Item.Value);
    ReopenSrvLOSOpeAtrDef(ID_Item);
  end;
end;

procedure TFmSrvLOSCab.ExcluiDespesaAtual1Click(Sender: TObject);
var
  Conta, Codigo, Controle: Integer;
begin
  Codigo   := QrSrvLOSOpeDspCodigo.Value;
  Controle := QrSrvLOSOpeDspControle.Value;
  //
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'srvlosopedsp', 'Conta', QrSrvLOSOpeDspConta.Value, Dmod.MyDB) = ID_YES then
  begin
    SrvL_PF.AtualizaTotaisSrvOSOpeGrn(Codigo, Controle);
    Conta := GOTOy.LocalizaPriorNextIntQr(QrSrvLOSOpeDsp,
      QrSrvLOSOpeDspConta, QrSrvLOSOpeDspConta.Value);
    LocCod(Codigo, Codigo);
    ReopenSrvLOSOpeDsp(Conta);
  end;
end;

procedure TFmSrvLOSCab.Excluijornadaatual1Click(Sender: TObject);
var
  Codigo, Controle, Conta, Nivel4: Integer;
begin
  Nivel4 := QrSrvLOSOpeCPANivel4.Value;
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o da jornada selecionada?',
  'srvlosopecpa', 'Nivel4', Nivel4, Dmod.MyDB) = ID_YES then
  begin
    Codigo   := QrSrvLOSOpeCPACodigo.Value;
    Controle := QrSrvLOSOpeCPAControle.Value;
    Conta    := QrSrvLOSOpeCPAConta.Value;
    //
    SrvL_PF.AtualizaTotaisSrvLOSOpeAge(Conta);
    SrvL_PF.AtualizaTotaisSrvOSOpeGrn(Codigo, Controle);
    Nivel4   := GOTOy.LocalizaPriorNextIntQr(QrSrvLOSOpeCPA,
      QrSrvLOSOpeCPANivel4, QrSrvLOSOpeCPANivel4.Value);
    ReopenEscada(Codigo, Controle, Conta, Nivel4);
  end;
end;

procedure TFmSrvLOSCab.IncluiNovaOperacao1Click(Sender: TObject);
begin
  MostraFormSrvLOSOpeGrn(stIns);
end;

procedure TFmSrvLOSCab.IncluiSrvLOSAtrDef1Click(Sender: TObject);
begin
  MostraFormSrvLOSOpeAtrDef(stIns);
end;

procedure TFmSrvLOSCab.IncluiNovaDespesa1Click(Sender: TObject);
begin
  MostraFormSrvLOSOpeDsp(stIns);
end;

procedure TFmSrvLOSCab.Incluinovajornada1Click(Sender: TObject);
begin
  MostraFormSrvLOSOpeAge(stIns);
end;

procedure TFmSrvLOSCab.IncluiOSAge1Click(Sender: TObject);
const
  Aviso   = '...';
  Titulo  = 'XXX-XXXXX-004 :: Sele��o de Agentes';
  Prompt1 = 'Seleciones os agentes operacionais:';
  Prompt2 = 'Seleciones os agentes desejados:';
  Campo   = 'Descricao';
  //
  Lider   = 0;
var
  Res: Variant;
  SrvLOSCab, SrvLOSOpeGrn, AgeEqiCfg, N, GruAddSeq: Integer;
  Agentes: array of Integer;
  DataExeIni: TDateTime;
begin
  if DBCheck.EscolheCodigosMultiplos_0(
    Aviso, Caption, Prompt2, nil, 'Ativo', 'Nivel1', 'Nome', [
    'DELETE FROM _selcods_; ',
    'INSERT INTO _selcods_ ',
    'SELECT Codigo Nivel1, 0 Nivel2, ',
    '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
    'FROM ' + TMeuDB + '.entidades',
    'WHERE Nome <> "" ',
    'AND ' + VAR_FP_FUNCION,
    ''],[
    'SELECT * FROM _selcods_ ',
    'ORDER BY Nome;',
    ''], Dmod.QrUpd) then
  begin
    //Lider := 0;
    N := 0;
    DModG.QrSelCods.First;
    while not DModG.QrSelCods.Eof do
    begin
      if DModG.QrSelCodsAtivo.Value = 1 then
      begin
        N := N + 1;
        SetLength(Agentes, N);
        Agentes[N-1] := DModG.QrSelCodsNivel1.Value;
      end;
      DModG.QrSelCods.Next;
    end;
  end;
  //
  if N > 0 then
  begin
    SrvLOSCab    := QrSrvLOSCabCodigo.Value;
    SrvLOSOpeGrn := QrSrvLOSOpeGrnControle.Value;
    AgeEqiCfg    := QrSrvLOSOpeGrnAgeEqiCfg.Value;
    DataExeIni   := QrSrvLOSOpeGrnDtaExeIni.Value;
    //
    GruAddSeq := SrvL_PF.CriaItensDeAgentes_New(SrvLOSCab, SrvLOSOpeGrn,
      (*AgeEqiCab,*) AgeEqiCfg, DataExeIni, Agentes, Lider);
    Empg_PF.MD5_AtualizaCheckSumOSAge('srvlosopegrn', 'Controle',
      'srvlosopeage', SrvLOSOpeGrn);
    SrvL_PF.DistribuiValoresAgentesSrvLOSOpeGrn(SrvLOSOpeGrn, GruAddSeq);
    //
    LocCod(SrvLOSCab, SrvLOSCab);
  end;
{
const
  Aviso  = '...';
  Titulo = 'XXX-XXXXX-004 :: Sele��o de Agentes';
  Prompt = 'Seleciones os agentes operacionais:';
  //Campo  = 'Descricao';
  //
  DestTab = 'srvlosopeage';
  DestNiv2 = 'Codigo';
  DestMaster = 'Controle';
  DestDetail = 'Conta';
  DestSelec1 = 'Agente';
  SorcField  = 'Agente';
  ExcluiAnteriores = True;
var
  SrvLOSCab, ValrNiv3, ValrNiv2, ValrMaster: Integer;
begin
  //ValrMaster := QrOSCabCodigo.Value;
  //OSCab := QrOSCabCodigo.Value;
  ValrNiv2   := QrSrvLOSOpeGrnCodigo.Value;
  ValrMaster := QrSrvLOSOpeGrnControle.Value;
  SrvLOSCab  := QrSrvLOSCabCodigo.Value;
  if DBCheck.EscolheCodigosMultiplos_A(
  Aviso, Titulo, Prompt, nil, 'Ativo', 'Nivel1', 'Nome', [
  'DELETE FROM _selcods_; ',
  'INSERT INTO _selcods_ ',
  'SELECT Codigo Nivel1, 0 Nivel2, ',
  '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
  'FROM ' + TMeuDB + '.entidades',
  'WHERE Nome <> "" ',
  'AND ' + VAR_FP_FUNCION,
  ''],[
  'SELECT * FROM _selcods_ ',
  'ORDER BY Nome;',
  ''], DestTab, [DestNiv2, DestMaster], DestDetail, DestSelec1,
  [ValrNiv2, ValrMaster],
  QrSrvLOSOpeAge, SorcField, ExcluiAnteriores, Dmod.QrUpd) then
  begin
    Empg_PF.MD5_AtualizaCheckSumOSAge('srvlosopegrn', 'Controle', 'srvlosopeage', QrSrvLOSOpeGrnControle.Value);
    SrvL_PF.AdicionaSrvLOSOpeCPAemAgentesNovos(QrSrvLOSOpeGrnControle.Value);
    SrvL_PF.DistribuiValoresAgentesSrvLOSCab(SrvLOSCab);
    ReopenSrvLOSOpeGrn(ValrMaster);
    ReopenSrvLOSOpeAge(0);
  end;
}
end;

procedure TFmSrvLOSCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormSrvLOSEsc(stUpd);
end;

procedure TFmSrvLOSCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmSrvLOSCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSrvLOSCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSrvLOSCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'srvlosesc', 'Controle', QrSrvLOSEscControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrSrvLOSEsc,
      QrSrvLOSEscControle, QrSrvLOSEscControle.Value);
    ReopenSrvLOSEsc(Controle);
  end;
}
end;

procedure TFmSrvLOSCab.RemoveOSAge1Click(Sender: TObject);
const
  GruAddSeq = 0;
var
  SrvLOSOpeGrn, Conta, Codigo: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada do agente selecionado?',
  'srvlosopeage', 'Conta', QrSrvLOSOpeAgeConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := QrSrvLOSOpeGrnCodigo.Value;
    SrvLOSOpeGrn := QrSrvLOSOpeGrnControle.Value;
    Conta := GOTOy.LocalizaPriorNextIntQr(QrSrvLOSOpeAge,
      QrSrvLOSOpeAgeConta, QrSrvLOSOpeAgeConta.Value);
    SrvL_PF.DistribuiValoresAgentesSrvLOSOpeGrn(QrSrvLOSOpeGrnControle.Value, GruAddSeq);
    Empg_PF.MD5_AtualizaCheckSumOSAge(
      'srvlosopegrn', 'Controle', 'srvlosopeage', QrSrvLOSOpeGrnControle.Value);
    //
    LocCod(Codigo, Codigo);
    //ReopenSrvLOSOpeGrn(SrvLOSOpeGrn);
    QrSrvLOSOpeGrn.Locate('Controle', SrvLOSOpeGrn, []);
    //ReopenSrvLOSOpeAge(Conta);
    QrSrvLOSOpeAge.Locate('Conta', Conta, []);
  end;
end;

procedure TFmSrvLOSCab.ReopenSrvLOSOpeAge(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLOSOpeAge, Dmod.MyDB, [
  'SELECT ent.Nome NO_AGENTE, age.* ',
  'FROM srvlosopeage age ',
  'LEFT JOIN entidades ent on ent.Codigo=age.Agente ',
  'WHERE age.Controle=' + Geral.FF0(QrSrvlOSOpeGrnControle.Value),
  '']);
  //
  if Conta <> 0 then
    QrSrvLOSOpeAge.Locate('Conta', Conta, []);
end;

procedure TFmSrvLOSCab.ReopenSrvLOSOpeAtrDef(ID_Item: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLOSOpeAtrDef, Dmod.MyDB, [
(*
  'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ATRITS, ',
  'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,',
  'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp',
  'FROM srvlosopeatrdef def',
  'LEFT JOIN srvlosopeatrits its ON its.Controle=def.AtrIts ',
  'LEFT JOIN srvlosopeatrcad cad ON cad.Codigo=def.AtrCad ',
  'WHERE def.ID_Sorc=' + Geral.FF0(QrSrvLOSOpeGrnControle.Value),
  ' ',
  'UNION  ',
  ' ',
  'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, ',
  'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,',
  'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp ',
  'FROM srvlosopeatrtxt def ',
  'LEFT JOIN srvlosopeatrcad cad ON cad.Codigo=def.AtrCad ',
  'WHERE def.ID_Sorc=' + Geral.FF0(QrSrvLOSOpeGrnControle.Value),
  ' ',
  'ORDER BY NO_CAD, NO_ITS ',
*)
  'SELECT "srvlosopeatrdef" TABELA, ',
  'def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ATRITS, ',
  'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,',
  'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp',
  'FROM srvlosopeatrdef def',
  'LEFT JOIN srvlosopeatrits its ON its.Controle=def.AtrIts ',
  'LEFT JOIN srvlosopeatrcad cad ON cad.Codigo=def.AtrCad ',
  'WHERE def.ID_Sorc=' + Geral.FF0(QrSrvLOSOpeGrnControle.Value),
  ' ',
  'UNION  ',
  ' ',
  'SELECT "srvlosopeatrtxt" TABELA,',
  'def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, ',
  'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,',
  'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp ',
  'FROM srvlosopeatrtxt def ',
  'LEFT JOIN srvlosopeatrcad cad ON cad.Codigo=def.AtrCad ',
  'WHERE def.ID_Sorc=' + Geral.FF0(QrSrvLOSOpeGrnControle.Value),
  ' ',
  'ORDER BY NO_CAD, NO_ITS ',
  '']);
end;

procedure TFmSrvLOSCab.ReopenSrvLOSOpeCPA(Nivel4: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLOSOpeCPA, Dmod.MyDB, [
  'SELECT ',
  'ELT(ValEverAut + 1, "N�o", "Sim", "???") NO_ValEverAut,  ',
  'ELT(TemInterv + 1, "N�o", "Sim", "???") NO_TemInterv, ',
  'ooc.*   ',
  'FROM srvlosopecpa ooc ',
  'WHERE ooc.Conta=' + Geral.FF0(QrSrvLOSOpeAgeConta.Value),
  '']);
  //
  if Nivel4 > 0 then
    QrSrvLOSOpeCPA.Locate('Nivel4', Nivel4, []);
end;

procedure TFmSrvLOSCab.ReopenEscada(Codigo, Controle, Conta, Nivel4: Integer);
begin
  LocCod(Codigo, Codigo);
  QrSrvLOSOpeGrn.Locate('Controle', Controle, []);
  QrSrvLOSOpeAge.Locate('Conta', Conta, []);
  QrSrvLOSOpeCPA.Locate('Nivel4', Nivel4, []);
end;

procedure TFmSrvLOSCab.ReopenSrvLOSEsc(Controle: Integer);
begin
(*&^%$
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLOSEsc, Dmod.MyDB, [
  'SELECT * ',
  'FROM srvlosesc ',
  'WHERE Codigo=' + Geral.FF0(QrSrvLOSCabCodigo.Value),
  '']);
  //
  QrSrvLOSEsc.Locate('Controle', Controle, []);
*)
end;

procedure TFmSrvLOSCab.ReopenSrvLOSOpeGrn(Controle: Integer);
var
  ATT_FrmFtrCobr, ATT_FrmFtrPaga, ATT_FrmRatPaga: String;
begin
  ATT_FrmFtrCobr := dmkPF.ArrayToTexto('oog.FrmFtrCobr', 'NO_FrmFtrCobr',
    pvPos, True, sPrmFatorValor);
  ATT_FrmFtrPaga := dmkPF.ArrayToTexto('oog.FrmFtrPaga', 'NO_FrmFtrPaga',
    pvPos, True, sPrmFatorValor);
  ATT_FrmRatPaga := dmkPF.ArrayToTexto('oog.FrmRatPaga', 'NO_FrmRatPaga',
    pvPos, True, sPrmFormaRateio);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLOSOpeGrn, Dmod.MyDB, [
  'SELECT slc.Nome NO_SrvLCad, prm.Nome NO_SrvLPrmCad,  ',
  ATT_FrmFtrCobr,
  ATT_FrmFtrPaga,
  ATT_FrmRatPaga,
  'IF(DtaExeIni <= "1900-01-01", "", ',
  '  DATE_FORMAT(DtaExeIni, "%d/%m/%Y %H:%i:%S")) DtaExeIni_TXT, ',
  'IF(DtaExeFim <= "1900-01-01", "", ',
  '  DATE_FORMAT(DtaExeFim, "%d/%m/%Y %H:%i:%S")) DtaExeFim_TXT, ',
  'umc.Sigla SIGLA_UMC, ump.Sigla SIGLA_UMP, ',
  'aec.Nome NO_AgeEquiCab, oog.*  ',
  'FROM srvlosopegrn oog ',
  'LEFT JOIN srvlcad slc ON slc.Codigo=oog.SrvLCad ',
  'LEFT JOIN srvlprmcad prm ON prm.Codigo=oog.SrvLPrmCad ',
  'LEFT JOIN ageeqicab aec ON oog.AgeEqiCab=aec.Codigo ',
  'LEFT JOIN unidmed umc ON umc.Codigo=oog.UnidMedCobr ',
  'LEFT JOIN unidmed ump ON ump.Codigo=oog.UnidMedPaga ',
  'WHERE oog.Codigo=' + Geral.FF0(QrSrvLOSCabCodigo.Value),
  '']);
  //
  QrSrvLOSOpeGrn.Locate('Controle', Controle, []);
  //Geral.MB_SQL(Self, QrSrvLOSOpeGrn);
end;

procedure TFmSrvLOSCab.ReopenSrvLOSOpeDsp(Conta: Integer);
var
  ATT_SrvLCtbDsp: String;
begin
  ATT_SrvLCtbDsp := dmkPF.ArrayToTexto('oop.FrmCtbDspFat', 'NO_FrmCtbDspFat',
    pvPos, True, sSrvLCtbDsp);
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLOSOpeDsp, Dmod.MyDB, [
  'SELECT gen.Nome NO_GENERO, ',
  'IF(age.Tipo=0, age.RazaoSocial, age.Nome) NO_AGENTE, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  ATT_SrvLCtbDsp,
  'oop.*  ',
  'FROM srvlosopedsp oop ',
  'LEFT JOIN contas gen ON gen.Codigo=oop.Genero ',
  'LEFT JOIN entidades age ON age.Codigo=oop.Agente ',
  'LEFT JOIN entidades frn ON frn.Codigo=oop.Fornece ',
  'WHERE oop.Controle=' + Geral.FF0(QrSrvLOSOpeGrnControle.Value),
  '']);
  //
  QrSrvLOSOpeDsp.Locate('Conta', Conta, []);
end;

procedure TFmSrvLOSCab.DBGOSAgeCellClick(Column: TColumn);
var
  //Codigo,
  Controle, Conta, Agente, Responsa: Integer;
begin
  if (QrSrvLOSOpeAge.State <> dsInactive) and (QrSrvLOSOpeAge.RecordCount > 0) then
  begin
    if Lowercase(Column.FieldName) = Lowercase('Responsa') then
    begin
      Screen.Cursor := crHourGlass;
      try
        //Codigo         := QrSrvLOSOpeAgeCodigo.Value;
        Controle       := QrSrvLOSOpeAgeControle.Value;
        Conta          := QrSrvLOSOpeAgeConta.Value;
        Responsa       := 0;
        //
        // Zerar todos "Responsa" da OS
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopeage', False, [
        'Responsa'], ['Controle'], [
        Responsa], [Controle], True) then
        begin
          // Ativar "Responsa" do Agente selecionado
          Responsa := 1;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopeage', False, [
          'Responsa'], ['Conta'], [
          Responsa], [Conta], True);
        end;
        //
        ReopenSrvLOSOpeAge(Conta);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmSrvLOSCab.DBGOSAgeColEnter(Sender: TObject);
begin
  if DBGOSAge.Columns[THackDBGrid(DBGOSAge).Col -1].FieldName = CO_FldRespo then
    DBGOSAge.Options := DBGOSAge.Options - [dgEditing] else
    DBGOSAge.Options := DBGOSAge.Options + [dgEditing]
end;

procedure TFmSrvLOSCab.DBGOSAgeColExit(Sender: TObject);
begin
  DBGOSAge.Options := DBGOSAge.Options - [dgEditing];
end;

procedure TFmSrvLOSCab.DBGOSAgeDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = CO_FldRespo then
    MeuVCLSkin.DrawGrid(DBGOSAge, Rect, 1, QrSrvLOSOpeAgeResponsa.Value);
end;

procedure TFmSrvLOSCab.DBGSrvLOSPrzCellClick(Column: TColumn);
var
  Escolhido, Codigo, Controle: Integer;
begin
  if Column.FieldName = CO_FldEscolhido then
  begin
    if QrSrvLOSPrzEscolhido.Value = 0 then
      Escolhido := 1
    else
      Escolhido := 0;
    //
    Codigo   := QrSrvLOSPrzCodigo.Value;
    Controle := QrSrvLOSPrzControle.Value;
    //
    // Definir todas as condi��es da OS como n�o escolhidas
    if Escolhido = 1 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosprz', False, [
      'Escolhido'], ['Codigo'], [0], [Codigo], True);
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosprz', False, [
    'Escolhido'], [
    'Controle'], [
    Escolhido
    ], [
    Controle], True) then
    begin
      SrvL_PF.AtualizaTotaisSrvLOSCab(Codigo);
      LocCod(Codigo, Codigo);
      QrSrvLOSPrz.Locate('Controle', Controle, []);
    end;
  end;
end;

procedure TFmSrvLOSCab.DBGSrvLOSPrzColEnter(Sender: TObject);
begin
  if DBGOSAge.Columns[THackDBGrid(DBGSrvLOSPrz).Col -1].FieldName = CO_FldEscolhido then
    DBGSrvLOSPrz.Options := DBGSrvLOSPrz.Options - [dgEditing] else
    DBGSrvLOSPrz.Options := DBGSrvLOSPrz.Options + [dgEditing];
end;

procedure TFmSrvLOSCab.DBGSrvLOSPrzDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Escolhido' then
    MeuVCLSkin.DrawGrid(DBGSrvLOSPrz, Rect, 1, QrSrvLOSPrzEscolhido.Value);
end;

procedure TFmSrvLOSCab.DBGSrvLOSPrzMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMSrvLOSPrz, DBGSrvLOSPrz, X,Y);
end;

procedure TFmSrvLOSCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSrvLOSCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSrvLOSCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSrvLOSCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSrvLOSCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSrvLOSCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLOSCab.BtSrvLOSOpeGrnClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMSrvLOSOpeGrn, BtSrvLOSOpeGrn);
end;

procedure TFmSrvLOSCab.BtSrvLOSOpeDspClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMSrvLOSOpeDsp, BtSrvLOSOpeDsp);
end;

procedure TFmSrvLOSCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSrvLOSCabCodigo.Value;
  Close;
end;

procedure TFmSrvLOSCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormSrvLOSEsc(stIns);
end;

procedure TFmSrvLOSCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  DModG.HabilitaEdCBEmpresa(EdEmpresa, CBEmpresa, stUpd, EdCliente);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrSrvLOSCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'srvloscab');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrSrvLOSCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
  //
end;

procedure TFmSrvLOSCab.BtConfirmaClick(Sender: TObject);
var
  Nome, DtAbertura, DtEncerrad: String;
  Codigo, Empresa, SrvLPre, SrvLCad, Cliente, Estatus: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  SrvLPre        := EdSrvLPre.ValueVariant;
  Estatus        := EdEstatus.ValueVariant;
  SrvLCad        := EdSrvLCad.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  DtAbertura     := Geral.FDT_TP_Ed(TPDtAbertura.Date, EdDtAbertura.Text);
  DtEncerrad     := Geral.FDT_TP_Ed(TPDtEncerrad.Date, EdDtEncerrad.Text);;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
  //if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('srvloscab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'srvloscab', False, [
    'Nome', 'SrvLPre', 'SrvLCad',
    'Cliente', 'DtAbertura', 'DtEncerrad',
    'Empresa', 'Estatus'], [
    'Codigo'], [
    Nome, SrvLPre, SrvLCad,
    Cliente, DtAbertura, DtEncerrad,
    Empresa, Estatus], [
    Codigo], True) then
  begin
    SrvL_PF.AtualizaTotaisSrvLOSCab(Codigo);
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then
      Close
    else if SQLType = stIns then
      MostraFormSrvLOSOpeGrn(stIns);
  end;
end;

procedure TFmSrvLOSCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'srvloscab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'srvloscab', 'Codigo');
end;

procedure TFmSrvLOSCab.BtSrvLOSPrzClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMSrvLOSPrz, BtSrvLOSPrz);
end;

procedure TFmSrvLOSCab.Alterajornadaatual1Click(Sender: TObject);
begin
  MostraFormSrvLOSOpeAge(stUpd);
end;

procedure TFmSrvLOSCab.AlteraOperacaoAtual1Click(Sender: TObject);
begin
  MostraFormSrvLOSOpeGrn(stUpd);
end;

procedure TFmSrvLOSCab.AlteraSrvLOSAtrDef1Click(Sender: TObject);
begin
  MostraFormSrvLOSOpeAtrDef(stUpd);
end;

procedure TFmSrvLOSCab.AlteraDespesaAtual1Click(Sender: TObject);
begin
  MostraFormSrvLOSOpeDsp(stUpd);
end;

procedure TFmSrvLOSCab.BtSrvLOSOpeAgeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMOSOpeAge, BtSrvLOSOpeAge);
end;

procedure TFmSrvLOSCab.BtSrvLOSOpeAtrDefClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMSrvLOSOpeAtrDef, BtSrvLOSOpeAtrDef);
end;

procedure TFmSrvLOSCab.BtSrvLOSOpeCPAClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMSrvLOSOpeCPA, BtSrvLOSOpeCPA);
end;

procedure TFmSrvLOSCab.BtCabClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMSrvLOSCab, BtCab);
end;

procedure TFmSrvLOSCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  //DGDados.Align := alClient;
  //DBGSrvLOSOpeAtrDef.Align := alClient;
  DBGSrvLOSOpeDsp.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  PageControl1.Align := alClient;
  CriaOForm;
  FSeq := 0;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrSrvLPOCab, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSrvLCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEstatusOSs, Dmod.MyDB);
end;

procedure TFmSrvLOSCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSrvLOSCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvLOSCab.SBClienteClick(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade     := EdCliente.ValueVariant;
  VAR_CADASTRO := 0;

  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCliente, CBCliente, QrClientes, VAR_CADASTRO);
    EdCliente.SetFocus;
  end;
end;

procedure TFmSrvLOSCab.SbImprimeClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSrvlOSCabImp, FmSrvlOSCabImp, afmoNegarComAviso) then
  begin
    FmSrvlOSCabImp.FEmpresa                  := QrSrvLOSCabEmpresa.Value;
    FmSrvlOSCabImp.FCliente                  := QrSrvLOSCabCliente.Value;
    FmSrvlOSCabImp.frxDsSrvLOSCab.DataSet    := QrSrvLOSCab;
    FmSrvlOSCabImp.frxDsSrvLOSOpeGrn.DataSet := QrSrvLOSOpeGrn;
    //
    FmSrvlOSCabImp.ImprimeOSCab();
    //FmSrvlOSCabImp.ShowModal;
    //
    FmSrvlOSCabImp.Destroy;
  end;
end;

procedure TFmSrvLOSCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSrvLOSCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrSrvLOSCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvLOSCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSrvLOSCab.QrSrvLOSCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSrvLOSCab.QrSrvLOSCabAfterScroll(DataSet: TDataSet);
const
  Controle = 0;
var
  Codigo: Integer;
begin
  Codigo := QrSrvLOSCabCodigo.Value;
  ReopenSrvLOSOpeGrn(Controle);
  ReopenSrvLOSEsc(Controle);
  SrvL_PF.ReopenXPrz(QrSrvLOSPrz, 'SrvLOSPrz', Codigo, Controle);
end;

procedure TFmSrvLOSCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrSrvLOSCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmSrvLOSCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSrvLOSCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'srvloscab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSrvLOSCab.SbSrvLCadClick(Sender: TObject);
var
  SrvLCad: Integer;
begin
  VAR_CADASTRO := 0;
  SrvLCad      := EdSrvLCad.ValueVariant;

  SrvL_PF.MostraFormSrvLCad(SrvLCad);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdSrvLCad, CBSrvLCad, QrSrvLCad, VAR_CADASTRO);
    EdSrvLCad.SetFocus;
  end;
end;

procedure TFmSrvLOSCab.SbSrvLPreClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  SrvL_PF.MostraFormSrvLPOCab(EdSrvLPre.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdSrvLPre, CBSrvLPre, QrSrvLPOCab, VAR_CADASTRO);
end;

procedure TFmSrvLOSCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLOSCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrSrvLOSCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'srvloscab');
  //
  Agora := DModG.ObtemAgora();
  TPDtAbertura.Date := Trunc(Agora);
  EdDtAbertura.ValueVariant := Agora;
  DModG.HabilitaEdCBEmpresa(EdEmpresa, CBEmpresa, stIns, EdCliente);
end;

procedure TFmSrvLOSCab.QrSrvLOSCabBeforeClose(
  DataSet: TDataSet);
begin
  QrSrvLOSEsc.Close;
end;

procedure TFmSrvLOSCab.QrSrvLOSCabBeforeOpen(DataSet: TDataSet);
begin
  QrSrvLOSCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmSrvLOSCab.QrSrvLOSOpeAgeAfterScroll(DataSet: TDataSet);
begin
  ReopenSrvLOSOpeCPA(0);
end;

procedure TFmSrvLOSCab.QrSrvLOSOpeAgeBeforeClose(DataSet: TDataSet);
begin
  QrSrvLOSOpeCPA.Close;
end;

procedure TFmSrvLOSCab.QrSrvLOSOpeGrnAfterScroll(DataSet: TDataSet);
begin
  ReopenSrvLOSOpeAge(0);
  ReopenSrvLOSOpeDsp(0);
  ReopenSrvLOSOpeAtrDef(0);
end;

procedure TFmSrvLOSCab.QrSrvLOSOpeGrnBeforeClose(DataSet: TDataSet);
begin
  QrSrvLOSOpeAge.Close;
  QrSrvLOSOpeDsp.Close;
  QrSrvLOSOpeAtrDef.Close;
end;

end.

