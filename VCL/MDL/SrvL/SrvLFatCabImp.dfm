object FmSrvLFatCabImp: TFmSrvLFatCabImp
  Left = 0
  Top = 0
  Caption = 'LOC-SERVI-021 :: Impress'#227'o de Faturamento'
  ClientHeight = 415
  ClientWidth = 652
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object frxDsSrvLFatCab: TfrxDBDataset
    UserName = 'frxDsSrvLFatCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_EMPRESA=NO_EMPRESA'
      'NO_CLIENTE=NO_CLIENTE'
      'Codigo=Codigo'
      'Nome=Nome'
      'Empresa=Empresa'
      'Cliente=Cliente'
      'AnoMes=AnoMes'
      'DtHrIni=DtHrIni'
      'DtHrFim=DtHrFim'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'DtHrFim_TXT=DtHrFim_TXT'
      'AnoMes_TXT=AnoMes_TXT'
      'ValTotCbr=ValTotCbr'
      'ValDspToDsc=ValDspToDsc'
      'ValLiqToFat=ValLiqToFat'
      'ValLiqOkFat=ValLiqOkFat'
      'ValDspToDpl=ValDspToDpl'
      'ValDspOkDpl=ValDspOkDpl'
      'EntPagante=EntPagante'
      'ValOutToFat=ValOutToFat'
      'ValDesToFat=ValDesToFat'
      'CondicaoPG=CondicaoPG'
      'CartEmis=CartEmis'
      'Genero=Genero'
      'SerNF=SerNF'
      'NumNF=NumNF'
      'DtaFimFat=DtaFimFat'
      'NO_ENTPAGANTE=NO_ENTPAGANTE'
      'Filial=Filial')
    DataSet = QrSrvLFatCab
    BCDToCurrency = False
    Left = 80
    Top = 104
  end
  object frxDsSrvLOSOpeGrn: TfrxDBDataset
    UserName = 'frxDsSrvLOSOpeGrn'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'SrvLCad=SrvLCad'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Nome=Nome'
      'NO_SrvLCad=NO_SrvLCad'
      'SrvLPrmCad=SrvLPrmCad'
      'NO_SrvLPrmCad=NO_SrvLPrmCad'
      'NO_AgeEquiCab=NO_AgeEquiCab'
      'AgeEqiCab=AgeEqiCab'
      'UnidMedCobr=UnidMedCobr'
      'QtdRlzCobr=QtdRlzCobr'
      'ValFtrCobr=ValFtrCobr'
      'FrmFtrCobr=FrmFtrCobr'
      'UnidMedPaga=UnidMedPaga'
      'QtdRlzPaga=QtdRlzPaga'
      'ValRatPaga=ValRatPaga'
      'FrmRatPaga=FrmRatPaga'
      'FrmFtrPaga=FrmFtrPaga'
      'ValTotCobr=ValTotCobr'
      'NO_FrmRatPaga=NO_FrmRatPaga'
      'SIGLA_UMC=SIGLA_UMC'
      'SIGLA_UMP=SIGLA_UMP'
      'NO_FrmFtrCobr=NO_FrmFtrCobr'
      'NO_FrmFtrPaga=NO_FrmFtrPaga'
      'ValFtrPaga=ValFtrPaga'
      'ValTotDesp=ValTotDesp'
      'Autorizado=Autorizado')
    DataSet = QrSrvLOSOpeGrn
    BCDToCurrency = False
    Left = 80
    Top = 196
  end
  object frxLOC_SERVI_021_01: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.635407800900000000
    ReportOptions.LastChange = 40429.473596203700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxLOC_SERVI_021_01__GetValue
    Left = 80
    Top = 8
    Datasets = <
      item
        DataSet = frxDsSrvLFatCab
        DataSetName = 'frxDsSrvLFatCab'
      end
      item
        DataSet = frxDsSrvLOSOpeAtrDef
        DataSetName = 'frxDsSrvLOSOpeAtrDef'
      end
      item
        DataSet = frxDsSrvLOSOpeGrn
        DataSetName = 'frxDsSrvLOSOpeGrn'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 495.118430000000000000
        Width = 680.315400000000000000
        object Memo23: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 355.275820000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 143.622108270000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 90.708720000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 68.031540000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 68.031540000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 665.197280000000000000
          Height = 49.133890000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ENDERECO_EMP]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Top = 68.031540000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo10: TfrxMemoView
          Top = 94.488250000000000000
          Width = 680.315400000000000000
          Height = 49.133858270000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ENDERECO_CLI]')
          ParentFont = False
        end
        object frxDsSrvLOSCabNO_CLIENTE: TfrxMemoView
          Left = 105.826840000000000000
          Top = 68.031540000000000000
          Width = 468.661720000000000000
          Height = 22.677180000000000000
          DataSet = frxDsSrvLFatCab
          DataSetName = 'frxDsSrvLFatCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_CLIENTE]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSrvLOSOpeGrn."Codigo"'
        object Memo14: TfrxMemoView
          Left = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Localizador')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'A?')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 71.811070000000000000
          Width = 151.181102362204700000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Servi'#231'o')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 222.992270000000000000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Modalidade')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496062992130000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor servi'#231'o')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 529.134200000000000000
          Width = 83.149606299212600000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 461.102660000000000000
          Width = 68.031496062992130000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 385.512060000000000000
          Width = 75.590551181102360000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor modal')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 411.968770000000000000
        Width = 680.315400000000000000
        object Memo27: TfrxMemoView
          Left = 597.165740000000000000
          Width = 83.149625830000000000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSrvLOSOpeGrn."ValTotCobr">,DetailData1)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 514.016080000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSrvLOSOpeGrn
        DataSetName = 'frxDsSrvLOSOpeGrn'
        RowCount = 0
        object CheckBox1: TfrxCheckBoxView
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'Autorizado'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo13: TfrxMemoView
          Left = 18.897650000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          DataField = 'Controle'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."Controle"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 71.811070000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataField = 'Nome'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."Nome"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 222.992270000000000000
          Width = 162.519755830000000000
          Height = 18.897650000000000000
          DataField = 'NO_SrvLPrmCad'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."NO_SrvLPrmCad"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          DataField = 'ValTotCobr'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."ValTotCobr"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 529.134200000000000000
          Width = 83.149606300000000000
          Height = 18.897650000000000000
          DataField = 'SIGLA_UMC'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."SIGLA_UMC"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 461.102660000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          DataField = 'QtdRlzCobr'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."QtdRlzCobr"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 385.512060000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'ValFtrCobr'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."ValFtrCobr"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSrvLFatCab
        DataSetName = 'frxDsSrvLFatCab'
        RowCount = 0
        object Memo40: TfrxMemoView
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'FATURAMENTO DE SERVI'#199'OS N'#176' [frxDsSrvLFatCab."Codigo"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 45.354360000000000000
          Top = 22.677180000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'DtHrIni'
          DataSet = frxDsSrvLFatCab
          DataSetName = 'frxDsSrvLFatCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSrvLFatCab."DtHrIni"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 22.677180000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Abertura:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 207.874150000000000000
          Top = 22.677180000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'DtHrFim'
          DataSet = frxDsSrvLFatCab
          DataSetName = 'frxDsSrvLFatCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSrvLFatCab."DtHrFim"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 139.842610000000000000
          Top = 22.677180000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Encerramento:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 336.378170000000000000
          Top = 22.677180000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DataField = 'ValTotCbr'
          DataSet = frxDsSrvLFatCab
          DataSetName = 'frxDsSrvLFatCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSrvLFatCab."ValTotCbr"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 302.362400000000000000
          Top = 22.677180000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor:')
          ParentFont = False
        end
      end
      object SubdetailData1: TfrxSubdetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 370.393940000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSrvLOSOpeAtrDef
        DataSetName = 'frxDsSrvLOSOpeAtrDef'
        RowCount = 0
        object Memo5: TfrxMemoView
          Width = 680.315302360000000000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeAtrDef
          DataSetName = 'frxDsSrvLOSOpeAtrDef'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeAtrDef."NO_CAD"]: [frxDsSrvLOSOpeAtrDef."NO_ITS"]')
          ParentFont = False
        end
      end
    end
  end
  object frxLOC_SERVI_021_01__: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42235.310737523150000000
    ReportOptions.LastChange = 42235.310737523150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxLOC_SERVI_021_01__GetValue
    Left = 244
    Top = 8
    Datasets = <
      item
        DataSet = frxDsSrvLFatCab
        DataSetName = 'frxDsSrvLFatCab'
      end
      item
        DataSet = frxDsSrvLOSOpeGrn
        DataSetName = 'frxDsSrvLOSOpeGrn'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 143.622108270000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 90.708720000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 68.031540000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 68.031540000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 665.197280000000000000
          Height = 49.133890000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ENDERECO_EMP]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Top = 68.031540000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo10: TfrxMemoView
          Top = 94.488250000000000000
          Width = 680.315400000000000000
          Height = 49.133858270000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ENDERECO_CLI]')
          ParentFont = False
        end
        object frxDsSrvLOSCabNO_CLIENTE: TfrxMemoView
          Left = 105.826840000000000000
          Top = 68.031540000000000000
          Width = 468.661720000000000000
          Height = 22.677180000000000000
          DataSet = frxDsSrvLFatCab
          DataSetName = 'frxDsSrvLFatCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_CLIENTE]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        object Memo23: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 355.275820000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrSrvLFatCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrSrvLFatCabBeforeClose
    AfterScroll = QrSrvLFatCabAfterScroll
    SQL.Strings = (
      'SELECT '
      'CONCAT(RIGHT(AnoMes, 2), "/", LEFT(AnoMes, 4)) AnoMes_TXT, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE,'
      'IF(DtHrFim <= "1900-01-01", "",'
      '  DATE_FORMAT(DtHrFim, "%d/%m/%Y %H:%i:%S")) DtHrFim_TXT,'
      'sfc.* '
      'FROM srvlfatcab sfc'
      'LEFT JOIN entidades emp ON emp.Codigo=sfc.Empresa'
      'LEFT JOIN entidades cli ON cli.Codigo=sfc.Cliente')
    Left = 80
    Top = 53
    object QrSrvLFatCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrSrvLFatCabNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrSrvLFatCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLFatCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrSrvLFatCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSrvLFatCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSrvLFatCabAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrSrvLFatCabDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrSrvLFatCabDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrSrvLFatCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLFatCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLFatCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLFatCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLFatCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLFatCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLFatCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLFatCabDtHrFim_TXT: TWideStringField
      FieldName = 'DtHrFim_TXT'
      Size = 19
    end
    object QrSrvLFatCabAnoMes_TXT: TWideStringField
      FieldName = 'AnoMes_TXT'
      Size = 7
    end
    object QrSrvLFatCabValTotCbr: TFloatField
      FieldName = 'ValTotCbr'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLFatCabValDspToDsc: TFloatField
      FieldName = 'ValDspToDsc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLFatCabValLiqToFat: TFloatField
      FieldName = 'ValLiqToFat'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLFatCabValLiqOkFat: TFloatField
      FieldName = 'ValLiqOkFat'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLFatCabValDspToDpl: TFloatField
      FieldName = 'ValDspToDpl'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLFatCabValDspOkDpl: TFloatField
      FieldName = 'ValDspOkDpl'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLFatCabEntPagante: TIntegerField
      FieldName = 'EntPagante'
    end
    object QrSrvLFatCabValOutToFat: TFloatField
      FieldName = 'ValOutToFat'
    end
    object QrSrvLFatCabValDesToFat: TFloatField
      FieldName = 'ValDesToFat'
    end
    object QrSrvLFatCabCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
    end
    object QrSrvLFatCabCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
    object QrSrvLFatCabGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrSrvLFatCabSerNF: TWideStringField
      FieldName = 'SerNF'
      Size = 3
    end
    object QrSrvLFatCabNumNF: TIntegerField
      FieldName = 'NumNF'
    end
    object QrSrvLFatCabDtaFimFat: TDateTimeField
      FieldName = 'DtaFimFat'
    end
    object QrSrvLFatCabNO_ENTPAGANTE: TWideStringField
      FieldName = 'NO_ENTPAGANTE'
      Size = 100
    end
    object QrSrvLFatCabFilial: TIntegerField
      FieldName = 'Filial'
    end
  end
  object QrSrvLOSOpeGrn: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrSrvLOSOpeGrnBeforeClose
    AfterScroll = QrSrvLOSOpeGrnAfterScroll
    SQL.Strings = (
      'SELECT slc.Nome NO_SrvLCad, prm.Nome NO_SrvLPrmCad,  '
      
        ' ELT(oog.FrmFtrCobr + 1,"Indefinido","Valor total","Valor unit'#225'r' +
        'io") NO_FrmFtrCobr,'
      
        ' ELT(oog.FrmFtrPaga + 1,"Indefinido","Valor total","Valor unit'#225'r' +
        'io") NO_FrmFtrPaga,'
      
        ' ELT(oog.FrmRatPaga + 1,"Indefinido","Valor total","Valor por pe' +
        'ssoa") NO_FrmRatPaga,'
      'umc.Sigla SIGLA_UMC, ump.Sigla SIGLA_UMP, '
      'aec.Nome NO_AgeEquiCab, oog.*  '
      'FROM srvlosopegrn oog '
      'LEFT JOIN srvlcad slc ON slc.Codigo=oog.SrvLCad '
      'LEFT JOIN srvlprmcad prm ON prm.Codigo=oog.SrvLPrmCad '
      'LEFT JOIN ageeqicab aec ON oog.AgeEqiCab=aec.Codigo '
      'LEFT JOIN unidmed umc ON umc.Codigo=oog.UnidMedCobr '
      'LEFT JOIN unidmed ump ON ump.Codigo=oog.UnidMedPaga '
      'WHERE oog.Codigo=2'
      '')
    Left = 80
    Top = 152
    object QrSrvLOSOpeGrnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLOSOpeGrnControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrvLOSOpeGrnSrvLCad: TIntegerField
      FieldName = 'SrvLCad'
    end
    object QrSrvLOSOpeGrnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLOSOpeGrnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLOSOpeGrnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLOSOpeGrnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLOSOpeGrnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLOSOpeGrnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLOSOpeGrnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLOSOpeGrnNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSrvLOSOpeGrnNO_SrvLCad: TWideStringField
      FieldName = 'NO_SrvLCad'
      Size = 60
    end
    object QrSrvLOSOpeGrnSrvLPrmCad: TIntegerField
      FieldName = 'SrvLPrmCad'
    end
    object QrSrvLOSOpeGrnNO_SrvLPrmCad: TWideStringField
      FieldName = 'NO_SrvLPrmCad'
      Size = 60
    end
    object QrSrvLOSOpeGrnNO_AgeEquiCab: TWideStringField
      FieldName = 'NO_AgeEquiCab'
      Size = 255
    end
    object QrSrvLOSOpeGrnAgeEqiCab: TIntegerField
      FieldName = 'AgeEqiCab'
    end
    object QrSrvLOSOpeGrnUnidMedCobr: TIntegerField
      FieldName = 'UnidMedCobr'
    end
    object QrSrvLOSOpeGrnQtdRlzCobr: TFloatField
      FieldName = 'QtdRlzCobr'
    end
    object QrSrvLOSOpeGrnValFtrCobr: TFloatField
      FieldName = 'ValFtrCobr'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrSrvLOSOpeGrnFrmFtrCobr: TSmallintField
      FieldName = 'FrmFtrCobr'
    end
    object QrSrvLOSOpeGrnUnidMedPaga: TIntegerField
      FieldName = 'UnidMedPaga'
    end
    object QrSrvLOSOpeGrnQtdRlzPaga: TFloatField
      FieldName = 'QtdRlzPaga'
    end
    object QrSrvLOSOpeGrnValRatPaga: TFloatField
      FieldName = 'ValRatPaga'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnFrmRatPaga: TSmallintField
      FieldName = 'FrmRatPaga'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnFrmFtrPaga: TSmallintField
      FieldName = 'FrmFtrPaga'
    end
    object QrSrvLOSOpeGrnValTotCobr: TFloatField
      FieldName = 'ValTotCobr'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnNO_FrmRatPaga: TWideStringField
      FieldName = 'NO_FrmRatPaga'
      Size = 60
    end
    object QrSrvLOSOpeGrnSIGLA_UMC: TWideStringField
      FieldName = 'SIGLA_UMC'
      Size = 6
    end
    object QrSrvLOSOpeGrnSIGLA_UMP: TWideStringField
      FieldName = 'SIGLA_UMP'
      Size = 6
    end
    object QrSrvLOSOpeGrnNO_FrmFtrCobr: TWideStringField
      FieldName = 'NO_FrmFtrCobr'
      Size = 60
    end
    object QrSrvLOSOpeGrnNO_FrmFtrPaga: TWideStringField
      FieldName = 'NO_FrmFtrPaga'
      Size = 60
    end
    object QrSrvLOSOpeGrnValFtrPaga: TFloatField
      FieldName = 'ValFtrPaga'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrSrvLOSOpeGrnValTotDesp: TFloatField
      FieldName = 'ValTotDesp'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnAutorizado: TSmallintField
      FieldName = 'Autorizado'
    end
  end
  object QrSrvLOSOpeAtrDef: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT "srvlosopeatrdef" TABELA, '
      
        'def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ATRITS,' +
        ' '
      'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,'
      'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp'
      'FROM srvlosopeatrdef def'
      'LEFT JOIN srvlosopeatrits its ON its.Controle=def.AtrIts '
      'LEFT JOIN srvlosopeatrcad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc>0'
      ' '
      'UNION  '
      ' '
      'SELECT "srvlosopeatrtxt" TABELA,'
      'def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, '
      'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,'
      'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp '
      'FROM srvlosopeatrtxt def '
      'LEFT JOIN srvlosopeatrcad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc>0'
      ' '
      'ORDER BY NO_CAD, NO_ITS ')
    Left = 80
    Top = 244
    object QrSrvLOSOpeAtrDefID_Item: TIntegerField
      FieldName = 'ID_Item'
      Required = True
    end
    object QrSrvLOSOpeAtrDefID_Sorc: TIntegerField
      FieldName = 'ID_Sorc'
      Required = True
    end
    object QrSrvLOSOpeAtrDefAtrCad: TIntegerField
      FieldName = 'AtrCad'
      Required = True
    end
    object QrSrvLOSOpeAtrDefATRITS: TFloatField
      FieldName = 'ATRITS'
      Required = True
    end
    object QrSrvLOSOpeAtrDefCU_CAD: TIntegerField
      FieldName = 'CU_CAD'
    end
    object QrSrvLOSOpeAtrDefCU_ITS: TLargeintField
      FieldName = 'CU_ITS'
    end
    object QrSrvLOSOpeAtrDefAtrTxt: TWideStringField
      FieldName = 'AtrTxt'
      Size = 255
    end
    object QrSrvLOSOpeAtrDefNO_CAD: TWideStringField
      FieldName = 'NO_CAD'
      Size = 30
    end
    object QrSrvLOSOpeAtrDefNO_ITS: TWideStringField
      FieldName = 'NO_ITS'
      Size = 255
    end
    object QrSrvLOSOpeAtrDefAtrTyp: TSmallintField
      FieldName = 'AtrTyp'
    end
    object QrSrvLOSOpeAtrDefTABELA: TWideStringField
      FieldName = 'TABELA'
      Size = 30
    end
  end
  object frxDsSrvLOSOpeAtrDef: TfrxDBDataset
    UserName = 'frxDsSrvLOSOpeAtrDef'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID_Item=ID_Item'
      'ID_Sorc=ID_Sorc'
      'AtrCad=AtrCad'
      'ATRITS=ATRITS'
      'CU_CAD=CU_CAD'
      'CU_ITS=CU_ITS'
      'AtrTxt=AtrTxt'
      'NO_CAD=NO_CAD'
      'NO_ITS=NO_ITS'
      'AtrTyp=AtrTyp'
      'TABELA=TABELA')
    DataSet = QrSrvLOSOpeAtrDef
    BCDToCurrency = False
    Left = 80
    Top = 292
  end
end
