unit UnSrvL_Tabs;
{ Colocar no MyListas:

Uses UnSrvL_Tabs;


//
function TMyListas.CriaListaTabelas(:
      UnUnSrvL_Tabs.CarregaListaTabelas(Lista);
//
function TMyListas.CriaListaSQL:
    UnUnSrvL_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
    UnUnSrvL_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      UnUnSrvL_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      UnUnSrvL_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    UnUnSrvL_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  UnUnSrvL_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts, dmkGeral, UnDmkEnums;

type
  TUnSrvL_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

var
  SrvL_Tabs: TUnSrvL_Tabs;


//const
  // Relacionamentos com a tabela de SrvL //////////////////////////////////////
  //                                                                          //
  // FIM Relacionamentos com a tabela de SrvL //////////////////////////////////

implementation

uses UMySQLModule;

function TUnSrvL_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, 'EstatusOSs', '');                          //   - Status (passos) das OSs                            //////////
    //
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLCad'), '');
    //
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLFatCab'), '');
    //MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLFatOpe'), '');
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLFatPrz'), 'SrvLOSPrz');
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLHonCab'), '');
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLHonAge'), '');
    //
    //MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLOpeCad'), '');
    //MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLOpePrm'), '');
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLOSCab'), '');                // OS de v�rias opera��es
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLOSOpeGrn'), '');             // Opera��o em si
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLOSOpeDsp'), '');             // Despesas da opera��o
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLOSOpeAge'), '');             // Agentes operacionais da opera��o
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLOSOpeCPA'), '');             // Cart�o Ponto dos Agentes operacionais
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLOSPrz'), '');
    // Atributos de Lugares
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLOSOpeAtrCad'), '_atr_cad');  // Cadastro do atributo                               //////////
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLOSOpeAtrIts'), '_atr_its');  // Cadastro do valores para cada atributo             //////////
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLOSOpeAtrDef'), '_atr_def');  // Defini��o dos atributos e valores                  //////////
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLOSOpeAtrTxt'), '_atr_txt');  // Texto livre do atributo selecionado                //////////
    //                                                                                                                      //////////
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLPOCab'), LowerCase('SrvLOSCab'));
    //
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLPrmCad'), '');
    MyLinguas.AdTbLst(Lista, False, LowerCase('SrvLPrmNvs'), '');
    //
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnSrvL_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('EstatusOSs') then
  begin
    if FListaSQL.Count = 0 then  // evitar duplica��o de cabe�alho
      FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0000_ABERTURA) + '|"Abertura de OS"');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0100_PRE_ATEN) + '|"Pr�-atendimento"');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0200_VISTORIA) + '|"Vistoria"');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0300_EM_ORCAM) + '|"Em or�amento"');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0400_ORCA_PAS) + '|"Or�amento passado"');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0450_ORCA_OK_) + '|"Or�amento aprovado"');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0500_AGEN_EXE) + '|"Agendado a execu��o"');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0550_AGEN_OK_) + '|"Confirmado Agendamento"');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0600_EM_EXECU) + '|"Em execu��o "');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0700_EXE_FINA) + '|"Execu��o finalizada"');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0750_CEX_ENVI) + '|"Comprovante Enviado"');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_0800_FATURADO) + '|"Faturado"');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_1000_POS_VEND) + '|"P�s venda"');
    FListaSQL.Add(Geral.FF0(CO_BUG_STATUS_2000_O_S_BAIX) + '|"OS baixada"');
  end else
  ;
end;

function TUnSrvL_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades","Cadastro de pessoas f�sicas e jur�dicas (clientes, fornecedores, etc.)"');
    //FListaSQL.Add('"",""');
  end;
end;


function TUnSrvL_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
  TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //
  //end else
  if Uppercase(Tabela) = Uppercase('EstatusOSs') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SrvLCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SrvLFatCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SrvLFatOpe') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SrvLHonCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SrvLHonAge') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
(*
  if Uppercase(Tabela) = Uppercase('SrvLOpeCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SrvLOpePrm') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
*)
  if Uppercase(Tabela) = Uppercase('SrvLOSCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SrvLOSOpeGrn') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
(*
  if Uppercase(Tabela) = Uppercase('SrvLOSOpePrm') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
  end else
*)
  if Uppercase(Tabela) = Uppercase('SrvLOSOpeDsp') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SrvLOSOpeAge') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Agente';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SrvLOSOpeCPA') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Nivel4';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SrvLOSPrz') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SrvLPrmCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('SrvLPrmNvs') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  ;
end;

function TUnSrvL_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
  TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    //end else
    if Uppercase(Tabela) = Uppercase('EstatusOSs') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '999999999';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeCorIni';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := IntToStr($FFFFFF); //clWhite);
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeCorFim';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := IntToStr($FFFFFF); //clWhite);
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeCorDir';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeCorFon';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := IntToStr($000000); //clBlack);
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeCorHin';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := IntToStr($0000FF); //clRed);
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Execucao';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  // 0 - Nao definido, 1=Em ser 2=Realizada 3=Cancelada
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SrvLCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ItemListaServico';     // LC116/03
      FRCampos.Tipo       := 'varchar(5)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodigoCnae';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMed';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WrnLctDay';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  // 0=Nao, 1=Sim
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SrvLFatCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntPagante';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValTotCbr';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValDspToDsc';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValOutToFat';  //  Manual ??
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValDesToFat'; // Conforme Prazo escolhido
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValLiqToFat';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValLiqOkFat';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValDspToDpl';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValDspOkDpl';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondicaoPG';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
      New(FRCampos);
      FRCampos.Field      := 'CartEmis';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Genero';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerNF';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaFimFat';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SrvLFatOpe') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvLOSOpeCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SrvLHonCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'AnoMesCmpt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'ValTotCbr';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'ValTotRat';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMesEmss';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsaQualDta';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  // 0=Indefinido, 1=Cadastro Agente, 2=Informada aqui
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SrvLHonAge') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
(*
    if Uppercase(Tabela) = Uppercase('SrvLOpeCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //

    end else
    if Uppercase(Tabela) = Uppercase('SrvLOpePrm') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      /
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
*)
    if Uppercase(Tabela) = Uppercase('SrvLOSCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvLPre';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvLCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtAbertura';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtEncerrad';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValTotCobr';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValRatPaga';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValTotDesp';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValTotPaga';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Estatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SrvLOSOpeGrn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvLCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvLPrmCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeEqiCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //////////////////////////////////////////////////////////////////////////
      //
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMedCobr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdRlzCobr';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValFtrCobr';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmFtrCobr';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // 0=Indefinido
      FRCampos.Extra      := '';  // 1=Valor total
      FLCampos.Add(FRCampos);     // 2=Por unidade
      //
      New(FRCampos);
      FRCampos.Field      := 'ValTotCobr';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMedPaga';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdRlzPaga';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValRatPaga';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmRatPaga';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // 0=Indef;
      FRCampos.Extra      := '';  // 1=Divide ValRatPaga por (qtd pessoas * fator pesso);
      FLCampos.Add(FRCampos);     // 2=ValRatPaga por pessoa
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmFtrPaga';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // 0=Indefinido
      FRCampos.Extra      := '';  // 1=Valor total
      FLCampos.Add(FRCampos);     // 2=Por unidade
      //
      New(FRCampos);
      FRCampos.Field      := 'ValFtrPaga';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValTotDesp';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvLFatCab'; // Faturamento do servi�o
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvLHonCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaExeIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaExeFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WrnLctDay';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  // 0=Nao, 1=Sim
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AgeEqiCfg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValTotPaga';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Autorizado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaLstCPA';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
(*
    if Uppercase(Tabela) = Uppercase('SrvLOSOpePrm') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMedCobr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdMaxCobr';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValFtrCobr';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmFtrCobr';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // 0=Indefinido
      FRCampos.Extra      := '';  // 1=Valor total
      FLCampos.Add(FRCampos);     // 2=Por unidade
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMedPaga';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdMaxPaga';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValRatPaga';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmRatPaga';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // 0=Indef;
      FRCampos.Extra      := '';  // 1=Divide ValRatPaga por (qtd pessoas * fator pesso);
      FLCampos.Add(FRCampos);     // 2=ValRatPaga por pessoa
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmFtrPaga';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // 0=Indefinido
      FRCampos.Extra      := '';  // 1=Valor total
      FLCampos.Add(FRCampos);     // 2=Por unidade
      //
      //
    end else
*)
    if Uppercase(Tabela) = Uppercase('SrvLOSOpeDsp') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Genero';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Agente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornece';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Debito';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmCtbDspFat'; // Forma de contabilizar despesas no faturamento
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SrvLOSOpeAge') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Agente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Responsa';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'FatorPrm';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'ValPremio';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvLHonCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GruAddSeq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SrvLOSOpeCPA') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel4';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaCompet';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HrInnDay';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HrOutMid';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HrInnMid';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HrOutDay';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemInterv';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorPrm';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValPremio';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValEverAut';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SrvLOSPrz') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Condicao';  // PediPrzCab
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Parcelas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescoPer';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BaseVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescoVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TotaVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Escolhido';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('SrvLPrmCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvLCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtValIni';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtValFim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMedCobr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmFtrCobr';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // 0=Indefinido
      FRCampos.Extra      := '';  // 1=Valor total
      FLCampos.Add(FRCampos);     // 2=Por unidade
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMedPaga';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmRatPaga';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // 0=Indef;
      FRCampos.Extra      := '';  // 1=Divide ValRatPaga por (qtd pessoas * fator pesso);
      FLCampos.Add(FRCampos);     // 2=ValRatPaga por pessoa
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmFtrPaga';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // 0=Indefinido
      FRCampos.Extra      := '';  // 1=Valor total
      FLCampos.Add(FRCampos);     // 2=Por unidade
      //
    end else
    if Uppercase(Tabela) = Uppercase('SrvLPrmNvs') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMedCobr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmFtrCobr';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // 0=Indefinido
      FRCampos.Extra      := '';  // 1=Valor total
      FLCampos.Add(FRCampos);     // 2=Por unidade
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdMaxCobr';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValFtrCobr';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMedPaga';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmRatPaga';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // 0=Indef;
      FRCampos.Extra      := '';  // 1=Divide ValRatPaga por (qtd pessoas * fator pesso);
      FLCampos.Add(FRCampos);     // 2=ValRatPaga por pessoa
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdMaxPaga';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValRatPaga';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmFtrPaga';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // 0=Indefinido
      FRCampos.Extra      := '';  // 1=Valor total
      FLCampos.Add(FRCampos);     // 2=Por unidade
      //
      //
    end else
    ;
  except
    raise;
    Result := False;
  end;
end;

function TUnSrvL_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
  TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
(*
      New(FRCampos);
      FRCampos.Field      := '';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else
    ;
  except
    raise;
    Result := False;
  end;
end;

function TUnSrvL_Tabs.CompletaListaFRJanelas(FRJanelas:
  TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // LOC-SERVI-001 :: Cadastro de Servi�os
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-001';
  FRJanelas.Nome      := 'FmSrvLCad';
  FRJanelas.Descricao := 'Cadastro de Servi�os';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-002 :: Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-002';
  FRJanelas.Nome      := 'FmSrvLOSCab';
  FRJanelas.Descricao := 'Ordem de Servi�o';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-003 :: Escalonamento de Servi�o
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-003';
  FRJanelas.Nome      := 'FmSrvLOSEsc';
  FRJanelas.Descricao := 'Escalonamento de Servi�o';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
(*
  SrvLOpeCad
  SrvLOpePrm
*)
  //
  // LOC-SERVI-004 :: Item de Servi�o de OS
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-004';
  FRJanelas.Nome      := 'FmSrvLOSOpeGrn';
  FRJanelas.Descricao := 'Item de Servi�o de OS';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
(*
  // LOC-SERVI-005 :: Pr�mios de Servi�o de OS
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-005';
  FRJanelas.Nome      := 'FmSrvLOSOpePrm';
  FRJanelas.Descricao := 'Pr�mios de Servi�o de OS';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
*)
  // LOC-SERVI-006 :: Cadastro de Pr�mios
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-006';
  FRJanelas.Nome      := 'FmSrvLPrmCad';
  FRJanelas.Descricao := 'Cadastro de Pr�mios';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-007 :: N�vel de Pr�mio
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-007';
  FRJanelas.Nome      := 'FmSrvLPrmNvs';
  FRJanelas.Descricao := 'N�vel de Pr�mio';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-008 :: Despesa de Item de OS
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-008';
  FRJanelas.Nome      := 'FmSrvLOSOpeDsp';
  FRJanelas.Descricao := 'Despesa de Item de OS';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-009 :: Faturamento de OSs
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-009';
  FRJanelas.Nome      := 'FmSrvLFatCab';
  FRJanelas.Descricao := 'Faturamento de OSs';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-010 :: Itens de Faturamento de OSs
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-010';
  FRJanelas.Nome      := 'FmSrvLFatOpe';
  FRJanelas.Descricao := 'Itens de Faturamento de OSs';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-012 :: Honor�rios de OSs
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-012';
  FRJanelas.Nome      := 'FmSrvLHonCab';
  FRJanelas.Descricao := 'Honor�rios de OSs';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-013 :: Itens de Honor�rios de OSs
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-013';
  FRJanelas.Nome      := 'FmSrvLHonAge';
  FRJanelas.Descricao := 'Itens de Honor�rios de OSs';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-014 :: OS - Condi��es de Pagamento
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-014';
  FRJanelas.Nome      := 'FmSrvLOSPrz';
  FRJanelas.Descricao := 'OS - Condi��es de Pagamento';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-015 :: Faturamento - Condi��es de Pagamento
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-015';
  FRJanelas.Nome      := 'FmSrvLFatPrz';
  FRJanelas.Descricao := 'Faturamento - Condi��es de Pagamento';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-016 :: Emiss�o de Faturamento de Servi�o
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-016';
  FRJanelas.Nome      := 'FmSrvLFatEmi';
  FRJanelas.Descricao := 'Emiss�o de Faturamento de Servi�o';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-017 :: Emiss�o de Pagamento de Honor�rios
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-017';
  FRJanelas.Nome      := 'FmSrvLHonEmi';
  FRJanelas.Descricao := 'Emiss�o de Pagamento de Honor�rios';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-018 :: Jornada de Trabalho de Agente
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-018';
  FRJanelas.Nome      := 'SrvLOSOpeCPA';
  FRJanelas.Descricao := 'Jornada de Trabalho de Agente';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-019 :: Impress�o de OS
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-019';
  FRJanelas.Nome      := 'SrvLOSCabImp';
  FRJanelas.Descricao := 'Impress�o de OS';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-020 :: Relat�rios de Servi�os
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-020';
  FRJanelas.Nome      := 'SrvLImp';
  FRJanelas.Descricao := 'Relat�rios de Servi�os';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  //  OSS-STATU-001 :: Status de OSs
  New(FRJanelas);
  FRJanelas.ID        := 'OSS-STATU-001';
  FRJanelas.Nome      := 'FmEstatusOSs';
  FRJanelas.Descricao := 'Status de OSs';
  FLJanelas.Add(FRJanelas);
  //
  // LOC-SERVI-021 :: Impress�o de Faturamento
  New(FRJanelas);
  FRJanelas.ID        := 'LOC-SERVI-021';
  FRJanelas.Nome      := 'SrvLFatCabImp';
  FRJanelas.Descricao := 'Impress�o de Faturamento';
  FRJanelas.Modulo    := 'SrvL';
  FLJanelas.Add(FRJanelas);
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;



(*
DELETE FROM srvloscab;
DELETE FROM srvlosopeage;
/*srvlosopeatrcad*/
DELETE FROM srvlosopeatrdef;
/*srvlosopeatrits*/
DELETE FROM srvlosopeatrtxt;
DELETE FROM srvlosopecpa;
DELETE FROM srvlosopedsp;
DELETE FROM srvlosopegrn;
DELETE FROM srvlosprz;

DELETE FROM srvlfatcab;
DELETE FROM srvlfatprz;

DELETE FROM srvlhonage;
DELETE FROM srvlhoncab;

/*srvlpocab*/
/*srvlprmcad*/
/*srvlprmnvs*/
*)


end.
