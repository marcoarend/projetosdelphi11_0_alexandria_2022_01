unit SrvLHonCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, AppListas, dmkDBGridZTO, UnProjGroup_Consts,
  frxClass, frxDBSet;

type
  THackDBGrid = class(TDBGrid);
  TFmSrvLHonCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    QrSrvLHonCab: TmySQLQuery;
    DsSrvLHonCab: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    EdEmpresa: TdmkEditCB;
    Label18: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label12: TLabel;
    TPDtHrIni: TdmkEditDateTimePicker;
    EdDtHrIni: TdmkEdit;
    Label13: TLabel;
    TPDtHrFim: TdmkEditDateTimePicker;
    EdDtHrFim: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label11: TLabel;
    EdAnoMesCmpt: TdmkEdit;
    Label14: TLabel;
    QrAgeOSs: TmySQLQuery;
    DsAgeOSs: TDataSource;
    DBEdit9: TDBEdit;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    BtFat: TBitBtn;
    PMFat: TPopupMenu;
    Fatura1: TMenuItem;
    Gerabloqueto1: TMenuItem;
    N4: TMenuItem;
    Excluifaturamento1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DGDados: TdmkDBGridZTO;
    QrSrvLOSOpeAge: TmySQLQuery;
    DsSrvLOSOpeAge: TDataSource;
    QrLctFatRef: TmySQLQuery;
    QrLctFatRefPARCELA: TIntegerField;
    QrLctFatRefCodigo: TIntegerField;
    QrLctFatRefControle: TIntegerField;
    QrLctFatRefConta: TIntegerField;
    QrLctFatRefLancto: TLargeintField;
    QrLctFatRefValor: TFloatField;
    QrLctFatRefVencto: TDateField;
    DsLctFatRef: TDataSource;
    PB1: TProgressBar;
    QrSrvLHonCabFilial: TIntegerField;
    QrSrvLHonCabAnoMesCmpt_TXT: TWideStringField;
    QrSrvLHonCabNO_EMPRESA: TWideStringField;
    QrSrvLHonCabDtHrFim_TXT: TWideStringField;
    QrSrvLHonCabCodigo: TIntegerField;
    QrSrvLHonCabNome: TWideStringField;
    QrSrvLHonCabEmpresa: TIntegerField;
    QrSrvLHonCabAnoMesCmpt: TIntegerField;
    QrSrvLHonCabValTotRat: TFloatField;
    QrSrvLHonCabDtHrIni: TDateTimeField;
    QrSrvLHonCabDtHrFim: TDateTimeField;
    QrSrvLHonCabLk: TIntegerField;
    QrSrvLHonCabDataCad: TDateField;
    QrSrvLHonCabDataAlt: TDateField;
    QrSrvLHonCabUserCad: TIntegerField;
    QrSrvLHonCabUserAlt: TIntegerField;
    QrSrvLHonCabAlterWeb: TSmallintField;
    QrSrvLHonCabAtivo: TSmallintField;
    QrAgeOSsAgente: TIntegerField;
    QrAgeOSsNO_AGENTE: TWideStringField;
    QrAgeOSsValPremio: TFloatField;
    QrSrvLOSOpeAgeNO_SrvlCad: TWideStringField;
    QrSrvLOSOpeAgeCodigo: TIntegerField;
    QrSrvLOSOpeAgeControle: TIntegerField;
    QrSrvLOSOpeAgeConta: TIntegerField;
    QrSrvLOSOpeAgeAgente: TIntegerField;
    QrSrvLOSOpeAgeResponsa: TSmallintField;
    QrSrvLOSOpeAgeValPremio: TFloatField;
    QrSrvLOSOpeAgeLk: TIntegerField;
    QrSrvLOSOpeAgeDataCad: TDateField;
    QrSrvLOSOpeAgeDataAlt: TDateField;
    QrSrvLOSOpeAgeUserCad: TIntegerField;
    QrSrvLOSOpeAgeUserAlt: TIntegerField;
    QrSrvLOSOpeAgeAlterWeb: TSmallintField;
    QrSrvLOSOpeAgeAtivo: TSmallintField;
    QrSrvLOSOpeAgeSrvLCad: TIntegerField;
    QrSrvLFatPrz: TmySQLQuery;
    QrSrvLFatPrzCodigo: TIntegerField;
    QrSrvLFatPrzControle: TIntegerField;
    QrSrvLFatPrzCondicao: TIntegerField;
    QrSrvLFatPrzNO_CONDICAO: TWideStringField;
    QrSrvLFatPrzDescoPer: TFloatField;
    QrSrvLFatPrzEscolhido: TSmallintField;
    QrSrvLFatPrzParcelas: TIntegerField;
    QrSrvLFatPrzVAL_COM_DESCO: TFloatField;
    QrSrvLFatPrzVAL_PARCELA: TFloatField;
    QrSrvLFatPrzORC_COM_DESCO: TFloatField;
    QrSrvLFatPrzORC_PARCELA: TFloatField;
    QrSrvLFatPrzINV_COM_DESCO: TFloatField;
    QrSrvLFatPrzINV_PARCELA: TFloatField;
    DsSrvLFatPrz: TDataSource;
    QrNulos: TmySQLQuery;
    QrNulosCtrlSHA: TIntegerField;
    QrNulosCodigo: TIntegerField;
    QrNulosAgente: TIntegerField;
    QrAgeOSsCtrlSHA: TIntegerField;
    GroupBox1: TGroupBox;
    dmkDBGridZTO1: TdmkDBGridZTO;
    GroupBox16: TGroupBox;
    DBGLctFatRef: TDBGrid;
    Agenteselecionado1: TMenuItem;
    odosagentes1: TMenuItem;
    QrAgeErr: TmySQLQuery;
    frxLOC_SERVI_012_Err: TfrxReport;
    frxDsAgeErr: TfrxDBDataset;
    QrAgeErrHow_CondiCaoPG: TWideStringField;
    QrAgeErrHow_GeneroHon: TWideStringField;
    QrAgeErrHow_DiaPagaHon: TWideStringField;
    QrAgeErrHow_UsaQualCrt: TWideStringField;
    QrAgeErrHow_CartEmis: TWideStringField;
    QrAgeErrHow_CartGera: TWideStringField;
    QrAgeErrNO_AGENTE: TWideStringField;
    QrAgeErrEntidade: TIntegerField;
    PMAgentes: TPopupMenu;
    Irparajaneladecadastrodoagente1: TMenuItem;
    QrAgeEntCad: TmySQLQuery;
    QrAgeEntCadCodigo: TIntegerField;
    QrAgeEntCadCondicaoPG: TIntegerField;
    QrAgeEntCadGeneroHon: TIntegerField;
    QrAgeEntCadDiaPagaHon: TSmallintField;
    QrAgeEntCadUsaQualCrt: TSmallintField;
    QrAgeEntCadCartEmis: TIntegerField;
    QrAgeEntCadEmpresa: TIntegerField;
    QrAgeEntCadControle: TIntegerField;
    RGUsaQualDta: TdmkRadioGroup;
    EdAnoMesEmss: TdmkEdit;
    Label4: TLabel;
    dmkRadioGroup1: TDBRadioGroup;
    Label10: TLabel;
    DBEdit3: TDBEdit;
    QrSrvLHonCabAnoMesEmss_TXT: TWideStringField;
    QrSrvLHonCabAnoMesEmss: TIntegerField;
    QrSrvLHonCabUsaQualDta: TSmallintField;
    AgenteSelecionado2: TMenuItem;
    odosAgentes2: TMenuItem;
    AgenteSelecionado3: TMenuItem;
    QrLoc: TmySQLQuery;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSrvLHonCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSrvLHonCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrSrvLHonCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrSrvLHonCabBeforeClose(DataSet: TDataSet);
    procedure BtFatClick(Sender: TObject);
    procedure Visualizarbloquetos1Click(Sender: TObject);
    procedure DBGSrvLFatPrzCellClick(Column: TColumn);
    procedure QrLctFatRefCalcFields(DataSet: TDataSet);
    procedure QrAgeOSsAfterScroll(DataSet: TDataSet);
    procedure QrAgeOSsBeforeClose(DataSet: TDataSet);
    procedure odosagentes1Click(Sender: TObject);
    procedure frxLOC_SERVI_012_ErrGetValue(const VarName: string;
      var Value: Variant);
    procedure Agenteselecionado1Click(Sender: TObject);
    procedure Irparajaneladecadastrodoagente1Click(Sender: TObject);
    procedure PMAgentesPopup(Sender: TObject);
    procedure AgenteSelecionado2Click(Sender: TObject);
    procedure odosAgentes2Click(Sender: TObject);
    procedure AgenteSelecionado3Click(Sender: TObject);
    procedure PMFatPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormSrvLHonAge(SQLType: TSQLType);
    procedure MostraFormSrvLHonEmi(SQLType: TSQLType);
    function  CondicaoPGSelecionado(): Integer;
    procedure ReopenAgeEntCad(Agente: Integer);
    procedure ReopenAgeOSs(Agente: Integer);
    procedure ReopenSrvLOSOpeAge(Conta: Integer);
    //
    function  AgentesDeHonCabOK(SrvLHonCab, Empresa: Integer): Boolean;
    function  ObtemDiaPagaHon(): TDateTime;
    procedure ObtemDadosParaRecidoDeAgente(var Emitente, Beneficiario: Integer;
              var Valor, ValorP, ValorE: Double; var NumRecibo, Referente,
              ReferenteP, ReferenteE: String; var Data: TDateTime; var Sit:
              Integer);
  public
    { Public declarations }
    FRecibos: String;
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    
  end;

var
  FmSrvLHonCab: TFmSrvLHonCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, SrvLFatOpe, ModuleGeral,
  UnSrvL_PF, ModuleFin, ModuleFatura, SrvLHonEmi, MyVCLSkin, UnBloquetos,
  Principal, SrvLHonAge, UCreateFin;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSrvLHonCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSrvLHonCab.MostraFormSrvLHonEmi(SQLType: TSQLType);
var
  CondicaoPG: Integer;
  ValPago: Double;
  DiaPagaHon: TDateTime;
  Qry: TmySQLQuery;
begin
  if QrAgeOSsCtrlSHA.Value = 0 then
  begin
    SrvL_PF.VerificaNovosAgentesSrvlHon(QrSrvLHonCabCodigo.Value);
    Geral.MB_Info('Agente sem ID de faturamento! ' + sLineBreak +
    'ID Criado! A janela ser� fechada!' + sLinebreak +
    'Reabra a janela e tente novamente!');
    //
    Close;
    Exit;
  end;
  //
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    SrvL_PF.ReopenLctFatRefTotais(Qry, QrAgeOSsCtrlSHA.Value, 0, VAR_FATID_3011);
    //
    ValPago := Qry.FieldByName('Valor').AsFloat;
  finally
    Qry.Free;
  end;

  if DBCheck.CriaFm(TFmSrvLHonEmi, FmSrvLHonEmi, afmoNegarComAviso) then
  begin
    FmSrvLHonEmi.ImgTipo.SQLType := SQLType;
    FmSrvLHonEmi.FQrCab := QrSrvLHonCab;
    FmSrvLHonEmi.FDsCab := DsSrvLHonCab;
    FmSrvLHonEmi.FQrIts := QrLctFatRef;

    FmSrvLHonEmi.DBEdControle.DataSource  := DsAgeOSs;
    FmSrvLHonEmi.DBEdControle.DataField   := 'CtrlSHA';
    FmSrvLHonEmi.DBEdEmpresa.DataSource   := DsSrvLHonCab;
    FmSrvLHonEmi.DBEdNO_EMP.DataSource    := DsSrvLHonCab;
    FmSrvLHonEmi.DBEdAgente.DataSource    := DsAgeOSs;
    FmSrvLHonEmi.DBEdNO_AGENTE.DataSource := DsAgeOSs;

    FmSrvLHonEmi.FCodigo    := QrSrvLHonCabCodigo.Value;
    FmSrvLHonEmi.FEmpresa   := QrSrvLHonCabEmpresa.Value;

    FmSrvLHonEmi.ReopenCartEmis();

    FmSrvLHonEmi.FAgente    := QrAgeOSsAgente.Value;
    FmSrvLHonEmi.FDataAbriu := QrSrvLHonCabDtHrIni.Value;
    FmSrvLHonEmi.FFatID     := VAR_FATID_3011;

    // N�o h� faturamento parcial: Controle e c�digo s�o o mesmo n�mero
    FmSrvLHonEmi.FEncerra := True;
    FmSrvLHonEmi.EdControle.ValueVariant := QrSrvLHonCabCodigo.Value;
    // FIM N�o h� faturamento parcial!

    FmSrvLHonEmi.FValAFat             := QrAgeOSsValPremio.Value - ValPago;
    FmSrvLHonEmi.EdValor.ValueVariant := QrAgeOSsValPremio.Value - ValPago;

    DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);

    if SQLType = stIns then
    begin
      FmSrvLHonEmi.TPDataFat.Date := Trunc(Date);
      FmSrvLHonEmi.EdHoraFat.Text := FormatDateTime('hh:nn:ss', Now());
      //
      FmSrvLHonEmi.EdGenero.ValueVariant := DModG.QrParamsEmpCtaServicoPg.Value;
      FmSrvLHonEmi.CBGenero.KeyValue     := DModG.QrParamsEmpCtaServicoPg.Value;
      //
      ReopenAgeEntCad(QrAgeOSsAgente.Value);
      //
      if QrAgeEntCad.RecordCount > 0 then
      begin
        FmSrvLHonEmi.TPDataFat.Date            := ObtemDiaPagaHon();
        FmSrvLHonEmi.EdCondicaoPG.ValueVariant := QrAgeEntCadCondicaoPG.Value;
        FmSrvLHonEmi.CBCondicaoPG.KeyValue     := QrAgeEntCadCondicaoPG.Value;
        FmSrvLHonEmi.EdGenero.ValueVariant     := QrAgeEntCadGeneroHon.Value;
        FmSrvLHonEmi.CBGenero.KeyValue         := QrAgeEntCadGeneroHon.Value;
        case TUsaQualPredef(QrAgeEntCadUsaQualCrt.Value) of
          TUsaQualPredef.uqpEspecifico:
          begin
            FmSrvLHonEmi.EdCartEmis.ValueVariant := QrAgeEntCadCartEmis.Value;
            FmSrvLHonEmi.CbCartEmis.KeyValue     := QrAgeEntCadCartEmis.Value;
          end;
          TUsaQualPredef.uqpGeral:
          begin
            FmSrvLHonEmi.EdCartEmis.ValueVariant := DModG.QrParamsEmpCartEmisHonFun.Value;
            FmSrvLHonEmi.CbCartEmis.KeyValue     := DModG.QrParamsEmpCartEmisHonFun.Value;
          end;
        end;
      end else
      begin
        CondicaoPG := CondicaoPGSelecionado();
        //
        UMyMod.SetaCodUsuDeCodigo(FmSrvLHonEmi.EdCondicaoPG,
          FmSrvLHonEmi.CBCondicaoPG, DmFatura.QrPediPrzCab, CondicaoPG);
        //
        FmSrvLHonEmi.EdCartEmis.ValueVariant := DModG.QrParamsEmpCartEmisHonFun.Value;
        FmSrvLHonEmi.CbCartEmis.KeyValue     := DModG.QrParamsEmpCartEmisHonFun.Value;
      end;
    end else
    begin
(*
      //
      UMyMod.SetaCodUsuDeCodigo(FmSrvLHonEmi.EdCondicaoPG,
        FmSrvLHonEmi.CBCondicaoPG, DmFatura.QrPediPrzCab, QrSrvLHonCabCondicaoPG.Value);

      FmSrvLHonEmi.EdCartEmis.ValueVariant := QrSrvLHonCabCartEmis.Value;
      FmSrvLHonEmi.CBCartEmis.KeyValue     := QrSrvLHonCabCartEmis.Value;
      FmSrvLHonEmi.EdGenero.ValueVariant   := QrSrvLHonCabGenero.Value;
      FmSrvLHonEmi.CBGenero.KeyValue       := QrSrvLHonCabGenero.Value;

      //FmSrvLHonEmi.EdValorTotal.ValueVariant := QrSrvLHonCabValorTotal.Value;

      FmSrvLHonEmi.TPDataFat.Date := Trunc(QrSrvLHonCabDtaFimFat.Value);
      FmSrvLHonEmi.EdHoraFat.Text := FormatDateTime('hh:nn:ss', QrSrvLHonCabDtaFimFat.Value);
*)
    end;
    //configurar melhor
    FmSrvLHonEmi.ShowModal;
    FmSrvLHonEmi.Destroy;
  end;
end;

procedure TFmSrvLHonCab.ObtemDadosParaRecidoDeAgente(var Emitente, Beneficiario:
  Integer; var Valor, ValorP, ValorE: Double; var NumRecibo, Referente,
  ReferenteP, ReferenteE: String; var Data: TDateTime; var Sit: Integer);
var
  Servicos: String;
begin
  Servicos := '';
  QrSrvLOSOpeAge.DisableControls;
  try
    QrSrvLOSOpeAge.First;
    while not QrSrvLOSOpeAge.Eof do
    begin
      Servicos := Servicos + 'ID' + Geral.FF0(QrSrvLOSOpeAgeConta.Value) + ' ' +
        QrSrvLOSOpeAgeNO_SrvlCad.Value + ': $' + Geral.FFT(
        QrSrvLOSOpeAgeValPremio.Value, 2, siNegativo) + '. ';
      //
      QrSrvLOSOpeAge.Next;
    end;
  finally
    QrSrvLOSOpeAge.EnableControls;
  end;
  Emitente     := QrSrvLHonCabEmpresa.Value;
  Beneficiario := QrAgeOSsAgente.Value;
  Valor        := QrAgeOSsValPremio.Value;
  ValorP       := 0;
  ValorE       := 0;
  NumRecibo    := 'SrvL.H-' + Geral.FF0(QrAgeOSsCtrlSHA.Value);
  Referente    := Servicos;
  ReferenteP   := '';
  ReferenteE   := '';
  Data         := ObtemDiaPagaHon();
  Sit          := 0;
end;

function TFmSrvLHonCab.ObtemDiaPagaHon(): TDateTime;
var
  Ano, Mes, Dia: Word;
begin
  case QrSrvLHonCabUsaQualDta.Value of
    0: Result := DmodG.ObtemAgora();
    1:
    begin
      Dia := QrAgeEntCadDiaPagaHon.Value;
      if Dia = 0 then
        Dia := 1;
      dmkPF.AnoMesDecode(QrSrvLHonCabAnoMesEmss.Value, Ano, Mes);
      Result := EncodeDate(Ano, Mes, Dia);
    end;
    2: Result := QrSrvLHonCabDtHrIni.Value;
    3: Result := QrSrvLHonCabDtHrFim.Value;
    4: Result := DmodG.ObtemAgora();
    else
    begin
      Result := DmodG.ObtemAgora();
      Geral.MB_Erro(
      'Item de "Qual data usa para emiss�o em massa de pagamento de honor�rios" n�o implementado');
    end;
  end;
  if Result < 2 then
    Result := DmodG.ObtemAgora();
end;

procedure TFmSrvLHonCab.odosagentes1Click(Sender: TObject);
  function InsereAtual(): Boolean;
  const
    Represen = 0;
    Encerra  = False;
    FatID    = VAR_FATID_3011;
  var
    //DtaFimFat: String;
    Controle, CondicaoPg, CartEmis, Genero, Filial, Entidade, FatNum, Terceiro,
    TipoCart, FatGru: Integer;
    DataAbriu, DataEncer, DataFat: TDateTime;
    ValTotal: Double;
  begin
    Result         := False;
    Controle       := QrAgeOSsCtrlSHA.Value;
    //Estatus        := CO_BUG_STATUS_0800_FATURADO;
    ValTotal       := QrAgeOSsValPremio.Value;
    //DtaFimFat      := ObtemDiaPagaHon();
    DataFat        := ObtemDiaPagaHon();
    CondicaoPg     := QrAgeEntCadCondicaoPG.Value;
    Genero         := QrAgeEntCadGeneroHon.Value;
    case TUsaQualPredef(QrAgeEntCadUsaQualCrt.Value) of
      TUsaQualPredef.uqpEspecifico: CartEmis := QrAgeEntCadCartEmis.Value;
      TUsaQualPredef.uqpGeral:      CartEmis := DModG.QrParamsEmpCartEmisHonFun.Value;
      else CartEmis := 0;
    end;
    //
    // N�o h� faturamento parcial: Controle e c�digo s�o o mesmo n�mero
    FatNum      := Controle;
    // FIM N�o h� faturamento parcial
    Filial      := QrSrvLHonCabFilial.Value;
    Entidade    := QrSrvLHonCabEmpresa.Value;
    Terceiro    := QrAgeOSsAgente.Value;
    DataAbriu   := QrSrvLHonCabDtHrIni.Value;
    DataEncer   := QrSrvLHonCabDtHrFim.Value;
    TipoCart    := DModFin.ObtemTipoDeCarteira(CartEmis);
    FatGru      := QrSrvLHonCabCodigo.Value;
    //
    Result := SrvL_PF.PagamentoHonorarioFuncionario(FatGru, Entidade, FatID,
    FatNum, Terceiro, DataAbriu, DataEncer, (*IDDuplicata, NumeroNF, SerieNF,*)
    CartEmis, TipoCart, Genero, CondicaoPG, Represen, (*TipoFatura, FaturaDta,
    Financeiro,*) ValTotal, Encerra, nil, DataFat, False);
  end;
begin
  if not AgentesDeHonCabOK(QrSrvLHonCabCodigo.Value, QrSrvLHonCabEmpresa.Value) then
    Exit;
  if Geral.MB_Pergunta(
  'Confirma o lan�amento de TODOS pagamentos ainda n�o lan�ados?') = ID_YES then
  begin
    QrAgeOSs.DisableControls;
    try
      QrAgeOSs.First;
      while not QrAgeOSs.Eof do
      begin
        if QrLctFatRef.RecordCount = 0 then
        begin
          ReopenAgeEntCad(QrAgeOSsAgente.Value);
          InsereAtual();
        end;
        //
        QrAgeOSs.Next;
      end;
    finally
      QrAgeOSs.EnableControls;
    end;
  end;
end;

procedure TFmSrvLHonCab.odosAgentes2Click(Sender: TObject);
var
  Emitente, Beneficiario: Integer;
  Valor, ValorP, ValorE: Double;
  NumRecibo, Referente, ReferenteP, ReferenteE: String;
  Data: TDateTime;
  Sit: Integer;
  //
  SeqID: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando tabela tempor�ria!');
  Screen.Cursor := crHourGlass;
  try
    SeqID := 0;
    FRecibos :=
      UCriarFin.RecriaTempTableNovo(ntrtt_Recibos, DModG.QrUpdPID1, False);
    //
    QrAgeOSs.DisableControls;
    try
      QrAgeOSs.First;
      while not QrAgeOSs.Eof do
      begin
        ObtemDadosParaRecidoDeAgente(Emitente, Beneficiario, Valor, ValorP,
        ValorE, NumRecibo, Referente, ReferenteP, ReferenteE, Data, Sit);
        //
        SeqID := SeqID + 1;
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_recibos_', False, [
        'Emitente', 'Beneficiario', 'Valor',
        'ValorP', 'ValorE', 'NumRecibo',
        'Referente', 'ReferenteP', 'ReferenteE',
        'Data', 'Sit'], [
        'SeqID'], [
        Emitente, Beneficiario, Valor,
        ValorP, ValorE, NumRecibo,
        Referente, ReferenteP, ReferenteE,
        Data, Sit], [
        SeqID], False);
        //
        QrAgeOSs.Next;
      end;
    finally
      QrAgeOSs.EnableControls;
    end;
  finally
    Screen.Cursor := crDefault;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
  //
  GOTOy.EmiteReciboMany(FRecibos);
end;

procedure TFmSrvLHonCab.MostraFormSrvLHonAge(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmSrvLHonAge, FmSrvLHonAge, afmoNegarComAviso) then
  begin
    FmSrvLHonAge.ImgTipo.SQLType := SQLType;
    FmSrvLHonAge.FQrCab := QrSrvLHonCab;
    FmSrvLHonAge.FDsCab := DsSrvLHonCab;
    FmSrvLHonAge.FQrIts := QrAgeOSs;
    FmSrvLHonAge.FEmpresa := QrSrvLHonCabEmpresa.Value;
    //FmSrvLHonAge.FCliente := QrSrvLHonCabCliente.Value;
    FmSrvLHonAge.FCodigo  := QrSrvLHonCabCodigo.Value;
    if SQLType = stIns then
      //
    else
    begin
(*
      FmSrvLHonAge.EdControle.ValueVariant := QrSrvLHonAgeControle.Value;
*)
    end;
    FmSrvLHonAge.ReopenSrvLOSOpeAge();
    FmSrvLHonAge.ShowModal;
    FmSrvLHonAge.Destroy;
  end;
end;

procedure TFmSrvLHonCab.PMAgentesPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(Irparajaneladecadastrodoagente1, QrAgeOSs);
end;

procedure TFmSrvLHonCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrSrvLHonCab);
  //MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrSrvLHonCab, QrSrvLFatOpe);
  CabExclui1.Enabled := False;//MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrSrvLHonCab, QrAgeOSs);
end;

procedure TFmSrvLHonCab.PMFatPopup(Sender: TObject);
var
  Encerr: Boolean;
begin
  Encerr := QrSrvLHonCabDtHrFim_TXT.Value <> '';

  if not Encerr then
  begin
    MyObjects.HabilitaMenuItemItsIns(Fatura1, QrSrvLOSOpeAge);
    MyObjects.HabilitaMenuItemItsIns(Gerabloqueto1, QrLctFatRef);
    MyObjects.HabilitaMenuItemItsIns(Excluifaturamento1, QrLctFatRef);
  end else
  begin
    Fatura1.Enabled            := False;
    Gerabloqueto1.Enabled      := False;
    Excluifaturamento1.Enabled := False;
  end;
end;

procedure TFmSrvLHonCab.PMItsPopup(Sender: TObject);
var
  Encerr: Boolean;
begin
  Encerr := QrSrvLHonCabDtHrFim_TXT.Value <> '';

  if not Encerr then
  begin
    MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrSrvLHonCab);
    //MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrSrvLFatOpe);
    //MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrSrvLFatOpe);
    MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrAgeOSs);
    MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrAgeOSs);
  end else
  begin
    ItsInclui1.Enabled := False;
    ItsAltera1.Enabled := False;
    ItsExclui1.Enabled := False;
  end;
end;

procedure TFmSrvLHonCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSrvLHonCabCodigo.Value, LaRegistro.Caption[2]);
end;
procedure TFmSrvLHonCab.Visualizarbloquetos1Click(Sender: TObject);
begin
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSrvLHonCab.DefParams;
begin
  VAR_GOTOTABELA := 'srvlhoncab';
  VAR_GOTOMYSQLTABLE := QrSrvLHonCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT eci.CodCliInt Filial, ');
  VAR_SQLx.Add('CONCAT(RIGHT(AnoMesCmpt, 2), "/", LEFT(AnoMesCmpt, 4)) AnoMesCmpt_TXT, ');
  VAR_SQLx.Add('CONCAT(RIGHT(AnoMesEmss, 2), "/", LEFT(AnoMesEmss, 4)) AnoMesEmss_TXT, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(DtHrFim <= "1900-01-01", "",');
  VAR_SQLx.Add('  DATE_FORMAT(DtHrFim, "%d/%m/%Y %H:%i:%S")) DtHrFim_TXT,');
  VAR_SQLx.Add('sfh.* ');
  VAR_SQLx.Add('FROM srvlhoncab sfh');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=sfh.Empresa');
  VAR_SQLx.Add('LEFT JOIN enticliint eci ON eci.CodEnti=sfh.Empresa');
  VAR_SQLx.Add('WHERE sfh.Codigo > 0');
  //
  VAR_SQL1.Add('AND sfh.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND sfh.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sfh.Nome Like :P0');
  //
end;

procedure TFmSrvLHonCab.Irparajaneladecadastrodoagente1Click(Sender: TObject);
begin
  SrvL_PF.MostraFormAgeEntCad(QrAgeOSsAgente.Value);
end;

procedure TFmSrvLHonCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormSrvLHonAge(stUpd);
end;

procedure TFmSrvLHonCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmSrvLHonCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSrvLHonCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSrvLHonCab.ItsExclui1Click(Sender: TObject);
const
  SrvLHonCab = 0;
var
  Codigo: Integer;
  Controles: String;
  Qry: TmySQLQuery;
begin
  Controles := MyObjects.GetSelecionadosBookmark_Int1_ToStr(Self, TDBGrid(dmkDBGridZTO1), 'Controle');

  if MyObjects.FIC(Controles = '', nil, 'Nenhum item foi selecionado!') then
    Exit;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM lctfatref ',
      'WHERE Controle IN ('+ Controles +') ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Exclus�o cancelada!' + sLineBreak + 'Motivo: Este(s) item(ns) possui(em) fatura(s)!');
      Exit;
    end;
  finally
    Qry.Free;
  end;
  //
  if Geral.MB_Pergunta('Confirma a retirada dos itens selecionados?') = ID_YES then
  begin
    Codigo := QrSrvLHonCabCodigo.Value;
    (*
    // SQL ao inverso da adi��o!! primeiro 'srvlosopedsp' e s� depois 'srvlosopegrn'
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopedsp', False, [
    'FrmCtbDspFat'], [
    CO_JOKE_SQL], [
    FrmCtbDspFat], [
    'Controle IN (' + Controles + ') AND FrmCtbDspFat=0'], True) then
    begin
    *)
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopeage', False, [
      'srvlhoncab'], [
      CO_JOKE_SQL], [
      SrvLHonCab], [
      'Controle IN (' + Controles + ')'], True) then
      begin
        SrvL_PF.TotalizaSrvlHonCab(Codigo);
        LocCod(Codigo, Codigo);
      end;
    (*
    end;
    *)
  end;
end;

procedure TFmSrvLHonCab.ReopenAgeEntCad(Agente: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgeEntCad, Dmod.MyDB, [
  'SELECT *  ',
  'FROM ageentcad ',
  'WHERE Codigo=' + Geral.FF0(Agente),
  'AND Empresa=' + Geral.FF0(QrSrvLHonCabEmpresa.Value),
  '']);
end;

procedure TFmSrvLHonCab.ReopenAgeOSs(Agente: Integer);
var
  CodTXT: String;
  //
begin
  CodTXT := Geral.FF0(QrSrvLHonCabCodigo.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgeOSs, Dmod.MyDB, [
  'SELECT sha.Controle CtrlSHA, ooa.Agente, ',
  'SUM(ooa.ValPremio) ValPremio, ',
  'IF(age.Tipo=0, age.RazaoSocial, age.Nome) NO_AGENTE  ',
  'FROM srvlosopeage ooa  ',
  'LEFT JOIN entidades age ON age.Codigo=ooa.Agente  ',
  'LEFT JOIN srvlhonage sha ON sha.Codigo=' +
  CodTXT + ' AND sha.Entidade=ooa.Agente',
  'WHERE ooa.SrvLHonCab=' + CodTXT,
  'GROUP BY ooa.Agente  ',
  '']);
  //
  if Agente > 0 then
    QrAgeOSs.Locate('Agente', Agente, []);
end;

procedure TFmSrvLHonCab.ReopenSrvLOSOpeAge(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLOSOpeAge, Dmod.MyDB, [
  'SELECT oog.SrvLCad, cad.Nome NO_SrvlCad, ooa.*',
  'FROM srvlosopeage ooa  ',
  'LEFT JOIN srvlosopegrn oog ON oog.Controle=ooa.Controle',
  'LEFT JOIN srvlcad cad ON cad.Codigo=oog.SrvLCad',
  'WHERE ooa.SrvLHonCab=' + Geral.FF0(QrSrvLHonCabCodigo.Value),
  'AND ooa.Agente=' + Geral.FF0(QrAgeOSsAgente.Value),
  '']);
  //
  if Conta <> 0 then
    QrSrvLOSOpeAge.Locate('Conta', Conta, []);
end;

procedure TFmSrvLHonCab.DBGSrvLFatPrzCellClick(Column: TColumn);
var
  Escolhido, Codigo, Controle: Integer;
begin
  if Column.FieldName = CO_FldEscolhido then
  begin
    if QrSrvLFatPrzEscolhido.Value = 0 then
      Escolhido := 1
    else
      Escolhido := 0;
    //
    Codigo   := QrSrvLFatPrzCodigo.Value;
    Controle := QrSrvLFatPrzControle.Value;
    //
    // Definir todas as condi��es da OS como n�o escolhidas
    if Escolhido = 1 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosprz', False, [
      'Escolhido'], ['Codigo'], [0], [Codigo], True);
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosprz', False, [
    'Escolhido'], [
    'Controle'], [
    Escolhido
    ], [
    Controle], True) then
    begin
      SrvL_PF.AtualizaTotaisSrvLOSCab(Codigo);
      LocCod(Codigo, Codigo);
      QrSrvLFatPrz.Locate('Controle', Controle, []);
    end;
  end;
end;

procedure TFmSrvLHonCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSrvLHonCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSrvLHonCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSrvLHonCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSrvLHonCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSrvLHonCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLHonCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSrvLHonCabCodigo.Value;
  Close;
end;

procedure TFmSrvLHonCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormSrvLHonAge(stIns);
end;

procedure TFmSrvLHonCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  DModG.HabilitaEdCBEmpresa(EdEmpresa, CBEmpresa, stUpd, EdNome);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrSrvLHonCab, [PnDados],
  [PnEdita], TPDtHrIni, ImgTipo, 'srvlhoncab');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrSrvLHonCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
  //
end;

procedure TFmSrvLHonCab.BtConfirmaClick(Sender: TObject);
var
  Nome, DtHrIni, DtHrFim: String;
  Codigo, Empresa, AnoMesCmpt, AnoMesEmss, UsaQualDta: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  DtHrIni        := Geral.FDT_TP_Ed(TPDtHrIni.Date, EdDtHrIni.Text);
  DtHrFim        := Geral.FDT_TP_Ed(TPDtHrFim.Date, EdDtHrFim.Text);
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  AnoMesCmpt     := Geral.IMV(Geral.FDT(EdAnoMesCmpt.ValueVariant, 20));
  AnoMesEmss     := Geral.IMV(Geral.FDT(EdAnoMesEmss.ValueVariant, 20));
  UsaQualDta     := RGUsaQualDta.ItemIndex;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
  if MyObjects.FIC(AnoMesCmpt = 0, EdAnoMesCmpt, 'Defina a compet�ncia!') then
    Exit;
  if MyObjects.FIC((AnoMesEmss = 0) and (UsaQualDta = 1), EdAnoMesEmss,
  'Defina o m�s/ano de emiss�o!') then
    Exit;
  //ValTotCbr      := ;
  //ValTotRat      := ;
  //
  Codigo := UMyMod.BPGS1I32('srvlhoncab', 'Codigo', '', '', tsPos,
    ImgTipo.SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'srvlhoncab', False, [
  'Nome', 'Empresa', 'AnoMesCmpt',
  'DtHrIni', 'DtHrFim', 'AnoMesEmss',
  'UsaQualDta'], [
  'Codigo'], [
  Nome, Empresa, AnoMesCmpt,
  DtHrIni, DtHrFim, AnoMesEmss,
  UsaQualDta], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    SrvL_PF.TotalizaSrvlHonCab(Codigo);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmSrvLHonCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'srvlhoncab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'srvlhoncab', 'Codigo');
end;

procedure TFmSrvLHonCab.BtFatClick(Sender: TObject);
begin
  //PageControl1.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMFat, BtFat);
end;

procedure TFmSrvLHonCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

function TFmSrvLHonCab.AgentesDeHonCabOK(SrvLHonCab, Empresa: Integer): Boolean;
begin
  Result := False;
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  if MyObjects.FIC(DModG.QrParamsEmpCartEmisHonFun.Value = 0, nil,
  'Falta informar "Carteira gen�rica (geral) de pagamento de honor�rios" ' +
  'nos par�metros da filial.') then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgeErr, Dmod.MyDB, [
  'SELECT   ',
  '  ',
  'IF(aec.CondicaoPG IS NULL or aec.CondicaoPG < 1,   ',
  '"ERRO", "OK") How_CondiCaoPG,  ',
  '  ',
  'IF(aec.GeneroHon IS NULL or aec.GeneroHon < 1,   ',
  '"ERRO", "OK") How_GeneroHon,  ',
  '  ',
  'IF(aec.DiaPagaHon IS NULL or aec.DiaPagaHon < 1,   ',
  '"ERRO", "OK") How_DiaPagaHon,  ',
  '  ',
  'IF(aec.UsaQualCrt IS NULL or aec.UsaQualCrt < 2,   ',
  '"ERRO", "OK") How_UsaQualCrt,  ',
  '  ',
  'IF(aec.CartEmis IS NULL or (  ',
  'aec.CartEmis < 1 AND aec.UsaQualCrt = 2),   ',
  '"ERRO", "OK") How_CartEmis,  ',
  '  ',
  'IF(aec.UsaQualCrt IS NULL or (  ',
  Geral.FF0(DModG.QrParamsEmpCartEmisHonFun.Value) + ' < 1 AND aec.UsaQualCrt = 3),   ',
  '"ERRO", "OK") How_CartGera,  ',
  '  ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)   ',
  'NO_AGENTE, sha.Entidade  ',
  '',
  'FROM srvlhonage sha  ',
  'LEFT JOIN ageentcad aec ON aec.Codigo=sha.Entidade AND aec.Empresa=' +
  Geral.FF0(Empresa),
  'LEFT JOIN entidades ent ON ent.Codigo=sha.Entidade  ',
  'WHERE sha.Codigo=' + Geral.FF0(SrvLHonCab),
  'AND (  ',
  '  aec.Codigo IS NULL  ',
  '  OR  ',
  '  (  ',
  '    aec.CondicaoPG<1  ',
  '    OR  ',
  '    aec.GeneroHon<1  ',
  '    OR  ',
  '    aec.DiaPagaHon<1  ',
  '    OR ',
  '    aec.UsaQualCrt<2 ',
  '    OR   ',
  '    (aec.CartEmis<1 AND aec.UsaQualCrt = 2) ',
  '    OR  ',
  '    (' + Geral.FF0(DModG.QrParamsEmpCartEmisHonFun.Value) +
       ' < 1 AND aec.UsaQualCrt = 3) ',
  '  )  ',
  ')  ',
  'ORDER BY sha.Entidade',
  '']);
  //Geral.MB_SQL(Self, QrAgeErr);
  if QrAgeErr.RecordCount > 0 then
  begin
    MyObjects.FrxMostra(frxLOC_SERVI_012_Err, 'Erros que impedem pagamento');
    Exit;
  end else
    Result := True;
end;

procedure TFmSrvLHonCab.Agenteselecionado1Click(Sender: TObject);
var
  ValPago: Double;
  Qry: TmySQLQuery;
begin
  if Myobjects.FIC(QrAgeOSs.RecordCount = 0, nil,
  'Pagamento n�o realizado! Agente n�o selecionado!') then
    Exit;
  //
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    SrvL_PF.ReopenLctFatRefTotais(Qry, QrAgeOSsCtrlSHA.Value, 0, VAR_FATID_3011);
    //
    ValPago := Qry.FieldByName('Valor').AsFloat;
  finally
    Qry.Free;
  end;
  if (QrAgeOSsValPremio.Value - ValPago) <= 0 then
  begin
    Geral.MB_Aviso('A��o cancelada! Pagamento j� realizado!');
    Exit;
  end;
  MostraFormSrvLHonEmi(stIns);
end;

procedure TFmSrvLHonCab.AgenteSelecionado2Click(Sender: TObject);
const
  Codigo = 0;
var
  Emitente, Beneficiario: Integer;
  Valor, ValorP, ValorE: Double;
  NumRecibo, Referente, ReferenteP, ReferenteE: String;
  Data: TDateTime;
  Sit: Integer;
begin
  ObtemDadosParaRecidoDeAgente(Emitente, Beneficiario, Valor, ValorP, ValorE,
  NumRecibo, Referente, ReferenteP, ReferenteE, Data, Sit);
  //
  GOTOy.EmiteRecibo(Codigo, Emitente, Beneficiario, Valor, ValorP, ValorE, NumRecibo,
  Referente, ReferenteP, ReferenteE, Data, Sit);
end;

procedure TFmSrvLHonCab.AgenteSelecionado3Click(Sender: TObject);
var
  Filial, Quitados, FatNum, Terceiro: Integer;
  TabLctA: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  Terceiro := QrAgeOSsAgente.Value;
  FatNum   := QrAgeOSsCtrlSHA.Value;
  //Filial := DmFatura.ObtemFilialDeEntidade(QrSrvLHonCabEmpresa.Value);
  Filial   := QrSrvLHonCabFilial.Value;
  TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  //
  Quitados := SrvL_PF.FaturamentosQuitados(TabLctA, VAR_FATID_3011, FatNum);
  //
  if Quitados > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(Quitados) +
    ' itens j� quitados que impedem a exclus�o deste faturamento');
    Exit;
  end;
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do faturamento e de TODOS seus lan�amentos financeiros atrelados?'
  ) = ID_YES then
  begin
    if UBloquetos.DesfazerBoletosFatID(QrLoc, Dmod.MyDB, VAR_FATID_3011,
      FatNum, TabLctA) then
    begin
      Screen.Cursor := crHourGlass;
      try
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
        'DELETE FROM ' + TabLctA,
        'WHERE FatID=' + Geral.FF0(VAR_FATID_3011),
        'AND FatNum=' + Geral.FF0(FatNum),
        '']);
        //
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
        'DELETE FROM lctfatref',
        'WHERE Controle=' + Geral.FF0(FatNum),
        'AND FatID=' + Geral.FF0(VAR_FATID_3011),
        '']);
        //
        (* N�o tem m�ltiplos faturamentos na mesma OS!
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
        'DELETE FROM ? ',
        'WHERE Controle=' + Geral.FF0(FatNum),
        '']);
        *)
        //

        SrvL_PF.ReopenLctFatRef(QrLctFatRef, FatNum, 0, VAR_FATID_3011);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmSrvLHonCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmSrvLHonCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
  EdAnoMesCmpt.ValueVariant := DModG.ObtemAgora();
  EdAnoMesEmss.ValueVariant := DModG.ObtemAgora();
end;

procedure TFmSrvLHonCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSrvLHonCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvLHonCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSrvLHonCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrSrvLHonCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvLHonCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSrvLHonCab.QrAgeOSsAfterScroll(DataSet: TDataSet);
begin
  ReopenSrvLOSOpeAge(0);
  //SrvL_PF.ReopenXPrz(QrSrvLFatPrz, 'SrvLFatPrz', Codigo, Controle);
  //SrvL_PF.ReopenLctFatRef(QrLctFatRef, QrSrvLHonCabCodigo.Value, 0);

  SrvL_PF.ReopenLctFatRef(QrLctFatRef, QrAgeOSsCtrlSHA.Value, 0, VAR_FATID_3011);
end;

procedure TFmSrvLHonCab.QrAgeOSsBeforeClose(DataSet: TDataSet);
begin
  QrSrvLOSOpeAge.Close;
end;

procedure TFmSrvLHonCab.QrLctFatRefCalcFields(DataSet: TDataSet);
begin
  QrLctFatRefPARCELA.Value := QrLctFatRef.RecNo;
end;

procedure TFmSrvLHonCab.QrSrvLHonCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSrvLHonCab.QrSrvLHonCabAfterScroll(DataSet: TDataSet);
const
  Controle = 0;
var
  Codigo: Integer;
begin
  Codigo := QrSrvLHonCabCodigo.Value;
  ReopenAgeOSs(0);
end;

procedure TFmSrvLHonCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrSrvLHonCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmSrvLHonCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSrvLHonCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'srvlhoncab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSrvLHonCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLHonCab.frxLOC_SERVI_012_ErrGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', QrSrvLHonCabNO_Empresa.Value, QrSrvLHonCabEmpresa.Value, 'TODAS')
  else
  if VarName = 'VARF_DATA' then
    Value := Now()
   //
  else

end;

procedure TFmSrvLHonCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrSrvLHonCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'srvlhoncab');
  //
  Agora := DModG.ObtemAgora();
  TPDtHrIni.Date := Trunc(Agora);
  EdDtHrIni.ValueVariant := Agora;
  EdAnoMesCmpt.ValueVariant := DModG.ObtemAgora();
  EdAnoMesEmss.ValueVariant := DModG.ObtemAgora();
  //
  DModG.HabilitaEdCBEmpresa(EdEmpresa, CBEmpresa, stIns, TPDtHrIni);
end;

function TFmSrvLHonCab.CondicaoPGSelecionado(): Integer;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Condicao ',
    'FROM srvlosprz ',
    'WHERE Codigo=' + Geral.FF0(QrSrvLHonCabCodigo.Value),
    'AND Escolhido=1',
    ' ',
    '']);
    //
    Result := Dmod.QrAux.FieldByName('Condicao').AsInteger;
  finally
    Qry.Free;
  end;
end;

procedure TFmSrvLHonCab.QrSrvLHonCabBeforeClose(
  DataSet: TDataSet);
begin
  //QrSrvLFatOpe.Close;
  QrAgeOSs.Close;
  QrSrvLFatPrz.Close;
  QrLctFatRef.Close;
end;

procedure TFmSrvLHonCab.QrSrvLHonCabBeforeOpen(DataSet: TDataSet);
begin
  QrSrvLHonCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

