unit SrvLHonEmi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, dmkDBEdit, dmkValUsu, UnDmkEnums,
  UnProjGroup_Consts;

type
  TFmSrvLHonEmi = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    PnEdita: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    DBEdControle: TdmkDBEdit;
    DBEdAgente: TDBEdit;
    DBEdNO_AGENTE: TDBEdit;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    CBCondicaoPG: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    Label21: TLabel;
    TPDataFat: TdmkEditDateTimePicker;
    EdHoraFat: TdmkEdit;
    Label12: TLabel;
    EdValor: TdmkEdit;
    Label7: TLabel;
    EdCartEmis: TdmkEditCB;
    CBCartEmis: TdmkDBLookupComboBox;
    VuCondicaoPG: TdmkValUsu;
    Label8: TLabel;
    DBEdEmpresa: TDBEdit;
    Label9: TLabel;
    DBEdNO_EMP: TDBEdit;
    Label15: TLabel;
    EdGenero: TdmkEditCB;
    CBGenero: TdmkDBLookupComboBox;
    QrCartEmis: TmySQLQuery;
    QrCartEmisCodigo: TIntegerField;
    QrCartEmisNome: TWideStringField;
    QrCartEmisTipo: TIntegerField;
    QrCartEmisForneceI: TIntegerField;
    DsCartEmis: TDataSource;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdValLiqToFatChange(Sender: TObject);
    procedure EdValConsuChange(Sender: TObject);
    procedure EdValUsadoChange(Sender: TObject);
    procedure EdValOutToFatChange(Sender: TObject);
    procedure EdValDesToFatChange(Sender: TObject);
    procedure DBEdEmpresaChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaValores();
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FCodigo, FEmpresa, FAgente: Integer;
    FDataAbriu: TDateTime;
    FEncerra: Boolean;
    FValAFat: Double;
    FFatID: Integer; // ID Faturamento parcial e total???
    //
    procedure ReopenCartEmis();
  end;

  var
  FmSrvLHonEmi: TFmSrvLHonEmi;

implementation

uses UnMyObjects, Module, ModuleFatura, UMySQLModule, NFSe_PF_0201,
  ModuleGeral(*, UnBugs_Tabs, ModAgenda*), DmkDAC_PF, UnSrvL_PF, UnGFat_Jan;

{$R *.DFM}

{
procedure TFmSrvLHonEmi.BtOKClick(Sender: TObject);
const
  TipoFatura = tfatServico; (*TTipoFatura*)
  FaturaDta = dfEncerramento; (*TDataFatura*)
  Financeiro = tfinDeb; (*TTipoFinanceiro*)
  //
  // N�o usa ainda
  Represen = 0;
var
  DtaFimFat, SerNF: String;
  Controle, Estatus, CondicaoPg, CartEmis, Genero, NumNF: Integer;
  //ValLiqOkFat, ValLiqToFat, ValDesToFat, ValOutToFat: Double;
  //
  Filial, Entidade, FatNum, Terceiro, IDDuplicata, NumeroNF, TipoCart: Integer;
  DataAbriu, DataEncer: TDateTime;
  SerieNF: String;
  ValTotal: Double;
  //
  function AlteraOSCab(): Boolean;
  begin
(*
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlfatcab', False, [
    'Estatus', 'ValLiqOkFat', 'ValLiqToFat',
    'ValDesToFat', 'DtaFimFat', 'ValOutToFat',
    'CondicaoPg', 'CartEmis', 'Genero'], [
    'Codigo'], [
    Estatus, ValLiqOkFat, ValLiqToFat,
    ValDesToFat, DtaFimFat, ValOutToFat,
    CondicaoPg, CartEmis, Genero], [
    Codigo], True);
*)
  end;
var
  QuestaoTyp: TAgendaQuestao;
  QuestaoCod: Integer;
begin
  Controle       := Geral.IMV(Geral.SoNumero_TT(DBEdControle.Text));
  Estatus        := CO_BUG_STATUS_0800_FATURADO;
(*
  ValLiqOkFat    := EdValLiqOkFat.ValueVariant;
  ValLiqToFat    := EdValLiqToFat.ValueVariant;
  ValDesToFat    := EdValDesToFat.ValueVariant;
  ValOutToFat    := EdValOutToFat.ValueVariant;
*)
  ValTotal       := EdValor.ValueVariant;
  DtaFimFat      := Geral.FDT(TPDataFat.Date, 1) + ' ' + EdHoraFat.Text;
  CondicaoPg     := EdCondicaoPG.ValueVariant;
  CartEmis       := EdCartEmis.ValueVariant;
  Genero         := EdGenero.ValueVariant;
  //
  if MyObjects.FIC(CondicaoPG = 0, EdCondicaoPG, 'Informe a condi��o de pagamento!') then Exit;
  if MyObjects.FIC(CartEmis = 0, EdCartEmis, 'Informe a carteira!') then Exit;
  if MyObjects.FIC(Genero = 0, EdGenero, 'Informe a conta (do plano de contas)!') then Exit;
  //if ValLiqOkFat < 0.01 then
  if ValTotal < 0.01 then
  begin
    if FEncerra then
    begin
      if Geral.MB_Pergunta('Valor inv�lido para faturamento!' + sLineBreak +
      'Deseja encerrar assim mesmo?') <> ID_YES then
        Exit;
    end else
    begin
      Geral.MB_Aviso('Valor inv�lido para faturamento!');
      Exit;
    end;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    if AlteraOSCab() then
    begin
      // N�o h� faturamento parcial: Controle e c�digo s�o o mesmo n�mero
      FatNum      := Controle;
      IDDuplicata := Controle;
      // FIM N�o h� faturamento parcial
      Filial      := Geral.IMV(Geral.SoNumeroESinal_TT(DBEdEmpresa.Text));
      Entidade    := DModG.ObtemEntidadeDeFilial(Filial);
      Terceiro    := Geral.IMV(Geral.SoNumeroESinal_TT(DBEdAgente.Text));
      DataAbriu   := FDataAbriu;
      // 2013-06-18
      //DataEncer   := Now();
      DataEncer   := TPDataFat.Date;
      // FIM  2013-06-18
      NumeroNF    := NumNF;
      SerieNF     := SerNF;
      TipoCart    := DmFatura.QrCartEmisTipo.Value;
      //
      DmFatura.EmiteFaturas(FCodigo, FatGru, Entidade, FFatID, FatNum, Terceiro,
        DataAbriu, DataEncer, IDDuplicata, NumeroNF, SerieNF, CartEmis,
        TipoCart, Genero, CondicaoPG, Represen, TipoFatura, FaturaDta, Financeiro,
        ValTotal, 0, 0, 0, 0, 0, 0, True, False);
      //
 //TODO : Ver se um dia vai fazer
(*
      QuestaoTyp := qagOSBgstrl;
      QuestaoCod := Codigo;
      DmModAgenda.AtualizaStatusTodosEventosMesmoTipoCodigo(Estatus,
        QuestaoTyp, QuestaoCod);
*)
      if FQrIts <> nil then
      begin
        UMyMod.AbreQuery(FQrIts, Dmod.MyDB);
        //FQrIts.Locate('Controle', Controle, []);
      end;
      Close;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TFmSrvLHonEmi.BtOKClick(Sender: TObject);
const
  Represen = 0;
var
  DtaFimFat: String;
  Controle, CondicaoPg, CartEmis, Genero, Filial, Entidade, FatNum, Terceiro,
  TipoCart, FatGru: Integer;
  DataAbriu, DataEncer: TDateTime;
  ValTotal: Double;
begin
  Controle       := Geral.IMV(Geral.SoNumero_TT(DBEdControle.Text));
  //Estatus        := CO_BUG_STATUS_0800_FATURADO;
  ValTotal       := EdValor.ValueVariant;
  DtaFimFat      := Geral.FDT(TPDataFat.Date, 1) + ' ' + EdHoraFat.Text;
  CondicaoPg     := EdCondicaoPG.ValueVariant;
  CartEmis       := EdCartEmis.ValueVariant;
  Genero         := EdGenero.ValueVariant;
  //
  // N�o h� faturamento parcial: Controle e c�digo s�o o mesmo n�mero
  FatNum      := Controle;
  // FIM N�o h� faturamento parcial
  Filial      := Geral.IMV(Geral.SoNumeroESinal_TT(DBEdEmpresa.Text));
  Entidade    := DModG.ObtemEntidadeDeFilial(Filial);
  Terceiro    := Geral.IMV(Geral.SoNumeroESinal_TT(DBEdAgente.Text));
  DataAbriu   := FDataAbriu;
  DataEncer   := TPDataFat.Date;
  TipoCart    := QrCartEmisTipo.Value;
  FatGru      := FCodigo;
  //
  if MyObjects.FIC(CondicaoPG = 0, EdCondicaoPG, 'Informe a condi��o de pagamento!') then Exit;
  if MyObjects.FIC(CartEmis = 0, EdCartEmis, 'Informe a carteira!') then Exit;
  if MyObjects.FIC(Genero = 0, EdGenero, 'Informe a conta (do plano de contas)!') then Exit;
  //
  if (FValAFat > 0) and (ValTotal > FValAFat) then
  begin
    if Geral.MB_Pergunta('O valor preenchido � maior que o valor a ser pago!' +
      sLineBreak + 'Deseja continuar?') <> ID_YES then
    begin
      EdValor.SetFocus;
      Exit;
    end;
  end;
  if SrvL_PF.PagamentoHonorarioFuncionario(FatGru, Entidade, FFatID,
    FatNum, Terceiro, DataAbriu, DataEncer, (*IDDuplicata, NumeroNF, SerieNF,*)
    CartEmis, TipoCart, Genero, CondicaoPG, Represen, (*TipoFatura, FaturaDta,
    Financeiro,*) ValTotal, FEncerra, FQrIts, TPDataFat.Date, True)
  then
    Close;
end;

procedure TFmSrvLHonEmi.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLHonEmi.CalculaValores();
//var
  //ValLiqToFat, ValOutToFat, ValDesToFat, ValLiqOkFat: Double;
begin
{
  ValLiqToFat := EdValLiqToFat.ValueVariant;
  ValOutToFat := EdValOutToFat.ValueVariant;
  ValDesToFat := EdValDesToFat.ValueVariant;
  ValLiqOkFat := ValLiqToFat + ValOutToFat - ValDesToFat;
  //
  EdValLiqOkFat.ValueVariant := ValLiqOkFat;
}
end;

procedure TFmSrvLHonEmi.DBEdEmpresaChange(Sender: TObject);
var
  Empresa, Entidade: Integer;
begin
  if DBEDEmpresa.Text <> '' then
  begin
    Empresa  := Geral.IMV(DBEdEmpresa.Text);
    Entidade := DmodG.ObtemEntidadeDeFilial(Empresa);
    DmFatura.ReopenCartEmis(Entidade, True);
  end else
    DmFatura.QrCartEmis.Close;
end;

procedure TFmSrvLHonEmi.EdValConsuChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmSrvLHonEmi.EdValDesToFatChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmSrvLHonEmi.EdValOutToFatChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmSrvLHonEmi.EdValLiqToFatChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmSrvLHonEmi.EdValUsadoChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmSrvLHonEmi.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSrvLHonEmi.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DmFatura.ReopenPediPrzCab();
  DmFatura.ReopenContas(tfinDeb);
  //
  CBCondicaoPG.ListSource := DmFatura.DsPediPrzCab;
  CBCartEmis.ListSource   := DsCartEmis;
  CBGenero.ListSource     := DmFatura.DsContas;
end;

procedure TFmSrvLHonEmi.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLHonEmi.ReopenCartEmis();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartEmis, Dmod.MyDB, [
  'SELECT car.Codigo, car.Nome, ',
  'car.Tipo, car.ForneceI ',
  'FROM carteiras car ',
  'WHERE Codigo<>0 ',
  //'AND Tipo=2 ',
  Geral.ATS_IF(FEmpresa <> 0, ['AND ForneceI=' + Geral.FF0(FEmpresa)]),
  'ORDER BY Nome ',
  '']);
end;

procedure TFmSrvLHonEmi.SpeedButton1Click(Sender: TObject);
begin
  GFat_Jan.InsereEDefinePediPrzCab(EdCondicaoPG.ValueVariant, EdCondicaoPG,
    CBCondicaoPG, DmFatura.QrPediPrzCab);
end;

end.
