object FmSrvLFatCab: TFmSrvLFatCab
  Left = 368
  Top = 194
  Caption = 'LOC-SERVI-009 :: Faturamento de OSs'
  ClientHeight = 631
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 109
    Width = 1008
    Height = 522
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 137
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label3: TLabel
        Left = 504
        Top = 16
        Width = 43
        Height = 13
        Caption = 'Abertura:'
      end
      object Label4: TLabel
        Left = 16
        Top = 56
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label5: TLabel
        Left = 504
        Top = 56
        Width = 69
        Height = 13
        Caption = 'Encerramento:'
      end
      object Label6: TLabel
        Left = 16
        Top = 96
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 640
        Top = 96
        Width = 65
        Height = 13
        Caption = 'Compet'#234'ncia:'
      end
      object Label8: TLabel
        Left = 632
        Top = 16
        Width = 47
        Height = 13
        Caption = 'ValTotCbr'
        FocusControl = DBEdit7
      end
      object Label10: TLabel
        Left = 708
        Top = 16
        Width = 66
        Height = 13
        Caption = 'ValDspToDsc'
        FocusControl = DBEdit8
      end
      object Label15: TLabel
        Left = 784
        Top = 16
        Width = 57
        Height = 13
        Caption = 'ValLiqToFat'
        FocusControl = DBEdit10
      end
      object Label16: TLabel
        Left = 860
        Top = 16
        Width = 58
        Height = 13
        Caption = 'ValLiqOkFat'
        FocusControl = DBEdit11
      end
      object Label21: TLabel
        Left = 632
        Top = 56
        Width = 56
        Height = 13
        Caption = 'EntPagante'
        FocusControl = DBEdit14
      end
      object Label20: TLabel
        Left = 860
        Top = 56
        Width = 64
        Height = 13
        Caption = 'ValDspOkDpl'
        FocusControl = DBEdit13
      end
      object Label19: TLabel
        Left = 784
        Top = 56
        Width = 63
        Height = 13
        Caption = 'ValDspToDpl'
        FocusControl = DBEdit12
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsSrvLFatCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 16
        Top = 112
        Width = 621
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsSrvLFatCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsSrvLFatCab
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 369
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsSrvLFatCab
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsSrvLFatCab
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 72
        Top = 72
        Width = 429
        Height = 21
        DataField = 'NO_CLIENTE'
        DataSource = DsSrvLFatCab
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 504
        Top = 32
        Width = 124
        Height = 21
        DataField = 'DtHrIni'
        DataSource = DsSrvLFatCab
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 504
        Top = 72
        Width = 124
        Height = 21
        DataField = 'DtHrFim_TXT'
        DataSource = DsSrvLFatCab
        TabOrder = 7
      end
      object DBEdit9: TDBEdit
        Left = 640
        Top = 112
        Width = 64
        Height = 21
        DataField = 'AnoMes_TXT'
        DataSource = DsSrvLFatCab
        TabOrder = 8
      end
      object DBEdit7: TDBEdit
        Left = 632
        Top = 32
        Width = 72
        Height = 21
        DataField = 'ValTotCbr'
        DataSource = DsSrvLFatCab
        TabOrder = 9
      end
      object DBEdit8: TDBEdit
        Left = 708
        Top = 32
        Width = 72
        Height = 21
        DataField = 'ValDspToDsc'
        DataSource = DsSrvLFatCab
        TabOrder = 10
      end
      object DBEdit10: TDBEdit
        Left = 784
        Top = 32
        Width = 72
        Height = 21
        DataField = 'ValLiqToFat'
        DataSource = DsSrvLFatCab
        TabOrder = 11
      end
      object DBEdit11: TDBEdit
        Left = 860
        Top = 32
        Width = 72
        Height = 21
        DataField = 'ValLiqOkFat'
        DataSource = DsSrvLFatCab
        TabOrder = 12
      end
      object DBEdit14: TDBEdit
        Left = 632
        Top = 72
        Width = 148
        Height = 21
        DataField = 'EntPagante'
        DataSource = DsSrvLFatCab
        TabOrder = 13
      end
      object DBEdit12: TDBEdit
        Left = 784
        Top = 72
        Width = 72
        Height = 21
        DataField = 'ValDspToDpl'
        DataSource = DsSrvLFatCab
        TabOrder = 14
      end
      object DBEdit13: TDBEdit
        Left = 860
        Top = 72
        Width = 72
        Height = 21
        DataField = 'ValDspOkDpl'
        DataSource = DsSrvLFatCab
        TabOrder = 15
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 458
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 300
        Top = 15
        Width = 706
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 573
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 302
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fatura'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 613
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'Itens de &OSs'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtFat: TBitBtn
          Tag = 414
          Left = 250
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cond. e Pgt.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtFatClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 137
      Width = 1008
      Height = 193
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'OS e Servi'#231'os'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DGDados: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 1000
          Height = 165
          Align = alClient
          DataSource = DsSrvLOSOpeGrn
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'OS'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SrvLCad'
              Title.Caption = 'Servi'#231'o'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SrvLPrmCad'
              Title.Caption = 'Configura'#231#227'o de pr'#234'mios'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Observa'#231#227'o'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FrmFtrCobr'
              Title.Caption = 'Fator de cobran'#231'a'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SIGLA_UMC'
              Title.Caption = 'Unidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValFtrCobr'
              Title.Caption = '$ Unit.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdRlzCobr'
              Title.Caption = 'Qtde cobr.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValTotCobr'
              Title.Caption = '$ Total cobr'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValTotDesp'
              Title.Caption = '$ Despesas'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FrmFtrPaga'
              Title.Caption = 'Fator de pagamento'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SIGLA_UMP'
              Title.Caption = 'Unidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValFtrPaga'
              Title.Caption = '$ Unit.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdRlzPaga'
              Title.Caption = 'Qtde pag.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FrmRatPaga'
              Title.Caption = 'Forma de rateio'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_AgeEquiCab'
              Title.Caption = 'Equipe de agentes'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValRatPaga'
              Title.Caption = '$ Total pag.'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Condi'#231#245'es e pagamentos'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 165
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox15: TGroupBox
            Left = 0
            Top = 0
            Width = 796
            Height = 165
            Align = alLeft
            Caption = ' Condi'#231#245'es de pagamento: '
            TabOrder = 0
            object DBGSrvLFatPrz: TDBGrid
              Left = 2
              Top = 15
              Width = 792
              Height = 148
              Align = alClient
              DataSource = DsSrvLFatPrz
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              PopupMenu = PMFat
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = DBGSrvLFatPrzCellClick
              OnColEnter = DBGSrvLFatPrzColEnter
              OnDrawColumnCell = DBGSrvLFatPrzDrawColumnCell
              OnMouseUp = DBGSrvLFatPrzMouseUp
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Escolhido'
                  Title.Caption = 'E'
                  Width = 17
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_CONDICAO'
                  Title.Caption = 'Descri'#231#227'o da condi'#231#227'o de pagamento'
                  Width = 214
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DescoPer'
                  Title.Caption = '% Desconto'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ORC_COM_DESCO'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Title.Caption = '$ Or'#231'ado'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'INV_COM_DESCO'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 4227327
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Title.Caption = '$ N'#227'o Aprov.'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VAL_COM_DESCO'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Title.Caption = '$ Aprovado'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Parcelas'
                  Title.Caption = 'Parc.'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ORC_PARCELA'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Title.Caption = '$ Parc. Or'#231'.'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'INV_PARCELA'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 4227327
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Title.Caption = '$ Parc. N'#227'o'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VAL_PARCELA'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Title.Caption = '$ Parc. Aprov.'
                  Width = 68
                  Visible = True
                end>
            end
          end
          object GroupBox16: TGroupBox
            Left = 796
            Top = 0
            Width = 204
            Height = 165
            Align = alClient
            Caption = ' Parcelas do faturamento selecionado: '
            TabOrder = 1
            object DBGLctFatRef: TDBGrid
              Left = 2
              Top = 15
              Width = 200
              Height = 148
              Align = alClient
              DataSource = DsLctFatRef
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'PARCELA'
                  Title.Caption = 'Parc.'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Lancto'
                  Width = 82
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Valor'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencto'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 109
    Width = 1008
    Height = 522
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 141
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 96
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label18: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label17: TLabel
        Left = 16
        Top = 56
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label12: TLabel
        Left = 660
        Top = 16
        Width = 43
        Height = 13
        Caption = 'Abertura:'
      end
      object Label13: TLabel
        Left = 828
        Top = 16
        Width = 69
        Height = 13
        Caption = 'Encerramento:'
      end
      object Label11: TLabel
        Left = 924
        Top = 96
        Width = 65
        Height = 13
        Caption = 'Compet'#234'ncia:'
      end
      object Label22: TLabel
        Left = 504
        Top = 56
        Width = 43
        Height = 13
        Caption = 'Pagante:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 112
        Width = 905
        Height = 21
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 525
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCliente: TdmkEditCB
        Left = 16
        Top = 72
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 68
        Top = 72
        Width = 433
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsClientes
        TabOrder = 8
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object TPDtHrIni: TdmkEditDateTimePicker
        Left = 660
        Top = 32
        Width = 112
        Height = 21
        Date = 40724.768997777780000000
        Time = 40724.768997777780000000
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrIni'
        UpdCampo = 'DtHrIni'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrIni: TdmkEdit
        Left = 772
        Top = 32
        Width = 52
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        QryCampo = 'DtHrIni'
        UpdCampo = 'DtHrIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtHrFim: TdmkEditDateTimePicker
        Left = 828
        Top = 32
        Width = 112
        Height = 21
        Date = 40724.768997777780000000
        Time = 40724.768997777780000000
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrFim'
        UpdCampo = 'DtHrFim'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrFim: TdmkEdit
        Left = 940
        Top = 32
        Width = 52
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        QryCampo = 'DtHrFim'
        UpdCampo = 'DtHrFim'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAnoMes: TdmkEdit
        Left = 924
        Top = 112
        Width = 65
        Height = 21
        Alignment = taCenter
        TabOrder = 12
        FormatType = dmktf_AAAAMM
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '08/2015'
        QryCampo = 'AnoMes'
        UpdCampo = 'AnoMes'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 42217d
        ValWarn = False
      end
      object EdEntPagante: TdmkEditCB
        Left = 504
        Top = 72
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EntPagante'
        UpdCampo = 'EntPagante'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEntPagante
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEntPagante: TdmkDBLookupComboBox
        Left = 556
        Top = 72
        Width = 433
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsEntPagante
        TabOrder = 10
        dmkEditCB = EdEntPagante
        QryCampo = 'EntPagante'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 459
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 253
        Height = 32
        Caption = 'Faturamento de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 253
        Height = 32
        Caption = 'Faturamento de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 253
        Height = 32
        Caption = 'Faturamento de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 57
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 23
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrSrvLFatCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrSrvLFatCabBeforeOpen
    AfterOpen = QrSrvLFatCabAfterOpen
    BeforeClose = QrSrvLFatCabBeforeClose
    AfterScroll = QrSrvLFatCabAfterScroll
    SQL.Strings = (
      'SELECT '
      'CONCAT(RIGHT(AnoMes, 2), "/", LEFT(AnoMes, 4)) AnoMes_TXT, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE,'
      'IF(DtHrFim <= "1900-01-01", "",'
      '  DATE_FORMAT(DtHrFim, "%d/%m/%Y %H:%i:%S")) DtHrFim_TXT,'
      'sfc.* '
      'FROM srvlfatcab sfc'
      'LEFT JOIN entidades emp ON emp.Codigo=sfc.Empresa'
      'LEFT JOIN entidades cli ON cli.Codigo=sfc.Cliente')
    Left = 24
    Top = 297
    object QrSrvLFatCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrSrvLFatCabNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrSrvLFatCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLFatCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrSrvLFatCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSrvLFatCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSrvLFatCabAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrSrvLFatCabDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrSrvLFatCabDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrSrvLFatCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLFatCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLFatCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLFatCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLFatCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLFatCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLFatCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLFatCabDtHrFim_TXT: TWideStringField
      FieldName = 'DtHrFim_TXT'
      Size = 19
    end
    object QrSrvLFatCabAnoMes_TXT: TWideStringField
      FieldName = 'AnoMes_TXT'
      Size = 7
    end
    object QrSrvLFatCabValTotCbr: TFloatField
      FieldName = 'ValTotCbr'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLFatCabValDspToDsc: TFloatField
      FieldName = 'ValDspToDsc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLFatCabValLiqToFat: TFloatField
      FieldName = 'ValLiqToFat'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLFatCabValLiqOkFat: TFloatField
      FieldName = 'ValLiqOkFat'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLFatCabValDspToDpl: TFloatField
      FieldName = 'ValDspToDpl'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLFatCabValDspOkDpl: TFloatField
      FieldName = 'ValDspOkDpl'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLFatCabEntPagante: TIntegerField
      FieldName = 'EntPagante'
    end
    object QrSrvLFatCabValOutToFat: TFloatField
      FieldName = 'ValOutToFat'
    end
    object QrSrvLFatCabValDesToFat: TFloatField
      FieldName = 'ValDesToFat'
    end
    object QrSrvLFatCabCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
    end
    object QrSrvLFatCabCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
    object QrSrvLFatCabGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrSrvLFatCabSerNF: TWideStringField
      FieldName = 'SerNF'
      Size = 3
    end
    object QrSrvLFatCabNumNF: TIntegerField
      FieldName = 'NumNF'
    end
    object QrSrvLFatCabDtaFimFat: TDateTimeField
      FieldName = 'DtaFimFat'
    end
    object QrSrvLFatCabNO_ENTPAGANTE: TWideStringField
      FieldName = 'NO_ENTPAGANTE'
      Size = 100
    end
    object QrSrvLFatCabFilial: TIntegerField
      FieldName = 'Filial'
    end
  end
  object DsSrvLFatCab: TDataSource
    DataSet = QrSrvLFatCab
    Left = 24
    Top = 341
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 476
    Top = 536
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      Visible = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 348
    Top = 540
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      'ufs.Nome NOMEUF'
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 452
    Top = 52
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClientesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrClientesCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrClientesNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrClientesCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrClientesIE: TWideStringField
      FieldName = 'IE'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 452
    Top = 96
  end
  object QrSrvLOSOpeGrn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT slc.Nome NO_SrvLCad, prm.Nome NO_SrvLPrmCad,  '
      
        ' ELT(oog.FrmFtrCobr + 1,"Indefinido","Valor total","Valor unit'#225'r' +
        'io") NO_FrmFtrCobr,'
      
        ' ELT(oog.FrmFtrPaga + 1,"Indefinido","Valor total","Valor unit'#225'r' +
        'io") NO_FrmFtrPaga,'
      
        ' ELT(oog.FrmRatPaga + 1,"Indefinido","Valor total","Valor por pe' +
        'ssoa") NO_FrmRatPaga,'
      'umc.Sigla SIGLA_UMC, ump.Sigla SIGLA_UMP, '
      'aec.Nome NO_AgeEquiCab, oog.*  '
      'FROM srvlosopegrn oog '
      'LEFT JOIN srvlcad slc ON slc.Codigo=oog.SrvLCad '
      'LEFT JOIN srvlprmcad prm ON prm.Codigo=oog.SrvLPrmCad '
      'LEFT JOIN ageeqicab aec ON oog.AgeEqiCab=aec.Codigo '
      'LEFT JOIN unidmed umc ON umc.Codigo=oog.UnidMedCobr '
      'LEFT JOIN unidmed ump ON ump.Codigo=oog.UnidMedPaga '
      'WHERE oog.Codigo=2'
      '')
    Left = 116
    Top = 296
    object QrSrvLOSOpeGrnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLOSOpeGrnControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrvLOSOpeGrnSrvLCad: TIntegerField
      FieldName = 'SrvLCad'
    end
    object QrSrvLOSOpeGrnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLOSOpeGrnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLOSOpeGrnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLOSOpeGrnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLOSOpeGrnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLOSOpeGrnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLOSOpeGrnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLOSOpeGrnNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSrvLOSOpeGrnNO_SrvLCad: TWideStringField
      FieldName = 'NO_SrvLCad'
      Size = 60
    end
    object QrSrvLOSOpeGrnSrvLPrmCad: TIntegerField
      FieldName = 'SrvLPrmCad'
    end
    object QrSrvLOSOpeGrnNO_SrvLPrmCad: TWideStringField
      FieldName = 'NO_SrvLPrmCad'
      Size = 60
    end
    object QrSrvLOSOpeGrnNO_AgeEquiCab: TWideStringField
      FieldName = 'NO_AgeEquiCab'
      Size = 255
    end
    object QrSrvLOSOpeGrnAgeEqiCab: TIntegerField
      FieldName = 'AgeEqiCab'
    end
    object QrSrvLOSOpeGrnUnidMedCobr: TIntegerField
      FieldName = 'UnidMedCobr'
    end
    object QrSrvLOSOpeGrnQtdRlzCobr: TFloatField
      FieldName = 'QtdRlzCobr'
    end
    object QrSrvLOSOpeGrnValFtrCobr: TFloatField
      FieldName = 'ValFtrCobr'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrSrvLOSOpeGrnFrmFtrCobr: TSmallintField
      FieldName = 'FrmFtrCobr'
    end
    object QrSrvLOSOpeGrnUnidMedPaga: TIntegerField
      FieldName = 'UnidMedPaga'
    end
    object QrSrvLOSOpeGrnQtdRlzPaga: TFloatField
      FieldName = 'QtdRlzPaga'
    end
    object QrSrvLOSOpeGrnValRatPaga: TFloatField
      FieldName = 'ValRatPaga'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnFrmRatPaga: TSmallintField
      FieldName = 'FrmRatPaga'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnFrmFtrPaga: TSmallintField
      FieldName = 'FrmFtrPaga'
    end
    object QrSrvLOSOpeGrnValTotCobr: TFloatField
      FieldName = 'ValTotCobr'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnNO_FrmRatPaga: TWideStringField
      FieldName = 'NO_FrmRatPaga'
      Size = 60
    end
    object QrSrvLOSOpeGrnSIGLA_UMC: TWideStringField
      FieldName = 'SIGLA_UMC'
      Size = 6
    end
    object QrSrvLOSOpeGrnSIGLA_UMP: TWideStringField
      FieldName = 'SIGLA_UMP'
      Size = 6
    end
    object QrSrvLOSOpeGrnNO_FrmFtrCobr: TWideStringField
      FieldName = 'NO_FrmFtrCobr'
      Size = 60
    end
    object QrSrvLOSOpeGrnNO_FrmFtrPaga: TWideStringField
      FieldName = 'NO_FrmFtrPaga'
      Size = 60
    end
    object QrSrvLOSOpeGrnValFtrPaga: TFloatField
      FieldName = 'ValFtrPaga'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrSrvLOSOpeGrnValTotDesp: TFloatField
      FieldName = 'ValTotDesp'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnAutorizado: TSmallintField
      FieldName = 'Autorizado'
    end
  end
  object DsSrvLOSOpeGrn: TDataSource
    DataSet = QrSrvLOSOpeGrn
    Left = 116
    Top = 340
  end
  object PMFat: TPopupMenu
    OnPopup = PMFatPopup
    Left = 588
    Top = 536
    object Adicionanovacondiodepagamento1: TMenuItem
      Caption = '&Adiciona nova condi'#231#227'o de pagamento'
      OnClick = Adicionanovacondiodepagamento1Click
    end
    object Editaacondiodepagamentoatual1: TMenuItem
      Caption = '&Edita a condi'#231#227'o de pagamento atual'
      OnClick = Editaacondiodepagamentoatual1Click
    end
    object Removecondiaodepagamentoatual1: TMenuItem
      Caption = '&Remove condi'#231'ao de pagamento atual'
      OnClick = Removecondiaodepagamentoatual1Click
    end
    object N15: TMenuItem
      Caption = '-'
    end
    object Fatura1: TMenuItem
      Caption = '&Fatura (financeiro)'
      OnClick = Fatura1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Gerabloqueto1: TMenuItem
      Caption = 'Emite boleto(s)'
      OnClick = Gerabloqueto1Click
    end
    object Visualizarbloquetos1: TMenuItem
      Caption = 'Visualizar bloquetos'
      OnClick = Visualizarbloquetos1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Excluifaturamento1: TMenuItem
      Caption = 'Exclui todo faturamento'
      OnClick = Excluifaturamento1Click
    end
  end
  object QrSrvLFatPrz: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSrvLFatPrzCalcFields
    SQL.Strings = (
      'SELECT opz.Parcelas, '
      'opz.Codigo, opz.Controle, opz.Condicao, '
      'opz.DescoPer, opz.Escolhido, ppc.Nome NO_CONDICAO'
      'FROM osprz opz'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=opz.Condicao'
      'WHERE opz.Codigo=:P0')
    Left = 212
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSrvLFatPrzCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osprz.Codigo'
    end
    object QrSrvLFatPrzControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osprz.Controle'
    end
    object QrSrvLFatPrzCondicao: TIntegerField
      FieldName = 'Condicao'
      Origin = 'osprz.Condicao'
    end
    object QrSrvLFatPrzNO_CONDICAO: TWideStringField
      FieldName = 'NO_CONDICAO'
      Origin = 'pediprzcab.Nome'
      Size = 50
    end
    object QrSrvLFatPrzDescoPer: TFloatField
      FieldName = 'DescoPer'
      Origin = 'osprz.DescoPer'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrSrvLFatPrzEscolhido: TSmallintField
      FieldName = 'Escolhido'
      Origin = 'osprz.Escolhido'
      MaxValue = 1
    end
    object QrSrvLFatPrzParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrSrvLFatPrzVAL_COM_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_COM_DESCO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLFatPrzVAL_PARCELA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_PARCELA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLFatPrzORC_COM_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ORC_COM_DESCO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLFatPrzORC_PARCELA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ORC_PARCELA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLFatPrzINV_COM_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'INV_COM_DESCO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLFatPrzINV_PARCELA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'INV_PARCELA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
  end
  object DsSrvLFatPrz: TDataSource
    DataSet = QrSrvLFatPrz
    Left = 212
    Top = 344
  end
  object QrLctFatRef: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLctFatRefCalcFields
    SQL.Strings = (
      'SELECT lfr.* '
      'FROM lctfatref lfr '
      'WHERE lfr.Controle=0')
    Left = 304
    Top = 296
    object QrLctFatRefPARCELA: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'PARCELA'
      Calculated = True
    end
    object QrLctFatRefCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLctFatRefControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctFatRefConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrLctFatRefLancto: TLargeintField
      FieldName = 'Lancto'
    end
    object QrLctFatRefValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctFatRefVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsLctFatRef: TDataSource
    DataSet = QrLctFatRef
    Left = 304
    Top = 344
  end
  object QrEntPagante: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      'ufs.Nome NOMEUF'
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 536
    Top = 52
    object QrEntPaganteCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntPaganteNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrEntPaganteCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEntPaganteNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrEntPaganteCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrEntPaganteIE: TWideStringField
      FieldName = 'IE'
    end
  end
  object DsEntPagante: TDataSource
    DataSet = QrEntPagante
    Left = 536
    Top = 96
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 860
    Top = 284
  end
end
