object FmSrvLHonAge: TFmSrvLHonAge
  Left = 339
  Top = 185
  Caption = 'LOC-SERVI-013 :: Itens de Honor'#225'rios de OSs'
  ClientHeight = 607
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 468
    Width = 1008
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 57
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    TabOrder = 0
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label5: TLabel
        Left = 8
        Top = 0
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label3: TLabel
        Left = 552
        Top = 0
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label1: TLabel
        Left = 68
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdEmpresa
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TDBEdit
        Left = 552
        Top = 16
        Width = 420
        Height = 21
        TabStop = False
        Color = clWhite
        DataField = 'Nome'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdEmpresa: TdmkDBEdit
        Left = 68
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Empresa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNO_Empresa: TDBEdit
        Left = 128
        Top = 16
        Width = 420
        Height = 21
        TabStop = False
        Color = clWhite
        DataField = 'NO_Empresa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 241
    Width = 1008
    Height = 227
    Align = alClient
    Caption = ' OSs / Servi'#231'os em aberto: '
    TabOrder = 1
    object DBGOSAge: TdmkDBGridZTO
      Left = 2
      Top = 15
      Width = 1004
      Height = 210
      Align = alClient
      DataSource = DsSrvLOSOpeAge
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'OS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtAbertura'
          Title.Caption = 'Abertura da OS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID Servi'#231'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_AGENTE'
          Title.Caption = 'Agente'
          Width = 254
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValPremio'
          Title.Caption = '$ Pr'#234'mio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SRVL'
          Title.Caption = 'Servi'#231'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta'
          Title.Caption = 'ID'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 337
        Height = 32
        Caption = 'Itens de Honor'#225'rios de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 337
        Height = 32
        Caption = 'Itens de Honor'#225'rios de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 337
        Height = 32
        Caption = 'Itens de Honor'#225'rios de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 493
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 537
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtTudo: TBitBtn
        Tag = 127
        Left = 396
        Top = 4
        Width = 100
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTudoClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 498
        Top = 4
        Width = 100
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtNenhumClick
      end
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 105
    Width = 1008
    Height = 136
    Align = alTop
    Caption = ' Filtros: '
    TabOrder = 6
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 119
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label17: TLabel
        Left = 8
        Top = 0
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label4: TLabel
        Left = 8
        Top = 40
        Width = 37
        Height = 13
        Caption = 'Agente:'
      end
      object Label2: TLabel
        Left = 8
        Top = 80
        Width = 39
        Height = 13
        Caption = 'Servi'#231'o:'
      end
      object EdCliente: TdmkEditCB
        Left = 8
        Top = 16
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 60
        Top = 16
        Width = 517
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsClientes
        TabOrder = 1
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdAgente: TdmkEditCB
        Left = 8
        Top = 56
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdAgenteChange
        DBLookupComboBox = CBAgente
        IgnoraDBLookupComboBox = False
      end
      object CBAgente: TdmkDBLookupComboBox
        Left = 76
        Top = 56
        Width = 500
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_AGENTE'
        ListSource = DsAgentes
        TabOrder = 3
        dmkEditCB = EdAgente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdSrvLCad: TdmkEditCB
        Left = 8
        Top = 96
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdSrvLCadChange
        DBLookupComboBox = CBSrvLCad
        IgnoraDBLookupComboBox = False
      end
      object CBSrvLCad: TdmkDBLookupComboBox
        Left = 76
        Top = 96
        Width = 500
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSrvLCad
        TabOrder = 5
        dmkEditCB = EdSrvLCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 750
        Top = 40
        Width = 100
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtReabreClick
      end
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      'ufs.Nome NOMEUF'
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 24
    Top = 296
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClientesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrClientesCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrClientesNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrClientesCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrClientesIE: TWideStringField
      FieldName = 'IE'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 24
    Top = 340
  end
  object QrAgentes: TmySQLQuery
    Database = Dmod.MyDB
    Left = 108
    Top = 298
    object QrAgentesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgentesNO_AGENTE: TWideStringField
      FieldName = 'NO_AGENTE'
      Size = 100
    end
  end
  object DsAgentes: TDataSource
    DataSet = QrAgentes
    Left = 108
    Top = 346
  end
  object QrSrvLOSOpeAge: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSrvLOSOpeAgeAfterOpen
    BeforeClose = QrSrvLOSOpeAgeBeforeClose
    SQL.Strings = (
      'SELECT slc.Nome NO_SRVL,  '
      'ent.Nome NO_AGENTE, age.*  '
      'FROM srvlosopeage age  '
      'LEFT JOIN srvlosopegrn oog on oog.Controle=age.Controle '
      'LEFT JOIN srvloscab ooc on ooc.Codigo=age.Codigo '
      'LEFT JOIN srvlcad slc ON slc.Codigo=ooc.SrvLCad '
      'LEFT JOIN entidades ent on ent.Codigo=age.Agente  '
      'WHERE age.SrvLHonCab=0  ')
    Left = 304
    Top = 300
    object QrSrvLOSOpeAgeNO_SRVL: TWideStringField
      FieldName = 'NO_SRVL'
      Size = 60
    end
    object QrSrvLOSOpeAgeNO_AGENTE: TWideStringField
      FieldName = 'NO_AGENTE'
      Size = 100
    end
    object QrSrvLOSOpeAgeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLOSOpeAgeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrvLOSOpeAgeConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrSrvLOSOpeAgeAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrSrvLOSOpeAgeResponsa: TSmallintField
      FieldName = 'Responsa'
    end
    object QrSrvLOSOpeAgeValPremio: TFloatField
      FieldName = 'ValPremio'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLOSOpeAgeLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLOSOpeAgeDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLOSOpeAgeDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLOSOpeAgeUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLOSOpeAgeUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLOSOpeAgeAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLOSOpeAgeAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLOSOpeAgeSrvLHonCab: TIntegerField
      FieldName = 'SrvLHonCab'
    end
    object QrSrvLOSOpeAgeGruAddSeq: TIntegerField
      FieldName = 'GruAddSeq'
    end
    object QrSrvLOSOpeAgeDtAbertura: TDateTimeField
      FieldName = 'DtAbertura'
    end
  end
  object DsSrvLOSOpeAge: TDataSource
    DataSet = QrSrvLOSOpeAge
    Left = 304
    Top = 344
  end
  object QrSrvLCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM srvlcad'
      'ORDER BY Nome')
    Left = 204
    Top = 300
    object QrSrvLCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsSrvLCad: TDataSource
    DataSet = QrSrvLCad
    Left = 204
    Top = 344
  end
end
