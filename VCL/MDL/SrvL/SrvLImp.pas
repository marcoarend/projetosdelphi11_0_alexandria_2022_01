unit SrvLImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, mySQLDbTables, frxClass, frxDBSet,
  dmkCheckBox;

type
  TFmSrvLImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label7: TLabel;
    Label54: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    TPDataRelativa: TdmkEditDateTimePicker;
    Panel6: TPanel;
    PCRelatorio: TPageControl;
    TabSheet1: TTabSheet;
    Panel7: TPanel;
    GB00Periodo: TGroupBox;
    TP00DataIni: TdmkEditDateTimePicker;
    Ck00DataIni: TCheckBox;
    Ck00DataFim: TCheckBox;
    TP00DataFim: TdmkEditDateTimePicker;
    Qr00Abertos: TmySQLQuery;
    frxLOC_SERVI_000_OPN: TfrxReport;
    frxDs00Abertos: TfrxDBDataset;
    Qr00AbertosCodigo: TIntegerField;
    Qr00AbertosControle: TIntegerField;
    Qr00AbertosSrvLCad: TIntegerField;
    Qr00AbertosDtaExeIni: TDateTimeField;
    Qr00AbertosNome: TWideStringField;
    Qr00Margem: TmySQLQuery;
    frxDs00Margem: TfrxDBDataset;
    Qr00MargemNO_SrvLCad: TWideStringField;
    Qr00MargemNO_ESTATUS: TWideStringField;
    Qr00MargemNO_CLIENTE: TWideStringField;
    Qr00MargemDtaExeFim_TXT: TWideStringField;
    Qr00MargemValTotLiqu: TFloatField;
    Qr00MargemPerTotLiqu: TFloatField;
    Ck00Abertos: TdmkCheckBox;
    frxLOC_SERVI_000_01: TfrxReport;
    Qr00MargemValTotCobr: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxLOC_SERVI_000_OPNGetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    procedure ImprimeMargem();
  public
    { Public declarations }
  end;

  var
  FmSrvLImp: TFmSrvLImp;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, UnDmkProcFunc;

{$R *.DFM}

procedure TFmSrvLImp.BtOKClick(Sender: TObject);
begin
  case PCRelatorio.ActivePageIndex of
    0: ImprimeMargem();
    //
    else Geral.MB_Erro('Relat�rio n�o implementado!');
  end;
end;

procedure TFmSrvLImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSrvLImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  TPDataRelativa.Date := Date;
  //
  //
  TP00DataIni.Date := Date - 30;
  TP00DataFim.Date := Date;
  //
end;

procedure TFmSrvLImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLImp.frxLOC_SERVI_000_OPNGetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_MEU' then
    Value := TPDataRelativa.Date
  else
  if VarName ='VARF_DATA_IMP' then
    Value := Now()
  else
  if VarName = 'VARF_PERIODO_0' then
    Value := dmkPF.PeriodoImp2(TP00DataIni.Date, TP00DataFim.Date,
    Ck00DataIni.Checked, Ck00DataFim.Checked, '', 'at�', '')
(*
  else
  if VarName = 'VARF_AGRUP_00_1' then
    Value := RG00Agrupa.ItemIndex >= 1
  else
  if VarName = 'VARF_AGRUP_00_2' then
    Value := RG00Agrupa.ItemIndex >= 2
*)
  else
end;

procedure TFmSrvLImp.ImprimeMargem();
var
  DtaLstCPA, SQL_Abertos, SQL_Periodo: String;
begin
  if Ck00DataFim.Checked then
    DtaLstCPA := Geral.FDT(TP00DataFim.Date, 1)
  else
    DtaLstCPA := Geral.FDT(DModG.ObtemAgora(), 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr00Abertos, Dmod.MyDB, [
  'SELECT Codigo, Controle, SrvLCad, DtaExeIni, Nome ',
  'FROM srvlosopegrn ',
  'WHERE DtaExeFim < "1900-01-01" ',
  'AND DtaLstCPA < "' + DtaLstCPA + '" ',
  '']);
  //
  if Qr00Abertos.RecordCount > 0 then
  begin
    MyObjects.frxDefineDataSets(frxLOC_SERVI_000_OPN, [
    DModG.frxDsDono,
    frxDs00Abertos
    ]);
    MyObjects.frxMostra(frxLOC_SERVI_000_OPN,
      'Servi�os Abertos que Impedem Relat�rio');
    //
    Exit;
  end;
  if Ck00Abertos.Checked then
    SQL_Abertos := ' OR oog.DtaExeFim < "1900-01-01"'
  else
    SQL_Abertos := '';
  //
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE (oog.DtaExeFim' ,
  TP00DataIni.Date, TP00DataFim.Date, Ck00DataIni.Checked, Ck00DataFim.Checked)
  + SQL_Abertos + ') ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr00Margem, Dmod.MyDB, [
  'SELECT cad.Nome NO_SrvLCad, sta.Nome NO_ESTATUS,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE,',
  'IF(DtaExeFim <= "1900-01-01", "", DATE_FORMAT(',
  '  DtaExeFim, "%d/%m/%Y %H:%i")) DtaExeFim_TXT,',
  'oog.ValTotCobr-(oog.ValTotPaga+oog.ValTotDesp) ValTotLiqu,',
  '(oog.ValTotCobr-(oog.ValTotPaga+oog.ValTotDesp)) /',
  '  oog.ValTotCobr * 100 PerTotLiqu, oog.ValTotCobr',
  'FROM srvlosopegrn oog',
  'LEFT JOIN srvloscab osc ON osc.Codigo=oog.Codigo',
  'LEFT JOIN srvlcad cad ON cad.Codigo=osc.SrvLCad',
  'LEFT JOIN entidades ent ON ent.Codigo=osc.Cliente',
  'LEFT JOIN entidades emp ON emp.Codigo=osc.Empresa',
  'LEFT JOIN estatusoss sta ON sta.Codigo=osc.Estatus',
  SQL_Periodo,
  '']);
  //
  MyObjects.frxDefineDataSets(frxLOC_SERVI_000_01, [
  DModG.frxDsDono,
  frxDs00Margem
  ]);
  MyObjects.frxMostra(frxLOC_SERVI_000_01, 'Margem');
  //
end;

end.
