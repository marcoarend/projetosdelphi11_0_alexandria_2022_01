unit SrvLOSOpeGrn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.ComCtrls,
  dmkEditDateTimePicker, dmkCheckBox;

type
  TFmSrvLOSOpeGrn = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    DsSrvLCad: TDataSource;
    QrSrvLCad: TmySQLQuery;
    QrSrvLCadCodigo: TIntegerField;
    QrSrvLCadNome: TWideStringField;
    QrSrvLPrmCad: TmySQLQuery;
    QrSrvLPrmCadCodigo: TIntegerField;
    QrSrvLPrmCadNome: TWideStringField;
    DsSrvLPrmCad: TDataSource;
    QrAgeEqiCab: TmySQLQuery;
    IntegerField2: TIntegerField;
    QrAgeEqiCabNome: TWideStringField;
    DsAgeEqiCab: TDataSource;
    GroupBox4: TGroupBox;
    Panel5: TPanel;
    Label1: TLabel;
    EdSrvLCad: TdmkEditCB;
    CBSrvLCad: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdSrvLPrmCad: TdmkEditCB;
    CBSrvLPrmCad: TdmkDBLookupComboBox;
    Label55: TLabel;
    EdAgeEqiCab: TdmkEditCB;
    CBAgeEqiCab: TdmkDBLookupComboBox;
    Label7: TLabel;
    EdNome: TdmkEdit;
    SBSrvLCad: TSpeedButton;
    SBSrvLPrmCad: TSpeedButton;
    GroupBox3: TGroupBox;
    PnExecucao: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    CkExecucao: TCheckBox;
    QrUnidMedCobr: TmySQLQuery;
    QrUnidMedCobrCodigo: TIntegerField;
    QrUnidMedCobrCodUsu: TIntegerField;
    QrUnidMedCobrSigla: TWideStringField;
    QrUnidMedCobrNome: TWideStringField;
    DsUnidMedCobr: TDataSource;
    QrUnidMedPaga: TmySQLQuery;
    QrUnidMedPagaCodigo: TIntegerField;
    QrUnidMedPagaCodUsu: TIntegerField;
    QrUnidMedPagaSigla: TWideStringField;
    QrUnidMedPagaNome: TWideStringField;
    DsUnidMedPaga: TDataSource;
    VUUnidMedCobr: TdmkValUsu;
    VUUnidMedPaga: TdmkValUsu;
    GroupBox5: TGroupBox;
    Panel3: TPanel;
    Label4: TLabel;
    SBUnidMed: TSpeedButton;
    LaValFtrCobr: TLabel;
    EdUnidMedCobr: TdmkEditCB;
    EdSiglaCobr: TdmkEdit;
    CBUnidMedCobr: TdmkDBLookupComboBox;
    EdValFtrCobr: TdmkEdit;
    QrSrvLPrmCadUnidMedCobr: TIntegerField;
    QrSrvLPrmCadFrmFtrCobr: TSmallintField;
    QrSrvLPrmCadUnidMedPaga: TIntegerField;
    QrSrvLPrmCadFrmRatPaga: TSmallintField;
    QrSrvLPrmNvs: TmySQLQuery;
    LaQtdRlzCobr: TLabel;
    EdQtdRlzCobr: TdmkEdit;
    RGFrmFtrCobr: TdmkRadioGroup;
    QrSrvLPrmCadFrmFtrPaga: TIntegerField;
    QrSrvLPrmNvsCodigo: TIntegerField;
    QrSrvLPrmNvsControle: TIntegerField;
    QrSrvLPrmNvsUnidMedCobr: TIntegerField;
    QrSrvLPrmNvsFrmFtrCobr: TSmallintField;
    QrSrvLPrmNvsQtdMaxCobr: TFloatField;
    QrSrvLPrmNvsValFtrCobr: TFloatField;
    QrSrvLPrmNvsUnidMedPaga: TIntegerField;
    QrSrvLPrmNvsFrmRatPaga: TSmallintField;
    QrSrvLPrmNvsQtdMaxPaga: TFloatField;
    QrSrvLPrmNvsValRatPaga: TFloatField;
    QrSrvLPrmNvsFrmFtrPaga: TSmallintField;
    EdValTotCobr: TdmkEdit;
    LaValTotCobr: TLabel;
    CkWrnLctDay: TdmkCheckBox;
    QrSrvLCadWrnLctDay: TIntegerField;
    CBAgeEqiCfg: TdmkDBLookupComboBox;
    EdAgeEqiCfg: TdmkEditCB;
    Label9: TLabel;
    SbAgeEqiCfg: TSpeedButton;
    QrAgeEqiCfg: TmySQLQuery;
    QrAgeEqiCfgCodigo: TIntegerField;
    QrAgeEqiCfgNome: TWideStringField;
    DsAgeEqiCfg: TDataSource;
    RGAutorizado: TdmkRadioGroup;
    Panel6: TPanel;
    GroupBox6: TGroupBox;
    Panel7: TPanel;
    Label8: TLabel;
    SpeedButton1: TSpeedButton;
    LaValFtrPaga: TLabel;
    LaQtdRlzPaga: TLabel;
    LaValRatPaga: TLabel;
    EdUnidMedPaga: TdmkEditCB;
    EdSiglaPaga: TdmkEdit;
    CBUnidMedPaga: TdmkDBLookupComboBox;
    EdValFtrPaga: TdmkEdit;
    EdQtdRlzPaga: TdmkEdit;
    RGFrmRatPaga: TdmkRadioGroup;
    RGFrmFtrPaga: TdmkRadioGroup;
    EdValRatPaga: TdmkEdit;
    GroupBox7: TGroupBox;
    Panel8: TPanel;
    TPDtaExeIni: TdmkEditDateTimePicker;
    EdDtaExeFim: TdmkEdit;
    TPDtaExeFim: TdmkEditDateTimePicker;
    Label13: TLabel;
    EdDtaExeIni: TdmkEdit;
    Label12: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBSrvLCadClick(Sender: TObject);
    procedure SBSrvLPrmCadClick(Sender: TObject);
    procedure EdSrvLCadRedefinido(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
    procedure CkExecucaoClick(Sender: TObject);
    procedure RGFrmFtrCobrClick(Sender: TObject);
    procedure EdUnidMedCobrChange(Sender: TObject);
    procedure EdUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaCobrChange(Sender: TObject);
    procedure EdSiglaCobrExit(Sender: TObject);
    procedure EdSiglaCobrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBUnidMedClick(Sender: TObject);
    procedure EdUnidMedPagaChange(Sender: TObject);
    procedure EdUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaPagaChange(Sender: TObject);
    procedure EdSiglaPagaExit(Sender: TObject);
    procedure EdSiglaPagaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdQtdRlzCobrChange(Sender: TObject);
    procedure EdQtdRlzPagaChange(Sender: TObject);
    procedure RGFrmFtrPagaClick(Sender: TObject);
    procedure RGFrmRatPagaClick(Sender: TObject);
    procedure EdValFtrPagaChange(Sender: TObject);
    procedure EdValFtrCobrChange(Sender: TObject);
    procedure EdValRatPagaChange(Sender: TObject);
    procedure EdValTotCobrChange(Sender: TObject);
    procedure EdAgeEqiCfgRedefinido(Sender: TObject);
    procedure SbAgeEqiCfgClick(Sender: TObject);
  private
    { Private declarations }
    FCarregando: Boolean;
    //
    procedure DefineValor(Quem: TPrmRefQuem);
    procedure ReopenSrvLOSOpeGrn(Controle: Integer);
    procedure ReopenSrvLPrmCad();
    procedure ReopenAgeEqiCab();

  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmSrvLOSOpeGrn: TFmSrvLOSOpeGrn;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnSrvL_PF, ModProd, UnMySQLCuringa, UnGrade_PF, AppListas, UnEmpg_PF,
  ModuleGeral;

{$R *.DFM}

procedure TFmSrvLOSOpeGrn.BtOKClick(Sender: TObject);
var
  UnidMedCobr, FrmFtrCobr, UnidMedPaga, AgeEqiCab, FrmRatPaga, FrmFtrPaga,
  Codigo, Controle, SrvLCad, SrvLPrmCad, WrnLctDay, AgeEqiCfg, GruAddSeq,
  Autorizado: Integer;
  QtdRlzCobr, ValFtrCobr, QtdRlzPaga, ValTotCobr, ValFtrPaga, ValRatPaga: Double;
  Nome, DtaExeIni, DtaExeFim: String;
  ExecSQL: Boolean;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  SrvLCad        := EdSrvlCad.ValueVariant;
  SrvLPrmCad     := EdSrvLPrmCad.ValueVariant;
  Nome           := EdNome.Text;
  AgeEqiCab      := EdAgeEqiCab.ValueVariant;
  DtaExeIni      := Geral.FDT_TP_Ed(TPDtaExeIni.Date, EdDtaExeIni.Text);
  DtaExeFim      := Geral.FDT_TP_Ed(TPDtaExeFim.Date, EdDtaExeFim.Text);
  Autorizado     := RGAutorizado.ItemIndex;
  //
  //  Execu��o
  UnidMedCobr    := EdUnidMedCobr.ValueVariant;
  QtdRlzCobr     := EdQtdRlzCobr.ValueVariant;
  ValFtrCobr     := EdValFtrCobr.ValueVariant;
  ValTotCobr     := EdValTotCobr.ValueVariant;
  FrmFtrCobr     := RGFrmFtrCobr.ItemIndex;
  UnidMedPaga    := EdUnidMedPaga.ValueVariant;
  QtdRlzPaga     := EdQtdRlzPaga.ValueVariant;
  ValRatPaga     := EdValRatPaga.ValueVariant;
  ValFtrPaga     := EdValFtrPaga.ValueVariant;
  FrmRatPaga     := RGFrmRatPaga.ItemIndex;
  FrmFtrPaga     := RGFrmFtrPaga.ItemIndex;
  //
  WrnLctDay      := Geral.BoolToInt(CkWrnLctDay.Checked);
  AgeEqiCfg      := EdAgeEqiCfg.ValueVariant;
  //
  if MyObjects.FIC(AgeEqiCab = 0,
  EdAgeEqiCab, 'Informe a equipe de agentes!') then
    Exit;
  if MyObjects.FIC(SrvLCad = 0, EdSrvLCad, 'Servi�o') then
    Exit;
  Controle := UMyMod.BPGS1I32('srvlosopegrn', 'Controle', '', '', tsPos,
    ImgTipo.SQLType, Controle);
  if CkExecucao.Checked then
    ExecSQL :=
    UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'srvlosopegrn', False, [
    'Codigo', 'SrvLCad', 'Nome',
    'SrvLPrmCad', 'AgeEqiCab',
    //
    'DtaExeIni', 'DtaExeFim', 'UnidMedCobr',
    'QtdRlzCobr', 'ValFtrCobr', 'FrmFtrCobr',
    'UnidMedPaga', 'QtdRlzPaga', 'ValRatPaga',
    'FrmRatPaga', 'FrmFtrPaga', 'ValTotCobr',
    'ValFtrPaga',
    //
    'WrnLctDay', 'AgeEqiCfg', 'Autorizado'], [
    'Controle'], [
    Codigo, SrvLCad, Nome,
    SrvLPrmCad, AgeEqiCab,
    //
    DtaExeIni, DtaExeFim, UnidMedCobr,
    QtdRlzCobr, ValFtrCobr, FrmFtrCobr,
    UnidMedPaga, QtdRlzPaga, ValRatPaga,
    FrmRatPaga, FrmFtrPaga, ValTotCobr,
    ValFtrPaga,
    //
    WrnLctDay, AgeEqiCfg, Autorizado], [
    Controle], True)
  else
    ExecSQL :=
    UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'srvlosopegrn', False, [
    'Codigo', 'SrvLCad', 'Nome',
    'SrvLPrmCad', 'AgeEqiCab',
    'WrnLctDay', 'AgeEqiCfg', 'Autorizado'], [
    'Controle'], [
    Codigo, SrvLCad, Nome,
    SrvLPrmCad, AgeEqiCab,
    WrnLctDay, AgeEqiCfg, Autorizado], [
    Controle], True);
  //
  if ExecSQL then
  begin
    if ImgTipo.SQLType = stIns then
    begin
      GruAddSeq := SrvL_PF.CriaItensDeAgentes_Ger(Codigo, Controle, AgeEqiCab,
      AgeEqiCfg, TPDtaExeIni.Date);
    end else
      GruAddSeq := 0;
 { TODO : Falta mudar!! }
    // &^%$# Parei Aqui
    SrvL_PF.DistribuiValoresAgentesSrvLOSOpeGrn(Controle, GruAddSeq);
    SrvL_PF.AtualizaTotaisSrvLOSCab(Codigo);
    ReopenSrvLOSOpeGrn(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdControle.ValueVariant   := 0;
      EdSrvLCad.ValueVariant    := 0;
      CBSrvLCad.KeyValue        := Null;
      EdNome.ValueVariant       := '';
      EdSrvLPrmCad.ValueVariant := 0;
      CBSrvLPrmCad.KeyValue     := 0;
      //
      CkExecucao.Checked        := False;
      //
      EdSrvLCad.SetFocus;
    end else Close;
  end;
end;

procedure TFmSrvLOSOpeGrn.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLOSOpeGrn.CBUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCobr, CBUnidMedCobr, dmktfInteger);
end;

procedure TFmSrvLOSOpeGrn.CBUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedPaga, CBUnidMedPaga, dmktfInteger);
end;

procedure TFmSrvLOSOpeGrn.CkExecucaoClick(Sender: TObject);
var
  Qry: TmySQLQuery;
  SrvLPrmCad: Integer;
begin
  PnExecucao.Visible := CkExecucao.Checked;
  //
  if CkExecucao.Checked then
  begin
    if RGFrmFtrCobr.ItemIndex = 0 then
      RGFrmFtrCobr.ItemIndex := QrSrvLPrmCadFrmFtrCobr.Value;
    if EdUnidMedCobr.ValueVariant = 0 then
    begin
      EdUnidMedCobr.ValueVariant := QrSrvLPrmCadUnidMedCobr.Value;
      CBUnidMedCobr.KeyValue     := QrSrvLPrmCadUnidMedCobr.Value;
    end;
    //
    if RGFrmFtrPaga.ItemIndex = 0 then
      RGFrmFtrPaga.ItemIndex := QrSrvLPrmCadFrmFtrPaga.Value;
    if EdUnidMedPaga.ValueVariant = 0 then
    begin
      EdUnidMedPaga.ValueVariant := QrSrvLPrmCadUnidMedPaga.Value;
      CBUnidMedPaga.KeyValue     := QrSrvLPrmCadUnidMedPaga.Value;
    end;
    if RGFrmRatPaga.ItemIndex = 0 then
      RGFrmRatPaga.ItemIndex := QrSrvLPrmCadFrmRatPaga.Value;
    //
    if EdQtdRlzCobr.CanFocus and (FCarregando = False) then
      EdQtdRlzCobr.SetFocus;
  end else
  begin
    EdQtdRlzCobr.ValueVariant  := 0;
    EdUnidMedCobr.ValueVariant := 0;
    EdSiglaCobr.ValueVariant   := '';
    CBUnidMedCobr.KeyValue     := 0;
    EdValFtrCobr.ValueVariant  := 0;
    RGFrmFtrCobr.ItemIndex     := 0;
    //
    EdQtdRlzPaga.ValueVariant  := 0;
    EdUnidMedPaga.ValueVariant := 0;
    EdSiglaPaga.ValueVariant   := '';
    CBUnidMedPaga.KeyValue     := 0;
    EdValRatPaga.ValueVariant  := 0;
    RGFrmFtrPaga.ItemIndex     := 0;
    RGFrmRatPaga.ItemIndex     := 0;
  end;
end;

procedure TFmSrvLOSOpeGrn.DefineValor(Quem: TPrmRefQuem);
var
  SrvLPrmCad, FrmFtr: Integer;
  ValBas, QtdTot, QtdRlz, QtdMul: Double;
begin
  SrvLPrmCad := EdSrvLPrmCad.ValueVariant;
  case Quem of
    TPrmRefQuem.prqCliente: QtdRlz := EdQtdRlzCobr.ValueVariant;
    TPrmRefQuem.prqAgente:  QtdRlz := EdQtdRlzPaga.ValueVariant;
    else QtdRlz := 0;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLPrmNvs, Dmod.MyDB, [
  'SELECT prm.* ',
  'FROM srvlprmnvs prm  ',
  'WHERE prm.Codigo=' + Geral.FF0(SrvLPrmCad),
  'AND QtdMaxCobr>' + Geral.FFT_Dot(QtdRlz, 3, siNegativo),
  'ORDER BY QtdMaxCobr ',
  ' ']);
  //
  case Quem of
    TPrmRefQuem.prqCliente:
    begin
      ValBas := QrSrvLPrmNvsValFtrCobr.Value;
      FrmFtr := QrSrvLPrmNvsFrmFtrCobr.Value;
      EdUnidMedCobr.ValueVariant := QrSrvLPrmNvsUnidMedCobr.Value;
      CBUnidMedCobr.KeyValue     := QrSrvLPrmNvsUnidMedCobr.Value;
      RGFrmFtrCobr.ItemIndex     := QrSrvLPrmNvsFrmFtrCobr.Value;
      EdValFtrCobr.ValueVariant  := QrSrvLPrmNvsValFtrCobr.Value;
    end;
    TPrmRefQuem.prqAgente:
    begin
      ValBas := QrSrvLPrmNvsValRatPaga.Value;
      FrmFtr := QrSrvLPrmNvsFrmFtrPaga.Value;
      EdUnidMedPaga.ValueVariant := QrSrvLPrmNvsUnidMedPaga.Value;
      CBUnidMedPaga.KeyValue     := QrSrvLPrmNvsUnidMedPaga.Value;
      RGFrmFtrPaga.ItemIndex     := QrSrvLPrmNvsFrmFtrPaga.Value;
      RGFrmRatPaga.ItemIndex     := QrSrvLPrmNvsFrmRatPaga.Value;
      EdValFtrPaga.ValueVariant  := QrSrvLPrmNvsValRatPaga.Value;
    end;
    else
    begin
      ValBas := 0;
      FrmFtr := 0;
    end;
  end;
  case TPrmFatorValor(FrmFtr) of
    pfvValorTotal    : QtdMul := 1;
    pfvValorUnitario : QtdMul := QtdRlz;
    else QtdMul := 0;
  end;
  QtdTot := QtdMul * ValBas;
  case Quem of
    TPrmRefQuem.prqCliente: EdValTotCobr.ValueVariant := QtdTot;
    TPrmRefQuem.prqAgente:  EdValRatPaga.ValueVariant := QtdTot;
    else ; // ???
  end;
end;

procedure TFmSrvLOSOpeGrn.EdAgeEqiCfgRedefinido(Sender: TObject);
begin
  ReopenAgeEqiCab();
end;

procedure TFmSrvLOSOpeGrn.EdQtdRlzCobrChange(Sender: TObject);
begin
  if EdQtdRlzCobr.Focused then
  begin
    DefineValor(TPrmRefQuem.prqCliente);
    EdQtdRlzPaga.ValueVariant := EdQtdRlzCobr.ValueVariant;
  end;
end;

procedure TFmSrvLOSOpeGrn.EdQtdRlzPagaChange(Sender: TObject);
begin
  if EdQtdRlzCobr.Focused or EdQtdRlzPaga.Focused then
  begin
    DefineValor(TPrmRefQuem.prqAgente);
  end;
end;

procedure TFmSrvLOSOpeGrn.EdSiglaCobrChange(Sender: TObject);
begin
  if EdSiglaCobr.Focused then
    DmProd.PesquisaPorSigla(False, EdSiglaCobr, EdUnidMedCobr, CBUnidMedCobr);
  //
  //LaQtdMaxCobr.Caption := 'M�ximo de ' + EdSiglaCobr.Text + ':';
end;

procedure TFmSrvLOSOpeGrn.EdSiglaCobrExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSiglaCobr, EdUnidMedCobr, CBUnidMedCobr);
end;

procedure TFmSrvLOSOpeGrn.EdSiglaCobrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCobr, CBUnidMedCobr, dmktfInteger)
end;

procedure TFmSrvLOSOpeGrn.EdSiglaPagaChange(Sender: TObject);
begin
  if EdSiglaPaga.Focused then
    DmProd.PesquisaPorSigla(False, EdSiglaPaga, EdUnidMedPaga, CBUnidMedPaga);
  //
  //LaQtdMaxPaga.Caption := 'M�ximo de ' + EdSiglaPaga.Text + ':';
end;

procedure TFmSrvLOSOpeGrn.EdSiglaPagaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSiglaPaga, EdUnidMedPaga, CBUnidMedPaga);
end;

procedure TFmSrvLOSOpeGrn.EdSiglaPagaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedPaga, CBUnidMedPaga, dmktfInteger)
end;

procedure TFmSrvLOSOpeGrn.EdSrvLCadRedefinido(Sender: TObject);
begin
  ReopenSrvLPrmCad();
  CkWrnLctDay.Checked := QrSrvLCadWrnLctDay.Value = 1;
end;

procedure TFmSrvLOSOpeGrn.EdUnidMedCobrChange(Sender: TObject);
begin
  if not EdSiglaCobr.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMedCobr.ValueVariant, EdSiglaCobr);
end;

procedure TFmSrvLOSOpeGrn.EdUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCobr, CBUnidMedCobr, dmktfInteger);
end;

procedure TFmSrvLOSOpeGrn.EdUnidMedPagaChange(Sender: TObject);
begin
  if not EdSiglaPaga.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMedPaga.ValueVariant, EdSiglaPaga);
end;

procedure TFmSrvLOSOpeGrn.EdUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedPaga, CBUnidMedPaga, dmktfInteger);
end;

procedure TFmSrvLOSOpeGrn.EdValFtrCobrChange(Sender: TObject);
begin
  if EdValFtrCobr.Focused then
    EdValTotCobr.ValueVariant := EdValFtrCobr.ValueVariant * EdQtdRlzCobr.ValueVariant
end;

procedure TFmSrvLOSOpeGrn.EdValFtrPagaChange(Sender: TObject);
begin
  if EdValFtrPaga.Focused then
    EdValRatPaga.ValueVariant := EdValFtrPaga.ValueVariant * EdQtdRlzPaga.ValueVariant
end;

procedure TFmSrvLOSOpeGrn.EdValRatPagaChange(Sender: TObject);
begin
  if EdValRatPaga.Focused then
    if EdQtdRlzPaga.ValueVariant > 0 then
      EdValFtrPaga.ValueVariant := EdValRatPaga.ValueVariant / EdQtdRlzPaga.ValueVariant
end;

procedure TFmSrvLOSOpeGrn.EdValTotCobrChange(Sender: TObject);
begin
  if EdValTotCobr.Focused then
    if EdQtdRlzCobr.ValueVariant > 0 then
      EdValFtrCobr.ValueVariant := EdValTotCobr.ValueVariant / EdQtdRlzCobr.ValueVariant
end;

procedure TFmSrvLOSOpeGrn.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
  FCarregando := False;
end;

procedure TFmSrvLOSOpeGrn.FormCreate(Sender: TObject);
begin
  FCarregando := True;
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrSrvLCad, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrSrvLPrmCad N�o!!! Fazer OnChange do EdSrvLCad
  ReopenAgeEqiCab();
  //
  //
  MyObjects.ConfiguraRadioGroup(RGFrmFtrCobr, sPrmFatorValor, 3, 0);
  MyObjects.ConfiguraRadioGroup(RGFrmFtrPaga, sPrmFatorValor, 3, 0);
  MyObjects.ConfiguraRadioGroup(RGFrmRatPaga, sPrmFormaRateio, 3, 0);
  //
  UnDmkDAC_PF.AbreQuery(QrUnidMedCobr, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMedPaga, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrAgeEqiCfg, Dmod.MyDB);
  //
  TPDtaExeIni.Date := DmodG.ObtemAgora();
end;

procedure TFmSrvLOSOpeGrn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLOSOpeGrn.ImgTipoChange(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := ImgTipo.SQLType = stIns;
  //
  ReopenAgeEqiCab();
  EdAgeEqiCab.Enabled := Habilita;
  CBAgeEqiCab.Enabled := Habilita;
end;

procedure TFmSrvLOSOpeGrn.ReopenAgeEqiCab();
var
  SQL_AgeEqiCfg: String;
begin
  if ImgTipo.SQLType = stIns then
    SQL_AgeEqiCfg := 'AND SQL_AgeEqiCfg=' + Geral.FF0(EdAgeEqiCfg.ValueVariant)
  else
    SQL_AgeEqiCfg := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgeEqiCab, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM ageeqicab  ',
  'ORDER BY Nome ',
  '  ']);
end;

procedure TFmSrvLOSOpeGrn.ReopenSrvLOSOpeGrn(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmSrvLOSOpeGrn.ReopenSrvLPrmCad();
begin
  EdSrvLPrmCad.ValueVariant := 0;
  CBSrvLPrmCad.KeyValue     := 0;
  if (EdSrvLCad.ValueVariant = 0) or (EdSrvLCad.ValueVariant = Null) then
    QrSrvLPrmCad.Close
  else
    UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLPrmCad, Dmod.MyDB, [
    'SELECT * ',
    'FROM srvlprmcad ',
    'WHERE SrvLCad=' + Geral.FF0(EdSrvLCad.ValueVariant),
    'AND SYSDATE() BETWEEN DtValIni AND DtValFim',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmSrvLOSOpeGrn.RGFrmFtrCobrClick(Sender: TObject);
begin
  LaValFtrCobr.Caption := RGFrmFtrCobr.Items[RGFrmFtrCobr.ItemIndex] + ':';
end;

procedure TFmSrvLOSOpeGrn.RGFrmFtrPagaClick(Sender: TObject);
begin
  LaValFtrPaga.Caption := RGFrmFtrCobr.Items[RGFrmFtrPaga.ItemIndex] + ':';
end;

procedure TFmSrvLOSOpeGrn.RGFrmRatPagaClick(Sender: TObject);
begin
  LaValRatPaga.Caption := RGFrmRatPaga.Items[RGFrmRatPaga.ItemIndex] + ':';
end;

procedure TFmSrvLOSOpeGrn.SbAgeEqiCfgClick(Sender: TObject);
var
  AgeEqiCfg: Integer;
begin
  VAR_CADASTRO := 0;
  AgeEqiCfg    := EdAgeEqiCfg.ValueVariant;

  Empg_PF.MostraFormAgeEqiCfg(AgeEqiCfg);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdAgeEqiCfg, CBAgeEqiCfg, QrAgeEqiCfg, VAR_CADASTRO);
    EdAgeEqiCfg.SetFocus;
  end;
end;

procedure TFmSrvLOSOpeGrn.SBSrvLCadClick(Sender: TObject);
var
  SrvLCad: Integer;
begin
  VAR_CADASTRO := 0;
  SrvLCad      := EdSrvLCad.ValueVariant;

  SrvL_PF.MostraFormSrvLCad(SrvLCad);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdSrvLCad, CBSrvLCad, QrSrvLCad, VAR_CADASTRO);
    EdSrvLCad.SetFocus;
  end;
end;

procedure TFmSrvLOSOpeGrn.SBSrvLPrmCadClick(Sender: TObject);
var
  SrvLPrmCad: Integer;
begin
  VAR_CADASTRO := 0;
  SrvLPrmCad   := EdSrvLPrmCad.ValueVariant;

  SrvL_PF.MostraFormSrvLPrmCad(SrvLPrmCad);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdSrvLPrmCad, CBSrvLPrmCad, QrSrvLPrmCad, VAR_CADASTRO);
    EdSrvLPrmCad.SetFocus;
  end;
end;

procedure TFmSrvLOSOpeGrn.SBUnidMedClick(Sender: TObject);
begin
  Grade_PF.CadastroEDefinicaoDeUnidMed(VUUnidMedCobr, EdUnidMedCobr,
    CBUnidMedCobr, QrUnidMedCobr);
end;

procedure TFmSrvLOSOpeGrn.SpeedButton1Click(Sender: TObject);
begin
  Grade_PF.CadastroEDefinicaoDeUnidMed(VUUnidMedCobr, EdUnidMedCobr,
    CBUnidMedCobr, QrUnidMedCobr);
end;

end.
