object FmSrvLOSOpePrm: TFmSrvLOSOpePrm
  Left = 339
  Top = 185
  Caption = 'LOC-SERVI-005 :: Pr'#234'mios de Servi'#231'o de OS'
  ClientHeight = 524
  ClientWidth = 924
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 385
    Width = 924
    Height = 25
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 493
    ExplicitHeight = 24
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 924
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 18
      Height = 13
      Caption = 'OS:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 132
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNO_SrvLCad
    end
    object Label2: TLabel
      Left = 72
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdControle
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNO_SrvLCad: TDBEdit
      Left = 132
      Top = 36
      Width = 473
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'NO_SrvLCad'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object DBEdControle: TdmkDBEdit
      Left = 72
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Controle'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 924
    Height = 273
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object EdConta: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object GroupBox3: TGroupBox
      Left = 12
      Top = 56
      Width = 593
      Height = 61
      Caption = ' Dados de cobran'#231'a do cliente:'
      TabOrder = 1
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 589
        Height = 44
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 897
        ExplicitHeight = 128
        object Label4: TLabel
          Left = 8
          Top = 0
          Width = 117
          Height = 13
          Caption = 'Unidade de Medida [F3]:'
        end
        object SBUnidMed: TSpeedButton
          Left = 396
          Top = 16
          Width = 21
          Height = 22
          Caption = '...'
          OnClick = SBUnidMedClick
        end
        object Label7: TLabel
          Left = 420
          Top = 0
          Width = 50
          Height = 13
          Caption = 'Valor total:'
        end
        object Label9: TLabel
          Left = 504
          Top = 0
          Width = 62
          Height = 13
          Caption = 'QtdMaxCobr:'
        end
        object EdUnidMedCobr: TdmkEditCB
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdUnidMedCobrChange
          OnKeyDown = EdUnidMedCobrKeyDown
          DBLookupComboBox = CBUnidMedCobr
          IgnoraDBLookupComboBox = False
        end
        object EdSiglaCobr: TdmkEdit
          Left = 64
          Top = 16
          Width = 40
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdSiglaCobrChange
          OnExit = EdSiglaCobrExit
          OnKeyDown = EdSiglaCobrKeyDown
        end
        object CBUnidMedCobr: TdmkDBLookupComboBox
          Left = 104
          Top = 16
          Width = 289
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsUnidMedCobr
          TabOrder = 2
          OnKeyDown = CBUnidMedCobrKeyDown
          dmkEditCB = EdUnidMedCobr
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdValTotCobr: TdmkEdit
          Left = 420
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValTotCobr'
          UpdCampo = 'ValTotCobr'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdQtdMaxCobr: TdmkEdit
          Left = 504
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'QtdMaxCobr'
          UpdCampo = 'QtdMaxCobr'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 12
      Top = 120
      Width = 901
      Height = 145
      Caption = ' Dados de pagamento de funcion'#225'rios:'
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 897
        Height = 128
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 117
          Height = 13
          Caption = 'Unidade de Medida [F3]:'
        end
        object SpeedButton1: TSpeedButton
          Left = 740
          Top = 16
          Width = 21
          Height = 22
          Caption = '...'
          OnClick = SBUnidMedClick
        end
        object Label8: TLabel
          Left = 8
          Top = 80
          Width = 56
          Height = 13
          Caption = 'Valor rateio:'
        end
        object Label10: TLabel
          Left = 92
          Top = 80
          Width = 65
          Height = 13
          Caption = 'QtdMaxPaga:'
        end
        object EdUnidMedPaga: TdmkEditCB
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdUnidMedPagaChange
          OnKeyDown = EdUnidMedPagaKeyDown
          DBLookupComboBox = CBUnidMedPaga
          IgnoraDBLookupComboBox = False
        end
        object EdSiglaPaga: TdmkEdit
          Left = 64
          Top = 16
          Width = 40
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdSiglaPagaChange
          OnExit = EdSiglaPagaExit
          OnKeyDown = EdSiglaPagaKeyDown
        end
        object CBUnidMedPaga: TdmkDBLookupComboBox
          Left = 104
          Top = 16
          Width = 633
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsUnidMedPaga
          TabOrder = 2
          OnKeyDown = CBUnidMedPagaKeyDown
          dmkEditCB = EdUnidMedPaga
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdValRatPaga: TdmkEdit
          Left = 8
          Top = 96
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValRatPaga'
          UpdCampo = 'ValRatPaga'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGFrmRatPaga: TdmkRadioGroup
          Left = 8
          Top = 40
          Width = 753
          Height = 37
          Caption = ' Forma de rateio: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            '0 - Indefinido'
            '1 - Divide valor por (qtd pessoas * fator pessoa)'
            '2 - ValRatPaga por pessoa')
          TabOrder = 4
          QryCampo = 'FrmRatPaga'
          UpdCampo = 'FrmRatPaga'
          UpdType = utYes
          OldValor = 0
        end
        object EdQtdMaxPaga: TdmkEdit
          Left = 92
          Top = 96
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'QtdMaxPaga'
          UpdCampo = 'QtdMaxPaga'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 924
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 876
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 828
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 319
        Height = 32
        Caption = 'Pr'#234'mios de Servi'#231'o de OS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 319
        Height = 32
        Caption = 'Pr'#234'mios de Servi'#231'o de OS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 319
        Height = 32
        Caption = 'Pr'#234'mios de Servi'#231'o de OS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 410
    Width = 924
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitTop = 517
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 920
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 454
    Width = 924
    Height = 70
    Align = alBottom
    TabOrder = 5
    ExplicitTop = 561
    object PnSaiDesis: TPanel
      Left = 778
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 776
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrUnidMedCobr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 196
    Top = 40
    object QrUnidMedCobrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCobrCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedCobrSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedCobrNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMedCobr: TDataSource
    DataSet = QrUnidMedCobr
    Left = 196
    Top = 84
  end
  object QrUnidMedPaga: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 280
    Top = 40
    object QrUnidMedPagaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedPagaCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedPagaSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedPagaNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMedPaga: TDataSource
    DataSet = QrUnidMedPaga
    Left = 280
    Top = 84
  end
  object VUUnidMedCobr: TdmkValUsu
    dmkEditCB = EdUnidMedCobr
    Panel = Panel3
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 360
    Top = 40
  end
  object VUUnidMedPaga: TdmkValUsu
    dmkEditCB = EdUnidMedPaga
    Panel = Panel5
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 360
    Top = 88
  end
end
