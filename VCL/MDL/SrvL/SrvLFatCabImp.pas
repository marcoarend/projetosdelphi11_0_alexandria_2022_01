unit SrvLFatCabImp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass, frxDBSet, Data.DB,
  mySQLDbTables, AppListas, UnDmkEnums, dmkGeral, UnInternalConsts;

type
  TFmSrvLFatCabImp = class(TForm)
    frxDsSrvLFatCab: TfrxDBDataset;
    frxDsSrvLOSOpeGrn: TfrxDBDataset;
    frxLOC_SERVI_021_01: TfrxReport;
    frxLOC_SERVI_021_01__: TfrxReport;
    QrSrvLFatCab: TmySQLQuery;
    QrSrvLFatCabNO_EMPRESA: TWideStringField;
    QrSrvLFatCabNO_CLIENTE: TWideStringField;
    QrSrvLFatCabCodigo: TIntegerField;
    QrSrvLFatCabNome: TWideStringField;
    QrSrvLFatCabEmpresa: TIntegerField;
    QrSrvLFatCabCliente: TIntegerField;
    QrSrvLFatCabAnoMes: TIntegerField;
    QrSrvLFatCabDtHrIni: TDateTimeField;
    QrSrvLFatCabDtHrFim: TDateTimeField;
    QrSrvLFatCabLk: TIntegerField;
    QrSrvLFatCabDataCad: TDateField;
    QrSrvLFatCabDataAlt: TDateField;
    QrSrvLFatCabUserCad: TIntegerField;
    QrSrvLFatCabUserAlt: TIntegerField;
    QrSrvLFatCabAlterWeb: TSmallintField;
    QrSrvLFatCabAtivo: TSmallintField;
    QrSrvLFatCabDtHrFim_TXT: TWideStringField;
    QrSrvLFatCabAnoMes_TXT: TWideStringField;
    QrSrvLFatCabValTotCbr: TFloatField;
    QrSrvLFatCabValDspToDsc: TFloatField;
    QrSrvLFatCabValLiqToFat: TFloatField;
    QrSrvLFatCabValLiqOkFat: TFloatField;
    QrSrvLFatCabValDspToDpl: TFloatField;
    QrSrvLFatCabValDspOkDpl: TFloatField;
    QrSrvLFatCabEntPagante: TIntegerField;
    QrSrvLFatCabValOutToFat: TFloatField;
    QrSrvLFatCabValDesToFat: TFloatField;
    QrSrvLFatCabCondicaoPG: TIntegerField;
    QrSrvLFatCabCartEmis: TIntegerField;
    QrSrvLFatCabGenero: TIntegerField;
    QrSrvLFatCabSerNF: TWideStringField;
    QrSrvLFatCabNumNF: TIntegerField;
    QrSrvLFatCabDtaFimFat: TDateTimeField;
    QrSrvLFatCabNO_ENTPAGANTE: TWideStringField;
    QrSrvLFatCabFilial: TIntegerField;
    QrSrvLOSOpeGrn: TmySQLQuery;
    QrSrvLOSOpeGrnCodigo: TIntegerField;
    QrSrvLOSOpeGrnControle: TIntegerField;
    QrSrvLOSOpeGrnSrvLCad: TIntegerField;
    QrSrvLOSOpeGrnLk: TIntegerField;
    QrSrvLOSOpeGrnDataCad: TDateField;
    QrSrvLOSOpeGrnDataAlt: TDateField;
    QrSrvLOSOpeGrnUserCad: TIntegerField;
    QrSrvLOSOpeGrnUserAlt: TIntegerField;
    QrSrvLOSOpeGrnAlterWeb: TSmallintField;
    QrSrvLOSOpeGrnAtivo: TSmallintField;
    QrSrvLOSOpeGrnNome: TWideStringField;
    QrSrvLOSOpeGrnNO_SrvLCad: TWideStringField;
    QrSrvLOSOpeGrnSrvLPrmCad: TIntegerField;
    QrSrvLOSOpeGrnNO_SrvLPrmCad: TWideStringField;
    QrSrvLOSOpeGrnNO_AgeEquiCab: TWideStringField;
    QrSrvLOSOpeGrnAgeEqiCab: TIntegerField;
    QrSrvLOSOpeGrnUnidMedCobr: TIntegerField;
    QrSrvLOSOpeGrnQtdRlzCobr: TFloatField;
    QrSrvLOSOpeGrnValFtrCobr: TFloatField;
    QrSrvLOSOpeGrnFrmFtrCobr: TSmallintField;
    QrSrvLOSOpeGrnUnidMedPaga: TIntegerField;
    QrSrvLOSOpeGrnQtdRlzPaga: TFloatField;
    QrSrvLOSOpeGrnValRatPaga: TFloatField;
    QrSrvLOSOpeGrnFrmRatPaga: TSmallintField;
    QrSrvLOSOpeGrnFrmFtrPaga: TSmallintField;
    QrSrvLOSOpeGrnValTotCobr: TFloatField;
    QrSrvLOSOpeGrnNO_FrmRatPaga: TWideStringField;
    QrSrvLOSOpeGrnSIGLA_UMC: TWideStringField;
    QrSrvLOSOpeGrnSIGLA_UMP: TWideStringField;
    QrSrvLOSOpeGrnNO_FrmFtrCobr: TWideStringField;
    QrSrvLOSOpeGrnNO_FrmFtrPaga: TWideStringField;
    QrSrvLOSOpeGrnValFtrPaga: TFloatField;
    QrSrvLOSOpeGrnValTotDesp: TFloatField;
    QrSrvLOSOpeGrnAutorizado: TSmallintField;
    QrSrvLOSOpeAtrDef: TmySQLQuery;
    QrSrvLOSOpeAtrDefID_Item: TIntegerField;
    QrSrvLOSOpeAtrDefID_Sorc: TIntegerField;
    QrSrvLOSOpeAtrDefAtrCad: TIntegerField;
    QrSrvLOSOpeAtrDefATRITS: TFloatField;
    QrSrvLOSOpeAtrDefCU_CAD: TIntegerField;
    QrSrvLOSOpeAtrDefCU_ITS: TLargeintField;
    QrSrvLOSOpeAtrDefAtrTxt: TWideStringField;
    QrSrvLOSOpeAtrDefNO_CAD: TWideStringField;
    QrSrvLOSOpeAtrDefNO_ITS: TWideStringField;
    QrSrvLOSOpeAtrDefAtrTyp: TSmallintField;
    QrSrvLOSOpeAtrDefTABELA: TWideStringField;
    frxDsSrvLOSOpeAtrDef: TfrxDBDataset;
    procedure frxLOC_SERVI_021_01__GetValue(const VarName: string;
      var Value: Variant);
    procedure QrSrvLFatCabAfterScroll(DataSet: TDataSet);
    procedure QrSrvLFatCabBeforeClose(DataSet: TDataSet);
    procedure QrSrvLOSOpeGrnAfterScroll(DataSet: TDataSet);
    procedure QrSrvLOSOpeGrnBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenSrvLOSOpeGrn();
    procedure ReopenSrvLOSOpeAtrDef();
  public
    { Public declarations }
    FSrvLFatCab, FEmpresa, FCliente: Integer;
    FNO_Empresa, FNO_Cliente: String;
    //
    procedure ImprimeFatCab();
  end;

var
  FmSrvLFatCabImp: TFmSrvLFatCabImp;

implementation

uses SrvLFatCab, UnMyObjects, ModuleGeral, Module, DmkDAC_PF, UnDmkProcFunc,
  UnSrvL_PF;

{$R *.dfm}

procedure TFmSrvLFatCabImp.frxLOC_SERVI_021_01__GetValue(const VarName: string;
  var Value: Variant);
var
  Linha1, linha2, Linha3: String;
begin
  if VarName = 'VARF_NO_EMPRESA' then
    Value := FNO_Empresa
  else
  if VarName = 'VARF_NO_CLIENTE' then
    Value := FNO_Cliente
  else
  if VarName = 'VARF_ENDERECO_EMP' then
  begin
    DModG.ObtemEnderecoEtiqueta3Linhas(FEmpresa, Linha1, linha2, Linha3);
    Value := Linha1 + sLineBreak + Linha2 + sLineBreak + Linha3;
  end else
  if VarName = 'VARF_ENDERECO_CLI' then
  begin
    DModG.ObtemEnderecoEtiqueta3Linhas(FCliente, Linha1, linha2, Linha3);
    Value := Linha1 + sLineBreak + Linha2 + sLineBreak + Linha3;
  end else
end;

procedure TFmSrvLFatCabImp.ImprimeFatCab();
begin
  UnDmkDAC_PF.AbreQuery(QrSrvLFatCab, Dmod.MyDB);
  MyObjects.frxDefineDataSets(frxLOC_SERVI_021_01, [
  frxDsSrvLFatCab,
  frxDsSrvLOSOpeGrn,
  frxDsSrvLOSOpeAtrDef
  ]);
  MyObjects.frxMostra(frxLOC_SERVI_021_01, 'OS');
end;

procedure TFmSrvLFatCabImp.QrSrvLFatCabAfterScroll(DataSet: TDataSet);
const
  Controle = 0;
var
  Codigo: Integer;
begin
  Codigo := QrSrvLFatCabCodigo.Value;
  ReopenSrvLOSOpeGrn();
  //SrvL_PF.ReopenXPrz(QrSrvLFatPrz, 'SrvLFatPrz', Codigo, Controle);
  //SrvL_PF.ReopenLctFatRef(QrLctFatRef, QrSrvLFatCabCodigo.Value, 0);
end;

procedure TFmSrvLFatCabImp.QrSrvLFatCabBeforeClose(DataSet: TDataSet);
begin
  QrSrvLOSOpeGrn.Close;
end;

procedure TFmSrvLFatCabImp.QrSrvLOSOpeGrnAfterScroll(DataSet: TDataSet);
begin
  ReopenSrvLOSOpeAtrDef();
end;

procedure TFmSrvLFatCabImp.QrSrvLOSOpeGrnBeforeClose(DataSet: TDataSet);
begin
  QrSrvLOSOpeAtrDef.Close;
end;

procedure TFmSrvLFatCabImp.ReopenSrvLOSOpeAtrDef();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLOSOpeAtrDef, Dmod.MyDB, [
  'SELECT "srvlosopeatrdef" TABELA, ',
  'def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ATRITS, ',
  'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,',
  'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp',
  'FROM srvlosopeatrdef def',
  'LEFT JOIN srvlosopeatrits its ON its.Controle=def.AtrIts ',
  'LEFT JOIN srvlosopeatrcad cad ON cad.Codigo=def.AtrCad ',
  'WHERE def.ID_Sorc=' + Geral.FF0(QrSrvLOSOpeGrnControle.Value),
  ' ',
  'UNION  ',
  ' ',
  'SELECT "srvlosopeatrtxt" TABELA,',
  'def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, ',
  'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,',
  'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp ',
  'FROM srvlosopeatrtxt def ',
  'LEFT JOIN srvlosopeatrcad cad ON cad.Codigo=def.AtrCad ',
  'WHERE def.ID_Sorc=' + Geral.FF0(QrSrvLOSOpeGrnControle.Value),
  ' ',
  'ORDER BY NO_CAD, NO_ITS ',
  '']);
end;


procedure TFmSrvLFatCabImp.ReopenSrvLOSOpeGrn();
var
  ATT_FrmFtrCobr, ATT_FrmFtrPaga, ATT_FrmRatPaga: String;
begin
  ATT_FrmFtrCobr := dmkPF.ArrayToTexto('oog.FrmFtrCobr', 'NO_FrmFtrCobr',
    pvPos, True, sPrmFatorValor);
  ATT_FrmFtrPaga := dmkPF.ArrayToTexto('oog.FrmFtrPaga', 'NO_FrmFtrPaga',
    pvPos, True, sPrmFatorValor);
  ATT_FrmRatPaga := dmkPF.ArrayToTexto('oog.FrmRatPaga', 'NO_FrmRatPaga',
    pvPos, True, sPrmFormaRateio);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLOSOpeGrn, Dmod.MyDB, [
  'SELECT slc.Nome NO_SrvLCad, prm.Nome NO_SrvLPrmCad,  ',
  ATT_FrmFtrCobr,
  ATT_FrmFtrPaga,
  ATT_FrmRatPaga,
  'umc.Sigla SIGLA_UMC, ump.Sigla SIGLA_UMP, ',
  'aec.Nome NO_AgeEquiCab, oog.*  ',
  'FROM srvlosopegrn oog ',
  'LEFT JOIN srvlcad slc ON slc.Codigo=oog.SrvLCad ',
  'LEFT JOIN srvlprmcad prm ON prm.Codigo=oog.SrvLPrmCad ',
  'LEFT JOIN ageeqicab aec ON oog.AgeEqiCab=aec.Codigo ',
  'LEFT JOIN unidmed umc ON umc.Codigo=oog.UnidMedCobr ',
  'LEFT JOIN unidmed ump ON ump.Codigo=oog.UnidMedPaga ',
  'WHERE oog.SrvLFatCab=' + Geral.FF0(QrSrvLFatCabCodigo.Value),
  '']);
end;

end.
