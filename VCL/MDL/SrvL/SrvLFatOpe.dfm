object FmSrvLFatOpe: TFmSrvLFatOpe
  Left = 339
  Top = 185
  Caption = 'LOC-SERVI-010 :: Itens de Faturamento de OSs'
  ClientHeight = 607
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 468
    Width = 1008
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 141
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    TabOrder = 0
    object Panel3: TPanel
      Left = 609
      Top = 15
      Width = 397
      Height = 124
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object RGFrmCtbDspFat: TdmkRadioGroup
        Left = 8
        Top = 11
        Width = 169
        Height = 105
        Caption = ' Pagto despesas (a info no item):'
        ItemIndex = 0
        Items.Strings = (
          'TSrvLCtbDsp.slcdUnknown=0, '
          'TSrvLCtbDsp.slcdNoGerCtaAPag=1, '
          'TSrvLCtbDsp.slcdDescoFaturam=2,'
          'TSrvLCtbDsp.slcdGeraCtaAPag=3')
        TabOrder = 0
        UpdType = utYes
        OldValor = 0
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 607
      Height = 124
      Align = alLeft
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object Label5: TLabel
        Left = 8
        Top = 0
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label3: TLabel
        Left = 68
        Top = 0
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label1: TLabel
        Left = 8
        Top = 40
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdEmpresa
      end
      object Label4: TLabel
        Left = 8
        Top = 80
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdCliente
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TDBEdit
        Left = 68
        Top = 16
        Width = 533
        Height = 21
        TabStop = False
        Color = clWhite
        DataField = 'Nome'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdEmpresa: TdmkDBEdit
        Left = 8
        Top = 56
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Empresa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNO_Empresa: TDBEdit
        Left = 68
        Top = 56
        Width = 533
        Height = 21
        TabStop = False
        Color = clWhite
        DataField = 'NO_Empresa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
      end
      object DBEdCliente: TdmkDBEdit
        Left = 8
        Top = 96
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 4
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNO_Cliente: TDBEdit
        Left = 68
        Top = 96
        Width = 533
        Height = 21
        TabStop = False
        Color = clWhite
        DataField = 'NO_Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 189
    Width = 1008
    Height = 279
    Align = alClient
    Caption = ' OSs / Servi'#231'os em aberto: '
    TabOrder = 1
    object DGDados: TdmkDBGridZTO
      Left = 2
      Top = 15
      Width = 1004
      Height = 262
      Align = alClient
      DataSource = DsSrvLOSOpeGrn
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'OS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtAbertura'
          Title.Caption = 'Abertura da OS'
          Width = 115
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID Servi'#231'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SrvLCad'
          Title.Caption = 'Servi'#231'o'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SrvLPrmCad'
          Title.Caption = 'Configura'#231#227'o de pr'#234'mios'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Observa'#231#227'o'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FrmFtrCobr'
          Title.Caption = 'Fator de cobran'#231'a'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SIGLA_UMC'
          Title.Caption = 'Unidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValFtrCobr'
          Title.Caption = '$ Unit.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdRlzCobr'
          Title.Caption = 'Qtde cobr.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValTotCobr'
          Title.Caption = '$ Total cobr'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValTotDesp'
          Title.Caption = '$ Despesas'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FrmFtrPaga'
          Title.Caption = 'Fator de pagamento'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SIGLA_UMP'
          Title.Caption = 'Unidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValFtrPaga'
          Title.Caption = '$ Unit.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdRlzPaga'
          Title.Caption = 'Qtde pag.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FrmRatPaga'
          Title.Caption = 'Forma de rateio'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_AgeEquiCab'
          Title.Caption = 'Equipe de agentes'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValRatPaga'
          Title.Caption = '$ Total pag.'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 359
        Height = 32
        Caption = 'Itens de Faturamento de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 359
        Height = 32
        Caption = 'Itens de Faturamento de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 359
        Height = 32
        Caption = 'Itens de Faturamento de OSs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 493
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 537
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtTudo: TBitBtn
        Tag = 127
        Left = 284
        Top = 4
        Width = 100
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTudoClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 394
        Top = 4
        Width = 100
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtNenhumClick
      end
    end
  end
  object QrSrvLOSOpeGrn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT slc.Nome NO_SrvLCad, prm.Nome NO_SrvLPrmCad,  '
      
        ' ELT(oog.FrmFtrCobr + 1,"Indefinido","Valor total","Valor unit'#225'r' +
        'io") NO_FrmFtrCobr,'
      
        ' ELT(oog.FrmFtrPaga + 1,"Indefinido","Valor total","Valor unit'#225'r' +
        'io") NO_FrmFtrPaga,'
      
        ' ELT(oog.FrmRatPaga + 1,"Indefinido","Valor total","Valor por pe' +
        'ssoa") NO_FrmRatPaga,'
      'umc.Sigla SIGLA_UMC, ump.Sigla SIGLA_UMP, '
      'aec.Nome NO_AgeEquiCab, oog.*  '
      'FROM srvlosopegrn oog '
      'LEFT JOIN srvlcad slc ON slc.Codigo=oog.SrvLCad '
      'LEFT JOIN srvlprmcad prm ON prm.Codigo=oog.SrvLPrmCad '
      'LEFT JOIN ageeqicab aec ON oog.AgeEqiCab=aec.Codigo '
      'LEFT JOIN unidmed umc ON umc.Codigo=oog.UnidMedCobr '
      'LEFT JOIN unidmed ump ON ump.Codigo=oog.UnidMedPaga '
      'WHERE oog.Codigo=2'
      '')
    Left = 88
    Top = 260
    object QrSrvLOSOpeGrnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLOSOpeGrnControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrvLOSOpeGrnSrvLCad: TIntegerField
      FieldName = 'SrvLCad'
    end
    object QrSrvLOSOpeGrnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLOSOpeGrnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLOSOpeGrnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLOSOpeGrnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLOSOpeGrnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLOSOpeGrnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLOSOpeGrnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLOSOpeGrnNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSrvLOSOpeGrnNO_SrvLCad: TWideStringField
      FieldName = 'NO_SrvLCad'
      Size = 60
    end
    object QrSrvLOSOpeGrnSrvLPrmCad: TIntegerField
      FieldName = 'SrvLPrmCad'
    end
    object QrSrvLOSOpeGrnNO_SrvLPrmCad: TWideStringField
      FieldName = 'NO_SrvLPrmCad'
      Size = 60
    end
    object QrSrvLOSOpeGrnNO_AgeEquiCab: TWideStringField
      FieldName = 'NO_AgeEquiCab'
      Size = 255
    end
    object QrSrvLOSOpeGrnAgeEqiCab: TIntegerField
      FieldName = 'AgeEqiCab'
    end
    object QrSrvLOSOpeGrnUnidMedCobr: TIntegerField
      FieldName = 'UnidMedCobr'
    end
    object QrSrvLOSOpeGrnQtdRlzCobr: TFloatField
      FieldName = 'QtdRlzCobr'
    end
    object QrSrvLOSOpeGrnValFtrCobr: TFloatField
      FieldName = 'ValFtrCobr'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrSrvLOSOpeGrnFrmFtrCobr: TSmallintField
      FieldName = 'FrmFtrCobr'
    end
    object QrSrvLOSOpeGrnUnidMedPaga: TIntegerField
      FieldName = 'UnidMedPaga'
    end
    object QrSrvLOSOpeGrnQtdRlzPaga: TFloatField
      FieldName = 'QtdRlzPaga'
    end
    object QrSrvLOSOpeGrnValRatPaga: TFloatField
      FieldName = 'ValRatPaga'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnFrmRatPaga: TSmallintField
      FieldName = 'FrmRatPaga'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnFrmFtrPaga: TSmallintField
      FieldName = 'FrmFtrPaga'
    end
    object QrSrvLOSOpeGrnValTotCobr: TFloatField
      FieldName = 'ValTotCobr'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnNO_FrmRatPaga: TWideStringField
      FieldName = 'NO_FrmRatPaga'
      Size = 60
    end
    object QrSrvLOSOpeGrnSIGLA_UMC: TWideStringField
      FieldName = 'SIGLA_UMC'
      Size = 6
    end
    object QrSrvLOSOpeGrnSIGLA_UMP: TWideStringField
      FieldName = 'SIGLA_UMP'
      Size = 6
    end
    object QrSrvLOSOpeGrnNO_FrmFtrCobr: TWideStringField
      FieldName = 'NO_FrmFtrCobr'
      Size = 60
    end
    object QrSrvLOSOpeGrnNO_FrmFtrPaga: TWideStringField
      FieldName = 'NO_FrmFtrPaga'
      Size = 60
    end
    object QrSrvLOSOpeGrnValFtrPaga: TFloatField
      FieldName = 'ValFtrPaga'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrSrvLOSOpeGrnValTotDesp: TFloatField
      FieldName = 'ValTotDesp'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnDtAbertura: TDateTimeField
      FieldName = 'DtAbertura'
    end
  end
  object DsSrvLOSOpeGrn: TDataSource
    DataSet = QrSrvLOSOpeGrn
    Left = 88
    Top = 316
  end
end
