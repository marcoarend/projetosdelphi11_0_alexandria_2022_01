object FmSrvLOSOpeDsp: TFmSrvLOSOpeDsp
  Left = 339
  Top = 185
  Caption = 'LOC-SERVI-008 :: Despesa de Item de OS'
  ClientHeight = 532
  ClientWidth = 621
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 368
    Width = 621
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 621
    Height = 61
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 18
      Height = 13
      Caption = 'OS:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 140
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object Label1: TLabel
      Left = 72
      Top = 20
      Width = 49
      Height = 13
      Caption = 'ID Opera.:'
      FocusControl = DBEdControle
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 140
      Top = 36
      Width = 465
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object DBEdControle: TdmkDBEdit
      Left = 72
      Top = 36
      Width = 64
      Height = 21
      TabStop = False
      DataField = 'Controle'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 109
    Width = 621
    Height = 259
    Align = alClient
    Caption = ' Dados do item: '
    TabOrder = 1
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 617
      Height = 38
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label6: TLabel
        Left = 12
        Top = 0
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object EdConta: TdmkEdit
        Left = 12
        Top = 16
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 53
      Width = 617
      Height = 204
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object LaConta: TLabel
        Left = 12
        Top = 4
        Width = 131
        Height = 13
        Caption = 'Conta (do plano de contas):'
      end
      object Label4: TLabel
        Left = 12
        Top = 48
        Width = 37
        Height = 13
        Caption = 'Agente:'
      end
      object Label2: TLabel
        Left = 12
        Top = 92
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object SbConta: TSpeedButton
        Left = 584
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbContaClick
      end
      object SbAgente: TSpeedButton
        Left = 584
        Top = 64
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbAgenteClick
      end
      object SbFornece: TSpeedButton
        Left = 584
        Top = 108
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbForneceClick
      end
      object Label7: TLabel
        Left = 12
        Top = 132
        Width = 74
        Height = 13
        Caption = 'Valor de d'#233'bito:'
      end
      object EdGenero: TdmkEditCB
        Left = 12
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGenero
        IgnoraDBLookupComboBox = False
      end
      object CBGenero: TdmkDBLookupComboBox
        Left = 80
        Top = 20
        Width = 500
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 1
        dmkEditCB = EdGenero
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdAgente: TdmkEditCB
        Left = 12
        Top = 64
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBAgente
        IgnoraDBLookupComboBox = False
      end
      object CBAgente: TdmkDBLookupComboBox
        Left = 80
        Top = 64
        Width = 500
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_AGENTE'
        ListSource = DsAgentes
        TabOrder = 3
        dmkEditCB = EdAgente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFornece: TdmkEditCB
        Left = 12
        Top = 108
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornece
        IgnoraDBLookupComboBox = False
      end
      object CBFornece: TdmkDBLookupComboBox
        Left = 80
        Top = 108
        Width = 500
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTI'
        ListSource = DsFornece
        TabOrder = 5
        dmkEditCB = EdFornece
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDebito: TdmkEdit
        Left = 12
        Top = 147
        Width = 76
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object RGFrmCtbDspFat: TdmkRadioGroup
        Left = 92
        Top = 132
        Width = 513
        Height = 49
        Caption = ' Pagamento das despesas quando faturar:'
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'TSrvLCtbDsp.slcdUnknown=0, '
          'TSrvLCtbDsp.slcdNoGerCtaAPag=1, '
          'TSrvLCtbDsp.slcdDescoFaturam=2,'
          'TSrvLCtbDsp.slcdGeraCtaAPag=3')
        TabOrder = 7
        QryCampo = 'FrmCtbDspFat'
        UpdCampo = 'FrmCtbDspFat'
        UpdType = utYes
        OldValor = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 621
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 573
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 525
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 292
        Height = 32
        Caption = 'Despesa de Item de OS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 292
        Height = 32
        Caption = 'Despesa de Item de OS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 292
        Height = 32
        Caption = 'Despesa de Item de OS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 418
    Width = 621
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 617
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 462
    Width = 621
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 475
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 473
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 164
    Top = 42
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 164
    Top = 90
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 292
    Top = 90
  end
  object QrFornece: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'ORDER BY NOMEENTI')
    Left = 292
    Top = 42
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object QrAgentes: TmySQLQuery
    Database = Dmod.MyDB
    Left = 228
    Top = 42
    object QrAgentesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgentesNO_AGENTE: TWideStringField
      FieldName = 'NO_AGENTE'
      Size = 100
    end
  end
  object DsAgentes: TDataSource
    DataSet = QrAgentes
    Left = 228
    Top = 90
  end
end
