object FmSrvLOSOpeGrn: TFmSrvLOSOpeGrn
  Left = 339
  Top = 185
  Caption = 'LOC-SERVI-004 :: Item de Servi'#231'o de OS'
  ClientHeight = 671
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 532
    Width = 1008
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object Label6: TLabel
      Left = 532
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 457
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object EdControle: TdmkEdit
      Left = 532
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 1008
    Height = 420
    Align = alClient
    Caption = ' Dados do item: '
    TabOrder = 1
    object GroupBox4: TGroupBox
      Left = 2
      Top = 15
      Width = 1004
      Height = 138
      Align = alTop
      Caption = ' Contrata'#231#227'o do servi'#231'o:'
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 1000
        Height = 121
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 39
          Height = 13
          Caption = 'Servi'#231'o:'
        end
        object Label2: TLabel
          Left = 504
          Top = 0
          Width = 440
          Height = 13
          Caption = 
            'Configura'#231#227'o de pr'#234'mios (ser'#227'o mostrados somente os ativos do se' +
            'rvi'#231'o selecionado acima!):'
        end
        object Label55: TLabel
          Left = 504
          Top = 40
          Width = 92
          Height = 13
          Caption = 'Equipe de agentes:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label7: TLabel
          Left = 8
          Top = 80
          Width = 61
          Height = 13
          Caption = 'Observa'#231#227'o:'
        end
        object SBSrvLCad: TSpeedButton
          Left = 478
          Top = 16
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBSrvLCadClick
        end
        object SBSrvLPrmCad: TSpeedButton
          Left = 974
          Top = 16
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBSrvLPrmCadClick
        end
        object Label9: TLabel
          Left = 8
          Top = 40
          Width = 177
          Height = 13
          Caption = 'Configura'#231#227'o de equipes de agentes:'
          Color = clBtnFace
          ParentColor = False
        end
        object SbAgeEqiCfg: TSpeedButton
          Left = 478
          Top = 56
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbAgeEqiCfgClick
        end
        object EdSrvLCad: TdmkEditCB
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'SrvLCad'
          UpdCampo = 'SrvLCad'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdSrvLCadRedefinido
          DBLookupComboBox = CBSrvLCad
          IgnoraDBLookupComboBox = False
        end
        object CBSrvLCad: TdmkDBLookupComboBox
          Left = 64
          Top = 16
          Width = 412
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsSrvLCad
          TabOrder = 1
          dmkEditCB = EdSrvLCad
          QryCampo = 'SrvLCad'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdSrvLPrmCad: TdmkEditCB
          Left = 504
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'SrvLPrmCad'
          UpdCampo = 'SrvLPrmCad'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBSrvLPrmCad
          IgnoraDBLookupComboBox = False
        end
        object CBSrvLPrmCad: TdmkDBLookupComboBox
          Left = 560
          Top = 16
          Width = 412
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsSrvLPrmCad
          TabOrder = 3
          dmkEditCB = EdSrvLPrmCad
          QryCampo = 'SrvLPrmCad'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdAgeEqiCab: TdmkEditCB
          Left = 504
          Top = 56
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'AgeEqiCab'
          UpdCampo = 'AgeEqiCab'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBAgeEqiCab
          IgnoraDBLookupComboBox = False
        end
        object CBAgeEqiCab: TdmkDBLookupComboBox
          Left = 560
          Top = 56
          Width = 313
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsAgeEqiCab
          TabOrder = 7
          dmkEditCB = EdAgeEqiCab
          QryCampo = 'AgeEqiCab'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdNome: TdmkEdit
          Left = 8
          Top = 96
          Width = 865
          Height = 21
          TabOrder = 8
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CBAgeEqiCfg: TdmkDBLookupComboBox
          Left = 64
          Top = 56
          Width = 413
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsAgeEqiCfg
          TabOrder = 5
          dmkEditCB = EdAgeEqiCfg
          QryCampo = 'AgeEqiCfg'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdAgeEqiCfg: TdmkEditCB
          Left = 8
          Top = 56
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'AgeEqiCfg'
          UpdCampo = 'AgeEqiCfg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdAgeEqiCfgRedefinido
          DBLookupComboBox = CBAgeEqiCfg
          IgnoraDBLookupComboBox = False
        end
        object RGAutorizado: TdmkRadioGroup
          Left = 880
          Top = 56
          Width = 115
          Height = 61
          Caption = ' Autorizado: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'N'#227'o'
            'Sim')
          TabOrder = 9
          QryCampo = 'Autorizado'
          UpdCampo = 'Autorizado'
          UpdType = utYes
          OldValor = 0
        end
      end
    end
    object GroupBox3: TGroupBox
      Left = 2
      Top = 153
      Width = 1004
      Height = 265
      Align = alClient
      Caption = '                                                 '
      TabOrder = 1
      object PnExecucao: TPanel
        Left = 2
        Top = 15
        Width = 1000
        Height = 248
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object GroupBox5: TGroupBox
          Left = 0
          Top = 0
          Width = 1000
          Height = 97
          Align = alTop
          Caption = ' Dados de cobran'#231'a do cliente:'
          TabOrder = 0
          object Panel3: TPanel
            Left = 2
            Top = 15
            Width = 996
            Height = 80
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label4: TLabel
              Left = 124
              Top = 0
              Width = 138
              Height = 13
              Caption = 'Unidade de Medida [F3] [F5]:'
            end
            object SBUnidMed: TSpeedButton
              Left = 512
              Top = 16
              Width = 21
              Height = 22
              Caption = '...'
              OnClick = SBUnidMedClick
            end
            object LaValFtrCobr: TLabel
              Left = 536
              Top = 40
              Width = 33
              Height = 13
              Caption = '?????:'
            end
            object LaQtdRlzCobr: TLabel
              Left = 8
              Top = 0
              Width = 58
              Height = 13
              Caption = 'Quantidade:'
            end
            object LaValTotCobr: TLabel
              Left = 640
              Top = 40
              Width = 50
              Height = 13
              Caption = 'Valor total:'
            end
            object EdUnidMedCobr: TdmkEditCB
              Left = 124
              Top = 16
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdUnidMedCobrChange
              OnKeyDown = EdUnidMedCobrKeyDown
              DBLookupComboBox = CBUnidMedCobr
              IgnoraDBLookupComboBox = False
            end
            object EdSiglaCobr: TdmkEdit
              Left = 180
              Top = 16
              Width = 40
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdSiglaCobrChange
              OnExit = EdSiglaCobrExit
              OnKeyDown = EdSiglaCobrKeyDown
            end
            object CBUnidMedCobr: TdmkDBLookupComboBox
              Left = 220
              Top = 16
              Width = 289
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DsUnidMedCobr
              TabOrder = 3
              OnKeyDown = CBUnidMedCobrKeyDown
              dmkEditCB = EdUnidMedCobr
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdValFtrCobr: TdmkEdit
              Left = 536
              Top = 56
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              QryCampo = 'ValFtrCobr'
              UpdCampo = 'ValFtrCobr'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdValFtrCobrChange
            end
            object EdQtdRlzCobr: TdmkEdit
              Left = 8
              Top = 16
              Width = 112
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              QryCampo = 'QtdRlzCobr'
              UpdCampo = 'QtdRlzCobr'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdQtdRlzCobrChange
            end
            object RGFrmFtrCobr: TdmkRadioGroup
              Left = 8
              Top = 40
              Width = 525
              Height = 37
              Caption = ' Fator de cobran'#231'a: '
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                'pfcIndefinido=0, '
                'pfcValorTotal=1, '
                'pfcValorUnitario=2')
              TabOrder = 4
              OnClick = RGFrmFtrCobrClick
              QryCampo = 'FrmFtrCobr'
              UpdCampo = 'FrmFtrCobr'
              UpdType = utYes
              OldValor = 0
            end
            object EdValTotCobr: TdmkEdit
              Left = 640
              Top = 56
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'ValTotCobr'
              UpdCampo = 'ValTotCobr'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdValTotCobrChange
            end
            object CkWrnLctDay: TdmkCheckBox
              Left = 852
              Top = 35
              Width = 97
              Height = 17
              Caption = 'Empreita incerta.'
              TabOrder = 7
              QryCampo = 'WrnLctDay'
              UpdCampo = 'WrnLctDay'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
          end
        end
        object Panel6: TPanel
          Left = 0
          Top = 97
          Width = 1000
          Height = 151
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object GroupBox6: TGroupBox
            Left = 0
            Top = 0
            Width = 645
            Height = 151
            Align = alLeft
            Caption = ' Dados de pagamento de funcion'#225'rios:'
            TabOrder = 0
            object Panel7: TPanel
              Left = 2
              Top = 15
              Width = 641
              Height = 134
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label8: TLabel
                Left = 124
                Top = 0
                Width = 138
                Height = 13
                Caption = 'Unidade de Medida [F3] [F5]:'
              end
              object SpeedButton1: TSpeedButton
                Left = 512
                Top = 16
                Width = 21
                Height = 22
                Caption = '...'
                OnClick = SBUnidMedClick
              end
              object LaValFtrPaga: TLabel
                Left = 536
                Top = 40
                Width = 33
                Height = 13
                Caption = '?????:'
              end
              object LaQtdRlzPaga: TLabel
                Left = 8
                Top = 0
                Width = 58
                Height = 13
                Caption = 'Quantidade:'
              end
              object LaValRatPaga: TLabel
                Left = 536
                Top = 80
                Width = 33
                Height = 13
                Caption = '?????:'
              end
              object EdUnidMedPaga: TdmkEditCB
                Left = 124
                Top = 16
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdUnidMedPagaChange
                OnKeyDown = EdUnidMedPagaKeyDown
                DBLookupComboBox = CBUnidMedPaga
                IgnoraDBLookupComboBox = False
              end
              object EdSiglaPaga: TdmkEdit
                Left = 180
                Top = 16
                Width = 40
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdSiglaPagaChange
                OnExit = EdSiglaPagaExit
                OnKeyDown = EdSiglaPagaKeyDown
              end
              object CBUnidMedPaga: TdmkDBLookupComboBox
                Left = 220
                Top = 16
                Width = 289
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                ListSource = DsUnidMedPaga
                TabOrder = 3
                OnKeyDown = CBUnidMedPagaKeyDown
                dmkEditCB = EdUnidMedPaga
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdValFtrPaga: TdmkEdit
                Left = 536
                Top = 56
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryCampo = 'ValFtrPaga'
                UpdCampo = 'ValFtrPaga'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdValFtrPagaChange
              end
              object EdQtdRlzPaga: TdmkEdit
                Left = 8
                Top = 16
                Width = 112
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 3
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000'
                QryCampo = 'QtdRlzPaga'
                UpdCampo = 'QtdRlzPaga'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdQtdRlzPagaChange
              end
              object RGFrmRatPaga: TdmkRadioGroup
                Left = 8
                Top = 80
                Width = 525
                Height = 37
                Caption = ' Forma de rateio: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'pfrIndefinido=0, '
                  'pfrValorTotal=1, '
                  'pfrValorPorPessoa=2')
                TabOrder = 6
                OnClick = RGFrmRatPagaClick
                QryCampo = 'FrmRatPaga'
                UpdCampo = 'FrmRatPaga'
                UpdType = utYes
                OldValor = 0
              end
              object RGFrmFtrPaga: TdmkRadioGroup
                Left = 8
                Top = 40
                Width = 525
                Height = 37
                Caption = ' Fator de pagamento: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'pfcIndefinido=0, '
                  'pfcValorTotal=1, '
                  'pfcValorUnitario=2')
                TabOrder = 4
                OnClick = RGFrmFtrPagaClick
                QryCampo = 'FrmFtrPaga'
                UpdCampo = 'FrmFtrPaga'
                UpdType = utYes
                OldValor = 0
              end
              object EdValRatPaga: TdmkEdit
                Left = 536
                Top = 96
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 7
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'ValRatPaga'
                UpdCampo = 'ValRatPaga'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdValRatPagaChange
              end
            end
          end
          object GroupBox7: TGroupBox
            Left = 645
            Top = 0
            Width = 355
            Height = 151
            Align = alClient
            Caption = ' Dados gerais da execu'#231#227'o: '
            TabOrder = 1
            object Panel8: TPanel
              Left = 2
              Top = 15
              Width = 351
              Height = 134
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label13: TLabel
                Left = 176
                Top = 0
                Width = 75
                Height = 13
                Caption = 'Final execu'#231#227'o:'
              end
              object Label12: TLabel
                Left = 4
                Top = 0
                Width = 80
                Height = 13
                Caption = 'In'#237'cio execu'#231#227'o:'
              end
              object TPDtaExeIni: TdmkEditDateTimePicker
                Left = 4
                Top = 16
                Width = 112
                Height = 21
                Date = 0.768997777777258400
                Time = 0.768997777777258400
                TabOrder = 0
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                QryCampo = 'DtaExeIni'
                UpdCampo = 'DtaExeIni'
                UpdType = utYes
              end
              object EdDtaExeFim: TdmkEdit
                Left = 288
                Top = 16
                Width = 56
                Height = 21
                TabOrder = 3
                FormatType = dmktfTime
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfLong
                HoraFormat = dmkhfLong
                Texto = '00:00:00'
                QryCampo = 'DtaExeFim'
                UpdCampo = 'DtaExeFim'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object TPDtaExeFim: TdmkEditDateTimePicker
                Left = 176
                Top = 16
                Width = 112
                Height = 21
                Date = 0.768997777777258400
                Time = 0.768997777777258400
                TabOrder = 2
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                QryCampo = 'DtaExeFim'
                UpdCampo = 'DtaExeFim'
                UpdType = utYes
              end
              object EdDtaExeIni: TdmkEdit
                Left = 116
                Top = 16
                Width = 56
                Height = 21
                TabOrder = 1
                FormatType = dmktfTime
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfLong
                HoraFormat = dmkhfLong
                Texto = '00:00:00'
                QryCampo = 'DtaExeIni'
                UpdCampo = 'DtaExeIni'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
        end
      end
      object CkExecucao: TCheckBox
        Left = 12
        Top = 0
        Width = 133
        Height = 17
        Caption = ' Execu'#231#227'o do servi'#231'o: '
        TabOrder = 0
        OnClick = CkExecucaoClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 274
        Height = 32
        Caption = 'Item de Servi'#231'o de OS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 274
        Height = 32
        Caption = 'Item de Servi'#231'o de OS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 274
        Height = 32
        Caption = 'Item de Servi'#231'o de OS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 557
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 601
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DsSrvLCad: TDataSource
    DataSet = QrSrvLCad
    Left = 372
    Top = 48
  end
  object QrSrvLCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, WrnLctDay'
      'FROM srvlcad'
      'ORDER BY Nome')
    Left = 372
    Top = 4
    object QrSrvLCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrSrvLCadWrnLctDay: TIntegerField
      FieldName = 'WrnLctDay'
    end
  end
  object QrSrvLPrmCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prm.*, slc.Nome NO_SrvLCad'
      'FROM srvlprmcad prm '
      'LEFT JOIN srvlcad slc ON slc.Codigo=prm.SrvLCad'
      'WHERE prm.Codigo=0')
    Left = 452
    Top = 5
    object QrSrvLPrmCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLPrmCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrSrvLPrmCadUnidMedCobr: TIntegerField
      FieldName = 'UnidMedCobr'
    end
    object QrSrvLPrmCadFrmFtrCobr: TSmallintField
      FieldName = 'FrmFtrCobr'
    end
    object QrSrvLPrmCadFrmFtrPaga: TIntegerField
      FieldName = 'FrmFtrPaga'
    end
    object QrSrvLPrmCadUnidMedPaga: TIntegerField
      FieldName = 'UnidMedPaga'
    end
    object QrSrvLPrmCadFrmRatPaga: TSmallintField
      FieldName = 'FrmRatPaga'
    end
  end
  object DsSrvLPrmCad: TDataSource
    DataSet = QrSrvLPrmCad
    Left = 452
    Top = 49
  end
  object QrAgeEqiCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ageeqicab '
      'ORDER BY Nome'
      ' ')
    Left = 528
    Top = 4
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgeEqiCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsAgeEqiCab: TDataSource
    DataSet = QrAgeEqiCab
    Left = 528
    Top = 52
  end
  object QrUnidMedCobr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 624
    object QrUnidMedCobrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCobrCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedCobrSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedCobrNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMedCobr: TDataSource
    DataSet = QrUnidMedCobr
    Left = 624
    Top = 44
  end
  object QrUnidMedPaga: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 708
    object QrUnidMedPagaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedPagaCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedPagaSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedPagaNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMedPaga: TDataSource
    DataSet = QrUnidMedPaga
    Left = 708
    Top = 44
  end
  object VUUnidMedCobr: TdmkValUsu
    dmkEditCB = EdUnidMedPaga
    Panel = Panel6
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 788
  end
  object VUUnidMedPaga: TdmkValUsu
    dmkEditCB = EdUnidMedCobr
    Panel = Panel5
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 788
    Top = 48
  end
  object QrSrvLPrmNvs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nvs.*'
      'FROM srvlprmnvs nvs '
      'WHERE nvs.Codigo=1')
    Left = 940
    Top = 49
    object QrSrvLPrmNvsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLPrmNvsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrvLPrmNvsUnidMedCobr: TIntegerField
      FieldName = 'UnidMedCobr'
    end
    object QrSrvLPrmNvsFrmFtrCobr: TSmallintField
      FieldName = 'FrmFtrCobr'
    end
    object QrSrvLPrmNvsQtdMaxCobr: TFloatField
      FieldName = 'QtdMaxCobr'
    end
    object QrSrvLPrmNvsValFtrCobr: TFloatField
      FieldName = 'ValFtrCobr'
    end
    object QrSrvLPrmNvsUnidMedPaga: TIntegerField
      FieldName = 'UnidMedPaga'
    end
    object QrSrvLPrmNvsFrmRatPaga: TSmallintField
      FieldName = 'FrmRatPaga'
    end
    object QrSrvLPrmNvsQtdMaxPaga: TFloatField
      FieldName = 'QtdMaxPaga'
    end
    object QrSrvLPrmNvsValRatPaga: TFloatField
      FieldName = 'ValRatPaga'
    end
    object QrSrvLPrmNvsFrmFtrPaga: TSmallintField
      FieldName = 'FrmFtrPaga'
    end
  end
  object QrAgeEqiCfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ageeqicfg'
      'ORDER BY Nome')
    Left = 864
    Top = 1
    object QrAgeEqiCfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgeEqiCfgNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsAgeEqiCfg: TDataSource
    DataSet = QrAgeEqiCfg
    Left = 864
    Top = 49
  end
end
