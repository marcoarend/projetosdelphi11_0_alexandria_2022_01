unit SrvLPrmNvs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmSrvLPrmNvs = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrUnidMedCobr: TmySQLQuery;
    QrUnidMedCobrCodigo: TIntegerField;
    QrUnidMedCobrCodUsu: TIntegerField;
    QrUnidMedCobrSigla: TWideStringField;
    QrUnidMedCobrNome: TWideStringField;
    DsUnidMedCobr: TDataSource;
    QrUnidMedPaga: TmySQLQuery;
    QrUnidMedPagaCodigo: TIntegerField;
    QrUnidMedPagaCodUsu: TIntegerField;
    QrUnidMedPagaSigla: TWideStringField;
    QrUnidMedPagaNome: TWideStringField;
    DsUnidMedPaga: TDataSource;
    GroupBox3: TGroupBox;
    Panel3: TPanel;
    Label4: TLabel;
    EdUnidMedCobr: TdmkEditCB;
    EdSiglaCobr: TdmkEdit;
    CBUnidMedCobr: TdmkDBLookupComboBox;
    SBUnidMed: TSpeedButton;
    VUUnidMedCobr: TdmkValUsu;
    GroupBox4: TGroupBox;
    Panel5: TPanel;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    EdUnidMedPaga: TdmkEditCB;
    EdSiglaPaga: TdmkEdit;
    CBUnidMedPaga: TdmkDBLookupComboBox;
    VUUnidMedPaga: TdmkValUsu;
    EdValFtrCobr: TdmkEdit;
    LaValFtrCobr: TLabel;
    LaValRatPaga: TLabel;
    EdValRatPaga: TdmkEdit;
    EdQtdMaxCobr: TdmkEdit;
    LaQtdMaxCobr: TLabel;
    RGFrmRatPaga: TdmkRadioGroup;
    LaQtdMaxPaga: TLabel;
    EdQtdMaxPaga: TdmkEdit;
    RGFrmFtrCobr: TdmkRadioGroup;
    RGFrmFtrPaga: TdmkRadioGroup;
    Panel6: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdSiglaCobrChange(Sender: TObject);
    procedure EdSiglaCobrExit(Sender: TObject);
    procedure EdSiglaCobrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUnidMedCobrChange(Sender: TObject);
    procedure EdUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBUnidMedClick(Sender: TObject);
    procedure EdUnidMedPagaChange(Sender: TObject);
    procedure EdUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaPagaChange(Sender: TObject);
    procedure EdSiglaPagaExit(Sender: TObject);
    procedure EdSiglaPagaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGFrmFtrCobrClick(Sender: TObject);
    procedure RGFrmRatPagaClick(Sender: TObject);
    procedure RGFrmFtrPagaClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenSrvLOSOpePrm(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmSrvLPrmNvs: TFmSrvLPrmNvs;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnMySQLCuringa, UnGrade_PF, ModProd, AppListas;

{$R *.DFM}

procedure TFmSrvLPrmNvs.BtOKClick(Sender: TObject);
var
  Codigo, Controle, UnidMedCobr, FrmFtrCobr, FrmFtrPaga, UnidMedPaga,
  FrmRatPaga: Integer;
  QtdMaxCobr, ValFtrCobr, QtdMaxPaga, ValRatPaga: Double;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  //
  UnidMedCobr    := VUUnidMedCobr.ValueVariant;
  FrmFtrCobr     := RGFrmFtrCobr.ItemIndex;
  QtdMaxCobr     := EdQtdMaxCobr.ValueVariant;
  ValFtrCobr     := EdValFtrCobr.ValueVariant;
  //
  UnidMedPaga    := VUUnidMedPaga.ValueVariant;
  FrmFtrPaga     := RGFrmFtrPaga.ItemIndex;
  FrmRatPaga     := RGFrmRatPaga.ItemIndex;
  QtdMaxPaga     := EdQtdMaxPaga.ValueVariant;
  ValRatPaga     := EdValRatPaga.ValueVariant;
  //
  //
  if MyObjects.FIC(FrmFtrCobr < 1, RGFrmFtrCobr,
  'Informe o " Fator de cobran�a"') then
    Exit;
  //
  if MyObjects.FIC(FrmRatPaga < 1, RGFrmRatPaga,
  'Informe a "Forma de rateio"') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('srvlprmnvs', 'Controle', '', '', tsPos,
    ImgTipo.SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'srvlprmnvs', False, [
  'Codigo', 'QtdMaxCobr', 'ValFtrCobr',
  'QtdMaxPaga', 'ValRatPaga', 'UnidMedCobr',
  'FrmFtrCobr', 'UnidMedPaga', 'FrmRatPaga',
  'FrmFtrPaga'], [
  'Controle'], [
  Codigo, QtdMaxCobr, ValFtrCobr,
  QtdMaxPaga, ValRatPaga, UnidMedCobr,
  FrmFtrCobr, UnidMedPaga, FrmRatPaga,
  FrmFtrPaga], [
  Controle], True) then
  begin
    ReopenSrvLOSOpePrm(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      //
      //EdUnidMedPaga.ValueVariant := 0;
      //EdSiglaPaga.ValueVariant   := '';
      //CBUnidMedPaga.KeyValue      := Null;
      //RGFrmRatPaga.ItemIndex := ?
      EdValRatPaga.ValueVariant  := 0;
      EdQtdMaxPaga.ValueVariant  := 0;
      //EdUnidMedCobr.ValueVariant := 0;
      //EdSiglaCobr.ValueVariant   := '';
      //CBUnidMedCobr.KeyValue     := Null;
      EdValFtrCobr.ValueVariant  := 0;
      EdQtdMaxCobr.ValueVariant  := 0;
      //
      EdUnidMedCobr.SetFocus;
    end else Close;
  end;
end;

procedure TFmSrvLPrmNvs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLPrmNvs.CBUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCobr, CBUnidMedCobr, dmktfInteger);
end;

procedure TFmSrvLPrmNvs.CBUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedPaga, CBUnidMedPaga, dmktfInteger);
end;

procedure TFmSrvLPrmNvs.EdSiglaCobrChange(Sender: TObject);
begin
  if EdSiglaCobr.Focused then
    DmProd.PesquisaPorSigla(False, EdSiglaCobr, EdUnidMedCobr, CBUnidMedCobr);
  //
  LaQtdMaxCobr.Caption := 'M�ximo de ' + EdSiglaCobr.Text + ':';
end;

procedure TFmSrvLPrmNvs.EdSiglaCobrExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSiglaCobr, EdUnidMedCobr, CBUnidMedCobr);
end;

procedure TFmSrvLPrmNvs.EdSiglaCobrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCobr, CBUnidMedCobr, dmktfInteger)
end;

procedure TFmSrvLPrmNvs.EdSiglaPagaChange(Sender: TObject);
begin
  if EdSiglaPaga.Focused then
    DmProd.PesquisaPorSigla(False, EdSiglaPaga, EdUnidMedPaga, CBUnidMedPaga);
  //
  LaQtdMaxPaga.Caption := 'M�ximo de ' + EdSiglaPaga.Text + ':';
end;

procedure TFmSrvLPrmNvs.EdSiglaPagaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSiglaPaga, EdUnidMedPaga, CBUnidMedPaga);
end;

procedure TFmSrvLPrmNvs.EdSiglaPagaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedPaga, CBUnidMedPaga, dmktfInteger);
end;

procedure TFmSrvLPrmNvs.EdUnidMedCobrChange(Sender: TObject);
begin
  if not EdSiglaCobr.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMedCobr.ValueVariant, EdSiglaCobr);
end;

procedure TFmSrvLPrmNvs.EdUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCobr, CBUnidMedCobr, dmktfInteger);
end;

procedure TFmSrvLPrmNvs.EdUnidMedPagaChange(Sender: TObject);
begin
  if not EdSiglaPaga.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMedPaga.ValueVariant, EdSiglaPaga);
end;

procedure TFmSrvLPrmNvs.EdUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedPaga, CBUnidMedPaga, dmktfInteger);
end;

procedure TFmSrvLPrmNvs.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource   := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmSrvLPrmNvs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.ConfiguraRadioGroup(RGFrmFtrCobr, sPrmFatorValor, 3, 0);
  MyObjects.ConfiguraRadioGroup(RGFrmFtrPaga, sPrmFatorValor, 3, 0);
  MyObjects.ConfiguraRadioGroup(RGFrmRatPaga, sPrmFormaRateio, 3, 0);
  //
  UnDmkDAC_PF.AbreQuery(QrUnidMedCobr, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMedPaga, Dmod.MyDB);
end;

procedure TFmSrvLPrmNvs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLPrmNvs.ReopenSrvLOSOpePrm(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmSrvLPrmNvs.RGFrmFtrCobrClick(Sender: TObject);
begin
  LaValFtrCobr.Caption := RGFrmFtrCobr.Items[RGFrmFtrCobr.ItemIndex] + ':';
end;

procedure TFmSrvLPrmNvs.RGFrmFtrPagaClick(Sender: TObject);
begin
  LaValRatPaga.Caption := RGFrmFtrPaga.Items[RGFrmFtrPaga.ItemIndex] + ':';
end;

procedure TFmSrvLPrmNvs.RGFrmRatPagaClick(Sender: TObject);
begin
  LaValRatPaga.Caption := RGFrmRatPaga.Items[RGFrmRatPaga.ItemIndex] + ':';
end;

procedure TFmSrvLPrmNvs.SBUnidMedClick(Sender: TObject);
begin
  Grade_PF.CadastroEDefinicaoDeUnidMed(
    VUUnidMedCobr, EdUnidMedCobr, CBUnidMedCobr, QrUnidMedCobr);
end;

end.
