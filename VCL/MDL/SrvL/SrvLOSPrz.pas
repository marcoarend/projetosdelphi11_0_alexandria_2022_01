unit SrvLOSPrz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, dmkRadioGroup, dmkValUsu, dmkCheckBox, UnDmkEnums;

type
  TFmSrvLOSPrz = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    DBEdCliente: TDBEdit;
    DBEdNO_Cliente: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdCodigo: TDBEdit;
    QrPediPrzCab: TmySQLQuery;
    DsPediPrzCab: TDataSource;
    Label2: TLabel;
    EdCondicao: TdmkEditCB;
    CBCondicao: TdmkDBLookupComboBox;
    EdControle: TdmkEdit;
    Label1: TLabel;
    CkContinuar: TCheckBox;
    VuCondicao: TdmkValUsu;
    EdDescoPer: TdmkEdit;
    Label6: TLabel;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    CkEscolhido: TdmkCheckBox;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    SpeedButton6: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdCondicaoChange(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure EdDescoPerKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrInsUpd: TmySQLQuery;
    FDsCab: TDataSource;
    FValorIni, FValorFim, FPorcento: Double;
    FCodigo, FEmpresa: Integer;
    //
    procedure ReopenPedPrzCab();
  end;

  var
  FmSrvLOSPrz: TFmSrvLOSPrz;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, UnSrvL_PF, CalculaPercentual,
  UnMyVCLRef, Principal, UnGFat_Jan;

{$R *.DFM}

procedure TFmSrvLOSPrz.BtOKClick(Sender: TObject);
var
  Codigo, Controle, ConCodUsu, Condicao, Escolhido, Parcelas: Integer;
  DescoPer: Double;
begin
  Codigo     := FCodigo; // QrOSCabCodigo.Value;
  Controle   := EdControle.ValueVariant;
  ConCodUsu  := EdCondicao.ValueVariant;
  DescoPer   := EdDescoPer.ValueVariant;
  Escolhido  := Geral.BoolToInt(CkEscolhido.Checked);
  Parcelas   := QrPediPrzCabParcelas.Value;
  //
  if MyObjects.FIC(ConCodUsu = 0, EdCondicao,
  'Informe a Condi��o de Pagamento') then
    Exit;

  if not UMyMod.ObtemCodigoDeCodUsu(EdCondicao, Condicao, '') then
    Exit;

  Controle := UMyMod.BPGS1I32('srvlosprz', 'Controle', '', '',
    tsDef, ImgTipo.SQLType, Controle);

  //
  // Definir todas as condi��es da OS como n�o escolhidas
  if Escolhido = 1 then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosprz', False, [
    'Escolhido'], ['Codigo'], [0], [Codigo], True);
  end;
  //

  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'srvlosprz', False, [
  'Codigo', 'Condicao', 'DescoPer', 'Parcelas', 'Escolhido'], [
  'Controle'], [
  Codigo, Condicao, DescoPer, Parcelas, Escolhido
  ], [
  Controle], True) then
  begin
    SrvL_PF.AtualizaTotaisSrvLOSCab(Codigo);
    if FQrInsUpd <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(FQrInsUpd, Dmod.MyDB);
      FQrInsUpd.Locate('Controle', Controle, []);
    end;
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      EdControle.ValueVariant := 0;
      EdCondicao.ValueVariant := 0;
      CBCondicao.KeyValue := 0;
    end else
      Close;
  end;
end;

procedure TFmSrvLOSPrz.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLOSPrz.EdCondicaoChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    EdDescoPer.ValueVariant := QrPediPrzCabMaxDesco.Value;
end;

procedure TFmSrvLOSPrz.EdDescoPerKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
const
  CasasValI = 2;
  CasasValF = 2;
  CasasPerc = 6;
var
  ValorValI, ValorValF, ValorPerc: Double;
begin
  if Key = VK_F4 then
  begin
    ValorValI := FValorIni;
    ValorValF := 0;
    ValorPerc := EdDescoPer.ValueVariant;
    if MyVCLRef.CalculaPercentual(TFmCalculaPercentual, FmCalculaPercentual,
    fcpercDesconto,
    CasasValI, CasasValF, CasasPerc,
    ValorValI, ValorValF, ValorPerc) then
      EdDescoPer.ValueVariant := ValorPerc;
  end;
end;

procedure TFmSrvLOSPrz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  DBEdCodigo.DataSource     := FDsCab;
  DBEdNome.DataSource       := FDsCab;
  DBEdCliente.DataSource    := FDsCab;
  DBEdNO_Cliente.DataSource := FDsCab;
end;

procedure TFmSrvLOSPrz.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLOSPrz.ReopenPedPrzCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzCab, Dmod.MyDB, [
  'SELECT ppc.MaxDesco, ppc.Parcelas, ',
  'ppc.Codigo, ppc.CodUsu, ppc.Nome ',
  'FROM pediprzcab ppc ',
  'LEFT JOIN pediprzemp ppe ON ppe.Codigo=ppc.Codigo ',
  'LEFT JOIN enticliint eci ON eci.CodEnti=ppe.Empresa ',
  //'WHERE eci.CodCliInt=' + Geral.FF0(QrOSCabEmpresa.Value),
  'WHERE eci.CodEnti=' + Geral.FF0(FEmpresa),
  'ORDER BY ppc.Nome ',
  ' ']);
end;

procedure TFmSrvLOSPrz.SpeedButton6Click(Sender: TObject);
begin
  GFat_Jan.InsereEDefinePediPrzCab(EdCondicao.ValueVariant, EdCondicao,
    CBCondicao, QrPediPrzCab);
end;

end.
