unit SrvLOSCabImp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass, frxDBSet;

type
  TFmSrvLOSCabImp = class(TForm)
    frxDsSrvLOSCab: TfrxDBDataset;
    frxDsSrvLOSOpeGrn: TfrxDBDataset;
    frxLOC_SERVI_019_01: TfrxReport;
    procedure frxLOC_SERVI_019_01GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
    FEmpresa, FCliente: Integer;
    //
    procedure ImprimeOSCab();
  end;

var
  FmSrvLOSCabImp: TFmSrvLOSCabImp;

implementation

uses SrvLOSCab, UnMyObjects, ModuleGeral;

{$R *.dfm}

procedure TFmSrvLOSCabImp.frxLOC_SERVI_019_01GetValue(const VarName: string;
  var Value: Variant);
var
  Linha1, linha2, Linha3: String;
begin
  if VarName = 'VARF_ENDERECO_EMP' then
  begin
    DModG.ObtemEnderecoEtiqueta3Linhas(FEmpresa, Linha1, linha2, Linha3);
    Value := Linha1 + sLineBreak + Linha2 + sLineBreak + Linha3;
  end else
  if VarName = 'VARF_ENDERECO_CLI' then
  begin
    DModG.ObtemEnderecoEtiqueta3Linhas(FCliente, Linha1, linha2, Linha3);
    Value := Linha1 + sLineBreak + Linha2 + sLineBreak + Linha3;
  end else
end;

procedure TFmSrvLOSCabImp.ImprimeOSCab();
begin
  MyObjects.frxDefineDataSets(frxLOC_SERVI_019_01, [
  frxDsSrvLOSCab,
  frxDsSrvLOSOpeGrn
  ]);
  MyObjects.frxMostra(frxLOC_SERVI_019_01, 'OS');
end;

end.
