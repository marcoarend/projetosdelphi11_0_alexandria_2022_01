unit UnSrvL_PF;


interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  UnDmkProcFunc, UnProjGroup_Vars;

type
  TUnSrvL_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AtualizaTotaisSrvLOSOpeAge(Conta: Integer);
    procedure AtualizaTotaisSrvOSOpeGrn(SrvLosCab, SrvOSOpeGrn: Integer);
    procedure AtualizaTotaisSrvLOSCab(SrvLosCab: Integer);
    procedure RecalculaValoresDeCondicoes(Codigo: Integer);
    procedure CriaItensDeAgentes_Old(SrvLOSCab, SrvLOSOpeGrn, AgeEqiCab,
              AgeEqiCfg: Integer; DataExeIni: TDateTime);
    function  CriaItensDeAgentes_Ger(SrvLOSCab, SrvLOSOpeGrn, AgeEqiCab,
              AgeEqiCfg: Integer; DataExeIni: TDateTime): Integer;
    function  CriaItensDeAgentes_New(SrvLOSCab, SrvLOSOpeGrn, (*AgeEqiCab,*)
              AgeEqiCfg: Integer; DataExeIni: TDateTime; Agentes: array of
              Integer; Lider: Integer): Integer;
    //procedure DistribuiValoresAgentesSrvLOSCab(SrvLOSCab: Integer);
    procedure DistribuiValoresAgentesSrvLOSOpeGrn(SrvLOSOpeGrn, GruAddSeq:
              Integer);
    //
    procedure MostraFormAgeEntCad(Codigo: Integer);
    procedure MostraFormSrvLCad(Codigo: Integer);
    procedure MostraFormSrvLFatCad(Codigo: Integer);
    procedure MostraFormSrvLFatPrz(SQLType: TSQLType; QrOSCab,
              QrFatPrz: TmySQLQuery; ValorIni, ValorFim, Porcento: Double;
              Codigo, Empresa: Integer; DsCab: TDataSource);
    procedure MostraFormSrvLHonCad(Codigo: Integer);
    procedure MostraFormSrvLImp(Guia: Integer);
    procedure MostraFormSrvLOSCab(Codigo: Integer);
    procedure MostraFormSrvLOSPrz(SQLType: TSQLType; QrOSCab,
              QrOsPrz: TmySQLQuery; ValorIni, ValorFim, Porcento: Double;
              Codigo, Empresa: Integer; DsCab: TDataSource);
    procedure MostraFormSrvLPOCab(Codigo: Integer);
    procedure MostraFormSrvLPrmCad(Codigo: Integer);
    procedure MostraFormStatusOSs();

    //
    function  ObtemProximoGruAddSeq_Age(SrvLOSOpeGrn: Integer): Integer;
    function  PagamentoHonorarioFuncionario(FatGru: Integer;
              Entidade, FatID: Integer; FatNum: Double;
              Terceiro: Integer; DataAbriu, DataEncer: TDateTime;
              //EMP_IDDuplicata, NumeroNF: Integer; SerieNF: String;
              CartEmis, TipoCart, (*Conta*)Genero, CondicaoPG, Represen: Integer;
              //TipoFiscal: TTipoFiscal;
              //EMP_FaturaDta: TDataFatura; Financeiro: TTipoFinanceiro;
              ValorServico: Double;
              //Associada: Integer = 0; AFP_Sit: Integer = 0;
              //AFP_Per: Double = 0; ASS_CtaFaturas: Integer = 0; ASS_IDDuplicata: Integer = 0
              Encerra: Boolean; QrIts: TmySQLQuery; DataFat: TDateTime;
              AskIns: Boolean): Boolean;
    function  FaturamentosQuitados(TabLctA: String; FatID, FatNum: Integer): Integer;
    procedure ReopenLctFatRef(QrLctFatRef: TmySQLQuery; Controle,
              Conta, FatID: Integer);
    procedure ReopenLctFatRefTotais(QrLctFatRef: TmySQLQuery; Controle,
              Conta, FatID: Integer);
    procedure ReopenXPrz(QrXPrz: TmySQLQuery; Tabela: String; Codigo, Controle:
              Integer);
    procedure TotalizaSrvlFatCab(Codigo: Integer);
    procedure TotalizaSrvlHonCab(Codigo: Integer);
    procedure VerificaNovosAgentesSrvlHon(SrvlHonCab: Integer);
  end;

var
  SrvL_PF: TUnSrvL_PF;

implementation

uses MyDBCheck, DmkDAC_PF, Module, ModuleGeral, UMySQLModule, ModuleFatura,
  SrvLCad, (*SrvLPre,*) SrvLOSCab, SrvLPrmCad, SrvLFatCab, SrvLOsPrz, SrvLImp,
  SrvLFatPrz, SrvLFatEmi, SrvLHonCab, AgeEntCad, UnMyObjects, SrvlOSCabImp,
  CadAnyStatus;

{ TUnSrvL_PF }

function  TUnSrvL_PF.ObtemProximoGruAddSeq_Age(SrvLOSOpeGrn: Integer): Integer;
var
  Qry: TmySQLQuery;
  GruAddSeq: Integer;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(GruAddSeq) GruAddSeq ',
    'FROM srvlosopeage ',
    'WHERE Controle=' + Geral.FF0(SrvLOSOpeGrn),
    '']);
    GruAddSeq := Qry.FieldByName('GruAddSeq').AsInteger;
    Result := GruAddSeq + 1;
    //
(*
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM srvlosopeage ',
    'WHERE Controle=' + Geral.FF0(Controle),
    'AND GruAddSeq=0',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      // Parei aqui
      //
      Qry.Next;
    end;
*)
  finally
    Qry.Free;
  end;
end;

procedure TUnSrvL_PF.AtualizaTotaisSrvLOSCab(SrvLosCab: Integer);
var
  ValTotCobr, ValTotDesp, ValRatPaga, ValTotPaga: Double;
  Codigo: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(ValTotCobr) ValTotCobr,  ',
    'SUM(ValRatPaga) ValRatPaga, SUM(ValTotDesp) ValTotDesp, ',
    'SUM(ValTotPaga) ValTotPaga ',
    'FROM srvlosopegrn ',
    'WHERE Codigo=' + Geral.FF0(SrvLosCab),
    ' ']);
    //
    ValTotCobr := Qry.FieldByName('ValTotCobr').AsFloat;
    ValRatPaga := Qry.FieldByName('ValRatPaga').AsFloat;
    ValTotDesp := Qry.FieldByName('ValTotDesp').AsFloat;
    ValTotPaga := Qry.FieldByName('ValTotPaga').AsFloat;
    //
    Codigo := SrvLosCab;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvloscab', False, [
    'ValTotCobr', 'ValTotDesp', 'ValRatPaga',
    'ValTotPaga'], [
    'Codigo'], [
    ValTotCobr, ValTotDesp, ValRatPaga,
    ValTotPaga], [
    Codigo], True);
    //
  finally
    Qry.Free;
  end;
end;

procedure TUnSrvL_PF.AtualizaTotaisSrvLOSOpeAge(Conta: Integer);
var
  Qry: TmySQLQuery;
  ValPremio: Double;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(ValPremio) ValPremio ',
    'FROM srvlosopecpa ',
    'WHERE Conta=' + Geral.FF0(Conta),
    '']);
    ValPremio := Qry.FieldByName('ValPremio').AsFloat;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopeage', False, [
    'ValPremio'], [
    'Conta'], [
    ValPremio], [
    Conta], True) then
    begin
      //
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnSrvL_PF.AtualizaTotaisSrvOSOpeGrn(SrvLosCab, SrvOSOpeGrn: Integer);
var
  ValTotDesp, ValTotPaga: Double;
  Controle: Integer;
  Qry: TmySQLQuery;
  DtaLstCPA: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Debito) ValTotDesp ',
    'FROM srvlosopedsp ',
    'WHERE Controle=' + Geral.FF0(SrvOSOpeGrn),
    ' ']);
    ValTotDesp := Qry.FieldByName('ValTotDesp').AsFloat;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(ValPremio) ValTotPaga ',
    'FROM srvlosopeage ',
    'WHERE Controle=' + Geral.FF0(SrvOSOpeGrn),
    ' ']);
    ValTotPaga := Qry.FieldByName('ValTotPaga').AsFloat;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(DtaCompet) DtaCompet ',
    'FROM srvlosopecpa ',
    'WHERE Controle=' + Geral.FF0(SrvOSOpeGrn),
    ' ']);
    DtaLstCPA := Geral.FDT(Qry.FieldByName('DtaCompet').AsDateTime, 109);
    //
    Controle := SrvOSOpeGrn;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopegrn', False, [
    'ValTotDesp', 'ValTotPaga', 'DtaLstCPA'], [
    'Controle'], [
    ValTotDesp, ValTotPaga, DtaLstCPA], [
    Controle], True) then
      AtualizaTotaisSrvLOSCab(SrvLosCab);
  finally
    Qry.Free;
  end;
end;

procedure TUnSrvL_PF.CriaItensDeAgentes_Old(SrvLOSCab, SrvLOSOpeGrn, AgeEqiCab,
  AgeEqiCfg: Integer; DataExeIni: TDateTime);
var
  Responsa, Agente, Conta, Controle, Codigo: Integer;
  Qry, Qr2: TmySQLQuery;
var
  DtaCompet, HrInnDay, HrOutMid, HrInnMid, HrOutDay: String;
  Nivel4, TemInterv: Integer;
  //FatorPrm, ValPremio: Double;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qr2 := TmySQLQuery.Create(Dmod);
    try
      Controle  := SrvLOSOpeGrn;
      Codigo    := SrvLOSCab;
      DtaCompet := Geral.FDT(DataExeIni, 1);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qr2, Dmod.MyDB, [
      'SELECT *',
      'FROM AgeEqiCfg ',
      'WHERE Codigo=' + Geral.FF0(AgeEqiCfg),
      '']);
      HrInnDay  := Geral.FDT(Qr2.FieldByName('HrInnDay').AsDateTime, 100);
      HrOutMid  := Geral.FDT(Qr2.FieldByName('HrOutMid').AsDateTime, 100);
      HrInnMid  := Geral.FDT(Qr2.FieldByName('HrInnMid').AsDateTime, 100);
      HrOutDay  := Geral.FDT(Qr2.FieldByName('HrOutDay').AsDateTime, 100);
      TemInterv := Qr2.FieldByName('TemInterv').AsInteger;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT aei.Controle, aei.Entidade, aei.EhLider',
      'FROM ageeqiits aei ',
      'WHERE aei.Codigo=' + Geral.FF0(AgeEqiCab),
      '']);
      Qry.First;
      while not Qry.Eof do
      begin
        Responsa := Qry.FieldByName('EhLider').AsInteger;
        Agente   := Qry.FieldByName('Entidade').AsInteger;
        //
        Conta := UMyMod.BPGS1I32('srvlosopeage', 'Conta', '', '', tsPos, stIns, 0);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'srvlosopeage', False, [
        'Codigo', 'Controle',
        'Agente', 'Responsa'], [
        'Conta'], [
        Codigo, Controle,
        Agente, Responsa], [
        Conta], True) then
        begin
          Nivel4         := UMyMod.BPGS1I32('srvlosopecpa', 'Nivel4', '', '', tsPos, stIns, 0);
          //FatorPrm       := ;
          //ValPremio      := ;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'srvlosopecpa', False, [
          'Codigo', 'Controle', 'Conta',
          'DtaCompet', 'HrInnDay', 'HrOutMid',
          'HrInnMid', 'HrOutDay', 'TemInterv'(*,
          'FatorPrm', 'ValPremio'*)], [
          'Nivel4'], [
          Codigo, Controle, Conta,
          DtaCompet, HrInnDay, HrOutMid,
          HrInnMid, HrOutDay, TemInterv(*,
          FatorPrm, ValPremio*)], [
          Nivel4], True) then ;
        end;
        //
        Qry.Next;
      end;
    finally
      Qr2.Free;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnSrvL_PF.CriaItensDeAgentes_Ger(SrvLOSCab, SrvLOSOpeGrn, AgeEqiCab,
  AgeEqiCfg: Integer; DataExeIni: TDateTime): Integer;
var
  N, Lider: Integer;
  Agentes: array of Integer;
  Qry: TmySQLQuery;
begin
  Result := 0;
  Lider := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT aei.Controle, aei.Entidade, aei.EhLider',
    'FROM ageeqiits aei ',
    'WHERE aei.Codigo=' + Geral.FF0(AgeEqiCab),
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      N := N + 1;
      SetLength(Agentes, N);
      Agentes[N-1] := DModG.QrSelCodsNivel1.Value;
      if Qry.FieldByName('EhLider').AsInteger = 1 then
        Lider := Agentes[N-1];
      //
      Qry.Next;
    end;
    Result := CriaItensDeAgentes_New(SrvLOSCab, SrvLOSOpeGrn, (*AgeEqiCab,*)
    AgeEqiCfg, DataExeIni, Agentes, Lider);
  finally
    Qry.Free;
  end;
end;

function TUnSrvL_PF.CriaItensDeAgentes_New(SrvLOSCab, SrvLOSOpeGrn, (*AgeEqiCab,*)
  AgeEqiCfg: Integer; DataExeIni: TDateTime; Agentes: array of Integer;
  Lider: Integer): Integer;
var
  Responsa, Agente, Conta, Controle, Codigo, GruAddSeq: Integer;
  Qry, Qr2: TmySQLQuery;
var
  DtaCompet, HrInnDay, HrOutMid, HrInnMid, HrOutDay: String;
  Nivel4, TemInterv, I: Integer;
  //FatorPrm, ValPremio: Double;
begin
  GruAddSeq := ObtemProximoGruAddSeq_Age(SrvLOSOpeGrn);
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qr2 := TmySQLQuery.Create(Dmod);
    try
      Controle  := SrvLOSOpeGrn;
      Codigo    := SrvLOSCab;
      DtaCompet := Geral.FDT(DataExeIni, 1);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qr2, Dmod.MyDB, [
      'SELECT *',
      'FROM AgeEqiCfg ',
      'WHERE Codigo=' + Geral.FF0(AgeEqiCfg),
      '']);
      HrInnDay  := Geral.FDT(Qr2.FieldByName('HrInnDay').AsDateTime, 100);
      HrOutMid  := Geral.FDT(Qr2.FieldByName('HrOutMid').AsDateTime, 100);
      HrInnMid  := Geral.FDT(Qr2.FieldByName('HrInnMid').AsDateTime, 100);
      HrOutDay  := Geral.FDT(Qr2.FieldByName('HrOutDay').AsDateTime, 100);
      TemInterv := Qr2.FieldByName('TemInterv').AsInteger;
      //
(*
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT aei.Controle, aei.Entidade, aei.EhLider',
      'FROM ageeqiits aei ',
      'WHERE aei.Codigo=' + Geral.FF0(AgeEqiCab),
      '']);
      Qry.First;
      while not Qry.Eof do
*)
      for I := Low(Agentes) to High(Agentes) do
      begin
        //Responsa := Qry.FieldByName('EhLider').AsInteger;
        //Agente   := Qry.FieldByName('Entidade').AsInteger;
        Agente   := Agentes[I];
        //Verificar se j� nao est�!!
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Agente ',
        'FROM srvlosopeage ',
        'WHERE Controle=' + Geral.FF0(Controle),
        'AND Agente=' + Geral.FF0(Agente),
        '']);
        if Qry.RecordCount = 0 then
        begin
          Responsa := dmkPF.EscolhaDe2Int(Agente = Lider, 1, 0);
          //
          Conta := UMyMod.BPGS1I32('srvlosopeage', 'Conta', '', '', tsPos, stIns, 0);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'srvlosopeage', False, [
          'Codigo', 'Controle', 'Agente',
          'Responsa', 'GruAddSeq'], [
          'Conta'], [
          Codigo, Controle, Agente,
          Responsa, GruAddSeq], [
          Conta], True) then
          begin
            Nivel4         := UMyMod.BPGS1I32('srvlosopecpa', 'Nivel4', '', '', tsPos, stIns, 0);
            //FatorPrm       := ;
            //ValPremio      := ;
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'srvlosopecpa', False, [
            'Codigo', 'Controle', 'Conta',
            'DtaCompet', 'HrInnDay', 'HrOutMid',
            'HrInnMid', 'HrOutDay', 'TemInterv'(*,
            'FatorPrm', 'ValPremio'*)], [
            'Nivel4'], [
            Codigo, Controle, Conta,
            DtaCompet, HrInnDay, HrOutMid,
            HrInnMid, HrOutDay, TemInterv(*,
            FatorPrm, ValPremio*)], [
            Nivel4], True) then ;
          end;
        end;
      end;
    finally
      Qr2.Free;
    end;
  finally
    Qry.Free;
  end;
  Result := GruAddSeq;
end;

{
procedure TUnSrvL_PF.DistribuiValoresAgentesSrvLOSCab(SrvLOSCab: Integer);
var
  Qry: TmySQLQuery;
  SrvLOSOpeGrn: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM srvlosopegrn ',
    'WHERE Codigo=' + Geral.FF0(SrvLOSCab),
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      SrvLOSOpeGrn := Qry.FieldByName('Controle').AsInteger;
      DistribuiValoresAgentesSrvLOSOpeGrn(SrvLOSOpeGrn);
      //
      Qry.Next;
    end;
    //
  finally
    Qry.Free;
  end;
end;
}

procedure TUnSrvL_PF.DistribuiValoresAgentesSrvLOSOpeGrn(SrvLOSOpeGrn,
  GruAddSeq: Integer);
var
  Qry: TmySQLQuery;
  ValRatPaga, ValPremio, TotalFator: Double;
  FrmRatPaga: TPrmFormaRateio;
  Agentes, SrvLOSCab: Integer;
  EmpreitaIncerta: Boolean;
begin
  if SrvLOSOpeGrn = 0 then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM srvlosopegrn ',
    'WHERE Controle=' + Geral.FF0(SrvLOSOpeGrn),
    '']);
    //
    EmpreitaIncerta := Qry.FieldByName('WrnLctDay').AsInteger = 1;
    SrvLOSCab       := Qry.FieldByName('Codigo').AsInteger;
    if (not EmpreitaIncerta) or (GruAddSeq = 1) then
    begin
      ValRatPaga := Qry.FieldByName('ValRatPaga').AsFloat;
      FrmRatPaga := TPrmFormaRateio(Qry.FieldByName('FrmRatPaga').AsInteger);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(FatorPrm) TotalFator  ',
      //'FROM srvlosopeage ',
      'FROM srvlosopecpa ',
      'WHERE Controle=' + Geral.FF0(SrvLOSOpeGrn),
      '']);
      //
      TotalFator := Qry.FieldByName('TotalFator').AsFloat;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      //'FROM srvlosopeage ',
      'FROM srvlosopecpa ',
      'WHERE Controle=' + Geral.FF0(SrvLOSOpeGrn),
      '']);
      //
      Agentes := Qry.RecordCount;
      if (Agentes > 0) and (TotalFator > 0) then
      begin
        case FrmRatPaga of
          //pfrIndefinido=0,
          (*1*)pfrValorTotal:
          begin
            //ValPremio := ValRatPaga / Agentes;
            ValPremio := ValRatPaga / TotalFator;
          end;
          (*2*)pfrValorPorPessoa:
          begin
            //ValPremio := ValRatPaga;
            ValPremio := ValRatPaga;
          end;
        end;
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        //'UPDATE srvlosopeage SET ',
        'UPDATE srvlosopecpa SET ',
        'ValPremio=FatorPrm * ' + Geral.FFT_Dot(ValPremio, 6, siNegativo),
        'WHERE Controle=' + Geral.FF0(SrvLOSOpeGrn),
        // Ainda nao alterado manualmente!
        'AND ValEverAut=1 ',
        '']);
        //
        //Atualizar srvlosopeage por QrSum
        Qry.First;
        while not Qry.Eof do
        begin
          AtualizaTotaisSrvLOSOpeAge(Qry.FieldByName('Conta').AsInteger);
          //
          Qry.Next;
        end;
        AtualizaTotaisSrvOSOpeGrn(SrvLOSCab, SrvLOSOpeGrn);
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnSrvL_PF.MostraFormAgeEntCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmAgeEntCad, FmAgeEntCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmAgeEntCad.LocCod(Codigo, Codigo);
    //
    FmAgeEntCad.ShowModal;
    FmAgeEntCad.Destroy;
  end;
end;

procedure TUnSrvL_PF.MostraFormSrvLCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmSrvLCad, FmSrvLCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmSrvLCad.LocCod(Codigo, Codigo);
    //
    FmSrvLCad.ShowModal;
    FmSrvLCad.Destroy;
  end;
end;

procedure TUnSrvL_PF.MostraFormSrvLFatCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmSrvLFatCab, FmSrvLFatCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmSrvLFatCab.LocCod(Codigo, Codigo);
    //
    FmSrvLFatCab.ShowModal;
    FmSrvLFatCab.Destroy;
  end;
end;

procedure TUnSrvL_PF.MostraFormSrvLFatPrz(SQLType: TSQLType; QrOSCab,
  QrFatPrz: TmySQLQuery; ValorIni, ValorFim, Porcento: Double; Codigo,
  Empresa: Integer; DsCab: TDataSource);
begin
  if DBCheck.CriaFm(TFmSrvLFatPrz, FmSrvLFatPrz, afmoNegarComAviso) then
  begin
    FmSrvLFatPrz.ImgTipo.SQLType := SQLType;
    FmSrvLFatPrz.FQrInsUpd := QrFatPrz;
    //
    FmSrvLFatPrz.FCodigo   := Codigo;
    FmSrvLFatPrz.FEmpresa  := Empresa;
    FmSrvLFatPrz.FDsCab    := DsCab;
    FmSrvLFatPrz.ReopenPedPrzCab();
    //
    FmSrvLFatPrz.FValorIni := ValorIni;
    FmSrvLFatPrz.FValorFim := ValorFim;
    FmSrvLFatPrz.FPorcento := Porcento;
    //
    if SQLType = stUpd then
    begin
      FmSrvLFatPrz.EdControle.ValueVariant := QrFatPrz.FieldByName('Controle').AsInteger;
      FmSrvLFatPrz.EdCondicao.ValueVariant := QrFatPrz.FieldByName('Condicao').AsInteger;
      FmSrvLFatPrz.CBCondicao.KeyValue     := QrFatPrz.FieldByName('Condicao').AsInteger;
      FmSrvLFatPrz.EdDescoPer.ValueVariant := QrFatPrz.FieldByName('DescoPer').AsFloat;
      FmSrvLFatPrz.CkEscolhido.Checked     := QrFatPrz.FieldByName('Escolhido').AsInteger = 1;
    end else
      FmSrvLFatPrz.CkEscolhido.Checked := True;
    FmSrvLFatPrz.ShowModal;
    FmSrvLFatPrz.Destroy;
    //
    RecalculaValoresDeCondicoes(QrOSCab.FieldByName('Codigo').AsInteger);
    //
    //FQrOSCab.Close;
    UnDmkDAC_PF.AbreQuery(QrOSCab, Dmod.MyDB);
  end;
end;

procedure TUnSrvL_PF.MostraFormSrvLHonCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmSrvLHonCab, FmSrvLHonCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmSrvLHonCab.LocCod(Codigo, Codigo);
    //
    FmSrvLHonCab.ShowModal;
    FmSrvLHonCab.Destroy;
  end;
end;

procedure TUnSrvL_PF.MostraFormSrvLImp(Guia: Integer);
begin
  if DBCheck.CriaFm(TFmSrvLImp, FmSrvLImp, afmoNegarComAviso) then
  begin
    FmSrvLImp.PCRelatorio.ActivePageIndex := Guia;
    DModG.SelecionaEmpresaSeUnica(FmSrvLImp.EdEmpresa, FmSrvLImp.CBEmpresa);
    //
    FmSrvLImp.ShowModal;
    FmSrvLImp.Destroy;
  end;
end;

procedure TUnSrvL_PF.MostraFormSrvLOSCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmSrvLOSCab, FmSrvLOSCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmSrvLOSCab.LocCod(Codigo, Codigo);
    //
    FmSrvLOSCab.ShowModal;
    FmSrvLOSCab.Destroy;
  end;
end;

procedure TUnSrvL_PF.MostraFormSrvLOSPrz(SQLType: TSQLType; QrOSCab,
  QrOsPrz: TmySQLQuery; ValorIni, ValorFim, Porcento: Double;
  Codigo, Empresa: Integer; DsCab: TDataSource);
begin
  if DBCheck.CriaFm(TFmSrvLOsPrz, FmSrvLOsPrz, afmoNegarComAviso) then
  begin
    FmSrvLOsPrz.ImgTipo.SQLType := SQLType;
    FmSrvLOsPrz.FQrInsUpd := QrOsPrz;
    //
    FmSrvLOsPrz.FCodigo   := Codigo;
    FmSrvLOsPrz.FEmpresa  := Empresa;
    FmSrvLOsPrz.FDsCab    := DsCab;
    FmSrvLOsPrz.ReopenPedPrzCab();
    //
    FmSrvLOsPrz.FValorIni := ValorIni;
    FmSrvLOsPrz.FValorFim := ValorFim;
    FmSrvLOsPrz.FPorcento := Porcento;
    //
    if SQLType = stUpd then
    begin
      FmSrvLOsPrz.EdControle.ValueVariant := QrOsPrz.FieldByName('Controle').AsInteger;
      FmSrvLOsPrz.EdCondicao.ValueVariant := QrOsPrz.FieldByName('Condicao').AsInteger;
      FmSrvLOsPrz.CBCondicao.KeyValue := QrOsPrz.FieldByName('Condicao').AsInteger;
      FmSrvLOsPrz.EdDescoPer.ValueVariant := QrOsPrz.FieldByName('DescoPer').AsFloat;
      FmSrvLOsPrz.CkEscolhido.Checked := QrOsPrz.FieldByName('Escolhido').AsInteger = 1;
    end else
    begin
      FmSrvLOsPrz.CkEscolhido.Checked := True;
    end;
    FmSrvLOsPrz.ShowModal;
    FmSrvLOsPrz.Destroy;
    //
    RecalculaValoresDeCondicoes(QrOSCab.FieldByName('Codigo').AsInteger);
    //
    //FQrOSCab.Close;
    UnDmkDAC_PF.AbreQuery(QrOSCab, Dmod.MyDB);
  end;
end;

procedure TUnSrvL_PF.MostraFormSrvLPOCab(Codigo: Integer);
begin
{
  if DBCheck.CriaFm(TFmSrvLPOCab, FmSrvLPOCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmSrvLPOCab.LocCod(Codigo, Codigo);
    //
    FmSrvLPOCab.ShowModal;
    FmSrvLPOCab.Destroy;
  end;
}
end;

procedure TUnSrvL_PF.MostraFormSrvLPrmCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmSrvLPrmCad, FmSrvLPrmCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmSrvLPrmCad.LocCod(Codigo, Codigo);
    //
    FmSrvLPrmCad.ShowModal;
    FmSrvLPrmCad.Destroy;
  end;
end;

procedure TUnSrvL_PF.MostraFormStatusOSs();
begin
  if DBCheck.CriaFm(TFmCadAnyStatus, FmCadAnyStatus, afmoNegarComAviso,
  'OSS-STATU-001 :: Status de OSs') then
  begin
    FmCadAnyStatus.TbCad.TableName := 'estatusoss';
    FmCadAnyStatus.ShowModal;
    FmCadAnyStatus.Destroy;
  end;
end;

function TUnSrvL_PF.PagamentoHonorarioFuncionario(FatGru: Integer;
              Entidade, FatID: Integer; FatNum: Double;
              Terceiro: Integer; DataAbriu, DataEncer: TDateTime;
              //EMP_IDDuplicata, NumeroNF: Integer; SerieNF: String;
              CartEmis, TipoCart, (*Conta*)Genero, CondicaoPG, Represen: Integer;
              //TipoFiscal: TTipoFiscal;
              //EMP_FaturaDta: TDataFatura; Financeiro: TTipoFinanceiro;
              ValorServico: Double;
              //Associada: Integer = 0; AFP_Sit: Integer = 0;
              //AFP_Per: Double = 0; ASS_CtaFaturas: Integer = 0; ASS_IDDuplicata: Integer = 0
              Encerra: Boolean; QrIts: TmySQLQuery; DataFat: TDateTime;
              AskIns: Boolean): Boolean;
const
  TipoFatura   = TTipoFiscal.tfatServico;
  FaturaDta    = TDataFatura.dfPreDefinido;
  Financeiro   = TTipoFinanceiro.tfinDeb;
  IDDuplicata  = 0;
  NumeroNF     = 0;
  SerieNF      = '';
  Associada    = 0;
  AFP_Sit      = 0;
  AFP_Per      = 0.00;
  ASS_CtaFaturas  = 0;
  ASS_IDDuplicata = 0;
var
  ValTotal: Double;
  function AlteraOSCab(): Boolean;
  begin
    Result := True;
(*
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlfatcab', False, [
    'Estatus', 'ValLiqOkFat', 'ValLiqToFat',
    'ValDesToFat', 'DtaFimFat', 'ValOutToFat',
    'CondicaoPg', 'CartEmis', 'Genero'], [
    'Codigo'], [
    Estatus, ValLiqOkFat, ValLiqToFat,
    ValDesToFat, DtaFimFat, ValOutToFat,
    CondicaoPg, CartEmis, Genero], [
    Codigo], True);
*)

  end;
begin
  Result   := False;
  ValTotal := ValorServico;
  //
  if MyObjects.FIC(CondicaoPG = 0, nil, 'Informe a condi��o de pagamento!') then Exit;
  if MyObjects.FIC(CartEmis = 0, nil, 'Informe a carteira!') then Exit;
  if MyObjects.FIC(Genero = 0, nil, 'Informe a conta (do plano de contas)!') then Exit;
  if ValTotal < 0.01 then
  begin
    //if FEncerra then
    if Encerra then
    begin
      if Geral.MB_Pergunta('Valor inv�lido para faturamento!' + sLineBreak +
      'Deseja encerrar assim mesmo?') <> ID_YES then
        Exit;
    end else
    begin
      Geral.MB_Aviso('Valor inv�lido para faturamento!');
      Exit;
    end;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    if AlteraOSCab() then
    begin
      //
      DmFatura.EmiteFaturas(FatGru, FatID, Entidade, FatID, FatNum, Terceiro,
        DataAbriu, DataEncer, IDDuplicata, NumeroNF, SerieNF, CartEmis,
        TipoCart, Genero, CondicaoPG, Represen, TipoFatura, FaturaDta,
        Financeiro, ValTotal, Associada, AFP_Sit, AFP_Per, ASS_CtaFaturas,
        ASS_IDDuplicata, DataFat, AskIns, False);


      //
{ TODO : Ver se um dia vai fazer }
(*
      QuestaoTyp := qagOSBgstrl;
      QuestaoCod := Codigo;
      DmModAgenda.AtualizaStatusTodosEventosMesmoTipoCodigo(Estatus,
        QuestaoTyp, QuestaoCod);
*)
      if QrIts <> nil then
      begin
        UMyMod.AbreQuery(QrIts, Dmod.MyDB);
        //FQrIts.Locate('Controle', Controle, []);
      end;
      Result := True;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnSrvL_PF.RecalculaValoresDeCondicoes(Codigo: Integer);
begin
  //
end;

procedure TUnSrvL_PF.ReopenLctFatRef(QrLctFatRef: TmySQLQuery; Controle,
  Conta, FatID: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLctFatRef, Dmod.MyDB, [
  'SELECT lfr.* ',
  'FROM lctfatref lfr ',
  'WHERE lfr.Controle=' + Geral.FF0(Controle),
  'AND lfr.FatID IN (0, '+ Geral.FF0(FatID) +') ',
  '']);
  //
  QrLctFatRef.Locate('Conta', Conta, []);
end;

procedure TUnSrvL_PF.ReopenLctFatRefTotais(QrLctFatRef: TmySQLQuery; Controle,
  Conta, FatID: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLctFatRef, Dmod.MyDB, [
  'SELECT SUM(lfr.Valor) Valor ',
  'FROM lctfatref lfr ',
  'WHERE lfr.Controle=' + Geral.FF0(Controle),
  'AND lfr.FatID IN (0, '+ Geral.FF0(FatID) +') ',
  'GROUP BY lfr.Controle ',
  '']);
end;

procedure TUnSrvL_PF.ReopenXPrz(QrXPrz: TmySQLQuery; Tabela: String; Codigo,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrXPrz, Dmod.MyDB, [
  'SELECT opz.Parcelas, ',
  'opz.Codigo, opz.Controle, opz.Condicao, ',
  'opz.DescoPer, opz.Escolhido, ppc.Nome NO_CONDICAO ',
  'FROM ' + LowerCase(Tabela) + ' opz ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=opz.Condicao ',
  'WHERE opz.Codigo=' + Geral.FF0(Codigo),
  ' ']);
  //
  if Controle <> 0 then
    QrXPrz.Locate('Controle', Controle, []);
end;

procedure TUnSrvL_PF.TotalizaSrvlFatCab(Codigo: Integer);
var
  Qry: TmySQLQuery;
  ValTotCbr, ValDspToDsc, ValLiqToFat, ValLiqOkFat, ValDspToDpl, ValDspOkDpl: Double;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(ValTotCobr) ValTotCobr ',
    'FROM srvlosopegrn ',
    'WHERE srvlfatcab=' + Geral.FF0(Codigo),
    '']);
    ValTotCbr := Qry.FieldByName('ValTotCobr').AsFloat;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ood.FrmCtbDspFat, SUM(ood.Debito) Debito ',
    'FROM srvlosopedsp ood ',
    'LEFT JOIN srvlosopegrn oog ON oog.Controle=ood.Controle ',
    'WHERE oog.srvlfatcab=' + Geral.FF0(Codigo),
    'AND ood.FrmCtbDspFat IN (' +
    Geral.FF0(Integer(slcdDescoFaturam)) + ',' +
    Geral.FF0(Integer(slcdGeraCtaAPagr)) + ')',
    'GROUP BY ood.FrmCtbDspFat ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      case TSrvLCtbDsp(Qry.FieldByName('FrmCtbDspFat').AsInteger) of
        slcdDescoFaturam: ValDspToDsc := Qry.FieldByName('Debito').AsFloat;
        slcdGeraCtaAPagr: ValDspToDpl := Qry.FieldByName('Debito').AsFloat;
        else Geral.MB_Erro(
          '"TSrvLCtbDsp" n�o definido em "TUnSrvL_PF.TotalizaSrvlFatCab()"');
      end;
      //
      Qry.Next;
    end;

    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlfatcab', False, [
    'ValTotCbr'], [
    'Codigo'], [
    ValTotCbr], [
    Codigo], True) then
    begin
      //
    end;
    // Faturamenro
    //ValDspToDsc    := ;
    ValLiqToFat    := ValTotCbr - ValDspToDsc;
    ValLiqOkFat    := 0; // Falta fazer !!!
    //ValDspToDpl    := ;
    ValDspOkDpl    := 0; // Falta Fazer !!!
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlfatcab', False, [
    'ValTotCbr', 'ValDspToDsc', 'ValLiqToFat',
    'ValLiqOkFat', 'ValDspToDpl', 'ValDspOkDpl'], [
    'Codigo'], [
    ValTotCbr, ValDspToDsc, ValLiqToFat,
    ValLiqOkFat, ValDspToDpl, ValDspOkDpl], [
    Codigo], True) then
    begin
      //
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnSrvL_PF.TotalizaSrvlHonCab(Codigo: Integer);
var
  Qry: TmySQLQuery;
  ValTotRat: Double;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(ValPremio) ValPremio ',
    'FROM srvlosopeage ',
    'WHERE SrvLHonCab=' + Geral.FF0(Codigo),
    '']);
    ValTotRat := Qry.FieldByName('ValPremio').AsFloat;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlhoncab', False, [
    'ValTotRat'], [
    'Codigo'], [
    ValTotRat], [
    Codigo], True) then
    begin
      //
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnSrvL_PF.VerificaNovosAgentesSrvlHon(SrvlHonCab: Integer);
var
  Nome, CodTXT: String;
  Codigo, Controle, Entidade: Integer;
  Qry: TmySQLQuery;
begin
  Codigo         := SrvlHonCab;
  Controle       := 0;
  Nome           := '';
  Entidade       := 0;
  CodTXT         := Geral.FF0(Codigo);
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT sha.Controle CtrlSHA, ooa.Codigo, ooa.Agente',
    'FROM srvlosopeage ooa  ',
    'LEFT JOIN srvlhonage sha ON sha.Codigo=' +
    CodTXT + ' AND sha.Entidade=ooa.Agente',
    'WHERE ooa.SrvLHonCab=' + CodTXT,
    'AND sha.Controle IS NULL',
    'GROUP BY ooa.Agente  ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Qry.First;
      while not Qry.Eof do
      begin
        Entidade := Qry.FieldByName('Agente').Value;
        Controle := UMyMod.BPGS1I32('srvlhonage', 'Controle', '', '', tsPos,
          stIns, Controle);
        //if
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'srvlhonage', False, [
        'Codigo', 'Nome', 'Entidade'], [
        'Controle'], [
        Codigo, Nome, Entidade], [
        Controle], True);
        //
        Qry.Next;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnSrvL_PF.FaturamentosQuitados(TabLctA: String;
  FatID, FatNum: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT COUNT(lct.Controle) Itens ',
      'FROM ' + TabLctA + ' lct ',
      'LEFT JOIN carteiras car ON car.Codigo = lct.Carteira ',
      'WHERE lct.FatID=' + Geral.FF0(FatID),
      'AND lct.FatNum=' + Geral.FF0(FatNum),
      'AND ((lct.Sit > 0 OR lct.Compensado > 2) AND (car.Tipo = 2)) ',
      '']);
    //
    Result := Qry.FieldByName('Itens').AsInteger;
  finally
    Qry.Free;
  end;
end;

end.
