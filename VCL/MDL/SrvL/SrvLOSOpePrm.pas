unit SrvLOSOpePrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmSrvLOSOpePrm = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNO_SrvLCad: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    DBEdControle: TdmkDBEdit;
    Label2: TLabel;
    QrUnidMedCobr: TmySQLQuery;
    QrUnidMedCobrCodigo: TIntegerField;
    QrUnidMedCobrCodUsu: TIntegerField;
    QrUnidMedCobrSigla: TWideStringField;
    QrUnidMedCobrNome: TWideStringField;
    DsUnidMedCobr: TDataSource;
    QrUnidMedPaga: TmySQLQuery;
    QrUnidMedPagaCodigo: TIntegerField;
    QrUnidMedPagaCodUsu: TIntegerField;
    QrUnidMedPagaSigla: TWideStringField;
    QrUnidMedPagaNome: TWideStringField;
    DsUnidMedPaga: TDataSource;
    GroupBox3: TGroupBox;
    Panel3: TPanel;
    Label4: TLabel;
    EdUnidMedCobr: TdmkEditCB;
    EdSiglaCobr: TdmkEdit;
    CBUnidMedCobr: TdmkDBLookupComboBox;
    SBUnidMed: TSpeedButton;
    VUUnidMedCobr: TdmkValUsu;
    GroupBox4: TGroupBox;
    Panel5: TPanel;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    EdUnidMedPaga: TdmkEditCB;
    EdSiglaPaga: TdmkEdit;
    CBUnidMedPaga: TdmkDBLookupComboBox;
    VUUnidMedPaga: TdmkValUsu;
    EdValTotCobr: TdmkEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdValRatPaga: TdmkEdit;
    EdQtdMaxCobr: TdmkEdit;
    Label9: TLabel;
    RGFrmRatPaga: TdmkRadioGroup;
    Label10: TLabel;
    EdQtdMaxPaga: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdSiglaCobrChange(Sender: TObject);
    procedure EdSiglaCobrExit(Sender: TObject);
    procedure EdSiglaCobrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUnidMedCobrChange(Sender: TObject);
    procedure EdUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBUnidMedClick(Sender: TObject);
    procedure EdUnidMedPagaChange(Sender: TObject);
    procedure EdUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaPagaChange(Sender: TObject);
    procedure EdSiglaPagaExit(Sender: TObject);
    procedure EdSiglaPagaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenSrvLOSOpePrm(Conta: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmSrvLOSOpePrm: TFmSrvLOSOpePrm;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnMySQLCuringa,
  UnGrade_PF, ModProd;

{$R *.DFM}

procedure TFmSrvLOSOpePrm.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta, UnidMedCobr, UnidMedPaga, FrmRatPaga: Integer;
  ValTotCobr, QtdMaxCobr, QtdMaxPaga, ValRatPaga: Double;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := Geral.IMV(DBEdControle.Text);
  Conta          := EdConta.ValueVariant;
  //
  ValTotCobr     := EdValTotCobr.ValueVariant;
  UnidMedCobr    := VUUnidMedCobr.ValueVariant;
  QtdMaxCobr     := EdQtdMaxCobr.ValueVariant;
  //
  UnidMedPaga    := VUUnidMedPaga.ValueVariant;
  QtdMaxPaga     := EdQtdMaxPaga.ValueVariant;
  ValRatPaga     := EdValRatPaga.ValueVariant;
  FrmRatPaga     := RGFrmRatPaga.ItemIndex;
  //
  if MyObjects.FIC(FrmRatPaga < 1, RGFrmRatPaga,
  'Informe a "Forme de rateio"') then
    Exit;
  //
  Conta := UMyMod.BPGS1I32('srvlosopeprm', 'Conta', '', '', tsPos, ImgTipo.SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'srvlosopeprm', False, [
  'Codigo', 'Controle', 'ValTotCobr',
  'UnidMedCobr', 'QtdMaxCobr', 'UnidMedPaga',
  'QtdMaxPaga', 'ValRatPaga', 'FrmRatPaga'], [
  'Conta'], [
  Codigo, Controle, ValTotCobr,
  UnidMedCobr, QtdMaxCobr, UnidMedPaga,
  QtdMaxPaga, ValRatPaga, FrmRatPaga], [
  Conta], True) then
  begin
    ReopenSrvLOSOpePrm(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdConta.ValueVariant       := 0;
      //
      EdUnidMedPaga.ValueVariant := 0;
      EdSiglaPaga.ValueVariant   := 0;
      CBUnidMedPaga.KeyValue      := Null;
      //RGFrmRatPaga.ItemIndex := ?
      EdValRatPaga.ValueVariant  := 0;
      EdQtdMaxPaga.ValueVariant  := 0;
      EdUnidMedCobr.ValueVariant := 0;
      EdSiglaCobr.ValueVariant   := 0;
      CBUnidMedCobr.KeyValue     := Null;
      EdValTotCobr.ValueVariant  := 0;
      EdQtdMaxCobr.ValueVariant  := 0;
      //
      EdUnidMedCobr.SetFocus;
    end else Close;
  end;
end;

procedure TFmSrvLOSOpePrm.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLOSOpePrm.CBUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCobr, CBUnidMedCobr, dmktfInteger);
end;

procedure TFmSrvLOSOpePrm.CBUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedPaga, CBUnidMedPaga, dmktfInteger);
end;

procedure TFmSrvLOSOpePrm.EdSiglaCobrChange(Sender: TObject);
begin
  if EdSiglaCobr.Focused then
    DmProd.PesquisaPorSigla(False, EdSiglaCobr, EdUnidMedCobr, CBUnidMedCobr);
end;

procedure TFmSrvLOSOpePrm.EdSiglaCobrExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSiglaCobr, EdUnidMedCobr, CBUnidMedCobr);
end;

procedure TFmSrvLOSOpePrm.EdSiglaCobrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCobr, CBUnidMedCobr, dmktfInteger)
end;

procedure TFmSrvLOSOpePrm.EdSiglaPagaChange(Sender: TObject);
begin
  if EdSiglaPaga.Focused then
    DmProd.PesquisaPorSigla(False, EdSiglaPaga, EdUnidMedPaga, CBUnidMedPaga);
end;

procedure TFmSrvLOSOpePrm.EdSiglaPagaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSiglaPaga, EdUnidMedPaga, CBUnidMedPaga);
end;

procedure TFmSrvLOSOpePrm.EdSiglaPagaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedPaga, CBUnidMedPaga, dmktfInteger)
end;

procedure TFmSrvLOSOpePrm.EdUnidMedCobrChange(Sender: TObject);
begin
  if not EdSiglaCobr.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMedCobr.ValueVariant, EdSiglaCobr);
end;

procedure TFmSrvLOSOpePrm.EdUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCobr, CBUnidMedCobr, dmktfInteger);
end;

procedure TFmSrvLOSOpePrm.EdUnidMedPagaChange(Sender: TObject);
begin
  if not EdSiglaPaga.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMedPaga.ValueVariant, EdSiglaPaga);
end;

procedure TFmSrvLOSOpePrm.EdUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedPaga, CBUnidMedPaga, dmktfInteger);
end;

procedure TFmSrvLOSOpePrm.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdControle.DataSource := FDsCab;
  DBEdNO_SrvLCad.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmSrvLOSOpePrm.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrUnidMedCobr, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMedPaga, Dmod.MyDB);
end;

procedure TFmSrvLOSOpePrm.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLOSOpePrm.ReopenSrvLOSOpePrm(Conta: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Conta <> 0 then
      FQrIts.Locate('Conta', Conta, []);
  end;
end;

procedure TFmSrvLOSOpePrm.SBUnidMedClick(Sender: TObject);
begin
  Grade_PF.CadastroEDefinicaoDeUnidMed(
    VUUnidMedCobr, EdUnidMedCobr, CBUnidMedCobr, QrUnidMedCobr);
end;

end.
