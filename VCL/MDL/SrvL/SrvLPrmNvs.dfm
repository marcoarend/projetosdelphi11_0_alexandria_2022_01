object FmSrvLPrmNvs: TFmSrvLPrmNvs
  Left = 339
  Top = 185
  Caption = 'LOC-SERVI-007 :: N'#237'vel de Pr'#234'mio'
  ClientHeight = 536
  ClientWidth = 654
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 654
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 581
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 654
    Height = 289
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object GroupBox3: TGroupBox
      Left = 2
      Top = 53
      Width = 650
      Height = 97
      Align = alTop
      Caption = ' Dados de cobran'#231'a do cliente:'
      TabOrder = 0
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 646
        Height = 80
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label4: TLabel
          Left = 8
          Top = 40
          Width = 117
          Height = 13
          Caption = 'Unidade de Medida [F3]:'
        end
        object SBUnidMed: TSpeedButton
          Left = 396
          Top = 56
          Width = 21
          Height = 22
          Caption = '...'
          OnClick = SBUnidMedClick
        end
        object LaValFtrCobr: TLabel
          Left = 420
          Top = 40
          Width = 33
          Height = 13
          Caption = '?????:'
        end
        object LaQtdMaxCobr: TLabel
          Left = 524
          Top = 40
          Width = 75
          Height = 13
          Caption = 'M'#225'ximo de ???:'
        end
        object EdUnidMedCobr: TdmkEditCB
          Left = 8
          Top = 56
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdUnidMedCobrChange
          OnKeyDown = EdUnidMedCobrKeyDown
          DBLookupComboBox = CBUnidMedCobr
          IgnoraDBLookupComboBox = False
        end
        object EdSiglaCobr: TdmkEdit
          Left = 64
          Top = 56
          Width = 40
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdSiglaCobrChange
          OnExit = EdSiglaCobrExit
          OnKeyDown = EdSiglaCobrKeyDown
        end
        object CBUnidMedCobr: TdmkDBLookupComboBox
          Left = 104
          Top = 56
          Width = 289
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsUnidMedCobr
          TabOrder = 3
          OnKeyDown = CBUnidMedCobrKeyDown
          dmkEditCB = EdUnidMedCobr
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdValFtrCobr: TdmkEdit
          Left = 420
          Top = 56
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValFtrCobr'
          UpdCampo = 'ValFtrCobr'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdQtdMaxCobr: TdmkEdit
          Left = 524
          Top = 56
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'QtdMaxCobr'
          UpdCampo = 'QtdMaxCobr'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGFrmFtrCobr: TdmkRadioGroup
          Left = 8
          Top = 0
          Width = 629
          Height = 37
          Caption = ' Fator de cobran'#231'a: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'pfcIndefinido=0, '
            'pfcValorTotal=1, '
            'pfcValorUnitario=2')
          TabOrder = 0
          OnClick = RGFrmFtrCobrClick
          QryCampo = 'FrmFtrCobr'
          UpdCampo = 'FrmFtrCobr'
          UpdType = utYes
          OldValor = 0
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 2
      Top = 150
      Width = 650
      Height = 135
      Align = alTop
      Caption = ' Dados de pagamento de funcion'#225'rios:'
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 646
        Height = 118
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 40
          Width = 117
          Height = 13
          Caption = 'Unidade de Medida [F3]:'
        end
        object SpeedButton1: TSpeedButton
          Left = 396
          Top = 56
          Width = 21
          Height = 22
          Caption = '...'
          OnClick = SBUnidMedClick
        end
        object LaValRatPaga: TLabel
          Left = 420
          Top = 40
          Width = 33
          Height = 13
          Caption = '?????:'
        end
        object LaQtdMaxPaga: TLabel
          Left = 524
          Top = 40
          Width = 75
          Height = 13
          Caption = 'M'#225'ximo de ???:'
        end
        object EdUnidMedPaga: TdmkEditCB
          Left = 8
          Top = 56
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdUnidMedPagaChange
          OnKeyDown = EdUnidMedPagaKeyDown
          DBLookupComboBox = CBUnidMedPaga
          IgnoraDBLookupComboBox = False
        end
        object EdSiglaPaga: TdmkEdit
          Left = 64
          Top = 56
          Width = 40
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdSiglaPagaChange
          OnExit = EdSiglaPagaExit
          OnKeyDown = EdSiglaPagaKeyDown
        end
        object CBUnidMedPaga: TdmkDBLookupComboBox
          Left = 104
          Top = 56
          Width = 289
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsUnidMedPaga
          TabOrder = 3
          OnKeyDown = CBUnidMedPagaKeyDown
          dmkEditCB = EdUnidMedPaga
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdValRatPaga: TdmkEdit
          Left = 420
          Top = 56
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValRatPaga'
          UpdCampo = 'ValRatPaga'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGFrmRatPaga: TdmkRadioGroup
          Left = 8
          Top = 80
          Width = 629
          Height = 37
          Caption = ' Forma de rateio: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'pfrIndefinido=0, '
            'pfrValorTotal=1, '
            'pfrValorPorPessoa=2')
          TabOrder = 6
          OnClick = RGFrmRatPagaClick
          QryCampo = 'FrmRatPaga'
          UpdCampo = 'FrmRatPaga'
          UpdType = utYes
          OldValor = 0
        end
        object EdQtdMaxPaga: TdmkEdit
          Left = 524
          Top = 56
          Width = 112
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'QtdMaxPaga'
          UpdCampo = 'QtdMaxPaga'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGFrmFtrPaga: TdmkRadioGroup
          Left = 8
          Top = 0
          Width = 629
          Height = 37
          Caption = ' Fator de cobran'#231'a: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'pfcIndefinido=0, '
            'pfcValorTotal=1, '
            'pfcValorUnitario=2')
          TabOrder = 0
          OnClick = RGFrmFtrPagaClick
          QryCampo = 'FrmFtrPaga'
          UpdCampo = 'FrmFtrPaga'
          UpdType = utYes
          OldValor = 0
        end
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 650
      Height = 38
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Label6: TLabel
        Left = 12
        Top = 0
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object EdControle: TdmkEdit
        Left = 12
        Top = 16
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 654
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 606
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 558
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 189
        Height = 32
        Caption = 'N'#237'vel de Pr'#234'mio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 189
        Height = 32
        Caption = 'N'#237'vel de Pr'#234'mio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 189
        Height = 32
        Caption = 'N'#237'vel de Pr'#234'mio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 422
    Width = 654
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 650
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 466
    Width = 654
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 508
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 506
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 401
    Width = 654
    Height = 21
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object QrUnidMedCobr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 196
    Top = 40
    object QrUnidMedCobrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCobrCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedCobrSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedCobrNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMedCobr: TDataSource
    DataSet = QrUnidMedCobr
    Left = 196
    Top = 84
  end
  object QrUnidMedPaga: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 280
    Top = 40
    object QrUnidMedPagaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedPagaCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedPagaSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedPagaNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMedPaga: TDataSource
    DataSet = QrUnidMedPaga
    Left = 280
    Top = 84
  end
  object VUUnidMedCobr: TdmkValUsu
    dmkEditCB = EdUnidMedCobr
    Panel = Panel3
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 360
    Top = 40
  end
  object VUUnidMedPaga: TdmkValUsu
    dmkEditCB = EdUnidMedPaga
    Panel = Panel5
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 360
    Top = 88
  end
end
