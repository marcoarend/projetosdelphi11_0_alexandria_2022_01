object FmSrvLOSCab: TFmSrvLOSCab
  Left = 368
  Top = 194
  Caption = 'LOC-SERVI-002 :: Ordem de Servi'#231'o'
  ClientHeight = 841
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 16
    Top = 56
    Width = 96
    Height = 13
    Caption = 'Pr'#233' cadastro de OS:'
  end
  object Label11: TLabel
    Left = 16
    Top = 96
    Width = 39
    Height = 13
    Caption = 'Servi'#231'o:'
  end
  object Label14: TLabel
    Left = 508
    Top = 96
    Width = 35
    Height = 13
    Caption = 'Cliente:'
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 745
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 137
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 352
        Top = 96
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object SBCliente: TSpeedButton
        Left = 480
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBClienteClick
      end
      object SbSrvLPre: TSpeedButton
        Left = 972
        Top = 32
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbSrvLPreClick
      end
      object SbSrvLCad: TSpeedButton
        Left = 975
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbSrvLCadClick
      end
      object Label12: TLabel
        Left = 16
        Top = 96
        Width = 43
        Height = 13
        Caption = 'Abertura:'
      end
      object Label13: TLabel
        Left = 184
        Top = 96
        Width = 72
        Height = 13
        Caption = 'Encerramento: '
      end
      object Label15: TLabel
        Left = 508
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Pr'#233' cadastro de OS:'
      end
      object Label16: TLabel
        Left = 508
        Top = 56
        Width = 39
        Height = 13
        Caption = 'Servi'#231'o:'
      end
      object Label17: TLabel
        Left = 16
        Top = 56
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label18: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label6: TLabel
        Left = 792
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Status:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 352
        Top = 112
        Width = 645
        Height = 21
        TabOrder = 15
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 16
        Top = 72
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 68
        Top = 72
        Width = 409
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsClientes
        TabOrder = 8
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdSrvLPre: TdmkEditCB
        Left = 508
        Top = 32
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SrvLPre'
        UpdCampo = 'SrvLPre'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSrvLPre
        IgnoraDBLookupComboBox = False
      end
      object CBSrvLPre: TdmkDBLookupComboBox
        Left = 560
        Top = 32
        Width = 229
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSrvLPOCab
        TabOrder = 4
        dmkEditCB = EdSrvLPre
        QryCampo = 'SrvLPre'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdSrvLCad: TdmkEditCB
        Left = 508
        Top = 72
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SrvLCad'
        UpdCampo = 'SrvLCad'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSrvLCad
        IgnoraDBLookupComboBox = False
      end
      object CBSrvLCad: TdmkDBLookupComboBox
        Left = 560
        Top = 72
        Width = 412
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSrvLCad
        TabOrder = 10
        dmkEditCB = EdSrvLCad
        QryCampo = 'SrvLCad'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object TPDtAbertura: TdmkEditDateTimePicker
        Left = 16
        Top = 112
        Width = 112
        Height = 21
        Date = 40724.768997777780000000
        Time = 40724.768997777780000000
        TabOrder = 11
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtAbertura'
        UpdCampo = 'DtAbertura'
        UpdType = utYes
      end
      object EdDtAbertura: TdmkEdit
        Left = 128
        Top = 112
        Width = 52
        Height = 21
        TabOrder = 12
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        QryCampo = 'DtAbertura'
        UpdCampo = 'DtAbertura'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtEncerrad: TdmkEditDateTimePicker
        Left = 184
        Top = 112
        Width = 112
        Height = 21
        Date = 40724.768997777780000000
        Time = 40724.768997777780000000
        TabOrder = 13
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtEncerrad'
        UpdCampo = 'DtEncerrad'
        UpdType = utYes
      end
      object EdDtEncerrad: TdmkEdit
        Left = 296
        Top = 112
        Width = 52
        Height = 21
        TabOrder = 14
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        QryCampo = 'DtEncerrad'
        UpdCampo = 'DtEncerrad'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 369
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEstatus: TdmkEditCB
        Left = 792
        Top = 32
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Estatus'
        UpdCampo = 'Estatus'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEstatus
        IgnoraDBLookupComboBox = False
      end
      object CBEstatus: TdmkDBLookupComboBox
        Left = 836
        Top = 32
        Width = 160
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEstatusOSs
        TabOrder = 6
        dmkEditCB = EdEstatus
        QryCampo = 'Estatus'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 682
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 745
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 137
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 120
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label20: TLabel
          Left = 16
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label21: TLabel
          Left = 76
          Top = 0
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label22: TLabel
          Left = 508
          Top = 0
          Width = 96
          Height = 13
          Caption = 'Pr'#233' cadastro de OS:'
        end
        object Label23: TLabel
          Left = 16
          Top = 40
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object Label24: TLabel
          Left = 508
          Top = 40
          Width = 39
          Height = 13
          Caption = 'Servi'#231'o:'
        end
        object Label25: TLabel
          Left = 16
          Top = 80
          Width = 43
          Height = 13
          Caption = 'Abertura:'
        end
        object Label26: TLabel
          Left = 132
          Top = 80
          Width = 69
          Height = 13
          Caption = 'Encerramento:'
        end
        object Label27: TLabel
          Left = 608
          Top = 80
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label1: TLabel
          Left = 248
          Top = 80
          Width = 58
          Height = 13
          Caption = '$ Cobran'#231'a:'
          FocusControl = DBEdit10
        end
        object Label2: TLabel
          Left = 324
          Top = 80
          Width = 59
          Height = 13
          Caption = '$ Despesas:'
          FocusControl = DBEdit11
        end
        object Label3: TLabel
          Left = 400
          Top = 80
          Width = 51
          Height = 13
          Caption = '$ Agentes:'
          FocusControl = DBEdit12
        end
        object Label4: TLabel
          Left = 476
          Top = 80
          Width = 48
          Height = 13
          Caption = '$ L'#237'quido:'
          FocusControl = DBEdit13
        end
        object Label5: TLabel
          Left = 552
          Top = 80
          Width = 37
          Height = 13
          Caption = '% MCB:'
          FocusControl = DBEdit14
        end
        object Label8: TLabel
          Left = 792
          Top = 0
          Width = 33
          Height = 13
          Caption = 'Status:'
          Color = clBtnFace
          ParentColor = False
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 16
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsSrvLOSCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdit8: TDBEdit
          Left = 76
          Top = 16
          Width = 56
          Height = 21
          DataField = 'Empresa'
          DataSource = DsSrvLOSCab
          TabOrder = 1
        end
        object DBEdit9: TDBEdit
          Left = 132
          Top = 16
          Width = 373
          Height = 21
          DataField = 'NO_EMPRESA'
          DataSource = DsSrvLOSCab
          TabOrder = 2
        end
        object DBEdit1: TDBEdit
          Left = 508
          Top = 16
          Width = 56
          Height = 21
          DataField = 'SrvLPre'
          DataSource = DsSrvLOSCab
          TabOrder = 3
        end
        object DBEdit3: TDBEdit
          Left = 16
          Top = 56
          Width = 56
          Height = 21
          DataField = 'Cliente'
          DataSource = DsSrvLOSCab
          TabOrder = 4
        end
        object DBEdit7: TDBEdit
          Left = 72
          Top = 56
          Width = 433
          Height = 21
          DataField = 'NO_CLIENTE'
          DataSource = DsSrvLOSCab
          TabOrder = 5
        end
        object DBEdit2: TDBEdit
          Left = 508
          Top = 56
          Width = 56
          Height = 21
          DataField = 'SrvLCad'
          DataSource = DsSrvLOSCab
          TabOrder = 6
        end
        object DBEdit6: TDBEdit
          Left = 564
          Top = 56
          Width = 433
          Height = 21
          DataField = 'NO_SrvLCad'
          DataSource = DsSrvLOSCab
          TabOrder = 7
        end
        object DBEdit4: TDBEdit
          Left = 16
          Top = 96
          Width = 112
          Height = 21
          DataField = 'DtAbertura'
          DataSource = DsSrvLOSCab
          TabOrder = 8
        end
        object DBEdit5: TDBEdit
          Left = 132
          Top = 96
          Width = 112
          Height = 21
          DataField = 'DtEncerrad_TXT'
          DataSource = DsSrvLOSCab
          TabOrder = 9
        end
        object DBEdNome: TdmkDBEdit
          Left = 608
          Top = 96
          Width = 389
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsSrvLOSCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 10
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit10: TDBEdit
          Left = 248
          Top = 96
          Width = 72
          Height = 21
          DataField = 'ValTotCobr'
          DataSource = DsSrvLOSCab
          TabOrder = 11
        end
        object DBEdit11: TDBEdit
          Left = 324
          Top = 96
          Width = 72
          Height = 21
          DataField = 'ValTotDesp'
          DataSource = DsSrvLOSCab
          TabOrder = 12
        end
        object DBEdit12: TDBEdit
          Left = 400
          Top = 96
          Width = 72
          Height = 21
          DataField = 'ValTotPaga'
          DataSource = DsSrvLOSCab
          TabOrder = 13
        end
        object DBEdit13: TDBEdit
          Left = 476
          Top = 96
          Width = 72
          Height = 21
          DataField = 'ValTotLiqu'
          DataSource = DsSrvLOSCab
          TabOrder = 14
        end
        object DBEdit14: TDBEdit
          Left = 552
          Top = 96
          Width = 52
          Height = 21
          DataField = 'PerTotLiqu'
          DataSource = DsSrvLOSCab
          TabOrder = 15
        end
        object DBEdit15: TDBEdit
          Left = 792
          Top = 16
          Width = 45
          Height = 21
          DataField = 'Estatus'
          DataSource = DsSrvLOSCab
          TabOrder = 16
        end
        object DBEdit16: TDBEdit
          Left = 836
          Top = 16
          Width = 160
          Height = 21
          DataField = 'NO_ESTATUS'
          DataSource = DsSrvLOSCab
          TabOrder = 17
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 681
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 22
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 196
        Top = 15
        Width = 810
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 704
          Top = 0
          Width = 106
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 4
          object BtSaida: TBitBtn
            Tag = 13
            Left = 0
            Top = 4
            Width = 100
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 613
          Left = 4
          Top = 4
          Width = 100
          Height = 40
          Cursor = crHandPoint
          Caption = '&OS'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCabClick
        end
        object BtSrvLOSOpeGrn: TBitBtn
          Tag = 612
          Left = 104
          Top = 4
          Width = 100
          Height = 40
          Cursor = crHandPoint
          Caption = 'O&pera'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtSrvLOSOpeGrnClick
        end
        object BtSrvLOSOpeAge: TBitBtn
          Tag = 110
          Left = 204
          Top = 4
          Width = 100
          Height = 40
          Cursor = crHandPoint
          Caption = '&Agente'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtSrvLOSOpeAgeClick
        end
        object BtSrvLOSOpeDsp: TBitBtn
          Tag = 10044
          Left = 404
          Top = 4
          Width = 100
          Height = 40
          Cursor = crHandPoint
          Caption = '&Despesas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtSrvLOSOpeDspClick
        end
        object BtSrvLOSOpeAtrDef: TBitBtn
          Tag = 555
          Left = 504
          Top = 4
          Width = 100
          Height = 40
          Cursor = crHandPoint
          Caption = 'A&tributos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtSrvLOSOpeAtrDefClick
        end
        object BtSrvLOSPrz: TBitBtn
          Tag = 407
          Left = 604
          Top = 4
          Width = 100
          Height = 40
          Cursor = crHandPoint
          Caption = '&Prazos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = BtSrvLOSPrzClick
        end
        object BtSrvLOSOpeCPA: TBitBtn
          Left = 304
          Top = 4
          Width = 100
          Height = 40
          Cursor = crHandPoint
          Caption = '&Jornada'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          OnClick = BtSrvLOSOpeCPAClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 137
      Width = 1008
      Height = 428
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Opera'#231#245'es'
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 400
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Splitter1: TSplitter
            Left = 0
            Top = 240
            Width = 1000
            Height = 5
            Cursor = crVSplit
            Align = alBottom
            ExplicitTop = -71
          end
          object Splitter3: TSplitter
            Left = 575
            Top = 84
            Width = 5
            Height = 156
            Align = alRight
            ExplicitLeft = 448
            ExplicitTop = 7
          end
          object DBGSrvLOSOpeGrn: TDBGrid
            Left = 0
            Top = 0
            Width = 1000
            Height = 84
            Align = alTop
            DataSource = DsSrvLOSOpeGrn
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            PopupMenu = PMSrvLOSOpeGrn
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBGSrvLOSOpeGrnCellClick
            OnColEnter = DBGSrvLOSOpeGrnColEnter
            OnColExit = DBGSrvLOSOpeGrnColExit
            OnDrawColumnCell = DBGSrvLOSOpeGrnDrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'Autorizado'
                Title.Caption = 'A'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaExeIni_TXT'
                Title.Caption = 'Dta.Exe.Ini.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaExeFim_TXT'
                Title.Caption = 'Dta.Exe.Fim.'
                Width = 112
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_SrvLCad'
                Title.Caption = 'Servi'#231'o'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_SrvLPrmCad'
                Title.Caption = 'Configura'#231#227'o de pr'#234'mios'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_AgeEquiCab'
                Title.Caption = 'Equipe de agentes'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Observa'#231#227'o'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FrmFtrCobr'
                Title.Caption = 'Fator de cobran'#231'a'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValFtrCobr'
                Title.Caption = '$ Unit.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLA_UMC'
                Title.Caption = 'Unidade'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QtdRlzCobr'
                Title.Caption = 'Qtde cobr.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTotCobr'
                Title.Caption = '$ Total cobr'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FrmFtrPaga'
                Title.Caption = 'Fator de pagamento'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIGLA_UMP'
                Title.Caption = 'Unidade'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValFtrPaga'
                Title.Caption = '$ Unit.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QtdRlzPaga'
                Title.Caption = 'Qtde pag.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValRatPaga'
                Title.Caption = '$ Total pag.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FrmRatPaga'
                Title.Caption = 'Forma de rateio'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTotDesp'
                Title.Caption = '$ Despesas'
                Visible = True
              end>
          end
          object GroupBox10: TGroupBox
            Left = 0
            Top = 245
            Width = 1000
            Height = 155
            Align = alBottom
            Caption = ' Agentes da opera'#231#227'o selecionada: '
            TabOrder = 1
            object Splitter2: TSplitter
              Left = 445
              Top = 15
              Width = 5
              Height = 138
              ExplicitLeft = 448
              ExplicitTop = 7
            end
            object DBGOSAge: TDBGrid
              Left = 2
              Top = 15
              Width = 443
              Height = 138
              Align = alLeft
              DataSource = DsSrvLOSOpeAge
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              PopupMenu = PMOSOpeAge
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = DBGOSAgeCellClick
              OnColEnter = DBGOSAgeColEnter
              OnColExit = DBGOSAgeColExit
              OnDrawColumnCell = DBGOSAgeDrawColumnCell
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Responsa'
                  Title.Caption = 'R'
                  Width = 17
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_AGENTE'
                  Title.Caption = 'Agente'
                  Width = 254
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValPremio'
                  Title.Caption = '$ Pr'#234'mio'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Title.Caption = 'ID'
                  Visible = True
                end>
            end
            object GroupBox1: TGroupBox
              Left = 450
              Top = 15
              Width = 548
              Height = 138
              Align = alClient
              Caption = ' Dados do agente selecionado: '
              TabOrder = 1
              object DBGrid2: TDBGrid
                Left = 2
                Top = 15
                Width = 544
                Height = 121
                Align = alClient
                DataSource = DsSrvLOSOpeCPA
                PopupMenu = PMSrvLOSOpeCPA
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_ValEverAut'
                    Title.Caption = 'Aut.'
                    Width = 28
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DtaCompet'
                    Title.Caption = 'Compet'#234'ncia'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'FatorPrm'
                    Title.Caption = 'Fator'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValPremio'
                    Title.Caption = 'Valor'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'HrInnDay'
                    Title.Caption = 'Entrada'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'HrOutDay'
                    Title.Caption = 'Sa'#237'da'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TemInterv'
                    Title.Caption = 'Interv?'
                    Width = 38
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'HrOutMid'
                    Title.Caption = 'Sai'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'HrInnMid'
                    Title.Caption = 'Retorno'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nivel4'
                    Title.Caption = 'ID'
                    Visible = True
                  end>
              end
            end
          end
          object GroupBox2: TGroupBox
            Left = 580
            Top = 84
            Width = 420
            Height = 156
            Align = alRight
            Caption = ' Atributos da opera'#231#227'o selecionada: '
            TabOrder = 2
            object DBGSrvLOSOpeAtrDef: TDBGrid
              Left = 2
              Top = 15
              Width = 416
              Height = 139
              Align = alClient
              DataSource = DsSrvLOSOpeAtrDef
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
              PopupMenu = PMSrvLOSOpeAtrDef
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_CAD'
                  Title.Caption = 'Atributo'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_ITS'
                  Title.Caption = 'Descri'#231#227'o do item do atributo'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ID_Item'
                  Title.Caption = 'ID'
                  Width = 56
                  Visible = True
                end>
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 84
            Width = 575
            Height = 156
            Align = alClient
            Caption = ' Despesas da opera'#231#227'o selecionada:'
            TabOrder = 3
            object DBGSrvLOSOpeDsp: TDBGrid
              Left = 2
              Top = 15
              Width = 571
              Height = 139
              Align = alClient
              DataSource = DsSrvLOSOpeDsp
              PopupMenu = PMSrvLOSOpeDsp
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Caption = 'D'#233'bito'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_GENERO'
                  Title.Caption = 'Conta (plano de contas)'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_AGENTE'
                  Title.Caption = 'Agente'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_FORNECE'
                  Title.Caption = 'Fornecedor'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_FrmCtbDspFat'
                  Title.Caption = 'Contab. Despesa'
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Condi'#231#245'es de pagamento'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGSrvLOSPrz: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 400
          Align = alClient
          DataSource = DsSrvLOSPrz
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          PopupMenu = PMSrvLOSPrz
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGSrvLOSPrzCellClick
          OnColEnter = DBGSrvLOSPrzColEnter
          OnDrawColumnCell = DBGSrvLOSPrzDrawColumnCell
          OnMouseUp = DBGSrvLOSPrzMouseUp
          Columns = <
            item
              Expanded = False
              FieldName = 'Escolhido'
              Title.Caption = 'E'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CONDICAO'
              Title.Caption = 'Descri'#231#227'o da condi'#231#227'o de pagamento'
              Width = 214
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DescoPer'
              Title.Caption = '% Desconto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ORC_COM_DESCO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Title.Caption = '$ Or'#231'ado'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'INV_COM_DESCO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 4227327
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Title.Caption = '$ N'#227'o Aprov.'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VAL_COM_DESCO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Title.Caption = '$ Aprovado'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Parcelas'
              Title.Caption = 'Parc.'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ORC_PARCELA'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Title.Caption = '$ Parc. Or'#231'.'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'INV_PARCELA'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 4227327
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Title.Caption = '$ Parc. N'#227'o'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VAL_PARCELA'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Title.Caption = '$ Parc. Aprov.'
              Width = 68
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 218
        Height = 32
        Caption = 'Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 218
        Height = 32
        Caption = 'Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 218
        Height = 32
        Caption = 'Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 68
    Top = 52
  end
  object QrSrvLOSCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrSrvLOSCabBeforeOpen
    AfterOpen = QrSrvLOSCabAfterOpen
    BeforeClose = QrSrvLOSCabBeforeClose
    AfterScroll = QrSrvLOSCabAfterScroll
    SQL.Strings = (
      'SELECT osc.*, cad.Nome NO_SrvLCad, sta.Nome NO_ESTATUS,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE,'
      'IF(DtEncerrad <= "1900-01-01", "",'
      '  DATE_FORMAT(DtEncerrad, "%d/%m/%Y")) DtEncerrad_TXT'
      'FROM srvloscab osc'
      'LEFT JOIN srvlcad cad ON cad.Codigo=osc.SrvLCad'
      'LEFT JOIN entidades ent ON ent.Codigo=osc.Cliente'
      'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus'
      'WHERE osc.Codigo > 0'
      '')
    Left = 152
    Top = 53
    object QrSrvLOSCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLOSCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSrvLOSCabSrvLPre: TIntegerField
      FieldName = 'SrvLPre'
    end
    object QrSrvLOSCabSrvLCad: TIntegerField
      FieldName = 'SrvLCad'
    end
    object QrSrvLOSCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSrvLOSCabDtAbertura: TDateTimeField
      FieldName = 'DtAbertura'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrSrvLOSCabDtEncerrad: TDateTimeField
      FieldName = 'DtEncerrad'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrSrvLOSCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLOSCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLOSCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLOSCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLOSCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLOSCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLOSCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLOSCabNO_SrvLCad: TWideStringField
      FieldName = 'NO_SrvLCad'
      Size = 60
    end
    object QrSrvLOSCabNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrSrvLOSCabDtEncerrad_TXT: TWideStringField
      FieldName = 'DtEncerrad_TXT'
      Size = 10
    end
    object QrSrvLOSCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSrvLOSCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrSrvLOSCabValTotCobr: TFloatField
      FieldName = 'ValTotCobr'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSCabValTotDesp: TFloatField
      FieldName = 'ValTotDesp'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSCabValRatPaga: TFloatField
      FieldName = 'ValRatPaga'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSCabValTotPaga: TFloatField
      FieldName = 'ValTotPaga'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSCabValTotLiqu: TFloatField
      FieldName = 'ValTotLiqu'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSCabPerTotLiqu: TFloatField
      FieldName = 'PerTotLiqu'
      DisplayFormat = '0.00'
    end
    object QrSrvLOSCabEstatus: TIntegerField
      FieldName = 'Estatus'
    end
    object QrSrvLOSCabNO_ESTATUS: TWideStringField
      FieldName = 'NO_ESTATUS'
      Size = 60
    end
  end
  object DsSrvLOSCab: TDataSource
    DataSet = QrSrvLOSCab
    Left = 148
    Top = 97
  end
  object QrSrvLOSEsc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_its'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 380
    Top = 53
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DsSrvLOSEsc: TDataSource
    DataSet = QrSrvLOSEsc
    Left = 380
    Top = 97
  end
  object PMSrvLOSEsc: TPopupMenu
    OnPopup = PMSrvLOSEscPopup
    Left = 392
    Top = 460
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMSrvLOSCab: TPopupMenu
    OnPopup = PMSrvLOSCabPopup
    Left = 232
    Top = 728
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrSrvLCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM srvlcad'
      'ORDER BY Nome')
    Left = 228
    Top = 52
    object QrSrvLCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsSrvLCad: TDataSource
    DataSet = QrSrvLCad
    Left = 228
    Top = 100
  end
  object PMSrvLOSOpeGrn: TPopupMenu
    OnPopup = PMSrvLOSOpeGrnPopup
    Left = 324
    Top = 728
    object IncluiNovaOperacao1: TMenuItem
      Caption = '&Inclui nova opera'#231#227'o'
      OnClick = IncluiNovaOperacao1Click
    end
    object AlteraOperacaoAtual1: TMenuItem
      Caption = '&Altera opera'#231#227'o atual'
      OnClick = AlteraOperacaoAtual1Click
    end
    object ExcluiOperacaoAtual1: TMenuItem
      Caption = '&Exclui opera'#231#227'o atual'
      OnClick = ExcluiOperacaoAtual1Click
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      'ufs.Nome NOMEUF'
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 452
    Top = 52
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClientesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrClientesCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrClientesNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrClientesCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrClientesIE: TWideStringField
      FieldName = 'IE'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 452
    Top = 96
  end
  object QrSrvLPOCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM srvlpocab'
      'ORDER BY Nome')
    Left = 304
    Top = 52
    object QrSrvLPOCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLPOCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsSrvLPOCab: TDataSource
    DataSet = QrSrvLPOCab
    Left = 304
    Top = 100
  end
  object QrSrvLOSOpeGrn: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrSrvLOSOpeGrnBeforeClose
    AfterScroll = QrSrvLOSOpeGrnAfterScroll
    SQL.Strings = (
      'SELECT slc.Nome NO_SrvLCad, prm.Nome NO_SrvLPrmCad,  '
      
        ' ELT(oog.FrmFtrCobr + 1,"Indefinido","Valor total","Valor unit'#225'r' +
        'io") NO_FrmFtrCobr,'
      
        ' ELT(oog.FrmFtrPaga + 1,"Indefinido","Valor total","Valor unit'#225'r' +
        'io") NO_FrmFtrPaga,'
      
        ' ELT(oog.FrmRatPaga + 1,"Indefinido","Valor total","Valor por pe' +
        'ssoa") NO_FrmRatPaga,'
      'umc.Sigla SIGLA_UMC, ump.Sigla SIGLA_UMP, '
      'aec.Nome NO_AgeEquiCab, oog.*  '
      'FROM srvlosopegrn oog '
      'LEFT JOIN srvlcad slc ON slc.Codigo=oog.SrvLCad '
      'LEFT JOIN srvlprmcad prm ON prm.Codigo=oog.SrvLPrmCad '
      'LEFT JOIN ageeqicab aec ON oog.AgeEqiCab=aec.Codigo '
      'LEFT JOIN unidmed umc ON umc.Codigo=oog.UnidMedCobr '
      'LEFT JOIN unidmed ump ON ump.Codigo=oog.UnidMedPaga '
      'WHERE oog.Codigo=2'
      '')
    Left = 84
    Top = 304
    object QrSrvLOSOpeGrnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLOSOpeGrnControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrvLOSOpeGrnSrvLCad: TIntegerField
      FieldName = 'SrvLCad'
    end
    object QrSrvLOSOpeGrnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLOSOpeGrnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLOSOpeGrnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLOSOpeGrnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLOSOpeGrnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLOSOpeGrnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLOSOpeGrnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLOSOpeGrnNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSrvLOSOpeGrnNO_SrvLCad: TWideStringField
      FieldName = 'NO_SrvLCad'
      Size = 60
    end
    object QrSrvLOSOpeGrnSrvLPrmCad: TIntegerField
      FieldName = 'SrvLPrmCad'
    end
    object QrSrvLOSOpeGrnNO_SrvLPrmCad: TWideStringField
      FieldName = 'NO_SrvLPrmCad'
      Size = 60
    end
    object QrSrvLOSOpeGrnNO_AgeEquiCab: TWideStringField
      FieldName = 'NO_AgeEquiCab'
      Size = 255
    end
    object QrSrvLOSOpeGrnAgeEqiCab: TIntegerField
      FieldName = 'AgeEqiCab'
    end
    object QrSrvLOSOpeGrnUnidMedCobr: TIntegerField
      FieldName = 'UnidMedCobr'
    end
    object QrSrvLOSOpeGrnQtdRlzCobr: TFloatField
      FieldName = 'QtdRlzCobr'
    end
    object QrSrvLOSOpeGrnValFtrCobr: TFloatField
      FieldName = 'ValFtrCobr'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrSrvLOSOpeGrnFrmFtrCobr: TSmallintField
      FieldName = 'FrmFtrCobr'
    end
    object QrSrvLOSOpeGrnUnidMedPaga: TIntegerField
      FieldName = 'UnidMedPaga'
    end
    object QrSrvLOSOpeGrnQtdRlzPaga: TFloatField
      FieldName = 'QtdRlzPaga'
    end
    object QrSrvLOSOpeGrnValRatPaga: TFloatField
      FieldName = 'ValRatPaga'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnFrmRatPaga: TSmallintField
      FieldName = 'FrmRatPaga'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnFrmFtrPaga: TSmallintField
      FieldName = 'FrmFtrPaga'
    end
    object QrSrvLOSOpeGrnValTotCobr: TFloatField
      FieldName = 'ValTotCobr'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnNO_FrmRatPaga: TWideStringField
      FieldName = 'NO_FrmRatPaga'
      Size = 60
    end
    object QrSrvLOSOpeGrnSIGLA_UMC: TWideStringField
      FieldName = 'SIGLA_UMC'
      Size = 6
    end
    object QrSrvLOSOpeGrnSIGLA_UMP: TWideStringField
      FieldName = 'SIGLA_UMP'
      Size = 6
    end
    object QrSrvLOSOpeGrnNO_FrmFtrCobr: TWideStringField
      FieldName = 'NO_FrmFtrCobr'
      Size = 60
    end
    object QrSrvLOSOpeGrnNO_FrmFtrPaga: TWideStringField
      FieldName = 'NO_FrmFtrPaga'
      Size = 60
    end
    object QrSrvLOSOpeGrnValFtrPaga: TFloatField
      FieldName = 'ValFtrPaga'
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrSrvLOSOpeGrnValTotDesp: TFloatField
      FieldName = 'ValTotDesp'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
    end
    object QrSrvLOSOpeGrnDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrSrvLOSOpeGrnDtaExeFim_TXT: TWideStringField
      FieldName = 'DtaExeFim_TXT'
    end
    object QrSrvLOSOpeGrnWrnLctDay: TSmallintField
      FieldName = 'WrnLctDay'
    end
    object QrSrvLOSOpeGrnValTotPaga: TFloatField
      FieldName = 'ValTotPaga'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeGrnAgeEqiCfg: TIntegerField
      FieldName = 'AgeEqiCfg'
    end
    object QrSrvLOSOpeGrnAutorizado: TSmallintField
      FieldName = 'Autorizado'
    end
    object QrSrvLOSOpeGrnDtaExeIni_TXT: TWideStringField
      FieldName = 'DtaExeIni_TXT'
    end
  end
  object DsSrvLOSOpeGrn: TDataSource
    DataSet = QrSrvLOSOpeGrn
    Left = 84
    Top = 352
  end
  object PMSrvLOSOpeDsp: TPopupMenu
    OnPopup = PMSrvLOSOpeDspPopup
    Left = 644
    Top = 728
    object IncluiNovaDespesa1: TMenuItem
      Caption = '&Inclui nova despesa'
      OnClick = IncluiNovaDespesa1Click
    end
    object AlteraDespesaAtual1: TMenuItem
      Caption = '&Altera despesa atual'
      OnClick = AlteraDespesaAtual1Click
    end
    object ExcluiDespesaAtual1: TMenuItem
      Caption = '&Exclui despesa atual'
      OnClick = ExcluiDespesaAtual1Click
    end
  end
  object QrSrvLOSOpeDsp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gen.Nome NO_GENERO, '
      'IF(age.Tipo=0, age.RazaoSocial, age.Nome) NO_AGENTE, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, '
      'oop.*  '
      'FROM srvlosopedsp oop '
      'LEFT JOIN contas gen ON gen.Codigo=oop.Genero '
      'LEFT JOIN entidades age ON age.Codigo=oop.Agente '
      'LEFT JOIN entidades frn ON frn.Codigo=oop.Fornece ')
    Left = 196
    Top = 304
    object QrSrvLOSOpeDspNO_GENERO: TWideStringField
      FieldName = 'NO_GENERO'
      Size = 50
    end
    object QrSrvLOSOpeDspNO_AGENTE: TWideStringField
      FieldName = 'NO_AGENTE'
      Size = 100
    end
    object QrSrvLOSOpeDspNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrSrvLOSOpeDspCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLOSOpeDspControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrvLOSOpeDspConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrSrvLOSOpeDspGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrSrvLOSOpeDspAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrSrvLOSOpeDspDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSrvLOSOpeDspLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLOSOpeDspDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLOSOpeDspDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLOSOpeDspUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLOSOpeDspUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLOSOpeDspAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLOSOpeDspAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLOSOpeDspFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrSrvLOSOpeDspFrmCtbDspFat: TSmallintField
      FieldName = 'FrmCtbDspFat'
    end
    object QrSrvLOSOpeDspNO_FrmCtbDspFat: TWideStringField
      FieldName = 'NO_FrmCtbDspFat'
      Size = 30
    end
  end
  object DsSrvLOSOpeDsp: TDataSource
    DataSet = QrSrvLOSOpeDsp
    Left = 196
    Top = 356
  end
  object PMOSOpeAge: TPopupMenu
    OnPopup = PMOSOpeAgePopup
    Left = 420
    Top = 728
    object IncluiOSAge1: TMenuItem
      Caption = '&Adiciona agente(s)'
      OnClick = IncluiOSAge1Click
    end
    object RemoveOSAge1: TMenuItem
      Caption = '&Retira agente'
      OnClick = RemoveOSAge1Click
    end
  end
  object QrSrvLOSOpeAge: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrSrvLOSOpeAgeBeforeClose
    AfterScroll = QrSrvLOSOpeAgeAfterScroll
    SQL.Strings = (
      'SELECT ent.Nome NO_AGENTE, age.* '
      'FROM srvlosage age'
      'LEFT JOIN entidades ent on ent.Codigo=age.Agente'
      'WHERE age.Codigo=:P0')
    Left = 284
    Top = 304
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSrvLOSOpeAgeNO_AGENTE: TWideStringField
      FieldName = 'NO_AGENTE'
      Size = 100
    end
    object QrSrvLOSOpeAgeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLOSOpeAgeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrvLOSOpeAgeConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrSrvLOSOpeAgeAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrSrvLOSOpeAgeResponsa: TSmallintField
      FieldName = 'Responsa'
    end
    object QrSrvLOSOpeAgeLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLOSOpeAgeDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLOSOpeAgeDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLOSOpeAgeUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLOSOpeAgeUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLOSOpeAgeAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLOSOpeAgeAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLOSOpeAgeValPremio: TFloatField
      FieldName = 'ValPremio'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSrvLOSOpeAge: TDataSource
    DataSet = QrSrvLOSOpeAge
    Left = 284
    Top = 352
  end
  object PMSrvLOSOpeAtrDef: TPopupMenu
    OnPopup = PMSrvLOSOpeAtrDefPopup
    Left = 748
    Top = 728
    object IncluiSrvLOSOpeAtrDef1: TMenuItem
      Caption = '&Inclui um novo atributo'
      OnClick = IncluiSrvLOSAtrDef1Click
    end
    object AlteraSrvLOSOpeAtrDef1: TMenuItem
      Caption = '&Altera o atributo selecionado'
      OnClick = AlteraSrvLOSAtrDef1Click
    end
    object ExcluiSrvLOSOpeAtrDef1: TMenuItem
      Caption = '&Exclui o atributo selecionado'
      OnClick = ExcluiSrvLOSAtrDef1Click
    end
  end
  object QrSrvLOSOpeAtrDef: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT "srvlosopeatrdef" TABELA, '
      
        'def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ATRITS,' +
        ' '
      'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,'
      'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp'
      'FROM srvlosopeatrdef def'
      'LEFT JOIN srvlosopeatrits its ON its.Controle=def.AtrIts '
      'LEFT JOIN srvlosopeatrcad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc>0'
      ' '
      'UNION  '
      ' '
      'SELECT "srvlosopeatrtxt" TABELA,'
      'def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, '
      'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,'
      'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp '
      'FROM srvlosopeatrtxt def '
      'LEFT JOIN srvlosopeatrcad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc>0'
      ' '
      'ORDER BY NO_CAD, NO_ITS ')
    Left = 568
    Top = 316
    object QrSrvLOSOpeAtrDefID_Item: TIntegerField
      FieldName = 'ID_Item'
      Required = True
    end
    object QrSrvLOSOpeAtrDefID_Sorc: TIntegerField
      FieldName = 'ID_Sorc'
      Required = True
    end
    object QrSrvLOSOpeAtrDefAtrCad: TIntegerField
      FieldName = 'AtrCad'
      Required = True
    end
    object QrSrvLOSOpeAtrDefATRITS: TFloatField
      FieldName = 'ATRITS'
      Required = True
    end
    object QrSrvLOSOpeAtrDefCU_CAD: TIntegerField
      FieldName = 'CU_CAD'
    end
    object QrSrvLOSOpeAtrDefCU_ITS: TLargeintField
      FieldName = 'CU_ITS'
    end
    object QrSrvLOSOpeAtrDefAtrTxt: TWideStringField
      FieldName = 'AtrTxt'
      Size = 255
    end
    object QrSrvLOSOpeAtrDefNO_CAD: TWideStringField
      FieldName = 'NO_CAD'
      Size = 30
    end
    object QrSrvLOSOpeAtrDefNO_ITS: TWideStringField
      FieldName = 'NO_ITS'
      Size = 255
    end
    object QrSrvLOSOpeAtrDefAtrTyp: TSmallintField
      FieldName = 'AtrTyp'
    end
    object QrSrvLOSOpeAtrDefTABELA: TWideStringField
      FieldName = 'TABELA'
      Size = 30
    end
  end
  object DsSrvLOSOpeAtrDef: TDataSource
    DataSet = QrSrvLOSOpeAtrDef
    Left = 568
    Top = 364
  end
  object DsSrvLOSPrz: TDataSource
    DataSet = QrSrvLOSPrz
    Left = 372
    Top = 364
  end
  object QrSrvLOSPrz: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT opz.Parcelas, '
      'opz.Codigo, opz.Controle, opz.Condicao, '
      'opz.DescoPer, opz.Escolhido, ppc.Nome NO_CONDICAO'
      'FROM osprz opz'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=opz.Condicao'
      'WHERE opz.Codigo=:P0')
    Left = 372
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSrvLOSPrzCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osprz.Codigo'
    end
    object QrSrvLOSPrzControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osprz.Controle'
    end
    object QrSrvLOSPrzCondicao: TIntegerField
      FieldName = 'Condicao'
      Origin = 'osprz.Condicao'
    end
    object QrSrvLOSPrzNO_CONDICAO: TWideStringField
      FieldName = 'NO_CONDICAO'
      Origin = 'pediprzcab.Nome'
      Size = 50
    end
    object QrSrvLOSPrzDescoPer: TFloatField
      FieldName = 'DescoPer'
      Origin = 'osprz.DescoPer'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrSrvLOSPrzEscolhido: TSmallintField
      FieldName = 'Escolhido'
      Origin = 'osprz.Escolhido'
      MaxValue = 1
    end
    object QrSrvLOSPrzParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrSrvLOSPrzVAL_COM_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_COM_DESCO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLOSPrzVAL_PARCELA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_PARCELA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLOSPrzORC_COM_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ORC_COM_DESCO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLOSPrzORC_PARCELA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ORC_PARCELA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLOSPrzINV_COM_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'INV_COM_DESCO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrSrvLOSPrzINV_PARCELA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'INV_PARCELA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
  end
  object PMSrvLOSPrz: TPopupMenu
    OnPopup = PMSrvLOSPrzPopup
    Left = 848
    Top = 728
    object OSPrz1_Inclui: TMenuItem
      Caption = '&Adiciona nova condi'#231#227'o de pagamento'
      Enabled = False
      OnClick = OSPrz1_IncluiClick
    end
    object OSPrz1_Altera: TMenuItem
      Caption = '&Edita a condi'#231#227'o de pagamento atual'
      Enabled = False
      OnClick = OSPrz1_AlteraClick
    end
    object OSPrz1_Exclui: TMenuItem
      Caption = '&Remove condi'#231'ao de pagamento atual'
      Enabled = False
      OnClick = OSPrz1_ExcluiClick
    end
  end
  object QrSrvLOSOpeCPA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'ELT(ValEverAut + 1, "N'#227'o", "Sim", "???") NO_ValEverAut,  '
      'ELT(TemInterv + 1, "N'#227'o", "Sim", "???") NO_TemInterv,'
      'ooc.*   '
      'FROM srvlosopecpa ooc '
      'WHERE ooc.Conta=8 ')
    Left = 464
    Top = 316
    object QrSrvLOSOpeCPANO_ValEverAut: TWideStringField
      FieldName = 'NO_ValEverAut'
      Size = 3
    end
    object QrSrvLOSOpeCPANO_TemInterv: TWideStringField
      FieldName = 'NO_TemInterv'
      Size = 3
    end
    object QrSrvLOSOpeCPACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLOSOpeCPAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrvLOSOpeCPAConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrSrvLOSOpeCPANivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrSrvLOSOpeCPADtaCompet: TDateField
      FieldName = 'DtaCompet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSrvLOSOpeCPAHrInnDay: TTimeField
      FieldName = 'HrInnDay'
      DisplayFormat = 'hh:nn'
    end
    object QrSrvLOSOpeCPAHrOutMid: TTimeField
      FieldName = 'HrOutMid'
      DisplayFormat = 'hh:nn'
    end
    object QrSrvLOSOpeCPAHrInnMid: TTimeField
      FieldName = 'HrInnMid'
      DisplayFormat = 'hh:nn'
    end
    object QrSrvLOSOpeCPAHrOutDay: TTimeField
      FieldName = 'HrOutDay'
      DisplayFormat = 'hh:nn'
    end
    object QrSrvLOSOpeCPATemInterv: TSmallintField
      FieldName = 'TemInterv'
    end
    object QrSrvLOSOpeCPAFatorPrm: TFloatField
      FieldName = 'FatorPrm'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrSrvLOSOpeCPAValPremio: TFloatField
      FieldName = 'ValPremio'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSrvLOSOpeCPALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLOSOpeCPADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLOSOpeCPADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLOSOpeCPAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLOSOpeCPAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLOSOpeCPAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLOSOpeCPAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLOSOpeCPAValEverAut: TSmallintField
      FieldName = 'ValEverAut'
    end
  end
  object DsSrvLOSOpeCPA: TDataSource
    DataSet = QrSrvLOSOpeCPA
    Left = 464
    Top = 364
  end
  object PMSrvLOSOpeCPA: TPopupMenu
    OnPopup = PMSrvLOSOpeCPAPopup
    Left = 528
    Top = 728
    object Incluinovajornada1: TMenuItem
      Caption = '&Inclui nova jornada'
      OnClick = Incluinovajornada1Click
    end
    object Alterajornadaatual1: TMenuItem
      Caption = '&Altera jornada atual'
      OnClick = Alterajornadaatual1Click
    end
    object Excluijornadaatual1: TMenuItem
      Caption = '&Exclui jornada atual'
      OnClick = Excluijornadaatual1Click
    end
  end
  object QrEstatusOSs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM estatusoss'
      'ORDER BY Codigo')
    Left = 520
    Top = 4
    object QrEstatusOSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstatusOSsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEstatusOSs: TDataSource
    DataSet = QrEstatusOSs
    Left = 520
    Top = 52
  end
end
