object FmSrvLOSOpeCPA: TFmSrvLOSOpeCPA
  Left = 339
  Top = 185
  Caption = 'LOC-SERVI-018 :: Jornada de Trabalho de Agente'
  ClientHeight = 318
  ClientWidth = 646
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 633
    Width = 646
    Height = 33
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 215
    ExplicitWidth = 617
    ExplicitHeight = 28
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 646
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    ExplicitWidth = 617
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Conta'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 533
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'NO_Agente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 646
    Height = 521
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    ExplicitWidth = 899
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label12: TLabel
      Left = 96
      Top = 16
      Width = 26
      Height = 13
      Caption = 'Data:'
    end
    object Label8: TLabel
      Left = 212
      Top = 16
      Width = 49
      Height = 13
      Caption = 'Honor'#225'rio:'
    end
    object EdNivel4: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object TPDtaCompet: TdmkEditDateTimePicker
      Left = 96
      Top = 32
      Width = 112
      Height = 21
      Date = 40724.768997777780000000
      Time = 40724.768997777780000000
      TabOrder = 1
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'DtAbertura'
      UpdCampo = 'DtAbertura'
      UpdType = utYes
    end
    object GroupBox3: TGroupBox
      Left = 300
      Top = 12
      Width = 305
      Height = 81
      Caption = ' Hor'#225'rio padr'#227'o de trabalho'
      TabOrder = 2
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 139
        Height = 64
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitHeight = 92
        object Label1: TLabel
          Left = 12
          Top = 4
          Width = 40
          Height = 13
          Caption = 'Entrada:'
        end
        object Label4: TLabel
          Left = 76
          Top = 4
          Width = 32
          Height = 13
          Caption = 'Sa'#237'da:'
        end
        object EdHrInnDay: TdmkEdit
          Left = 12
          Top = 20
          Width = 56
          Height = 21
          TabOrder = 0
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'HrInnDay'
          UpdCampo = 'HrInnDay'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdHrOutDay: TdmkEdit
          Left = 76
          Top = 20
          Width = 56
          Height = 21
          TabOrder = 1
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'HrOutDay'
          UpdCampo = 'HrOutDay'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox4: TGroupBox
        Left = 141
        Top = 15
        Width = 149
        Height = 64
        Align = alLeft
        Caption = '                           '
        TabOrder = 1
        ExplicitLeft = 144
        ExplicitTop = 24
        ExplicitHeight = 77
        object CkTemInterv: TdmkCheckBox
          Left = 16
          Top = 0
          Width = 69
          Height = 17
          Caption = ' Intervalo: '
          TabOrder = 0
          OnClick = CkTemIntervClick
          QryCampo = 'TemInterv'
          UpdCampo = 'TemInterv'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object PnIntervalo: TPanel
          Left = 2
          Top = 15
          Width = 145
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          Visible = False
          ExplicitHeight = 75
          object Label2: TLabel
            Left = 80
            Top = 8
            Width = 41
            Height = 13
            Caption = 'Retorno:'
          end
          object Label7: TLabel
            Left = 16
            Top = 8
            Width = 32
            Height = 13
            Caption = 'Sa'#237'da:'
          end
          object EdHrOutMid: TdmkEdit
            Left = 16
            Top = 24
            Width = 56
            Height = 21
            TabOrder = 0
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '00:00'
            QryCampo = 'HrOutMid'
            UpdCampo = 'HrOutMid'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdHrInnMid: TdmkEdit
            Left = 80
            Top = 24
            Width = 56
            Height = 21
            TabOrder = 1
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '00:00'
            QryCampo = 'HrInnMid'
            UpdCampo = 'HrInnMid'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
    object EdValPremio: TdmkEdit
      Left = 212
      Top = 32
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 646
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 617
    object GB_R: TGroupBox
      Left = 598
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 569
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 550
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 521
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 379
        Height = 32
        Caption = 'Jornada de Trabalho de Agente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 379
        Height = 32
        Caption = 'Jornada de Trabalho de Agente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 379
        Height = 32
        Caption = 'Jornada de Trabalho de Agente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 204
    Width = 646
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitTop = 243
    ExplicitWidth = 617
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 642
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 613
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 248
    Width = 646
    Height = 70
    Align = alBottom
    TabOrder = 5
    ExplicitTop = 287
    ExplicitWidth = 617
    object PnSaiDesis: TPanel
      Left = 500
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 471
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 498
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 469
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
end
