object FmSrvLHonEmi: TFmSrvLHonEmi
  Left = 339
  Top = 185
  Caption = 'LOC-SERVI-017 :: Emiss'#227'o de Pagamento de Honor'#225'rios'
  ClientHeight = 370
  ClientWidth = 662
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 662
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 614
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 566
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 465
        Height = 32
        Caption = 'Emiss'#227'o de Pagamento de Honor'#225'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 465
        Height = 32
        Caption = 'Emiss'#227'o de Pagamento de Honor'#225'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 465
        Height = 32
        Caption = 'Emiss'#227'o de Pagamento de Honor'#225'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 48
    Width = 662
    Height = 208
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 662
      Height = 208
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 662
        Height = 105
        Align = alTop
        Caption = ' Dados do cabe'#231'alho:'
        Enabled = False
        TabOrder = 0
        object Label5: TLabel
          Left = 12
          Top = 20
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdControle
        end
        object Label4: TLabel
          Left = 12
          Top = 60
          Width = 37
          Height = 13
          Caption = 'Agente:'
          FocusControl = DBEdAgente
        end
        object Label3: TLabel
          Left = 96
          Top = 60
          Width = 83
          Height = 13
          Caption = 'Nome do Agente:'
          FocusControl = DBEdNO_AGENTE
        end
        object Label8: TLabel
          Left = 72
          Top = 20
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdEmpresa
        end
        object Label9: TLabel
          Left = 156
          Top = 20
          Width = 89
          Height = 13
          Caption = 'Nome da empresa:'
          FocusControl = DBEdNO_EMP
        end
        object DBEdControle: TdmkDBEdit
          Left = 12
          Top = 36
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'CtrlSHA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdAgente: TDBEdit
          Left = 12
          Top = 76
          Width = 80
          Height = 21
          TabStop = False
          DataField = 'Agente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
        object DBEdNO_AGENTE: TDBEdit
          Left = 96
          Top = 76
          Width = 553
          Height = 21
          TabStop = False
          Color = clWhite
          DataField = 'NO_AGENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
        end
        object DBEdEmpresa: TDBEdit
          Left = 72
          Top = 36
          Width = 80
          Height = 21
          TabStop = False
          DataField = 'Filial'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnChange = DBEdEmpresaChange
        end
        object DBEdNO_EMP: TDBEdit
          Left = 156
          Top = 36
          Width = 493
          Height = 21
          TabStop = False
          Color = clWhite
          DataField = 'NO_EMPRESA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 105
        Width = 662
        Height = 100
        Align = alTop
        Caption = ' Dados do item (Patrim'#244'nio principal): '
        TabOrder = 1
        object Label6: TLabel
          Left = 12
          Top = 16
          Width = 51
          Height = 13
          Caption = 'ID do item:'
        end
        object Label1: TLabel
          Left = 268
          Top = 16
          Width = 119
          Height = 13
          Caption = 'Condi'#231#227'o de pagamento:'
        end
        object Label21: TLabel
          Left = 95
          Top = 16
          Width = 117
          Height = 13
          Caption = 'Data / hora faturamento:'
        end
        object Label12: TLabel
          Left = 569
          Top = 16
          Width = 36
          Height = 13
          Caption = '$ Total:'
        end
        object Label7: TLabel
          Left = 12
          Top = 56
          Width = 39
          Height = 13
          Caption = 'Carteira:'
        end
        object Label15: TLabel
          Left = 328
          Top = 56
          Width = 125
          Height = 13
          Caption = 'Conta do plano de contas:'
        end
        object SpeedButton1: TSpeedButton
          Left = 542
          Top = 32
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object EdControle: TdmkEdit
          Left = 12
          Top = 32
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBCondicaoPG: TdmkDBLookupComboBox
          Left = 316
          Top = 32
          Width = 222
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DmFatura.DsPediPrzCab
          TabOrder = 4
          dmkEditCB = EdCondicaoPG
          QryCampo = 'Cargo'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCondicaoPG: TdmkEditCB
          Left = 268
          Top = 32
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cargo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCondicaoPG
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object TPDataFat: TdmkEditDateTimePicker
          Left = 96
          Top = 32
          Width = 112
          Height = 21
          Date = 41131.000000000000000000
          Time = 0.724786689817847200
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataEmi'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdHoraFat: TdmkEdit
          Left = 208
          Top = 32
          Width = 56
          Height = 21
          TabOrder = 2
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfLong
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          QryCampo = 'HoraEmi'
          UpdCampo = 'HoraIni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdValor: TdmkEdit
          Left = 569
          Top = 32
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdCartEmis: TdmkEditCB
          Left = 12
          Top = 72
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cargo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCartEmis
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCartEmis: TdmkDBLookupComboBox
          Left = 68
          Top = 72
          Width = 253
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCartEmis
          TabOrder = 7
          dmkEditCB = EdCartEmis
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdGenero: TdmkEditCB
          Left = 328
          Top = 72
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cargo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBGenero
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGenero: TdmkDBLookupComboBox
          Left = 388
          Top = 72
          Width = 261
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DmFatura.DsCartEmis
          TabOrder = 9
          dmkEditCB = EdGenero
          QryCampo = 'Cargo'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 256
    Width = 662
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 658
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 300
    Width = 662
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 516
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 514
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 10
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object VuCondicaoPG: TdmkValUsu
    dmkEditCB = EdCondicaoPG
    Panel = PnEdita
    QryCampo = 'CondicaoPG'
    UpdCampo = 'CondicaoPG'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 4
    Top = 4
  end
  object QrCartEmis: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome, car.Tipo'
      'FROM carteiras car'
      'WHERE Tipo=2'
      'ORDER BY Nome')
    Left = 308
    Top = 44
    object QrCartEmisCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCartEmisNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCartEmisTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCartEmisForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
  end
  object DsCartEmis: TDataSource
    DataSet = QrCartEmis
    Left = 308
    Top = 92
  end
end
