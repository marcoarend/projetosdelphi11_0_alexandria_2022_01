unit SrvLCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkValUsu,
  dmkCheckBox;

type
  TFmSrvLCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrSrvLCad: TmySQLQuery;
    DsSrvLCad: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrListServ: TmySQLQuery;
    QrListServNome: TWideStringField;
    QrListServCodAlf: TWideStringField;
    DsListServ: TDataSource;
    QrCNAE21Cad: TmySQLQuery;
    QrCNAE21CadCodAlf: TWideStringField;
    QrCNAE21CadNome: TWideStringField;
    DsCNAE21Cad: TDataSource;
    Label18: TLabel;
    Label36: TLabel;
    EdCodigoCnae: TdmkEditCB;
    EdItemListaServico: TdmkEditCB;
    CBItemListaServico: TdmkDBLookupComboBox;
    CBCodigoCnae: TdmkDBLookupComboBox;
    SbItemListaServico: TSpeedButton;
    SbCodigoCnae: TSpeedButton;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    Label6: TLabel;
    SBUnidMed: TSpeedButton;
    VUUnidMed: TdmkValUsu;
    QrSrvLCadNO_LISTASERV: TWideStringField;
    QrSrvLCadNO_CNAE: TWideStringField;
    QrSrvLCadNO_UNIDMED: TWideStringField;
    QrSrvLCadCodigo: TIntegerField;
    QrSrvLCadNome: TWideStringField;
    QrSrvLCadItemListaServico: TWideStringField;
    QrSrvLCadCodigoCnae: TWideStringField;
    QrSrvLCadUnidMed: TIntegerField;
    QrSrvLCadLk: TIntegerField;
    QrSrvLCadDataCad: TDateField;
    QrSrvLCadDataAlt: TDateField;
    QrSrvLCadUserCad: TIntegerField;
    QrSrvLCadUserAlt: TIntegerField;
    QrSrvLCadAlterWeb: TSmallintField;
    QrSrvLCadAtivo: TSmallintField;
    EdUnidMed: TdmkEditCB;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrSrvLCadSIGLAUNIDMED: TWideStringField;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    CkWrnLctDay: TdmkCheckBox;
    QrSrvLCadWrnLctDay: TIntegerField;
    DBCheckBox1: TDBCheckBox;
    QrListServCodigo: TIntegerField;
    QrCNAE21CadCodigo: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSrvLCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSrvLCadBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbItemListaServicoClick(Sender: TObject);
    procedure SbCodigoCnaeClick(Sender: TObject);
    procedure SBUnidMedClick(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmSrvLCad: TFmSrvLCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, NFSe_PF_0000, UnidMed, DmkDAC_PF,
  MyDBCheck, ModProd;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSrvLCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSrvLCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSrvLCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSrvLCad.DefParams;
begin
  VAR_GOTOTABELA := 'srvlcad';
  VAR_GOTOMYSQLTABLE := QrSrvLCad;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT srv.Nome NO_LISTASERV, cna.Nome NO_CNAE, ');
  VAR_SQLx.Add('med.Nome NO_UNIDMED, med.Sigla SIGLAUNIDMED, slc.* ');
  VAR_SQLx.Add('FROM srvlcad slc  ');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.listserv srv ON srv.CodAlf=slc.ItemListaServico ');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.cnae21cad cna ON cna.CodAlf=slc.CodigoCnae ');
  VAR_SQLx.Add('LEFT JOIN unidmed med ON med.Codigo=slc.UnidMed ');
  VAR_SQLx.Add('WHERE slc.Codigo > 0');
  //
  VAR_SQL1.Add('AND slc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND slc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND slc.Nome Like :P0');
  //
end;

procedure TFmSrvLCad.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmSrvLCad.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmSrvLCad.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger)
end;

procedure TFmSrvLCad.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmSrvLCad.EdUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmSrvLCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSrvLCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSrvLCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSrvLCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSrvLCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSrvLCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSrvLCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSrvLCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLCad.BtAlteraClick(Sender: TObject);
begin
  if (QrSrvLCad.State <> dsInactive) and (QrSrvLCad.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrSrvLCad, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'srvlcad');
  end;
end;

procedure TFmSrvLCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSrvLCadCodigo.Value;
  Close;
end;

procedure TFmSrvLCad.BtConfirmaClick(Sender: TObject);
var
  Nome, ItemListaServico, CodigoCnae: String;
  Codigo, UnidMed, WrnLctDay: Integer;
begin
  Codigo           := EdCodigo.ValueVariant;
  Nome             := EdNome.ValueVariant;
  ItemListaServico := EdItemListaServico.ValueVariant;
  CodigoCnae       := EdCodigoCnae.ValueVariant;
  (*UnidMed*)         UMyMod.ObtemCodigoDeCodUsu(EdUnidMed, UnidMed, '');
  WrnLctDay        := Geral.BoolToInt(CkWrnLctDay.Checked);
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('srvlcad', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'srvlcad', False, [
    'Nome', 'ItemListaServico', 'CodigoCnae',
    'UnidMed', 'WrnLctDay'], [
    'Codigo'], [
    Nome, ItemListaServico, CodigoCnae,
    UnidMed, WrnLctDay], [
    Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    //
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmSrvLCad.BtDesisteClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo          := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'srvlcad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmSrvLCad.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrSrvLCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'srvlcad');
end;

procedure TFmSrvLCad.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBDados.Align   := alClient;
  //
  CriaOForm;
  //
  UMyMod.AbreQuery(QrListServ, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrCNAE21Cad, DmodG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
end;

procedure TFmSrvLCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSrvLCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvLCad.SbCodigoCnaeClick(Sender: TObject);
var
  CodAlf: String;
  Codigo: Integer;
begin
  VAR_CADASTROX := '';
  CodAlf        := EdCodigoCnae.ValueVariant;

  if CodAlf <> '' then
    Codigo := QrCNAE21CadCodigo.Value
  else
    Codigo := 0;

  UnNFSe_PF_0000.MostraFormCNAE21Cad(Codigo);

  UMyMod.AbreQuery(QrCNAE21Cad, DmodG.AllID_DB);

  if VAR_CADASTROX <> '' then
  begin
    UMyMod.SetaCodTxtPesquisado(EdCodigoCnae, CBCodigoCnae, QrCNAE21Cad, VAR_CADASTROX, 'CodAlf');
    EdCodigoCnae.SetFocus;
  end;
end;

procedure TFmSrvLCad.SbItemListaServicoClick(Sender: TObject);
var
  CodAlf: String;
  Codigo: Integer;
begin
  VAR_CADASTROX := '';
  CodAlf        := EdItemListaServico.ValueVariant;

  if CodAlf <> '' then
    Codigo := QrListServCodigo.Value
  else
    Codigo := 0;

  UnNFSe_PF_0000.MostraFormListServ(Codigo);

  UMyMod.AbreQuery(QrListServ, DModG.AllID_DB);

  if VAR_CADASTROX <> '' then
  begin
    UMyMod.SetaCodTxtPesquisado(EdItemListaServico, CBItemListaServico,
      QrListServ, VAR_CADASTROX, 'CodAlf');
    EdItemListaServico.SetFocus;
  end;
end;

procedure TFmSrvLCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSrvLCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrSrvLCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvLCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSrvLCad.QrSrvLCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSrvLCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSrvLCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSrvLCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'srvlcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSrvLCad.SBUnidMedClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo       := VUUnidMed.ValueVariant;
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmUnidMed.LocCod(Codigo, Codigo);
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    { N�o presisa no SetaCodUsoDeCodigo
    QrUnidMed.Close;
    UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
    }
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
        'Codigo', 'CodUsu');
      CBUnidMed.SetFocus;
    end;
  end;
end;

procedure TFmSrvLCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLCad.QrSrvLCadBeforeOpen(DataSet: TDataSet);
begin
  QrSrvLCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

