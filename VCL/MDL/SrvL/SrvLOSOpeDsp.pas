unit SrvLOSOpeDsp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, AppListas;

type
  TFmSrvLOSOpeDsp = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    DsFornece: TDataSource;
    QrFornece: TmySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTI: TWideStringField;
    Panel5: TPanel;
    Label6: TLabel;
    EdConta: TdmkEdit;
    Panel3: TPanel;
    LaConta: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    EdGenero: TdmkEditCB;
    CBGenero: TdmkDBLookupComboBox;
    EdAgente: TdmkEditCB;
    CBAgente: TdmkDBLookupComboBox;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    SbConta: TSpeedButton;
    SbAgente: TSpeedButton;
    SbFornece: TSpeedButton;
    QrAgentes: TmySQLQuery;
    DsAgentes: TDataSource;
    QrAgentesCodigo: TIntegerField;
    QrAgentesNO_AGENTE: TWideStringField;
    DBEdControle: TdmkDBEdit;
    Label1: TLabel;
    Label7: TLabel;
    EdDebito: TdmkEdit;
    RGFrmCtbDspFat: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbContaClick(Sender: TObject);
    procedure SbAgenteClick(Sender: TObject);
    procedure SbForneceClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenSrvLOSOpeDsp(Conta: Integer);
    procedure ReopenAgentes(Codigo: Integer);
  public
    { Public declarations }
    FCliente, FLastConta: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmSrvLOSOpeDsp: TFmSrvLOSOpeDsp;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
UnFinanceiroJan, ModuleGeral, UnSrvL_PF;

{$R *.DFM}

procedure TFmSrvLOSOpeDsp.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta, Genero, Agente, Fornece, FrmCtbDspFat: Integer;
  Debito: Double;
  SQLType: TSQLType;
  Incoerente: Boolean;
begin
  SQLType := ImgTipo.SQLType;
  //
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := Geral.IMV(DBEdControle.Text);
  Conta          := EdConta.ValueVariant;
  Genero         := EdGenero.ValueVariant;
  Agente         := EdAgente.ValueVariant;
  Debito         := EdDebito.ValueVariant;
  Fornece        := EdFornece.ValueVariant;
  FrmCtbDspFat   := RGFrmCtbDspFat.ItemIndex;

  Incoerente     := (FrmCtbDspFat = 2) and (Fornece <> FCliente);

  if MyObjects.FIC(Genero = 0, EdGenero, 'Conta (do plano de contas)!') then
    Exit;
  if MyObjects.FIC(Fornece = 0, EdFornece, 'Informe o fornecedor!') then
    Exit;
  if MyObjects.FIC(Debito < 0.01, EdDebito, 'Informe o valor de d�bito!') then
    Exit;
  if MyObjects.FIC(Incoerente, nil,
  'Fornecedor deve ser o cliente para desconto da despesa no faturamento!') then
    Exit;
  //
  if MyObjects.FIC(FrmCtbDspFat = 3, nil, 'Forma de pagamento n�o implementada!' +
    sLineBreak + 'Solicite a libera��o junto a Dermatek!') then Exit;
  //
  Conta := UMyMod.BPGS1I32('srvlosopedsp', 'Conta', '', '', tsPos, SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'srvlosopedsp', False, [
  'Codigo', 'Controle', 'Genero',
  'Agente', 'Debito', 'Fornece',
  'FrmCtbDspFat'], [
  'Conta'], [
  Codigo, Controle, Genero,
  Agente, Debito, Fornece,
  FrmCtbDspFat], [
  Conta], True) then
  begin
    SrvL_PF.AtualizaTotaisSrvOSOpeGrn(Codigo, Controle);
    ReopenSrvLOSOpeDsp(Conta);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdConta.ValueVariant     := 0;
      //
      EdGenero.ValueVariant    := 0;
      CBGenero.KeyValue        := Null;
      EdAgente.ValueVariant    := 0;
      CBAgente.KeyValue        := Null;
      EdFornece.ValueVariant   := 0;
      CBFornece.KeyValue       := Null;
      EdDebito.ValueVariant    := 0;
      //RGFrmCtbDspFat.ItemIndex := 0;
      //
      EdGenero.SetFocus;
    end else Close;
  end;
end;

procedure TFmSrvLOSOpeDsp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLOSOpeDsp.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdControle.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmSrvLOSOpeDsp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenAgentes(0);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  //
  MyObjects.ConfiguraRadioGroup(RGFrmCtbDspFat, sSrvLCtbDsp, 4, 0);
end;

procedure TFmSrvLOSOpeDsp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLOSOpeDsp.ReopenAgentes(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgentes, Dmod.MyDB, [
  'SELECT ent.Codigo,  ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_AGENTE  ',
  'FROM entidades ent ',
  'WHERE ent.' + VAR_FP_FUNCION,
  'ORDER BY NO_AGENTE ',
  ' ']);
  //
  if Codigo <> 0 then
    QrAgentes.Locate('Codigo', Codigo, []);
end;

procedure TFmSrvLOSOpeDsp.ReopenSrvLOSOpeDsp(Conta: Integer);
begin
  FLastConta := Conta;
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Conta <> 0 then
      FQrIts.Locate('Conta', Conta, []);
  end;
end;

procedure TFmSrvLOSOpeDsp.SbAgenteClick(Sender: TObject);
var
  Agente: Integer;
begin
  VAR_CADASTRO := 0;
  Agente       := EdAgente.ValueVariant;

  DModG.CadastroDeEntidade(Agente, fmcadEntidade2, fmcadEntidade2);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdAgente, CBAgente, QrAgentes, VAR_CADASTRO);
    EdAgente.SetFocus;
  end;
end;

procedure TFmSrvLOSOpeDsp.SbContaClick(Sender: TObject);
var
  Genero: Integer;
begin
  VAR_CADASTRO := 0;
  Genero       := EdGenero.ValueVariant;

  FinanceiroJan.CadastroDeContas(Genero);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdGenero, CBGenero, QrContas, VAR_CADASTRO);
    EdGenero.SetFocus;
  end;
end;

procedure TFmSrvLOSOpeDsp.SbForneceClick(Sender: TObject);
var
  Fornece: Integer;
begin
  VAR_CADASTRO := 0;
  Fornece      := EdFornece.ValueVariant;

  DModG.CadastroDeEntidade(Fornece, fmcadEntidade2, fmcadEntidade2);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdFornece, CBFornece, QrFornece, VAR_CADASTRO);
    EdFornece.SetFocus;
  end;
end;

end.
