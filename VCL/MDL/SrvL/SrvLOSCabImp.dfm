object FmSrvLOSCabImp: TFmSrvLOSCabImp
  Left = 0
  Top = 0
  Caption = 'LOC-SERVI-019 :: Impress'#227'o de OS'
  ClientHeight = 259
  ClientWidth = 433
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object frxDsSrvLOSCab: TfrxDBDataset
    UserName = 'frxDsSrvLOSCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'SrvLPre=SrvLPre'
      'SrvLCad=SrvLCad'
      'Cliente=Cliente'
      'DtAbertura=DtAbertura'
      'DtEncerrad=DtEncerrad'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_SrvLCad=NO_SrvLCad'
      'NO_CLIENTE=NO_CLIENTE'
      'DtEncerrad_TXT=DtEncerrad_TXT'
      'Empresa=Empresa'
      'NO_EMPRESA=NO_EMPRESA'
      'ValTotCobr=ValTotCobr'
      'ValTotDesp=ValTotDesp'
      'ValRatPaga=ValRatPaga'
      'ValTotPaga=ValTotPaga'
      'ValTotLiqu=ValTotLiqu'
      'PerTotLiqu=PerTotLiqu'
      'Estatus=Estatus'
      'NO_ESTATUS=NO_ESTATUS')
    DataSet = FmSrvLOSCab.QrSrvLOSCab
    BCDToCurrency = False
    Left = 80
    Top = 56
  end
  object frxDsSrvLOSOpeGrn: TfrxDBDataset
    UserName = 'frxDsSrvLOSOpeGrn'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'SrvLCad=SrvLCad'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Nome=Nome'
      'NO_SrvLCad=NO_SrvLCad'
      'SrvLPrmCad=SrvLPrmCad'
      'NO_SrvLPrmCad=NO_SrvLPrmCad'
      'NO_AgeEquiCab=NO_AgeEquiCab'
      'AgeEqiCab=AgeEqiCab'
      'UnidMedCobr=UnidMedCobr'
      'QtdRlzCobr=QtdRlzCobr'
      'ValFtrCobr=ValFtrCobr'
      'FrmFtrCobr=FrmFtrCobr'
      'UnidMedPaga=UnidMedPaga'
      'QtdRlzPaga=QtdRlzPaga'
      'ValRatPaga=ValRatPaga'
      'FrmRatPaga=FrmRatPaga'
      'FrmFtrPaga=FrmFtrPaga'
      'ValTotCobr=ValTotCobr'
      'NO_FrmRatPaga=NO_FrmRatPaga'
      'SIGLA_UMC=SIGLA_UMC'
      'SIGLA_UMP=SIGLA_UMP'
      'NO_FrmFtrCobr=NO_FrmFtrCobr'
      'NO_FrmFtrPaga=NO_FrmFtrPaga'
      'ValFtrPaga=ValFtrPaga'
      'ValTotDesp=ValTotDesp'
      'DtaExeIni=DtaExeIni'
      'DtaExeFim=DtaExeFim'
      'DtaExeFim_TXT=DtaExeFim_TXT'
      'WrnLctDay=WrnLctDay'
      'ValTotPaga=ValTotPaga'
      'AgeEqiCfg=AgeEqiCfg'
      'Autorizado=Autorizado')
    DataSet = FmSrvLOSCab.QrSrvLOSOpeGrn
    BCDToCurrency = False
    Left = 80
    Top = 104
  end
  object frxLOC_SERVI_019_01: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.635407800900000000
    ReportOptions.LastChange = 40429.473596203700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxLOC_SERVI_019_01GetValue
    Left = 80
    Top = 8
    Datasets = <
      item
        DataSet = frxDsSrvLOSCab
        DataSetName = 'frxDsSrvLOSCab'
      end
      item
        DataSet = frxDsSrvLOSOpeGrn
        DataSetName = 'frxDsSrvLOSOpeGrn'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 491.338900000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo23: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 355.275820000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Top = 18.897650000000000000
        Width = 680.315400000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 90.708720000000000000
        Top = 79.370130000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSrvLOSCab
        DataSetName = 'frxDsSrvLOSCab'
        RowCount = 0
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 90.708720000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSrvLOSCab."NO_EMPRESA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 68.031540000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 68.031540000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 68.031540000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'ORDEM DE SERVI'#199'O N'#176' [frxDsSrvLOSCab."Codigo"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 665.197280000000000000
          Height = 49.133890000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ENDERECO_EMP]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Top = 68.031540000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSrvLOSOpeGrn."Codigo"'
        object Memo14: TfrxMemoView
          Left = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Localizador')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'A?')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 71.811070000000000000
          Width = 151.181102362204700000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Servi'#231'o')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 222.992270000000000000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Modalidade')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496062992130000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor servi'#231'o')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 529.134200000000000000
          Width = 83.149606299212600000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 461.102660000000000000
          Width = 68.031496062992130000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 385.512060000000000000
          Width = 75.590551181102360000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor modal')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Memo27: TfrxMemoView
          Left = 597.165740000000000000
          Width = 83.149625830000000000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSrvLOSOpeGrn."ValTotCobr">,DetailData2)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 514.016080000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 105.826840000000000000
        Top = 192.756030000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Memo1: TfrxMemoView
          Left = 45.354360000000000000
          Top = 68.031540000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSrvLOSCab."DtAbertura"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 68.031540000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Abertura:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 207.874150000000000000
          Top = 68.031540000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSrvLOSCab."DtEncerrad_TXT"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 139.842610000000000000
          Top = 68.031540000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Encerramento:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 45.354360000000000000
          Top = 86.929190000000000000
          Width = 634.961040000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSrvLOSCab."NO_SrvLCad"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Top = 86.929190000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Servi'#231'o:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 336.378170000000000000
          Top = 68.031540000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSrvLOSCab."ValTotCobr"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 302.362400000000000000
          Top = 68.031540000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor:')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 49.133858270000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ENDERECO_CLI]')
          ParentFont = False
        end
        object frxDsSrvLOSCabNO_CLIENTE: TfrxMemoView
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSCab
          DataSetName = 'frxDsSrvLOSCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente: [frxDsSrvLOSCab."NO_CLIENTE"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 445.984540000000000000
          Top = 68.031540000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSrvLOSCab."NO_ESTATUS"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 408.189240000000000000
          Top = 68.031540000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status:')
          ParentFont = False
        end
      end
      object DetailData2: TfrxDetailData
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSrvLOSOpeGrn
        DataSetName = 'frxDsSrvLOSOpeGrn'
        RowCount = 0
        object CheckBox1: TfrxCheckBoxView
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'Autorizado'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo13: TfrxMemoView
          Left = 18.897650000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."Controle"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 71.811070000000000000
          Width = 151.181102362204700000
          Height = 18.897650000000000000
          DataField = 'NO_SrvLCad'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."NO_SrvLCad"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 222.992270000000000000
          Width = 162.519755830000000000
          Height = 18.897650000000000000
          DataField = 'NO_SrvLPrmCad'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."NO_SrvLPrmCad"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496062992130000
          Height = 18.897650000000000000
          DataField = 'ValTotCobr'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."ValTotCobr"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 529.134200000000000000
          Width = 83.149606299212600000
          Height = 18.897650000000000000
          DataField = 'SIGLA_UMC'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."SIGLA_UMC"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 461.102660000000000000
          Width = 68.031496062992130000
          Height = 18.897650000000000000
          DataField = 'QtdRlzCobr'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."QtdRlzCobr"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 385.512060000000000000
          Width = 75.590551181102360000
          Height = 18.897650000000000000
          DataField = 'ValFtrCobr'
          DataSet = frxDsSrvLOSOpeGrn
          DataSetName = 'frxDsSrvLOSOpeGrn'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSrvLOSOpeGrn."ValFtrCobr"]')
          ParentFont = False
        end
      end
    end
  end
end
