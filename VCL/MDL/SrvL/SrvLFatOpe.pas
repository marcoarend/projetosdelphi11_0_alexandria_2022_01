unit SrvLFatOpe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO,
  AppListas, dmkRadioGroup;

type
  TFmSrvLFatOpe = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrSrvLOSOpeGrn: TmySQLQuery;
    QrSrvLOSOpeGrnCodigo: TIntegerField;
    QrSrvLOSOpeGrnControle: TIntegerField;
    QrSrvLOSOpeGrnSrvLCad: TIntegerField;
    QrSrvLOSOpeGrnLk: TIntegerField;
    QrSrvLOSOpeGrnDataCad: TDateField;
    QrSrvLOSOpeGrnDataAlt: TDateField;
    QrSrvLOSOpeGrnUserCad: TIntegerField;
    QrSrvLOSOpeGrnUserAlt: TIntegerField;
    QrSrvLOSOpeGrnAlterWeb: TSmallintField;
    QrSrvLOSOpeGrnAtivo: TSmallintField;
    QrSrvLOSOpeGrnNome: TWideStringField;
    QrSrvLOSOpeGrnNO_SrvLCad: TWideStringField;
    QrSrvLOSOpeGrnSrvLPrmCad: TIntegerField;
    QrSrvLOSOpeGrnNO_SrvLPrmCad: TWideStringField;
    QrSrvLOSOpeGrnNO_AgeEquiCab: TWideStringField;
    QrSrvLOSOpeGrnAgeEqiCab: TIntegerField;
    QrSrvLOSOpeGrnUnidMedCobr: TIntegerField;
    QrSrvLOSOpeGrnQtdRlzCobr: TFloatField;
    QrSrvLOSOpeGrnValFtrCobr: TFloatField;
    QrSrvLOSOpeGrnFrmFtrCobr: TSmallintField;
    QrSrvLOSOpeGrnUnidMedPaga: TIntegerField;
    QrSrvLOSOpeGrnQtdRlzPaga: TFloatField;
    QrSrvLOSOpeGrnValRatPaga: TFloatField;
    QrSrvLOSOpeGrnFrmRatPaga: TSmallintField;
    QrSrvLOSOpeGrnFrmFtrPaga: TSmallintField;
    QrSrvLOSOpeGrnValTotCobr: TFloatField;
    QrSrvLOSOpeGrnNO_FrmRatPaga: TWideStringField;
    QrSrvLOSOpeGrnSIGLA_UMC: TWideStringField;
    QrSrvLOSOpeGrnSIGLA_UMP: TWideStringField;
    QrSrvLOSOpeGrnNO_FrmFtrCobr: TWideStringField;
    QrSrvLOSOpeGrnNO_FrmFtrPaga: TWideStringField;
    QrSrvLOSOpeGrnValFtrPaga: TFloatField;
    QrSrvLOSOpeGrnValTotDesp: TFloatField;
    DsSrvLOSOpeGrn: TDataSource;
    DGDados: TdmkDBGridZTO;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    Panel3: TPanel;
    RGFrmCtbDspFat: TdmkRadioGroup;
    Panel5: TPanel;
    Label5: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label3: TLabel;
    DBEdNome: TDBEdit;
    Label1: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    DBEdNO_Empresa: TDBEdit;
    Label4: TLabel;
    DBEdCliente: TdmkDBEdit;
    DBEdNO_Cliente: TDBEdit;
    QrSrvLOSOpeGrnDtAbertura: TDateTimeField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenSrvLFatOpe(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FCliente, FCodigo: Integer;
    //
    procedure ReopeSrvLOSOpeGrn();
  end;

  var
  FmSrvLFatOpe: TFmSrvLFatOpe;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnDmkProcFunc, UnSrvL_PF;

{$R *.DFM}

procedure TFmSrvLFatOpe.BtNenhumClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(TDBGrid(DGDados), False);
end;

procedure TFmSrvLFatOpe.BtOKClick(Sender: TObject);
var
  Controles, Lista: String;
  SrvLFatCab, FrmCtbDspFat: Integer;
  Qry: TmySQLQuery;
begin
  SrvLFatCab     := FCodigo;
  FrmCtbDspFat   := RGFrmCtbDspFat.ItemIndex;
  Controles      :=
    MyObjects.GetSelecionadosBookmark_Int1_ToStr(Self, TDBGrid(DGDados), 'Controle');
  if MyObjects.FIC(Controles = '', nil, 'Nenhum item foi selecionado!') then
    Exit;
  case TSrvLCtbDsp(FrmCtbDspFat) of
    (*0*)slcdUnknownToInf:
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UndmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM srvlosopedsp ',
        'WHERE FrmCtbDspFat=' + Geral.FF0(Integer(slcdUnknownToInf)), // Ainda nao definido!
        'AND Controle IN (' + Controles + ') ',
        '']);
        if MyObjects.FIC(Qry.RecordCount > 0, nil, 'Informe "' +
        RGFrmCtbDspFat.Caption + '"') then
          Exit;
      finally
        Qry.Free;
      end;
    end;
    (*1*)slcdNoGerCtaAPag: ;
    (*2*)slcdDescoFaturam:
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UndmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM srvlosopedsp ',
        'WHERE FrmCtbDspFat=' + Geral.FF0(Integer(slcdUnknownToInf)), // Ainda nao definido!
        'AND Fornece<>' + Geral.FF0(FCliente),
        'AND Controle IN (' + Controles + ') ',
        '']);
        if Qry.RecordCount > 0 then
        begin
          Lista := '';
          Qry.First;
          while not Qry.Eof do
          begin
            Lista := 'ID = ' + Geral.FF0(Qry.FieldByName('Conta').AsInteger) + sLineBreak;
            Qry.Next;
          end;
          Geral.MB_Aviso(
          'Fornecedor deve ser o cliente para desconto das despesas no faturamento!'
          + sLineBreak + Geral.FF0(Qry.RecordCount) +
          ' lan�amentos de d�bito deve ser corrigidos!' + sLineBreak +
          Lista);
          Exit;
        end;
      finally
        Qry.Free;
      end;
    end;
    (*3*)slcdGeraCtaAPagr: ;
    else
      Geral.MB_Erro('"TSrvLCtbDsp" n�o implementado em "BtOKClick()"');
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopegrn', False, [
  'SrvLFatCab'], [
  CO_JOKE_SQL], [
  SrvLFatCab], [
  'Controle IN (' + Controles + ')'], True) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopedsp', False, [
    'FrmCtbDspFat'], [
    CO_JOKE_SQL], [
    FrmCtbDspFat], [
    'Controle IN (' + Controles + ') AND FrmCtbDspFat=0'], True) then
    begin
      SrvL_PF.TotalizaSrvlFatCab(FCodigo);
      ReopenSrvLFatOpe(0);
      Close;
    end;
  end;
end;

procedure TFmSrvLFatOpe.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLFatOpe.BtTudoClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(TDBGrid(DGdados), True);
end;

procedure TFmSrvLFatOpe.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdNome.DataSource       := FDsCab;
  DBEdEmpresa.DataSource    := FDsCab;
  DBEdNO_Empresa.DataSource := FDsCab;
  DBEdCliente.DataSource    := FDsCab;
  DBEdNO_Cliente.DataSource := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmSrvLFatOpe.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.ConfiguraRadioGroup(RGFrmCtbDspFat, sSrvLCtbDsp, 1, 0);
end;

procedure TFmSrvLFatOpe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLFatOpe.ReopenSrvLFatOpe(Controle: Integer);
begin
  if FQrCab <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrCab, FQrCab.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmSrvLFatOpe.ReopeSrvLOSOpeGrn();
var
  ATT_FrmFtrCobr, ATT_FrmFtrPaga, ATT_FrmRatPaga: String;
begin
  //
  ATT_FrmFtrCobr := dmkPF.ArrayToTexto('oog.FrmFtrCobr', 'NO_FrmFtrCobr',
    pvPos, True, sPrmFatorValor);
  ATT_FrmFtrPaga := dmkPF.ArrayToTexto('oog.FrmFtrPaga', 'NO_FrmFtrPaga',
    pvPos, True, sPrmFatorValor);
  ATT_FrmRatPaga := dmkPF.ArrayToTexto('oog.FrmRatPaga', 'NO_FrmRatPaga',
    pvPos, True, sPrmFormaRateio);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLOSOpeGrn, Dmod.MyDB, [
  'SELECT slc.Nome NO_SrvLCad, prm.Nome NO_SrvLPrmCad,  ',
  ATT_FrmFtrCobr,
  ATT_FrmFtrPaga,
  ATT_FrmRatPaga,
  'umc.Sigla SIGLA_UMC, ump.Sigla SIGLA_UMP, ',
  'aec.Nome NO_AgeEquiCab, ooc.DtAbertura, oog.*  ',
  'FROM srvlosopegrn oog ',
  'LEFT JOIN srvlcad slc ON slc.Codigo=oog.SrvLCad ',
  'LEFT JOIN srvlprmcad prm ON prm.Codigo=oog.SrvLPrmCad ',
  'LEFT JOIN ageeqicab aec ON oog.AgeEqiCab=aec.Codigo ',
  'LEFT JOIN unidmed umc ON umc.Codigo=oog.UnidMedCobr ',
  'LEFT JOIN unidmed ump ON ump.Codigo=oog.UnidMedPaga ',
  'LEFT JOIN srvloscab ooc ON ooc.Codigo=oog.Codigo ',
  'WHERE ooc.Empresa=' + Geral.FF0(FEmpresa),
  'AND ooc.Cliente=' + Geral.FF0(FCliente),
  'AND oog.SrvLFatCab=0 ',
  'ORDER BY Controle DESC ',
  ' ']);
end;

end.
