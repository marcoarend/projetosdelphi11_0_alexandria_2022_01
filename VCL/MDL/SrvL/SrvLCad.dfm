object FmSrvLCad: TFmSrvLCad
  Left = 368
  Top = 194
  Caption = 'LOC-SERVI-001 :: Cadastro de Servi'#231'os'
  ClientHeight = 426
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 217
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 99
        Height = 13
        Caption = 'Servi'#231'o (LC 116/03):'
      end
      object Label4: TLabel
        Left = 16
        Top = 96
        Width = 83
        Height = 13
        Caption = 'C'#243'digo do CNAE:'
      end
      object Label5: TLabel
        Left = 16
        Top = 136
        Width = 117
        Height = 13
        Caption = 'Unidade de Medida [F3]:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsSrvLCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsSrvLCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'ItemListaServico'
        DataSource = DsSrvLCad
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 72
        Top = 72
        Width = 696
        Height = 21
        DataField = 'NO_LISTASERV'
        DataSource = DsSrvLCad
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 72
        Top = 112
        Width = 696
        Height = 21
        DataField = 'NO_CNAE'
        DataSource = DsSrvLCad
        TabOrder = 5
      end
      object DBEdit4: TDBEdit
        Left = 112
        Top = 152
        Width = 656
        Height = 21
        DataField = 'NO_UNIDMED'
        DataSource = DsSrvLCad
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        DataField = 'CodigoCnae'
        DataSource = DsSrvLCad
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 72
        Top = 152
        Width = 40
        Height = 21
        DataField = 'SIGLAUNIDMED'
        DataSource = DsSrvLCad
        TabOrder = 7
      end
      object DBEdit7: TDBEdit
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        DataField = 'UnidMed'
        DataSource = DsSrvLCad
        TabOrder = 8
      end
      object DBCheckBox1: TDBCheckBox
        Left = 16
        Top = 180
        Width = 109
        Height = 17
        Caption = 'Empreita incerta.'
        DataField = 'WrnLctDay'
        DataSource = DsSrvLCad
        TabOrder = 9
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 266
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 229
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label18: TLabel
        Left = 16
        Top = 56
        Width = 99
        Height = 13
        Caption = 'Servi'#231'o (LC 116/03):'
      end
      object Label36: TLabel
        Left = 16
        Top = 96
        Width = 83
        Height = 13
        Caption = 'C'#243'digo do CNAE:'
      end
      object SbItemListaServico: TSpeedButton
        Left = 747
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbItemListaServicoClick
      end
      object SbCodigoCnae: TSpeedButton
        Left = 747
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbCodigoCnaeClick
      end
      object Label6: TLabel
        Left = 16
        Top = 136
        Width = 117
        Height = 13
        Caption = 'Unidade de Medida [F3]:'
      end
      object SBUnidMed: TSpeedButton
        Left = 747
        Top = 151
        Width = 21
        Height = 22
        Caption = '...'
        OnClick = SBUnidMedClick
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodigoCnae: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'CodigoCnae'
        UpdCampo = 'CodigoCnae'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        DBLookupComboBox = CBCodigoCnae
        IgnoraDBLookupComboBox = False
      end
      object EdItemListaServico: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'ItemListaServico'
        UpdCampo = 'ItemListaServico'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        DBLookupComboBox = CBItemListaServico
        IgnoraDBLookupComboBox = False
      end
      object CBItemListaServico: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 673
        Height = 21
        KeyField = 'CodAlf'
        ListField = 'Nome'
        ListSource = DsListServ
        TabOrder = 3
        dmkEditCB = EdItemListaServico
        QryCampo = 'ItemListaServico'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBCodigoCnae: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 673
        Height = 21
        KeyField = 'CodAlf'
        ListField = 'Nome'
        ListSource = DsCNAE21Cad
        TabOrder = 5
        dmkEditCB = EdCodigoCnae
        QryCampo = 'CodigoCnae'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdUnidMed: TdmkEditCB
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdUnidMedChange
        OnKeyDown = EdUnidMedKeyDown
        DBLookupComboBox = CBUnidMed
        IgnoraDBLookupComboBox = False
      end
      object EdSigla: TdmkEdit
        Left = 72
        Top = 152
        Width = 40
        Height = 21
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdSiglaChange
        OnExit = EdSiglaExit
        OnKeyDown = EdSiglaKeyDown
      end
      object CBUnidMed: TdmkDBLookupComboBox
        Left = 112
        Top = 152
        Width = 633
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsUnidMed
        TabOrder = 8
        OnKeyDown = EdUnidMedKeyDown
        dmkEditCB = EdUnidMed
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CkWrnLctDay: TdmkCheckBox
        Left = 16
        Top = 180
        Width = 113
        Height = 17
        Caption = 'Empreita incerta.'
        TabOrder = 9
        QryCampo = 'WrnLctDay'
        UpdCampo = 'WrnLctDay'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 267
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 261
        Height = 32
        Caption = 'Cadastro de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 261
        Height = 32
        Caption = 'Cadastro de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 261
        Height = 32
        Caption = 'Cadastro de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrSrvLCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrSrvLCadBeforeOpen
    AfterOpen = QrSrvLCadAfterOpen
    SQL.Strings = (
      'SELECT srv.Nome NO_LISTASERV, cna.Nome NO_CNAE,  '
      'med.Nome NO_UNIDMED, slc.*  '
      'FROM toolrent.srvlcad slc '
      
        'LEFT JOIN loctoolrall.listserv srv ON srv.CodAlf=slc.ItemListaSe' +
        'rvico '
      
        'LEFT JOIN loctoolrall.cnae21cad cna ON cna.CodAlf=slc.CodigoCnae' +
        ' '
      'LEFT JOIN toolrent.unidmed med ON med.Codigo=slc.UnidMed  ')
    Left = 56
    Top = 4
    object QrSrvLCadNO_LISTASERV: TWideStringField
      FieldName = 'NO_LISTASERV'
      Size = 255
    end
    object QrSrvLCadNO_CNAE: TWideStringField
      FieldName = 'NO_CNAE'
      Size = 255
    end
    object QrSrvLCadNO_UNIDMED: TWideStringField
      FieldName = 'NO_UNIDMED'
      Size = 30
    end
    object QrSrvLCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrSrvLCadItemListaServico: TWideStringField
      FieldName = 'ItemListaServico'
      Size = 5
    end
    object QrSrvLCadCodigoCnae: TWideStringField
      FieldName = 'CodigoCnae'
    end
    object QrSrvLCadUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrSrvLCadLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLCadUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLCadAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLCadSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
    end
    object QrSrvLCadWrnLctDay: TIntegerField
      FieldName = 'WrnLctDay'
    end
  end
  object DsSrvLCad: TDataSource
    DataSet = QrSrvLCad
    Left = 56
    Top = 48
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrListServ: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, CodAlf, Nome'
      'FROM listserv'
      'ORDER BY Nome')
    Left = 412
    Top = 72
    object QrListServNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrListServCodAlf: TWideStringField
      FieldName = 'CodAlf'
      Required = True
    end
    object QrListServCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsListServ: TDataSource
    DataSet = QrListServ
    Left = 412
    Top = 120
  end
  object QrCNAE21Cad: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, CodAlf, Nome'
      'FROM cnae21Cad'
      'ORDER BY Nome')
    Left = 580
    Top = 72
    object QrCNAE21CadCodAlf: TWideStringField
      FieldName = 'CodAlf'
    end
    object QrCNAE21CadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCNAE21CadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsCNAE21Cad: TDataSource
    DataSet = QrCNAE21Cad
    Left = 580
    Top = 120
  end
  object QrUnidMed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 704
    Top = 72
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 704
    Top = 116
  end
  object VUUnidMed: TdmkValUsu
    dmkEditCB = EdUnidMed
    Panel = PnEdita
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 200
    Top = 64
  end
end
