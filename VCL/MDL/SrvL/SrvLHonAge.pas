unit SrvLHonAge;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO,
  AppListas, dmkRadioGroup;

type
  TFmSrvLHonAge = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    Panel5: TPanel;
    Label5: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label3: TLabel;
    DBEdNome: TDBEdit;
    Label1: TLabel;
    DBEdEmpresa: TdmkDBEdit;
    DBEdNO_Empresa: TDBEdit;
    GroupBox3: TGroupBox;
    Panel3: TPanel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENT: TWideStringField;
    QrClientesCIDADE: TWideStringField;
    QrClientesNOMEUF: TWideStringField;
    QrClientesCodUsu: TIntegerField;
    QrClientesIE: TWideStringField;
    DsClientes: TDataSource;
    Label17: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrAgentes: TmySQLQuery;
    QrAgentesCodigo: TIntegerField;
    QrAgentesNO_AGENTE: TWideStringField;
    DsAgentes: TDataSource;
    Label4: TLabel;
    EdAgente: TdmkEditCB;
    CBAgente: TdmkDBLookupComboBox;
    QrSrvLOSOpeAge: TmySQLQuery;
    DsSrvLOSOpeAge: TDataSource;
    DBGOSAge: TdmkDBGridZTO;
    Label2: TLabel;
    EdSrvLCad: TdmkEditCB;
    CBSrvLCad: TdmkDBLookupComboBox;
    QrSrvLCad: TmySQLQuery;
    QrSrvLCadCodigo: TIntegerField;
    QrSrvLCadNome: TWideStringField;
    DsSrvLCad: TDataSource;
    BtReabre: TBitBtn;
    QrSrvLOSOpeAgeNO_SRVL: TWideStringField;
    QrSrvLOSOpeAgeNO_AGENTE: TWideStringField;
    QrSrvLOSOpeAgeCodigo: TIntegerField;
    QrSrvLOSOpeAgeControle: TIntegerField;
    QrSrvLOSOpeAgeConta: TIntegerField;
    QrSrvLOSOpeAgeAgente: TIntegerField;
    QrSrvLOSOpeAgeResponsa: TSmallintField;
    QrSrvLOSOpeAgeValPremio: TFloatField;
    QrSrvLOSOpeAgeLk: TIntegerField;
    QrSrvLOSOpeAgeDataCad: TDateField;
    QrSrvLOSOpeAgeDataAlt: TDateField;
    QrSrvLOSOpeAgeUserCad: TIntegerField;
    QrSrvLOSOpeAgeUserAlt: TIntegerField;
    QrSrvLOSOpeAgeAlterWeb: TSmallintField;
    QrSrvLOSOpeAgeAtivo: TSmallintField;
    QrSrvLOSOpeAgeSrvLHonCab: TIntegerField;
    QrSrvLOSOpeAgeGruAddSeq: TIntegerField;
    QrSrvLOSOpeAgeDtAbertura: TDateTimeField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure SbAgenteClick(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdAgenteChange(Sender: TObject);
    procedure EdSrvLCadChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure QrSrvLOSOpeAgeAfterOpen(DataSet: TDataSet);
    procedure QrSrvLOSOpeAgeBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenSrvLFatOpe(Controle: Integer);
    procedure FechaPesquisa();
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, FCliente, FCodigo: Integer;
    //
    procedure ReopenSrvLOSOpeAge();
  end;

  var
  FmSrvLHonAge: TFmSrvLHonAge;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnDmkProcFunc, UnSrvL_PF, ModuleGeral;

{$R *.DFM}

procedure TFmSrvLHonAge.BtNenhumClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(TDBGrid(DBGOSAge), False);
end;

procedure TFmSrvLHonAge.BtOKClick(Sender: TObject);
var
  Contas, Lista: String;
  SrvLHonCab: Integer;
  Qry: TmySQLQuery;
begin
  SrvLHonCab  := FCodigo;
  Contas      :=
    MyObjects.GetSelecionadosBookmark_Int1_ToStr(Self, TDBGrid(DBGOSAge), 'Conta');
  if MyObjects.FIC(Contas = '', nil, 'Nenhum item foi selecionado!') then
    Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopeage', False, [
  'SrvLHonCab'], [
  CO_JOKE_SQL], [
  SrvLHonCab], [
  'Conta IN (' + Contas + ')'], True) then
  begin
    SrvL_PF.VerificaNovosAgentesSrvlHon(FCodigo);
    //
    SrvL_PF.TotalizaSrvLHonCab(FCodigo);
    ReopenSrvLFatOpe(0);
    Close;
  end;


{
  SrvLFatCab     := FCodigo;
  FrmCtbDspFat   := RGFrmCtbDspFat.ItemIndex;
  Controles      :=
    MyObjects.GetSelecionadosBookmark_Int1_ToStr(Self, TDBGrid(DGDados), 'Controle');
  if MyObjects.FIC(Controles = '', nil, 'Nenhum item foi selecionado!') then
    Exit;
  case TSrvLCtbDsp(FrmCtbDspFat) of
    (*0*)slcdUnknownToInf:
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UndmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM srvlosopedsp ',
        'WHERE FrmCtbDspFat=' + Geral.FF0(Integer(slcdUnknownToInf)), // Ainda nao definido!
        'AND Controle IN (' + Controles + ') ',
        '']);
        if MyObjects.FIC(Qry.RecordCount > 0, nil, 'Informe "' +
        RGFrmCtbDspFat.Caption + '"') then
          Exit;
      finally
        Qry.Free;
      end;
    end;
    (*1*)slcdNoGerCtaAPag: ;
    (*2*)slcdDescoFaturam:
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UndmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM srvlosopedsp ',
        'WHERE FrmCtbDspFat=' + Geral.FF0(Integer(slcdUnknownToInf)), // Ainda nao definido!
        'AND Fornece<>' + Geral.FF0(FCliente),
        'AND Controle IN (' + Controles + ') ',
        '']);
        if Qry.RecordCount > 0 then
        begin
          Lista := '';
          Qry.First;
          while not Qry.Eof do
          begin
            Lista := 'ID = ' + Geral.FF0(Qry.FieldByName('Conta').AsInteger) + sLineBreak;
            Qry.Next;
          end;
          Geral.MB_Aviso(
          'Fornecedor deve ser o cliente para desconto das despesas no faturamento!'
          + sLineBreak + Geral.FF0(Qry.RecordCount) +
          ' lan�amentos de d�bito deve ser corrigidos!' + sLineBreak +
          Lista);
          Exit;
        end;
      finally
        Qry.Free;
      end;
    end;
    (*3*)slcdGeraCtaAPagr: ;
    else
      Geral.MB_Erro('"TSrvLCtbDsp" n�o implementado em "BtOKClick()"');
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopegrn', False, [
  'SrvLFatCab'], [
  CO_JOKE_SQL], [
  SrvLFatCab], [
  'Controle IN (' + Controles + ')'], True) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'srvlosopedsp', False, [
    'FrmCtbDspFat'], [
    CO_JOKE_SQL], [
    FrmCtbDspFat], [
    'Controle IN (' + Controles + ') AND FrmCtbDspFat=0'], True) then
    begin
      SrvL_PF.TotalizaSrvlFatCab(FCodigo);
      ReopenSrvLFatOpe(0);
      Close;
    end;
  end;
}
end;

procedure TFmSrvLHonAge.BtReabreClick(Sender: TObject);
begin
  ReopenSrvLOSOpeAge();
end;

procedure TFmSrvLHonAge.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLHonAge.BtTudoClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(TDBGrid(DBGOSAge), True);
end;

procedure TFmSrvLHonAge.EdAgenteChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmSrvLHonAge.EdClienteChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmSrvLHonAge.EdSrvLCadChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmSrvLHonAge.FechaPesquisa;
begin
  QrSrvLOSOpeAge.Close;
end;

procedure TFmSrvLHonAge.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource     := FDsCab;
  DBEdNome.DataSource       := FDsCab;
  DBEdEmpresa.DataSource    := FDsCab;
  DBEdNO_Empresa.DataSource := FDsCab;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmSrvLHonAge.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrAgentes, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgentes, Dmod.MyDB, [
  'SELECT ent.Codigo,  ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_AGENTE  ',
  'FROM entidades ent ',
  'WHERE ent.' + VAR_FP_FUNCION,
  'ORDER BY NO_AGENTE ',
  ' ']);
  UnDmkDAC_PF.AbreQuery(QrSrvLCad, Dmod.MyDB);
end;

procedure TFmSrvLHonAge.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLHonAge.QrSrvLOSOpeAgeAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita         := QrSrvLOSOpeAge.RecordCount > 0;
  BtOK.Enabled     := Habilita;
  BtTudo.Enabled   := Habilita;
  BtNenhum.Enabled := Habilita;
end;

procedure TFmSrvLHonAge.QrSrvLOSOpeAgeBeforeClose(DataSet: TDataSet);
begin
  BtOK.Enabled := False;
  BtTudo.Enabled := False;
  BtNenhum.Enabled := False;
end;

procedure TFmSrvLHonAge.ReopenSrvLFatOpe(Controle: Integer);
begin
  if FQrCab <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrCab, FQrCab.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmSrvLHonAge.ReopenSrvLOSOpeAge();
var
  SQLAgente, SQLCliente, SQLSrvLCad: String;
  Agente, Cliente, SrvLCad: Integer;
begin
  Agente   := EdAgente.ValueVariant;
  Cliente  := EdCliente.ValueVariant;
  SrvLCad  := EdSrvLCad.ValueVariant;
  //
  if Agente <> 0 then
    SQLAgente  := 'AND age.Agente=' + Geral.FF0(Agente)
  else
    SQLAgente  := '';
  //
  if Cliente <> 0 then
    SQLCliente := 'AND ooc.Cliente=' + Geral.FF0(Cliente)
  else
    SQLCliente := '';
  //
  if SrvLCad <> 0 then
    SQLSrvLCad := 'AND oog.SrvLCad=' + Geral.FF0(SrvLCad)
  else
    SQLSrvLCad := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLOSOpeAge, Dmod.MyDB, [
  'SELECT slc.Nome NO_SRVL,  ',
  'ent.Nome NO_AGENTE, ooc.DtAbertura, age.*  ',
  'FROM srvlosopeage age  ',
  'LEFT JOIN srvlosopegrn oog on oog.Controle=age.Controle ',
  'LEFT JOIN srvloscab ooc on ooc.Codigo=age.Codigo ',
  'LEFT JOIN srvlcad slc ON slc.Codigo=ooc.SrvLCad ',
  'LEFT JOIN entidades ent on ent.Codigo=age.Agente  ',
  'WHERE age.SrvLHonCab=0  ',
  'AND ooc.Empresa=' + Geral.FF0(FEmpresa),
  SQLAgente,
  SQLCliente,
  SQLSrvLCad,
  '']);
end;

procedure TFmSrvLHonAge.SbAgenteClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(
    EdAgente.ValueVariant, fmcadEntidade2, fmcadEntidade2);
end;

end.
