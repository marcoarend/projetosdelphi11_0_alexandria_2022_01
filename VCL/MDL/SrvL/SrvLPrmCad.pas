unit SrvLPrmCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, Vcl.ComCtrls,
  dmkEditDateTimePicker, dmkValUsu;

type
  TFmSrvLPrmCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    QrSrvLPrmCad: TmySQLQuery;
    DsSrvLPrmCad: TDataSource;
    QrSrvLPrmNvs: TmySQLQuery;
    DsSrvLPrmNvs: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrSrvLPrmNvsCodigo: TIntegerField;
    QrSrvLPrmNvsControle: TIntegerField;
    QrSrvLPrmNvsQtdMaxCobr: TFloatField;
    QrSrvLPrmNvsValFtrCobr: TFloatField;
    QrSrvLPrmNvsQtdMaxPaga: TFloatField;
    QrSrvLPrmNvsValRatPaga: TFloatField;
    QrSrvLPrmNvsLk: TIntegerField;
    QrSrvLPrmNvsDataCad: TDateField;
    QrSrvLPrmNvsDataAlt: TDateField;
    QrSrvLPrmNvsUserCad: TIntegerField;
    QrSrvLPrmNvsUserAlt: TIntegerField;
    QrSrvLPrmNvsAlterWeb: TSmallintField;
    QrSrvLPrmNvsAtivo: TSmallintField;
    QrSrvLPrmCadCodigo: TIntegerField;
    QrSrvLPrmCadNome: TWideStringField;
    QrSrvLPrmCadSrvLCad: TIntegerField;
    QrSrvLPrmCadDtValIni: TDateField;
    QrSrvLPrmCadDtValFim: TDateField;
    QrSrvLPrmCadLk: TIntegerField;
    QrSrvLPrmCadDataCad: TDateField;
    QrSrvLPrmCadDataAlt: TDateField;
    QrSrvLPrmCadUserCad: TIntegerField;
    QrSrvLPrmCadUserAlt: TIntegerField;
    QrSrvLPrmCadAlterWeb: TSmallintField;
    QrSrvLPrmCadAtivo: TSmallintField;
    QrSrvLPrmCadNO_SrvLCad: TWideStringField;
    DsSrvLCad: TDataSource;
    QrSrvLCad: TmySQLQuery;
    QrSrvLCadCodigo: TIntegerField;
    QrSrvLCadNome: TWideStringField;
    EdSrvLCad: TdmkEditCB;
    Label3: TLabel;
    CBSrvLCad: TdmkDBLookupComboBox;
    SBSrvLCad: TSpeedButton;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    TPDtValIni: TdmkEditDateTimePicker;
    Label4: TLabel;
    TPDtValFim: TdmkEditDateTimePicker;
    Label5: TLabel;
    Panel8: TPanel;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    Label12: TLabel;
    SBUnidMed: TSpeedButton;
    EdUnidMedCobr: TdmkEditCB;
    EdSiglaCobr: TdmkEdit;
    CBUnidMedCobr: TdmkDBLookupComboBox;
    RGFrmFtrCobr: TdmkRadioGroup;
    GroupBox4: TGroupBox;
    Panel10: TPanel;
    Label13: TLabel;
    SpeedButton5: TSpeedButton;
    EdUnidMedPaga: TdmkEditCB;
    EdSiglaPaga: TdmkEdit;
    CBUnidMedPaga: TdmkDBLookupComboBox;
    RGFrmRatPaga: TdmkRadioGroup;
    QrUnidMedCobr: TmySQLQuery;
    QrUnidMedCobrCodigo: TIntegerField;
    QrUnidMedCobrCodUsu: TIntegerField;
    QrUnidMedCobrSigla: TWideStringField;
    QrUnidMedCobrNome: TWideStringField;
    DsUnidMedCobr: TDataSource;
    QrUnidMedPaga: TmySQLQuery;
    QrUnidMedPagaCodigo: TIntegerField;
    QrUnidMedPagaCodUsu: TIntegerField;
    QrUnidMedPagaSigla: TWideStringField;
    QrUnidMedPagaNome: TWideStringField;
    DsUnidMedPaga: TDataSource;
    VUUnidMedCobr: TdmkValUsu;
    VUUnidMedPaga: TdmkValUsu;
    QrSrvLPrmCadUnidMedCobr: TIntegerField;
    QrSrvLPrmCadFrmFtrPaga: TSmallintField;
    QrSrvLPrmCadUnidMedPaga: TIntegerField;
    QrSrvLPrmCadFrmRatPaga: TSmallintField;
    QrSrvLPrmCadSIGLA_UMC: TWideStringField;
    QrSrvLPrmCadSIGLA_UMP: TWideStringField;
    QrSrvLPrmCadNO_FrmFtrCobr: TWideStringField;
    QrSrvLPrmCadNO_FrmRatPaga: TWideStringField;
    Panel11: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Panel7: TPanel;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    GroupBox5: TGroupBox;
    Panel12: TPanel;
    GroupBox6: TGroupBox;
    Panel13: TPanel;
    Label14: TLabel;
    DBEdit5: TDBEdit;
    QrSrvLPrmCadNO_UMC: TWideStringField;
    QrSrvLPrmCadNO_UMP: TWideStringField;
    DBEdit6: TDBEdit;
    Label16: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label15: TLabel;
    DBEdit9: TDBEdit;
    Label17: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    QrSrvLPrmNvsUnidMedCobr: TIntegerField;
    QrSrvLPrmNvsFrmFtrCobr: TSmallintField;
    QrSrvLPrmNvsUnidMedPaga: TIntegerField;
    QrSrvLPrmNvsFrmRatPaga: TSmallintField;
    QrSrvLPrmNvsSIGLA_UMC: TWideStringField;
    QrSrvLPrmNvsSIGLA_UMP: TWideStringField;
    QrSrvLPrmNvsNO_FrmFtrCobr: TWideStringField;
    QrSrvLPrmNvsNO_FrmRatPaga: TWideStringField;
    QrSrvLPrmCadNO_FrmFtrPaga: TWideStringField;
    QrSrvLPrmNvsNO_FrmFtrPaga: TWideStringField;
    RGFrmFtrPaga: TdmkRadioGroup;
    QrSrvLPrmCadFrmFtrCobr: TSmallintField;
    QrSrvLPrmNvsFrmFtrPaga: TIntegerField;
    EdNome: TdmkEdit;
    Label9: TLabel;
    QrSrvLCadUnidMed: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSrvLPrmCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSrvLPrmCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrSrvLPrmCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrSrvLPrmCadBeforeClose(DataSet: TDataSet);
    procedure SBSrvLCadClick(Sender: TObject);
    procedure EdUnidMedCobrChange(Sender: TObject);
    procedure EdUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaCobrChange(Sender: TObject);
    procedure EdSiglaCobrExit(Sender: TObject);
    procedure EdSiglaCobrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUnidMedPagaChange(Sender: TObject);
    procedure EdUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaPagaChange(Sender: TObject);
    procedure EdSiglaPagaExit(Sender: TObject);
    procedure EdSiglaPagaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBUnidMedClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure CBSrvLCadExit(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormSrvLPrmNvs(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenSrvLPrmNvs(Controle: Integer);

  end;

var
  FmSrvLPrmCad: TFmSrvLPrmCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, SrvLPrmNvs, UnSrvL_PF,
  ModuleGeral, AppListas, ModProd, UnGrade_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSrvLPrmCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSrvLPrmCad.MostraFormSrvLPrmNvs(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmSrvLPrmNvs, FmSrvLPrmNvs, afmoNegarComAviso) then
  begin
    FmSrvLPrmNvs.ImgTipo.SQLType := SQLType;
    FmSrvLPrmNvs.FQrCab := QrSrvLPrmCad;
    FmSrvLPrmNvs.FDsCab := DsSrvLPrmCad;
    FmSrvLPrmNvs.FQrIts := QrSrvLPrmNvs;
    if SQLType = stIns then
    begin
      FmSrvLPrmNvs.RGFrmFtrCobr.ItemIndex     := QrSrvLPrmCadFrmFtrCobr.Value;
      FmSrvLPrmNvs.VUUnidMedCobr.ValueVariant := QrSrvLPrmCadUnidMedCobr.Value;
      FmSrvLPrmNvs.EdQtdMaxCobr.ValueVariant  := 999999999;
      //
      FmSrvLPrmNvs.RGFrmFtrPaga.ItemIndex     := QrSrvLPrmCadFrmFtrPaga.Value;
      FmSrvLPrmNvs.VUUnidMedPaga.ValueVariant := QrSrvLPrmCadUnidMedPaga.Value;
      FmSrvLPrmNvs.EdQtdMaxPaga.ValueVariant  := 999999999;
      FmSrvLPrmNvs.RGFrmRatPaga.ItemIndex     := QrSrvLPrmCadFrmRatPaga.Value;
    end else
    begin
      FmSrvLPrmNvs.EdControle.ValueVariant := QrSrvLPrmNvsControle.Value;
      //
      FmSrvLPrmNvs.EdValFtrCobr.ValueVariant := QrSrvLPrmNvsValFtrCobr.Value;
      FmSrvLPrmNvs.EdQtdMaxCobr.ValueVariant := QrSrvLPrmNvsQtdMaxCobr.Value;
      //
      FmSrvLPrmNvs.EdValRatPaga.ValueVariant := QrSrvLPrmNvsValRatPaga.Value;
      FmSrvLPrmNvs.EdQtdMaxPaga.ValueVariant := QrSrvLPrmNvsQtdMaxPaga.Value;
      //
      FmSrvLPrmNvs.EdUnidMedCobr.ValueVariant := QrSrvLPrmNvsUnidMedCobr.Value;
      FmSrvLPrmNvs.CBUnidMedCobr.KeyValue     := QrSrvLPrmNvsUnidMedCobr.Value;
      FmSrvLPrmNvs.RGFrmFtrCobr.ItemIndex     := QrSrvLPrmNvsFrmFtrCobr.Value;
      FmSrvLPrmNvs.EdUnidMedPaga.ValueVariant := QrSrvLPrmNvsUnidMedPaga.Value;
      FmSrvLPrmNvs.CBUnidMedPaga.KeyValue     := QrSrvLPrmNvsUnidMedPaga.Value;
      FmSrvLPrmNvs.RGFrmRatPaga.ItemIndex     := QrSrvLPrmNvsFrmRatPaga.Value;
      FmSrvLPrmNvs.RGFrmFtrPaga.ItemIndex     := QrSrvLPrmNvsFrmFtrPaga.Value;
    end;
    FmSrvLPrmNvs.ShowModal;
    FmSrvLPrmNvs.Destroy;
  end;
end;

procedure TFmSrvLPrmCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrSrvLPrmCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrSrvLPrmCad, QrSrvLPrmNvs);
end;

procedure TFmSrvLPrmCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrSrvLPrmCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrSrvLPrmNvs);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrSrvLPrmNvs);
end;

procedure TFmSrvLPrmCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSrvLPrmCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSrvLPrmCad.DefParams;
var
  ATT_FrmFtrCobr, ATT_FrmFtrPaga, ATT_FrmRatPaga: String;
begin
  ATT_FrmFtrCobr := dmkPF.ArrayToTexto('prm.FrmFtrCobr', 'NO_FrmFtrCobr',
    pvPos, True, sPrmFatorValor);
  ATT_FrmFtrPaga := dmkPF.ArrayToTexto('prm.FrmFtrPaga', 'NO_FrmFtrPaga',
    pvPos, True, sPrmFatorValor);
  ATT_FrmRatPaga := dmkPF.ArrayToTexto('prm.FrmRatPaga', 'NO_FrmRatPaga',
    pvPos, True, sPrmFormaRateio);
  //
  VAR_GOTOTABELA := 'srvlprmcad';
  VAR_GOTOMYSQLTABLE := QrSrvLPrmCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT prm.*, slc.Nome NO_SrvLCad, ');
  VAR_SQLx.Add(ATT_FrmFtrCobr);
  VAR_SQLx.Add(ATT_FrmRatPaga);
  VAR_SQLx.Add(ATT_FrmFtrPaga);
  VAR_SQLx.Add('umc.Nome NO_UMC, ump.Nome NO_UMP, ');
  VAR_SQLx.Add('umc.Sigla SIGLA_UMC, ump.Sigla SIGLA_UMP ');
  VAR_SQLx.Add('FROM srvlprmcad prm ');
  VAR_SQLx.Add('LEFT JOIN srvlcad slc ON slc.Codigo=prm.SrvLCad ');
  VAR_SQLx.Add('LEFT JOIN unidmed umc ON umc.Codigo=prm.UnidMedCobr ');
  VAR_SQLx.Add('LEFT JOIN unidmed ump ON ump.Codigo=prm.UnidMedPaga ');
  VAR_SQLx.Add('WHERE prm.Codigo > 0 ');
  //
  VAR_SQL1.Add('AND prm.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND prm.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND prm.Nome Like :P0');
  //
end;

procedure TFmSrvLPrmCad.EdSiglaCobrChange(Sender: TObject);
begin
  if EdSiglaCobr.Focused then
    DmProd.PesquisaPorSigla(False, EdSiglaCobr, EdUnidMedCobr, CBUnidMedCobr);
  //
  //LaQtdMaxCobr.Caption := 'M�ximo de ' + EdSiglaCobr.Text + ':';
end;

procedure TFmSrvLPrmCad.EdSiglaCobrExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSiglaCobr, EdUnidMedCobr, CBUnidMedCobr);
end;

procedure TFmSrvLPrmCad.EdSiglaCobrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCobr, CBUnidMedCobr, dmktfInteger)
end;

procedure TFmSrvLPrmCad.EdSiglaPagaChange(Sender: TObject);
begin
  if EdSiglaPaga.Focused then
    DmProd.PesquisaPorSigla(False, EdSiglaPaga, EdUnidMedPaga, CBUnidMedPaga);
  //
  //LaQtdMaxPaga.Caption := 'M�ximo de ' + EdSiglaPaga.Text + ':';
end;

procedure TFmSrvLPrmCad.EdSiglaPagaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSiglaPaga, EdUnidMedPaga, CBUnidMedPaga);
end;

procedure TFmSrvLPrmCad.EdSiglaPagaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedPaga, CBUnidMedPaga, dmktfInteger);
end;

procedure TFmSrvLPrmCad.EdUnidMedCobrChange(Sender: TObject);
begin
  if not EdSiglaCobr.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMedCobr.ValueVariant, EdSiglaCobr);
end;

procedure TFmSrvLPrmCad.EdUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCobr, CBUnidMedCobr, dmktfInteger);
end;

procedure TFmSrvLPrmCad.EdUnidMedPagaChange(Sender: TObject);
begin
  if not EdSiglaPaga.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMedPaga.ValueVariant, EdSiglaPaga);
end;

procedure TFmSrvLPrmCad.EdUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedPaga, CBUnidMedPaga, dmktfInteger);
end;

procedure TFmSrvLPrmCad.ItsAltera1Click(Sender: TObject);
begin
  MostraFormSrvLPrmNvs(stUpd);
end;

procedure TFmSrvLPrmCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmSrvLPrmCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSrvLPrmCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSrvLPrmCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'srvlprmnvs', 'Controle', QrSrvLPrmNvsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrSrvLPrmNvs,
      QrSrvLPrmNvsControle, QrSrvLPrmNvsControle.Value);
    ReopenSrvLPrmNvs(Controle);
  end;
end;

procedure TFmSrvLPrmCad.ReopenSrvLPrmNvs(Controle: Integer);
var
  ATT_FrmFtrCobr, ATT_FrmFtrPaga, ATT_FrmRatPaga: String;
begin
  ATT_FrmFtrCobr := dmkPF.ArrayToTexto('nvs.FrmFtrCobr', 'NO_FrmFtrCobr',
    pvPos, True, sPrmFatorValor);
  ATT_FrmFtrPaga := dmkPF.ArrayToTexto('nvs.FrmFtrPaga', 'NO_FrmFtrPaga',
    pvPos, True, sPrmFatorValor);
  ATT_FrmRatPaga := dmkPF.ArrayToTexto('nvs.FrmRatPaga', 'NO_FrmRatPaga',
    pvPos, True, sPrmFormaRateio);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLPrmNvs, Dmod.MyDB, [
  'SELECT nvs.*, ',
  ATT_FrmFtrCobr,
  ATT_FrmFtrPaga,
  ATT_FrmRatPaga,
  'umc.Sigla SIGLA_UMC, ump.Sigla SIGLA_UMP ',
  'FROM srvlprmnvs nvs ',
  'LEFT JOIN unidmed umc ON umc.Codigo=nvs.UnidMedCobr ',
  'LEFT JOIN unidmed ump ON ump.Codigo=nvs.UnidMedPaga ',
  'WHERE nvs.Codigo=' + Geral.FF0(QrSrvLPrmCadCodigo.Value),
  '']);
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrvLPrmNvs, Dmod.MyDB, [
  'SELECT nvs.* ',
  'FROM srvlprmnvs nvs  ',
  'WHERE nvs.Codigo=' + Geral.FF0(QrSrvLPrmCadCodigo.Value),
  '']);
*)
  //
  //Geral.MB_SQL(Self, QrSrvLPrmNvs);
  //
  QrSrvLPrmNvs.Locate('Controle', Controle, []);
end;


procedure TFmSrvLPrmCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSrvLPrmCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSrvLPrmCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSrvLPrmCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSrvLPrmCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSrvLPrmCad.SpeedButton5Click(Sender: TObject);
begin
  Grade_PF.CadastroEDefinicaoDeUnidMed(
    VUUnidMedPaga, EdUnidMedPaga, CBUnidMedPaga, QrUnidMedPaga);
end;

procedure TFmSrvLPrmCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSrvLPrmCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSrvLPrmCadCodigo.Value;
  Close;
end;

procedure TFmSrvLPrmCad.ItsInclui1Click(Sender: TObject);
begin
  MostraFormSrvLPrmNvs(stIns);
end;

procedure TFmSrvLPrmCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrSrvLPrmCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'srvlprmcad');
end;

procedure TFmSrvLPrmCad.BtConfirmaClick(Sender: TObject);
var
  Nome, DtValIni, DtValFim: String;
  Codigo, SrvLCad, UnidMedCobr, FrmFtrCobr, UnidMedPaga, FrmRatPaga,
  FrmFtrPaga: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  SrvLCad        := EdSrvLCad.ValueVariant;
  DtValIni       := Geral.FDT(TPDtValIni.Date, 1);
  DtValFim       := Geral.FDT(TPDtValFim.Date, 1);
  //
  //
  UnidMedCobr    := VUUnidMedCobr.ValueVariant;
  FrmFtrCobr     := RGFrmFtrCobr.ItemIndex;
  //
  UnidMedPaga    := VUUnidMedPaga.ValueVariant;
  FrmRatPaga     := RGFrmRatPaga.ItemIndex;
  FrmFtrPaga     := RGFrmFtrPaga.ItemIndex;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(SrvLCad = 0, EdSrvLCad, 'Defina o servi�o!') then Exit;
  if MyObjects.FIC(TPDtValIni.Date < 2, TPDtValIni, 'Defina a data inicial!') then Exit;
  if MyObjects.FIC(TPDtValFim.Date < 2, TPDtValFim, 'Defina a data final!') then Exit;
  if MyObjects.FIC(TPDtValFim.Date < TPDtValIni.Date, TPDtValFim,
    'A data final deve ser superior a inicial!') then Exit;
  //
  if MyObjects.FIC(FrmFtrCobr < 1, RGFrmFtrCobr,
  'Informe o " Fator de cobran�a"') then
    Exit;
  //
  if MyObjects.FIC(FrmFtrPaga < 1, RGFrmFtrPaga,
  'Informe o " Fator de pagamento"') then
    Exit;
  //
  if MyObjects.FIC(FrmRatPaga < 1, RGFrmRatPaga,
  'Informe a "Forma de rateio"') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('srvlprmcad', 'Codigo', '', '', tsPos,
    ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'srvlprmcad', False, [
  'Nome', 'SrvLCad', 'DtValIni',
  'DtValFim', 'UnidMedCobr', 'FrmFtrCobr',
  'UnidMedPaga', 'FrmRatPaga', 'FrmFtrPaga'], [
  'Codigo'], [
  Nome, SrvLCad, DtValIni,
  DtValFim, UnidMedCobr, FrmFtrCobr,
  UnidMedPaga, FrmRatPaga, FrmFtrPaga], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmSrvLPrmCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'srvlprmcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'srvlprmcad', 'Codigo');
end;

procedure TFmSrvLPrmCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmSrvLPrmCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmSrvLPrmCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  MyObjects.ConfiguraRadioGroup(RGFrmFtrCobr, sPrmFatorValor, 3, 0);
  MyObjects.ConfiguraRadioGroup(RGFrmFtrPaga, sPrmFatorValor, 3, 0);
  MyObjects.ConfiguraRadioGroup(RGFrmRatPaga, sPrmFormaRateio, 3, 0);
  //
  UnDmkDAC_PF.AbreQuery(QrSrvLCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMedCobr, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMedPaga, Dmod.MyDB);
end;

procedure TFmSrvLPrmCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSrvLPrmCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvLPrmCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSrvLPrmCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrSrvLPrmCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSrvLPrmCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSrvLPrmCad.QrSrvLPrmCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSrvLPrmCad.QrSrvLPrmCadAfterScroll(DataSet: TDataSet);
begin
  ReopenSrvLPrmNvs(0);
end;

procedure TFmSrvLPrmCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrSrvLPrmCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmSrvLPrmCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSrvLPrmCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'srvlprmcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSrvLPrmCad.SBSrvLCadClick(Sender: TObject);
var
  SrvLCad: Integer;
begin
  VAR_CADASTRO := 0;
  SrvLCad      := EdSrvLCad.ValueVariant;

  SrvL_PF.MostraFormSrvLCad(SrvLCad);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdSrvLCad, CBSrvLCad, QrSrvLCad, VAR_CADASTRO);
    EdSrvLCad.SetFocus;
  end;
end;

procedure TFmSrvLPrmCad.SBUnidMedClick(Sender: TObject);
begin
  Grade_PF.CadastroEDefinicaoDeUnidMed(
    VUUnidMedCobr, EdUnidMedCobr, CBUnidMedCobr, QrUnidMedCobr);
end;

procedure TFmSrvLPrmCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSrvLPrmCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrSrvLPrmCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'srvlprmcad');
  //
  TPDtValIni.Date := DmodG.ObtemAgora();
  TPDtValFim.Date := EncodeDate(9999, 12, 31);
end;

procedure TFmSrvLPrmCad.CBSrvLCadExit(Sender: TObject);
var
  Nome: String;
  UnidMed: Integer;
begin
  Nome    := QrSrvLCadNome.Value;
  UnidMed := QrSrvLCadUnidMed.Value;
  //
  if EdNome.ValueVariant = '' then
    EdNome.ValueVariant := Nome;

  if EdUnidMedCobr.ValueVariant = 0 then
    VUUnidMedCobr.ValueVariant := UnidMed;

  if EdUnidMedPaga.ValueVariant = 0 then
      VUUnidMedPaga.ValueVariant := UnidMed;
end;

procedure TFmSrvLPrmCad.CBUnidMedCobrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedCobr, CBUnidMedCobr, dmktfInteger);
end;

procedure TFmSrvLPrmCad.CBUnidMedPagaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMedPaga, CBUnidMedPaga, dmktfInteger);
end;

procedure TFmSrvLPrmCad.QrSrvLPrmCadBeforeClose(
  DataSet: TDataSet);
begin
  QrSrvLPrmNvs.Close;
end;

procedure TFmSrvLPrmCad.QrSrvLPrmCadBeforeOpen(DataSet: TDataSet);
begin
  QrSrvLPrmCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

