object FmSrvLPrmCad: TFmSrvLPrmCad
  Left = 368
  Top = 194
  Caption = 'LOC-SERVI-006 :: Cadastro de Pr'#234'mios'
  ClientHeight = 591
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 495
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 277
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel11: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 146
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 68
          Top = 0
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label6: TLabel
          Left = 8
          Top = 40
          Width = 39
          Height = 13
          Caption = 'Servi'#231'o:'
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsSrvLPrmCad
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 68
          Top = 16
          Width = 640
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsSrvLPrmCad
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 56
          Width = 56
          Height = 21
          DataField = 'SrvLCad'
          DataSource = DsSrvLPrmCad
          TabOrder = 2
        end
        object DBEdit2: TDBEdit
          Left = 68
          Top = 56
          Width = 640
          Height = 21
          DataField = 'NO_SrvLCad'
          DataSource = DsSrvLPrmCad
          TabOrder = 3
        end
        object GroupBox2: TGroupBox
          Left = 12
          Top = 80
          Width = 696
          Height = 65
          Caption = ' Per'#237'odo de validade do pr'#234'mio: '
          TabOrder = 4
          object Label11: TLabel
            Left = 472
            Top = 16
            Width = 37
            Height = 13
            Caption = 'DtValIni'
          end
          object Panel7: TPanel
            Left = 2
            Top = 15
            Width = 692
            Height = 48
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label8: TLabel
              Left = 64
              Top = 7
              Width = 55
              Height = 13
              Caption = 'Data inicial:'
            end
            object Label10: TLabel
              Left = 256
              Top = 7
              Width = 48
              Height = 13
              Caption = 'Data final:'
            end
            object DBEdit3: TDBEdit
              Left = 64
              Top = 23
              Width = 186
              Height = 21
              DataField = 'DtValIni'
              DataSource = DsSrvLPrmCad
              TabOrder = 0
            end
            object DBEdit4: TDBEdit
              Left = 256
              Top = 23
              Width = 186
              Height = 21
              DataField = 'DtValFim'
              DataSource = DsSrvLPrmCad
              TabOrder = 1
            end
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 2
        Top = 161
        Width = 780
        Height = 56
        Align = alTop
        Caption = ' Dados de cobran'#231'a do cliente:'
        TabOrder = 1
        object Panel12: TPanel
          Left = 2
          Top = 15
          Width = 776
          Height = 39
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label14: TLabel
            Left = 12
            Top = 0
            Width = 90
            Height = 13
            Caption = 'Fator de cobran'#231'a:'
            FocusControl = DBEdit5
          end
          object Label16: TLabel
            Left = 408
            Top = 0
            Width = 95
            Height = 13
            Caption = 'Unidade de medida:'
            FocusControl = DBEdit7
          end
          object DBEdit5: TDBEdit
            Left = 12
            Top = 16
            Width = 392
            Height = 21
            DataField = 'NO_FrmFtrCobr'
            DataSource = DsSrvLPrmCad
            TabOrder = 0
          end
          object DBEdit6: TDBEdit
            Left = 504
            Top = 16
            Width = 204
            Height = 21
            DataField = 'NO_UMC'
            DataSource = DsSrvLPrmCad
            TabOrder = 1
          end
          object DBEdit7: TDBEdit
            Left = 408
            Top = 16
            Width = 56
            Height = 21
            DataField = 'UnidMedCobr'
            DataSource = DsSrvLPrmCad
            TabOrder = 2
          end
          object DBEdit8: TDBEdit
            Left = 464
            Top = 16
            Width = 40
            Height = 21
            DataField = 'SIGLA_UMC'
            DataSource = DsSrvLPrmCad
            TabOrder = 3
          end
        end
      end
      object GroupBox6: TGroupBox
        Left = 2
        Top = 217
        Width = 780
        Height = 56
        Align = alTop
        Caption = ' Dados de pagamento de funcion'#225'rios:'
        TabOrder = 2
        object Panel13: TPanel
          Left = 2
          Top = 15
          Width = 776
          Height = 39
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label15: TLabel
            Left = 12
            Top = 0
            Width = 82
            Height = 13
            Caption = ' Forma de rateio: '
            FocusControl = DBEdit9
          end
          object Label17: TLabel
            Left = 408
            Top = 0
            Width = 95
            Height = 13
            Caption = 'Unidade de medida:'
            FocusControl = DBEdit10
          end
          object DBEdit9: TDBEdit
            Left = 12
            Top = 16
            Width = 392
            Height = 21
            DataField = 'NO_FrmRatPaga'
            DataSource = DsSrvLPrmCad
            TabOrder = 0
          end
          object DBEdit10: TDBEdit
            Left = 408
            Top = 16
            Width = 56
            Height = 21
            DataField = 'UnidMedPaga'
            DataSource = DsSrvLPrmCad
            TabOrder = 1
          end
          object DBEdit11: TDBEdit
            Left = 464
            Top = 16
            Width = 40
            Height = 21
            DataField = 'SIGLA_UMP'
            DataSource = DsSrvLPrmCad
            TabOrder = 2
          end
          object DBEdit12: TDBEdit
            Left = 504
            Top = 16
            Width = 204
            Height = 21
            DataField = 'NO_UMP'
            DataSource = DsSrvLPrmCad
            TabOrder = 3
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 431
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 418
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 414
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Item'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 277
      Width = 784
      Height = 80
      Align = alTop
      DataSource = DsSrvLPrmNvs
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FrmFtrCobr'
          Title.Caption = 'Fator de cobran'#231'a'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SIGLA_UMC'
          Title.Caption = 'Unidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValFtrCobr'
          Title.Caption = 'Valor cobra'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdMaxCobr'
          Title.Caption = 'M'#225'ximo cobra'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FrmFtrPaga'
          Title.Caption = 'Fator de pagamento'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SIGLA_UMP'
          Title.Caption = 'Unidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValRatPaga'
          Title.Caption = 'Valor paga'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdMaxPaga'
          Title.Caption = 'M'#225'ximo paga'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FrmRatPaga'
          Title.Caption = 'Forma de rateio'
          Width = 92
          Visible = True
        end>
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 495
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object Panel8: TPanel
      Left = 0
      Top = 169
      Width = 784
      Height = 164
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 57
        Align = alTop
        Caption = ' Dados de cobran'#231'a do cliente:'
        TabOrder = 0
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 40
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label12: TLabel
            Left = 404
            Top = 0
            Width = 117
            Height = 13
            Caption = 'Unidade de Medida [F3]:'
          end
          object SBUnidMed: TSpeedButton
            Left = 684
            Top = 16
            Width = 21
            Height = 22
            Caption = '...'
            OnClick = SBUnidMedClick
          end
          object EdUnidMedCobr: TdmkEditCB
            Left = 404
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdUnidMedCobrChange
            OnKeyDown = EdUnidMedCobrKeyDown
            DBLookupComboBox = CBUnidMedCobr
            IgnoraDBLookupComboBox = False
          end
          object EdSiglaCobr: TdmkEdit
            Left = 460
            Top = 16
            Width = 40
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdSiglaCobrChange
            OnExit = EdSiglaCobrExit
            OnKeyDown = EdSiglaCobrKeyDown
          end
          object CBUnidMedCobr: TdmkDBLookupComboBox
            Left = 500
            Top = 16
            Width = 184
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsUnidMedCobr
            TabOrder = 3
            OnKeyDown = CBUnidMedCobrKeyDown
            dmkEditCB = EdUnidMedCobr
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object RGFrmFtrCobr: TdmkRadioGroup
            Left = 6
            Top = -1
            Width = 392
            Height = 37
            Caption = ' Fator de cobran'#231'a: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'pfcIndefinido=0, '
              'pfcValorTotal=1, '
              'pfcValorUnitario=2')
            TabOrder = 0
            QryCampo = 'FrmFtrCobr'
            UpdCampo = 'FrmFtrCobr'
            UpdType = utYes
            OldValor = 0
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 57
        Width = 784
        Height = 96
        Align = alTop
        Caption = ' Dados de pagamento de funcion'#225'rios:'
        TabOrder = 1
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 79
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label13: TLabel
            Left = 404
            Top = 0
            Width = 117
            Height = 13
            Caption = 'Unidade de Medida [F3]:'
          end
          object SpeedButton5: TSpeedButton
            Left = 684
            Top = 16
            Width = 21
            Height = 22
            Caption = '...'
            OnClick = SpeedButton5Click
          end
          object EdUnidMedPaga: TdmkEditCB
            Left = 404
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdUnidMedPagaChange
            OnKeyDown = EdUnidMedPagaKeyDown
            DBLookupComboBox = CBUnidMedPaga
            IgnoraDBLookupComboBox = False
          end
          object EdSiglaPaga: TdmkEdit
            Left = 460
            Top = 16
            Width = 40
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdSiglaPagaChange
            OnExit = EdSiglaPagaExit
            OnKeyDown = EdSiglaPagaKeyDown
          end
          object CBUnidMedPaga: TdmkDBLookupComboBox
            Left = 500
            Top = 16
            Width = 184
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsUnidMedPaga
            TabOrder = 3
            OnKeyDown = CBUnidMedPagaKeyDown
            dmkEditCB = EdUnidMedPaga
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object RGFrmRatPaga: TdmkRadioGroup
            Left = 8
            Top = 40
            Width = 392
            Height = 37
            Caption = ' Forma de rateio: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'pfrIndefinido=0, '
              'pfrValorTotal=1, '
              'pfrValorPorPessoa=2')
            TabOrder = 4
            QryCampo = 'FrmRatPaga'
            UpdCampo = 'FrmRatPaga'
            UpdType = utYes
            OldValor = 0
          end
          object RGFrmFtrPaga: TdmkRadioGroup
            Left = 8
            Top = 0
            Width = 392
            Height = 37
            Caption = ' Fator de pagamento: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'pfcIndefinido=0, '
              'pfcValorTotal=1, '
              'pfcValorUnitario=2')
            TabOrder = 0
            QryCampo = 'FrmFtrPaga'
            UpdCampo = 'FrmFtrPaga'
            UpdType = utYes
            OldValor = 0
          end
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 432
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 169
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 12
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 75
        Top = 16
        Width = 39
        Height = 13
        Caption = 'Servi'#231'o:'
      end
      object SBSrvLCad: TSpeedButton
        Left = 686
        Top = 31
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBSrvLCadClick
      end
      object Label9: TLabel
        Left = 12
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSrvLCad: TdmkEditCB
        Left = 75
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SrvLCad'
        UpdCampo = 'SrvLCad'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSrvLCad
        IgnoraDBLookupComboBox = False
      end
      object CBSrvLCad: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 554
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSrvLCad
        TabOrder = 2
        OnExit = CBSrvLCadExit
        dmkEditCB = EdSrvLCad
        QryCampo = 'SrvLCad'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object GroupBox1: TGroupBox
        Left = 12
        Top = 96
        Width = 696
        Height = 65
        Caption = ' Per'#237'odo de validade do pr'#234'mio: '
        TabOrder = 4
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 692
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label4: TLabel
            Left = 64
            Top = 7
            Width = 55
            Height = 13
            Caption = 'Data inicial:'
          end
          object Label5: TLabel
            Left = 256
            Top = 7
            Width = 48
            Height = 13
            Caption = 'Data final:'
          end
          object TPDtValIni: TdmkEditDateTimePicker
            Left = 64
            Top = 23
            Width = 186
            Height = 21
            Date = 42189.613065706020000000
            Time = 42189.613065706020000000
            TabOrder = 0
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DtValIni'
            UpdCampo = 'DtValIni'
            UpdType = utYes
          end
          object TPDtValFim: TdmkEditDateTimePicker
            Left = 256
            Top = 23
            Width = 186
            Height = 21
            Date = 42189.613065706020000000
            Time = 42189.613065706020000000
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DtValFim'
            UpdCampo = 'DtValFim'
            UpdType = utYes
          end
        end
      end
      object EdNome: TdmkEdit
        Left = 12
        Top = 72
        Width = 695
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 255
        Height = 32
        Caption = 'Cadastro de Pr'#234'mios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 255
        Height = 32
        Caption = 'Cadastro de Pr'#234'mios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 255
        Height = 32
        Caption = 'Cadastro de Pr'#234'mios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrSrvLPrmCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrSrvLPrmCadBeforeOpen
    AfterOpen = QrSrvLPrmCadAfterOpen
    BeforeClose = QrSrvLPrmCadBeforeClose
    AfterScroll = QrSrvLPrmCadAfterScroll
    SQL.Strings = (
      'SELECT prm.*, slc.Nome NO_SrvLCad, '
      
        ' ELT(prm.FrmFtrCobr + 1,"Indefinido","Valor total","Valor unit'#225'r' +
        'io") NO_FrmFtrCobr,'
      
        ' ELT(prm.FrmFtrPaga + 1,"Indefinido","Valor total","Valor unit'#225'r' +
        'io") NO_FrmFtrPaga,'
      
        ' ELT(prm.FrmRatPaga + 1,"Indefinido","Valor total","Valor por pe' +
        'ssoa") NO_FrmRatPaga,'
      'umc.Nome NO_UMC, ump.Nome NO_UMP, '
      'umc.Sigla SIGLA_UMC, ump.Sigla SIGLA_UMP '
      'FROM srvlprmcad prm '
      'LEFT JOIN srvlcad slc ON slc.Codigo=prm.SrvLCad '
      'LEFT JOIN unidmed umc ON umc.Codigo=prm.UnidMedCobr '
      'LEFT JOIN unidmed ump ON ump.Codigo=prm.UnidMedPaga '
      'WHERE prm.Codigo > 0 '
      'AND prm.Codigo=:P0')
    Left = 496
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSrvLPrmCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLPrmCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrSrvLPrmCadSrvLCad: TIntegerField
      FieldName = 'SrvLCad'
    end
    object QrSrvLPrmCadDtValIni: TDateField
      FieldName = 'DtValIni'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrSrvLPrmCadDtValFim: TDateField
      FieldName = 'DtValFim'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrSrvLPrmCadNO_SrvLCad: TWideStringField
      FieldName = 'NO_SrvLCad'
      Size = 60
    end
    object QrSrvLPrmCadUnidMedCobr: TIntegerField
      FieldName = 'UnidMedCobr'
    end
    object QrSrvLPrmCadFrmFtrCobr: TSmallintField
      FieldName = 'FrmFtrCobr'
    end
    object QrSrvLPrmCadFrmFtrPaga: TSmallintField
      FieldName = 'FrmFtrPaga'
    end
    object QrSrvLPrmCadUnidMedPaga: TIntegerField
      FieldName = 'UnidMedPaga'
    end
    object QrSrvLPrmCadFrmRatPaga: TSmallintField
      FieldName = 'FrmRatPaga'
    end
    object QrSrvLPrmCadSIGLA_UMC: TWideStringField
      FieldName = 'SIGLA_UMC'
      Size = 6
    end
    object QrSrvLPrmCadSIGLA_UMP: TWideStringField
      FieldName = 'SIGLA_UMP'
      Size = 6
    end
    object QrSrvLPrmCadNO_FrmFtrCobr: TWideStringField
      FieldName = 'NO_FrmFtrCobr'
      Size = 60
    end
    object QrSrvLPrmCadNO_FrmFtrPaga: TWideStringField
      FieldName = 'NO_FrmFtrPaga'
      Size = 60
    end
    object QrSrvLPrmCadNO_FrmRatPaga: TWideStringField
      FieldName = 'NO_FrmRatPaga'
      Size = 60
    end
    object QrSrvLPrmCadAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLPrmCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLPrmCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLPrmCadUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLPrmCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLPrmCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLPrmCadLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLPrmCadNO_UMC: TWideStringField
      FieldName = 'NO_UMC'
      Size = 30
    end
    object QrSrvLPrmCadNO_UMP: TWideStringField
      FieldName = 'NO_UMP'
      Size = 30
    end
  end
  object DsSrvLPrmCad: TDataSource
    DataSet = QrSrvLPrmCad
    Left = 496
    Top = 57
  end
  object QrSrvLPrmNvs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nvs.*,'
      
        ' ELT(nvs.FrmFtrCobr + 1,"Indefinido","Valor total","Valor unit'#225'r' +
        'io") NO_FrmFtrCobr,'
      
        ' ELT(nvs.FrmFtrPaga + 1,"Indefinido","Valor total","Valor unit'#225'r' +
        'io") NO_FrmFtrPaga,'
      
        ' ELT(nvs.FrmRatPaga + 1,"Indefinido","Valor total","Valor por pe' +
        'ssoa") NO_FrmRatPaga,'
      'umc.Sigla SIGLA_UMC, ump.Sigla SIGLA_UMP'
      'FROM srvlprmnvs nvs'
      'LEFT JOIN unidmed umc ON umc.Codigo=nvs.UnidMedCobr'
      'LEFT JOIN unidmed ump ON ump.Codigo=nvs.UnidMedPaga'
      'WHERE nvs.Codigo=1'
      '')
    Left = 576
    Top = 17
    object QrSrvLPrmNvsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLPrmNvsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrvLPrmNvsQtdMaxCobr: TFloatField
      FieldName = 'QtdMaxCobr'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSrvLPrmNvsValFtrCobr: TFloatField
      FieldName = 'ValFtrCobr'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrSrvLPrmNvsQtdMaxPaga: TFloatField
      FieldName = 'QtdMaxPaga'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSrvLPrmNvsValRatPaga: TFloatField
      FieldName = 'ValRatPaga'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrSrvLPrmNvsUnidMedCobr: TIntegerField
      FieldName = 'UnidMedCobr'
    end
    object QrSrvLPrmNvsFrmFtrCobr: TSmallintField
      FieldName = 'FrmFtrCobr'
    end
    object QrSrvLPrmNvsUnidMedPaga: TIntegerField
      FieldName = 'UnidMedPaga'
    end
    object QrSrvLPrmNvsFrmRatPaga: TSmallintField
      FieldName = 'FrmRatPaga'
    end
    object QrSrvLPrmNvsSIGLA_UMC: TWideStringField
      FieldName = 'SIGLA_UMC'
      Size = 6
    end
    object QrSrvLPrmNvsSIGLA_UMP: TWideStringField
      FieldName = 'SIGLA_UMP'
      Size = 6
    end
    object QrSrvLPrmNvsNO_FrmFtrCobr: TWideStringField
      FieldName = 'NO_FrmFtrCobr'
      Size = 60
    end
    object QrSrvLPrmNvsNO_FrmFtrPaga: TWideStringField
      FieldName = 'NO_FrmFtrPaga'
      Size = 60
    end
    object QrSrvLPrmNvsNO_FrmRatPaga: TWideStringField
      FieldName = 'NO_FrmRatPaga'
      Size = 60
    end
    object QrSrvLPrmNvsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSrvLPrmNvsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSrvLPrmNvsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSrvLPrmNvsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSrvLPrmNvsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSrvLPrmNvsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSrvLPrmNvsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSrvLPrmNvsFrmFtrPaga: TIntegerField
      FieldName = 'FrmFtrPaga'
    end
  end
  object DsSrvLPrmNvs: TDataSource
    DataSet = QrSrvLPrmNvs
    Left = 576
    Top = 61
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 432
    Top = 500
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 296
    Top = 504
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object DsSrvLCad: TDataSource
    DataSet = QrSrvLCad
    Left = 652
    Top = 60
  end
  object QrSrvLCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, UnidMed '
      'FROM srvlcad'
      'ORDER BY Nome')
    Left = 652
    Top = 16
    object QrSrvLCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrvLCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrSrvLCadUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
  end
  object QrUnidMedCobr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 268
    Top = 404
    object QrUnidMedCobrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCobrCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedCobrSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedCobrNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMedCobr: TDataSource
    DataSet = QrUnidMedCobr
    Left = 268
    Top = 448
  end
  object QrUnidMedPaga: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 352
    Top = 404
    object QrUnidMedPagaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedPagaCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedPagaSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedPagaNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMedPaga: TDataSource
    DataSet = QrUnidMedPaga
    Left = 352
    Top = 448
  end
  object VUUnidMedCobr: TdmkValUsu
    dmkEditCB = EdUnidMedCobr
    Panel = PnEdita
    QryCampo = 'UnidMedCobr'
    UpdCampo = 'UnidMedCobr'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 432
    Top = 404
  end
  object VUUnidMedPaga: TdmkValUsu
    dmkEditCB = EdUnidMedPaga
    Panel = PnEdita
    QryCampo = 'UnidMedPaga'
    UpdCampo = 'UnidMedPaga'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 432
    Top = 452
  end
end
