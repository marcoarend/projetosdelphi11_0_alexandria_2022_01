object FmFavoritos1: TFmFavoritos1
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: Bot'#227'o Favorito'
  ClientHeight = 498
  ClientWidth = 499
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 499
    Height = 336
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 499
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label4: TLabel
        Left = 8
        Top = 8
        Width = 39
        Height = 13
        Caption = 'Usu'#225'rio:'
      end
      object Label2: TLabel
        Left = 364
        Top = 8
        Width = 37
        Height = 13
        Caption = 'ID guia:'
      end
      object EdUsuario_Cod: TdmkEdit
        Left = 52
        Top = 4
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdUsuario_Log: TdmkEdit
        Left = 108
        Top = 4
        Width = 253
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNivel1: TdmkEdit
        Left = 404
        Top = 3
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Nivel1'
        UpdCampo = 'Nivel1'
        UpdType = utIdx
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 128
      Width = 499
      Height = 208
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 3
      object Label1: TLabel
        Left = 8
        Top = 168
        Width = 34
        Height = 13
        Caption = 'Ordem:'
      end
      object Label3: TLabel
        Left = 92
        Top = 168
        Width = 38
        Height = 13
        Caption = 'Linha 1:'
      end
      object Label9: TLabel
        Left = 224
        Top = 168
        Width = 38
        Height = 13
        Caption = 'Linha 2:'
      end
      object Label10: TLabel
        Left = 356
        Top = 168
        Width = 38
        Height = 13
        Caption = 'Linha 3:'
      end
      object Label11: TLabel
        Left = 8
        Top = 4
        Width = 80
        Height = 13
        Caption = 'Pager de origem:'
      end
      object Label12: TLabel
        Left = 8
        Top = 44
        Width = 89
        Height = 13
        Caption = 'ToolBar de origem:'
      end
      object Label13: TLabel
        Left = 8
        Top = 84
        Width = 80
        Height = 13
        Caption = 'Bot'#227'o de origem:'
      end
      object Label14: TLabel
        Left = 8
        Top = 128
        Width = 136
        Height = 13
        Caption = 'Nome do bot'#227'o selecionado:'
        Enabled = False
        FocusControl = DBEdNome
      end
      object Label15: TLabel
        Left = 248
        Top = 128
        Width = 139
        Height = 13
        Caption = 'Classe do bot'#227'o selecionado:'
        Enabled = False
        FocusControl = DbEdClasse
      end
      object EdOrdem: TdmkEdit
        Left = 8
        Top = 184
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '1'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        QryCampo = 'Ordem'
        UpdCampo = 'Ordem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
      end
      object EdDescri1: TdmkEdit
        Left = 92
        Top = 184
        Width = 128
        Height = 21
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Descri1'
        UpdCampo = 'Descri1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdDescri2: TdmkEdit
        Left = 224
        Top = 184
        Width = 128
        Height = 21
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Descri2'
        UpdCampo = 'Descri2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdDescri3: TdmkEdit
        Left = 356
        Top = 184
        Width = 128
        Height = 21
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Descri3'
        UpdCampo = 'Descri3'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPage: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPageChange
        DBLookupComboBox = CBPage
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPage: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 420
        Height = 21
        KeyField = 'Codigo'
        ListField = 'DescrPage'
        ListSource = DsPages
        TabOrder = 1
        dmkEditCB = EdPage
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTBar: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTBarChange
        DBLookupComboBox = CBTBar
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTBar: TdmkDBLookupComboBox
        Left = 64
        Top = 60
        Width = 420
        Height = 21
        KeyField = 'Controle'
        ListField = 'DescrTBar'
        ListSource = DsTBars
        TabOrder = 3
        dmkEditCB = EdTBar
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdBtns: TdmkEditCB
        Left = 8
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdBtnsChange
        DBLookupComboBox = CBBtns
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBBtns: TdmkDBLookupComboBox
        Left = 64
        Top = 100
        Width = 420
        Height = 21
        KeyField = 'Seq'
        ListField = 'DescrBtn'
        ListSource = DsBtns
        TabOrder = 5
        dmkEditCB = EdBtns
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object DBEdNome: TdmkDBEdit
        Left = 8
        Top = 144
        Width = 236
        Height = 21
        TabStop = False
        DataField = 'Nome'
        DataSource = DsBtns
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        UpdCampo = 'Nome'
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DbEdClasse: TdmkDBEdit
        Left = 248
        Top = 144
        Width = 236
        Height = 21
        TabStop = False
        DataField = 'Classe'
        DataSource = DsBtns
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        UpdCampo = 'Classe'
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 32
      Width = 499
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 499
        Height = 48
        Align = alClient
        Caption = ' Guia: '
        TabOrder = 0
        object Label5: TLabel
          Left = 8
          Top = 20
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdit1
        end
        object Label6: TLabel
          Left = 92
          Top = 20
          Width = 31
          Height = 13
          Caption = 'T'#237'tulo:'
          FocusControl = DBEdit2
        end
        object DBEdit1: TdmkDBEdit
          Left = 32
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Nivel3'
          DataSource = FmFavoritosG.DsFavoritos3
          TabOrder = 0
          UpdCampo = 'Nivel3'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit2: TDBEdit
          Left = 128
          Top = 16
          Width = 356
          Height = 21
          TabStop = False
          DataField = 'Titulo'
          DataSource = FmFavoritosG.DsFavoritos3
          TabOrder = 1
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 80
      Width = 499
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 499
        Height = 48
        Align = alClient
        Caption = ' P'#225'gina: '
        TabOrder = 0
        object Label7: TLabel
          Left = 8
          Top = 20
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = dmkDBEdit1
        end
        object Label8: TLabel
          Left = 92
          Top = 20
          Width = 31
          Height = 13
          Caption = 'T'#237'tulo:'
          FocusControl = DBEdit3
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 32
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Nivel2'
          DataSource = FmFavoritosG.DsFavoritos2
          TabOrder = 0
          UpdCampo = 'Nivel2'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit3: TDBEdit
          Left = 128
          Top = 16
          Width = 356
          Height = 21
          TabStop = False
          DataField = 'Titulo'
          DataSource = FmFavoritosG.DsFavoritos2
          TabOrder = 1
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 499
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 451
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 403
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 177
        Height = 32
        Caption = 'Bot'#227'o Favorito'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 177
        Height = 32
        Caption = 'Bot'#227'o Favorito'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 177
        Height = 32
        Caption = 'Bot'#227'o Favorito'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 384
    Width = 499
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 495
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 428
    Width = 499
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 353
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 351
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPages: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Codigo, DescrPage '
      'FROM _fav_btns_'
      'ORDER BY DescrPage')
    Left = 288
    Top = 244
    object QrPagesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPagesDescrPage: TWideStringField
      FieldName = 'DescrPage'
      Size = 255
    end
  end
  object QrTBars: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Controle, DescrTBar '
      'FROM _fav_btns_'
      'WHERE Codigo=:P0'
      'ORDER BY DescrTBar')
    Left = 288
    Top = 272
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTBarsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTBarsDescrTBar: TWideStringField
      FieldName = 'DescrTBar'
      Size = 255
    end
  end
  object QrBtns: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Conta, DescrBtn, Nome, Classe'
      'FROM _fav_btns_'
      'WHERE Codigo=:P0'
      'AND Controle=:P1'
      'ORDER BY DescrBtn')
    Left = 288
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBtnsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrBtnsDescrBtn: TWideStringField
      FieldName = 'DescrBtn'
      Size = 255
    end
    object QrBtnsNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrBtnsSeq: TIntegerField
      FieldName = 'Seq'
    end
    object QrBtnsClasse: TWideStringField
      FieldName = 'Classe'
      Size = 255
    end
  end
  object DsBtns: TDataSource
    DataSet = QrBtns
    Left = 316
    Top = 300
  end
  object DsTBars: TDataSource
    DataSet = QrTBars
    Left = 316
    Top = 272
  end
  object DsPages: TDataSource
    DataSet = QrPages
    Left = 316
    Top = 244
  end
end
