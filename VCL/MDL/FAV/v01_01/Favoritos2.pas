unit Favoritos2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, Mask, dmkDBEdit, dmkImage, UnDmkEnums;

type
  TFmFavoritos2 = class(TForm)
    Panel1: TPanel;
    Panel6: TPanel;
    Label4: TLabel;
    EdUsuario_Cod: TdmkEdit;
    EdUsuario_Log: TdmkEdit;
    EdNivel2: TdmkEdit;
    Label2: TLabel;
    Panel3: TPanel;
    Label1: TLabel;
    EdOrdem: TdmkEdit;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    DBEdit1: TdmkDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    Label3: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    EdTitulo: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFavoritos2: TFmFavoritos2;

implementation

uses FavoritosG, Module, UMySQLModule, UnMyObjects, UnInternalConsts;

{$R *.DFM}

procedure TFmFavoritos2.BtOKClick(Sender: TObject);
var
  Nivel2, OrdemAtual: Integer;
begin
  if MyObjects.FIC(Trim(EdTitulo.Text) = '', EdTitulo,
    'Informe o t�tulo') then Exit;
  Nivel2 := UMyMod.BuscaEmLivreY_Def_Geral('Favoritos2', 'Nivel2', ImgTipo.SQLType,
    FmFavoritosG.QrFavoritos2Nivel2.Value, EdNivel2);
  if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, 'Favoritos2', Nivel2,
  Dmod.QrUpd) then
  begin
    if ImgTipo.SQLType = stIns then
      OrdemAtual := 0
    else
      OrdemAtual := FmFavoritosG.QrFavoritos2Ordem.Value;
    FmFavoritosG.CorrigeOrdem2(Nivel2, OrdemAtual, 
      EdOrdem.ValueVariant, FmFavoritosG.QrFavoritos3Nivel3.Value);
    FmFavoritosG.ReopenNivel2(Nivel2);
    Close;
  end;
end;

procedure TFmFavoritos2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFavoritos2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFavoritos2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  // Vale para stUpd tamb�m?
  EdOrdem.ValMax := FormatFloat('0', FmFavoritosG.QrFavoritos2.RecordCount + 1);
end;

procedure TFmFavoritos2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
