object FmFavoritosG: TFmFavoritosG
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: Configura'#231#227'o de Barras de Ferramentas Favoritas'
  ClientHeight = 579
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 413
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 512
      Top = 28
      Width = 496
      Height = 385
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 0
        Top = 0
        Width = 212
        Height = 15
        Align = alTop
        Alignment = taCenter
        Caption = 'BOT'#213'ES DO TOOLBAR SELECIONADO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 15
        Width = 496
        Height = 370
        Align = alClient
        DataSource = DsFavoritos1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Ordem'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Bot'#227'o de origem'
            Width = 132
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descri1'
            Title.Caption = 'Linha 1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descri2'
            Title.Caption = 'Linha 2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descri3'
            Title.Caption = 'Linha 3'
            Visible = True
          end>
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 28
      Width = 256
      Height = 385
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 0
        Top = 0
        Width = 52
        Height = 15
        Align = alTop
        Alignment = taCenter
        Caption = ' PAGERS '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 15
        Width = 256
        Height = 370
        Align = alClient
        DataSource = DsFavoritos3
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Ordem'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Titulo'
            Title.Caption = 'T'#237'tulo'
            Width = 180
            Visible = True
          end>
      end
    end
    object Panel5: TPanel
      Left = 256
      Top = 28
      Width = 256
      Height = 385
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
      object Label3: TLabel
        Left = 0
        Top = 0
        Width = 209
        Height = 15
        Align = alTop
        Alignment = taCenter
        Caption = 'TOOLBARS DO PAGER SELECIONADA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBGrid3: TDBGrid
        Left = 0
        Top = 15
        Width = 256
        Height = 370
        Align = alClient
        DataSource = DsFavoritos2
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Ordem'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Titulo'
            Title.Caption = 'T'#237'tulo'
            Width = 180
            Visible = True
          end>
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 28
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 3
      object Label4: TLabel
        Left = 8
        Top = 8
        Width = 39
        Height = 13
        Caption = 'Usu'#225'rio:'
      end
      object EdUsuario_Cod: TdmkEdit
        Left = 52
        Top = 4
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdUsuario_Log: TdmkEdit
        Left = 108
        Top = 4
        Width = 261
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdUsuario_Nom: TdmkEdit
        Left = 368
        Top = 4
        Width = 629
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_M: TGroupBox
      Left = 0
      Top = 0
      Width = 960
      Height = 52
      Align = alClient
      TabOrder = 1
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 607
        Height = 32
        Caption = 'Configura'#231#227'o de Barras de Ferramentas Favoritas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 607
        Height = 32
        Caption = 'Configura'#231#227'o de Barras de Ferramentas Favoritas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 607
        Height = 32
        Caption = 'Configura'#231#227'o de Barras de Ferramentas Favoritas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 465
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1005
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 509
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 863
      Top = 15
      Width = 144
      Height = 54
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 861
      Height = 54
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtNivel3: TBitBtn
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pager'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtNivel3Click
      end
      object BtNivel2: TBitBtn
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&ToolBar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtNivel2Click
      end
      object BtNivel1: TBitBtn
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Bot'#227'o'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtNivel1Click
      end
    end
  end
  object QrFavoritos3: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFavoritos3BeforeClose
    AfterScroll = QrFavoritos3AfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM favoritos3'
      'WHERE Usuario=:P0'
      'ORDER BY Ordem, Nivel3, Titulo'
      '')
    Left = 4
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFavoritos3Nivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrFavoritos3Usuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrFavoritos3Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFavoritos3Reordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrFavoritos3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrFavoritos3Titulo: TWideStringField
      FieldName = 'Titulo'
      Size = 50
    end
  end
  object DsFavoritos3: TDataSource
    DataSet = QrFavoritos3
    Left = 32
    Top = 136
  end
  object QrFavoritos2: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFavoritos2BeforeClose
    AfterScroll = QrFavoritos2AfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM favoritos2'
      'WHERE Nivel3=:P0'
      'ORDER BY Ordem, Reordem, Titulo')
    Left = 4
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFavoritos2Nivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrFavoritos2Nivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrFavoritos2Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFavoritos2Reordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrFavoritos2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrFavoritos2Titulo: TWideStringField
      FieldName = 'Titulo'
      Size = 50
    end
  end
  object DsFavoritos2: TDataSource
    DataSet = QrFavoritos2
    Left = 32
    Top = 164
  end
  object QrFavoritos1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM favoritos1'
      'WHERE Nivel2=:P0'
      'ORDER BY Ordem, Reordem, Descri1, Descri2, descri3')
    Left = 4
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFavoritos1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrFavoritos1Nivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrFavoritos1Nivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrFavoritos1Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFavoritos1Reordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrFavoritos1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrFavoritos1Descri1: TWideStringField
      FieldName = 'Descri1'
      Size = 15
    end
    object QrFavoritos1Descri2: TWideStringField
      FieldName = 'Descri2'
      Size = 15
    end
    object QrFavoritos1Descri3: TWideStringField
      FieldName = 'Descri3'
      Size = 15
    end
  end
  object DsFavoritos1: TDataSource
    DataSet = QrFavoritos1
    Left = 32
    Top = 192
  end
  object PMNivel3: TPopupMenu
    OnPopup = PMNivel3Popup
    Left = 52
    Top = 420
    object Incluinovaguia1: TMenuItem
      Caption = '&Inclui novo pager'
      OnClick = Incluinovaguia1Click
    end
    object AlteraguiaAtual1: TMenuItem
      Caption = '&Altera pager Atual'
      Enabled = False
      OnClick = AlteraguiaAtual1Click
    end
    object ExcluiGuiaAtual1: TMenuItem
      Caption = '&Exclui pager Atual'
      Enabled = False
      OnClick = ExcluiGuiaAtual1Click
    end
  end
  object PMNivel2: TPopupMenu
    OnPopup = PMNivel2Popup
    Left = 136
    Top = 420
    object Incluinovapginanaguiaatual1: TMenuItem
      Caption = '&Inclui novo toolbar no pager atual'
      Enabled = False
      OnClick = Incluinovapginanaguiaatual1Click
    end
    object Alteraapginaselecionada1: TMenuItem
      Caption = '&Altera o toolbar selecionado'
      Enabled = False
      OnClick = Alteraapginaselecionada1Click
    end
    object Excluiapginaselecionada1: TMenuItem
      Caption = '&Exclui o toolbar selecionado'
      Enabled = False
      OnClick = Excluiapginaselecionada1Click
    end
  end
  object PMNivel1: TPopupMenu
    OnPopup = PMNivel1Popup
    Left = 228
    Top = 424
    object Adicionabotopginaselecionada1: TMenuItem
      Caption = '&Adiciona bot'#227'o '#224' p'#225'gina selecionada'
      Enabled = False
      OnClick = Adicionabotopginaselecionada1Click
    end
    object Removeobotoselecionado1: TMenuItem
      Caption = '&Remove o bot'#227'o selecionado'
      Enabled = False
      OnClick = Removeobotoselecionado1Click
    end
  end
end
