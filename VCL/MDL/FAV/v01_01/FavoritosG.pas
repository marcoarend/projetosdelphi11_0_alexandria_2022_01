unit FavoritosG;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, Menus, dmkImage, UnDmkEnums,
  Vcl.ComCtrls;

type
  TFmFavoritosG = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    DBGrid1: TDBGrid;
    Panel4: TPanel;
    Label2: TLabel;
    DBGrid2: TDBGrid;
    Panel5: TPanel;
    Label3: TLabel;
    DBGrid3: TDBGrid;
    QrFavoritos3: TmySQLQuery;
    DsFavoritos3: TDataSource;
    QrFavoritos2: TmySQLQuery;
    DsFavoritos2: TDataSource;
    QrFavoritos1: TmySQLQuery;
    DsFavoritos1: TDataSource;
    PMNivel3: TPopupMenu;
    Incluinovaguia1: TMenuItem;
    AlteraguiaAtual1: TMenuItem;
    ExcluiGuiaAtual1: TMenuItem;
    Panel6: TPanel;
    Label4: TLabel;
    EdUsuario_Cod: TdmkEdit;
    EdUsuario_Log: TdmkEdit;
    EdUsuario_Nom: TdmkEdit;
    QrFavoritos3Nivel3: TIntegerField;
    QrFavoritos3Usuario: TIntegerField;
    QrFavoritos3Ordem: TIntegerField;
    QrFavoritos3Reordem: TIntegerField;
    QrFavoritos3Nome: TWideStringField;
    QrFavoritos3Titulo: TWideStringField;
    PMNivel2: TPopupMenu;
    Incluinovapginanaguiaatual1: TMenuItem;
    Alteraapginaselecionada1: TMenuItem;
    Excluiapginaselecionada1: TMenuItem;
    QrFavoritos2Nivel2: TIntegerField;
    QrFavoritos2Nivel3: TIntegerField;
    QrFavoritos2Ordem: TIntegerField;
    QrFavoritos2Reordem: TIntegerField;
    QrFavoritos2Nome: TWideStringField;
    QrFavoritos2Titulo: TWideStringField;
    QrFavoritos1Nivel1: TIntegerField;
    QrFavoritos1Nivel2: TIntegerField;
    QrFavoritos1Nivel3: TIntegerField;
    QrFavoritos1Ordem: TIntegerField;
    QrFavoritos1Reordem: TIntegerField;
    QrFavoritos1Nome: TWideStringField;
    QrFavoritos1Descri1: TWideStringField;
    QrFavoritos1Descri2: TWideStringField;
    QrFavoritos1Descri3: TWideStringField;
    PMNivel1: TPopupMenu;
    Adicionabotopginaselecionada1: TMenuItem;
    Removeobotoselecionado1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel8: TPanel;
    BtNivel3: TBitBtn;
    BtNivel2: TBitBtn;
    BtNivel1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtNivel3Click(Sender: TObject);
    procedure Incluinovaguia1Click(Sender: TObject);
    procedure AlteraguiaAtual1Click(Sender: TObject);
    procedure ExcluiGuiaAtual1Click(Sender: TObject);
    procedure PMNivel3Popup(Sender: TObject);
    procedure QrFavoritos3BeforeClose(DataSet: TDataSet);
    procedure QrFavoritos3AfterScroll(DataSet: TDataSet);
    procedure PMNivel2Popup(Sender: TObject);
    procedure Incluinovapginanaguiaatual1Click(Sender: TObject);
    procedure Alteraapginaselecionada1Click(Sender: TObject);
    procedure Excluiapginaselecionada1Click(Sender: TObject);
    procedure BtNivel2Click(Sender: TObject);
    procedure QrFavoritos2BeforeClose(DataSet: TDataSet);
    procedure QrFavoritos2AfterScroll(DataSet: TDataSet);
    procedure PMNivel1Popup(Sender: TObject);
    procedure Adicionabotopginaselecionada1Click(Sender: TObject);
    procedure BtNivel1Click(Sender: TObject);
    procedure Removeobotoselecionado1Click(Sender: TObject);
  private
    { Private declarations }
    FIniciou: Boolean;
    FFavoritosPage, FFavoritosTBar, FFavoritosBtns: String;
    //
    procedure VerificaBotoes();
    procedure GerenciaNivel3(SQLType: TSQLType);
    procedure GerenciaNivel2(SQLType: TSQLType);
    procedure GerenciaNivel1(SQLType: TSQLType);

  public
    { Public declarations }
    procedure ReopenNivel1(Nivel1: Integer);
    procedure ReopenNivel2(Nivel2: Integer);
    procedure ReopenNivel3(Nivel3: Integer);
    procedure CorrigeOrdem1(Nivel1, OrdemAtual, NovaOrdem, Nivel2: Integer);
    procedure CorrigeOrdem2(Nivel2, OrdemAtual, NovaOrdem, Nivel3: Integer);
    procedure CorrigeOrdem3(Nivel3, OrdemAtual, NovaOrdem: Integer);
  end;

  var
  FmFavoritosG: TFmFavoritosG;

implementation

uses UnMyObjects, Module, ModuleGeral, Principal, UCreate, UMySQLModule, UnInternalConsts,
  Favoritos3, MyDBCheck, Favoritos2, Favoritos1;

{$R *.DFM}

procedure TFmFavoritosG.Adicionabotopginaselecionada1Click(Sender: TObject);
begin
  GerenciaNivel1(stIns);
end;

procedure TFmFavoritosG.Alteraapginaselecionada1Click(Sender: TObject);
begin
  GerenciaNivel2(stUpd);
end;

procedure TFmFavoritosG.AlteraguiaAtual1Click(Sender: TObject);
begin
  GerenciaNivel3(stUpd);
end;

procedure TFmFavoritosG.BtNivel1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNivel1, BtNivel1);
end;

procedure TFmFavoritosG.BtNivel2Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNivel2, BtNivel2);
end;

procedure TFmFavoritosG.BtNivel3Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNivel3, BtNivel3);
end;

procedure TFmFavoritosG.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFavoritosG.CorrigeOrdem1(Nivel1, OrdemAtual, NovaOrdem,
  Nivel2: Integer);
var
  MyCursor: TCursor;
  Ordem: Integer;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if NovaOrdem > OrdemAtual then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE favoritos1 SET Ordem=Ordem-1 ');
      Dmod.QrUpd.SQL.Add('WHERE Nivel2=:P0');
      Dmod.QrUpd.SQL.Add('AND Ordem>:P1');
      Dmod.QrUpd.SQL.Add('AND Ordem<=:P2');
      Dmod.QrUpd.Params[00].AsInteger := Nivel2;
      Dmod.QrUpd.Params[01].AsInteger := OrdemAtual;
      Dmod.QrUpd.Params[02].AsInteger := NovaOrdem;
      Dmod.QrUpd.ExecSQL;
    end;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE favoritos1 SET Reordem=1 ');
    Dmod.QrUpd.SQL.Add('WHERE Nivel2=:P0');
    Dmod.QrUpd.Params[00].AsInteger := Nivel2;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE favoritos1 SET Reordem=0, ');
    Dmod.QrUpd.SQL.Add('Ordem=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Nivel1=:P1');
    Dmod.QrUpd.Params[00].AsInteger := NovaOrdem;
    Dmod.QrUpd.Params[01].AsInteger := Nivel1;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Ordem, Reordem, Nivel1');
    Dmod.QrAux.SQL.Add('FROM favoritos1');
    Dmod.QrAux.SQL.Add('WHERE Nivel2=:P0');
    Dmod.QrAux.SQL.Add('ORDER BY Ordem, Reordem, Nivel1');
    Dmod.QrAux.Params[00].AsInteger := Nivel2;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    //
    Ordem := 0;
    Dmod.QrAux.First;
    while not Dmod.QrAux.Eof do
    begin
      Ordem := Ordem + 1;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE favoritos1 SET Reordem=0, ');
      Dmod.QrUpd.SQL.Add('Ordem=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Nivel1=:P1');
      Dmod.QrUpd.Params[00].AsInteger := Ordem;
      Dmod.QrUpd.Params[01].AsInteger := Dmod.QrAux.FieldByName('Nivel1').AsInteger;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrAux.Next;
    end;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TFmFavoritosG.CorrigeOrdem2(Nivel2, OrdemAtual, NovaOrdem, Nivel3: Integer);
var
  MyCursor: TCursor;
  Ordem: Integer;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if NovaOrdem > OrdemAtual then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE favoritos2 SET Ordem=Ordem-1 ');
      Dmod.QrUpd.SQL.Add('WHERE Nivel3=:P0');
      Dmod.QrUpd.SQL.Add('AND Ordem>:P1');
      Dmod.QrUpd.SQL.Add('AND Ordem<=:P2');
      Dmod.QrUpd.Params[00].AsInteger := Nivel3;
      Dmod.QrUpd.Params[01].AsInteger := OrdemAtual;
      Dmod.QrUpd.Params[02].AsInteger := NovaOrdem;
      Dmod.QrUpd.ExecSQL;
    end;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE favoritos2 SET Reordem=1 ');
    Dmod.QrUpd.SQL.Add('WHERE Nivel3=:P0');
    Dmod.QrUpd.Params[00].AsInteger := Nivel3;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE favoritos2 SET Reordem=0, ');
    Dmod.QrUpd.SQL.Add('Ordem=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Nivel2=:P1');
    Dmod.QrUpd.Params[00].AsInteger := NovaOrdem;
    Dmod.QrUpd.Params[01].AsInteger := Nivel2;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Ordem, Reordem, Nivel2');
    Dmod.QrAux.SQL.Add('FROM favoritos2');
    Dmod.QrAux.SQL.Add('WHERE Nivel3=:P0');
    Dmod.QrAux.SQL.Add('ORDER BY Ordem, Reordem, Nivel2');
    Dmod.QrAux.Params[00].AsInteger := Nivel3;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    //
    Ordem := 0;
    Dmod.QrAux.First;
    while not Dmod.QrAux.Eof do
    begin
      Ordem := Ordem + 1;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE favoritos2 SET Reordem=0, ');
      Dmod.QrUpd.SQL.Add('Ordem=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Nivel2=:P1');
      Dmod.QrUpd.Params[00].AsInteger := Ordem;
      Dmod.QrUpd.Params[01].AsInteger := Dmod.QrAux.FieldByName('Nivel2').AsInteger;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrAux.Next;
    end;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TFmFavoritosG.CorrigeOrdem3(Nivel3, OrdemAtual, NovaOrdem: Integer);
var
  MyCursor: TCursor;
  Ordem: Integer;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if NovaOrdem > OrdemAtual then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE favoritos3 SET Ordem=Ordem-1 ');
      Dmod.QrUpd.SQL.Add('WHERE Usuario=:P0');
      Dmod.QrUpd.SQL.Add('AND Ordem>:P1');
      Dmod.QrUpd.SQL.Add('AND Ordem<=:P2');
      Dmod.QrUpd.Params[00].AsInteger := VAR_USUARIO;
      Dmod.QrUpd.Params[01].AsInteger := OrdemAtual;
      Dmod.QrUpd.Params[02].AsInteger := NovaOrdem;
      Dmod.QrUpd.ExecSQL;
    end;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE favoritos3 SET Reordem=1 ');
    Dmod.QrUpd.SQL.Add('WHERE Usuario=:P0');
    Dmod.QrUpd.Params[00].AsInteger := VAR_USUARIO;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE favoritos3 SET Reordem=0, ');
    Dmod.QrUpd.SQL.Add('Ordem=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Nivel3=:P1');
    Dmod.QrUpd.Params[00].AsInteger := NovaOrdem;
    Dmod.QrUpd.Params[01].AsInteger := Nivel3;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Ordem, Reordem, Nivel3');
    Dmod.QrAux.SQL.Add('FROM favoritos3');
    Dmod.QrAux.SQL.Add('WHERE Usuario=:P0');
    Dmod.QrAux.SQL.Add('ORDER BY Ordem, Reordem, Nivel3');
    Dmod.QrAux.Params[00].AsInteger := VAR_USUARIO;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    //
    Ordem := 0;
    Dmod.QrAux.First;
    while not Dmod.QrAux.Eof do
    begin
      Ordem := Ordem + 1;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE favoritos3 SET Reordem=0, ');
      Dmod.QrUpd.SQL.Add('Ordem=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Nivel3=:P1');
      Dmod.QrUpd.Params[00].AsInteger := Ordem;
      Dmod.QrUpd.Params[01].AsInteger := Dmod.QrAux.FieldByName('Nivel3').AsInteger;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrAux.Next;
    end;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TFmFavoritosG.Excluiapginaselecionada1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do toolbar selecionado "' +
  QrFavoritos2Titulo.Value + '"?') =
  ID_YES then
  begin
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrFavoritos2, 'favoritos2',
    ['Nivel2'], ['Nivel2'], True);
    CorrigeOrdem2(0, 0, 0, QrFavoritos3Nivel3.Value);
    ReopenNivel2(0);
  end;
end;

procedure TFmFavoritosG.ExcluiGuiaAtual1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do pager selecionado "' +
  QrFavoritos3Titulo.Value + '"?') =
  ID_YES then
  begin
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrFavoritos3, 'favoritos3',
    ['Nivel3'], ['Nivel3'], True);
    CorrigeOrdem3(0,0,0);
    ReopenNivel3(0);
    //
    if (QrFavoritos3.State = dsInactive) or (QrFavoritos3.RecordCount = 0) then
      Geral.MB_Aviso('Reabra o aplicativo para atualizar o menu!');
  end;
end;

procedure TFmFavoritosG.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if not FIniciou then
  begin
    Screen.Cursor := crHourGlass;
    try
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando bot�es dispon�veis...');
      FFavoritosPage := UCriar.RecriaTempTableNovo(ntrttFavoritosPage, DmodG.QrUpdPID1, False);
      FFavoritosTBar := UCriar.RecriaTempTableNovo(ntrttFavoritosTBar, DmodG.QrUpdPID1, False);
      FFavoritosBtns := UCriar.RecriaTempTableNovo(ntrttFavoritosBtns, DmodG.QrUpdPID1, False);
      //
      VerificaBotoes();
      //
      FIniciou := True;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmFavoritosG.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  EdUsuario_Cod.ValueVariant := VAR_USUARIO;
  EdUsuario_Log.ValueVariant := VAR_LOGIN;
  FIniciou := False;
  ReopenNivel3(0);
end;

procedure TFmFavoritosG.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFavoritosG.GerenciaNivel1(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmFavoritos1, FmFavoritos1, afmoLiberado,
    QrFavoritos1, SQLType) then
  begin
    FmFavoritos1.EdUsuario_Cod.ValueVariant := VAR_USUARIO;
    FmFavoritos1.EdUsuario_Log.ValueVariant := VAR_LOGIN;
    if SQLType = stIns then
      FmFavoritos1.EdOrdem.ValueVariant := FmFavoritos1.EdOrdem.ValMax;
    FmFavoritos1.ShowModal;
    FmFavoritos1.Destroy;
  end;
end;

procedure TFmFavoritosG.GerenciaNivel2(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmFavoritos2, FmFavoritos2, afmoLiberado,
    QrFavoritos2, SQLType) then
  begin
    FmFavoritos2.EdUsuario_Cod.ValueVariant := VAR_USUARIO;
    FmFavoritos2.EdUsuario_Log.ValueVariant := VAR_LOGIN;
    if SQLType = stIns then
      FmFavoritos2.EdOrdem.ValueVariant := FmFavoritos2.EdOrdem.ValMax;
    FmFavoritos2.ShowModal;
    FmFavoritos2.Destroy;
  end;
end;

procedure TFmFavoritosG.GerenciaNivel3(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmFavoritos3, FmFavoritos3, afmoLiberado,
    QrFavoritos3, SQLType) then
  begin
    FmFavoritos3.EdUsuario_Cod.ValueVariant := VAR_USUARIO;
    FmFavoritos3.EdUsuario_Log.ValueVariant := VAR_LOGIN;
    if SQLType = stIns then
      FmFavoritos3.EdOrdem.ValueVariant := FmFavoritos3.EdOrdem.ValMax;
    FmFavoritos3.ShowModal;
    FmFavoritos3.Destroy;
  end;
end;

procedure TFmFavoritosG.Incluinovaguia1Click(Sender: TObject);
begin
  GerenciaNivel3(stIns);
end;

procedure TFmFavoritosG.Incluinovapginanaguiaatual1Click(Sender: TObject);
begin
  GerenciaNivel2(stIns);
end;

procedure TFmFavoritosG.PMNivel1Popup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrFavoritos2.State <> dsInactive) and
              (QrFavoritos2.RecordCount > 0);
  Adicionabotopginaselecionada1.Enabled := Habilita;
  Habilita := (QrFavoritos1.State <> dsInactive) and
              (QrFavoritos1.RecordCount > 0);
  Removeobotoselecionado1.Enabled := Habilita;
end;

procedure TFmFavoritosG.PMNivel2Popup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrFavoritos3.State <> dsInactive) and
              (QrFavoritos3.RecordCount > 0);
  Incluinovapginanaguiaatual1.Enabled := Habilita;
  Habilita := (QrFavoritos2.State <> dsInactive) and
              (QrFavoritos2.RecordCount > 0);
  Alteraapginaselecionada1.Enabled := Habilita;
  Habilita := Habilita and
              (QrFavoritos1.State <> dsInactive) and
              (QrFavoritos1.RecordCount = 0);
  Excluiapginaselecionada1.Enabled := Habilita;
end;

procedure TFmFavoritosG.PMNivel3Popup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrFavoritos3.State <> dsInactive) and
              (QrFavoritos3.RecordCount > 0);
  AlteraguiaAtual1.Enabled := Habilita;
  Habilita := Habilita and
              (QrFavoritos2.State <> dsInactive) and
              (QrFavoritos2.RecordCount = 0);
  ExcluiguiaAtual1.Enabled := Habilita;
end;

procedure TFmFavoritosG.QrFavoritos2AfterScroll(DataSet: TDataSet);
begin
  ReopenNivel1(0);
end;

procedure TFmFavoritosG.QrFavoritos2BeforeClose(DataSet: TDataSet);
begin
  QrFavoritos1.Close;
end;

procedure TFmFavoritosG.QrFavoritos3AfterScroll(DataSet: TDataSet);
begin
  ReopenNivel2(0);
end;

procedure TFmFavoritosG.QrFavoritos3BeforeClose(DataSet: TDataSet);
begin
  QrFavoritos2.Close;
end;

procedure TFmFavoritosG.Removeobotoselecionado1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do bot�o selecionado "' +
  QrFavoritos1Nome.Value + '"?') =
  ID_YES then
  begin
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrFavoritos1, 'favoritos1',
    ['Nivel1'], ['Nivel1'], True);
    CorrigeOrdem1(0, 0, 0, QrFavoritos2Nivel2.Value);
    ReopenNivel1(0);
  end;
end;

procedure TFmFavoritosG.ReopenNivel1(Nivel1: Integer);
begin
  QrFavoritos1.Close;
  QrFavoritos1.Params[0].AsInteger := QrFavoritos2Nivel2.Value;
  UnDmkDAC_PF.AbreQuery(QrFavoritos1, Dmod.MyDB);
  QrFavoritos1.Locate('Nivel1', Nivel1, []);
end;

procedure TFmFavoritosG.ReopenNivel2(Nivel2: Integer);
begin
  QrFavoritos2.Close;
  QrFavoritos2.Params[0].AsInteger := QrFavoritos3Nivel3.Value;
  UnDmkDAC_PF.AbreQuery(QrFavoritos2, Dmod.MyDB);
  QrFavoritos2.Locate('Nivel2', Nivel2, []);
end;

procedure TFmFavoritosG.ReopenNivel3(Nivel3: Integer);
begin
  QrFavoritos3.Close;
  QrFavoritos3.Params[0].AsInteger := VAR_USUARIO;
  UnDmkDAC_PF.AbreQuery(QrFavoritos3, Dmod.MyDB);
  QrFavoritos3.Locate('Nivel3', Nivel3, []);
end;

procedure TFmFavoritosG.VerificaBotoes();
  function TiraQuebras(Texto: String): String;
  var
    I: Integer;
  begin
    Result := '';
    for I := 1 to Length(Texto) do
    begin
      case Ord(Texto[I]) of
        10: Result := Result + ' ';
        13: ;//nada
        else Result := Result + Texto[I];
      end;
    end;
    Result := Trim(Result);
  end;
var
  I, Codigo, Controle, Conta, Seq: Integer;
  Button, ToolBar, Page: TComponent;
  //
  Nome, Classe, DescrBtn, DescrTBar, DescrPage: String;
begin
  Seq := 0;
  with FmPrincipal do
  begin
    for i := 0 to ComponentCount -1 do
    begin
      Button := Components[i];
  {$IfDef cAdvToolx} //Berlin
      if Button is TAdvCustomGlowButton then
      begin
        DescrTBar := '';
        DescrPage := '';
        Codigo := 0;
        Controle := 0;
        Conta := TAdvCustomGlowButton(Button).TabOrder + 1;
        Nome := TAdvCustomGlowButton(Button).Name;
        DescrBtn := TiraQuebras(TAdvCustomGlowButton(Button).Caption);
        if (Trim(DescrBtn) <> '') and (Uppercase(Copy(Nome, 1,
        Length(VAR_NomeAdvGlowBtn_TempoExec))) <> Uppercase(VAR_NomeAdvGlowBtn_TempoExec)) then
        begin
          //
          //Page := nil;
          ToolBar := Button.GetParentComponent;
          if ToolBar is TAdvToolBar then
          begin
            Controle := TAdvToolBar(ToolBar).ToolBarIndex + 1;
            DescrTBar := Trim(TAdvToolBar(ToolBar).Caption);
            Page := ToolBar.GetParentComponent;
            if Page <> nil then
            begin
              Codigo := TAdvPage(Page).PageIndex + 1;
              DescrPage := Trim(TAdvPage(Page).Caption);
            end;
          end;
          Classe := Button.ClassName;
          Seq := Seq + 1;
          //
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_fav_btns_', False, [
          'Nome', 'Classe', 'DescrPage', 'DescrTBar',
          'DescrBtn', 'Codigo', 'Controle', 'Conta'], ['Seq'
          ], [
          Nome, Classe, DescrPage, DescrTBar,
          DescrBtn, Codigo, Controle, Conta], [Seq
          ], False);
        end;
      end;
  {$EndIf}
      if Button is TBitBtn then
      begin
        DescrTBar := '';
        DescrPage := '';
        Codigo := 0;
        Controle := 0;
        Conta := TBitBtn(Button).TabOrder + 1;
        Nome := TBitBtn(Button).Name;
        DescrBtn := TiraQuebras(TBitBtn(Button).Caption);
        if (Trim(DescrBtn) <> '') and (Uppercase(Copy(Nome, 1,
        Length(VAR_NomeAdvGlowBtn_TempoExec))) <> Uppercase(VAR_NomeAdvGlowBtn_TempoExec)) then
        begin
          //
          //Page := nil;
          ToolBar := Button.GetParentComponent;
          if ToolBar is TPanel then
          begin
            //Controle := TPanel(ToolBar).ToolBarIndex + 1;
            Controle := Controle + 1;
            //DescrTBar := Trim(TPanel(ToolBar).Caption);
            DescrTBar := Trim(TPanel(ToolBar).Name);
            Page := ToolBar.GetParentComponent;
            if (Page <> nil) and (Page is TTabSheet) then
            begin
              Codigo := TTabSheet(Page).PageIndex + 1;
              DescrPage := Trim(TTabSheet(Page).Caption);
//            end;
//          end;
              Classe := Button.ClassName;
              Seq := Seq + 1;
              //
              UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_fav_btns_', False, [
              'Nome', 'Classe', 'DescrPage', 'DescrTBar',
              'DescrBtn', 'Codigo', 'Controle', 'Conta'], ['Seq'
              ], [
              Nome, Classe, DescrPage, DescrTBar,
              DescrBtn, Codigo, Controle, Conta], [Seq
              ], False);
            end;
          end;
        end;
      end;
    end;
  end;
end;

end.
