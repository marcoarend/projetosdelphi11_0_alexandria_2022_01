unit Favoritos3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmFavoritos3 = class(TForm)
    Panel1: TPanel;
    Panel6: TPanel;
    Label4: TLabel;
    EdUsuario_Cod: TdmkEdit;
    EdUsuario_Log: TdmkEdit;
    EdNivel3: TdmkEdit;
    Label2: TLabel;
    Panel3: TPanel;
    Label1: TLabel;
    EdOrdem: TdmkEdit;
    Label3: TLabel;
    EdTitulo: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdNome: TdmkEdit;
    Label5: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    //procedure VerificaOrdem(ZeraAntes: Boolean);
  public
    { Public declarations }
  end;

  var
  FmFavoritos3: TFmFavoritos3;

implementation

uses FavoritosG, Module, UMySQLModule, UnMyObjects, UnInternalConsts;

{$R *.DFM}

procedure TFmFavoritos3.BtOKClick(Sender: TObject);
var
  Nivel3, OrdemAtual: Integer;
begin
  // Evitar erro -> Usuario = 0
  EdUsuario_Cod.ValueVariant := VAR_USUARIO;
  if MyObjects.FIC(Trim(EdTitulo.Text) = '', EdTitulo,
    'Informe o t�tulo') then Exit;
  Nivel3 := UMyMod.BuscaEmLivreY_Def_Geral('Favoritos3', 'Nivel3', ImgTipo.SQLType,
    FmFavoritosG.QrFavoritos3Nivel3.Value, EdNivel3);
  if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, 'Favoritos3', Nivel3,
  Dmod.QrUpd) then
  begin
    if ImgTipo.SQLType = stIns then
      OrdemAtual := 0
    else
      OrdemAtual := FmFavoritosG.QrFavoritos3Ordem.Value;
    FmFavoritosG.CorrigeOrdem3(Nivel3, OrdemAtual, EdOrdem.ValueVariant);
    FmFavoritosG.ReopenNivel3(Nivel3);
    Close;
  end;
end;

procedure TFmFavoritos3.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFavoritos3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFavoritos3.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  // Vale para stUpd tamb�m?
  EdOrdem.ValMax := FormatFloat('0', FmFavoritosG.QrFavoritos3.RecordCount + 1);
end;

procedure TFmFavoritos3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

{
procedure TFmFavoritos3.VerificaOrdem(ZeraAntes: Boolean);
begin
  if ZeraAntes then
    EdOrdem.ValueVariant := 0;
  if EdOrdem.ValueVariant = 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT MAX(Ordem) Ordem');
    Dmod.QrAux.SQL.Add('FROM tintastin');
    Dmod.QrAux.SQL.Add('WHERE Numero=:P0');
    Dmod.QrAux.Params[0].AsInteger := FmTintasCab.QrTintasCabNumero.Value;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, ?
    //
    EdOrdem.ValueVariant := Dmod.QrAux.FieldByName('Ordem').AsInteger + 1;
  end;
end;
}

end.
