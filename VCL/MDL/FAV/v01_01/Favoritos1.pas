unit Favoritos1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, Mask, dmkDBEdit, dmkImage, UnDmkEnums;

type
  TFmFavoritos1 = class(TForm)
    Panel1: TPanel;
    Panel6: TPanel;
    Label4: TLabel;
    EdUsuario_Cod: TdmkEdit;
    EdUsuario_Log: TdmkEdit;
    EdNivel1: TdmkEdit;
    Label2: TLabel;
    Panel3: TPanel;
    Label1: TLabel;
    EdOrdem: TdmkEdit;
    Label3: TLabel;
    EdDescri1: TdmkEdit;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    DBEdit1: TdmkDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    Panel5: TPanel;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    DBEdit3: TDBEdit;
    Label9: TLabel;
    EdDescri2: TdmkEdit;
    Label10: TLabel;
    EdDescri3: TdmkEdit;
    EdPage: TdmkEditCB;
    CBPage: TdmkDBLookupComboBox;
    Label11: TLabel;
    Label12: TLabel;
    EdTBar: TdmkEditCB;
    CBTBar: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdBtns: TdmkEditCB;
    CBBtns: TdmkDBLookupComboBox;
    QrPages: TmySQLQuery;
    QrPagesCodigo: TIntegerField;
    QrPagesDescrPage: TWideStringField;
    QrTBars: TmySQLQuery;
    QrTBarsControle: TIntegerField;
    QrTBarsDescrTBar: TWideStringField;
    QrBtns: TmySQLQuery;
    QrBtnsConta: TIntegerField;
    QrBtnsDescrBtn: TWideStringField;
    QrBtnsNome: TWideStringField;
    DsBtns: TDataSource;
    DsTBars: TDataSource;
    DsPages: TDataSource;
    Label14: TLabel;
    DBEdNome: TdmkDBEdit;
    QrBtnsSeq: TIntegerField;
    QrBtnsClasse: TWideStringField;
    Label15: TLabel;
    DbEdClasse: TdmkDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel7: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdBtnsChange(Sender: TObject);
    procedure EdPageChange(Sender: TObject);
    procedure EdTBarChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenBtns();
  public
    { Public declarations }
  end;

  var
  FmFavoritos1: TFmFavoritos1;

implementation

uses FavoritosG, Module, UMySQLModule, UnMyObjects, UnInternalConsts,
  ModuleGeral;

{$R *.DFM}

procedure TFmFavoritos1.BtOKClick(Sender: TObject);
var
  Nivel1, OrdemAtual: Integer;
begin
  if MyObjects.FIC(Trim(EdDescri1.Text) = '', EdDescri1,
    'Informe o texto da linha 1') then Exit;
  if MyObjects.FIC(Trim(DBEdNome.Text) = '', EdBtns,
    'Informe o bot�o') then Exit;
  //
  Nivel1 := UMyMod.BuscaEmLivreY_Def_Geral('Favoritos1', 'Nivel1', ImgTipo.SQLType,
    FmFavoritosG.QrFavoritos1Nivel1.Value, EdNivel1);
  if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, 'Favoritos1', Nivel1,
  Dmod.QrUpd) then
  begin
    if ImgTipo.SQLType = stIns then
      OrdemAtual := 0
    else
      OrdemAtual := FmFavoritosG.QrFavoritos1Ordem.Value;
    FmFavoritosG.CorrigeOrdem1(Nivel1, OrdemAtual,
      EdOrdem.ValueVariant, FmFavoritosG.QrFavoritos2Nivel2.Value);
    FmFavoritosG.ReopenNivel1(Nivel1);
    Close;
  end;
end;

procedure TFmFavoritos1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFavoritos1.EdBtnsChange(Sender: TObject);
begin
  if EdBtns.ValueVariant <> 0 then
    DBEdNome.DataField := 'Nome'
  else
    DBEdNome.DataField := '';
end;

procedure TFmFavoritos1.EdPageChange(Sender: TObject);
var
  Page: Integer;
begin
  Page := EdPage.ValueVariant;
  EdTBar.ValueVariant := 0;
  CBTBar.KeyValue := 0;
  QrTBars.Close;
  if Page <> 0 then
  begin
    QrTBars.Params[0].AsInteger := Page;
    UnDmkDAC_PF.AbreQuery(QrTBars, DmodG.MyPID_DB);
  end;
  ReopenBtns();
end;

procedure TFmFavoritos1.EdTBarChange(Sender: TObject);
begin
  ReopenBtns();
end;

procedure TFmFavoritos1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFavoritos1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  // Vale para stUpd tamb�m?
  EdOrdem.ValMax := FormatFloat('0', FmFavoritosG.QrFavoritos1.RecordCount + 1);
  //

  QrTBars.Close;
  QrTBars.Database := DmodG.MyPID_DB;
  //
  QrBtns.Close;
  QrBtns.Database := DmodG.MyPID_DB;
  //
  QrPages.Close;
  QrPages.Database := DmodG.MyPID_DB;
  UnDmkDAC_PF.AbreQuery(QrPages, DmodG.MyPID_DB);
  //
  ReopenBtns();
  DBEdNome.DataField := '';
end;

procedure TFmFavoritos1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFavoritos1.ReopenBtns();
var
  Page, TBar: Integer;
begin
  Page := EdPage.ValueVariant;
  TBar := EdTBar.ValueVariant;
  EdBtns.ValueVariant := 0;
  CBBtns.KeyValue := 0;
  QrBtns.Close;
  QrBtns.SQL.Clear;
  QrBtns.SQL.Add('SELECT DISTINCT Seq, Conta, DescrBtn, Nome, Classe');
  QrBtns.SQL.Add('FROM _fav_btns_');
  QrBtns.SQL.Add('WHERE Conta>0');
  if Page <> 0 then
  begin
    QrBtns.SQL.Add('AND Codigo=' + FormatFloat('0', Page));
    if TBar <> 0 then
      QrBtns.SQL.Add('AND Controle=' + FormatFloat('0', TBar));
    QrBtns.SQL.Add('');
  end;
  QrBtns.SQL.Add('ORDER BY DescrBtn');
  UnDmkDAC_PF.AbreQuery(QrBtns, DmodG.MyPID_DB);
end;

end.
