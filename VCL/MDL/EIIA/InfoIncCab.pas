unit InfoIncCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBGridZTO, frxClass, UnDmkListas, UnAppPF;

type
  TFmInfoIncCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdModuloDesc: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrInfoIncCab: TmySQLQuery;
    DsInfoIncCab: TDataSource;
    QrInfoIncIts: TmySQLQuery;
    DsInfoIncIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrInfoIncItsCodigo: TIntegerField;
    QrInfoIncItsControle: TIntegerField;
    QrInfoIncItsDataHora: TDateTimeField;
    QrInfoIncItsTexto: TWideStringField;
    QrInfoIncItsAlterWeb: TSmallintField;
    QrInfoIncItsAtivo: TSmallintField;
    EdModuloNiv1: TdmkEdit;
    EdModuloNiv2: TdmkEdit;
    EdModuloNiv3: TdmkEdit;
    EdNivel1: TdmkEdit;
    EdNivel2: TdmkEdit;
    EdNivel3: TdmkEdit;
    EdNivel5: TdmkEdit;
    EdNivel4: TdmkEdit;
    EdNivNivel: TdmkEdit;
    RGStatus: TdmkRadioGroup;
    Label2: TLabel;
    QrInfoIncCabStatus_TXT: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBGInfoIncIts: TdmkDBGridZTO;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    QrInfoIncCabCodigo: TIntegerField;
    QrInfoIncCabAppJanela: TWideStringField;
    QrInfoIncCabModuloDesc: TWideStringField;
    QrInfoIncCabModuloNiv1: TIntegerField;
    QrInfoIncCabModuloNiv2: TIntegerField;
    QrInfoIncCabModuloNiv3: TIntegerField;
    QrInfoIncCabNivNivel: TSmallintField;
    QrInfoIncCabNivValor: TIntegerField;
    QrInfoIncCabNivel0: TIntegerField;
    QrInfoIncCabNivel1: TIntegerField;
    QrInfoIncCabNivel2: TIntegerField;
    QrInfoIncCabNivel3: TIntegerField;
    QrInfoIncCabNivel4: TIntegerField;
    QrInfoIncCabNivel5: TIntegerField;
    QrInfoIncCabStatus: TIntegerField;
    QrInfoIncCabLk: TIntegerField;
    QrInfoIncCabDataCad: TDateField;
    QrInfoIncCabDataAlt: TDateField;
    QrInfoIncCabUserCad: TIntegerField;
    QrInfoIncCabUserAlt: TIntegerField;
    QrInfoIncCabAlterWeb: TSmallintField;
    QrInfoIncCabAtivo: TSmallintField;
    EdNivValor: TdmkEdit;
    DBEdit13: TDBEdit;
    Label4: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    EdNivel0: TdmkEdit;
    Label5: TLabel;
    EdAppJanela: TdmkEdit;
    N1: TMenuItem;
    Localiza1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrInfoIncCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrInfoIncCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrInfoIncCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrInfoIncCabBeforeClose(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrInfoIncCabCalcFields(DataSet: TDataSet);
    procedure Localiza1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraInfoIncIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenInfoIncIts(Controle: Integer);

  end;

var
  FmInfoIncCab: TFmInfoIncCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, InfoIncIts, UnEiia_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmInfoIncCab.Localiza1Click(Sender: TObject);
begin
  AppPF.MostraJanelaOrigemInfoIncCab(QrInfoIncCabCodigo.Value);
end;

procedure TFmInfoIncCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmInfoIncCab.MostraInfoIncIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmInfoIncIts, FmInfoIncIts, afmoNegarComAviso) then
  begin
    FmInfoIncIts.ImgTipo.SQLType := SQLType;
    FmInfoIncIts.FQrCab := QrInfoIncCab;
    FmInfoIncIts.FDsCab := DsInfoIncCab;
    FmInfoIncIts.FQrIts := QrInfoIncIts;
    if SQLType = stIns then
      //
    else
    begin
      FmInfoIncIts.EdControle.ValueVariant := QrInfoIncItsControle.Value;
      //
      FmInfoIncIts.TPData.Date := QrInfoIncItsDataHora.Value;
      FmInfoIncIts.EdHora.ValueVariant := QrInfoIncItsDataHora.Value;
      FmInfoIncIts.MeTexto.Text := QrInfoIncItsTexto.Value;
    end;
    FmInfoIncIts.ShowModal;
    FmInfoIncIts.Destroy;
  end;
end;

procedure TFmInfoIncCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrInfoIncCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrInfoIncCab, QrInfoIncIts);
end;

procedure TFmInfoIncCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrInfoIncCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrInfoIncIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrInfoIncIts);
end;

procedure TFmInfoIncCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrInfoIncCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmInfoIncCab.DefParams;
begin
  VAR_GOTOTABELA := 'infoinccab';
  VAR_GOTOMYSQLTABLE := QrInfoIncCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT iic.* ');
  VAR_SQLx.Add('FROM infoinccab iic');
  VAR_SQLx.Add('WHERE iic.Codigo > 0');
  //
  VAR_SQL1.Add('AND iic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmInfoIncCab.ItsAltera1Click(Sender: TObject);
begin
  MostraInfoIncIts(stUpd);
end;

procedure TFmInfoIncCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmInfoIncCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmInfoIncCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmInfoIncCab.ItsExclui1Click(Sender: TObject);
(*
var
  Controle: Integer;
*)
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'InfoIncIts', 'Controle', QrInfoIncItsControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrInfoIncIts,
      QrInfoIncItsControle, QrInfoIncItsControle.Value);
    ReopenInfoIncIts(Controle);
  end;
}
end;

procedure TFmInfoIncCab.ReopenInfoIncIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrInfoIncIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM infoincits ',
  'WHERE Codigo=' + Geral.FF0(QrInfoIncCabCodigo.Value),
  '']);
  //
  QrInfoIncIts.Locate('Controle', Controle, []);
end;


procedure TFmInfoIncCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmInfoIncCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmInfoIncCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmInfoIncCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmInfoIncCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmInfoIncCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmInfoIncCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrInfoIncCabCodigo.Value;
  Close;
end;

procedure TFmInfoIncCab.ItsInclui1Click(Sender: TObject);
begin
  MostraInfoIncIts(stIns);
end;

procedure TFmInfoIncCab.CabAltera1Click(Sender: TObject);
var
  Status: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrInfoIncCab, [PnDados],
  [PnEdita], RGStatus, ImgTipo, 'infoinccab');
  //
  Status := (QrInfoIncCabStatus.Value div 4) - 1;
  RGStatus.ItemIndex := Status;
end;

procedure TFmInfoIncCab.BtConfirmaClick(Sender: TObject);
var
  AppJanela, ModuloDesc: String;
  Codigo, ModuloNiv1, ModuloNiv2, ModuloNiv3, NivNivel, NivValor, Nivel0,
  Nivel1, Nivel2, Nivel3, Nivel4, Nivel5, Status: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  AppJanela      := EdAppJanela.Text;
  ModuloDesc     := EdModuloDesc.Text;
  ModuloNiv1     := EdModuloNiv1.ValueVariant;
  ModuloNiv2     := EdModuloNiv2.ValueVariant;
  ModuloNiv3     := EdModuloNiv3.ValueVariant;
  NivNivel       := EdNivNivel.ValueVariant;
  NivValor       := EdNivValor.ValueVariant;
  Nivel0         := EdNivel0.ValueVariant;
  Nivel1         := EdNivel1.ValueVariant;
  Nivel2         := EdNivel2.ValueVariant;
  Nivel3         := EdNivel3.ValueVariant;
  Nivel4         := EdNivel4.ValueVariant;
  Nivel5         := EdNivel5.ValueVariant;
  Status         := (RGStatus.ItemIndex + 1) * 4;
  //
  Codigo := UMyMod.BPGS1I32('infoinccab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'infoinccab', False, [
  'AppJanela', 'ModuloDesc', 'ModuloNiv1',
  'ModuloNiv2', 'ModuloNiv3', 'NivNivel',
  'NivValor', 'Nivel0', 'Nivel1',
  'Nivel2', 'Nivel3', 'Nivel4',
  'Nivel5', 'Status'], [
  'Codigo'], [
  AppJanela, ModuloDesc, ModuloNiv1,
  ModuloNiv2, ModuloNiv3, NivNivel,
  NivValor, Nivel0, Nivel1,
  Nivel2, Nivel3, Nivel4,
  Nivel5, Status], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
//
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmInfoIncCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'infoinccab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'infoinccab', 'Codigo');
end;

procedure TFmInfoIncCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmInfoIncCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmInfoIncCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DBGInfoIncIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmInfoIncCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrInfoIncCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmInfoIncCab.SbImprimeClick(Sender: TObject);
begin
  Eiia_PF.MostraFormInfoIncImp();
end;

procedure TFmInfoIncCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmInfoIncCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrInfoIncCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmInfoIncCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmInfoIncCab.QrInfoIncCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmInfoIncCab.QrInfoIncCabAfterScroll(DataSet: TDataSet);
begin
  ReopenInfoIncIts(0);
end;

procedure TFmInfoIncCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrInfoIncCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmInfoIncCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrInfoIncCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'infoinccab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmInfoIncCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmInfoIncCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrInfoIncCab, [PnDados],
  [PnEdita], RGStatus, ImgTipo, 'infoinccab');
end;

procedure TFmInfoIncCab.QrInfoIncCabBeforeClose(
  DataSet: TDataSet);
begin
  QrInfoIncIts.Close;
end;

procedure TFmInfoIncCab.QrInfoIncCabBeforeOpen(DataSet: TDataSet);
begin
  QrInfoIncCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmInfoIncCab.QrInfoIncCabCalcFields(DataSet: TDataSet);
begin
  QrInfoIncCabStatus_TXT.Value :=
    DmkListas.StatusInformeInconsistente(TInfoInconsSta(QrInfoIncCabStatus.Value));
end;

end.

