object FmInfoIncImp: TFmInfoIncImp
  Left = 339
  Top = 185
  Caption = 'INF-INCST-003 :: Impress'#245'es de Inconsist'#234'ncia de Informa'#231#227'o'
  ClientHeight = 254
  ClientWidth = 722
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 722
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 674
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 626
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 542
        Height = 32
        Caption = 'Impress'#245'es de Inconsist'#234'ncia de Informa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 542
        Height = 32
        Caption = 'Impress'#245'es de Inconsist'#234'ncia de Informa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 542
        Height = 32
        Caption = 'Impress'#245'es de Inconsist'#234'ncia de Informa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 722
    Height = 92
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 722
      Height = 92
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      ExplicitHeight = 467
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 722
        Height = 92
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 4
        ExplicitWidth = 927
        ExplicitHeight = 428
        object CGStatus: TdmkCheckGroup
          Left = 12
          Top = 8
          Width = 697
          Height = 73
          Caption = ' Status da Inconsist'#234'ncia: '
          Columns = 5
          ItemIndex = 6
          Items.Strings = (
            'Aberto'
            'Reportado'
            'ignorado'
            'N'#227'o aceito'
            'Em discuss'#227'o'
            'Discutido'
            'A resolver'
            'N'#227'o resolvido'
            'Insol'#250'vel'
            'Resolvido')
          TabOrder = 0
          UpdType = utYes
          Value = 127
          OldValor = 0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 140
    Width = 722
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 718
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 184
    Width = 722
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 576
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 574
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 24
    Top = 11
  end
  object frxINF_INCST_001_00_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxINF_INCST_001_00_AGetValue
    Left = 516
    Top = 140
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsInfoIncCab
        DataSetName = 'frxDsInfoIncCab'
      end
      item
        DataSet = frxDsInfoIncIts
        DataSetName = 'frxDsInfoIncIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 77.480356460000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        object Shape3: TfrxShapeView
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 994.016390000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 1009.134510000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 782.362710000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Inconsist'#234'ncias de Informa'#231#245'es')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 895.748610000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 37.795300000000000000
          Top = 64.252010000000000000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Janela - Data/hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitCodi: TfrxMemoView
          Top = 64.252010000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 585.827150000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID Nivel 1')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 525.354670000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID Nivel 0')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo19: TfrxMemoView
          Left = 434.645950000000000000
          Top = 64.252010000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Niv1')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          Left = 464.882190000000000000
          Top = 64.252010000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Niv2')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Left = 109.606370000000000000
          Top = 64.252010000000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'M'#243'dulo - Texto')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 495.118430000000000000
          Top = 64.252010000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Niv3')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 706.772110000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID Nivel 3')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 646.299630000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID Nivel 2')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 827.717070000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID Nivel 5')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Left = 767.244590000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID Nivel 4')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Left = 948.662030000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID Sel.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Left = 888.189550000000000000
          Top = 64.252010000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Nivel Sel.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo22: TfrxMemoView
          Left = 222.992270000000000000
          Top = 64.252010000000000000
          Width = 211.653533540000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 185.196970000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsInfoIncCab
        DataSetName = 'frxDsInfoIncCab'
        PrintIfDetailEmpty = True
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 37.795300000000000000
          Width = 71.811021180000000000
          Height = 15.118110240000000000
          DataField = 'AppJanela'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          Memo.UTF8W = (
            '[frxDsInfoIncCab."AppJanela"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'Codigo'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.LeftLine.Width = 1.000000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsInfoIncCab."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 585.827150000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Nivel1'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInfoIncCab."Nivel1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 525.354670000000100000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Nivel0'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInfoIncCab."Nivel0"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 434.645950000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'ModuloNiv1'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInfoIncCab."ModuloNiv1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 464.882190000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'ModuloNiv2'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInfoIncCab."ModuloNiv2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 109.606370000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'ModuloDesc'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          Memo.UTF8W = (
            '[frxDsInfoIncCab."ModuloDesc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 495.118430000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'ModuloNiv3'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInfoIncCab."ModuloNiv3"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 706.772110000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Nivel3'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInfoIncCab."Nivel3"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 646.299630000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Nivel2'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInfoIncCab."Nivel2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Nivel5'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInfoIncCab."Nivel5"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Nivel4'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInfoIncCab."Nivel4"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 948.662030000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'NivValor'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          Frame.RightLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInfoIncCab."NivValor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 888.189550000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'NivNivel'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInfoIncCab."NivNivel"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 222.992270000000000000
          Width = 211.653533540000000000
          Height = 15.118110240000000000
          DataField = 'Status_TXT'
          DataSet = frxDsInfoIncCab
          DataSetName = 'frxDsInfoIncCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          Memo.UTF8W = (
            '[frxDsInfoIncCab."Status_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 325.039580000000000000
        Width = 1009.134510000000000000
        object Memo93: TfrxMemoView
          Width = 763.465060000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 744.567410000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 249.448980000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsInfoIncIts
        DataSetName = 'frxDsInfoIncIts'
        RowCount = 0
        object Memo15: TfrxMemoView
          Left = 151.181200000000000000
          Width = 857.953310000000000000
          Height = 15.118110240000000000
          DataField = 'Texto'
          DataSet = frxDsInfoIncIts
          DataSetName = 'frxDsInfoIncIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.RightLine.Width = 1.000000000000000000
          Memo.UTF8W = (
            '[frxDsInfoIncIts."Texto"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsInfoIncIts
          DataSetName = 'frxDsInfoIncIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.LeftLine.Width = 1.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsInfoIncIts."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 37.795300000000000000
          Width = 113.385851180000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsInfoIncIts
          DataSetName = 'frxDsInfoIncIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsInfoIncIts."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 158.740260000000000000
        Width = 1009.134510000000000000
        object Line1: TfrxLineView
          Top = 3.779527559999991000
          Width = 1009.134510000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 222.992270000000000000
        Width = 1009.134510000000000000
        object Line2: TfrxLineView
          Width = 1009.134510000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
    end
  end
  object QrInfoIncCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrInfoIncCabBeforeClose
    AfterScroll = QrInfoIncCabAfterScroll
    OnCalcFields = QrInfoIncCabCalcFields
    SQL.Strings = (
      'SELECT * FROM infoinccab'
      'WHERE Codigo > 0')
    Left = 324
    Top = 137
    object QrInfoIncCabStatus_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Status_TXT'
      Size = 50
      Calculated = True
    end
    object QrInfoIncCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrInfoIncCabAppJanela: TWideStringField
      FieldName = 'AppJanela'
      Size = 13
    end
    object QrInfoIncCabModuloDesc: TWideStringField
      FieldName = 'ModuloDesc'
      Size = 30
    end
    object QrInfoIncCabModuloNiv1: TIntegerField
      FieldName = 'ModuloNiv1'
    end
    object QrInfoIncCabModuloNiv2: TIntegerField
      FieldName = 'ModuloNiv2'
    end
    object QrInfoIncCabModuloNiv3: TIntegerField
      FieldName = 'ModuloNiv3'
    end
    object QrInfoIncCabNivNivel: TSmallintField
      FieldName = 'NivNivel'
    end
    object QrInfoIncCabNivValor: TIntegerField
      FieldName = 'NivValor'
    end
    object QrInfoIncCabNivel0: TIntegerField
      FieldName = 'Nivel0'
    end
    object QrInfoIncCabNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrInfoIncCabNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrInfoIncCabNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrInfoIncCabNivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrInfoIncCabNivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrInfoIncCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrInfoIncCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrInfoIncCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrInfoIncCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrInfoIncCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrInfoIncCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrInfoIncCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrInfoIncCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrInfoIncIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM infoincits'
      'WHERE Codigo =:P0'
      'ORDER BY Controle')
    Left = 412
    Top = 137
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrInfoIncItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrInfoIncItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrInfoIncItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrInfoIncItsTexto: TWideStringField
      FieldName = 'Texto'
      Size = 255
    end
    object QrInfoIncItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrInfoIncItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object frxDsInfoIncCab: TfrxDBDataset
    UserName = 'frxDsInfoIncCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Status_TXT=Status_TXT'
      'Codigo=Codigo'
      'AppJanela=AppJanela'
      'ModuloDesc=ModuloDesc'
      'ModuloNiv1=ModuloNiv1'
      'ModuloNiv2=ModuloNiv2'
      'ModuloNiv3=ModuloNiv3'
      'NivNivel=NivNivel'
      'NivValor=NivValor'
      'Nivel0=Nivel0'
      'Nivel1=Nivel1'
      'Nivel2=Nivel2'
      'Nivel3=Nivel3'
      'Nivel4=Nivel4'
      'Nivel5=Nivel5'
      'Status=Status'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrInfoIncCab
    BCDToCurrency = False
    Left = 324
    Top = 184
  end
  object frxDsInfoIncIts: TfrxDBDataset
    UserName = 'frxDsInfoIncIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'DataHora=DataHora'
      'Texto=Texto'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrInfoIncIts
    BCDToCurrency = False
    Left = 412
    Top = 184
  end
end
