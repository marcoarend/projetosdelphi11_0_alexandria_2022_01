object FmInfoIncCab: TFmInfoIncCab
  Left = 368
  Top = 194
  Caption = 'INF-INCST-001 :: Inconsist'#234'ncia de Informa'#231#227'o'
  ClientHeight = 400
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 304
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 180
        Top = 16
        Width = 104
        Height = 13
        Caption = 'Descri'#231#227'o do M'#243'dulo:'
        Color = clBtnFace
        ParentColor = False
      end
      object TLabel
        Left = 380
        Top = 16
        Width = 81
        Height = 13
        Caption = 'M'#243'dulo Niv.1-2-3'
        Color = clBtnFace
        ParentColor = False
      end
      object TLabel
        Left = 480
        Top = 16
        Width = 145
        Height = 13
        Caption = 'Lan'#231'amento Niveis 0-1-2-3-4-5'
        Color = clBtnFace
        ParentColor = False
      end
      object TLabel
        Left = 868
        Top = 16
        Width = 61
        Height = 13
        Caption = 'N'#237'vel objeto:'
        Color = clBtnFace
        ParentColor = False
      end
      object TLabel
        Left = 936
        Top = 16
        Width = 46
        Height = 13
        Caption = 'ID objeto:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 68
        Top = 16
        Width = 104
        Height = 13
        Caption = 'Descri'#231#227'o do M'#243'dulo:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdModuloDesc: TdmkEdit
        Left = 180
        Top = 32
        Width = 197
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'ModuloDesc'
        UpdCampo = 'ModuloDesc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdModuloNiv1: TdmkEdit
        Left = 380
        Top = 32
        Width = 30
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ModuloNiv1'
        UpdCampo = 'ModuloNiv1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdModuloNiv2: TdmkEdit
        Left = 412
        Top = 32
        Width = 30
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ModuloNiv2'
        UpdCampo = 'ModuloNiv2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdModuloNiv3: TdmkEdit
        Left = 444
        Top = 32
        Width = 30
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ModuloNiv3'
        UpdCampo = 'ModuloNiv3'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNivel1: TdmkEdit
        Left = 544
        Top = 32
        Width = 62
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Nivel1'
        UpdCampo = 'Nivel1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNivel2: TdmkEdit
        Left = 608
        Top = 32
        Width = 62
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Nivel2'
        UpdCampo = 'Nivel2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNivel3: TdmkEdit
        Left = 672
        Top = 32
        Width = 62
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Nivel3'
        UpdCampo = 'Nivel3'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNivel5: TdmkEdit
        Left = 800
        Top = 32
        Width = 62
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Nivel5'
        UpdCampo = 'Nivel5'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNivel4: TdmkEdit
        Left = 736
        Top = 32
        Width = 62
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Nivel4'
        UpdCampo = 'Nivel4'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNivNivel: TdmkEdit
        Left = 868
        Top = 32
        Width = 62
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'NivNivel'
        UpdCampo = 'NivNivel'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNivValor: TdmkEdit
        Left = 936
        Top = 32
        Width = 62
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'NivValor'
        UpdCampo = 'NivValor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNivel0: TdmkEdit
        Left = 480
        Top = 32
        Width = 62
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Nivel0'
        UpdCampo = 'Nivel0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdAppJanela: TdmkEdit
        Left = 68
        Top = 32
        Width = 109
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'AppJanela'
        UpdCampo = 'AppJanela'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 241
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object RGStatus: TdmkRadioGroup
      Left = 16
      Top = 60
      Width = 969
      Height = 73
      Caption = ' Status da Inconsist'#234'ncia: '
      Columns = 5
      ItemIndex = 0
      Items.Strings = (
        'Aberto'
        'Reportado'
        'ignorado'
        'N'#227'o aceito'
        'Em discuss'#227'o'
        'Discutido'
        'A resolver'
        'N'#227'o resolvido'
        'Insol'#250'vel'
        'Resolvido')
      TabOrder = 2
      QryCampo = 'Status'
      UpdCampo = 'Status'
      UpdType = utYes
      OldValor = 0
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 304
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 89
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 4
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 192
        Top = 8
        Width = 104
        Height = 13
        Caption = 'Descri'#231#227'o do M'#243'dulo:'
        Color = clBtnFace
        ParentColor = False
      end
      object TLabel
        Left = 388
        Top = 8
        Width = 81
        Height = 13
        Caption = 'M'#243'dulo Niv.1-2-3'
        Color = clBtnFace
        ParentColor = False
      end
      object TLabel
        Left = 488
        Top = 8
        Width = 145
        Height = 13
        Caption = 'Lan'#231'amento Niveis 0-1-2-3-4-5'
        Color = clBtnFace
        ParentColor = False
      end
      object TLabel
        Left = 876
        Top = 8
        Width = 61
        Height = 13
        Caption = 'N'#237'vel objeto:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 4
        Top = 48
        Width = 30
        Height = 13
        Caption = 'Status'
        FocusControl = DBEdit1
      end
      object TLabel
        Left = 940
        Top = 8
        Width = 46
        Height = 13
        Caption = 'ID objeto:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 76
        Top = 8
        Width = 63
        Height = 13
        Caption = 'ID da Janela:'
        FocusControl = DBEdit14
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 4
        Top = 24
        Width = 69
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsInfoIncCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 4
        Top = 64
        Width = 53
        Height = 21
        DataField = 'Status'
        DataSource = DsInfoIncCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 60
        Top = 64
        Width = 941
        Height = 21
        DataField = 'Status_TXT'
        DataSource = DsInfoIncCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 192
        Top = 24
        Width = 193
        Height = 21
        DataField = 'ModuloDesc'
        DataSource = DsInfoIncCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 388
        Top = 24
        Width = 30
        Height = 21
        DataField = 'ModuloNiv1'
        DataSource = DsInfoIncCab
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 420
        Top = 24
        Width = 30
        Height = 21
        DataField = 'ModuloNiv2'
        DataSource = DsInfoIncCab
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 452
        Top = 24
        Width = 30
        Height = 21
        DataField = 'ModuloNiv3'
        DataSource = DsInfoIncCab
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 552
        Top = 24
        Width = 62
        Height = 21
        DataField = 'Nivel1'
        DataSource = DsInfoIncCab
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 616
        Top = 24
        Width = 62
        Height = 21
        DataField = 'Nivel2'
        DataSource = DsInfoIncCab
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 680
        Top = 24
        Width = 62
        Height = 21
        DataField = 'Nivel3'
        DataSource = DsInfoIncCab
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 744
        Top = 24
        Width = 62
        Height = 21
        DataField = 'Nivel4'
        DataSource = DsInfoIncCab
        TabOrder = 10
      end
      object DBEdit11: TDBEdit
        Left = 808
        Top = 24
        Width = 62
        Height = 21
        DataField = 'Nivel5'
        DataSource = DsInfoIncCab
        TabOrder = 11
      end
      object DBEdit12: TDBEdit
        Left = 876
        Top = 24
        Width = 62
        Height = 21
        DataField = 'NivNivel'
        DataSource = DsInfoIncCab
        TabOrder = 12
      end
      object DBEdit13: TDBEdit
        Left = 940
        Top = 24
        Width = 62
        Height = 21
        DataField = 'NivValor'
        DataSource = DsInfoIncCab
        TabOrder = 13
      end
      object DBEdit14: TDBEdit
        Left = 76
        Top = 24
        Width = 113
        Height = 21
        DataField = 'AppJanela'
        DataSource = DsInfoIncCab
        TabOrder = 14
      end
      object DBEdit15: TDBEdit
        Left = 488
        Top = 24
        Width = 62
        Height = 21
        DataField = 'Nivel0'
        DataSource = DsInfoIncCab
        TabOrder = 15
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 240
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Linha (texto)'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DBGInfoIncIts: TdmkDBGridZTO
      Left = 0
      Top = 89
      Width = 1008
      Height = 120
      Align = alTop
      DataSource = DsInfoIncIts
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data/hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 355
        Height = 32
        Caption = 'Inconsist'#234'ncia de Informa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 355
        Height = 32
        Caption = 'Inconsist'#234'ncia de Informa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 355
        Height = 32
        Caption = 'Inconsist'#234'ncia de Informa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrInfoIncCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrInfoIncCabBeforeOpen
    AfterOpen = QrInfoIncCabAfterOpen
    BeforeClose = QrInfoIncCabBeforeClose
    AfterScroll = QrInfoIncCabAfterScroll
    OnCalcFields = QrInfoIncCabCalcFields
    SQL.Strings = (
      'SELECT * FROM infoinccab'
      'WHERE Codigo > 0')
    Left = 644
    Top = 153
    object QrInfoIncCabStatus_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Status_TXT'
      Size = 50
      Calculated = True
    end
    object QrInfoIncCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrInfoIncCabAppJanela: TWideStringField
      FieldName = 'AppJanela'
      Size = 13
    end
    object QrInfoIncCabModuloDesc: TWideStringField
      FieldName = 'ModuloDesc'
      Size = 30
    end
    object QrInfoIncCabModuloNiv1: TIntegerField
      FieldName = 'ModuloNiv1'
    end
    object QrInfoIncCabModuloNiv2: TIntegerField
      FieldName = 'ModuloNiv2'
    end
    object QrInfoIncCabModuloNiv3: TIntegerField
      FieldName = 'ModuloNiv3'
    end
    object QrInfoIncCabNivNivel: TSmallintField
      FieldName = 'NivNivel'
    end
    object QrInfoIncCabNivValor: TIntegerField
      FieldName = 'NivValor'
    end
    object QrInfoIncCabNivel0: TIntegerField
      FieldName = 'Nivel0'
    end
    object QrInfoIncCabNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrInfoIncCabNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrInfoIncCabNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrInfoIncCabNivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrInfoIncCabNivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrInfoIncCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrInfoIncCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrInfoIncCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrInfoIncCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrInfoIncCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrInfoIncCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrInfoIncCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrInfoIncCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsInfoIncCab: TDataSource
    DataSet = QrInfoIncCab
    Left = 644
    Top = 197
  end
  object QrInfoIncIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM infoincits'
      'WHERE Codigo =:P0'
      'ORDER BY Controle')
    Left = 740
    Top = 157
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrInfoIncItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrInfoIncItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrInfoIncItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrInfoIncItsTexto: TWideStringField
      FieldName = 'Texto'
      Size = 255
    end
    object QrInfoIncItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrInfoIncItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsInfoIncIts: TDataSource
    DataSet = QrInfoIncIts
    Left = 740
    Top = 201
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 224
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Localiza1: TMenuItem
      Caption = '&Localiza'
      OnClick = Localiza1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 220
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
end
