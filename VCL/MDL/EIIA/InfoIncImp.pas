unit InfoIncImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkCheckGroup, dmkRadioGroup, mySQLDbTables, frxClass, frxDBSet, UnDmkListas;

type
  TFmInfoIncImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    CGStatus: TdmkCheckGroup;
    frxINF_INCST_001_00_A: TfrxReport;
    QrInfoIncCab: TmySQLQuery;
    QrInfoIncCabStatus_TXT: TWideStringField;
    QrInfoIncCabCodigo: TIntegerField;
    QrInfoIncCabAppJanela: TWideStringField;
    QrInfoIncCabModuloDesc: TWideStringField;
    QrInfoIncCabModuloNiv1: TIntegerField;
    QrInfoIncCabModuloNiv2: TIntegerField;
    QrInfoIncCabModuloNiv3: TIntegerField;
    QrInfoIncCabNivNivel: TSmallintField;
    QrInfoIncCabNivValor: TIntegerField;
    QrInfoIncCabNivel0: TIntegerField;
    QrInfoIncCabNivel1: TIntegerField;
    QrInfoIncCabNivel2: TIntegerField;
    QrInfoIncCabNivel3: TIntegerField;
    QrInfoIncCabNivel4: TIntegerField;
    QrInfoIncCabNivel5: TIntegerField;
    QrInfoIncCabStatus: TIntegerField;
    QrInfoIncCabLk: TIntegerField;
    QrInfoIncCabDataCad: TDateField;
    QrInfoIncCabDataAlt: TDateField;
    QrInfoIncCabUserCad: TIntegerField;
    QrInfoIncCabUserAlt: TIntegerField;
    QrInfoIncCabAlterWeb: TSmallintField;
    QrInfoIncCabAtivo: TSmallintField;
    QrInfoIncIts: TmySQLQuery;
    QrInfoIncItsCodigo: TIntegerField;
    QrInfoIncItsControle: TIntegerField;
    QrInfoIncItsDataHora: TDateTimeField;
    QrInfoIncItsTexto: TWideStringField;
    QrInfoIncItsAlterWeb: TSmallintField;
    QrInfoIncItsAtivo: TSmallintField;
    frxDsInfoIncCab: TfrxDBDataset;
    frxDsInfoIncIts: TfrxDBDataset;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frxINF_INCST_001_00_AGetValue(const VarName: string;
      var Value: Variant);
    procedure BtOKClick(Sender: TObject);
    procedure QrInfoIncCabAfterScroll(DataSet: TDataSet);
    procedure QrInfoIncCabBeforeClose(DataSet: TDataSet);
    procedure QrInfoIncCabCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenInfoIncIts();
  public
    { Public declarations }
    procedure ImprimeInconsistencias();
  end;

  var
  FmInfoIncImp: TFmInfoIncImp;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmInfoIncImp.BtOKClick(Sender: TObject);
begin
  ImprimeInconsistencias();
end;

procedure TFmInfoIncImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmInfoIncImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmInfoIncImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  CGStatus.Value := 127;
end;

procedure TFmInfoIncImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmInfoIncImp.frxINF_INCST_001_00_AGetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName = 'VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmInfoIncImp.ImprimeInconsistencias();
var
  I: Integer;
  Status: String;
begin
  Status := '';
  for I := 0 to CGStatus.Items.Count -1 do
  begin
    if CGStatus.Checked[I] then
    begin
      if Status <> '' then
        Status := Status + ',';
      Status := Status + Geral.FF0((I + 1) * 4);
    end;
  end;
  if Status <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrInfoIncCab, Dmod.MyDB, [
    'SELECT * ',
    'FROM infoinccab ',
    'WHERE Status IN (' + Status + ')',
    'ORDER BY Codigo',
    '']);
    //
    MyObjects.frxDefineDataSets(frxINF_INCST_001_00_A, [
      DModG.frxDsDono,
      frxDsInfoIncCab,
      frxDsInfoIncIts
    ]);
    //
    MyObjects.frxMostra(frxINF_INCST_001_00_A, 'Inconsistências de Informações');
  end else
    Geral.MB_Aviso('Nenhum status foi selecionado!');
end;

procedure TFmInfoIncImp.QrInfoIncCabAfterScroll(DataSet: TDataSet);
begin
  ReopenInfoIncIts();
end;

procedure TFmInfoIncImp.QrInfoIncCabBeforeClose(DataSet: TDataSet);
begin
  QrInfoIncIts.Close;
end;

procedure TFmInfoIncImp.QrInfoIncCabCalcFields(DataSet: TDataSet);
begin
  QrInfoIncCabStatus_TXT.Value :=
    DmkListas.StatusInformeInconsistente(TInfoInconsSta(QrInfoIncCabStatus.Value));
end;

procedure TFmInfoIncImp.ReopenInfoIncIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrInfoIncIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM infoincits ',
  'WHERE Codigo=' + Geral.FF0(QrInfoIncCabCodigo.Value),
  '']);
end;

end.
