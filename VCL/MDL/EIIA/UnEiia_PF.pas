unit UnEiia_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  dmkDBGridZTO, UnDmkProcFunc, UnProjGroup_Vars, BlueDermConsts, TypInfo,
  System.Math, UnProjGroup_Consts, DBCtrls, Grids, Mask,
  dmkEditDateTimePicker, UnGrl_Consts, UnGrl_Geral,
  dmkDBEdit, dmkDBGrid;

type
  TUnEiia_PF = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure InsereNovaInconsistenciaDeInformacao((*ModuloDesc: String;
              ModuloNiv1, ModuloNiv2, ModuloNiv3, NivelX, Nivel1, Nivel2,
              Nivel3, Nivel4, Nivel5: Integer*));
    procedure MostraFormInfoIncCab(Codigo: Integer);
    procedure MostraFormInfoIncImp();
  end;

var
  Eiia_PF: TUnEiia_PF;

implementation

uses
  Module, ModuleGeral, UMySQLModule, MyDBCheck, InfoIncCab, InfoIncImp;

{ TUnEiia_PF }

procedure TUnEiia_PF.InsereNovaInconsistenciaDeInformacao((*ModuloDesc: String;
  ModuloNiv1, ModuloNiv2, ModuloNiv3, NivelX, Nivel1, Nivel2, Nivel3, Nivel4,
  Nivel5: Integer*));
const
  sProcName = 'UnEiia_PF.InsereNovaInconsistenciaDeInformacao()';
  SQLType = TSQLType.stIns;
  Status  = Integer(TInfoInconsSta.infincstaAberto);
var
  AppJanela, ModuloDesc: String;
  Codigo, ModuloNiv1, ModuloNiv2, ModuloNiv3, NivNivel, NivValor, Nivel0,
  Nivel1, Nivel2, Nivel3, Nivel4, Nivel5: Integer;
  //
  I: Integer;
  Ds: TDataSource;
  Qr: TDataSet;
  GrupoJan, Tabela: String;
  Modulo: TdmkModuloApp;
  Fld: TField;
  //
var
  Texto, DataHora: String;
  Controle: Integer;
begin
  Codigo         := 0;
  AppJanela      := '';
  ModuloDesc     := '';
  ModuloNiv1     := 0;
  ModuloNiv2     := 0;
  ModuloNiv3     := 0;
  NivNivel       := 0;
  NivValor       := 0;
  Nivel0         := 0;
  Nivel1         := 0;
  Nivel2         := 0;
  Nivel3         := 0;
  Nivel4         := 0;
  Nivel5         := 0;
  //Status         := 0; Acima > const
  //
  Tabela := '';
  if (VAR_POPUP_ACTIVE_CONTROL is TDBEdit)
  or (VAR_POPUP_ACTIVE_CONTROL is TdmkDBEdit) then
  begin
    Ds := TDBEdit(VAR_POPUP_ACTIVE_CONTROL).DataSource;
    Qr := Ds.DataSet;
    Tabela := Qr.Name;
  end else
  if (VAR_POPUP_ACTIVE_CONTROL is TDBGrid)
  or (VAR_POPUP_ACTIVE_CONTROL is TdmkDBGrid)
  or (VAR_POPUP_ACTIVE_CONTROL is TdmkDBGridZTO) then
  begin
    Ds := TDBGrid(VAR_POPUP_ACTIVE_CONTROL).DataSource;
    Qr := Ds.DataSet;
    Tabela := Qr.Name;
  end
  else Geral.MB_Info('O componente ' + TForm(VAR_POPUP_ACTIVE_FORM).Caption +
  '.' + TComponent(VAR_POPUP_ACTIVE_CONTROL).Name +
  ' n�o est� implementado em "' + sProcName + '"!' + sLineBreak +
  'Clique em outro controle e chame o Menu Geral novamente..' + sLineBreak +
  '.. ou solicite � DERMATEK implementa��o para este controle caso necessite!');
  //
  if Tabela <> '' then
  begin
    //Geral.MB_Info(TForm(VAR_POPUP_ACTIVE_FORM).Caption + '.' + Tabela);
    AppJanela  := Copy(TForm(VAR_POPUP_ACTIVE_FORM).Caption, 1, 13);
    ModuloDesc := Grl_Geral.ObtemModuloDeGrupoJan(AppJanela);
    Modulo := Grl_Geral.ObtemModuloDeSigla(ModuloDesc);
    //
    ModuloNiv1 := Integer(Modulo);
    case Modulo of
      TdmkModuloApp.mdlappVS:
      begin
        ModuloNiv2     := 0;
        ModuloNiv3     := 0;
        Fld := Qr.FindField('Codigo');
        if Fld <> nil then
        begin
          NivNivel      := 1;
          Nivel1        := Fld.AsInteger;
          NivValor      := Nivel1;
        end;
        Fld := Qr.FindField('MovimCod');
        if Fld <> nil then
        begin
          NivNivel      := 0;
          Nivel0        := Fld.AsInteger;
          NivValor      := Nivel0;
        end;
        Fld := Qr.FindField('Controle');
        if Fld <> nil then
        begin
          NivNivel      := 2;
          Nivel2        := Fld.AsInteger;
          NivValor      := Nivel2;
        end;
      end
      else
      Geral.MB_Aviso('M�dulo ' +
      GetEnumName(TypeInfo(TdmkModuloApp), Integer(Modulo)) +
      ' n�o implementado em ' + sProcName);
    end;
  end;
  //
  Texto   := '';
  if InputQuery('Inconsist�ncia de Informa��o',
  'Descreva o ocorrido com o m�ximo de detalhes. M�ximo 255 caracteres:',
  Texto) then
  begin
    Codigo  := UMyMod.BPGS1I32('infoinccab', 'Codigo', '', '', tsPos, SQLType, Codigo);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'infoinccab', False, [
    'AppJanela', 'ModuloDesc', 'ModuloNiv1',
    'ModuloNiv2', 'ModuloNiv3', 'NivNivel',
    'NivValor', 'Nivel0', 'Nivel1',
    'Nivel2', 'Nivel3', 'Nivel4',
    'Nivel5', 'Status'], [
    'Codigo'], [
    AppJanela, ModuloDesc, ModuloNiv1,
    ModuloNiv2, ModuloNiv3, NivNivel,
    NivValor, Nivel0, Nivel1,
    Nivel2, Nivel3, Nivel4,
    Nivel5, Status], [
    Codigo], True) then
    begin
      DataHora := Geral.FDT(DModG.ObtemAgora(), 109);
      Controle := UMyMod.BPGS1I32('infoincits', 'Controle', '', '', tsPos, SQLType, Controle);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'infoincits', False, [
      'Codigo', 'Texto', 'DataHora'], [
      'Controle'], [
      Codigo, Texto, DataHora], [
      Controle], True) then
      begin
        if Geral.MB_Pergunta(
        'Deseja visualizar / gerenciar a inconsist�ncia rec�m aberta?') =
        ID_YES then
          MostraFormInfoIncCab(Codigo);
      end;
    end;
  end;
end;

procedure TUnEiia_PF.MostraFormInfoIncCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmInfoIncCab, FmInfoIncCab, afmoNegarComAviso) then
  begin
    FmInfoIncCab.LocCod(Codigo, Codigo);
    //
    FmInfoIncCab.ShowModal;
    FmInfoIncCab.Destroy;
  end;
end;

procedure TUnEiia_PF.MostraFormInfoIncImp;
begin
  if DBCheck.CriaFm(TFmInfoIncImp, FmInfoIncImp, afmoNegarComAviso) then
  begin
    FmInfoIncImp.ShowModal;
    FmInfoIncImp.Destroy;
  end;
end;

end.
