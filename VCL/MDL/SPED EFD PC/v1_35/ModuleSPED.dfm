object DmSPED: TDmSPED
  Height = 480
  Width = 640
  PixelsPerInch = 96
  object QrAtrela0053: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_A_;'
      'CREATE TABLE _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_A_'
      'SELECT NFe_FatID, NFe_FatNum, DT_E_S DataFiscalInn'
      'FROM bluederm_colosso.efdinnnfscab'
      'WHERE NFe_FatID=53'
      'AND  NFe_FatNum<>0'
      'AND Empresa=-11'
      'AND DT_E_S BETWEEN "2022-03-01" AND "2022-03-31";'
      ''
      'DROP TABLE IF EXISTS _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_B_;'
      'CREATE TABLE _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_B_'
      
        'SELECT FatID, FatNum, Empresa, ide_dEmi, DataFiscal DataFiscalNu' +
        'l'
      'FROM bluederm_colosso.nfecaba'
      'WHERE FatID=53'
      'AND  FatNum<>0'
      'AND Empresa=-11'
      'AND DataFiscal < "1900-01-01";'
      ''
      'SELECT b.*, a.*'
      'FROM _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_B_ b'
      'LEFT JOIN _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_A_ a '
      '  ON b.FatNum=a.NFe_FatNum'
      'WHERE NOT (a.NFe_FatNum) IS NULL'
      ';')
    Left = 184
    Top = 1
    object QrAtrela0053NFe_FatNum: TIntegerField
      FieldName = 'NFe_FatNum'
    end
    object QrAtrela0053FatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrAtrela0053FatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrAtrela0053Empresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrAtrela0053ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrAtrela0053NFe_FatID: TIntegerField
      FieldName = 'NFe_FatID'
    end
    object QrAtrela0053DataFiscalNul: TDateField
      FieldName = 'DataFiscalNul'
      Required = True
    end
    object QrAtrela0053DataFiscalInn: TDateField
      FieldName = 'DataFiscalInn'
    end
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pem.SPED_EFD_IND_PERFIL, pem.SPED_EFD_IND_ATIV,'
      'pem.SPED_EFD_CadContador, pem.SPED_EFD_CRCContador,'
      'pem.SPED_EFD_EscriContab, pem.SPED_EFD_EnderContab,'
      
        'SPED_EFD_Path, SPED_EFD_DtaFiscalSaida, SPED_EFD_DtaFiscalEntrad' +
        'a,'
      'SPED_EFD_ID_0150, SPED_EFD_ID_0200,'
      'pem.SPED_EFD_IND_NAT_PJ,'
      'IF(ctd.Tipo=0, ctd.RazaoSocial, ctd.Nome) NO_CTD,'
      'ctd.CPF CPF_CTD, ctb.CNPJ CNPJ_CTB,'
      'IF(ctb.Tipo=0, ctb.RazaoSocial, ctb.Nome) NO_CTB'
      'FROM paramsemp pem'
      'LEFT JOIN entidades ctd ON ctd.Codigo=pem.SPED_EFD_CadContador'
      'LEFT JOIN entidades ctb ON ctb.Codigo=pem.SPED_EFD_EscriContab'
      '')
    Left = 64
    object QrParamsEmpSPED_EFD_IND_PERFIL: TWideStringField
      FieldName = 'SPED_EFD_IND_PERFIL'
      Size = 1
    end
    object QrParamsEmpSPED_EFD_IND_ATIV: TSmallintField
      FieldName = 'SPED_EFD_IND_ATIV'
    end
    object QrParamsEmpSPED_EFD_IND_NAT_PJ: TWideStringField
      FieldName = 'SPED_EFD_IND_NAT_PJ'
      Size = 2
    end
    object QrParamsEmpSPED_EFD_CadContador: TIntegerField
      FieldName = 'SPED_EFD_CadContador'
    end
    object QrParamsEmpSPED_EFD_CRCContador: TWideStringField
      FieldName = 'SPED_EFD_CRCContador'
      Size = 15
    end
    object QrParamsEmpSPED_EFD_EscriContab: TIntegerField
      FieldName = 'SPED_EFD_EscriContab'
    end
    object QrParamsEmpSPED_EFD_EnderContab: TSmallintField
      FieldName = 'SPED_EFD_EnderContab'
    end
    object QrParamsEmpSPED_EFD_Path: TWideStringField
      FieldName = 'SPED_EFD_Path'
      Size = 255
    end
    object QrParamsEmpNO_CTD: TWideStringField
      FieldName = 'NO_CTD'
      Size = 100
    end
    object QrParamsEmpNO_CTB: TWideStringField
      FieldName = 'NO_CTB'
      Size = 100
    end
    object QrParamsEmpCPF_CTD: TWideStringField
      FieldName = 'CPF_CTD'
      Size = 18
    end
    object QrParamsEmpCNPJ_CTB: TWideStringField
      FieldName = 'CNPJ_CTB'
      Size = 18
    end
    object QrParamsEmpSPED_EFD_ID_0200: TSmallintField
      FieldName = 'SPED_EFD_ID_0200'
    end
    object QrParamsEmpSPED_EFD_ID_0150: TSmallintField
      FieldName = 'SPED_EFD_ID_0150'
    end
    object QrParamsEmpSPED_EFD_DtaFiscalSaida: TSmallintField
      FieldName = 'SPED_EFD_DtaFiscalSaida'
    end
    object QrParamsEmpSPED_EFD_DtaFiscalEntrada: TSmallintField
      FieldName = 'SPED_EFD_DtaFiscalEntrada'
      Required = True
    end
  end
  object QrCorrige002: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodInfoEmit, CodInfoDest,'
      'ide_tpNF, DataFiscal,'
      'FatID, FatNum, Empresa, IDCtrl, ide_mod, '
      'ide_serie, ide_nNF, ide_dEmi, ide_dSaiEnt, '
      'ide_finNFe, emit_CNPJ, emit_CPF, Status, '
      'NFG_Serie, NFG_SubSerie, COD_MOD'
      'FROM nfecaba nfea '
      'WHERE ide_tpAmb<>2'
      'AND COD_SIT in (98,99)'
      'AND Empresa=:P0'
      'AND DataFiscal BETWEEN :P1 AND :P2')
    Left = 408
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCorrige002FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCorrige002FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCorrige002Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCorrige002IDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCorrige002ide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrCorrige002ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrCorrige002ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrCorrige002ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrCorrige002ide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrCorrige002ide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrCorrige002emit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrCorrige002emit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrCorrige002Status: TIntegerField
      FieldName = 'Status'
    end
    object QrCorrige002NFG_Serie: TWideStringField
      FieldName = 'NFG_Serie'
      Size = 3
    end
    object QrCorrige002NFG_SubSerie: TWideStringField
      FieldName = 'NFG_SubSerie'
      Size = 3
    end
    object QrCorrige002COD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Size = 2
    end
    object QrCorrige002CodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrCorrige002ide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrCorrige002DataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrCorrige002CodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
  end
  object QrForca: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfea.DataFiscal, nfea.CriAForca, '
      'nfei.FatID, nfei.FatNum, nfei.Empresa, '
      'nfei.nItem, nfei.GraGruX GGX_NfeI, '
      'nfei.prod_uTrib, smia.GraGruX GGX_SMIA'
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.EMpresa'
      'LEFT JOIN stqmovitsa smia ON smia.Tipo=nfei.FatID'
      '     AND smia.OriCodi=nfei.FatNum'
      '     AND smia.Empresa=nfei.EMpresa'
      '     AND smia.OriCtrl=nfei.MeuID'
      'WHERE nfea.CriAForca = 1'
      'AND smia.GraGruX <> 0'
      'AND (smia.GraGruX <> nfei.GraGruX'
      '     OR nfei.prod_uTrib = '#39#39
      '     )')
    Left = 324
    object QrForcaDataFiscal: TDateField
      FieldName = 'DataFiscal'
    end
    object QrForcaCriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
    object QrForcaFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrForcaFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrForcaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrForcanItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrForcaGGX_NfeI: TIntegerField
      FieldName = 'GGX_NfeI'
    end
    object QrForcaprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrForcaGGX_SMIA: TIntegerField
      FieldName = 'GGX_SMIA'
    end
  end
  object QrGGX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.UnidMed,'
      'unm.Sigla'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 324
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGGXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGGXSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
  end
  object QrCorrige001: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa, '
      'nfei.nItem, nfei.prod_cProd '
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.Empresa'
      'WHERE nfea.Empresa=nfea.CodInfoEmit'
      'AND GraGruX=0'
      'AND prod_cProd <> '#39#39)
    Left = 408
    object QrCorrige001FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCorrige001FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCorrige001Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCorrige001nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrCorrige001prod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
  end
  object QrCorrige1x3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa, '
      'nfei.nItem, nfei.Nivel1'
      'FROM nfeitsi nfei'
      'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID'
      '     AND nfea.FatNum=nfei.FatNum'
      '     AND nfea.Empresa=nfei.Empresa'
      'WHERE nfei.FatID IN (103,113)'
      'AND GraGruX=0'
      'AND Nivel1 < 0')
    Left = 560
    Top = 420
    object QrCorrige1x3FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCorrige1x3FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCorrige1x3Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCorrige1x3nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrCorrige1x3Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
  end
  object QrAux: TMySQLQuery
    Left = 64
    Top = 52
  end
end
