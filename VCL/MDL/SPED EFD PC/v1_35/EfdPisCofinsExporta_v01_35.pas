unit EfdPisCofinsExporta_v01_35;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnSPED_Geral, Variants,
  UnDmkProcFunc, DmkDAC_PF, UnDmkEnums, UnGrade_Jan, dmkImage, UnAppPF,
  UnEfdPisCofins_PF_v01_35, UnGrl_Consts,
  SPED_Listas, Vcl.Menus, frxClass, frxDBSet, UnSPED_PF, SPED_EFD_PC_Tabs;

type
  //
  TFmEfdPisCofinsExporta_v01_35 = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdMes: TdmkEdit;
    LaMes: TLabel;
    QrEmpresa: TmySQLQuery;
    QrEmpresaTipo: TSmallintField;
    QrEmpresaENumero: TIntegerField;
    QrEmpresaPNumero: TIntegerField;
    QrEmpresaELograd: TSmallintField;
    QrEmpresaPLograd: TSmallintField;
    QrEmpresaECEP: TIntegerField;
    QrEmpresaPCEP: TIntegerField;
    QrEmpresaNOME_ENT: TWideStringField;
    QrEmpresaCodigo: TIntegerField;
    QrEmpresaCNPJ_CPF: TWideStringField;
    QrVersao: TMySQLQuery;
    QrCampos: TmySQLQuery;
    QrCamposBloco: TWideStringField;
    QrCamposRegistro: TWideStringField;
    QrCamposNumero: TIntegerField;
    QrCamposVersaoIni: TIntegerField;
    QrCamposVersaoFim: TIntegerField;
    QrCamposCampo: TWideStringField;
    QrCamposTipo: TWideStringField;
    QrCamposTam: TSmallintField;
    QrCamposTObrig: TSmallintField;
    QrCamposDecimais: TSmallintField;
    QrCamposEObrig: TWideStringField;
    QrCamposDescrLin1: TWideStringField;
    QrCamposDescrLin2: TWideStringField;
    QrCamposDescrLin3: TWideStringField;
    QrCamposDescrLin4: TWideStringField;
    QrCamposDescrLin5: TWideStringField;
    QrCamposDescrLin6: TWideStringField;
    QrCamposDescrLin7: TWideStringField;
    QrCamposDescrLin8: TWideStringField;
    RGTIPO_ESCRIT: TRadioGroup;
    QrEmpresaNOMEUF: TWideStringField;
    QrEmpresaIE: TWideStringField;
    QrEmpresaECodMunici: TIntegerField;
    QrEmpresaPCodMunici: TIntegerField;
    QrEmpresaNIRE: TWideStringField;
    QrEmpresaSUFRAMA: TWideStringField;
    QrEmpresaNO_2_ENT: TWideStringField;
    QrEmpresaNOMELOGRAD: TWideStringField;
    QrEmpresaRUA: TWideStringField;
    QrEmpresaCOMPL: TWideStringField;
    QrEmpresaBAIRRO: TWideStringField;
    QrEmpresaTE1: TWideStringField;
    QrEmpresaFAX: TWideStringField;
    QrEmpresaEMAIL: TWideStringField;
    QrEnder: TmySQLQuery;
    QrEnderTipo: TSmallintField;
    QrEnderENumero: TIntegerField;
    QrEnderPNumero: TIntegerField;
    QrEnderELograd: TSmallintField;
    QrEnderPLograd: TSmallintField;
    QrEnderECEP: TIntegerField;
    QrEnderPCEP: TIntegerField;
    QrEnderIE: TWideStringField;
    QrEnderECodMunici: TIntegerField;
    QrEnderPCodMunici: TIntegerField;
    QrEnderNIRE: TWideStringField;
    QrEnderSUFRAMA: TWideStringField;
    QrEnderNOME_ENT: TWideStringField;
    QrEnderCNPJ_CPF: TWideStringField;
    QrEnderNOMEUF: TWideStringField;
    QrEnderNO_2_ENT: TWideStringField;
    QrEnderNOMELOGRAD: TWideStringField;
    QrEnderRUA: TWideStringField;
    QrEnderCOMPL: TWideStringField;
    QrEnderBAIRRO: TWideStringField;
    QrEnderTE1: TWideStringField;
    QrEnderFAX: TWideStringField;
    QrEnderEMAIL: TWideStringField;
    QrEnderCodigo: TIntegerField;
    QrSelEnt: TmySQLQuery;
    QrEnderECodiPais: TIntegerField;
    QrEnderPCodiPais: TIntegerField;
    QrUniMedi: TmySQLQuery;
    CkCorrigeCriadosAForca: TCheckBox;
    QrReduzidos: TmySQLQuery;
    QrSPEDEFDPisCofinsErrs: TMySQLQuery;
    QrProduto2: TmySQLQuery;
    QrProduto2Controle: TIntegerField;
    QrProduto2NO_PRD_TAM_COR: TWideStringField;
    QrProduto2cGTIN_EAN: TWideStringField;
    QrProduto2NCM: TWideStringField;
    QrProduto2EX_TIPI: TWideStringField;
    QrProduto2SIGLA: TWideStringField;
    QrProduto2Tipo_Item: TSmallintField;
    QrReduzidosGraGruX: TIntegerField;
    QrProduto2COD_LST: TWideStringField;
    QrProduto2SPEDEFD_ALIQ_ICMS: TFloatField;
    QrCabA_C: TmySQLQuery;
    QrCabA_CFatID: TIntegerField;
    QrCabA_CFatNum: TIntegerField;
    QrCabA_CEmpresa: TIntegerField;
    QrCabA_CIDCtrl: TIntegerField;
    QrCabA_CId: TWideStringField;
    QrCabA_Cide_indPag: TSmallintField;
    QrCabA_Cide_mod: TSmallintField;
    QrCabA_Cide_serie: TIntegerField;
    QrCabA_Cide_nNF: TIntegerField;
    QrCabA_Cide_dEmi: TDateField;
    QrCabA_Cide_dSaiEnt: TDateField;
    QrCabA_Cide_tpNF: TSmallintField;
    QrCabA_Cide_tpAmb: TSmallintField;
    QrCabA_Cide_finNFe: TSmallintField;
    QrCabA_Cemit_CNPJ: TWideStringField;
    QrCabA_Cemit_CPF: TWideStringField;
    QrCabA_Cdest_CNPJ: TWideStringField;
    QrCabA_Cdest_CPF: TWideStringField;
    QrCabA_CICMSTot_vBC: TFloatField;
    QrCabA_CICMSTot_vICMS: TFloatField;
    QrCabA_CICMSTot_vBCST: TFloatField;
    QrCabA_CICMSTot_vST: TFloatField;
    QrCabA_CICMSTot_vProd: TFloatField;
    QrCabA_CICMSTot_vFrete: TFloatField;
    QrCabA_CICMSTot_vSeg: TFloatField;
    QrCabA_CICMSTot_vDesc: TFloatField;
    QrCabA_CICMSTot_vII: TFloatField;
    QrCabA_CICMSTot_vIPI: TFloatField;
    QrCabA_CICMSTot_vPIS: TFloatField;
    QrCabA_CICMSTot_vCOFINS: TFloatField;
    QrCabA_CICMSTot_vOutro: TFloatField;
    QrCabA_CICMSTot_vNF: TFloatField;
    QrCabA_CISSQNtot_vServ: TFloatField;
    QrCabA_CISSQNtot_vBC: TFloatField;
    QrCabA_CISSQNtot_vISS: TFloatField;
    QrCabA_CISSQNtot_vPIS: TFloatField;
    QrCabA_CISSQNtot_vCOFINS: TFloatField;
    QrCabA_CRetTrib_vRetPIS: TFloatField;
    QrCabA_CRetTrib_vRetCOFINS: TFloatField;
    QrCabA_CRetTrib_vRetCSLL: TFloatField;
    QrCabA_CRetTrib_vBCIRRF: TFloatField;
    QrCabA_CRetTrib_vIRRF: TFloatField;
    QrCabA_CRetTrib_vBCRetPrev: TFloatField;
    QrCabA_CRetTrib_vRetPrev: TFloatField;
    QrCabA_CModFrete: TSmallintField;
    QrCabA_CCobr_Fat_nFat: TWideStringField;
    QrCabA_CCobr_Fat_vOrig: TFloatField;
    QrCabA_CCobr_Fat_vDesc: TFloatField;
    QrCabA_CCobr_Fat_vLiq: TFloatField;
    QrCabA_CInfAdic_InfAdFisco: TWideMemoField;
    QrCabA_CInfAdic_InfCpl: TWideMemoField;
    QrCabA_CExporta_UFEmbarq: TWideStringField;
    QrCabA_CExporta_XLocEmbarq: TWideStringField;
    QrCabA_CCompra_XNEmp: TWideStringField;
    QrCabA_CCompra_XPed: TWideStringField;
    QrCabA_CCompra_XCont: TWideStringField;
    QrCabA_CStatus: TIntegerField;
    QrCabA_CinfProt_Id: TWideStringField;
    QrCabA_CinfProt_cStat: TIntegerField;
    QrCabA_CinfCanc_cStat: TIntegerField;
    QrCabA_CDataFiscal: TDateField;
    QrCabA_CSINTEGRA_ExpDeclNum: TWideStringField;
    QrCabA_CSINTEGRA_ExpDeclDta: TDateField;
    QrCabA_CSINTEGRA_ExpNat: TWideStringField;
    QrCabA_CSINTEGRA_ExpRegNum: TWideStringField;
    QrCabA_CSINTEGRA_ExpRegDta: TDateField;
    QrCabA_CSINTEGRA_ExpConhNum: TWideStringField;
    QrCabA_CSINTEGRA_ExpConhDta: TDateField;
    QrCabA_CSINTEGRA_ExpConhTip: TWideStringField;
    QrCabA_CSINTEGRA_ExpPais: TWideStringField;
    QrCabA_CSINTEGRA_ExpAverDta: TDateField;
    QrCabA_CCodInfoEmit: TIntegerField;
    QrCabA_CCodInfoDest: TIntegerField;
    QrCabA_CCriAForca: TSmallintField;
    QrCabA_Cide_hSaiEnt: TTimeField;
    QrCabA_Cide_dhCont: TDateTimeField;
    QrCabA_Cemit_CRT: TSmallintField;
    QrCabA_Cdest_email: TWideStringField;
    QrCabA_CVagao: TWideStringField;
    QrCabA_CBalsa: TWideStringField;
    QrCabA_CCodInfoTrsp: TIntegerField;
    QrCabA_COrdemServ: TIntegerField;
    QrCabA_CSituacao: TSmallintField;
    QrCabA_CAntigo: TWideStringField;
    QrCabA_CNFG_Serie: TWideStringField;
    QrCabA_CNF_ICMSAlq: TFloatField;
    QrCabA_CNF_CFOP: TWideStringField;
    QrCabA_CImportado: TSmallintField;
    QrCabA_CNFG_SubSerie: TWideStringField;
    QrCabA_CNFG_ValIsen: TFloatField;
    QrCabA_CNFG_NaoTrib: TFloatField;
    QrCabA_CNFG_Outros: TFloatField;
    QrCabA_CCOD_MOD: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrCabA_CCOD_SIT: TSmallintField;
    QrCabA_CVL_ABAT_NT: TFloatField;
    QrCamposSObrig: TWideStringField;
    QrCamposCObrig: TWideStringField;
    QrItsI_: TMySQLQuery;
    QrItsI_nItem: TIntegerField;
    QrItsI_prod_CFOP: TIntegerField;
    QrItsI_GraGruX: TIntegerField;
    QrItsI_prod_xProd: TWideStringField;
    QrItsN: TmySQLQuery;
    QrItsNnItem: TIntegerField;
    QrItsNICMS_Orig: TSmallintField;
    QrItsNICMS_CST: TSmallintField;
    QrItsNICMS_vBC: TFloatField;
    QrItsNICMS_pICMS: TFloatField;
    QrItsNICMS_vICMS: TFloatField;
    QrItsNICMS_vBCST: TFloatField;
    QrItsNICMS_vICMSST: TFloatField;
    QrItsI_FatID: TIntegerField;
    QrItsI_FatNum: TIntegerField;
    QrItsI_Empresa: TIntegerField;
    QrItsI_prod_vProd: TFloatField;
    QrItsI_prod_vDesc: TFloatField;
    QrItsI_prod_NCM: TWideStringField;
    QrStqMovIts: TmySQLQuery;
    QrItsI_MeuID: TIntegerField;
    QrStqMovItsBaixa: TSmallintField;
    QrItsO: TmySQLQuery;
    QrItsOIND_APUR: TWideStringField;
    QrItsNCOD_NAT: TWideStringField;
    QrItsNICMS_pICMSST: TFloatField;
    QrItsOIPI_CST: TSmallintField;
    QrItsOIPI_vBC: TFloatField;
    QrItsOIPI_qUnid: TFloatField;
    QrItsOIPI_vUnid: TFloatField;
    QrItsOIPI_pIPI: TFloatField;
    QrItsOIPI_vIPI: TFloatField;
    QrItsOIPI_cEnq: TWideStringField;
    QrItsQ: TmySQLQuery;
    QrItsQPIS_CST: TSmallintField;
    QrItsQPIS_vBC: TFloatField;
    QrItsQPIS_pPIS: TFloatField;
    QrItsQPIS_vPIS: TFloatField;
    QrItsQPIS_qBCProd: TFloatField;
    QrItsQPIS_vAliqProd: TFloatField;
    QrItsS: TmySQLQuery;
    QrItsSCOFINS_CST: TSmallintField;
    QrItsSCOFINS_vBC: TFloatField;
    QrItsSCOFINS_pCOFINS: TFloatField;
    QrItsSCOFINS_qBCProd: TFloatField;
    QrItsSCOFINS_vAliqProd: TFloatField;
    QrItsSCOFINS_vCOFINS: TFloatField;
    QrGruI: TmySQLQuery;
    QrItsI_prod_uTrib: TWideStringField;
    QrItsI_prod_qTrib: TFloatField;
    QrCorrI: TmySQLQuery;
    QrCorrIFatID: TIntegerField;
    QrCorrIFatNum: TIntegerField;
    QrCorrIEmpresa: TIntegerField;
    QrCorrInItem: TIntegerField;
    QrCorrIGraGruX: TIntegerField;
    QrErrCFOP: TmySQLQuery;
    QrErrCFOPFatID: TIntegerField;
    QrErrCFOPFatNum: TIntegerField;
    QrErrCFOPEmpresa: TIntegerField;
    QrErrCFOPnItem: TIntegerField;
    QrErrCFOPprod_CFOP: TIntegerField;
    QrErrCFOPCodTxt: TWideStringField;
    QrErrCFOPCodigo: TIntegerField;
    QrErrCFOPNome: TWideStringField;
    QrErrCFOPDataIni: TDateField;
    QrErrCFOPDataFim: TDateField;
    QrCFOP: TmySQLQuery;
    QrAllI: TmySQLQuery;
    QrGruInCST: TSmallintField;
    QrGruInCFOP: TIntegerField;
    QrGruIValor: TFloatField;
    QrGruIprod_vProd: TFloatField;
    QrGruIIPI_vIPI: TFloatField;
    QrAllIValor: TFloatField;
    QrAllIprod_vProd: TFloatField;
    QrAllIIPI_vIPI: TFloatField;
    TabSheet3: TTabSheet;
    MeErros: TMemo;
    MeGerado: TMemo;
    Panel4: TPanel;
    QrBlocos: TmySQLQuery;
    QrBlocosBloco: TWideStringField;
    QrBlocosOrdem: TIntegerField;
    QrBlocosAtivo: TSmallintField;
    TVBlocos: TTreeView;
    QrErrMod: TmySQLQuery;
    QrErrModDataFiscal: TDateField;
    QrErrModFatID: TIntegerField;
    QrErrModFatNum: TIntegerField;
    QrErrModEmpresa: TIntegerField;
    QrErrModide_mod: TSmallintField;
    QrErrModide_serie: TIntegerField;
    QrErrModide_nNF: TIntegerField;
    QrErrModide_dEmi: TDateField;
    QrErrModICMSTot_vNF: TFloatField;
    QrErrModTerceiro: TLargeintField;
    QrErrModNO_TERCEIRO: TWideStringField;
    TabSheet4: TTabSheet;
    DsErrMod: TDataSource;
    DBGrid1: TDBGrid;
    Label4: TLabel;
    QrErrModIDCtrl: TIntegerField;
    QrSelEntCodigo: TIntegerField;
    QrBlcs: TmySQLQuery;
    QrBlcsBloco: TWideStringField;
    QrBlcsRegistro: TWideStringField;
    QrBlcsNivel: TSmallintField;
    QrBlcsRegisPai: TWideStringField;
    QrBlcsOcorAciNiv: TIntegerField;
    QrBlcsOcorEstNiv: TIntegerField;
    QrBlcsImplementd: TSmallintField;
    QrBlcsOrdem: TIntegerField;
    QrBlcsAtivo: TSmallintField;
    QrNiv2: TmySQLQuery;
    QrNiv2Registro: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel47: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    GBConfirma: TGroupBox;
    Panel16: TPanel;
    StatusBar: TStatusBar;
    PainelConfirma: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    BtErros: TBitBtn;
    RGPreenche: TRadioGroup;
    QrGruIICMS_vBC: TFloatField;
    QrGruIICMS_vICMS: TFloatField;
    QrGruIICMS_vBCST: TFloatField;
    QrGruIICMS_vICMSST: TFloatField;
    QrAllIICMS_vBC: TFloatField;
    QrAllIICMS_vICMS: TFloatField;
    QrAllIICMS_vBCST: TFloatField;
    QrAllIICMS_vICMSST: TFloatField;
    CkRecriaRegAotmaticos: TCheckBox;
    QrItsNICMS_vICMSOp: TFloatField;
    QrItsNICMS_pDif: TFloatField;
    QrItsNICMS_vICMSDif: TFloatField;
    QrItsNICMS_vICMSDeson: TFloatField;
    QrItsNICMS_modBC: TSmallintField;
    QrItsNICMS_pRedBC: TFloatField;
    QrItsNICMS_modBCST: TSmallintField;
    QrItsNICMS_pMVAST: TFloatField;
    QrItsNICMS_pRedBCST: TFloatField;
    QrItsNICMS_CSOSN: TIntegerField;
    QrItsNICMS_UFST: TWideStringField;
    QrItsNICMS_pBCOp: TFloatField;
    QrItsNICMS_vBCSTRet: TFloatField;
    QrItsNICMS_vICMSSTRet: TFloatField;
    QrItsNICMS_motDesICMS: TSmallintField;
    QrItsNICMS_pCredSN: TFloatField;
    QrItsNICMS_vCredICMSSN: TFloatField;
    QrGruIpICMS: TFloatField;
    QrVersaoCodTxt: TWideStringField;
    QrProduto1: TmySQLQuery;
    QrProduto1Controle: TIntegerField;
    QrProduto1NO_PRD_TAM_COR: TWideStringField;
    QrProduto1cGTIN_EAN: TWideStringField;
    QrProduto1NCM: TWideStringField;
    QrProduto1EX_TIPI: TWideStringField;
    QrProduto1COD_LST: TWideStringField;
    QrProduto1SIGLA: TWideStringField;
    QrProduto1Tipo_Item: TSmallintField;
    QrProduto1SPEDEFD_ALIQ_ICMS: TFloatField;
    QrProduto2prod_CEST: TIntegerField;
    QrGraGru1Cons: TmySQLQuery;
    QrProduto2GraGru1: TIntegerField;
    QrGraGru1ConsQtd_Comp: TFloatField;
    QrGraGru1ConsPerda: TFloatField;
    QrPsq: TmySQLQuery;
    QrGraGru1ConsGGX_CONSU: TIntegerField;
    QrEnt: TmySQLQuery;
    QrNFeEFD_C170: TmySQLQuery;
    QrNFeEFD_C170FatID: TIntegerField;
    QrNFeEFD_C170FatNum: TIntegerField;
    QrNFeEFD_C170Empresa: TIntegerField;
    QrNFeEFD_C170nItem: TIntegerField;
    QrNFeEFD_C170GraGruX: TIntegerField;
    QrNFeEFD_C170COD_ITEM: TWideStringField;
    QrNFeEFD_C170DESCR_COMPL: TWideStringField;
    QrNFeEFD_C170QTD: TFloatField;
    QrNFeEFD_C170UNID: TWideStringField;
    QrNFeEFD_C170VL_ITEM: TFloatField;
    QrNFeEFD_C170VL_DESC: TFloatField;
    QrNFeEFD_C170IND_MOV: TWideStringField;
    QrNFeEFD_C170CST_ICMS: TIntegerField;
    QrNFeEFD_C170CFOP: TIntegerField;
    QrNFeEFD_C170COD_NAT: TWideStringField;
    QrNFeEFD_C170VL_BC_ICMS: TFloatField;
    QrNFeEFD_C170ALIQ_ICMS: TFloatField;
    QrNFeEFD_C170VL_ICMS: TFloatField;
    QrNFeEFD_C170VL_BC_ICMS_ST: TFloatField;
    QrNFeEFD_C170ALIQ_ST: TFloatField;
    QrNFeEFD_C170VL_ICMS_ST: TFloatField;
    QrNFeEFD_C170IND_APUR: TWideStringField;
    QrNFeEFD_C170CST_IPI: TWideStringField;
    QrNFeEFD_C170COD_ENQ: TWideStringField;
    QrNFeEFD_C170VL_BC_IPI: TFloatField;
    QrNFeEFD_C170ALIQ_IPI: TFloatField;
    QrNFeEFD_C170VL_IPI: TFloatField;
    QrNFeEFD_C170CST_PIS: TSmallintField;
    QrNFeEFD_C170VL_BC_PIS: TFloatField;
    QrNFeEFD_C170ALIQ_PIS_p: TFloatField;
    QrNFeEFD_C170QUANT_BC_PIS: TFloatField;
    QrNFeEFD_C170ALIQ_PIS_r: TFloatField;
    QrNFeEFD_C170VL_PIS: TFloatField;
    QrNFeEFD_C170CST_COFINS: TSmallintField;
    QrNFeEFD_C170VL_BC_COFINS: TFloatField;
    QrNFeEFD_C170ALIQ_COFINS_p: TFloatField;
    QrNFeEFD_C170QUANT_BC_COFINS: TFloatField;
    QrNFeEFD_C170ALIQ_COFINS_r: TFloatField;
    QrNFeEFD_C170VL_COFINS: TFloatField;
    QrNFeEFD_C170COD_CTA: TWideStringField;
    QrNFeEFD_C170Lk: TIntegerField;
    QrNFeEFD_C170DataCad: TDateField;
    QrNFeEFD_C170DataAlt: TDateField;
    QrNFeEFD_C170UserCad: TIntegerField;
    QrNFeEFD_C170UserAlt: TIntegerField;
    QrNFeEFD_C170AlterWeb: TSmallintField;
    QrNFeEFD_C170Ativo: TSmallintField;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedNome: TWideStringField;
    QrUnidMedSigla: TWideStringField;
    QrUniMediNoUnidMed: TWideStringField;
    Panel5: TPanel;
    SbTeste_minimo_EFD_PIS_COFINS: TSpeedButton;
    BtMes: TSpeedButton;
    BtEstoque: TBitBtn;
    frxSEII_Estq: TfrxReport;
    QrSEII_0190: TmySQLQuery;
    QrSEII_0190REG: TWideStringField;
    QrSEII_0190UNID: TWideStringField;
    QrSEII_0190DESCR: TWideStringField;
    QrSEII_0190Ativo: TSmallintField;
    frxDsSEII_0190: TfrxDBDataset;
    QrSEII_0200: TmySQLQuery;
    QrSEII_0200REG: TWideStringField;
    QrSEII_0200COD_ITEM: TWideStringField;
    QrSEII_0200DESCR_ITEM: TWideStringField;
    QrSEII_0200COD_BARRA: TWideStringField;
    QrSEII_0200COD_ANT_ITEM: TWideStringField;
    QrSEII_0200UNID_INV: TWideStringField;
    QrSEII_0200TIPO_ITEM: TSmallintField;
    QrSEII_0200COD_NCM: TWideStringField;
    QrSEII_0200EX_IPI: TWideStringField;
    QrSEII_0200COD_GEN: TSmallintField;
    QrSEII_0200COD_LST: TWideStringField;
    QrSEII_0200ALIQ_ICMS: TFloatField;
    QrSEII_0200CEST: TIntegerField;
    QrSEII_0200Ativo: TSmallintField;
    QrSEII_0200GraGruX: TIntegerField;
    frxDsSEII_0200: TfrxDBDataset;
    QrEfdInnNFsCab: TMySQLQuery;
    QrEfdInnNFsCabMovFatID: TIntegerField;
    QrEfdInnNFsCabMovFatNum: TIntegerField;
    QrEfdInnNFsCabMovimCod: TIntegerField;
    QrEfdInnNFsCabEmpresa: TIntegerField;
    QrEfdInnNFsCabControle: TIntegerField;
    QrEfdInnNFsCabTerceiro: TIntegerField;
    QrEfdInnNFsCabPecas: TFloatField;
    QrEfdInnNFsCabPesoKg: TFloatField;
    QrEfdInnNFsCabAreaM2: TFloatField;
    QrEfdInnNFsCabAreaP2: TFloatField;
    QrEfdInnNFsCabValorT: TFloatField;
    QrEfdInnNFsCabMotorista: TIntegerField;
    QrEfdInnNFsCabPlaca: TWideStringField;
    QrEfdInnNFsCabCOD_MOD: TSmallintField;
    QrEfdInnNFsCabCOD_SIT: TSmallintField;
    QrEfdInnNFsCabSER: TIntegerField;
    QrEfdInnNFsCabNUM_DOC: TIntegerField;
    QrEfdInnNFsCabCHV_NFE: TWideStringField;
    QrEfdInnNFsCabNFeStatus: TIntegerField;
    QrEfdInnNFsCabDT_DOC: TDateField;
    QrEfdInnNFsCabDT_E_S: TDateField;
    QrEfdInnNFsCabVL_DOC: TFloatField;
    QrEfdInnNFsCabIND_PGTO: TWideStringField;
    QrEfdInnNFsCabVL_DESC: TFloatField;
    QrEfdInnNFsCabVL_ABAT_NT: TFloatField;
    QrEfdInnNFsCabVL_MERC: TFloatField;
    QrEfdInnNFsCabIND_FRT: TWideStringField;
    QrEfdInnNFsCabVL_FRT: TFloatField;
    QrEfdInnNFsCabVL_SEG: TFloatField;
    QrEfdInnNFsCabVL_OUT_DA: TFloatField;
    QrEfdInnNFsCabVL_BC_ICMS: TFloatField;
    QrEfdInnNFsCabVL_ICMS: TFloatField;
    QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField;
    QrEfdInnNFsCabVL_ICMS_ST: TFloatField;
    QrEfdInnNFsCabVL_IPI: TFloatField;
    QrEfdInnNFsCabVL_PIS: TFloatField;
    QrEfdInnNFsCabVL_COFINS: TFloatField;
    QrEfdInnNFsCabVL_PIS_ST: TFloatField;
    QrEfdInnNFsCabVL_COFINS_ST: TFloatField;
    QrEfdInnNFsCabNFe_FatID: TIntegerField;
    QrEfdInnNFsCabNFe_FatNum: TIntegerField;
    QrEfdInnNFsCabNFe_StaLnk: TSmallintField;
    QrEfdInnNFsCabVSVmcWrn: TSmallintField;
    QrEfdInnNFsCabVSVmcObs: TWideStringField;
    QrEfdInnNFsCabVSVmcSeq: TWideStringField;
    QrEfdInnNFsCabVSVmcSta: TSmallintField;
    QrEfdInnNFsCabIsLinked: TSmallintField;
    QrEfdInnNFsCabSqLinked: TIntegerField;
    QrEfdInnNFsCabCliInt: TIntegerField;
    QrCabA_CAtrelaFatID: TIntegerField;
    QrCabA_CAtrelaStaLnk: TSmallintField;
    QrEfdInnNFsIts: TMySQLQuery;
    QrEfdInnNFsItsMovFatID: TIntegerField;
    QrEfdInnNFsItsMovFatNum: TIntegerField;
    QrEfdInnNFsItsMovimCod: TIntegerField;
    QrEfdInnNFsItsEmpresa: TIntegerField;
    QrEfdInnNFsItsMovimNiv: TIntegerField;
    QrEfdInnNFsItsMovimTwn: TIntegerField;
    QrEfdInnNFsItsControle: TIntegerField;
    QrEfdInnNFsItsConta: TIntegerField;
    QrEfdInnNFsItsGraGru1: TIntegerField;
    QrEfdInnNFsItsGraGruX: TIntegerField;
    QrEfdInnNFsItsprod_vProd: TFloatField;
    QrEfdInnNFsItsprod_vFrete: TFloatField;
    QrEfdInnNFsItsprod_vSeg: TFloatField;
    QrEfdInnNFsItsprod_vOutro: TFloatField;
    QrEfdInnNFsItsQTD: TFloatField;
    QrEfdInnNFsItsUNID: TWideStringField;
    QrEfdInnNFsItsVL_ITEM: TFloatField;
    QrEfdInnNFsItsVL_DESC: TFloatField;
    QrEfdInnNFsItsIND_MOV: TWideStringField;
    QrEfdInnNFsItsCFOP: TIntegerField;
    QrEfdInnNFsItsCOD_NAT: TWideStringField;
    QrEfdInnNFsItsVL_BC_ICMS: TFloatField;
    QrEfdInnNFsItsALIQ_ICMS: TFloatField;
    QrEfdInnNFsItsVL_ICMS: TFloatField;
    QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField;
    QrEfdInnNFsItsALIQ_ST: TFloatField;
    QrEfdInnNFsItsVL_ICMS_ST: TFloatField;
    QrEfdInnNFsItsIND_APUR: TWideStringField;
    QrEfdInnNFsItsCOD_ENQ: TWideStringField;
    QrEfdInnNFsItsVL_BC_IPI: TFloatField;
    QrEfdInnNFsItsALIQ_IPI: TFloatField;
    QrEfdInnNFsItsVL_IPI: TFloatField;
    QrEfdInnNFsItsVL_BC_PIS: TFloatField;
    QrEfdInnNFsItsALIQ_PIS_p: TFloatField;
    QrEfdInnNFsItsQUANT_BC_PIS: TFloatField;
    QrEfdInnNFsItsALIQ_PIS_r: TFloatField;
    QrEfdInnNFsItsVL_PIS: TFloatField;
    QrEfdInnNFsItsVL_BC_COFINS: TFloatField;
    QrEfdInnNFsItsALIQ_COFINS_p: TFloatField;
    QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField;
    QrEfdInnNFsItsALIQ_COFINS_r: TFloatField;
    QrEfdInnNFsItsVL_COFINS: TFloatField;
    QrEfdInnNFsItsCOD_CTA: TWideStringField;
    QrEfdInnNFsItsVL_ABAT_NT: TFloatField;
    QrEfdInnNFsItsDtCorrApo: TDateTimeField;
    QrEfdInnNFsItsxLote: TWideStringField;
    QrEfdInnNFsItsOri_IPIpIPI: TFloatField;
    QrEfdInnNFsItsOri_IPIvIPI: TFloatField;
    QrEfdInnNFsItsIsLinked: TSmallintField;
    QrEfdInnNFsItsSqLinked: TIntegerField;
    QrEfdInnNFsItsSqLnkID: TIntegerField;
    QrItsI_prod_cProd: TWideStringField;
    QrItsI_prod_cEAN: TWideStringField;
    QrItsI_prod_cBarra: TWideStringField;
    QrItsI_prod_CEST: TIntegerField;
    QrItsI_prod_indEscala: TWideStringField;
    QrItsI_prod_CNPJFab: TWideStringField;
    QrItsI_prod_cBenef: TWideStringField;
    QrItsI_prod_EXTIPI: TWideStringField;
    QrItsI_prod_genero: TSmallintField;
    QrItsI_prod_uCom: TWideStringField;
    QrItsI_prod_qCom: TFloatField;
    QrItsI_prod_vUnCom: TFloatField;
    QrItsI_prod_cEANTrib: TWideStringField;
    QrItsI_prod_cBarraTrib: TWideStringField;
    QrItsI_prod_vUnTrib: TFloatField;
    QrItsI_prod_vFrete: TFloatField;
    QrItsI_prod_vSeg: TFloatField;
    QrItsI_prod_vOutro: TFloatField;
    QrItsI_prod_indTot: TSmallintField;
    QrItsI_prod_xPed: TWideStringField;
    QrItsI_prod_nItemPed: TIntegerField;
    QrItsI_Tem_IPI: TSmallintField;
    QrItsI__Ativo_: TSmallintField;
    QrItsI_InfAdCuztm: TIntegerField;
    QrItsI_EhServico: TIntegerField;
    QrItsI_UsaSubsTrib: TSmallintField;
    QrItsI_ICMSRec_pRedBC: TFloatField;
    QrItsI_ICMSRec_vBC: TFloatField;
    QrItsI_ICMSRec_pAliq: TFloatField;
    QrItsI_ICMSRec_vICMS: TFloatField;
    QrItsI_IPIRec_pRedBC: TFloatField;
    QrItsI_IPIRec_vBC: TFloatField;
    QrItsI_IPIRec_pAliq: TFloatField;
    QrItsI_IPIRec_vIPI: TFloatField;
    QrItsI_PISRec_pRedBC: TFloatField;
    QrItsI_PISRec_vBC: TFloatField;
    QrItsI_PISRec_pAliq: TFloatField;
    QrItsI_PISRec_vPIS: TFloatField;
    QrItsI_COFINSRec_pRedBC: TFloatField;
    QrItsI_COFINSRec_vBC: TFloatField;
    QrItsI_COFINSRec_pAliq: TFloatField;
    QrItsI_COFINSRec_vCOFINS: TFloatField;
    QrItsI_Nivel1: TIntegerField;
    QrItsI_UnidMedCom: TIntegerField;
    QrItsI_UnidMedTrib: TIntegerField;
    QrItsI_ICMSRec_vBCST: TFloatField;
    QrItsI_ICMSRec_vICMSST: TFloatField;
    QrItsI_ICMSRec_pAliqST: TFloatField;
    QrItsI_Tem_II: TSmallintField;
    QrItsI_prod_nFCI: TWideStringField;
    QrItsI_StqMovValA: TIntegerField;
    QrItsI_AtrelaID: TIntegerField;
    QrCabA_CFisRegCad: TIntegerField;
    QrIndMov: TMySQLQuery;
    QrIndMovIND_MOV: TFloatField;
    QrI: TMySQLQuery;
    QrIFatID: TIntegerField;
    QrIFatNum: TIntegerField;
    QrIEmpresa: TIntegerField;
    QrInItem: TIntegerField;
    QrIprod_cProd: TWideStringField;
    QrIprod_cEAN: TWideStringField;
    QrIprod_cBarra: TWideStringField;
    QrIprod_xProd: TWideStringField;
    QrIprod_NCM: TWideStringField;
    QrIprod_CEST: TIntegerField;
    QrIprod_indEscala: TWideStringField;
    QrIprod_CNPJFab: TWideStringField;
    QrIprod_cBenef: TWideStringField;
    QrIprod_EXTIPI: TWideStringField;
    QrIprod_genero: TSmallintField;
    QrIprod_CFOP: TIntegerField;
    QrIprod_uCom: TWideStringField;
    QrIprod_qCom: TFloatField;
    QrIprod_vUnCom: TFloatField;
    QrIprod_vProd: TFloatField;
    QrIprod_cEANTrib: TWideStringField;
    QrIprod_cBarraTrib: TWideStringField;
    QrIprod_uTrib: TWideStringField;
    QrIprod_qTrib: TFloatField;
    QrIprod_vUnTrib: TFloatField;
    QrIprod_vFrete: TFloatField;
    QrIprod_vSeg: TFloatField;
    QrIprod_vDesc: TFloatField;
    QrIprod_vOutro: TFloatField;
    QrIprod_indTot: TSmallintField;
    QrIprod_xPed: TWideStringField;
    QrIprod_nItemPed: TIntegerField;
    QrITem_IPI: TSmallintField;
    QrI_Ativo_: TSmallintField;
    QrIInfAdCuztm: TIntegerField;
    QrIEhServico: TIntegerField;
    QrIUsaSubsTrib: TSmallintField;
    QrIICMSRec_pRedBC: TFloatField;
    QrIICMSRec_vBC: TFloatField;
    QrIICMSRec_pAliq: TFloatField;
    QrIICMSRec_vICMS: TFloatField;
    QrIIPIRec_pRedBC: TFloatField;
    QrIIPIRec_vBC: TFloatField;
    QrIIPIRec_pAliq: TFloatField;
    QrIIPIRec_vIPI: TFloatField;
    QrIPISRec_pRedBC: TFloatField;
    QrIPISRec_vBC: TFloatField;
    QrIPISRec_pAliq: TFloatField;
    QrIPISRec_vPIS: TFloatField;
    QrICOFINSRec_pRedBC: TFloatField;
    QrICOFINSRec_vBC: TFloatField;
    QrICOFINSRec_pAliq: TFloatField;
    QrICOFINSRec_vCOFINS: TFloatField;
    QrIMeuID: TIntegerField;
    QrINivel1: TIntegerField;
    QrIGraGruX: TIntegerField;
    QrIUnidMedCom: TIntegerField;
    QrIUnidMedTrib: TIntegerField;
    QrIICMSRec_vBCST: TFloatField;
    QrIICMSRec_vICMSST: TFloatField;
    QrIICMSRec_pAliqST: TFloatField;
    QrITem_II: TSmallintField;
    QrIprod_nFCI: TWideStringField;
    QrIStqMovValA: TIntegerField;
    QrIAtrelaID: TIntegerField;
    QrIICMS_Orig: TSmallintField;
    QrIICMS_CST: TSmallintField;
    QrIICMS_vBC: TFloatField;
    QrIICMS_pICMS: TFloatField;
    QrIICMS_vICMS: TFloatField;
    QrIICMS_vBCST: TFloatField;
    QrIICMS_pICMSST: TFloatField;
    QrIICMS_vICMSST: TFloatField;
    QrIIPI_cEnq: TWideStringField;
    QrIIND_APUR: TWideStringField;
    QrIIPI_CST: TSmallintField;
    QrIIPI_vBC: TFloatField;
    QrIIPI_pIPI: TFloatField;
    QrIIPI_vIPI: TFloatField;
    QrIPIS_CST: TSmallintField;
    QrIPIS_vBC: TFloatField;
    QrIPIS_pPIS: TFloatField;
    QrIPIS_vPIS: TFloatField;
    QrIPISST_vBC: TFloatField;
    QrIPISST_pPIS: TFloatField;
    QrIPISST_vPIS: TFloatField;
    QrICOFINS_CST: TSmallintField;
    QrICOFINS_vBC: TFloatField;
    QrICOFINS_pCOFINS: TFloatField;
    QrICOFINS_vCOFINS: TFloatField;
    QrICOFINSST_vBC: TFloatField;
    QrICOFINSST_pCOFINS: TFloatField;
    QrICOFINSST_vCOFINS: TFloatField;
    QrCabA_CAtrelaFatNum: TIntegerField;
    QrAnal0053: TMySQLQuery;
    QrEfdInnCTsCab: TMySQLQuery;
    SbImplementado: TSpeedButton;
    QrEfdInnNFsItsCST_IPI: TWideStringField;
    QrEfdInnNFsItsCST_PIS: TWideStringField;
    QrEfdInnNFsItsCST_COFINS: TWideStringField;
    QrEfdInnNFsItsCST_ICMS: TWideStringField;
    QrAnal0053CST_ICMS: TWideStringField;
    QrAnal0053CFOP: TIntegerField;
    QrAnal0053ALIQ_ICMS: TFloatField;
    QrAnal0053VL_BC_ICMS: TFloatField;
    QrAnal0053VL_ICMS: TFloatField;
    QrAnal0053VL_BC_ICMS_ST: TFloatField;
    QrAnal0053VL_ICMS_ST: TFloatField;
    QrAnal0053VL_RED_BC: TFloatField;
    QrAnal0053VL_IPI: TFloatField;
    QrAnal0053VL_OPR: TFloatField;
    QrEfdInnNFsCabRegrFiscal: TIntegerField;
    QrNatOp: TMySQLQuery;
    QrNatOpFisRegCad: TIntegerField;
    QrNatOpide_natOp: TWideStringField;
    QrEFD_D100: TMySQLQuery;
    QrEFD_D100ImporExpor: TSmallintField;
    QrEFD_D100AnoMes: TIntegerField;
    QrEFD_D100Empresa: TIntegerField;
    QrEFD_D100LinArq: TIntegerField;
    QrEFD_D100REG: TWideStringField;
    QrEFD_D100IND_OPER: TWideStringField;
    QrEFD_D100IND_EMIT: TWideStringField;
    QrEFD_D100COD_PART: TWideStringField;
    QrEFD_D100COD_MOD: TWideStringField;
    QrEFD_D100COD_SIT: TWideStringField;
    QrEFD_D100SER: TWideStringField;
    QrEFD_D100SUB: TWideStringField;
    QrEFD_D100NUM_DOC: TIntegerField;
    QrEFD_D100CHV_CTE: TWideStringField;
    QrEFD_D100DT_DOC: TDateField;
    QrEFD_D100DT_A_P: TDateField;
    QrEFD_D100TP_CTE: TSmallintField;
    QrEFD_D100CHV_CTE_REF: TWideStringField;
    QrEFD_D100VL_DOC: TFloatField;
    QrEFD_D100VL_DESC: TFloatField;
    QrEFD_D100IND_FRT: TWideStringField;
    QrEFD_D100VL_SERV: TFloatField;
    QrEFD_D100VL_BC_ICMS: TFloatField;
    QrEFD_D100VL_ICMS: TFloatField;
    QrEFD_D100VL_NT: TFloatField;
    QrEFD_D100COD_INF: TWideStringField;
    QrEFD_D100COD_CTA: TWideStringField;
    QrEFD_D100Terceiro: TIntegerField;
    QrEFD_D100Importado: TSmallintField;
    QrEFD_D100Lk: TIntegerField;
    QrEFD_D100DataCad: TDateField;
    QrEFD_D100DataAlt: TDateField;
    QrEFD_D100UserCad: TIntegerField;
    QrEFD_D100UserAlt: TIntegerField;
    QrEFD_D100AlterWeb: TSmallintField;
    QrEFD_D100Ativo: TSmallintField;
    QrEFD_D100CST_ICMS: TIntegerField;
    QrEFD_D100CFOP: TIntegerField;
    QrEFD_D100ALIQ_ICMS: TFloatField;
    QrEFD_D100VL_RED_BC: TFloatField;
    TabSheet5: TTabSheet;
    Panel6: TPanel;
    EdIND_SIT_ESP: TdmkEdit;
    Label16: TLabel;
    EdIND_SIT_ESP_TXT: TdmkEdit;
    Label5: TLabel;
    EdNUM_REC_ANTERIOR: TdmkEdit;
    QrEmpresaFilial: TIntegerField;
    QrCabA_CInfoEFD_PisCofins_Entrada: TSmallintField;
    QrCabA_CInfoEFD_PisCofins_Saida: TSmallintField;
    QrCtas0500: TMySQLQuery;
    QrCtas0500Codigo: TIntegerField;
    QrCtas0500DtaSPED: TDateField;
    QrCtas0500Nome: TWideStringField;
    QrCtas0500SPED_COD_CTA_REF: TWideStringField;
    QrCtas0500Ordens: TWideStringField;
    QrCtas0500FinContab: TSmallintField;
    QrEfdInnCTsCabIND_OPER: TWideStringField;
    QrEfdInnCTsCabIND_EMIT: TWideStringField;
    QrEfdInnCTsCabCOD_MOD: TWideStringField;
    QrEfdInnCTsCabCOD_SIT: TSmallintField;
    QrEfdInnCTsCabSER: TWideStringField;
    QrEfdInnCTsCabSUB: TWideStringField;
    QrEfdInnCTsCabNUM_DOC: TIntegerField;
    QrEfdInnCTsCabCHV_CTE: TWideStringField;
    QrEfdInnCTsCabCTeStatus: TIntegerField;
    QrEfdInnCTsCabDT_DOC: TDateField;
    QrEfdInnCTsCabDT_A_P: TDateField;
    QrEfdInnCTsCabTP_CT_e: TSmallintField;
    QrEfdInnCTsCabCHV_CTE_REF: TWideStringField;
    QrEfdInnCTsCabVL_DOC: TFloatField;
    QrEfdInnCTsCabVL_DESC: TFloatField;
    QrEfdInnCTsCabIND_FRT: TWideStringField;
    QrEfdInnCTsCabVL_SERV: TFloatField;
    QrEfdInnCTsCabVL_BC_ICMS: TFloatField;
    QrEfdInnCTsCabVL_ICMS: TFloatField;
    QrEfdInnCTsCabVL_NT: TFloatField;
    QrEfdInnCTsCabCOD_INF: TWideStringField;
    QrEfdInnCTsCabCOD_CTA: TWideStringField;
    QrEfdInnCTsCabCOD_MUN_ORIG: TIntegerField;
    QrEfdInnCTsCabCOD_MUN_DEST: TIntegerField;
    QrEfdInnCTsCabCST_ICMS: TIntegerField;
    QrEfdInnCTsCabCFOP: TIntegerField;
    QrEfdInnCTsCabALIQ_ICMS: TFloatField;
    QrEfdInnCTsCabVL_OPR: TFloatField;
    QrEfdInnCTsCabVL_RED_BC: TFloatField;
    QrEfdInnCTsCabCOD_OBS: TWideStringField;
    QrEfdInnCTsCabRegrFiscal: TIntegerField;
    QrEfdInnCTsCabIND_NAT_FRT: TWideStringField;
    QrEfdInnCTsCabVL_ITEM: TFloatField;
    QrEfdInnCTsCabCST_PIS: TWideStringField;
    QrEfdInnCTsCabNAT_BC_CRED: TWideStringField;
    QrEfdInnCTsCabVL_BC_PIS: TFloatField;
    QrEfdInnCTsCabALIQ_PIS: TFloatField;
    QrEfdInnCTsCabVL_PIS: TFloatField;
    QrEfdInnCTsCabCST_COFINS: TWideStringField;
    QrEfdInnCTsCabVL_BC_COFINS: TFloatField;
    QrEfdInnCTsCabALIQ_COFINS: TFloatField;
    QrEfdInnCTsCabVL_COFINS: TFloatField;
    QrEfdInnCTsCabTerceiro: TIntegerField;
    QrEfdInnCTsCabControle: TIntegerField;
    QrIIND_MOV: TSmallintField;
    QrILk: TIntegerField;
    QrIDataCad: TDateField;
    QrIDataAlt: TDateField;
    QrIUserCad: TIntegerField;
    QrIUserAlt: TIntegerField;
    QrIAlterWeb: TSmallintField;
    QrIAWServerID: TIntegerField;
    QrIAWStatSinc: TSmallintField;
    QrIAtivo: TSmallintField;
    QrEfdInnC500Cab: TMySQLQuery;
    QrEfdInnC500CabNO_TERC: TWideStringField;
    QrEfdInnC500CabAnoMes: TIntegerField;
    QrEfdInnC500CabEmpresa: TIntegerField;
    QrEfdInnC500CabCodigo: TIntegerField;
    QrEfdInnC500CabControle: TIntegerField;
    QrEfdInnC500CabIND_OPER: TWideStringField;
    QrEfdInnC500CabIND_EMIT: TWideStringField;
    QrEfdInnC500CabCOD_MOD: TWideStringField;
    QrEfdInnC500CabCOD_SIT: TWideStringField;
    QrEfdInnC500CabSER: TWideStringField;
    QrEfdInnC500CabSUB: TWideStringField;
    QrEfdInnC500CabCOD_CONS: TWideStringField;
    QrEfdInnC500CabNUM_DOC: TIntegerField;
    QrEfdInnC500CabDT_DOC: TDateField;
    QrEfdInnC500CabDT_E_S: TDateField;
    QrEfdInnC500CabVL_DOC: TFloatField;
    QrEfdInnC500CabVL_DESC: TFloatField;
    QrEfdInnC500CabVL_FORN: TFloatField;
    QrEfdInnC500CabVL_SERV_NT: TFloatField;
    QrEfdInnC500CabVL_TERC: TFloatField;
    QrEfdInnC500CabVL_DA: TFloatField;
    QrEfdInnC500CabVL_BC_ICMS: TFloatField;
    QrEfdInnC500CabVL_ICMS: TFloatField;
    QrEfdInnC500CabVL_BC_ICMS_ST: TFloatField;
    QrEfdInnC500CabVL_ICMS_ST: TFloatField;
    QrEfdInnC500CabCOD_INF: TWideStringField;
    QrEfdInnC500CabVL_PIS: TFloatField;
    QrEfdInnC500CabVL_COFINS: TFloatField;
    QrEfdInnC500CabTP_LIGACAO: TWideStringField;
    QrEfdInnC500CabCOD_GRUPO_TENSAO: TWideStringField;
    QrEfdInnC500CabTerceiro: TIntegerField;
    QrEfdInnC500CabImportado: TSmallintField;
    QrEfdInnC500CabCHV_DOCe: TWideStringField;
    QrEfdInnC500CabFIN_DOCe: TSmallintField;
    QrEfdInnC500CabCHV_DOCe_REF: TWideStringField;
    QrEfdInnC500CabIND_DEST: TSmallintField;
    QrEfdInnC500CabCOD_MUN_DEST: TIntegerField;
    QrEfdInnC500CabCOD_CTA: TWideStringField;
    QrEfdInnC500CabCOD_MOD_DOC_REF: TSmallintField;
    QrEfdInnC500CabHASH_DOC_REF: TWideStringField;
    QrEfdInnC500CabSER_DOC_REF: TWideStringField;
    QrEfdInnC500CabNUM_DOC_REF: TIntegerField;
    QrEfdInnC500CabMES_DOC_REF: TIntegerField;
    QrEfdInnC500CabENER_INJET: TFloatField;
    QrEfdInnC500CabOUTRAS_DED: TFloatField;
    QrEfdInnD500Cab: TMySQLQuery;
    QrEfdInnD500CabNO_TERC: TWideStringField;
    QrEfdInnD500CabAnoMes: TIntegerField;
    QrEfdInnD500CabEmpresa: TIntegerField;
    QrEfdInnD500CabCodigo: TIntegerField;
    QrEfdInnD500CabControle: TIntegerField;
    QrEfdInnD500CabIND_OPER: TWideStringField;
    QrEfdInnD500CabIND_EMIT: TWideStringField;
    QrEfdInnD500CabCOD_MOD: TWideStringField;
    QrEfdInnD500CabCOD_SIT: TWideStringField;
    QrEfdInnD500CabSER: TWideStringField;
    QrEfdInnD500CabSUB: TWideStringField;
    QrEfdInnD500CabNUM_DOC: TIntegerField;
    QrEfdInnD500CabDT_DOC: TDateField;
    QrEfdInnD500CabDT_A_P: TDateField;
    QrEfdInnD500CabVL_DOC: TFloatField;
    QrEfdInnD500CabVL_DESC: TFloatField;
    QrEfdInnD500CabVL_SERV: TFloatField;
    QrEfdInnD500CabVL_SERV_NT: TFloatField;
    QrEfdInnD500CabVL_TERC: TFloatField;
    QrEfdInnD500CabVL_DA: TFloatField;
    QrEfdInnD500CabVL_BC_ICMS: TFloatField;
    QrEfdInnD500CabVL_ICMS: TFloatField;
    QrEfdInnD500CabCOD_INF: TWideStringField;
    QrEfdInnD500CabVL_PIS: TFloatField;
    QrEfdInnD500CabVL_COFINS: TFloatField;
    QrEfdInnD500CabCOD_CTA: TWideStringField;
    QrEfdInnD500CabTP_ASSINANTE: TWideStringField;
    QrEfdInnD500CabTerceiro: TIntegerField;
    QrEfdInnD500CabImportado: TSmallintField;
    QrEfdInnC500ItsPIS: TMySQLQuery;
    QrEfdInnD500Its: TMySQLQuery;
    QrEfdInnD500ItsCST_ICMS: TIntegerField;
    QrEfdInnD500ItsCFOP: TIntegerField;
    QrEfdInnD500ItsALIQ_ICMS: TFloatField;
    QrEfdInnD500ItsVL_OPR: TFloatField;
    QrEfdInnD500ItsVL_BC_ICMS: TFloatField;
    QrEfdInnD500ItsVL_ICMS: TFloatField;
    QrEfdInnD500ItsVL_BC_ICMS_UF: TFloatField;
    QrEfdInnD500ItsVL_ICMS_UF: TFloatField;
    QrEfdInnD500ItsVL_RED_BC: TFloatField;
    QrEfdInnC500ItsCOFINS: TMySQLQuery;
    QrEfdInnC500ItsPISCST_PIS: TWideStringField;
    QrEfdInnC500ItsPISALIQ_PIS: TFloatField;
    QrEfdInnC500ItsPISNAT_BC_CRED: TWideStringField;
    QrEfdInnC500ItsPISCOD_CTA: TWideStringField;
    QrEfdInnC500ItsPISVL_ITEM: TFloatField;
    QrEfdInnC500ItsPISVL_PIS: TFloatField;
    QrEfdInnC500ItsPISVL_BC_PIS: TFloatField;
    QrEfdInnC500ItsCOFINSCST_COFINS: TWideStringField;
    QrEfdInnC500ItsCOFINSALIQ_COFINS: TFloatField;
    QrEfdInnC500ItsCOFINSNAT_BC_CRED: TWideStringField;
    QrEfdInnC500ItsCOFINSCOD_CTA: TWideStringField;
    QrEfdInnC500ItsCOFINSVL_ITEM: TFloatField;
    QrEfdInnC500ItsCOFINSVL_COFINS: TFloatField;
    QrEfdInnC500ItsCOFINSVL_BC_COFINS: TFloatField;
    QrEfdInnD500ItsPIS: TMySQLQuery;
    QrEfdInnD500ItsPISCST_PIS: TWideStringField;
    QrEfdInnD500ItsPISALIQ_PIS: TFloatField;
    QrEfdInnD500ItsPISNAT_BC_CRED: TWideStringField;
    QrEfdInnD500ItsPISCOD_CTA: TWideStringField;
    QrEfdInnD500ItsPISVL_ITEM: TFloatField;
    QrEfdInnD500ItsPISVL_PIS: TFloatField;
    QrEfdInnD500ItsPISVL_BC_PIS: TFloatField;
    QrEfdInnD500ItsCOFINS: TMySQLQuery;
    QrEfdInnD500ItsCOFINSCST_COFINS: TWideStringField;
    QrEfdInnD500ItsCOFINSALIQ_COFINS: TFloatField;
    QrEfdInnD500ItsCOFINSNAT_BC_CRED: TWideStringField;
    QrEfdInnD500ItsCOFINSCOD_CTA: TWideStringField;
    QrEfdInnD500ItsCOFINSVL_ITEM: TFloatField;
    QrEfdInnD500ItsCOFINSVL_COFINS: TFloatField;
    QrEfdInnD500ItsCOFINSVL_BC_COFINS: TFloatField;
    QrSumM210: TMySQLQuery;
    QrSumM210CST_PIS: TWideStringField;
    QrSumM210COD_INC_TRIB: TSmallintField;
    QrSumM210RegimPisCofins: TSmallintField;
    QrSumM210VL_ITEM: TFloatField;
    QrSumM210VL_BC_PIS: TFloatField;
    QrSumM210ALIQ_PIS: TFloatField;
    QrSumM210QUANT_BC_PIS: TFloatField;
    QrSumM210ALIQ_PIS_QUANT: TFloatField;
    QrSumM210VL_CONT_APUR: TFloatField;
    QrSumM210VL_CONT_PER: TFloatField;
    QrSumM210COD_CONT: TWideStringField;
    QrSumM210LinArq: TIntegerField;
    QrSumM210IND_ESCRI: TWideStringField;
    QrSumM210IND_OPER: TWideStringField;
    QrSumM610: TMySQLQuery;
    QrSumM610CST_COFINS: TWideStringField;
    QrSumM610COD_INC_TRIB: TSmallintField;
    QrSumM610RegimPisCofins: TSmallintField;
    QrSumM610VL_ITEM: TFloatField;
    QrSumM610VL_BC_COFINS: TFloatField;
    QrSumM610ALIQ_COFINS: TFloatField;
    QrSumM610QUANT_BC_COFINS: TFloatField;
    QrSumM610ALIQ_COFINS_QUANT: TFloatField;
    QrSumM610VL_CONT_APUR: TFloatField;
    QrSumM610VL_CONT_PER: TFloatField;
    QrSumM610COD_CONT: TWideStringField;
    QrSumM610LinArq: TIntegerField;
    QrSumM610IND_ESCRI: TWideStringField;
    QrSumM610IND_OPER: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdMesChange(Sender: TObject);
    procedure MeGeradoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure MeGeradoMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MeGeradoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RGTIPO_ESCRITClick(Sender: TObject);
    procedure BtErrosClick(Sender: TObject);
    procedure TVBlocosClick(Sender: TObject);
    procedure TVBlocosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure SbTeste_minimo_EFD_PIS_COFINSClick(Sender: TObject);
    procedure BtMesClick(Sender: TObject);
    procedure frxSEII_EstqGetValue(const VarName: string; var Value: Variant);
    procedure SbImplementadoClick(Sender: TObject);
    procedure EdMesRedefinido(Sender: TObject);
    procedure EdIND_SIT_ESPChange(Sender: TObject);
  private
    { Private declarations }
    FDtFiscal0053_Entr, FCordaDe0001, FCordaDe0053, FCordaDeCTes,
    FDtFiscalFrete_Entr, FCordaSai100: String;
    //FContaIND_MOV_1: Integer;
    FGeraBloco, FConfigurouRegistros: Boolean;
    FSelEnt, FSPEDEFD_Blcs, FBloco, FRegistro, FEmpresa_Txt, FAnoMes_Txt,
    FVersao, FSPEDEFD_0200, FSPEDEFD_0190(*, FSpedEfdPisCofins_0150,
    FSpedEfdPisCofins_0190, FSpedEfdPisCofins_0200*), FSPEDEFD_0400,
    FSPEDEFD_0500: String;
    FEmpresa_Int, FAnoMes_Int, FLinArq,
    FLin0200, FLin0400, FLin0500, FLin0900,
    FLinC010, FLinC100, FLinC500, FLinD010, FLinD100, FLinD195, FLinD500,
    FLinM100, FLinM200, FLinM300, FLinM400, FLinM500, FLinM600, FLinM700, FLinM800,


    FQTD_LIN_0400, FQTD_LIN_0500,
    FQTD_LIN_C170, FQTD_LIN_C501, FQTD_LIN_C505,
    FQTD_LIN_D101, FQTD_LIN_D105, FQTD_LIN_D501, FQTD_LIN_D505, FQTD_LIN_D590,

    FQTD_LIN_0, FQTD_LIN_A, FQTD_LIN_C, FQTD_LIN_D, FQTD_LIN_F, FQTD_LIN_I,
    FQTD_LIN_M, FQTD_LIN_M205, FQTD_LIN_M210, FQTD_LIN_M605, FQTD_LIN_M610,
    FQTD_LIN_P, FQTD_LIN_1, FQTD_LIN_9, FREG_BLC_Count: Integer;
    FREG_BLC_Fields: array of String;
    FREG_BLC_Linhas: array of Integer;
    FLinStr: WideString;
    FArqStr: TStringList;
    FDataIni_Dta, FDataFim_Dta: TDateTime;
    FDataIni_Txt, FDataFim_Txt, FIntervaloFiscal: String;
    FSQLCampos, FSQLIndex: array of String;
    FValCampos, FValIndex: array of Variant;
    FSelReg0400, FSelReg0500,
    FSelRegC100, FSelRegC500,
    FSelRegD100, FSelRegD195, FSelRegD197, FSelRegD500,
    FSelRegK210, FSelRegK220, FSelRegK230, FSelRegK250, FSelRegK290,
    FSelRegK300: Boolean;
    //
    FLstCAT66SP2018: Array of Integer;


    function  AddCampoSQL(Campo: String; Valor: Variant): Boolean;
    procedure AtualizaTabelasSPEDEFD();
    function  AtualizaCampoNFeCabA(Campo: String; Valor: Variant;
              FatID, FatNum, Empresa: Integer): Boolean;
    //
    procedure AdicionaLinha(var Contador: Integer);
    procedure AdicionaBLC(Linhas: Integer; Registro: String = '');
    function  AC_6(IndOper: TIndicadorDoTipoDeOperacao; Campo: String;
              Inteiro64: Int64; PermiteZero, InformaZero: Boolean): Boolean;
    function  AC_d(IndOper: TIndicadorDoTipoDeOperacao; Campo: String;
              Data: TDateTime; PermiteZero, InformaZero: Boolean;
              Formato: Integer): Boolean;
    function  AC_f(IndOper: TIndicadorDoTipoDeOperacao; Campo: String;
              Flutuante: Double; PermiteZero, InformaZero: Boolean): Boolean;
    function  AC_i(IndOper: TIndicadorDoTipoDeOperacao; Campo: String;
              Inteiro: Integer; PermiteZero, InformaZero: Boolean;
              LeftZeros: Boolean = False): Boolean;
    function  AC_n(IndOper: TIndicadorDoTipoDeOperacao; Campo, Valor: String):
              Boolean;
    function  AC_x(IndOper: TIndicadorDoTipoDeOperacao; Campo, Valor: String):
              Boolean;
    procedure InfoPosMeGerado();
    function  CalculaCampoEFD_M210_03(): Double;
    function  CampoNaoConfere(): Boolean;
    function  CampoNaoEhNumero(Valor: String): Boolean;
    function  CampoObrigatorio(LODC: String): Boolean;
    function  CampoObrigatorio_6(LODC: String; Inteiro64: Int64; PermiteZero: Boolean): Boolean;
    function  CampoObrigatorio_d(LODC: String; Data: TDatetime; PermiteZero: Boolean): Boolean;
    function  CampoObrigatorio_f(LODC: String; Flutuante: Double; PermiteZero: Boolean): Boolean;
    function  CampoObrigatorio_i(LODC: String; Inteiro: Integer; PermiteZero: Boolean): Boolean;
    function  CampoObrigatorio_x(LODC: String; Valor: String): Boolean;
    procedure DefineEmpresaEAnoMes();
    function  Define_REG(): Boolean;
    function  ExcluiPeriodoSelecionado(): Boolean;
    procedure HabilitaBotaoOK();
    function  MensagemCampoDesconhecido(): Boolean;
    function  MensagemCampoNaoEhNumero(): Boolean;
    procedure MensagemCampoObrigatorio();
    procedure MensagemCampoIncorreto(tpEsper: String);
    procedure MensagemDecimaisIncorreto(tpEsper: String);
    procedure MensagemDocumentoInvalido(Doc: String);
    procedure MensagemTamanhoDifereDoObrigatorio(Valor: String);
    procedure MensagemTamanhoMuitoGrande(Valor: String);
    function  NaoTemErros(Avisa: Boolean): Boolean;
    function  ObrigatoriedadeConfere(const LODC: String;
              var IndicadorPreenchimento: TIndicadorPreenchimento): Boolean;

    function  ReopenCampos(var Res: Boolean): Boolean;
    function  SalvaArquivo(): Boolean;
    procedure PredefineLinha();
    function  TamanhoMuitoGrande(Valor: String): Boolean;
    function  TamanhoDifereDoObrigatorio(LODC: String; Valor: String): Boolean;
    //
    function  GeraRegistro_0000(): Boolean;
    function  GeraRegistro_0001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_0100(): Boolean;
    function  GeraRegistro_0110(): Boolean;
    function  GeraRegistro_0140(): Boolean;
    function  GeraRegistro_0150(): Boolean;
    function  GeraRegistro_0190(): Boolean;
    function  GeraRegistro_0200(): Boolean;
    function  GeraRegistro_0205(): Boolean;
    function  GeraRegistro_0400(): Boolean;
    function  GeraRegistro_0500(): Boolean;
    function  GeraRegistro_0900_Pre(): Boolean;
    function  GeraRegistro_0900_Pos(): Boolean;
    function  GeraRegistro_0990(): Boolean;

    function  GeraRegistro_A001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_A990(): Boolean;

    function  GeraRegistro_C001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_C010(): Boolean;
    function  GeraRegistro_C100_Emit(): Boolean;
    function  GeraRegistro_C100_Entr(): Boolean;
    function  GeraRegistro_C170(): Boolean;
    function  GeraRegistro_C170_Emit(): Boolean;
    function  GeraRegistro_C170_Entr(): Boolean;
    function  GeraRegistro_C500(): Boolean;
    function  GeraRegistro_C501(): Boolean;
    function  GeraRegistro_C505(): Boolean;
    function  GeraRegistro_C990(): Boolean;

    function  GeraRegistro_D001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_D010(): Boolean;
    function  GeraRegistro_D100(): Boolean;
    function  GeraRegistro_D500(): Boolean;
    function  GeraRegistro_D501(): Boolean;
    function  GeraRegistro_D505(): Boolean;
    function  GeraRegistro_D990(): Boolean;

    function  GeraRegistro_F001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_F990(): Boolean;

    function  GeraRegistro_I001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_I990(): Boolean;

    function  GeraRegistro_M001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_M990(): Boolean;
    function  GeraRegistro_M200(): Boolean;
    function  GeraRegistro_M205(): Boolean;
    function  GeraRegistro_M210(): Boolean;
    function  GeraRegistro_M600(): Boolean;
    function  GeraRegistro_M605(): Boolean;
    function  GeraRegistro_M610(): Boolean;

    function  GeraRegistro_P001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_P990(): Boolean;

    function  GeraRegistro_1001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_1010(): Boolean;
    function  GeraRegistro_1100(): Boolean;
    function  GeraRegistro_1990(): Boolean;
    function  GeraRegistro_9001(IND_MOV: Integer): Boolean;
    function  GeraRegistro_9900(): Boolean;
    function  GeraRegistro_9990(): Boolean;
    function  GeraRegistro_9999(): Boolean;

    function  ReCriaNoh(const Noh: TTreeNode; const nN: String; const cN:
              Integer; const Ativo: Integer): TTreeNode;
    procedure RecriaRegistroEFD_E110();
    procedure RecriaRegistroEFD_E520();

    function  ReopenCabA_C(Filtra: Boolean; Filtro: Variant): Boolean;
    function  ReopenDocumentos(): Boolean;
    procedure ReopenEfdInnCTsCab();
    procedure ReopenEfdInnC500Cab();
    procedure ReopenEfdInnC500ItsPIS(Controle: Integer);
    procedure ReopenEfdInnC500ItsCOFINS(Controle: Integer);
    procedure ReopenEfdInnD500Cab();
    procedure ReopenEfdInnD500ItsPIS(Controle: Integer);
    procedure ReopenEfdInnD500ItsCOFINS(Controle: Integer);

    procedure ReopenEmpresa();
    procedure ReopenEnder(Entidade: Integer);
    procedure ReopenVersoes();
    procedure ReopenSumM210();
    procedure ReopenSumM610();
    function  Blocos_Selecionados2(Excecao: array of String): String;
    procedure Grava_SpedEfdPisCofinsErrs(Erro: TEFDExpErr; Campo: String);
    function  PodeGerarBloco(Bloco: String): Boolean;
    procedure ConfiguraTreeView2(Ativos, Cinzas: array of String);
    //
    function  ObtemCOD_ITEM(GraGruX: Integer): String;
    function  ObtemCOD_PART(Entidade: Integer): String;
    function  ObtemITO_CabA(var EhE: Boolean; var IND_OPER, tpNF: String;
              var ITO: TIndicadorDoTipoDeOperacao): Boolean;
    function  ObtemITO_InNF(var EhE: Boolean; var IND_OPER, tpNF: String;
              var ITO: TIndicadorDoTipoDeOperacao): Boolean;
    function  LetraObrigatoriedadeDoCampo(ITO: TIndicadorDoTipoDeOperacao): String;
    procedure VerificaDESCR_ITEM(DESCR_ITEM: String; Query: TmySQLQuery);
    function  SelReg(Reg: String): Boolean;
    //function AddCampoSQL(Campo: String; Valor: Variant): Boolean;
    //function  DefineIND_MOV(FisRegCad, ide_finNFe: Integer; prod_cProd: String): Integer;
    procedure RedefinePeriodos();
    function  GeraCOD_OBS_CAT66SP2018(Seq: Integer): String;
    function  CordaDeQuery_Sai100(): String;

  public
    { Public declarations }
  end;

  var
  FmEfdPisCofinsExporta_v01_35: TFmEfdPisCofinsExporta_v01_35;

implementation

uses UnMyObjects, ModuleGeral, Module, SPED_EFD_DownTabs, UMySQLModule, MyDBCheck,
  EfdPisCofinsErros_v01_35, Principal, ModuleNFe_0000, ModProd, ModuleSPED,
  UnGrade_Create, UnInternalConsts, MyGlyfs, UCreate, UnSPED_Create, NFe_PF;

{$R *.DFM}

const
  FImporExpor: Integer = 2; // Exporta. N�o Mexer.
  FIE_TXT: String = '2';

function TFmEfdPisCofinsExporta_v01_35.AC_6(IndOper: TIndicadorDoTipoDeOperacao;
Campo: String; Inteiro64: Int64;
PermiteZero, InformaZero: Boolean): Boolean;
var
  Valor: String;
  N: Integer;
  IndicadorPreenchimento: TIndicadorPreenchimento;
  LODC: String;
begin
  if QrCamposCampo.Value = Campo then
  begin
    Result := True;
    //
    LODC := LetraObrigatoriedadeDoCampo(IndOper);
    //
    if (Inteiro64 = 0) and (not InformaZero) then
      Valor := ''
    else
      Valor := FormatFloat('0', Inteiro64);
    if (QrCamposTObrig.Value = 1) and (Valor <> '') then
    begin
      while Length(Valor) < QrCamposTam.Value do
        Valor := '0' + Valor;
    end;
    //
    if ObrigatoriedadeConfere(LODC, IndicadorPreenchimento) then
      if CampoObrigatorio_x(LODC, Valor) then
        MensagemCampoObrigatorio()
    else
    if CampoObrigatorio_6(LODC, Inteiro64, PermiteZero) then
      MensagemCampoObrigatorio()
    else
    if QrCamposTipo.Value <> 'N'  then
      MensagemCampoIncorreto('N')
    else
    if QrCamposDecimais.Value > 0  then
      MensagemDecimaisIncorreto('6')
    else
    if TamanhoMuitoGrande(Valor)  then
      MensagemTamanhoMuitoGrande(Valor)
    else
    if TamanhoDifereDoObrigatorio(LODC, Valor)  then
      MensagemTamanhoDifereDoObrigatorio(Valor);
    //
   if (Inteiro64 = 0) and (not InformaZero) then
      Valor := ''
    else
      Valor := FormatFloat('0', Inteiro64);
    if (QrCamposTObrig.Value = 1) and (Valor <> '') then
    begin
      while Length(Valor) < QrCamposTam.Value do
        Valor := '0' + Valor;
    end;
    //
    if IndicadorPreenchimento = TIndicadorPreenchimento.ipcNaoPreencher then
      FLinStr := FLinStr + '|'
    else
      FLinStr := FLinStr + Trim(Geral.TxtSemPipe(Valor)) + '|';
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := Campo;
    SetLength(FValCampos, N + 1);
    FValCampos[N] := Valor;
    //
     Result := SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
  end else
    Result := False;
end;

function TFmEfdPisCofinsExporta_v01_35.AC_d(IndOper: TIndicadorDoTipoDeOperacao;
  Campo: String; Data: TDateTime; PermiteZero, InformaZero: Boolean;
  Formato: Integer): Boolean;
var
  ValTxt, ValSql: String;
  N: Integer;
  IndicadorPreenchimento: TIndicadorPreenchimento;
  LODC: String;
begin
  if QrCamposCampo.Value = Campo then
  begin
    Result := True;
    //
    LODC := LetraObrigatoriedadeDoCampo(IndOper);
    //
    if (Data < 2) and (not InformaZero) then
    begin
      ValSql := '0000-00-00';
      ValTxt := '';
    end else
    begin
      ValSql := Geral.FDT(Data, 1);
      ValTxt := Geral.FDT(Data, Formato);
    end;
    if ObrigatoriedadeConfere(LODC, IndicadorPreenchimento) then
    if CampoObrigatorio_x(LODC, ValTxt) then
      MensagemCampoObrigatorio()
    else
    if CampoObrigatorio_d(LODC, Data, PermiteZero) then
      MensagemCampoObrigatorio()
    else
    if QrCamposTipo.Value <> 'N'  then
      MensagemCampoIncorreto('N')
    else
    if QrCamposDecimais.Value > 0  then
      MensagemDecimaisIncorreto('I')
    else
    if TamanhoMuitoGrande(ValTxt)  then
      MensagemTamanhoMuitoGrande(ValTxt)
    else
    if TamanhoDifereDoObrigatorio(LODC, ValTxt)  then
      MensagemTamanhoDifereDoObrigatorio(ValTxt);
    //
    if IndicadorPreenchimento = TIndicadorPreenchimento.ipcNaoPreencher then
      FLinStr := FLinStr + '|'
    else
      FLinStr := FLinStr + Geral.TxtSemPipe(ValTxt) + '|';
    //
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := Campo;
    SetLength(FValCampos, N + 1);
    FValCampos[N] := ValSql;
    //
    //
    Result := SPED_Geral.VDom(FBloco, FRegistro, Campo, ValTxt, MeErros);
  end else
    Result := False;
end;

function TFmEfdPisCofinsExporta_v01_35.AC_f(IndOper: TIndicadorDoTipoDeOperacao;
Campo: String; Flutuante: Double; PermiteZero,
  InformaZero: Boolean): Boolean;
var
  Valor: String;
  N: Integer;
  IndicadorPreenchimento: TIndicadorPreenchimento;
  LODC: String;
begin
  if QrCamposCampo.Value = Campo then
  begin
    Result := True;
    //
    LODC := LetraObrigatoriedadeDoCampo(IndOper);
    //
    if (Flutuante = 0) and (not InformaZero) then
      Valor := ''
    else
      Valor := Geral.DFT_SPED(Flutuante, QrCamposDecimais.Value, siNegativo);
    if (QrCamposTObrig.Value = 1) and (Valor <> '') then
    begin
      while Length(Valor) < QrCamposTam.Value do
        Valor := '0' + Valor;
    end;
    if ObrigatoriedadeConfere(LODC, IndicadorPreenchimento) then

    if CampoObrigatorio_x(LODC, Valor) then
      MensagemCampoObrigatorio()
    else
    if CampoObrigatorio_f(LODC, Flutuante, PermiteZero) then
      MensagemCampoObrigatorio()
    else
    if QrCamposTipo.Value <> 'N'  then
      MensagemCampoIncorreto('N')
    else
    if QrCamposDecimais.Value = 0  then
      MensagemDecimaisIncorreto('F')
    else
    if TamanhoMuitoGrande(Valor)  then
      MensagemTamanhoMuitoGrande(Valor)
    else
    if TamanhoDifereDoObrigatorio(LODC, Valor)  then
      MensagemTamanhoDifereDoObrigatorio(Valor);
    //
    if IndicadorPreenchimento = TIndicadorPreenchimento.ipcNaoPreencher then
      FLinStr := FLinStr + '|'
    else
      FLinStr := FLinStr + Trim(Geral.TxtSemPipe(Valor)) + '|';
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := Campo;
    SetLength(FValCampos, N + 1);
    FValCampos[N] := Geral.DMV(Valor);
    //
    Result := SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
  end else
    Result := False;
end;

function TFmEfdPisCofinsExporta_v01_35.AC_i(IndOper: TIndicadorDoTipoDeOperacao;
Campo: String; Inteiro: Integer;
PermiteZero, InformaZero: Boolean; LeftZeros: Boolean): Boolean;
var
  Valor: String;
  N: Integer;
  IndicadorPreenchimento: TIndicadorPreenchimento;
  LODC: String;
begin
  if QrCamposCampo.Value = Campo then
  begin
    Result := True;
    //
    LODC := LetraObrigatoriedadeDoCampo(IndOper);
    //
    if (Inteiro = 0) and (not InformaZero) then
      Valor := ''
    else
      Valor := Geral.FF0(Inteiro);
    if ((QrCamposTObrig.Value = 1) and (Valor <> '')) or LeftZeros then
    begin
      while Length(Valor) < QrCamposTam.Value do
        Valor := '0' + Valor;
    end;
    if ObrigatoriedadeConfere(LODC, IndicadorPreenchimento) then


    if CampoObrigatorio_x(LODC, Valor) then
      MensagemCampoObrigatorio()
    else
    if CampoObrigatorio_i(LODC, Inteiro, PermiteZero) then
      MensagemCampoObrigatorio()
    else
    if QrCamposTipo.Value <> 'N'  then
      MensagemCampoIncorreto('N')
    else
    if QrCamposDecimais.Value > 0  then
      MensagemDecimaisIncorreto('I')
    else
    if TamanhoMuitoGrande(Valor)  then
      MensagemTamanhoMuitoGrande(Valor)
    else
    if TamanhoDifereDoObrigatorio(LODC, Valor)  then
      MensagemTamanhoDifereDoObrigatorio(Valor);
    //
    if IndicadorPreenchimento = TIndicadorPreenchimento.ipcNaoPreencher then
      FLinStr := FLinStr + '|'
    else
      FLinStr := FLinStr + Trim(Geral.TxtSemPipe(Valor)) + '|';
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := Campo;
    SetLength(FValCampos, N + 1);
    FValCampos[N] := Inteiro;
    //
    Result := SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
  end else
    Result := False;
end;

function TFmEfdPisCofinsExporta_v01_35.AC_n(IndOper: TIndicadorDoTipoDeOperacao; Campo,
  Valor: String): Boolean;
var
  N: Integer;
  IndicadorPreenchimento: TIndicadorPreenchimento;
  LODC: String;
begin
  if QrCamposCampo.Value = Campo then
  begin
    Result := True;
    //
    LODC := LetraObrigatoriedadeDoCampo(IndOper);
    //
    if ObrigatoriedadeConfere(LODC, IndicadorPreenchimento) then
    if CampoObrigatorio_x(LODC, Valor) then
      MensagemCampoObrigatorio()
    else
    if CampoNaoEhNumero(Valor) then
      MensagemCampoNaoEhNumero()
    else
    if QrCamposTipo.Value <> 'N'  then
      MensagemCampoIncorreto('N')
    else
    if QrCamposDecimais.Value > 0  then
      MensagemDecimaisIncorreto('N')
    else
    if TamanhoMuitoGrande(Valor) then
      MensagemTamanhoMuitoGrande(Valor)
    else
    if TamanhoDifereDoObrigatorio(LODC, Valor) then
      MensagemTamanhoDifereDoObrigatorio(Valor);
    //
    if IndicadorPreenchimento = TIndicadorPreenchimento.ipcNaoPreencher then
      FLinStr := FLinStr + '|'
    else
      FLinStr := FLinStr + Trim(Geral.TxtSemPipe(Valor)) + '|';
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := Campo;
    SetLength(FValCampos, N + 1);
    FValCampos[N] := Valor;
    //
    Result := SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
  end else
    Result := False;
end;

function TFmEfdPisCofinsExporta_v01_35.AC_x(IndOper: TIndicadorDoTipoDeOperacao;
Campo, Valor: String): Boolean;
var
  N: Integer;
  Texto: String;
  IndicadorPreenchimento: TIndicadorPreenchimento;
  LODC: String;
begin
  if QrCamposCampo.Value = Campo then
  begin
    Result  := True;
    //
    LODC := LetraObrigatoriedadeDoCampo(IndOper);
    //
    Texto   := Geral.Substitui(Geral.Substitui(Valor, #13, ''), #10, ' ');
    if ObrigatoriedadeConfere(LODC, IndicadorPreenchimento) then
    if CampoObrigatorio_x(LODC, Texto) then
      MensagemCampoObrigatorio()
    else
    if QrCamposTipo.Value <> 'C'  then
      MensagemCampoIncorreto('C')
    else
    if QrCamposDecimais.Value > 0  then
      MensagemDecimaisIncorreto('C')
    else
    if TamanhoMuitoGrande(Texto)  then
      MensagemTamanhoMuitoGrande(Texto)
    else
    if TamanhoDifereDoObrigatorio(LODC, Texto)  then
      MensagemTamanhoDifereDoObrigatorio(Texto);
    //
    if IndicadorPreenchimento = TIndicadorPreenchimento.ipcNaoPreencher then
      FLinStr := FLinStr + '|'
    else
      FLinStr := FLinStr + Trim(Geral.TxtSemPipe(Texto)) + '|';
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := Campo;
    SetLength(FValCampos, N + 1);
    FValCampos[N] := Texto;
    //
    Result := SPED_Geral.VDom(FBloco, FRegistro, Campo, Texto, MeErros);
  end else
    Result := False;
end;

function TFmEfdPisCofinsExporta_v01_35.AddCampoSQL(Campo: String; Valor: Variant): Boolean;
var
  N: Integer;
begin
  N := Length(FSQLCampos);
  SetLength(FSQLCampos, N + 1);
  FSQLCampos[N] := Campo;
  SetLength(FValCampos, N + 1);
  FValCampos[N] := Valor;
end;

procedure TFmEfdPisCofinsExporta_v01_35.AdicionaBLC(Linhas: Integer; Registro: String = '');
var
  Reg: String;
begin
  if Linhas > 0 then
  begin
    if Registro = '' then
      Reg := FRegistro
    else
      Reg := Registro;
    FREG_BLC_Count := FREG_BLC_Count + 1;
    SetLength(FREG_BLC_Fields, FREG_BLC_Count);
    SetLength(FREG_BLC_Linhas, FREG_BLC_Count);
    FREG_BLC_Fields[FREG_BLC_Count - 1] := Reg;
    FREG_BLC_Linhas[FREG_BLC_Count - 1] := Linhas;
  end;
end;

procedure TFmEfdPisCofinsExporta_v01_35.AdicionaLinha(var Contador: Integer);
var
  Tabela: String;
begin
  FArqStr.Add(FLinStr);
  MeGerado.Lines.Add(FLinStr);
  //
  Tabela := 'spedefdpiscofins' + FValCampos[1];
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False,
    FSQLCampos, FSQLIndex, FValCampos, FValIndex, True);
  //
  Contador := Contador + 1;
  if FLinStr <> FArqStr[FArqStr.Count-1] then
    Geral.MB_ERRO(
    'Linha gerada n�o foi adicionada corretamente ao texto do arquivo a ser gerado (1)!'
    + sLineBreak + 'Informe a Dermatek!');
  if FLinStr <> MeGerado.Lines[MeGerado.Lines.Count-1] then
    Geral.MB_ERRO('Linha gerada n�o foi adicionada corretamente ao texto do arquivo a ser gerado (2)!'
    + sLineBreak + 'Informe a Dermatek!');
end;

function TFmEfdPisCofinsExporta_v01_35.AtualizaCampoNFeCabA(Campo: String;
  Valor: Variant; FatID, FatNum, Empresa: Integer): Boolean;
begin
  Result :=
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
  Campo], ['FatID', 'FatNum', 'Empresa'], [
  Valor], [FatID, FatNum, Empresa], True);
end;

procedure TFmEfdPisCofinsExporta_v01_35.AtualizaTabelasSPEDEFD();
begin
  if Geral.MB_Pergunta(
  'Deseja verificar se h� atualiza��o das tabelas de consulta do SPED?') = ID_YES then
  begin
    if DBCheck.CriaFm(TFmSPED_EFD_DownTabs, FmSPED_EFD_DownTabs, afmoNegarComAviso) then
    begin
      FmSPED_EFD_DownTabs.ShowModal;
      FmSPED_EFD_DownTabs.Destroy;
    end;
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.Blocos_Selecionados2(Excecao: array of String): String;
var
  _OR: String;
begin
  _OR := '';
  Result := '';
  //
  QrNiv2.Close;
  QrNiv2.Database := DmodG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(QrNiv2, DmodG.MyPID_DB, [
  'SELECT Registro  ',
  'FROM ' + FSPEDEFD_Blcs,
  'WHERE Nivel=2 ',
  'AND Implementd <> 0 ',
  'AND Ativo=1',
  'AND ( ',
  '  Bloco = "C" ',
  '  OR ',
  '  Bloco = "D" ',
  ') ',
  '']);
  if QrNiv2.RecordCount > 0 then
  begin
    Result := 'AND (';
    QrNiv2.First;
    while not QrNiv2.Eof do
    begin
      if dmkPF.IndexOfArrStr(QrNiv2Registro.Value, Excecao) = -1 then
      begin
        if QrNiv2.RecNo = 1 then
        begin
          Result := Result + sLineBreak +
            '  (EFD_EXP_REG="' + QrNiv2Registro.Value + '")';
          _OR := 'OR';
        end else
          Result := Result + sLineBreak +
            '  ' + _OR + ' (EFD_EXP_REG="' + QrNiv2Registro.Value + '")';
      end;
      //
      QrNiv2.Next;
    end;
    Result := Result + ')';
  end;
end;

procedure TFmEfdPisCofinsExporta_v01_35.BtErrosClick(Sender: TObject);
begin
  DefineEmpresaEAnoMes();
  NaoTemErros(True);
end;

procedure TFmEfdPisCofinsExporta_v01_35.BtMesClick(Sender: TObject);
var
  Qry: TmySQLQuery;
  AnoMes: Integer;
  Mes: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(AnoMes) AnoMes ',
    'FROM ' + CO_PREFIX_TAB_ALL_SPED_EFD + 'e001 ',
    'WHERE Empresa=' + FEmpresa_Txt,
    '']);
    if Qry.RecordCount > 0 then
    begin
      AnoMes := Qry.FieldByName('AnoMes').AsInteger;
      if AnoMes > 200001 then
        EdMes.ValueVariant := Geral.AnoMesToData(AnoMes, 1);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmEfdPisCofinsExporta_v01_35.BtOKClick(Sender: TObject);
var
  TextoErr: String;
  //DataIni, DataFim: TDateTime;
  I, P, IND_MOV, IND_DAD, Registros, Emitente: Integer;
  //Gerar,
  TemMovim, TemErro, InfReg, GerouC100: Boolean;
  Noh: TTreeNode;
begin
  if FConfigurouRegistros = False then
    SbImplementadoClick(SbImplementado);
  //
  FLin0900 := 0;
  //
  MeErros.Lines.Add('Mudar campo DataFiscal para ide_dEmi ou ???');
  //FContaIND_MOV_1 := 0;
  //
  DefineEmpresaEAnoMes();
  //
  //FDtFiscal0053_Entr := 'DT_E_S';
  case DmSPED.QrParamsEmpSPED_EFD_DtaFiscalEntrada.Value of
    1: FDtFiscal0053_Entr := 'DT_DOC';
    2: FDtFiscal0053_Entr := 'DT_E_S';
    else FDtFiscal0053_Entr := 'DT_???';
  end;
  //
  //FDtFiscalFrete_Entr := 'DT_A_P';
  case DmSPED.QrParamsEmpSPED_EFD_DtaFiscalEntrada.Value of
    1: FDtFiscalFrete_Entr := 'DT_DOC';
    2: FDtFiscalFrete_Entr := 'DT_A_P';
    else FDtFiscalFrete_Entr := 'DT_???';
  end;
  //
  Dmod.QrAux.Close;
  // fim 2022-04-14
  FSelReg0400 := Selreg('0400');
  FSelReg0500 := Selreg('0500');
  FSelRegC100 := SelReg('C100');
  FSelRegC500 := SelReg('C500');
  //
  FSelRegD100 := SelReg('D100');
  FSelRegD195 := SelReg('D195');
  FSelRegD197 := SelReg('D197');
  FSelRegD500 := SelReg('D500');
  //
  FSelRegK210 := SelReg('K210');
  FSelRegK220 := SelReg('K220');
  FSelRegK230 := SelReg('K230');
  FSelRegK250 := SelReg('K250');
  FSelRegK290 := SelReg('K290');
  FSelRegK300 := SelReg('K300');
  //
  (* Desmarcar depois
  if not EfdPisCofins_PF_v01_35.LiberaAcaoVS_SPED(FImporExpor, FAnoMes_Int, FEmpresa_Int,
  TEstagioVS_SPED.evsspedExportaSPED) then
    Exit;
  *)
  // Temporario - implementar opcoes SPED da Filial
  VAR_INF_SPED_EFD_VAL_ZERO := True;
  VAR_INF_SPED_EFD_NOME_DESCR_COMPL := True;
  // FimTemporario
  MeGerado.Lines.Clear;
  MeErros.Lines.Clear;
  (* Parei Aqui! ver *)
  TextoErr := '';
  if (DmSPED.QrParamsEmpSPED_EFD_IND_PERFIL.Value = '?') then
    TextoErr := TextoErr + '- Perfil de apresenta��o do arquivo fiscal' + sLineBreak;
  if (DmSPED.QrParamsEmpSPED_EFD_IND_ATIV.Value = 9) then
    TextoErr := TextoErr + '- Indicador de tipo de atividade' + sLineBreak;
  if (DmSPED.QrParamsEmpSPED_EFD_DtaFiscalEntrada.Value < 1) then
    TextoErr := TextoErr + '- Data de Escritura��o de NFs de Entradas' + sLineBreak;
  if (DmSPED.QrParamsEmpSPED_EFD_DtaFiscalSaida.Value < 1) then
    TextoErr := TextoErr + '- Data de Escritura��o de NFs de Sa�das' + sLineBreak;
  if (DmSPED.QrParamsEmpSPED_EFD_ID_0150.Value < 1) then
    TextoErr := TextoErr + '- ID no arquivo exporta��o - Entidade' + sLineBreak;
  if (DmSPED.QrParamsEmpSPED_EFD_ID_0200.Value < 1) then
    TextoErr := TextoErr + '- ID no arquivo exporta��o - Produto' + sLineBreak;
  if TextoErr <> '' then
  begin
    TextoErr := 'Exporta��o abortada!' + sLineBreak +
    'Certifique-se que os itens abaixo est�o corretos: ' + sLineBreak +
    TextoErr;
    MyObjects.FIC(True, nil, TextoErr);
    Exit;
  end;
  //
  if not DmSPED.CorrigeDataFiscal(FDtFiscal0053_Entr, FEmpresa_txt,
  FIntervaloFiscal, LaAviso1, LaAviso2) then
    Exit;
  //
  if CkCorrigeCriadosAForca.Checked then
    if not DmSPED.CorrigeItensCraidosAForca(PB1, LaAviso1, LaAviso2) then
      Exit;
  DmSPED.CorrigeNotasFiscaiProprias(PB1,LaAviso1, LaAviso2);
  DmSPED.CorrigeNotasFiscaiDeTerceiros(LaAviso1, LaAviso2);
  //DmSPED.CorrigeItensDeMateriaPrima();
  //DmSPED.CorrigeUnidMedNfeItsI();
  (* Desmarcar depois
  AtualizaTabelasSPEDEFD();
  *)
  //
  //
  if not DmSPED.CorrigeCOD_SIT(FEmpresa_Int, FAnoMes_Int, FIntervaloFiscal,
  PB1, LaAviso1, LaAviso2, MeErros) then
    if (RGPreenche.ItemIndex = 2) then
    begin
      PageControl1.ActivePageIndex := 2;
      //Exit;
    end;
  //
  FLinArq := 0;
  FQTD_LIN_0400 := 0;
  FQTD_LIN_0500 := 0;
  FQTD_LIN_C170 := 0;
  //
  FQTD_LIN_C501 := 0;
  FQTD_LIN_C505 := 0;
  //
  FQTD_LIN_D501 := 0;
  FQTD_LIN_D505 := 0;
  //
  FQTD_LIN_D101 := 0;
  FQTD_LIN_D105 := 0;
  //
  FQTD_LIN_M205 := 0;
  FQTD_LIN_M210 := 0;
  FQTD_LIN_M605 := 0;
  FQTD_LIN_M610 := 0;
  //
  FQTD_LIN_0 := 0;
  FQTD_LIN_A := 0;
  FQTD_LIN_C := 0;
  FQTD_LIN_D := 0;
  FQTD_LIN_F := 0;
  FQTD_LIN_I := 0;
  FQTD_LIN_M := 0;
  FQTD_LIN_P := 0;
  FQTD_LIN_1 := 0;
  FQTD_LIN_9 := 0;
  FREG_BLC_Count := 0;
  SetLength(FREG_BLC_Fields, 0);
  SetLength(FREG_BLC_Linhas, 0);
      //
  Screen.Cursor := crHourGlass;
  FArqStr := TStringList.Create;
  FArqStr.Clear;
  try
    ReopenVersoes();
    FVersao := QrVersaoCodTxt.Value;
    if MyObjects.FIC(FVersao = '', nil, 'N�o foi poss�vel definir a vers�o do arquivo' +
    sLineBreak + 'AVISE A DERMATEK!') then
      Exit;
    //
    ReopenEmpresa();
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo dados duplic�veis');
    if not ExcluiPeriodoSelecionado() then
      Exit;
    //
    if not ReopenDocumentos() then
      Exit;
    //
    if not Define_REG() then
      Exit;

    //
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   0                                                          //
    ////////////////////////////////////////////////////////////////////////////
    if GeraRegistro_0000() then
    begin
      FGeraBloco := PodeGerarBloco('0');
      IND_MOV := dmkPF.EscolhaDe2Int(FGeraBloco, 0, 1);
      if GeraRegistro_0001(IND_MOV) then
      begin
        if FGeraBloco then
        begin
          //GeraRegistro_0002();
          //GeraRegistro_0005();
          GeraRegistro_0100();
          GeraRegistro_0110();
          GeraRegistro_0140();
          if (RGPreenche.ItemIndex = 2) then
          begin
            if GeraRegistro_0150() then
            begin
              // Falta fazer!
              //if ? then GeraRegistro_0175() then
            end;
            InfReg := UpperCase(DmSPED.QrParamsEmpSPED_EFD_IND_PERFIL.Value) <> 'C';
            if InfReg then
            begin
              GeraRegistro_0190();
              GeraRegistro_0200();
              if FSelReg0400 then
                GeraRegistro_0400();
              if FSelReg0500 then
                GeraRegistro_0500();
              //
              AdicionaBLC(FQTD_LIN_0400, '0400');
              AdicionaBLC(FQTD_LIN_0500, '0500');
            end;
          end;
        end;
      end;
      GeraRegistro_0900_Pre();
      GeraRegistro_0990();
    end;
    //
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   A                                                          //
    ////////////////////////////////////////////////////////////////////////////
    IND_MOV := 1; // N�o gera!
    if GeraRegistro_A001(IND_MOV) then
    begin
      // ...
      GeraRegistro_A990();
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   C                                                          //
    ////////////////////////////////////////////////////////////////////////////
    if FSelRegC100 or FSelRegC500 then
    begin
      FGeraBloco := (RGPreenche.ItemIndex = 2) and PodeGerarBloco('C');
      if FGeraBloco then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando para gerar o bloco "C"');
        // J� foi aberto!
        //ReopenCabA_C(True, 'C100');
        QrCabA_C.First;
        //
        //ReopenEFD_C500();
        ReopenEfdInnC500Cab();
        //Registros := QrCabA_C.RecordCount + QrEFD_C500.RecordCount;
        Registros := QrCabA_C.RecordCount + QrEfdInnC500Cab.RecordCount;
        IND_MOV := dmkPF.EscolhaDe2Int(Registros > 0, 0, 1);
      end else
        IND_MOV := 1;
    end else
      IND_MOV := 1;
    if GeraRegistro_C001(IND_MOV) then
    begin
      if FGeraBloco then
      begin
        if (RGPreenche.ItemIndex = 2) then
        begin
          PB1.Position := 0;
          PB1.Max := QrCabA_C.RecordCount;
          if FSelRegC100 then
          begin
            //
            GeraRegistro_C010();
            //
            QrCabA_C.First;
            //Geral.MB_Teste(QrCabA_C.SQL.Text);
            while not QrCabA_C.Eof do
            begin
              MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
              //
              // ini 2022-04-12
              //if GeraRegistro_C100() then
              case QrCabA_CFatID.Value of
                VAR_FATID_0001,
                VAR_FATID_0050: // emiss�es pr�prias importadas de xml emitido por outo sistema
                begin
                  Emitente  := QrCabA_CCodInfoEmit.Value;
                  GerouC100 := GeraRegistro_C100_Emit();
                end;
                VAR_FATID_0053:
                begin
                  Emitente  := QrEfdInnNFsCabTerceiro.Value;
                  GerouC100 := GeraRegistro_C100_Entr();
                  //GerouC100 := False;
                end;
                else
                begin
                  GerouC100 := False;
                  Geral.MB_Aviso('FatID ' + Geral.FF0(QrCabA_CFatID.Value) +
                  ' n�o implementado para gera��o do registro C100!');
                end;
              end;
              if GerouC100 then
              begin
                //
                // 2016-02-07 Nao entendi porque!!!
                (*if (Emitente = FEmpresa_int) then
                  InfReg := False
                else*)
                  InfReg := True;
                if UpperCase(DmSPED.QrParamsEmpSPED_EFD_IND_PERFIL.Value) = 'C' then
                   InfReg := False;
                if InfReg then
                begin
                  // Itens
                  case QrCabA_CFatID.Value of
                  VAR_FATID_0001,
                  VAR_FATID_0050: // emiss�es pr�prias importadas de xml emitido por outo sistema
                    begin
                      SPED_PF.ReopenNFeItens_EFD(QrI, QrCabA_CFatID.Value,
                        QrCabA_CFatNum.Value, QrCabA_CEmpresa.Value);
                      //
                      QrI.First;
                      while not QrI.Eof do
                      begin
                        GeraRegistro_C170_Emit();
                        //
                        QrI.Next;
                      end;
                    end;
                    VAR_FATID_0053:
                    begin
                      UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnNFsIts, Dmod.MyDB, [
                      'SELECT * ',
                      'FROM efdinnnfsits',
                      'WHERE Controle=' + Geral.FF0(QrEfdInnNFsCabControle.Value),
                      'ORDER BY Conta ',
                      '']);
                      QrEfdInnNFsIts.First;
                      while not QrEfdInnNFsIts.Eof do
                      begin
                        GeraRegistro_C170_Entr();
                        //
                        QrEfdInnNFsIts.Next;
                      end;
                    end;
                    else
                    begin
                      //GerouC170 := False;
                      Geral.MB_Aviso('FatID ' + Geral.FF0(QrCabA_CFatID.Value) +
                      ' n�o implementado para gera��o do registro C170!');
                    end;
                  end;
                end;
                // Fim itens
              end;
              QrCabA_C.Next;
            end;
          end;
          //
          // Energia El�trica, �gua, G�s
          if FSelRegC500 then
          begin
            QrEfdInnC500Cab.First;
            while not QrEfdInnC500Cab.Eof do
            begin
              GeraRegistro_C500();
              //
              ReopenEfdInnC500ItsPIS(QrEfdInnC500CabControle.Value);
              QrEfdInnC500ItsPIS.First;
              while not QrEfdInnC500ItsPIS.Eof do
              begin
                GeraRegistro_C501();
                //
                QrEfdInnC500ItsPIS.Next;
              end;
              //
              ReopenEfdInnC500ItsCOFINS(QrEfdInnC500CabControle.Value);
              QrEfdInnC500ItsCOFINS.First;
              while not QrEfdInnC500ItsCOFINS.Eof do
              begin
                GeraRegistro_C505();
                //
                QrEfdInnC500ItsCOFINS.Next;
              end;
              //
              QrEfdInnC500Cab.Next;
            end;
          end;
          if FSelRegC100 then
          begin
            AdicionaBLC(QrCabA_C.RecordCount, 'C100');
            AdicionaBLC(FQTD_LIN_C170, 'C170');
          end;
         if FSelRegC500 then
          begin
            AdicionaBLC(QrEfdInnC500Cab.RecordCount, 'C500');
            AdicionaBLC(FQTD_LIN_C501, 'C501');
            AdicionaBLC(FQTD_LIN_C505, 'C505');
          end;
          //
          PB1.Position := 0;
        end;
      end;
      GeraRegistro_C990();
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   D                                                          //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('D');
    //
    if FGeraBloco then
    begin

      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando para gerar o bloco "D"');
      //
      ReopenEfdInnD500Cab();
      //
      Registros := QrEfdInnCTsCab.RecordCount + QrEfdInnD500Cab.RecordCount;
      //
      if FSelRegD100 or FSelRegD500 then
        IND_MOV := dmkPF.EscolhaDe2Int(Registros > 0, 0, 1)
      else
        IND_MOV := 1;
    end else
      IND_MOV := 1;
    //
    if GeraRegistro_D001(IND_MOV) then
    begin
      if FGeraBloco and (Registros > 0) then
      begin
        if (RGPreenche.ItemIndex = 2) then
        begin
          if FSelRegD100 then
          begin
            GeraRegistro_D010();
            GeraRegistro_D100();
            //
            AdicionaBLC(QrEfdInnCTsCab.RecordCount, 'D100');
            AdicionaBLC(FQTD_LIN_D101, 'D101');
            AdicionaBLC(FQTD_LIN_D105, 'D105');
          end;
          //
          if FSelRegD500 then
          begin
            QrEfdInnD500Cab.First;
            while not QrEfdInnD500Cab.Eof do
            begin
              GeraRegistro_D500();
              ReopenEfdInnD500ItsPIS(QrEfdInnD500CabControle.Value);
              QrEfdInnD500ItsPIS.First;
              while not QrEfdInnD500ItsPIS.Eof do
              begin
                GeraRegistro_D501();
                QrEfdInnD500ItsPIS.Next;
              end;
              //
              ReopenEfdInnD500ItsCOFINS(QrEfdInnD500CabControle.Value);
              QrEfdInnD500ItsCOFINS.First;
              while not QrEfdInnD500ItsCOFINS.Eof do
              begin
                GeraRegistro_D505();
                QrEfdInnD500ItsCOFINS.Next;
              end;
              //
              QrEfdInnD500Cab.Next;
            end;
            AdicionaBLC(QrEfdInnD500Cab.RecordCount, 'D500');
            AdicionaBLC(FQTD_LIN_D501, 'D501');
            AdicionaBLC(FQTD_LIN_D505, 'D505');
          end;
        end;
      end;
      GeraRegistro_D990();
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   F                                                          //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('F');
    if FGeraBloco then
    begin
      TemMovim   := False;
      IND_MOV := dmkPF.EscolhaDe2Int(TemMovim, 0, 1);
      GeraRegistro_F001(IND_MOV);
      GeraRegistro_F990();
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   I                                                          //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('I');
    if FGeraBloco then
    begin
      TemMovim   := False;
      IND_MOV := dmkPF.EscolhaDe2Int(TemMovim, 0, 1);
      GeraRegistro_I001(IND_MOV);
      GeraRegistro_I990();
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   M Teste!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('M');
    if FGeraBloco then
    begin
      TemMovim   := True; // ver!
      IND_MOV := dmkPF.EscolhaDe2Int(TemMovim, 0, 1);
      if GeraRegistro_M001(IND_MOV) then
      begin
        if TemMovim then
        begin
          ReopenSumM210();
          GeraRegistro_M200();
          GeraRegistro_M205();
          GeraRegistro_M210();
          //
          ReopenSumM610();
          GeraRegistro_M600();
          GeraRegistro_M605();
          GeraRegistro_M610();
          //
          AdicionaBLC(FQTD_LIN_M205, 'M205');
          AdicionaBLC(FQTD_LIN_M210, 'M210');
          AdicionaBLC(FQTD_LIN_M210, 'M605');
          AdicionaBLC(FQTD_LIN_M210, 'M610');
        end;
        GeraRegistro_M990();
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   P                                                          //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('P');
    if FGeraBloco then
    begin
      TemMovim   := False;
      IND_MOV := dmkPF.EscolhaDe2Int(TemMovim, 0, 1);
      GeraRegistro_P001(IND_MOV);
      GeraRegistro_P990();
    end;
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   1                                                          //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('1');
    if FGeraBloco then
    begin
(*
      UnDmkDAC_PF.AbreMySQLQuery0(QrEFD_1010, Dmod.MyDB, [
      'SELECT * FROM ' + CO_PREFIX_TAB_ALL_SPED_EFD + '1010 ',
      'WHERE ImporExpor=3',
      'AND AnoMes=' + FAnoMes_Txt,
      'AND Empresa=' + FEmpresa_TXT,
      '']);
      TemMovim   := (QrEFD_1010.RecordCount > 0);
      //TemMovim   := True;
      if TemMovim and (QrEFD_1010.RecordCount = 0) then
      begin
        Grava_SpedEfdPisCofinsErrs(efdexerRegFilhObrig, 'REG 1010');
        Geral.MB_Aviso('O registro de obrigatoriedade de refgistros do bloco 1 n�o foi cadastrado!' +
        sLineBreak + 'Cadastre na janela de apura��o de ICMS / IPI - Aba "1-Outros"!');
      end;
      IND_MOV    := dmkPF.EscolhaDe2Int(TemMovim, 0, 1);
*)
      IND_MOV    := 1; // Parei Aqui! Ver o que fazer!
      GeraRegistro_1001(IND_MOV);
      begin
        if TemMovim then
        begin
          GeraRegistro_1010();
        end;
      end;
    end;
    GeraRegistro_1990();
    ////////////////////////////////////////////////////////////////////////////
    // B L O C O   9                                                          //
    ////////////////////////////////////////////////////////////////////////////
    FGeraBloco := PodeGerarBloco('9');
    IND_MOV := dmkPF.EscolhaDe2Int(FGeraBloco, 0, 1);
    if GeraRegistro_9001(IND_MOV) then
    begin
      if FGeraBloco then
      begin
        GeraRegistro_9900();
        GeraRegistro_9990();
      end;
      GeraRegistro_9999();
    end;
    PB1.Position := 0;
    //
    if NaoTemErros(False) then ; // deixar gravar pois alguns erros podem ser aceitos
    begin
      if SalvaArquivo() then
      begin
        EfdPisCofins_PF_v01_35.EncerraMesSPED(FImporExpor, FAnoMes_Int, FEmpresa_Int, 'EnvioEFD');
        MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
      end;
    end;
    //
(*
    if FContaIND_MOV_1 > 0 then
      Geral.MB_Info(Geral.FF0(FContaIND_MOV_1) +
        ' itens de NF de sa�da sem movimenta��o de estoque!');
*)
  finally
    if LaAviso1.Caption <> '' then
    begin
      P := pos('...', LaAviso1.Caption) + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'EXPORTA��O ABORTADA EM: ' +
        Copy(LaAviso1.Caption, P));
    end
    else
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Exporta��o finalizada!');
    //
    Screen.Cursor := crDefault;
    if FArqStr <> nil then
      FArqStr.Free;
    //
    if MeErros.Lines.Count > 0 then
      PageControl1.ActivePageIndex := 2;
  end;
end;

procedure TFmEfdPisCofinsExporta_v01_35.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmEfdPisCofinsExporta_v01_35.CalculaCampoEFD_M210_03(): Double;
begin
  // SPED_COD_INC_TRIB
  // 1 � Escritura��o de opera��es com incid�ncia exclusivamente no regime n�o-cumulativo;
  // 2 � Escritura��o de opera��es com incid�ncia exclusivamente no regime cumulativo;
  // 3 � Escritura��o de opera��es com incid�ncia nos regimes n�o-cumulativo e cumulativo.

  //COD_INC_TRIB = DModG.QrParamsSPEDSPED_COD_INC_TRIB.Value

  //Parei aqui!

end;

function TFmEfdPisCofinsExporta_v01_35.CampoNaoConfere(): Boolean;
begin
  Result := False;
  if QrCamposNumero.Value <> QrCampos.RecNo then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
    'Erro'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'LinArq', 'REG', 'Campo'], [
    Integer(efdexerErrSeqGerLin)], [
    FImporExpor, FAnoMes_Int, FEmpresa_Int,
    FLinArq, FRegistro, QrCamposCampo.Value], True);
    //
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.CampoNaoEhNumero(Valor: String): Boolean;
begin
  Result := Length(Valor) <> Length(Geral.SoNumero_TT(Valor));
end;

function TFmEfdPisCofinsExporta_v01_35.CampoObrigatorio(LODC: String): Boolean;
begin
  Result :=
  (Uppercase(LODC) = 'O')
  or
  (Uppercase(LODC) = 'S');
end;

function TFmEfdPisCofinsExporta_v01_35.CampoObrigatorio_6(LODC: String; Inteiro64: Int64;
PermiteZero: Boolean): Boolean;
begin
  Result := CampoObrigatorio(LODC) and (Inteiro64 = 0) and (PermiteZero = False);
end;

function TFmEfdPisCofinsExporta_v01_35.CampoObrigatorio_d(LODC: String; Data: TDatetime;
  PermiteZero: Boolean): Boolean;
begin
  Result := CampoObrigatorio(LODC) and (Data < 2) and (PermiteZero = False);
end;

function TFmEfdPisCofinsExporta_v01_35.CampoObrigatorio_f(LODC: String; Flutuante: Double;
  PermiteZero: Boolean): Boolean;
begin
  Result := CampoObrigatorio(LODC) and (Flutuante = 0) and (PermiteZero = False);
end;

function TFmEfdPisCofinsExporta_v01_35.CampoObrigatorio_i(LODC: String; Inteiro: Integer; PermiteZero: Boolean): Boolean;
begin
  Result := CampoObrigatorio(LODC) and (Inteiro = 0) and (PermiteZero = False);
end;

function TFmEfdPisCofinsExporta_v01_35.CampoObrigatorio_x(LODC: String; Valor: String): Boolean;
begin
  Result := CampoObrigatorio(LODC) and (Length(Valor) = 0);
end;

procedure TFmEfdPisCofinsExporta_v01_35.ConfiguraTreeView2(Ativos, Cinzas: array of String);
  function CriaNoh(const Noh: TTreeNode; const nN: String; const cN: Integer;
  Ativar: Integer): TTreeNode;
  begin
    if Noh = nil then
      Result := TvBlocos.Items.Add(Noh, nN)
    else
      Result := TvBlocos.Items.AddChild(Noh, nN);
    Result.ImageIndex := cN;
    //Result.StateIndex := QrBlcsAtivo.Value + Integer(cCBNot);
    Result.StateIndex := Ativar;
  end;
const
  MaxNiv = 255;
var
  Idxs: array[0..MaxNiv] of Integer;
  Nohs: array[0..MaxNiv] of TTreeNode;
  I, J, K: Integer;
  sRegistros: String;
begin
  //try
  for I := 0 to MaxNiv do
  begin
    Idxs[I] := 0;
    Nohs[I] := nil;
  end;
  //
  QrBlcs.Close;
  QrBlcs.Database := DModG.MyPID_DB;
  //
  FSPEDEFD_Blcs := GradeCriar.RecriaTempTableNovo(ntrttSPEDEFD_BLCS, DModG.QrUpdPID1, False);
  // Blocos
  UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
    'INSERT INTO ' + FSPEDEFD_Blcs,
    'SELECT DISTINCT Bloco, Bloco Registro, 0 Nivel,',
    'Bloco RegisPai,  0 OcorAciNiv, 1 OcorEstNiv,',
    'Implementd, Ordem, 0 Ativo, 2 StateIndex ',
    'FROM ' + VAR_AllID_DB_NOME + '.spedefdpiscofinsblcs ',
    'WHERE Implementd <> 0 ',
    '']);
  // Registros
  UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
    'INSERT INTO ' + FSPEDEFD_Blcs,
    'SELECT Bloco, Registro, Nivel,',
    'RegisPai,  OcorAciNiv, OcorEstNiv,',
    'Implementd, Ordem, 0 Ativo, 2 StateIndex ',
    'FROM ' + VAR_AllID_DB_NOME + '.spedefdpiscofinsblcs ',
    'WHERE Nivel > 0 ',
    'AND Implementd <> 0 ',
    '']);
  //
  sRegistros := MyObjects.CordaDeSQL_IN(Ativos);
  if sRegistros <> '' then
  begin
    UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
    'UPDATE ' + FSPEDEFD_Blcs,
    'SET Ativo=1 ',
    'WHERE Registro IN (' + sRegistros + ') ',
    '']);
  end;
  TVBlocos.StateImages := FmMyGlyfs.ImgChkTV;
  TVBlocos.Items.Clear;
  // Desmarcar!
  TVBlocos.FullExpand();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrBlcs, DModG.MyPID_DB, [
  'SELECT * FROM ' + FSPEDEFD_Blcs,
  'WHERE Implementd <> 0',
  'ORDER BY Ordem, Registro',
  '']);
  while not QrBlcs.Eof do
  begin
    //Ativa := False;
    K := 2;
    I := MaxNiv - QrBlcsNivel.Value;
    // Cinzas
    for J := Low(Cinzas) to High(Cinzas) do
    begin
      if Cinzas[J] = QrBlcsRegistro.Value then
      begin
        //Ativa := True;
        K := 1;
        Break;
      end;
    end;
    // Ativos
    for J := Low(Ativos) to High(Ativos) do
    begin
      if Ativos[J] = QrBlcsRegistro.Value then
      begin
        //Ativa := True;
        K := 3;
        Break;
      end;
    end;
    Nohs[I] := CriaNoh(Nohs[I+1], QrBlcsRegistro.Value, 0, K);
    TVBlocos.FullExpand();
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'UPDATE ' + FSPEDEFD_Blcs,
    'SET StateIndex=' + Geral.FF0(K),
    'WHERE Bloco="' + QrBlcsBloco.Value + '"',
    'AND Registro="' + QrBlcsRegistro.Value + '"',
    '']);
    QrBlcs.Next;
  end;
  //
end;

function TFmEfdPisCofinsExporta_v01_35.CordaDeQuery_Sai100: String;
var
  I: Integer;
begin
  if QrCabA_C.RecordCount > 0 then
  begin
    Result := '';
    try
      QrCabA_C.DisableControls;
      QrCabA_C.First;
      while not QrCabA_C.Eof do
      begin
        if (QrCabA_CStatus.Value = 100) and (QrCabA_CFatID.Value in ([
        VAR_FATID_0001, VAR_FATID_0050])) then
        begin
          if Result <> '' then
            Result := Result + ', ';
          Result := Result + Geral.FF0(QrCabA_CFatNum.Value);
        end;
        //
        QrCabA_C.Next;
      end;
    finally
      QrCabA_C.EnableControls;
    end;
  end;
  if Result = '' then
    Result := '-999999999';
end;


procedure TFmEfdPisCofinsExporta_v01_35.DBGrid1DblClick(Sender: TObject);
begin
  if (QrErrMod.State <> dsInactive) and (QrErrMod.RecordCount > 0) then
    Grade_Jan.MostraFormEntrada(QrErrModFatID.Value, QrErrModIDCtrl.Value);
end;

procedure TFmEfdPisCofinsExporta_v01_35.DefineEmpresaEAnoMes();
  function ValidaPeriodoInteiroDeUmMes(const DtI, DtF:
  TDateTime): Boolean;
  var
    //AnoIa, MesIa, DiaIa,
    AnoIi, MesIi, DiaIi,
    AnoFf, AnoFd, MesFf, MesFd, DiaFf, DiaFd: Word;
  begin
    Result := False;
    FAnoMes_Int := 0;
    DecodeDate(DtI, AnoIi, MesIi, DiaIi);
    //DecodeDate(DtI-1, AnoIi, MesIi, DiaIi);
    DecodeDate(DtF, AnoFf, MesFf, DiaFf);
    DecodeDate(DtF + 1, AnoFd, MesFd, DiaFd);
    FAnoMes_Int := AnoIi * 100 + MesIi;
    FAnoMes_Txt := Geral.FF0(FAnoMes_Int);

    FDataIni_Txt := Geral.FDT(DtI, 1);
    FDataFim_Txt := Geral.FDT(DtF, 1);
    FIntervaloFiscal := ' "' + FDataIni_Txt + '" AND "' + FDataFim_Txt + '"';
    //
    if DiaIi <> 1 then
      Geral.MB_Aviso(
      'Exporta��o cancelada! Dia inicial deve ser o primeiro dia (dia 1) do m�s,'
      + sLineBreak +
      'mesmo que as atividades tenham se iniciado ap�s o dia primeiro!')
    else if DiaFd <> 1 then
      Geral.MB_Aviso(
      'Exporta��o cancelada! Dia final deve ser o �ltimo dia do m�s,'
      + sLineBreak +
      'mesmo que as atividades tenham sido finalizados antes do �ltimo dia do m�s!')
    else if MesIi <> MesFf then
      Geral.MB_Aviso(
      'Exporta��o cancelada! O per�odo deve ser de um �nico m�s!' + sLineBreak +
      'Data inicial selecionada:  ' + Geral.FDT(DtI, 3) + sLineBreak +
      'Data Final selecionada:   ' + Geral.FDT(DtF, 3))
    else if FAnoMes_Int < 200001 then
      Geral.MB_Aviso(
      'Exporta��o cancelada! O per�odo deve ser de um ano ap�s 2000!' + sLineBreak +
      'Data inicial selecionada:  ' + Geral.FDT(DtI, 3) + sLineBreak +
      'Data Final selecionada:   ' + Geral.FDT(DtF, 3))
    else
      Result := True;
  end;
begin
  FEmpresa_Int := 0;
  FEmpresa_Txt := '0';
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Empresa n�o definida!') then
    Exit;
  FEmpresa_Int := DModG.QrEmpresasCodigo.Value;
  FEmpresa_Txt := Geral.FF0(FEmpresa_Int);
(*
  DmSPED.QrParamsEmp.Close;
  DmSPED.QrParamsEmp.Params[0].AsInteger := FEmpresa_Int;
  UnDmkDAC_PF.AbreQuery(DmSPED.QrParamsEmp, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
*)
  DmSPED.ReopenParamsEmp(FEmpresa_Txt);
  //
  DModG.ReopenParamsSPED(FEmpresa_Int);
  if not ValidaPeriodoInteiroDeUmMes(FDataIni_Dta, FDataFim_Dta) then
    Exit;
end;

{
function TFmEfdPisCofinsExporta_v01_35.DefineIND_MOV(FisRegCad, ide_finNFe:
  Integer; prod_cProd: String): Integer;
begin
  Result := 0; // Movimenta
  if ide_finNFe in ([
    2, // NF-e complementar
    3// NF-e de Ajuste
  ]) then
    Result := 1 // n�o movimenta
  else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrFisRegMvt, Dmod.MyDB, [
    'SELECT frm.TipoCalc ',
    'FROM fisregmvt frm ',
    'WHERE frm.Codigo=' + Geral.FF0(FisRegCad),
    'AND frm.Empresa=' + Geral.FF0(FEmpresa_Int),
    '']);
    //
    if QrFisRegMvt.RecordCount > 0 then
    begin
      if QrFisRegMvtTipoCalc.Value = 0 then // Movimento Nulo
        Result := 1; // sem movimento
    end;
    //
    if Result = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrIndMov, Dmod.MyDB, [
      'SELECT  ',
      '(IF((gg1.Nivel2 <> 0) and (gg2.Tipo_Item in (0,1,2,3,4,5,6,7,8,10)), 0,  ',
      '  IF(pgt.TipPrd=3, 1,  ',
      '    IF(pgt.Tipo_Item in (0,1,2,3,4,5,6,7,8,10), 0,  ',
      '      1) ',
      '  ) ',
      ') + 0.000) IND_MOV ',
      'FROM gragrux ggx ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN gragru2 gg2 ON gg1.Nivel2=gg2.Nivel2 ',
      'LEFT JOIN prdgruptip pgt ON gg1.PrdGrupTip=pgt.Codigo ',
      'WHERE ggx.Controle=' + prod_cProd,
      '']);
      Result := Trunc(QrIndMovIND_MOV.Value);
    end;
  end;
end;
}

function TFmEfdPisCofinsExporta_v01_35.Define_REG(): Boolean;
var
  Modelo: Variant;
begin
  //ReopenCabA_C(False, Null);
  QrCabA_C.First;
  PB1.Position := 0;
  PB1.Max := QrCabA_C.RecordCount;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Definindo blocos dos modelos');
  while not QrCabA_C.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    Modelo :=  SPED_Geral.DefineREG_de_Modelo_de_NF(QrCabA_Cide_Mod.Value,
      QrCabA_CCOD_MOD.Value);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
    'EFD_EXP_REG'], [
    'FatID', 'FatNum', 'Empresa'], [
    Modelo], [
    QrCabA_CFatID.Value, QrCabA_CFatNum.Value, QrCabA_CEmpresa.Value], True);
    //
    QrCabA_C.Next;
  end;
  QrCabA_C.First;
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrMod, Dmod.MyDB, [
  'SELECT nfa.DataFiscal, nfa.FatID, nfa.FatNum, nfa.Empresa,',
  'nfa.ide_mod, nfa.ide_serie, nfa.ide_nNF, nfa.ide_dEmi, nfa.ICMSTot_vNF,',
  'IF(nfa.CodInfoEmit=' + FEmpresa_Txt + ', nfa.CodInfoDest, nfa.CodInfoEmit) Terceiro,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, nfa.IDCtrl',
  'FROM nfecaba nfa',
  'LEFT JOIN entidades ent ON ent.Codigo=',
  '     IF(CodInfoEmit=' + FEmpresa_Txt + ', CodInfoDest, CodInfoEmit)',
  'WHERE ide_tpAmb<>2',
  'AND Status IN (100,101,102,110,301)',
  'AND DataFiscal BETWEEN ' + FIntervaloFiscal,
  'AND EFD_EXP_REG=""',
  'ORDER BY DataFiscal, ide_dEmi, NO_TERCEIRO',
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrMod, Dmod.MyDB, [
  'SELECT nfea.DataFiscal, nfea.FatID, nfea.FatNum, nfea.Empresa,',
  'nfea.ide_mod, nfea.ide_serie, nfea.ide_nNF, nfea.ide_dEmi, nfea.ICMSTot_vNF,',
  'IF(nfea.CodInfoEmit=' + FEmpresa_Txt + ', nfea.CodInfoDest, nfea.CodInfoEmit) Terceiro,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, nfea.IDCtrl',
  'FROM nfecaba nfea',
  DmSPED.SQL_LEFT_JOIN_NFe_E_INN(),
  'LEFT JOIN entidades ent ON ent.Codigo=',
  '     IF(CodInfoEmit=' + FEmpresa_Txt + ', CodInfoDest, CodInfoEmit)',
  'WHERE ide_tpAmb<>2',
  'AND nfea.Status IN (100,101,102,110,301)',
  DmSPED.SQL_PeriodoFiscalNFe_e_INN(FIntervaloFiscal),
  'AND nfea.EFD_EXP_REG=""',
  'ORDER BY DataFiscal, ide_dEmi, NO_TERCEIRO',
  '']);

  Result := QrErrMod.RecordCount = 0;
  if not Result then
  begin
    PageControl1.ActivePageIndex := 3;
    Geral.MB_Aviso('Exporta��o abortada!' + sLineBreak + sLineBreak +
    'Existem ' + Geral.FF0(QrErrMod.RecordCount) +
    ' notas fiscais sem o modelo fiscal definido!' + sLineBreak + sLineBreak +
    'O modelo fiscal � fundamental para criar os blocos de exporta��o!');
  end;
end;

procedure TFmEfdPisCofinsExporta_v01_35.EdEmpresaChange(Sender: TObject);
begin
  HabilitaBotaoOK();
  DefineEmpresaEAnoMes();
end;

procedure TFmEfdPisCofinsExporta_v01_35.EdIND_SIT_ESPChange(Sender: TObject);
var
  IND_SIT_ESP: Integer;
  s: String;
begin
  IND_SIT_ESP := EdIND_SIT_ESP.ValueVariant;
  case IND_SIT_ESP of
    -1: s := '';
    0: s := 'Abertura';
    1: s := 'Cis�o';
    2: s := 'Fus�o';
    3: s := 'Incorpora��o';
    4: s := 'Encerramento';
    else s := '? ? ? ? ? ?'
  end;
  EdIND_SIT_ESP_TXT.Text := s;
end;

procedure TFmEfdPisCofinsExporta_v01_35.EdMesChange(Sender: TObject);
begin
  RedefinePeriodos();
end;

procedure TFmEfdPisCofinsExporta_v01_35.EdMesRedefinido(Sender: TObject);
begin
  RedefinePeriodos();
end;

function TFmEfdPisCofinsExporta_v01_35.ExcluiPeriodoSelecionado(): Boolean;
var
  //IE_TXT: String;
  I: Integer;
begin
  Result := False;
  //IE_TXT := Geral.FF0(FImporExpor);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT AnoMes ',
  'FROM spedefdpiscofinserrs ',
  'WHERE ImporExpor=' + FIE_TXT,
  'AND Empresa=' + FEmpresa_Txt,
  'AND AnoMes=' + FAnoMes_Txt,
  ' ',
  'UNION ',
  ' ',
  'SELECT AnoMes ',
  'FROM spedefdpiscofins0000 ',
  'WHERE ImporExpor=' + FIE_TXT,
  'AND Empresa=' + FEmpresa_Txt,
  'AND AnoMes=' + FAnoMes_Txt,
  ' ',
  'UNION ',
  ' ',
  'SELECT AnoMes ',
  'FROM spedefdpiscofins0001 ',
  'WHERE ImporExpor=' + FIE_TXT,
  'AND Empresa=' + FEmpresa_Txt,
  'AND AnoMes=' + FAnoMes_Txt,
  ' ',
  'UNION ',
  ' ',
  'SELECT AnoMes ',
  'FROM spedefdpiscofinsc100 ',
  'WHERE ImporExpor=' + FIE_TXT,
  'AND Empresa=' + FEmpresa_Txt,
  'AND AnoMes=' + FAnoMes_Txt,
  '']);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SHOW TABLES ',
    'FROM ' + TMeuDB,
    'LIKE "spedefdpiscofins%" ',
    '']);
    if Dmod.QrAux.RecordCount - FNot_SPED_EFD_PC <> FQtd_SPED_EFD_PC then
    begin
      Geral.MB_Aviso(
      'Importa��o cancelada! Quantidade de tabelas desatualizadas!' + sLineBreak +
      'Tot - Nao - Sim' + sLineBreak +
      '===============' + sLineBreak +
      Geral.FF0(Dmod.QrAux.RecordCount) + ' - ' +
      Geral.FF0(FNot_SPED_EFD_PC) + ' <> ' +
      Geral.FF0(FQtd_SPED_EFD_PC) + ' !!! ' +
      'AVISE A DERMATEK!');
    end else begin
      (* Desmarcar depois!
      if Geral.MB_Pergunta(
      'J� existem dados para este per�odo!' + sLineBreak +
      'Os dados j� existentes ser�o exclu�dos! Deseja continuar assim mesmo?') = ID_YES then
      *)
      begin
        Screen.Cursor := crHourGlass;
        try
          for I := 0 to High(FTbs_SPED_EFD_PC) (*- 1*) do
          begin
            try
              Dmod.QrUpd.SQL.Clear;
              Dmod.QrUpd.SQL.Add('DELETE FROM ' + FTbs_SPED_EFD_PC[I]);
              Dmod.QrUpd.SQL.Add('WHERE ImporExpor=' + FIE_TXT);
              Dmod.QrUpd.SQL.Add('AND Empresa=' + FEmpresa_Txt);
              Dmod.QrUpd.SQL.Add('AND AnoMes=' + FAnoMes_Txt);
              Dmod.QrUpd.ExecSQL;
            except
              on E: Exception do
                Geral.MB_Erro(Dmod.QrUpd.SQL.Text + sLineBreak + E.Message)
            end;
          end;
          Result := True;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
  end else
    Result := True;
end;

procedure TFmEfdPisCofinsExporta_v01_35.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEfdPisCofinsExporta_v01_35.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType       := stLok;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  FConfigurouRegistros := False;
  FVersao := '';
  FEmpresa_Int := 0;
  FEmpresa_Txt := '0';
  FAnoMes_Int := 0;
  FAnoMes_Txt := '';
  FDataIni_Dta := 0;
  FDataFim_Dta := 0;
  FDataIni_Txt := '';
  FDataFim_Txt := '';
  FIntervaloFiscal := '';
  EdMes.ValueVariant := Date - 21;
  //
  ConfiguraTreeView2([], []);
  //
  PageControl1.ActivePageIndex := 0;
  QrSelEnt.Close;
  QrSelEnt.Database := DModG.MyPID_DB;
end;

procedure TFmEfdPisCofinsExporta_v01_35.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEfdPisCofinsExporta_v01_35.frxSEII_EstqGetValue(
  const VarName: string; var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_SPED' then
    Value := EdMes.Text
  else
  if VarName ='VARF_DATA_IMP' then
    Value := Now()
  else
end;

function TFmEfdPisCofinsExporta_v01_35.GeraCOD_OBS_CAT66SP2018(
  Seq: Integer): String;
begin
  Result := 'QT' + Geral.FF0(Seq);
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0000(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_INI, DT_FIN: TDateTime;
  TIPO_ESCRIT, CodMunici: Integer;
  IND_SIT_ESP, NUM_REC_ANTERIOR, CNPJ_CPF: String;
  CNPJ, CPF: Int64;
begin
  Result := False;
  FBloco := '0';
  FRegistro := '0000';
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  TIPO_ESCRIT := RGTIPO_ESCRIT.ItemIndex;
  NUM_REC_ANTERIOR := EdNUM_REC_ANTERIOR.Text;
  case TIPO_ESCRIT of
    0:
    begin
      if MyObjects.FIC(NUM_REC_ANTERIOR <> EmptyStr, EdNUM_REC_ANTERIOR,
      'Informe o "N�mero do Recibo da Escritura��o anterior a ser retificada" somente quando o "Tipo de Escritura��o" for "1 - Retificadora"')
      then exit;
    end;
    1:
    begin
      if MyObjects.FIC(NUM_REC_ANTERIOR = EmptyStr, EdNUM_REC_ANTERIOR,
      'Informe o "N�mero do Recibo da Escritura��o anterior a ser retificada" com 41 caracteres!')
      then exit;
    end;
  end;
  //
  if EdIND_SIT_ESP.ValueVariant = -1 then
    IND_SIT_ESP := ''
  else
    IND_SIT_ESP := EdIND_SIT_ESP.ValueVariant;
  //
  DT_INI := Trunc(TPDataIni.Date);
  DT_FIN := Trunc(TPDataFim.Date);
  //
  CNPJ_CPF := Geral.SoNumero_TT(QrEmpresaCNPJ_CPF.Value);
  case Length(CNPJ_CPF) of
    11:
    begin
      CPF := Geral.I64(CNPJ_CPF);
      CNPJ := 0;
      CodMunici := QrEmpresaPCodMunici.Value;
    end;
    14:
    begin
      CNPJ := Geral.I64(CNPJ_CPF);
      CPF := 0;
      CodMunici := QrEmpresaECodMunici.Value;
    end;
    else
    begin
      MensagemDocumentoInvalido(QrEmpresaCNPJ_CPF.Value);
      Exit;
    end;
  end;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_n(ITO, 'COD_VER', FVersao) then
      (*03*) if not AC_i(ITO, 'TIPO_ESCRIT', TIPO_ESCRIT, True, True) then
      (*04*) if not AC_n(ITO, 'IND_SIT_ESP', IND_SIT_ESP) then
      (*05*) if not AC_x(ITO, 'NUM_REC_ANTERIOR', NUM_REC_ANTERIOR) then
      (*06*) if not AC_d(ITO, 'DT_INI', DT_INI, False, False, 23) then
      (*07*) if not AC_d(ITO, 'DT_FIN', DT_FIN, False, False, 23) then
      (*08*) if not AC_x(ITO, 'NOME', Copy(QrEmpresaNOME_ENT.Value, 1, 100)) then
      (*09*) if not AC_6(ITO, 'CNPJ', CNPJ, False, False) then
      (*10*) if not AC_x(ITO, 'UF', QrEmpresaNOMEUF.Value) then
      (*11*) if not AC_i(ITO, 'COD_MUN', CodMunici, False, False) then
      (*12*) if not AC_x(ITO, 'SUFRAMA', QrEmpresaSUFRAMA.Value) then
      (*13*) if not AC_n(ITO, 'IND_NAT_PJ', DmSPED.QrParamsEmpSPED_EFD_IND_NAT_PJ.Value) then
      (*14*) if not AC_i(ITO, 'IND_ATIV', DmSPED.QrParamsEmpSPED_EFD_IND_ATIV.Value, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    if CodMunici = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCodMunIndef)(*FNumErro[17]*)], [
      FImporExpor, FAnoMes_Int,
      FEmpresa_Int, FLinArq, FRegistro, 'COD_MUN'], True);
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_0);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := '0';
  FRegistro := '0001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_0);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0100(): Boolean;
const
  ITO = itoNaoAplicavel;
var
 CPF, CNPJ: Int64;
 CEP, COD_MUN, Numero_i: Integer;
 LOGR_RUA, NUM, COMPL, BAIRRO, FONE, FAX, EMAIL, Numero_x: String;
begin
  FBloco := '0';
  FRegistro := '0100';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  CPF := Geral.I64(Geral.SoNumero_TT(DmSPED.QrParamsEmpCPF_CTD.Value));
  CNPJ := Geral.I64(Geral.SoNumero_TT(DmSPED.QrParamsEmpCNPJ_CTB.Value));
  if DmSPED.QrParamsEmpSPED_EFD_EnderContab.Value = 0 then
  begin
    // Ningu�m
    Geral.MB_Aviso(
    'Aten��o! ' + sLineBreak +
    'N�o est� definido se o endere�o a ser informado no registro 0100 ser� do contador ou do escrit�rio de contabilidade!'
    + sLinebreak + 'Nenhum ser� considerado!');
    //
    CEP := 0;
    LOGR_RUA := '';
    NUM := '';
    COMPL := '';
    BAIRRO := '';
    FONE := '';
    FAX := '';
    EMAIL := '';
    COD_MUN := 0;
  end else
  begin
    // 1: do contador.
    // 2: do escrit�rio
    if DmSPED.QrParamsEmpSPED_EFD_EnderContab.Value = 1 then
    begin
      ReopenEnder(DmSPED.QrParamsEmpSPED_EFD_CadContador.Value);
      //
      Numero_i := QrEnderPNumero.Value;
      CEP := QrEnderPCEP.Value;
      COD_MUN := QrEnderPCodMunici.Value;
    end else
    begin
      ReopenEnder(DmSPED.QrParamsEmpSPED_EFD_EscriContab.Value);
      //
      Numero_i := QrEnderENumero.Value;
      CEP := QrEnderECEP.Value;
      COD_MUN := QrEnderPCodMunici.Value;
    end;
    LOGR_RUA := QrEnderNOMELOGRAD.Value;
    if Trim(LOGR_RUA) <> '' then
      LOGR_RUA := LOGR_RUA + ' ';
    LOGR_RUA := LOGR_RUA + QrEnderRua.Value;
    if Numero_i = 0 then
      NUMERO_x := 'S/N'
    else
      NUMERO_x := Geral.FF0(Numero_i);
    Fone := Geral.SoNumero_TT(QrEnderTE1.Value);
    COMPL := QrEnderCOMPL.Value;
    BAIRRO := QrEnderBAIRRO.Value;
    Fone := Geral.SoNumero_TT(QrEmpresaTE1.Value);
    while Length(Fone) > 10 do
      Fone := Copy(Fone, 2);
    Fax := Geral.SoNumero_TT(QrEmpresaFax.Value);
    while Length(Fax) > 10 do
      Fax := Copy(Fax, 2);
    //
    EMAIL := QrEnderEMAIL.Value;
  end;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'NOME', DmSPED.QrParamsEmpNO_CTD.Value) then
      (*03*) if not AC_6(ITO, 'CPF', CPF, False, False) then
      (*04*) if not AC_x(ITO, 'CRC', DmSPED.QrParamsEmpSPED_EFD_CRCContador.Value) then
      (*05*) if not AC_6(ITO, 'CNPJ', CNPJ, False, False) then
      (*06*) if not AC_i(ITO, 'CEP', CEP, False, False) then
      (*07*) if not AC_x(ITO, 'END', LOGR_RUA) then
      (*08*) if not AC_x(ITO, 'NUM', Numero_x) then
      (*09*) if not AC_x(ITO, 'COMPL', COMPL) then
      (*10*) if not AC_x(ITO, 'BAIRRO', BAIRRO) then
      (*11*) if not AC_x(ITO, 'FONE', FONE) then
      (*12*) if not AC_x(ITO, 'FAX', FAX) then
      (*13*) if not AC_x(ITO, 'EMAIL', EMAIL) then
      (*14*) if not AC_i(ITO, 'COD_MUN', COD_MUN, False, False) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    if COD_MUN = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCodMunIndef)(*FNumErro[17]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'COD_MUN'], True);
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_0);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0110(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_INC_TRIB, IND_APRO_CRED, COD_TIPO_CONT, IND_REG_CUM: String;
begin
  Result := False;
  FBloco := '0';
  FRegistro := '0110';
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  COD_INC_TRIB   := DModG.QrParamsSPEDSPED_COD_INC_TRIB.Value;
  IND_APRO_CRED  := DModG.QrParamsSPEDSPED_IND_APRO_CRED.Value;
  COD_TIPO_CONT  := DModG.QrParamsSPEDSPED_COD_TIPO_CONT.Value;
  IND_REG_CUM    := DModG.QrParamsSPEDSPED_IND_REG_CUM.Value;
  //
  if (COD_INC_TRIB = '2') and (IND_REG_CUM = EmptyStr) then
    Grava_SpedEfdPisCofinsErrs(efdexerObrigSemCont, 'IND_REG_CUM');
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_n(ITO, 'COD_INC_TRIB',   COD_INC_TRIB) then
      (*03*) if not AC_n(ITO, 'IND_APRO_CRED',  IND_APRO_CRED) then
      (*04*) if not AC_n(ITO, 'COD_TIPO_CONT',  COD_TIPO_CONT) then
      (*05*) if not AC_n(ITO, 'IND_REG_CUM',    IND_REG_CUM) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_0);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0140: Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_EST, NOME, CNPJ, UF, IE, IM, SUFRAMA: String;
  ImporExpor, AnoMes, Empresa, LinArq, COD_MUN: Integer;
  SQLType: TSQLType;
(*
var
  DT_INI, DT_FIN: TDateTime;
  TIPO_ESCRIT, CodMunici: Integer;
  IND_SIT_ESP, NUM_REC_ANTERIOR, CNPJ_CPF: String;
  CNPJ, CPF: Int64;
*)
  CNPJ_CPF: String;
begin
  Result := False;
  FBloco := '0';
  FRegistro := '0140';
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  COD_EST        := Geral.FF0(QrEmpresaFilial.Value);
  NOME           := QrEmpresaNOME_ENT.Value;
  CNPJ           := Geral.SoNumero_TT(QrEmpresaCNPJ_CPF.Value);
  UF             := QrEmpresaNOMEUF.Value;
  IE             := QrEmpresaIE.Value;
  //COD_MUN        := QrEmpresaECodMunici.Value;
  IM             := QrEmpresaNIRE.Value;
  SUFRAMA        := QrEmpresaSUFRAMA.Value;
  //
  CNPJ_CPF := Geral.SoNumero_TT(QrEmpresaCNPJ_CPF.Value);
  case Length(CNPJ_CPF) of
    11: COD_MUN := QrEmpresaPCodMunici.Value;
    14: COD_MUN := QrEmpresaECodMunici.Value;
    else
    begin
      MensagemDocumentoInvalido(QrEmpresaCNPJ_CPF.Value);
      Exit;
    end;
  end;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'COD_EST',  COD_EST) then
      (*03*) if not AC_x(ITO, 'NOME',     NOME) then
      (*04*) if not AC_n(ITO, 'CNPJ',     CNPJ) then
      (*05*) if not AC_x(ITO, 'UF',       UF) then
      (*06*) if not AC_x(ITO, 'IE',       IE) then
      (*07*) if not AC_i(ITO, 'COD_MUN',  COD_MUN, False, True) then
      (*08*) if not AC_x(ITO, 'IM',       IM) then
      (*09*) if not AC_x(ITO, 'SUFRAMA',  SUFRAMA) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    if COD_MUN = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCodMunIndef)(*FNumErro[17]*)], [
      FImporExpor, FAnoMes_Int,
      FEmpresa_Int, FLinArq, FRegistro, 'COD_MUN'], True);
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_0);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0150(): Boolean;
  procedure InsereSelEntEfdInnCTsCab();
    function MontaSQL(Tabela, Campo, CampoData: String; FiltraPorNFs: Boolean): String;
    begin
      Result := Geral.ATS([
      'INSERT INTO ' + FSelEnt,
      'SELECT DISTINCT einc.' + Campo + ' Codigo, "" Nome, 1 Ativo ',
      'FROM ' + TMeuDB + '.' + 'efdinnctscab einc',
      //'WHERE einc.' + CampoData + ' BETWEEN  ' + FIntervaloFiscal,
      'WHERE einc.Controle IN (' + FCordaDeCTes + ')',
      'ON DUPLICATE KEY UPDATE Ativo=1 ',
      '']);
    end;
  var
    SQL, Campo: String;
  begin
    if FCordaDe0053 <> EmptyStr then
    begin
      //SQL := MontaSQL('efdinnnfscab', 'Terceiro', FDtFiscal0053_Entr, True);
      //UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [SQL]);
      //Geral.MB_Teste(SQL);
      //
      SQL := MontaSQL('efdinnctscab', 'Terceiro', FDtFiscalFrete_Entr, False);
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [SQL]);
    end;
  end;

  procedure Insere_Cod_Part_B(Tabela, Campo: String);
  begin
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'INSERT INTO ' + FSelEnt,
    ' ',
    'SELECT DISTINCT ' + Campo + ' Codigo, ',
    '"" Nome, 1 Ativo ',
    'FROM ' + TMeuDB + '.' + Tabela,
    'WHERE ImporExpor IN (1,3) ',
    'AND AnoMes=' + FAnoMes_Txt,
    'AND Empresa=' + FEmpresa_Txt,
    'AND ' + Campo + ' <> "" ',
    'ON DUPLICATE KEY UPDATE Ativo=1 ',
    '']);
  end;
const
  ITO = itoNaoAplicavel;
var
  COD_PART, NOME, LOGR_RUA, NUMERO_x, CNPJ_CPF, IE: String;
  COD_PAIS, COD_MUN, NUMERO_i: Integer;
  CNPJ, CPF: Int64;
  Blc, SQLExec: String;
begin
  FBloco := '0';
  FRegistro := '0150';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  FSelEnt := UCriar.RecriaTempTableNovo(ntrtt_CodNom, DmodG.QrUpdPID1, False, 0, '_sel_ent_');
  Blc := Blocos_Selecionados2([]);
  SQLExec := Geral.ATS([
  'INSERT INTO ' + FSelEnt,
  '',
  'SELECT DISTINCT CodInfoDest Codigo,',
  '"" Nome, 1 Ativo ',
  'FROM ' + TMeuDB + '.nfecaba ',
  'WHERE ide_tpAmb<>2  ',
  'AND Empresa=' + FEmpresa_Txt,
  'AND FatID IN (' +
  Geral.FF0(VAR_FATID_0001) + ', ' +
  Geral.FF0(VAR_FATID_0050) +
  ') ',
  'AND FatNum IN (' + FCordaDe0001 + ') ',
  'AND CodInfoDest <> ' + FEmpresa_Txt,
  (*'AND DataFiscal BETWEEN ' +  FIntervaloFiscal,
  'AND Status IN (100,101,102,110,301) ',*)
  Blc,
  ' ',
  'UNION ',
  ' ',
  'SELECT DISTINCT CodInfoEmit Codigo,',
  '"" Nome, 1 Ativo ',
  'FROM ' + TMeuDB + '.nfecaba ',
  'WHERE ide_tpAmb<>2  ',
  'AND Empresa=' + FEmpresa_Txt,
  'AND FatID=' + Geral.FF0(VAR_FATID_0053),    // Tem FatID=2 ou 51 ????
  'AND FatNum IN (' + FCordaDe0053 + ') ',
  'AND CodInfoEmit <> ' + FEmpresa_Txt,
  (*'AND DataFiscal BETWEEN ' +  FIntervaloFiscal,
  'AND Status IN (100,101,102,110,301) ',*)
  Blc,
  '']);

  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, SQLExec);
  //
  //InsereSelEntFatID0053();
  //
  InsereSelEntEfdInnCTsCab();
  //
  if FSelRegC500 then
    Insere_Cod_Part_B('efdinnc500cab', 'Terceiro');
  if FSelRegD500 then
    Insere_Cod_Part_B('efdinnd500cab', 'Terceiro');

//////////////////  ANTIGOS DEPRECADOS /////////////////////////////////////////
  if FSelRegC100 then                                                         //
    Insere_Cod_Part_B(CO_PREFIX_TAB_ALL_SPED_EFD + 'C100', 'Terceiro');       //
  if FSelRegC500 then                                                         //
    Insere_Cod_Part_B(CO_PREFIX_TAB_ALL_SPED_EFD + 'C500', 'Terceiro');       //
  if FSelRegD500 then                                                         //
    Insere_Cod_Part_B(CO_PREFIX_TAB_ALL_SPED_EFD + 'D500', 'Terceiro');       //
////////////////////////////////////////////////////////////////////////////////


  UnDmkDAC_PF.AbreMySQLQuery0(QrSelEnt, DModG.MyPID_DB, [
  'SELECT DISTINCT Codigo ',
  'FROM ' + FSelEnt,
  '']);
  //
  PB1.Position := 0;
  PB1.Max := QrSelEnt.RecordCount;
  QrSelEnt.First;
  while not QrSelEnt.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    Numero_i := 0;
    ReopenEnder(QrSelEntCodigo.Value);
    //
    //COD_PART := Geral.FF0(QrSelEntCodigo.Value); /
    COD_PART := ObtemCOD_PART(QrSelEntCodigo.Value);
    NOME := Copy(QrEnderNOME_ENT.Value, 1, 100);
    CNPJ_CPF := Geral.SoNumero_TT(QrEnderCNPJ_CPF.Value);
    if QrEnderTipo.Value = 0 then
    begin
      COD_PAIS := QrEnderECodiPais.Value;
      CNPJ := Geral.I64MV(CNPJ_CPF);
      CPF := 0;
      COD_MUN := QrEnderECodMunici.Value;
    end else
    begin
      COD_PAIS := QrEnderPCodiPais.Value;
      CNPJ := 0;
      CPF := Geral.I64MV(CNPJ_CPF);
      COD_MUN := QrEnderPCodMunici.Value;
    end;
    IE := Geral.SoNumero_TT(QrEnderIE.Value);
    LOGR_RUA := QrEnderNOMELOGRAD.Value;
    if Trim(LOGR_RUA) <> '' then
      LOGR_RUA := LOGR_RUA + ' ';
    LOGR_RUA := LOGR_RUA + QrEnderRua.Value;
    if Numero_i = 0 then
      NUMERO_x := 'S/N'
    else
      NUMERO_x := Geral.FF0(Numero_i);
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_PART', COD_PART) then
        (*03*) if not AC_x(ITO, 'NOME', NOME) then
        (*04*) if not AC_i(ITO, 'COD_PAIS', COD_PAIS, False, True) then
        (*05*) if not AC_6(ITO, 'CNPJ', CNPJ, True, False) then
        (*06*) if not AC_6(ITO, 'CPF', CPF, True, False) then
        (*07*) if not AC_x(ITO, 'IE', IE) then
        (*08*) if not AC_i(ITO, 'COD_MUN', COD_MUN, COD_PAIS <> 1058, True) then
        (*09*) if not AC_x(ITO, 'SUFRAMA', QrEnderSUFRAMA.Value) then
        (*10*) if not AC_x(ITO, 'END', LOGR_RUA) then
        (*11*) if not AC_x(ITO, 'NUM', NUMERO_x) then
        (*12*) if not AC_x(ITO, 'COMPL', QrEnderCOMPL.Value) then
        (*13*) if not AC_x(ITO, 'BAIRRO', QrEnderBAIRRO.Value) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
    if COD_MUN = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCodMunIndef)(*FNumErro[17]*)], [
      FImporExpor, FAnoMes_Int,
      FEmpresa_Int, FLinArq, FRegistro, 'COD_MUN'], True);
    end;
    finally
      AdicionaLinha(FQTD_LIN_0);
    end;
    //
    QrSelEnt.Next;
  end;
  AdicionaBLC(QrSelEnt.RecordCount);
  Result := True;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0190(): Boolean;
  procedure InsereUnidMedFatID0053();
  begin
    if FCordaDe0053 <> EmptyStr then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
      'INSERT INTO ' + FSPEDEFD_0190,
      'SELECT DISTINCT eini.UNID, 1 Ativo ',
      'FROM ' + TMeuDB + '.efdinnnfsits eini',
      'LEFT JOIN ' + TMeuDB + '.efdinnnfscab einc ON einc.Controle=eini.Controle',
      //'WHERE einc.' + FDtFiscal0053_Entr + ' BETWEEN  ' + FIntervaloFiscal,
      'WHERE einc.NFe_FatID=' + Geral.FF0(VAR_FATID_0053),
      'AND einc.NFe_FatNum IN (' + FCordaDe0053 + ')',
      '']);
      //Geral.MB_Teste(DModG.QrUpdPID1.SQL.Text);
    end;
  end;
  procedure InsereUnidMed0190B(Tabela, Campo: String);
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + FSPEDEFD_0190,
    'SELECT DISTINCT med.Sigla NoUnidMed, 1 Ativo ',
    'FROM ' + TMeuDB + '.' + Tabela + ' cab',
    'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=cab.' + Campo,
    'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
    'LEFT JOIN ' + TMeuDB + '.unidmed med ON med.Codigo=gg1.UnidMed',
    'WHERE cab.ImporExpor=3 ',
    'AND cab.Empresa=' + Geral.FF0(FEmpresa_Int),
    'AND cab.AnoMes=' + Geral.FF0(FAnoMes_Int),
    'AND cab.' + Campo + ' <> 0',
    '']);
  end;
const
  ITO = itoNaoAplicavel;
var
  UNID, DESCR, SQL_Proprio: String;
  JaGravou: Boolean;
begin
  FBloco := '0';
  FRegistro := '0190';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  JaGravou := False;
  FSPEDEFD_0190 :=
    SPED_Create.RecriaTempTableNovo(ntrttSPEDEFD_0190, DmodG.QrUpdPID1, False);
  // 2016-02-07 Nao entendi porque!!!
  SQL_Proprio := 'AND CodInfoEmit<>' + FEmpresa_Txt;
  //
(* N�o pode ser aqui, pois pode ser mudado no FatID 50
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FSPEDEFD_0190,
  'SELECT DISTINCT nfei.prod_uTrib NoUnidMed, 1 Ativo ',
  'FROM ' + TMeuDB + '.nfeitsi nfei ',
  'LEFT JOIN ' + TMeuDB + '.nfecaba nfea ON nfea.FatID=nfei.FatID ',
  '     AND nfea.FatNum=nfei.FatNum ',
  '     AND nfea.Empresa=nfei.Empresa ',
  'WHERE nfea.Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND nfea.FatID IN (' +
  Geral.FF0(VAR_FATID_0001) + ', ' +
  Geral.FF0(VAR_FATID_0050) +
  ') ',
  'AND nfea.FatNum IN (' + FCordaDe0001 + ')',
  '']);
*)
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FSPEDEFD_0190,
  'SELECT DISTINCT med.Sigla NoUnidMed, 1 Ativo ',
  'FROM ' + TMeuDB + '.nfeitsi nfei ',
  'LEFT JOIN ' + TMeuDB + '.nfecaba nfea ON nfea.FatID=nfei.FatID ',
  '     AND nfea.FatNum=nfei.FatNum ',
  '     AND nfea.Empresa=nfei.Empresa ',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=nfei.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN ' + TMeuDB + '.unidmed med ON med.Codigo=gg1.UnidMed',
  'WHERE nfea.Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND nfea.FatID IN (' +
  Geral.FF0(VAR_FATID_0001) + ', ' +
  Geral.FF0(VAR_FATID_0050) +
  ') ',
  'AND nfea.FatNum IN (' + FCordaDe0001 + ')',
  //
  'AND GraGruX <> 0 ',
  Blocos_Selecionados2(['C500','D500']),
  //SQL_Proprio,
  'ORDER BY GraGruX ',
  '']);
  //Geral.MB_Teste(DModG.QrUpdPID1.SQL.Text);


(* N�o pode ser aqui, pois pode ser mudado (Exemplo: de ton para kg no cal)
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FSPEDEFD_0190,
  'SELECT DISTINCT nfei.prod_uTrib NoUnidMed, 1 Ativo ',
  'FROM ' + TMeuDB + '.nfeitsi nfei ',
  'LEFT JOIN ' + TMeuDB + '.nfecaba nfea ON nfea.FatID=nfei.FatID ',
  '     AND nfea.FatNum=nfei.FatNum ',
  '     AND nfea.Empresa=nfei.EMpresa ',
  'WHERE nfea.Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND nfea.FatID=' + Geral.FF0(VAR_FATID_0053),
  'AND nfea.FatNum IN (' + FCordaDe0053 + ')',
  '']);
*)
  InsereUnidMedFatID0053();
  //
  if FSelRegC500 then
    InsereUnidMed0190B('efdinnc500its', 'GraGruX');
  if FSelRegD500 then
    InsereUnidMed0190B('efdinnd500its', 'GraGruX');

{
  InsereUnidMed0190A('efdpiscofinsh010', 'UNID');
  InsereUnidMed0190B('efdpiscofinsk200', 'GraGruX');
  InsereUnidMed0190B('efdpiscofinsk220', 'GGXDst');
  InsereUnidMed0190B('efdpiscofinsk220', 'GGXOri');
  InsereUnidMed0190B('efdpiscofinsk230', 'GraGruX');
  InsereUnidMed0190B('efdpiscofinsk235', 'GraGruX');
  InsereUnidMed0190B('efdpiscofinsk250', 'GraGruX');
  InsereUnidMed0190B('efdpiscofinsk255', 'GraGruX');
  InsereUnidMed0190B('efdpiscofinsk280', 'GraGruX');
  //InsereUnidMed0190B('efdpiscofinsk290', 'GraGruX');
  InsereUnidMed0190B('efdpiscofinsk291', 'GraGruX');
  InsereUnidMed0190B('efdpiscofinsk292', 'GraGruX');
  //InsereUnidMed0190B('efdpiscofinsk300', 'GraGruX');
  InsereUnidMed0190B('efdpiscofinsk301', 'GraGruX');
  InsereUnidMed0190B('efdpiscofinsk302', 'GraGruX');
}
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrUniMedi, DModG.MyPID_DB, [
  'SELECT DISTINCT NoUnidMed ',
  'FROM ' + FSPEDEFD_0190,
  '']);
  PB1.Position := 0;
  PB1.Max := QrUniMedi.RecordCount;
  //
  QrUniMedi.First;
  while not QrUniMedi.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    UNID  := QrUniMediNoUnidMed.Value;
    UnNFe_PF.ReopenUnidMed(UNID, QrUnidMed, null);
    //
    DESCR := QrUnidMedNome.Value;
    if DESCR = '' then
      //DESCR := UNID;
    try
      //Geral.MB_SQL(Self, QrUnidMed);
      if not JaGravou then
      begin
        Grava_SpedEfdPisCofinsErrs(efdexerObrigSemCont, 'DESCR');
        JaGravou := True;
      end;
    except
      ;
    end;
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'UNID', UNID) then
        (*03*) if not AC_x(ITO, 'DESCR', DESCR) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
    finally
      AdicionaLinha(FQTD_LIN_0);
    end;
    //
    QrUniMedi.Next;
  end;
  AdicionaBLC(QrUniMedi.RecordCount);
  Result := True;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0200(): Boolean;
  //
  procedure InsereGraGruXFatID0053();
  begin
    if FCordaDe0053 <> EmptyStr then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
      'INSERT INTO ' + FSPEDEFD_0200,
      'SELECT DISTINCT eini.GraGruX, 1 Ativo ',
      'FROM ' + TMeuDB + '.efdinnnfsits eini',
      'LEFT JOIN ' + TMeuDB + '.efdinnnfscab einc ON einc.Controle=eini.Controle',
      'WHERE einc.' + FDtFiscal0053_Entr + ' BETWEEN  ' + FIntervaloFiscal,
      'AND einc.NFe_FatNum IN (' + FCordaDe0053 + ') ',
      '']);
    end;
  end;
  //
  procedure InsereGraGruX0200(Tabela, Campo: String);
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + FSPEDEFD_0200,
    'SELECT ' + Campo + ' GraGruX, 1 Ativo ',
    'FROM ' + TMeuDB + '.' + Tabela + ' ',
    'WHERE ImporExpor=3 ',// + Geral.FF0(FImporExpor),
    'AND Empresa=' + Geral.FF0(FEmpresa_Int),
    'AND AnoMes=' + Geral.FF0(FAnoMes_Int),
    '']);
  end;
const
  ITO = itoNaoAplicavel;
var
  COD_ITEM, DESCR_ITEM, COD_BARRA, COD_ANT_ITEM, UNID_INV, COD_NCM, EX_IPI,
  C_L, NCM, Campo, TIPO_ITEM, COD_LST, SQL_Proprio: String;
  COD_GEN, CEST, GraGruX: Integer;
  ALIQ_ICMS: Double;
  //
  procedure InsereAtual();
  begin
    FBloco := '0';
    FRegistro := '0200';
    Result := False;
    //PB1.Max := 0;
    //PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    try
      PredefineLinha();
      FLin0200 := FLinArq;
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
        (*03*) if not AC_x(ITO, 'DESCR_ITEM', DESCR_ITEM) then
        (*04*) if not AC_x(ITO, 'COD_BARRA', COD_BARRA) then
        (*05*) if not AC_n(ITO, 'COD_ANT_ITEM', COD_ANT_ITEM) then
        (*06*) if not AC_x(ITO, 'UNID_INV', UNID_INV) then
        (*07*) if not AC_n(ITO, 'TIPO_ITEM', TIPO_ITEM) then
        (*08*) if not AC_x(ITO, 'COD_NCM', COD_NCM) then
        (*09*) if not AC_x(ITO, 'EX_IPI', EX_IPI) then
        (*10*) if not AC_i(ITO, 'COD_GEN', COD_GEN, COD_LST <> '', True) then
        (*11*) if not AC_x(ITO, 'COD_LST', COD_LST) then
        (*12*) if not AC_f(ITO, 'ALIQ_ICMS', ALIQ_ICMS, True, False) then
        (*13*) if not AC_i(ITO, 'CEST', CEST, True, (*True*)False) then
        //
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      AddCampoSQL('GraGruX', GraGruX);
      //
      if C_L <> QrProduto2COD_LST.Value then
      begin
        Campo := 'COD_LST';
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes', 'Empresa',
        'LinArq', 'REG', 'Campo'], [
        Integer(efdexerServicInval)(*FNumErro[10]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, Campo], True);
      end;
      NCM := Geral.SoNumero_TT(QrProduto2NCM.Value);
      if Length(NCM) < 2 then
      begin
        Campo := 'COD_NCM';
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes', 'Empresa',
        'LinArq', 'REG', 'Campo'], [
        Integer(efdexerFaltaNCM)(*FNumErro[11]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, Campo], True);
      end;
    finally
      AdicionaLinha(FQTD_LIN_0);
      //
    end;
  end;
begin
  FSPEDEFD_0200 :=
    SPED_Create.RecriaTempTableNovo(ntrttSPEDEFD_0200, DmodG.QrUpdPID1, False);
  // 2016-02-07 Nao entendi porque!!!
  SQL_Proprio := 'AND CodInfoEmit<>' + FEmpresa_Txt;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FSPEDEFD_0200,
  'SELECT DISTINCT nfei.GraGruX, 1 Ativo ',
  'FROM ' + TMeuDB + '.nfeitsi nfei ',
  'LEFT JOIN ' + TMeuDB + '.nfecaba nfea ON nfea.FatID=nfei.FatID ',
  '     AND nfea.FatNum=nfei.FatNum ',
  '     AND nfea.Empresa=nfei.EMpresa ',
  //
  'WHERE nfea.Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND nfea.FatID IN (' +
  Geral.FF0(VAR_FATID_0001) + ', ' +
  Geral.FF0(VAR_FATID_0050) +
  ') ',
  'AND nfea.FatNum IN (' + FCordaDe0001 + ')',
  //
  'AND GraGruX <> 0 ',
  Blocos_Selecionados2(['C500','D500']),
  //SQL_Proprio,
  'ORDER BY GraGruX ',
  '']);
  //
  //Geral.MB_Teste(DModG.QrUpdPID1.SQL.Text);

  InsereGraGruXFatID0053();
(*
  InsereGraGruX0200('efdpiscofinsh010', 'COD_ITEM');
  InsereGraGruX0200('efdpiscofinsk200', 'GraGruX');
  InsereGraGruX0200('efdpiscofinsk220', 'GGXDst');
  InsereGraGruX0200('efdpiscofinsk220', 'GGXOri');
  InsereGraGruX0200('efdpiscofinsk230', 'GraGruX');
  InsereGraGruX0200('efdpiscofinsk235', 'GraGruX');
  InsereGraGruX0200('efdpiscofinsk250', 'GraGruX');
  InsereGraGruX0200('efdpiscofinsk255', 'GraGruX');
  InsereGraGruX0200('efdpiscofinsk280', 'GraGruX');
  if Geral.IMV(FVersao) >= 13 then // 2019-01-01
  begin
    //InsereGraGruX0200('efdpiscofinsk290', 'GraGruX');
    InsereGraGruX0200('efdpiscofinsk291', 'GraGruX');
    InsereGraGruX0200('efdpiscofinsk292', 'GraGruX');
    //InsereGraGruX0200('efdpiscofinsk300', 'GraGruX');
    InsereGraGruX0200('efdpiscofinsk301', 'GraGruX');
    InsereGraGruX0200('efdpiscofinsk302', 'GraGruX');
  end;
*)
  //
  if not AppPF.ReabreSPEDEFD0200Exportacao(QrProduto2, FSPEDEFD_0200) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrProduto2, DmodG.MyPID_DB, [
    'SELECT ggx.Controle, ggx.GraGru1, CONCAT(gg1.Nome,  ',
    'IF(gtc.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome)))  ',
    'NO_PRD_TAM_COR, gg1.cGTIN_EAN , gg1.NCM, ',
    'gg1.EX_TIPI, gg1.COD_LST, gg1.SPEDEFD_ALIQ_ICMS, ',
    //'unm.SIGLA, pgt.Tipo_Item ',
    'unm.SIGLA, gg1.prod_CEST, ',
    //'IF(gg1.Nivel2 <> 0, gg2.Tipo_Item, pgt.Tipo_Item) Tipo_Item ',
    'IF(gg1.Nivel2 <> 0 AND gg2.Tipo_Item >= 0, gg2.Tipo_Item, pgt.Tipo_Item) Tipo_Item',
    //
    'FROM ' + TMeuDB + '.gragrux ggx ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gratamcad  gtc ON gtc.Codigo=gti.Codigo ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
    'LEFT JOIN ' + TMeuDB + '.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
    'WHERE ggx.Controle IN ( ',
    '  SELECT DISTINCT GraGruX  ',
    '  FROM ' + FSPEDEFD_0200,
    ') ',
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
  end;
  //Geral.MB_SQL(Self, QrProduto2);
  //
  PB1.Position := 0;
  PB1.Max := QrProduto2.RecordCount;
    //
  QrProduto2.First;
  while not QrProduto2.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    C_L := Geral.SoNumero_TT(QrProduto2COD_LST.Value);
    if Geral.IMV(C_L) = 0 then
      C_L := ''
    else
    begin
      while Length(C_L) < 4 do
      begin
        C_L := '0' + C_L;
      end;
      C_L := C_L[1] + C_L[2] + '.' + C_L[3] + C_L[4];
    end;
    //
    COD_ITEM     := ObtemCOD_ITEM(QrProduto2Controle.Value);
    DESCR_ITEM   := Copy(QrProduto2NO_PRD_TAM_COR.Value, 1, 255);
    VerificaDESCR_ITEM(DESCR_ITEM, QrProduto2);
    COD_BARRA    := QrProduto2cGTIN_EAN.Value;
    COD_ANT_ITEM := ''; // Nao preeencher aqui, mas no 0205
    UNID_INV     := QrProduto2SIGLA.Value;
    //
    TIPO_ITEM    := FormatFloat('00', QrProduto2Tipo_Item.Value);
    COD_NCM      := Geral.SoNumero_TT(QrProduto2NCM.Value);
    EX_IPI       := QrProduto2EX_TIPI.Value;
    COD_GEN      := Geral.IMV(Copy(COD_NCM, 1, 2));
    //COD_LST      := Geral.IMV(C_L);
    COD_LST      := C_L;
    ALIQ_ICMS    := QrProduto2SPEDEFD_ALIQ_ICMS.Value;
    CEST         := QrProduto2prod_CEST.Value;
    //
    GraGruX      := QrProduto2Controle.Value;
    //
    InsereAtual();
    //
    GeraRegistro_0205();
    //
    QrProduto2.Next;
  end;
  AdicionaBLC(QrProduto2.RecordCount, '0200');
  //
  Result := True;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0205(): Boolean;
begin
  // Parei aqui! Altera��es de Nome!
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0400(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_NAT, DESCR_NAT: String;
  //
  procedure InsereAtual();
  begin
    FBloco := '0';
    FRegistro := '0400';
    Result := False;
    //PB1.Max := 0;
    //PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    try
      PredefineLinha();
      FLin0400 := FLinArq;
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_NAT', COD_NAT) then
        (*03*) if not AC_x(ITO, 'DESCR_NAT', DESCR_NAT) then
        //
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
    finally
      AdicionaLinha(FQTD_LIN_0);
      //
    end;
  end;
begin
  FSPEDEFD_0400 :=
    SPED_Create.RecriaTempTableNovo(ntrttSPEDEFD_0400, DmodG.QrUpdPID1, False);
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrNatOp, Dmod.MyDB, [
  'SELECT nfa.FisRegCad, frc.ide_natOp',
  'FROM nfecaba nfa',
  'LEFT JOIN fisregcad frc ON frc.Codigo=nfa.FisRegCad',
  'WHERE nfa.DataFiscal BETWEEN "2022-01-01" AND "2022-02-28"',
  'WHERE nfa.DataFiscal  BETWEEN  ' + FIntervaloFiscal,
  'GROUP BY nfa.FisRegCad',
  '']);
  *)

  //UnDmkDAC_PF.AbreMySQLQuery0(QrNatOp, DModG.MyPID_DB, [
  // NFs de saida
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FSPEDEFD_0400,
  'SELECT nfea.FisRegCad, frc.ide_natOp, 1 Ativo',
  'FROM ' + TMeuDB + '.nfecaba nfea',
  //SQL_LEFT_JOIN_NFe_E_INN(),
  'LEFT JOIN ' + TMeuDB + '.fisregcad frc ON frc.Codigo=nfea.FisRegCad',
  //
  'WHERE nfea.Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND nfea.FatID IN (' +
  Geral.FF0(VAR_FATID_0001) + ', ' +
  Geral.FF0(VAR_FATID_0050) +
  ') ',
  'AND nfea.FatNum IN (' + FCordaDe0001 + ')',
  //
  'AND nfea.FisRegCad <> 0 ',
  //
  //DmSPED.SQL_PeriodoFiscalNFe_e_INN(),
  'GROUP BY nfea.FisRegCad',
  '']);
  //Geral.MB_Teste(DModG.QrUpdPID1.SQL.Text);
  // NFs de Entrada
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FSPEDEFD_0400,
  'SELECT einc.RegrFiscal, frc.ide_natOp, 1 Ativo',
  'FROM ' + TMeuDB + '.nfecaba nfea',
  DmSPED.SQL_LEFT_JOIN_NFe_E_INN(True),
  'LEFT JOIN ' + TMeuDB + '.fisregcad frc ON frc.Codigo=einc.RegrFiscal',
  //
  'WHERE nfea.Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND nfea.FatID=' + Geral.FF0(VAR_FATID_0053),
  'AND nfea.FatNum IN (' + FCordaDe0053 + ')',
  //
  'AND einc.RegrFiscal <> 0 ',
  //
  //DmSPED.SQL_PeriodoFiscalNFe_e_INN(),
  'GROUP BY nfea.FisRegCad',
  '']);
  //
  //Geral.MB_Teste(DModG.QrUpdPID1.SQL.Text);
  // Fretes
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FSPEDEFD_0400,
  'SELECT eicc.RegrFiscal, frc.ide_natOp, 1 Ativo',
  'FROM ' + TMeuDB + '.efdinnctscab eicc',
  'LEFT JOIN ' + TMeuDB + '.fisregcad frc ON frc.Codigo=eicc.RegrFiscal',
  'WHERE eicc.Controle IN (' + FCordaDeCTes + ')',
  'AND eicc.RegrFiscal <> 0 ',
  'GROUP BY eicc.RegrFiscal ',
  '']);
  //Geral.MB_Teste(DModG.QrUpdPID1.SQL.Text);
  //
   UnDmkDAC_PF.AbreMySQLQuery0(QrNatOp, DModG.MyPID_DB, [
  'SELECT FisRegCad, ide_natOp',
  'FROM ' + FSPEDEFD_0400,
  'GROUP BY FisRegCad',
  'ORDER BY FisRegCad',
  '']);

  QrNatOp.First;
  while not QrNatOp.Eof do
  begin
    COD_NAT   := Geral.FF0(QrNatOpFisRegCad.Value);
    DESCR_NAT := QrNatOpide_natOp.Value;
    InsereAtual();
    //
    QrNatOp.Next;
  end;
  //AdicionaBLC(QrNatOp.RecordCount, '0400');
  FQTD_LIN_0400 := FQTD_LIN_0400 + QrNatOp.RecordCount;
  //
  Result := True;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0500: Boolean;
const
  ITO = itoNaoAplicavel;
  //
  idDeb  = 1;
  idCred = 2;
  fldDeb = 'GenCtbD';
  fldCred = 'GenCtbC';
  //
var
  Corda: String;
  //
  DT_ALT: TDateTime;
  COD_NAT_CC, IND_CTA, COD_CTA, NOME_CTA, COD_CTA_REF, CNPJ_EST: String;
  Nivel: Integer;
  //
  procedure InsereAtual();
  begin
    FBloco := '0';
    FRegistro := '0500';
    Result := False;
    //PB1.Max := 0;
    //PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    try
      PredefineLinha();
      FLin0500 := FLinArq;
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_d(ITO, 'DT_ALT', DT_ALT, False, False, 23) then
        (*03*) if not AC_x(ITO, 'COD_NAT_CC', COD_NAT_CC) then
        (*04*) if not AC_x(ITO, 'IND_CTA', IND_CTA) then
        (*05*) if not AC_i(ITO, 'NIVEL', Nivel, False, True) then
        (*06*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
        (*07*) if not AC_x(ITO, 'NOME_CTA', NOME_CTA) then
        (*08*) if not AC_x(ITO, 'COD_CTA_REF', COD_CTA_REF) then
        (*09*) if not AC_N(ITO, 'CNPJ_EST', CNPJ_EST) then
        //
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
    finally
      AdicionaLinha(FQTD_LIN_0);
      //
    end;
  end;

begin
  FSPEDEFD_0500 :=
    SPED_Create.RecriaTempTableNovo(ntrttSPEDEFD_0500, DmodG.QrUpdPID1, False);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _CTAS_PLA_EFD_PIS_COFINS_A;',
  '',
  'CREATE TABLE _CTAS_PLA_EFD_PIS_COFINS_A',
  '',
  'SELECT DISTINCT cta.Codigo',
  'FROM ' + TMeuDB + '.nfecaba nfea',
  'LEFT JOIN ' + TMeuDB + '.fisregcad frc ON frc.Codigo=nfea.FisRegCad',
  'LEFT JOIN ' + TMeuDB + '.contas cta ON cta.Codigo=frc.' + fldDeb,
  //
  'WHERE nfea.Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND nfea.FatID IN (' +
  Geral.FF0(VAR_FATID_0001) + ', ' +
  Geral.FF0(VAR_FATID_0050) +
  ') ',
  'AND nfea.FatNum IN (' + FCordaDe0001 + ')',
  //
  'AND nfea.FisRegCad <> 0 ',
  'AND cta.Codigo <> 0',
  'AND CreDeb_PisCofins=' + Geral.FF0(idDeb),
  '',
  'UNION',
  '',
  'SELECT DISTINCT cta.Codigo',
  'FROM ' + TMeuDB + '.nfecaba nfea',
  'LEFT JOIN ' + TMeuDB + '.fisregcad frc ON frc.Codigo=nfea.FisRegCad',
  'LEFT JOIN ' + TMeuDB + '.contas cta ON cta.Codigo=frc.' + fldCred,
  //
  'WHERE nfea.Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND nfea.FatID IN (' +
  Geral.FF0(VAR_FATID_0001) + ', ' +
  Geral.FF0(VAR_FATID_0050) +
  ') ',
  'AND nfea.FatNum IN (' + FCordaDe0001 + ')',
  //
  'AND nfea.FisRegCad <> 0 ',
  'AND cta.Codigo <> 0',
  'AND CreDeb_PisCofins=' + Geral.FF0(idCred),
  '',
  'UNION',
  '',
  'SELECT DISTINCT cta.Codigo',
  'FROM ' + TMeuDB + '.nfecaba nfea',
  DmSPED.SQL_LEFT_JOIN_NFe_E_INN(True),
  'LEFT JOIN ' + TMeuDB + '.fisregcad frc ON frc.Codigo=einc.RegrFiscal',
  'LEFT JOIN ' + TMeuDB + '.contas cta ON cta.Codigo=frc.' + fldDeb,
  //
  'WHERE nfea.Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND nfea.FatID=' + Geral.FF0(VAR_FATID_0053),
  'AND nfea.FatNum IN (' + FCordaDe0053 + ')',
  //
  'AND nfea.FisRegCad <> 0 ',
  'AND cta.Codigo <> 0 ',
  'AND CreDeb_PisCofins=' + Geral.FF0(idDeb),
  '',
  'UNION',
  '',
  'SELECT DISTINCT cta.Codigo',
  'FROM ' + TMeuDB + '.nfecaba nfea',
  DmSPED.SQL_LEFT_JOIN_NFe_E_INN(True),
  'LEFT JOIN ' + TMeuDB + '.fisregcad frc ON frc.Codigo=einc.RegrFiscal',
  'LEFT JOIN ' + TMeuDB + '.contas cta ON cta.Codigo=frc.' + fldCred,
  //
  'WHERE nfea.Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND nfea.FatID=' + Geral.FF0(VAR_FATID_0053),
  'AND nfea.FatNum IN (' + FCordaDe0053 + ')',
  //
  'AND nfea.FisRegCad <> 0 ',
  'AND cta.Codigo <> 0 ',
  'AND CreDeb_PisCofins=' + Geral.FF0(idCred),
  ';',
  '',
  'SELECT Codigo FROM _CTAS_PLA_EFD_PIS_COFINS_A',
  ';',
  '']);
  //Geral.MB_Teste(DModG.QrAux.SQL.Text);
  //
  Corda := MyObjects.CordaDeQuery(DModG.QrAux, 'Codigo', '-999999999');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtas0500, DModG.MyPID_DB, [
  'INSERT INTO ' + FSPEDEFD_0500,
  'SELECT co.Codigo, pl.FinContab,',
  'pl.DataCad DtaSPED, co.Nome, co.SPED_COD_CTA_REF,',
  'pl.DataCad DataCad_pl, pl.DataAlt DataAlt_pl,',
  'cj.DataCad DataCad_cj, cj.DataAlt DataAlt_cj,',
  'gr.DataCad DataCad_gr, gr.DataAlt DataAlt_gr,',
  'sg.DataCad DataCad_sg, sg.DataAlt DataAlt_sg,',
  'co.DataCad DataCad_co, co.DataAlt DataAlt_co,',
  'CONCAT( ',
  '  pl.OrdemLista, ".", ',
  '  cj.OrdemLista, ".", ',
  '  gr.OrdemLista, ".", ',
  '  sg.OrdemLista, ".", ',
  '  co.OrdemLista) Ordens, ',
  '1 Ativo ',
  'FROM ' + TMeuDB + '.contas co ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.Subgrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    gr ON gr.Codigo=sg.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos cj ON cj.Codigo=gr.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     pl ON pl.Codigo=cj.Plano ',
  'LEFT JOIN ' + TMeuDB + '.entidades cl ON cl.Codigo=co.Empresa ',
  'WHERE co.Codigo IN (' + Corda + ')',
  ';',
  'UPDATE _CTAS_PLA_EFD_PIS_COFINS_B SET DtaSPED=DataAlt_pl',
  'WHERE DataAlt_pl <= "' + FDataFim_Txt + '" AND DataAlt_pl > DtaSPED',
  ';',
  'UPDATE _CTAS_PLA_EFD_PIS_COFINS_B SET DtaSPED=DataCad_cj',
  'WHERE DataCad_cj <= "' + FDataFim_Txt + '" AND DataCad_cj > DtaSPED',
  ';',
  'UPDATE _CTAS_PLA_EFD_PIS_COFINS_B SET DtaSPED=DataAlt_cj',
  'WHERE DataAlt_cj <= "' + FDataFim_Txt + '" AND DataAlt_cj > DtaSPED',
  ';',
  'UPDATE _CTAS_PLA_EFD_PIS_COFINS_B SET DtaSPED=DataCad_gr',
  'WHERE DataCad_gr <= "' + FDataFim_Txt + '" AND DataCad_gr > DtaSPED',
  ';',
  'UPDATE _CTAS_PLA_EFD_PIS_COFINS_B SET DtaSPED=DataAlt_gr',
  'WHERE DataAlt_gr <= "' + FDataFim_Txt + '" AND DataAlt_gr > DtaSPED',
  ';',
  'UPDATE _CTAS_PLA_EFD_PIS_COFINS_B SET DtaSPED=DataCad_sg',
  'WHERE DataCad_sg <= "' + FDataFim_Txt + '" AND DataCad_sg > DtaSPED',
  ';',
  'UPDATE _CTAS_PLA_EFD_PIS_COFINS_B SET DtaSPED=DataAlt_sg',
  'WHERE DataAlt_sg <= "' + FDataFim_Txt + '" AND DataAlt_sg > DtaSPED',
  ';',
  'UPDATE _CTAS_PLA_EFD_PIS_COFINS_B SET DtaSPED=DataCad_co',
  'WHERE DataCad_co <= "' + FDataFim_Txt + '" AND DataCad_co > DtaSPED',
  ';',
  'UPDATE _CTAS_PLA_EFD_PIS_COFINS_B SET DtaSPED=DataAlt_co',
  'WHERE DataAlt_co <= "' + FDataFim_Txt + '" AND DataAlt_co > DtaSPED',
  ';',
  'SELECT * ',
  'FROM ' + FSPEDEFD_0500 + ' ;',
  '']);
  //Geral.MB_Teste(QrCtas0500.SQL.Text);
  //
  // N�o fechar, sera usado em outros registros posteriores

  QrCtas0500.First;
  while not QrCtas0500.Eof do
  begin
    DT_ALT     :=  QrCtas0500DtaSPED.Value;
    COD_NAT_CC :=  Geral.FFN(QrCtas0500FinContab.Value, 2);
    IND_CTA    :=  'A';
    Nivel      :=  5;
    COD_CTA    :=  QrCtas0500Ordens.Value;
    NOME_CTA   :=  QrCtas0500Nome.Value;
    COD_CTA_REF:=  QrCtas0500SPED_COD_CTA_REF.Value;
    CNPJ_EST   :=  '';
    //
    InsereAtual();
    //
    QrCtas0500.Next;
  end;
  //AdicionaBLC(QrCtas0500.RecordCount, '0500');
  FQTD_LIN_0500 := FQTD_LIN_0500 + QrCtas0500.RecordCount;
  //
  Result := True;

  // N�o fechar a query, sera usada em outros registros posteriores
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0900_Pos(): Boolean;
begin
  //
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0900_Pre(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  CNPJ: String;
  REC_TOTAL_BLOCO_A, REC_NRB_BLOCO_A, REC_TOTAL_BLOCO_C, REC_NRB_BLOCO_C,
  REC_TOTAL_BLOCO_D, REC_NRB_BLOCO_D, REC_TOTAL_BLOCO_F, REC_NRB_BLOCO_F,
  REC_TOTAL_BLOCO_I, REC_NRB_BLOCO_I, REC_TOTAL_BLOCO_1, REC_NRB_BLOCO_1,
  REC_TOTAL_PERIODO, REC_TOTAL_NRB_PERIODO: Double;
begin
  FBloco := '0';
  FRegistro := '0900';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  REC_TOTAL_BLOCO_A     := 0.00;
  REC_NRB_BLOCO_A       := 0.00;
  REC_TOTAL_BLOCO_C     := 0.00; // define abaixo
  REC_NRB_BLOCO_C       := 0.00;
  REC_TOTAL_BLOCO_D     := 0.00;
  REC_NRB_BLOCO_D       := 0.00;
  REC_TOTAL_BLOCO_F     := 0.00;
  REC_NRB_BLOCO_F       := 0.00;
  REC_TOTAL_BLOCO_I     := 0.00;
  REC_NRB_BLOCO_I       := 0.00;
  REC_TOTAL_BLOCO_1     := 0.00;
  REC_NRB_BLOCO_1       := 0.00;
  REC_TOTAL_PERIODO     := 0.00; // define abaixo
  REC_TOTAL_NRB_PERIODO := 0.00; // define abaixo
  //
(*
  UnDmkDAC_PF.AbremySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT SUM(prod_vProd) prod_vProd ',
  'FROM nfeitsi ',
  'WHERE FatID IN (' +
  Geral.FF0(VAR_FATID_0001) + ', ' +
  Geral.FF0(VAR_FATID_0050) +
  ') ',
  'AND Empresa=' + FEmpresa_TXT,
  'AND FatNum IN (' + FCordaSai100 + ') ',
  ' ']);
  //Geral.MB_Teste(Dmod.QrAux.SQL.Text);
*)
  //  alida��o: o valor informado deve ser igual ao somat�rio dos seguintes registros/campos, cujos respectivos campos
  //  ST_COFINS estejam preenchidos com os c�digos 01, 02, 03, 04, 05, 06, 07, 08 e 09:
  //  - C170.VL_ITEM, quando o registro pai C100.COD_MOD <> 55 e C100.IND_OPER= �1�,
  //  - C170.VL_ITEM, quando o registro pai C100.COD_MOD = 55 e IND_OPER= �1�, e campo 010.IND_ESCRI <> 1;
  UnDmkDAC_PF.AbremySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT SUM(nfei.prod_vProd) prod_vProd ',
  'FROM nfeitsi nfei',
  'LEFT JOIN nfecaba nfea ',
  '  ON nfea.FatID=nfei.FatID',
  '  AND nfea.FatNum=nfei.FatNum',
  '  AND nfea.Empresa=nfei.Empresa',
  'LEFT JOIN nfeitss nfes',
  '  ON nfes.FatID=nfei.FatID',
  '  AND nfes.FatNum=nfei.FatNum',
  '  AND nfes.Empresa=nfei.Empresa',
  'WHERE nfei.FatID IN (' +
  Geral.FF0(VAR_FATID_0001) + ', ' +
  Geral.FF0(VAR_FATID_0050) +
  ') ',
  'AND nfei.Empresa=' + FEmpresa_TXT,
  'AND nfei.FatNum IN (' + FCordaSai100 + ') ',
  'AND nfes.COFINS_CST IN (01, 02, 03, 04, 05, 06, 07, 08, 09)',
  '']);
  REC_TOTAL_BLOCO_C := Dmod.QrAux.Fields[0].AsFloat;

  // ....

  REC_TOTAL_PERIODO     := REC_TOTAL_BLOCO_A + REC_TOTAL_BLOCO_C + REC_TOTAL_BLOCO_D + REC_TOTAL_BLOCO_F + REC_TOTAL_BLOCO_A + REC_TOTAL_BLOCO_1;
  REC_TOTAL_NRB_PERIODO := REC_NRB_BLOCO_A + REC_NRB_BLOCO_C + REC_NRB_BLOCO_D + REC_NRB_BLOCO_F + REC_NRB_BLOCO_A + REC_NRB_BLOCO_1;
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then

      (*02*) if not AC_f(ITO, 'REC_TOTAL_BLOCO_A', REC_TOTAL_BLOCO_A, True, True) then
      (*03*) if not AC_f(ITO, 'REC_NRB_BLOCO_A', REC_NRB_BLOCO_A, True, False) then

      (*04*) if not AC_f(ITO, 'REC_TOTAL_BLOCO_C', REC_TOTAL_BLOCO_C, True, True) then
      (*05*) if not AC_f(ITO, 'REC_NRB_BLOCO_C', REC_NRB_BLOCO_C, True, True) then

      (*06*) if not AC_f(ITO, 'REC_TOTAL_BLOCO_D', REC_TOTAL_BLOCO_D, True, True) then
      (*07*) if not AC_f(ITO, 'REC_NRB_BLOCO_D', REC_NRB_BLOCO_D, True, False) then

      (*08*) if not AC_f(ITO, 'REC_TOTAL_BLOCO_F', REC_TOTAL_BLOCO_F, True, True) then
      (*09*) if not AC_f(ITO, 'REC_NRB_BLOCO_F', REC_NRB_BLOCO_F, True, False) then

      (*10*) if not AC_f(ITO, 'REC_TOTAL_BLOCO_I', REC_TOTAL_BLOCO_I, True, True) then
      (*11*) if not AC_f(ITO, 'REC_NRB_BLOCO_I', REC_NRB_BLOCO_I, True, False) then

      (*12*) if not AC_f(ITO, 'REC_TOTAL_BLOCO_1', REC_TOTAL_BLOCO_1, True, True) then
      (*13*) if not AC_f(ITO, 'REC_NRB_BLOCO_1', REC_NRB_BLOCO_1, True, False) then

      (*14*) if not AC_f(ITO, 'REC_TOTAL_PERIODO', REC_TOTAL_PERIODO, True, True) then
      (*15*) if not AC_f(ITO, 'REC_TOTAL_NRB_PERIODO', REC_TOTAL_NRB_PERIODO, True, False) then

      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_0);
  end;

  //MeGerado.Lines.Add('|0900|');
  FLin0900 := MeGerado.Lines.Count;
  //FQTD_LIN_0900 :=

end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_0990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := '0';
  FRegistro := '0990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_0', FQTD_LIN_0 + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_0);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_1001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := '1';
  FRegistro := '1001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_1);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_1010(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  IND_EXP, IND_CCRF, IND_COMB, IND_USINA, IND_VA, IND_EE, IND_CART, IND_FORM,
  IND_AER, IND_GIAF1, IND_GIAF3, IND_GIAF4, IND_REST_RESSARC_COMPL_ICMS: String;
begin
  Geral.MB_Info('Fazer Registro 1010?');
{
  FBloco := '1';
  FRegistro := '1010';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  IND_EXP                     := QrEFD_1010IND_EXP.Value;
  IND_CCRF                    := QrEFD_1010IND_CCRF.Value;
  IND_COMB                    := QrEFD_1010IND_COMB.Value;
  IND_USINA                   := QrEFD_1010IND_USINA.Value;
  IND_VA                      := QrEFD_1010IND_VA.Value;
  IND_EE                      := QrEFD_1010IND_EE.Value;
  IND_CART                    := QrEFD_1010IND_CART.Value;
  IND_FORM                    := QrEFD_1010IND_FORM.Value;
  IND_AER                     := QrEFD_1010IND_AER.Value;
  IND_GIAF1                   := QrEFD_1010IND_GIAF1.Value;
  IND_GIAF3                   := QrEFD_1010IND_GIAF3.Value;
  IND_GIAF4                   := QrEFD_1010IND_GIAF4.Value;
  IND_REST_RESSARC_COMPL_ICMS := QrEFD_1010IND_REST_RESSARC_COMPL_ICMS.Value;

  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_EXP',   IND_EXP) then
      (*03*) if not AC_x(ITO, 'IND_CCRF',  IND_CCRF) then
      (*04*) if not AC_x(ITO, 'IND_COMB',  IND_COMB) then
      (*05*) if not AC_x(ITO, 'IND_USINA', IND_USINA) then
      (*06*) if not AC_x(ITO, 'IND_VA',    IND_VA) then
      (*07*) if not AC_x(ITO, 'IND_EE',    IND_EE) then
      (*08*) if not AC_x(ITO, 'IND_CART',  IND_CART) then
      (*09*) if not AC_x(ITO, 'IND_FORM',  IND_FORM) then
      (*10*) if not AC_x(ITO, 'IND_AER',   IND_AER) then
      (*11*) if not AC_x(ITO, 'IND_GIAF1', IND_GIAF1) then
      (*12*) if not AC_x(ITO, 'IND_GIAF3', IND_GIAF3) then
      (*13*) if not AC_x(ITO, 'IND_GIAF4', IND_GIAF4) then
      (*14*) if not AC_x(ITO, 'IND_REST_RESSARC_COMPL_ICMS', IND_REST_RESSARC_COMPL_ICMS) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    MeErros.Lines.Add('ATEN��O!! Registro 1010 informado incompleto!!!');
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_1);
  end;
}
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_1100(): Boolean;
begin
//
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_1990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := '1';
  FRegistro := '1990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_1', FQTD_LIN_1 + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_1);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_9001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := '9';
  FRegistro := '9001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_9);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_9900(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  I: Integer;
begin
  FBloco := '9';
  FRegistro := '9900';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  AdicionaBLC(FREG_BLC_Count + 3); // 9900, 9990 e 9999
  AdicionaBLC(1, '9990');
  AdicionaBLC(1, '9999');
  for I := 0 to FREG_BLC_Count -1 do
  begin
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'REG_BLC', FREG_BLC_Fields[I]) then
        (*03*) if not AC_i(ITO, 'QTD_REG_BLC', FREG_BLC_Linhas[I], False, True) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_9);
    end;
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_9990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := '9';
  FRegistro := '9990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_9', FQTD_LIN_9 + 2, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_9);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_9999(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  Qtd_Lin: Integer;
begin
  FBloco := '9';
  FRegistro := '9999';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  Qtd_Lin := FQTD_LIN_0 + FQTD_LIN_A + FQTD_LIN_C + FQTD_LIN_D + FQTD_LIN_F +
  FQTD_LIN_I + FQTD_LIN_M + FQTD_LIN_P + FQTD_LIN_1 + FQTD_LIN_9;
  //
  if (Qtd_Lin <> FArqStr.Count) or (Qtd_lin <> MeGerado.Lines.Count)  then
  begin
    Geral.MB_ERRO('Contagem da quantidade de linhas no arquivo n�o confere!'
    + sLineBreak + 'AVISE A DERMATEK!');
    Exit;
  end;
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN', Qtd_Lin + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    Result := True;
  finally
    AdicionaLinha(Qtd_lin);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_A001(
  IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'A';
  FRegistro := 'A001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_A);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_A990: Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'A';
  FRegistro := 'A990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_A', FQTD_LIN_A + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_A);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_C001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'C';
  FRegistro := 'C001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_C010(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  CNPJ: String;
begin
  FBloco := 'C';
  FRegistro := 'C010';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  CNPJ := Geral.SoNumero_TT(QrEmpresaCNPJ_CPF.Value);
  //
  try
    PredefineLinha();
    FLinC010 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_N(ITO, 'CNPJ', CNPJ) then
      (*02*) if not AC_x(ITO, 'IND_ESCRI', '2') then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_C100_Emit(): Boolean;
const
  ModelosNF: array[00..03] of String = ('01', '55', '1B', '04');
var
  ITO: TIndicadorDoTipoDeOperacao;
  //
  CodEmit, CodDest, (*OutInn,*) tpNF,
  IND_OPER, IND_EMIT, COD_PART, COD_MOD, SER, CHV_NFE, IND_PGTO, IND_FRT: String;
  N, COD_SIT, NUM_DOC: Integer;
  DT_DOC, DT_E_S: TDateTime;
  EhE: Boolean;
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST: Double;
begin
  //Geral.MB_Teste(Geral.FF0(QrCabA_CFatID.Value));
  FBloco := 'C';
  FRegistro := 'C100';
  Result := False;
(* Configurado antes da chamada da procedure
  PB1.Max := 0;
  PB1.Position := 0;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  //COD_PART := ObtemCOD_PART(QrCabA_CCodInfoEmit.Value); // ??? Parei Aqui
  COD_PART := ObtemCOD_PART(QrCabA_CCodInfoDest.Value); // ??? Parei Aqui
  //
  ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
    //
  IND_EMIT := dmkPF.EscolhaDe2Str(EhE, '0', '1');
  COD_MOD := FormatFloat('00', QrCabA_Cide_mod.Value);
  if (COD_MOD <> '01') and (COD_MOD <> '55') then
    COD_MOD := QrCabA_CCOD_MOD.Value;
  if QrCabA_CNFG_Serie.Value <> '' then
    SER := QrCabA_CNFG_Serie.Value
  else SER := Geral.FF0(QrCabA_Cide_serie.Value );
  NUM_DOC := QrCabA_Cide_nNF.Value;
  COD_SIT := QrCabA_CCOD_SIT.Value;
  CHV_NFE := QrCabA_CID.Value;
  DT_DOC := QrCabA_Cide_dEmi.Value;
  if QrCabA_Cide_dSaiEnt.Value < QrCabA_Cide_dEmi.Value then
  begin
    AtualizaCampoNFeCabA('ide_dSaiEnt', QrCabA_Cide_dEmi.Value,
    QrCabA_CFatID.Value, QrCabA_CFatNum.Value, QrCabA_CEmpresa.Value);
    //DT_E_S := QrCabA_Cide_dEmi.Value
  end else
  begin
    //DT_E_S := QrCabA_Cide_dEmi.Value;
  end;
  DT_E_S := QrCabA_CDataFiscal.Value;
  //
  VL_DOC := QrCabA_CICMSTot_vNF.Value;
  IND_PGTO := SPED_Geral.Obtem_SPED_IND_PGTO_de_NFe_indPag(QrCabA_Cide_indPag.Value);
  VL_DESC := QrCabA_CICMSTot_vDesc.Value;
  VL_ABAT_NT := QrCabA_CVL_ABAT_NT.Value;
  VL_MERC := QrCabA_CICMSTot_vProd.Value;
  IND_FRT := SPED_Geral.Obtem_SPED_IND_FRT_de_NFe_modFrete(QrCabA_CmodFrete.Value);
  VL_FRT := QrCabA_CICMSTot_vFrete.Value;
  VL_SEG := QrCabA_CICMSTot_vSeg.Value;
  VL_OUT_DA := QrCabA_CICMSTot_vOutro.Value;
  VL_BC_ICMS := QrCabA_CICMSTot_vBC.Value;
  VL_ICMS := QrCabA_CICMSTot_vICMS.Value;
  VL_BC_ICMS_ST := QrCabA_CICMSTot_vBCST.Value;
  VL_ICMS_ST := QrCabA_CICMSTot_vST.Value;
  VL_IPI := QrCabA_CICMSTot_vIPI.Value;
  VL_PIS := QrCabA_CICMSTot_vPIS.Value;
  VL_COFINS := QrCabA_CICMSTot_vCOFINS.Value;
  VL_PIS_ST := QrCabA_CRetTrib_vRetPIS.Value;
  VL_COFINS_ST := QrCabA_CRetTrib_vRetCOFINS.Value;
  //
  try
    PredefineLinha();
    FLinC100 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_OPER', IND_OPER) then
      (*03*) if not AC_x(ITO, 'IND_EMIT', IND_EMIT) then
      (*04*) if not AC_x(ITO, 'COD_PART', COD_PART) then
      (*05*) if not AC_x(ITO, 'COD_MOD', COD_MOD) then
      (*06*) if not AC_i(ITO, 'COD_SIT', COD_SIT, True, True) then
      (*07*) if not AC_x(ITO, 'SER', SER) then
      (*08*) if not AC_i(ITO, 'NUM_DOC', NUM_DOC, False, True) then
      (*09*) if not AC_n(ITO, 'CHV_NFE', CHV_NFE) then
      (*10*) if not AC_d(ITO, 'DT_DOC', DT_DOC, False, False, 23) then
      (*11*) if not AC_d(ITO, 'DT_E_S', DT_E_S, True, False, 23) then
      (*12*) if not AC_f(ITO, 'VL_DOC', VL_DOC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*13*) if not AC_x(ITO, 'IND_PGTO', IND_PGTO) then
      (*14*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*15*) if not AC_f(ITO, 'VL_ABAT_NT', VL_ABAT_NT, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*16*) if not AC_f(ITO, 'VL_MERC', VL_MERC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*17*) if not AC_x(ITO, 'IND_FRT', IND_FRT) then
      (*18*) if not AC_f(ITO, 'VL_FRT', VL_FRT, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*19*) if not AC_f(ITO, 'VL_SEG', VL_SEG, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*20*) if not AC_f(ITO, 'VL_OUT_DA', VL_OUT_DA, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*21*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*22*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*23*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*24*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*25*) if not AC_f(ITO, 'VL_IPI', VL_IPI, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*26*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*27*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*28*) if not AC_f(ITO, 'VL_PIS_ST', VL_PIS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*29*) if not AC_f(ITO, 'VL_COFINS_ST', VL_COFINS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'FatID';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrCabA_CFatID.Value;
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'FatNum';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrCabA_CFatNum.Value;;
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'C010';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinC010;
    //
    if dmkPF.IndexOfArrStr(COD_MOD, ModelosNF) = -1 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerModFiscInval)(*FNumErro[14]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'COD_MOD'], True);
    end;
    //
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
  // Parei Aqui!!
 //TODO : notas denegadas e inutilizadas! }
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_C100_Entr(): Boolean;
const
  ModelosNF: array[00..03] of String = ('01', '55', '1B', '04');
var
  ITO: TIndicadorDoTipoDeOperacao;
  //
  CodEmit, CodDest, (*OutInn,*) tpNF,
  IND_OPER, IND_EMIT, COD_PART, COD_MOD, SER, CHV_NFE, IND_PGTO, IND_FRT: String;
  N, COD_SIT, NUM_DOC: Integer;
  DT_DOC, DT_E_S: TDateTime;
  EhE: Boolean;
  VL_DOC, VL_DESC, VL_ABAT_NT, VL_MERC, VL_FRT, VL_SEG, VL_OUT_DA, VL_BC_ICMS,
  VL_ICMS, VL_BC_ICMS_ST, VL_ICMS_ST, VL_IPI, VL_PIS, VL_COFINS, VL_PIS_ST,
  VL_COFINS_ST: Double;
begin
  FBloco := 'C';
  FRegistro := 'C100';
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnNFsCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM efdinnnfscab',
  'WHERE MovFatID=' + Geral.FF0(QrCabA_CAtrelaFatID.Value),
  'AND MovFatNum=' + Geral.FF0(QrCabA_CAtrelaFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrCabA_CEmpresa.Value),
  'AND CHV_NFE="' + QrCabA_CId.Value + '"',
  'ORDER BY Controle DESC ',
  '']);
  //Geral.MB_Teste(QrEfdInnNFsCab.SQL.Text); Desmarcar se necess�rio
  if QrEfdInnNFsCab.RecordCount = 0 then
  begin
    MeErros.Lines.Add(
    'NFe entrada uso e consumo: Lan�amento n�o encontrado: MovFatNum=' +
    Geral.FF0(QrCabA_CAtrelaFatNum.Value) + ' NFe ' +
    Geral.FF0(QrCabA_Cide_nNF.Value) + ' de ' +
    Geral.FDT(QrCabA_Cide_dEmi.Value, 2));
    Exit;
  end else
  if QrEfdInnNFsCab.RecordCount > 1 then
  begin
    MeErros.Lines.Add(
    'NFe entrada uso e consumo: Mais de uma entrada para o MovFatNum=' +
    Geral.FF0(QrCabA_CAtrelaFatNum.Value) + '. Considerado o �ltimo, Controle:' +
    Geral.FF0(QrEfdInnNFsCabControle.Value));
  end;


  //COD_PART := ObtemCOD_PART(QrCabA_CCodInfoEmit.Value);
  COD_PART := ObtemCOD_PART(QrEfdInnNFsCabTerceiro.Value);
  //
  //ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  ObtemITO_InNF(EhE, IND_OPER, tpNF, ITO);
  //
  IND_EMIT := '1'; //dmkPF.EscolhaDe2Str(EhE, '0', '1');
  //COD_MOD := FormatFloat('00', QrCabA_Cide_mod.Value);
  COD_MOD := Geral.FFN(QrEfdInnNFsCabCOD_MOD.Value, 2);
  (*if (COD_MOD <> '01') and (COD_MOD <> '55') then
    COD_MOD := QrCabA_CCOD_MOD.Value;
  if QrCabA_CNFG_Serie.Value <> '' then
    SER := QrCabA_CNFG_Serie.Value
  else SER := Geral.FF0(QrCabA_Cide_serie.Value );*)
  SER := Geral.FF0(QrEfdInnNFsCabSER.Value);
  //NUM_DOC := QrCabA_Cide_nNF.Value;
  NUM_DOC := QrEfdInnNFsCabNUM_DOC.Value;
  //COD_SIT := QrCabA_CCOD_SIT.Value;
  COD_SIT := QrEfdInnNFsCabCOD_SIT.Value;
  //CHV_NFE := QrCabA_CID.Value;
  CHV_NFE := QrEfdInnNFsCabCHV_NFE.Value;
  //DT_DOC := QrCabA_Cide_dEmi.Value;
  DT_DOC := QrEfdInnNFsCabDT_DOC.Value;
  DT_E_S := QrEfdInnNFsCabDT_E_S.Value;
  //
  //VL_DOC := QrCabA_CICMSTot_vNF.Value;
  VL_DOC := QrEfdInnNFsCabVL_DOC.Value;
  //IND_PGTO := SPED_Geral.Obtem_SPED_IND_PGTO_de_NFe_indPag(QrCabA_Cide_indPag.Value);
  IND_PGTO := QrEfdInnNFsCabIND_PGTO.Value;
  //VL_DESC := QrCabA_CICMSTot_vDesc.Value;
  VL_DESC := QrEfdInnNFsCabVL_DESC.Value;
  //VL_ABAT_NT := QrCabA_CVL_ABAT_NT.Value;
  VL_ABAT_NT    := QrEfdInnNFsCabVL_ABAT_NT.Value;
  VL_MERC       := QrEfdInnNFsCabVL_MERC.Value;
  IND_FRT       := SPED_Geral.Obtem_SPED_IND_FRT_de_NFe_modFrete(QrCabA_CmodFrete.Value);
  VL_FRT        := QrEfdInnNFsCabVL_FRT.Value;
  VL_SEG        := QrEfdInnNFsCabVL_SEG.Value;
  VL_OUT_DA     := QrEfdInnNFsCabVL_OUT_DA.Value;
  VL_BC_ICMS    := QrEfdInnNFsCabVL_BC_ICMS.Value;
  VL_ICMS       := QrEfdInnNFsCabVL_ICMS.Value;
  VL_BC_ICMS_ST := QrEfdInnNFsCabVL_BC_ICMS_ST.Value;
  VL_ICMS_ST    := QrEfdInnNFsCabVL_ICMS_ST.Value;
  VL_IPI        := QrEfdInnNFsCabVL_IPI.Value;
  VL_PIS        := QrEfdInnNFsCabVL_PIS.Value;
  VL_COFINS     := QrEfdInnNFsCabVL_COFINS.Value;
  VL_PIS_ST     := QrEfdInnNFsCabVL_PIS_ST.Value;
  VL_COFINS_ST  := QrEfdInnNFsCabVL_COFINS_ST.Value;
  //

//Campo 16 (VL_MERC) - Valida��o: se o campo COD_MOD for diferente de �55�, campo IND_EMIT for diferente de �0�
//e o campo COD_SIT for igual a �00� ou �01�, o valor informado no campo deve ser igual � soma do campo VL_ITEM dos
//registros C170 (�filhos� deste registro C100).

  VL_MERC := VL_MERC + VL_FRT + VL_SEG + VL_OUT_DA - VL_DESC;
  try
    PredefineLinha();
    FLinC100 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_OPER', IND_OPER) then
      (*03*) if not AC_x(ITO, 'IND_EMIT', IND_EMIT) then
      (*04*) if not AC_x(ITO, 'COD_PART', COD_PART) then
      (*05*) if not AC_x(ITO, 'COD_MOD', COD_MOD) then
      (*06*) if not AC_i(ITO, 'COD_SIT', COD_SIT, True, True) then
      (*07*) if not AC_x(ITO, 'SER', SER) then
      (*08*) if not AC_i(ITO, 'NUM_DOC', NUM_DOC, False, True) then
      (*09*) if not AC_n(ITO, 'CHV_NFE', CHV_NFE) then
      (*10*) if not AC_d(ITO, 'DT_DOC', DT_DOC, False, False, 23) then
      (*11*) if not AC_d(ITO, 'DT_E_S', DT_E_S, True, False, 23) then
      (*12*) if not AC_f(ITO, 'VL_DOC', VL_DOC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*13*) if not AC_x(ITO, 'IND_PGTO', IND_PGTO) then
      (*14*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*15*) if not AC_f(ITO, 'VL_ABAT_NT', VL_ABAT_NT, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*16*) if not AC_f(ITO, 'VL_MERC', VL_MERC, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*17*) if not AC_x(ITO, 'IND_FRT', IND_FRT) then
      (*18*) if not AC_f(ITO, 'VL_FRT', VL_FRT, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*19*) if not AC_f(ITO, 'VL_SEG', VL_SEG, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*20*) if not AC_f(ITO, 'VL_OUT_DA', VL_OUT_DA, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*21*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*22*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*23*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*24*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*25*) if not AC_f(ITO, 'VL_IPI', VL_IPI, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*26*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*27*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*28*) if not AC_f(ITO, 'VL_PIS_ST', VL_PIS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      (*29*) if not AC_f(ITO, 'VL_COFINS_ST', VL_COFINS_ST, True, VAR_INF_SPED_EFD_VAL_ZERO) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'FatID';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrCabA_CFatID.Value;
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'FatNum';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrCabA_CFatNum.Value;;
    //
    if dmkPF.IndexOfArrStr(COD_MOD, ModelosNF) = -1 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerModFiscInval)(*FNumErro[14]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'COD_MOD'], True);
    end;
    //
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
  // Parei Aqui!!
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_C170(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, IND_OPER: String;
  //
  N, NUM_ITEM, CST_ICMS, CFOP: Integer;
  COD_ITEM, DESCR_COMPL, UNID, IND_MOV, COD_NAT, IND_APUR, CST_IPI, COD_ENQ,
  CST_PIS, CST_COFINS, COD_CTA: String;
  QTD, VL_ITEM, VL_DESC, VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST, ALIQ_ST,
  VL_ICMS_ST, VL_BC_IPI, ALIQ_IPI, VL_IPI, VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
  VL_PIS, ALIQ_PIS_r, VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS, ALIQ_COFINS_r,
  VL_COFINS: Double;
  GrupoCFOP: Byte;
  TemErro: Boolean;
begin
  //Result := True; EXIT;
  //
  FBloco := 'C';
  FRegistro := 'C170';
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeEFD_C170, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfeefd_c170 ',
  'WHERE FatID=' + Geral.FF0(QrItsI_FatID.Value),
  'AND FatNum=' + Geral.FF0(QrItsI_FatNum.Value),
  'AND Empresa=' + Geral.FF0(QrItsI_Empresa.Value),
  'AND nItem=' + Geral.FF0(QrItsI_nItem.Value),
  '']);
  //
  NUM_ITEM       := QrItsI_nItem.Value;
  COD_ITEM       := QrNFeEFD_C170COD_ITEM.Value;
  DESCR_COMPL    := QrNFeEFD_C170DESCR_COMPL.Value;
  QTD            := QrNFeEFD_C170QTD.Value;
  UNID           := QrNFeEFD_C170UNID.Value;
  VL_ITEM        := QrNFeEFD_C170VL_ITEM.Value;
  VL_DESC        := QrNFeEFD_C170VL_DESC.Value;
  IND_MOV        := QrNFeEFD_C170IND_MOV.Value;
  CST_ICMS       := QrNFeEFD_C170CST_ICMS.Value;
  CFOP           := QrNFeEFD_C170CFOP.Value;
  COD_NAT        := QrNFeEFD_C170COD_NAT.Value;
  VL_BC_ICMS     := QrNFeEFD_C170VL_BC_ICMS.Value;
  ALIQ_ICMS      := QrNFeEFD_C170ALIQ_ICMS.Value;
  VL_ICMS        := QrNFeEFD_C170VL_ICMS.Value;
  VL_BC_ICMS_ST  := QrNFeEFD_C170VL_BC_ICMS_ST.Value;
  ALIQ_ST        := QrNFeEFD_C170ALIQ_ST.Value;
  VL_ICMS_ST     := QrNFeEFD_C170VL_ICMS_ST.Value;
  IND_APUR       := QrNFeEFD_C170IND_APUR.Value;
  CST_IPI        := QrNFeEFD_C170CST_IPI.Value;
  COD_ENQ        := QrNFeEFD_C170COD_ENQ.Value;
  VL_BC_IPI      := QrNFeEFD_C170VL_BC_IPI.Value;
  ALIQ_IPI       := QrNFeEFD_C170ALIQ_IPI.Value;
  VL_IPI         := QrNFeEFD_C170VL_IPI.Value;
  CST_PIS        := Geral.FF0_EmptyZero(QrNFeEFD_C170CST_PIS.Value);
  VL_BC_PIS      := QrNFeEFD_C170VL_BC_PIS.Value;
  ALIQ_PIS_p     := QrNFeEFD_C170ALIQ_PIS_p.Value;
  QUANT_BC_PIS   := QrNFeEFD_C170QUANT_BC_PIS .Value;
  ALIQ_PIS_r     := QrNFeEFD_C170ALIQ_PIS_r.Value;
  VL_PIS         := QrNFeEFD_C170VL_PIS.Value;
  CST_COFINS     := Geral.FF0_EmptyZero(QrNFeEFD_C170CST_COFINS.Value);
  VL_BC_COFINS   := QrNFeEFD_C170VL_BC_COFINS.Value;
  ALIQ_COFINS_p  := QrNFeEFD_C170ALIQ_COFINS_p.Value;
  QUANT_BC_COFINS:= QrNFeEFD_C170QUANT_BC_COFINS.Value;
  ALIQ_COFINS_r  := QrNFeEFD_C170ALIQ_COFINS_r.Value;
  VL_COFINS      := QrNFeEFD_C170VL_COFINS.Value;
  COD_CTA        := QrNFeEFD_C170COD_CTA.Value;
  //
  if (Trim(DESCR_COMPL) = '') and VAR_INF_SPED_EFD_NOME_DESCR_COMPL then
    DESCR_COMPL := QrItsI_prod_xProd.Value;
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'NUM_ITEM', NUM_ITEM, False, True, True) then
      (*03*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*04*) if not AC_x(ITO, 'DESCR_COMPL', DESCR_COMPL) then
      (*05*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      (*06*) if not AC_x(ITO, 'UNID', UNID) then
      (*07*) if not AC_f(ITO, 'VL_ITEM', VL_ITEM, True, True) then
      (*08*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, True) then
      (*09*) if not AC_x(ITO, 'IND_MOV', IND_MOV) then
      (*10*) if not AC_i(ITO, 'CST_ICMS', CST_ICMS, True, True) then
      (*11*) if not AC_i(ITO, 'CFOP', CFOP, False, True) then
      (*12*) if not AC_x(ITO, 'COD_NAT', COD_NAT) then
      (*13*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
      (*14*) if not AC_f(ITO, 'ALIQ_ICMS', ALIQ_ICMS, True, True) then
      (*15*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*16*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, True) then
      (*17*) if not AC_f(ITO, 'ALIQ_ST', ALIQ_ST, True, True) then
      (*18*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, True) then
      (*19*) if not AC_x(ITO, 'IND_APUR', IND_APUR) then
      (*20*) if not AC_x(ITO, 'CST_IPI', CST_IPI) then
      (*21*) if not AC_x(ITO, 'COD_ENQ', COD_ENQ) then
      (*22*) if not AC_f(ITO, 'VL_BC_IPI', VL_BC_IPI, True, True) then
      (*23*) if not AC_f(ITO, 'ALIQ_IPI', ALIQ_IPI, True, True) then
      (*24*) if not AC_f(ITO, 'VL_IPI', VL_IPI, True, True) then
      (*25*) if not AC_n(ITO, 'CST_PIS', CST_PIS) then
      (*26*) if not AC_f(ITO, 'VL_BC_PIS', VL_BC_PIS, True, True) then
      (*27*) if not AC_f(ITO, 'ALIQ_PIS', ALIQ_PIS_p, True, True) then
      (*28*) if not AC_f(ITO, 'QUANT_BC_PIS', QUANT_BC_PIS, True, True) then
      (*29*) if not AC_f(ITO, 'ALIQ_PIS_QUANT', ALIQ_PIS_r, True, True) then
      (*30*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, True) then
      (*31*) if not AC_n(ITO, 'CST_COFINS', CST_COFINS) then
      (*32*) if not AC_f(ITO, 'VL_BC_COFINS', VL_BC_COFINS, True, True) then
      (*33*) if not AC_f(ITO, 'ALIQ_COFINS', ALIQ_COFINS_p, True, True) then
      (*34*) if not AC_f(ITO, 'QUANT_BC_COFINS', QUANT_BC_COFINS, True, True) then
      (*35*) if not AC_f(ITO, 'ALIQ_COFINS_QUANT', ALIQ_COFINS_r, True, True) then
      (*36*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, True) then
      (*37*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCFOP, DModG.AllID_DB, [
    'SELECT Codigo ',
    'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
    'WHERE Codigo="' + Geral.FF0(CFOP) + '"',
    '']);
    if QrCFOP.RecordCount = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCFOPUnkn)(*FNumErro[13]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'CFOP'], True);
    end else
    begin
      GrupoCFOP := CFOP div 1000;
      case ITO of
        itoEntrada: TemErro := not (GrupoCFOP in ([1,2,3]));
        itoSaida: TemErro := not (GrupoCFOP in ([5,6,7]));
        else TemErro := True;
      end;
      if TemErro then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes',
        'Empresa', 'LinArq', 'REG', 'Campo'], [
        Integer(efdexerCFOPInval)(*FNumErro[15]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, 'CFOP'], True);
      end;
    end;
    //AdicionaBLC(1);
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'C100';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinC100;
    //
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'GraGruX';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrItsI_GraGruX.Value;
    //
    FQTD_LIN_C170 := FQTD_LIN_C170 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_C170_Emit(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, IND_OPER: String;
  //
  N, NUM_ITEM, CST_ICMS, CFOP: Integer;
  COD_ITEM, DESCR_COMPL, UNID, IND_MOV, COD_NAT, IND_APUR, CST_IPI, COD_ENQ,
  CST_PIS, CST_COFINS, COD_CTA: String;
  QTD, VL_ITEM, VL_DESC, VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST, ALIQ_ST,
  VL_ICMS_ST, VL_BC_IPI, ALIQ_IPI, VL_IPI, VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
  VL_PIS, ALIQ_PIS_r, VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS, ALIQ_COFINS_r,
  VL_COFINS, VL_ABAT_NT: Double;
  GrupoCFOP: Byte;
  TemErro: Boolean;
  //IndMov: Integer;
begin
  //Result := True; EXIT;
  //
  FBloco := 'C';
  FRegistro := 'C170';
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  //
  //
(*
  IndMov := DefineIND_MOV(QrCabA_CFisRegCad.Value, QrCabA_Cide_finNFe.Value, QrIprod_cProd.Value);
  if IndMov = 1 then
    FContaIND_MOV_1 := FContaIND_MOV_1 + 1;
*)

  //
  NUM_ITEM       := QrInItem.Value;
  if QrIFatID.Value = VAR_FATID_0001 then
    COD_ITEM       := QrIprod_cProd.Value
  else
    COD_ITEM       := Geral.FF0(QrIGraGruX.Value);
  //
  DESCR_COMPL    := '';//QrI.Value;
  QTD            := QrIprod_qCom.Value;
  UNID           := QrIprod_uCom.Value;
  VL_ITEM        := QrIprod_vProd.Value;
  VL_DESC        := QrIprod_vDesc.Value;
  //IND_MOV        := Geral.FF0(IndMov);
  IND_MOV        := Geral.FF0(QrIIND_MOV.Value);

  CST_ICMS       := (QrIICMS_Orig.Value * 100) + QrIICMS_CST.Value;
  CFOP           := QrIprod_CFOP.Value;
  if not FSelReg0400 then
    COD_NAT := ''
  else
    COD_NAT        := Geral.FF0(QrCabA_CFisRegCad.Value);
  VL_BC_ICMS     := QrIICMS_vBC.Value;
  ALIQ_ICMS      := QrIICMS_pICMS.Value;
  VL_ICMS        := QrIICMS_vICMS.Value;
  VL_BC_ICMS_ST  := QrIICMS_vBCST.Value;
  ALIQ_ST        := QrIICMS_pICMSST.Value;
  VL_ICMS_ST     := QrIICMS_vICMSST.Value;
  IND_APUR       := QrIIND_APUR.Value;
  CST_IPI        := Geral.FFN(QrIIPI_CST.Value, 2);
  COD_ENQ        := QrIIPI_cEnq.Value;
  VL_BC_IPI      := QrIIPI_vBC.Value;
  ALIQ_IPI       := QrIIPI_pIPI.Value;
  VL_IPI         := QrIIPI_vIPI.Value;
  CST_PIS        := Geral.FFN(QrIPIS_CST.Value, 2);
  VL_BC_PIS      := QrIPIS_vBC.Value;
  ALIQ_PIS_p     := QrIPIS_pPIS.Value;
  QUANT_BC_PIS   := 0.00;
  ALIQ_PIS_r     := 0.00;
  VL_PIS         := QrIPIS_vPIS.Value;
  CST_COFINS     := Geral.FFN(QrICOFINS_CST.Value, 2);
  VL_BC_COFINS   := QrICOFINS_vBC.Value;
  ALIQ_COFINS_p  := QrICOFINS_pCOFINS.Value;
  QUANT_BC_COFINS:= 0.000;
  ALIQ_COFINS_r  := 0.00;
  VL_COFINS      := QrICOFINS_vCOFINS.Value;
  COD_CTA        := '';
  VL_ABAT_NT     := 0.00;
  //
  if (Trim(DESCR_COMPL) = '') and VAR_INF_SPED_EFD_NOME_DESCR_COMPL then
    DESCR_COMPL := QrIprod_xProd.Value;
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'NUM_ITEM', NUM_ITEM, False, True, True) then
      (*03*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*04*) if not AC_x(ITO, 'DESCR_COMPL', DESCR_COMPL) then
      (*05*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      (*06*) if not AC_x(ITO, 'UNID', UNID) then
      (*07*) if not AC_f(ITO, 'VL_ITEM', VL_ITEM, True, True) then
      (*08*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, True) then
      (*09*) if not AC_x(ITO, 'IND_MOV', IND_MOV) then
      (*10*) if not AC_i(ITO, 'CST_ICMS', CST_ICMS, True, True) then
      (*11*) if not AC_i(ITO, 'CFOP', CFOP, False, True) then
      (*12*) if not AC_x(ITO, 'COD_NAT', COD_NAT) then
      (*13*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
      (*14*) if not AC_f(ITO, 'ALIQ_ICMS', ALIQ_ICMS, True, True) then
      (*15*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*16*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, True) then
      (*17*) if not AC_f(ITO, 'ALIQ_ST', ALIQ_ST, True, True) then
      (*18*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, True) then
      (*19*) if not AC_x(ITO, 'IND_APUR', IND_APUR) then
      (*20*) if not AC_x(ITO, 'CST_IPI', CST_IPI) then
      (*21*) if not AC_x(ITO, 'COD_ENQ', COD_ENQ) then
      (*22*) if not AC_f(ITO, 'VL_BC_IPI', VL_BC_IPI, True, True) then
      (*23*) if not AC_f(ITO, 'ALIQ_IPI', ALIQ_IPI, True, True) then
      (*24*) if not AC_f(ITO, 'VL_IPI', VL_IPI, True, True) then
      (*25*) if not AC_n(ITO, 'CST_PIS', CST_PIS) then
      (*26*) if not AC_f(ITO, 'VL_BC_PIS', VL_BC_PIS, True, False) then
      (*27*) if not AC_f(ITO, 'ALIQ_PIS', ALIQ_PIS_p, True, False) then
      (*28*) if not AC_f(ITO, 'QUANT_BC_PIS', QUANT_BC_PIS, True, False) then
      (*29*) if not AC_f(ITO, 'ALIQ_PIS_QUANT', ALIQ_PIS_r, True, False) then
      (*30*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, True) then
      (*31*) if not AC_n(ITO, 'CST_COFINS', CST_COFINS) then
      (*32*) if not AC_f(ITO, 'VL_BC_COFINS', VL_BC_COFINS, True, False) then
      (*33*) if not AC_f(ITO, 'ALIQ_COFINS', ALIQ_COFINS_p, True, False) then
      (*34*) if not AC_f(ITO, 'QUANT_BC_COFINS', QUANT_BC_COFINS, True, False) then
      (*35*) if not AC_f(ITO, 'ALIQ_COFINS_QUANT', ALIQ_COFINS_r, True, False) then
      (*36*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, True) then
      (*37*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCFOP, DModG.AllID_DB, [
    'SELECT Codigo ',
    'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
    'WHERE Codigo="' + Geral.FF0(CFOP) + '"',
    '']);
    if QrCFOP.RecordCount = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCFOPUnkn)(*FNumErro[13]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'CFOP'], True);
    end else
    begin
      GrupoCFOP := CFOP div 1000;
      case ITO of
        itoEntrada: TemErro := not (GrupoCFOP in ([1,2,3]));
        itoSaida: TemErro := not (GrupoCFOP in ([5,6,7]));
        else TemErro := True;
      end;
      if TemErro then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes',
        'Empresa', 'LinArq', 'REG', 'Campo'], [
        Integer(efdexerCFOPInval)(*FNumErro[15]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, 'CFOP'], True);
      end;
    end;
    //AdicionaBLC(1);
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'C100';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinC100;
    //
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'GraGruX';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrIGraGruX.Value;
    //
    FQTD_LIN_C170 := FQTD_LIN_C170 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_C170_Entr: Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, IND_OPER: String;
  //
  N, NUM_ITEM, CFOP: Integer;
  COD_ITEM, DESCR_COMPL, UNID, IND_MOV, COD_NAT, IND_APUR, CST_IPI, COD_ENQ,
  CST_PIS, CST_COFINS, COD_CTA, CST_ICMS: String;
  QTD, VL_ITEM, VL_DESC, VL_BC_ICMS, ALIQ_ICMS, VL_ICMS, VL_BC_ICMS_ST, ALIQ_ST,
  VL_ICMS_ST, VL_BC_IPI, ALIQ_IPI, VL_IPI, VL_BC_PIS, ALIQ_PIS_p, QUANT_BC_PIS,
  VL_PIS, ALIQ_PIS_r, VL_BC_COFINS, ALIQ_COFINS_p, QUANT_BC_COFINS, ALIQ_COFINS_r,
  VL_COFINS, VL_ABAT_NT: Double;
  GrupoCFOP: Byte;
  TemErro: Boolean;
begin
  //Result := True; EXIT;
  //
  FBloco := 'C';
  FRegistro := 'C170';
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  //ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  ObtemITO_InNF(EhE, IND_OPER, tpNF, ITO);
  NUM_ITEM       := QrEfdInnNFsIts.RecNo;
  COD_ITEM       := Geral.FF0(QrEfdInnNFsItsGraGruX.Value); // QrNFeEFD_C170COD_ITEM.Value;
  DESCR_COMPL    := ''; //ver o que fazer! QrNFeEFD_C170DESCR_COMPL.Value;
  QTD            := QrEfdInnNFsItsQTD            .Value; // QrNFeEFD_C170QTD.Value;
  UNID           := QrEfdInnNFsItsUNID           .Value; // QrNFeEFD_C170UNID.Value;
  VL_ITEM        := QrEfdInnNFsItsVL_ITEM        .Value; // QrNFeEFD_C170VL_ITEM.Value;
  VL_DESC        := QrEfdInnNFsItsVL_DESC        .Value; // QrNFeEFD_C170VL_DESC.Value;
  IND_MOV        := QrEfdInnNFsItsIND_MOV        .Value; // QrNFeEFD_C170IND_MOV.Value;
  CST_ICMS       := QrEfdInnNFsItsCST_ICMS       .Value; // QrNFeEFD_C170CST_ICMS.Value;
  CFOP           := QrEfdInnNFsItsCFOP           .Value; // QrNFeEFD_C170CFOP.Value;
  if not FSelReg0400 then
    COD_NAT := ''
  else
    COD_NAT        := QrEfdInnNFsItsCOD_NAT        .Value; // QrNFeEFD_C170COD_NAT.Value;
  VL_BC_ICMS     := QrEfdInnNFsItsVL_BC_ICMS     .Value; // QrNFeEFD_C170VL_BC_ICMS.Value;
  ALIQ_ICMS      := QrEfdInnNFsItsALIQ_ICMS      .Value; // QrNFeEFD_C170ALIQ_ICMS.Value;
  VL_ICMS        := QrEfdInnNFsItsVL_ICMS        .Value; // QrNFeEFD_C170VL_ICMS.Value;
  VL_BC_ICMS_ST  := QrEfdInnNFsItsVL_BC_ICMS_ST  .Value; // QrNFeEFD_C170VL_BC_ICMS_ST.Value;
  ALIQ_ST        := QrEfdInnNFsItsALIQ_ST        .Value; // QrNFeEFD_C170ALIQ_ST.Value;
  VL_ICMS_ST     := QrEfdInnNFsItsVL_ICMS_ST     .Value; // QrNFeEFD_C170VL_ICMS_ST.Value;
  IND_APUR       := QrEfdInnNFsItsIND_APUR       .Value; // QrNFeEFD_C170IND_APUR.Value;
  CST_IPI        := QrEfdInnNFsItsCST_IPI        .Value; // QrNFeEFD_C170CST_IPI.Value;
  COD_ENQ        := QrEfdInnNFsItsCOD_ENQ        .Value; // QrNFeEFD_C170COD_ENQ.Value;
  VL_BC_IPI      := QrEfdInnNFsItsVL_BC_IPI      .Value; // QrNFeEFD_C170VL_BC_IPI.Value;
  ALIQ_IPI       := QrEfdInnNFsItsALIQ_IPI       .Value; // QrNFeEFD_C170ALIQ_IPI.Value;
  VL_IPI         := QrEfdInnNFsItsVL_IPI         .Value; // QrNFeEFD_C170VL_IPI.Value;
  CST_PIS        := QrEfdInnNFsItsCST_PIS        .Value; // Geral.FF0_EmptyZero(QrNFeEFD_C170CST_PIS.Value);
  VL_BC_PIS      := QrEfdInnNFsItsVL_BC_PIS      .Value; // QrNFeEFD_C170VL_BC_PIS.Value;
  ALIQ_PIS_p     := QrEfdInnNFsItsALIQ_PIS_p     .Value; // QrNFeEFD_C170ALIQ_PIS_p.Value;
  QUANT_BC_PIS   := QrEfdInnNFsItsQUANT_BC_PIS   .Value; // QrNFeEFD_C170QUANT_BC_PIS .Value;
  ALIQ_PIS_r     := QrEfdInnNFsItsALIQ_PIS_r     .Value; // QrNFeEFD_C170ALIQ_PIS_r.Value;
  VL_PIS         := QrEfdInnNFsItsVL_PIS         .Value; // QrNFeEFD_C170VL_PIS.Value;
  CST_COFINS     := QrEfdInnNFsItsCST_COFINS     .Value; // Geral.FF0_EmptyZero(QrNFeEFD_C170CST_COFINS.Value);
  VL_BC_COFINS   := QrEfdInnNFsItsVL_BC_COFINS   .Value; // QrNFeEFD_C170VL_BC_COFINS.Value;
  ALIQ_COFINS_p  := QrEfdInnNFsItsALIQ_COFINS_p  .Value; // QrNFeEFD_C170ALIQ_COFINS_p.Value;
  QUANT_BC_COFINS:= QrEfdInnNFsItsQUANT_BC_COFINS.Value; // QrNFeEFD_C170QUANT_BC_COFINS.Value;
  ALIQ_COFINS_r  := QrEfdInnNFsItsALIQ_COFINS_r  .Value; // QrNFeEFD_C170ALIQ_COFINS_r.Value;
  VL_COFINS      := QrEfdInnNFsItsVL_COFINS      .Value; // QrNFeEFD_C170VL_COFINS.Value;
  COD_CTA        := QrEfdInnNFsItsCOD_CTA        .Value; // QrNFeEFD_C170COD_CTA.Value;
  // ini 2022-04-12
  VL_ABAT_NT     := QrEfdInnNFsItsVL_ABAT_NT     .Value;
  //if (Trim(DESCR_COMPL) = '') and VAR_INF_SPED_EFD_NOME_DESCR_COMPL then
  //  DESCR_COMPL := QrItsIprod_xProd.Value;
  // fim 2022-04-12
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;

      //if QrCamposCampo.Value = 'VL_BC_PIS' then
        //Geral.MB_Info('Ver aqui!');

      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'NUM_ITEM', NUM_ITEM, False, True, True) then
      (*03*) if not AC_x(ITO, 'COD_ITEM', COD_ITEM) then
      (*04*) if not AC_x(ITO, 'DESCR_COMPL', DESCR_COMPL) then
      (*05*) if not AC_f(ITO, 'QTD', QTD, True, True) then
      (*06*) if not AC_x(ITO, 'UNID', UNID) then
      (*07*) if not AC_f(ITO, 'VL_ITEM', VL_ITEM, True, True) then
      (*08*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, True) then
      (*09*) if not AC_x(ITO, 'IND_MOV', IND_MOV) then
      (*10*) if not AC_n(ITO, 'CST_ICMS', CST_ICMS) then
      (*11*) if not AC_i(ITO, 'CFOP', CFOP, False, True) then
      (*12*) if not AC_x(ITO, 'COD_NAT', COD_NAT) then
      (*13*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
      (*14*) if not AC_f(ITO, 'ALIQ_ICMS', ALIQ_ICMS, True, True) then
      (*15*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*16*) if not AC_f(ITO, 'VL_BC_ICMS_ST', VL_BC_ICMS_ST, True, True) then
      (*17*) if not AC_f(ITO, 'ALIQ_ST', ALIQ_ST, True, True) then
      (*18*) if not AC_f(ITO, 'VL_ICMS_ST', VL_ICMS_ST, True, True) then
      (*19*) if not AC_x(ITO, 'IND_APUR', IND_APUR) then
      (*20*) if not AC_x(ITO, 'CST_IPI', CST_IPI) then
      (*21*) if not AC_x(ITO, 'COD_ENQ', COD_ENQ) then
      (*22*) if not AC_f(ITO, 'VL_BC_IPI', VL_BC_IPI, True, True) then
      (*23*) if not AC_f(ITO, 'ALIQ_IPI', ALIQ_IPI, True, True) then
      (*24*) if not AC_f(ITO, 'VL_IPI', VL_IPI, True, True) then
      (*25*) if not AC_n(ITO, 'CST_PIS', CST_PIS) then
      (*26*) if not AC_f(ITO, 'VL_BC_PIS', VL_BC_PIS, True, False) then
      (*27*) if not AC_f(ITO, 'ALIQ_PIS', ALIQ_PIS_p, False, True) then
      (*28*) if not AC_f(ITO, 'QUANT_BC_PIS', QUANT_BC_PIS, True, False) then
      (*29*) if not AC_f(ITO, 'ALIQ_PIS_QUANT', ALIQ_PIS_r, True, False) then
      (*30*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, True) then
      (*31*) if not AC_n(ITO, 'CST_COFINS', CST_COFINS) then
      (*32*) if not AC_f(ITO, 'VL_BC_COFINS', VL_BC_COFINS, True, False) then
      (*33*) if not AC_f(ITO, 'ALIQ_COFINS', ALIQ_COFINS_p, False, True) then
      (*34*) if not AC_f(ITO, 'QUANT_BC_COFINS', QUANT_BC_COFINS, True, False) then
      (*35*) if not AC_f(ITO, 'ALIQ_COFINS_QUANT', ALIQ_COFINS_r, True, False) then
      (*36*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, False, True) then
      (*37*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCFOP, DModG.AllID_DB, [
    'SELECT Codigo ',
    'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
    'WHERE Codigo="' + Geral.FF0(CFOP) + '"',
    '']);
    if QrCFOP.RecordCount = 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
      'Erro'], [
      'ImporExpor', 'AnoMes', 'Empresa',
      'LinArq', 'REG', 'Campo'], [
      Integer(efdexerCFOPUnkn)(*FNumErro[13]*)], [
      FImporExpor, FAnoMes_Int, FEmpresa_Int,
      FLinArq, FRegistro, 'CFOP'], True);
    end else
    begin
      GrupoCFOP := CFOP div 1000;
      case ITO of
        itoEntrada: TemErro := not (GrupoCFOP in ([1,2,3]));
        itoSaida: TemErro := not (GrupoCFOP in ([5,6,7]));
        else TemErro := True;
      end;
      if TemErro then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
        'Erro'], [
        'ImporExpor', 'AnoMes',
        'Empresa', 'LinArq', 'REG', 'Campo'], [
        Integer(efdexerCFOPInval)(*FNumErro[15]*)], [
        FImporExpor, FAnoMes_Int, FEmpresa_Int,
        FLinArq, FRegistro, 'CFOP'], True);
      end;
    end;
    //AdicionaBLC(1);
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'C100';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinC100;
    //
    //
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'GraGruX';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := QrEfdInnNFsItsGraGruX.Value;
    //
    FQTD_LIN_C170 := FQTD_LIN_C170 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_C500(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  //
  COD_PART, COD_MOD, COD_SIT, SER, SUB: String;
  NUM_DOC: Integer;
  DT_DOC, DT_ENT: TDateTime;
  VL_DOC, VL_ICMS: Double;
  COD_INF: String;
  VL_PIS, VL_COFINS: Double;
  TP_LIGACAO, COD_GRUPO_TENSAO: String;
var
  CHV_DOCe, CHV_DOCe_REF, COD_CTA, HASH_DOC_REF, SER_DOC_REF: String;
  FIN_DOCe, IND_DEST, COD_MUN_DEST, COD_MOD_DOC_REF, NUM_DOC_REF, MES_DOC_REF: Integer;
  ENER_INJET, OUTRAS_DED: Double;
begin
  FBloco := 'C';
  FRegistro := 'C500';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  //COD_PART         := Geral.FF0(QrEfdInnC500CabTerceiro.Value);
  COD_PART         := ObtemCOD_PART(QrEfdInnC500CabTerceiro.Value);
  COD_MOD          := QrEfdInnC500CabCOD_MOD.Value;
  COD_SIT          := QrEfdInnC500CabCOD_SIT.Value;
  SER              := QrEfdInnC500CabSER.Value;
  SUB              := QrEfdInnC500CabSUB.Value;
  NUM_DOC          := QrEfdInnC500CabNUM_DOC.Value;
  DT_DOC           := QrEfdInnC500CabDT_DOC.Value;
  DT_ENT           := QrEfdInnC500CabDT_E_S.Value;
  VL_DOC           := QrEfdInnC500CabVL_DOC.Value;
  VL_ICMS          := QrEfdInnC500CabVL_ICMS.Value;
  COD_INF          := QrEfdInnC500CabCOD_INF.Value;
  VL_PIS           := QrEfdInnC500CabVL_PIS.Value;
  VL_COFINS        := QrEfdInnC500CabVL_COFINS.Value;
  CHV_DOCe         := QrEfdInnC500CabCHV_DOCe.Value;
  //

(*
  if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
*)
  ITO := itoEntrada;

  try
    PredefineLinha();
    FLinC500 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
       //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'COD_PART', COD_PART) then
      (*03*) if not AC_x(ITO, 'COD_MOD', COD_MOD) then
      (*04*) if not AC_n(ITO, 'COD_SIT', COD_SIT) then
      (*05*) if not AC_x(ITO, 'SER', SER) then
      (*06*) if not AC_n(ITO, 'SUB', SUB) then
      (*07*) if not AC_i(ITO, 'NUM_DOC', NUM_DOC, False, True) then
      (*08*) if not AC_d(ITO, 'DT_DOC', DT_DOC, False, True, 23) then
      (*09*) if not AC_d(ITO, 'DT_ENT', DT_ENT, False, True, 23) then
      (*10*) if not AC_f(ITO, 'VL_DOC', VL_DOC, True, True) then
      (*11*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*12*) if not AC_x(ITO, 'COD_INF', COD_INF) then
      (*13*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, True) then
      (*14*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, True) then
      (*15*) if not AC_n(ITO, 'CHV_DOCe', CHV_DOCe) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_C501(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, CST_PIS: String;
  //
  N: Integer;
  VL_ITEM, VL_BC_PIS, ALIQ_PIS, VL_PIS: Double;
  NAT_BC_CRED, COD_CTA: String;
begin
  FBloco := 'C';
  FRegistro := 'C501';
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  //ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  ITO := itoEntrada;
  //

  CST_PIS := QrEfdInnC500ItsPISCST_PIS.Value;
  VL_ITEM := QrEfdInnC500ItsPISVL_ITEM.Value;
  NAT_BC_CRED := QrEfdInnC500ItsPISNAT_BC_CRED.Value;
  VL_BC_PIS := QrEfdInnC500ItsPISVL_BC_PIS.Value;
  ALIQ_PIS := QrEfdInnC500ItsPISALIQ_PIS.Value;
  VL_PIS := QrEfdInnC500ItsPISVL_PIS.Value;
  COD_CTA := QrEfdInnC500ItsPISCOD_CTA.Value;
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_n(ITO, 'CST_PIS', CST_PIS(*, True, True*)) then
      (*03*) if not AC_f(ITO, 'VL_ITEM', VL_ITEM, True, True) then
      (*04*) if not AC_x(ITO, 'NAT_BC_CRED', NAT_BC_CRED) then
      (*05*) if not AC_f(ITO, 'VL_BC_PIS',  VL_BC_PIS, True, True) then
      (*06*) if not AC_f(ITO, 'ALIQ_PIS',  ALIQ_PIS, True, True) then
      (*07*) if not AC_f(ITO, 'VL_PIS',  VL_PIS, True, True) then
      (*08*) if not AC_x(ITO, 'COD_CTA',  COD_CTA) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'C500';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinC500;
    //
    FQTD_LIN_C501 := FQTD_LIN_C501 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_C505(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, CST_COFINS: String;
  //
  N: Integer;
  VL_ITEM, VL_BC_COFINS, ALIQ_COFINS, VL_COFINS: Double;
  NAT_BC_CRED, COD_CTA: String;
begin
  FBloco := 'C';
  FRegistro := 'C505';
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  //ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  ITO := itoEntrada;
  //

  CST_COFINS := QrEfdInnC500ItsCOFINSCST_COFINS.Value;
  VL_ITEM := QrEfdInnC500ItsCOFINSVL_ITEM.Value;
  NAT_BC_CRED := QrEfdInnC500ItsCOFINSNAT_BC_CRED.Value;
  VL_BC_COFINS := QrEfdInnC500ItsCOFINSVL_BC_COFINS.Value;
  ALIQ_COFINS := QrEfdInnC500ItsCOFINSALIQ_COFINS.Value;
  VL_COFINS := QrEfdInnC500ItsCOFINSVL_COFINS.Value;
  COD_CTA := QrEfdInnC500ItsCOFINSCOD_CTA.Value;
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_n(ITO, 'CST_COFINS', CST_COFINS(*, True, True*)) then
      (*03*) if not AC_f(ITO, 'VL_ITEM', VL_ITEM, True, True) then
      (*04*) if not AC_x(ITO, 'NAT_BC_CRED', NAT_BC_CRED) then
      (*05*) if not AC_f(ITO, 'VL_BC_COFINS',  VL_BC_COFINS, True, True) then
      (*06*) if not AC_f(ITO, 'ALIQ_COFINS',  ALIQ_COFINS, True, True) then
      (*07*) if not AC_f(ITO, 'VL_COFINS',  VL_COFINS, True, True) then
      (*08*) if not AC_x(ITO, 'COD_CTA',  COD_CTA) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'C500';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinC500;
    //
    FQTD_LIN_C505 := FQTD_LIN_C505 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_C990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'C';
  FRegistro := 'C990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_C', FQTD_LIN_C + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_C);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_D001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'D';
  FRegistro := 'D001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_D);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_D010(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  CNPJ: String;
begin
  FBloco := 'D';
  FRegistro := 'D010';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  CNPJ := Geral.SoNumero_TT(QrEmpresaCNPJ_CPF.Value);
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_N(ITO, 'CNPJ', CNPJ) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_D);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_D100(): Boolean;
  function GeraRegistro_D100_Atual(): Boolean;
  var
    ITO: TIndicadorDoTipoDeOperacao;
    //
    DT_DOC, DT_A_P: TDateTime;
    COD_SIT, TP_CTE, NUM_DOC: Integer;
    IND_OPER, IND_EMIT, CHV_CTE, CHV_CTE_REF, COD_INF, COD_CTA,
    COD_PART, COD_MOD, SER, SUB, IND_FRT: String;
    VL_DESC, VL_DOC, VL_SERV, VL_BC_ICMS, VL_ICMS, VL_NT: Double;
    COD_MUN_ORIG, COD_MUN_DEST: Integer;
    //
    N: Integer;
  begin
    FBloco := 'D';
    FRegistro := 'D100';
    Result := False;
    PB1.Max := 0;
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    IND_OPER    := QrEfdInnCTsCabIND_OPER.Value;
    IND_EMIT    := QrEfdInnCTsCabIND_EMIT.Value;
    //COD_PART    := Geral.FF0(QrEfdInnCTsCabTerceiro.Value);
    COD_PART    := ObtemCOD_PART(QrEfdInnCTsCabTerceiro.Value);
    COD_MOD     := QrEfdInnCTsCabCOD_MOD.Value;
    while Length(COD_MOD) < 2 do
      COD_MOD := '0' + COD_MOD;
    COD_SIT     := QrEfdInnCTsCabCOD_SIT.Value;
    SER         := QrEfdInnCTsCabSER.Value;
    SUB         := QrEfdInnCTsCabSUB.Value;
    NUM_DOC     := QrEfdInnCTsCabNUM_DOC.Value;
    CHV_CTE     := QrEfdInnCTsCabCHV_CTE.Value;
  //  DT_DOC      := Geral.IMV(Geral.FDT(QrEfdInnCTsCabDT_DOC.Value, 23));
  //  DT_A_P      := Geral.IMV(Geral.FDT(QrEfdInnCTsCabDT_A_P.Value, 23));
    DT_DOC      := QrEfdInnCTsCabDT_DOC.Value;
    DT_A_P      := QrEfdInnCTsCabDT_A_P.Value;
    //
    TP_CTE      := QrEfdInnCTsCabTP_CT_e.Value;
    CHV_CTE_REF := QrEfdInnCTsCabCHV_CTE_REF.Value;
    VL_DOC      := QrEfdInnCTsCabVL_DOC.Value;
    VL_DESC     := QrEfdInnCTsCabVL_DESC.Value;
    IND_FRT     := QrEfdInnCTsCabIND_FRT.Value;
    VL_SERV     := QrEfdInnCTsCabVL_SERV.Value;
    VL_BC_ICMS  := QrEfdInnCTsCabVL_BC_ICMS.Value;
    VL_ICMS     := QrEfdInnCTsCabVL_ICMS.Value;
    VL_NT       := QrEfdInnCTsCabVL_NT.Value;
    COD_INF     := QrEfdInnCTsCabCOD_INF.Value;
    COD_CTA     := QrEfdInnCTsCabCOD_CTA.Value;
    COD_MUN_ORIG := QrEfdInnCTsCabCOD_MUN_ORIG.Value;
    COD_MUN_DEST := QrEfdInnCTsCabCOD_MUN_DEST.Value;
     //
    if IND_OPER = '0' then
      ITO := itoEntrada
    else
      ITO := itoSaida;
   try
      PredefineLinha();
      FLinD100 := FLinArq;
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
         //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'IND_OPER', IND_OPER) then
        (*03*) if not AC_x(ITO, 'IND_EMIT', IND_EMIT) then
        (*04*) if not AC_x(ITO, 'COD_PART', COD_PART) then
        (*05*) if not AC_x(ITO, 'COD_MOD', COD_MOD) then
        (*06*) if not AC_n(ITO, 'COD_SIT', Geral.FFN(COD_SIT, 2)) then
        (*07*) if not AC_x(ITO, 'SER', SER) then
        (*08*) if not AC_x(ITO, 'SUB', SUB) then
        (*09*) if not AC_i(ITO, 'NUM_DOC', NUM_DOC, False, True) then
        (*10*) if not AC_n(ITO, 'CHV_CTE', CHV_CTE) then
        (*11*) if not AC_d(ITO, 'DT_DOC', DT_DOC, False, True, 23) then
        (*12*) if not AC_d(ITO, 'DT_A_P', DT_A_P, False, True, 23) then
        (*13*) if not AC_i(ITO, 'TP_CTE', TP_CTE, True, True) then
        (*14*) if not AC_n(ITO, 'CHV_CTE_REF', CHV_CTE_REF) then
        (*15*) if not AC_f(ITO, 'VL_DOC', VL_DOC, True, True) then
        (*16*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, True) then
        (*17*) if not AC_x(ITO, 'IND_FRT', IND_FRT) then
        (*18*) if not AC_f(ITO, 'VL_SERV', VL_SERV, True, True) then
        (*19*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
        (*20*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
        (*21*) if not AC_f(ITO, 'VL_NT', VL_NT, True, True) then
        (*22*) if not AC_x(ITO, 'COD_INF', COD_INF) then
        (*23*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
        (*24*) if not AC_i(ITO, 'COD_MUN_ORIG', COD_MUN_ORIG, True, True) then
        (*25*) if not AC_i(ITO, 'COD_MUN_DEST', COD_MUN_DEST, True, True) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
      //
      //AdicionaBLC(1);
      //
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'D010';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinD010;
      //
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_D);
    end;
  end;
  //
  function GeraRegistro_D101_Atual(): Boolean;
  var
    ITO: TIndicadorDoTipoDeOperacao;
    //
    IND_NAT_FRT, CST_PIS, NAT_BC_CRED, COD_CTA: String;
    VL_ITEM, VL_BC_PIS, ALIQ_PIS, VL_PIS: Double;
    //
    N: Integer;
  begin
    FBloco := 'D';
    FRegistro := 'D101';
    Result := False;
    PB1.Max := 0;
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    //D100      :=
    //
    IND_NAT_FRT := QrEfdInnCTsCabIND_NAT_FRT.Value;
    VL_ITEM     := QrEfdInnCTsCabVL_ITEM.Value;
    CST_PIS     := QrEfdInnCTsCabCST_PIS.Value;
    NAT_BC_CRED := QrEfdInnCTsCabNAT_BC_CRED.Value;
    COD_CTA     := QrEfdInnCTsCabCOD_CTA.Value;
    VL_BC_PIS   := QrEfdInnCTsCabVL_BC_PIS.Value;
    ALIQ_PIS    := QrEfdInnCTsCabALIQ_PIS.Value;
    VL_PIS      := QrEfdInnCTsCabVL_PIS.Value;
     //
    if QrEfdInnCTsCabIND_OPER.Value = '0' then
      ITO := itoEntrada
    else
      ITO := itoSaida;
   try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
         //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'IND_NAT_FRT', IND_NAT_FRT) then
        (*03*) if not AC_f(ITO, 'VL_ITEM', VL_ITEM, False, True) then
        (*04*) if not AC_n(ITO, 'CST_PIS', CST_PIS) then
        (*05*) if not AC_x(ITO, 'NAT_BC_CRED', NAT_BC_CRED) then
        (*06*) if not AC_f(ITO, 'VL_BC_PIS', VL_BC_PIS, True, True) then
        (*07*) if not AC_f(ITO, 'ALIQ_PIS', ALIQ_PIS, True, True) then
        (*08*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, True) then
        (*09*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
      //
      //AdicionaBLC(1);
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'D100';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinD100;
      //
      FQTD_LIN_D101 := FQTD_LIN_D101 + 1;
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_D);
    end;
  end;
  //
  function GeraRegistro_D105_Atual(): Boolean;
  var
    ITO: TIndicadorDoTipoDeOperacao;
    //
    IND_NAT_FRT, CST_COFINS, NAT_BC_CRED, COD_CTA: String;
    VL_ITEM, VL_BC_COFINS, ALIQ_COFINS, VL_COFINS: Double;
    //
    N: Integer;
  begin
    FBloco := 'D';
    FRegistro := 'D105';
    Result := False;
    PB1.Max := 0;
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
    //
    if not ReopenCampos(Result) then
      Exit
    else
      Result := False;
    //
    //D100      :=
    //
    IND_NAT_FRT  := QrEfdInnCTsCabIND_NAT_FRT.Value;
    VL_ITEM      := QrEfdInnCTsCabVL_ITEM.Value;
    CST_COFINS   := QrEfdInnCTsCabCST_COFINS.Value;
    NAT_BC_CRED  := QrEfdInnCTsCabNAT_BC_CRED.Value;
    COD_CTA      := QrEfdInnCTsCabCOD_CTA.Value;
    VL_BC_COFINS := QrEfdInnCTsCabVL_BC_COFINS.Value;
    ALIQ_COFINS  := QrEfdInnCTsCabALIQ_COFINS.Value;
    VL_COFINS    := QrEfdInnCTsCabVL_COFINS.Value;
     //
    if QrEfdInnCTsCabIND_OPER.Value = '0' then
      ITO := itoEntrada
    else
      ITO := itoSaida;
   try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
         //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'IND_NAT_FRT', IND_NAT_FRT) then
        (*03*) if not AC_f(ITO, 'VL_ITEM', VL_ITEM, False, True) then
        (*04*) if not AC_n(ITO, 'CST_COFINS', CST_COFINS) then
        (*05*) if not AC_x(ITO, 'NAT_BC_CRED', NAT_BC_CRED) then
        (*06*) if not AC_f(ITO, 'VL_BC_COFINS', VL_BC_COFINS, True, True) then
        (*07*) if not AC_f(ITO, 'ALIQ_COFINS', ALIQ_COFINS, True, True) then
        (*08*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, True) then
        (*09*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
      //
      //AdicionaBLC(1);
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'D100';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinD100;
      //
      FQTD_LIN_D105 := FQTD_LIN_D105 + 1;
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_D);
    end;
  end;
begin
  QrEfdInnCTsCab.First;
  while not QrEfdInnCTsCab.Eof do
  begin
    GeraRegistro_D100_Atual();
    GeraRegistro_D101_Atual();
    GeraRegistro_D105_Atual();
    //
    QrEfdInnCTsCab.Next;
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_D500(): Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  //
  COD_SIT: String;
  NUM_DOC: Integer;
  DT_DOC, DT_A_P: TDateTime;
  IND_OPER, IND_EMIT, COD_INF, COD_CTA,
  COD_PART, COD_MOD, SER, SUB: String;
  VL_DESC, VL_DOC, VL_SERV, VL_SERV_NT, VL_TERC, VL_DA, VL_BC_ICMS, VL_ICMS,
  VL_PIS, VL_COFINS: Double;
  TP_ASSINANTE: String;
begin
  FBloco := 'D';
  FRegistro := 'D500';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  IND_OPER     := QrEfdInnD500CabIND_OPER.Value;
  IND_EMIT     := QrEfdInnD500CabIND_EMIT.Value;
  //COD_PART     := Geral.FF0(QrEfdInnD500CabTerceiro.Value);
  COD_PART     := ObtemCOD_PART(QrEfdInnD500CabTerceiro.Value);
  COD_MOD      := QrEfdInnD500CabCOD_MOD.Value;
  COD_SIT      := QrEfdInnD500CabCOD_SIT.Value;
  SER          := QrEfdInnD500CabSER.Value;
  SUB          := QrEfdInnD500CabSUB.Value;
  NUM_DOC      := QrEfdInnD500CabNUM_DOC.Value;
  DT_DOC       := QrEfdInnD500CabDT_DOC.Value;
  DT_A_P       := QrEfdInnD500CabDT_A_P.Value;
  VL_DOC       := QrEfdInnD500CabVL_DOC.Value;
  VL_DESC      := QrEfdInnD500CabVL_DESC.Value;
  VL_SERV      := QrEfdInnD500CabVL_SERV.Value;
  VL_SERV_NT   := QrEfdInnD500CabVL_SERV_NT.Value;
  VL_TERC      := QrEfdInnD500CabVL_TERC.Value;
  VL_DA        := QrEfdInnD500CabVL_DA.Value;
  VL_BC_ICMS   := QrEfdInnD500CabVL_BC_ICMS.Value;
  VL_ICMS      := QrEfdInnD500CabVL_ICMS.Value;
  COD_INF      := QrEfdInnD500CabCOD_INF.Value;
  VL_PIS       := QrEfdInnD500CabVL_PIS.Value;
  VL_COFINS    := QrEfdInnD500CabVL_COFINS.Value;
  COD_CTA      := QrEfdInnD500CabCOD_CTA.Value;
  TP_ASSINANTE := QrEfdInnD500CabTP_ASSINANTE.Value;
   //
  if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
 try
    PredefineLinha();
    FLinD500 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
       //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'IND_OPER', IND_OPER) then
      (*03*) if not AC_x(ITO, 'IND_EMIT', IND_EMIT) then
      (*04*) if not AC_x(ITO, 'COD_PART', COD_PART) then
      (*05*) if not AC_x(ITO, 'COD_MOD', COD_MOD) then
      (*06*) if not AC_n(ITO, 'COD_SIT', COD_SIT) then
      (*07*) if not AC_x(ITO, 'SER', SER) then
      (*08*) if not AC_n(ITO, 'SUB', SUB) then
      (*09*) if not AC_i(ITO, 'NUM_DOC', NUM_DOC, False, True) then
      (*10*) if not AC_d(ITO, 'DT_DOC', DT_DOC, False, True, 23) then
      (*11*) if not AC_d(ITO, 'DT_A_P', DT_A_P, False, True, 23) then
      (*12*) if not AC_f(ITO, 'VL_DOC', VL_DOC, True, True) then
      (*13*) if not AC_f(ITO, 'VL_DESC', VL_DESC, True, True) then
      (*14*) if not AC_f(ITO, 'VL_SERV', VL_SERV, True, True) then
      (*15*) if not AC_f(ITO, 'VL_SERV_NT', VL_SERV_NT, True, True) then
      (*16*) if not AC_f(ITO, 'VL_TERC', VL_TERC, True, True) then
      (*17*) if not AC_f(ITO, 'VL_DA', VL_DA, True, True) then
      (*18*) if not AC_f(ITO, 'VL_BC_ICMS', VL_BC_ICMS, True, True) then
      (*19*) if not AC_f(ITO, 'VL_ICMS', VL_ICMS, True, True) then
      (*20*) if not AC_x(ITO, 'COD_INF', COD_INF) then
      (*21*) if not AC_f(ITO, 'VL_PIS', VL_PIS, True, True) then
      (*22*) if not AC_f(ITO, 'VL_COFINS', VL_COFINS, True, True) then
      (*23*) if not AC_x(ITO, 'COD_CTA', COD_CTA) then
      (*24*) if not AC_n(ITO, 'TP_ASSINANTE', TP_ASSINANTE) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_D);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_D501: Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, CST_PIS: String;
  //
  N: Integer;
  VL_ITEM, VL_BC_PIS, ALIQ_PIS, VL_PIS: Double;
  NAT_BC_CRED, COD_CTA: String;
begin
  FBloco := 'D';
  FRegistro := 'D501';
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  //ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  ITO := itoEntrada;
  //

  CST_PIS := QrEfdInnD500ItsPISCST_PIS.Value;
  VL_ITEM := QrEfdInnD500ItsPISVL_ITEM.Value;
  NAT_BC_CRED := QrEfdInnD500ItsPISNAT_BC_CRED.Value;
  VL_BC_PIS := QrEfdInnD500ItsPISVL_BC_PIS.Value;
  ALIQ_PIS := QrEfdInnD500ItsPISALIQ_PIS.Value;
  VL_PIS := QrEfdInnD500ItsPISVL_PIS.Value;
  COD_CTA := QrEfdInnD500ItsPISCOD_CTA.Value;
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_n(ITO, 'CST_PIS', CST_PIS(*, True, True*)) then
      (*03*) if not AC_f(ITO, 'VL_ITEM', VL_ITEM, True, True) then
      (*04*) if not AC_x(ITO, 'NAT_BC_CRED', NAT_BC_CRED) then
      (*05*) if not AC_f(ITO, 'VL_BC_PIS',  VL_BC_PIS, True, True) then
      (*06*) if not AC_f(ITO, 'ALIQ_PIS',  ALIQ_PIS, True, True) then
      (*07*) if not AC_f(ITO, 'VL_PIS',  VL_PIS, True, True) then
      (*08*) if not AC_x(ITO, 'COD_CTA',  COD_CTA) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'D500';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinD500;
    //
    FQTD_LIN_D501 := FQTD_LIN_D501 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_D);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_D505: Boolean;
var
  ITO: TIndicadorDoTipoDeOperacao;
  EhE: Boolean;
  tpNF, CST_COFINS: String;
  //
  N: Integer;
  VL_ITEM, VL_BC_COFINS, ALIQ_COFINS, VL_COFINS: Double;
  NAT_BC_CRED, COD_CTA: String;
begin
  FBloco := 'D';
  FRegistro := 'D505';
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  //ObtemITO_CabA(EhE, IND_OPER, tpNF, ITO);
  ITO := itoEntrada;
  //

  CST_COFINS := QrEfdInnD500ItsCOFINSCST_COFINS.Value;
  VL_ITEM := QrEfdInnD500ItsCOFINSVL_ITEM.Value;
  NAT_BC_CRED := QrEfdInnD500ItsCOFINSNAT_BC_CRED.Value;
  VL_BC_COFINS := QrEfdInnD500ItsCOFINSVL_BC_COFINS.Value;
  ALIQ_COFINS := QrEfdInnD500ItsCOFINSALIQ_COFINS.Value;
  VL_COFINS := QrEfdInnD500ItsCOFINSVL_COFINS.Value;
  COD_CTA := QrEfdInnD500ItsCOFINSCOD_CTA.Value;
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_n(ITO, 'CST_COFINS', CST_COFINS(*, True, True*)) then
      (*03*) if not AC_f(ITO, 'VL_ITEM', VL_ITEM, True, True) then
      (*04*) if not AC_x(ITO, 'NAT_BC_CRED', NAT_BC_CRED) then
      (*05*) if not AC_f(ITO, 'VL_BC_COFINS',  VL_BC_COFINS, True, True) then
      (*06*) if not AC_f(ITO, 'ALIQ_COFINS',  ALIQ_COFINS, True, True) then
      (*07*) if not AC_f(ITO, 'VL_COFINS',  VL_COFINS, True, True) then
      (*08*) if not AC_x(ITO, 'COD_CTA',  COD_CTA) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //AdicionaBLC(1);
    N := Length(FSQLCampos);
    SetLength(FSQLCampos, N + 1);
    FSQLCampos[N] := 'D500';
    SetLength(FValCampos, N + 1);
    FValCampos[N] := FLinD500;
    //
    FQTD_LIN_D505 := FQTD_LIN_D505 + 1;
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_D);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_D990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'D';
  FRegistro := 'D990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_D', FQTD_LIN_D + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_D);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_F001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'F';
  FRegistro := 'F001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_F);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_F990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'F';
  FRegistro := 'F990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_F', FQTD_LIN_F + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_F);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_I001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'I';
  FRegistro := 'I001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_I);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_I990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'I';
  FRegistro := 'I990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_I', FQTD_LIN_I + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_I);
  end;
end;


function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_M001(
  IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'M';
  FRegistro := 'M001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_M);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_M200(): Boolean;
const
  ITO = itoNaoAplicavel;
var
 VL_TOT_CONT_NC_PER, VL_TOT_CRED_DESC, VL_TOT_CRED_DESC_ANT, VL_TOT_CONT_NC_DEV,
 VL_RET_NC, VL_OUT_DED_NC, VL_CONT_NC_REC, VL_TOT_CONT_CUM_PER,
 VL_RET_CUM, VL_OUT_DED_CUM, VL_CONT_CUM_REC, VL_TOT_CONT_REC,
 _VL_TOT_CONT_NC_PER, _VL_TOT_CONT_CUM_PER: Double;
begin
  FBloco := 'M';
  FRegistro := 'M200';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  _VL_TOT_CONT_NC_PER := 0.00;
  QrSumM210.First;
  while not QrSumM210.Eof do
  begin
    if Geral.IMV(Geral.SoNumero_TT(QrSumM210COD_CONT.Value)) in ([1, 2, 3, 4, 32, 71]) then
      _VL_TOT_CONT_NC_PER := _VL_TOT_CONT_NC_PER + QrSumM210VL_CONT_PER.Value;
    //
    if Geral.IMV(Geral.SoNumero_TT(QrSumM210COD_CONT.Value)) in ([31, 32, 51, 52, 53, 54, 72]) then
      _VL_TOT_CONT_CUM_PER := _VL_TOT_CONT_CUM_PER + QrSumM210VL_CONT_PER.Value;
    //
    QrSumM210.Next;
  end;
  QrSumM210.First;  // vai usar depois novamente!

  VL_TOT_CONT_NC_PER    := _VL_TOT_CONT_NC_PER;
  VL_TOT_CRED_DESC      := 0.00; //ver! VL_TOT_CRED_DESC
  VL_TOT_CRED_DESC_ANT  := 0.00; //ver! VL_TOT_CRED_DESC_ANT
  VL_TOT_CONT_NC_DEV    := VL_TOT_CONT_NC_PER - VL_TOT_CRED_DESC - VL_TOT_CRED_DESC_ANT;
  VL_RET_NC             := 0.00; //ver! VL_RET_NC
  VL_OUT_DED_NC         := 0.00; //ver! VL_OUT_DED_NC
  VL_CONT_NC_REC        := VL_TOT_CONT_NC_DEV - VL_RET_NC - VL_OUT_DED_NC;
  VL_TOT_CONT_CUM_PER   :=_VL_TOT_CONT_CUM_PER;
  VL_RET_CUM            := 0.00; //ver! VL_RET_CUM
  VL_OUT_DED_CUM        := 0.00; //ver! VL_OUT_DED_CUM
  VL_CONT_CUM_REC       := _VL_TOT_CONT_CUM_PER - VL_RET_CUM - VL_OUT_DED_CUM;
  VL_TOT_CONT_REC       := VL_CONT_NC_REC + VL_CONT_CUM_REC;
  //
  try
    PredefineLinha();
    FLinM200 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_f(ITO, 'VL_TOT_CONT_NC_PER'  , VL_TOT_CONT_NC_PER  , True, True) then
      (*03*) if not AC_f(ITO, 'VL_TOT_CRED_DESC'    , VL_TOT_CRED_DESC    , True, True) then
      (*04*) if not AC_f(ITO, 'VL_TOT_CRED_DESC_ANT', VL_TOT_CRED_DESC_ANT, True, True) then
      (*05*) if not AC_f(ITO, 'VL_TOT_CONT_NC_DEV'  , VL_TOT_CONT_NC_DEV  , True, True) then
      (*06*) if not AC_f(ITO, 'VL_RET_NC'           , VL_RET_NC           , True, True) then
      (*07*) if not AC_f(ITO, 'VL_OUT_DED_NC'       , VL_OUT_DED_NC       , True, True) then
      (*08*) if not AC_f(ITO, 'VL_CONT_NC_REC'      , VL_CONT_NC_REC      , True, True) then
      (*09*) if not AC_f(ITO, 'VL_TOT_CONT_CUM_PER' , VL_TOT_CONT_CUM_PER , True, True) then
      (*10*) if not AC_f(ITO, 'VL_RET_CUM'          , VL_RET_CUM          , True, True) then
      (*11*) if not AC_f(ITO, 'VL_OUT_DED_CUM'      , VL_OUT_DED_CUM      , True, True) then
      (*12*) if not AC_f(ITO, 'VL_CONT_CUM_REC'     , VL_CONT_CUM_REC     , True, True) then
      (*13*) if not AC_f(ITO, 'VL_TOT_CONT_REC'     , VL_TOT_CONT_REC     , True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_M);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_M205(): Boolean;
const
  ITO = itoNaoAplicavel;
  sProcName = 'FmEfdPisCofinsExporta_v01_35.GeraRegistro_M205()';
var
  Nc, NUM_CAMPO, COD_REC: String;
  N, M200: Integer;
  VL_DEBITO, _VL_TOT_CONT_NC_PER, _VL_TOT_CONT_CUM_PER: Double;
  //
  procedure GeraRegistro_M205_Atual();
  begin
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'NUM_CAMPO'  , NUM_CAMPO) then
        (*03*) if not AC_x(ITO, 'COD_REC'    , COD_REC  ) then
        (*04*) if not AC_f(ITO, 'VL_DEBITO'  , VL_DEBITO, True, True) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
      //AdicionaBLC(1);
      FQTD_LIN_M205 := FQTD_LIN_M205 + 1;
      //
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'M200';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinM200;
      //
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_M);
    end;
  end;

begin
  FBloco := 'M';
  FRegistro := 'M205';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  QrSumM210.First;
  while not QrSumM210.Eof do
  begin
    if Geral.IMV(Geral.SoNumero_TT(QrSumM210COD_CONT.Value)) in ([1, 2, 3, 4, 32, 71]) then
      _VL_TOT_CONT_NC_PER := _VL_TOT_CONT_NC_PER + QrSumM210VL_CONT_PER.Value;
    //
    if Geral.IMV(Geral.SoNumero_TT(QrSumM210COD_CONT.Value)) in ([31, 32, 51, 52, 53, 54, 72]) then
      _VL_TOT_CONT_CUM_PER := _VL_TOT_CONT_CUM_PER + QrSumM210VL_CONT_PER.Value;
    //
    QrSumM210.Next;
  end;
  QrSumM210.First;  // vai usar depois novamente!
  //
  if _VL_TOT_CONT_NC_PER > 0 then
  begin
    Nc := '08';
    (*case DModG.QrParamsSPEDRegimPisCofins.Value of
      0: Geral.MB_Aviso('"Regime PIS/COFINS" n�o informado nos par�metros da filial!');
      // N�o cumulativo
      1: Nc := '08';
      // Cumulativo
      2: Nc := '12';
      else Geral.MB_Aviso('"Regime PIS/COFINS" n�o implementado em ' + sProcName);
    end;*)
    //
    NUM_CAMPO      := Nc;
    COD_REC        := DModG.QrParamsSPEDCodDctfPis.Value;
    VL_DEBITO      := _VL_TOT_CONT_NC_PER;
    //
    GeraRegistro_M205_Atual();
  end;
  //
  //
  if _VL_TOT_CONT_CUM_PER > 0 then
  begin
    Nc := '12';
    (*case DModG.QrParamsSPEDRegimPisCofins.Value of
      0: Geral.MB_Aviso('"Regime PIS/COFINS" n�o informado nos par�metros da filial!');
      // N�o cumulativo
      1: Nc := '08';
      // Cumulativo
      2: Nc := '12';
      else Geral.MB_Aviso('"Regime PIS/COFINS" n�o implementado em ' + sProcName);
    end;*)
    //
    NUM_CAMPO      := Nc;
    COD_REC        := DModG.QrParamsSPEDCodDctfPis.Value;
    VL_DEBITO      := _VL_TOT_CONT_CUM_PER;
    //
    GeraRegistro_M205_Atual();
  end;
  //
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_M210(): Boolean;
const
  ITO = itoNaoAplicavel;
  sProcName = 'TFmEfdPisCofinsExporta_v01_35.GeraRegistro_M210()';
var
  COD_CONT: String;
  N, M200: Integer;
  VL_REC_BRT, VL_BC_CONT, VL_AJUS_ACRES_BC_PIS, VL_AJUS_REDUC_BC_PIS,
  VL_BC_CONT_AJUS, ALIQ_PIS, QUANT_BC_PIS, ALIQ_PIS_QUANT, VL_CONT_APUR,
  VL_AJUS_ACRES, VL_AJUS_REDUC, VL_CONT_DIFER, VL_CONT_DIFER_ANT, VL_CONT_PER:
  Double;
  //
  procedure GeraRegistro_M210_Atual();
  begin
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        //if QrCamposCampo.Value = 'VL_AJUS_ACRES_BC_PIS' then
          //Geral.MB_Teste('VL_AJUS_ACRES_BC_PIS');
        //
        (*01*) if not AC_x(ITO, 'REG',                  FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_CONT',             COD_CONT) then
        (*03*) if not AC_f(ITO, 'VL_REC_BRT',           VL_REC_BRT, True, True) then
        (*04*) if not AC_f(ITO, 'VL_BC_CONT',           VL_BC_CONT, True, True) then
        (*05*) if not AC_f(ITO, 'VL_AJUS_ACRES_BC_PIS', VL_AJUS_ACRES_BC_PIS, True, True) then
        (*06*) if not AC_f(ITO, 'VL_AJUS_REDUC_BC_PIS', VL_AJUS_REDUC_BC_PIS, True, True) then
        (*07*) if not AC_f(ITO, 'VL_BC_CONT_AJUS',      VL_BC_CONT_AJUS, True, True) then
        (*08*) if not AC_f(ITO, 'ALIQ_PIS',             ALIQ_PIS, True, True) then
        (*09*) if not AC_f(ITO, 'QUANT_BC_PIS',         QUANT_BC_PIS, True, False) then
        (*10*) if not AC_f(ITO, 'ALIQ_PIS_QUANT',       ALIQ_PIS_QUANT, True, False) then
        (*11*) if not AC_f(ITO, 'VL_CONT_APUR',         VL_CONT_APUR, True, True) then
        (*12*) if not AC_f(ITO, 'VL_AJUS_ACRES',        VL_AJUS_ACRES, True, True) then
        (*13*) if not AC_f(ITO, 'VL_AJUS_REDUC',        VL_AJUS_REDUC, True, True) then
        (*14*) if not AC_f(ITO, 'VL_CONT_DIFER',        VL_CONT_DIFER, True, True) then
        (*15*) if not AC_f(ITO, 'VL_CONT_DIFER_ANT',    VL_CONT_DIFER_ANT, True, True) then
        (*16*) if not AC_f(ITO, 'VL_CONT_PER',          VL_CONT_PER, True, True) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
      //AdicionaBLC(1);
      FQTD_LIN_M210 := FQTD_LIN_M210 + 1;
      //
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'M200';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinM200;
      //
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_M);
    end;
  end;
begin
  FBloco := 'M';
  FRegistro := 'M210';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  QrSumM210.First;
  while not QrSumM210.Eof do
  begin
    COD_CONT              := QrSumM210COD_CONT.Value;
    VL_REC_BRT            := QrSumM210VL_ITEM.Value;
    VL_BC_CONT            := QrSumM210VL_BC_PIS.Value;
    VL_AJUS_ACRES_BC_PIS  := 0.00; // ver!!   QrSumM210VL_AJUS_ACRES_BC_PIS.Value;
    VL_AJUS_REDUC_BC_PIS  := 0.00; // ver!!   QrSumM210VL_AJUS_REDUC_BC_PIS.Value;
    VL_BC_CONT_AJUS       := VL_BC_CONT + VL_AJUS_ACRES_BC_PIS - VL_AJUS_REDUC_BC_PIS;
    ALIQ_PIS              := QrSumM210ALIQ_PIS.Value;
    QUANT_BC_PIS          := QrSumM210QUANT_BC_PIS.Value;
    ALIQ_PIS_QUANT        := QrSumM210ALIQ_PIS_QUANT.Value;
    VL_CONT_APUR          := QrSumM210VL_CONT_APUR.Value;
    VL_AJUS_ACRES         := 0.00; // ver!!   QrSumM210VL_AJUS_ACRES.Value;
    VL_AJUS_REDUC         := 0.00; // ver!!   QrSumM210VL_AJUS_REDUC.Value;
    VL_CONT_DIFER         := 0.00; // ver!!   QrSumM210VL_CONT_DIFER.Value;
    VL_CONT_DIFER_ANT     := 0.00; // ver!!   QrSumM210VL_CONT_DIFER_ANT.Value;
    VL_CONT_PER           := QrSumM210VL_CONT_PER.Value;
    //
    GeraRegistro_M210_Atual();
    //
    QrSumM210.Next;
  end;
  QrSumM210.First;  // vai usar depois novamente!

  //
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_M600(): Boolean;
const
  ITO = itoNaoAplicavel;
var
 VL_TOT_CONT_NC_PER, VL_TOT_CRED_DESC, VL_TOT_CRED_DESC_ANT, VL_TOT_CONT_NC_DEV,
 VL_RET_NC, VL_OUT_DED_NC, VL_CONT_NC_REC, VL_TOT_CONT_CUM_PER,
 VL_RET_CUM, VL_OUT_DED_CUM, VL_CONT_CUM_REC, VL_TOT_CONT_REC,
 _VL_TOT_CONT_NC_PER, _VL_TOT_CONT_CUM_PER: Double;
begin
  FBloco := 'M';
  FRegistro := 'M600';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  _VL_TOT_CONT_NC_PER := 0.00;
  QrSumM610.First;
  while not QrSumM610.Eof do
  begin
    if Geral.IMV(Geral.SoNumero_TT(QrSumM610COD_CONT.Value)) in ([1, 2, 3, 4, 32, 71]) then
      _VL_TOT_CONT_NC_PER := _VL_TOT_CONT_NC_PER + QrSumM610VL_CONT_PER.Value;
    //
    if Geral.IMV(Geral.SoNumero_TT(QrSumM610COD_CONT.Value)) in ([31, 32, 51, 52, 53, 54, 72]) then
      _VL_TOT_CONT_CUM_PER := _VL_TOT_CONT_CUM_PER + QrSumM610VL_CONT_PER.Value;
    //
    QrSumM610.Next;
  end;
  QrSumM610.First;  // vai usar depois novamente!

  VL_TOT_CONT_NC_PER    := _VL_TOT_CONT_NC_PER;
  VL_TOT_CRED_DESC      := 0.00; //ver! VL_TOT_CRED_DESC
  VL_TOT_CRED_DESC_ANT  := 0.00; //ver! VL_TOT_CRED_DESC_ANT
  VL_TOT_CONT_NC_DEV    := VL_TOT_CONT_NC_PER - VL_TOT_CRED_DESC - VL_TOT_CRED_DESC_ANT;
  VL_RET_NC             := 0.00; //ver! VL_RET_NC
  VL_OUT_DED_NC         := 0.00; //ver! VL_OUT_DED_NC
  VL_CONT_NC_REC        := VL_TOT_CONT_NC_DEV - VL_RET_NC - VL_OUT_DED_NC;
  VL_TOT_CONT_CUM_PER   :=_VL_TOT_CONT_CUM_PER;
  VL_RET_CUM            := 0.00; //ver! VL_RET_CUM
  VL_OUT_DED_CUM        := 0.00; //ver! VL_OUT_DED_CUM
  VL_CONT_CUM_REC       := _VL_TOT_CONT_CUM_PER - VL_RET_CUM - VL_OUT_DED_CUM;
  VL_TOT_CONT_REC       := VL_CONT_NC_REC + VL_CONT_CUM_REC;
  //
  try
    PredefineLinha();
    FLinM600 := FLinArq;
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_f(ITO, 'VL_TOT_CONT_NC_PER'  , VL_TOT_CONT_NC_PER  , True, True) then
      (*03*) if not AC_f(ITO, 'VL_TOT_CRED_DESC'    , VL_TOT_CRED_DESC    , True, True) then
      (*04*) if not AC_f(ITO, 'VL_TOT_CRED_DESC_ANT', VL_TOT_CRED_DESC_ANT, True, True) then
      (*05*) if not AC_f(ITO, 'VL_TOT_CONT_NC_DEV'  , VL_TOT_CONT_NC_DEV  , True, True) then
      (*06*) if not AC_f(ITO, 'VL_RET_NC'           , VL_RET_NC           , True, True) then
      (*07*) if not AC_f(ITO, 'VL_OUT_DED_NC'       , VL_OUT_DED_NC       , True, True) then
      (*08*) if not AC_f(ITO, 'VL_CONT_NC_REC'      , VL_CONT_NC_REC      , True, True) then
      (*09*) if not AC_f(ITO, 'VL_TOT_CONT_CUM_PER' , VL_TOT_CONT_CUM_PER , True, True) then
      (*10*) if not AC_f(ITO, 'VL_RET_CUM'          , VL_RET_CUM          , True, True) then
      (*11*) if not AC_f(ITO, 'VL_OUT_DED_CUM'      , VL_OUT_DED_CUM      , True, True) then
      (*12*) if not AC_f(ITO, 'VL_CONT_CUM_REC'     , VL_CONT_CUM_REC     , True, True) then
      (*13*) if not AC_f(ITO, 'VL_TOT_CONT_REC'     , VL_TOT_CONT_REC     , True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_M);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_M605(): Boolean;
const
  ITO = itoNaoAplicavel;
  sProcName = 'FmEfdPisCofinsExporta_v01_35.GeraRegistro_M605()';
var
  Nc, NUM_CAMPO, COD_REC: String;
  N, M600: Integer;
  VL_DEBITO, _VL_TOT_CONT_NC_PER, _VL_TOT_CONT_CUM_PER: Double;
  //
  procedure GeraRegistro_M605_Atual();
  begin
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        (*01*) if not AC_x(ITO, 'REG', FRegistro) then
        (*02*) if not AC_x(ITO, 'NUM_CAMPO'  , NUM_CAMPO) then
        (*03*) if not AC_x(ITO, 'COD_REC'    , COD_REC  ) then
        (*04*) if not AC_f(ITO, 'VL_DEBITO'  , VL_DEBITO, True, True) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
      //AdicionaBLC(1);
      FQTD_LIN_M605 := FQTD_LIN_M605 + 1;
      //
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'M600';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinM600;
      //
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_M);
    end;
  end;

begin
  FBloco := 'M';
  FRegistro := 'M605';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  QrSumM610.First;
  while not QrSumM610.Eof do
  begin
    if Geral.IMV(Geral.SoNumero_TT(QrSumM610COD_CONT.Value)) in ([1, 2, 3, 4, 32, 71]) then
      _VL_TOT_CONT_NC_PER := _VL_TOT_CONT_NC_PER + QrSumM610VL_CONT_PER.Value;
    //
    if Geral.IMV(Geral.SoNumero_TT(QrSumM610COD_CONT.Value)) in ([31, 32, 51, 52, 53, 54, 72]) then
      _VL_TOT_CONT_CUM_PER := _VL_TOT_CONT_CUM_PER + QrSumM610VL_CONT_PER.Value;
    //
    QrSumM610.Next;
  end;
  QrSumM610.First;  // vai usar depois novamente!
  //
  if _VL_TOT_CONT_NC_PER > 0 then
  begin
    Nc := '08';
    (*case DModG.QrParamsSPEDRegimPisCofins.Value of
      0: Geral.MB_Aviso('"Regime COFINS/COFINS" n�o informado nos par�metros da filial!');
      // N�o cumulativo
      1: Nc := '08';
      // Cumulativo
      2: Nc := '12';
      else Geral.MB_Aviso('"Regime COFINS/COFINS" n�o implementado em ' + sProcName);
    end;*)
    //
    NUM_CAMPO      := Nc;
    COD_REC        := DModG.QrParamsSPEDCodDctfCofins.Value;
    VL_DEBITO      := _VL_TOT_CONT_NC_PER;
    //
    GeraRegistro_M605_Atual();
  end;
  //
  //
  if _VL_TOT_CONT_CUM_PER > 0 then
  begin
    Nc := '12';
    (*case DModG.QrParamsSPEDRegimPisCofins.Value of
      0: Geral.MB_Aviso('"Regime COFINS/COFINS" n�o informado nos par�metros da filial!');
      // N�o cumulativo
      1: Nc := '08';
      // Cumulativo
      2: Nc := '12';
      else Geral.MB_Aviso('"Regime COFINS/COFINS" n�o implementado em ' + sProcName);
    end;*)
    //
    NUM_CAMPO      := Nc;
    COD_REC        := DModG.QrParamsSPEDCodDctfCofins.Value;
    VL_DEBITO      := _VL_TOT_CONT_CUM_PER;
    //
    GeraRegistro_M605_Atual();
  end;
  //
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_M610(): Boolean;
const
  ITO = itoNaoAplicavel;
  sProcName = 'TFmEfdPisCofinsExporta_v01_35.GeraRegistro_M610()';
var
  COD_CONT: String;
  N, M600: Integer;
  VL_REC_BRT, VL_BC_CONT, VL_AJUS_ACRES_BC_COFINS, VL_AJUS_REDUC_BC_COFINS,
  VL_BC_CONT_AJUS, ALIQ_COFINS, QUANT_BC_COFINS, ALIQ_COFINS_QUANT, VL_CONT_APUR,
  VL_AJUS_ACRES, VL_AJUS_REDUC, VL_CONT_DIFER, VL_CONT_DIFER_ANT, VL_CONT_PER:
  Double;
  //
  procedure GeraRegistro_M610_Atual();
  begin
    try
      PredefineLinha();
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        if CampoNaoConfere() then
          Exit;
        //
        //if QrCamposCampo.Value = 'VL_AJUS_ACRES_BC_COFINS' then
          //Geral.MB_Teste('VL_AJUS_ACRES_BC_COFINS');
        //
        (*01*) if not AC_x(ITO, 'REG',                  FRegistro) then
        (*02*) if not AC_x(ITO, 'COD_CONT',             COD_CONT) then
        (*03*) if not AC_f(ITO, 'VL_REC_BRT',           VL_REC_BRT, True, True) then
        (*04*) if not AC_f(ITO, 'VL_BC_CONT',           VL_BC_CONT, True, True) then
        (*05*) if not AC_f(ITO, 'VL_AJUS_ACRES_BC_COFINS', VL_AJUS_ACRES_BC_COFINS, True, True) then
        (*06*) if not AC_f(ITO, 'VL_AJUS_REDUC_BC_COFINS', VL_AJUS_REDUC_BC_COFINS, True, True) then
        (*07*) if not AC_f(ITO, 'VL_BC_CONT_AJUS',      VL_BC_CONT_AJUS, True, True) then
        (*08*) if not AC_f(ITO, 'ALIQ_COFINS',             ALIQ_COFINS, True, True) then
        (*09*) if not AC_f(ITO, 'QUANT_BC_COFINS',         QUANT_BC_COFINS, True, False) then
        (*10*) if not AC_f(ITO, 'ALIQ_COFINS_QUANT',       ALIQ_COFINS_QUANT, True, False) then
        (*11*) if not AC_f(ITO, 'VL_CONT_APUR',         VL_CONT_APUR, True, True) then
        (*12*) if not AC_f(ITO, 'VL_AJUS_ACRES',        VL_AJUS_ACRES, True, True) then
        (*13*) if not AC_f(ITO, 'VL_AJUS_REDUC',        VL_AJUS_REDUC, True, True) then
        (*14*) if not AC_f(ITO, 'VL_CONT_DIFER',        VL_CONT_DIFER, True, True) then
        (*15*) if not AC_f(ITO, 'VL_CONT_DIFER_ANT',    VL_CONT_DIFER_ANT, True, True) then
        (*16*) if not AC_f(ITO, 'VL_CONT_PER',          VL_CONT_PER, True, True) then
        begin
          if MensagemCampoDesconhecido() then Exit;
        end;
        QrCampos.Next;
      end;
      //
      //AdicionaBLC(1);
      FQTD_LIN_M610 := FQTD_LIN_M610 + 1;
      //
      N := Length(FSQLCampos);
      SetLength(FSQLCampos, N + 1);
      FSQLCampos[N] := 'M600';
      SetLength(FValCampos, N + 1);
      FValCampos[N] := FLinM600;
      //
      Result := True;
    finally
      AdicionaLinha(FQTD_LIN_M);
    end;
  end;
begin
  FBloco := 'M';
  FRegistro := 'M610';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  QrSumM610.First;
  while not QrSumM610.Eof do
  begin
    COD_CONT                 := QrSumM610COD_CONT.Value;
    VL_REC_BRT               := QrSumM610VL_ITEM.Value;
    VL_BC_CONT               := QrSumM610VL_BC_COFINS.Value;
    VL_AJUS_ACRES_BC_COFINS  := 0.00; // ver!!   QrSumM610VL_AJUS_ACRES_BC_COFINS.Value;
    VL_AJUS_REDUC_BC_COFINS  := 0.00; // ver!!   QrSumM610VL_AJUS_REDUC_BC_COFINS.Value;
    VL_BC_CONT_AJUS          := VL_BC_CONT + VL_AJUS_ACRES_BC_COFINS - VL_AJUS_REDUC_BC_COFINS;
    ALIQ_COFINS              := QrSumM610ALIQ_COFINS.Value;
    QUANT_BC_COFINS          := QrSumM610QUANT_BC_COFINS.Value;
    ALIQ_COFINS_QUANT        := QrSumM610ALIQ_COFINS_QUANT.Value;
    VL_CONT_APUR             := QrSumM610VL_CONT_APUR.Value;
    VL_AJUS_ACRES            := 0.00; // ver!!   QrSumM610VL_AJUS_ACRES.Value;
    VL_AJUS_REDUC            := 0.00; // ver!!   QrSumM610VL_AJUS_REDUC.Value;
    VL_CONT_DIFER            := 0.00; // ver!!   QrSumM610VL_CONT_DIFER.Value;
    VL_CONT_DIFER_ANT        := 0.00; // ver!!   QrSumM610VL_CONT_DIFER_ANT.Value;
    VL_CONT_PER              := QrSumM610VL_CONT_PER.Value;
    //
    GeraRegistro_M610_Atual();
    //
    QrSumM610.Next;
  end;
  QrSumM610.First;  // vai usar depois novamente!

  //
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_M990: Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'M';
  FRegistro := 'M990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_M', FQTD_LIN_M + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_M);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_P001(
  IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'P';
  FRegistro := 'P001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_P);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_P990: Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'P';
  FRegistro := 'P990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_P', FQTD_LIN_P + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_P);
  end;
end;

{
function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_H001(IND_MOV: Integer): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'H';
  FRegistro := 'H001';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'IND_MOV', IND_MOV, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_H);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_H005: Boolean;
const
  ITO = itoNaoAplicavel;
var
  DT_INV: TDateTime;
  VL_INV: Double;
  MOT_INV: String;
begin
  FBloco := 'H';
  FRegistro := 'H005';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  DT_INV   := QrEFD_H005DT_INV.Value;
  VL_INV   := QrEFD_H005VL_INV.Value;
  MOT_INV  := QrEFD_H005MOT_INV.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_d(ITO, 'DT_INV', DT_INV, False, False, 23) then
      (*03*) if not AC_f(ITO, 'VL_INV', VL_INV, True, True) then
      (*04*) if not AC_x(ITO, 'MOT_INV', MOT_INV) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    //
    //
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_H);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_H010(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  COD_ITEM, UNID, IND_PROP, COD_PART, TXT_COMPL, COD_CTA: String;
  H005: Integer;
  QTD, VL_UNIT, VL_ITEM, VL_ITEM_IR: Double;
  SQLType: TSQLType;
begin
  FBloco := 'H';
  FRegistro := 'H010';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  COD_ITEM       := ObtemCOD_ITEM(Geral.IMV(QrEFD_H010COD_ITEM.Value));
  UNID           := QrEFD_H010UNID.Value;
  QTD            := QrEFD_H010QTD.Value;
  VL_UNIT        := QrEFD_H010VL_UNIT.Value;
  VL_ITEM        := QrEFD_H010VL_ITEM.Value;
  IND_PROP       := QrEFD_H010IND_PROP.Value;
  //COD_PART       := QrEFD_H010COD_PART.Value;
  COD_PART       := ObtemCOD_PART(Geral.IMV(QrEFD_H010COD_PART.Value));
  TXT_COMPL      := QrEFD_H010TXT_COMPL.Value;
  COD_CTA        := QrEFD_H010COD_CTA.Value;
  H005           := QrEFD_H010H005.Value;
  VL_ITEM_IR     := QrEFD_H010VL_ITEM_IR.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_x(ITO, 'COD_ITEM'  , COD_ITEM) then
      (*03*) if not AC_x(ITO, 'UNID'      , UNID) then
      (*04*) if not AC_f(ITO, 'QTD'       , QTD, True, True) then
      (*05*) if not AC_f(ITO, 'VL_UNIT'   , VL_UNIT, True, True) then
      (*06*) if not AC_f(ITO, 'VL_ITEM'   , VL_ITEM, True, True) then
      (*07*) if not AC_x(ITO, 'IND_PROP'  , IND_PROP) then
      (*08*) if not AC_x(ITO, 'COD_PART'  , COD_PART) then
      (*09*) if not AC_x(ITO, 'TXT_COMPL' , TXT_COMPL) then
      (*10*) if not AC_x(ITO, 'COD_CTA'   , COD_CTA) then
      (*11*) if not AC_f(ITO, 'VL_ITEM_IR', VL_ITEM_IR, True, True) then
      (****) if not AC_i(ITO, 'H005'      , H005, False, False) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    FQTD_LIN_H010 := FQTD_LIN_H010 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_H);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_H020(): Boolean;
const
  ITO = itoNaoAplicavel;
var
  H010, CST_ICMS: Integer;
  BC_ICMS, VL_ICMS: Double;
  SQLType: TSQLType;
begin
  FBloco := 'H';
  FRegistro := 'H020';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  CST_ICMS       := QrEFD_H020CST_ICMS.Value;
  BC_ICMS        := QrEFD_H020BC_ICMS.Value;
  VL_ICMS        := QrEFD_H020VL_ICMS.Value;
  H010           := QrEFD_H020H010.Value;
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'CST_ICMS'  , CST_ICMS, False, False) then
      (*03*) if not AC_f(ITO, 'BC_ICMS'   , BC_ICMS, True, True) then
      (*04*) if not AC_f(ITO, 'VL_ICMS'   , VL_ICMS, True, True) then
      (****) if not AC_i(ITO, 'H010'      , H010, False, False) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    FQTD_LIN_H020 := FQTD_LIN_H020 + 1;
    //AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_H);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.GeraRegistro_H990(): Boolean;
const
  ITO = itoNaoAplicavel;
begin
  FBloco := 'H';
  FRegistro := 'H990';
  Result := False;
  PB1.Max := 0;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando registro ' + FRegistro);
  //
  if not ReopenCampos(Result) then
    Exit
  else
    Result := False;
  //
  // sem configura��o de campos ?
  //
  try
    PredefineLinha();
    QrCampos.First;
    while not QrCampos.Eof do
    begin
      if CampoNaoConfere() then
        Exit;
      //
      (*01*) if not AC_x(ITO, 'REG', FRegistro) then
      (*02*) if not AC_i(ITO, 'QTD_LIN_H', FQTD_LIN_H + 1, True, True) then
      begin
        if MensagemCampoDesconhecido() then Exit;
      end;
      QrCampos.Next;
    end;
    //
    AdicionaBLC(1);
    Result := True;
  finally
    AdicionaLinha(FQTD_LIN_H);
  end;
end;
}

procedure TFmEfdPisCofinsExporta_v01_35.Grava_SpedEfdPisCofinsErrs(Erro: TEFDExpErr; Campo: String);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(Erro)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, Campo], True);
end;

procedure TFmEfdPisCofinsExporta_v01_35.HabilitaBotaoOK;
begin
  BtOK.Enabled := (EdEmpresa.ValueVariant <> 0);
end;

procedure TFmEfdPisCofinsExporta_v01_35.InfoPosMeGerado();
var
  L, C: Integer;
  //MudouLinha: Boolean;
  //Linha: String;
  //
  //Registro, Campo: Integer;
  //SubTipo: Char;
  //DescriCampo, ConteudoCampo, ValorCampo: String;
begin
  //MudouLinha := False;
  L := Geral.RichRow(MeGerado) + 1;
  C := Geral.RichCol(MeGerado) + 1;
  StatusBar.Panels[1].Text := Format('L: %3d   C: %3d', [L, C]);
  //
end;

function TFmEfdPisCofinsExporta_v01_35.LetraObrigatoriedadeDoCampo(ITO: TIndicadorDoTipoDeOperacao): String;
begin
  Result := '?';
  case ITO of
    itoNaoAplicavel: Result := QrCamposCObrig.Value;
    itoEntrada:
    begin
      Result := QrCamposEObrig.Value;
      if Result = EmptyStr then
        Result := QrCamposCObrig.Value;
    end;
    itoSaida:
    begin
      Result := QrCamposSObrig.Value;
      if Result = EmptyStr then
        Result := QrCamposCObrig.Value;
    end;
    itoAmbos:
    begin
      Result := QrCamposCObrig.Value;
    end;
  end;
end;

procedure TFmEfdPisCofinsExporta_v01_35.MeGeradoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  InfoPosMeGerado();
end;

procedure TFmEfdPisCofinsExporta_v01_35.MeGeradoMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  InfoPosMeGerado();
end;

procedure TFmEfdPisCofinsExporta_v01_35.MeGeradoMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  InfoPosMeGerado();
end;

function  TFmEfdPisCofinsExporta_v01_35.MensagemCampoDesconhecido(): Boolean;
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerUnknown)(*FNumErro[1]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
  Result := False;
end;

procedure TFmEfdPisCofinsExporta_v01_35.MensagemCampoIncorreto(tpEsper: String);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerTipoFldIncorr)(*FNumErro[4]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

function TFmEfdPisCofinsExporta_v01_35.MensagemCampoNaoEhNumero: Boolean;
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerErrFldAlfa)(*FNumErro[12]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

procedure TFmEfdPisCofinsExporta_v01_35.MensagemCampoObrigatorio();
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerObrigSemCont)(*FNumErro[2]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

procedure TFmEfdPisCofinsExporta_v01_35.MensagemDecimaisIncorreto(tpEsper: String);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'],  [
  Integer(efdexerDifDecimais)(*[FNumErro[5]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

procedure TFmEfdPisCofinsExporta_v01_35.MensagemDocumentoInvalido(Doc: String);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerDocInvalido)(*FNumErro[6]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

procedure TFmEfdPisCofinsExporta_v01_35.MensagemTamanhoDifereDoObrigatorio(Valor: String);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerTamTxtDif)(*FNumErro[7]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

procedure TFmEfdPisCofinsExporta_v01_35.MensagemTamanhoMuitoGrande(Valor: String);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
  'Erro'], [
  'ImporExpor', 'AnoMes', 'Empresa',
  'LinArq', 'REG', 'Campo'], [
  Integer(efdexerTamTxtExce)(*FNumErro[8]*)], [
  FImporExpor, FAnoMes_Int, FEmpresa_Int,
  FLinArq, FRegistro, QrCamposCampo.Value], True);
  //
end;

function TFmEfdPisCofinsExporta_v01_35.NaoTemErros(Avisa: Boolean): Boolean;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando exist�ncia de erros em registros!');
  UnDmkDAC_PF.AbreMySQLQuery0(QrSpedEfdPisCofinsErrs, Dmod.MyDB, [
  'SELECT *  ',
  'FROM spedefdpiscofinserrs ',
  'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
  'AND AnoMes=' + Geral.FF0(FAnoMes_Int),
  'AND Empresa=' + Geral.FF0(FEmpresa_Int),
  '']);
  Result := QrSpedEfdPisCofinsErrs.RecordCount = 0;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  if not Result then
  begin
    if DBCheck.CriaFm(TFmEfdPisCofinsErros_v01_35, FmEfdPisCofinsErros_v01_35, afmoLiberado) then
    begin
      FmEfdPisCofinsErros_v01_35.FImporExpor := FImporExpor;
      FmEfdPisCofinsErros_v01_35.FAnoMes     := FAnoMes_Int;
      FmEfdPisCofinsErros_v01_35.FEmpresa    := FEmpresa_Int;
      FmEfdPisCofinsErros_v01_35.FDataIni    := FDataIni_Txt;
      FmEfdPisCofinsErros_v01_35.FDataFim    := FDataFim_Txt;
      FmEfdPisCofinsErros_v01_35.FEmprTXT    := FEmpresa_Txt;
      FmEfdPisCofinsErros_v01_35.ReopenErros();
      //
      FmEfdPisCofinsErros_v01_35.ShowModal;
      FmEfdPisCofinsErros_v01_35.Destroy;
    end;
  end else
  if Avisa then
    Geral.MB_Aviso('N�o h� reportagem de erros para o per�odo selecionado!');
end;

function TFmEfdPisCofinsExporta_v01_35.ObrigatoriedadeConfere(const LODC: String;
  var IndicadorPreenchimento: TIndicadorPreenchimento): Boolean;
begin
  if LODC = 'N' then
  begin
    Result := True;
    IndicadorPreenchimento := TIndicadorPreenchimento.ipcNaoPreencher;
  end
  else
  if CampoObrigatorio(LODC) then
  begin
    Result := True;
    IndicadorPreenchimento := TIndicadorPreenchimento.ipcObrigatorio;
  end else
  if LODC = 'OC' then
  begin
    Result := True;
    IndicadorPreenchimento := TIndicadorPreenchimento.ipcCondicional;
  end else
  begin
    Result := False;
    IndicadorPreenchimento := TIndicadorPreenchimento.ipcIndefinido;
  end;
  //
  if not Result then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'spedefdpiscofinserrs', False, [
    'Erro'], [
    'ImporExpor', 'AnoMes', 'Empresa',
    'LinArq', 'REG', 'Campo'], [
    Integer(efdexerTipObrigUnkn)(*FNumErro[9]*)], [
    FImporExpor, FAnoMes_Int, FEmpresa_Int,
    FLinArq, FRegistro, QrCamposCampo.Value], True);
    //
    Result := True;
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.ObtemCOD_ITEM(GraGruX: Integer): String;
const
  Campo = 'COD_ITEM...';
var
  Valor: String;
  //
begin
  if GraGruX = 0 then
    Geral.MB_Erro('GraGuuX = 0');
  if UnNFe_PF.ObtemSPEDEFD_COD_ITEM(GraGruX,
  DmSPED.QrParamsEmpSPED_EFD_ID_0200.Value, Result) then
  begin
    Valor := 'Reduzido c�digo ' + Geral.FF0(GraGruX);
    SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.ObtemCOD_PART(Entidade: Integer): String;
const
  Campo = 'COD_PART...';
var
  COD_PART, Valor: String;
begin
  Result := '0';
  if not UnNFe_PF.ObtemSPEDEFD_COD_PART(Entidade,
  DmSPED.QrParamsEmpSPED_EFD_ID_0150.Value, COD_PART) then
  begin
    Valor  := 'Entidade c�digo ' + Geral.FF0(Entidade);
    SPED_Geral.VDom(FBloco, FRegistro, Campo, Valor, MeErros);
  end else
    Result := COD_PART;
end;

function TFmEfdPisCofinsExporta_v01_35.ObtemITO_CabA(var EhE: Boolean; var IND_OPER, tpNF:
  String; var ITO: TIndicadorDoTipoDeOperacao): Boolean;
begin
  EhE := QrCabA_CCodInfoEmit.Value = FEmpresa_Int;
  tpNF := Geral.FF0(QrCabA_Cide_tpNF.Value);
  IND_OPER := dmkPF.EscolhaDe2Str(EhE, tpNF, '0');
  if IND_OPER = '0' then
    ITO := itoEntrada
  else
    ITO := itoSaida;
end;

function TFmEfdPisCofinsExporta_v01_35.ObtemITO_InNF(var EhE: Boolean;
  var IND_OPER, tpNF: String; var ITO: TIndicadorDoTipoDeOperacao): Boolean;
begin
  EhE := QrEfdInnNFsCabEmpresa.Value = FEmpresa_Int;
  tpNF := '0'; // Tipo de Opera��o da NF-e: 0=Entrada; 1=Sa�da
  IND_OPER := dmkPF.EscolhaDe2Str(EhE, tpNF, '0');
  ITO := itoEntrada;
end;

function TFmEfdPisCofinsExporta_v01_35.PodeGerarBloco(Bloco: String): Boolean;
begin
  QrBlocos.Close;
  QrBlocos.Database := DmodG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(QrBlocos, DmodG.MyPID_DB, [
'SELECT * ',
'FROM ' + FSPEDEFD_Blcs,
'WHERE Registro="' + Bloco + '"',
//'AND Ativo=1',
'AND StateIndex IN (1,3)',
'']);
  Result := QrBlocos.RecordCount > 0;
end;

procedure TFmEfdPisCofinsExporta_v01_35.PredefineLinha();
begin
  FLinStr := '|';
  FLinArq := FLinArq + 1;
  //
  SetLength(FSQLCampos, 1);
  FSQLCampos[0] := 'LinArq';
  //
  SetLength(FValCampos, 1);
  FValCampos[0] := FLinArq;
  //
  SetLength(FSQLIndex, 3);
  SetLength(FValIndex, 3);
  //
  FSQLIndex[0] := 'ImporExpor';
  FValIndex[0] := FImporExpor;
  //
  FSQLIndex[1] := 'AnoMes';
  FValIndex[1] := FAnoMes_Int;
  ///
  FSQLIndex[2] := 'Empresa';
  FValIndex[2] := FEmpresa_Int;
end;

function TFmEfdPisCofinsExporta_v01_35.ReCriaNoh(const Noh: TTreeNode; const nN: String;
  const cN: Integer; const Ativo: Integer): TTreeNode;
begin
  if Noh = nil then
    Result := TvBlocos.Items.Add(Noh, nN)
  else
    Result := TvBlocos.Items.AddChild(Noh, nN);
  Result.ImageIndex := cN;
  //Result.StateIndex := QrBlcsAtivo.Value + Integer(cCBNot);
  Result.StateIndex := Ativo + Integer(cCBNot);
end;


procedure TFmEfdPisCofinsExporta_v01_35.RecriaRegistroEFD_E110;
begin

end;

procedure TFmEfdPisCofinsExporta_v01_35.RecriaRegistroEFD_E520;
begin

end;

procedure TFmEfdPisCofinsExporta_v01_35.RedefinePeriodos;
begin
  if Length(EdMes.Text) = 7 then
  begin
    TPDataIni.Date := Int(Geral.ValidaDataBR(EdMes.ValueVariant, False, False));
    TPDataFim.Date := IncMonth(TPDataIni.Date, 1) - 1;
    //
    FDataIni_Dta := Int(TPDataIni.Date);
    FDataFim_Dta := Int(TPDataFim.Date);
    //
  end;
end;

(*
procedure TFmEfdPisCofinsExporta_v01_35.ReopenCabA_C(Filtra: Boolean; Filtro: Variant);
var
  LinFiltro: String;
begin
  if Filtra then
    LinFiltro := 'AND EFD_EXP_REG = "' + Filtro + '"'
  else
    LinFiltro := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrCabA_C, Dmod.MyDB, [

  'SELECT nfea.*  ',
  '/*FatID, FatNum, Empresa, IDCtrl, Id,  ide_indPag, ide_mod,   ',
  'ide_serie, ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF,  ',
  'ide_tpAmb, ide_finNFe, emit_CNPJ, emit_CPF,   ',
  'dest_CNPJ, dest_CPF,   ',
  'ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,   ',
  'ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete,   ',
  'ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII,   ',
  'ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,   ',
  'ICMSTot_vOutro, ICMSTot_vNF, ISSQNtot_vServ,   ',
  'ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS,   ',
  'ISSQNtot_vCOFINS, RetTrib_vRetPIS, RetTrib_vRetCOFINS,   ',
  'RetTrib_vRetCSLL, RetTrib_vBCIRRF, RetTrib_vIRRF,   ',
  'RetTrib_vBCRetPrev, RetTrib_vRetPrev, ModFrete,   ',
  'Cobr_Fat_nFat, Cobr_Fat_vOrig,   ',
  'Cobr_Fat_vDesc, Cobr_Fat_vLiq, InfAdic_InfAdFisco,   ',
  'InfAdic_InfCpl, Exporta_UFEmbarq, Exporta_XLocEmbarq,   ',
  'Compra_XNEmp, Compra_XPed, Compra_XCont,   ',
  '  ',
  'Status, infProt_Id,   ',
  'infProt_cStat, infCanc_cStat,   ',
  '  ',
  'ICMSRec_pRedBC,   ',
  'ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,   ',
  'IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,   ',
  'IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,   ',
  'PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,   ',
  'COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,   ',
  'DataFiscal, SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta, SINTEGRA_ExpNat,   ',
  'SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta, SINTEGRA_ExpConhNum,   ',
  'SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip, SINTEGRA_ExpPais,   ',
  'SINTEGRA_ExpAverDta, CodInfoEmit, CodInfoDest,   ',
  'CriAForca, ide_hSaiEnt, ide_dhCont,   ',
  'emit_CRT, dest_email,   ',
  'Vagao, Balsa, CodInfoTrsp,   ',
  'OrdemServ, Situacao, Antigo,   ',
  'NFG_Serie, NF_ICMSAlq, NF_CFOP,   ',
  'Importado, NFG_SubSerie, NFG_ValIsen,   ',
  'NFG_NaoTrib, NFG_Outros, COD_MOD, COD_SIT, VL_ABAT_NT,  ',
  'AtrelaFatID, AtrelaFatNum, AtrelaStaLnk, FisRegCad */ ',
  'FROM nfecaba nfea   ',
  ' ',
  'LEFT JOIN efdinnnfscab einc  ',
  '  ON  ',
  '    einc.MovFatID=nfea.AtrelaFatID ',
  '    AND einc.MovFatNum=nfea.AtrelaFatNum ',
  '    AND einc.Empresa=nfea.Empresa ',
  '    AND einc.CHV_NFE=nfea.Id ',
  'WHERE nfea.ide_tpAmb<>2  ',
  'AND nfea.Status IN (100,101,102,110,301)  ',
  'AND nfea.Empresa=' + FEmpresa_Txt,
  'AND nfea.DataFiscal BETWEEN ' +  FIntervaloFiscal,
  'ORDER BY DataFiscal, ide_dEmi, IDCtrl ',
  '']);
  //Geral.MB_Teste(QrCabA_C.SQL.Text);
end;
*)

function TFmEfdPisCofinsExporta_v01_35.ReopenCabA_C(Filtra: Boolean; Filtro:
  Variant): Boolean;
const
  Avisa = True;
var
  //LinFiltro,
  Creditos_Filtro: String;
  Cordas: Array of String;
  Chaves: String;
begin
  Result := False;
  Chaves := '';
  (*if Filtra then
    LinFiltro := 'AND EFD_EXP_REG = "' + Filtro + '"'
  else
    LinFiltro := '';
  *)
   /////////////////////////////////////////////////////////////////////////////////////
  /// O contribuinte que � do Lucro Presumido usa o chamado regime de
  /// incid�ncia cumulativa, onde a base de c�lculo � a receita operacional
  /// bruta, sem dedu��es nas aquisi��es. O regime n�o cumulativo por outro
  /// lado tem a possibilidade de dedu��o dos custos, despesas e encargos na
  /// aquisi��o de mercadorias, desde que respeitando as regras de
  /// apropria��o da legisla��o. O cr�dito, portanto, n�o � abrangente a
  /// todas as aquisi��es.
  ///////////////////////////////////////////////////////////////////////////////////////
  if DModG.QrParamsSPEDRegimPisCofins.Value = CO_RegimPisCofins_1_NaoCumulativo then   //
    //Creditos_Filtro := '' // Busca tudo para cr�ditos                                //
    Creditos_Filtro := 'AND ((nfea.FatID IN (1,50)) OR (nfea.FatID=53 AND frcent.OriTES=1))'   //
  else                                                                                 //
  begin                                                                                //
    //15 B11 tpNF Tipo de Opera��o E B01 N 1-1 1 0=Entrada; 1=Sa�da                    //
    Creditos_Filtro := 'AND (nfea.FatID IN (1,50) AND nfea.ide_tpNF=1)'; // S� notas de sa�da  // 50 neste caso s� para "compatibilidade"
  end;                                                                                 //
  //////////////////////////////////////////////////////////////////////////////////////
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCabA_C, Dmod.MyDB, [
  'SELECT nfea.*,  ',
  'frcent.InfoEFD_PisCofins InfoEFD_PisCofins_Entrada,  ',
  'frcsai.InfoEFD_PisCofins InfoEFD_PisCofins_Saida ',
  'FROM nfecaba nfea    ',
  DmSPED.SQL_LEFT_JOIN_NFe_E_INN(),
  'LEFT JOIN fisregcad frcsai ON frcsai.Codigo=nfea.FisRegCad ',
  'LEFT JOIN fisregcad frcent ON frcent.Codigo=einc.RegrFiscal ',
  'WHERE nfea.ide_tpAmb<>2   ',
  'AND nfea.Status IN (100,101,102,110,301)   ',
  'AND nfea.Empresa=' + FEmpresa_Txt,
  DmSPED.SQL_PeriodoFiscalNFe_e_INN(FIntervaloFiscal),
  Creditos_Filtro,
  'AND ( ',
  //////////////////////////////////// Provis�rio //////////////////////////////
  ///  implementar defini��o de FisRegCad para FatID = VAR_FATID_0050 caso
  ///  use em produ��o algum dia!
  '  (IF(nfea.FatID IN (1,50), frcsai.InfoEFD_PisCofins, frcent.InfoEFD_PisCofins) = 1) ',
  '  OR ',
  '  (nfea.FisRegCad=0) ', // buscar NFes sem regraFiscal FatID=50
  //'  IF(nfea.FatID=50, 1, IF(nfea.FatID = 1, frcsai.InfoEFD_PisCofins, frcent.InfoEFD_PisCofins)) = 1 ',
  //////////////////////////////////// Provis�rio //////////////////////////////
  ') ',
  'ORDER BY nfea.FatID DESC, nfea.ide_dEmi  ',
  '']);
  //Geral.MB_Teste(QrCabA_C.SQL.Text);
  // Corda das NF status 100 para somat�rios
(*  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT nfea.FatNum ',
  'FROM nfecaba nfea    ',
  'WHERE nfea.ide_tpAmb<>2   ',
  'AND nfea.Status = 100   ',
  'AND nfea.Empresa=' + FEmpresa_Txt,
  'AND nfea.ide_dEmi BETWEEN ' +  FIntervaloFiscal,
  'AND FatID IN (' +
  Geral.FF0(VAR_FATID_0001) + ', ' +
  Geral.FF0(VAR_FATID_0050) +
  ') ',
  'ORDER BY nfea.FatID DESC, nfea.ide_dEmi  ',
  '']);
  FCordaSai100 := MyObjects.CordaDeQuery(Dmod.QrAux, 'FatNum', '-999999999');*)
  FCordaSai100 := CordaDeQuery_Sai100();
  //
  QrCabA_C.First;
  while not QrCabA_C.Eof do
  begin
    if QrCabA_CFisRegCad.Value = 0 then
      Chaves := Chaves + QrCabA_CId.Value + sLineBreak;
    QrCabA_C.Next;
  end;
  QrCabA_C.First;
  if Chaves <> EmptyStr then
  begin
    Geral.MB_Aviso('As NFes abaixo n�o tem definida sua regra fiscal.' +
    'O aqrquivo SPED ser� gerado incorretamente!' + sLineBreak +
    Chaves);
  end else
    Result := True;
end;

procedure TFmEfdPisCofinsExporta_v01_35.ReopenEfdInnC500Cab;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnC500Cab, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ',
  'NO_TERC, c500.*',
  'FROM efdinnc500cab c500 ',
  'LEFT JOIN entidades ent ON ent.Codigo=c500.Terceiro',
  'WHERE c500.ImporExpor=3 ',
  'AND c500.AnoMes=' + FAnoMes_Txt,
  'AND c500.Empresa=' + FEmpresa_Txt,
  '']);
  //
end;

procedure TFmEfdPisCofinsExporta_v01_35.ReopenEfdInnC500ItsPIS(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnC500ItsPIS, Dmod.MyDB, [
  'SELECT CST_PIS, ALIQ_PIS, NAT_BC_CRED, COD_CTA,',
  'SUM(VL_ITEM) VL_ITEM, ',
  'SUM(VL_PIS) VL_PIS, SUM(VL_BC_PIS) VL_BC_PIS ',
  'FROM efdinnc500its',
  'WHERE Controle=' + Geral.FF0(Controle),
  'GROUP BY CST_PIS, ALIQ_PIS',
  'ORDER BY CST_PIS, ALIQ_PIS',
  '']);
end;

procedure TFmEfdPisCofinsExporta_v01_35.ReopenEfdInnC500ItsCOFINS(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnC500ItsCOFINS, Dmod.MyDB, [
  'SELECT CST_COFINS, ALIQ_COFINS, NAT_BC_CRED, COD_CTA,',
  'SUM(VL_ITEM) VL_ITEM, ',
  'SUM(VL_COFINS) VL_COFINS, SUM(VL_BC_COFINS) VL_BC_COFINS ',
  'FROM efdinnc500its',
  'WHERE Controle=' + Geral.FF0(Controle),
  'GROUP BY CST_COFINS, ALIQ_COFINS',
  'ORDER BY CST_COFINS, ALIQ_COFINS',
  '']);
end;

procedure TFmEfdPisCofinsExporta_v01_35.ReopenEfdInnCTsCab();
var
  Creditos_Filtro, SQL_Periodo: String;
begin
  SQL_Periodo := 'WHERE ' + FDtFiscalFrete_Entr + ' BETWEEN  ' + FIntervaloFiscal;

   /////////////////////////////////////////////////////////////////////////////////////
  /// O contribuinte que � do Lucro Presumido usa o chamado regime de
  /// incid�ncia cumulativa, onde a base de c�lculo � a receita operacional
  /// bruta, sem dedu��es nas aquisi��es. O regime n�o cumulativo por outro
  /// lado tem a possibilidade de dedu��o dos custos, despesas e encargos na
  /// aquisi��o de mercadorias, desde que respeitando as regras de
  /// apropria��o da legisla��o. O cr�dito, portanto, n�o � abrangente a
  /// todas as aquisi��es.
  ///////////////////////////////////////////////////////////////////////////////////////
  if DModG.QrParamsSPEDRegimPisCofins.Value = CO_RegimPisCofins_1_NaoCumulativo then   //
    Creditos_Filtro := SQL_Periodo                                                     //
  else                                                                                 //
  begin                                                                                //
    //15 B11 tpNF Tipo de Opera��o E B01 N 1-1 1 0=Entrada; 1=Sa�da                    //
    Creditos_Filtro := 'WHERE 1=2'; //Nenhum registro                                  //
  end;                                                                                 //
  //////////////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnCTsCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM efdinnctscab ',
  Creditos_Filtro,
  'ORDER BY ' + FDtFiscalFrete_Entr + ', Controle ',
  '']);
  //Geral.MB_Teste(QrEfdInnCTsCab.SQL.Text);
end;

procedure TFmEfdPisCofinsExporta_v01_35.ReopenEfdInnD500Cab;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnd500Cab, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ',
  'NO_TERC, d500.*',
  'FROM efdinnd500cab d500 ',
  'LEFT JOIN entidades ent ON ent.Codigo=d500.Terceiro',
  'WHERE d500.ImporExpor=3 ',
  'AND d500.AnoMes=' + FAnoMes_Txt,
  'AND d500.Empresa=' + FEmpresa_Txt,
  '']);
end;

procedure TFmEfdPisCofinsExporta_v01_35.ReopenEfdInnD500ItsPIS(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnD500ItsPIS, Dmod.MyDB, [
  'SELECT CST_PIS, ALIQ_PIS, NAT_BC_CRED, COD_CTA,',
  'SUM(VL_ITEM) VL_ITEM, ',
  'SUM(VL_PIS) VL_PIS, SUM(VL_BC_PIS) VL_BC_PIS ',
  'FROM efdinnd500its',
  'WHERE Controle=' + Geral.FF0(Controle),
  'GROUP BY CST_PIS, ALIQ_PIS',
  'ORDER BY CST_PIS, ALIQ_PIS',
  '']);
  //Geral.MB_Teste(QrEfdInnD500Its.SQL.Text);
end;

procedure TFmEfdPisCofinsExporta_v01_35.ReopenEfdInnD500ItsCOFINS(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEfdInnD500ItsCOFINS, Dmod.MyDB, [
  'SELECT CST_COFINS, ALIQ_COFINS, NAT_BC_CRED, COD_CTA,',
  'SUM(VL_ITEM) VL_ITEM, ',
  'SUM(VL_COFINS) VL_COFINS, SUM(VL_BC_COFINS) VL_BC_COFINS ',
  'FROM efdinnd500its',
  'WHERE Controle=' + Geral.FF0(Controle),
  'GROUP BY CST_COFINS, ALIQ_COFINS',
  'ORDER BY CST_COFINS, ALIQ_COFINS',
  '']);
  //Geral.MB_Teste(QrEfdInnD500Its.SQL.Text);
end;

function TFmEfdPisCofinsExporta_v01_35.ReopenCampos(var Res: Boolean): Boolean;
const
  IniDir = 'C:\Dermatek\Downloads\';
  Arquivo = 'Tabelas_SPED_EFD.SQL';
  Titulo = 'Carregamento de tabelas SPED EFD';
  Filtro = '';
var
  //Arq: String;
  I, StateIndex: Integer;
  Noh: TTreeNode;
begin
  // Verificar se os campos foram cadastrados
  if ((FBloco = '0') and (FRegistro = '0000'))
  or ((FBloco = '9') and (FRegistro = '9999')) then
  //or ((FBloco = 'B') and (FRegistro = 'B001')) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCampos, DmodG.AllID_DB, [
    'SELECT * FROM spedefdpiscofinsflds ',
    'WHERE Bloco="' + FBloco + '" ',
    'AND Registro="' + FRegistro + '"',
    'ORDER BY Numero ',
    '']);
    if QrCampos.RecordCount = 0 then
    begin
      Geral.MB_ERRO('Nenhum campo do Registro ' + FRegistro + ' do Bloco ' +
      FBloco + ' foi localizado na base de dados!');
      if Geral.MB_Pergunta('Deseja carregar os campos na base e dados?') = ID_YES then
      begin
        FmPrincipal.AGBLaySPEDEFDClick(Self);
        Close;
        Exit;
      end;
    end;
  end
  else
  begin
    // Ver se foi selecionado
    for I := 0 to TVBlocos.Items.Count -1 do
    begin
      Noh := TVBlocos.Items[I];
      if (Noh.Text = FRegistro) then
      begin
        StateIndex := Noh.StateIndex;
        Break;
      end;
    end;
    if not (StateIndex in ([1(*Check Cinza*), 3(*Check Preto*)])) then Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCampos, DmodG.AllID_DB, [
    'SELECT fld.*  ',
    'FROM spedefdpiscofinsflds fld ',
    'LEFT JOIN ' + VAR_MyPID_DB_NOME + '.' + FSPEDEFD_Blcs + ' blc ON blc.Bloco=fld.Bloco ',
    '  AND blc.Registro=fld.Registro ',
    'WHERE fld.Bloco="' + FBloco + '"  ',
    'AND fld.Registro="' + FRegistro + '" ',
    //'AND blc.StateIndex IN (1,3) ',
    'ORDER BY fld.Numero  ',
    '']);
    //Geral.MB_SQL(Self, QrCampos);
  end;
  //
  Result := QrCampos.RecordCount > 0;
  Res := Result;
  if not Result then
  begin
    Geral.MB_ERRO('Nenhum campo do Registro ' + FRegistro + ' do Bloco ' +
    FBloco + ' foi localizado na base de dados!');
    if Geral.MB_Pergunta('Deseja carregar os campos na base e dados?') = ID_YES then
    begin
      FmPrincipal.AGBLaySPEDEFDClick(Self);
      Close;
      Exit;
    end;
    //
  end else
  begin
    // Abrir s� os campos v�lidos
    UnDmkDAC_PF.AbreMySQLQuery0(QrCampos, DmodG.AllID_DB, [
      'SELECT * FROM spedefdpiscofinsflds ',
      'WHERE Bloco="' + FBloco + '" ',
      'AND Registro="' + FRegistro + '"',
      'AND VersaoIni <=' + FVersao,
      'AND (VersaoFim=0 OR VersaoFim>=' + FVersao + ') ',
      'ORDER BY Numero ',
      '']);
    //
    Result := QrCampos.RecordCount > 0;
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.ReopenDocumentos(): Boolean;
const
  Avisa = True;
var
  Cordas: Array of String;
  FCordaDe0050: String;
begin
  /////////////////// NFe /////////////////////////////////////////////////////
  Result := ReopenCabA_C(False, Null);
  //
  SetLength(Cordas, 3);
  MyObjects.CordaDeQuery_Mul(QrCabA_C, 'FatID', 'FatNum', [VAR_FATID_0001,
    VAR_FATID_0050, VAR_FATID_0053], ['-999999999', '-999999999', '-999999999'], Cordas, Avisa);
  FCordaDe0001 := MyObjects.CordaDeQuery_Mul_SelcionaItem(Cordas, 0);
  FCordaDe0050 := MyObjects.CordaDeQuery_Mul_SelcionaItem(Cordas, 1);
  FCordaDe0053 := MyObjects.CordaDeQuery_Mul_SelcionaItem(Cordas, 2);
  FCordaDe0001 := FCordaDe0001 + ',' + FCordaDe0050;

  ///////////////// CTe //////////////////////////////////////////////////////
  ReopenEfdInnCTsCab();
  FCordaDeCTes := MyObjects.CordaDeQuery(QrEfdInnCTsCab, 'Controle', '-999999999');

  /////////////////     //////////////////////////////////////////////////////

end;

procedure TFmEfdPisCofinsExporta_v01_35.ReopenEmpresa();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresa, Dmod.MyDB, [
  'SELECT Tipo, Filial, ENumero, PNumero, ELograd, PLograd, ECEP, PCEP,  ',
  'IE, ECodMunici, PCodMunici, NIRE, SUFRAMA, ',
  'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOME_ENT,  ',
  'IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJ_CPF,  ',
  'IF(en.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,  ',
  'IF(en.Tipo=0, en.Fantasia, en.Apelido) NO_2_ENT,  ',
  'IF(en.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,  ',
  'IF(en.Tipo=0, en.ERua, en.PRua) RUA,  ',
  'IF(en.Tipo=0, en.ECompl, en.PCompl) COMPL, ',
  'IF(en.Tipo=0, en.EBairro, en.PBairro) BAIRRO, ',
  'IF(en.Tipo=0, en.ETe1, en.PTe1) TE1, ',
  'IF(en.Tipo=0, en.EFax, en.PFax) FAX, ',
  'IF(en.Tipo=0, en.EEmail, en.PEmail) EMAIL, ',
  'en.Codigo ',
  'FROM entidades en  ',
  'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
  'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
  'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
  'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
  'WHERE en.Codigo=' + Geral.FF0(FEmpresa_Int),
  '']);
end;

procedure TFmEfdPisCofinsExporta_v01_35.ReopenEnder(Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEnder, Dmod.MyDB, [
  'SELECT Tipo, ENumero, PNumero, ELograd, PLograd,  ',
  'ECEP, PCEP, IE, ECodMunici, PCodMunici, ',
  ' NIRE, SUFRAMA, ECodiPais, PCodiPais, ',
  'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOME_ENT,  ',
  'IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJ_CPF,  ',
  'IF(en.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,  ',
  'IF(en.Tipo=0, en.Fantasia, en.Apelido) NO_2_ENT,  ',
  'IF(en.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,  ',
  'IF(en.Tipo=0, en.ERua, en.PRua) RUA,  ',
  'IF(en.Tipo=0, en.ECompl, en.PCompl) COMPL, ',
  'IF(en.Tipo=0, en.EBairro, en.PBairro) BAIRRO, ',
  'IF(en.Tipo=0, en.ETe1, en.PTe1) TE1, ',
  'IF(en.Tipo=0, en.EFax, en.PFax) FAX, ',
  'IF(en.Tipo=0, en.EEmail, en.PEmail) EMAIL, ',
  'en.Codigo ',
  'FROM entidades en  ',
  'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
  'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
  'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
  'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
  'WHERE en.Codigo=' + Geral.FF0(Entidade),
  '']);
end;

procedure TFmEfdPisCofinsExporta_v01_35.ReopenSumM210();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumM210, Dmod.MyDB, [
  'SELECT c170.CST_PIS, _110.COD_INC_TRIB, pem.RegimPisCofins,  ',
  'SUM(c170.VL_ITEM) VL_ITEM, SUM(VL_BC_PIS) VL_BC_PIS, ALIQ_PIS, ',
  'SUM(QUANT_BC_PIS) QUANT_BC_PIS, ALIQ_PIS_QUANT,  ',
  '/*SUM(c170.VL_PIS) VL_PIS, */ ',
  'SUM(VL_BC_PIS) * ALIQ_PIS / 100 VL_CONT_APUR, ',
  '/*0.00 VL_AJUS_ACRES,  ',
  '0.00 VL_AJUS_REDUC,  ',
  '0.00 VL_CONT_DIFER,  ',
  '0.00 VL_CONT_DIFER_ANT,  *?',
  '/*SUM(c170.VL_PIS) VL_PIS, */ ',
  'SUM(VL_BC_PIS) * ALIQ_PIS / 100 VL_CONT_PER, ',
  ' ',
  'CASE  ',
  '  WHEN c170.CST_PIS=01 AND _110.COD_INC_TRIB IN (1,3) AND pem.RegimPisCofins=1 THEN "01" ',
  '  WHEN c170.CST_PIS=01 AND _110.COD_INC_TRIB IN (2,3) AND pem.RegimPisCofins=2 THEN "51" ',
  ' ',
  '  WHEN c170.CST_PIS=02 AND _110.COD_INC_TRIB IN (1,3) AND pem.RegimPisCofins=1 THEN "02" ',
  '  WHEN c170.CST_PIS=02 AND _110.COD_INC_TRIB IN (2) AND pem.RegimPisCofins=2 THEN "52" ',
  ' ',
  '  WHEN c170.CST_PIS=03 AND _110.COD_INC_TRIB IN (1,3) AND pem.RegimPisCofins=1 THEN "03" ',
  '  WHEN c170.CST_PIS=03 AND _110.COD_INC_TRIB IN (2) AND pem.RegimPisCofins=2 THEN "53" ',
  '  WHEN c170.CST_PIS=05 AND c170.ALIQ_PIS = 0.65 THEN "31" ',
  '  WHEN c170.CST_PIS=05 AND c170.ALIQ_PIS <> 0.65 THEN "32" ',
  'ELSE "" ',
  'END COD_CONT,  ',
  'c010.LinArq, c010.IND_ESCRI,  ',
  'c100.IND_OPER ',
  '  ',
  'FROM spedefdpiscofinsc170 c170 ',
  'LEFT JOIN spedefdpiscofinsc100 c100 ',
  '  ON c100.LinArq=c170.C100 ',
  '  AND c100.ImporExpor=' + FIE_TXT,
  '  AND c100.Empresa=' + FEmpresa_Txt,
  '  AND c100.AnoMes=' + FAnoMes_Txt,
  'LEFT JOIN spedefdpiscofinsc010 c010 ',
  '  ON c010.LinArq=c100.C010 ',
  '  AND c010.ImporExpor=' + FIE_TXT,
  '  AND c010.Empresa=' + FEmpresa_Txt,
  '  AND c010.AnoMes=' + FAnoMes_Txt,
  'LEFT JOIN spedefdpiscofins0110 _110 ',
  '  ON 1=1 ',
  '  AND _110.ImporExpor=' + FIE_TXT,
  '  AND _110.Empresa=' + FEmpresa_Txt,
  '  AND _110.AnoMes=' + FAnoMes_Txt,
  'LEFT JOIN paramsemp pem ON pem.Codigo=' + FEmpresa_Txt,
  'WHERE c170.ImporExpor=' + FIE_TXT,
  'AND c170.Empresa=' + FEmpresa_Txt,
  'AND c170.AnoMes=' + FAnoMes_Txt,
  'AND c100.IND_OPER=1 ',
  'AND ( ',
  '     (c100.COD_MOD=55 ',
  '     AND ',
  '     c010.IND_ESCRI=2) ',
  '   OR ',
  '     (c100.COD_MOD<>55) ',
  ')  ',
  'AND c170.CST_PIS IN (0,1,2,3,5) ',
  ' ',
  'GROUP BY COD_CONT, ALIQ_PIS_QUANT, ALIQ_PIS ',
  '']);
  //Geral.MB_Teste(QrSumM210.SQL.Text);
end;

procedure TFmEfdPisCofinsExporta_v01_35.ReopenSumM610();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumM610, Dmod.MyDB, [
  'SELECT c170.CST_COFINS, _110.COD_INC_TRIB, pem.RegimPisCofins,  ',
  'SUM(c170.VL_ITEM) VL_ITEM, SUM(VL_BC_COFINS) VL_BC_COFINS, ALIQ_COFINS, ',
  'SUM(QUANT_BC_COFINS) QUANT_BC_COFINS, ALIQ_COFINS_QUANT,  ',
  '/*SUM(c170.VL_COFINS) VL_COFINS, */ ',
  'SUM(VL_BC_COFINS) * ALIQ_COFINS / 100 VL_CONT_APUR, ',
  '/*0.00 VL_AJUS_ACRES,  ',
  '0.00 VL_AJUS_REDUC,  ',
  '0.00 VL_CONT_DIFER,  ',
  '0.00 VL_CONT_DIFER_ANT,  *?',
  '/*SUM(c170.VL_COFINS) VL_COFINS, */ ',
  'SUM(VL_BC_COFINS) * ALIQ_COFINS / 100 VL_CONT_PER, ',
  ' ',
  'CASE  ',
  '  WHEN c170.CST_COFINS=01 AND _110.COD_INC_TRIB IN (1,3) AND pem.RegimPisCofins=1 THEN "01" ',
  '  WHEN c170.CST_COFINS=01 AND _110.COD_INC_TRIB IN (2,3) AND pem.RegimPisCofins=2 THEN "51" ',
  ' ',
  '  WHEN c170.CST_COFINS=02 AND _110.COD_INC_TRIB IN (1,3) AND pem.RegimPisCofins=1 THEN "02" ',
  '  WHEN c170.CST_COFINS=02 AND _110.COD_INC_TRIB IN (2) AND pem.RegimPisCofins=2 THEN "52" ',
  ' ',
  '  WHEN c170.CST_COFINS=03 AND _110.COD_INC_TRIB IN (1,3) AND pem.RegimPisCofins=1 THEN "03" ',
  '  WHEN c170.CST_COFINS=03 AND _110.COD_INC_TRIB IN (2) AND pem.RegimPisCofins=2 THEN "53" ',
  '  WHEN c170.CST_COFINS=05 AND c170.ALIQ_COFINS = 0.65 THEN "31" ',
  '  WHEN c170.CST_COFINS=05 AND c170.ALIQ_COFINS <> 0.65 THEN "32" ',
  'ELSE "" ',
  'END COD_CONT,  ',
  'c010.LinArq, c010.IND_ESCRI,  ',
  'c100.IND_OPER ',
  '  ',
  'FROM spedefdpiscofinsc170 c170 ',
  'LEFT JOIN spedefdpiscofinsc100 c100 ',
  '  ON c100.LinArq=c170.C100 ',
  '  AND c100.ImporExpor=' + FIE_TXT,
  '  AND c100.Empresa=' + FEmpresa_Txt,
  '  AND c100.AnoMes=' + FAnoMes_Txt,
  'LEFT JOIN spedefdpiscofinsc010 c010 ',
  '  ON c010.LinArq=c100.C010 ',
  '  AND c010.ImporExpor=' + FIE_TXT,
  '  AND c010.Empresa=' + FEmpresa_Txt,
  '  AND c010.AnoMes=' + FAnoMes_Txt,
  'LEFT JOIN spedefdpiscofins0110 _110 ',
  '  ON 1=1 ',
  '  AND _110.ImporExpor=' + FIE_TXT,
  '  AND _110.Empresa=' + FEmpresa_Txt,
  '  AND _110.AnoMes=' + FAnoMes_Txt,
  'LEFT JOIN paramsemp pem ON pem.Codigo=' + FEmpresa_Txt,
  'WHERE c170.ImporExpor=' + FIE_TXT,
  'AND c170.Empresa=' + FEmpresa_Txt,
  'AND c170.AnoMes=' + FAnoMes_Txt,
  'AND c100.IND_OPER=1 ',
  'AND ( ',
  '     (c100.COD_MOD=55 ',
  '     AND ',
  '     c010.IND_ESCRI=2) ',
  '   OR ',
  '     (c100.COD_MOD<>55) ',
  ')  ',
  'AND c170.CST_COFINS IN (0,1,2,3,5) ',
  ' ',
  'GROUP BY COD_CONT, ALIQ_COFINS_QUANT, ALIQ_COFINS ',
  '']);
  //Geral.MB_Teste(QrSumM610.SQL.Text);
end;

procedure TFmEfdPisCofinsExporta_v01_35.ReopenVersoes();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVersao, DModG.AllID_DB, [
  'SELECT CodTxt ',
  'FROM tbspedcods132 ',
  'WHERE (DataFim >="' + FDataFim_Txt + '") ',
  'OR (DataIni <"' + FDataFim_Txt + '" AND DataFim<2) ',
  '']);
end;

procedure TFmEfdPisCofinsExporta_v01_35.RGTIPO_ESCRITClick(Sender: TObject);
begin
  HabilitaBotaoOK();
end;

function TFmEfdPisCofinsExporta_v01_35.SalvaArquivo(): Boolean;
var
  dIni, dFim, Arq, Dir, Arquivo: String;
  Continua: Boolean;
begin
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando dados em arquivo!');
  //
  dIni := FormatDateTime('YYYYMMDD', TPDataIni.Date);
  dFim := FormatDateTime('YYYYMMDD', TPDataFim.Date);
  Arq := QrEmpresaCNPJ_CPF.Value + '_' + dIni + '_' + dFim;
  Dir := '';
  Dir := DmSPED.QrParamsEmpSPED_EFD_Path.Value;
  if Trim(Dir) = '' then
    Dir := 'C:\Dermatek\SPED\EFD';
  Dir := Dir + '\Contribui��es';
  Arquivo := dmkPF.CaminhoArquivo(Dir, Arq, 'txt');
  if FileExists(Arquivo) then
    Continua := Geral.MB_Pergunta('O arquivo "' + Arquivo +
    ' j� existe. Deseja sobrescrev�-lo?') = ID_YES
  else
    Continua := True;
  if Continua then
  begin
    //if MLAGeral.ExportaMemoToFileExt(MeGerado, Arquivo, True, False, True, 1310, Null) then
    if MyObjects.ExportaMemoToFileExt(MeGerado, Arquivo, True, False, etxtsaNaoMuda, 1310, Null) then
    begin
      Result := True;
      StatusBar.Panels[3].Text := Arquivo;
      //
      Geral.MB_Aviso('O arquivo digital foi salvo como: ' + sLineBreak
      + sLineBreak + Arquivo + sLineBreak + sLineBreak);
    end;
  end;
end;

procedure TFmEfdPisCofinsExporta_v01_35.SbImplementadoClick(Sender: TObject);
begin
  ConfiguraTreeView2(
  [
    '0200', '0400', '0500', 'C170', 'C190', 'C590', 'D590', 'K200', 'K280'
  ],
  [
    '0', '0001', '0002', '0005', '0110', '0100', '0140', '0150', '0190', '0200', '0400', '0500', '0900', '0990',
    'A', 'A001', 'A990',
    'C', 'C001', 'C010', 'C100', 'C170', 'C500', 'C501', 'C505', 'C990',
    'D', 'D001', 'D010', 'D100', 'D101', 'D105', 'D500', 'D501', 'D505', 'D990',
    'F', 'F001', 'F990',
    'I', 'I001', 'I990',
    'M', 'M001', 'M200', 'M205', 'M210', 'M400', 'M410', 'M600', 'M605', 'M610', 'M800', 'M810', 'M990',
    'P', 'P001', 'P990',
    '1', '1001', '1990',
    '9', '9001', '9900', '9990', '9999'
    ]);
  //
  FConfigurouRegistros := True;
end;

procedure TFmEfdPisCofinsExporta_v01_35.SbTeste_minimo_EFD_PIS_COFINSClick(Sender: TObject);
begin
  ConfiguraTreeView2(
  [

  ],
  [
    '0', '0001', '0100', '0110', '0140', '0150', '0190', '0200', '0400', '0500', '0900', '0990',
    'A', 'A001', 'A990',
    'C', 'C001', 'C990',
    'D', 'D001', 'D990',
    'F', 'F001', 'F990',
    'I', 'I001', 'I990',
    'M', 'M001', 'M990',
    'P', 'P001', 'P990',
    '1', '1001', '1010', '1990',
    'H', 'H001', 'H990',
    '9', '9001', '9900', '9990', '9999'
  ]);
  //
  FConfigurouRegistros := True;
end;

function TFmEfdPisCofinsExporta_v01_35.SelReg(Reg: String): Boolean;
var
  I: Integer;
  Noh: TTreeNode;
begin
  Result := False;
  for I := 0 to TVBlocos.Items.Count -1 do
  begin
    Noh := TVBlocos.Items[I];
    if Uppercase(Noh.Text) = Uppercase(Reg) then
    begin
      //Geral.MB_Info(Noh.Text + sLineBreak + 'StateIndex = ' +
      //Geral.FF0(Noh.StateIndex) + sLineBreak + 'ImageIndex = ' + Geral.FF0(Noh.ImageIndex));
      Result := Noh.StateIndex in ([1,3]);
      Exit;
    end;
  end;
end;

function TFmEfdPisCofinsExporta_v01_35.TamanhoDifereDoObrigatorio(LODC: String; Valor: String): Boolean;
begin
  if QrCamposTObrig.Value = 1 then
  begin
    if CampoObrigatorio(LODC) then
      Result := Length(Valor) <> QrCamposTam.Value
    else
      Result := (Length(Valor) <> 0) and ((Length(Valor) <> QrCamposTam.Value));
  end else
    Result := False;
end;

function TFmEfdPisCofinsExporta_v01_35.TamanhoMuitoGrande(Valor: String): Boolean;
begin
  if QrCamposTam.Value = 0 then
    Result := Length(Valor) > 255
  else
    Result := Length(Valor) > QrCamposTam.Value;
end;

procedure TFmEfdPisCofinsExporta_v01_35.TVBlocosClick(Sender: TObject);
var
  P: TPoint;
  Nivel, (*Codigo,*) Ativa: Integer;
  cNx: String;
begin
  GetCursorPos(P);
  P := TVBlocos.ScreenToClient(P);
  if (htOnStateIcon in TVBlocos.GetHitTestInfoAt(P.X,P.Y)) then
  begin
    if TVBlocos.Selected.StateIndex = 2 then
      Ativa := 1
    else
      Ativa := 0;
    //
    Nivel := 5 - MyObjects.ToggleTreeViewCheckBoxes_Child(TVBlocos.Selected);
    cNx := Geral.FF0(Nivel);
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE ' + FSPEDEFD_Blcs + ' SET ');
    DModG.QrUpdPID1.SQL.Add('Ativo=' + Geral.FF0(Ativa));
    DModG.QrUpdPID1.SQL.Add('WHERE Registro="' + TVBlocos.Selected.Text + '"');
    DModG.QrUpdPID1.ExecSQL;
    //
  end;
end;

procedure TFmEfdPisCofinsExporta_v01_35.TVBlocosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_SPACE) and Assigned(TVBlocos.Selected) then
    MyObjects.ToggleTreeViewCheckBoxes_Child(TVBlocos.Selected);
end;
procedure TFmEfdPisCofinsExporta_v01_35.VerificaDESCR_ITEM(DESCR_ITEM: String;
  Query: TmySQLQuery);
begin
  if Trim(DESCR_ITEM) = '' then
    (*//*)Geral.MB_SQL(Self, Query);
end;


{ TODO : Atualiza tabelas EFD na web }


{

DROP TABLE IF EXISTS _Copy_Bloco_Flds_;
CREATE TABLE _Copy_Bloco_Flds_
SElECT * FROM spedefdpiscofinsflds
WHERE
(
  RegIstro='C195'
OR
  RegIstro='C197'
)
;
UPDATE _Copy_Bloco_Flds_
SET Bloco="D"
;
UPDATE _Copy_Bloco_Flds_
SET REGISTRO="D195"
WHERE REGISTRO="D195"
;
UPDATE _Copy_Bloco_Flds_
SET REGISTRO="D197"
WHERE REGISTRO="D197"
;
SELECT * FROM _Copy_Bloco_Flds_;
INSERT INTO spedefdpiscofinsflds
SELECT * FROM _Copy_Bloco_Flds_;

}

end.

