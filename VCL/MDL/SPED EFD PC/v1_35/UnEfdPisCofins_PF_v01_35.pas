unit UnEfdPisCofins_PF_v01_35;


interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) dmkEdit, dmkRadioGroup,
  dmkMemo, dmkCheckGroup, UnDmkProcFunc, TypInfo, UnMyObjects,
  dmkEditDateTimePicker, SPED_Listas, UnMyVCLRef;

type
  TMyArrOf2ArrOfInt = array of array[0..1] of Integer;
  //
  TUnEfdpiscofins_PF_v01_35 = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function  EncerraMesSPED(ImporExpor, AnoMes, Empresa: Integer; Campo:
              String): Boolean;
    function  LiberaAcaoVS_SPED(ImporExpor, AnoMes, Empresa: Integer; EstagioVS_SPED:
              TEstagioVS_SPED): Boolean;
    function  SPEDEFDEnce_AnoMes(Campo: String): Integer;
  end;
var
  Efdpiscofins_PF_v01_35: TUnEfdpiscofins_PF_v01_35;

implementation

uses UnMLAGeral, MyDBCheck, DmkDAC_PF, Module, ModuleGeral, UnDmkWeb, Restaura2,
  UMySQLModule, UnGrade_PF, ModProd;


{ TUnEfdpiscofins_PF_v01_35 }

function TUnEfdpiscofins_PF_v01_35.EncerraMesSPED(ImporExpor, AnoMes,
  Empresa: Integer; Campo: String): Boolean;
const
  Valor = 1;
var
  Qry: TmySQLQuery;
  Nome: String;
  MovimXX, GerLibEFD, EnvioEFD: Integer;
  SQLType: TSQLType;
begin
  if VAR_USUARIO <> -1 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
  end;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM spedefdpiscofinsence ',
    'WHERE ImporExpor=' + Geral.FF0(AnoMes),
    'AND AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
    if Qry.RecordCount > 0 then
      SQLType      := stUpd
    else
      SQLType      := stIns;
    //AnoMes         := ;
    //Empresa        := ;
    //Nome           := ;
    //MovimXX        := ;
    //GerLibEFD      := ;
    //EnvioEFD       := ;

    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'spedefdpiscofinsence', False, [
    Campo], [
    'ImporExpor', 'AnoMes', 'Empresa'], [
    Valor], [
    ImporExpor, AnoMes, Empresa], True);
  finally
    Qry.Free;
  end;
end;

function TUnEfdpiscofins_PF_v01_35.LiberaAcaoVS_SPED(ImporExpor, AnoMes,
  Empresa: Integer; EstagioVS_SPED: TEstagioVS_SPED): Boolean;
var
  FldPre, FldPos, AskPre, AskPos: String;
  Continua: Boolean;
  AMPre, AMPos: Integer;
begin
  Result := False;
  AMPre := 0;
  AMPos := 0;
  case EstagioVS_SPED of
    TEstagioVS_SPED.evsspedEncerraVS:
    begin
      FldPre := 'MovimXX';
      FldPos := 'GerLibEFD';
      AskPre :=
      'O movimento de couros ainda n�o foi encerrado para o per�odo selecionado.'
      + sLineBreak +
      'Ap�s o encerramento os novos lan�amentos feitos com data anterior ao per�odo ser�o lan�ados em SPED posterior como ajuste!'
      + sLineBreak + 'Deseja encerrar o per�odo?';
    end;
    TEstagioVS_SPED.evsspedGeraSPED:
    begin
      FldPre := 'GerLibEFD';
      FldPos := 'EnvioEFD';
      AskPre :=
      'A gera��o do SPED ainda n�o foi liberada para o per�odo selecionado.'
      + sLineBreak + 'Deseja liberar?';
    end;
    TEstagioVS_SPED.evsspedExportaSPED:
    begin
      FldPre := 'EnvioEFD';
      FldPos := 'EncerraEFD';
      AskPre :=
      'A exporta��o do SPED ainda n�o foi liberada para o per�odo selecionado.'
      + sLineBreak + 'Deseja liberar?';
    end;
    //TEstagioVS_SPED.evsspedNone:
    else
    begin
      FldPre := '';
      FldPos := '';
      AskPre := '';
      AskPos := '';
    end;
  end;
  //
  AMPre := SPEDEFDEnce_AnoMes(FldPre);
  AMPos := SPEDEFDEnce_AnoMes(FldPos);
  //
  if AMPre < AnoMes then
  begin
    if Geral.MB_Pergunta(AskPre) = ID_Yes then
    begin
      Continua := EncerraMesSPED(ImporExpor, AnoMes, Empresa, FldPre);
      if Continua then
      begin
        // reconfirmar que criou!
        AMPre := SPEDEFDEnce_AnoMes(FldPre);
        Continua := AMPre >= AnoMes;
      end;
    end else
      Continua := False;
  end;
  if Continua then
  begin
    Continua := AMPos < AnoMes;
  end;
  if not Continua then
  begin
    case EstagioVS_SPED of
      TEstagioVS_SPED.evsspedEncerraVS: ;
      TEstagioVS_SPED.evsspedGeraSPED:  ;
      TEstagioVS_SPED.evsspedExportaSPED: Continua := AMPre = AnoMes;
      else ;
    end;
  end;
  Result := Continua;
end;

function TUnEfdpiscofins_PF_v01_35.SPEDEFDEnce_AnoMes(Campo: String): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(AnoMes) AnoMes ',
    'FROM spedefdpiscofinsence ',
    'WHERE ' + Campo + '=1 ', // 1=Feito
    '']);
    Result := Qry.FieldByName('AnoMes').AsInteger;
  finally
    Qry.Free;
  end;
end;

end.
