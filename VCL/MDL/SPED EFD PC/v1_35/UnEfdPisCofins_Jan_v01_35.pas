unit UnEfdPisCofins_Jan_v01_35;


interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) dmkEdit, dmkRadioGroup,
  dmkMemo, dmkCheckGroup, UnDmkProcFunc, TypInfo, UnMyObjects,
  dmkEditDateTimePicker;

type
  TUnEfdPisCofins_Jan_v01_35 = class(TObject)
  private
    { Private declarations }
    procedure MsgVersao_EFD_ICMS_IPI_NaoImplementada();

  public
    { Public declarations }
    procedure MostraFormEfdPisCofinsExporta();
    //
  end;
var
  EfdPisCofins_Jan_v01_35: TUnEfdPisCofins_Jan_v01_35;

implementation

uses UnMLAGeral, MyDBCheck, DmkDAC_PF, Module, ModuleGeral, UnDmkWeb, Restaura2,
  UMySQLModule, UnGrade_PF,
  (*EFD_C001_v01_35,*)
///////////////////////////     SPED PIS COFINS    ///////////////////////////////
  EfdPisCofinsExporta_v01_35; (*EfdPisCofinsE001_v01_35, EfdPisCofinsEnce_v01_35;*)
///////////////////////////     SPED PIS COFINS    ///////////////////////////////
  //ModAppGraG1;


{ TUnEfdPisCofins_Jan_v01_35 }

procedure TUnEfdPisCofins_Jan_v01_35.MostraFormEfdPisCofinsExporta;
begin
  if DBCheck.CriaFm(TFmEfdPisCofinsExporta_v01_35, FmEfdPisCofinsExporta_v01_35, afmoNegarComAviso) then
  begin
    FmEfdPisCofinsExporta_v01_35.EdEmpresa.ValueVariant := 1;
    FmEfdPisCofinsExporta_v01_35.CBEmpresa.KeyValue := 1;
    FmEfdPisCofinsExporta_v01_35.RGPreenche.ItemIndex := 2;
    FmEfdPisCofinsExporta_v01_35.EdMes.ValueVariant := Date - 21;
    FmEfdPisCofinsExporta_v01_35.ShowModal;
    FmEfdPisCofinsExporta_v01_35.Destroy;
  end;

end;

procedure TUnEfdPisCofins_Jan_v01_35.MsgVersao_EFD_ICMS_IPI_NaoImplementada;
begin
  Geral.MB_Aviso('Vers�o de SPED EFD PIS/COFINS n�o implementada!');
end;

end.
