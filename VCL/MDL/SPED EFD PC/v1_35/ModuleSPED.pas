unit ModuleSPED;

interface

uses
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables,
  Windows, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  DBCtrls, ComCtrls, UnSPED_Geral, Variants,
  UnDmkProcFunc, DmkDAC_PF, UnDmkEnums, UnGrade_Jan, UnAppPF,
  SPED_Listas, Vcl.Menus, UnSPED_PF, UnInternalConsts;

type
  TDmSPED = class(TDataModule)
    QrAtrela0053: TMySQLQuery;
    QrAtrela0053NFe_FatNum: TIntegerField;
    QrAtrela0053FatID: TIntegerField;
    QrAtrela0053FatNum: TIntegerField;
    QrAtrela0053Empresa: TIntegerField;
    QrAtrela0053ide_dEmi: TDateField;
    QrAtrela0053NFe_FatID: TIntegerField;
    QrAtrela0053DataFiscalNul: TDateField;
    QrAtrela0053DataFiscalInn: TDateField;
    QrParamsEmp: TMySQLQuery;
    QrParamsEmpSPED_EFD_IND_PERFIL: TWideStringField;
    QrParamsEmpSPED_EFD_IND_ATIV: TSmallintField;
    QrParamsEmpSPED_EFD_CadContador: TIntegerField;
    QrParamsEmpSPED_EFD_CRCContador: TWideStringField;
    QrParamsEmpSPED_EFD_EscriContab: TIntegerField;
    QrParamsEmpSPED_EFD_EnderContab: TSmallintField;
    QrParamsEmpSPED_EFD_Path: TWideStringField;
    QrParamsEmpNO_CTD: TWideStringField;
    QrParamsEmpNO_CTB: TWideStringField;
    QrParamsEmpCPF_CTD: TWideStringField;
    QrParamsEmpCNPJ_CTB: TWideStringField;
    QrParamsEmpSPED_EFD_ID_0200: TSmallintField;
    QrParamsEmpSPED_EFD_ID_0150: TSmallintField;
    QrParamsEmpSPED_EFD_DtaFiscalSaida: TSmallintField;
    QrParamsEmpSPED_EFD_DtaFiscalEntrada: TSmallintField;
    QrCorrige002: TMySQLQuery;
    QrCorrige002FatID: TIntegerField;
    QrCorrige002FatNum: TIntegerField;
    QrCorrige002Empresa: TIntegerField;
    QrCorrige002IDCtrl: TIntegerField;
    QrCorrige002ide_mod: TSmallintField;
    QrCorrige002ide_serie: TIntegerField;
    QrCorrige002ide_nNF: TIntegerField;
    QrCorrige002ide_dEmi: TDateField;
    QrCorrige002ide_dSaiEnt: TDateField;
    QrCorrige002ide_finNFe: TSmallintField;
    QrCorrige002emit_CNPJ: TWideStringField;
    QrCorrige002emit_CPF: TWideStringField;
    QrCorrige002Status: TIntegerField;
    QrCorrige002NFG_Serie: TWideStringField;
    QrCorrige002NFG_SubSerie: TWideStringField;
    QrCorrige002COD_MOD: TWideStringField;
    QrCorrige002CodInfoEmit: TIntegerField;
    QrCorrige002ide_tpNF: TSmallintField;
    QrCorrige002DataFiscal: TDateField;
    QrCorrige002CodInfoDest: TIntegerField;
    QrForca: TMySQLQuery;
    QrForcaDataFiscal: TDateField;
    QrForcaCriAForca: TSmallintField;
    QrForcaFatID: TIntegerField;
    QrForcaFatNum: TIntegerField;
    QrForcaEmpresa: TIntegerField;
    QrForcanItem: TIntegerField;
    QrForcaGGX_NfeI: TIntegerField;
    QrForcaprod_uTrib: TWideStringField;
    QrForcaGGX_SMIA: TIntegerField;
    QrGGX: TMySQLQuery;
    QrGGXUnidMed: TIntegerField;
    QrGGXSigla: TWideStringField;
    QrCorrige001: TMySQLQuery;
    QrCorrige001FatID: TIntegerField;
    QrCorrige001FatNum: TIntegerField;
    QrCorrige001Empresa: TIntegerField;
    QrCorrige001nItem: TIntegerField;
    QrCorrige001prod_cProd: TWideStringField;
    QrCorrige1x3: TMySQLQuery;
    QrCorrige1x3FatID: TIntegerField;
    QrCorrige1x3FatNum: TIntegerField;
    QrCorrige1x3Empresa: TIntegerField;
    QrCorrige1x3nItem: TIntegerField;
    QrCorrige1x3Nivel1: TIntegerField;
    QrParamsEmpSPED_EFD_IND_NAT_PJ: TWideStringField;
    QrAux: TMySQLQuery;
  private
    { Private declarations }
    procedure ReabreAtrela0053(JaAtrelados: TSimNao; FDtFiscal0053_Entr,
              FEmpresa_txt, FIntervaloFiscal: String);
  public
    { Public declarations }
    function  CorrigeCOD_SIT(FEmpresa_Int, FAnoMes_Int: Integer; FIntervaloFiscal: String;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel; MeErros: TMemo): Boolean;
    function  CorrigeDataFiscal(FDtFiscal0053_Entr, FEmpresa_txt,
              FIntervaloFiscal: String;
              LaAviso1, LaAviso2: TLabel): Boolean;
    function  CorrigeItensCraidosAForca(PB1: TProgressBar; LaAviso1, LaAviso2: TLabel): Boolean;
    function  CorrigeNotasFiscaiDeTerceiros(LaAviso1, LaAviso2: TLabel): Boolean;
    function  CorrigeNotasFiscaiProprias(PB1: TProgressBar; LaAviso1, LaAviso2: TLabel): Boolean;
    //function  CorrigeItensDeMateriaPrima(): Boolean;
    //procedure CorrigeUnidMedNfeItsI();
    //
    procedure ReopenParamsEmp(FEmpresa_Txt: String);
    //
    function  SQL_LEFT_JOIN_NFe_E_INN(UsaTMeuDB: Boolean = False): String;
    function  SQL_PeriodoFiscalNFe_e_INN(FIntervaloFiscal: String): String;

  end;

var
  DmSPED: TDmSPED;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TDmSPED }

uses
  UnMyObjects, UMySQLModule,
  Module, ModuleGeral;

function TDmSPED.CorrigeCOD_SIT(FEmpresa_Int, FAnoMes_Int: Integer; FIntervaloFiscal: String;
  PB1: TProgressBar; LaAviso1, LaAviso2: TLabel; MeErros: TMemo): Boolean;
const
  Texto = 'Corrigindo o c�digo da situa��o dos documentos fiscais';
var
  Finalidade: TFinalidadeEmissaoDocFiscal;
  COD_SIT, NUM_DOC: Integer;
  tpNF, IND_OPER, IND_EMIT, COD_MOD, SER: String;
  EhE: Boolean;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, Texto);
  Result := True;
  (*
  UnDmkDAC_PF.AbreMySQLQuery0(QrCorrige002, Dmod.MyDB, [
  'SELECT CodInfoEmit, CodInfoDest, ',
  'ide_tpNF, DataFiscal, ',
  'FatID, FatNum, Empresa, IDCtrl, ide_mod, ',
  'ide_serie, ide_nNF, ide_dEmi, ide_dSaiEnt, ',
  'ide_finNFe, emit_CNPJ, emit_CPF, Status, ',
  'NFG_Serie, NFG_SubSerie, COD_MOD ',
  'FROM nfecaba nfea ',
  'WHERE ide_tpAmb<>2 ',
  'AND COD_SIT in (98,99) ',
  'AND Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND DataFiscal BETWEEN  ' + FIntervaloFiscal,
  '']);
  *)
  UnDmkDAC_PF.AbreMySQLQuery0(QrCorrige002, Dmod.MyDB, [
  'SELECT nfea.CodInfoEmit, nfea.CodInfoDest, ',
  'nfea.ide_tpNF, nfea.DataFiscal, ',
  'nfea.FatID, nfea.FatNum, nfea.Empresa, nfea.IDCtrl, nfea.ide_mod, ',
  'nfea.ide_serie, nfea.ide_nNF, nfea.ide_dEmi, nfea.ide_dSaiEnt, ',
  'nfea.ide_finNFe, nfea.emit_CNPJ, nfea.emit_CPF, nfea.Status, ',
  'nfea.NFG_Serie, nfea.NFG_SubSerie, nfea.COD_MOD ',
  'FROM nfecaba nfea ',
  DmSPED.SQL_LEFT_JOIN_NFe_E_INN(),
  'WHERE nfea.ide_tpAmb<>2 ',
  'AND nfea.COD_SIT in (98,99) ',
  'AND nfea.Empresa=' + Geral.FF0(FEmpresa_Int),
  DmSPED.SQL_PeriodoFiscalNFe_e_INN(FIntervaloFiscal),
  '']);
  //Geral.MB_SQL(self, QrCorrige002);
  PB1.Position := 0;
  PB1.Max := QrCorrige002.RecordCount;
  //
  QrCorrige002.First;
  while not QrCorrige002.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, Texto + '. Item ' + FormatFloat('0',
    QrCorrige002.RecNo) + ' de ' + Geral.FF0(QrCorrige002.RecordCount));
    //
    case QrCorrige002ide_finNFe.Value of
      01: Finalidade := fedfNormal;
      02: Finalidade := fedfComplementar;
      03: Finalidade := fedfAjuste;
    end;
    //
    EhE := QrCorrige002CodInfoEmit.Value = FEmpresa_Int;
    tpNF := Geral.FF0(QrCorrige002ide_tpNF.Value);
    IND_OPER := dmkPF.EscolhaDe2Str(EhE, tpNF, '0');
    IND_EMIT := dmkPF.EscolhaDe2Str(EhE, '0', '1');
    COD_MOD := FormatFloat('00', QrCorrige002ide_mod.Value);
    if (COD_MOD <> '01') and (COD_MOD <> '55') then
      COD_MOD := QrCorrige002COD_MOD.Value;

    if QrCorrige002NFG_Serie.Value <> '' then
      SER := QrCorrige002NFG_Serie.Value
    else SER := Geral.FF0(QrCorrige002ide_serie.Value );
    NUM_DOC := QrCorrige002ide_nNF.Value;
    if SPED_Geral.Obtem_SPED_COD_SIT_de_NFe_Status(IND_OPER, IND_EMIT,
    QrCorrige002Status.Value, QrCorrige002ide_dEmi.Value,
    QrCorrige002ide_dSaiEnt.Value, QrCorrige002DataFiscal.Value, COD_MOD, SER,
    NUM_DOC, QrCorrige002FatID.Value, QrCorrige002FatNum.Value,
    QrCorrige002CodInfoEmit.Value, QrCorrige002CodInfoDest.Value, Finalidade,
    FAnoMes_Int, COD_SIT, MeErros)
    then
    begin
      if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
      'COD_SIT'], ['IDCtrl'
      ], [COD_SIT], [QrCorrige002IDCtrl.Value], True) then
        Result := False;
    end else
      Result := False;
    //
    QrCorrige002.Next;
  end;
end;

function TDmSPED.CorrigeDataFiscal(FDtFiscal0053_Entr, FEmpresa_txt,
  FIntervaloFiscal: String;
  LaAviso1, LaAviso2: TLabel): Boolean;
var
  QtdeIni, QtdeFim: Integer;
  Qry: TmySQLQuery;
  Texto, Campo, Corda: String;
  //
  procedure ReopenErros_ID001();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ide_dSaiEnt,  ide_dEmi, DataFiscal ',
    'FROM nfecaba ',
    //'WHERE FatID=' + Geral.FF0(VAR_FatID_0001),
    'WHERE FatID IN (' +
    Geral.FF0(VAR_FATID_0001) + ', ' +
    Geral.FF0(VAR_FATID_0050) +
    ') ',
    'AND DataFiscal <> ' + Campo,
    'AND ' + Campo + ' BETWEEN  ' + FIntervaloFiscal,
    '']);
  end;
begin
  Result := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo Data Fiscal!');
  //Texto := '';
  //
  // Notas da empresa
  Qry := TmySQLQuery.Create(Dmod);
  try
    case QrParamsEmpSPED_EFD_DtaFiscalSaida.Value of
      1: Campo := 'ide_dEmi';
      2: Campo := 'ide_dSaiEnt';
      else Campo := 'ide_?????';
    end;
    ReopenErros_ID001();
    QtdeIni := Qry.RecordCount;
    if QtdeIni > 0 then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
      'UPDATE nfecaba ',
      'SET DataFiscal=' + Campo,
      //'WHERE FatID=' + Geral.FF0(VAR_FatID_0001),
      'WHERE FatID IN (' +
      Geral.FF0(VAR_FATID_0001) + ', ' +
      Geral.FF0(VAR_FATID_0050) +
      ') ',
      'AND DataFiscal <> ' + Campo,
      'AND ' + Campo + ' BETWEEN  ' + FIntervaloFiscal,
      '']);
      ReopenErros_ID001();
      QtdeFim := Qry.RecordCount;
      Result := QtdeFim = 0;
      if QtdeFim < QtdeIni then
      begin
        Texto := Geral.FF0(QtdeIni - QtdeFim) +
        ' registros tiveram sua data fiscal corrigida!' + sLinebreak;
        if QtdeFim > 0 then
        begin
          Texto := Texto + 'Mas ' + Geral.FF0(QtdeFim) +
          ' registros n�o forma corrigidos!';
        end;
        Geral.MB_ERRO(Texto);
      end;
    end else
      Result := True;
  finally
    Qry.Free;
  end;
  // Notas de terceiros
  ReabreAtrela0053(TSimNao.snNao, FDtFiscal0053_Entr, FEmpresa_txt, FIntervaloFiscal); // Parei aqui! Mudar para sim depois?????
  QtdeIni := QrAtrela0053.RecordCount;
  if QtdeIni > 0 then
  begin
    QrAtrela0053.First;
    //
    while not QrAtrela0053.Eof do
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE nfecaba ',
      'SET DataFiscal="' + Geral.FDT(QrAtrela0053DataFiscalInn.Value, 1) + '"',
      'WHERE FatID=' + Geral.FF0(QrAtrela0053FatID.Value),
      'AND FatNum=' + Geral.FF0(QrAtrela0053FatNum.Value),
      'AND Empresa=' + Geral.FF0(QrAtrela0053Empresa.Value),
      'AND DataFiscal < "1900-01-01"',
      '']);
      //
      QrAtrela0053.Next;
    end;
    //
    ReabreAtrela0053(TSimNao.snNao, FDtFiscal0053_Entr, FEmpresa_txt, FIntervaloFiscal); // Parei aqui! Mudar para sim depois?????
    QtdeFim := QrAtrela0053.RecordCount;
    Result := QtdeFim = 0;
    if QtdeFim < QtdeIni then
    begin
      Texto := Geral.FF0(QtdeIni - QtdeFim) +
      ' registros de notas de terceiros tiveram sua data fiscal corrigida!' +
      sLinebreak;
      if QtdeFim > 0 then
      begin
        Texto := Texto + 'Mas ' + Geral.FF0(QtdeFim) +
        ' registros n�o forma corrigidos!';
      end;
      Geral.MB_ERRO(Texto);
    end;
  end;
end;

function TDmSPED.CorrigeItensCraidosAForca(PB1: TProgressBar; LaAviso1, LaAviso2: TLabel): Boolean;
var
  prod_uCom, prod_uTrib: String;
  GraGruX, UnidMedCom, UnidMedTrib, FatID, FatNum, Empresa, nItem: Integer;
begin
  //Result := False;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo itens criados a for�a');
    QrForca.Close;
    UnDmkDAC_PF.AbreQuery(QrForca, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
    PB1.Position := 0;
    PB1.Max := QrForca.RecordCount;
    //
    while not QrForca.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Corrigindo item criados a for�a: ' +
      FormatFloat('', QrForca.RecNo));
      //
      QrGGX.Close;
      QrGGX.Params[0].AsInteger := QrForcaGGX_SMIA.Value;
      UnDmkDAC_PF.AbreQuery(QrGGX, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
      if QrGGX.RecordCount > 0 then
      begin
        prod_uCom   := QrGGXSigla.Value;
        prod_uTrib  := QrGGXSigla.Value;
        GraGruX     := QrForcaGGX_SMIA.Value;
        UnidMedCom  := QrGGXUnidMed.Value;
        UnidMedTrib := QrGGXUnidMed.Value;
        FatID       := QrForcaFatID.Value;
        FatNum      := QrForcaFatNum.Value;
        Empresa     := QrForcaEmpresa.Value;
        nItem       := QrForcanItem.Value;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeitsi', False, [
        (*'prod_cProd', 'prod_cEAN', 'prod_xProd',
        'prod_NCM', 'prod_EXTIPI', 'prod_genero',
        'prod_CFOP',*) 'prod_uCom', (*'prod_qCom',
        'prod_vUnCom', 'prod_vProd', 'prod_cEANTrib',*)
        'prod_uTrib', (*'prod_qTrib', 'prod_vUnTrib',
        'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
        'Tem_IPI', '_Ativo_', 'InfAdCuztm',
        'EhServico', 'ICMSRec_pRedBC', 'ICMSRec_vBC',
        'ICMSRec_pAliq', 'ICMSRec_vICMS', 'IPIRec_pRedBC',
        'IPIRec_vBC', 'IPIRec_pAliq', 'IPIRec_vIPI',
        'PISRec_pRedBC', 'PISRec_vBC', 'PISRec_pAliq',
        'PISRec_vPIS', 'COFINSRec_pRedBC', 'COFINSRec_vBC',
        'COFINSRec_pAliq', 'COFINSRec_vCOFINS', 'MeuID',
        'Nivel1', 'prod_vOutro', 'prod_indTot',
        'prod_xPed', 'prod_nItemPed',*) 'GraGruX',
        'UnidMedCom', 'UnidMedTrib'], [
        'FatID', 'FatNum', 'Empresa', 'nItem'], [
        (*prod_cProd, prod_cEAN, prod_xProd,
        prod_NCM, prod_EXTIPI, prod_genero,
        prod_CFOP,*) prod_uCom, (*prod_qCom,
        prod_vUnCom, prod_vProd, prod_cEANTrib,*)
        prod_uTrib, (*prod_qTrib, prod_vUnTrib,
        prod_vFrete, prod_vSeg, prod_vDesc,
        Tem_IPI, _Ativo_, InfAdCuztm,
        EhServico, ICMSRec_pRedBC, ICMSRec_vBC,
        ICMSRec_pAliq, ICMSRec_vICMS, IPIRec_pRedBC,
        IPIRec_vBC, IPIRec_pAliq, IPIRec_vIPI,
        PISRec_pRedBC, PISRec_vBC, PISRec_pAliq,
        PISRec_vPIS, COFINSRec_pRedBC, COFINSRec_vBC,
        COFINSRec_pAliq, COFINSRec_vCOFINS, MeuID,
        Nivel1, prod_vOutro, prod_indTot,
        prod_xPed, prod_nItemPed,*) GraGruX,
        UnidMedCom, UnidMedTrib], [
        FatID, FatNum, Empresa, nItem], True);
      end;
      QrForca.Next;
    end;
    Result := True;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Item criados a for�a verificados.');
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDmSPED.CorrigeNotasFiscaiDeTerceiros(LaAviso1, LaAviso2: TLabel): Boolean;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo Reduzido de notas fiscais de terceiros!');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE nfecaba SET Status=infCanc_cStat');
  Dmod.QrUpd.SQL.Add('WHERE Status=0');
  Dmod.QrUpd.SQL.Add('AND infCanc_cStat<>0');
  Dmod.QrUpd.SQL.Add('AND Empresa<>CodInfoEmit');
  Dmod.QrUpd.SQL.Add('AND FatID<>1;');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('UPDATE nfecaba SET Status=infProt_cStat');
  Dmod.QrUpd.SQL.Add('WHERE Status=0');
  Dmod.QrUpd.SQL.Add('AND infCanc_cStat=0');
  Dmod.QrUpd.SQL.Add('AND infProt_cStat<>0');
  Dmod.QrUpd.SQL.Add('AND Empresa<>CodInfoEmit');
  Dmod.QrUpd.SQL.Add('AND FatID<>1;');
  Dmod.QrUpd.SQL.Add('');
(*
  Dmod.QrUpd.SQL.Add('UPDATE nfecaba SET Status=100');
  Dmod.QrUpd.SQL.Add('WHERE Status=0');
  Dmod.QrUpd.SQL.Add('AND infProt_cStat=0');
  Dmod.QrUpd.SQL.Add('AND infCanc_cStat=0');
  Dmod.QrUpd.SQL.Add('AND Empresa<>CodInfoEmit');
  Dmod.QrUpd.SQL.Add('AND FatID<>1;');
*)
  Dmod.QrUpd.ExecSQL;
end;

function TDmSPED.CorrigeNotasFiscaiProprias(PB1: TProgressBar; LaAviso1, LaAviso2: TLabel): Boolean;
const
  sProcName = 'TDmSPED.CorrigeNotasFiscaiProprias()';
var
  //UnidMedCom, UnidMedTrib,
  GraGruX, FatID, FatNum, Empresa, nItem: Integer;
begin
  //Result := False;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo Reduzido de notas fiscais pr�prias!');
(*
    QrCorrige001.Close;
    UnDmkDAC_PF.AbreQuery(QrCorrige001, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorrige001, Dmod.MyDB, [
    'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa,  ',
    'nfei.nItem, nfei.prod_cProd  ',
    'FROM nfeitsi nfei ',
    'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID ',
    '     AND nfea.FatNum=nfei.FatNum ',
    '     AND nfea.Empresa=nfei.Empresa ',
    'WHERE nfea.Empresa=nfea.CodInfoEmit ',
    'AND GraGruX=0 ',
    'AND prod_cProd <> "" ',
    '']);
    PB1.Position := 0;
    PB1.Max := QrCorrige001.RecordCount;
    //
    while not QrCorrige001.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      'Corrigindo item pr�prio: ' + FormatFloat('', QrCorrige001.RecNo));
      //
      FatID := QrCorrige001FatID.Value;
      case FatID of
        VAR_FATID_0001:
        begin
          GraGruX     := StrToInt(QrCorrige001prod_cProd.Value);
          FatID       := QrCorrige001FatID.Value;
          FatNum      := QrCorrige001FatNum.Value;
          Empresa     := QrCorrige001Empresa.Value;
          nItem       := QrCorrige001nItem.Value;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeitsi', False, [
          'GraGruX'], [
          'FatID', 'FatNum', 'Empresa', 'nItem'], [
          GraGruX], [
          FatID, FatNum, Empresa, nItem], True);
        end;
        else
          Geral.MB_Erro('FatID ' + Geral.FF0(FatID) + ' n�o implementado em ' + sProcName);
      end;

      //
      QrCorrige001.Next;
    end;
    Result := True;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Item pr�prios verificados.');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TDmSPED.ReabreAtrela0053(JaAtrelados: TSimNao; FDtFiscal0053_Entr,
FEmpresa_txt, FIntervaloFiscal: String);
var
  SQL_Atralados: String;
begin
  case JaAtrelados of
    TSimNao.snNao: SQL_Atralados := ' NOT ';
    TSimNao.snSim: SQL_Atralados := '';
    else SQL_Atralados := ' ??? ';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtrela0053, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_A_;',
  'CREATE TABLE _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_A_',
  'SELECT NFe_FatID, NFe_FatNum, ' + FDtFiscal0053_Entr + ' DataFiscalInn ',
  'FROM ' + TMeuDB + '.efdinnnfscab',
  'WHERE NFe_FatID=' + Geral.FF0(VAR_FATID_0053),
  'AND  NFe_FatNum<>0',
  'AND Empresa=' + FEmpresa_txt,
  'AND ' + FDtFiscal0053_Entr + ' BETWEEN ' + FIntervaloFiscal,
  ';',
  'DROP TABLE IF EXISTS _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_B_;',
  'CREATE TABLE _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_B_',
  'SELECT FatID, FatNum, Empresa, ide_dEmi, DataFiscal DataFiscalNul',
  'FROM ' + TMeuDB + '.nfecaba',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_0053),
  'AND  FatNum<>0',
  'AND Empresa=' + FEmpresa_txt,
  'AND DataFiscal < "1900-01-01"',
  ';',
  'SELECT b.*, a.*',
  'FROM _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_B_ b',
  'LEFT JOIN _SPED_EFD_CORRIGE_NFE_0053_DATAFISCAL_A_ a ',
  '  ON b.FatNum=a.NFe_FatNum',
  'WHERE ' + SQL_Atralados + ' (a.NFe_FatNum) IS NULL',
  ';']);
  //
  //Geral.MB_Teste(QrAtrela0053.SQL.Text)
end;

procedure TDmSPED.ReopenParamsEmp(FEmpresa_Txt: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DmSPED.QrParamsEmp, Dmod.MyDB, [
  'SELECT pem.SPED_EFD_IND_PERFIL, pem.SPED_EFD_IND_ATIV, ',
  'pem.SPED_EFD_CadContador, pem.SPED_EFD_CRCContador, ',
  'pem.SPED_EFD_EscriContab, pem.SPED_EFD_EnderContab, ',
  'SPED_EFD_Path, SPED_EFD_DtaFiscalSaida, SPED_EFD_DtaFiscalEntrada, ',
  'SPED_EFD_ID_0150, SPED_EFD_ID_0200, ',
  'pem.SPED_EFD_IND_NAT_PJ, ',
  'IF(ctd.Tipo=0, ctd.RazaoSocial, ctd.Nome) NO_CTD, ',
  'ctd.CPF CPF_CTD, ctb.CNPJ CNPJ_CTB, ',
  'IF(ctb.Tipo=0, ctb.RazaoSocial, ctb.Nome) NO_CTB ',
  'FROM paramsemp pem ',
  'LEFT JOIN entidades ctd ON ctd.Codigo=pem.SPED_EFD_CadContador ',
  'LEFT JOIN entidades ctb ON ctb.Codigo=pem.SPED_EFD_EscriContab ',
  'WHERE pem.Codigo=' + FEmpresa_Txt,
  '']);
end;

function TDmSPED.SQL_LEFT_JOIN_NFe_e_INN(UsaTMeuDB: Boolean): String;
var
  NomeDB: String;
begin
  if UsaTMeuDB then
    NomeDB := TMeuDB + '.'
  else
    NomeDB := '';
  //
  Result := Geral.ATS([
  '',
  'LEFT JOIN ' + NomeDB + 'efdinnnfscab einc  ',
  '  ON  ',
  '    einc.MovFatID=nfea.AtrelaFatID ',
  '    AND einc.MovFatNum=nfea.AtrelaFatNum ',
  '    AND einc.Empresa=nfea.Empresa ',
  '    AND einc.CHV_NFE=nfea.Id ',
  '']);
end;

function TDmSPED.SQL_PeriodoFiscalNFe_e_INN(FIntervaloFiscal: String): String;
begin
  //'AND nfea.DataFiscal BETWEEN ' +  FIntervaloFiscal,
  Result := Geral.ATS([
  'AND ( ',
  '  IF(nfea.FatID IN (' +
  Geral.FF0(VAR_FATID_0001) + ', ' +
  Geral.FF0(VAR_FATID_0050) +
  '), nfea.ide_dEmi, einc.DT_E_S) BETWEEN ' +  FIntervaloFiscal,
  ')'
  ]);
end;

(*
function TFmEfdIcmsIpiExporta_v03_0_9.CorrigeItensDeMateriaPrima(): Boolean;
var
  //UnidMedCom, UnidMedTrib,
  GraGruX, FatID, FatNum, Empresa, nItem: Integer;
begin
  //Result := False;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo Reduzido de notas fiscais de mat�ria-prima!');
    UnDmkDAC_PF.AbreMySQLQuery0(QrCorrige1x3, Dmod.MyDB, [
    'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa,  ',
    'nfei.nItem, nfei.Nivel1 ',
    'FROM nfeitsi nfei ',
    'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID ',
    '     AND nfea.FatNum=nfei.FatNum ',
    '     AND nfea.Empresa=nfei.Empresa ',
    'WHERE nfei.FatID IN (103,113) ',
    'AND GraGruX=0 ',
    'AND Nivel1 < 0 ',
    '']);
    PB1.Position := 0;
    PB1.Max := QrCorrige1x3.RecordCount;
    //
    while not QrCorrige1x3.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      'Corrigindo item de mat�ria-prima: ' + FormatFloat('', QrCorrige1x3.RecNo));
      //
      GraGruX     := QrCorrige1x3Nivel1.Value;
      FatID       := QrCorrige1x3FatID.Value;
      FatNum      := QrCorrige1x3FatNum.Value;
      Empresa     := QrCorrige1x3Empresa.Value;
      nItem       := QrCorrige1x3nItem.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeitsi', False, [
      'GraGruX'], [
      'FatID', 'FatNum', 'Empresa', 'nItem'], [
      GraGruX], [
      FatID, FatNum, Empresa, nItem], True);
      //
      QrCorrige1x3.Next;
    end;
    Result := True;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Item de mat�ria-prima verificados.');
  finally
    Screen.Cursor := crDefault;
  end;
end;
*)

(*
procedure TFmEfdIcmsIpiExporta_v03_0_9.CorrigeUnidMedNfeItsI();
var
  FatID, FatNum, Empresa, nItem: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCorrI, Dmod.MyDB, [
  'SELECT nfei.FatID, nfei.FatNum, nfei.Empresa, nfei.nItem, nfei.GraGruX ',
  'FROM nfeitsi nfei ',
  'LEFT JOIN nfecaba nfea ON nfea.FatID=nfei.FatID ',
  '     AND nfea.FatNum=nfei.FatNum ',
  '     AND nfea.Empresa=nfei.EMpresa ',
  'WHERE prod_uTrib="" ',
  'AND  nfea.Empresa=' + FEmpresa_Txt,
  'AND nfea.DataFiscal BETWEEN ' + FIntervaloFiscal,
  '']);
  if QrCorrI.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('Existem ' + Geral.FF0(QrCorrI.RecordCount) +
    ' itens de notas sem a unidade tribut�vel.' + sLineBreak +
    'Deseja atribuir a unidade do cadastro do produto para estes itens?') = ID_YES then
    begin
      QrCorrI.First;
      PB1.Max := QrCorrI.RecordCount;
      //
      while not QrCorrI.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Corrigindo unidade tribut�vel: ' + FormatFloat('', QrCorrI.RecNo));
        //
        DmProd.ReopenProd(QrCorrIGraGruX.Value);
        //
        FatID := QrCorrIFatID.Value;
        FatNum := QrCorrIFatNum.Value;
        Empresa := QrCorrIEmpresa.Value;
        nItem := QrCorrInItem.Value;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeitsi', False, [
        'prod_uTrib'], [
        'FatID', 'FatNum', 'Empresa', 'nItem'], [
        DmProd.QrProdSigla.Value], [
        FatID, FatNum, Empresa, nItem], True);
        //
        QrCorrI.Next;
      end;
      QrCorrI.Close;
      UnDmkDAC_PF.AbreQuery(QrCorrI, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ;
      if QrCorrI.RecordCount > 0 then
      begin
        Geral.MB_Aviso(Geral.FF0(QrCorrI.RecordCount) +
        ' itens de notas sem a unidade tribut�vel n�o foram corrigidos.' + sLineBreak +
        'Verifica a unidade no cadastro do produto!');
      end;
    end;
  end;
end;
*)


end.
