unit SPED_EFD_PC_Tabelas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, Variants, DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmSPED_EFD_PC_Tabelas = class(TForm)
    Panel1: TPanel;
    Panel16: TPanel;
    LaAviso1: TLabel;
    PB1: TProgressBar;
    StatusBar: TStatusBar;
    QrTabelas: TmySQLQuery;
    QrVersao: TmySQLQuery;
    QrVersaoVersao: TWideStringField;
    PB2: TProgressBar;
    LaAviso2: TLabel;
    QrDupl: TmySQLQuery;
    QrDuplCodTxt: TWideStringField;
    QrSorc: TmySQLQuery;
    QrDest: TmySQLQuery;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    CkForcaCad: TCheckBox;
    BtOK: TBitBtn;
    QrSPEDEFDTabT: TmySQLQuery;
    GridPanel1: TGridPanel;
    Panel5: TPanel;
    Label1: TLabel;
    Panel6: TPanel;
    Label4: TLabel;
    Memo1: TMemo;
    Memo2: TMemo;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaDTB_Munici();
    procedure AtualizaBacen_Pais();
    function  EhArquivoValido(Arquivo: String; lstArq: TStringList): Boolean;
    function  ObtemVersaoArquivo(Arquivo: String; lstArq: TStringList): String;
    function  AtualizaTabela(idTabela: Integer; TabelaTxt, Versao: String;
              lstArq: TStringList): Boolean;
  public
    { Public declarations }
  end;

  var
    FmSPED_EFD_PC_Tabelas: TFmSPED_EFD_PC_Tabelas;
  const
    CO_TB_Muni = 4;
    CO_TB_Pais = 6;

implementation

uses Module, UnMyObjects, dmkGeral, ModuleGeral, UMySQLModule, UnInternalConsts,
  UnDmkWeb;

{$R *.DFM}

procedure TFmSPED_EFD_PC_Tabelas.AtualizaBacen_Pais;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrAllUpd, DmodG.AllID_DB, [
  'DELETE FROM bacen_pais;  ',
  'INSERT INTO bacen_pais (Codigo, Nome)  ',
  'SELECT Codigo, Nome  ',
  'FROM tbspedefdpiscofins006 ',
  'WHERE DataFim = "1899-12-30" ',
  'OR DataFim >= SYSDATE();  ',
  '']);
end;

procedure TFmSPED_EFD_PC_Tabelas.AtualizaDTB_Munici;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrAllUpd, DmodG.AllID_DB, [
    'INSERT INTO dtb_munici (Codigo, DTB_UF, CodUsu, Nome) ',
    'SELECT DISTINCT Codigo, SUBSTRING(Codigo, 1, 2) DTB_UF, ',
    'SUBSTRING(Codigo, 3) CodUsu, Nome ',
    //'FROM tbspedefd004 ',
    'FROM tbspedefdpiscofins004 ',
    'WHERE Codigo NOT IN ',
    '( ',
    '  SELECT Codigo ',
    '  FROM dtb_munici ',
    ') ',
    'ON DUPLICATE KEY UPDATE Ativo = 1 ',
    '']);
end;

function TFmSPED_EFD_PC_Tabelas.AtualizaTabela(idTabela: Integer; TabelaTxt,
  Versao: String; lstArq: TStringList): Boolean;
var
  Codigo: Variant;
  DataI, DataF: TDateTime;
  lstLin: TStringList;
  I, P: Integer;
  Achou, Continua: Boolean;
  CodTxt, NomeTb, idTabelaTxt, Linha, Nome, DataIni, DataFim, OGC, teste: String;
begin
  Result      := False;
  idTabelaTxt := FormatFloat('000', idTabela);
  //NomeTb      := 'tbspedefd' + idTabelaTxt;
  NomeTb      := 'tbspedefdpiscofins' + idTabelaTxt;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTabelas, DmodG.AllID_DB, [
    'SHOW TABLES ',
    //'LIKE "tbspedefd%" ',
    'LIKE "tbspedefdpiscofins%" ',
    '']);
  //
  QrTabelas.First;
  while not QrTabelas.Eof do
  begin
    if Lowercase(QrTabelas.Fields[0].AsString) = NomeTb then
    begin
      Achou := True;
      Break;
    end;
    //
    QrTabelas.Next;
  end;
  if not Achou then
  begin
    Geral.MB_Erro('Tabela n�o localizada:' + sLineBreak +
      NomeTb + sLineBreak + 'AVISE A DERMATEK!');
    Exit;
  end;
  MyObjects.Informa(LaAviso1, True, 'Tabela: ' +
    TabelaTxt + '. Arquivo baixado. Atualizando base de dados');
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrAllUpd, DmodG.AllID_DB, [
    'DELETE FROM ' + NomeTb,
    '']);
  //
  PB2.Position := 0;
  PB2.Max := lstArq.Count - 1;
  //
  for I := 1 to lstArq.Count - 1 do
  begin
    Linha := lstArq[I];
    PB2.Position := PB2.Position + 1;
    MyObjects.Informa(LaAviso2, True, 'Inserindo registro: ' + Linha);
    teste := Copy(Linha, 1, 4);
(*
    if teste = '2454' then
      Geral.MB_Info(
        'idTabela: ' + IntToStr(idTabela) + sLineBreak +
        'TabelaTxt: ' + TabelaTxt + sLineBreak +
        'Versao: ' + Versao + sLineBreak +
        Linha);
*)
    //
    P := pos('|', Linha);
    if P > 0 then
    begin
      lstLin := TStringList.Create;
      try
        Geral.MyExtractStrings(['|'], [' '], PChar(Linha + '|'), lstLin);
        CodTxt  := Copy(Linha, 1, P-1);
        // Evitar erro de SQL STRICT TRANS TABLES
        //Nome    := lstLin[00];
(*      ini 2020-02-10
        Nome    := Copy(lstLin[00], 1, 255);
        //
        DataIni := lstLin[01];
        DataFim := '0000000000';
        //
        if lstLin.Count > 2 then
          DataFim := lstLin[02];
*)
        Nome    := '';
        DataIni := '30/12/1899';
        DataFim := '0000000000';
        //
        if lstLin.Count > 0 then
          Nome    := Copy(lstLin[00], 1, 255);
        if lstLin.Count > 1 then
          DataIni := lstLin[01];
        if lstLin.Count > 2 then
          DataFim := lstLin[02];

        //
        DataI    := Geral.ValidaDataBR(DataIni, True, False);
        DataF    := Geral.ValidaDataBR(DataFim, True, False);
        DataIni  := Geral.FDT(DataI, 1);
        DataFim  := Geral.FDT(DataF, 1);
        Continua := True;
        //
        case idTabela of
                                   001..007: Codigo := Geral.IMV(CodTxt);
                                        008: Codigo := 0; // NULL;
                                        009: Codigo := Geral.IMV(CodTxt);
                                        010: Codigo := 0; // NULL;
                                   011..013: Codigo := Geral.IMV(CodTxt);
                                   014, 015: Codigo := 0; // NULL;
                                        016: Codigo := Geral.IMV(CodTxt);
                                        017: Codigo := 0; // NULL;
                                   018, 019: Codigo := Geral.IMV(CodTxt);
                                        020: Codigo := 0; // NULL;
                                   021..032: Codigo := Geral.IMV(CodTxt);
                                   033..060: Codigo := 0; // NULL;
                                   121..124: Codigo := 0; // NULL;
                                        125: Codigo := Geral.IMV(CodTxt);
                                   126..129: Codigo := 0; // NULL;
                                        130: Codigo := Geral.IMV(CodTxt);
          131, 170..173, 175, 176, 179, 180: Codigo := 0; // NULL;
                                   181..189: Codigo := Geral.IMV(CodTxt);
                                        190: Codigo := 0; // NULL;
                                   191..192: Codigo := Geral.IMV(CodTxt);
                                        193: Codigo := 0; // NULL;
                                   194..197: Codigo := Geral.IMV(CodTxt);
                              212, 220..223: Codigo := 0; // NULL;
                                        224: Codigo := Geral.IMV(CodTxt);
                    244..249, 408, 555, 598: Codigo := 0; // NULL;
          else
          begin
            Memo1.Text := 'Tabela n�o implementada: ' + NomeTb + sLineBreak + Memo1.Text;
            Continua   := False;
          end;
        end;
        if Continua then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrDupl, DModG.AllID_DB, [
            'SELECT CodTxt ',
            'FROM ' + NomeTb,
            'WHERE CodTxt="' + CodTxt + '"',
            'AND  DataIni="' + DataIni + '"',
            '']);
          //
          if QrDupl.RecordCount > 0 then
          begin
            Memo1.Text := 'Registro duplicado na tabela ' + TabelaTxt + ': ' + CodTxt;
          end else
          begin
            (*
            case idTabela of
              155:
              begin
                OGC := lstLin[03];
                //
                UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, NomeTb, False, [
                'Codigo', 'Nome', 'DataIni',
                'DataFim', 'OGC'], [
                'CodTxt'], [
                Codigo, Nome, DataIni,
                DataFim, OGC], [
                CodTxt], True);
              end;
              156, 162, 163:
              begin
                NCM              :=              lstLin[03];
                NCM_EXCECAO      :=              lstLin[04];
                EX               :=              lstLin[05];
                ALIQ_PIS_PERC    := Geral.DMV(lstLin[06]);
                ALIQ_COFINS_PERC := Geral.DMV(lstLin[07]);
                //
                UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, NomeTb, False, [
                'Codigo', 'Nome', 'DataIni',
                'DataFim', 'NCM', 'NCM_EXCECAO',
                'EX', 'ALIQ_PIS_PERC', 'ALIQ_COFINS_PERC'], [
                'CodTxt'], [
                Codigo, Nome, DataIni,
                DataFim, NCM, NCM_EXCECAO,
                EX, ALIQ_PIS_PERC, ALIQ_COFINS_PERC], [
                CodTxt], True);
              end;
              164:
              begin
                NCM               :=              lstLin[03];
                NCM_EXCECAO       :=              lstLin[04];
                EX                :=              lstLin[05];
                UNIDADE           :=              lstLin[06];
                ALIQ_PIS_QUANT    := Geral.DMV(lstLin[07]);
                ALIQ_COFINS_QUANT := Geral.DMV(lstLin[08]);
                //
                UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, NomeTb, False, [
                'Codigo', 'Nome', 'DataIni',
                'DataFim', 'NCM', 'NCM_EXCECAO',
                'EX', 'UNIDADE', 'ALIQ_PIS_QUANT',
                'ALIQ_COFINS_QUANT'], [
                'CodTxt'], [
                Codigo, Nome, DataIni,
                DataFim, NCM, NCM_EXCECAO,
                EX, UNIDADE, ALIQ_PIS_QUANT,
                ALIQ_COFINS_QUANT], [
                CodTxt], True);
              end;
              165:
              begin
                NCM               :=              lstLin[03];
                NCM_EXCECAO       :=              lstLin[04];
                EX                :=              lstLin[05];
                ALIQ_PIS_PERC     := Geral.DMV(lstLin[06]);
                ALIQ_PIS_QUANT    := Geral.DMV(lstLin[07]);
                ALIQ_COFINS_PERC  := Geral.DMV(lstLin[08]);
                ALIQ_COFINS_QUANT := Geral.DMV(lstLin[09]);
                //
                UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, NomeTb, False, [
                'Codigo', 'Nome', 'DataIni',
                'DataFim', 'NCM', 'NCM_EXCECAO',
                'EX', 'ALIQ_PIS_PERC', 'ALIQ_PIS_QUANT',
                'ALIQ_COFINS_PERC', 'ALIQ_COFINS_QUANT'], [
                'CodTxt'], [
                Codigo, Nome, DataIni,
                DataFim, NCM, NCM_EXCECAO,
                EX, ALIQ_PIS_PERC, ALIQ_PIS_QUANT,
                ALIQ_COFINS_PERC, ALIQ_COFINS_QUANT], [
                CodTxt], True);
              end;
              166..169:
              begin
                NCM               :=              lstLin[03];
                NCM_EXCECAO       :=              lstLin[04];
                EX                :=              lstLin[05];
                //
                UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, NomeTb, False, [
                'Codigo', 'Nome', 'DataIni',
                'DataFim', 'NCM', 'NCM_EXCECAO',
                'EX'], [
                'CodTxt'], [
                Codigo, Nome, DataIni,
                DataFim, NCM, NCM_EXCECAO,
                EX], [
                CodTxt], True);
              end;
              else
              begin
                if (Codigo = 1058) and (Uppercase(Nome)='BRASIL') then
                   Fbacen_pais := NomeTb;
                if (Codigo = 5300108) and (Lowercase(Nome)='bras�lia') then
                   Fdtb_munici := NomeTb;
                UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, NomeTb, False, [
                'Codigo', 'Nome', 'DataIni', 'DataFim'], [
                'CodTxt'], [
                Codigo, Nome, DataIni, DataFim], [
                CodTxt], True);
              end;
            end;
            *)
            UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, NomeTb, False,
              ['Codigo', 'Nome', 'DataIni', 'DataFim'], ['CodTxt'],
              [Codigo, Nome, DataIni, DataFim], [CodTxt], True);
          end;
        end;
        Result := True;
      finally
        if lstLin <> nil then
          lstLin.Free;
      end;
    end;
  end;
  UMyMod.SQLInsUpd(DModG.QrAllUpd, stUpd, 'spedefdpiscofinstabt', False, [
    'Versao'], ['Controle'], [Versao], [idTabela], True);
  //
  MyObjects.Informa(LaAviso2, False, '...');
end;

procedure TFmSPED_EFD_PC_Tabelas.BitBtn1Click(Sender: TObject);
const
  // I C M S / I P I >>>>>>> 'http://www.sped.fazenda.gov.br/spedtabelas/appconsulta/obterTabelaExterna.aspx?';
  //       http://www.sped.fazenda.gov.br/spedtabelas/AppConsulta/publico/aspx/ConsultaTabelasExternas.aspx?
  DirF = 'http://www.sped.fazenda.gov.br/spedtabelas/appconsulta/obterTabelaExterna.aspx?';
  DirD = CO_DIR_RAIZ_DMK + '\SPED\DownLoadTabsEFD_PIS_COFINS';
  MinP = 1;
  MaxP = 100;
  MinT = 1;
  MaxT = 100;
var
  I: Integer;
  Tabela, Fonte, Destino, Linha, Versao: String;
  lstArq(*, lstLin*): TStringList;

  Arquivo: TStringList;
  Stream: TFileStream;
  idPacote, idTabela: Integer;
  URL, tbVersao, arqVersao, noPacote, noTabela, sPacTab: String;
  //
  p, t: Integer;
begin
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  //
  PB2.Position := 0;
  //
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrSPEDEFDTabT, DmodG.AllID_DB, [
      'SELECT bg.Nome noPacote, bt.Nome noTabela, bt.* ',
      //'FROM spedefdtabt bt ',
      'FROM spedefdpiscofinstabt bt ',
      //'LEFT JOIN spedefdtabg bg ON bg.Codigo = bt.Codigo ',
      'LEFT JOIN spedefdpiscofinstabg bg ON bg.Codigo = bt.Codigo ',
      '']);
    {if QrSPEDEFDTabT.RecordCount > 0 then
    begin}
      if not DirectoryExists(DirD) then
      begin
        if not ForceDirectories(DirD) then
        begin
          Geral.MB_Aviso('N�o foi poss�vel criar o diret�rio tempor�rio:' +
            sLineBreak + DirD + '!');
          Exit;
        end;
      end;
      //
      PB1.Position := 0;
      PB1.Max      := QrSPEDEFDTabT.RecordCount;
      //
      MyObjects.Informa(LaAviso1, False, 'Fazendo download de tabelas!');
      //
      QrSPEDEFDTabT.First;
      (*while not QrSPEDEFDTabT.EOF do
      begin*)

      PB1.Max := MaxP;
      PB1.Position := 0;

        PB2.Max := MaxP * MaxT;
        PB2.Position := 0;

      for p := MinP to MaxP do
      begin
{
        MyObjects.UpdPB(PB1, LaAviso1, nil);

        PB2.Max := MaxT;
        PB2.Position := 0;
}

        for t := MinT to MaxT do
        begin
          idPacote := p; //QrSPEDEFDTabT.FieldByName('Codigo').AsInteger;
          idTabela := t; //QrSPEDEFDTabT.FieldByName('Controle').AsInteger;
          //
          sPacTab  := 'idPacote=' + Geral.FF0(idPacote) + '&idTabela=' + Geral.FF0(idTabela);
          URL      := DirF + sPacTab;
          //
          MyObjects.Informa2EUpdPB(PB2, nil, LaAviso2, False, sPacTab);
          //
          Destino := DirD + '\tb_' + Geral.FF0(idTabela) + '_' + Geral.FF0(idPacote) + '.txt';
          //
          if FileExists(Destino) then
          begin
            if not DeleteFile(Destino) then
            begin
              Geral.MB_Aviso('N�o foi poss�vel remover o arquivo tempor�rio:' +
                sLineBreak + Destino + '!');
              Exit;
            end;
          end;
          //
          Stream  := TFileStream.Create(Destino, fmCreate);
          try
            if not DmkWeb.URLGet(URL, Stream) then
            begin
              Geral.MB_Erro('Falha ao carregar tabelas!');
              Exit;
            end;
            Stream.Free;
          except
            Stream.Free;
            //
            Geral.MB_Erro('Falha ao carregar tabelas!');
            Exit;
          end;
          //






          if FileExists(Destino) then
          begin
            Arquivo   := TStringList.Create;
            try
              Arquivo.LoadFromFile(Destino);
              //
              if EhArquivoValido(Destino, Arquivo) then
              begin
                arqVersao := ObtemVersaoArquivo(Destino, Arquivo);
                //
                Memo1.Lines.Add('Pacote [' + Geral.FF0(p) + '] Tabela [' +
                Geral.FF0(t) + '] Vers�o: ' + arqVersao);
                //
                (*
                if (arqVersao <> tbVersao) or (CkForcaCad.Checked = True) then
                begin
                  if AtualizaTabela(idTabela, noPacote + ' - ' + noTabela, arqVersao, Arquivo) then
                  begin
                    Memo2.Text := Geral.FF0(idPacote) + ' ' + noPacote + ' - ' +
                                    Geral.FF0(idTabela) + ' ' + noTabela +
                                    sLineBreak + Memo2.Text;
                  end;
                  if idTabela = CO_TB_Muni then
                    AtualizaDTB_Munici()
                  else if idTabela = CO_TB_Pais then
                    AtualizaBacen_Pais();
                end;
                *)
              end else
              if FileExists(Destino) then
              begin
                if not DeleteFile(Destino) then
                begin
                  {
                  Geral.MB_Aviso('N�o foi poss�vel remover o arquivo tempor�rio:' +
                    sLineBreak + Destino + '!');
                  Exit;
                  }
                end;
              end;

            finally
              Arquivo.Free;
            end;

          end;









          QrSPEDEFDTabT.Next;
        end;
        PB1.Position := 0;
        PB1.Max      := QrSPEDEFDTabT.RecordCount;
      end;







      //
      (*
      MyObjects.Informa(LaAviso1, False, 'Verificando tabelas!');
      //
      QrSPEDEFDTabT.First;
      while not QrSPEDEFDTabT.EOF do
      begin
        idPacote  := QrSPEDEFDTabT.FieldByName('Codigo').AsInteger;
        idTabela  := QrSPEDEFDTabT.FieldByName('Controle').AsInteger;
        noPacote  := QrSPEDEFDTabT.FieldByName('noPacote').AsString;
        noTabela  := QrSPEDEFDTabT.FieldByName('noTabela').AsString;
        tbVersao  := QrSPEDEFDTabT.FieldByName('Versao').AsString;
        Destino   := DirD + '\tb_' + Geral.FF0(idTabela) + '_' + Geral.FF0(idPacote) + '.txt';
        Arquivo   := TStringList.Create;
        try
          Arquivo.LoadFromFile(Destino);
          //
          arqVersao := ObtemVersaoArquivo(Destino, Arquivo);
          //
          if (arqVersao <> tbVersao) or (CkForcaCad.Checked = True) then
          begin
            if AtualizaTabela(idTabela, noPacote + ' - ' + noTabela, arqVersao, Arquivo) then
            begin
              Memo2.Text := Geral.FF0(idPacote) + ' ' + noPacote + ' - ' +
                              Geral.FF0(idTabela) + ' ' + noTabela +
                              sLineBreak + Memo2.Text;
            end;
            if idTabela = CO_TB_Muni then
              AtualizaDTB_Munici()
            else if idTabela = CO_TB_Pais then
              AtualizaBacen_Pais();
          end;
        finally
          Arquivo.Free;
        end;
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        QrSPEDEFDTabT.Next;
      end;*)
    //end;
  finally
    PB1.Position  := 0;
    Screen.Cursor := crDefault;
    //
    Geral.MB_Aviso('Importa��o finalizada!');
  end;
end;

procedure TFmSPED_EFD_PC_Tabelas.BtOKClick(Sender: TObject);
const
  // I C M S / I P I >>>>>>> 'http://www.sped.fazenda.gov.br/spedtabelas/appconsulta/obterTabelaExterna.aspx?';
  DirF = 'http://www.sped.fazenda.gov.br/spedtabelas/AppConsulta/publico/aspx/ConsultaTabelasExternas.aspx?';
  DirD = CO_DIR_RAIZ_DMK + '\SPED\DownLoadTabsEFD_Contribuicoes';
var
  I: Integer;
  Tabela, Fonte, Destino, Linha, Versao: String;
  lstArq(*, lstLin*): TStringList;

  Arquivo: TStringList;
  Stream: TFileStream;
  idPacote, idTabela: Integer;
  URL, tbVersao, arqVersao, noPacote, noTabela: String;
begin
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  //
  PB2.Position := 0;
  //
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrSPEDEFDTabT, DmodG.AllID_DB, [
      'SELECT bg.Nome noPacote, bt.Nome noTabela, bt.* ',
      //'FROM spedefdtabt bt ',
      'FROM spedefdpiscofinstabt bt ',
      //'LEFT JOIN spedefdtabg bg ON bg.Codigo = bt.Codigo ',
      'LEFT JOIN spedefdpiscofinstabg bg ON bg.Codigo = bt.Codigo ',
      '']);
    if QrSPEDEFDTabT.RecordCount > 0 then
    begin
      if not DirectoryExists(DirD) then
      begin
        if not ForceDirectories(DirD) then
        begin
          Geral.MB_Aviso('N�o foi poss�vel criar o diret�rio tempor�rio:' +
            sLineBreak + DirD + '!');
          Exit;
        end;
      end;
      //
      PB1.Position := 0;
      PB1.Max      := QrSPEDEFDTabT.RecordCount;
      //
      MyObjects.Informa(LaAviso1, False, 'Fazendo download de tabelas!');
      //
      QrSPEDEFDTabT.First;
      while not QrSPEDEFDTabT.EOF do
      begin
        idPacote := QrSPEDEFDTabT.FieldByName('Codigo').AsInteger;
        idTabela := QrSPEDEFDTabT.FieldByName('Controle').AsInteger;
        URL      := DirF + 'idPacote=' + Geral.FF0(idPacote) + '&idTabela=' + Geral.FF0(idTabela);
        //
        Destino := DirD + '\tb_' + Geral.FF0(idTabela) + '_' + Geral.FF0(idPacote) + '.txt';
        //
        if FileExists(Destino) then
        begin
          if not DeleteFile(Destino) then
          begin
            Geral.MB_Aviso('N�o foi poss�vel remover o arquivo tempor�rio:' +
              sLineBreak + Destino + '!');
            Exit;
          end;
        end;
        //
        Stream  := TFileStream.Create(Destino, fmCreate);
        try
          if not DmkWeb.URLGet(URL, Stream) then
          begin
            Geral.MB_Erro('Falha ao carregar tabelas!');
            Exit;
          end;
          Stream.Free;
        except
          Stream.Free;
          //
          Geral.MB_Erro('Falha ao carregar tabelas!');
          Exit;
        end;
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        QrSPEDEFDTabT.Next;
      end;
      PB1.Position := 0;
      PB1.Max      := QrSPEDEFDTabT.RecordCount;
      //
      MyObjects.Informa(LaAviso1, False, 'Verificando tabelas!');
      //
      QrSPEDEFDTabT.First;
      while not QrSPEDEFDTabT.EOF do
      begin
        idPacote  := QrSPEDEFDTabT.FieldByName('Codigo').AsInteger;
        idTabela  := QrSPEDEFDTabT.FieldByName('Controle').AsInteger;
        noPacote  := QrSPEDEFDTabT.FieldByName('noPacote').AsString;
        noTabela  := QrSPEDEFDTabT.FieldByName('noTabela').AsString;
        tbVersao  := QrSPEDEFDTabT.FieldByName('Versao').AsString;
        Destino   := DirD + '\tb_' + Geral.FF0(idTabela) + '_' + Geral.FF0(idPacote) + '.txt';
        Arquivo   := TStringList.Create;
        try
          Arquivo.LoadFromFile(Destino);
          //
          arqVersao := ObtemVersaoArquivo(Destino, Arquivo);
          //
          if (arqVersao <> tbVersao) or (CkForcaCad.Checked = True) then
          begin
            if AtualizaTabela(idTabela, noPacote + ' - ' + noTabela, arqVersao, Arquivo) then
            begin
              Memo2.Text := Geral.FF0(idPacote) + ' ' + noPacote + ' - ' +
                              Geral.FF0(idTabela) + ' ' + noTabela +
                              sLineBreak + Memo2.Text;
            end;
            if idTabela = CO_TB_Muni then
              AtualizaDTB_Munici()
            else if idTabela = CO_TB_Pais then
              AtualizaBacen_Pais();
          end;
        finally
          Arquivo.Free;
        end;
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        QrSPEDEFDTabT.Next;
      end;
    end;
  finally
    PB1.Position  := 0;
    Screen.Cursor := crDefault;
    //
    Geral.MB_Aviso('Importa��o finalizada!');
  end;
end;

procedure TFmSPED_EFD_PC_Tabelas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmSPED_EFD_PC_Tabelas.EhArquivoValido(Arquivo: String;
  lstArq: TStringList): Boolean;
var
  Txt: String;
  Ini, Fim: Integer;
begin
  Result := False;
  Txt := lstArq[0];
  //
  Ini := Pos('vers�o=', Txt);
  //
  Result := Ini > 0;
(*
  if Ini > 0 then
  begin
    Ini := Ini + Length('vers�o=');
    Fim := Pos(' ', Txt);
    //
    if (Ini <> 0) and (Fim <> 0) then
      Result := Copy(Txt, Ini, Fim - Ini)
    else if (Ini <> 0) and (Fim = 0) then
      Result := Copy(Txt, Ini);
  end;
*)
end;

procedure TFmSPED_EFD_PC_Tabelas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSPED_EFD_PC_Tabelas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  //
  StatusBar.Visible := False;
end;

procedure TFmSPED_EFD_PC_Tabelas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmSPED_EFD_PC_Tabelas.ObtemVersaoArquivo(Arquivo: String; lstArq: TStringList): String;
var
  Txt: String;
  Ini, Fim: Integer;
begin
  Result := '';
  Txt := lstArq[0];
  //
  Ini := Pos('vers�o=', Txt);
  //
  if Ini > 0 then
  begin
    Ini := Ini + Length('vers�o=');
    Fim := Pos(' ', Txt);
    //
    if (Ini <> 0) and (Fim <> 0) then
      Result := Copy(Txt, Ini, Fim - Ini)
    else if (Ini <> 0) and (Fim = 0) then
      Result := Copy(Txt, Ini);
  end;
end;

end.
