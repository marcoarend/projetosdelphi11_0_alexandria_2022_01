unit GruUsrEdtIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmGruUsrEdtIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label7: TLabel;
    EdUsuario: TdmkEditCB;
    CBUsuario: TdmkDBLookupComboBox;
    Panel5: TPanel;
    Label2: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    QrUsuarios: TMySQLQuery;
    QrUsuariosNumero: TIntegerField;
    QrUsuariosLogin: TWideStringField;
    DsUsuarios: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmGruUsrEdtIts: TFmGruUsrEdtIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  GruUsrEdtCab;

{$R *.DFM}

procedure TFmGruUsrEdtIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Usuario: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  Usuario        := EdUsuario.ValueVariant;
  //
  if MyObjects.FIC(Usuario = 0, EdUsuario, 'Informe o usu�rio!') then
    Exit;
  //
  if FmGruUsrEdtCab.QrGruUsrEdtIts.Locate('Usuario', Usuario, []) then
  begin
    Geral.MB_Aviso('Usu�rio j� cadastrado para este grupo');
    Exit;
  end;
  //
  Controle := UMyMod.BPGS1I32('gruusredtits', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gruusredtits', False, [
  'Codigo', 'Usuario'], [
  'Controle'], [
  Codigo, Usuario], [
  Controle], True) then
  begin
     FmGruUsrEdtCab.ReopenGruUsrEdtIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdUsuario.ValueVariant   := 0;
      CBUsuario.KeyValue       := Null;
    end else Close;
  end;
end;

procedure TFmGruUsrEdtIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGruUsrEdtIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGruUsrEdtIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrUsuarios, Dmod.MyDB);
end;

procedure TFmGruUsrEdtIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
